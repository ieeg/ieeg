function hfostabadetector(userName)
  %HFOSTABADETECTOR  HFO detector as described by Staba et. al 2002
  %   High frequency oscillation detector adapted from:
  %   Staba RJ, Wilson CL, Bragin A, Fried I, Engel J Jr.
  %   Quantitative analysis of high-frequency oscillations (80-500 Hz) recorded
  %   in human epileptic hippocampus and entorhinal cortex.
  %   J Neurophysiol. 2002 Oct;88(4):1743-52.
  %
  %   This version was adapted from an algorithm provided by the University of
  %   Freiburg. It was altered by J.B. Wagenaar, and Z. Ives for efficiency, and
  %   compatibility with the international epilepsy database. Parameters that
  %   were provided in the original code were unaltered by both editors.
  %   Additional alterations by BH Brinkmann, Mayo foundation, primarily bug
  %   fixes.
  
  TOOLID    = '85e651d6-c293-41db-8c98-d3c086fb8634';
    
  % Initiate session
  ieegconnect(userName, TOOLID);
  st      = ieegstatus;               % Get all status information.
  sf      = st.params.sampleFreq;     % Get the sampling frequency of the data.

  % Getting first block of data.
  [values, startT] = ieeggetblock;       
  nrChannels = length(st.taskParams.chIds); % Get number of channels.
  
  % Analyze data until all blocks/tasks are analyzed. 
  while ~isempty(values)
    for iChannel = 1: nrChannels
      
      % Do data analysis on single channel
      endT    = startT + size(values,1)*sf;
      results   = stabadetect(values(:,iChannel), startT, endT);
      
      % Check which spikes are bigger than threshold.
      if ~iesempty(results)
        results.chIndex = iChannel;
        ieegannotate(results);
      end
    end
    
    % Get the next block of data.
    [values, startT] = ieeggetblock; 
  end
  
  %Finish job, and get snapshot id for results.
  ieegclearjob(); 
  
end

function results = stabadetect(values, startT)
  % STABADETECT  Analyzes a single channel of data for HFO's.
  


  % Calculating a running RMS value
  NrSmoothingSamples = 100;
  dRunningRMS         = zeros(length(values),1);
  for i = NrSmoothingSamples: length(values)
    dRunningRMS(i) = sqrt( mean(values((i-NrSmoothingSamples+1):i).^2 ));
  end
    
  % prelim HFO detection, in case treshhold is exceeded
  lMinInterval  = 15;  % 15 milisecond 
  lInCounter    = 0;
  dThres = mean(dRunningRMS) + std(dRunningRMS)*6;

  nDetectionCounter = 0;
  MinIndex2 = 0;
  for n = 1: length(deegValues)
    
    if n < MinIndex2  + 2
       continue; 
    end    
  
    % Reset counter when dRunningRMS falls below threshold.
    if dRunningRMS(n) > dThres 
      lInCounter = lInCounter + 1;
    else
      lInCounter = 0;
    end    

    if lInCounter > lMinInterval

      % look for mimum before
      dMin = 1e35;
      MinIndex1 = 0;
      lIntStart = n - (2 * lMinInterval);
      if lIntStart < 0
        lIntStart = 0;
      end    
      for lMin = lIntStart : n
        if dRunningRMS(lMin) < dMin
          dMin = dRunningRMS(lMin);
          MinIndex1 = lMin;
        end
      end


      % look for minimum after
      dMin = 1e35;
      MinIndex2 = 0;
      lIntEnd = n + (2*lMinInterval);
      if lIntEnd > length(dRunningRMS)-1
         lIntEnd =  length(dRunningRMS)-1;
      end
      for lMin=n : lIntEnd
        if dRunningRMS(lMin) < dMin
          dMin = dRunningRMS(lMin);
          MinIndex2 = lMin;
        end
      end

      % Somehow set a minimum length for the HFO to 5 samples.
      if MinIndex2 - n < 5
          MinIndex2 = MinIndex2 + 5;
      end      

      nDetectionCounter = nDetectionCounter + 1;
      Detections(nDetectionCounter).channel   = channel; %#ok<AGROW>
      Detections(nDetectionCounter).start     = MinIndex1; %#ok<AGROW>
      Detections(nDetectionCounter).stop      = MinIndex2; %#ok<AGROW>
      
      %Reset counter for threshold.
      lInCounter = 0;
    end
  end
    
  if nDetectionCounter > 0

    % join nearby detections
    joinedDetections = joinDetections(Detections);

    % check for sufficient number of oscillations
    results = checkOscillations(joinedDetections, values, mean(abs(values)), ...
      std(abs(values)), channel);
    
  else
    % create one detection with invalid members
    results(1).chIdex =  channel;
    results(1).start  =  -1;
    results(1).stop   =  -1;  
  end
    
end
 
function joinedDetections = joinDetections(Detections)
            
  maxIntervalToJoin = 10;
  nOrigDetections    = length(Detections);

  % fill result with first detection
  joinedDetections = struct('channel','','start','','stop','');
  joinedDetections(1).channel   =  Detections(1).channel;
  joinedDetections(1).start    =  Detections(1).start;
  joinedDetections(1).stop  =  Detections(1).stop;
  nDetectionCounter = 1;

  for n = 2 : nOrigDetections
    % join detection
    if Detections(n).start > joinedDetections(nDetectionCounter).start
      nDiff = Detections(n).start - joinedDetections(nDetectionCounter).stop;
      if nDiff < maxIntervalToJoin        
        joinedDetections(nDetectionCounter).stop = Detections(n).stop; 

      else
        nDetectionCounter = nDetectionCounter + 1;
        joinedDetections(nDetectionCounter).channel =  Detections(n).channel; 
        joinedDetections(nDetectionCounter).start =  Detections(n).start; 
        joinedDetections(nDetectionCounter).stop =  Detections(n).stop;      
      end
    end
  end
end
 
function checkedOscillations = checkOscillations(Detections, deegValues, ...
  AbsoluteMean, AbsoluteStd, channel)
 
  minNumberOscillatins = 5;
  dFactor = 3.0;
  nDetectionCounter = 0;
  
  fprintf('Number of detections: %d',length(Detections));
  

  for n = 1 : length(Detections)
    % get EEG for interval     
    intervalEEG = deegValues(Detections(n).start : Detections(n).stop);
    
    % compute abs values for oscillation interval 
    absEEG = abs(intervalEEG);

    % look for zeros
    nZeros = 0;
    zeroVec = [];
    for ii = 1:length(intervalEEG)-1
      if ( (intervalEEG(ii) > 0.0 && intervalEEG(ii+1) < 0.0)) || ...
          ((intervalEEG(ii) < 0.0 && intervalEEG(ii+1) > 0.0))
        nZeros = nZeros + 1;
        zeroVec(nZeros) = ii; %#ok<AGROW>
      end 
    end
    
    nMaxCounter = 0;
    if nZeros > 0
      % look for maxima with sufficient amplitude between zeros
      for ii = 1 : nZeros-1
        lStart = zeroVec(ii);
        lEnd   = zeroVec(ii+1);
        dMax = max(absEEG(lStart:lEnd));

        if dMax > AbsoluteMean + dFactor * AbsoluteStd;
          nMaxCounter = nMaxCounter + 1;
        end 
      end
    end   

    if nMaxCounter + 1 > minNumberOscillatins
      nDetectionCounter = nDetectionCounter + 1;

      checkedOscillations(nDetectionCounter).channel  =  Detections(n).channel; %#ok<AGROW>
      checkedOscillations(nDetectionCounter).start    =  Detections(n).start; %#ok<AGROW>
      checkedOscillations(nDetectionCounter).stop     =  Detections(n).stop; %#ok<AGROW>
    end
  end
  
  if nDetectionCounter < 1
     % create one detection with invalid members
     checkedOscillations(1).channel =  channel;
     checkedOscillations(1).start =  -1;
     checkedOscillations(1).stop =  -1;  
  end
end 


