function [values, startIndex] = ieeggetblock()
  %IEEGGETBLOCK  Returns a block of data for the current job.
  %   [VALUES, STARTINDEX] = IEEGGETBLOCK() returns the next available block of
  %   data for the current job. It automatically requests a new task when the
  %   previous task has ended. 
  %
  %   Depending on the settings associated with the tool, the returned datablock
  %   can either be a subset of data from a single channel, or a subset of data
  %   from all channels. In the latter case, the VALUES argument will be a
  %   2-dimensional array with channels as the second dimension. the STARTINDEX
  %   is a numeric variable that marks the timestamp of the first index of the
  %   VALUES array in microseconds.
  % 
  %   It is recommended that the user calls this method in a while loop until
  %   no more blocks are available. In this case, the method will return an
  %   empty matrix.
  %
  %   see also: IEEGCONNECT IEEGSTATUS IEEGANNOTATE
  
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  try
    
    % Get Status
    st = ieegstatus('all'); 
    
    assert(~isempty(st.tsi), 'IEEGGETBLOCK:NoJob', ...
      ['The IEEG-Toolbox is not connected to the server,\n'...
      'please use the IEEGCONNECT method to establish a connection.']);
    
    assert(st.isActive, 'IEEGGETBLOCK:Inactive', ...
      ['The current job is inactive, which means that all tasks have been ' ...
      'completed. Clear the job with the IEEGCLEARJOB method.']);
    
    % -- -- CREATE A NEW TASK IF NECESSARY -- --
    if isempty(st.taskParams)
      
      % No previous task know, start first task.
      taskParams = getnexttask(st.tsi, st.snapId, st.params);
      
      % Check for error that can occur if user used IEEGCONNECT and server
      % response is an empty job.
      assert(~isempty(taskParams), ['No tasks are available for this job, '...
        'please initiate a new job using IEEGCLEARJOB and IEEGCONNECT.']);
      
      display(sprintf('Started Task Nr: %d', st.totTask + 1));
      activeTask = true;
      [chObjects, chLabels] = getchannelobjs(taskParams.chIds);
      
      %Append taskParams
      taskParams.chObjects = chObjects;
      taskParams.chLabels  = chLabels;
      
      % Copy the filterSpec to each channel
      filterSpecs = st.tsi.createFilterSpecArray(length(chObjects));
      for i = 1:length(chObjects)
          filterSpecs(i) = st.params.filter.filterSpec;
      end
      taskParams.chFilters = filterSpecs;
      
      % Update ieeg status.
      ieegstatus(...
        'taskParams', taskParams, ...
        'curBlock'  , 1, ...
        'curTime'   , taskParams.start, ...
        'totTask'   , st.totTask + 1, ...
        'totTime'   , 0);
    else
      % A task is assigned, check if all blocks have been processed. If so,
      % start a new task.
      
      % Check that numBlocks is set
      numBlocks = st.taskParams.numBlocks;
      assert(numBlocks > 0, 'Number of Block per Task is unknown.');
      
      if st.curBlock > numBlocks
        % finished all block in task, get new task
        taskParams = getnexttask(st.tsi, st.snapId, st.params,...
          st.taskParams.name);

        if ~isempty(taskParams)
          display(sprintf('Started Task Nr: %d', st.totTask + 1));
          activeTask = true;
          [chObjects, chLabels] = getchannelobjs(taskParams.chIds);

          %Append taskParams
          taskParams.chObjects = chObjects;
          taskParams.chLabels  = chLabels;
          
          % Copy the filterSpec to each channel
          filterSpecs = st.tsi.createFilterSpecArray(length(chObjects));
          for i = 1:length(chObjects)
              filterSpecs(i) = st.params.filter.filterSpec;
          end
          taskParams.chFilters = filterSpecs;

          % Update ieeg status.
          ieegstatus(...
            'taskParams', taskParams, ...
            'curBlock'  , 1, ...
            'curTime'   , taskParams.start, ...
            'totTask'   , st.totTask + 1, ...
            'totTime'   , 0);
        else
          activeTask = false;
          % Update ieeg status.
          ieegstatus(...
            'curBlock'  , 0, ...
            'curTime'   , 0);
        end
      else
        activeTask = true;
      end 
    end

    % Get Updated Status
    st = ieegstatus('all');
    
    % -- -- GET A NEW BLOCK OF DATA -- --
    if activeTask
      
      % Get TimeStamp for first index in seconds.
      curTime = st.curTime;
      
      % The the data for the current block
      values = getblockvalues(st.tsi, st.snapId, st.params, ...
        st.taskParams, curTime);

      % Update the status with the values for the next block.
      ieegstatus('curTime', curTime + st.params.blockSize, ...
        'curBlock',st.curBlock + 1, 'totBlock', st.totBlock + 1);

      % Define the returned variables.
      startIndex  = curTime;
    else
      % All tasks finished for job.
      values     = [];
      startIndex = [];
      ieegstatus('isActive', false); % Set to inactive.
      display('-- -- All Tasks Completed -- --');
    end
    
  catch ME
    % Catching errors and rethrowing errors with alternative error message.
    switch ME.identifier
      case 'MATLAB:Java:GenericException'
        message = ME.message;
        throwAsCaller(MException('IEEGGETBLOCK:Connect',message));
      otherwise
        throwAsCaller(MException('IEEGGETBLOCK:Connect',...
          ['IEEGGETBLOCK:  ' ME.message]));
    end
  end
end

function [chObjects, chLabels] = getchannelobjs(chIds)
  %GETCHANNELOBJS  Returns channel objects belonging to Channel IDs.
  %   CHOBJS = GETCHANNELOBJS(CHIDS) returns the channel objects for the CHIDS
  %   that are provided as inputs. The objects are ordered the same as the
  %   channel IDs.
  
  tsi     = ieegstatus('tsi');
  snapId  = ieegstatus('snapId');
  
  % Get All the channel Objects for Job.
  details = tsi.getDataSnapshotTimeSeriesDetails(snapId);      
  chObjects(details.size) = details.get(details.size-1);
  chLabels{details.size } = char(details.get(details.size-1).getLabel);
  chRevIds{details.size } = char(chObjects(details.size).getRevId);
  for i = 0: (details.size-2)
    chObjects(i+1) = details.get(i);
    chLabels{i+1}  = char(chObjects(i+1).getLabel);
    chRevIds{i+1}  = char(chObjects(i+1).getRevId);
  end
  
  % Sort the channelObjects and return only channels for current task.
  sortArray = zeros(length(chIds),1);
  for i = 1: length(sortArray)
    index = find(strcmp(chIds{i}, chRevIds),1);
    assert(~isempty(index),'GETCHANNELOBJS:  Unable to match Channel IDs.');
    assert(sortArray(i)==0,'GETCHANNELOBJS:  Double match for Channel ID.');
    sortArray(i) = index;
  end
  chObjects = chObjects(sortArray);
  chLabels  = chLabels(sortArray);
  
end

function values = getblockvalues(tsi, snapId, params, taskParams, startIndex)
  %GETBLOCKVALUES  Returns double array of values for current block.

  % Get time values in microseconds.
  startIndexus = startIndex * 1000000;
  blockSizeus  = params.blockSize * 1000000;
  
  display(sprintf('Reading Block Nr: %d, (t = [%3.1f : %3.1f])', ...
    ieegstatus('curBlock'), startIndex, startIndex + params.blockSize)); 
  if params.raw
    seriesListOfLists = tsi.getUnscaledTimeSeriesSetRaw(snapId, ...
      taskParams.chIds, startIndexus, blockSizeus, 1);
  else
    seriesListOfLists = tsi.getUnscaledTimeSeriesSet(snapId, ...
      taskParams.chIds, taskParams.chFilters, startIndexus, blockSizeus, ...
      params.sampleFreq);
  end    
  
  % Get the data series segments associated with the each channel.
  for iChannel = 1:length(taskParams.chIds)
  
    seriesList = seriesListOfLists(iChannel);
    firstSeries = seriesList(1);

    % On first iteration, create full array for returned values and find the
    % scalefactor.
    if iChannel==1
      values = zeros(firstSeries.getSeriesLength, length(taskParams.chIds));
      scaleFactor   = firstSeries.getScale();
    end
    
    values(:, iChannel) = double(firstSeries.getSeries()) * firstSeries.getScale();

    % Set values to NaN if they are in a gap
    gStart = firstSeries.getGapsStart();
    gEnd   = firstSeries.getGapsEnd();
    for i = 1: length(gStart)
        startInx = gStart(i);
        % The gap end is actually 1 more than the last item
        endInx = gEnd(i);
        assert(endInx > startInx, 'Gap End is before Gap Start.');
        values((startInx+1):endInx, iChannel) = NaN;
        
    end
  end
  
end

function taskParams = getnexttask(tsi, snapId, params, varargin)
  %GETNEXTTASK  Requests new task from server and returns task parameters.
  %  TASKPARAMS = GETNEXTTASK(TSI, 'SnapId', PARAMS) requests a task if no task
  %  has previously been requested. 
  %  TASKPARAMS = GETNEXTTASK(TSI, 'SnapId', PARAMS, 'PreviousTaskName') closes
  %  the previous task and requests a new task from the server.
  
  switch nargin
    case 3
      task = tsi.getFirstTimeSeriesTask(snapId);
    case 4 
      task = tsi.getNextTimeSeriesTask(snapId, varargin{1});
    otherwise
      error('Incorrect number of input arguments for GETNEXTTASK.')
  end
  
  if ~task.isEmpty()
    taskParams = struct();
    taskParams.name       = char(task.getName());
    taskParams.start      = task.getStartTime() / 1000000;
    taskParams.end        = task.getEndTime() / 1000000;
    taskParams.duration   = taskParams.end - taskParams.start;
    taskParams.numBlocks  = round(taskParams.duration / params.blockSize);
    taskParams.chIds      = cell(task.getChannels());
  else
    taskParams = [];
  end

end
