function out = ieegsettings(varargin)
  %IEEGSETTINGS  General settings for the IEEG portal interface.
  %   OUT = IEEGSETTINGS() returns a structure with general settings for the
  %   IEEG portal interface. This includes the location of the server and the
  %   location of the password file that is used to set up the connection.
  %
  %   OUT = IEEGSETTINGS('optionStr', Value, ...) sets the option
  %   definined in 'optionStr' to 'value'. For all available
  %   options, see below. 
  %
  %   OUT = IEEGSETTINGS('default') sets all options back to their
  %   default values.
  %
  %   Options:
  %   'server'      : (str)  Sets the address to the server.
  %   'context'     : (str)  Sets the context on the server.
  %   'jobTracker'  : (str)  Sets the address to the job tracker.
  %   'isSecure'    : (bool) Determines whether a secure connection is used.
  %   'pwdFile'     : (str)  Location of the ieegpwd.bin file.
  %
  %   See also: IEEGPWDFILE IEEGCONNECT
  
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  persistent options
  
  try
    if isempty(options)
      options = struct(...
        'server', 'view.ieeg.org',...
        'context','',...
        'jobTracker','jobtracker.ieeg.org',...
        'isSecure',true,...
        'pwdFile','');
      mlock
    end

    if nargin
      if strcmp(varargin{1},'default')
        assert(length(varargin) == 1, ['IEEG:IEEGOPTION','IEEGOPTION ' ...
          'Setting IEEGOPTION to ''default'' values requires only one input.']);
        options = struct(...
          'server', 'view.ieeg.org',...
          'context','',...
          'jobTracker','jobtracker.ieeg.org',...
          'isSecure',true,...
          'pwdFile','');
      end
    else
      out = options;
      return
    end

    ix = 1;
    while ix < nargin
      if nargin >= (ix + 1)
        switch varargin{ix}
          case 'server'
            assert(ischar(varargin{ix+1}), ...
              'IEEGOPTION: Incorrect value for ''server'' option.');
            options.server = varargin{ix+1};
            ix = ix+2;
          case 'context'
            assert(ischar(varargin{ix+1}), ...
              'IEEGOPTION: Incorrect value for ''context'' option.');
            options.context = varargin{ix+1};
            ix = ix+2;  
          case 'jobTracker'
            assert(ischar(varargin{ix+1}), ...
              'IEEGOPTION: Incorrect value for ''jobTracker'' option.');
            options.jobTracker = varargin{ix+1};
            ix = ix+2;
          case 'isSecure'
            assert(islogical(varargin{ix+1}), ...
              'IEEGOPTION: Incorrect value for ''isSecure'' option.');
            options.isSecure = varargin{ix+1};
            ix = ix+2;
          case 'pwdFile'
            assert(ischar(varargin{ix+1}), ...
              'IEEGOPTION: Incorrect value for ''pwdFile'' option.');
            options.pwdFile = varargin{ix+1};
            ix = ix+2;                  
          case 'default'
            error('IEEG:IEEGOPTION',['IEEGOPTION  Setting IEEGOPTION to '...
              '''default'' values requires only one input.']);
          otherwise
            error('IEEG:IEEGOPTION','IEEGOPTION: Incorrect option.');
        end
      end
    end

    out = options;
    
  catch ME
    throwAsCaller(ME)
  end
  
end