function out = ieegannotate(annotations, varargin)
%IEEGANNOTATE  Uploads annotations for current job to server.
%   OUT = IEEGANNOTATE(ANNOTATIONS) uploads annotations specified in the
%   annotations structure to the snapshot that is created by the current job
%   on a specific channel. ANNOTATIONS is a matlab structure with the fields:
%   'chIndex', 'start' and 'stop'.
%
%   The 'chIndex' is a numeric that identifies the index of the channle
%   that should be annotated. It can also be a nx1 array of numerics if
%   multiple channels are annotated, and ':' if all channels should be
%   annotated. 'Start' and 'Stop' are vectors of equal length that define
%   the start and stop times for each event in microseconds (see Example
%   below).
%
%   OUT = IEEGANNOTATE(..., TYPE) adds a TYPE to the annotations. The default
%   type is omitted is 'Event'. TYPE should be a string.
%
%   OUT = IEEGANNOTATE(..., TYPE, NOTE) adds a TYPE and a NOTE. NOTE should be
%   as string and can be used to describe the annotations that are uploaded.
%   The default NOTE is an empty string.
%
%   OUT = IEEGANNOTATE(...., TYPE, NOTE, CREATOR) adds a TYPE, a NOTE and a
%   CREATOR to the annotations. CREATOR is a string and defaults to the
%   username for the current session if omitted.
%
%
%   NOTE: It is currently not possible to annotate events with zero length. In
%   this case, use the same vector for the start and stop times.
%
%   Example:
%           startA = [0 1000000 2000000];
%           stopA  = [500000 1500000 2500000];
%           annStruct = struct('chIndex', 1, 'start', startA, 'stop', stopA);
%           out = ieegannotate(annStruct, 'HFO', 'Annotation set 3');
%
%   See also: IEEGGETBLOCK IEEGSTATUS IEEGMONITOR

try
  
  import edu.upenn.cis.db.mefview.services.*;
  
  assert(nargin > 0 && nargin < 6, ...
    'Incorrect number of input arguments.')
  
  % Get status information
  assert(isa(annotations,'struct'),...
    'ANNOTATIONS argument should be a structure.');
  
  % Check Input variables.
  sNames = fieldnames(annotations);
  assert(length(annotations) == 1, 'Incorrect length annotations structure');
  assert(any(strcmp('start',sNames)) && any(strcmp('stop',sNames)) ...
    && any(strcmp('chIndex',sNames)), ['The annotations structure should '...
    'contain the properties ''chIndex'', ''start'' and ''stop''.']);
  assert(isvector(annotations.start) && isvector(annotations.stop) && ...
    length(annotations.start)==length(annotations.stop), ...
    'Incorrect size of the start and/or stop properties.');
  assert(ischar('tag') && ischar('note'), 'TAG and NOTE should be strings.');
  assert(all(annotations.start>=0) && all(annotations.stop>=0), ...
    'All annotation start- and stop times must be equal or greater than 0.');
  
  % Check Job Status
  st = ieegstatus('all');
  assert(~isempty(st.tsi), 'IEEGANNOTATE:NoJob', ['The IEEG-Toolbox is '...
    'not connected to the server,\nplease use the IEEGCONNECT method to '...
    'establish a connection.']);
  assert(~isempty(st.taskParams),'IEEGANNOTATE:NoTask',...
    ['The IEEG-Toolbox is connected but no task has been\n'...
    'assigned to run. Please use IEEGGETBLOCK to assign task, and\n'...
    'receive data.']);
  
  % Define optional parameters
  type = ' '; % Type cannot be empty, so single space for default.
  note = ' ';
  creator = st.userName;
  switch nargin
    case 2
      type = varargin{1};
    case 3
      type = varargin{1};
      note = varargin{2};
    case 4
      type = varargin{1};
      note = varargin{2};
      creator = varargin{3};
  end
  
  %Check type is longer than 1
  assert(~isempty(type),'Type needs to have a length > 0.');
  assert(~isempty(note),'Note needs to have a length > 0.');
  assert(~isempty(creator),'Creator needs to have a length > 0.');
  
  % Get channelIndex vector.
  if ischar(annotations.chIndex)
    assert(strcmp(annotations.chIndex,':'),...
      'If ''chIndex'' is String, ''chIndex'' can only be '':''.');
    chanObjArr(1) = st.taskParams.chObjects(1).getTimeSeries;
    for i=2:length(st.taskParams.chObjects)
      chanObjArr(i) = st.taskParams.chObjects(i).getTimeSeries; %#ok<AGROW>
    end
  else
    chanObjArr(1) = st.taskParams.chObjects(annotations.chIndex(1)).getTimeSeries;
    for i=2:length(annotations.chIndex)
      curIndex = annotations.chIndex(i);
      chanObjArr(i) = st.taskParams.chObjects(curIndex).getTimeSeries;  %#ok<AGROW>
    end
    
  end
  
  % Upload the annotations; change annotation times to microseconds.
  annotationList = st.tsi.createAnnotationList();
  nrAnnotations = length(annotations.start);
  for iAnn = 1: nrAnnotations
    ann = edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation(...
      creator, annotations.start(iAnn) * 1e6, annotations.stop(iAnn) * 1e6,...
      type);
    
    
    for i = 1: length(chanObjArr)
      ann.addAnnotated(chanObjArr(i));
    end
    
    annotationList.add(ann);
  end
  
  % Add the annotations to our target instance
  st.tsi.addAnnotationsToDataSnapshot(st.snapId, annotationList);
  
  % Define output
  out = length(annotations.start);
  
catch ME
  % Catching errors and rethrowing errors with alternative error message.
  switch ME.identifier
    case 'MATLAB:Java:GenericException'
      lines = regexp(ME.message,'\n');
      if length(lines) >2
        message = ME.message(1:lines(2));
      else
        message = ME.message;
      end
      throwAsCaller(MException('IEEGANNOTATE:Connect',message));
    otherwise
      throwAsCaller(MException('IEEGANNOTATE:Connect',...
        ['IEEGANNOTATE:  ' ME.message]));
  end
end

end