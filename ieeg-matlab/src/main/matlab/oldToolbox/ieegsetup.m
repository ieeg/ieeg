%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2012 Trustees of the University of Pennsylvania
% 
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
% 
% http://www.apache.org/licenses/LICENSE-2.0
% 
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  

function ieegsetup()
    thisFile = mfilename('fullpath');
    [toolboxDir, ~, ~] = fileparts(thisFile);
	ieegjar = strcat(toolboxDir, '/lib/${project.artifactId}-${project.version}.jar');
    
    display(['IEEGSETUP: Adding ', ieegjar, ' to dynamic classpath']);
    javaaddpath(ieegjar);
    
    jpath = javaclasspath('-all');
    foundLog4j = false;
    for i=1:size(jpath, 1);
        jar = jpath{i};
        idx = strfind(jar, strcat(filesep, 'log4j'));
        if ~isempty(idx)
            display(['IEEGSETUP: Found ', jar, ' on Java classpath. Will not add another log4j jar.']);
            foundLog4j = true;
            break;
        end;
    end;
    if (~foundLog4j)
        log4jjar  = strcat(toolboxDir, '/lib/log4j-1.2.16.jar');
        display(['IEEGSETUP: Adding ', log4jjar, ' to dynamic classpath']);
        javaaddpath(log4jjar);
    end;

	java.lang.Thread.currentThread.setContextClassLoader(edu.upenn.cis.db.mefview.services.ClassLoaderHelper.getClassLoader)
	org.apache.log4j.LogManager.resetConfiguration
	org.apache.log4j.PropertyConfigurator.configure(strcat(toolboxDir, '/lib/log4j.properties'))
end
