function ieegmonitor(varargin)
  %IEEGMONITOR  Provides continuous information about IEEG connection status.
  %   IEEGMONITOR() creates a UI that displays information about the current
  %   connection to the IEEG portal and the status of ongoing tasks. By default,
  %   the UI is updated every 0.5 seconds.
  %
  %   IEEGMONITOR(UPDATERATE) can be used to specify a user-defined update rate.
  %   The value for the UPDATERATE parameter is defined in seconds.
  %
  %   IEEGMONITOR('clear') clears the monitor display. This option is
  %   automatically called during IEEGCONNECT.
  %
  %   If Matlab is run without a user interface, the hdsmonitor will display a
  %   status report in the command line during each update.
  %
  %   See also:  IEEGSTATUS IEEGCONNECT
  
  % ****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  persistent p q uihandle v1 index ht1 t1 myhandles
  
  switch nargin
    case 0 % Init monitor with default time-out.
      period = 0.5;
      option = 'init';
    case 1 % User defined time-out setting.     
      if isnumeric(varargin{1}) && isscalar(varargin{1})
        period = varargin{1};
        option = 'init';
      elseif ischar(varargin{1})
        if strcmp(varargin{1},'clear')
          if ~isempty(myhandles)
            try
              v1 = zeros(200,1);
              set(myhandles.curBlockLine,'YData', v1);
            catch ME %#ok<NASGU>
            end
          end
          return
        else
          error('IEEGMONITOR:Input','Incorrect input argument.');
        end
      else
        error('IEEGMONITOR:Input','Incorrect input argument.');
      end
    case 2 % Updating monitor; no special init. 
      assert(isa(varargin{1},'timer'),...
        'Incorrect number of input arguments.');
      option = 'update';
    case 3 % Stop the monitor                   
      
      % Check that this scenario is not called by user
      assert(isa(varargin{1},'timer') || ishandle(varargin{1}), ...
        'Incorrect number of input arguments,');
      assert(strcmp(varargin{3},'stop'),'Incorrect number of input arguments,');
      
      % The function is called by either deletion of the timer object or closing
      % of the figure window. The following code, deletes both figure and timer
      % objects in either case. 
      obj = varargin{1};
      if ishandle(obj)
        if ~q
          % Function caller by Figure
          handle = get(obj, 'UserData');
          p = 1;
          stop(handle);
          delete(handle);
        else
          q = false;
        end
      else
        if ~p 
          % Function called by Timer
          handle = get(obj, 'UserData');
          q = true;
          if ishandle(handle)
            close(handle);
          end

        else
          p = false;
        end
      end
      return
    otherwise
      error('IEEGMONITOR:Input',...
        'IEEGMONITOR:  Incorrect number of input arguments.');
  end

  switch option
    case 'init'  
      
      % Future version of this method will also run in the terminal, but current
      % version will throw an error if the Matlab desktop is not running.
      if ~usejava('awt')
         error('IEEGMONITOR:NoDisplay', ['The MATLAB session does not '...
           'support UserInterface objects.']);
      end
      
      if isempty(uihandle) || ~ishandle(uihandle)
        
        % MLock locks the m-file which means that the timer and the persistent
        % variables are not cleared during clear all. To unlock, use
        % MUNLOCK('ieegmonitor')
        mlock;
        
        p = false;
        q = false;

        index = 1;
        % Init figure
        scrSize = get(0,'ScreenSize');
        uihandle = figure('Resize', 'off', ...
          'position', [scrSize(3)/1.25 scrSize(4)/4 330 120], ...
          'MenuBar', 'none', ...
          'Name','IEEG Monitor', ...
          'NumberTitle','off');
        set(uihandle,'deleteFcn', {@ieegmonitor 'stop'});
        uicontrol(uihandle,'Style', 'text', 'String', 'Server :',...
         'Position', [5 90 65 30] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');   
        uicontrol(uihandle,'Style', 'text', 'String', 'No connection...',...
         'Position', [65 90 260 30] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','server'); 
        uicontrol(uihandle,'Style', 'text', 'String', 'Username :',...
         'Position', [5 81 65 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');  
        uicontrol(uihandle,'Style', 'text', 'String', '',...
         'Position', [65 81 260 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','userName'); 
        uicontrol(uihandle,'Style', 'text', 'String', 'Duration :',...
         'Position', [5 66 65 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');  
        uicontrol(uihandle,'Style', 'text', 'String', '',...
         'Position', [65 66 260 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','duration'); 
       

        a1 = axes('Units','pixels','Position',[5 5 190 60],'Box','on','YTick',[],...
            'Xtick',[],'YLim', [0 10],'Tag','a1','DrawMode','fast','Layer','top',...
            'YTick',0:1:100,'YTickLabel',{});
        hold(a1,'on')

        % Static Text  
        uicontrol(uihandle,'Style', 'text', 'String', 'Total Blocks :',...
         'Position', [205 50 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');  
        uicontrol(uihandle,'Style', 'text', 'String', 'Total Tasks :',...
         'Position', [205 35 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');  
        uicontrol(uihandle,'Style', 'text', 'String', 'Nr. Channels :',...
         'Position', [205 20 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');  
        uicontrol(uihandle,'Style', 'text', 'String', 'Current Block :',...
         'Position', [205 5 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left');   
        % Dynamic Text
        uicontrol(uihandle,'Style', 'text', 'String', '-',...
         'Position', [290 50 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','p1');  
        uicontrol(uihandle,'Style', 'text', 'String', '-',...
         'Position', [290 35 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','p2');  
        uicontrol(uihandle,'Style', 'text', 'String', '-',...
         'Position', [290 20 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','p3');  
        uicontrol(uihandle,'Style', 'text', 'String', '-',...
         'Position', [290 5 80 15] ,'BackgroundColor',get(uihandle,'Color'),...
         'HorizontalAlignment','left','Tag','p4');       

        t1 = 1;
        v1 = zeros(200,1);
        stairs(a1, v1,'Color','green','LineWidth',1.5,'Tag','curBlockLine');
        line( [0 200],[0 0],'Color','blue','LineWidth',1,'Tag','numBlockLine',...
          'LineStyle','--')
        ht1 = text(5,0.1,'|| 1 block','FontSize',10,'HorizontalAlignment','left',...
          'VerticalAlignment','bottom'); 

        % Init Timer
        ui_timer = timer;
        set(ui_timer, 'ExecutionMode', 'FixedRate');
        set(ui_timer, 'BusyMode','queue');
        set(ui_timer, 'Period', period);
        set(ui_timer, 'StartDelay',1);
        set(ui_timer, 'TimerFcn', @ieegmonitor);
        set(ui_timer, 'stopFcn', {@ieegmonitor 'stop'});
        set(ui_timer, 'UserData', uihandle);
        start(ui_timer);

        set(uihandle,'UserData', ui_timer); 
        set(uihandle,'HandleVisibility','callback');
        set(a1,'HandleVisibility','callback');
        
        myhandles = guihandles(uihandle);
      
      elseif nargin==1
        % Update timeOut period.
        timerHandle = get(uihandle,'UserData');
        set(timerHandle,'stopFcn',[]);
        stop(timerHandle);
        set(timerHandle,'Period',varargin{1});
        set(timerHandle, 'stopFcn', {@ieegmonitor 'stop'});
        start(timerHandle);
      end
    case 'update'
      % Check that the method is called by the timer-callback.
      assert(isa(varargin{1},'timer'),...
        'IEEGMONITOR:TimeOut','Incorrect number of input arguments.');
      
      % Only update plot if MATLAB is running a job.
      status = ieegstatus('all');
      
      % If inactive, set title and server name and exit. This means that the
      % graph does not update and the variables resemble the totals from the
      % last job.
      if ~status.isActive
        set(uihandle,'Name','IEEG Monitor (Inactive)')
        set(myhandles.server,'String', 'No connection.');
        set(myhandles.userName,'String', '-');
        set(myhandles.p1,'String', '-');
        set(myhandles.p2,'String', '-');
        set(myhandles.p3,'String', '-');
        set(myhandles.p4,'String', '-');
        set(myhandles.duration,'String', '-');
        
        return        
      end
      
      set(uihandle,'Name','IEEG Monitor (Active)')
      set(myhandles.server,'String', status.url);
      set(myhandles.userName,'String', status.userName);    
      set(myhandles.p1,'String', status.totBlock);
      set(myhandles.p2,'String', status.totTask);
      set(myhandles.p4,'String', status.curBlock);
      
      durationStr = timeToVec(etime(clock,status.initTime));
      set(myhandles.duration,'String', durationStr);
      
      
      % Update Graph if Task present.
      if ~isempty(status.taskParams)
        time        = [(index + 1) : 200 1:index ];
        v1(index)   = status.curBlock - 1; %-1 because showing finished blocks.
        set(myhandles.curBlockLine,'YData', v1(time));

        % Update numBlocksPerTask
        v2 = status.taskParams.numBlocks.*ones(2,1);
        set(myhandles.numBlockLine,'YData', v2);

        numBlocks = status.taskParams.numBlocks;
        set(myhandles.p3,'String', length(status.taskParams.chIds));
      else
        numBlocks = 0;
      end

      % Update scale if needed.
      YLim = get(myhandles.a1,'YLim');
      lim  = ceil(YLim(2)/10);

      if numBlocks > (YLim(2) - lim)

        newYLim = [0 (YLim(2) + (numBlocks - YLim(2) + 2*lim))];
        set(myhandles.a1,'YLim', newYLim);
        while 1
          if (newYLim(2)/t1) > 20
            % Update ticks
            t1 = t1*5;
            set(myhandles.a1,'YTick',0:t1:t1*100);
            set(ht1,'String',sprintf('|| %d Blocks',t1));
          else
            break
          end
        end

      elseif max([numBlocks 10]) < (YLim(2) - 4*lim)

        newYLim = [0 max([(YLim(2) - 5*lim) 10])];
        set(myhandles.a1,'YLim', newYLim);
        while 1
          if (newYLim(2)/t1) < 3
            % Update ticks
            t1 = t1/5;
            set(myhandles.a1,'YTick',0:t1:t1*100);
            set(ht1,'String',sprintf('|| %d Blocks',t1));
          else
            break
          end
        end
      end  

      index = index+1;
      if index > 200
          index = 1;
      end

      drawnow expose
    otherwise
      error('IEEGMONITOR:IncorrectOption', 'IEEGMONITOR  Incorrect option.');
  end
  
 
end

function timeStr = timeToVec(tim)
  %TIMETOVEC  Format numeric seconds to string.
  days = floor(tim./86400);
  hours = floor((tim - days*86400)./3600);
  minutes = floor((tim - days*86400 - hours*3600)./60);
  seconds = round((tim - days*86400 - hours*3600 - minutes*60));
  timeStr = sprintf('%02.0f:%02.0f:%02.0f:%02.0f',...
    days, hours, minutes, seconds);
end
