function varargout = ieegclearjob()
  %IEEGCLEARJOB  Disconnects matlab from server.
  %   IEEGCLEARJOB() terminates the IEEG Matlab session and clears the session
  %   variables in Matlab. The method returns the snapshotID where the results
  %   of the analysis are stored.
  %
  %   SNAPID = IEEGCLEARJOB terminates the IEEG Matlab session and returns the
  %   snapshot ID that is associated with the analyzed data.
  %
  %   [SNAPID, PARAMS] = IEEGCLEARJOB() returns the PARAMS structure from the
  %   job that is being cleared. This structure contains information about the
  %   cleared job and was retrieved from IEEGSTATUS.
  %
  %   [SNAPID, PARAMS, TASKPARAMS] = IEEGCLEARJOB() returns the PARAMS and
  %   TASKPARMS structure from the job that is being cleared. These structures
  %   contain information about the cleared job and were retrieved from
  %   IEEGSTATUS. The TASKPARAMS structure is related to the last processed
  %   task.
  %
  %   This method currently does not interact with the IEEG-server, but is used
  %   to clear the parameters stored in IEEGSTATUS. It also reinitializes the
  %   IEEGMONITOR. 
  %
  %   See also: IEEGSTATUS IEEGMONITOR IEEGCONNECT
  
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  try
    ts = ieegstatus('all');
    snapID = ts.snapId;
    if ~isempty(ts.tsi)
      ieegstatus('clear');
      ieegmonitor('clear');
    else
      error('IEEGCLEARJOB:NoJob', 'There is no job to clear.');
    end

    % Display Snapshot ID.
    fprintf(1, ...
          '\nThe analyzed snapshot can be accessed using Snapshot ID:\n');
    fprintf(2,'%s\n\n',snapID);
        
    if nargout
      varargout = {snapID ts.params ts.taskParams};
    end
  
  catch ME
    throwAsCaller(MException(ME.identifier,...
      ['IEEGCLEARJOB: ' ME.message]));
  end
end