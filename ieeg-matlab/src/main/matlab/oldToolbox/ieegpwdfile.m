function varargout = ieegpwdfile(username, password, varargin)
  %IEEGPWDFILE  Creates a pwd-file to be used with the IEEG Portal
  %   PATH = IEEGPWDFILE('username', 'password') create a pwd-file that is used
  %   by the IEEG Toolbox to connect to the IEEG portal. The method returns the
  %   full path to the created file.
  %
  %   PATH = IEEGPWDFILE('username', 'password', 'filename') does the same thing
  %   but uses the provided filename instead of using a UI to select a
  %   file-name.
  %
  %   !! The implementation of this code is not in any way secure as the
  %   password is still stored as a string and loaded into matlab during the
  %   IEEGCONNECT method.  However, it provides a simple solution to access the
  %   password without having to specify the password in any scripts or having
  %   to supply the password in a user interface in Matlab. This makes it
  %   unlikely that the password is accidentally shared when tools are uploaded
  %   to the portal.
  %
  %   see also: IEEGSETTINGS IEEGCONNECT IEEGSTATUS
    
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  try
    switch nargin
      case 2
        % Append default filename with partial username. If username is too
        % short, do not append filename.
        if length(username) < 4
          appFileName = '';
        else
          appFileName = [username(1:3) '_'];
        end
        
        defaultName = sprintf('%sieeglogin.bin',appFileName);
        [fileName, pathName, ~] = uiputfile('*.*', ...
          'Select a location to save the password file',defaultName);
        
        filePath = fullfile(pathName, fileName);
      case 3
        assert(ischar(varargin{1}),'IEEGPWDFILE: Incorrect input argument.');
        [pathName, fileName, ext] = fileparts(varargin{1});
        
        if isempty(pathName); pathName = pwd; end
        assert(exist(pathName,'dir')==7,...
          'IEEGPWDFILE: The supplied path does not exist.');

        filePath = fullfile(pathName, [fileName ext]);
      otherwise
        error('IEEGPWDFILE:INPUT',['IEEGPWDFILE: Incorrect number of '...
          'input arguments.']);
    end
    
    % Write the password as a binary to file.
    fid = fopen(filePath,'w');
    fwrite(fid, password);
    fclose(fid);    
    fprintf(2,'-- -- IEEG password file saved -- --\n');
    
    % Define output of method if requested.
    if nargout
      varargout = {filePath};
    else
      varargout = {};
    end
    
  catch ME
    throwAsCaller(ME)
  end
  
end