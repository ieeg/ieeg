function varargout = ieegstatus(varargin)
  %IEEGSTATUS  Provides information about the current job.
  %   STATUS = IEEGSTATUS() provides a structure with the current status of the
  %   job such as the progress, channel information, ect. This results in the
  %   same output as IEEGSTATUS('info'). If the method is call without an output
  %   argument, some basic information is displayed in the command window.
  %
  %   STATUS = IEEGSTATUS('all') returns the same structure including some
  %   properties that are not available by default such as the TSI object that
  %   is used internally by the IEEG-Toolbox. In general, this is mostly used by
  %   internal methods and should not be used by the end-user.
  %
  %   STATUS = IEEGSTATUS(FIELDNAME) returns a single status parameter.
  %   FIELDNAME is a string that indicates one of the fieldnames of the status
  %   structure. It is possible to request properties from nested structures by
  %   using the '.'-notation in the property string.
  %
  %     For example:
  %       snapID  = ieegstatus('snapId');
  %       chNames = ieegstatus('params.channelNames');
  %
  %   STATUS = IEEGSTATUS('clear') clears all status information, including any
  %   open sessions. This is similar to clearing all variables related to a
  %   session. A new session, will need to be initiated to continue.
  %
  %   STATUS = IEEGSTATUS(VARARGIN) Multiple inputs are used by various other
  %   IEEG methods to update the status and should in general not be used by the
  %   user.
  %
  %   See also: IEEGSETTINGS IEEGCONNECT
    
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  % Status is persistent variable, which means that it retains its value between
  % calls to the method. The matlab 'clear all' command will erase the
  % persistent status values.
  persistent status
  
  % Initialyze persistent variable on first call.
  if isempty(status)
    status = struct( ...
      'isActive', false,... % Boolean that is true when matlab is running a job.
      'tsi', [], ...        % Time Series Java Object
      'toolId', [], ...     % Tool Identifier
      'snapId', [], ...     % Snapshot Identifier
      'url','',...          % URL to Server.
      'userName','',...     % UserName
      'params', [], ...     % Structure with job parameters
      'taskParams',[], ...  % Structure with task parameters
      'totBlock',0,...      % Total number of blocks analyzed in session
      'totTask',0,...       % Total number of tasks analyzed in session
      'initTime',zeros(1,6),... % Init time of session (used to calc totTime)
      'curBlock', [],...    % Current Block Identifier
      'curTime',0);         % Timestamp of first index of current block in us.      
  end
  
  try
    % -- CHECK INPUT ARGUMENTS --
    switch nargin
      case 0    % Return subset of status settings        
        option = 'userView';
      case 1    % Return single variable, 'clear' or 'all'
        switch varargin{1}
          case 'clear' % Clear the structure.
            status = [];
            option = 'clear';
          case 'all' % Return all status items (used for internal methods)
            option = 'allView';
          case 'info' % Return subset of the status items (same as no input)
            option = 'userView';
          otherwise
            option = 'returnField';
        end
      otherwise % Assign values to the structure.         
        % Make sure that number of inputs is even number.
        assert(rem(length(varargin),2) == 0, ...
          'Incorrect number of input arguments.');
        option = 'setData';
    end

    % -- RETURN STATUS STRUCTURE VARIANT DEPENDING ON OPTION --
    switch option
      case 'userView'   
        if nargout

          % Remove JAVA objects from struct for user-view.
          jobParams = status.params;
          if ~isempty(jobParams)
            jobParams = rmfield(jobParams,{ 'chMap'});  
            filterParams = jobParams.filter;
            filterParams = rmfield(filterParams, 'filterSpec');
            jobParams.filter = filterParams;
          end
          
          % Remove JAVA objects from taskparams struct for user-view.
          taskParams = status.taskParams;
          if ~isempty(taskParams)
            taskParams = rmfield(taskParams,{ 'name' 'chObjects' 'chFilters'});  
          end
          
          userParams = rmfield(status,{'tsi'});
          userParams.params = jobParams;
          userParams.taskParams = taskParams;
          varargout{1} = userParams;

        else

          fprintf(2,'-- -- IEEGSTATUS  -- --\n');
          if status.isActive
            isActiveStr = 'true';
            durationStr = timeToVec(etime(clock,status.initTime));
          else
            isActiveStr = 'false';
            durationStr = '-';
          end

          if ~isempty(status.taskParams)
            fprintf([' Session Active: %s\n Current Task: %d\n' ...
              ' Current Block: %d of %d\n Total Tasks: %d\n'...
              ' Total Blocks: %d\n Total Time: %s\n'], isActiveStr, status.totTask,...
              status.curBlock, status.taskParams.numBlocks, status.totTask, ...
              status.totBlock, durationStr );
                    
            fprintf(2,'-- -- -- -- -- -- -- --\n\n');

          else
            fprintf([' Session Active: %s\n Total Tasks: %d\n' ...
              ' Total Blocks: %d\n Total Time: %s\n'], isActiveStr, status.totTask, ...
              status.totBlock, durationStr);
            fprintf(2,'-- -- -- -- -- -- -- --\n\n');
          end
        end
      case 'allView'    
        varargout{1} = status;
      case 'returnField'
        % Getting single property from status structure.
        try
          % Check if we request direct or nested property.
          aux = regexp(varargin{1},'\.','split');

          if length(aux) > 1
            % Return a nested property.
            out  = status;
            for i = 1:length(aux)
              out = subsref(out,substruct('.',aux{i}));
            end
            varargout{1} = out;
          else
            % Return direct property.
            varargout{1} = status.(varargin{1});
          end
        catch ME
          throw(MException('IEEGSTATUS:INPUT',...
            'Property does not exist.'));
        end
      case 'setData'    
        % Setting the data in status structure.
        try

          % Iterate over number of properties that will be set.
          for i = 1:2:(length(varargin)-1)

            % Check if we are setting direct property or nested property.
            aux = regexp(varargin{i},'\.','split');
            if length(aux) >1
              % Create substructure for nested property indexing.
              propCell = cell(length(aux),2);
              propCell(1,:) = {'.'};
              propCell(2,:) = aux;
              propCell = propCell(1:end);

              % Try to fetch data, will return error if property does not exist.
              subsref(status,substruct(propCell{:}));

              % Setting data
              subsasgn(status, substruct(propCell{:}),varargin{i+1});
            else
              % Set direct property
              status.(varargin{i}) = varargin{i+1};
            end

          end
        catch ME
          throw(MException('IEEGSTATUS:SetIncorrect',...
            'Trying to set a status field that does not exist.'));
        end
      otherwise         
        if nargout
          varargout{1} = status;
        end
    end
  catch ME
    % Catching errors and rethrowing errors with alternative error message.
    switch ME.identifier
      case 'MATLAB:Java:GenericException'
        lines = regexp(ME.message,'\n');
        if length(lines) >2
          message = ME.message(1:lines(2));
        else
          message = ME.message;
        end
        throwAsCaller(MException('IEEGSTATUS:Connect',message));
      otherwise
        throwAsCaller(MException('IEEGSTATUS:Connect',...
          ['IEEGSTATUS:  ' ME.message]));
    end
  end
end

function timeStr = timeToVec(tim)
  %TIMETOVEC  Format numeric seconds to string.
  days = floor(tim./86400);
  hours = floor((tim - days*86400)./3600);
  minutes = round((tim - days*86400 - hours*3600)./60);
  timeStr = sprintf('%d days - %d hours - %d minutes.',days, hours, minutes);
end
