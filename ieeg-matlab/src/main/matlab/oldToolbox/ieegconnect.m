function status = ieegconnect(userid, toolId, varargin)
  %IEEGCONNECT  Creates an interface between the IEEG portal and MATLAB.
  %   STATUS = IEEGCONNECT('userId', 'toolId') creates an interface between the
  %   IEEG server and the Matlab session. The USERID identifies the user and the
  %   TOOLID identifies the tool that is going to analyze the data. It is
  %   assumed that a job for this tool is started on the IEEG portal. If the
  %   method is succesful, the method will return a structure with information
  %   about the connection.
  %
  %   STATUS = IEEGCONNECT('userId', 'toolId', OPTION) allows the user to
  %   specify whether the method should perform a single job request or repeatly
  %   request a job until a job is available. There are two options:
  %
  %     'continuous': will request a job from the server every 5 seconds until
  %                   the server responds with a job. 
  %     'single'    : will perform a single request for a job. This is the
  %                   default behavior of the method if no option is supplied.
  %
  %
  %   NOTE: Make sure that you change the IEEGSETTINGS to point to the correct
  %   server and password file.
  %
  %   Example:
  %     status = ieegconnect('john Doe','85e651d6-c293-41db-8c98-d3c086fb8634');
  %
  %   See also: IEEGGETBLOCK IEEGSTATUS IEEGSETTINGS
 
  %*****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
  
  import edu.upenn.cis.db.mefview.services.*;
  
  % -- CHECK INPUT ARGUMENTS --
  try
    switch nargin
      case 2
        repeatRequest = false;
      case 3
        switch varargin{1}
          case 'continuous'
            repeatRequest = true;
          case 'single'
            repeatRequest = false;
          otherwise
            error('IEEGCONNECT:Input',...
              'IEEGCONNECT: Incorrect input argument.');
        end
      otherwise
        error('IEEGCONNECT:Input',...
          'IEEGCONNECT: Incorrect number of input arguments.');
    end
  catch ME
    throwAsCaller(ME);
  end
    
  % Read password from file. This method is not secure at all but it is a simple
  % solution that does not require the user to provide the password as a string
  % as an input to this method. The password file is generated using
  % IEEGPWDFILE. 
  try
    options = ieegsettings();
    h = fopen(options.pwdFile,'r');
    password = fread(h,'*char');
    password = password';
    fclose(h);
  catch ME 
    throwAsCaller(MException('IEEGCONNECT:Password', ...
      ['IEEGCONNECT was unable to find the password associated '...
      'with the user-id.\nSet the correct password file '...
      'with the IEEGSETTINGS method (also see IEEGPWDFILE).']));
  end
  
  % -- GET JOB FROM SERVER --
  try
    % Check whether a previous job exists
    assert(isempty(ieegstatus('tsi')), ['A connection with '...
      'the IEEG-server already exists,\n please use IEEGCLEARJOB to clear '...
      'the job or IEEGGETBLOCK to fetch data.']);
    
    % Create the URL to the ieeg webservice.
    url = ieegurlnoport(options.server, options.context, options.isSecure);
    jobTrackerUrl = ieegurlnoport(options.jobTracker, '', true);
    
    % Get TimeSeries object.
    ieegsetup;
    tsi = edu.upenn.cis.db.mefview.services.TimeSeriesInterface(toolId, ...
    url, userid, password, jobTrackerUrl);

    % Depending on the input options, a job is requested once or is requested
    % every 5 seconds until a job is provided.
    if repeatRequest
      snapId = [];
      while isempty(snapId)
        snapId = char(tsi.getNewJob());
        if isempty(snapId)
          display('No job found; trying again in 5 sec.');
          pause(5);
        end
      end 
    else
      snapId = char(tsi.getNewJob());
    end

    % Populate the Status variable if a job was found.
    if ~isempty(snapId)
      
      % Clear Status Information.
      ieegstatus('clear');
      ieegmonitor('clear');
      
      % Retrieve details on a given time series
      params = getrequestparameters(tsi, snapId);

      ieegstatus(...
        'isActive', true,...
        'tsi', tsi, ...
        'toolId', toolId, ...
        'snapId', snapId, ...
        'url',url,...
        'userName', userid, ...
        'params', params,...
        'totBlock',0,...
        'totTask',0,...
        'totTime',0,...
        'curBlock',0,...
        'taskParams',[],...
        'initTime',clock);
      
      
      display(sprintf('\n-- -- Session Started -- --'));

      status  = ieegstatus();
    else
      error('IEEGCONNECT:NoJob',['The IEEG-Toolbox did not '...
        'find a job on the server,\nNo connection was established.']);
    end
  catch ME

    % Catching errors and rethrowing errors
    switch ME.identifier
      case 'MATLAB:Java:GenericException'
        lines = regexp(ME.message,'\n');
        message = ME.message;
        throwAsCaller(MException('IEEGCONNECT:Connect',message));
      case 'IEEGCONNECT:NoJob'
        throwAsCaller(ME);
      otherwise
        throwAsCaller(MException('IEEGCONNECT:Connect',...
          ['IEEGCONNECT:  ' ME.message]));
    end
   
  end
end

function params = getrequestparameters(tsi, snapId)
  %GETREQUESTPARAMETERS  Returns general information about a snapshot.
  %   PARAMS = GETREQUESTPARAMETERS(TSI, SNAPID) returns information about the
  %   SNAPID that is provided. TSI is the TimeSeriesInterface object of the
  %   current job and the SNAPID is a string that indicates the current
  %   snapshot. The returned PARAMS struct contains information about the
  %   channels and filters associated with the snapshot.
  %

  % Setup Params structure
  aux = tsi.getSnapshotParameters(snapId);
  params = getParamsStruct(aux);

  % Create filter object
  fs = createFilter(params.filter, params.raw);

  % Create the return list and map of channel names and rev IDs
  details = tsi.getDataSnapshotTimeSeriesDetails(snapId);
  channelList = params.channelNames;
  channelIDs = cell(length(channelList), 1);
  channelMap = containers.Map();
  for i = 1: length(channelList)
      channelIDs(i) = details.get(i-1).getRevId();
      channelMap(char(details.get(i-1).getRevId())) = i;
  end

  params.filter.filterSpec  = fs;
  params.chIds    = channelIDs;
  params.chMap    = channelMap;

end

function params = getParamsStruct(javaObj)
  %GETPARAMSSTRUCT  Returns matlab struct from snapshotparameters.
  
  params = struct();
  
  filterNames = {'highFreqPass' 'lowFreqPass' 'lowFreqStop' 'numPoles' ...
    'bandStop' 'highFreqStop' 'filterType' 'lowFreqStop'};
  paramNames  = {'numBlocks' 'raw' 'blockSize' 'startTime' 'sampleFreq' ...
    'channelNames'};
  
  keys = cell(javaObj.keySet.toArray);
  for i = 1: length(keys)
    switch keys{i}
      case paramNames
        switch keys{i}
          case {'blockSize' 'startTime'}
            params.(keys{i}) = javaObj.get(keys{i}) / 1000000; % to seconds.
          case {'channelNames'}        
            params.(keys{i}) = cell(javaObj.get(keys{i}));
          otherwise
            params.(keys{i}) = javaObj.get(keys{i});
        end
      case filterNames
        switch keys{i}
          case {'filterType'}        
            filterStruct.(keys{i}) = cell(javaObj.get(keys{i}));
          otherwise
            filterStruct.(keys{i}) = javaObj.get(keys{i});
        end
      otherwise
        error('IEEGCONNECT:GetParamsStruct','New parameter in getParamsStruct'); 
    end
  end
  
  params.filter = filterStruct;
  
end

function fs = createFilter(filterStruct, useRaw)
  %CREATEFILTER  Creates a filter object.
  %   FS = CREATEFILTER(filterStruct) creates a filter object based on the params input
  %   where filterStruct is a java list which is a structure with properties:
  %   {'filterType' 'raw' 'Bandpass' 'Lowpass' 'Highpass' 'numPoles' 'lowFreqPass'
  %   'highFreqPass' 'lowFreqStop' 'highFreqStop'}
  % 
  
  filterName = filterStruct.filterType{1};  

  if useRaw
    spec = edu.upenn.cis.db.mefview.services.FilterSpec.NO_FILTER;
  else
    switch filterName
      case 'Bandpass'
        spec = edu.upenn.cis.db.mefview.services.FilterSpec.BANDPASS_FILTER;
      case 'LowPass'
        spec = edu.upenn.cis.db.mefview.services.FilterSpec.LOWPASS_FILTER;
      case 'HighPass'
        spec = edu.upenn.cis.db.mefview.services.FilterSpec.HIGHPASS_FILTER;
      otherwise
        spec = edu.upenn.cis.db.mefview.services.FilterSpec.NO_FILTER;
    end
    
    % Add bandstop filter if the bandstop property exists.
    if isfield(filterStruct, 'bandStop')
      bandStop = filterStruct.bandStop;
      if bandStop
        spec = spec + ...
          edu.upenn.cis.db.mefview.services.FilterSpec.BANDSTOP_FILTER;
      end
    end 
    
  end

  % Initialize a specific filter
  fs = edu.upenn.cis.db.mefview.services.FilterSpec(spec, filterStruct.numPoles, ...
  filterStruct.lowFreqPass, filterStruct.highFreqPass, filterStruct.lowFreqStop, ...
  filterStruct.highFreqStop);
end

function url = ieegurl(server, context, secure )
  %IEEGURL  Creates the url to the IEEG server.
  %   URL = IEEGURL('server', 'context', ISSECURE) returns the URL to request
  %   data from the Web service. SERVER is a string that specifies the server
  %   location, CONTEXT specifies the services location on the server and
  %   ISSECURE specifies whether a secure connection should be used.
  %
  %   If the code is deployed, a secure connection is used irrespective of the
  %   input arguments.
  %
  %   Example:
  %     URL = ieegurl('view.ieeg.org','', true);
  %
  %   See also: IEEGCONNECT IEEGSETTINGS

  if isdeployed
      isSecure = strcmp(secure, 'true');
  else
      isSecure = secure;
  end

  if isSecure
      prefix = 'https';
      port = '8443';
  elseif strcmp(server, 'localhost')
      prefix = 'http';
      port = '8888';
  else
      prefix = 'http';
      port = '8088';
  end

  if strcmp(context, '/')
      spc = '';
  else
      spc = '/';
  end

  url = [prefix '://' server ':' port context spc 'services'];
        
end

function url = ieegurlnoport(server, context, secure )
  %IEEGURLNOPORT  Creates the url to the IEEG server.
  %   URL = IEEGURL('server', 'context', ISSECURE) returns the URL to request
  %   data from the Web service. SERVER is a string that specifies the server
  %   location, CONTEXT specifies the services location on the server and
  %   ISSECURE specifies whether a secure connection should be used.
  %
  %   If the code is deployed, a secure connection is used irrespective of the
  %   input arguments.
  %
  %   Example:
  %     URL = ieegurl('view.ieeg.org','', true);
  %
  %   See also: IEEGCONNECT IEEGSETTINGS

  if isdeployed
      isSecure = strcmp(secure, 'true');
  else
      isSecure = secure;
  end

  if isSecure
      prefix = 'https';
  elseif strcmp(server, 'localhost')
      prefix = 'http';
  else
      prefix = 'http';
  end

  if strcmp(context, '/')
      spc = '';
  else
      spc = '/';
  end

  url = [prefix '://' server context spc 'services'];
        
end

