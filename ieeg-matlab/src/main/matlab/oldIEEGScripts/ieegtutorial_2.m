function varargout = ieegtutorial_2(username)
  %IEEGEXAMPLETOOL  Test tool for IEEG-Toolbox.
  %   IEEGEXAMPLETOOL('userName') can be used to test the interface with the
  %   IEEG portal.
  %
  %   [SNAPID, PARAMS, TASKPARAMS] = IEEGTUTORIAL_2(...) returns the snapshotID
  %   and the PARAMS and TASKPARAMS structures that were used for this tool. In
  %   case the tool was run in parallel mode, the TASKPARAMS is associated with
  %   the last task that was performed.
  %
  %   In the parallel test case, a similar result should appear but the pattern
  %   should slant from top-right to bottom left as the proportion is reversely
  %   correlated with the task iteration.
  %
  %   Each annotation should be 0.1 seconds long and have the type 'Test' and
  %   the description ('Results of the IEEGTutorial_2 tool.'). The creator
  %   should be the username used to run the tool.
  %
  %   -- -- -- -- -- --
  %
  %   In order to use this example, make sure that you:
  %     1)  Created a password file associated with your login name 
  %         using IEEGPWDFILE).
  %     2)  Use the IEEGSETTINGS method to update the settings for
  %         the toolbox such as the server name, context and location 
  %         of the password file.
  %     3)  Created a job in the IEEG-portal using the toolID that is 
  %         specified below.
  %
  %   The general workflow of the IEEG-Toolbox is to get a job from the server
  %   using IEEGCONNECT, then repeatedly use IEEGGETBLOCK and IEEGANNOTATE to
  %   get data and push annotations back to the server. When the job is
  %   finished, you can use IEEGCLEARJOB to remove the connection from MATLAB's
  %   memory. 
  %
  %   If you want to keep track of the current status of the session, you can
  %   use the IEEGMONITOR or IEEGSTATUS.
  %
  %   NOTE: The code in this example is by no means the only correct way to
  %   set up your analysis; it only provides one possible implementation. It
  %   would be very easy to set an analysis that is not limited to a single
  %   channel per iteration.
  %
  %   Example: 
  %     ieegpwdfile('John Doe', 'aPassW0rd');
  %     ieegsettings('pwdfile', 'c:\mydocument\myPWDlocation');
  %     ieegtutorial_2('JohnDoe');
  %
  %   See also: IEEGPWDFILE IEEGSTATUS IEEGMONITOR IEEGCONNECT IEEGGETBLOCK
  %             IEEGANNOTATE IEEGCLEARJOB
  
  % ****************************************************************************
  % * Copyright 2010-12 Trustees of the University of Pennsylvania
  % * 
  % * Licensed under the Apache License, Version 2.0 (the "License");
  % * you may not use this file except in compliance with the License.
  % * You may obtain a copy of the License at
  % * 
  % *   http://www.apache.org/licenses/LICENSE-2.0
  % * 
  % * Unless required by applicable law or agreed to in writing, software
  % * distributed under the License is distributed on an "AS IS" BASIS,
  % * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  % * See the License for the specific language governing permissions and
  % * limitations under the License.
  % ****************************************************************************
   
  % Set ToolId for IEEGTutorial_2
  TOOLID = 'caf7d6a8-d060-467c-998d-0c6916b88869';

  ieegconnect(username, TOOLID);          % Connect to server, get session.
  [values, startT] = ieeggetblock;        % Getting first block of data.
  status      = ieegstatus();             % Get all status information.
  taskParams  = status.taskParams;        % Get TaskParameters 
  nrChannels  = length(taskParams.chIds); % Get number of channels.
  
  % Analyze data until all blocks/tasks are analyzed. 
  while ~isempty(values)
    for iChannel = 1: nrChannels
      results = doanalysis(values, startT, iChannel);
      if ~isempty(results.start)
        ieegannotate(results, 'Test', 'Results of the IEEGTutorial_2 tool.');
      end
    end
    [values, startT] = ieeggetblock; % Get the next block of data.
  end
  
  %Finish job, and show snapshot id for results.
  [snapId, params, taskparams] = ieegclearjob(); 
  
  if nargout
    varargout = {snapId params taskparams};
  end
  
end


function results = doanalysis(values, startT, iChan) %#ok<INUSL>
  %DOANALYSIS  Generates test annotations
  %   RESULTS = DOANALYSISS(VALUES, STARTT, ICHAN) returns a structure
  %   with annotation timestamps for a single channel. The timestamp of the
  %   first index is provided in STARTT.
  %
  %   We do not use the VALUES input for this example but naturally, this input
  %   is used during standard analysis.

  results = struct('chIndex',iChan, 'start',[], 'stop',[], 'value',[]);

  % Create annotation with offset depending on iTask
  curTask = ieegstatus('totTask');
  totNrTasks = length(ieegstatus('params.chIds'));
  blockLength = ieegstatus('params.blockSize');
  offset = blockLength - (curTask-1)/totNrTasks * blockLength;

  results.start = startT + offset;
  results.stop = results.start+0.1;

end
 