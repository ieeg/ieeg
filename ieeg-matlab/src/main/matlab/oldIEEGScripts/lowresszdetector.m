function lowresszdetector(userName)
  %LOWRESSZDETECTOR  Very basic seizure detector.
  %
  % NOTE: Current implementation has a hardcoded threshold that is set for
  % sampling rates of 200Hz. 

  TOOLID    = '566a0c66-b96f-48a4-a6fa-a13d12dc17e8';
  
  % Initiate session
  ieegconnect(userName, TOOLID);
  st      = ieegstatus;               % Get all status information.
  sf      = st.params.sampleFreq;     % Get the sampling frequency of the data.

  % Getting first block of data.
  [values, startT] = ieeggetblock;       
  nrChannels = size(values,2); % Get number of channels.
  
  % Analyze data until all blocks/tasks are analyzed. 
  while ~isempty(values)
    for iChannel = 1: nrChannels
      
      % Do data analysis on single channel
      endT    = startT + size(values,1)*(1/sf);
      results   = dolinelength(values(:,iChannel), startT, endT);
      
      % Check which spikes are bigger than threshold.
      if ~isempty(results)
        results.chIndex = iChannel;
        ieegannotate(results);
      end
    end
    
    % Get the next block of data.
    [values, startT] = ieeggetblock; 
  end
  
  %Finish job, and get snapshot id for results.
  ieegclearjob(); 

end


function results = dolinelength(values, startT, endT)

  % Low-resolution seizure detector. This only takes 4 sec (set by blockSize above)
  % of non-overlapping data in channel Grid22 and annotates when it sees a seizure.
  % This is piggybacking feature extraction onto the Web service download operation.
  % For more flexible feature extraction, including overlapping sliding windows,
  % move annotation creation from on-the-fly to being performed only after all data
  % have been received.
  
  LL = mean(abs(diff(values)));  % Signal length-independent line length. 
  
  % Annotate segments where line length thinks there is a seizure
  
  % thr=30 is for sampling rate=200 Hz; other rates are somewhat compensated for
  if LL>=30/length(values)*800  
    results = struct('chIndex',[], 'start', startT, 'stop', endT);
  else
    results = [];
  end
end
