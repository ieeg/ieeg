function ieegexample_1(userName)
  %IEEGEXAMPLE_1 IEEG Example Workflow.
  %
  %   The workflow described in this script differs from the IEEGTUTORIAL files
  %   in that it requests multiple blocks of data before the analysis is
  %   performed. 
  %
  %   Note that we use combination of a for and a while loop to iterate over all
  %   the blocks of data.

  ToolID = 'e7b45606-e375-404d-b153-633e0228fb1c';
  ieegconnect(userName, ToolID, 'continuous');

  st = ieegstatus;
  st.params;  
  nchans = size(st.params.channelNames,1);
  
  % Set the number of blocks you want to fetch before running the analysis. If
  % you leave this set to 20, you will get 20 blocks, then run the analysis code
  % and then get the next 20 blocks.
  nBlockPerAnalysis = 5;   
  
  % Perform analysis on blocks of 1hour data until all blocks are processed.
  taskAlive = true;
  while taskAlive

    % Init values
    endIndex = 0; 
    values = [];  
    
    % Get multiple blocks of data and concatenate them in a single array.
    for iBlock = 1: nBlockPerAnalysis 
      
      % Get Block
      [tmp_values, ~] = ieeggetblock;
      
      % Find Block Length
      blockLength = size(tmp_values,1);

      % Initialize values during the first run.
      if isempty(values)
        values = zeros(nBlockPerAnalysis*blockLength, nchans); 
      end

      % Check if data is returned, append block to other blocks
      if ~isempty(tmp_values)
        startIndex = endIndex + 1;
        endIndex = startIndex + blockLength -1;
        values(startIndex: endIndex, :) = tmp_values; 
      else
        % Truncate in case last block is shorter than the rest.
        values = values(1 : endIndex, :);
        
        % Set the taskAlive flag to false, all blocks are processed.
        taskAlive = false;
        break
      end

    end 

    % Display information
    fprintf(2,'--Do work now-- Array Size: [%ix%i]\n',...
      size(values,1), size(values,2));
    
    
    
    % -- -- -- -- -- -- --
    % CALL YOUR ANALYSIS FUNCTION HERE
    % -- -- -- -- -- -- -- 
    annotations = doanalysis();  %PlaceHolder for your analysis
    

    
    
    % Upload the annotations. Annotations should be an array of structs. Each
    % index should contain the annotations for a single channel.
    for j = 1 : length(annotations)
      ieegannotate(annotations(j));
    end
    
  end

  % Clear the Job using IEEGCLEARJOB
  ieegclearjob
end

function annotations = doanalysis()
  annotations = [];
end

