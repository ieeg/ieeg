The International Epilepsy Electrophysiology Portal is a collaborative initiative funded by the National Institutes of Neurological Disorders and Stroke. This initiative seeks to advance research towards the understanding of epilepsy by providing a platform for sharing data, tools and expertise between researchers. The portal includes a large database of scientific data and tools to analyze these datasets. (United States National Institutes of Health Grant # 1 U24 NS063930-01)


https://www.ieeg.org