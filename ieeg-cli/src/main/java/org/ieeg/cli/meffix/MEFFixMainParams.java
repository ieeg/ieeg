/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.meffix;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.beust.jcommander.Parameter;

/**
 * Parameters for the MefValidateMain Class.
 * 
 * @author  Jordan Hurwitz
 * 
 */
public class MEFFixMainParams {
	
	@Parameter(description = "<file names to repair>")
	private List<String> fileNames = newArrayList();
	
	@Parameter(
			names = {
					"-h",
					"--help" },
			description = "print help message",
			help = true)
	private boolean help = false;

	public boolean isHelp() {
		return help;
	}
	
	public List<String> getFileNames() {
		return fileNames;
	}
}
