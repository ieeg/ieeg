/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.meffix;

import java.util.List;

import org.ieeg.cli.meffix.MEFFixMainParams;
import org.ieeg.cli.meffix.MEFFixer;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

/**
 * The main class for a MEF repair system.
 * Parses the command line arguments and creates mef fixers for each file.
 * Outputs the successes of the repair
 * 
 * @author Jordan Hurwitz
 * 
 */
public class MEFFixMain {
	public static void main(String[] args) {
		final MEFFixMainParams mainParams = new MEFFixMainParams();

		final JCommander jc = new JCommander(mainParams);
		jc.setProgramName("meffix");
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			jc.usage();
			System.exit(1);
		}
		if (mainParams.isHelp()) {
			System.out
					.println('\n' + "meffix corrects some errors in the mef.");
			System.out
					.println("See https://code.google.com/p/braintrust/wiki/meffix" + '\n');
			jc.usage();
			return;
		}

		if (!mainParams.getFileNames().isEmpty()) {
			List<String> fileNames = mainParams.getFileNames();
			for (int i = 0; i < fileNames.size(); i++) {
				MEFFixer mf = new MEFFixer(fileNames.get(i));
				System.out.println(mf.status());
			}
		}
	}

}
