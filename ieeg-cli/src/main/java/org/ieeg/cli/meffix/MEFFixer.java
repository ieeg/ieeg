/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.meffix;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import edu.upenn.cis.eeg.EegFileUtils;
import edu.upenn.cis.eeg.mef.MefHeader2;

/**
 * 
 * Fixer for the MEF format. Check the wiki for usage:
 * https://code.google.com/p/braintrust/wiki/meffix
 * 
 * @author Jordan Hurwitz
 * 
 */
public class MEFFixer {
	
	private String fileName;
	private RandomAccessFile raf;
	private byte[] header;
	private byte[] entryStart;
	private byte[] indexStart;
	long offsetOfIndex;
	final int ENTRY_START_OFFSET = 1032;

	private Long headerStartTime;
	private Long entryStartTime;
	private Long indexStartTime;
	
	private Boolean headerTimeFixed;
	private Boolean indexTimeFixed;


	public MEFFixer(String fileName) {
		this.fileName = fileName;
		header = new byte[1024];
		entryStart = new byte[8];
		indexStart = new byte[8];
		fixFile();
	}

	// Attempts to read file and construct MEFHeader. Queries MEFHeader for
	// relevant offsets
	private void fixFile() {

		try {
			raf = new RandomAccessFile(fileName, "rw");
			raf.read(header);
			raf.seek(ENTRY_START_OFFSET);
			raf.read(entryStart);

			MefHeader2 h = new MefHeader2(header);
			offsetOfIndex = h.getIndexDataOffset();
			
			raf.seek(offsetOfIndex);
			raf.read(indexStart);
			
			headerStartTime = h.getRecordingStartTime();
			
			fixTimes(entryStart, indexStart, h);

		} catch (FileNotFoundException fnf) {
			fnf.printStackTrace();
			System.exit(2);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.exit(2);
		} catch (RuntimeException rte) {
			rte.printStackTrace();
			System.exit(2);
		} catch (Error e) {
			e.printStackTrace();
			System.exit(2);

		} finally {
			if (raf != null) {
				try {
					raf.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		}
	}

	private void fixTimes(byte[] es, byte[] is, MefHeader2 h) throws IOException {
		entryStartTime = EegFileUtils.getLongFromBytes(es);
		indexStartTime = EegFileUtils.getLongFromBytes(is);
		
		if(!headerStartTime.equals(entryStartTime)){
			h.setRecordingStartTime(entryStartTime);
			headerStartTime = h.getRecordingStartTime();
			raf.seek(408);
			raf.write(entryStart);
			headerTimeFixed = true;
		}
		
		if(!indexStartTime.equals(entryStartTime)){
			raf.seek(offsetOfIndex);
			raf.write(entryStart);
			indexTimeFixed = true;
		}
		
		
	}

	public String status() {
		String s = "";
		if(headerTimeFixed != null){
			if(headerTimeFixed == true) s = s + "Fixed header start time to match entry start time";
			else  s = s + "identified problem with header, unable to fix it";
		}
		if(indexTimeFixed != null){
			if(indexTimeFixed == true) s = s + "Fixed index start time to match entry start time";
			else  s = s + "identified problem with index, unable to fix it";
		}
		return s;
	}

}
