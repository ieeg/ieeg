/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.mefvalidate;

import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

/**
 * The main class for a MEF integrity checker. Parses the command line arguments
 * and creates mefvalidators for each file. Outputs the results of the tests
 * 
 * @author Jordan Hurwitz
 * 
 */
public class MEFValidateMain {

	public static void main(String[] args) {
		final MEFValidateMainParams mainParams = new MEFValidateMainParams();

		final JCommander jc = new JCommander(mainParams);
		jc.setProgramName("mefvalidate");
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			jc.usage();
			System.exit(2);
		}
		if (mainParams.isHelp()) {
			System.out
					.println('\n' + "mefvalidate checks for errors in one or more mef.");
			System.out
					.println("See https://bitbucket.org/ieeg/ieeg/wiki/mefvalidate.md" + '\n');
			jc.usage();
			return;
		}

		MEFValidator mv = null;
		boolean passed = true;

		if (!mainParams.getFileNames().isEmpty()) {
			List<String> fileNames = mainParams.getFileNames();
			for (int i = 0; i < fileNames.size(); i++) {
				String fileName = fileNames.get(i);
				mv = new MEFValidator(fileName);
				System.out.println('\n' + "Starting Validation on "
						+ fileName + "...");
				System.out.println("Report: ");
				System.out.println(mv.getMaxPresent());
				System.out.println(mv.getMinPresent());
				System.out.println(mv.getStartTimesMatch());
				System.out.println(mv.getCorrectBlockSize());
				System.out.println(mv.getBlocksOverlap());
				System.out.println("Completed validation of " + fileName);
				if (!mv.testsPass()) passed = false;
			}
		}

		if (mv != null) {
			if (passed) {
				return;
			} else {
				System.exit(1);
			}
		}
	}

}
