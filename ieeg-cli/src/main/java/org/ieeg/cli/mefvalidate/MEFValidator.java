/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.mefvalidate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.List;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.EegFileUtils;
import edu.upenn.cis.eeg.mef.MEFStreamer;
import edu.upenn.cis.eeg.mef.MefHeader2;

/**
 * 
 * Validator for the MEF format. Check the wiki for usage:
 * https://bitbucket.org/ieeg/ieeg/wiki/mefvalidate.md
 * 
 * @author Jordan Hurwitz
 * 
 */
public class MEFValidator {

	private String fileName;
	private MefHeader2 header;

	private long offsetOfIndex;
	private static final int ENTRY_START_OFFSET = 1032;

	private boolean maxPresent = false;
	private boolean minPresent = false;
	private int max;
	private int min;

	private Long headerStartTime;
	private Long entryStartTime;
	private Long indexStartTime;
	private boolean startTimesMatch;

	private long numSamples;
	private long numEntries;
	private float avgBlockSize;
	private boolean correctBlockSize;
	private static final int MINIMUM_THRESHOLD = 2400;

	private long minOverlap;
	private long maxOverlap;
	private long numBlocks;
	private int overlaps;
	private boolean blocksOverlap;

	public MEFValidator(String fileName) {
		this.fileName = fileName;

		validateFile();
	}

	/**
	 * Starts the process of MEF validation by instantiating appropriate
	 * variables and making a MefHeader2. Then proceeds to perform multiple
	 * checks on file.
	 * 
	 * @see MefHeader2
	 */
	private void validateFile() {
		final byte[] headerBytes = new byte[1024];
		final byte[] entryStart = new byte[8];
		final byte[] indexStart = new byte[8];

		try (final RandomAccessFile raf = new RandomAccessFile(fileName, "r")) {

			raf.read(headerBytes);
			raf.seek(ENTRY_START_OFFSET);
			raf.read(entryStart);

			header = new MefHeader2(headerBytes);
			offsetOfIndex = header.getIndexDataOffset();
			numBlocks = header.getNumberOfIndexEntries();

			raf.seek(offsetOfIndex);
			raf.read(indexStart);

			checkMaxValue(header);
			checkMinValue(header);
			compareStartTimes(header, entryStart, indexStart);
			checkBlockSize(header);
			checkBlockOverlap(fileName, numBlocks);

		} catch (FileNotFoundException fnf) {
			System.err.println(fnf.getMessage());
			System.exit(2);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.exit(2);
		} catch (RuntimeException rte) {
			rte.printStackTrace();
			System.exit(2);
		} catch (Error e) {
			e.printStackTrace();
			System.exit(2);

		}
	}

	/**
	 * Determines if the max-value field in the MefHeader2 has been set to a
	 * value that is non-zero
	 * 
	 * @param header - the MefHeader2 of the file being validated
	 */
	private void checkMaxValue(MefHeader2 header) {
		max = header.getMaximumDataValue();
		maxPresent = max != Integer.MIN_VALUE;
	}

	/**
	 * Determines if the min-value field in the MefHeader2 has been set to a
	 * value that is non-zero
	 * 
	 * @param header - the MefHeader2 of the file being validated
	 */
	private void checkMinValue(MefHeader2 header) {
		min = header.getMinimumDataValue();
		minPresent = min != Integer.MAX_VALUE;
	}

	/**
	 * Compares the start times of the header, first entry, and the index.
	 * 
	 * @param header - the MefHeader2 of the file being validated
	 * @param entryStartBytes - the bytes encoding the start time of the first
	 *            entry
	 * @param indexStartBytes - the bytes encoding the start time listed in the
	 *            index
	 */
	private void compareStartTimes(MefHeader2 header, byte[] entryStartBytes,
			byte[] indexStartBytes) {
		headerStartTime = header.getRecordingStartTime();

		entryStartTime = EegFileUtils.getLongFromBytes(entryStartBytes);

		indexStartTime = EegFileUtils.getLongFromBytes(indexStartBytes);

		startTimesMatch = entryStartTime.equals(headerStartTime)
				&& indexStartTime.equals(headerStartTime);
	}

	/**
	 * Determines if the average size of the data blocks in the file exceeds
	 * MIN_THRESHOLD samples
	 * 
	 * @param header - the MefHeader2 of the file being validated
	 */
	private void checkBlockSize(MefHeader2 header) {
		numSamples = header.getNumberOfSamples();
		numEntries = header.getNumberOfIndexEntries();
		avgBlockSize = numEntries == 0 ? Float.NaN : numSamples / numEntries;
		correctBlockSize = !Float.isNaN(avgBlockSize) && avgBlockSize >= MINIMUM_THRESHOLD;
	}

	/**
	 * Determines if there is overlap within the first blocks. 100 blocks are
	 * checked if possible, if not possible, because the file is too small, all
	 * blocks are checked
	 * 
	 * @param fileName - the name of the file being validated
	 * @param numBlocks - the number of blocks in the file being validated
	 * @throws IOException
	 */
	private void checkBlockOverlap(String fileName, long numBlocks)
			throws IOException {
		List<TimeSeriesPage> tspList = null;
		try (final InputStream mefInputStream = new FileInputStream(fileName);
				final MEFStreamer ms = new MEFStreamer(mefInputStream, false)) {

			if (numBlocks > 100) {
				numBlocks = 100;
			}

			tspList = ms.getNextBlocks((int) numBlocks);
		}
		Long comparedEndTime = null;

		for (int i = 0; i < tspList.size(); i++) {
			TimeSeriesPage tsp = tspList.get(i);
			long startTime = tsp.timeStart;
			long endTime = tsp.timeEnd;

			if (comparedEndTime != null) {
				if (startTime < comparedEndTime) {
					overlaps++;
					blocksOverlap = true;

					if (comparedEndTime - startTime < minOverlap
							|| minOverlap == 0) {
						minOverlap = comparedEndTime - startTime;
					}

					if (comparedEndTime - startTime > maxOverlap
							|| maxOverlap == 0) {
						maxOverlap = comparedEndTime - startTime;
					}
				}
			}

			comparedEndTime = endTime;
		}

	}

	/**
	 * Returns the message result of the max existence check.
	 * 
	 * @return the string outputted by the result of the test
	 */
	public String getMaxPresent() {
		if (maxPresent)
			return "[PASS] [MaxValuePresent] maximum_data_value present: value "
					+ "[" + max + "]";
		else
			return "[FAIL] [MaxValuePresent] maximum_data_value not present";
	}

	/**
	 * Returns the message result of the min existence check.
	 * 
	 * @return the string outputted by the result of the test
	 */
	String getMinPresent() {
		if (minPresent)
			return "[PASS] [MinValuePresent] minimum_data_value present: value "
					+ "[" + min + "]";
		else
			return "[FAIL] [MinValuePresent] minimum_data_value not present";
	}

	/**
	 * Returns the message result of the cohesive start time check.
	 * 
	 * @return the string outputted by the result of the test
	 */
	public String getStartTimesMatch() {
		if (startTimesMatch)
			return "[PASS] [StartTimesMatch] header start time matches first index start time and first entry's start time:"
					+ " hst: "
					+ "["
					+ headerStartTime
					+ "]"
					+ " ist: "
					+ "["
					+ indexStartTime
					+ "]"
					+ " est: "
					+ "["
					+ entryStartTime
					+ "]";
		else
			return "[FAIL] [StartTimesMatch] header start time does not match first index start time nor first entry's start time:"
					+ " hst: "
					+ "["
					+ headerStartTime
					+ "]"
					+ " ist: "
					+ "["
					+ indexStartTime
					+ "]"
					+ " est: "
					+ "["
					+ entryStartTime
					+ "]";
	}

	/**
	 * Returns the message result of the block size check.
	 * 
	 * @return the string outputted by the result of the test
	 */
	public String getCorrectBlockSize() {
		if (correctBlockSize)
			return "[PASS] [CorrectBlockSize] average of at least "
					+ MINIMUM_THRESHOLD
					+ " samples in each block:"
					+ " (number_of_samples = "
					+ numSamples
					+ " / number_of_index_entries = "
					+ numEntries
					+ ") >= "
					+ MINIMUM_THRESHOLD
					+ ": found "
					+ "[" + avgBlockSize + "]";
		else
			return "[FAIL] [CorrectBlockSize] less than "
					+ MINIMUM_THRESHOLD
					+ " samples in each block:"
					+ " (number_of_samples = "
					+ numSamples
					+ " / number_of_index_entries = "
					+ numEntries
					+ ") < "
					+ MINIMUM_THRESHOLD
					+ ": found "
					+ "[" + avgBlockSize + "]";
	}

	/**
	 * Returns the message result of the block overlap test.
	 * 
	 * @return the string outputted by the result of the test
	 */
	public String getBlocksOverlap() {
		if (!blocksOverlap)
			return "[PASS] [NoBlockOverlap] first 100 or less block times do not overlap";
		else
			return "[FAIL] [NoBlockOverlap] first 100 or less block times overlap. Found "
					+ overlaps
					+ " overlapping blocks, min overlap ["
					+ minOverlap
					+ "usecs] max overlap of ["
					+ maxOverlap
					+ "usecs]";
	}

	/**
	 * Returns the boolean result of all checks.
	 * 
	 * @return true if all tests pass and false otherwise
	 */
	public boolean testsPass() {
		return minPresent && maxPresent && startTimesMatch && correctBlockSize
				&& !blocksOverlap;
	}

	/**
	 * Returns the boolean result of the max existence check.
	 * 
	 * @return true if the max existence check passes and false otherwise
	 */
	public boolean isMaxPresent() {
		return maxPresent;
	}

	/**
	 * Returns the boolean result of the max existence check.
	 * 
	 * @return true if the max existence check passes and false otherwise
	 */
	public boolean isMinPresent() {
		return minPresent;
	}

	/**
	 * Returns the boolean result of the cohesive start time check.
	 * 
	 * @return true if the cohesive start time check passes and false otherwise
	 */
	public boolean doStartTimesMatch() {
		return startTimesMatch;
	}

	/**
	 * Returns the boolean result of the block size check.
	 * 
	 * @return true if the block size check passes and false otherwise
	 */
	public boolean isCorrectBlockSize() {
		return correctBlockSize;
	}

	/**
	 * Returns the boolean result of the block overlap check.
	 * 
	 * @return true if the block overlap check passes and false otherwise
	 */
	public boolean doBlocksOverlap() {
		return blocksOverlap;
	}

}
