/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.BaseConverter;

/**
 * Parameters for the ieeg download-objects.
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "List montages")
public class IeegMontageListCommand {

	public static enum OutputType {
		SUMMARY("summary"), JSON("json"), INI("ini");

		private final String optionValue;

		OutputType(String optionValue) {
			this.optionValue = optionValue;
		}

		public static final String description = "'summary' (default), 'json', or 'ini'";

		public static OutputType fromString(String value) {
			for (final OutputType ot : OutputType.values()) {
				if (ot.optionValue.equalsIgnoreCase(value)) {
					return ot;
				}
			}
			return null;
		}
	}

	public static final class OutputTypeConverter extends
			BaseConverter<OutputType> {

		public OutputTypeConverter(String optionName) {
			super(optionName);
		}

		@Override
		public OutputType convert(String value) {
			final OutputType type = OutputType.fromString(value);
			if (type == null) {
				throw new ParameterException("Invalid output type: [" + value
						+ "]. If specified must be " + OutputType.description);
			}
			return type;
		}

	}

	final public static String DEFAULT_CHANNEL_SEPARATOR = "-";
	final public static String DEFAULT_PAIR_SEPARATOR = ",";

	final public static String COMMAND_NAME = "list-montages";

	final public static String CHANNEL_SEPARATOR_SHORT = "-c";
	final public static String PAIR_SEPARATOR_SHORT = "-p";

	@Parameter(description = "<montages to list>")
	private List<String> montages = newArrayList();

	@Parameter(names = {
			"-o",
			"--output"
	},
			description = OutputType.description,
			converter = OutputTypeConverter.class)
	private OutputType output = OutputType.SUMMARY;

	@Parameter(
			names = {
					CHANNEL_SEPARATOR_SHORT,
					"--channel-sepatator"
			},
			description = "String to be used to separate channels in a pair. Ignored unless output is INI")
	private String channelSeparator = DEFAULT_CHANNEL_SEPARATOR;

	@Parameter(
			names = {
					PAIR_SEPARATOR_SHORT,
					"--pair-sepatator"
			},
			description = "String to be used to separate pairs in a list. Ignored unless output is INI")
	private String pairSeparator = DEFAULT_PAIR_SEPARATOR;

	@Parameter(
			names = {
					"-d",
					"--dataset" },
			description = "<name of dataset>",
			required = true)
	private String datasetName;

	public List<String> getMontages() {
		return montages;
	}

	public String getDatasetName() {
		return datasetName;
	}

	public OutputType getOutput() {
		return output;
	}

	public String getChannelSeparator() {
		return channelSeparator;
	}

	public String getPairSeparator() {
		return pairSeparator;
	}

	public void checkChannelSeparator(String montageName, String channel) {
		if (channel.contains(channelSeparator)) {
			throw new IllegalArgumentException("Channel "
					+ channel
					+ " in montage "
					+ montageName
					+ " contains channel separator ["
					+ channelSeparator
					+ "]. Use "
					+ CHANNEL_SEPARATOR_SHORT
					+ " option to specify a different separator.");
		}
	}

	public void checkPairSeparator(String montageName, String channel) {
		if (channel.contains(pairSeparator)) {
			throw new IllegalArgumentException("Channel "
					+ channel
					+ " in montage "
					+ montageName
					+ " contains pair separator ["
					+ pairSeparator
					+ "]. Use "
					+ PAIR_SEPARATOR_SHORT
					+ " option to specify a different separator.");
		}
	}
	
	public void checkSeparators() {
		if (channelSeparator.equals(pairSeparator)) {
			throw new IllegalArgumentException("channel separator ["
					+ channelSeparator
					+ "] is the same as pair separator ["
					+ pairSeparator
					+ "]");
		}
	}
}
