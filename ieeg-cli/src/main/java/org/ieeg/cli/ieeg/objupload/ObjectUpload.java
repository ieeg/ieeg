/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.objupload;

import static com.google.common.base.Preconditions.checkNotNull;

import com.amazonaws.services.s3.transfer.Upload;

public final class ObjectUpload {
	private final String name;
	private final Upload upload;
	private volatile boolean cancelled = false;

	public ObjectUpload(
			String name,
			Upload upload) {
		this.name = checkNotNull(name);
		this.upload = checkNotNull(upload);
	}

	public boolean isCancelled() {
		return cancelled;
	}
	
	public String getName() {
		return name;
	}

	public Upload getUpload() {
		return upload;
	}

	public void cancel() {
		cancelled = true;
	}

}