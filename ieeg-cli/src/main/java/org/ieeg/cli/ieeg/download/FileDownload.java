/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;

import org.ieeg.cli.ieeg.IHasTransferState;
import org.ieeg.cli.ieeg.TransferState;

public final class FileDownload implements IHasTransferState {

	private final String name;
	private final long sizeBytes;
	private long writtenBytes;
	private TransferState state;

	public FileDownload(
			String name,
			long sizeBytes) {
		this.name = checkNotNull(name);
		this.sizeBytes = sizeBytes;
		this.writtenBytes = 0;
		this.state = TransferState.WAITING;
	}

	public String getName() {
		return name;
	}

	public long getSizeBytes() {
		return sizeBytes;
	}

	public synchronized TransferState getState() {
		return state;
	}

	public synchronized long getWrittenBytes() {
		return writtenBytes;
	}

	public synchronized void setState(TransferState state) {
		this.state = state;
	}

	public synchronized void setWrittenBytes(long writtenBytes) {
		this.writtenBytes = writtenBytes;
	}

}
