/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.staging.delete;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegStagingDeleteCommand;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.ieeg.cli.ieeg.staging.S3ClientFactory;
import org.ieeg.cli.ieeg.staging.S3ClientFactory.S3ClientCredentials;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.beust.jcommander.JCommander;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

/**
 * @author John Frommeyer
 *
 */
public class StagingDeleter {

	private final static int deleteObjectLimit = 1000;
	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegStagingDeleteCommand stagingDeleteCommand;
	private final S3ClientFactory s3Factory;

	public StagingDeleter(JCommander jc,
			IeegMainParams mainParams,
			IeegStagingDeleteCommand stagingDeleteCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.stagingDeleteCommand = checkNotNull(stagingDeleteCommand);
		this.s3Factory = new S3ClientFactory(wsClient);
	}

	/**
	 * 
	 * @return exit status
	 */
	public int delete() {
		final String collectionName = stagingDeleteCommand.getCollectionName();
		final S3ClientCredentials s3ClientCredentials = s3Factory
				.newClient(mainParams.isVerbose(),
						stagingDeleteCommand.awsCertChecking.isDisabled(),
						collectionName);

		final String authzBucket = s3ClientCredentials.getAuthzBucket();
		final String authzPrefix = s3ClientCredentials.getAuthzPrefix();

		final AmazonS3Client s3Client = s3ClientCredentials.getS3Client();
		final ListObjectsRequest loRequest = new ListObjectsRequest(
				authzBucket, authzPrefix, null, null, null);

		ObjectListing listObjects = s3Client.listObjects(loRequest);

		final List<String> toBeDeleted = new ArrayList<>();
		for (final S3ObjectSummary summary : listObjects.getObjectSummaries()) {
			toBeDeleted.add(summary.getKey());
		}
		checkState(listObjects.getCommonPrefixes().isEmpty());

		boolean truncated = listObjects.isTruncated();
		while (truncated) {
			listObjects = s3Client.listNextBatchOfObjects(listObjects);
			for (final S3ObjectSummary summary : listObjects
					.getObjectSummaries()) {
				toBeDeleted.add(summary.getKey());
			}
			checkState(listObjects.getCommonPrefixes().isEmpty());

			truncated = listObjects.isTruncated();
		}

		if (toBeDeleted.isEmpty()) {
			System.out.println("No objects found under " + authzPrefix);
			return 0;
		}
		boolean okayToDelete = true;
		if (!stagingDeleteCommand.isForce()) {
			final int size = toBeDeleted.size();
			final String prompt = size == 1
					? "delete " + toBeDeleted.get(0) + " ? "
					: size <= 5
							? "delete\n" + Joiner.on("\n").join(toBeDeleted)
									+ "\n? "
							: "delete " + size + " files from " + authzBucket
									+ "/"
									+ authzPrefix + " ? ";
			System.out.print(prompt);

			@SuppressWarnings("resource")
			Scanner inputScanner = new Scanner(System.in);
			String response = inputScanner.next();
			if (!response.startsWith("y") && !response.startsWith("Y")) {
				okayToDelete = false;
			}
		}

		if (!okayToDelete) {
			System.out.println("delete canceled");
		} else {
			final List<List<String>> keyPartition = Lists.partition(
					toBeDeleted, deleteObjectLimit);
			int deleteCount = 0;
			for (final List<String> keys : keyPartition) {
				final DeleteObjectsRequest req = new DeleteObjectsRequest(
						authzBucket)
						.withKeys(keys
								.toArray(new String[keys.size()]));
				final DeleteObjectsResult result = s3Client.deleteObjects(req);
				deleteCount += result.getDeletedObjects().size();
			}
			System.out.println("deleted " + deleteCount
					+ " files");
			if (deleteCount != toBeDeleted.size()) {
				System.out.println("not all requested files were deleted");
			}
		}

		return 0;
	}
}
