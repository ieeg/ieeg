/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.recobjupload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IHasTransferState;
import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegRecObjListCommand;
import org.ieeg.cli.ieeg.IeegRecObjUploadCommand;
import org.ieeg.cli.ieeg.IeegUtil;
import org.ieeg.cli.ieeg.TransferState;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;
import com.google.common.base.Stopwatch;
import com.google.common.base.Throwables;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.upenn.cis.db.mefview.services.IeegWsError;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteAppException;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class RecObjUploader {
	private final class UploadRecObjWRetryTask implements
			Callable<RecObjUpload> {
		private final RecObjUpload upload;
		private final File file;
		private final String objectMd5Base64;
		private final String datasetId;

		UploadRecObjWRetryTask(
				RecObjUpload upload,
				File file,
				String objectMd5Base64,
				String datasetId) {
			this.upload = checkNotNull(upload);
			this.file = checkNotNull(file);
			this.objectMd5Base64 = checkNotNull(objectMd5Base64);
			this.datasetId = checkNotNull(datasetId);
		}

		@Override
		public RecObjUpload call() throws Exception {
			upload.setState(TransferState.RUNNING);
			final String objectName = upload.getName();
			System.out.println(
					"Starting upload of "
							+ objectName);
			int retries = 5;
			RecordingObject recordingObject = null;
			try {
				recordingObject = oneTry(
						objectName,
						1,
						retries);

				for (int i = 0; recordingObject == null && i < retries; i++) {
					System.out
							.println("Waiting 5 seconds to retry upload of "
									+ objectName);
					Thread.sleep(5000);
					System.out
							.println("Retrying upload of "
									+ objectName);
					recordingObject = oneTry(
							objectName,
							i + 2,
							retries - i - 1);
				}

			} finally {
				upload.setRecordingObject(recordingObject);
				if (recordingObject == null) {
					upload.setState(TransferState.FAILED);
					System.out.println(
							"Upload of "
									+ objectName
									+ " failed.");
				} else {
					upload.setState(TransferState.FINISHED);
					System.out.println(
							"Upload of "
									+ objectName
									+ " completed successfully.");
				}
			}
			return upload;

		}

		private RecordingObject oneTry(
				final String objectName,
				int thisTry,
				int retries) {
			final Stopwatch watch = Stopwatch.createStarted();
			try {
				wsClient.testCreateObject(
						datasetId,
						objectName);
				final FileInputStream is = new FileInputStream(file);
				final RecordingObject recordingObject = wsClient
						.createObject(
								datasetId,
								is,
								objectName,
								objectMd5Base64);
				return recordingObject;
			} catch (IeegWsRemoteAppException e) {
				final String errorCode = e.getErrorCode();
				if (errorCode.equals(IeegWsError.AUTHORIZATION_FAILURE
						.getCode())
						|| errorCode.equals(IeegWsError.BAD_OBJECT_NAME
								.getCode())
						|| errorCode.equals(IeegWsError.DUPLICATE_NAME
								.getCode())
						|| errorCode.equals(IeegWsError.NO_SUCH_RECORDING
								.getCode())
						|| errorCode.equals(IeegWsError.UPLOAD_IN_PROGRESS
								.getCode())) {
					upload.setIeegErrorCode(errorCode);
					throw new IllegalStateException(e);

				}
				String exceptionMsg = e.getMessage();
				System.err
						.println(
						"An error occurred while uploading "
								+ objectName
								+ ": "
								+ exceptionMsg
								+ ((exceptionMsg.endsWith(".")) ? ""
										: ".")
								+ " Will retry upload "
								+ retries
								+ " more times.");
				if (e.getErrorCode().equals(
						IeegWsError.TOO_MANY_REQUESTS.getCode())) {
					System.err
							.println("That could mean the server is still busy processing canceled requests.");
				}
			} catch (Exception e) {
				System.err.println(
						"An error occurred while uploading "
								+ objectName
								+ ": "
								+ "\n"
								+ Throwables.getStackTraceAsString(e)
								+ "Will retry upload "
								+ retries
								+ " times.");

			} finally {
				watch.stop();
				System.out.println("Try "
						+ thisTry
						+ " for "
						+ objectName
						+ ": "
						+ watch.elapsed(TimeUnit.SECONDS)
						+ " seconds");
			}
			return null;
		}
	}

	private final class RecObjErrorUploadReporter implements
			FutureCallback<RecObjUpload> {

		private String fileName;
		private IHasTransferState hasTransferState;

		public RecObjErrorUploadReporter(
				String fileName,
				IHasTransferState hasDownloadState) {
			this.fileName = checkNotNull(fileName);
			this.hasTransferState = checkNotNull(hasDownloadState);
		}

		@Override
		public void onSuccess(@Nullable RecObjUpload result) {}

		@Override
		public void onFailure(Throwable t) {
			String errorMsg = Throwables.getStackTraceAsString(t);
			System.err
					.println(
					"An error occured during the upload of file "
							+
							fileName
							+ ": "
							+ "\n"
							+ errorMsg);
			if (hasTransferState.getState() != TransferState.FAILED) {
				System.err
						.println("Although there was an exception the final state of the upload is reported as "
								+ hasTransferState.getState()
								+ ". Please check the recording objects of "
								+ datasetName
								+ " before trying again.");
			}
		}

	}

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegRecObjUploadCommand recObjUploadCommand;
	private final WebServiceClient wsClient;
	private final String datasetName;
	private final ListeningExecutorService threadPool;

	/**
	 * 
	 * @param jc
	 * @param mainParams
	 * @param recObjUploadCommand
	 * @param wsClient
	 * @param threads
	 */
	public RecObjUploader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegRecObjUploadCommand recObjUploadCommand,
			WebServiceClient wsClient,
			int threads) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.recObjUploadCommand = checkNotNull(recObjUploadCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = recObjUploadCommand.getDatasetName();
		threadPool = MoreExecutors
				.listeningDecorator(Executors.newFixedThreadPool(threads));
	}

	public int upload() {
		final Stopwatch watch = Stopwatch.createStarted();
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}
		final List<String> objectPaths = recObjUploadCommand.getObjects();
		if (objectPaths.isEmpty()) {
			System.out.println("No objects specified.");
			return 0;
		}

		Set<String> existingObjectNames = newHashSet();
		try {
			final RecordingObjects existingObjectsContainer = wsClient
					.listObjects(datasetId);
			for (RecordingObject existingObject : existingObjectsContainer
					.getRecordingObjects()) {
				existingObjectNames.add(existingObject.getName());
			}
		} catch (IeegWsRemoteException e) {
			System.out
					.println(
					"Could not retrieve existing objects for "
							+ datasetName
							+ ": "
							+ e.getMessage()
							+ "\n"
							+ "Will continue with upload without first checking if objects already exist.");

		}
		int exitStatus = 0;
		final Set<String> notUploaded = newLinkedHashSet();
		final Set<RecObjUpload> uploads = newLinkedHashSet();
		for (final String objectPath : objectPaths) {
			final File objectFile = new File(objectPath);
			final String objectName = objectFile.getName();
			if (existingObjectNames.contains(objectName)) {
				System.err.println("Dataset "
						+ datasetName
						+ " already contains an object named "
						+ objectName
						+ ". Will not upload "
						+ objectPath);
				notUploaded.add(objectName);
				continue;
			}
			System.out.println("Computing MD5 for " + objectName);
			HashCode md5HashCode = null;
			try {
				md5HashCode = Files.asByteSource(objectFile).hash(
						Hashing.md5());
			} catch (IOException e) {
				String message = e.getMessage();
				System.err.println(
						"Could not compute MD5 hash for ["
								+ objectPath
								+ "]: "
								+ message
								+ (message.endsWith(".") ? " " : ". ")
								+ "Will not upload");
				continue;
			}
			final String objectMd5Base64 = BaseEncoding.base64().encode(
					md5HashCode.asBytes());
			System.out.println(
					"MD5 for "
							+ objectPath
							+ ": "
							+ md5HashCode.toString());
			final RecObjUpload upload = new RecObjUpload(objectName);
			final UploadRecObjWRetryTask task = new UploadRecObjWRetryTask(
					upload,
					objectFile,
					objectMd5Base64,
					datasetId);
			final ListenableFuture<RecObjUpload> uploadFuture = threadPool
					.submit(task);
			Futures.addCallback(
					uploadFuture,
					new RecObjErrorUploadReporter(
							objectPath,
							upload));
			uploads.add(upload);
		}
		exitStatus = waitForCompletion(
				uploads,
				notUploaded);
		System.out.println("Upload time: "
				+ watch.stop().elapsed(TimeUnit.SECONDS) + " seconds");
		return exitStatus;
	}

	private int waitForCompletion(
			final Set<RecObjUpload> uploads,
			final Set<String> notUploaded) {
		int exitStatus = 0;
		if (uploads.isEmpty()) {
			System.out.println("No uploads started");
		} else {

			while (true) {
				final Set<RecObjUpload> successful = newLinkedHashSet();
				final Set<RecObjUpload> failed = newLinkedHashSet();
				for (final RecObjUpload upload : uploads) {
					TransferState state = upload.getState();
					if (state == TransferState.FINISHED) {
						successful.add(upload);
					} else if (state == TransferState.FAILED) {
						failed.add(upload);
					}
				}
				if (successful.size() + failed.size() == uploads.size()) {
					System.out.println(successful.size() + " uploads of "
							+ uploads.size() + " completed successfully");
					System.out.println("Summary:");

					// Successful
					for (final RecObjUpload upload : successful) {
						System.out
								.println("Successful: "
										+ IeegUtil
												.recordingObjectDisplayString(upload
														.getRecordingObject()
														.get()));
					}

					// Failed
					for (final RecObjUpload upload : failed) {
						String msg = "Failed: "
								+ upload.getName();
						if (upload.getIeegErrorCode().isPresent()) {
							String ieegErrorCode = upload.getIeegErrorCode()
									.get();
							if (ieegErrorCode.equals(
									IeegWsError.UPLOAD_IN_PROGRESS.getCode())) {
								msg += ". There is another upload in progress for dataset "
										+ datasetName
										+ " of an object with the same name. "
										+ "Please wait a few minutes and run 'ieeg "
										+ IeegRecObjListCommand.COMMAND_NAME
										+ " "
										+ datasetName
										+ "' to see if this in-progress upload completes. "
										+ "If you saw any timeout errors for this file during this run the "
										+ "in-progress upload may be one started by this run.";
							} else if (ieegErrorCode
									.equals(IeegWsError.DUPLICATE_NAME
											.getCode())) {
								msg += ". An object with the same name has been added to dataset "
										+ datasetName
										+ " during the run of this program. "
										+ "Please run 'ieeg "
										+ IeegRecObjListCommand.COMMAND_NAME
										+ " "
										+ datasetName
										+ "' to see what objects this dataset already contains. "
										+ "If you saw any timeout errors for this file during this run the "
										+ "completed upload may be one started by this run.";
							}
						}
						System.out
								.println(msg);
					}

					// Not Uploaded
					for (String name : notUploaded) {
						System.out.println("Not uploaded: " + name);
					}

					if (failed.size() > 0) {
						exitStatus = 1;
					}
					break;
				} else {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						System.err
								.println("Caught interrupted exception. Reasserting.");
					}
				}
			}
		}
		return exitStatus;
	}

	public void shutdown() {
		threadPool.shutdown();
	}
}
