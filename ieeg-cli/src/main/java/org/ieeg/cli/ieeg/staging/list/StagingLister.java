/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.staging.list;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegStagingListCommand;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.ieeg.cli.ieeg.staging.S3ClientFactory;
import org.ieeg.cli.ieeg.staging.S3ClientFactory.S3ClientCredentials;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.beust.jcommander.JCommander;

/**
 * @author John Frommeyer
 *
 */
public class StagingLister {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegStagingListCommand stagingListCommand;
	private final S3ClientFactory s3Factory;

	public StagingLister(JCommander jc,
			IeegMainParams mainParams,
			IeegStagingListCommand stagingListCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.stagingListCommand = checkNotNull(stagingListCommand);
		this.s3Factory = new S3ClientFactory(wsClient);
	}

	/**
	 * 
	 * @return exit status
	 */
	public int list() {
		final String originalCollectionName = stagingListCommand
				.getCollectionName();
		final String collectionName = originalCollectionName != null
				&& originalCollectionName
						.endsWith("/")
				? originalCollectionName.substring(0,
						originalCollectionName.length() - 1)
				: originalCollectionName;
		final S3ClientCredentials s3ClientCredentials = s3Factory
				.newClient(mainParams.isVerbose(),
						stagingListCommand.awsCertChecking.isDisabled(),
						collectionName);

		final String authzBucket = s3ClientCredentials.getAuthzBucket();
		final String authzPrefix = s3ClientCredentials.getAuthzPrefix();

		final AmazonS3Client s3Client = s3ClientCredentials.getS3Client();
		final ListObjectsRequest loRequest = new ListObjectsRequest(
				authzBucket, authzPrefix, null, "/", null);
		ObjectListing listObjects = s3Client.listObjects(loRequest);
		printPrefixes(listObjects, s3Client);
		printSummaries(listObjects);

		boolean truncated = listObjects.isTruncated();
		while (truncated) {
			listObjects = s3Client.listNextBatchOfObjects(listObjects);
			printPrefixes(listObjects, s3Client);
			printSummaries(listObjects);

			truncated = listObjects.isTruncated();
		}

		return 0;
	}

	private void printSummaries(
			final ObjectListing listObjects) {
		for (final S3ObjectSummary summary : listObjects.getObjectSummaries()) {
			System.out.println(summaryToString(summary));
		}
	}

	private void printPrefixes(
			final ObjectListing listObjects,
			AmazonS3Client s3Client) {
		for (final String commonPrefix : listObjects.getCommonPrefixes()) {
			System.out.println(prefixToString(listObjects.getBucketName(),
					commonPrefix,
					s3Client));
		}
	}

	private String summaryToString(S3ObjectSummary summary) {
		final String key = getKey(summary.getKey());
		if (stagingListCommand.isLongListing()) {
			return summary.getSize() + " "
					+ key;
		}
		return key;
	}

	private String prefixToString(String bucket,
			String prefix,
			AmazonS3Client s3Client) {
		final String key = getKey(prefix);
		if (!stagingListCommand.isLongListing()) {
			return key;
		} else {
			long sizeBytes = 0;
			final ListObjectsRequest loRequest = new ListObjectsRequest(
					bucket, prefix, null, null, null);
			ObjectListing listObjects = s3Client.listObjects(loRequest);
			checkState(listObjects.getCommonPrefixes().isEmpty());
			for (final S3ObjectSummary summary : listObjects
					.getObjectSummaries()) {
				sizeBytes += summary.getSize();
			}

			boolean truncated = listObjects.isTruncated();
			while (truncated) {
				listObjects = s3Client.listNextBatchOfObjects(listObjects);
				checkState(listObjects.getCommonPrefixes().isEmpty());
				for (final S3ObjectSummary summary : listObjects
						.getObjectSummaries()) {
					sizeBytes += summary.getSize();
				}

				truncated = listObjects.isTruncated();
			}
			return sizeBytes + " " + key;
		}
	}

	private String getKey(String key) {
		return stagingListCommand.showUserId()
				? key
				: key.replaceFirst(
						"\\d+/", "");
	}

}
