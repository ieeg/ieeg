/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.util.Set;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.TransferState;
import org.ieeg.cli.ieeg.size.SizeToStringConverter;
import org.ieeg.cli.ieeg.size.Unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;


public final class DatasetDownload {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final long toDownloadBytes;
	private final long onDiskBytes;
	private final Set<FileDownload> fileDownloads = newLinkedHashSet();
	private final Optional<TsAnnotationDownload> tsAnnDownload;
	private final long totalDatasetBytes;
	private final String totalDatasetBytesString;
	private final Unit unit;
	private final SizeToStringConverter sizeConverter = new SizeToStringConverter();

	public DatasetDownload(
			Set<FileDownload> fileDownloads,
			long toDownloadBytes,
			long totalDatasetBytes,
			@Nullable TsAnnotationDownload tsAnnDownload) {
		this.fileDownloads.addAll(checkNotNull(fileDownloads));
		this.toDownloadBytes = toDownloadBytes;
		this.totalDatasetBytes = totalDatasetBytes;
		this.onDiskBytes = this.totalDatasetBytes - this.toDownloadBytes;
		this.tsAnnDownload = Optional.fromNullable(tsAnnDownload);
		this.unit = sizeConverter.getUnitForSizeBytes(totalDatasetBytes);
		this.totalDatasetBytesString = sizeConverter
				.asString(
						totalDatasetBytes,
						unit)
				+ unit.toString();

	}

	public int waitForCompletion() {
		final String m = "waitForCompletion(...)";
		int exitStatus = 0;
		if (fileDownloads.isEmpty()) {
			System.out.println("No files to download");
		} else {
			while (true) {
				long completedBytes = 0;
				int completedFilesCount = 0;
				for (FileDownload download : fileDownloads) {
					completedBytes += download.getWrittenBytes();
					TransferState state = download.getState();
					if (state == TransferState.FINISHED
							|| state == TransferState.FAILED) {
						completedFilesCount++;
					}
				}

				System.out.println(
						"Downloaded "
								+
								sizeConverter.asString(
										onDiskBytes
												+ completedBytes,
										unit)
								+ "/"
								+ totalDatasetBytesString);
				if (completedFilesCount == fileDownloads.size()) {

					if (completedBytes == toDownloadBytes) {
						System.out.println("Download of "
								+ completedFilesCount
								+ " file"
								+ (completedFilesCount > 1 ? "s" : "")
								+ " successful.");
					} else {
						exitStatus = 1;
						System.out
								.println("Download incomplete. Try re-running the command to resume the incomplete download.");
					}
					break;
				}
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					logger.error("Reassserting interrupt", e);
				}
			}
		}
		if (tsAnnDownload.isPresent()) {

			TsAnnotationDownload tsAnnDownloadValue = tsAnnDownload.get();
			while (true) {
				TransferState tsAnnState = tsAnnDownloadValue.getState();
				if (tsAnnState == TransferState.FINISHED) {
					System.out.println(
							"Download of annotations successful. "
									+ tsAnnDownloadValue.getWrittenChars()
									+ " characters written.");
					break;
				} else if (tsAnnState == TransferState.FAILED) {
					exitStatus = 1;
					System.out
							.println("Download of annotations failed. Annotation download cannot be resumed. Please delete annotations.xml if it exists and re-run the command. Only missing or incomplete files will be downloaded.");
					break;
				} else {
					System.out
							.println(
							"Downloading annotation file. "
									+ tsAnnDownloadValue.getWrittenChars()
									+ " characters written.");
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}

				}
			}
		}
		System.out.println("Download finished.");
		return exitStatus;
	}

}
