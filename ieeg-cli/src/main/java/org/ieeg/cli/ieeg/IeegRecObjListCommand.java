/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * Parameters for the ieeg list-objects command.
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "List a dataset's recording objects")
public class IeegRecObjListCommand {

	final public static String COMMAND_NAME = "list-rec-objects";

	@Parameter(description = "<name of dataset>",
			required = true)
	private List<String> datasetNames = newArrayList();

	public List<String> getDatasetNames() {
		return datasetNames;
	}
}
