/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.concurrent.Callable;

import javax.annotation.Nullable;
import javax.ws.rs.core.Response;

import org.ieeg.cli.ieeg.TransferState;

import com.google.common.base.Throwables;

import edu.upenn.cis.db.mefview.services.IeegWsError;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteAppException;

/**
 * 
 * @author John Frommeyer
 *
 */
public final class FileDownloadTask<T> implements Callable<Void> {

	public interface IContentsProvider<T> {
		Response getContents(
				T id,
				String eTag,
				@Nullable String range);
	}

	private final IContentsProvider<T> contentProvider;
	private final FileDownload download;
	private final T fileId;
	private final String fileETag;
	private final String fileName;
	private final long fileSizeBytes;
	private final File targetFile;
	private final String range;

	FileDownloadTask(
			IContentsProvider<T> contentProvider,
			FileDownload download,
			T fileId,
			String fileETag,
			String fileName,
			long fileSizeBytes,
			File targetFile,
			@Nullable String range) {
		this.contentProvider = checkNotNull(contentProvider);
		this.download = checkNotNull(download);
		this.fileId = checkNotNull(fileId);
		this.fileETag = checkNotNull(fileETag);
		this.fileName = checkNotNull(fileName);
		this.fileSizeBytes = fileSizeBytes;
		this.targetFile = checkNotNull(targetFile);
		this.range = range;
	}

	public static <T> FileDownloadTask<T> newFileDownloadTask(
			IContentsProvider<T> contentProvider,
			FileDownload download,
			T fileId,
			String fileETag,
			String fileName,
			long fileSizeBytes,
			File targetFile,
			@Nullable String range) {
		return new FileDownloadTask<T>(
				contentProvider,
				download,
				fileId,
				fileETag,
				fileName,
				fileSizeBytes,
				targetFile,
				range);
	}

	@Override
	public Void call() throws Exception {
		download.setState(TransferState.RUNNING);
		System.out.println(
				"Starting download of "
						+ fileName);
		long bytesWritten = 0;
		boolean append = range != null;
		int retries = 20;
		boolean needRetry = false;
		try {
			FileOutputStream outputStream = null;
			InputStream inputStream = null;
			Response response = null;
			try {
				outputStream = new FileOutputStream(
						targetFile,
						append);
				response = contentProvider.getContents(
						fileId,
						fileETag,
						range);
				inputStream = response
						.readEntity(InputStream.class);
				int read = 0;
				byte[] buffer = new byte[4096];
				while ((read = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, read);
					bytesWritten += read;
					download.setWrittenBytes(bytesWritten);
				}
			} catch (IeegWsRemoteAppException e) {
				needRetry = true;
				String exceptionMsg = e.getMessage();
				System.err
						.println(
						"An error occurred while downloading "
								+ fileName
								+ ": "
								+ exceptionMsg
								+ ((exceptionMsg.endsWith(".")) ? ""
										: ".")
								+ " Will retry download "
								+ retries
								+ " more times.");
				if (e.getErrorCode().equals(
						IeegWsError.TOO_MANY_REQUESTS.getCode())) {
					System.err
							.println("That could mean the server is still busy processing canceled requests.");
				}
			} catch (Exception e) {
				needRetry = true;
				System.err.println(
						"An error occurred while downloading "
								+ fileName
								+ ": "
								+ "\n"
								+ Throwables.getStackTraceAsString(e)
								+ "Will retry download "
								+ retries
								+ " times.");
			} finally {
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				if (response != null) {
					response.close();
				}
				long actualSizeBytes = targetFile.length();
				if (actualSizeBytes == fileSizeBytes) {
					needRetry = false;
				}
			}
			if (needRetry) {
				for (int i = 0; i < retries && needRetry
						&& bytesWritten < fileSizeBytes; i++) {
					System.out
							.println("Waiting 5 seconds to retry download of "
									+ fileName);
					Thread.sleep(5000);
					System.out
							.println("Retrying download of "
									+ fileName);
					try {
						outputStream = new FileOutputStream(
								targetFile,
								true);
						String retryRange = bytesWritten == 0 ? null
								: "bytes=" + bytesWritten + "-";
						response = contentProvider.getContents(
								fileId,
								fileETag,
								retryRange);
						inputStream = response
								.readEntity(InputStream.class);
						int read = 0;
						byte[] buffer = new byte[4096];
						while ((read = inputStream.read(buffer)) != -1) {
							outputStream.write(buffer, 0, read);
							bytesWritten += read;
							download.setWrittenBytes(bytesWritten);
						}
					} catch (IeegWsRemoteAppException e) {
						needRetry = true;
						String exceptionMsg = e.getMessage();
						System.err
								.println(
								"Retry "
										+ (i + 1)
										+ ". An error occurred while retrying download of "
										+ fileName
										+ ": "
										+ exceptionMsg
										+ ((exceptionMsg.endsWith(".")) ? ""
												: ".")
										+ " Will retry download "
										+ (retries - i - 1)
										+ " more times.");
						if (e.getErrorCode().equals(
								IeegWsError.TOO_MANY_REQUESTS.getCode())) {
							System.err
									.println("That could mean the server is still busy processing canceled requests.");
						}
					} catch (Exception e) {
						needRetry = true;
						System.err
								.println(
								"Retry "
										+ (i + 1)
										+ ". An error occurred while retrying download of "
										+ fileName
										+ ": "
										+ "\n"
										+ Throwables
												.getStackTraceAsString(e)
										+ "Will retry download "
										+ (retries - i - 1)
										+ " more times.");
					} finally {
						if (outputStream != null) {
							outputStream.close();
						}
						if (inputStream != null) {
							inputStream.close();
						}
						if (response != null) {
							response.close();
						}
						long actualSizeBytes = targetFile.length();
						if (actualSizeBytes == fileSizeBytes) {
							needRetry = false;
						}
					}
				}
			}
		} finally {
			long actualSizeBytes = targetFile.length();
			if (actualSizeBytes != fileSizeBytes) {
				download.setState(TransferState.FAILED);
			} else {
				download.setState(TransferState.FINISHED);
				System.out.println(
						"Download of "
								+ fileName
								+ " finished");
			}
		}
		return null;

	}
}
