/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.size;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class SizeToStringConverter {

	public String asString(long bytes, Unit unit) {
		checkNotNull(unit);
		final double bytesPerKb = 1024.0;
		final double bytesPerMb = 1048576.0;
		final double bytesPerGb = 1073741824.0;
		switch (unit) {
			case B:
				return Long.toString(bytes);
			case KB:
				return sizeToString(bytes / bytesPerKb);
			case MB:
				return sizeToString(bytes / bytesPerMb);
			case GB:
				return sizeToString(bytes / bytesPerGb);
			default:
				throw new AssertionError("Unknown Unit: " + unit);
		}
	}

	public Unit getUnitForSizeBytes(long bytes) {
		final double bytesPerKb = 1024.0;
		final double bytesPerMb = 1048576.0;
		final double bytesPerGb = 1073741824.0;

		double asGb = bytes / bytesPerGb;
		if (asGb > 1.0) {
			return Unit.GB;
		}

		double asMb = bytes / bytesPerMb;
		if (asMb > 1.0) {
			return Unit.MB;
		}

		double asKb = bytes / bytesPerKb;
		if (asKb > 1.0) {
			return Unit.KB;
		}
		return Unit.B;
	}

	public String sizeToString(double size) {
		BigDecimal sizeBd = new BigDecimal(size)
				.setScale(
						3,
						RoundingMode.HALF_UP);
		return sizeBd.toPlainString();
	}

}
