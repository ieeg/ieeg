/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.dirupload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static java.util.Objects.requireNonNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IeegDirUploadCommand;
import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.ieeg.cli.ieeg.objupload.S3Uploader;

import com.beust.jcommander.JCommander;
import com.google.common.base.Optional;
import com.google.common.base.StandardSystemProperty;

import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;

/**
 * @author John Frommeyer
 *
 */
public class DirUploader {

	private final class GatheringFileVisitor extends
			SimpleFileVisitor<Path> {
		private final Set<File> files = new HashSet<File>();

		public Set<File> getFiles() {
			return files;
		}

		@Override
		public FileVisitResult preVisitDirectory(
				Path dir,
				BasicFileAttributes attrs)
				throws IOException
		{
			Objects.requireNonNull(dir);
			Objects.requireNonNull(attrs);
			return Files.isHidden(dir)
					? FileVisitResult.SKIP_SUBTREE
					: FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(
				Path file,
				BasicFileAttributes attrs)
				throws IOException {
			requireNonNull(file);
			requireNonNull(attrs);
			if (Files.isRegularFile(file)
					&& !Files.isHidden(file)) {
				files.add(file.toFile());
			}
			return FileVisitResult.CONTINUE;
		}

	}

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegDirUploadCommand dirUploadCommand;
	private final WebServiceClient wsClient;
	private final String awsSecretKey;
	private File datasetIniFile;

	public DirUploader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegDirUploadCommand dirUploadCommand,
			WebServiceClient wsClient,
			int threads,
			@Nullable String awsSecretKey) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.dirUploadCommand = checkNotNull(dirUploadCommand);
		this.wsClient = checkNotNull(wsClient);
		this.awsSecretKey = awsSecretKey;
	}

	public int upload() {
		int exitStatus = 0;
		if (dirUploadCommand.getDirectories().isEmpty()) {
			System.out.println("No directory given. Nothing to upload.");
			return 0;
		}
		if (dirUploadCommand.getDirectories().size() > 1) {
			System.out.println("WARNING: All but first directory ignored.");
		}

		final String directoryArg = dirUploadCommand.getDirectories().get(0);
		final Path directoryPath = Paths.get(directoryArg);
		final GatheringFileVisitor visitor = new GatheringFileVisitor();
		final int maxDepth = dirUploadCommand.isRecursive() ? Integer.MAX_VALUE
				: 1;
		try {
			Files.walkFileTree(
					directoryPath,
					Collections.<FileVisitOption> emptySet(),
					maxDepth,
					visitor);
		} catch (IOException e) {
			System.err.println(
					"Error while gathering files to upload: "
							+ e.getMessage());
			e.printStackTrace();
			return 1;
		}

		final Set<File> files = visitor.getFiles();
		exitStatus = handleDatasetIniFile(files);
		if (exitStatus != 0) {
			return exitStatus;
		}
		final S3Uploader s3Uploader = new S3Uploader(
				files,
				mainParams,
				wsClient,
				dirUploadCommand.awsCertChecking.isDisabled(),
				awsSecretKey,
				dirUploadCommand.getBucket(),
				dirUploadCommand.getCollectionName());
		exitStatus = s3Uploader.upload();
		final int successfulUploadCount = s3Uploader.getSuccessfulUploadCount();
		final int failedUploadCount = files.size() - successfulUploadCount;
		if (successfulUploadCount > 0) {
			if (failedUploadCount == 0) {
				exitStatus = registerDirectory(s3Uploader);
			} else if (failedUploadCount > 0) {
				String msg = "Directory was not registered because of failed uploads.";
				if (dirUploadCommand.getCollectionName() != null) {
					msg = "Directory for ["
							+ dirUploadCommand.getCollectionName()
							+ "] was not registered because of failed uploads.";
				}
				System.err
						.println(msg);
			}
		}
		return exitStatus;
	}

	// ieeg-dataset.ini precedence: command line, then upload dir, then home
	// directory
	private int handleDatasetIniFile(Set<File> files) {

		// Validate command line ini file if it exists.
		final File clDatasetIniFile = dirUploadCommand.getConfig();
		if (clDatasetIniFile != null) {
			if (!clDatasetIniFile.exists()) {
				System.err.println("Cannot find "
						+ clDatasetIniFile.getAbsolutePath());
				return 1;
			}
			if (!clDatasetIniFile.getName().equals(
					IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME)) {
				System.err.println("Configuration file must be named "
						+ IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME
						+ ": "
						+ clDatasetIniFile.getAbsolutePath());
				return 1;
			}
		}

		// Validate that upload dir contains at most one ini file.
		final Set<File> uploadDirDatasetIniFiles = new HashSet<>();
		for (final File file : files) {
			if (file.getName().equals(
					IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME)) {
				uploadDirDatasetIniFiles.add(file);
			}
		}
		if (uploadDirDatasetIniFiles.size() > 1) {
			System.err.println("Upload directory contains more than one "
					+ IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME
					+ " file: "
					+ uploadDirDatasetIniFiles.size());
			return 1;
		}
		final File uploadDirDatasetIniFile = uploadDirDatasetIniFiles.isEmpty()
				? null
				: getOnlyElement(uploadDirDatasetIniFiles);

		// Now decide what to do.
		File datasetIniFile = null;
		if (clDatasetIniFile != null) {
			if (uploadDirDatasetIniFile != null) {
				files.remove(uploadDirDatasetIniFile);
			}
			files.add(clDatasetIniFile);
			datasetIniFile = clDatasetIniFile;
		} else {
			if (uploadDirDatasetIniFile != null) {
				datasetIniFile = uploadDirDatasetIniFile;
			} else {
				final String userHomeDir = StandardSystemProperty.USER_HOME
						.value();
				final File homeDirDatasetIniFile = new File(
						userHomeDir,
						IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME);
				if (homeDirDatasetIniFile.exists()) {
					files.add(homeDirDatasetIniFile);
					datasetIniFile = homeDirDatasetIniFile;
				}
			}
		}
		if (datasetIniFile != null) {
			System.out
					.println("Using dataset configuration file "
							+ datasetIniFile.getAbsolutePath());
		}
		return 0;

	}

	private int registerDirectory(S3Uploader s3Uploader) {
		int exitStatus = 0;
		final int maxTries = 20;
		for (int i = 0; i < maxTries; i++) {
			try {
				final Optional<String> queueName = dirUploadCommand
						.getQueueName();
				final ControlFileRegistration registration = wsClient
						.createControlFileRegistration(
								s3Uploader.getAuthzBucket(),
								s3Uploader.getAuthzPrefix()
										+ "/.",
								queueName.orNull());
				System.out
						.println(
						"Registered directory ["
								+ registration.getFileKey()
								+ "] in bucket ["
								+ registration.getBucket()
								+ "]"
								+ (queueName.isPresent()
										? (" to queue [" + queueName.get() + "]")
										: ""));
				break;
			} catch (Exception e) {
				int attemptNo = i + 1;
				System.out.println(
						"Failed to register directory on attempt "
								+ attemptNo
								+ " of "
								+ maxTries
								+ ": "
								+ e.getMessage());
				if (attemptNo == maxTries) {
					System.err.println("Giving up");
					exitStatus = 1;
				} else {
					System.out
							.println("Waiting 5 seconds to try registering directory again");
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						System.err.println("Reasserting interrupt");
						Thread.currentThread().interrupt();
					}
					System.out.println("Retrying");
				}
			}
		}
		return exitStatus;
	}

}
