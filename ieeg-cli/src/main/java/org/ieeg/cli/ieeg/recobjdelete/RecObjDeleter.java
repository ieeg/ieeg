/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.recobjdelete;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegRecObjDeleteCommand;
import org.ieeg.cli.ieeg.IeegUtil;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

/**
 * Deletes a recording object
 * 
 * @author John Frommeyer
 *
 */
public class RecObjDeleter {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegRecObjDeleteCommand recObjDeleteCommand;
	private final WebServiceClient wsClient;
	private final String datasetName;

	public RecObjDeleter(
			JCommander jc,
			IeegMainParams mainParams,
			IeegRecObjDeleteCommand recObjDeleteCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.recObjDeleteCommand = checkNotNull(recObjDeleteCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = this.recObjDeleteCommand.getDatasetName();
	}

	public int delete() {
		int exitStatus = 0;
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}
		final List<String> objectNames = recObjDeleteCommand.getObjects();
		if (objectNames.isEmpty()) {
			System.out.println("No objects specified.");
			return 0;
		}

		Set<RecordingObject> existingObjects = null;
		try {
			RecordingObjects objects = wsClient.listObjects(datasetId);
			existingObjects = objects.getRecordingObjects();
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}
		if (existingObjects.isEmpty()) {
			System.err.println(
					"Dataset ["
							+ datasetName
							+ "] does not contain any objects");
			return 1;
		}
		Set<RecordingObject> toDelete = newLinkedHashSet();
		for (String objectName : objectNames) {
			boolean found = false;
			for (RecordingObject existingObject : existingObjects) {
				if (objectName.equals(existingObject.getName())) {
					toDelete.add(existingObject);
					found = true;
				}
			}
			if (!found) {
				System.err.println(
						"Dataset ["
								+ datasetName
								+ "] does not contain an object ["
								+ objectName
								+ "]");
			}
		}
		if (toDelete.isEmpty()) {
			System.err.println(
					"None of the named objects belong to dataset ["
							+ datasetName
							+ "]. Nothing to delete");
			return 1;
		}
		boolean okayToDelete = true;
		if (!recObjDeleteCommand.isForce()) {
			System.out.println("Will delete:");
			IeegUtil.listRecordingObjects(
					toDelete,
					System.out);
			System.out.print("Okay to delete? ");

			@SuppressWarnings("resource")
			Scanner inputScanner = new Scanner(System.in);
			String response = inputScanner.next();
			if (!response.startsWith("y") && !response.startsWith("Y")) {
				okayToDelete = false;
			}
		}
		if (okayToDelete) {
			for (RecordingObject recordingObject : toDelete) {
				try {
					wsClient.deleteObject(
							recordingObject.getETag(),
							recordingObject.getId());
					System.out
							.println(
							"Deleted recording object: "
									+ IeegUtil
											.recordingObjectDisplayString(recordingObject));
				} catch (IeegWsRemoteException e) {
					System.err.println(
							"Could not delete recording object: "
									+ recordingObject.getName()
									+ "]: "
									+ e.getMessage());
					exitStatus = 1;
				}
			}
		}
		return exitStatus;
	}

}
