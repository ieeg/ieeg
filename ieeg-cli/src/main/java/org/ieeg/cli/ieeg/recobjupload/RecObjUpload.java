/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.recobjupload;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IHasTransferState;
import org.ieeg.cli.ieeg.TransferState;

import com.google.common.base.Optional;

import edu.upenn.cis.db.mefview.shared.RecordingObject;

public final class RecObjUpload implements IHasTransferState {

	private final String name;
	private TransferState state;
	private Optional<RecordingObject> recordingObjectOpt = Optional.absent();
	private Optional<String> ieegErrorCode = Optional.absent();

	public RecObjUpload(
			String name) {
		this.name = checkNotNull(name);
		this.state = TransferState.WAITING;
	}

	public synchronized Optional<String> getIeegErrorCode() {
		return ieegErrorCode;
	}

	public String getName() {
		return name;
	}

	public synchronized Optional<RecordingObject> getRecordingObject() {
		return recordingObjectOpt;
	}

	public synchronized TransferState getState() {
		return state;
	}

	public synchronized void setIeegErrorCode(String ieegErrorCode) {
		this.ieegErrorCode = Optional.of(ieegErrorCode);
	}

	public synchronized void setRecordingObject(
			@Nullable RecordingObject recordingObject) {
		this.recordingObjectOpt = Optional.fromNullable(recordingObject);
	}

	public synchronized void setState(TransferState state) {
		this.state = state;
	}

}
