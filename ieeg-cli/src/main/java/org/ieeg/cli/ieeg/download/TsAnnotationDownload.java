/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import org.ieeg.cli.ieeg.IHasTransferState;
import org.ieeg.cli.ieeg.TransferState;

public final class TsAnnotationDownload implements IHasTransferState {

	private long writtenChars = 0;
	private TransferState state = TransferState.WAITING;

	/**
	 * @return the state
	 */
	public synchronized TransferState getState() {
		return state;
	}

	/**
	 * @return the writtenChars
	 */
	public synchronized long getWrittenChars() {
		return writtenChars;
	}

	/**
	 * @param state the state to set
	 */
	public synchronized void setState(TransferState state) {
		this.state = state;
	}

	/**
	 * @param writtenChars the writtenChars to set
	 */
	public synchronized void setWrittenChars(long writtenBytes) {
		this.writtenChars = writtenBytes;
	}

}
