/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import java.util.List;

import org.ieeg.cli.ieeg.objupload.AwsCertCheckingOption;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;

/**
 * Parameters for the ieeg list-staging
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "Delete files in the staging area")
public class IeegStagingDeleteCommand {

	final public static String COMMAND_NAME = "delete-staging";

	@Parameter(
			names = {
					"-n",
					"--collectionName" },
			description = "<name of collection>")
	private String collectionName;

	public String getCollectionName() {
		return collectionName;
	}

	@Parameter(
			names = {
					"-f" },
			description = "delete objects without prompting for confirmation")
	private boolean force = false;

	public boolean isForce() {
		return force;
	}

	@ParametersDelegate
	public AwsCertCheckingOption awsCertChecking = new AwsCertCheckingOption();
}
