/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.ieeg.cli.ieeg.IeegDownloadCommand;
import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.TransferState;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.ieeg.cli.ieeg.download.FileDownloadTask.IContentsProvider;
import org.ieeg.cli.ieeg.size.SizeToStringConverter;
import org.ieeg.cli.ieeg.size.Unit;

import com.beust.jcommander.JCommander;
import com.google.common.base.Optional;
import com.google.common.io.Closer;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.upenn.cis.db.mefview.services.DatasetFileInfos;
import edu.upenn.cis.db.mefview.services.FileInfo;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.services.TimeSeriesIdAndDCheck;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class Downloader {
	private final String lockFileName = "ieeg.lock";
	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegDownloadCommand downloadCommand;
	private final WebServiceClient wsClient;
	private final IContentsProvider<String> contentsProvider;
	private final File baseDir;
	private final String datasetName;
	private final File datasetDir;
	private final ListeningExecutorService threadPool;
	private JAXBContext jaxbContext;
	private final SizeToStringConverter sizeToString = new SizeToStringConverter();

	private final class DownloadAnnotationFileTask implements Callable<Void> {
		private final TsAnnotationDownload download;
		private final String datasetId;
		private final File file;

		DownloadAnnotationFileTask(
				TsAnnotationDownload download,
				String datasetId,
				File file) {
			this.download = checkNotNull(download);
			this.datasetId = checkNotNull(datasetId);
			this.file = checkNotNull(file);
		}

		@Override
		public Void call() throws Exception {
			download.setState(TransferState.RUNNING);
			System.out.println("Starting download of annotations.xml");
			long charsWritten = 0;
			Closer closer = Closer.create();
			Response response = null;
			boolean innerTryCompleted = false;
			boolean innerFinallyCompleted = false;
			try {

				try {
					FileWriter writer = closer
							.register(new FileWriter(file));
					response = wsClient.getTsAnnotations(datasetId);
					InputStream responseInputStream = response
							.readEntity(InputStream.class);
					BufferedReader reader = closer
							.register(
							new BufferedReader(
									new InputStreamReader(
											responseInputStream)));
					int read = 0;
					char[] buffer = new char[4096];
					while ((read = reader.read(buffer)) != -1) {
						writer.write(buffer, 0, read);
						charsWritten += read;
						download.setWrittenChars(charsWritten);
					}
					innerTryCompleted = true;
				} catch (Throwable t) {
					throw closer.rethrow(t);
				} finally {
					closer.close();
					if (response != null) {
						response.close();
					}
					innerFinallyCompleted = true;
				}
			} finally {
				if (!file.exists()) {
					download.setState(TransferState.FAILED);
				} else {
					long sizeBytes = file.length();
					if (sizeBytes > 0
							&& innerTryCompleted
							&& innerFinallyCompleted) {
						download.setState(TransferState.FINISHED);
						System.out
								.println("Download of annotations.xml finished");
					} else {
						download.setState(TransferState.FAILED);
						file.delete();
						if (innerTryCompleted
								&& innerFinallyCompleted) {
							System.err
									.println("Download of annotations.xml failed for an unknown reason");
						}
					}
				}
			}
			return null;
		}
	}

	/**
	 * 
	 * @param jc
	 * @param mainParams
	 * @param downloadCommand
	 * @param wsClient
	 * @param baseDir
	 * @param threads
	 */
	public Downloader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegDownloadCommand downloadCommand,
			WebServiceClient wsClient,
			File baseDir,
			int threads) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.downloadCommand = checkNotNull(downloadCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = getDatasetName();
		this.baseDir = checkNotNull(baseDir);
		this.datasetDir = new File(
				baseDir,
				datasetName);
		threadPool = MoreExecutors
				.listeningDecorator(Executors.newFixedThreadPool(threads));
		try {
			jaxbContext = JAXBContext.newInstance(
					TimeSeriesIdAndDCheck.class,
					DatasetFileInfos.class);
		} catch (JAXBException e) {
			System.err.println("Cannot create JAXBContext for metadata file"
					+ ": " + e.getMessage());
			System.exit(1);
		}
		contentsProvider = new IContentsProvider<String>() {

			@Override
			public Response getContents(String id, String eTag, String range) {

				return Downloader.this.wsClient.getTimeSeriesContents(
						id,
						eTag,
						range);
			}

		};
	}

	public int download() {
		File metadataFile = new File(
				datasetDir,
				"ieeg-metadata.xml");

		DatasetDownload datasetDownload = null;
		if (datasetDir.exists() && datasetDir.isDirectory()) {
			datasetDownload = handleExistingDataset(metadataFile);
		} else {
			datasetDownload = handleNewDataset(metadataFile);
		}
		return datasetDownload.waitForCompletion();
	}

	private DatasetDownload handleNewDataset(File metadataFile) {
		System.out
				.println("Directory [" + datasetDir + "] does not exist.");
		String datasetId = null;
		DatasetFileInfos dsFileInfos = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
			dsFileInfos = wsClient.getDatasetFileInfos(datasetId);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			System.exit(1);
		}

		if (dsFileInfos.getFileInfos().isEmpty()) {
			System.out.println(
					"Dataset ["
							+ datasetName
							+ "] does not contain any files.");
			System.exit(0);
		}
		datasetDir.mkdir();
		File lockFile = new File(datasetDir, lockFileName);
		try {
			lockFile.createNewFile();
		} catch (IOException e) {
			System.err.println("Cannot create lock file in ["
					+ datasetDir
					+ "]: " + e.getMessage());
			System.exit(1);
		}
		lockFile.deleteOnExit();
		System.out
				.println("Created directory [" + datasetDir + "].");

		try {
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(
					Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			marshaller.marshal(dsFileInfos, metadataFile);
		} catch (JAXBException e) {
			System.err.println("Cannot write metadata file: [" + dsFileInfos
					+ "]: " + e.getMessage());
			System.exit(1);
		}

		return downloadNewDataset(
				datasetId,
				dsFileInfos);

	}

	private DatasetDownload downloadNewDataset(String datasetId,
			DatasetFileInfos dsFileInfos) {
		Set<FileInfo> fileInfosToDownload = newLinkedHashSet();
		if (downloadCommand.getChannelLabel() == null) {
			fileInfosToDownload.addAll(dsFileInfos.getFileInfos());
		} else {
			String channelLabel = downloadCommand.getChannelLabel();
			Optional<FileInfo> fileInfoOpt = findFileInfoByChannelLabel(
					dsFileInfos,
					channelLabel);
			if (fileInfoOpt.isPresent()) {
				fileInfosToDownload.add(fileInfoOpt.get());
			} else {
				System.err.println("Channel ["
						+ channelLabel
						+ "] not found in dataset ["
						+ datasetName
						+ "]");
				System.exit(1);
			}
		}

		long bytesToDownload = 0;
		Set<FileDownload> downloads = newLinkedHashSet();
		for (FileInfo fileInfo : fileInfosToDownload) {
			bytesToDownload += fileInfo.getSizeBytes();
			final String filename = fileInfo.getFileName();
			File file = new File(
					datasetDir,
					filename);
			FileDownload download = new FileDownload(
					filename,
					fileInfo.getSizeBytes());
			downloads.add(download);
			ListenableFuture<Void> downloadFuture = threadPool
					.submit(new FileDownloadTask<String>(
							contentsProvider,
							download,
							fileInfo.getId(),
							fileInfo.getETag(),
							fileInfo.getFileName(),
							fileInfo.getSizeBytes().longValue(),
							file,
							null));
			Futures.addCallback(
					downloadFuture,
					new DownloadErrorReporter(
							filename,
							download));
		}

		Unit sizeUnit = sizeToString.getUnitForSizeBytes(bytesToDownload);
		String bytesToDownloadString = sizeToString.asString(
				bytesToDownload,
				sizeUnit);
		String message = downloadCommand.getChannelLabel() == null
				? "Starting download of "
						+ fileInfosToDownload.size()
						+ " files totaling "
						+ bytesToDownloadString
						+ sizeUnit.toString()
						+ " plus annotations"
				: "Starting download. Size: "
						+ bytesToDownloadString
						+ sizeUnit.toString();

		TsAnnotationDownload tsAnnotationDownload = null;
		if (downloadCommand.getChannelLabel() == null) {
			File annotationsFile = new File(
					datasetDir,
					"annotations.xml");
			tsAnnotationDownload = new TsAnnotationDownload();
			ListenableFuture<Void> annotationsFuture = threadPool
					.submit(new DownloadAnnotationFileTask(
							tsAnnotationDownload,
							datasetId,
							annotationsFile));
			Futures.addCallback(
					annotationsFuture,
					new DownloadErrorReporter(
							"annotations.xml",
							tsAnnotationDownload));
		}
		System.out.println(message);
		return new DatasetDownload(
				downloads,
				bytesToDownload,
				bytesToDownload,
				tsAnnotationDownload);
	}

	private DatasetDownload handleExistingDataset(File metadataFile) {
		File lockFile = new File(
				datasetDir,
				lockFileName);
		boolean createdLockFile = false;
		try {
			createdLockFile = lockFile.createNewFile();
		} catch (IOException e) {
			System.err.println("Could not obtain lock on ["
					+ datasetDir
					+ "]: " + e.getMessage());
			System.exit(1);
		}
		if (createdLockFile) {
			lockFile.deleteOnExit();
		} else {
			System.err
					.println(
					"A lock file already exists for this dataset: ["
							+ lockFile
							+ "]. Concurrent downloads of the same dataset are not allowed."
							+ " If you are certain no other process is downloading this dataset, delete the lock file and re-run the command.");
			System.exit(1);
		}
		System.out
				.println(
				"Directory ["
						+ datasetDir
						+ "] exists. Checking for changes or incomplete files.");
		DatasetFileInfos localDsFileInfos = null;
		try {
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			localDsFileInfos = (DatasetFileInfos) unmarshaller
					.unmarshal(metadataFile);
		} catch (JAXBException e) {
			System.err.println("Could not read metadata file: ["
					+ metadataFile.getAbsolutePath()
					+ "]: " + e.getMessage());
			System.exit(1);
		}

		String datasetId = localDsFileInfos.getDatasetId();
		DatasetFileInfos serverDsFileInfos = null;
		try {
			serverDsFileInfos = wsClient.getDatasetFileInfos(datasetId);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]. Error: "
							+ e.getMessage());
			System.exit(1);
		}
		DatasetDiff datasetDiff = new DatasetDiff(
				localDsFileInfos,
				serverDsFileInfos);
		List<String> differences = datasetDiff.getDifferences();
		if (!differences.isEmpty()) {
			System.out.println("The dataset has changed on the server.");
			for (String diff : differences) {
				System.out.println(diff);
			}
			System.out
					.println("If you would like to download the new version rename the existing directory and re-run the command.");
			Set<FileDownload> empty = Collections.emptySet();
			return new DatasetDownload(
					empty,
					0,
					-1,
					null);

		}

		return downloadExistingDataset(
				datasetId,
				localDsFileInfos);

	}

	private DatasetDownload downloadExistingDataset(
			String datasetId,
			DatasetFileInfos localDsFileInfos) {
		Set<FileInfo> fileInfosToDownload = newLinkedHashSet();
		if (downloadCommand.getChannelLabel() == null) {
			fileInfosToDownload.addAll(localDsFileInfos.getFileInfos());
		} else {
			String channelLabel = downloadCommand.getChannelLabel();
			Optional<FileInfo> fileInfoOpt = findFileInfoByChannelLabel(
					localDsFileInfos,
					channelLabel);
			if (fileInfoOpt.isPresent()) {
				fileInfosToDownload.add(fileInfoOpt.get());
			} else {
				System.err.println("Channel ["
						+ channelLabel
						+ "] not found in dataset ["
						+ datasetName
						+ "]");
				System.exit(1);
			}
		}
		Set<FileDownload> downloads = newLinkedHashSet();
		long totalDatasetBytes = 0;
		long toDownloadBytes = 0;
		for (FileInfo fileInfo : fileInfosToDownload) {
			String fileName = fileInfo.getFileName();
			long serverSizeBytes = fileInfo
					.getSizeBytes().longValue();
			totalDatasetBytes += serverSizeBytes;
			File file = new File(
					datasetDir,
					fileName);
			FileDownload download = null;
			String range = null;
			if (file.exists()) {
				long localSizeBytes = file.length();
				if (localSizeBytes < serverSizeBytes) {
					toDownloadBytes += (serverSizeBytes - localSizeBytes);
					download = new FileDownload(
							fileName,
							fileInfo.getSizeBytes());
					range = "bytes=" + localSizeBytes + "-";
				}
			} else {
				toDownloadBytes += serverSizeBytes;
				download = new FileDownload(
						fileName,
						fileInfo.getSizeBytes());

			}
			if (download != null) {
				System.out
						.println("Resuming incomplete download of "
								+ fileName);
				downloads.add(download);
				ListenableFuture<Void> downloadFuture = threadPool
						.submit(
						new FileDownloadTask<String>(
								contentsProvider,
								download,
								fileInfo.getId(),
								fileInfo.getETag(),
								fileInfo.getFileName(),
								fileInfo.getSizeBytes().longValue(),
								file,
								range));
				Futures.addCallback(
						downloadFuture,
						new DownloadErrorReporter(
								fileName,
								download));
			}
		}

		TsAnnotationDownload tsAnnotationDownload = null;
		if (downloadCommand.getChannelLabel() == null) {
			File annotationsFile = new File(
					datasetDir,
					"annotations.xml");
			if (!annotationsFile.exists()) {
				tsAnnotationDownload = new TsAnnotationDownload();
				ListenableFuture<Void> annotationsFuture = threadPool
						.submit(new DownloadAnnotationFileTask(
								tsAnnotationDownload,
								datasetId,
								annotationsFile));
				Futures.addCallback(
						annotationsFuture,
						new DownloadErrorReporter(
								"annotations.xml",
								tsAnnotationDownload));

			}
		}
		return new DatasetDownload(
				downloads,
				toDownloadBytes,
				totalDatasetBytes,
				tsAnnotationDownload);
	}

	private String getDatasetName() {
		final List<String> datasetNames = downloadCommand.getDatasetNames();
		if (datasetNames.isEmpty()) {
			System.err.println("No dataset name specified.");
			jc.usage();
			System.exit(1);
		}
		final String datasetName = datasetNames.get(0);
		if (datasetNames.size() > 1) {
			System.out.println(
					"WARNING: Ignoring all but first dataset: ["
							+ datasetName
							+ "]");
		}

		if (mainParams.isVerbose()) {
			System.out.println(
					"Will attempt to download dataset: ["
							+ datasetName
							+ "]");
		}
		return datasetName;
	}

	private Optional<FileInfo> findFileInfoByChannelLabel(
			DatasetFileInfos dsFileInfos,
			String channelLabel) {
		checkNotNull(dsFileInfos);
		checkNotNull(channelLabel);
		Optional<FileInfo> notFound = Optional.absent();
		for (FileInfo fileInfo : dsFileInfos.getFileInfos()) {
			if (fileInfo.getFileName().equals(channelLabel + ".mef")) {
				return Optional.of(fileInfo);
			}
		}
		return notFound;

	}

	public void shutdown() {
		threadPool.shutdown();
	}
}
