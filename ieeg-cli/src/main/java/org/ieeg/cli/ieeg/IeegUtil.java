/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import java.io.PrintStream;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.RecordingObject;

/**
 * 
 * @author John Frommeyer
 *
 */
public class IeegUtil {
	private IeegUtil() {}

	public static void listRecordingObjects(
			Set<RecordingObject> recordingObjects,
			PrintStream stream) {
		for (RecordingObject recObj : recordingObjects) {
			listRecordingObject(
					recObj,
					stream);
		}
	}

	public static void listRecordingObject(
			RecordingObject recordingObject,
			PrintStream stream) {
		stream.println(recordingObjectDisplayString(recordingObject));
	}

	public static String recordingObjectDisplayString(
			RecordingObject recordingObject) {
		return "name="
				+ recordingObject.getName()
				+ ", description="
				+ recordingObject.getDescription()
				+ ", creator="
				+ recordingObject.getCreator()
				+ ", created="
				+ recordingObject.getCreateTime()
				+ ", sizeBytes="
				+ recordingObject.getSizeBytes()
				+ ", md5Hash="
				+ recordingObject.getMd5Hash();
	}
	
}
