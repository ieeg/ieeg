/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.montagelist;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegMontageListCommand;
import org.ieeg.cli.ieeg.IeegMontageListCommand.OutputType;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.EEGMontages;

/**
 * @author John Frommeyer
 *
 */
public class MontageLister {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegMontageListCommand montageListCommand;
	private final WebServiceClient wsClient;
	private final String datasetName;
	private final Optional<ObjectMapper> objectMapper;

	public MontageLister(
			JCommander jc,
			IeegMainParams mainParams,
			IeegMontageListCommand montageListCommand,
			WebServiceClient wsClient,
			int threads) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.montageListCommand = checkNotNull(montageListCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = montageListCommand.getDatasetName();
		if (montageListCommand.getOutput().equals(
				OutputType.JSON)) {
			final ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
			mapper.setSerializationInclusion(Include.NON_NULL);
			this.objectMapper = Optional.of(mapper);
		} else {
			this.objectMapper = Optional
					.absent();
		}
	}

	public int list() {
		int exitStatus = 0;
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}

		final Map<String, EEGMontage> existingNameToMontage = newHashMap();
		try {
			final EEGMontages existingMontagesContainer = wsClient
					.getMontages(datasetId);
			for (EEGMontage existingMontage : existingMontagesContainer
					.getMontages()) {
				existingNameToMontage.put(existingMontage.getName(),
						existingMontage);
			}
		} catch (IeegWsRemoteException e) {
			System.err
					.println(
					"Could not retrieve existing montages for "
							+ datasetName
							+ ": "
							+ e.getMessage());
			return 1;

		}

		final boolean errorOnMissingMontage = !montageListCommand.getMontages()
				.isEmpty();

		if (existingNameToMontage.isEmpty()
				&& !errorOnMissingMontage) {
			System.out.println(
					"No montages found for "
							+ datasetName);
			return 0;
		}

		final Collection<String> requestedMontageNames = montageListCommand
				.getMontages().isEmpty()
				? existingNameToMontage.keySet()
				: montageListCommand.getMontages();

		final List<EEGMontage> requestedMontages = new ArrayList<>();
		for (String requestedMontageName : requestedMontageNames) {
			EEGMontage existingMontage = existingNameToMontage
					.get(requestedMontageName);
			if (existingMontage == null && errorOnMissingMontage) {
				System.err.println("Dataset " + datasetName
						+ " does not contain a montage called "
						+ requestedMontageName);
				exitStatus = 1;
			} else if (existingMontage != null) {
				requestedMontages.add(existingMontage);
			}

		}
		exitStatus = list(System.out, requestedMontages,
				montageListCommand.getOutput());
		return exitStatus;
	}

	private int list(
			PrintStream stream,
			List<EEGMontage> montages,
			OutputType output) {

		switch (output) {
			case SUMMARY:
				return listAsSummary(stream, montages);
			case JSON:
				return listAsJson(stream, montages);
			case INI:
				montageListCommand.checkSeparators();
				return listAsIni(stream, montages);
			default:
				throw new IllegalArgumentException(output.toString());
		}

	}

	private String toSummary(EEGMontage montage) {
		return "name="
				+ montage.getName()
				+ ", serverId="
				+ montage.getServerId()
				+ ", isPublic="
				+ montage.getIsPublic()
				+ ", ownedByUser="
				+ montage.getOwnedByUser()
				+ ", number of pairs="
				+ montage.getPairs().size();
	}

	private String toIni(EEGMontage montage) {
		final StringBuilder sb = new StringBuilder();
		final String name = montage.getName();
		sb.append("[Montage." + name + "]\n");
		sb.append('\n');
		sb.append("serverId=" + montage.getServerId() + "\n");
		sb.append("isPublic=" + montage.getIsPublic() + "\n");
		final List<EEGMontagePair> pairs = montage.getPairs();
		if (!pairs.isEmpty()) {
			final String channelSeparator = montageListCommand
					.getChannelSeparator();
			if (!channelSeparator
					.equals(IeegMontageListCommand.DEFAULT_CHANNEL_SEPARATOR)) {
				sb.append("channelSeparator=" + channelSeparator + "\n");
			}
			final String pairSeparator = montageListCommand.getPairSeparator();
			if (!pairSeparator
					.equals(IeegMontageListCommand.DEFAULT_PAIR_SEPARATOR)) {
				sb.append("pairSeparator=" + pairSeparator + "\n");
			}
			final Joiner channelJoiner = Joiner.on(channelSeparator)
					.skipNulls();
			final Joiner pairJoiner = Joiner.on(pairSeparator).skipNulls();
			sb.append("pairs=");
			final List<String> pairStrings = new ArrayList<>();
			for (final EEGMontagePair pair : pairs) {
				final String el1 = pair.getEl1();
				montageListCommand.checkChannelSeparator(name, el1);
				montageListCommand.checkPairSeparator(name, el1);
				final String el2 = pair.getEl2();
				if (el2 != null) {
					montageListCommand.checkChannelSeparator(name, el2);
					montageListCommand.checkPairSeparator(name, el2);
				}
				pairStrings.add(channelJoiner.join(el1, el2));
			}
			sb.append(pairJoiner.join(pairStrings) + "\n");
		}

		return sb.toString();
	}

	private int listAsSummary(PrintStream stream, List<EEGMontage> montages) {
		for (final EEGMontage montage : montages) {
			stream.println(toSummary(montage));
		}
		return 0;
	}

	private int listAsJson(PrintStream stream, List<EEGMontage> montages) {
		int exitStatus = 0;
		final ObjectMapper mapper = objectMapper.get();

		// Don't want array if only one montage. But do want arrays if only one
		// pair in montage. So doing it manually instead of enabling the
		// SerializationFeature.
		final Object value = montages.size() == 1
				? montages.iterator().next()
				: montages;
		try {
			final String string = mapper.writeValueAsString(value);
			stream.println(string);
		} catch (JsonProcessingException e) {
			System.err.println("failed to convert montage list to JSON: "
					+ montages
					+ ": "
					+ e.getMessage());
			if (mainParams.isVerbose()) {
				e.printStackTrace();
			}
			exitStatus = 1;
		}
		return exitStatus;
	}

	private int listAsIni(PrintStream stream, List<EEGMontage> montages) {
		for (final EEGMontage montage : montages) {
			stream.println(toIni(montage));
		}
		return 0;
	}

}
