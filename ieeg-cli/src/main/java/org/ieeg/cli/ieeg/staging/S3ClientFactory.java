/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.staging;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.WebServiceClient;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.services.UploadCredentials;

/**
 * @author John Frommeyer
 *
 */
public final class S3ClientFactory {

	public static final class S3ClientCredentials {
		private final AmazonS3Client s3Client;
		private final String authzBucket;
		private final String authzPrefix;

		public S3ClientCredentials(AmazonS3Client s3Client,
				String authzBucket,
				String authzPrefix) {
			this.s3Client = checkNotNull(s3Client);
			this.authzBucket = checkNotNull(authzBucket);
			this.authzPrefix = checkNotNull(authzPrefix);
		}

		public AmazonS3Client getS3Client() {
			return s3Client;
		}

		public String getAuthzBucket() {
			return authzBucket;
		}

		public String getAuthzPrefix() {
			return authzPrefix;
		}

	}

	private final WebServiceClient wsClient;

	public S3ClientFactory(WebServiceClient wsClient) {
		this.wsClient = checkNotNull(wsClient);
	}

	public S3ClientCredentials newClient(boolean verbose,
			boolean disableCertCheck,
			@Nullable String collectionName) throws IeegWsRemoteException {

		final UploadCredentials credentials = wsClient.getUploadCredentials(
				null,
				collectionName);
		if (verbose) {
			System.out.println(
					"Access Key ID: "
							+ credentials.getId());

			boolean returnedSecretKey = credentials.getSecretKey() != null;
			System.out
					.println(returnedSecretKey
							? "A secret key was returned from the server"
							: "No secret key was returned from the server");

			boolean returnedSessionKey = credentials.getSessionToken() != null;
			System.out
					.println(returnedSessionKey
							? "A session key was returned from the server"
							: "No session key was returned from the server");

			System.out.println(
					"Bucket: "
							+ credentials.getAuthorizedBucket());
			System.out.println(
					"Prefix: "
							+ credentials.getAuthorizedPrefix());
		}
		final String secretKey = credentials.getSecretKey();

		final String sessionToken = credentials.getSessionToken();
		final AWSCredentials awsCredentials = new BasicSessionCredentials(
				credentials.getId(),
				secretKey,
				sessionToken);

		final String authzBucket = credentials.getAuthorizedBucket();
		final String authzPrefix = credentials.getAuthorizedPrefix().endsWith(
				"/")
				? credentials.getAuthorizedPrefix()
				: credentials.getAuthorizedPrefix() + "/";

		if (disableCertCheck) {
			System.out
					.println("Allowing self-signed certificates for AWS requests");
			System.setProperty(
					SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY,
					"true");
		}
		final ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setMaxErrorRetry(20);
		final AmazonS3Client s3Client = new AmazonS3Client(awsCredentials,
				clientConfiguration);
		// Ask for path-style access (s3.amazonaws.com/bucket-name/...) instead
		// of virtual-host-style access
		// (bucket-name.s3.amazonaws.com/...) because VH style leads to wildcard
		// SSL certs which some users' systems
		// replace with self-signed certs which our HTTP client rejects.
		// But some users' systems still replace non-wildcard certs too!
		// So we use the System Property above too.
		s3Client.setS3ClientOptions(new S3ClientOptions()
				.withPathStyleAccess(true));

		return new S3ClientCredentials(s3Client,
				authzBucket,
				authzPrefix);
	}
}
