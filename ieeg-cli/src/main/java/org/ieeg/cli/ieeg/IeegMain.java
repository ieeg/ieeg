/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.ieeg.cli.ieeg.delete.Deleter;
import org.ieeg.cli.ieeg.dirupload.DirUploader;
import org.ieeg.cli.ieeg.download.Downloader;
import org.ieeg.cli.ieeg.montagelist.MontageLister;
import org.ieeg.cli.ieeg.montageupload.MontageUploader;
import org.ieeg.cli.ieeg.objupload.ObjUploader;
import org.ieeg.cli.ieeg.recobjdelete.RecObjDeleter;
import org.ieeg.cli.ieeg.recobjdownload.RecObjDownloader;
import org.ieeg.cli.ieeg.recobjlist.RecObjLister;
import org.ieeg.cli.ieeg.recobjupload.RecObjUploader;
import org.ieeg.cli.ieeg.register.Registrar;
import org.ieeg.cli.ieeg.size.SizeReporter;
import org.ieeg.cli.ieeg.staging.delete.StagingDeleter;
import org.ieeg.cli.ieeg.staging.list.StagingLister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.beust.jcommander.ParameterException;
import com.google.common.base.StandardSystemProperty;
import com.google.common.base.Strings;
import com.google.common.io.CharStreams;
import com.google.common.io.Files;

import edu.upenn.cis.db.mefview.services.Config;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.services.VersionString;

/**
 * Main class for the ieeg command.
 * 
 * @author John Frommeyer
 * 
 */
public class IeegMain {
	final private static Logger logger = LoggerFactory
			.getLogger(IeegMain.class);
	final public static String IEEG_PROP_FILENAME = "ieeg.properties";
	final public static String BASE_DIR = "base_dir";
	final public static String URL = "url";
	final public static String USERNAME = "username";
	final public static String PASSWORD = "password";
	final public static String THREADS = "threads";
	final public static String TIMEOUT_SECS = "timeout_secs";
	final public static String AWS_SECRET_KEY = "aws_secret_key";

	public static void main(String[] args) {
		final String m = "main(...)";
		final IeegMainParams mainParams = new IeegMainParams();
		final IeegDownloadCommand downloadCommand = new IeegDownloadCommand();
		final IeegDeleteCommand deleteCommand = new IeegDeleteCommand();
		final IeegSizeCommand sizeCommand = new IeegSizeCommand();
		final IeegRecObjUploadCommand recObjUploadCommand = new IeegRecObjUploadCommand();
		final IeegRecObjListCommand recObjListCommand = new IeegRecObjListCommand();
		final IeegRecObjDeleteCommand recObjDeleteCommand = new IeegRecObjDeleteCommand();
		final IeegRecObjDownloadCommand recObjDownloadCommand = new IeegRecObjDownloadCommand();
		final IeegObjUploadCommand objUploadCommand = new IeegObjUploadCommand();
		final IeegDirUploadCommand dirUploadCommand = new IeegDirUploadCommand();
		final IeegRegisterCommand registerCommand = new IeegRegisterCommand();
		final IeegMontageUploadCommand montageUploadCommand = new IeegMontageUploadCommand();
		final IeegMontageListCommand montageListCommand = new IeegMontageListCommand();
		final IeegStagingListCommand stagingListCommand = new IeegStagingListCommand();
		final IeegStagingDeleteCommand stagingDeleteCommand = new IeegStagingDeleteCommand();

		final JCommander jc = new JCommander(mainParams);
		jc.addCommand(
				IeegDownloadCommand.COMMAND_NAME,
				downloadCommand);
		jc.addCommand(
				IeegDeleteCommand.COMMAND_NAME,
				deleteCommand);
		jc.addCommand(
				IeegSizeCommand.COMMAND_NAME,
				sizeCommand);
		jc.addCommand(
				IeegRecObjUploadCommand.COMMAND_NAME,
				recObjUploadCommand);
		jc.addCommand(
				IeegRecObjDownloadCommand.COMMAND_NAME,
				recObjDownloadCommand);
		jc.addCommand(
				IeegRecObjDeleteCommand.COMMAND_NAME,
				recObjDeleteCommand);
		jc.addCommand(
				IeegRecObjListCommand.COMMAND_NAME,
				recObjListCommand);
		jc.addCommand(
				IeegDirUploadCommand.COMMAND_NAME,
				dirUploadCommand);
		jc.addCommand(
				IeegObjUploadCommand.COMMAND_NAME,
				objUploadCommand);
		jc.addCommand(
				IeegRegisterCommand.COMMAND_NAME,
				registerCommand);
		jc.addCommand(IeegMontageUploadCommand.COMMAND_NAME,
				montageUploadCommand);
		jc.addCommand(IeegMontageListCommand.COMMAND_NAME,
				montageListCommand);
		jc.addCommand(IeegStagingListCommand.COMMAND_NAME,
				stagingListCommand);
		jc.addCommand(IeegStagingDeleteCommand.COMMAND_NAME,
				stagingDeleteCommand);
		jc.setProgramName("ieeg");
		try {
			jc.parse(args);
		} catch (MissingCommandException e) {
			System.err.println(e.getMessage());
			jc.usage();
			System.exit(1);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			jc.usage();
			System.exit(1);
		}
		if (mainParams.isHelp()) {
			jc.usage();
			System.exit(0);
		}

		final String command = jc.getParsedCommand();
		if (command == null) {
			System.err.println("No command specified. Exiting.");
			jc.usage();
			System.exit(1);
		}

		final String userHomeDir = StandardSystemProperty.USER_HOME.value();

		final File propFile = new File(
				userHomeDir,
				IEEG_PROP_FILENAME);
		if (mainParams.isVerbose()) {
			System.out.println(
					"Looking for properties file: ["
							+ propFile.getAbsolutePath()
							+ "]");
		}
		Reader propReader = null;
		try {
			propReader = new FileReader(propFile);
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find required properties file: ["
							+ propFile.getAbsolutePath()
							+ "]");
			System.exit(1);
		}
		if (mainParams.isVerbose()) {
			System.out.println(
					"Found properties file: ["
							+ propFile.getAbsolutePath()
							+ "]");
		}
		final Properties props = new Properties();
		try {
			props.load(propReader);
		} catch (IOException e) {
			System.err.println("Could not read properties file: ["
					+ propFile.getAbsolutePath()
					+ "]: " + e.getMessage());
			System.exit(1);
		} finally {
			if (propReader != null) {
				try {
					propReader.close();
				} catch (IOException e) {
					logger.warn(m + ": WARNING: Error closing "
							+ propFile.getAbsolutePath()
							+ " continuing with run.",
							e);
				}
			}
		}

		final int threads = getThreads(
				mainParams,
				props);
		final WebServiceClient wsClient = getWebServiceClient(
				mainParams,
				props,
				threads);
		final File baseDir = getBaseDir(
				mainParams,
				props);

		checkVersion(
				mainParams,
				wsClient);

		int exitStatus = 0;
		if (command.equals(IeegDownloadCommand.COMMAND_NAME)) {
			Downloader downloader = new Downloader(
					jc,
					mainParams,
					downloadCommand,
					wsClient,
					baseDir,
					threads);
			exitStatus = downloader.download();
			downloader.shutdown();
		} else if (command.equals(IeegDeleteCommand.COMMAND_NAME)) {
			Deleter deleter = new Deleter(
					jc,
					mainParams,
					deleteCommand,
					wsClient);
			exitStatus = deleter.delete();
		} else if (command.equals(IeegSizeCommand.COMMAND_NAME)) {
			SizeReporter sizeReporter = new SizeReporter(
					jc,
					mainParams,
					sizeCommand,
					wsClient);
			exitStatus = sizeReporter.reportSize();
		} else if (command.equals(IeegRecObjUploadCommand.COMMAND_NAME)) {
			RecObjUploader recObjUploader = new RecObjUploader(
					jc,
					mainParams,
					recObjUploadCommand,
					wsClient,
					threads);
			exitStatus = recObjUploader.upload();
			recObjUploader.shutdown();
		} else if (command.equals(IeegRecObjListCommand.COMMAND_NAME)) {
			RecObjLister recObjLister = new RecObjLister(
					jc,
					mainParams,
					recObjListCommand,
					wsClient);
			exitStatus = recObjLister.list();
		} else if (command.equals(IeegRecObjDeleteCommand.COMMAND_NAME)) {
			RecObjDeleter recObjDeleter = new RecObjDeleter(
					jc,
					mainParams,
					recObjDeleteCommand,
					wsClient);
			exitStatus = recObjDeleter.delete();
		} else if (command.equals(IeegRecObjDownloadCommand.COMMAND_NAME)) {
			RecObjDownloader recObjDownloader = new RecObjDownloader(
					jc,
					mainParams,
					recObjDownloadCommand,
					wsClient,
					threads);
			exitStatus = recObjDownloader.download();
			recObjDownloader.shutdown();
		} else if (command.equals(IeegObjUploadCommand.COMMAND_NAME)) {
			String awsSecretKey = props.getProperty(AWS_SECRET_KEY);
			awsSecretKey = Strings.emptyToNull(awsSecretKey);
			ObjUploader objUploader = new ObjUploader(
					jc,
					mainParams,
					objUploadCommand,
					wsClient,
					threads,
					awsSecretKey);
			exitStatus = objUploader.upload();
		} else if (command.equals(IeegDirUploadCommand.COMMAND_NAME)) {
			String awsSecretKey = props.getProperty(AWS_SECRET_KEY);
			awsSecretKey = Strings.emptyToNull(awsSecretKey);
			DirUploader dirUploader = new DirUploader(
					jc,
					mainParams,
					dirUploadCommand,
					wsClient,
					threads,
					awsSecretKey);
			exitStatus = dirUploader.upload();
		} else if (command.equals(IeegRegisterCommand.COMMAND_NAME)) {
			Registrar registrar = new Registrar(
					jc,
					mainParams,
					registerCommand,
					wsClient);
			exitStatus = registrar.register();
		} else if (command.equals(IeegMontageUploadCommand.COMMAND_NAME)) {
			final MontageUploader montageUploader = new MontageUploader(
					jc,
					mainParams,
					montageUploadCommand,
					wsClient,
					threads);
			exitStatus = montageUploader.upload();
		} else if (command.equals(IeegMontageListCommand.COMMAND_NAME)) {
			final MontageLister montageLister = new MontageLister(
					jc,
					mainParams,
					montageListCommand,
					wsClient,
					threads);
			exitStatus = montageLister.list();
		} else if (command.equals(IeegStagingListCommand.COMMAND_NAME)) {
			final StagingLister stagingLister = new StagingLister(
					jc,
					mainParams,
					stagingListCommand,
					wsClient);
			exitStatus = stagingLister.list();
		} else if (command.equals(IeegStagingDeleteCommand.COMMAND_NAME)) {
			final StagingDeleter stagingDeleter= new StagingDeleter(
					jc,
					mainParams,
					stagingDeleteCommand,
					wsClient);
			exitStatus = stagingDeleter.delete();
		}
		System.exit(exitStatus);
	}

	private static void checkVersion(
			IeegMainParams mainParams,
			WebServiceClient wsClient) {
		final String m = "checkVersion(...)";
		BufferedReader versionReader = new BufferedReader(
				new InputStreamReader(
						IeegMain.class.getResourceAsStream("/ieeg.version")));
		StringWriter strWriter = new StringWriter();
		try {
			CharStreams.copy(versionReader, strWriter);
		} catch (IOException e) {
			System.err.println("Cannot read version from ieeg.version"
					+ ": " + e.getMessage());
			System.exit(1);
		} finally {
			try {
				versionReader.close();
			} catch (IOException e) {
				logger.warn(
						m
								+ ": WARNING: Could not close ieeg.version. Continuting with program.",
						e);
			}
		}
		String myVersionStr = strWriter.toString().trim();

		if (myVersionStr.equals("")) {
			System.err.println("No version found in client.");
			System.exit(1);
		}
		if (mainParams.isVerbose()) {
			System.out.println("Client version: ["
					+ myVersionStr
					+ "]");
		}
		VersionString myVersion = new VersionString(myVersionStr);
		Config config = null;
		try {
			config = wsClient.getConfig();
		} catch (IeegWsRemoteException e) {
			System.err.println("Could not check version: " + e.getMessage());
			System.exit(1);
		}
		String leastOkayVersionStr = config.getLeastOkayIeegClientVersion();
		VersionString leastOkayVersion = new VersionString(
				leastOkayVersionStr);
		if (myVersion.compareTo(leastOkayVersion) < 0) {
			System.err
					.println("You must upgrade to latest version of the ieeg program");
			System.exit(1);
		}
	}

	private static File getBaseDir(
			IeegMainParams mainParams,
			Properties props) {
		File baseDir = null;
		final String baseDirStr = props.getProperty(BASE_DIR);
		checkStrProp(
				BASE_DIR,
				baseDirStr);

		String baseDirStrSimplified = Files.simplifyPath(baseDirStr);

		if (baseDirStrSimplified.startsWith("~/")) {
			baseDir = new File(
					StandardSystemProperty.USER_HOME.value(),
					baseDirStrSimplified.substring(2));
		} else {
			baseDir = new File(baseDirStrSimplified);
		}

		if (mainParams.isVerbose()) {
			System.out.println(
					BASE_DIR
							+ " = "
							+ baseDir.getAbsolutePath());
		}
		boolean createdBaseDir = baseDir.mkdirs();
		if (createdBaseDir) {
			System.out.println(
					"created base directory: ["
							+ baseDir.getAbsolutePath()
							+ "]");
		}

		return baseDir;
	}

	private static WebServiceClient getWebServiceClient(
			IeegMainParams mainParams,
			Properties props,
			int threads) {

		String url = props.getProperty(URL);
		checkStrProp(
				URL,
				url);
		String username = props.getProperty(USERNAME);
		checkStrProp(
				username,
				USERNAME);
		String password = props.getProperty(PASSWORD);
		checkStrProp(
				PASSWORD,
				password);
		if (mainParams.isVerbose()) {
			System.out.println(
					URL
							+ " = "
							+ url);
			System.out.println(
					USERNAME
							+ " = "
							+ username);
		}

		try {
			final URL realUrl = new URL(url);
			if ("".equals(realUrl.getHost())) {
				System.err.println(
						"URL missing host: ["
								+ url
								+ "]");
				System.exit(1);
			}
		} catch (MalformedURLException e) {
			System.err.println(
					"Illegal URL: ["
							+ url
							+ "]. Error: "
							+ e.getMessage());
			System.exit(1);
		}

		int timeoutSecs = 30;
		String timeoutSecsStr = props.getProperty(
				TIMEOUT_SECS,
				Integer.toString(timeoutSecs));
		if (mainParams.isVerbose()) {
			System.out.println(
					TIMEOUT_SECS
							+ " = "
							+ timeoutSecs);
		}
		try {
			timeoutSecs = Integer.parseInt(timeoutSecsStr);
		} catch (NumberFormatException e) {
			System.out.println(
					"WARNING: Invalid value for "
							+ TIMEOUT_SECS
							+ ": ["
							+ timeoutSecsStr
							+ "]. Using default: "
							+ timeoutSecs);
		}

		final WebServiceClient wsClient = new WebServiceClient(
				url,
				username,
				password,
				threads,
				timeoutSecs);
		return wsClient;
	}

	private static int getThreads(
			IeegMainParams mainParams,
			Properties props) {
		final String threadsStr = props.getProperty(THREADS);
		if (threadsStr == null) {
			System.err.println(
					"Properties file missing property: ["
							+ THREADS
							+ "].");
			System.exit(1);
		}
		int threads = -1;
		try {
			threads = Integer.parseInt(threadsStr);
		} catch (NumberFormatException e) {
			System.err
					.println(
					"Properties file must contain a positive integer value <= 5 for property: ["
							+ THREADS
							+ "].");
			System.exit(1);
		}
		if (threads <= 0 || threads > 5) {
			System.err
					.println(
					"Properties file must contain a positive integer value no greater than 5 for property: ["
							+ THREADS
							+ "].");
			System.exit(1);
		}
		if (mainParams.isVerbose()) {
			System.out.println(
					THREADS
							+ " = "
							+ threads);
		}
		return threads;
	}

	private static void checkStrProp(
			String propName,
			String propValue) {
		if (propValue == null) {
			System.err.println(
					"Properties file missing property: ["
							+ propName
							+ "].");
			System.exit(1);
		} else if ("".equals(propValue)) {
			System.err.println(
					"Properties file missing value for property: ["
							+ propName
							+ "].");
			System.exit(1);
		}
	}

}
