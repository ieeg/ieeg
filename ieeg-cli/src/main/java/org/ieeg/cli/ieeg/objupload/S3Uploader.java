/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.objupload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Set;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IeegMain;
import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferProgress;
import com.amazonaws.services.s3.transfer.Upload;
import com.google.common.base.Optional;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.services.UploadCredentials;

/**
 * @author John Frommeyer
 *
 */
public final class S3Uploader {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Optional<String> awsSecretKey;
	private final Optional<String> commandLineBucket;
	private final Optional<String> collectionName;
	private final boolean disableAwsCertChecking;
	private TransferManager transferManager;
	private final WebServiceClient wsClient;
	private final IeegMainParams mainParams;
	private final Set<File> files = newLinkedHashSet();

	private String authzBucket;
	private String authzPrefix;
	private int successfulUploadCount;

	public S3Uploader(
			Set<File> files,
			IeegMainParams mainParams,
			WebServiceClient wsClient,
			boolean disableAwsCertChecking,
			@Nullable String awsSecretKey,
			@Nullable String commandLineBucket,
			@Nullable String collectionName) {
		this.files.addAll(files);
		this.mainParams = checkNotNull(mainParams);
		this.wsClient = checkNotNull(wsClient);
		this.disableAwsCertChecking = disableAwsCertChecking;
		this.awsSecretKey = Optional.fromNullable(awsSecretKey);
		this.commandLineBucket = Optional.fromNullable(commandLineBucket);
		this.collectionName = Optional.fromNullable(collectionName);
	}

	public String getAuthzBucket() {
		return authzBucket;
	}

	public String getAuthzPrefix() {
		return authzPrefix;
	}

	public int getSuccessfulUploadCount() {
		return successfulUploadCount;
	}

	public int upload() {
		int exitStatus = 0;

		UploadCredentials credentials = null;
		try {
			credentials = wsClient.getUploadCredentials(
					commandLineBucket
							.orNull(),
					collectionName
							.orNull());
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not get upload credentials: "
							+ e.getMessage());
			return 1;
		}

		if (mainParams.isVerbose()) {
			System.out.println(
					"Access Key ID: "
							+ credentials.getId());

			boolean returnedSecretKey = credentials.getSecretKey() != null;
			System.out
					.println(returnedSecretKey
							? "A secret key was returned from the server"
							: "No secret key was returned from the server");

			boolean returnedSessionKey = credentials.getSessionToken() != null;
			System.out
					.println(returnedSessionKey
							? "A session key was returned from the server"
							: "No session key was returned from the server");

			System.out.println(
					"Bucket: "
							+ credentials.getAuthorizedBucket());
			System.out.println(
					"Prefix: "
							+ credentials.getAuthorizedPrefix());
		}
		AWSCredentials awsCredentials = null;
		String secretKey = credentials.getSecretKey();

		if (secretKey == null && !awsSecretKey.isPresent()) {
			System.err
					.println(
					"An AWS Access Key ID was found in your user profile but no value for "
							+ IeegMain.AWS_SECRET_KEY
							+ " is present in ieeg.properties.");
			return 1;
		}

		if (secretKey == null) {
			secretKey = awsSecretKey.get();
			awsCredentials = new BasicAWSCredentials(
					credentials.getId(),
					secretKey);
		} else {
			String sessionToken = credentials.getSessionToken();
			awsCredentials = new BasicSessionCredentials(
					credentials.getId(),
					secretKey,
					sessionToken);
		}

		final Set<ObjectUpload> uploads = newLinkedHashSet();
		authzBucket = credentials.getAuthorizedBucket();
		authzPrefix = credentials.getAuthorizedPrefix();
		System.out.println(
				"Starting upload to bucket ["
						+ authzBucket
						+ "] with prefix ["
						+ authzPrefix
						+ "]");
		if (disableAwsCertChecking) {
			System.out.println("Allowing self-signed certificates for AWS requests");
			System.setProperty(SDKGlobalConfiguration.DISABLE_CERT_CHECKING_SYSTEM_PROPERTY, "true");
		}
		final ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setMaxErrorRetry(20);
		final AmazonS3Client s3Client = new AmazonS3Client(awsCredentials,
				clientConfiguration);
		// Ask for path-style access (s3.amazonaws.com/bucket-name/...) instead
		// of virtual-host-style access
		// (bucket-name.s3.amazonaws.com/...) because VH style leads to wildcard
		// SSL certs which some users' systems
		// replace with self-signed certs which our HTTP client rejects. 
		// But some users' systems still replace non-wildcard certs too!
		// So we use the System Property above too.
		s3Client.setS3ClientOptions(new S3ClientOptions()
				.withPathStyleAccess(true));
		transferManager = new TransferManager(s3Client);
		for (final File fileToUpload : files) {
			final String name = fileToUpload.getName();
			final String authzFileKey = authzPrefix
					+ "/"
					+ name;
			System.out.println(
					"Scheduling upload of "
							+ fileToUpload.getAbsolutePath());
			final PutObjectRequest putObjectRequest = new PutObjectRequest(
					authzBucket,
					authzFileKey,
					fileToUpload)
					.withStorageClass(StorageClass.Standard);
			final Upload upload = transferManager.upload(putObjectRequest);
			final ObjectUpload objectUpload = new ObjectUpload(
					name,
					upload);
			final ProgressListener abortAfterFailedPartProgressListener = new AbortAfterFailedPartProgressListener(
					objectUpload);
			upload.addProgressListener(abortAfterFailedPartProgressListener);
			uploads.add(
					objectUpload);
		}

		successfulUploadCount = waitForCompletion(
				uploads);
		transferManager.shutdownNow();
		if (successfulUploadCount < files.size()) {
			exitStatus = 1;
		}
		return exitStatus;
	}

	/**
	 * Returns the number of successful uploads
	 * 
	 * @param exitStatus
	 * @param uploads
	 * @return
	 */
	private int waitForCompletion(
			final Set<ObjectUpload> uploads) {
		final String m = "waitForCompletion(...)";
		final DecimalFormat percentageFormat = new DecimalFormat("###.##");
		final Set<ObjectUpload> successful = newLinkedHashSet();
		final Set<ObjectUpload> failed = newLinkedHashSet();
		while (true) {
			successful.clear();
			failed.clear();
			for (final ObjectUpload uploadWithName : uploads) {
				final String name = uploadWithName.getName();
				final Upload upload = uploadWithName.getUpload();
				if (upload.isDone() || uploadWithName.isCancelled()) {
					if (upload.getState().equals(TransferState.Completed)) {
						successful.add(uploadWithName);
					} else {
						failed.add(uploadWithName);
					}
					logger.debug(
							"{}: Upload of {} has completed with state {}",
							m,
							name,
							upload.getState());
				} else {
					final TransferProgress progress = upload.getProgress();

					final long totalBytes = progress.getTotalBytesToTransfer();
					final long transferredBytes = progress
							.getBytesTransferred();

					logger.debug(
							"{}: Upload of {} has state {} and has transferred {} of {} bytes",
							m,
							name,
							upload.getState(),
							transferredBytes,
							totalBytes);
					if (transferredBytes < totalBytes) {
						System.out.println("Upload of "
								+ name
								+ " "
								+ percentageFormat.format(progress
										.getPercentTransferred())
								+ "% done");

					} else {
						// Sometimes get > 100% or multiple 100% lines. Maybe
						// because a part is re-uploaded
						// because of error?
						System.out.println("Upload of "
								+ name
								+ " finalizing");
					}

				}

			}
			if (successful.size() + failed.size() == uploads.size()) {
				System.out.println("Uploading finished: ");
				for (final ObjectUpload uploadWithName : successful) {
					System.out.println("Successful: "
							+ uploadWithName.getName());
				}
				for (final ObjectUpload uploadWithName : failed) {
					final String name = uploadWithName.getName();
					final TransferState state = uploadWithName.getUpload()
							.getState();
					System.out
							.println("Failed: "
									+ name
									+ ((uploadWithName.isCancelled() || state == TransferState.Failed)
											? ""
											: ". Upload was cancelled before it could finish."));
				}
				break;
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				System.err
						.println("Caught interrupted exception. Reasserting.");
			}
		}
		return successful.size();
	}

}
