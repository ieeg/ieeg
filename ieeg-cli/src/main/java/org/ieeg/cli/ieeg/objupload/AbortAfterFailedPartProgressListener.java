/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.objupload;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.SyncProgressListener;

/**
 * This class listens for failed upload part events. It aborts the upload and
 * sets the {@link ObjectUpload} to cancelled. The class was created because in
 * testing parallel multipart uploads it appeared that the {@link TransferState}
 * of the {@link Upload} was not moved to a done state when a part failed
 * leading to an infinite loop in {@link S3Uploader}. We also were left with
 * un-completed and un-aborted multipart uploads.
 *
 */
public final class AbortAfterFailedPartProgressListener extends
		SyncProgressListener {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final ObjectUpload upload;

	public AbortAfterFailedPartProgressListener(ObjectUpload upload) {
		this.upload = checkNotNull(upload);
	}

	@Override
	public void progressChanged(ProgressEvent progressEvent) {
		final String m = "progressChanged(...)";
		final ProgressEventType eventType = progressEvent.getEventType();
		logger.debug("{}: Progress event for {}: {}", m, upload.getName(), eventType);
		if (eventType.equals(ProgressEventType.TRANSFER_PART_FAILED_EVENT)) {
			if (!upload.isCancelled()) {
				upload
						.getUpload().abort();
				upload.cancel();
				logger.debug("{}: part upload of {} failed, aborting",
						m
						, upload.getName());
			}
		}
	}

}