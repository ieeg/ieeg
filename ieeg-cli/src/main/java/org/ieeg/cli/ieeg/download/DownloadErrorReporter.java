/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IHasTransferState;

import com.google.common.util.concurrent.FutureCallback;

public final class DownloadErrorReporter implements FutureCallback<Void> {

	private String fileName;
	private IHasTransferState hasDownloadState;

	public DownloadErrorReporter(
			String fileName,
			IHasTransferState hasDownloadState) {
		this.fileName = checkNotNull(fileName);
		this.hasDownloadState = checkNotNull(hasDownloadState);
	}

	@Override
	public void onSuccess(@Nullable Void result) {}

	@Override
	public void onFailure(Throwable t) {
		String errorMsg = t.getMessage();
		System.err
				.println(
				"An error occured during the download of file "
						+
						fileName
						+ ": "
						+ errorMsg
						+ ((errorMsg.endsWith(".")) ? "" : ".")
						+ " The final state of the download is: "
						+ hasDownloadState.getState());
	}

}