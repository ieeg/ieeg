/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.objupload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.File;
import java.util.Set;

import javax.annotation.Nullable;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegObjUploadCommand;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;
import com.google.common.base.Optional;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;

/**
 * @author John Frommeyer
 *
 */
public class ObjUploader {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegObjUploadCommand objUploadCommand;
	private final WebServiceClient wsClient;
	private final String awsSecretKey;

	public ObjUploader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegObjUploadCommand objUploadCommand,
			WebServiceClient wsClient,
			int threads,
			@Nullable String awsSecretKey) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.objUploadCommand = checkNotNull(objUploadCommand);
		this.wsClient = checkNotNull(wsClient);
		this.awsSecretKey = awsSecretKey;
	}

	public int upload() {
		int exitStatus = 0;
		final Set<String> objectPaths = newLinkedHashSet(objUploadCommand
				.getObjects());
		if (objectPaths.isEmpty()) {
			System.out.println("No objects given. Nothing to upload.");
			return 0;
		}
		final Set<File> files = newLinkedHashSet();
		final Optional<String> controlFilePath = Optional
				.fromNullable(objUploadCommand.getControlFile());
		Optional<File> controlFile = Optional.absent();
		if (controlFilePath.isPresent()) {
			final File controlFileTest = new File(controlFilePath.get());
			if (!controlFileTest.exists()) {
				System.err
						.println("Control file ["
								+ controlFilePath
								+ "] does not exist");
				return 1;
			}
			if (!controlFileTest.isFile()) {
				System.err
						.println("Control file ["
								+ controlFilePath
								+ "] is not a file. The control file must be a normal non-directory file.");
				return 1;
			}

			files.add(controlFileTest);
			controlFile = Optional.of(controlFileTest);
		}
		for (final String objectPath : objectPaths) {
			final File fileToUpload = new File(objectPath);
			if (!fileToUpload.exists()) {
				System.err
						.println("File ["
								+ objectPath
								+ "] does not exist");
				return 1;
			}
			if (!fileToUpload.isFile()) {
				System.err
						.println("File ["
								+ objectPath
								+ "] is not a file. The file must be a normal non-directory file.");
				return 1;
			}

			files.add(fileToUpload);
		}
		final S3Uploader s3Uploader = new S3Uploader(
				files,
				mainParams,
				wsClient,
				objUploadCommand.awsCertChecking.isDisabled(),
				awsSecretKey,
				objUploadCommand.getBucket(),
				objUploadCommand.getCollectionName());
		exitStatus = s3Uploader.upload();
		final int successfulUploadCount = s3Uploader.getSuccessfulUploadCount();

		if (successfulUploadCount > 0 && controlFile.isPresent()) {
			try {
				final Optional<String> queueName = objUploadCommand
						.getQueueName();
				final ControlFileRegistration registration = wsClient
						.createControlFileRegistration(
								s3Uploader.getAuthzBucket(),
								s3Uploader.getAuthzPrefix()
										+ "/"
										+ controlFile
												.get()
												.getName(),
								queueName.orNull());
				System.out
						.println(
						"Registered control file in bucket ["
								+ registration.getBucket()
								+ "] with file key ["
								+ registration.getFileKey()
								+ "]"
								+ (queueName.isPresent()
										? (" to queue [" + queueName.get() + "]")
										: ""));
			} catch (IeegWsRemoteException e) {
				System.err.println("Failed to register control file: "
						+ e.getMessage());
				return 1;
			}
		}
		return exitStatus;
	}

}
