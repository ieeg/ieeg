/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.register;

import static com.google.common.base.Preconditions.checkNotNull;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegRegisterCommand;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;

/**
 * @author John Frommeyer
 *
 */
public class Registrar {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegRegisterCommand registerCommand;
	private final WebServiceClient wsClient;

	public Registrar(
			JCommander jc,
			IeegMainParams mainParams,
			IeegRegisterCommand registerCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.registerCommand = checkNotNull(registerCommand);
		this.wsClient = checkNotNull(wsClient);
	}

	public int register() {
		int exitStatus = 0;
		final String bucket = registerCommand.getBucket();
		final String fileKey = registerCommand.getFileKey();

		try {
			final ControlFileRegistration registration = wsClient
					.createControlFileRegistration(
							bucket,
							fileKey,
							registerCommand.getQueueName().orNull());
			System.out
					.println("Registered control file with bucket ["
							+ registration.getBucket()
							+ "], and file key ["
							+ registration.getFileKey()
							+ "]"
							+ (registerCommand.getQueueName().isPresent()
									? (" to queue ["
											+ registerCommand.getQueueName()
													.get() + "]")
									: ""));
		} catch (IeegWsRemoteException e) {
			System.err.println("Cannot register control file: "
					+ e.getMessage());
			return 1;
		}
		return exitStatus;
	}

}
