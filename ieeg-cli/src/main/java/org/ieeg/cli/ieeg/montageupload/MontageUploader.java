/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.montageupload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.File;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegMontageUploadCommand;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;

/**
 * @author John Frommeyer
 *
 */
public class MontageUploader {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegMontageUploadCommand montageUploadCommand;
	private final WebServiceClient wsClient;

	public MontageUploader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegMontageUploadCommand montageUploadCommand,
			WebServiceClient wsClient,
			int threads) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.montageUploadCommand = checkNotNull(montageUploadCommand);
		this.wsClient = checkNotNull(wsClient);
	}

	public int upload() {

		final String datasetName = montageUploadCommand.getDatasetName();
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}
		final Set<String> montagePaths = newLinkedHashSet(montageUploadCommand
				.getMontages());
		if (montagePaths.isEmpty()) {
			System.out.println("No montages given. Nothing to upload.");
			return 0;
		}
		final Set<File> files = newLinkedHashSet();

		for (final String montagePath : montagePaths) {
			final File fileToUpload = new File(montagePath);
			if (!fileToUpload.exists()) {
				System.err
						.println("File ["
								+ montagePath
								+ "] does not exist");
				return 1;
			}
			if (!fileToUpload.isFile()) {
				System.err
						.println("File ["
								+ montagePath
								+ "] is not a file. The file must be a normal non-directory file.");
				return 1;
			}

			files.add(fileToUpload);
		}

		int successCount = 0;
		int failCount = 0;
		for (final File montageFile : files) {
			Response response = null;
			try {
				response = wsClient.createEditMontage(datasetId,
						montageFile);
				successCount++;
				if (response.getStatus() == Response.Status.CREATED
						.getStatusCode()) {
					System.out.println("created montage " 
						+ montageFile
						+ " on "
						+ datasetName);
				} else if (response.getStatus() == Response.Status.NO_CONTENT
						.getStatusCode()) {
					System.out.println("updated montage " 
						+ montageFile
						+ " on "
						+ datasetName);
				} else {
					System.out.println("unexpected status for montage "
							+ montageFile
							+ ": "
							+ response.getStatus());
				}
			} catch (IllegalArgumentException e) {
				failCount++;
				System.err.println("failed to parse montage file "
						+ montageFile
						+ ": "
						+ e.getMessage());
				if (mainParams.isVerbose()) {
					e.printStackTrace();
				}
			} catch (IeegWsRemoteException e) {
				failCount++;
				System.err.println("error during upload for montage file "
						+ montageFile
						+ ": "
						+ e.getMessage());
				if (mainParams.isVerbose()) {
					e.printStackTrace();
				}
			} finally {
				if (response != null) {
					response.close();
				}
			}
		}
		System.out.println(successCount + " of " + files.size()
				+ " montages uploaded");

		int exitStatus = 0;
		if (failCount > 0) {
			exitStatus = 1;
			System.out.println(failCount + " of " + files.size()
					+ " montages failed to upload");
		}

		return exitStatus;
	}
}
