/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.size;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegSizeCommand;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.DatasetFileInfos;
import edu.upenn.cis.db.mefview.services.FileInfo;
import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class SizeReporter {
	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegSizeCommand sizeCommand;
	private final WebServiceClient wsClient;
	private final String datasetName;
	private final SizeToStringConverter sizeToString = new SizeToStringConverter();

	/**
	 * 
	 * @param jc
	 * @param mainParams
	 * @param sizeCommand
	 * @param wsClient
	 */
	public SizeReporter(
			JCommander jc,
			IeegMainParams mainParams,
			IeegSizeCommand sizeCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.sizeCommand = checkNotNull(sizeCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = getDatasetName();
	}

	public int reportSize() {
		String datasetId = null;
		DatasetFileInfos dsFileInfos = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
			dsFileInfos = wsClient.getDatasetFileInfos(datasetId);
		} catch (IeegWsRemoteException e) {
			System.err
					.println("Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: " + e.getMessage());
			return 1;
		}

		if (dsFileInfos.getFileInfos().isEmpty()) {
			System.out.println(
					"Dataset ["
							+ datasetName
							+ "] does not contain any files.");
			return 0;
		}
		long totalBytes = 0;
		for (FileInfo fileInfo : dsFileInfos.getFileInfos()) {
			totalBytes += fileInfo.getSizeBytes().longValue();
		}
		Unit sizeUnit = sizeToString.getUnitForSizeBytes(totalBytes);
		String size = sizeToString.asString(totalBytes, sizeUnit);
		System.out.println(size + sizeUnit.toString());
		return 0;

	}

	private String getDatasetName() {
		final List<String> datasetNames = sizeCommand.getDatasetNames();
		if (datasetNames.isEmpty()) {
			System.err.println("No dataset name specified.");
			jc.usage();
			System.exit(1);
		}
		final String datasetName = datasetNames.get(0);
		if (datasetNames.size() > 1) {
			System.out.println(
					"WARNING: Ignoring all but first dataset: ["
							+ datasetName
							+ "]");
		}

		if (mainParams.isVerbose()) {
			System.out.println(
					"Will attempt to find size of dataset: ["
							+ datasetName
							+ "]");
		}
		return datasetName;
	}
}
