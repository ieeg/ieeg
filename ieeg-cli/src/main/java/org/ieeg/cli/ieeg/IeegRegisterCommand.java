/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.google.common.base.Optional;

/**
 * Parameters for the ieeg upload-objects.
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "Register control file")
public class IeegRegisterCommand {

	final public static String COMMAND_NAME = "register";

	@Parameter(
			required = true,
			names = {
					"-b",
					"--bucket" },
			description = "<name of control file bucket>")
	private String bucket;

	@Parameter(
			required = true,
			names = {
					"-f",
					"--fileKey" },
			description = "<file key of control file>")
	private String fileKey;

	@Parameter(
			names = {
					"-q",
					"--queueName" },
			description = "<name of upload queue>",
			required = false)
	private String queueName;

	public Optional<String> getQueueName() {
		return Optional.fromNullable(queueName);
	}

	public String getBucket() {
		return bucket;
	}

	public String getFileKey() {
		return fileKey;
	}

}
