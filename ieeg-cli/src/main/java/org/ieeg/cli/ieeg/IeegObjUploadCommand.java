/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.ieeg.cli.ieeg.objupload.AwsCertCheckingOption;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.google.common.base.Optional;

/**
 * Parameters for the ieeg upload-objects.
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "Upload objects")
public class IeegObjUploadCommand {

	final public static String COMMAND_NAME = "upload-objects";

	@Parameter(description = "<objects to upload>")
	private List<String> objects = newArrayList();

	@Parameter(
			names = {
					"-f",
					"--controlFile" },
			description = "<control file>")
	private String controlFile;

	@Parameter(
			names = {
					"-n",
					"--collectionName" },
			description = "<name of collection>")
	private String collectionName;

	@Parameter(
			names = {
					"-b",
					"--bucket" },
			description = "<name of bucket>")
	private String bucket;

	@Parameter(
			names = {
					"-q",
					"--queueName" },
			description = "<name of upload queue>",
			required = false)
	private String queueName;

	public Optional<String> getQueueName() {
		return Optional.fromNullable(queueName);
	}

	@ParametersDelegate
	public AwsCertCheckingOption awsCertChecking = new AwsCertCheckingOption();

	public String getBucket() {
		return bucket;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public String getControlFile() {
		return controlFile;
	}

	public List<String> getObjects() {
		return objects;
	}

}
