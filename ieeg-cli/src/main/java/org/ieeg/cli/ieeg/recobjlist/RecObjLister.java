/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.recobjlist;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegRecObjListCommand;
import org.ieeg.cli.ieeg.IeegUtil;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

/**
 * @author John Frommeyer
 *
 */
public class RecObjLister {
	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegRecObjListCommand recObjListCommand;
	private final WebServiceClient wsClient;

	public RecObjLister(
			JCommander jc,
			IeegMainParams mainParams,
			IeegRecObjListCommand recObjListCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.recObjListCommand = checkNotNull(recObjListCommand);
		this.wsClient = checkNotNull(wsClient);
	}

	public int list() {
		int exitStatus = 0;
		String datasetName = recObjListCommand.getDatasetNames().get(0);
		if (recObjListCommand.getDatasetNames().size() > 1) {
			System.out
					.println(
					"WARNING: More than one dataset name specified. Ignoring all but the first: ["
							+ datasetName
							+ "]");
		}
		RecordingObjects recordingObjectContainer = null;
		try {
			final String datasetId = wsClient
					.getDataSnapshotIdByName(datasetName);
			recordingObjectContainer = wsClient.listObjects(datasetId);
		} catch (IeegWsRemoteException e) {
			System.err
					.println("Could not retrieve recording objects for dataset ["
							+ datasetName
							+ "]: " + e.getMessage());
			return 1;
		}
		final Set<RecordingObject> recordingObjects = recordingObjectContainer
				.getRecordingObjects();
		if (recordingObjects.isEmpty()) {
			System.out.println(datasetName + " contains no objects");
		} else {
			IeegUtil.listRecordingObjects(recordingObjects,
					System.out);
		}
		return exitStatus;
	}
}
