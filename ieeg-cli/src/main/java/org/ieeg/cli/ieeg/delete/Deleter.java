/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.delete;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Scanner;

import org.ieeg.cli.ieeg.IeegDeleteCommand;
import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.WebServiceClient;

import com.beust.jcommander.JCommander;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;

/**
 * Deletes a recording object
 * 
 * @author John Frommeyer
 *
 */
public class Deleter {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegDeleteCommand deleteCommand;
	private final WebServiceClient wsClient;

	public Deleter(
			JCommander jc,
			IeegMainParams mainParams,
			IeegDeleteCommand deleteCommand,
			WebServiceClient wsClient) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.deleteCommand = checkNotNull(deleteCommand);
		this.wsClient = checkNotNull(wsClient);
	}

	public int delete() {
		int exitStatus = 0;
		final List<String> datasetNames = deleteCommand.getDataset();
		if (datasetNames.isEmpty()) {
			System.out.println("No objects specified.");
			return 0;
		}
		if (datasetNames.size() > 1) {
			System.out.println("Ignoring all but first dataset.");
		}
		final String datasetName = deleteCommand.getDataset().get(0);
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}

		boolean okayToDelete = true;
		if (!deleteCommand.isForce()) {
			System.out.print("delete " + datasetName + "? ");

			@SuppressWarnings("resource")
			Scanner inputScanner = new Scanner(System.in);
			String response = inputScanner.next();
			if (!response.startsWith("y") && !response.startsWith("Y")) {
				okayToDelete = false;
			}
		}
		if (okayToDelete) {
			final boolean deleteEmptySubject = deleteCommand.deleteEmptySubject();
			try {
				wsClient.deleteDataset(
						datasetId,
						null,
						deleteEmptySubject);
				System.out
						.println(
						"Deleted dataset "
								+ datasetName);
			} catch (IeegWsRemoteException e) {
				System.err.println(
						"Could not delete dataset ["
								+ datasetName
								+ "]: "
								+ e.getMessage());
				exitStatus = 1;
			}
		}
		return exitStatus;
	}

}
