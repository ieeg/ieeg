/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import edu.upenn.cis.db.mefview.services.Config;
import edu.upenn.cis.db.mefview.services.ContentTypeClientRequestFilter;
import edu.upenn.cis.db.mefview.services.DatasetFileInfos;
import edu.upenn.cis.db.mefview.services.IConfigResource;
import edu.upenn.cis.db.mefview.services.ICredentialResource;
import edu.upenn.cis.db.mefview.services.IDatasetResource;
import edu.upenn.cis.db.mefview.services.IObjectResource;
import edu.upenn.cis.db.mefview.services.IRegistrationResource;
import edu.upenn.cis.db.mefview.services.ITimeSeriesResource;
import edu.upenn.cis.db.mefview.services.IeegWsErrorHandler;
import edu.upenn.cis.db.mefview.services.SigClientRequestFilter;
import edu.upenn.cis.db.mefview.services.UploadCredentials;
import edu.upenn.cis.db.mefview.services.UserAndPassword;
import edu.upenn.cis.db.mefview.services.WebClientRequestor;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontages;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

/**
 * Talks to the required web services.
 * 
 * @author John Frommeyer
 */
@ThreadSafe
public final class WebServiceClient {
	// Add some content for test to avoid
	// https://issues.jboss.org/browse/RESTEASY-766
	private final static String fakeContentForTest = "fake";
	private final static String md5Base64ForFakeContent = BaseEncoding.base64()
			.encode(
					Hashing.md5()
							.hashString(fakeContentForTest,
									Charset.defaultCharset()).asBytes());
	
	private final ObjectMapper objectMapper = new ObjectMapper();
	private ITimeSeriesResource timeSeriesResource;
	private IDatasetResource datasetResource;
	private IObjectResource objectResource;
	private IConfigResource configResource;
	private ICredentialResource credentialResource;
	private IRegistrationResource registrationResource;
	private ResteasyWebTarget target;
	private String username;
	private String hashedPassword;
	private IeegWsErrorHandler exceptionHandler = new IeegWsErrorHandler();

	public WebServiceClient(
			String url,
			String username,
			String password,
			int threads,
			int timeoutSecs) {
		checkNotNull(url);
		this.username = checkNotNull(username);
		checkNotNull(password);
		this.hashedPassword = DigestUtils.md5Hex(password);
		checkArgument(threads > 0);
		checkArgument(timeoutSecs > 0);
		int timeoutMs = timeoutSecs * 1000;

		ResteasyClientBuilder clientBuilder = new ResteasyClientBuilder();
		final String proxyHost = System.getProperty("http.proxyHost");
		
		PoolingClientConnectionManager cm = new PoolingClientConnectionManager();
		cm.setMaxTotal(threads);
		cm.setDefaultMaxPerRoute(threads);
		HttpClient httpClient = WebClientRequestor.wrapClient(
				new DefaultHttpClient(cm),
				url.startsWith("https:"));
		HttpParams params = httpClient.getParams();
		// These should be longer than the timeout for acquiring per-user locks
		// for download on the server.
		HttpConnectionParams.setConnectionTimeout(params, timeoutMs);
		HttpConnectionParams.setSoTimeout(params, timeoutMs);
		
		ApacheHttpClient4Engine engine = new
				ApacheHttpClient4Engine(httpClient);
		if (proxyHost != null) {
			final String proxyPortStr = System.getProperty(
					"http.proxyPort",
					"-1");
			int proxyPort = -1;
			try {
				proxyPort = Integer.valueOf(proxyPortStr);
			} catch (NumberFormatException e) {
				System.err
						.println("Ignoring invalid value for http.proxyPort: ["
								+ proxyPortStr + "]");
			}
			System.out.println("HTTP proxy host: " + proxyHost);
			if (proxyPort != -1) {
				System.out.println("HTTP proxy port: " + proxyPort);
			}
			final HttpHost httpProxy = new HttpHost(proxyHost, proxyPort);
			engine.setDefaultProxy(httpProxy);
		}
		clientBuilder.httpEngine(engine);

		ContentTypeClientRequestFilter contentTypeClientRequestFilter = new ContentTypeClientRequestFilter();
		clientBuilder.register(contentTypeClientRequestFilter);

		SigClientRequestFilter sigClientRequestFilter = new SigClientRequestFilter();
		clientBuilder.register(sigClientRequestFilter);

		// clientBuilder.register(DebuggingClientResponseFilter.class);

		Client client = clientBuilder.build();
		target = (ResteasyWebTarget) client.target(url);
		timeSeriesResource = target.proxy(ITimeSeriesResource.class);
		datasetResource = target.proxy(IDatasetResource.class);
		objectResource = target.proxy(IObjectResource.class);
		configResource = target.proxy(IConfigResource.class);
		credentialResource = target.proxy(ICredentialResource.class);
		registrationResource = target.proxy(IRegistrationResource.class);
	}

	public Config getConfig() {
		try {
			setUserAndPasswd();
			return configResource.getConfig();
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public UploadCredentials getUploadCredentials(
			@Nullable String bucket,
			@Nullable String collectionName) {
		try {
			setUserAndPasswd();
			return credentialResource.getCredentials(
					bucket,
					collectionName);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public DatasetFileInfos getDatasetFileInfos(String datasetId) {
		try {
			setUserAndPasswd();
			return datasetResource.getDatasetFileInfos(
					datasetId);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public String getDataSnapshotIdByName(String datasetName) {
		try {
			setUserAndPasswd();
			return timeSeriesResource.getDataSnapshotIdByName(
					datasetName);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public Response getTimeSeriesContents(
			String timeSeriesId,
			String eTag,
			@Nullable String range) {
		setUserAndPasswd();
		final Response response = timeSeriesResource.getTimeSeriesContent(
				timeSeriesId,
				eTag,
				range);
		exceptionHandler.handleErrorClientResponse(response);
		return response;
	}

	public Response getTsAnnotations(String datasetId) {
		setUserAndPasswd();
		final Response response = datasetResource.getTsAnnotations(
				datasetId);
		exceptionHandler.handleErrorClientResponse(response);
		return response;
	}

	public Response getRecordingObjectContents(
			long recordingObjectId,
			String eTag,
			@Nullable String range) {
		setUserAndPasswd();
		final Response response = objectResource.getObject(
				String.valueOf(recordingObjectId),
				eTag,
				range,
				null);
		exceptionHandler.handleErrorClientResponse(response);
		return response;
	}

	public RecordingObjects listObjects(String datasetId) {
		try {
			setUserAndPasswd();
			return datasetResource.getRecordingObjects(
					datasetId);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public RecordingObject createObject(
			String datasetId,
			InputStream objectInputStream,
			String objectName,
			String md5HashBase64) {
		checkNotNull(datasetId);
		checkNotNull(objectName);
		checkNotNull(md5HashBase64);
		Response response = null;
		try {
			setUserAndPasswd();
			final File objectNameAsFile = new File(objectName);
			checkArgument(
					objectName.equals(objectNameAsFile.getName()),
					"illegal object name: ["
							+ objectName
							+ "]. Name must be a simple file name with no path component");
			response = datasetResource.createRecordingObject(
					datasetId,
					objectInputStream,
					objectName,
					md5HashBase64,
					false);
			exceptionHandler.handleErrorClientResponse(response);
			final RecordingObject recordingObject = response
					.readEntity(RecordingObject.class);
			return recordingObject;
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	public boolean testCreateObject(
			String datasetId,
			String objectName) {
		checkNotNull(datasetId);
		checkNotNull(objectName);
		Response response = null;
		try {
			setUserAndPasswd();
			final File objectNameAsFile = new File(objectName);
			checkArgument(
					objectName.equals(objectNameAsFile.getName()),
					"illegal object name: ["
							+ objectName
							+ "]. Name must be a simple file name with no path component");
			final InputStream objectInputStream = new ByteArrayInputStream(
					fakeContentForTest.getBytes());
			response = datasetResource.createRecordingObject(
					datasetId,
					objectInputStream,
					objectName,
					md5Base64ForFakeContent,
					true);
			exceptionHandler.handleErrorClientResponse(response);
			boolean passed = response
					.getStatus() == Response.Status.NO_CONTENT.getStatusCode();
			return passed;
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	public void deleteObject(
			String eTag,
			long objectId) {
		try {
			setUserAndPasswd();
			objectResource.deleteObject(
					eTag,
					objectId);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}
	
	public void deleteDataset(
			String datasetId,
			@Nullable String eTag,
			@Nullable Boolean deleteEmptySubject) {
		try {
			setUserAndPasswd();
			datasetResource.deleteDataset(
					datasetId,
					eTag,
					deleteEmptySubject);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}

	public ControlFileRegistration createControlFileRegistration(
			String bucket,
			String fileKey,
			@Nullable String queueName) {
		checkNotNull(bucket);
		checkNotNull(fileKey);
		Response response = null;
		try {
			setUserAndPasswd();
			final ControlFileRegistration registration = new ControlFileRegistration(
					bucket,
					fileKey);
			if (queueName != null) {
				registration.setMetadata(queueName);
			}
			response = registrationResource
					.createControlFileRegistration(registration);
			exceptionHandler.handleErrorClientResponse(response);
			final ControlFileRegistration newRegistration = response
					.readEntity(ControlFileRegistration.class);
			return newRegistration;
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}
	
	public EEGMontages getMontages(String datasetId) {
		try {
			setUserAndPasswd();
			return datasetResource.getMontages(datasetId);
		} catch (WebApplicationException e) {
			throw exceptionHandler.handleWebApplicationException(e);
		}
	}
	
	public Response createEditMontage(String datasetId, EEGMontage montage) {
		setUserAndPasswd();
		final Response response = datasetResource.createEditMontage(datasetId, montage);
		exceptionHandler.handleErrorClientResponse(response);
		return response;
	}
	
	public Response createEditMontage(String datasetId, File jsonMontageFile) {
		EEGMontage montage;
		try {
			montage = objectMapper.readValue(jsonMontageFile, EEGMontage.class);
		} catch (IOException e) {
			throw new IllegalArgumentException("cannot parse montage file", e);
		}
		setUserAndPasswd();
		final Response response = datasetResource.createEditMontage(datasetId, montage);
		exceptionHandler.handleErrorClientResponse(response);
		return response;
	}

	private void setUserAndPasswd() {
		UserAndPassword.set(
				username,
				hashedPassword);
	}

}
