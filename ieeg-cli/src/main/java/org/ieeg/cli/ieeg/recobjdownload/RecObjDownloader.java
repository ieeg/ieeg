/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.recobjdownload;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;

import org.ieeg.cli.ieeg.IeegMainParams;
import org.ieeg.cli.ieeg.IeegRecObjDownloadCommand;
import org.ieeg.cli.ieeg.WebServiceClient;
import org.ieeg.cli.ieeg.download.DatasetDownload;
import org.ieeg.cli.ieeg.download.DownloadErrorReporter;
import org.ieeg.cli.ieeg.download.FileDownload;
import org.ieeg.cli.ieeg.download.FileDownloadTask;
import org.ieeg.cli.ieeg.download.FileDownloadTask.IContentsProvider;

import com.beust.jcommander.JCommander;
import com.google.common.base.Stopwatch;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.upenn.cis.db.mefview.services.IeegWsRemoteException;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

/**
 * @author John Frommeyer
 *
 */
public class RecObjDownloader {

	private final JCommander jc;
	private final IeegMainParams mainParams;
	private final IeegRecObjDownloadCommand recObjDownloadCommand;
	private final WebServiceClient wsClient;
	private final String datasetName;
	private final ListeningExecutorService threadPool;

	public RecObjDownloader(
			JCommander jc,
			IeegMainParams mainParams,
			IeegRecObjDownloadCommand recObjDownloadCommand,
			WebServiceClient wsClient,
			int threads) {
		this.jc = checkNotNull(jc);
		this.mainParams = checkNotNull(mainParams);
		this.recObjDownloadCommand = checkNotNull(recObjDownloadCommand);
		this.wsClient = checkNotNull(wsClient);
		this.datasetName = recObjDownloadCommand.getDatasetName();
		this.threadPool = MoreExecutors.listeningDecorator(Executors
				.newFixedThreadPool(threads));
	}

	public int download() {
		int exitStatus = 0;
		final Stopwatch watch = Stopwatch.createStarted();
		String datasetId = null;
		try {
			datasetId = wsClient.getDataSnapshotIdByName(datasetName);
		} catch (IeegWsRemoteException e) {
			System.err.println(
					"Could not retrieve dataset information for name ["
							+ datasetName
							+ "]: "
							+ e.getMessage());
			return 1;
		}
		final List<String> objectNames = recObjDownloadCommand.getObjects();
		if (objectNames.isEmpty()) {
			System.out.println("No objects specified.");
			return 0;
		}

		Map<String, RecordingObject> existingNameToObject = newHashMap();
		try {
			final RecordingObjects existingObjectsContainer = wsClient
					.listObjects(datasetId);
			for (RecordingObject existingObject : existingObjectsContainer
					.getRecordingObjects()) {
				existingNameToObject.put(existingObject.getName(),
						existingObject);
			}
		} catch (IeegWsRemoteException e) {
			System.err
					.println(
					"Could not retrieve existing objects for "
							+ datasetName
							+ ": "
							+ e.getMessage());
			return 1;

		}
		if (existingNameToObject.isEmpty()) {
			System.err.println(
					"No objects found for "
							+ datasetName);
			return 1;
		}
		final IContentsProvider<Long> contentsProvider = new IContentsProvider<Long>() {

			@Override
			public Response getContents(Long id, String eTag, String range) {
				return wsClient.getRecordingObjectContents(
						id,
						eTag,
						range);
			}
		};
		Set<FileDownload> downloads = newLinkedHashSet();
		long toDownloadBytes = 0;
		int alreadyOnDiskCount = 0;
		for (String objectName : objectNames) {
			RecordingObject existingObject = existingNameToObject
					.get(objectName);
			if (existingObject == null) {
				System.err.println("Dataset " + datasetName
						+ " does not contain an object called " + objectName);
				exitStatus = 1;
				continue;
			}
			final long serverSizeBytes = existingObject.getSizeBytes();
			final File objectFile = new File(objectName);
			FileDownload download = null;
			String range = null;
			if (!objectFile.exists()) {
				System.out.println("Will download " + objectName);
				toDownloadBytes += serverSizeBytes;
				download = new FileDownload(
						objectName,
						serverSizeBytes);
			} else {
				final long localSizeBytes = objectFile.length();
				if (localSizeBytes < serverSizeBytes) {
					toDownloadBytes += (serverSizeBytes - localSizeBytes);
					range = "bytes=" + localSizeBytes + "-";
					download = new FileDownload(
							objectName,
							serverSizeBytes);
					System.out.println("Will resume download of partial file "
							+ objectName);
				} else if (localSizeBytes > serverSizeBytes) {
					System.err
							.println("Local copy of "
									+ objectName
									+ " is larger than server copy. Will not download.");
					exitStatus = 1;
				} else {
					System.err
							.println("Local copy of "
									+ objectName
									+ " is the same size as the server copy. Will not download.");
					alreadyOnDiskCount++;
				}
			}
			if (download != null) {
				downloads.add(download);
				final FileDownloadTask<Long> task = FileDownloadTask
						.newFileDownloadTask(
								contentsProvider,
								download,
								existingObject.getId(),
								existingObject.getETag(),
								existingObject.getName(),
								existingObject.getSizeBytes().longValue(),
								objectFile,
								range);
				ListenableFuture<Void> downloadFuture = threadPool
						.submit(
						task);
				Futures.addCallback(
						downloadFuture,
						new DownloadErrorReporter(
								objectName,
								download));
			}

		}
		if (downloads.isEmpty()) {
			System.err.println("None of the named objects could be downloaded");
			return 1;
		}
		final DatasetDownload download = new DatasetDownload(
				downloads,
				toDownloadBytes,
				toDownloadBytes,
				null);
		exitStatus = download.waitForCompletion();
		System.out.println("Download time: "
				+ watch.stop().elapsed(TimeUnit.SECONDS) + " seconds");
		return exitStatus;
	}

	public void shutdown() {
		threadPool.shutdown();
	}

}
