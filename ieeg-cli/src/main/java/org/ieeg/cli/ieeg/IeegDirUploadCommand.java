/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.util.List;

import org.ieeg.cli.ieeg.objupload.AwsCertCheckingOption;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.ParametersDelegate;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.Optional;

/**
 * Parameters for the ieeg upload-directory.
 * 
 * @author John Frommeyer
 * 
 */
@Parameters(commandDescription = "Upload and register a directory")
public class IeegDirUploadCommand {

	final public static String COMMAND_NAME = "upload-directory";

	@Parameter(description = "<directory to upload>")
	private List<String> directories = newArrayList();

	@Parameter(
			names = {
					"-r",
					"--recursive" },
			description = "recursively upload the directory")
	private boolean recursive;

	@Parameter(
			names = {
					"-n",
					"--collectionName" },
			description = "<name of collection>")
	private String collectionName;

	@Parameter(
			names = {
					"-b",
					"--bucket" },
			description = "<name of bucket>")
	private String bucket;

	@Parameter(
			names = {
					"-c",
					"--config" },
			converter = FileConverter.class,
			description = "<path to an "
					+ IeegDirUploadCommand.IEEG_DATASET_INI_FILENAME + " file>")
	private File configFile;

	@Parameter(
			names = {
					"-q",
					"--queueName" },
			description = "<name of upload queue>",
			required = false)
	private String queueName;

	final public static String IEEG_DATASET_INI_FILENAME = "ieeg-dataset.ini";

	@ParametersDelegate
	public AwsCertCheckingOption awsCertChecking = new AwsCertCheckingOption();

	public String getBucket() {
		return bucket;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public boolean isRecursive() {
		return recursive;
	}

	public List<String> getDirectories() {
		return directories;
	}

	public File getConfig() {
		return configFile;
	}

	public Optional<String> getQueueName() {
		return Optional.fromNullable(queueName);
	}

}
