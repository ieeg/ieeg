/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.ieeg.download;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.List;
import java.util.Map;

import edu.upenn.cis.db.mefview.services.DatasetFileInfos;
import edu.upenn.cis.db.mefview.services.FileInfo;

public class DatasetDiff {

	private final DatasetFileInfos localFileInfos;
	private final DatasetFileInfos serverFileInfos;
	private final Map<String, FileInfo> idToLocalFileInfo = newHashMap();
	private final Map<String, FileInfo> idToServerFileInfo = newHashMap();
	private final List<String> differences = newArrayList();

	public DatasetDiff(
			DatasetFileInfos localFileInfos,
			DatasetFileInfos serverFileInfos) {
		this.localFileInfos = checkNotNull(localFileInfos);
		this.serverFileInfos = checkNotNull(serverFileInfos);
		for (FileInfo fileInfo : this.localFileInfos.getFileInfos()) {
			idToLocalFileInfo.put(fileInfo.getId(), fileInfo);
		}
		for (FileInfo fileInfo : this.serverFileInfos.getFileInfos()) {
			idToServerFileInfo.put(fileInfo.getId(), fileInfo);
		}
	}

	public List<String> getDifferences() {
		String localName = localFileInfos.getDatasetName();
		String serverName = serverFileInfos.getDatasetName();
		if (!localName.equals(serverName)) {
			differences.add(
					"Dataset name has changed from "
							+ localName
							+ " to "
							+ serverName);
		}
		int localFileCount = localFileInfos.getFileInfos().size();
		int serverFileCount = serverFileInfos.getFileInfos().size();
		if (localFileCount != serverFileCount) {
			differences.add(
					"The number of files has changed from "
							+ localFileCount
							+ " to "
							+ serverFileCount);
		}
		for (Map.Entry<String, FileInfo> localEntry : idToLocalFileInfo
				.entrySet()) {
			String localId = localEntry.getKey();
			FileInfo localFileInfo = localEntry.getValue();
			FileInfo serverFileInfo = idToServerFileInfo.get(localId);
			if (serverFileInfo == null) {
				differences
						.add(
						"File "
								+ localFileInfo.getFileName()
								+ " has been removed from the dataset on the server.");
			} else {
				differences.addAll(getFileInfoDifferences(
						localFileInfo,
						serverFileInfo));
			}
		}
		for (Map.Entry<String, FileInfo> serverEntry : idToServerFileInfo
				.entrySet()) {
			String serverId = serverEntry.getKey();
			FileInfo serverFileInfo = serverEntry.getValue();
			FileInfo localFileInfo = idToLocalFileInfo.get(serverId);
			if (localFileInfo == null) {
				differences
						.add(
						"File "
								+ serverFileInfo.getFileName()
								+ " has been added to the dataset on the server.");
			}
		}
		return differences;
	}

	private List<String> getFileInfoDifferences(
			FileInfo localFileInfo,
			FileInfo serverFileInfo) {
		List<String> fileInfoDiffs = newArrayList();
		if (!localFileInfo.getFileName().equals(serverFileInfo.getFileName())) {
			fileInfoDiffs.add(
					"File name has changed from "
							+ localFileInfo.getFileName()
							+ " to "
							+ serverFileInfo.getFileName());
		}
		if (!localFileInfo.getETag().equals(serverFileInfo.getETag())) {
			fileInfoDiffs.add(
					"File "
							+ localFileInfo.getFileName()
							+ " has changed on the server");
		}
		if (!localFileInfo.getSizeBytes().equals(serverFileInfo.getSizeBytes())) {
			fileInfoDiffs.add(
					"File size has changed from "
							+ localFileInfo.getSizeBytes()
							+ " bytes to "
							+ serverFileInfo.getSizeBytes()
							+ " bytes");
		}
		return fileInfoDiffs;
	}
}
