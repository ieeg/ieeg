/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ieeg.cli;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.common.io.ByteStreams;
import com.google.common.io.LittleEndianDataInputStream;

import edu.upenn.cis.eeg.mef.MefHeader2;

/**
 * For reading forward through a mef file's index.
 * 
 * 
 * @author John Frommeyer
 */
public final class MEFIndexPrinter implements Closeable {

	private static final class MefIndexEntry {
		public final long sampleTimeUutc;
		public final long byteOffset;
		public final long sampleIndex;

		public MefIndexEntry(long sampleTimeUutc, long byteOffset,
				long sampleIndex) {
			this.sampleTimeUutc = sampleTimeUutc;
			this.byteOffset = byteOffset;
			this.sampleIndex = sampleIndex;
		}

	}

	private int indexEntriesRead = 0;
	private final MefHeader2 mefHeader;
	private final LittleEndianDataInputStream mefIndexStream;

	/**
	 * Streams through mef data. Useful if you don't need random access into the
	 * file.
	 * 
	 * {@code mefInputStream}'s pointer will be moved. It is imperative that
	 * clients do not move the pointer while use a {@code MEFStreamer}.
	 * 
	 * @param mefInputStream the mef file stream
	 * @param decompressData if true then decompress the data, otherwise the
	 *            {@code TimeSeriesPage}s will have no data, only start and end
	 *            times
	 * 
	 * @throws IOException if thrown will reading the mef file
	 */
	public MEFIndexPrinter(
			File mefFile) throws IOException {
		try (final RandomAccessFile mefDataStream = new RandomAccessFile(
				mefFile, "r")) {
			byte[] headerBytes = new byte[1024];
			mefDataStream.readFully(headerBytes);
			mefHeader = new MefHeader2(headerBytes);
			final long indexOffset = this.mefHeader.getIndexDataOffset();
			mefIndexStream = new LittleEndianDataInputStream(
					new FileInputStream(
							mefFile));
			ByteStreams.skipFully(mefIndexStream, indexOffset);
		}

	}

	/**
	 * Closes the client supplied {@code RandomAccessFile}.
	 * 
	 * @throws IOException from calling close.
	 */
	@Override
	public void close() throws IOException {
		mefIndexStream.close();
	}

	public MefHeader2 getMEFHeader() {
		return mefHeader;
	}

	/**
	 * Get the next {@code noBlocks} mef index entries or less from the file.
	 * Returns less than {@code noBlocks} if there are less left. Returns an
	 * empty list if there are no more blocks.
	 * 
	 * @param noEntries the number of entries you want to read
	 * 
	 * @return the entries that were read
	 * 
	 * @throws IOException if thrown when reading the file
	 */
	public List<MefIndexEntry> getNextEntries(int noEntries)
			throws IOException {
		List<MefIndexEntry> pages = newArrayList();
		for (int i = 0; i < noEntries; i++) {

			if (!moreEntries()) {
				return pages;
			}
			final MefIndexEntry entry = getNextEntry();
			pages.add(entry);
			indexEntriesRead++;
		}
		return pages;
	}

	private MefIndexEntry getNextEntry() throws IOException {
		checkState(moreEntries());

		// block start uutc
		final long sampleTimeUutc = mefIndexStream.readLong();
		// read offset
		final long blockOffset = mefIndexStream.readLong();
		// index of first sample in block
		final long sampleIndex = mefIndexStream.readLong();

		return new MefIndexEntry(sampleTimeUutc, blockOffset, sampleIndex);
	}

	/**
	 * Get the total number of blocks in the mef file.
	 * 
	 * @return the total number of blocks in the mef file
	 */
	public long getNumberOfBlocks() {
		return mefHeader.getNumberOfIndexEntries();
	}

	public double getSamplingPeriodMicros() {
		return 1E6 / mefHeader.getSamplingFrequency();
	}

	public long getRecordingStartTimeUutc() {
		return mefHeader.getRecordingStartTime();
	}

	/**
	 * Tells us if there are any blocks left.
	 * 
	 * @return true if there are blocks left, false otherwise
	 */
	public boolean moreEntries() {
		if (indexEntriesRead < mefHeader.getNumberOfIndexEntries()) {
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		final File textFile = new File(
				mefFile.getParent(),
				mefFile.getName()
						.replace(
								".mef",
								".index.txt"));
		try (final MEFIndexPrinter streamer = new MEFIndexPrinter(
						mefFile);
				final FileWriter textWriter = new FileWriter(textFile)) {
			textWriter.write(
					"File has "
							+ streamer.getNumberOfBlocks()
							+ " blocks\n");
			textWriter.write(
					"File has period "
							+ streamer.getSamplingPeriodMicros()
							+ " microseconds\n");
			int blockNo = 1;
			while (streamer.moreEntries()) {
				for (MefIndexEntry block : streamer.getNextEntries(1)) {
					final long startUutc = block.sampleTimeUutc;
					final DateTime startDateTime = uutcToDateTime(startUutc);
					textWriter.write("Block "
							+ blockNo
							+ " start: "
							+ startDateTime
							+ " ("
							+ startUutc
							+ "), starts at sample: "
							+ block.sampleIndex
							+ "\n");
					blockNo++;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}
	
	private static DateTime uutcToDateTime(long uutc) {
		final long mutc = TimeUnit.MILLISECONDS.convert(
				uutc,
				TimeUnit.MICROSECONDS);
		final DateTime dateTime = new DateTime(mutc,
				DateTimeZone.UTC);
		return dateTime;
	}

}
