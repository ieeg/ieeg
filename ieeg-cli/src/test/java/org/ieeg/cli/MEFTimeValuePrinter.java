/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;

/**
 * @author John Frommeyer
 *
 */
public class MEFTimeValuePrinter {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		final File textFile = new File(
				mefFile.getParent(),
				mefFile.getName()
						.replace(
								".mef",
								"-plot.txt"));
		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						true);
				final PrintWriter textWriter = new PrintWriter(new FileWriter(textFile))) {
			final double periodMicros = streamer.getSamplingPeriodMicros();
			final long recordingStartUutc = streamer.getRecordingStartTimeUutc();

			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {
					long sampleNo = 0;
					final double blockStartOffsetMicros = block.timeStart - recordingStartUutc;
					for (int value : block.values) {
						final double offsetMicros = blockStartOffsetMicros + sampleNo * periodMicros;
						textWriter.print(offsetMicros + " " + value + "\n");
						sampleNo++;
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}
}
