/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.mefvalidate;

import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;

/**
 * Tests system output for a correct MEF, an incorrect MEF, and both together.
 * Uses System Rules library to analyze the output log and expect system exits.
 * 
 * @author Jordan Hurwitz
 */
public class MEFValidateOutputTest {

	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();

	@Rule
	public final StandardOutputStreamLog log = new StandardOutputStreamLog();

	@Test
	public void testFileNotFound() {
		exit.expectSystemExitWithStatus(2);
		String[] args = { "non-existent.mef" };
		MEFValidateMain.main(args);
	}

	@Test
	public void testCorrectFile() {
		String[] args = { "src/test/resources/ch_06.mef" };
		MEFValidateMain.main(args);
		assertTrue(log
				.getLog()
				.contains(
						"Starting Validation on src/test/resources/ch_06.mef..."
								+ System.getProperty("line.separator")
								+ "Report: "
								+ System.getProperty("line.separator")
								+ "[PASS] [MaxValuePresent] maximum_data_value present: value [5996]"
								+ System.getProperty("line.separator")
								+ "[PASS] [MinValuePresent] minimum_data_value present: value [-8813]"
								+ System.getProperty("line.separator")
								+ "[PASS] [StartTimesMatch] header start time matches first index start time and first entry's start time: hst: [4000] ist: [4000] est: [4000]"
								+ System.getProperty("line.separator")
								+ "[PASS] [CorrectBlockSize] average of at least 2400 samples in each block: (number_of_samples = 91750 / number_of_index_entries = 23) >= 2400: found [3989.0]"
								+ System.getProperty("line.separator")
								+ "[PASS] [NoBlockOverlap] first 100 or less block times do not overlap"
								+ System.getProperty("line.separator")
								+ "Completed validation of src/test/resources/ch_06.mef"));

	}

	@Test
	public void testIncorrectFile() {
		exit.expectSystemExitWithStatus(1);
		exit.checkAssertionAfterwards(new Assertion() {
			public void checkAssertion() {
				assertTrue(log
						.getLog()
						.contains(
								"Starting Validation on src/test/resources/bad.mef..."
										+ System.getProperty("line.separator")
										+ "Report: "
										+ System.getProperty("line.separator")
										+ "[FAIL] [MaxValuePresent] maximum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [MinValuePresent] minimum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [StartTimesMatch] header start time does not match first index start time nor first entry's start time: hst: [3744] ist: [3488] est: [4000]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [CorrectBlockSize] less than 2400 samples in each block: (number_of_samples = 272 / number_of_index_entries = 23) < 2400: found [11.0]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [NoBlockOverlap] first 100 or less block times overlap. Found 2 overlapping blocks, min overlap [1048576usecs] max overlap of [16777216usecs]"
										+ System.getProperty("line.separator")
										+ "Completed validation of src/test/resources/bad.mef"));
			}
		});
		String[] args = { "src/test/resources/bad.mef" };
		MEFValidateMain.main(args);
	}

	@Test
	public void testBothFilesForward() {
		exit.expectSystemExitWithStatus(1);
		exit.checkAssertionAfterwards(new Assertion() {
			public void checkAssertion() {
				assertTrue(log
						.getLog()
						.contains(
								"Starting Validation on src/test/resources/ch_06.mef..."
										+ System.getProperty("line.separator")
										+ "Report: "
										+ System.getProperty("line.separator")
										+ "[PASS] [MaxValuePresent] maximum_data_value present: value [5996]"
										+ System.getProperty("line.separator")
										+ "[PASS] [MinValuePresent] minimum_data_value present: value [-8813]"
										+ System.getProperty("line.separator")
										+ "[PASS] [StartTimesMatch] header start time matches first index start time and first entry's start time: hst: [4000] ist: [4000] est: [4000]"
										+ System.getProperty("line.separator")
										+ "[PASS] [CorrectBlockSize] average of at least 2400 samples in each block: (number_of_samples = 91750 / number_of_index_entries = 23) >= 2400: found [3989.0]"
										+ System.getProperty("line.separator")
										+ "[PASS] [NoBlockOverlap] first 100 or less block times do not overlap"
										+ System.getProperty("line.separator")
										+ "Completed validation of src/test/resources/ch_06.mef"
										+ System.getProperty("line.separator")
										+ '\n'
										+ "Starting Validation on src/test/resources/bad.mef..."
										+ System.getProperty("line.separator")
										+ "Report: "
										+ System.getProperty("line.separator")
										+ "[FAIL] [MaxValuePresent] maximum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [MinValuePresent] minimum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [StartTimesMatch] header start time does not match first index start time nor first entry's start time: hst: [3744] ist: [3488] est: [4000]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [CorrectBlockSize] less than 2400 samples in each block: (number_of_samples = 272 / number_of_index_entries = 23) < 2400: found [11.0]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [NoBlockOverlap] first 100 or less block times overlap. Found 2 overlapping blocks, min overlap [1048576usecs] max overlap of [16777216usecs]"
										+ System.getProperty("line.separator")
										+ "Completed validation of src/test/resources/bad.mef"));
			}
		});

		String[] args = { "src/test/resources/ch_06.mef",
				"src/test/resources/bad.mef" };
		MEFValidateMain.main(args);
	}

	@Test
	public void testBothFilesBackward() {
		exit.expectSystemExitWithStatus(1);
		exit.checkAssertionAfterwards(new Assertion() {
			public void checkAssertion() {
				assertTrue(log
						.getLog()
						.contains(
								"Starting Validation on src/test/resources/bad.mef..."
										+ System.getProperty("line.separator")
										+ "Report: "
										+ System.getProperty("line.separator")
										+ "[FAIL] [MaxValuePresent] maximum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [MinValuePresent] minimum_data_value not present"
										+ System.getProperty("line.separator")
										+ "[FAIL] [StartTimesMatch] header start time does not match first index start time nor first entry's start time: hst: [3744] ist: [3488] est: [4000]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [CorrectBlockSize] less than 2400 samples in each block: (number_of_samples = 272 / number_of_index_entries = 23) < 2400: found [11.0]"
										+ System.getProperty("line.separator")
										+ "[FAIL] [NoBlockOverlap] first 100 or less block times overlap. Found 2 overlapping blocks, min overlap [1048576usecs] max overlap of [16777216usecs]"
										+ System.getProperty("line.separator")
										+ "Completed validation of src/test/resources/bad.mef"
										+ System.getProperty("line.separator")
										+ '\n'
										+ "Starting Validation on src/test/resources/ch_06.mef..."
										+ System.getProperty("line.separator")
										+ "Report: "
										+ System.getProperty("line.separator")
										+ "[PASS] [MaxValuePresent] maximum_data_value present: value [5996]"
										+ System.getProperty("line.separator")
										+ "[PASS] [MinValuePresent] minimum_data_value present: value [-8813]"
										+ System.getProperty("line.separator")
										+ "[PASS] [StartTimesMatch] header start time matches first index start time and first entry's start time: hst: [4000] ist: [4000] est: [4000]"
										+ System.getProperty("line.separator")
										+ "[PASS] [CorrectBlockSize] average of at least 2400 samples in each block: (number_of_samples = 91750 / number_of_index_entries = 23) >= 2400: found [3989.0]"
										+ System.getProperty("line.separator")
										+ "[PASS] [NoBlockOverlap] first 100 or less block times do not overlap"
										+ System.getProperty("line.separator")
										+ "Completed validation of src/test/resources/ch_06.mef"));
			}
		});

		String[] args = { "src/test/resources/bad.mef",
				"src/test/resources/ch_06.mef" };
		MEFValidateMain.main(args);
	}
}
