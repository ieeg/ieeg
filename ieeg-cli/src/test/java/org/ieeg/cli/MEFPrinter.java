/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;

/**
 * @author John Frommeyer
 *
 */
public class MEFPrinter {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		final File textFile = new File(
				mefFile.getParent(),
				mefFile.getName()
						.replace(
								".mef",
								".txt"));
		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						true);
				final FileWriter textWriter = new FileWriter(textFile)) {
			textWriter.write(
					"File has "
							+ streamer.getNumberOfBlocks()
							+ " blocks\n");
			textWriter.write(
					"File has period "
							+ streamer.getSamplingPeriodMicros()
							+ " microseconds\n");
			int blockNo = 1;
			long sampleNo = 1;
			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {
					final long startUutc = block.timeStart;
					final DateTime startDateTime = uutcToDateTime(startUutc);
					textWriter.write("Block "
							+ blockNo
							+ " start: "
							+ startDateTime
							+ " ("
							+ startUutc
							+ "), no. of samples: "
							+ block.values.length
							+ "\n");
					for (int value : block.values) {
						textWriter.write(sampleNo + ": " + value + "\n");
						sampleNo++;
					}
					final long endUutc = block.timeEnd;
					final DateTime endDateTime = uutcToDateTime(endUutc);
					textWriter.write(
							"Block "
									+ blockNo
									+ " end:   "
									+ endDateTime
									+ " ("
									+ block.timeEnd
									+ ")\n");
					blockNo++;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}

	private static DateTime uutcToDateTime(long uutc) {
		final long mutc = TimeUnit.MILLISECONDS.convert(
				uutc,
				TimeUnit.MICROSECONDS);
		final DateTime dateTime = new DateTime(mutc,
				DateTimeZone.UTC);
		return dateTime;
	}
}
