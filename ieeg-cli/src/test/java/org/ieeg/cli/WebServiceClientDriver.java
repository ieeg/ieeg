/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.Response;

import org.ieeg.cli.ieeg.WebServiceClient;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.base.Stopwatch;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.common.io.Files;

import edu.upenn.cis.db.mefview.services.UploadCredentials;
import edu.upenn.cis.db.mefview.shared.EEGMontages;
import edu.upenn.cis.db.mefview.shared.RecordingObject;

/**
 * @author John Frommeyer
 *
 */
public class WebServiceClientDriver {

	public static void main(String[] args) throws IOException {
		final String url = args[0];
		final String username = args[1];
		final String password = args[2];
		createEditMontage(
				args,
				url,
				username,
				password);
	}

	private static void createRecordingObject(
			String[] args,
			final String url,
			final String username,
			final String password) throws
			IOException,
			FileNotFoundException {
		final String recordingName = args[3];
		final String objectPath = args[4];

		final File objectFile = new File(objectPath);
		System.out.println("Computing MD5 for " + objectFile.getName());
		final HashCode md5HashCode = Files.asByteSource(objectFile).hash(
				Hashing.md5());
		final String objectMd5Base64 = BaseEncoding.base64().encode(
				md5HashCode.asBytes());
		System.out.println("Computed MD5: " + md5HashCode.toString());
		final String objectName = objectFile.getName();
		final InputStream is =
				new FileInputStream(objectFile);

		final WebServiceClient client = new WebServiceClient(
				url,
				username,
				password,
				5,
				300);
		final String datasetId = client.getDataSnapshotIdByName(recordingName);
		System.out.println("Found id " + datasetId + " for " + recordingName);
		Stopwatch watch = Stopwatch.createStarted();
		final boolean passed = client.testCreateObject(
				datasetId,
				objectName);
		System.out.println(passed);
		final RecordingObject recordingObject = client.createObject(
				datasetId,
				is,
				objectName,
				objectMd5Base64);
		watch.stop();

		System.out.println(watch.elapsed(TimeUnit.SECONDS) + ": "
				+ recordingObject);
	}

	private static void getUploadCredentials(
			String[] args,
			final String url,
			final String username,
			final String password) {
		final WebServiceClient client = new WebServiceClient(
				url,
				username,
				password,
				5,
				300);
		final UploadCredentials uploadCredentials = client
				.getUploadCredentials(
						null,
						null);

		System.out.println("accessKeyId: " + uploadCredentials.getId());
		System.out.println("secretKey: " + uploadCredentials.getSecretKey());
		System.out.println("sessionToken: "
				+ uploadCredentials.getSessionToken());
		System.out.println("authzPrefix: "
				+ uploadCredentials.getAuthorizedPrefix());
	}

	private static void deleteDataset(
			String[] args,
			final String url,
			final String username,
			final String password) {
		final String datasetName = args[3];
		final WebServiceClient client = new WebServiceClient(
				url,
				username,
				password,
				5,
				300);
		final String datasetId = client.getDataSnapshotIdByName(datasetName);
		System.out.println("Found id " + datasetId + " for " + datasetName);
		client.deleteDataset(datasetId, null, null);
	}

	private static void getMontages(
			String[] args,
			final String url,
			final String username,
			final String password) {
		final String datasetName = args[3];
		final WebServiceClient client = new WebServiceClient(
				url,
				username,
				password,
				5,
				300);
		final String datasetId = client.getDataSnapshotIdByName(datasetName);
		System.out.println("Found id " + datasetId + " for " + datasetName);
		final EEGMontages montages = client.getMontages(datasetId);
		System.out.println(montages.getMontages());
	}

	private static void createEditMontage(
			String[] args,
			final String url,
			final String username,
			final String password) throws JsonParseException,
			JsonMappingException, IOException {
		final String datasetName = args[3];
		final String montageFilePath = args[4];
		final File montageFile = new File(montageFilePath);
		final WebServiceClient client = new WebServiceClient(
				url,
				username,
				password,
				5,
				300);
		final String datasetId = client.getDataSnapshotIdByName(datasetName);
		System.out.println("Found id " + datasetId + " for " + datasetName);
		Response response = null;
		try {
			response = client.createEditMontage(datasetId, montageFile);
			System.out.println("status: " + response.getStatus());
			System.out.println("location: " + response.getLocation());
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}
}
