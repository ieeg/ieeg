/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;

/**
 * @author John Frommeyer
 *
 */
public class MEFGapPrinter {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		boolean exact = false;
		boolean subGap = false;
		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						false)) {
			long previousEndUutc = -1;
			int blockNo = 1;
			final double samplingPeriodMicros = streamer
					.getSamplingPeriodMicros();
			final long recordingStartTimeUutc = streamer.getRecordingStartTimeUutc();
			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {
					final long startUutc = block.timeStart;
					final long endUutc = block.timeEnd;
					if (previousEndUutc != -1) {
						final long gapMicros = startUutc - previousEndUutc;
						final long startOffsetMicros = previousEndUutc - recordingStartTimeUutc;
						final String gapReport = getGapReportString(
								blockNo,
								previousEndUutc,
								startUutc,
								startOffsetMicros,
								gapMicros,
								samplingPeriodMicros);
						if (gapMicros == 0) {
							if (exact) {
								System.out.println("Exact: " + gapReport);
							}
						} else {
							if (gapMicros > 0
									&& gapMicros < samplingPeriodMicros) {
								if (subGap) {
									System.out.println("Sub-gap: " + gapReport);
								}
							} else if (gapMicros >= samplingPeriodMicros) {
								System.out.println("Gap: " + gapReport);
							} else {
								System.out.println("Overlap: " + gapReport);
							}
						}

					}
					previousEndUutc = endUutc;
					blockNo++;
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}

	/**
	 * DOCUMENT ME
	 * 
	 * @param blockNo
	 * @param previousEndUutc
	 * @param startUutc
	 * @param gapMicros
	 */
	private static String getGapReportString(
			int blockNo,
			long previousEndUutc,
			final long startUutc,
			final long startOffsetMicros,
			final long gapMicros,
			final double periodMicros) {
		final DateTime previousDateTime = uutcToDateTime(previousEndUutc);
		final DateTime startDateTime = uutcToDateTime(startUutc);
		return "Block "
				+ (blockNo - 1)
				+ " end: "
				+ previousDateTime
				+ " ("
				+ previousEndUutc
				+ "), Block "
				+ blockNo
				+ " start: "
				+ startDateTime
				+ " ("
				+ startUutc
				+ "), space between: "
				+ gapMicros
				+ " usec ("
				+ gapMicros/1e6
				+ " sec, "
				+ gapMicros/periodMicros
				+ " periods) starting at "
				+ startOffsetMicros
				+ " usec ("
				+ startOffsetMicros/1e6
				+ " sec) from beginning of file";
	}

	private static DateTime uutcToDateTime(long uutc) {
		final long mutc = TimeUnit.MILLISECONDS.convert(
				uutc,
				TimeUnit.MICROSECONDS);
		final DateTime dateTime = new DateTime(mutc,
				DateTimeZone.UTC);
		return dateTime;
	}
}
