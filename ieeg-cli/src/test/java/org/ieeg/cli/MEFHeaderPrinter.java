/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.eeg.mef.MefHeader2;


/**
 * @author John Frommeyer
 *
 */
public class MEFHeaderPrinter {

	/**
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		File mefFile = new File(args[0]);
		System.out.println("Reading header for " + mefFile.getAbsolutePath());
		try (final FileInputStream is = new FileInputStream(mefFile)) {
			byte[] headerBytes = new byte[1024];
			ByteStreams.readFully(is, headerBytes);
			final MefHeader2 header = new MefHeader2(headerBytes);
			System.out.println("sampling freqency (Hz): "
					+ header.getSamplingFrequency());
			System.out.println("recording start time (uUTC): "
					+ header.getRecordingStartTime());
			System.out.println("recording end time (uUTC): "
					+ header.getRecordingEndTime());
			System.out.println("voltage conversion factor: "
					+ header.getVoltageConversionFactor());
			System.out.println("block interval: "
					+ header.getBlockInterval());
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not open file "
							+ args[0]
							+ ": "
							+ e.getMessage());
			System.exit(1);
		}
	}

}
