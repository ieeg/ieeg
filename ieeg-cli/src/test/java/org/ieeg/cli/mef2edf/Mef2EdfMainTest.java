/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli.mef2edf;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.edf.EDFReader;
import edu.upenn.cis.eeg.mef.MEFStreamer;

/**
 * Test for mef2edf command line tool.
 * 
 * @author Veena Krish
 *
 */
public class Mef2EdfMainTest {

	private static Mef2Edf test = null;
	private static Mef2Edf test2 = null;
	private static Mef2Edf test3 = null;
	private static Mef2Edf testStartTimes = null;

	@BeforeClass
	public static void setUpBeforeClass() throws IOException {
		List<String> args = new LinkedList<String>();
		args.add("src/test/resources/ch_06.mef");
		test = new Mef2Edf(args, "output.edf");

		List<String> args2 = new LinkedList<String>();
		args2.add("src/test/resources/ch_06_discont.mef");
		test2 = new Mef2Edf(args2, "output.edf");

		args.add("src/test/resources/ch_06_2.mef");
		test3 = new Mef2Edf(args, "output.edf");
	}

	@Test(expected = FileNotFoundException.class)
	public void testFileNotFound() throws FileNotFoundException, IOException {
		List<String> args3 = new LinkedList<String>();
		args3.add("File not found");
		new Mef2Edf(args3, "output");
	}

	@Rule
	public final ExpectedSystemExit exitStat = ExpectedSystemExit.none();

	@Test
	public void testFilesCreated() throws IOException, Mef2EdfException {
		test.main();
		File outputFile = new File(test.getOutFile());
		assertTrue(outputFile.exists());
	}

	@Test
	public void testCorrectNoFiles() throws IOException, Mef2EdfException {
		test.main();
		String out_file = test.getOutFile();
		int pos = out_file.lastIndexOf('_');
		assertEquals(out_file.substring(pos + 1), "0.edf");
	}

	@Test
	public void testDiscontinuous() throws IOException, Mef2EdfException {
		test2.main();
		String out_file = test2.getOutFile();
		File outputFile = new File(test2.getOutFile());
		outputFile.deleteOnExit();
		int pos = out_file.lastIndexOf('_');
		assertEquals(out_file.substring(pos + 1), "1.edf");
	}

	@Test(expected = Mef2EdfException.class)
	public void testTimeStampsFail() throws IOException, Mef2EdfException {
		test3.main();
	}

	@Test
	public void testSampPerBlockCalc1() throws IOException {
		double duration = test2.calculateBlockDuration();
		assertEquals(1, duration, 1E-7);
	}

	@Test
	public void testSampPerBlockCalc2() throws IOException {
		double duration = test3.calculateBlockDuration();
		assertEquals(1, duration, 1E-7);
	}

	@Test
	public void testStartTimes() throws IOException {
		final String mefPath = "src/test/resources/ch_06_discont.mef";
		final FileInputStream mefFile = new FileInputStream(mefPath);
		final MEFStreamer mefStreamer = new MEFStreamer(mefFile, false);
		final List<Long> expectedStartTimesUUTC = newArrayList();
		long previousEndTimeUUTC = -1;
		while (mefStreamer.moreBlocks()) {
			final TimeSeriesPage mefBlock = mefStreamer.getNextBlocks(1).get(
					0);
			if (previousEndTimeUUTC == -1) {
				expectedStartTimesUUTC.add(Long.valueOf(mefBlock.timeStart));
			} else {
				if (mefBlock.timeStart - previousEndTimeUUTC > mefStreamer
						.getSamplingPeriodMicros()) {
					expectedStartTimesUUTC
							.add(Long.valueOf(mefBlock.timeStart));
				}
			}
			previousEndTimeUUTC = mefBlock.timeEnd;
		}
		mefStreamer.close();

		final String outPrefix = "testStartTimeOutput";
		testStartTimes = new Mef2Edf(newArrayList(mefPath),
				outPrefix + ".edf");
		testStartTimes.main();
		final File outDir = new File(testStartTimes.getOutDir());
		final String[] outFileNames = outDir.list();
		assertNotNull(outFileNames);
		assertEquals(expectedStartTimesUUTC.size(), outFileNames.length);
		for (int fileNo = 0; fileNo < expectedStartTimesUUTC.size(); fileNo++) {
			final String outFile = testStartTimes.getOutDir()
					+ "/"
					+ outPrefix
					+ "_"
					+ fileNo
					+ ".edf";
			EDFReader edfReader = new EDFReader(outFile);
			final String actualStartDate = edfReader.getHeader().getStartDate();
			final String actualStartTime = edfReader.getHeader().getStartTime();

			final Date expectedStartDate = testStartTimes.uutcToEDFStartDate(
							expectedStartTimesUUTC.get(fileNo));

			final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			final String expectedStartDateString = dateFormat
					.format(expectedStartDate);
			assertEquals("Start dates failed on out file " + outFile,
					expectedStartDateString, actualStartDate);

			final SimpleDateFormat timeFormat = new SimpleDateFormat("HH.mm.ss");
			timeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			final String expectedStartTimeString = timeFormat
					.format(expectedStartDate);
			assertEquals("Start times failed on out file " + outFile,
					expectedStartTimeString, actualStartTime);
		}

		

	}

	@AfterClass
	public static void tearDownAfterClass() throws IOException {
		System.out.println("out dir; " + test.getOutDir());
		FileUtils.deleteDirectory(new File(test.getOutDir()));
		if (testStartTimes != null) {
			FileUtils.deleteDirectory(new File(testStartTimes.getOutDir()));
		}
	}

}
