/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ieeg.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;

/**
 * Prints a report on the continuous segments of the input MEF file
 * 
 * @author John Frommeyer
 *
 */
public class MEFSegmentPrinter {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						false)) {
			TimeSeriesPage previousBlock = null;
			int segmentNo = 0;
			int blockNo = 1;
			final double samplingPeriodMicros = streamer
					.getSamplingPeriodMicros();
			final long recordingStartTimeUutc = streamer
					.getRecordingStartTimeUutc();
			TimeSeriesPage currSegmentStartBlock = null;
			int currSegmentStartBlockNo = 0;
			int currSegmentLength = 0;
			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {
					if (previousBlock == null) {
						currSegmentStartBlock = block;
						currSegmentStartBlockNo = blockNo;
						currSegmentLength++;
						segmentNo++;
					} else {
						final long gapMicros = block.timeStart
								- previousBlock.timeEnd;
						if (gapMicros < samplingPeriodMicros) {
							currSegmentLength++;
						} else {
							System.out.println(
									getSegmentReportString(
											segmentNo,
											currSegmentStartBlock,
											currSegmentStartBlockNo,
											previousBlock,
											currSegmentLength,
											currSegmentStartBlock.timeStart
													- recordingStartTimeUutc));
							currSegmentStartBlock = block;
							currSegmentStartBlockNo = blockNo;
							currSegmentLength = 1;
							segmentNo++;
						}
					}
					previousBlock = block;
					blockNo++;
				}
			}
			if (previousBlock != null) {
				System.out.println(
						getSegmentReportString(
								segmentNo,
								currSegmentStartBlock,
								currSegmentStartBlockNo,
								previousBlock,
								currSegmentLength,
								currSegmentStartBlock.timeStart
										- recordingStartTimeUutc));
			}
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}

	/**
	 * DOCUMENT ME
	 * 
	 * @param blockNo
	 * @param previousEndUutc
	 * @param startUutc
	 * @param gapMicros
	 */
	private static String getSegmentReportString(
			int segmentNo,
			TimeSeriesPage startBlock,
			int startBlockNo,
			TimeSeriesPage endBlock,
			int segmentLength,
			long startOffsetMicros) {
		final DateTime endDateTime = uutcToDateTime(endBlock.timeEnd);
		final DateTime startDateTime = uutcToDateTime(startBlock.timeStart);
		long segmentMicros = endBlock.timeEnd - startBlock.timeStart;
		return "Segment "
				+ (segmentNo)
				+ ": start: "
				+ startDateTime
				+ " ("
				+ startBlock.timeStart
				+ "), end: "
				+ endDateTime
				+ " ("
				+ endBlock.timeEnd
				+ "), length: "
				+ segmentMicros
				+ " usec ("
				+ segmentMicros / 1e6
				+ ") sec, block length: "
				+ (segmentLength)
				+ ", block "
				+ startBlockNo
				+ " through block "
				+ (startBlockNo + segmentLength - 1)
				+ ", starting at "
				+ startOffsetMicros
				+ " usec ("
				+ startOffsetMicros / 1e6
				+ " sec) from beginning of file";
	}

	private static DateTime uutcToDateTime(long uutc) {
		final long mutc = TimeUnit.MILLISECONDS.convert(
				uutc,
				TimeUnit.MICROSECONDS);
		final DateTime dateTime = new DateTime(mutc,
				DateTimeZone.UTC);
		return dateTime;
	}
}
