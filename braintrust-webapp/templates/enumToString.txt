ENUM

private final String displayString;

	/**
	 * Maps a {@code Xxx}'s toString representation to the {@code Xxx} itself.
	 */
	public static final SortedMap<String, Xxx> fromString;
	static {
		final SortedMap<String, Xxx> temp = newTreeMap();
		for (final Xxx xxx : values()) {
			temp.put(xxx.toString(), xxx);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	private Xxx(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
	
PRESENTER

	/** The display strings for all xxx choices. */
	private static final String[] XXX_CHOICES = Xxx.fromString
			.keySet().toArray(new String[Xxx.values().length]);
