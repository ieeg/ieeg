/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static edu.upenn.cis.braintrust.client.presenter.StringToValue.toNonNegativeLong;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * A text field for entering dates in uUTC format.
 * 
 * @author John Frommeyer
 * 
 */
public class UutcDateWidget extends Composite implements
		ValueChangeHandler<String>, HasEnabled {

	private static UutcDateWidgetUiBinder uiBinder = GWT
			.create(UutcDateWidgetUiBinder.class);

	interface UutcDateWidgetUiBinder extends UiBinder<Widget, UutcDateWidget> {}

	@UiField
	TextBox uutcDate;

	@UiField
	Label dateLabel;

	@UiField
	Label errorMessage;

	private final static String noDateMessage = "No date set.";

	public UutcDateWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		uutcDate.setText("");
		uutcDate.setFocus(true);
		dateLabel.setText(noDateMessage);
		errorMessage.setText("");
		uutcDate.addValueChangeHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String uutc = uutcDate.getValue();
		final Long uutcLong = toNonNegativeLong(uutc);
		if (uutcLong == null) {
			dateLabel.setText(noDateMessage);
		} else {
			long utcLong = TimeUnit.MILLISECONDS.convert(uutcLong.longValue(), TimeUnit.MICROSECONDS);
			Date date = new Date(utcLong);
			String description = DateTimeFormat.getLongDateTimeFormat().format(date);
			dateLabel.setText(description);
		}
	}

	@Override
	public boolean isEnabled() {
		return uutcDate.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		uutcDate.setEnabled(enabled);
	}

}
