/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.presenter.ManagedEnumPresenter.IManagedEnumView;
import edu.upenn.cis.braintrust.shared.dto.IManagedEnum;

public class ManagedEnumDeleter<T extends IManagedEnum> implements
		FieldUpdater<T, String> {
	private final IManagedEnumView<T> display;
	private final IManagedEnumService<T> service;

	public ManagedEnumDeleter(IManagedEnumView<T> display,
			IManagedEnumService<T> service) {
		this.display = checkNotNull(display);
		this.service = checkNotNull(service);
	}

	@Override
	public void update(int index, final T object, String value) {
		display.getModifyValueErrorMessage().hideErrorMessage();
		service.deleteValue(object,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Could not delete "
								+ object.getLabel() + ": "
								+ caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						display.getValueList().remove(object);
						display.refresh();
					}
				});

	}

}
