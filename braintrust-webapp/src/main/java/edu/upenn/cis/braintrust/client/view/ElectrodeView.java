/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView;
import edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView;
import edu.upenn.cis.braintrust.client.presenter.IHasEnabledAndClickHandlers;
import edu.upenn.cis.braintrust.client.presenter.IHasValue;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * A dialog box for array installations
 * 
 * @author John Frommeyer
 * 
 */
public class ElectrodeView extends Composite implements IContactGroupView {

	private static ElectrodeViewUiBinder uiBinder = GWT
			.create(ElectrodeViewUiBinder.class);

	interface ElectrodeViewUiBinder extends
			UiBinder<Widget, ElectrodeView> {}

	@UiField
	CommonElectrodeView commonElectrodeView;

	@UiField
	Button tracesButton;

	@UiField
	TextBox channelPrefix;
	@UiField
	Label channelPrefixErrorMessage;
	private final IHasValueAndErrorMessage<String> channelPrefixView;

	@UiField
	DoubleBox samplingRate;
	@UiField
	Label samplingRateErrorMessage;
	private final IHasValueAndErrorMessage<Double> samplingRateView;
	@UiField
	DoubleBox lffSetting;
	@UiField
	Label lffSettingErrorMessage;
	private final IHasValueAndErrorMessage<Double> lffSettingView;

	@UiField
	DoubleBox hffSetting;
	@UiField
	Label hffSettingErrorMessage;
	private final IHasValueAndErrorMessage<Double> hffSettingView;

	@UiField
	ListBox notchFilterListBox;
	private final IHasValue<Boolean> notchFilterView;
	@UiField
	TextBox manufacturer;
	@UiField
	TextBox modelNumber;
	@UiField
	TextBox typeBox;
	@UiField
	Label typeErrorMessage;
	private final IHasValueAndErrorMessage<String> typeView;

	@UiField
	Button saveButton;

	@UiField
	Button cancelButton;

	private final IHasEnabledAndClickHandlers okButtonView;
	private final IHasEnabledAndClickHandlers cancelButtonView;

	/**
	 * Create a dialog box for the {@code ArrayInstallationView}.
	 * 
	 */
	public ElectrodeView() {
		initWidget(uiBinder.createAndBindUi(this));
		okButtonView = new ButtonWrapper(saveButton);
		cancelButtonView = new ButtonWrapper(cancelButton);
		samplingRateView = new NonNegativeDoubleView(samplingRate,
				samplingRateErrorMessage);
		lffSettingView = new DoubleBoxWithError(lffSetting,
				lffSettingErrorMessage);

		hffSettingView = new DoubleBoxWithError(hffSetting,
				hffSettingErrorMessage);
		typeView = new TextBoxWithError(typeBox, typeErrorMessage);

		samplingRate.setText("");
		lffSetting.setText("");

		hffSetting.setText("");

		notchFilterView = new BooleanListBoxView(notchFilterListBox);

		channelPrefix.setText("");
		channelPrefixErrorMessage.setText("");
		channelPrefixErrorMessage.setVisible(false);
		channelPrefixView = new TextBoxWithError(channelPrefix,
				channelPrefixErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getCommonElectrodeView()
	 */
	@Override
	public ICommonElectrodeView getCommonElectrodeView() {
		return commonElectrodeView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getCancelButtonView()
	 */
	@Override
	public IHasEnabledAndClickHandlers getCancelButtonView() {
		return cancelButtonView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getSaveButtonView()
	 */
	@Override
	public IHasEnabledAndClickHandlers getSaveButtonView() {
		return okButtonView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ArrayInstallationPresenter.IArrayInstallationView#getDataFileCountView()
	 */
	// @Override
	// public IHasValueWithErrorMessageView<String> getDataFileCountView() {
	// return dataFileCountView;
	// }

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ArrayInstallationPresenter.IArrayInstallationView#getFileSizeBytesView()
	 */
	// @Override
	// public IHasValueWithErrorMessageView<String> getFileSizeBytesView() {
	// return fileSizeBytesView;
	// }

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getHffSettingView()
	 */
	@Override
	public IHasValueAndErrorMessage<Double> getHffSettingView() {
		return hffSettingView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getLffSettingView()
	 */
	@Override
	public IHasValueAndErrorMessage<Double> getLffSettingView() {
		return lffSettingView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getNotchFilterView()
	 */
	@Override
	public IHasValue<Boolean> getNotchFilterView() {
		return notchFilterView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getSamplingRateView()
	 */
	@Override
	public IHasValueAndErrorMessage<Double> getSamplingRateView() {
		return samplingRateView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView#getChannelPrefixView()
	 */
	@Override
	public IHasValueAndErrorMessage<String> getChannelPrefixView() {
		return channelPrefixView;
	}

	@Override
	public void setUnmodifiable() {
		channelPrefix.setEnabled(false);
		hffSetting.setEnabled(false);
		lffSetting.setEnabled(false);
		notchFilterListBox.setEnabled(false);
		samplingRate.setEnabled(false);
		saveButton.setEnabled(false);
	}

	@Override
	public HasClickHandlers getTracesButton() {
		return tracesButton;
	}

	@Override
	public void setTracesButtonActive(boolean active) {
		tracesButton.setVisible(active);
		tracesButton.setEnabled(active);
	}

	@Override
	public HasValue<String> getManufacturerView() {
		return manufacturer;
	}

	@Override
	public HasValue<String> getModelNoView() {
		return modelNumber;
	}

	@Override
	public IHasValueAndErrorMessage<String> getElectrodeTypeView() {
		return typeView;
	}

}
