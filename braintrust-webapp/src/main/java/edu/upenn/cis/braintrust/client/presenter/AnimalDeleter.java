/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;

public class AnimalDeleter implements
		FieldUpdater<GwtAnimal, String> {
	private final HandlerManager eventBus;
	private final IAnimalTable display;
	private final BrainTrustServiceAsync service;

	public AnimalDeleter(HandlerManager eventBus, IAnimalTable display,
			BrainTrustServiceAsync service) {
		this.eventBus = checkNotNull(eventBus);
		this.display = checkNotNull(display);
		this.service = checkNotNull(service);
	}

	@Override
	public void update(int index, final GwtAnimal object, String value) {
		display.getModifyEntryErrorMessage().hideErrorMessage();
		service.deleteAnimal(object,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Could not delete "
								+ object.getLabel() + ": "
								+ caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						eventBus.fireEvent(new DataEntryMenuEvent());
					}
				});

	}

}
