/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.BooleanWithUnknownUtil;
import edu.upenn.cis.braintrust.client.presenter.IContactGroupTable;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;

/**
 * A table for electrodes
 * 
 * @author John Frommeyer
 * 
 */
public class ContactGroupTableWidget extends Composite implements
		IContactGroupTable {
	private static ContactGroupTableWidgetUiBinder uiBinder = GWT
			.create(ContactGroupTableWidgetUiBinder.class);

	interface ContactGroupTableWidgetUiBinder extends
			UiBinder<Widget, ContactGroupTableWidget> {}

	// Experiment
	@UiField(provided = true)
	DataEntryTableAndPagerWidget<GwtContactGroup> contactGroupTableWidget;
	@UiField
	Button addEntryButton;

	private static final GlobalCss css;

	private final EditTextCell channelPrefixCell = new EditTextCell();
	private final Column<GwtContactGroup, String> channelPrefixColumn = new Column<GwtContactGroup, String>(
			channelPrefixCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getChannelPrefix();
		}
	};

	private final EditTextCell electrodeTypeCell = new EditTextCell();
	private final Column<GwtContactGroup, String> electrodeTypeColumn = new Column<GwtContactGroup, String>(
			electrodeTypeCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getElectrodeType();
		}
	};

	private final Column<GwtContactGroup, String> manufacturerColumn = new Column<GwtContactGroup, String>(
			new EditTextCell()) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getManufacturer().isPresent() ? object
					.getManufacturer()
					.get() : "";
		}
	};

	private final Column<GwtContactGroup, String> modelNoColumn = new Column<GwtContactGroup, String>(
			new EditTextCell()) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getModelNo().isPresent() ? object.getModelNo()
					.get() : "";
		}
	};

	private final Column<GwtContactGroup, String> notchFilterColumn = new Column<GwtContactGroup, String>(
			new SelectionCell(
					ImmutableList
							.copyOf(BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN))) {

		@Override
		public String getValue(GwtContactGroup object) {
			return BooleanWithUnknownUtil
					.valueToString(object.getNotchFilter());
		}
	};

	private final Column<GwtContactGroup, String> modifyTracesColumn = new Column<GwtContactGroup, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtContactGroup object) {
			final String label = "modify channels";
			return label;
		}
	};;

	private final Column<GwtContactGroup, String> sideColumn;

	private final Column<GwtContactGroup, String> locationColumn;

	private final EditTextCell samplingRateCell = new EditTextCell();
	private final Column<GwtContactGroup, String> samplingRateColumn = new Column<GwtContactGroup, String>(
			samplingRateCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getSamplingRate().toString();
		}
	};

	private final EditTextCell lffCell = new EditTextCell();
	private final Column<GwtContactGroup, String> lffColumn = new Column<GwtContactGroup, String>(
			lffCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getLffSetting().isPresent() ? object.getLffSetting()
					.get().toString() : "";
		}
	};

	private final EditTextCell impedanceCell = new EditTextCell();
	private final Column<GwtContactGroup, String> impedanceColumn = new Column<GwtContactGroup, String>(
			impedanceCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getImpedance().isPresent() ? object.getImpedance()
					.get().toString() : "";
		}
	};

	private final EditTextCell hffCell = new EditTextCell();
	private final Column<GwtContactGroup, String> hffColumn = new Column<GwtContactGroup, String>(
			hffCell) {

		@Override
		public String getValue(GwtContactGroup object) {
			return object.getHffSetting().isPresent() ? object.getHffSetting()
					.get().toString() : "";
		}
	};

	private final Column<GwtContactGroup, String> deleteColumn = new Column<GwtContactGroup, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtContactGroup object) {
			return "x";
		}
	};

	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public ContactGroupTableWidget(List<String> sides,
			List<String> locations) {
		// Experiment
		final CellTable<GwtContactGroup> contactGroupTable = new CellTable<GwtContactGroup>(
				3,
				KeyProviderMap.getMap().getProvider(GwtContactGroup.class));
		contactGroupTable.setWidth("100%", false);

		sideColumn = new Column<GwtContactGroup, String>(
				new SelectionCell(sides)) {

			@Override
			public String getValue(GwtContactGroup object) {
				return object.getSide().toString();
			}
		};
		locationColumn = new Column<GwtContactGroup, String>(new SelectionCell(
				locations
				)) {

			@Override
			public String getValue(GwtContactGroup object) {
				return object.getLocation().toString();
			}

		};
		contactGroupTable.addColumn(channelPrefixColumn, "Channel Prefix");
		contactGroupTable.addColumn(electrodeTypeColumn, "Type");
		contactGroupTable.addColumn(samplingRateColumn,
				"Sampling Rate (Hz)");
		contactGroupTable.addColumn(modifyTracesColumn);
		contactGroupTable.addColumn(sideColumn, "Side");
		contactGroupTable.addColumn(locationColumn, "Location");
		contactGroupTable.addColumn(lffColumn, "LFF Setting");
		contactGroupTable.addColumn(hffColumn, "HFF Setting");
		contactGroupTable.addColumn(notchFilterColumn, "Notch Filter");
		contactGroupTable.addColumn(impedanceColumn, "Impedence");
		contactGroupTable.addColumn(manufacturerColumn, "Manufacturer");
		contactGroupTable.addColumn(modelNoColumn, "Model No.");
		contactGroupTable.addColumn(deleteColumn, "Delete");
		contactGroupTableWidget = new DataEntryTableAndPagerWidget<GwtContactGroup>(
				"Contact Groups", contactGroupTable);

		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public HasClickHandlers getAddEntryButton() {
		return addEntryButton;
	}

	@Override
	public List<GwtContactGroup> getEntryList() {
		return contactGroupTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		contactGroupTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		contactGroupTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return contactGroupTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setContactGroupDeleter(FieldUpdater<GwtContactGroup, String> deleter) {
		deleteColumn.setFieldUpdater(deleter);
	}

	@Override
	public void setChannelPrefixUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		channelPrefixColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearChannelPrefixView(GwtContactGroup electrode) {
		channelPrefixCell.clearViewData(electrode.getId());
	}

	@Override
	public void setElectrodeTypeUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		electrodeTypeColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearElectrodeTypeView(GwtContactGroup electrode) {
		electrodeTypeCell.clearViewData(electrode.getId());
	}

	@Override
	public void setTracesModifer(
			FieldUpdater<GwtContactGroup, String> updater) {
		modifyTracesColumn.setFieldUpdater(updater);
	}

	@Override
	public void setManufacturerUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		manufacturerColumn.setFieldUpdater(updater);
	}

	@Override
	public void setModelNoUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		modelNoColumn.setFieldUpdater(updater);
	}

	@Override
	public void setSamplingRateUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		samplingRateColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearSamplingRateView(GwtContactGroup electrode) {
		samplingRateCell.clearViewData(electrode.getId());
	}

	@Override
	public void setLffUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		lffColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearLffView(GwtContactGroup electrode) {
		lffCell.clearViewData(electrode.getId());
	}

	@Override
	public void setHffUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		hffColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearHffView(GwtContactGroup experiment) {
		hffCell.clearViewData(experiment.getId());
	}

	@Override
	public void setSideUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		sideColumn.setFieldUpdater(updater);
	}

	@Override
	public void setLocationUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		locationColumn.setFieldUpdater(updater);
	}

	@Override
	public void setNotchFilterUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		notchFilterColumn.setFieldUpdater(updater);
	}

	@Override
	public void setImpedanceUpdater(
			FieldUpdater<GwtContactGroup, String> updater) {
		impedanceColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearImpedanceView(GwtContactGroup electrode) {
		impedanceCell.clearViewData(electrode.getId());
	}

}
