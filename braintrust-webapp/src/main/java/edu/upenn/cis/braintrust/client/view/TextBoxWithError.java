/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

public class TextBoxWithError implements IHasValueAndErrorMessage<String> {

	private final TextBox textBox;
	private final Label textBoxErrorMessage;

	public TextBoxWithError(final TextBox textBox,
			final Label textBoxErrorMessage) {
		this.textBox = textBox;
		this.textBoxErrorMessage = textBoxErrorMessage;
	}

	@Override
	public HandlerRegistration addKeyPressHandler(final KeyPressHandler handler) {
		return textBox.addKeyPressHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return textBox.addKeyDownHandler(handler);
	}

	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(textBoxErrorMessage);
	}

	@Override
	public void setAndShowErrorMessage(final String errorMessage) {
		ViewUtil.setAndShowErrorMessage(textBoxErrorMessage,
				errorMessage);
	}

	@Override
	public void setValue(String value) {
		textBox.setValue(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#getValue()
	 */
	@Override
	public String getValue() {
		return textBox.getValue();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == textBox;
	}

}
