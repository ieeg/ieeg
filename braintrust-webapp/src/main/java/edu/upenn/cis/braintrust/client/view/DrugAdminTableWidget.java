/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.DrugAdminPresenter.IDrugAdminView;
import edu.upenn.cis.braintrust.client.presenter.IDrugAdminTable;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;

/**
 * A table for drug admins
 * 
 * @author John Frommeyer
 * 
 */
public class DrugAdminTableWidget extends Composite implements
		IDrugAdminTable {

	private static DrugAdminTableWidgetUiBinder uiBinder = GWT
			.create(DrugAdminTableWidgetUiBinder.class);

	interface DrugAdminTableWidgetUiBinder extends
			UiBinder<Widget, DrugAdminTableWidget> {}

	// Experiment
	@UiField(provided = true)
	DataEntryTableAndPagerWidget<GwtDrugAdmin> drugAdminTableWidget;

	private static final GlobalCss css;

	private final Column<GwtDrugAdmin, Date> adminTimeColumn = new Column<GwtDrugAdmin, Date>(
			new DateCell(IDrugAdminView.ADMIN_TIME_FORMAT)) {

		@Override
		public Date getValue(GwtDrugAdmin object) {
			return object.getAdminTime() == null ? null : object.getAdminTime();
		}
	};

	private final Column<GwtDrugAdmin, String> drugColumn;
	private final EditTextCell doseCell = new EditTextCell();
	private final Column<GwtDrugAdmin, String> doseColumn = new Column<GwtDrugAdmin, String>(
			doseCell) {

		@Override
		public String getValue(GwtDrugAdmin object) {
			return object.getDoseMgPerKg() == null ? "" : object
					.getDoseMgPerKg().toString();
		}
	};

	private final Column<GwtDrugAdmin, String> deleteColumn = new Column<GwtDrugAdmin, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtDrugAdmin object) {
			return "x";
		}
	};

	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public DrugAdminTableWidget(List<String> drugs) {
		// Experiment
		final CellTable<GwtDrugAdmin> electrodeTable = new CellTable<GwtDrugAdmin>(
				4,
				KeyProviderMap.getMap().getProvider(GwtDrugAdmin.class));
				//GwtDrugAdmin.KEY_PROVIDER);
		electrodeTable.setWidth("100%", false);

		drugColumn = new Column<GwtDrugAdmin, String>(
				new SelectionCell(drugs)) {

			@Override
			public String getValue(GwtDrugAdmin object) {
				return object.getDrug().getLabel();
			}
		};
		electrodeTable.addColumn(adminTimeColumn, "Time");
		electrodeTable.addColumn(drugColumn, "Drug");
		electrodeTable.addColumn(doseColumn, "Dose (mg/kg)");
		electrodeTable.addColumn(deleteColumn, "Delete");
		drugAdminTableWidget = new DataEntryTableAndPagerWidget<GwtDrugAdmin>(
				"", electrodeTable);

		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public List<GwtDrugAdmin> getEntryList() {
		return drugAdminTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		drugAdminTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		drugAdminTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return drugAdminTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setDeleter(FieldUpdater<GwtDrugAdmin, String> deleter) {
		deleteColumn.setFieldUpdater(deleter);
	}

	@Override
	public void setDrugUpdater(
			FieldUpdater<GwtDrugAdmin, String> updater) {
		drugColumn.setFieldUpdater(updater);
	}

	@Override
	public void setDoseUpdater(FieldUpdater<GwtDrugAdmin, String> updater) {
		doseColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearDoseView(GwtDrugAdmin drugAdmin) {
		doseCell.clearViewData(drugAdmin.getId());
	}

}
