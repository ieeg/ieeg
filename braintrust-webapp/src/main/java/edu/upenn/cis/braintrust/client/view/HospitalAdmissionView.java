/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView;
import edu.upenn.cis.braintrust.client.presenter.IHasEnabledAndClickHandlers;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * A screen for hospital admissions.
 * 
 * @author John Frommeyer
 * 
 */
public class HospitalAdmissionView extends Composite implements
		IHospitalAdmissionView {

	private static HospitalAdmissionViewUiBinder uiBinder = GWT
			.create(HospitalAdmissionViewUiBinder.class);

	interface HospitalAdmissionViewUiBinder extends
			UiBinder<Widget, HospitalAdmissionView> {}

	@UiField
	IntegerBox ageAtAdmission;
	@UiField
	Label ageAtAdmissionErrorMessage;
	private final IHasValueAndErrorMessage<Integer> ageAtAdmissionView;
	@UiField
	FlexTable studyTable;
	@UiField
	Button addStudyButton;
	private final IHasEnabledAndClickHandlers addStudyButtonView;
	
	@UiField
	Label addStudyErrorMessage;

	@UiField
	Button saveButton;
	private final IHasEnabledAndClickHandlers saveButtonView;

	@UiField
	Button cancelButton;
	private final IHasEnabledAndClickHandlers cancelButtonView;

	private final List<Long> studyIds = newArrayList();
	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}
	public HospitalAdmissionView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		ageAtAdmission.setText("");
		ageAtAdmissionErrorMessage.setText("");
		ageAtAdmissionErrorMessage.setVisible(false);
		ageAtAdmissionView = new IntegerBoxWithError(ageAtAdmission,
				ageAtAdmissionErrorMessage);

		// Study panel
		studyTable.getRowFormatter().addStyleName(0, css.flexTableHeader());
		studyTable.getColumnFormatter()
				.addStyleName(2, css.flexTableButtonColumn());
		studyTable.setText(0, 0, "Study");
		studyTable.setText(0, 1, "Edit");
		studyTable.setText(0, 2, "Remove");
		addStudyErrorMessage.setVisible(false);
		saveButtonView = new ButtonWrapper(saveButton);
		cancelButtonView = new ButtonWrapper(cancelButton);
		addStudyButtonView = new ButtonWrapper(addStudyButton);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#addStudy(java.lang.Long,
	 *      java.lang.String)
	 */
	@Override
	public EditRemoveButtonPair addStudy(Long id, String description, boolean studyIsModifiable) {
		if (studyIds.contains(id)) {
			return null;
		}
		studyIds.add(id);
		int row = studyTable.getRowCount();
		studyTable.setText(row, 0, description);
		Button remove = new Button("x");
		if (!studyIsModifiable) {
			remove.setEnabled(false);
			remove.setTitle("Contains unremovable time series.");
		}
		remove.addStyleDependentName(css.gwtButtonRemove());
		Button edit = new Button("Edit");
		studyTable.setWidget(row, 1, edit);
		studyTable.setWidget(row, 2, remove);
		return new EditRemoveButtonPair(edit, remove);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#getAddStudyButton()
	 */
	@Override
	public IHasEnabledAndClickHandlers getAddStudyButton() {
		return addStudyButtonView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#getAgeAtAdmissionView()
	 */
	@Override
	public IHasValueAndErrorMessage<Integer> getAgeAtAdmissionView() {
		return ageAtAdmissionView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#getCancelButton()
	 */
	@Override
	public IHasEnabledAndClickHandlers getCancelButton() {
		return cancelButtonView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#getSaveButton()
	 */
	@Override
	public IHasEnabledAndClickHandlers getSaveButton() {
		return saveButtonView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#hideAddStudyErrorMessage()
	 */
	@Override
	public void hideAddStudyErrorMessage() {
		ViewUtil.hideErrorMessage(addStudyErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#removeStudy(java.lang.Long)
	 */
	@Override
	public void removeStudy(Long studyId) {
	int arrayRow = studyIds.indexOf(studyId);
		studyIds.remove(arrayRow);
		studyTable.removeRow(arrayRow + 1);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView#setAndShowAddStudyErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowAddStudyErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(addStudyErrorMessage, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

}
