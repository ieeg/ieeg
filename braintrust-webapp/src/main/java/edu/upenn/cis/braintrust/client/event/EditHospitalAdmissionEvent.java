/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;

/**
 * Fired when user edits a GwtHospitalAdmission.
 * 
 * @author John Frommeyer
 * 
 */
public class EditHospitalAdmissionEvent extends GwtEvent<EditHospitalAdmissionEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<EditHospitalAdmissionEventHandler> TYPE = new Type<EditHospitalAdmissionEventHandler>();

	private final GwtHospitalAdmission admission;

	/**
	 * Creates edit study event.
	 * 
	 * @param study the study to be edited.
	 */
	public EditHospitalAdmissionEvent(final GwtHospitalAdmission study) {
		this.admission = study;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(EditHospitalAdmissionEventHandler handler) {
		handler.onEditHospitalAdmission(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditHospitalAdmissionEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * @return the study
	 */
	public GwtHospitalAdmission getHospitalAdmission() {
		return admission;
	}
}
