/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

public class ExperimentStimDurationValidator implements
		IExperimentFieldValidator<String> {

	@Override
	public boolean isValidFieldValue(String fieldValue) {
		if ("".equals(fieldValue.trim())) {
			// This will be interpreted as null since this is a nullable field.
			return true;
		}
		try {
			Integer.parseInt(fieldValue);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	@Override
	public void clearFieldView(IExperimentTable display,
			GwtExperiment experiment) {
		display.clearExperimentStimDurationView(experiment);
		display.redrawTable();
	}

}
