/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.common.base.Optional;

/**
 * Methods for dealing with "unknown", "True", "False" fields.
 * 
 * @author John Frommeyer
 * 
 */
public final class BooleanWithUnknownUtil {

	private BooleanWithUnknownUtil() {
		throw new AssertionError("Class should not be instantiated.");
	}

	public static final String[] BOOLEAN_WITH_UNKNOWN = new String[] {
			"False",
			"True",
			"Unknown" };

	public static String valueToString(Optional<Boolean> value) {
		final Boolean notchFilter = value.orNull();
		if (notchFilter == null) {
			return BOOLEAN_WITH_UNKNOWN[2];
		} else if (notchFilter == Boolean.TRUE) {
			return BOOLEAN_WITH_UNKNOWN[1];
		} else {
			return BOOLEAN_WITH_UNKNOWN[0];
		}
	}

	public static Optional<Boolean> stringToValue(String str) {
		if (str.equals(BOOLEAN_WITH_UNKNOWN[2])) {
			return Optional.absent();
		} else if (str.equals(BOOLEAN_WITH_UNKNOWN[1])) {
			return Optional.of(Boolean.TRUE);
		} else if (str.equals(BOOLEAN_WITH_UNKNOWN[0])) {
			return Optional.of(Boolean.FALSE);
		} else {
			throw new IllegalArgumentException("Illegal value: [" + str + "]");
		}
	}

	public static int booleanToIndex(Boolean value) {
		if (value == null) {
			return 2;
		} else if (value == Boolean.TRUE) {
			return 1;
		} else {
			return 0;
		}
	}
}
