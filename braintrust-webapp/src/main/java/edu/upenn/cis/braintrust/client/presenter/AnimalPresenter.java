/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Lists.newArrayList;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.CreateExperimentEvent;
import edu.upenn.cis.braintrust.client.presenter.DrugAdminPresenter.IDrugAdminView;
import edu.upenn.cis.braintrust.client.presenter.RecordingPresenter.IRecordingView;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;

/**
 * Controls a GwtAnimal view.
 * 
 * @author John Frommeyer
 * 
 */
public class AnimalPresenter implements IPresenter,
		ClickHandler, IHasAnimal, IHasRecordingParent<GwtExperiment> {

	/**
	 * Views for this presenter should implement this interface.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IAnimalView extends IView {
		IAnimalTable getAnimalTable();

		IRecordingView getRecordingView();

		IExperimentTable getExperimentTable();

		IDrugAdminView getDrugAdminView();

	}

	private final HandlerManager eventBus;
	private final IAnimalView display;
	private final BrainTrustServiceAsync service;
	private final List<GwtSpecies> speciesList;
	private final List<GwtDrug> drugList;
	private final List<GwtStimRegion> stimRegionList;
	private final List<GwtStimType> stimTypeList;
	private final RecordingPresenter recordingPresenter;
	private final DrugAdminPresenter drugAdminPresenter;

	private final AnimalLabelUpdater animalLabelUpdater;
	private final AnimalOrganizationUpdater animalOrganizationUpdater;
	private final AnimalStrainUpdater animalStrainUpdater;
	private final AnimalDeleter animalDeleter;
	private final FieldUpdater<GwtExperiment, String> experimentLabelUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentAgeUpdater;
	private final FieldUpdater<GwtExperiment, Boolean> experimentIsPublishedUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentStimRegionUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentStimTypeUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentStimDurationUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentStimIsiUpdater;
	private final FieldUpdater<GwtExperiment, String> experimentDeleter;

	/**
	 * Create a {@code AnimalPresenter}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 * @param organizationList TODO
	 * @param pair
	 * @param arrayInfoStore
	 */
	public AnimalPresenter(
			HandlerManager eventBus,
			IAnimalView display,
			BrainTrustServiceAsync service,
			List<GwtOrganization> organizationList,
			List<GwtSpecies> speciesList,
			List<GwtDrug> drugList,
			List<GwtStimRegion> stimRegionList,
			List<GwtStimType> stimTypeList, 
			GwtAnimalExperimentPair pair) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = service;
		this.speciesList = speciesList;
		this.drugList = drugList;
		this.stimRegionList = stimRegionList;
		this.stimTypeList = stimTypeList;
		recordingPresenter = new RecordingPresenter(
				this.eventBus,
				this.display,
				this.service,
				this);
		drugAdminPresenter = new DrugAdminPresenter(
				this.eventBus,
				this.display.getDrugAdminView(),
				this.service,
				drugList);
		animalLabelUpdater = new AnimalLabelUpdater(
				this.display.getAnimalTable(),
				this.service);
		animalOrganizationUpdater = new AnimalOrganizationUpdater(
				this.display.getAnimalTable(),
				this.service, 
				organizationList);
		animalStrainUpdater = new AnimalStrainUpdater(
				this.display.getAnimalTable(), 
				this.service, 
				this.speciesList);
		animalDeleter = new AnimalDeleter(
				this.eventBus,
				this.display.getAnimalTable(), 
				this.service);

		experimentAgeUpdater = new ExperimentFieldUpdater<String>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromAge(),
				IExperimentFieldValidator.noValidationString,
				this);
		experimentLabelUpdater = new ExperimentFieldUpdater<String>(
				display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromLabel(),
				new ExperimentLabelValidator(),
				this);
		experimentIsPublishedUpdater = new ExperimentFieldUpdater<Boolean>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromIsPublished(),
				IExperimentFieldValidator.noValidationBoolean,
				this);
		experimentStimRegionUpdater = new ExperimentFieldUpdater<String>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromStimRegion(this.stimRegionList),
				IExperimentFieldValidator.noValidationString,
				this);
		experimentStimTypeUpdater = new ExperimentFieldUpdater<String>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromStimType(this.stimTypeList),
				IExperimentFieldValidator.noValidationString,
				this);
		experimentStimDurationUpdater = new ExperimentFieldUpdater<String>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromStimDuration(),
				new ExperimentStimDurationValidator(),
				this);
		experimentStimIsiUpdater = new ExperimentFieldUpdater<String>(
				this.display.getExperimentTable(),
				this.service,
				new ExperimentFactoryFromStimIsi(),
				new ExperimentStimIsiValidator(),
				this);
		experimentDeleter = new ExperimentDeleter<String>(
				this.display.getExperimentTable(),
				this.service,
				this);
		bind();
		populateDisplay(pair.getAnimal(), pair.getExperiments());

	}

	private void bind() {
		display.getExperimentTable().getAddEntryButton()
				.addClickHandler(this);
		display.getRecordingView().getContactGroupTable().getAddEntryButton()
				.addClickHandler(this);
		display.getExperimentTable().addExperimentSelectionHandler(
				recordingPresenter);
		display.getExperimentTable().addExperimentSelectionHandler(
				drugAdminPresenter);
		display.getAnimalTable().setAnimalLabelUpdater(animalLabelUpdater);
		display.getAnimalTable().setAnimalOrganizationUpdater(
				animalOrganizationUpdater);
		display.getAnimalTable().setAnimalStrainUpdater(animalStrainUpdater);
		display.getAnimalTable().setAnimalDeleter(animalDeleter);
		display.getExperimentTable().setExperimentAgeUpdater(
				experimentAgeUpdater);
		display.getExperimentTable().setExperimentDeleter(experimentDeleter);
		display.getExperimentTable().setExperimentIsPublishedUpdater(
				experimentIsPublishedUpdater);
		display.getExperimentTable().setExperimentLabelUpdater(
				experimentLabelUpdater);
		display.getExperimentTable().setExperimentStimDurationUpdater(
				experimentStimDurationUpdater);
		display.getExperimentTable().setExperimentStimIsiUpdater(
				experimentStimIsiUpdater);
		display.getExperimentTable().setExperimentStimRegionUpdater(
				experimentStimRegionUpdater);
		display.getExperimentTable().setExperimentStimTypeUpdater(
				experimentStimTypeUpdater);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getExperimentTable().getAddEntryButton()) {
			eventBus.fireEvent(new CreateExperimentEvent(getAnimal(), display));
		} else if (source == display.getRecordingView().getContactGroupTable()
				.getAddEntryButton()) {

		}
	}

	private void populateDisplay(GwtAnimal animal,
			ImmutableList<GwtExperiment> initalExperiments) {
		display.getAnimalTable().setAnimal(animal);
		display.getExperimentTable().getEntryList()
				.addAll(initalExperiments);
		display.getExperimentTable().refreshProvider();
	}

	@Override
	public void updateRecording(
			GwtRecording newRecording) {
		final GwtExperiment currentExperiment = getRecordingParent(newRecording)
				.orNull();
		if (currentExperiment == null) {
			Window.alert("Could not update recording " + newRecording.getDir());
		} else {
			// First update the experiment so that the recording is correct the
			// next time it is selected.
			final GwtExperiment modifedExperiment = new GwtExperiment(
					currentExperiment.getLabel(),
					newRecording,
					currentExperiment.isPublished(),
					currentExperiment.getAge(),
					currentExperiment.getStimType(),
					currentExperiment.getStimRegion(),
					currentExperiment.getStimIsiMs(),
					currentExperiment.getStimDurationMs(),
					currentExperiment.getId(),
					currentExperiment.getVersion());
			display.getExperimentTable().getEntryList()
					.remove(currentExperiment);
			display.getExperimentTable().getEntryList().add(modifedExperiment);
			Collections.sort(display.getExperimentTable().getEntryList(),
					GwtExperiment.LABEL_COMPARATOR);
			display.getExperimentTable().refreshProvider();

			// Also update the Recording view itself so that it is up to date
			display.getRecordingView().getRecordingTable()
					.setRecording(newRecording);
			List<GwtContactGroup> tempContactGroups = newArrayList(display
					.getRecordingView().getContactGroupTable().getEntryList());
			for (final GwtContactGroup electrode : tempContactGroups) {
				display.getRecordingView().getContactGroupTable().getEntryList()
						.remove(electrode);
			}
			display.getRecordingView().getContactGroupTable().getEntryList()
					.addAll(newRecording.getContactGroups());
			Collections.sort(display.getRecordingView().getContactGroupTable()
					.getEntryList(), GwtContactGroup.CHANNEL_PREFIX_COMPARATOR);
			display.getRecordingView().getContactGroupTable().refreshProvider();
		}
	}

	@Override
	public Optional<GwtExperiment> getRecordingParent(
			GwtRecording recordingChild) {
		for (final GwtExperiment experiment : display.getExperimentTable()
				.getEntryList()) {
			if (experiment.getRecording().getId()
					.equals(recordingChild.getId())) {
				return Optional.of(experiment);
			}
		}
		return Optional.absent();
	}

	@Override
	public GwtAnimal getAnimal() {
		final int animalCount = display.getAnimalTable().getEntryList().size();
		if (animalCount == 0 || animalCount > 1) {
			Window.alert("An error has occured in AnimalPresenter. Please report. Animal count: "
					+ animalCount);
		}
		return display.getAnimalTable().getEntryList().get(0);
	}
}
