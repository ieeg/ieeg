/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;

import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

public interface IExperimentTable extends IDataEntryTable<GwtExperiment>,
		IHasAddEntryButton {

	void setExperimentDeleter(FieldUpdater<GwtExperiment, String> deleter);

	void setExperimentLabelUpdater(FieldUpdater<GwtExperiment, String> updater);

	void clearExperimentLabelView(GwtExperiment experiment);

	void setExperimentAgeUpdater(FieldUpdater<GwtExperiment, String> updater);

	void setExperimentStimDurationUpdater(
			FieldUpdater<GwtExperiment, String> updater);

	void clearExperimentStimDurationView(GwtExperiment experiment);

	void setExperimentStimIsiUpdater(FieldUpdater<GwtExperiment, String> updater);

	void clearExperimentStimIsiView(GwtExperiment experiment);

	void setExperimentStimRegionUpdater(
			FieldUpdater<GwtExperiment, String> updater);

	void setExperimentStimTypeUpdater(
			FieldUpdater<GwtExperiment, String> updater);

	void setExperimentIsPublishedUpdater(
			FieldUpdater<GwtExperiment, Boolean> updater);

	void addExperimentSelectionHandler(
			IExperimentSelectionHandler handler);

}