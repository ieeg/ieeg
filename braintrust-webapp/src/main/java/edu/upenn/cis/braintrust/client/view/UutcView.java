/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static edu.upenn.cis.braintrust.client.presenter.StringToValue.toNonNegativeLong;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * A view for a {@code UutcWidget}.
 * 
 * @author John Frommeyer
 * 
 */
public class UutcView implements IHasValueAndErrorMessage<Long> {

	private final UutcDateWidget dateWidget;

	/**
	 * DOCUMENT ME
	 * 
	 * @param dateWidget
	 */
	UutcView(UutcDateWidget dateWidget) {
		this.dateWidget = dateWidget;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#getValue()
	 */
	@Override
	public Long getValue() {
		String str = dateWidget.uutcDate.getValue();
		if (str == null) {
			return null;
		}
		return toNonNegativeLong(str.trim());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Long value) {
		dateWidget.uutcDate.setValue(
				value == null ? "" : value.toString(),
				true);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasKeyPressHandlersView#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return dateWidget.uutcDate.addKeyPressHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return dateWidget.uutcDate.addKeyDownHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == dateWidget.uutcDate;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(dateWidget.errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(dateWidget.errorMessage, errorMessage);
	}

}
