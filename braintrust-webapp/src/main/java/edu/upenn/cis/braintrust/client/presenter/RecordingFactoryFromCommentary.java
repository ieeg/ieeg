/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

public class RecordingFactoryFromCommentary implements
		IRecordingFactory<String> {
	private final ContactGroupCopier copier = new ContactGroupCopier();

	@Override
	public GwtRecording newInstance(GwtRecording originalRecording,
			String newValue) {
		final String newCommentary = newValue;
		final GwtRecording newRecording = new GwtRecording(
				originalRecording.getDir(),
				originalRecording.getMefDir(),
				originalRecording.getStartTimeUutc(),
				originalRecording.getEndTimeUutc(),
				originalRecording.getRefElectrodeDescription().orNull(),
				originalRecording.getImagesFile(),
				originalRecording.getReportFile(),
				newCommentary,
				originalRecording.getId(),
				originalRecording.getVersion());
		copier.copyContactGroups(originalRecording, newRecording);
		return newRecording;
	}

}
