/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.List;

import com.google.common.base.Functions;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;

public class AnimalOrganizationUpdater implements
		FieldUpdater<GwtAnimal, String> {
	private final IAnimalTable display;
	private final BrainTrustServiceAsync service;
	private final List<GwtOrganization> organizationList;

	public AnimalOrganizationUpdater(
			IAnimalTable display,
			BrainTrustServiceAsync service,
			List<GwtOrganization> organizationList) {
		this.display = checkNotNull(display);
		this.service = checkNotNull(service);
		this.organizationList = checkNotNull(organizationList);
	}

	@Override
	public void update(final int index, final GwtAnimal object, String value) {
		display.getModifyEntryErrorMessage()
				.hideErrorMessage();
		final GwtOrganization origOrg = object.getOrganization();
		final GwtOrganization newOrg = tryFind(organizationList,
				compose(equalTo(value), Functions.toStringFunction())).orNull();
		if (newOrg == null) {
			Window.alert("Could not find " + value + " in organization list.");
			return;
		} else if (origOrg.equals(newOrg)) {
			return;
		} else {
			final GwtAnimal modifiedValue = new GwtAnimal(
					object.getLabel(),
					newOrg,
					object.getStrain(),
					object.getId(),
					object.getVersion());
			service.modifyAnimal(modifiedValue,
					new AsyncCallback<GwtAnimal>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("An error occurred while attempting to modify value "
									+ origOrg);
							display.redrawTable();
						}

						@Override
						public void onSuccess(GwtAnimal result) {
							display.setAnimal(result);
							display.refreshProvider();

						}
					});
		}
	}

}
