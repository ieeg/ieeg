/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.ExperimentFromFileKeysPresenter.IExperimentFromFileKeysView;
import edu.upenn.cis.braintrust.client.presenter.IHasEnabledAndClickHandlers;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;

/**
 * The main data entry menu
 * 
 * @author John Frommeyer
 * 
 */
public class ExperimentFromFileKeysView extends Composite
		implements IExperimentFromFileKeysView {

	private static ExperimentFromFileKeysViewUiBinder uiBinder = GWT
			.create(ExperimentFromFileKeysViewUiBinder.class);

	interface ExperimentFromFileKeysViewUiBinder extends
			UiBinder<Widget, ExperimentFromFileKeysView> {}

	@UiField
	VerticalPanel inputPanel;

	@UiField
	SpeciesStrainWidget speciesStrain;

	@UiField
	TextBox mefDirBox;
	@UiField
	Label mefDirErrorMessage;
	private final IHasValueAndErrorMessage<String> mefDirView;

	@UiField
	Button fileKeyLookupButton;
	private final ButtonWrapper fileKeyLookupButtonWrapper;

	@UiField
	Button createExperimentButton;
	private final ButtonWrapper createExperimentButtonWrapper;

	@UiField
	HTML createExperimentErrorMessage;

	@UiField
	TextArea fileKeyTextArea;

	@UiField
	VerticalPanel resultsPanel;
	@UiField
	HTML createExperimentResultsMessage;
	@UiField
	Button goAgainButton;
	@UiField
	Button editAnimalButton;

	/**
	 * Create a new menu screen.
	 * 
	 */
	public ExperimentFromFileKeysView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		fileKeyTextArea.setCharacterWidth(150);
		fileKeyTextArea.setVisibleLines(25);
		mefDirBox.setVisibleLength(45);
		mefDirView = new TextBoxWithError(mefDirBox, mefDirErrorMessage);
		fileKeyLookupButtonWrapper = new ButtonWrapper(fileKeyLookupButton);
		createExperimentButtonWrapper = new ButtonWrapper(
				createExperimentButton);
	}

	@Override
	public IHasEnabledAndClickHandlers getCreateExperimentButton() {
		return createExperimentButtonWrapper;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void setAndShowCreateExperimentErrorMessage(String string) {
		ViewUtil.setAndShowErrorMessage(createExperimentErrorMessage, string);
	}

	@Override
	public ISingleSelectionList getSpeciesView() {
		return speciesStrain.getSpeciesView();
	}

	@Override
	public ISingleSelectionList getStrainView() {
		return speciesStrain.getStrainView();
	}

	@Override
	public void hideCreateExperimentErrorMessage() {
		ViewUtil.hideErrorMessage(createExperimentErrorMessage);
	}

	@Override
	public HasValue<String> getFileKeys() {
		return fileKeyTextArea;
	}

	@Override
	public void setAndShowCreateExperimentErrorMessageList(
			List<String> stringList) {
		final StringBuilder sb = new StringBuilder();
		for (final String error : stringList) {
			sb.append(SafeHtmlUtils.htmlEscape(error));
			sb.append("<br />");
		}
		createExperimentErrorMessage.setHTML(sb.toString());
		createExperimentErrorMessage.setVisible(true);
	}

	@Override
	public HandlerRegistration addFileKeyKeyDownHandler(KeyDownHandler handler) {
		return fileKeyTextArea.addKeyDownHandler(handler);
	}

	@Override
	public HasHTML getCreateExperimentResultsMessage() {
		return createExperimentResultsMessage;
	}

	@Override
	public void hideInputShowResults() {
		inputPanel.setVisible(false);
		goAgainButton.setEnabled(true);
		editAnimalButton.setEnabled(true);
		resultsPanel.setVisible(true);
	}

	@Override
	public HasClickHandlers getGoAgainButton() {
		return goAgainButton;
	}

	@Override
	public HasClickHandlers getEditAnimalButton() {
		return editAnimalButton;
	}

	@Override
	public void setEditAnimalButtonText(String text) {
		editAnimalButton.setText(text);
	}

	@Override
	public void showInputHideResults() {
		resultsPanel.setVisible(false);
		goAgainButton.setEnabled(false);
		editAnimalButton.setEnabled(false);
		createExperimentButton.setEnabled(true);
		inputPanel.setVisible(true);
	}

	@Override
	public IHasEnabledAndClickHandlers getFileKeyLookupButton() {
		return fileKeyLookupButtonWrapper;
	}

	@Override
	public IHasValueAndErrorMessage<String> getMefDir() {
		return mefDirView;
	}
}
