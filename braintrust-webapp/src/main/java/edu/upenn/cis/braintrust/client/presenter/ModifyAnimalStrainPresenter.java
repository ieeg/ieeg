/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class ModifyAnimalStrainPresenter implements IPresenter, ClickHandler,
		ChangeHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IModifyAnimalStrainView extends IView, IHasSaveButton,
			IHasCancelButton, IDialogBox {
		ISingleSelectionList getSpeciesView();

		ISingleSelectionList getStrainView();
	}

	private final IModifyAnimalStrainView display;
	private final BrainTrustServiceAsync service;
	private final GwtAnimal parent;
	private final IAnimalTable callbackView;
	private final List<GwtSpecies> speciesList;
	private List<GwtStrain> strainList;

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param display
	 * @param rpcService
	 * @param speciesList TODO
	 * @param eventBus
	 * @param electrodeInfoStore
	 * @param experiment
	 */
	public ModifyAnimalStrainPresenter(
			final IModifyAnimalStrainView display,
			final BrainTrustServiceAsync rpcService,
			final GwtAnimal parent,
			List<GwtSpecies> speciesList,
			IAnimalTable callbackView) {
		this.display = display;
		this.service = rpcService;
		this.parent = parent;
		this.callbackView = callbackView;
		this.speciesList = speciesList;
		bind();
		populateDisplay();
	}

	private void populateDisplay() {
		String[] speciesChoices = new String[speciesList.size()];
		int i = 0;
		int selectedIndex = -1;
		for (final GwtSpecies species : speciesList) {
			speciesChoices[i] = species.getLabel();
			if (selectedIndex == -1
					&& parent.getStrain().getSpeciesId()
							.equals(species.getId())) {
				selectedIndex = i;
			}
			i++;
		}
		if (selectedIndex == -1) {
			selectedIndex = 0;
		}
		display.getSpeciesView().setChoices(speciesChoices);
		if (speciesChoices.length > 0) {
			display.getSpeciesView().setSelectedIndex(selectedIndex);
			// Fire event to trigger onChange() to load the related strains.
			display.getSpeciesView().fireChangeEvent();
		}
	}

	private void bind() {
		display.getSaveButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);
		display.getSpeciesView().addChangeHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		display.show();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getSaveButton()) {
			onSave();
		} else if (source == display.getCancelButton()) {
			display.hide();
		}
	}

	private void onSave() {
		final int selectedIndex = display.getStrainView().getSelectedIndex();
		final GwtStrain newStrain = strainList.get(selectedIndex);
		if (newStrain.getId().equals(parent.getStrain().getId())) {
			return;
		} else {
			final GwtAnimal modifedAnimal = new GwtAnimal(
					parent.getLabel(),
					parent.getOrganization(),
					newStrain,
					parent.getId(),
					parent.getVersion());
			service.modifyAnimal(modifedAnimal, new AsyncCallback<GwtAnimal>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Unable to save animal with the new species/strain: "
							+ newStrain.getDisplayLabel());
				}

				@Override
				public void onSuccess(GwtAnimal result) {
					callbackView.setAnimal(result);
					callbackView.refreshProvider();
					display.hide();
				}
			});

		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		final ISingleSelectionList strainView = display
				.getStrainView();
		strainView.clear();
		final int i = display.getSpeciesView().getSelectedIndex();
		if (i < 0 || i >= speciesList.size()) {
			Window
					.alert("There are no species loaded.");
		} else {
			final GwtSpecies species = speciesList.get(i);
			if (species == null) {
				Window.alert("Species not found.");
			} else {
				service.findStrains(species,
						new AsyncCallback<List<GwtStrain>>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Unable to retrieve strains for species "
										+ species.getLabel());
							}

							@Override
							public void onSuccess(List<GwtStrain> result) {
								strainList = result;
								final ISingleSelectionList strainView = display
										.getStrainView();
								final String[] strainChoices = new String[result
										.size()];
								int i = 0;
								int selectedIndex = 0;
								for (final GwtStrain strain : result) {
									strainChoices[i] = strain.getLabel();
									if (parent.getStrain().getId()
											.equals(strain.getId())) {
										selectedIndex = i;
									}
									i++;
								}
								strainView.setChoices(strainChoices);
								if (strainChoices.length > 0) {
									strainView.setSelectedIndex(selectedIndex);
								}
							}
						});
			}

		}

	}

}
