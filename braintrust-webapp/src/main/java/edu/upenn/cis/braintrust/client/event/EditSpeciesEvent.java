/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;

/**
 * Fired when an existing Species needs to be edited.
 * 
 * @author John Frommeyer
 * 
 */
public class EditSpeciesEvent extends GwtEvent<EditSpeciesEventHandler> {

	public static final Type<EditSpeciesEventHandler> TYPE = new Type<EditSpeciesEventHandler>();

	private final GwtSpecies species;
	private final List<GwtStrain> strains;

	/**
	 * Creates an {@code EditSpeciesEvent} for the given species.
	 * 
	 * @param species
	 */
	public EditSpeciesEvent(final GwtSpecies species,
			final List<GwtStrain> strains) {
		this.species = species;
		this.strains = strains;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(EditSpeciesEventHandler handler) {
		handler.onEditSpecies(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditSpeciesEventHandler> getAssociatedType() {
		return TYPE;
	}

	public GwtSpecies getSpecies() {
		return species;
	}

	public List<GwtStrain> getStrains() {
		return strains;
	}

}
