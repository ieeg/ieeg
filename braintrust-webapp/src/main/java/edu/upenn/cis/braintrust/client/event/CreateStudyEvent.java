/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;

/**
 * Fired when a study needs to be created.
 * 
 * @author John Frommeyer
 * 
 */
public class CreateStudyEvent extends GwtEvent<CreateStudyEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<CreateStudyEventHandler> TYPE = new Type<CreateStudyEventHandler>();
	private final GwtHospitalAdmission admission;
	
	/**
	 * Creates an {@code CreateStudyEvent} for the given patient.
	 * 
	 * @param hospitalAdmission
	 */
	public CreateStudyEvent(final GwtHospitalAdmission hospitalAdmission) {
		this.admission = hospitalAdmission;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreateStudyEventHandler handler) {
		handler.onCreateStudy(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateStudyEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Returns the {@code GwtHospitalAdmission} for which the study will be created.
	 * @return the {@code GwtHospitalAdmission}
	 */
	public GwtHospitalAdmission getHospitalAdmission() {
		return admission;
	}
}
