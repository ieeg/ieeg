/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.nullToEmpty;

import java.util.Collections;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter.IAnimalView;
import edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter.IContactGroupView;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class AddContactGroupPresenter implements IPresenter, ClickHandler,
		KeyPressHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IAddContactGroupView extends IView, IDialogBox {
		public IContactGroupView getContactGroupView();

	}

	private final IAddContactGroupView display;
	private final BrainTrustServiceAsync service;
	private final GwtRecording parent;
	private final GwtContactGroup contactGroup;
	private final CommonElectrodePresenter commonElectrodePresenter;
	private final IAnimalView callbackView;

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param electrodeInfoStore
	 * @param experiment
	 */
	public AddContactGroupPresenter(
			final IAddContactGroupView display,
			final BrainTrustServiceAsync rpcService,
			final GwtRecording parent,
			IAnimalView callbackView) {
		this.display = display;
		this.service = rpcService;
		this.parent = parent;
		this.contactGroup = new GwtContactGroup();
		this.callbackView = callbackView;
		this.commonElectrodePresenter = new CommonElectrodePresenter(
				this.display.getContactGroupView().getCommonElectrodeView(),
				this.contactGroup,
				ListChoices.SIDE_CHOICES,
				ListChoices.LOCATION_CHOICES,
				true);
		bind();
	}

	private void bind() {
		display.getContactGroupView().getSaveButtonView().addClickHandler(this);
		display.getContactGroupView().getCancelButtonView().addClickHandler(this);
		commonElectrodePresenter.bind();
		display.getContactGroupView().getHffSettingView().addKeyPressHandler(this);

		display.getContactGroupView().getLffSettingView().addKeyPressHandler(this);

		display.getContactGroupView().getSamplingRateView()
				.addKeyPressHandler(this);
		display.getContactGroupView().getChannelPrefixView()
				.addKeyPressHandler(this);

		display.getContactGroupView().getElectrodeTypeView()
				.addKeyPressHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		display.show();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (display.getContactGroupView().getSaveButtonView().isWrapping(source)) {
			onSave();
		} else if (display.getContactGroupView().getCancelButtonView()
				.isWrapping(source)) {
			display.hide();
		}
	}

	private void onSave() {
		boolean canSave = commonElectrodePresenter.populateModel();
		final String channelPrefix = nullToEmpty(
				display.getContactGroupView().getChannelPrefixView().getValue())
				.trim();
		if ("".equals(channelPrefix)) {
			canSave = false;
			display.getContactGroupView().getChannelPrefixView()
					.setAndShowErrorMessage(
							"You must enter a channel prefix.");
		}
		final Double hffSetting = display.getContactGroupView()
				.getHffSettingView()
				.getValue();
		final Double lffSetting = display.getContactGroupView()
				.getLffSettingView()
				.getValue();
		final Boolean isNotchFilter = display.getContactGroupView()
				.getNotchFilterView()
				.getValue();
		final String manufacturer = emptyToNull(display.getContactGroupView()
				.getManufacturerView().getValue());
		final String modelNo = emptyToNull(display.getContactGroupView()
				.getModelNoView().getValue());

		final String electrodeType = nullToEmpty(display.getContactGroupView()
				.getElectrodeTypeView().getValue());
		if ("".equals(electrodeType)) {
			canSave = false;
			display.getContactGroupView()
					.getElectrodeTypeView()
					.setAndShowErrorMessage("You must enter an electrode type.");
		}

		final Double samplingRate = display.getContactGroupView()
				.getSamplingRateView()
				.getValue();
		if (samplingRate == null) {
			canSave = false;
			display.getContactGroupView()
					.getSamplingRateView()
					.setAndShowErrorMessage(
							"You must enter a nonnegative value for sampling rate.");
		}
		if (canSave) {
			contactGroup.setChannelPrefix(channelPrefix);
			contactGroup.setElectrodeType(electrodeType);
			contactGroup.setManufacturer(manufacturer);
			contactGroup.setModelNo(modelNo);
			contactGroup.setHffSetting(hffSetting);

			contactGroup.setLffSetting(lffSetting);

			contactGroup.setNotchFilter(isNotchFilter);
			contactGroup.setSamplingRate(samplingRate);
			service.createContactGroup(parent, contactGroup,
					new AsyncCallback<GwtContactGroup>() {

						@Override
						public void onFailure(Throwable caught) {
							display.hide();
							Window.alert("Could not create electrode "
									+ channelPrefix);
						}

						@Override
						public void onSuccess(GwtContactGroup result) {
							display.hide();
							parent.addContactGroup(result);
							final IContactGroupTable electrodeTable = callbackView
									.getRecordingView().getContactGroupTable();
							electrodeTable.getEntryList().add(result);
							Collections.sort(electrodeTable.getEntryList(),
									GwtContactGroup.CHANNEL_PREFIX_COMPARATOR);
							electrodeTable.refreshProvider();
						}
					});
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		final Object source = event.getSource();
		if (display.getContactGroupView().getHffSettingView().isWrapping(source)) {
			display.getContactGroupView().getHffSettingView().hideErrorMessage();
		} else if (display.getContactGroupView().getLffSettingView()
				.isWrapping(source)) {
			display.getContactGroupView().getLffSettingView().hideErrorMessage();
		} else if (display.getContactGroupView().getSamplingRateView()
				.isWrapping(source)) {
			display.getContactGroupView().getSamplingRateView().hideErrorMessage();
		} else if (display.getContactGroupView().getChannelPrefixView()
				.isWrapping(source)) {
			display.getContactGroupView().getChannelPrefixView()
					.hideErrorMessage();
		} else if (display.getContactGroupView().getElectrodeTypeView()
				.isWrapping(source)) {
			display.getContactGroupView().getElectrodeTypeView()
					.hideErrorMessage();
		}
	}
}
