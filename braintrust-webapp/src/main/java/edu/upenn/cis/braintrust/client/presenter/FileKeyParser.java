/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Splitter;
import com.google.gwt.regexp.shared.RegExp;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class FileKeyParser {

	private final List<String> fileKeys = newArrayList();
	private final String requiredFileKeyPrefix;

	private final RegExp orgComponentRegExp;
	private final String orgComponentFormat;

	private final RegExp subjectComponentRegExp;
	private final String subjectComponentFormat;

	private final RegExp datasetComponentRegExp;
	private final String datasetComponentFormat;

	private String orgComponent;
	private String subjectComponent;
	private String datasetComponent;
	private boolean validationComplete = false;

	private final Splitter fileKeySplitter = Splitter.on("/");
	private static final String defaultChannelGroupPrefix = "DEFAULT";

	/**
	 * 
	 * @param requiredFileKeyPrefix
	 * @param orgComponentRegExp
	 * @param orgComponentFormat
	 * @param subjectComponentRegExp
	 * @param subjectComponentFormat
	 * @param datasetComponentRegEx
	 * @param datasetComponentFormat
	 */
	public FileKeyParser(
			List<String> fileKeys,
			String requiredFileKeyPrefix,
			RegExp orgComponentRegExp,
			String orgComponentFormat,
			RegExp subjectComponentRegExp,
			String subjectComponentFormat,
			RegExp datasetComponentRegExp,
			String datasetComponentFormat) {
		this.fileKeys.addAll(fileKeys);
		this.requiredFileKeyPrefix = checkNotNull(requiredFileKeyPrefix);
		this.orgComponentRegExp = checkNotNull(orgComponentRegExp);
		this.orgComponentFormat = checkNotNull(orgComponentFormat);
		this.subjectComponentRegExp = checkNotNull(subjectComponentRegExp);
		this.subjectComponentFormat = checkNotNull(subjectComponentFormat);
		this.datasetComponentRegExp = checkNotNull(datasetComponentRegExp);
		this.datasetComponentFormat = checkNotNull(datasetComponentFormat);
	}

	/**
	 * Processes fileKeys and sets this parser's {@code orgComponent} to the
	 * common organization component of the given file keys,
	 * {@code subjectComponent} to the common subject component,
	 * {@code datasetComponent} and to the common dataset component if possible.
	 * 
	 * 
	 * @throws FileKeyParserException if fileKeys is empty or if the file keys
	 *             do not have the correct format
	 */
	private void validate() throws FileKeyParserException {
		if (fileKeys.size() == 0) {
			throw new FileKeyParserException(
					Collections
							.singletonList("Must have at least one file key"));
		}

		final Set<String> orgComponents = newHashSet();
		final Set<String> subjectComponents = newHashSet();
		final Set<String> datasetComponents = newHashSet();
		final Set<String> mefFilenames = newHashSet();
		final List<String> validationErrors = newArrayList();
		for (final String fileKey : fileKeys) {
			final List<String> fileKeyComponents = fileKeySplitter
					.splitToList(fileKey);
			if (fileKeyComponents.size() != 6) {
				final int componentCount = fileKeyComponents.size();
				validationErrors
						.add(fileKey
								+ " has "
								+ componentCount
								+ " path component"
								+ (componentCount == 1 ? "" : "s")
								+ ", but 6 are required. The required format is: "
								+ requiredFileKeyPrefix
								+ "/<organization component>/<subject component>/<dataset component>/mef/<mef filename ending in '.mef'>");

			} else {
				final String prefix = fileKeyComponents.get(0);
				if (!prefix.equals(requiredFileKeyPrefix)) {
					validationErrors.add(fileKey + " does not start with "
							+ requiredFileKeyPrefix);
				}
				final String fileKeyOrgComponent = fileKeyComponents.get(1);
				if (orgComponentRegExp.test(fileKeyOrgComponent)) {
					orgComponents.add(fileKeyOrgComponent);
				} else {
					validationErrors.add("The organization component '"
							+ fileKeyOrgComponent
							+ "' of file key " + fileKey
							+ " does not have the required format: '"
							+ orgComponentFormat + "'");
				}
				final String fileKeySubjectComponent = fileKeyComponents.get(2);
				if (subjectComponentRegExp.test(fileKeySubjectComponent)) {
					subjectComponents.add(fileKeySubjectComponent);
				} else {
					validationErrors.add("The subject component '"
							+ fileKeySubjectComponent
							+ "' of file key " + fileKey
							+ " does not have the required format: '"
							+ subjectComponentFormat + "'");
				}
				final String fileKeyDatasetComponent = fileKeyComponents.get(3);
				if (datasetComponentRegExp.test(fileKeyDatasetComponent)) {
					datasetComponents.add(fileKeyDatasetComponent);
				} else {
					validationErrors.add("The dataset component '"
							+ fileKeyDatasetComponent
							+ "' of file key " + fileKey
							+ " does not have the required format: '"
							+ datasetComponentFormat + "'");
				}
				final String mefDirComponent = fileKeyComponents.get(4);
				if (!"mef".equals(mefDirComponent)) {
					validationErrors.add("The MEF directory component '"
							+ mefDirComponent
							+ "' of file key " + fileKey
							+ " does not have the required format: 'mef'");
				}
				final String mefFilename = fileKeyComponents.get(5);
				if (mefFilename.endsWith(".mef")) {
					mefFilenames.add(mefFilename);
				} else {
					validationErrors.add("The MEF filename component '"
							+ mefFilename
							+ "' of file key " + fileKey
							+ " does not end in .mef");
				}
			}

		}
		if (orgComponents.size() == 0) {
			validationErrors
					.add("No file key had a valid organization component");
		} else if (orgComponents.size() > 1) {
			validationErrors
					.add("All organization components must match. Found : "
							+ orgComponents);
		} else {
			this.orgComponent = getOnlyElement(orgComponents);
		}

		if (subjectComponents.size() == 0) {
			validationErrors.add("No file key had a valid subject component");
		} else if (subjectComponents.size() > 1) {
			validationErrors.add("All subject components must match. Found : "
					+ subjectComponents);
		} else {
			this.subjectComponent = getOnlyElement(subjectComponents);
		}

		if (datasetComponents.size() == 0) {
			validationErrors.add("No file key had a valid dataset component");
		} else if (datasetComponents.size() > 1) {
			validationErrors.add("All dataset components must match. Found : "
					+ datasetComponents);
		} else {
			this.datasetComponent = getOnlyElement(datasetComponents);
		}

		if (mefFilenames.size() != fileKeys.size()
				&& validationErrors.size() == 0) {
			validationErrors.add("The number of file keys: " + fileKeys.size()
					+ " does not match the number of unique filenames: "
					+ mefFilenames.size());
		}

		if (validationErrors.size() == 0) {
			this.validationComplete = true;
		} else {
			throw new FileKeyParserException(validationErrors);
		}

	}

	/**
	 * @return the orgComponent
	 */
	public String getOrgComponent() {
		checkState(
				validationComplete,
				"File keys have not been parsed. Call parse() first.");
		return orgComponent;
	}

	/**
	 * @return the subjectComponent
	 */
	public String getSubjectComponent() {
		checkState(
				validationComplete,
				"File keys have not been parsed. Call validate() parse.");
		return subjectComponent;
	}

	/**
	 * @return the datasetComponent
	 */
	public String getDatasetComponent() {
		checkState(
				validationComplete,
				"File keys have not been parsed. Call parse() first.");
		return datasetComponent;
	}

	public String getDir() {
		checkState(
				validationComplete,
				"File keys have not been parsed. Call parse() first.");
		return requiredFileKeyPrefix + "/" + orgComponent + "/"
				+ subjectComponent + "/" + datasetComponent;
	}

	public String getMefDir() {
		return "mef";
	}

	public Map<String, Set<GwtTrace>> parse()
			throws FileKeyParserException {
		validate();
		final Map<String, Set<GwtTrace>> channelPrefixToTraces = createContactGroups();
		return channelPrefixToTraces;

	}

	private Map<String, Set<GwtTrace>> createContactGroups() {
		checkState(
				validationComplete,
				"File keys have not been parsed. Call parse() first.");
		final Map<String, Set<GwtTrace>> channelPrefixToTraces = newHashMap();
		for (final String fileKey : fileKeys) {
			final List<String> fileKeyParts = fileKeySplitter
					.splitToList(fileKey);
			final String mefFilename = fileKeyParts.get(5);
			final String channelLabel = mefFilename.substring(
					0,
					mefFilename.lastIndexOf('.'));
			final int underscoreIdx = channelLabel.indexOf('_');
			final String channelPrefix = underscoreIdx == -1 ? defaultChannelGroupPrefix
					: channelLabel.substring(0, underscoreIdx);
			Set<GwtTrace> traceSet = channelPrefixToTraces.get(channelPrefix);
			if (traceSet == null) {
				traceSet = newHashSet();
				channelPrefixToTraces.put(channelPrefix, traceSet);
			}
			final GwtTrace trace = new GwtTrace(
					ContactType.MACRO,
					channelLabel,
					fileKey,
					null,
					null,
					null,
					null);
			traceSet.add(trace);
		}
		return channelPrefixToTraces;
	}

}
