/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static edu.upenn.cis.braintrust.client.view.ViewUtil.hideErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setAndShowErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setListBoxChoices;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView;

/**
 * The fields common to both {@code GwtElectrode} and {@code GwtReferenceElectrode}.
 * 
 * @author John Frommeyer
 * 
 */
public class CommonElectrodeView extends Composite implements
		ICommonElectrodeView {
	private static CommonElectrodeViewUiBinder uiBinder = GWT
			.create(CommonElectrodeViewUiBinder.class);

	interface CommonElectrodeViewUiBinder extends
			UiBinder<Widget, CommonElectrodeView> {}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#addImpedanceKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addImpedanceKeyPressHandler(
			KeyPressHandler handler) {
		return impedance.addKeyPressHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#getImpedance()
	 */
	@Override
	public HasValue<Double> getImpedance() {
		return impedance;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#getSelectedLocation()
	 */
	@Override
	public int getSelectedLocation() {
		return location.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#getSelectedSide()
	 */
	@Override
	public int getSelectedSide() {
		return side.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#hideImpedanceErrorMessage()
	 */
	@Override
	public void hideImpedanceErrorMessage() {
		hideErrorMessage(impedanceErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setAndShowImpedanceErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowImpedanceErrorMessage(String errorMessage) {
		setAndShowErrorMessage(impedanceErrorMessage, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setImpedanceEnabled(boolean)
	 */
	@Override
	public void setImpedanceEnabled(boolean enabled) {
		impedance.setEnabled(enabled);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setLocationChoices(java.lang.String[])
	 */
	@Override
	public void setLocationChoices(String[] lobes) {
		setListBoxChoices(location, lobes);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setSelectedLocation(int)
	 */
	@Override
	public void setSelectedLocation(int index) {
		location.setSelectedIndex(index);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setSelectedSide(int)
	 */
	@Override
	public void setSelectedSide(int index) {
		side.setSelectedIndex(index);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView#setSideChoices(java.lang.String[])
	 */
	@Override
	public void setSideChoices(String[] sides) {
		setListBoxChoices(side, sides);
	}

	@UiField
	ListBox side;
	

	@UiField
	ListBox location;
	

	@UiField
	DoubleBox impedance;


	@UiField
	Label impedanceErrorMessage;

	/**
	 * Create a {@code CommonElectrodeView}.
	 * 
	 */
	public CommonElectrodeView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		impedance.setText("");
		impedanceErrorMessage.setText("");
		impedanceErrorMessage.setVisible(false);
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void setUnmodifiable() {
		impedance.setEnabled(false);
		location.setEnabled(false);
		side.setEnabled(false);
	}

}
