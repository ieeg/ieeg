/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter;
import edu.upenn.cis.braintrust.client.presenter.DrugAdminPresenter.IDrugAdminView;
import edu.upenn.cis.braintrust.client.presenter.IAnimalTable;
import edu.upenn.cis.braintrust.client.presenter.IExperimentTable;
import edu.upenn.cis.braintrust.client.presenter.RecordingPresenter.IRecordingView;

/**
 * A GwtPatient screen.
 * 
 * @author John Frommeyer
 * 
 */
public class AnimalView extends Composite implements
		AnimalPresenter.IAnimalView {
	private static AnimalViewUiBinder uiBinder = GWT
			.create(AnimalViewUiBinder.class);

	interface AnimalViewUiBinder extends UiBinder<Widget, AnimalView> {}

	// Animal
	@UiField(provided = true)
	AnimalTableWidget animalWidget;

	// Experiment
	@UiField(provided = true)
	ExperimentTableWidget experimentTableWidget;

	// DrugAdmin
	@UiField(provided = true)
	DrugAdminView drugAdminView;

	// Recording
	@UiField(provided = true)
	RecordingView recordingView;

	private static final GlobalCss css;

	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public AnimalView(
			List<String> organizations,
			List<String> sides, 
			List<String> locations, 
			List<String> drugs,
			List<String> stimRegions, List<String> stimTypes) {

		animalWidget = new AnimalTableWidget(organizations);
		// Experiment
		experimentTableWidget = new ExperimentTableWidget(stimRegions,
				stimTypes);
		// Recording
		recordingView = new RecordingView(sides, locations);
		// DrugAdmin
		drugAdminView = new DrugAdminView(drugs);
		initWidget(uiBinder.createAndBindUi(this));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public IRecordingView getRecordingView() {
		return recordingView;
	}

	@Override
	public IExperimentTable getExperimentTable() {
		return experimentTableWidget;
	}

	@Override
	public IAnimalTable getAnimalTable() {
		return animalWidget;
	}

	@Override
	public IDrugAdminView getDrugAdminView() {
		return drugAdminView;
	}

}
