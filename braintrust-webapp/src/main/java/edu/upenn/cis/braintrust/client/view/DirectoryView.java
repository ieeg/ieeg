/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.base.Strings.nullToEmpty;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.removeLeadingAndTrailingSlashesAndBackSlashes;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

class DirectoryView implements IHasValueAndErrorMessage<String> {

	private final TextBox textBox;
	private final Label errorLabel;
	
	DirectoryView(final TextBox textBox, final Label errorLabel) {
		this.textBox = textBox;
		this.errorLabel = errorLabel;
	}
	
	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValue#getValue()
	 */
	@Override
	public String getValue() {
		return removeLeadingAndTrailingSlashesAndBackSlashes(nullToEmpty(
			textBox.getValue()).trim());
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValue#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(String value) {
		textBox.setValue(value, true);
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasKeyPressHandlers#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return textBox.addKeyPressHandler(handler);
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasWrappedObject#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == textBox;
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorLabel);
	}

	/**  {@inheritDoc}
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(errorLabel, errorMessage);
	}
	
	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return textBox.addKeyDownHandler(handler);
	}
	
}
