/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class DrugAdminPresenter implements IPresenter, ClickHandler,
		KeyDownHandler,
		IExperimentSelectionHandler {

	private final class DoseUpdater implements
			FieldUpdater<GwtDrugAdmin, String> {
		@Override
		public void update(int index, GwtDrugAdmin object,
				String value) {
			display.getDrugAdminTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String doseStr = value.trim();
			BigDecimal newDose = null;
			boolean doseConverted = true;
			if (!"".equals(doseStr)) {
				try {
					newDose = str2Dose(doseStr);
				} catch (NumberFormatException e) {
					doseConverted = false;
					display.getDrugAdminTable()
							.getModifyEntryErrorMessage()
							.setAndShowErrorMessage(
									"Invalid dose: " + doseStr);
					display.getDrugAdminTable().clearDoseView(
							object);
					display.getDrugAdminTable().redrawTable();

				}
			}
			if (doseConverted) {
				final GwtDrugAdmin modifedDrugAdmin = new GwtDrugAdmin(
						object.getDrug(),
						object.getAdminTime(),
						newDose,
						object.getId(),
						object.getVersion());
				service.modifyDrugAdmin(selectedExperiment,
						modifedDrugAdmin,
						new ModifyDrugAdminAsyncCallback(index,
								object));
			}
		}
	}

	private final class DrugUpdater implements
			FieldUpdater<GwtDrugAdmin, String> {
		@Override
		public void update(int index, GwtDrugAdmin object,
				String value) {
			display.getDrugAdminTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final GwtDrug newDrug = tryFind(drugList,
					compose(equalTo(value), GwtDrug.getLabel))
					.orNull();
			if (newDrug == null) {
				Window.alert("Could not change drug to " + value);
			} else {
				final GwtDrugAdmin modifiedAdmin = new GwtDrugAdmin(
						newDrug,
						object.getAdminTime(),
						object.getDoseMgPerKg(),
						object.getId(),
						object.getVersion());
				service.modifyDrugAdmin(selectedExperiment,
						modifiedAdmin,
						new ModifyDrugAdminAsyncCallback(index,
								object));
			}
		}
	}

	private final class DrugAdminDeleter implements
			FieldUpdater<GwtDrugAdmin, String> {
		@Override
		public void update(int index, final GwtDrugAdmin object,
				String value) {
			display.getDrugAdminTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			service.deleteDrugAdmin(selectedExperiment, object,
					new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("An error occurred while attempting to delete drug admin.");
						}

						@Override
						public void onSuccess(Void result) {
							display.getDrugAdminTable()
									.getEntryList().remove(object);
							display.getDrugAdminTable()
									.refreshProvider();
						}
					});
		}
	}

	private final class ModifyDrugAdminAsyncCallback implements
			AsyncCallback<GwtDrugAdmin> {
		private final int index;
		private final GwtDrugAdmin originalDrugAdmin;

		private ModifyDrugAdminAsyncCallback(int index,
				GwtDrugAdmin originalDrugAdmin) {
			this.index = index;
			this.originalDrugAdmin = originalDrugAdmin;
		}

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("An error occurred while attempting to modify drug admin.");
		}

		@Override
		public void onSuccess(GwtDrugAdmin result) {
			display.getDrugAdminTable().getEntryList()
					.set(index, result);
			Collections
					.sort(display.getDrugAdminTable()
							.getEntryList(),
							new GwtDrugAdmin.GwtDrugAdminComparator());
			display.getDrugAdminTable()
					.refreshProvider();
		}
	}

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IDrugAdminView extends IView, IHasEnabled {

		public static final DateTimeFormat ADMIN_TIME_FORMAT = DateTimeFormat
				.getFormat(PredefinedFormat.DATE_TIME_MEDIUM);

		public IDrugAdminTable getDrugAdminTable();

		/**
		 * Returns the edit button for Reference GwtElectrode.
		 * 
		 * @return the edit button for Reference GwtElectrode
		 */
		HasClickHandlers getAddDrugAdminButton();

		ISingleSelectionList getDrugList();

		HasValue<Date> getAdminTime();

		IHasValueAndErrorMessage<String> getDose();

	}

	private final IDrugAdminView display;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private final List<GwtDrug> drugList;
	private GwtExperiment selectedExperiment;

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param drugList
	 */
	public DrugAdminPresenter(final HandlerManager eventBus,
			final IDrugAdminView display,
			final BrainTrustServiceAsync rpcService,
			final List<GwtDrug> drugList) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = rpcService;
		this.drugList = drugList;

		bind();
	}

	private void bind() {
		// Keep it disabled until an experiment is selected
		display.setEnabled(false);
		display.getAddDrugAdminButton().addClickHandler(this);
		final String[] drugChoices = new String[drugList.size()];
		for (int i = 0; i < drugChoices.length; i++) {
			drugChoices[i] = drugList.get(i).getLabel();
		}
		display.getDrugList().setChoices(drugChoices);
		display.getDose().addKeyDownHandler(this);
		display.getDrugAdminTable().setDeleter(
				new DrugAdminDeleter());
		display.getDrugAdminTable().setDrugUpdater(
				new DrugUpdater());
		display.getDrugAdminTable().setDoseUpdater(
				new DoseUpdater());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getAddDrugAdminButton()) {
			onAddAdminClick();
		}

	}

	private void onAddAdminClick() {
		boolean canCreate = true;
		final Date adminDate = display.getAdminTime().getValue();
		final int selectedDrugIdx = display.getDrugList().getSelectedIndex();
		final GwtDrug drug = drugList.get(selectedDrugIdx);
		final String doseStr = display.getDose().getValue().trim();
		java.math.BigDecimal dose;
		if ("".equals(doseStr)) {
			dose = null;
		} else {
			try {
				dose = str2Dose(doseStr);
			} catch (NumberFormatException e) {
				canCreate = false;
				dose = null;
				display.getDose().setAndShowErrorMessage("Invalid dose: "
						+ doseStr);
			}
		}

		if (canCreate) {
			final GwtDrugAdmin newAdmin = new GwtDrugAdmin(
					drug,
					adminDate,
					dose,
					null,
					null);

			service.createDrugAdmin(selectedExperiment, newAdmin,
					new AsyncCallback<GwtDrugAdmin>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Could not create drug admin");
						}

						@Override
						public void onSuccess(GwtDrugAdmin result) {
							display.getDrugAdminTable().getEntryList()
									.add(result);
							Collections.sort(display.getDrugAdminTable()
									.getEntryList(),
									new GwtDrugAdmin.GwtDrugAdminComparator());
							display.getDrugAdminTable().refreshProvider();
							display.getAdminTime().setValue(null);
							if (!drugList.isEmpty()) {
								display.getDrugList().setSelectedIndex(0);
							}
							display.getDose().setValue("");
						}
					});
		}
	}

	@Override
	public void onExperimentSelection(final GwtExperiment experiment) {
		display.getAdminTime().setValue(null);
		if (!drugList.isEmpty()) {
			display.getDrugList().setSelectedIndex(0);
		}
		display.getDose().setValue("");
		display.setEnabled(true);
		selectedExperiment = experiment;
		display.getDrugAdminTable()
				.getEntryList().clear();
		service.findDrugAdmins(experiment,
				new AsyncCallback<List<GwtDrugAdmin>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Could not load drug administrations for experiment "
								+ experiment.getLabel());
					}

					@Override
					public void onSuccess(List<GwtDrugAdmin> result) {
						display.getDrugAdminTable()
								.getEntryList()
								.addAll(result);
						// Should already be in order.
						display.getDrugAdminTable().refreshProvider();

					}
				});

	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		Object source = event.getSource();
		if (display.getDose().isWrapping(source)) {
			display.getDose().hideErrorMessage();
		}
	}

	/**
	 * 
	 * Returns the dose with a scale of two and using rounding up.
	 * 
	 * @param doseStr
	 * @return the dose with a scale of two and using rounding up
	 * @throws NumberFormatException if doseStr cannot be converted to a
	 *             BigDecimal
	 */
	private BigDecimal str2Dose(String doseStr) {
		BigDecimal dose = new BigDecimal(doseStr);
		dose = dose.setScale(2, RoundingMode.HALF_UP);
		return dose;
	}
}
