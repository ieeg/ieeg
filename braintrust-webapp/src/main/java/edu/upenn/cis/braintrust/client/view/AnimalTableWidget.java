/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IAnimalTable;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;

/**
 * A table for animal experiments
 * 
 * @author John Frommeyer
 * 
 */
public class AnimalTableWidget extends Composite implements
		IAnimalTable {
	private static AnimalTableViewUiBinder uiBinder = GWT
			.create(AnimalTableViewUiBinder.class);

	interface AnimalTableViewUiBinder extends
			UiBinder<Widget, AnimalTableWidget> {}

	// Experiment
	@UiField(provided = true)
	DataEntryTableWidget<GwtAnimal> animalTableWidget;
	private final EditTextCell animalLabelCell = new EditTextCell();
	private final Column<GwtAnimal, String> animalLabelColumn = new Column<GwtAnimal, String>(
			animalLabelCell) {

		@Override
		public String getValue(GwtAnimal object) {
			return object.getLabel();
		}
	};
	private final Column<GwtAnimal, String> organizationColumn;

	private final Column<GwtAnimal, String> strainColumn = new Column<GwtAnimal, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtAnimal object) {
			return object.getStrain().getDisplayLabel();
		}
	};

	private final Column<GwtAnimal, String> deleteColumn = new Column<GwtAnimal, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtAnimal object) {
			return "x";
		}
	};

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public AnimalTableWidget(List<String> organizations) {
		// Animal
		final CellTable<GwtAnimal> animalTable = new CellTable<GwtAnimal>(1,
				KeyProviderMap.getMap().getProvider(GwtAnimal.class));
		animalTable.setWidth("20%", false);

		organizationColumn = new Column<GwtAnimal, String>(new SelectionCell(
				organizations)) {

			@Override
			public String getValue(GwtAnimal object) {
				return object.getOrganization().toString();
			}
		};
		animalTable.addColumn(animalLabelColumn, "Animal Id");
		animalTable.addColumn(organizationColumn, "Organization");
		animalTable.addColumn(strainColumn, "Species/Strain. Click to modify.");
		animalTable.addColumn(deleteColumn, "Delete animal");
		animalTableWidget = new DataEntryTableWidget<GwtAnimal>("Animal",
				animalTable);
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public List<GwtAnimal> getEntryList() {
		return animalTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		animalTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		animalTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return animalTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setAnimal(GwtAnimal animal) {
		if (animalTableWidget.getEntryList().size() == 0) {
			animalTableWidget.getEntryList().add(animal);
		} else if (animalTableWidget.getEntryList().size() == 1) {
			animalTableWidget.getEntryList().clear();
			animalTableWidget.getEntryList().add(animal);
		} else {
			Window.alert("An error has occurrred in AnimalView. Please report.");
		}
	}

	@Override
	public void setAnimalDeleter(FieldUpdater<GwtAnimal, String> animalDeleter) {
		deleteColumn.setFieldUpdater(animalDeleter);
	}

	@Override
	public void setAnimalOrganizationUpdater(
			FieldUpdater<GwtAnimal, String> organizationUpdater) {
		organizationColumn.setFieldUpdater(organizationUpdater);
	}

	@Override
	public void setAnimalLabelUpdater(
			FieldUpdater<GwtAnimal, String> labelUpdater) {
		animalLabelColumn.setFieldUpdater(labelUpdater);
	}

	@Override
	public void setAnimalStrainUpdater(
			FieldUpdater<GwtAnimal, String> strainUpdater) {
		strainColumn.setFieldUpdater(strainUpdater);
	}

	@Override
	public void clearAnimalLabelView(GwtAnimal animal) {
		animalLabelCell.clearViewData(KeyProviderMap.getMap().getProvider(GwtAnimal.class));

	}

}
