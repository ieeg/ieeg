/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static edu.upenn.cis.braintrust.client.presenter.StringToValue.toNonNegativeInteger;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * An input field for nonnegative integers and an associated error message.
 * 
 * @author John Frommeyer
 * 
 */
public class NonNegativeIntegerView implements
		IHasValueAndErrorMessage<Integer> {

	private final TextBox textBox;
	private final Label errorMessage;

	/**
	 * Creates a {@code NonNegativeIntegerView} from the given {@code TextBox}
	 * and {@code Label}.
	 * 
	 * @param textBox
	 * @param errorMessage
	 */
	NonNegativeIntegerView(final TextBox textBox,
			final Label errorMessage) {
		this.textBox = textBox;
		this.errorMessage = errorMessage;
	}

	/**
	 * {@inheritDoc} If the {@code String} value of the wrapped text box
	 * represents a nonnegative integer, then returns that value. Otherwise
	 * returns null.
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#getValue()
	 */
	@Override
	public Integer getValue() {
		final String str = textBox.getValue();
		if (str == null) {
			return null;
		}
		final Integer value = toNonNegativeInteger(str.trim());
		return value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Integer value) {
		textBox.setValue(value.toString());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasKeyPressHandlersView#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return textBox.addKeyPressHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return textBox.addKeyDownHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessageString) {
		ViewUtil.setAndShowErrorMessage(errorMessage, errorMessageString);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == textBox;
	}

}
