/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.base.Strings;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.CreateContactGroupEvent;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent;
import edu.upenn.cis.braintrust.client.event.EditContactGroupEvent;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView.EpSubtypePair;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtEpilepsySubtype;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class StudyPresenter implements IPresenter, ClickHandler,
		KeyPressHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IStudyView extends IView,
			IHasSaveButton, IHasCancelButton {
		public static final class EpSubtypePair {
			private final EpilepsyType epType;
			private final Etiology etiology;

			public EpSubtypePair(EpilepsyType epType, Etiology etiology) {
				this.epType = checkNotNull(epType);
				this.etiology = checkNotNull(etiology);
			}

			public EpilepsyType getEpilepsyType() {
				return epType;
			}

			public Etiology getEtiology() {
				return etiology;
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result
						+ ((epType == null) ? 0 : epType.hashCode());
				result = prime * result
						+ ((etiology == null) ? 0 : etiology.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj) {
					return true;
				}
				if (obj == null) {
					return false;
				}
				if (!(obj instanceof EpSubtypePair)) {
					return false;
				}
				EpSubtypePair other = (EpSubtypePair) obj;
				if (epType != other.epType) {
					return false;
				}
				if (etiology != other.etiology) {
					return false;
				}
				return true;
			}

			public String toString() {
				return "(" + epType.toString() + ", " + etiology.toString()
						+ ")";
			}

		}

		/**
		 * Returns the seizure count field.
		 * 
		 * @return the seizure count field
		 */
		HasValue<Integer> getSeizureCount();

		/**
		 * Sets and shows an error message for the seizure count field.
		 * 
		 * @param errorMessage
		 */
		void setAndShowSeizureCountErrorMessage(String errorMessage);

		/**
		 * Hides the error message for the seizure count field.
		 * 
		 */
		void hideSeizureCountErrorMessage();

		/**
		 * Add a {@code KeyPressHandler} to the seizure count field.
		 * 
		 * @param handler
		 * @return the {@code HandlerReqistration}
		 */
		HandlerRegistration addSeizureCountKeyPressHandler(
				KeyPressHandler handler);

		/**
		 * Returns the edit/remove button pair for the {@code GwtContactGroup}
		 * being added to the study.
		 * 
		 * @param id
		 * @param description
		 * @param studyIsModifiable
		 * @return the edit/remove button pair for the {@code GwtContactGroup}
		 *         being added to the study
		 */
		EditRemoveButtonPair addContactGroup(Long id, String description,
				boolean studyIsModifiable);

		/**
		 * Removes the GwtContactGroup with the given id from this view.
		 * 
		 * @param contactGroupId
		 */
		public void removeContactGroup(Long contactGroupId);

		/**
		 * Returns the add button for {@code GwtContactGroup}s.
		 * 
		 * @return the add button for {@code GwtContactGroup}s
		 */
		public HasClickHandlers getAddContactGroupButton();

		/**
		 * Sets and makes visible an error message for the add contact group
		 * panel.
		 * 
		 * @param errorMessage
		 */
		public void setAndShowAddContactGroupErrorMessage(String errorMessage);

		/**
		 * Hide the error message in the add contact group panel.
		 * 
		 */
		public void hideAddContactGroupErrorMessage();

		public IHasValueAndErrorMessage<Long> getStartDateView();

		public IHasValueAndErrorMessage<Long> getEndDateView();

		public IHasValueAndErrorMessage<String> getStudyLabelView();

		public IHasValueAndErrorMessage<String> getStudyDirView();

		public IHasValueAndErrorMessage<String> getMefDirView();

		public IHasValueAndErrorMessage<String> getReportFileView();

		public IHasValueAndErrorMessage<String> getImagesFileView();

		public IHasValueAndErrorMessage<String> getRefElectrodeDescriptionView();

		public ISingleSelectionList getStudyTypeView();

		public HasValue<Boolean> getPublishedView();

		public void setUnmodifiable();

		public void selectEpSubtype(EpSubtypePair subType);

		public Set<EpSubtypePair> getEpSubtypes();

		public boolean isEpSubtypeSelected(EpSubtypePair pair);
	}

	private final IStudyView display;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private final GwtHospitalAdmission hospitalAdmission;
	private GwtPatient patient;
	private final GwtEegStudy study;

	/**
	 * Create a {@code StudyPresenter}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 * @param parent
	 */
	public StudyPresenter(final HandlerManager eventBus,
			final IStudyView display, final BrainTrustServiceAsync service,
			final GwtHospitalAdmission parent) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = service;
		this.hospitalAdmission = parent;
		this.patient = this.hospitalAdmission.getParent();
		this.study = new GwtEegStudy();
		// This will be overwritten with real values in canSave.
		final GwtRecording blankRecording = new GwtRecording(
				"",
				"",
				Long.valueOf(0),
				Long.valueOf(0),
				null,
				null,
				null,
				null,
				null,
				null);
		this.study.setRecording(blankRecording);
		bind();

	}

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param study
	 */
	public StudyPresenter(
			final HandlerManager eventBus,
			final IStudyView display,
			final BrainTrustServiceAsync rpcService,
			final GwtEegStudy study) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = rpcService;
		this.study = study;
		this.hospitalAdmission = this.study.getParent();
		this.patient = this.hospitalAdmission.getParent();
		bind();
		populateDisplay();
	}

	private void populateDisplay() {
		final GwtRecording recording = study.getRecording();
		display.getStartDateView().setValue(recording.getStartTimeUutc());
		display.getEndDateView().setValue(recording.getEndTimeUutc());
		display.getSeizureCount().setValue(study.getSzCount().orNull());
		display.getRefElectrodeDescriptionView().setValue(
				recording.getRefElectrodeDescription().orNull());
		display.getStudyLabelView().setValue(study.getLabel());
		display.getStudyDirView().setValue(recording.getDir());
		display.getMefDirView().setValue(recording.getMefDir());
		display.getReportFileView().setValue(recording.getReportFile());
		display.getImagesFileView().setValue(recording.getImagesFile());
		display.getPublishedView().setValue(study.isPublished());
		EegStudyType studyType = study.getType();
		int studyTypeIndex = Arrays.binarySearch(
				ListChoices.EEG_STUDY_TYPE_CHOICES, studyType.toString());
		display.getStudyTypeView().setSelectedIndex(studyTypeIndex);
		final Set<GwtContactGroup> contactGroups = recording.getContactGroups();
		for (final GwtContactGroup contactGroup : contactGroups) {
			String description = contactGroup.getChannelPrefix() + ": "
					+ contactGroup.getSide().toString() + " "
					+ contactGroup.getLocation().toString();
			EditRemoveButtonPair editRemoveButtonPair = display
					.addContactGroup(
							contactGroup.getId(), description,
							study.isModifiable());
			HasClickHandlers editButton = editRemoveButtonPair.getEditButton();
			editButton.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					eventBus.fireEvent(new EditContactGroupEvent(study,
							contactGroup));
				}
			});
			HasClickHandlers removeButton = editRemoveButtonPair
					.getRemoveButton();
			removeButton.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					onRemoveContactGroup(contactGroup);
				}

			});
		}
		for (final GwtEpilepsySubtype subtype : study.getEpilepsySubtypes()) {
			final EpSubtypePair pair = new EpSubtypePair(
					subtype.getEpilepsyType(),
					subtype.getEtiology());
			display.selectEpSubtype(pair);
		}
	}

	private void onRemoveContactGroup(final GwtContactGroup contactGroup) {
		if (contactGroup.getId() == null) {
			study.getRecording().removeContactGroup(contactGroup);
		} else {
			contactGroup.setDeleted(Boolean.TRUE);
		}
		display.removeContactGroup(contactGroup.getId());
	}

	private void bind() {
		display.getSaveButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);
		display.getStartDateView().addKeyPressHandler(this);
		display.getEndDateView().addKeyPressHandler(this);
		display.addSeizureCountKeyPressHandler(this);
		display.getAddContactGroupButton().addClickHandler(this);
		display.getStartDateView().addKeyPressHandler(this);
		display.getStudyLabelView().addKeyPressHandler(this);
		display.getStudyDirView().addKeyPressHandler(this);
		display.getMefDirView().addKeyPressHandler(this);
		display.getReportFileView().addKeyPressHandler(this);
		display.getImagesFileView().addKeyPressHandler(this);
		display.getStudyTypeView().setChoices(
				ListChoices.EEG_STUDY_TYPE_CHOICES);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getSaveButton()) {
			onSave();
		} else if (source == display.getCancelButton()) {
			goToEditPatient(patient);
		} else if (source == display.getAddContactGroupButton()) {
			if (study.getId() == null) {
				Window.alert("Please save the study before adding contact groups.");
			} else {
				eventBus.fireEvent(new CreateContactGroupEvent(study));
			}
		}
	}

	private void onSave() {

		Long startDate = display.getStartDateView().getValue();
		Long endDate = display.getEndDateView().getValue();
		boolean canSave = true;
		if (startDate == null) {
			canSave = false;
			display.getStartDateView().setAndShowErrorMessage(
					"You must enter a start date.");
		}
		if (endDate == null) {
			canSave = false;
			display.getEndDateView().setAndShowErrorMessage(
					"You must enter an end date.");
		}
		if (startDate != null && endDate != null
				&& startDate.compareTo(endDate) > 0) {
			canSave = false;
			display.getStartDateView().setAndShowErrorMessage(
					"The start date must be earlier than the end date.");
		}

		Integer seizureCount = display.getSeizureCount().getValue();
		if (seizureCount != null && seizureCount.intValue() < 0) {
			display
					.setAndShowSeizureCountErrorMessage("The recorded seizure count must be a nonnegative number or left blank.");
			canSave = false;
		}

		final String studyLabel = display.getStudyLabelView().getValue();
		if ("".equals(studyLabel)) {
			display.getStudyLabelView().setAndShowErrorMessage(
					"You must enter a study label.");
			canSave = false;
		}

		final String studyDir = display.getStudyDirView().getValue();
		if ("".equals(studyDir)) {
			display.getStudyDirView().setAndShowErrorMessage(
					"You must enter a study directory.");
			canSave = false;
		}

		final String mefDir = display.getMefDirView().getValue();
		if ("".equals(mefDir)) {
			display.getMefDirView().setAndShowErrorMessage(
					"You must enter a Mef directory.");
			canSave = false;
		}

		// Report File is nullable.
		String reportFile = Strings.emptyToNull(display.getReportFileView()
				.getValue());

		// Images file is nullable.
		String imagesFile = Strings.emptyToNull(display.getImagesFileView()
				.getValue());

		// Ref. Elect. Descrip. is nullable
		String refElectrodeDescription = Strings.emptyToNull(display
				.getRefElectrodeDescriptionView().getValue());

		int selectedStudyTypeIndex = display.getStudyTypeView()
				.getSelectedIndex();
		EegStudyType studyType = EegStudyType.fromString
				.get(ListChoices.EEG_STUDY_TYPE_CHOICES[selectedStudyTypeIndex]);

		final Boolean published = display.getPublishedView().getValue();

		if (canSave) {
			final GwtRecording recording = study.getRecording();
			recording.setStartTimeUutc(startDate);
			recording.setEndTimeUutc(endDate);
			recording.setDir(studyDir);
			recording.setReportFile(reportFile);
			recording.setImagesFile(imagesFile);
			recording.setMefDir(mefDir);
			recording.setRefElectrodeDescription(refElectrodeDescription);
			study.setSzCount(seizureCount);
			study.setLabel(studyLabel);
			study.setType(studyType);
			study.setPublished(published);

			final boolean isNewStudy = study.getParent() == null;
			if (isNewStudy) {
				hospitalAdmission.addStudy(study);
			}

			final Set<EpSubtypePair> allPairs = display
					.getEpSubtypes();
			for (final EpSubtypePair pair : allPairs) {
				final GwtEpilepsySubtype studySubtype = tryFind(
						study.getEpilepsySubtypes(),
						and(compose(
								equalTo(
								pair.getEpilepsyType()),
								GwtEpilepsySubtype.getEpilepsyType),
								compose(
										equalTo(
										pair.getEtiology()),
										GwtEpilepsySubtype.getEtiology)))
						.orNull();
				if (studySubtype != null
						&& !display.isEpSubtypeSelected(pair)) {
					// We have it in the study, but it has been unselected.
					studySubtype.setDeleted(true);
				} else if (studySubtype == null
						&& display.isEpSubtypeSelected(pair)) {
					// It has been selected, but is not in the study.
					final GwtEpilepsySubtype newSubtype = new GwtEpilepsySubtype(
							pair.getEpilepsyType(),
							pair.getEtiology(),
							null,
							null);
					study.getEpilepsySubtypes().add(newSubtype);
				}
			}
			service.savePatient(patient, new AsyncCallback<GwtPatient>() {

				@Override
				public void onFailure(Throwable caught) {
					if (isNewStudy) {
						hospitalAdmission.removeStudy(study);
					}
					String message = caught.getMessage();
					if (caught instanceof StaleObjectException) {
						message = "This patient has changed since your last save. Please reload. ("
								+ message + ")";
					} else {
						message = "Could not save study: " + message;
					}
					Window.alert(message);
				}

				@Override
				public void onSuccess(GwtPatient result) {
					patient = result;
					Window.alert("Saved study.");
					// If we want to stay on this screen, then we'd have to
					// lookup the study we just saved.
					goToEditPatient(result);
				}
			});
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object source = event.getSource();
		if (source == display.getSeizureCount()) {
			display.hideSeizureCountErrorMessage();
		} else if (display.getStartDateView().isWrapping(source)) {
			display.getStartDateView().hideErrorMessage();
		} else if (display.getEndDateView().isWrapping(source)) {
			display.getEndDateView().hideErrorMessage();
		} else if (display.getStudyDirView().isWrapping(source)) {
			display.getStudyDirView().hideErrorMessage();
		} else if (display.getMefDirView().isWrapping(source)) {
			display.getMefDirView().hideErrorMessage();
		} else if (display.getStudyLabelView().isWrapping(source)) {
			display.getStudyLabelView().hideErrorMessage();
		} else if (display.getReportFileView().isWrapping(source)) {
			display.getReportFileView().hideErrorMessage();
		} else if (display.getImagesFileView().isWrapping(source)) {
			display.getImagesFileView().hideErrorMessage();
		}
	}

	private void goToEditPatient(final GwtPatient patient) {
		service.retrieveOrganizations(new AsyncCallback<List<GwtOrganization>>() {

			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof BrainTrustAuthorizationException) {
					Window.alert("You are not authorized to list organizations.");
				} else {
					Window.alert("Failed to load organizations.");
				}
				eventBus.fireEvent(new DataEntryMenuEvent());
			}

			@Override
			public void onSuccess(List<GwtOrganization> result) {
				eventBus.fireEvent(new EditPatientEvent(result, patient));
			}
		});
	}
}
