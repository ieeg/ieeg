/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.dto.IElectrode;

/**
 * Controls a {@code IReferenceElectrodeView}.
 * 
 * @author John Frommeyer
 * 
 */
public class CommonElectrodePresenter implements IPresenter, KeyPressHandler {

	/**
	 * What this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface ICommonElectrodeView extends IView {
		/**
		 * Returns the selected GwtSide index.
		 * 
		 * @return the selected GwtSide index
		 */
		int getSelectedSide();

		/**
		 * Sets the selected GwtSide index.
		 * 
		 * @param index
		 */
		void setSelectedSide(int index);

		/**
		 * Sets the possible values for GwtSide.
		 * 
		 * @param sides
		 */
		void setSideChoices(String[] sides);

		/**
		 * Returns the selected GwtLobe index.
		 * 
		 * @return the selected GwtLobe index
		 */
		int getSelectedLocation();

		/**
		 * Sets the selected GwtLobe index.
		 * 
		 * @param index
		 */
		void setSelectedLocation(int index);

		/**
		 * Sets the possible values for GwtLobe.
		 * 
		 * @param locations
		 */
		void setLocationChoices(String[] locations);

		/**
		 * Returns the impedance value field.
		 * 
		 * @return the impedance value field
		 */
		HasValue<Double> getImpedance();

		/**
		 * Sets and shows an error message.
		 * 
		 * @param errorMessage
		 */
		void setAndShowImpedanceErrorMessage(String errorMessage);

		/**
		 * Hides the error message.
		 * 
		 */
		void hideImpedanceErrorMessage();

		/**
		 * Returns the {@code HandlerRegistration} resulting from adding the
		 * given handler.
		 * 
		 * @param handler
		 * @return the {@code HandlerRegistration} resulting from adding the
		 *         given handler
		 */
		HandlerRegistration addImpedanceKeyPressHandler(KeyPressHandler handler);

		void setImpedanceEnabled(boolean enabled);

		void setUnmodifiable();
	}

	private final ICommonElectrodeView display;
	private final IElectrode commonElectrode;
	/** The display strings for all side choices. */
	private final String[] sideChoices;

	/** The display strings for all location choices. */
	private final String[] locationChoices;
	private final boolean modifiable;

	/**
	 * Creates a presenter for the given display and {@code ICommonElectrode}.
	 * 
	 * @param display
	 * @param commonElectrode
	 * @param sideChoices TODO
	 * @param locationChoices TODO
	 * @param modifiable
	 */
	public CommonElectrodePresenter(final ICommonElectrodeView display,
			final IElectrode commonElectrode, final String[] sideChoices,
			final String[] locationChoices, boolean modifiable) {
		this.display = display;
		this.commonElectrode = commonElectrode;
		this.locationChoices = locationChoices;
		this.sideChoices = sideChoices;
		this.modifiable = modifiable;
	}

	void populateDisplay() {
		final Location location = commonElectrode.getLocation();
		final String locationStr = location == null ? "Unknown" : location.toString();
		final int selectedLocation = choice2Index(locationChoices, locationStr);
		display.setSelectedLocation(selectedLocation);

		final Side side = commonElectrode.getSide();
		final String sideStr = side == null ? "Unknown" : side.toString();
		final int selectedSide = choice2Index(sideChoices, sideStr);
		display.setSelectedSide(selectedSide);

		final Double impedance = commonElectrode.getImpedance().orNull();
		display.getImpedance().setValue(impedance);

		if (!modifiable) {
			display.setUnmodifiable();
		}
	}

	void bind() {
		display.setSideChoices(sideChoices);
		display.setLocationChoices(locationChoices);
		display.addImpedanceKeyPressHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object source = event.getSource();

		if (source == display.getImpedance()) {
			display.hideImpedanceErrorMessage();
		}
	}

	boolean populateModel() {
		boolean canSave = true;

		// Impedance
		Double impedance = display.getImpedance().getValue();

		// Location
		Side side = Side.fromString
				.get((sideChoices[display
						.getSelectedSide()]));

		Location location = Location.fromString
				.get((locationChoices[display
						.getSelectedLocation()]));

		if (canSave) {
			commonElectrode.setImpedance(impedance);

			commonElectrode.setSide(side);
			commonElectrode.setLocation(location);
		}
		return canSave;
	}

	private static int choice2Index(String[] choices, String selected) {
		for (int i = 0; i < choices.length; i++) {
			if (choices[i].equals(selected)) {
				return i;
			}
		}
		return -1;
	}
}
