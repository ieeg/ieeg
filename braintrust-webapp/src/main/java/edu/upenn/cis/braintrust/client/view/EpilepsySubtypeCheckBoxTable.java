/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;

import edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView;
import edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView.EpSubtypePair;
import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;

/**
 * 
 * 
 * @author John Frommeyer
 * 
 */
public class EpilepsySubtypeCheckBoxTable extends Composite {

	private final Map<IStudyView.EpSubtypePair, CheckBox> pairToBox = newHashMap();
	private final Grid grid;

	private static final GlobalCss css;
	static {
		css = ICssResources.INSTANCE.globalCss();
		css.ensureInjected();
	}

	public EpilepsySubtypeCheckBoxTable() {
		final int rowCount = EpilepsyType.values().length + 1;
		final int colCount = Etiology.values().length + 1;
		grid = new Grid(rowCount, colCount);
		grid.addStyleName(css.epSubtypeTable());

		// First row is header row. Leave (0,0) blank
		int headerCol = 1;
		for (final Etiology etiology : Etiology.values()) {
			final Label etiologyLabel = new Label(etiology.toString());
			etiologyLabel.addStyleName(css.epSubtypeTableName());
			grid.setWidget(0, headerCol++, etiologyLabel);
		}
		// Now rest of rows
		int row = 1;
		for (final EpilepsyType epilepsyType : EpilepsyType.values()) {
			if ((row % 2) != 0) {
				grid.getRowFormatter().addStyleName(row,
						css.epSubtypeTableOddRow());
			}
			int bodyCol = 0;
			final Label epTypeLabel = new Label(epilepsyType.toString());
			epTypeLabel.addStyleName(css.epSubtypeTableName());
			grid.setWidget(row, bodyCol++, epTypeLabel);
			for (final Etiology etiology : Etiology.values()) {
				final CheckBox checkBox = new CheckBox();
				// Set tool-tip in case list is too long to see header row.
				checkBox.setTitle(etiology.toString());
				grid.setWidget(row, bodyCol++, checkBox);
				pairToBox.put(new IStudyView.EpSubtypePair(epilepsyType,
						etiology),
						checkBox);
			}
			row++;
		}
		initWidget(grid);
	}

	public void setSelected(IStudyView.EpSubtypePair pair, boolean selected) {
		checkNotNull(pair);
		final CheckBox checkBox = pairToBox.get(pair);
		if (checkBox == null) {
			throw new IllegalArgumentException("Pair " + pair
					+ " is not a valid pair");
		}
		checkBox.setValue(selected);
	}

	public Set<IStudyView.EpSubtypePair> getPairs() {
		return pairToBox.keySet();
	}

	public boolean isSelected(EpSubtypePair pair) {
		checkNotNull(pair);
		final CheckBox checkBox = pairToBox.get(pair);
		if (checkBox == null) {
			throw new IllegalArgumentException("Pair " + pair
					+ " is not a valid pair");
		}
		return checkBox.getValue().booleanValue();
	}
}
