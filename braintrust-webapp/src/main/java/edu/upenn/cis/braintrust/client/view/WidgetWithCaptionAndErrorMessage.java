/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Wraps a user supplied {@code Widget} so that it has a caption and an error
 * message. Uses CSS styles {@code widgetWithCaptionAndErrorMessage} and {@code
 * errorMessage}.
 * 
 * @author John Frommeyer
 * 
 */
class WidgetWithCaptionAndErrorMessage extends Composite {
	private final Label errorMessage;
		
	WidgetWithCaptionAndErrorMessage(String label, Widget widget) {
		VerticalPanel panel = new VerticalPanel();
		Label captionLabel = new Label(label);
		captionLabel.setStyleName("widgetWithCaptionAndErrorMessage");
		panel.add(captionLabel);
		panel.add(widget);
		errorMessage = new Label();
		errorMessage.setStyleName("errorMessage");
		errorMessage.setVisible(false);
		panel.add(errorMessage);
		initWidget(panel);

	}

	Label getErrorMessage() {
		return errorMessage;
	}

}
