/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;

public class AnimalLabelUpdater implements
		FieldUpdater<GwtAnimal, String> {
	private final IAnimalTable display;
	private final BrainTrustServiceAsync service;

	public AnimalLabelUpdater(IAnimalTable display,
			BrainTrustServiceAsync service) {
		this.display = checkNotNull(display);
		this.service = checkNotNull(service);
	}

	@Override
	public void update(final int index, final GwtAnimal object, String value) {
		display.getModifyEntryErrorMessage()
				.hideErrorMessage();
		if ("".equals(value)) {
			display.getModifyEntryErrorMessage()
					.setAndShowErrorMessage(
							"Value cannot be empty");
			display.clearAnimalLabelView(object);
			display.redrawTable();
		} else {
			final String origLabel = object.getLabel();
			final GwtAnimal modifiedValue = new GwtAnimal(
					value,
					object.getOrganization(),
					object.getStrain(),
					object.getId(),
					object.getVersion());
			service.modifyAnimal(modifiedValue,
					new AsyncCallback<GwtAnimal>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("An error occurred while attempting to modify value "
									+ origLabel);
							display.clearAnimalLabelView(object);
							display.redrawTable();
						}

						@Override
						public void onSuccess(GwtAnimal result) {
							display.setAnimal(result);
							display.refreshProvider();

						}
					});
		}

	}
}
