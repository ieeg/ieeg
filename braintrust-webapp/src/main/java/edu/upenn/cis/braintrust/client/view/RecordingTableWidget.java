/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.base.Strings.nullToEmpty;

import java.util.List;

import com.google.common.base.Strings;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IRecordingTable;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;

/**
 * A table for animal experiments
 * 
 * @author John Frommeyer
 * 
 */
public class RecordingTableWidget extends Composite implements
		IRecordingTable {
	private static RecordingTableViewUiBinder uiBinder = GWT
			.create(RecordingTableViewUiBinder.class);

	interface RecordingTableViewUiBinder extends
			UiBinder<Widget, RecordingTableWidget> {}

	// Experiment
	@UiField(provided = true)
	DataEntryTableWidget<GwtRecording> recordingTableWidget;
	private final EditTextCell mefDirCell = new EditTextCell();
	private final Column<GwtRecording, String> mefDirColumn = new Column<GwtRecording, String>(
			mefDirCell) {

		@Override
		public String getValue(GwtRecording object) {
			return object.getMefDir();
		}
	};
	private final EditTextCell startTimeCell = new EditTextCell();
	private final Column<GwtRecording, String> startTimeColumn = new Column<GwtRecording, String>(
			startTimeCell) {

		@Override
		public String getValue(GwtRecording object) {
			return object.getStartTimeUutc().toString();
		}
	};
	private final EditTextCell endTimeCell = new EditTextCell();
	private final Column<GwtRecording, String> endTimeColumn = new Column<GwtRecording, String>(
			endTimeCell) {

		@Override
		public String getValue(GwtRecording object) {
			return object.getEndTimeUutc().toString();
		}
	};
	private final EditTextCell dirCell = new EditTextCell();
	private final Column<GwtRecording, String> dirColumn = new Column<GwtRecording, String>(
			dirCell) {

		@Override
		public String getValue(GwtRecording object) {
			return object.getDir();
		}
	};

	private final Column<GwtRecording, String> imagesFileColumn = new Column<GwtRecording, String>(
			new EditTextCell()) {

		@Override
		public String getValue(GwtRecording object) {
			return Strings.nullToEmpty(object.getImagesFile());
		}
	};

	private final Column<GwtRecording, String> reportFileColumn = new Column<GwtRecording, String>(
			new EditTextCell()) {

		@Override
		public String getValue(GwtRecording object) {
			return Strings.nullToEmpty(object.getReportFile());
		}
	};

	private final Column<GwtRecording, String> commentaryColumn = new Column<GwtRecording, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtRecording object) {
			return "edit commentary";
		}
	};

	private final EditTextCell refElectrodeCell = new EditTextCell();
	private final Column<GwtRecording, String> refElectrodeColumn = new Column<GwtRecording, String>(
			refElectrodeCell) {

		@Override
		public String getValue(GwtRecording object) {
			return nullToEmpty(object.getRefElectrodeDescription().orNull());
		}
	};
	
	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public RecordingTableWidget() {
		// Animal
		final CellTable<GwtRecording> recordingTable = new CellTable<GwtRecording>(
				1,
				KeyProviderMap.getMap().getProvider(GwtRecording.class));
		recordingTable.setWidth("100%", false);
		recordingTable.addColumn(dirColumn, "Recording Directory");
		recordingTable.addColumn(mefDirColumn,
				"MEF Directory (relative to recording dir)");
		recordingTable.addColumn(startTimeColumn, "Start Time (µs)");
		recordingTable.addColumn(endTimeColumn, "End Time (µs)");
		recordingTable.addColumn(imagesFileColumn,
				"Images File (relative to recording dir)");
		recordingTable.addColumn(reportFileColumn,
				"Report File (relative to recording dir)");
		recordingTable.addColumn(commentaryColumn, "Commentary");
		recordingTable.addColumn(refElectrodeColumn, "Reference Electrode Description");
		recordingTableWidget = new DataEntryTableWidget<GwtRecording>(
				"Recording",
				recordingTable);
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public List<GwtRecording> getEntryList() {
		return recordingTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		recordingTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		recordingTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return recordingTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setRecording(GwtRecording recording) {
		if (recordingTableWidget.getEntryList().size() == 0) {
			recordingTableWidget.getEntryList().add(recording);
		} else if (recordingTableWidget.getEntryList().size() == 1) {
			recordingTableWidget.getEntryList().clear();
			recordingTableWidget.getEntryList().add(recording);
		} else {
			Window.alert("An error has occurrred in RecordingTableWidget. Please report.");
		}
	}

	@Override
	public void setRecordingDirUpdater(
			FieldUpdater<GwtRecording, String> institutionUpdater) {
		dirColumn.setFieldUpdater(institutionUpdater);
	}

	@Override
	public void setRecordingMefDirUpdater(
			FieldUpdater<GwtRecording, String> labelUpdater) {
		mefDirColumn.setFieldUpdater(labelUpdater);
	}

	@Override
	public void setRecordingImagesFileUpdater(
			FieldUpdater<GwtRecording, String> strainUpdater) {
		imagesFileColumn.setFieldUpdater(strainUpdater);
	}

	@Override
	public void clearRecordingDirView(GwtRecording animal) {
		dirCell.clearViewData(KeyProviderMap.getMap().getProvider(GwtRecording.class).getKey(animal));
	}

	@Override
	public void clearRecordingMefDirView(GwtRecording animal) {
		mefDirCell.clearViewData(KeyProviderMap.getMap().getProvider(GwtRecording.class).getKey(animal));

	}

	@Override
	public void setRecordingStartTimeUpdater(
			FieldUpdater<GwtRecording, String> updater) {
		startTimeColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearRecordingStartTimeView(GwtRecording recording) {
		startTimeCell
				.clearViewData(KeyProviderMap.getMap().getProvider(GwtRecording.class).getKey(recording));
	}

	@Override
	public void setRecordingEndTimeUpdater(
			FieldUpdater<GwtRecording, String> updater) {
		endTimeColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearRecordingEndTimeView(GwtRecording recording) {
		endTimeCell.clearViewData(KeyProviderMap.getMap().getProvider(GwtRecording.class).getKey(recording));
	}

	@Override
	public void setRecordingReportFileUpdater(
			FieldUpdater<GwtRecording, String> updater) {
		reportFileColumn.setFieldUpdater(updater);
	}
	
	@Override
	public void setRecordingRefElectrodeDescriptionUpdater(
			FieldUpdater<GwtRecording, String> updater) {
		refElectrodeColumn.setFieldUpdater(updater);
	}

	@Override
	public void setRecordingCommentaryUpdater(
			FieldUpdater<GwtRecording, String> updater) {
		commentaryColumn.setFieldUpdater(updater);
	}

}
