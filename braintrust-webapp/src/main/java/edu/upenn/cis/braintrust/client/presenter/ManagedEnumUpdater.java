/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.Collections;

import com.google.common.base.Optional;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.presenter.ManagedEnumPresenter.IManagedEnumView;
import edu.upenn.cis.braintrust.shared.dto.IManagedEnum;

public class ManagedEnumUpdater<T extends IManagedEnum> implements
		FieldUpdater<T, String> {
	private final IManagedEnumView<T> display;
	private final IManagedEnumFactory<T> factory;
	private final IManagedEnumService<T> service;

	public ManagedEnumUpdater(IManagedEnumView<T> display,
			IManagedEnumService<T> service, IManagedEnumFactory<T> factory) {
		this.display = checkNotNull(display);
		this.factory = checkNotNull(factory);
		this.service = checkNotNull(service);
	}

	@Override
	public void update(final int index, final T object, String value) {
		display.getModifyValueErrorMessage().hideErrorMessage();
		if ("".equals(value)) {
			display.getModifyValueErrorMessage()
					.setAndShowErrorMessage(
							"Value cannot be empty");
			display.clearViewData(object);
			display.redraw();
		} else {
			final String origLabel = object.getLabel();
			final Optional<T> valueOpt = tryFind(
					display.getValueList(),
					compose(equalTo(value), T.getLabel));
			if (valueOpt.isPresent()) {
				if (!valueOpt.get().getId().equals(object.getId())) {
					display.getModifyValueErrorMessage()
							.setAndShowErrorMessage(
									"The value "
											+ value
											+ " already exists.");
					display.clearViewData(object);
					display.redraw();
				}
			} else {
				final T modifiedValue = factory.newInstance(value,
						object.getId(), object.getVersion());
				service.modifyLabel(modifiedValue,
						new AsyncCallback<T>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("An error occurred while attempting to modify value "
										+ origLabel);
								display.clearViewData(object);
								display.redraw();
							}

							@Override
							public void onSuccess(T result) {
								display.getValueList().set(index, result);
								Collections.sort(display.getValueList(),
										T.LABEL_COMPARATOR);
								display.refresh();

							}
						});
			}

		}
	}
}
