/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;

import edu.upenn.cis.braintrust.shared.dto.GwtTrace;

public interface ITraceTable extends IDataEntryTable<GwtTrace> {

	void setTraceDeleter(FieldUpdater<GwtTrace, String> deleter);

	void setFileKeyUpdater(FieldUpdater<GwtTrace, String> updater);

	void setLabelUpdater(FieldUpdater<GwtTrace, String> updater);
	
	void setContactTypeUpdater(FieldUpdater<GwtTrace, String> updater);

	void clearFileKeyView(GwtTrace trace);

	void clearLabelView(GwtTrace trace);

	void removeDeleteColumn();

}
