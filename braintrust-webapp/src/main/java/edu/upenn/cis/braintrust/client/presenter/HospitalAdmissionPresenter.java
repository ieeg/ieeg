/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import java.util.List;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.CreateStudyEvent;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.client.event.EditStudyEvent;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * DOCUMENT ME
 * 
 * @author John Frommeyer
 * 
 */
public class HospitalAdmissionPresenter implements IPresenter, ClickHandler,
		KeyPressHandler {

	public static interface IHospitalAdmissionView extends IView {
		IHasValueAndErrorMessage<Integer> getAgeAtAdmissionView();

		IHasEnabledAndClickHandlers getSaveButton();

		IHasEnabledAndClickHandlers getCancelButton();

		/**
		 * Returns a pair of edit and remove buttons for the study being added.
		 * 
		 * @param id the id of the study
		 * @param description
		 * @param studyIsModifiable
		 * 
		 * @return a pair of edit and remove buttons for the study being added
		 */
		EditRemoveButtonPair addStudy(Long id, String description,
				boolean studyIsModifiable);

		/**
		 * Returns the add Study button of the view.
		 * 
		 * @return the add Study button of the view
		 */
		IHasEnabledAndClickHandlers getAddStudyButton();

		/**
		 * Hides the error message for the add study field.
		 * 
		 */
		void hideAddStudyErrorMessage();

		/**
		 * Removes the study type from the patient.
		 * 
		 * @param studyId the id of the study to be removed
		 */
		void removeStudy(Long studyId);

		/**
		 * Sets and shows an error message for the add study field.
		 * 
		 * @param errorMessage
		 */
		void setAndShowAddStudyErrorMessage(String errorMessage);
	}

	private final IHospitalAdmissionView display;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private GwtPatient patient;
	private final GwtHospitalAdmission hospitalAdmission;

	/**
	 * Create a {@code HospitalAdmissionPresenter}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 * @param patient
	 */
	public HospitalAdmissionPresenter(final HandlerManager eventBus,
			final IHospitalAdmissionView display,
			final BrainTrustServiceAsync service,
			final GwtPatient patient) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = service;
		this.patient = patient;
		this.hospitalAdmission = new GwtHospitalAdmission();
		bind();
	}

	/**
	 * Creates a presenter with an existing {@code GwtHospitalAdmission}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param hospitalAdmission
	 */
	public HospitalAdmissionPresenter(final HandlerManager eventBus,
			final IHospitalAdmissionView display,
			final BrainTrustServiceAsync rpcService,
			final GwtHospitalAdmission hospitalAdmission) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = rpcService;
		this.hospitalAdmission = hospitalAdmission;
		this.patient = this.hospitalAdmission.getParent();
		bind();
		populateDisplay();
	}

	private void bind() {
		display.getSaveButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);
		display.getAddStudyButton().addClickHandler(this);
		display.getAgeAtAdmissionView().addKeyPressHandler(this);
	}

	private void populateDisplay() {
		display.getAgeAtAdmissionView().setValue(
				hospitalAdmission.getAgeAtAdmission().orNull());

		// Studies
		Set<GwtEegStudy> studies = hospitalAdmission.getStudies();
		for (final GwtEegStudy study : studies) {
			Long id = study.getId();
			String description = study.getLabel();
			EditRemoveButtonPair addStudy = display.addStudy(id, description,
					study.isModifiable());
			if (addStudy != null) {
				HasClickHandlers edit = addStudy.getEditButton();
				edit.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						eventBus.fireEvent(new EditStudyEvent(study));
						// if (!study.isModifiable()) {
						// Window.alert("Study is read-only: A time series in this study is referenced by a user created data snapshot.");
						// }
					}
				});
				HasClickHandlers remove = addStudy.getRemoveButton();
				remove.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						onRemoveStudy(study);
					}

				});
			}
		}
	}

	private void onRemoveStudy(GwtEegStudy study) {
		if (study.getId() == null) {
			hospitalAdmission.removeStudy(study);
		} else {
			study.setDeleted(Boolean.TRUE);
		}
		display.removeStudy(study.getId());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (display.getSaveButton().isWrapping(source)) {
			doSave();
		} else if (display.getCancelButton().isWrapping(source)) {
				goToEditPatient(patient);
		} else if (display.getAddStudyButton().isWrapping(source)) {
			if (hospitalAdmission.getId() == null) {
				Window
						.alert("Please save hospital admission before adding a study.");
			} else {
				eventBus.fireEvent(new CreateStudyEvent(hospitalAdmission));
			}
		}
	}

	private void doSave() {
		boolean canSave = true;
		Integer admissionAge = display.getAgeAtAdmissionView().getValue();
		if (admissionAge != null && admissionAge.intValue() < 0) {
			display.getAgeAtAdmissionView()
					.setAndShowErrorMessage(
							"You must enter a non-negative value patient's age at admission or leave blank.");
			canSave = false;
		}
		if (canSave) {
			hospitalAdmission.setAgeAtAdmission(admissionAge);

			final boolean isNewHospitalAdmission = hospitalAdmission
					.getParent() == null;
			if (isNewHospitalAdmission) {
				patient.addAdmission(hospitalAdmission);
			}
			service.savePatient(patient, new AsyncCallback<GwtPatient>() {

				@Override
				public void onFailure(Throwable caught) {
					if (isNewHospitalAdmission) {
						patient.removeAdmission(hospitalAdmission);
					}
					String message = caught.getMessage();
					if (caught instanceof StaleObjectException) {
						message = "This patient has changed since your last save. Please reload. ("
								+ message + ")";
					} else {
						message = "Could not save hospital admission: "
								+ message;
					}
					Window.alert(message);
				}

				@Override
				public void onSuccess(GwtPatient result) {
					patient = result;
					Window.alert("Saved hospital admission.");
					// If we want to stay on this screen, then we'd have to
					// lookup the hospitalAdmission we just saved.
					goToEditPatient(result);
				}
			});
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object source = event.getSource();
		if (display.getAgeAtAdmissionView().isWrapping(source)) {
			display.getAgeAtAdmissionView().hideErrorMessage();
		}

	}

	private void goToEditPatient(final GwtPatient patient) {
		service.retrieveOrganizations(new AsyncCallback<List<GwtOrganization>>() {

			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof BrainTrustAuthorizationException) {
					Window.alert("You are not authorized to list organizations.");
				} else {
					Window.alert("Failed to load organizations.");
				}
				eventBus.fireEvent(new DataEntryMenuEvent());
			}

			@Override
			public void onSuccess(List<GwtOrganization> result) {
				eventBus.fireEvent(new EditPatientEvent(result, patient));
			}
		});
	}
}
