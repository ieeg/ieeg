/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.CreateHospitalAdmissionEvent;
import edu.upenn.cis.braintrust.client.event.CreatePatientCanceledEvent;
import edu.upenn.cis.braintrust.client.event.EditHospitalAdmissionEvent;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * Controls a GwtPatient view.
 * 
 * @author John Frommeyer
 * 
 */
public class PatientPresenter implements IPresenter, KeyPressHandler,
		ClickHandler {

	/** The display strings for all ethnicity choices. */
	static final String[] ETHNICITY_CHOICES = Ethnicity.fromString.keySet()
			.toArray(new String[Ethnicity.values().length]);

	/** The display strings for all gender choices. */
	static final String[] GENDER_CHOICES = Gender.fromString.keySet().toArray(
			new String[Gender.values().length]);

	/** The display strings for all handedness choices. */
	static final String[] HANDEDNESS_CHOICES = Handedness.fromString.keySet()
			.toArray(new String[Handedness.values().length]);

	/** The display strings for all situation choices. */
	static final String[] SITUATION_CHOICES = Precipitant.fromString.keySet()
			.toArray(new String[Precipitant.values().length]);

	private GwtPatient patient;

	/**
	 * Views for this presenter should implement this interface.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IPatientView extends IView, IHasSaveButton,
			IHasCancelButton, IHasDeleteButton {

		ISingleSelectionList getOrganizationView();

		/**
		 * Returns the patient identifier.
		 * 
		 * @return the patient identifier, or "" if none has been set.
		 */
		HasValue<String> getPatientLabel();

		/**
		 * Set and show an error message for the patient id field.
		 * 
		 * @param errorMessage the error message
		 * 
		 */
		void setAndShowPatientLabelErrorMessage(String errorMessage);

		/**
		 * Hide the institution error message.
		 * 
		 */
		void hidePatientLabelErrorMessage();

		/**
		 * Add a {@KeyPressHandler} for the patient id field.
		 * 
		 * @param handler
		 * @return {@code HandlerRegistration} used to remove this handler
		 */
		HandlerRegistration addPatientLabelKeyPressHandler(
				KeyPressHandler handler);

		/**
		 * Inform the display of possible ethnicities.
		 * 
		 * @param ethnicities
		 */
		void setEthnicityChoices(String[] ethnicities);

		/**
		 * Returns the index of the selected ethnicity, or -1 if none is
		 * selected.
		 * 
		 * @return the index of the selected ethnicity, or -1 if none is
		 *         selected
		 */
		int getSelectedEthnicity();

		/**
		 * Sets the selected ethnicity.
		 * 
		 * @param i the selected index with respect to {@code ETHNICITY_CHOICES}
		 *            .
		 */
		void setSelectedEthnicity(int i);

		/**
		 * Inform the display of possible genders.
		 * 
		 * @param genders
		 */
		void setGenderChoices(String[] genders);

		/**
		 * Returns the index of the selected gender, or -1 if none is selected.
		 * 
		 * @return the index of the selected gender, or -1 if none is selected
		 */
		int getSelectedGender();

		/**
		 * Sets the selected gender.
		 * 
		 * @param i the selected index with respect to {@code GENDER_CHOICES}.
		 */
		void setSelectedGender(int i);

		/**
		 * Inform the display of possible handednesses.
		 * 
		 * @param handednesses
		 */
		void setHandednessChoices(String[] handednesses);

		/**
		 * Returns the index of the selected handedness, or -1 if none is
		 * selected.
		 * 
		 * @return the index of the selected handedness, or -1 if none is
		 *         selected
		 */
		int getSelectedHandedness();

		/**
		 * Sets the selected handedness.
		 * 
		 * @param i the selected index with respect to
		 *            {@code HANDEDNESS_CHOICES}.
		 */
		void setSelectedHandedness(int i);

		/**
		 * Inform the display of possible epilepsy types.
		 * 
		 * @param seizureTypes
		 */
		void setSeizureTypeChoices(String[] seizureTypes);

		/**
		 * Returns the add epilepsy case button of the view.
		 * 
		 * @return the add epilepsy case button of the view
		 */
		HasClickHandlers getAddHospitalAdmissionButton();

		/**
		 * Removes the hospital admission from the patient.
		 * 
		 * @param hospitalAdmissionId the id of the hospital admission to be
		 *            removed
		 */
		void removeHospitalAdmission(Long hospitalAdmissionId);

		/**
		 * Returns a pair of edit and remove buttons for the case being added.
		 * 
		 * @param id
		 * 
		 * @param description
		 * @param removable
		 * @return a pair of edit and remove buttons for the case being added
		 */
		EditRemoveButtonPair addHospitalAdmission(Long id, String description,
				boolean removable);

		/**
		 * Sets and shows an error message for the add epilepsy case field.
		 * 
		 * @param errorMessage
		 */
		void setAndShowAddHospitalAdmissionErrorMessage(String errorMessage);

		/**
		 * Hides the error message for the add epilepsy case field.
		 * 
		 */
		void hideAddHospitalAdmissionErrorMessage();

		/**
		 * Sets the situation choices for the patient view.
		 * 
		 * @param situations
		 */
		void setSituationChoices(String[] situations);

		/**
		 * Returns the list of selected indices for epilepsy type situations.
		 * 
		 * @return the list of selected indices for epilepsy type situations
		 */
		Set<Integer> getSelectedSituationRows();

		/**
		 * Sets the given {@row} selected status to {@code selected}.
		 * 
		 * @param row
		 * @param selected
		 */
		void setSituationRowSelected(int row, boolean selected);

		/**
		 * Returns the list of selected indices for seizure type.
		 * 
		 * @return the list of selected indices for seizure type
		 */
		Set<Integer> getSelectedSeizureTypeRows();

		/**
		 * Sets the given {@row} selected status to {@code selected}.
		 * 
		 * @param row
		 * @param selected
		 */
		void setSeizureTypeRowSelected(int row, boolean selected);

		/**
		 * Returns a view for the "age at onset" field.
		 * 
		 * @return a view for the "age at onset" field
		 */
		IHasValueAndErrorMessage<Integer> getAgeAtOnsetView();

		/**
		 * Returns a view for the etiology field.
		 * 
		 * @return a view for the etiology field
		 */
		IHasValueAndSuggestions<String> getEtiologyView();

		/**
		 * Returns a view for the developmental delay field.
		 * 
		 * @return a view for the developmental delay field
		 */
		IHasValue<String> getDevelopmentalDelayView();

		/**
		 * Returns a view for the developmental disorder check box.
		 * 
		 * @return a view for the developmental disorder check box
		 */
		IHasValue<Boolean> getDevelopmentalDisorderView();

		/**
		 * Returns a view for the traumatic brain injury check box.
		 * 
		 * @return a view for the traumatic brain injury check box
		 */
		IHasValue<Boolean> getTraumaticBrainInjuryView();

		/**
		 * Returns a view for the family history check box.
		 * 
		 * @return a view for the family history check box
		 */
		IHasValue<Boolean> getFamilyHistoryView();

	}

	private final HandlerManager eventBus;
	private final IPatientView display;
	private final BrainTrustServiceAsync service;
	private List<GwtOrganization> organizations;

	/**
	 * Create a {@code PatientPresenter}.
	 * 
	 * @param theEventBus
	 * @param theDisplay
	 * @param theService
	 * @param organizations TODO
	 * @param arrayInfoStore
	 */
	public PatientPresenter(
			final HandlerManager theEventBus,
			final IPatientView theDisplay,
			final BrainTrustServiceAsync theService,
			List<GwtOrganization> organizations) {
		eventBus = theEventBus;
		display = theDisplay;
		service = theService;
		this.organizations = ImmutableList.copyOf(organizations);
		patient = new GwtPatient();

		bind();

	}

	/**
	 * Create a {@code PatientPresenter}.
	 * 
	 * @param theEventBus
	 * @param theDisplay
	 * @param theService
	 * @param organizations TODO
	 * @param thePatient
	 * @param arrayInfoStore
	 */
	public PatientPresenter(
			final HandlerManager theEventBus,
			final IPatientView theDisplay,
			final BrainTrustServiceAsync theService,
			List<GwtOrganization> organizations,
			final GwtPatient thePatient) {
		eventBus = theEventBus;
		display = theDisplay;
		service = theService;
		this.organizations = ImmutableList.copyOf(organizations);
		patient = thePatient;

		bind();
		populateDisplay();

	}

	private void bind() {
		final Set<String> etiologyChoices = newHashSet(Arrays
				.asList((ListChoices.PATHOLOGY_CHOICES)));
		service.retrieveEtiologies(new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				display.getEtiologyView().setSuggestions(etiologyChoices);
			}

			@Override
			public void onSuccess(List<String> result) {
				if (result == null) {
					display.getEtiologyView().setSuggestions(etiologyChoices);
				} else {
					etiologyChoices.addAll(result);
					display.getEtiologyView().setSuggestions(etiologyChoices);
				}
			}
		});
		final String[] organizationChoices = new String[organizations.size()];
		for (int i = 0; i < organizations.size(); i++) {
			organizationChoices[i] = organizations.get(i).toString();
		}
		display.getOrganizationView().setChoices(organizationChoices);
		display.setGenderChoices(GENDER_CHOICES);
		display.setEthnicityChoices(ETHNICITY_CHOICES);
		display.setHandednessChoices(HANDEDNESS_CHOICES);
		display.setSeizureTypeChoices(ListChoices.SZ_TYPE_CHOICES);
		display.setSituationChoices(SITUATION_CHOICES);
		display.addPatientLabelKeyPressHandler(this);
		display.getAgeAtOnsetView().addKeyPressHandler(this);
		display.getEtiologyView().addKeyPressHandler(this);
		display.getAddHospitalAdmissionButton().addClickHandler(this);
		display.getSaveButton().addClickHandler(this);
		display.getDeleteButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	private void doSave() {
		String gender = GENDER_CHOICES[display.getSelectedGender()];
		String ethnicity = ETHNICITY_CHOICES[display.getSelectedEthnicity()];
		String handedness = HANDEDNESS_CHOICES[display.getSelectedHandedness()];
		boolean canSave = true;
		final int orgIdx = display.getOrganizationView().getSelectedIndex();
		if (orgIdx < 0 || orgIdx >= organizations.size()) {
			canSave = false;
			Window.alert("Organization list out of date. Please return to main menu and try again.");
		}
		final GwtOrganization org = organizations.get(orgIdx);
		if (org == null) {
			canSave = false;
			Window.alert("Organization list out of date. Please return to main menu and try again.");
		}
		String patientLabel = display.getPatientLabel().getValue()
				.trim();

		// For new patients we enforce the label format but we cannot for
		// existing patients. So it is still possible for a user to update an
		// existing patient with a label which does not match the format.
		if (patient.getId() == null) {
			final RegExp newPatientLabelPattern = RegExp.compile("^"
					+ org.getCode() + "_P[0-9]{3}$");
			if (!newPatientLabelPattern.test(patientLabel)) {
				canSave = false;
				display.setAndShowPatientLabelErrorMessage("New patient ids must be of the form "
						+ org.getCode()
						+ "_Pxyz with x, y, & z in [0-9] for the chosen organization.");
			}
		} else {
			if ("".equals(patientLabel)) {
				canSave = false;
				display.setAndShowPatientLabelErrorMessage("You must enter a patient id.");
			}
		}
		Integer ageAtOnset = display.getAgeAtOnsetView().getValue();
		if (ageAtOnset != null && ageAtOnset.intValue() < 0) {
			canSave = false;
			display.getAgeAtOnsetView()
					.setAndShowErrorMessage(
							"You must enter a non-negative value patient's age at onset or leave blank.");
		}
		String etiology = display.getEtiologyView().getValue().trim();
		if ("".equals(etiology)) {
			canSave = false;
			display.getEtiologyView().setAndShowErrorMessage(
					"You must enter an etiology.");
		}
		String devDelay = display.getDevelopmentalDelayView().getValue().trim();
		if ("".equals(devDelay)) {
			// This is optional
			devDelay = null;
		}
		Boolean devDisorder = display.getDevelopmentalDisorderView().getValue();
		Boolean brainInjury = display.getTraumaticBrainInjuryView().getValue();
		Boolean familyHistory = display.getFamilyHistoryView().getValue();

		if (canSave) {
			patient.setOrganization(org);
			patient.setLabel(patientLabel);
			patient.setGender(Gender.fromString.get(gender));
			patient.setEthnicity(Ethnicity.fromString.get(ethnicity));
			patient.setHandedness(Handedness.fromString.get(handedness));
			patient.setAgeOfOnset(ageAtOnset);
			patient.setEtiology(etiology);
			patient.setDevelopmentalDelay(devDelay);
			patient.setDevelopmentalDisorders(devDisorder);
			patient.setTraumaticBrainInjury(brainInjury);
			patient.setFamilyHistory(familyHistory);

			// Add seizure situations
			final Set<Integer> selectedSituationIndices = display
					.getSelectedSituationRows();
			final Set<Precipitant> selectedSituations = newHashSet();
			for (Integer i : selectedSituationIndices) {
				final String situationString = SITUATION_CHOICES[i.intValue()];
				final Precipitant situation = Precipitant.fromString
						.get(situationString);
				selectedSituations.add(situation);
			}
			patient.getPrecipitants().clear();
			patient.getPrecipitants().addAll(selectedSituations);

			// Add seizure types
			final Set<Integer> selectedSzTypeIndices = display
					.getSelectedSeizureTypeRows();
			final Set<SeizureType> selectedSzTypes = ListChoices
					.indices2SzTypes(selectedSzTypeIndices);
			patient.getSzTypes().clear();
			patient.getSzTypes().addAll(selectedSzTypes);

			service.savePatient(patient, new AsyncCallback<GwtPatient>() {

				@Override
				public void onFailure(Throwable caught) {
					String message = caught.getMessage();
					if (caught instanceof StaleObjectException) {
						message = "This patient has changed since your last save. Please reload. ("
								+ message + ")";
					}
					Window.alert(message);
				}

				@Override
				public void onSuccess(GwtPatient result) {
					patient = result;
					String patientId = patient.getLabel();
					Window.alert("Saved information for " + patientId + ".");
				}
			});
		}
	}

	private void doDelete() {
		final String patientId = patient.getLabel();
		boolean canDelete = true;
		if ("".equals(patientId)) {
			canDelete = false;
			display.setAndShowPatientLabelErrorMessage("You must enter a patient id.");
		}
		if (canDelete) {
			service.deletePatient(patient, new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					final String message = (caught instanceof BrainTrustAuthorizationException)
							|| (caught instanceof StaleObjectException)
							|| (caught instanceof ObjectNotFoundException)
							|| (caught instanceof BrainTrustUserException) ? caught
							.getMessage()
							: "There was an error while trying to delete information for "
									+ patientId + ".";
					Window.alert(message);
				}

				@Override
				public void onSuccess(Void result) {
					Window.alert("Deleted information for " + patientId + ".");
				}
			});
		}
	}

	private void clearError(KeyPressEvent event) {
		Object source = event.getSource();
		if (source == display.getPatientLabel()) {
			display.hidePatientLabelErrorMessage();
		} else if (display.getAgeAtOnsetView().isWrapping(source)) {
			display.getAgeAtOnsetView().hideErrorMessage();
		} else if (display.getEtiologyView().isWrapping(source)) {
			display.getEtiologyView().hideErrorMessage();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		clearError(event);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getDeleteButton()) {
			doDelete();
		} else if (source == display.getSaveButton()) {
			doSave();
		} else if (source == display.getCancelButton()) {
			eventBus.fireEvent(new CreatePatientCanceledEvent());
		} else if (source == display.getAddHospitalAdmissionButton()) {
			if (patient.getId() == null) {
				Window.alert("Please save patient before adding a hospital admission.");
			} else {
				eventBus.fireEvent(new CreateHospitalAdmissionEvent(patient));
			}
		}
	}

	private void populateDisplay() {
		final GwtOrganization org = patient.getOrganization();
		final int orgIdx = organizations.indexOf(org);
		display.getOrganizationView().setSelectedIndex(orgIdx);
		display.getPatientLabel().setValue(patient.getLabel());
		display.getAgeAtOnsetView().setValue(patient.getAgeOfOnset().orNull());
		display.setSelectedEthnicity(Arrays.binarySearch(ETHNICITY_CHOICES,
				patient.getEthnicity().toString()));
		display.setSelectedGender(Arrays.binarySearch(GENDER_CHOICES, patient
				.getGender().toString()));
		display.setSelectedHandedness(Arrays.binarySearch(HANDEDNESS_CHOICES,
				patient.getHandedness().toString()));

		display.getEtiologyView().setValue(patient.getEtiology());
		
		display.getDevelopmentalDelayView().setValue(
				patient.getDevelopmentalDelay().orNull());
		display.getDevelopmentalDisorderView().setValue(
				patient.getDevelopmentalDisorders().orNull());
		display.getTraumaticBrainInjuryView().setValue(
				patient.getTraumaticBrainInjury().orNull());
		display.getFamilyHistoryView().setValue(
				patient.getFamilyHistory().orNull());

		// Seizure Types
		final Set<SeizureType> szTypes = patient.getSzTypes();
		final Set<Integer> indices = ListChoices.enums2Indices(szTypes,
				ListChoices.SZ_TYPE_CHOICES, SeizureType.fromString);
		for (Integer index : indices) {
			display.setSeizureTypeRowSelected(index.intValue(), true);
		}

		// Seizure Situations
		Set<Precipitant> situations = patient.getPrecipitants();
		for (Precipitant situation : situations) {
			int row = Arrays.binarySearch(SITUATION_CHOICES,
					situation.toString());
			display.setSituationRowSelected(row, true);
		}

		// Hospital Admissions
		Set<GwtHospitalAdmission> admissions = patient.getAdmissions();
		for (final GwtHospitalAdmission admission : admissions) {
			Long id = admission.getId();
			String ageAtAdmission = admission.getAgeAtAdmission().isPresent() ? String
					.valueOf(admission
							.getAgeAtAdmission().get())
					: GwtHospitalAdmission.UNKNOWN_ADMISSION_AGE;
			String description = "Age at admission: "
					+ ageAtAdmission;
			EditRemoveButtonPair addAdmission = display.addHospitalAdmission(
					id, description, admission.isRemovable());
			if (addAdmission != null) {
				HasClickHandlers edit = addAdmission.getEditButton();
				edit.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						eventBus.fireEvent(new EditHospitalAdmissionEvent(
								admission));
					}
				});
				HasClickHandlers remove = addAdmission.getRemoveButton();
				remove.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						onRemoveHospitalAdmission(admission);
					}
				});
			}
		}
	}

	private void onRemoveHospitalAdmission(final GwtHospitalAdmission admission) {
		if (admission.getId() == null) {
			patient.removeAdmission(admission);
		} else {
			admission.setDeleted(Boolean.TRUE);
		}
		display.removeHospitalAdmission(admission.getId());
	}
}
