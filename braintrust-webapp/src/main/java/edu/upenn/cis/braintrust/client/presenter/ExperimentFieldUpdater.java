/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import java.util.Collections;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

public class ExperimentFieldUpdater<T> implements
		FieldUpdater<GwtExperiment, T> {
	private final IExperimentTable display;
	private final BrainTrustServiceAsync service;
	private final IExperimentFactory<T> factory;
	private final IExperimentFieldValidator<T> validator;
	private final IHasAnimal hasAnimal;

	public ExperimentFieldUpdater(IExperimentTable display,
			BrainTrustServiceAsync service, IExperimentFactory<T> factory,
			IExperimentFieldValidator<T> validator,
			IHasAnimal hasAnimal) {
		this.display = display;
		this.service = service;
		this.factory = factory;
		this.validator = validator;
		this.hasAnimal = hasAnimal;
	}

	@Override
	public void update(final int index, final GwtExperiment object, T value) {
		display.getModifyEntryErrorMessage()
				.hideErrorMessage();
		if (false == validator.isValidFieldValue(value)) {
			display.getModifyEntryErrorMessage().setAndShowErrorMessage(
					"Invalid value: " + value);
			validator.clearFieldView(display, object);
		} else {

			final GwtExperiment modifiedValue = factory.newInstance(object,
					value);
			service.modifyExperiment(hasAnimal.getAnimal(), modifiedValue,
					new AsyncCallback<GwtExperiment>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("An error occurred while attempting to modify experiment.");
							validator.clearFieldView(display, object);
						}

						@Override
						public void onSuccess(GwtExperiment result) {
							display.getEntryList().set(index, result);
							Collections.sort(display.getEntryList(),
									GwtExperiment.LABEL_COMPARATOR);
							display.refreshProvider();
						}
					});
		}

	}
}
