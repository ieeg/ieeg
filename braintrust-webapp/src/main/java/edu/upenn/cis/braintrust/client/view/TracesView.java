/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.ITraceTable;
import edu.upenn.cis.braintrust.client.presenter.TracesPresenter.ITracesView;

/**
 * A screen to enter information traces
 * 
 * @author John Frommeyer
 * 
 */
public class TracesView extends Composite implements ITracesView {

	private static TracesViewUiBinder uiBinder = GWT
			.create(TracesViewUiBinder.class);

	interface TracesViewUiBinder extends
			UiBinder<DialogBox, TracesView> {}

	@UiField
	VerticalPanel tracesCountPanel;
	@UiField
	TextArea fileKeyList;
	@UiField
	Button createTracesButton;

	@UiField(provided = true)
	TraceTableWidget traceTableWidget;

	@UiField
	Button okButton;

	private final DialogBox dialogBox;

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Creates this view.
	 * @param contactTypes TODO
	 * 
	 */
	public TracesView(List<String> contactTypes) {
		traceTableWidget = new TraceTableWidget(contactTypes);
		dialogBox = uiBinder.createAndBindUi(this);
		// Can access @UiField after calling createAndBindUi
		fileKeyList.setCharacterWidth(50);
		fileKeyList.setVisibleLines(10);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void show() {
		dialogBox.center();
		dialogBox.show();
	}

	@Override
	public void hide() {
		dialogBox.hide();
	}

	@Override
	public HasClickHandlers getCreateTracesButton() {
		return createTracesButton;
	}

	@Override
	public HasValue<String> getFileKeyList() {
		return fileKeyList;
	}

	@Override
	public void hideTracesCountPanel() {
		tracesCountPanel.setVisible(false);
	}

	@Override
	public HasClickHandlers getOkButton() {
		return okButton;
	}

	@Override
	public ITraceTable getTraceTable() {
		return traceTableWidget;
	}

}
