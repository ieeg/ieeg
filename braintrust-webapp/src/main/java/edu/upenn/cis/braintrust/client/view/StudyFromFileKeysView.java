/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.IHasEnabledAndClickHandlers;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.StudyFromFileKeysPresenter.IStudyFromFileKeysView;

/**
 * 
 * 
 * @author John Frommeyer
 * 
 */
public class StudyFromFileKeysView extends Composite
		implements IStudyFromFileKeysView {

	private static StudyFromFileKeysViewUiBinder uiBinder = GWT
			.create(StudyFromFileKeysViewUiBinder.class);

	interface StudyFromFileKeysViewUiBinder extends
			UiBinder<Widget, StudyFromFileKeysView> {}

	@UiField
	VerticalPanel inputPanel;

	@UiField
	IntegerBox ageAtAdmission;

	@UiField
	TextBox mefDirBox;
	@UiField
	Label mefDirErrorMessage;
	private final IHasValueAndErrorMessage<String> mefDirView;

	@UiField
	Button fileKeyLookupButton;
	private final ButtonWrapper fileKeyLookupButtonWrapper;

	@UiField
	Button createStudyButton;
	private final ButtonWrapper createStudyButtonWrapper;

	@UiField
	HTML createStudyErrorMessage;

	@UiField
	TextArea fileKeyTextArea;

	@UiField
	VerticalPanel resultsPanel;
	@UiField
	HTML createStudyResultsMessage;
	@UiField
	Button goAgainButton;
	@UiField
	Button editPatientButton;

	/**
	 * Create a new menu screen.
	 * 
	 */
	public StudyFromFileKeysView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		fileKeyTextArea.setCharacterWidth(150);
		fileKeyTextArea.setVisibleLines(25);
		mefDirBox.setVisibleLength(45);
		mefDirView = new TextBoxWithError(mefDirBox, mefDirErrorMessage);
		createStudyButtonWrapper = new ButtonWrapper(
				createStudyButton);
		fileKeyLookupButtonWrapper = new ButtonWrapper(fileKeyLookupButton);
	}

	@Override
	public IHasEnabledAndClickHandlers getCreateStudyButton() {
		return createStudyButtonWrapper;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void setAndShowCreateStudyErrorMessage(String string) {
		ViewUtil.setAndShowErrorMessage(createStudyErrorMessage, string);
	}

	@Override
	public void hideCreateStudyErrorMessage() {
		ViewUtil.hideErrorMessage(createStudyErrorMessage);
	}

	@Override
	public HasValue<String> getFileKeys() {
		return fileKeyTextArea;
	}

	@Override
	public void setAndShowCreateStudyErrorMessageList(
			List<String> stringList) {
		final StringBuilder sb = new StringBuilder();
		for (final String error : stringList) {
			sb.append(SafeHtmlUtils.htmlEscape(error));
			sb.append("<br />");
		}
		createStudyErrorMessage.setHTML(sb.toString());
		createStudyErrorMessage.setVisible(true);
	}

	@Override
	public HandlerRegistration addFileKeyKeyDownHandler(KeyDownHandler handler) {
		return fileKeyTextArea.addKeyDownHandler(handler);
	}

	@Override
	public HasHTML getCreateStudyResultsMessage() {
		return createStudyResultsMessage;
	}

	@Override
	public void hideInputShowResults() {
		inputPanel.setVisible(false);
		fileKeyLookupButton.setEnabled(false);
		goAgainButton.setEnabled(true);
		editPatientButton.setEnabled(true);
		resultsPanel.setVisible(true);
	}

	@Override
	public HasClickHandlers getGoAgainButton() {
		return goAgainButton;
	}

	@Override
	public HasClickHandlers getEditPatientButton() {
		return editPatientButton;
	}

	@Override
	public void setEditPatientButtonText(String text) {
		editPatientButton.setText(text);
	}

	@Override
	public void showInputHideResults() {
		resultsPanel.setVisible(false);
		goAgainButton.setEnabled(false);
		editPatientButton.setEnabled(false);
		createStudyButton.setEnabled(true);
		fileKeyLookupButton.setEnabled(true);
		inputPanel.setVisible(true);
	}

	@Override
	public HasValue<Integer> getAgeAdmission() {
		return ageAtAdmission;
	}

	@Override
	public IHasEnabledAndClickHandlers getFileKeyLookupButton() {
		return fileKeyLookupButtonWrapper;
	}

	@Override
	public IHasValueAndErrorMessage<String> getMefDir() {
		return mefDirView;
	}
}
