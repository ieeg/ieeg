/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static edu.upenn.cis.braintrust.client.view.ViewUtil.hideErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setAndShowErrorMessage;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IOrganizationTable;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionListAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ManagedEnumPresenter.IManagedEnumView;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;

/**
 * The main data entry menu
 * 
 * @author John Frommeyer
 * 
 */
public class DataEntryMenuView extends Composite
		implements
		edu.upenn.cis.braintrust.client.presenter.DataEntryMenuPresenter.IDataEntryMenuView {

	private static DataEntryMenuViewUiBinder uiBinder = GWT
			.create(DataEntryMenuViewUiBinder.class);

	interface DataEntryMenuViewUiBinder extends
			UiBinder<Widget, DataEntryMenuView> {}

	@UiField
	Button createPatientButton;

	@UiField
	TextBox patientId;

	@UiField
	Button retrievePatientButton;

	@UiField
	Label retrievePatientErrorMessage;

	@UiField
	TextBox newAnimalLabelBox;

	@UiField
	ListBox newAnimalOrganizationListBox;

	@UiField
	SpeciesStrainWidget speciesStrain;

	@UiField
	Button createAnimalButton;

	@UiField
	Label createAnimalErrorMessage;

	@UiField
	TextBox animalId;

	@UiField
	Button retrieveAnimalButton;

	@UiField
	Label retrieveAnimalErrorMessage;

	@UiField
	TextBox createSpeciesBox;

	@UiField
	Label createSpeciesErrorMessage;

	@UiField
	Button createSpeciesButton;

	@UiField
	ListBox speciesListBox;

	@UiField
	Button editSpeciesButton;

	@UiField
	Button deleteSpeciesButton;

	@UiField
	Label editSpeciesErrorMessage;
	
	@UiField(provided = true)
	OrganizationTableWidget orgWidget;

	@UiField(provided = true)
	ManagedEnumView<GwtDrug> drugWidget;

	@UiField(provided = true)
	ManagedEnumView<GwtStimRegion> stimRegionWidget;

	@UiField(provided = true)
	ManagedEnumView<GwtStimType> stimTypeWidget;

	private final ISingleSelectionListAndErrorMessage editSpeciesView;
	private final IHasValueAndErrorMessage<String> createSpeciesView;
	private final ISingleSelectionList newAnimalOrganizationView;

	/**
	 * Create a new menu screen.
	 * 
	 */
	public DataEntryMenuView() {
		orgWidget = new OrganizationTableWidget();
		drugWidget = new ManagedEnumView<GwtDrug>(newDrugTable());
		stimRegionWidget = new ManagedEnumView<GwtStimRegion>(
				newStimRegionTable());
		stimTypeWidget = new ManagedEnumView<GwtStimType>(newStimTypeTable());
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		patientId.setText("");
		animalId.setText("");
		createSpeciesBox.setText("");
		newAnimalLabelBox.setText("");
		newAnimalOrganizationView = new SingleSelectionListBoxView(
				newAnimalOrganizationListBox);
		editSpeciesView = new SingleSelectionListBoxAndErrorMessageView(
				speciesListBox, editSpeciesErrorMessage);
		createSpeciesView = new TextBoxWithError(createSpeciesBox,
				createSpeciesErrorMessage);
	}

	@Override
	public HasClickHandlers getCreatePatientButton() {
		return createPatientButton;
	}

	@Override
	public HasClickHandlers getCreateAnimalButton() {
		return createAnimalButton;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public HasValue<String> getPatientId() {
		return patientId;
	}

	@Override
	public HasValue<String> getAnimalId() {
		return animalId;
	}

	@Override
	public HasClickHandlers getRetrievePatientButton() {
		return retrievePatientButton;
	}

	@Override
	public HasClickHandlers getRetrieveAnimalButton() {
		return retrieveAnimalButton;
	}

	@Override
	public void setAndShowRetrievePatientErrorMessage(String message) {
		setAndShowErrorMessage(retrievePatientErrorMessage, message);
	}

	@Override
	public void setAndShowRetrieveAnimalErrorMessage(String message) {
		setAndShowErrorMessage(retrieveAnimalErrorMessage, message);
	}

	@Override
	public void addRetrievePatientKeyDownHandler(KeyDownHandler handler) {
		patientId.addKeyDownHandler(handler);
	}

	@Override
	public void addRetrieveAnimalKeyDownHandler(KeyDownHandler handler) {
		animalId.addKeyDownHandler(handler);
	}

	@Override
	public void hideRetrievePatientErrorMessage() {
		hideErrorMessage(retrievePatientErrorMessage);
	}

	@Override
	public void hideRetrieveAnimalErrorMessage() {
		hideErrorMessage(retrieveAnimalErrorMessage);
	}

	@Override
	public ISingleSelectionListAndErrorMessage getEditSpeciesView() {
		return editSpeciesView;
	}

	@Override
	public HasClickHandlers getCreateSpeciesButton() {
		return createSpeciesButton;
	}

	@Override
	public IHasValueAndErrorMessage<String> getCreateSpeciesView() {
		return createSpeciesView;
	}

	@Override
	public HasClickHandlers getEditSpeciesButton() {
		return editSpeciesButton;
	}

	@Override
	public HasClickHandlers getDeleteSpeciesButton() {
		return deleteSpeciesButton;
	}

	private static CellTable<GwtDrug> newDrugTable() {
		final CellTable<GwtDrug> cellTable = new CellTable<GwtDrug>(20,
				KeyProviderMap.getMap().getProvider(GwtDrug.class));
		cellTable.setWidth("20%", false);
		cellTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellTable
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		return cellTable;
	}

	private static CellTable<GwtStimRegion> newStimRegionTable() {
		final CellTable<GwtStimRegion> cellTable = new CellTable<GwtStimRegion>(
				20,
				KeyProviderMap.getMap().getProvider(GwtStimRegion.class));
		cellTable.setWidth("20%", false);
		cellTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellTable
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		return cellTable;
	}

	private static CellTable<GwtStimType> newStimTypeTable() {
		final CellTable<GwtStimType> cellTable = new CellTable<GwtStimType>(20,
				KeyProviderMap.getMap().getProvider(GwtStimType.class));
		cellTable.setWidth("20%", false);
		cellTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellTable
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		return cellTable;
	}

	@Override
	public IManagedEnumView<GwtDrug> getDrugView() {
		return drugWidget;
	}

	@Override
	public IManagedEnumView<GwtStimRegion> getStimRegionView() {
		return stimRegionWidget;
	}

	@Override
	public IManagedEnumView<GwtStimType> getStimTypeView() {
		return stimTypeWidget;
	}

	@Override
	public HasValue<String> getNewAnimalLabel() {
		return newAnimalLabelBox;
	}

	@Override
	public void setAndShowCreateAnimalErrorMessage(String string) {
		setAndShowErrorMessage(createAnimalErrorMessage, string);
	}

	@Override
	public ISingleSelectionList getNewAnimalSpeciesView() {
		return speciesStrain.getSpeciesView();
	}

	@Override
	public ISingleSelectionList getNewAnimalStrainView() {
		return speciesStrain.getStrainView();
	}

	@Override
	public void hideCreateAnimalErrorMessage() {
		hideErrorMessage(createAnimalErrorMessage);
	}

	@Override
	public void addNewAnimalLabelKeyDownHandler(KeyDownHandler handler) {
		newAnimalLabelBox.addKeyDownHandler(handler);
	}

	@Override
	public ISingleSelectionList getNewAnimalOrganizationView() {
		return newAnimalOrganizationView;
	}

	@Override
	public IOrganizationTable getOrganizationView() {
		return orgWidget;
	}
}
