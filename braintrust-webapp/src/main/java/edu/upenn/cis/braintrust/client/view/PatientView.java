/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.collect.Lists.newArrayList;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.hideErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setAndShowErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setListBoxChoices;

import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.IHasValue;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndSuggestions;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;
import edu.upenn.cis.braintrust.client.presenter.PatientPresenter;

/**
 * A GwtPatient screen.
 * 
 * @author John Frommeyer
 * 
 */
public class PatientView extends Composite implements
		PatientPresenter.IPatientView {
	private static PatientViewUiBinder uiBinder = GWT
			.create(PatientViewUiBinder.class);

	interface PatientViewUiBinder extends UiBinder<Widget, PatientView> {}

	@UiField
	ListBox organization;
	@UiField
	TextBox patientLabel;
	@UiField
	Label patientLabelErrorMessage;
	@UiField
	IntegerBox ageAtOnset;
	@UiField
	Label ageAtOnsetErrorMessage;

	private final IHasValueAndErrorMessage<Integer> ageAtOnsetView;

	@UiField
	SuggestBox etiologyBox;
	@UiField
	Label etiologyErrorMessage;

	private final IHasValueAndSuggestions<String> etiologyView;

	@UiField
	TextBox devDelayBox;

	private final IHasValue<String> devDelayView;

	@UiField
	ListBox devDisorder;

	private final IHasValue<Boolean> devDisorderView;

	@UiField
	ListBox brainInjury;

	private final IHasValue<Boolean> brainInjuryView;

	@UiField
	ListBox familyHistory;

	private final IHasValue<Boolean> familyHistoryView;

	@UiField
	ListBox genderBox;
	@UiField
	ListBox ethnicityBox;
	@UiField
	ListBox handednessBox;
	@UiField
	CheckBoxList seizureTypeBox;
	@UiField
	CheckBoxList situationBox;
	@UiField
	Button saveButton;
	@UiField
	Button deleteButton;
	@UiField
	Button cancelButton;
	@UiField
	FlexTable hospitalAdmissionTable;
	@UiField
	Button addHospitalAdmissionButton;
	@UiField
	Label addHospitalAdmissionErrorMessage;

	private final List<Long> admissionIds = newArrayList();
	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}
	
	private final ISingleSelectionList organizationView;
	/**
	 * Create the patient view.
	 * 
	 */
	public PatientView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Basic Data Panel
		organizationView = new SingleSelectionListBoxView(organization);
		patientLabel.setText("");
		patientLabelErrorMessage.setVisible(false);
		ageAtOnset.setText("");
		ageAtOnsetErrorMessage.setText("");
		ageAtOnsetErrorMessage.setVisible(false);
		ageAtOnsetView = new IntegerBoxWithError(ageAtOnset,
				ageAtOnsetErrorMessage);
		etiologyBox.setText("unknown");
		etiologyErrorMessage.setText("");
		etiologyErrorMessage.setVisible(false);
		etiologyView = new SuggestBoxView(etiologyBox, etiologyErrorMessage);
		devDelayBox.setText("");
		devDelayView = new TextBoxWithError(devDelayBox, null);
		devDisorderView = new BooleanListBoxView(devDisorder);
		brainInjuryView = new BooleanListBoxView(brainInjury);
		familyHistoryView = new BooleanListBoxView(familyHistory);

		// Epilepsy Case panel
		hospitalAdmissionTable.getRowFormatter().addStyleName(0,
				css.flexTableHeader());
		hospitalAdmissionTable.getColumnFormatter().addStyleName(2,
				css.flexTableButtonColumn());
		hospitalAdmissionTable.setText(0, 0, "Hospital Admission");
		hospitalAdmissionTable.setText(0, 1, "Edit");
		hospitalAdmissionTable.setText(0, 2, "Remove");
		addHospitalAdmissionErrorMessage.setVisible(false);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSaveButton()
	 */
	@Override
	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setGenderChoices(String[])
	 */
	@Override
	public void setGenderChoices(String[] genders) {
		setListBoxChoices(genderBox, genders);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setHandednessChoices(String[])
	 */
	@Override
	public void setHandednessChoices(String[] handednesses) {
		setListBoxChoices(handednessBox, handednesses);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getPatientLabel()
	 */
	@Override
	public HasValue<String> getPatientLabel() {
		return patientLabel;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setEthnicityChoices(java.lang.String[])
	 */
	@Override
	public void setEthnicityChoices(String[] ethnicities) {
		setListBoxChoices(ethnicityBox, ethnicities);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSelectedEthnicity()
	 */
	@Override
	public int getSelectedEthnicity() {
		return ethnicityBox.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSelectedGender()
	 */
	@Override
	public int getSelectedGender() {
		return genderBox.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSelectedHandedness()
	 */
	@Override
	public int getSelectedHandedness() {
		return handednessBox.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#addPatientLabelKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addPatientLabelKeyPressHandler(
			KeyPressHandler handler) {
		return patientLabel.addKeyPressHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setAndShowPatientLabelErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowPatientLabelErrorMessage(String errorMessage) {
		patientLabelErrorMessage.setText(errorMessage);
		patientLabelErrorMessage.setVisible(true);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#hidePatientLabelErrorMessage()
	 */
	@Override
	public void hidePatientLabelErrorMessage() {
		patientLabelErrorMessage.setVisible(false);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getDeleteButton()
	 */
	@Override
	public HasClickHandlers getDeleteButton() {
		return deleteButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSituationChoices(java.lang.String[])
	 */
	@Override
	public void setSituationChoices(String[] situations) {
		situationBox.setChoices(situations);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSelectedSituationRows()
	 */
	@Override
	public Set<Integer> getSelectedSituationRows() {
		return situationBox.getSelectedIndices();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getCancelButton()
	 */
	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSelectedGender(int)
	 */
	@Override
	public void setSelectedGender(int i) {
		genderBox.setSelectedIndex(i);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSelectedEthnicity(int)
	 */
	@Override
	public void setSelectedEthnicity(int i) {
		ethnicityBox.setSelectedIndex(i);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSelectedHandedness(int)
	 */
	@Override
	public void setSelectedHandedness(int i) {
		handednessBox.setSelectedIndex(i);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#addHospitalAdmission(Long,
	 *      java.lang.String)
	 */
	@Override
	public EditRemoveButtonPair addHospitalAdmission(Long id, String description, boolean removable) {
		if (admissionIds.contains(id)) {
			return null;
		}
		admissionIds.add(id);
		int row = hospitalAdmissionTable.getRowCount();
		hospitalAdmissionTable.setText(row, 0, description);
		Button remove = new Button("x");
		if (!removable) {
			remove.setEnabled(false);
			remove.setTitle("Contains unremovable studies.");
		}
		remove.addStyleDependentName(css.gwtButtonRemove());
		Button edit = new Button("Edit");
		hospitalAdmissionTable.setWidget(row, 1, edit);
		hospitalAdmissionTable.setWidget(row, 2, remove);
		return new EditRemoveButtonPair(edit, remove);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getAddHospitalAdmissionButton()
	 */
	@Override
	public HasClickHandlers getAddHospitalAdmissionButton() {
		return addHospitalAdmissionButton;
	}

		/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#hideAddHospitalAdmissionErrorMessage()
	 */
	@Override
	public void hideAddHospitalAdmissionErrorMessage() {
		hideErrorMessage(addHospitalAdmissionErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setAndShowAddHospitalAdmissionErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowAddHospitalAdmissionErrorMessage(String errorMessage) {
		setAndShowErrorMessage(addHospitalAdmissionErrorMessage, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getSelectedSeizureTypeRows()
	 */
	@Override
	public Set<Integer> getSelectedSeizureTypeRows() {
		return seizureTypeBox.getSelectedIndices();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSeizureTypeChoices(java.lang.String[])
	 */
	@Override
	public void setSeizureTypeChoices(String[] seizureTypes) {
		seizureTypeBox.setChoices(seizureTypes);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSeizureTypeRowSelected(int,
	 *      boolean)
	 */
	@Override
	public void setSeizureTypeRowSelected(int row, boolean selected) {
		seizureTypeBox.setItemSelected(row, selected);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#setSituationRowSelected(int,
	 *      boolean)
	 */
	@Override
	public void setSituationRowSelected(int row, boolean selected) {
		situationBox.setItemSelected(row, selected);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getAgeAtOnsetView()
	 */
	@Override
	public IHasValueAndErrorMessage<Integer> getAgeAtOnsetView() {
		return ageAtOnsetView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getEtiologyView()
	 */
	@Override
	public IHasValueAndSuggestions<String> getEtiologyView() {
		return etiologyView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getDevelopmentalDelayView()
	 */
	@Override
	public IHasValue<String> getDevelopmentalDelayView() {
		return devDelayView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getDevelopmentalDisorderView()
	 */
	@Override
	public IHasValue<Boolean> getDevelopmentalDisorderView() {
		return devDisorderView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getTraumaticBrainInjuryView()
	 */
	@Override
	public IHasValue<Boolean> getTraumaticBrainInjuryView() {
		return brainInjuryView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#getFamilyHistoryView()
	 */
	@Override
	public IHasValue<Boolean> getFamilyHistoryView() {
		return familyHistoryView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.PatientPresenter.IPatientView#removeHospitalAdmission(java.lang.Long)
	 */
	@Override
	public void removeHospitalAdmission(Long hospitalAdmissionId) {
		int arrayRow = admissionIds.indexOf(hospitalAdmissionId);
		admissionIds.remove(arrayRow);
		hospitalAdmissionTable.removeRow(arrayRow + 1);

	}

	@Override
	public ISingleSelectionList getOrganizationView() {
		return organizationView;
	}

}
