/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Maps.newTreeMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.IlaeRating;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Pathology;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;

/**
 * For choices in lists. Use *_CHOICES for setting the choices in a list.
 * Utility methods for going from selected indices to the corresponding enums.
 * Utility methods for going from enums to their corresponding indices.
 * 
 * @author Sam Donnelly
 */
public class ListChoices {

	public static enum ElectrodeType {
		DEPTH("Depth"),
		SUBDURAL("Subdural"),
		SURFACE("Surface");

		private final String displayString;

		/**
		 * Maps a {@code EegStudyType}'s toString representation to the
		 * {@code EegStudyType} itself.
		 */
		public static final SortedMap<String, ElectrodeType> fromString;
		static {
			final SortedMap<String, ElectrodeType> temp = newTreeMap();
			for (final ElectrodeType type : values()) {
				temp.put(type.toString(), type);
			}
			fromString = Collections.unmodifiableSortedMap(temp);
		}

		private ElectrodeType(final String displayString) {
			this.displayString = displayString;
		}

		@Override
		public String toString() {
			return displayString;
		}

	}

	/** The display strings for all image type choices. */
	public static final String[] IMAGE_TYPE_CHOICES = ImageType.fromString
			.keySet().toArray(new String[0]);

	/** The display strings for the sides. */
	public static final String[] SIDE_CHOICES = Side.fromString.keySet()
			.toArray(new String[0]);

	/** The display strings for the locations. */
	public static final String[] LOCATION_CHOICES = Location.fromString
			.keySet()
			.toArray(new String[0]);

	/** The display strings for all postop ratings. */
	public static final String[] POSTOP_RATING_SCALE_CHOICES = IlaeRating.FROM_STRING
			.keySet().toArray(new String[0]);

	/** The display strings for all seizure type choices. */
	public static final String[] SZ_TYPE_CHOICES = new String[] {
			SeizureType.ABSENCE.toString(), SeizureType.TONIC.toString(),
			SeizureType.LONIC.toString(), SeizureType.MYOCLONIC.toString(),
			SeizureType.ATONIC.toString(), SeizureType.TONIC_CLONIC.toString(),
			SeizureType.SIMPLE.toString(), SeizureType.COMPLEX.toString(),
			SeizureType.PARTIAL_WITH_2NDARY_GENERALIZATION.toString(),
			SeizureType.NON_EPILEPTIC.toString() };

	public static final String[] GENDER_CHOICES = Gender.fromString
			.keySet().toArray(new String[0]);

	/** The display strings for all pathology choices. */
	public static final String[] PATHOLOGY_CHOICES = Pathology.fromString
			.keySet().toArray(new String[Pathology.values().length]);

	/** The display strings for all electrode info type choices. */
	public static final String[] ELECTRODE_TYPE_CHOICES = ListChoices.ElectrodeType.fromString
			.keySet().toArray(new String[0]);

	/** The display strings for all EegStudyType choices. */
	public static final String[] EEG_STUDY_TYPE_CHOICES = EegStudyType.fromString
			.keySet().toArray(new String[0]);

	public static final String[] CONTACT_TYPE_CHOICES = ContactType.FROM_STRING
			.keySet().toArray(new String[0]);

	public static final String[] EPILEPSY_TYPE_CHOICES = EpilepsyType.FROM_STRING
			.keySet().toArray(new String[0]);

	public static final String[] ETIOLOGY_CHOICES = Etiology.FROM_STRING
			.keySet().toArray(new String[0]);

	/**
	 * Returns a set of the corresponding indices.
	 * 
	 * @param szTypes
	 * @return
	 */
	public static <E extends Enum<?>> Set<Integer> enums2Indices(
			final Set<E> enums, final String[] choices,
			final Map<String, E> stringsToEs) {
		final Set<Integer> indices = newHashSet();
		for (int i = 0; i < choices.length; i++) {
			if (enums.contains(stringsToEs.get(choices[i]))) {
				indices.add(i);
			}
		}
		return indices;
	}

	public static <E extends Enum<?>> Set<E> indices2Enums(
			final Set<Integer> selectedIndices, final String[] choices,
			final Map<String, E> stringsToEs) {
		final Set<E> selectedEnums = newHashSet();
		for (final Integer i : selectedIndices) {
			final String typeString = choices[i.intValue()];
			final E anEnum = stringsToEs.get(typeString);
			selectedEnums.add(anEnum);
		}
		return selectedEnums;
	}

	public static Set<Location> indices2Locations(
			final Set<Integer> selectedIndices) {
		final Set<Location> selectedEnums = newHashSet();
		for (final Integer i : selectedIndices) {
			final String typeString = LOCATION_CHOICES[i.intValue()];
			final Location anEnum = Location.fromString.get(typeString);
			selectedEnums.add(anEnum);
		}
		return selectedEnums;
	}

	public static Set<Side> indices2Sides(final Set<Integer> selectedIndices) {
		final Set<Side> selectedEnums = newHashSet();
		for (final Integer i : selectedIndices) {
			final String typeString = SIDE_CHOICES[i.intValue()];
			final Side anEnum = Side.fromString.get(typeString);
			selectedEnums.add(anEnum);
		}
		return selectedEnums;
	}

	public static Set<IlaeRating> indices2SiPostopScales(
			final Set<Integer> selectedIndices) {
		final Set<IlaeRating> selectedScales = newHashSet();
		for (final Integer i : selectedIndices) {
			final String scaleString = POSTOP_RATING_SCALE_CHOICES[i.intValue()];
			final IlaeRating type = IlaeRating.FROM_STRING
					.get(scaleString);
			selectedScales.add(type);
		}
		return selectedScales;
	}

	public static Set<SeizureType> indices2SzTypes(
			final Set<Integer> selectedSzTypeIndices) {
		final Set<SeizureType> selectedSzTypes = newHashSet();
		for (final Integer i : selectedSzTypeIndices) {
			final String szTypeString = SZ_TYPE_CHOICES[i.intValue()];
			final SeizureType szType = SeizureType.fromString.get(szTypeString);
			selectedSzTypes.add(szType);
		}
		return selectedSzTypes;
	}

	/** Prevent inheritance and instantiation. */
	private ListChoices() {
		throw new AssertionError("can't instantiate a " + "DisplayStrings");
	}
}
