/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.SpeciesPresenter.ISpeciesView;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;

public class SpeciesView extends Composite implements ISpeciesView {

	private static SpeciesViewUiBinder uiBinder = GWT
			.create(SpeciesViewUiBinder.class);

	interface SpeciesViewUiBinder extends UiBinder<Widget, SpeciesView> {}

	/**
	 * The strain CellTable.
	 */
	@UiField(provided = true)
	CellTable<GwtStrain> strainTable;

	@UiField
	Label modifyStrainErrorMessage;

	/**
	 * The pager used to change the range of data.
	 */
	@UiField(provided = true)
	SimplePager pager;

	@UiField
	TextBox speciesLabelBox;
	@UiField
	Label speciesLabelErrorMessage;
	@UiField
	Button modifySpeciesLabelButton;

	@UiField
	TextBox addStrainBox;
	@UiField
	Button addStrainButton;
	@UiField
	Label addStrainErrorMessage;

	private final IHasValueAndErrorMessage<String> speciesLabelView;
	private final IHasValueAndErrorMessage<String> addStrainView;
	private final ListDataProvider<GwtStrain> strainProvider = new ListDataProvider<GwtStrain>();
	private final EditTextCell strainCell = new EditTextCell();
	private final Column<GwtStrain, String> strainColumn = new Column<GwtStrain, String>(
			strainCell) {

		@Override
		public String getValue(GwtStrain object) {
			return object.getLabel();
		}
	};

	private final Column<GwtStrain, String> deleteStrainColumn = new Column<GwtStrain, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtStrain object) {
			return "x";
		}
	};
	private final IHasErrorMessage modifyStrainLabelError;

	public SpeciesView() {
		// Instantiate provided elements before calling uiBinder.createAndBindUi
		strainTable = new CellTable<GwtStrain>(20, KeyProviderMap.getMap().getProvider(GwtStrain.class));
		strainTable.setTitle("strains for species");
		strainTable.setWidth("20%", false);
		strainTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		strainTable
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		strainTable.addColumn(strainColumn, "Strains");
		strainTable.addColumn(deleteStrainColumn);
		strainProvider.addDataDisplay(strainTable);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0,
				true);
		pager.setDisplay(strainTable);

		initWidget(uiBinder.createAndBindUi(this));
		speciesLabelBox.setText("");
		addStrainBox.setText("");
		speciesLabelView = new TextBoxWithError(speciesLabelBox,
				speciesLabelErrorMessage);
		addStrainView = new TextBoxWithError(addStrainBox,
				addStrainErrorMessage);
		modifyStrainLabelError = new ErrorMessageLabel(
				modifyStrainErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {

		return this;
	}

	@Override
	public HasClickHandlers getModifySpeciesLabelButton() {
		return modifySpeciesLabelButton;
	}

	@Override
	public IHasValueAndErrorMessage<String> getModifySpeciesLabelView() {
		return speciesLabelView;
	}

	@Override
	public List<GwtStrain> getStrainList() {
		return strainProvider.getList();
	}

	@Override
	public IHasValueAndErrorMessage<String> getAddStrainView() {
		return addStrainView;
	}

	@Override
	public HasClickHandlers getAddStrainButton() {
		return addStrainButton;
	}

	@Override
	public void setStrainUpdater(FieldUpdater<GwtStrain, String> strainUpdater) {
		strainColumn.setFieldUpdater(strainUpdater);
	}

	@Override
	public IHasErrorMessage getModifyStrainErrorMessage() {
		return modifyStrainLabelError;
	}

	@Override
	public void refresh() {
		strainProvider.refresh();
	}

	@Override
	public void redraw() {
		strainTable.redraw();
	}

	@Override
	public void clearStrainViewData(GwtStrain object) {
		strainCell.clearViewData(strainTable.getKeyProvider().getKey(object));
	}

	@Override
	public void setStrainDeleter(FieldUpdater<GwtStrain, String> strainDeleter) {
		deleteStrainColumn.setFieldUpdater(strainDeleter);
	}

}
