/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;

/**
 * Two list boxes to display species and strain.
 * 
 * @author John Frommeyer
 * 
 */
public class SpeciesStrainWidget extends Composite {

	private static SpeciesStrainWidgetUiBinder uiBinder = GWT
			.create(SpeciesStrainWidgetUiBinder.class);

	interface SpeciesStrainWidgetUiBinder extends
			UiBinder<Widget, SpeciesStrainWidget> {}

	@UiField
	ListBox speciesBox;

	@UiField
	ListBox strainBox;

	private final ISingleSelectionList speciesView;
	private final ISingleSelectionList strainView;

	public SpeciesStrainWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		speciesView = new SingleSelectionListBoxView(speciesBox);
		strainView = new SingleSelectionListBoxView(strainBox);
	}

	public ISingleSelectionList getSpeciesView() {
		return speciesView;
	}

	public ISingleSelectionList getStrainView() {
		return strainView;
	}
}
