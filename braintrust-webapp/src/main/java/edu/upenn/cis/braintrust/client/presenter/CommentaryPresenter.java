/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.nullToEmpty;

import com.google.common.base.Objects;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter.IAnimalView;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class CommentaryPresenter implements IPresenter, ClickHandler,
		KeyUpHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface ICommentaryView extends IView, IDialogBox, IHasSaveButton,
			IHasKeyUpHandlers,
			IHasCancelButton {
		public static final int MAX_COMMENTARY_LENGTH = 4000;

		public HasValue<String> getCommentary();

		void setCommentaryLabel(String text);

	}

	private final ICommentaryView display;
	private final BrainTrustServiceAsync service;
	private final IHasRecordingParent<GwtExperiment> parentPresenter;
	private final IAnimalView callbackView;
	private final IRecordingFactory<String> factory = new RecordingFactoryFromCommentary();

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param parentPresenter
	 * @param electrodeInfoStore
	 * @param experiment
	 */
	public CommentaryPresenter(
			final ICommentaryView display,
			final BrainTrustServiceAsync rpcService,
			IHasRecordingParent<GwtExperiment> parentPresenter,
			IAnimalView callbackView) {
		this.display = display;
		this.service = rpcService;
		this.parentPresenter = parentPresenter;
		this.callbackView = callbackView;
		bind();
		populateDisplay();
	}

	private void populateDisplay() {
		final GwtRecording recording = callbackView.getRecordingView()
				.getRecording();
		final String initialCommentary = nullToEmpty(recording.getCommentary());
		display.getCommentary().setValue(initialCommentary);
		display.setCommentaryLabel(initialCommentary.length() + "/"
				+ ICommentaryView.MAX_COMMENTARY_LENGTH);
	}

	private void bind() {
		display.getSaveButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);
		display.addKeyUpHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		display.show();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getSaveButton()) {
			onSave();
		} else if (source == display.getCancelButton()) {
			display.hide();
		}
	}

	private void onSave() {

		final String newCommentary = emptyToNull(display.getCommentary()
				.getValue().trim());
		final GwtRecording recording = callbackView.getRecordingView()
				.getRecording();
		final String existingCommentary = recording.getCommentary();
		if (Objects.equal(newCommentary, existingCommentary)) {
			display.hide();
			return;
		}
		if (newCommentary.length() > ICommentaryView.MAX_COMMENTARY_LENGTH) {
			Window.alert("Commentary must be less than "
					+ ICommentaryView.MAX_COMMENTARY_LENGTH
					+ " characters in length.");
		}
		final GwtExperiment experiment = parentPresenter.getRecordingParent(
				recording).orNull();
		final GwtRecording modifiedRecording = factory.newInstance(recording,
				newCommentary);
		service.modifyRecording(experiment, modifiedRecording,
				new AsyncCallback<GwtRecording>() {

					@Override
					public void onSuccess(GwtRecording result) {
						parentPresenter.updateRecording(result);
						callbackView.getRecordingView().getRecordingTable()
								.setRecording(result);
						callbackView.getRecordingView().getRecordingTable()
								.refreshProvider();
						display.hide();
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("An error occured while updating the recording commentary.");
					}
				});
	}

	public void onKeyUp(KeyUpEvent event) {
		final Object source = event.getSource();
		if (display.isWrapping(source)) {
			final int length = display.getCommentary().getValue().length();
			display.setCommentaryLabel(length + "/"
					+ ICommentaryView.MAX_COMMENTARY_LENGTH);
		}
	}

}
