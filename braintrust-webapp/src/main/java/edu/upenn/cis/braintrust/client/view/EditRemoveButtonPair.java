/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.event.dom.client.HasClickHandlers;

/**
 * An pair of buttons, one Edit and one Remove.
 * @author John Frommeyer
 *
 */
public class EditRemoveButtonPair {

	private final HasClickHandlers editButton;
	private final HasClickHandlers removeButton;

	/**
	 * Creates a button pair.
	 * 
	 * @param editButton
	 * @param removeButton
	 */
	public EditRemoveButtonPair(HasClickHandlers editButton,
			HasClickHandlers removeButton) {
		this.editButton = editButton;
		this.removeButton = removeButton;
	}

	/**
	 * Returns this pair's edit button.
	 * 
	 * @return this pair's edit button
	 */
	public HasClickHandlers getEditButton() {
		return editButton;
	}

	/**
	 * Returns this pair's remove button.
	 * 
	 * @return this pair's remove button
	 */
	public HasClickHandlers getRemoveButton() {
		return removeButton;
	}
	
}
