/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.client.event.EditStudyEvent;
import edu.upenn.cis.braintrust.client.presenter.CommonElectrodePresenter.ICommonElectrodeView;
import edu.upenn.cis.braintrust.client.view.TracesView;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * Controls a {@code IElectrodeView}.
 * 
 * @author John Frommeyer
 * 
 */
public class ContactGroupPresenter implements IPresenter, ClickHandler,
		KeyPressHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public static interface IContactGroupView extends IView {

		/**
		 * Returns the wrapped {@code ICommonElectrodeView}.
		 * 
		 * @return the wrapped {@code ICommonElectrodeView}
		 */
		ICommonElectrodeView getCommonElectrodeView();

		IHasValueAndErrorMessage<Double> getSamplingRateView();

		IHasValueAndErrorMessage<Double> getLffSettingView();

		IHasValueAndErrorMessage<Double> getHffSettingView();

		IHasValue<Boolean> getNotchFilterView();

		IHasValueAndErrorMessage<String> getChannelPrefixView();

		IHasEnabledAndClickHandlers getSaveButtonView();

		IHasEnabledAndClickHandlers getCancelButtonView();

		HasClickHandlers getTracesButton();

		HasValue<String> getManufacturerView();

		HasValue<String> getModelNoView();

		IHasValueAndErrorMessage<String> getElectrodeTypeView();

		void setTracesButtonActive(boolean active);

		/**
		 * DOCUMENT ME
		 * 
		 */
		void setUnmodifiable();
	}

	private final IContactGroupView display;
	private final GwtEegStudy study;
	private final GwtContactGroup contactGroup;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private final CommonElectrodePresenter commonElectrodePresenter;
	private final boolean isNewContactGroup;

	/**
	 * Creates a {@code ElectrodePresenter}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 * @param study
	 * @param electrodeInfoStore
	 */
	public ContactGroupPresenter(
			final HandlerManager eventBus,
			final IContactGroupView display,
			final BrainTrustServiceAsync service,
			final GwtEegStudy study) {
		this.display = display;
		this.study = study;
		this.eventBus = eventBus;
		this.service = service;
		this.contactGroup = new GwtContactGroup();
		this.isNewContactGroup = true;
		this.commonElectrodePresenter = new CommonElectrodePresenter(
				this.display.getCommonElectrodeView(),
				this.contactGroup,
				ListChoices.SIDE_CHOICES,
				ListChoices.LOCATION_CHOICES,
				true);
		bind();
	}

	/**
	 * 
	 * Creates a {@code ElectrodePresenter} for the given {@code GwtElectrode}.
	 * 
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 * @param study
	 * @param electrodeInfoStore
	 * @param contactGroup
	 */
	public ContactGroupPresenter(
			final HandlerManager eventBus,
			final IContactGroupView display,
			final BrainTrustServiceAsync service,
			final GwtEegStudy study,
			final GwtContactGroup contactGroup) {
		this.display = display;
		this.study = study;
		this.contactGroup = contactGroup;
		this.eventBus = eventBus;
		this.service = service;
		this.isNewContactGroup = false;
		this.commonElectrodePresenter = new CommonElectrodePresenter(
				this.display.getCommonElectrodeView(),
				this.contactGroup,
				ListChoices.SIDE_CHOICES,
				ListChoices.LOCATION_CHOICES,
				true);

		bind();
		populateDisplay();
	}

	private void populateDisplay() {
		commonElectrodePresenter.populateDisplay();
		display.setTracesButtonActive(true);
		display.getChannelPrefixView()
				.setValue(contactGroup.getChannelPrefix());
		display.getHffSettingView()
				.setValue(contactGroup.getHffSetting().orNull());

		display.getLffSettingView()
				.setValue(contactGroup.getLffSetting().orNull());

		display.getNotchFilterView().setValue(
				contactGroup.getNotchFilter().orNull());

		display.getSamplingRateView().setValue(contactGroup.getSamplingRate());
		display.getManufacturerView().setValue(
				contactGroup.getManufacturer().orNull());
		display.getModelNoView().setValue(contactGroup.getModelNo().orNull());
		display.getElectrodeTypeView()
				.setValue(contactGroup.getElectrodeType());
		// if (!study.isModifiable()) {
		// display.setUnmodifiable();
		// }
	}

	private void bind() {
		commonElectrodePresenter.bind();
		display.getSaveButtonView().addClickHandler(this);
		display.getCancelButtonView().addClickHandler(this);
		display.getTracesButton().addClickHandler(this);
		display.getHffSettingView().addKeyPressHandler(this);

		display.getLffSettingView().addKeyPressHandler(this);

		display.getSamplingRateView().addKeyPressHandler(this);
		display.getChannelPrefixView().addKeyPressHandler(this);
		display.getElectrodeTypeView().addKeyPressHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (display.getSaveButtonView().isWrapping(source)) {
			onSave();
		} else if (display.getCancelButtonView().isWrapping(source)) {
			onCancel();
		} else if (source == display.getTracesButton()) {
			onTracesClick();
		}
	}

	private void onTracesClick() {
		if (contactGroup.getId() == null) {
			// this should not happen because the traces should not be enabled
			// in this case.
			Window.alert("You must first save this contact group");
		} else {
			service.findTraces(contactGroup,
					new AsyncCallback<List<GwtTrace>>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Unable to load traces for this contact group.");
						}

						@Override
						public void onSuccess(List<GwtTrace> result) {
							IPresenter presenter = new TracesPresenter(
									new TracesView(
											newArrayList(ContactType.FROM_STRING
													.keySet())),
									service,
									contactGroup,
									study,
									study.isModifiable(),
									result);
							presenter.go(null);
						}
					});
		}
	}

	private void onCancel() {
		eventBus.fireEvent(new EditStudyEvent(study));
	}

	private void onSave() {
		boolean canSave = commonElectrodePresenter.populateModel();
		String channelPrefix = nullToEmpty(
				display.getChannelPrefixView().getValue()).trim();
		if ("".equals(channelPrefix)) {
			canSave = false;
			display.getChannelPrefixView().setAndShowErrorMessage(
					"You must enter a channel prefix.");
		}
		Double hffSetting = display.getHffSettingView().getValue();
		Double lffSetting = display.getLffSettingView().getValue();
		Boolean isNotchFilter = display.getNotchFilterView().getValue();
		String manufacturer = emptyToNull(display.getManufacturerView()
				.getValue());
		String modelNo = emptyToNull(display.getModelNoView().getValue());
		String electrodeType = nullToEmpty(display.getElectrodeTypeView()
				.getValue());
		if ("".equals(electrodeType)) {
			canSave = false;
			display.getElectrodeTypeView().setAndShowErrorMessage(
					"You must enter an electrode type.");
		}
		Double samplingRate = display.getSamplingRateView().getValue();
		if (samplingRate == null) {
			canSave = false;
			display.getSamplingRateView().setAndShowErrorMessage(
					"You must enter a nonnegative value for sampling rate.");
		}
		if (canSave) {
			contactGroup.setChannelPrefix(channelPrefix);
			contactGroup.setElectrodeType(electrodeType);
			contactGroup.setManufacturer(manufacturer);
			contactGroup.setModelNo(modelNo);
			contactGroup.setHffSetting(hffSetting);
			contactGroup.setLffSetting(lffSetting);
			contactGroup.setNotchFilter(isNotchFilter);
			contactGroup.setSamplingRate(samplingRate);
			if (isNewContactGroup) {
				study.getRecording().addContactGroup(contactGroup);
			}
			GwtPatient patient = study.getParent().getParent();
			service.savePatient(patient, new AsyncCallback<GwtPatient>() {

				@Override
				public void onFailure(Throwable caught) {
					if (isNewContactGroup) {
						study.getRecording().removeContactGroup(contactGroup);
					}
					String message = caught.getMessage();
					if (caught instanceof StaleObjectException) {
						message = "This patient has changed since your last save. Please reload. ("
								+ message + ")";
					} else {
						message = "Could not save contact group: " + message;
					}
					Window.alert(message);
				}

				@Override
				public void onSuccess(final GwtPatient savePatientResult) {
					Window.alert("Saved contact group.");
					// If we want to stay on this screen, then we'd have to
					// lookup the array we just saved.
					service.retrieveOrganizations(new AsyncCallback<List<GwtOrganization>>() {

						@Override
						public void onFailure(Throwable caught) {
							if (caught instanceof BrainTrustAuthorizationException) {
								Window.alert("You are not authorized to list organizations.");
							} else {
								Window.alert("Failed to load organizations.");
							}
							eventBus.fireEvent(new DataEntryMenuEvent());
						}

						@Override
						public void onSuccess(
								List<GwtOrganization> retrieveOrgsResult) {
							eventBus.fireEvent(new EditPatientEvent(
									retrieveOrgsResult, savePatientResult));
						}
					});

				}
			});

		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object source = event.getSource();
		if (display.getHffSettingView().isWrapping(source)) {
			display.getHffSettingView().hideErrorMessage();
		} else if (display.getLffSettingView().isWrapping(source)) {
			display.getLffSettingView().hideErrorMessage();
		} else if (display.getSamplingRateView().isWrapping(source)) {
			display.getSamplingRateView().hideErrorMessage();
		} else if (display.getChannelPrefixView().isWrapping(source)) {
			display.getChannelPrefixView().hideErrorMessage();
		} else if (display.getElectrodeTypeView().isWrapping(source)) {
			display.getElectrodeTypeView().hideErrorMessage();
		}
	}

}
