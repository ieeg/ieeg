/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import edu.upenn.cis.braintrust.client.presenter.DrugAdminPresenter.IDrugAdminView;
import edu.upenn.cis.braintrust.client.presenter.IDrugAdminTable;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;

/**
 * A screen to enter information about an experiment.
 * 
 * @author John Frommeyer
 * 
 */
public class DrugAdminView extends Composite implements IDrugAdminView {

	private static DrugAdminUiBinder uiBinder = GWT
			.create(DrugAdminUiBinder.class);

	interface DrugAdminUiBinder extends
			UiBinder<Widget, DrugAdminView> {}

	@UiField(provided = true)
	DrugAdminTableWidget drugAdminTableWidget;

	@UiField
	Button addAdminButton;

	@UiField
	Label addAdminErrorMessage;

	@UiField
	DateBox adminDateBox;

	@UiField
	ListBox drugListBox;
	@UiField
	TextBox doseBox;

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	private final ISingleSelectionList drugList;
	private final IHasValueAndErrorMessage<String> dose;

	/**
	 * Creates this view.
	 * 
	 */
	public DrugAdminView(List<String> drugs) {
		drugAdminTableWidget = new DrugAdminTableWidget(drugs);
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		adminDateBox.setValue(null);
		adminDateBox.setFormat(new DateBox.DefaultFormat(
				IDrugAdminView.ADMIN_TIME_FORMAT));
		doseBox.setValue("");
		drugList = new SingleSelectionListBoxView(drugListBox);
		dose = new TextBoxWithError(doseBox, addAdminErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public IDrugAdminTable getDrugAdminTable() {
		return drugAdminTableWidget;
	}

	@Override
	public HasClickHandlers getAddDrugAdminButton() {
		return addAdminButton;
	}

	@Override
	public void setEnabled(boolean enabled) {
		addAdminButton.setEnabled(enabled);
		drugList.setEnabled(enabled);
	}

	@Override
	public ISingleSelectionList getDrugList() {
		return drugList;
	}

	@Override
	public HasValue<Date> getAdminTime() {
		return adminDateBox;
	}

	@Override
	public IHasValueAndErrorMessage<String> getDose() {
		return dose;
	}

}
