/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.CommentaryPresenter.ICommentaryView;

/**
 * A screen to enter information about an experiment.
 * 
 * @author John Frommeyer
 * 
 */
public class CommentaryView extends Composite implements ICommentaryView {

	private static CommentaryViewUiBinder uiBinder = GWT
			.create(CommentaryViewUiBinder.class);

	interface CommentaryViewUiBinder extends
			UiBinder<DialogBox, CommentaryView> {}

	@UiField
	TextArea commentaryArea;
	@UiField
	Label lengthLabel;
	@UiField
	Button saveButton;
	@UiField
	Button cancelButton;

	private final DialogBox dialogBox;

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Creates this view.
	 * 
	 */
	public CommentaryView() {
		dialogBox = uiBinder.createAndBindUi(this);
		// Can access @UiField after calling createAndBindUi
		commentaryArea.setValue("");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void show() {
		dialogBox.center();
		dialogBox.show();
	}

	@Override
	public void hide() {
		dialogBox.hide();
	}

	@Override
	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	@Override
	public HasValue<String> getCommentary() {
		return commentaryArea;
	}

	@Override
	public void setCommentaryLabel(String text) {
		lengthLabel.setText(text);
	}

	@Override
	public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
		return commentaryArea.addKeyUpHandler(handler);
	}

	@Override
	public boolean isWrapping(Object o) {
		return o == commentaryArea;
	}

}
