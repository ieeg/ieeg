/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.Set;
import java.util.SortedSet;

import com.google.common.collect.ImmutableSortedSet;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

/**
 * Common utility methods for views.
 * 
 * @author John Frommeyer
 * 
 */
class ViewUtil {

	private ViewUtil() {
	// Prevent Instantiation
	}

	static void setListBoxChoices(ListBox listBox, String[] choices) {
		for (String choice : choices) {
			listBox.addItem(choice);
		}
	}

	static void hideErrorMessage(Label errorMessageLabel) {
		if (errorMessageLabel.isVisible()) {
			errorMessageLabel.setVisible(false);
			errorMessageLabel.setText("");
		}
	}

	static void setAndShowErrorMessage(Label errorMessageLabel,
			String errorMessage) {
		errorMessageLabel.setText(errorMessage);
		errorMessageLabel.setVisible(true);
	}

	static String removeLeadingAndTrailingSlashesAndBackSlashes(String str) {
		String result = str.replaceAll("^[\\\\/]+|[\\\\/]+$", "");
		return result;
	}

	
	/**
	 * 
	 * Returns a {@code String} describing {@code set}. If {@code set} is emtpy
	 * we return the empty string.
	 * 
	 * @param set a non-null set of {@code Integer}
	 * @return a {@code String} describing {@code set}
	 */
	static String set2Range(Set<Integer> set) {
		if (!set.isEmpty()) {
			final StringBuffer sb = new StringBuffer();
			final SortedSet<Integer> sortedSet = ImmutableSortedSet.copyOf(set);
			final Integer first = sortedSet.first();
			sb.append(first);
			Integer previous = first;
			final SortedSet<Integer> tail = sortedSet.tailSet(Integer
					.valueOf(first.intValue() + 1));
			boolean inARange = false;
			for (Integer i : tail) {
				// i >= previous + 1
				final int previousValue = previous.intValue();
				if (i.intValue() > previousValue + 1) {
					if (inARange) {
						sb.append("-");
						sb.append(previous);
					}
					sb.append(", ");
					sb.append(i);
					inARange = false;
				} else {// i == previous + 1
					inARange = true;
					if (i.equals(tail.last())) {
						sb.append("-");
						sb.append(i);
					}
				}
				previous = i;
			}
			return sb.toString();
		}
		return "";
	}

}
