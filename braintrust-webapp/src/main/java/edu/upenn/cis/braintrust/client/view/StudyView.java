/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.collect.Lists.newArrayList;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.hideErrorMessage;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.setAndShowErrorMessage;

import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;
import edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView;

/**
 * A screen to enter information about a study.
 * 
 * @author John Frommeyer
 * 
 */
public class StudyView extends Composite implements IStudyView {

	private static StudyViewUiBinder uiBinder = GWT
			.create(StudyViewUiBinder.class);

	interface StudyViewUiBinder extends UiBinder<Widget, StudyView> {}

	@UiField
	UutcDateWidget startDate;

	private final UutcView startDateView;

	@UiField
	UutcDateWidget endDate;
	private final UutcView endDateView;

	@UiField
	CheckBox publishedCheckBox;

	@UiField
	IntegerBox seizureCount;

	@UiField
	Label seizureCountErrorMessage;

	@UiField
	EpilepsySubtypeCheckBoxTable epilepsySubtypes;

	@UiField
	ListBox studyType;

	private final ISingleSelectionList studyTypeView;

	@UiField
	TextBox refElectrodeDescription;

	@UiField
	Label refElectrodeDescriptionErrorMessage;

	@UiField
	FlexTable contactGroupTable;

	@UiField
	Button addContactGroupButton;

	@UiField
	Label addContactGroupErrorMessage;

	@UiField
	TextBox studyLabel;
	@UiField
	Label studyLabelErrorMessage;

	@UiField
	StudyDirectoriesWidget studyDirs;

	private final IHasValueAndErrorMessage<String> studyLabelView;
	private final IHasValueAndErrorMessage<String> studyDirView;
	private final IHasValueAndErrorMessage<String> mefDirView;
	private final IHasValueAndErrorMessage<String> reportFileView;
	private final IHasValueAndErrorMessage<String> imagesFileView;
	private final IHasValueAndErrorMessage<String> refElectrodeDescriptionView;

	@UiField
	Button saveButton;

	@UiField
	Button cancelButton;

	private final List<Long> contactGroupIds = newArrayList();
	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Creates this view.
	 * 
	 */
	public StudyView() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		startDateView = new UutcView(startDate);
		endDateView = new UutcView(endDate);
		seizureCountErrorMessage.setText("");
		seizureCountErrorMessage.setVisible(false);
		refElectrodeDescriptionErrorMessage.setText("");
		refElectrodeDescriptionErrorMessage.setVisible(false);
		studyLabelView = new TextBoxWithError(studyLabel,
				studyLabelErrorMessage);
		studyDirView = new DirectoryView(studyDirs.studyDirectory,
				studyDirs.studyDirectoryErrorMessage);
		mefDirView = new DirectoryView(studyDirs.mefDirectory,
				studyDirs.mefDirectoryErrorMessage);
		reportFileView = new DirectoryView(studyDirs.reportFile,
				studyDirs.reportFileErrorMessage);
		imagesFileView = new DirectoryView(studyDirs.imagesFile,
				studyDirs.imagesFileErrorMessage);
		refElectrodeDescriptionView = new TextBoxWithError(
				refElectrodeDescription,
				refElectrodeDescriptionErrorMessage);
		// Array panel
		contactGroupTable.getRowFormatter().addStyleName(0, css.flexTableHeader());
		contactGroupTable.getColumnFormatter()
				.addStyleName(2, css.flexTableButtonColumn());
		contactGroupTable.setText(0, 0, "Contact Group");
		contactGroupTable.setText(0, 1, "Edit");
		contactGroupTable.setText(0, 2, "Remove");
		addContactGroupErrorMessage.setVisible(false);
		studyTypeView = new SingleSelectionListBoxView(studyType);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasSaveButton#getSaveButton()
	 */
	@Override
	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasCancelButton#getCancelButton()
	 */
	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getSeizureCount()
	 */
	@Override
	public HasValue<Integer> getSeizureCount() {
		return seizureCount;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#hideSeizureCountErrorMessage()
	 */
	@Override
	public void hideSeizureCountErrorMessage() {
		hideErrorMessage(seizureCountErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#setAndShowSeizureCountErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowSeizureCountErrorMessage(String errorMessage) {
		setAndShowErrorMessage(seizureCountErrorMessage, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#addSeizureCountKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addSeizureCountKeyPressHandler(
			KeyPressHandler handler) {
		return seizureCount.addKeyPressHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#addContactGroup(java.lang.Long,
	 *      java.lang.String, boolean)
	 */
	@Override
	public EditRemoveButtonPair addContactGroup(Long id, String description,
			boolean studyIsModifiable) {
		if (contactGroupIds.contains(id)) {
			return null;
		}
		contactGroupIds.add(id);
		int row = contactGroupTable.getRowCount();
		contactGroupTable.setText(row, 0, description);
		Button remove = new Button("x");
		if (!studyIsModifiable) {
			remove.setEnabled(false);
			remove.setTitle("Contains unremovable time series.");
		}
		remove.addStyleDependentName(css.gwtButtonRemove());
		Button edit = new Button("Edit");
		contactGroupTable.setWidget(row, 1, edit);
		contactGroupTable.setWidget(row, 2, remove);
		return new EditRemoveButtonPair(edit, remove);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#removeContactGroup(java.lang.Long)
	 */
	@Override
	public void removeContactGroup(Long arrayId) {
		int arrayRow = contactGroupIds.indexOf(arrayId);
		contactGroupIds.remove(arrayRow);
		contactGroupTable.removeRow(arrayRow + 1);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getAddContactGroupButton()
	 */
	@Override
	public HasClickHandlers getAddContactGroupButton() {
		return addContactGroupButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#hideAddContactGroupErrorMessage()
	 */
	@Override
	public void hideAddContactGroupErrorMessage() {
		hideErrorMessage(addContactGroupErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#setAndShowAddContactGroupErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowAddContactGroupErrorMessage(String errorMessage) {
		setAndShowErrorMessage(addContactGroupErrorMessage, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getEndDateView()
	 */
	@Override
	public IHasValueAndErrorMessage<Long> getEndDateView() {
		return endDateView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getStartDateView()
	 */
	@Override
	public IHasValueAndErrorMessage<Long> getStartDateView() {
		return startDateView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getMefDirView()
	 */
	@Override
	public IHasValueAndErrorMessage<String> getMefDirView() {
		return mefDirView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getStudyDirView()
	 */
	@Override
	public IHasValueAndErrorMessage<String> getStudyDirView() {
		return studyDirView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getStudyTypeView()
	 */
	@Override
	public ISingleSelectionList getStudyTypeView() {
		return studyTypeView;
	}

	@Override
	public IHasValueAndErrorMessage<String> getStudyLabelView() {
		return studyLabelView;
	}

	@Override
	public HasValue<Boolean> getPublishedView() {
		return publishedCheckBox;
	}

	@Override
	public void setUnmodifiable() {
		addContactGroupButton.setEnabled(false);
		endDate.setEnabled(false);
		publishedCheckBox.setEnabled(false);
		refElectrodeDescription.setEnabled(false);
		saveButton.setEnabled(false);
		seizureCount.setEnabled(false);
		startDate.setEnabled(false);
		studyLabel.setEnabled(false);
		studyDirs.setEnabled(false);
		studyType.setEnabled(false);

	}

	@Override
	public IHasValueAndErrorMessage<String> getReportFileView() {
		return reportFileView;
	}

	@Override
	public IHasValueAndErrorMessage<String> getImagesFileView() {
		return imagesFileView;
	}

	@Override
	public void selectEpSubtype(EpSubtypePair subType) {
		epilepsySubtypes.setSelected(subType, true);
	}

	@Override
	public Set<EpSubtypePair> getEpSubtypes() {
		return epilepsySubtypes.getPairs();
	}

	@Override
	public boolean isEpSubtypeSelected(EpSubtypePair pair) {
		return epilepsySubtypes.isSelected(pair);
	}

	@Override
	public IHasValueAndErrorMessage<String> getRefElectrodeDescriptionView() {
		return refElectrodeDescriptionView;
	}
}
