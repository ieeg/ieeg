/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;

/**
 * Fired when user edits a Study.
 * 
 * @author John Frommeyer
 * 
 */
public class EditStudyEvent extends GwtEvent<EditStudyEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<EditStudyEventHandler> TYPE = new Type<EditStudyEventHandler>();

	private final GwtEegStudy study;

	/**
	 * Creates edit study event.
	 * 
	 * @param study the study to be edited.
	 */
	public EditStudyEvent(final GwtEegStudy study) {
		this.study = study;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(EditStudyEventHandler handler) {
		handler.onEditStudy(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditStudyEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * @return the study
	 */
	public GwtEegStudy getStudy() {
		return study;
	}
}
