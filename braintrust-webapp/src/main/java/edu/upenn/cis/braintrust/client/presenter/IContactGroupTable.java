/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;

import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;

public interface IContactGroupTable extends IDataEntryTable<GwtContactGroup>,
		IHasAddEntryButton {

	void setContactGroupDeleter(FieldUpdater<GwtContactGroup, String> deleter);

	void setChannelPrefixUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void clearChannelPrefixView(GwtContactGroup contactGroup);

	void setTracesModifer(FieldUpdater<GwtContactGroup, String> updater);

	void setSamplingRateUpdater(
			FieldUpdater<GwtContactGroup, String> updater);

	void clearImpedanceView(GwtContactGroup contactGroup);

	void setLffUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void clearHffView(GwtContactGroup contactGroup);

	void setSideUpdater(
			FieldUpdater<GwtContactGroup, String> updater);

	void setLocationUpdater(
			FieldUpdater<GwtContactGroup, String> updater);

	void setNotchFilterUpdater(
			FieldUpdater<GwtContactGroup, String> updater);

	void clearSamplingRateView(GwtContactGroup contactGroup);

	void clearLffView(GwtContactGroup contactGroup);

	void setHffUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void setImpedanceUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void setElectrodeTypeUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void clearElectrodeTypeView(GwtContactGroup contactGroup);

	void setManufacturerUpdater(FieldUpdater<GwtContactGroup, String> updater);

	void setModelNoUpdater(FieldUpdater<GwtContactGroup, String> updater);

}