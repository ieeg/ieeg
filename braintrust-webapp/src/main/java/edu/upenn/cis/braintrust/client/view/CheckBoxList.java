/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.client.presenter.IMultiSelectionList;

/**
 * A list of check boxes.
 * 
 * @author John Frommeyer
 * 
 */
final public class CheckBoxList extends Composite implements
		IMultiSelectionList {

	private final VerticalPanel panel;
	private final List<CheckBox> checkBoxes = newArrayList();

	CheckBoxList() {
		panel = new VerticalPanel();
		initWidget(panel);
	}

	public void setChoices(String[] labels) {
		panel.clear();
		checkBoxes.clear();

		for (String label : labels) {
			CheckBox checkBox = new CheckBox(label);
			checkBox.setValue(Boolean.FALSE);
			panel.add(checkBox);
			checkBoxes.add(checkBox);
		}
	}

	public int getItemCount() {
		return checkBoxes.size();
	}

	public boolean isItemSelected(int index) {
		return checkBoxes.get(index).getValue().booleanValue();
	}

	public void setItemSelected(int index, boolean selected) {
		checkBoxes.get(index).setValue(Boolean.valueOf(selected));
	}

	public Set<Integer> getSelectedIndices() {
		Set<Integer> selectedRows = newHashSet();
		int itemCount = getItemCount();
		for (int i = 0; i < itemCount; i++) {
			if (isItemSelected(i)) {
				selectedRows.add(Integer.valueOf(i));
			}
		}
		return selectedRows;
	}

	@Override
	public void clear() {
		setAll(false);
	}

	@Override
	public void setAll(boolean selected) {
		for (int i = 0, itemCount = getItemCount(); i < itemCount; i++) {
			setItemSelected(i, selected);
		}
	}
}
