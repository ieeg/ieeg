/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;

import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;

/**
 * A view for a {@code ListBox} without multiple selections.
 * 
 * @author John Frommeyer
 * 
 */
public class SingleSelectionListBoxView implements ISingleSelectionList {

	private final ListBox listBox;

	SingleSelectionListBoxView(final ListBox listBox) {
		this.listBox = listBox;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#getSelectedIndex()
	 */
	@Override
	public int getSelectedIndex() {
		return listBox.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#setSelectedIndex(int)
	 */
	@Override
	public void setSelectedIndex(int i) {
		listBox.setSelectedIndex(i);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IListView#setChoices(java.lang.String[])
	 */
	@Override
	public void setChoices(String[] choices) {
		listBox.clear();
		ViewUtil.setListBoxChoices(listBox, choices);
	}

	@Override
	public void clear() {
		listBox.clear();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return listBox.addChangeHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == listBox;
	}

	@Override
	public void setEnabled(boolean enabled) {
		listBox.setEnabled(enabled);
	}

	@Override
	public void fireChangeEvent() {
		DomEvent.fireNativeEvent(Document.get().createChangeEvent(), listBox);
	}

}
