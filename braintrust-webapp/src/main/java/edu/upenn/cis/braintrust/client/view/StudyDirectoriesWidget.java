/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * DOCUMENT ME
 * 
 * @author John Frommeyer
 * 
 */
public class StudyDirectoriesWidget extends Composite implements
		ValueChangeHandler<String>, HasEnabled {

	private static StudyDirectoriesWidgetUiBinder uiBinder = GWT
			.create(StudyDirectoriesWidgetUiBinder.class);

	interface StudyDirectoriesWidgetUiBinder extends
			UiBinder<Widget, StudyDirectoriesWidget> {}

	@UiField
	TextBox studyDirectory;
	@UiField
	Label studyDirectoryErrorMessage;

	@UiField
	TextBox mefDirectory;
	@UiField
	Label mefPath;
	@UiField
	Label mefDirectoryErrorMessage;

	@UiField
	TextBox reportFile;
	@UiField
	Label reportPath;
	@UiField
	Label reportFileErrorMessage;

	@UiField
	TextBox imagesFile;
	@UiField
	Label imagesPath;
	@UiField
	Label imagesFileErrorMessage;

	boolean enabled = true;

	public StudyDirectoriesWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
		studyDirectory.setText("");
		studyDirectory.addValueChangeHandler(this);
		studyDirectoryErrorMessage.setText("");
		studyDirectoryErrorMessage.setVisible(false);

		mefDirectory.setText("");
		mefDirectory.addValueChangeHandler(this);
		mefPath.setText("");
		mefDirectoryErrorMessage.setText("");
		mefDirectoryErrorMessage.setVisible(false);


		reportFile.setText("");
		reportFile.addValueChangeHandler(this);
		reportPath.setText("");
		reportFileErrorMessage.setText("");
		reportFileErrorMessage.setVisible(false);

		imagesFile.setText("");
		imagesFile.addValueChangeHandler(this);
		imagesPath.setText("");
		imagesFileErrorMessage.setText("");
		imagesFileErrorMessage.setVisible(false);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String mefPathStr = studyDirectory.getValue() + "/"
				+ mefDirectory.getValue();
		mefPath.setText(mefPathStr);
		final String reportPathStr = studyDirectory.getValue() + "/"
				+ reportFile.getValue();
		reportPath.setText(reportPathStr);
		final String imagesPathStr = studyDirectory.getValue() + "/"
				+ imagesFile.getValue();
		imagesPath.setText(imagesPathStr);
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		mefDirectory.setEnabled(enabled);
		studyDirectory.setEnabled(enabled);
		reportFile.setEnabled(enabled);
		imagesFile.setEnabled(enabled);
		this.enabled = enabled;
	}
}
