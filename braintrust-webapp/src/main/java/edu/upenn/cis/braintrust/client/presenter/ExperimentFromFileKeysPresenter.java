/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.EditAnimalEvent;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.OptionsLists;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.FileNotFoundException;

/**
 * Controls the create new animal screen.
 * 
 * @author John Frommeyer
 * 
 */
public class ExperimentFromFileKeysPresenter implements IPresenter,
		ClickHandler,
		ChangeHandler,
		KeyDownHandler {

	/**
	 * @author John Frommeyer
	 * 
	 */
	private final class EditAnimalClickHandler implements ClickHandler {

		private final GwtAnimalExperimentPair result;

		/**
		 * 
		 * @param result
		 */
		private EditAnimalClickHandler(GwtAnimalExperimentPair result) {
			this.result = result;
		}

		@Override
		public void onClick(ClickEvent event) {
			service.retrieveOptionsLists(new AsyncCallback<OptionsLists>() {

				@Override
				public void onFailure(
						Throwable caught) {
					if (caught instanceof BrainTrustAuthorizationException) {
						Window.alert("You are not authorized to retrieve options.");
					} else {
						Window.alert("Failed to load options.");
					}
				}

				@Override
				public void onSuccess(
						OptionsLists optionsResult) {
					final EditAnimalEvent editAnimalEvent = new EditAnimalEvent(
							result,
							optionsResult
									.getOrganizations(),
							optionsResult
									.getSpecies(),
							optionsResult
									.getDrugs(),
							optionsResult
									.getStimRegions(),
							optionsResult
									.getStimTypes());
					eventBus.fireEvent(editAnimalEvent);
				}
			});

		}
	}

	/**
	 * Defines the kind of view we can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IExperimentFromFileKeysView extends IView {

		IHasEnabledAndClickHandlers getCreateExperimentButton();

		void setAndShowCreateExperimentErrorMessageList(List<String> stringList);

		void setAndShowCreateExperimentErrorMessage(String string);

		ISingleSelectionList getSpeciesView();

		ISingleSelectionList getStrainView();

		void hideCreateExperimentErrorMessage();

		HasValue<String> getFileKeys();

		HandlerRegistration addFileKeyKeyDownHandler(KeyDownHandler handler);

		HasHTML getCreateExperimentResultsMessage();

		void hideInputShowResults();

		HasClickHandlers getGoAgainButton();

		HasClickHandlers getEditAnimalButton();

		void setEditAnimalButtonText(String text);

		void showInputHideResults();

		IHasEnabledAndClickHandlers getFileKeyLookupButton();

		IHasValueAndErrorMessage<String> getMefDir();

	}

	private final class RetrieveOrganizationsCallBack implements
			AsyncCallback<List<GwtOrganization>> {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Failed to load organizations.");
		}

		@Override
		public void onSuccess(List<GwtOrganization> result) {
			organizationList = result;
		}
	}

	private final class RetrieveSpeciesCallBack implements
			AsyncCallback<List<GwtSpecies>> {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Failed to load species.");
		}

		@Override
		public void onSuccess(List<GwtSpecies> result) {
			speciesList = result;
			String[] speciesChoices = new String[speciesList.size()];
			for (int i = 0; i < speciesList.size(); i++) {
				speciesChoices[i] = speciesList.get(i).getLabel();
			}
			display.getSpeciesView().setChoices(speciesChoices);
			if (speciesChoices.length > 0) {
				display.getSpeciesView().setSelectedIndex(0);
				// Fire event to trigger onChange() to load the related strains.
				display.getSpeciesView().fireChangeEvent();
			}
		}
	}

	private final HandlerManager eventBus;
	private final IExperimentFromFileKeysView display;
	private final BrainTrustServiceAsync service;
	private List<GwtSpecies> speciesList = newArrayList();
	private List<GwtStrain> strainList = newArrayList();
	private List<GwtOrganization> organizationList = newArrayList();
	private static final String fileKeyPrefix = "Animal_Data";
	private static final String orgPattern = "I\\d{3}";
	private static final RegExp orgRegExp = RegExp
			.compile("^" + orgPattern + "$");
	private static final String orgFormatExample = "Ixyz";
	private static final String subjectPattern = "A\\d{4}";
	private static final RegExp subjectRegExp = RegExp
			.compile("^" + subjectPattern + "$");
	private static final String subjectFormatExample = "Auxyz";
	private static final String datasetPattern = "D\\d{3}";
	private static final RegExp datasetRegExp = RegExp
			.compile("^" + datasetPattern + "$");
	private static final String datasetFormatExample = "Dxyz";
	private static final RegExp mefDirRegExp = RegExp.compile(
			"^"
					+ fileKeyPrefix + "/" + orgPattern + "/"
					+ subjectPattern + "/"
					+ datasetPattern + "/mef/?$");
	private static final String mefDirFormatExample = fileKeyPrefix + "/"
			+ orgFormatExample + "/" + subjectFormatExample + "/"
			+ datasetFormatExample + "/mef";
	private final Splitter lineSplitter = Splitter.on('\n')
			.trimResults()
			.omitEmptyStrings();
	private final Joiner lineJoiner = Joiner.on('\n').skipNulls();

	/**
	 * Create the menu presenter.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 */
	public ExperimentFromFileKeysPresenter(
			HandlerManager eventBus,
			IExperimentFromFileKeysView display,
			BrainTrustServiceAsync service) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = service;
		this.service.retrieveSpecies(new RetrieveSpeciesCallBack());
		this.service.retrieveOrganizations(new RetrieveOrganizationsCallBack());
		bind();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());

	}

	private void bind() {
		display.getCreateExperimentButton().addClickHandler(this);
		display.getFileKeyLookupButton().addClickHandler(this);
		display.getMefDir().addKeyDownHandler(this);
		display.addFileKeyKeyDownHandler(this);

		// This change handler updates the strain choices when a new species is
		// selected.
		display.getSpeciesView().addChangeHandler(this);

		// This change handler clears any error message associated with new
		// animal creation when the selection of any of the new animal listboxes
		// is changed.
		final ChangeHandler hideNewExperimentErrorMessage = new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				display.hideCreateExperimentErrorMessage();
			}
		};
		display.getSpeciesView().addChangeHandler(
				hideNewExperimentErrorMessage);
		display.getStrainView().addChangeHandler(
				hideNewExperimentErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (display.getCreateExperimentButton().isWrapping(source)) {
			onCreateExperimentClick();
		} else if (display.getFileKeyLookupButton().isWrapping(source)) {
			onLookupFileKeys();
		}
	}

	private void onCreateExperimentClick() {
		display.getCreateExperimentButton().setEnabled(false);
		display.getFileKeyLookupButton().setEnabled(false);
		final List<String> fileKeys = lineSplitter.splitToList(display
				.getFileKeys().getValue());
		final FileKeyParser parser = new FileKeyParser(
				fileKeys,
				fileKeyPrefix,
				orgRegExp,
				orgFormatExample,
				subjectRegExp,
				subjectFormatExample,
				datasetRegExp,
				datasetFormatExample);

		Map<String, Set<GwtTrace>> channelPrefixToTraces = null;
		boolean canCreate = true;
		try {
			channelPrefixToTraces = parser
					.parse();
		} catch (FileKeyParserException e) {
			canCreate = false;
			display.setAndShowCreateExperimentErrorMessageList(e
					.getParseErrors());
		}

		final int strainIdx = display.getStrainView()
				.getSelectedIndex();
		if (strainIdx < 0 || strainIdx >= strainList.size()) {
			canCreate = false;
			display.setAndShowCreateExperimentErrorMessage("There are no strains loaded");
		}
		final GwtStrain strain = strainList.get(strainIdx);
		if (strain == null) {
			canCreate = false;
			display.setAndShowCreateExperimentErrorMessage("Strain not found");
		}

		if (!canCreate) {
			display.getCreateExperimentButton().setEnabled(true);
			display.getFileKeyLookupButton().setEnabled(true);
			return;
		} else {
			final String orgCode = parser.getOrgComponent();
			final GwtOrganization organization = tryFind(
					organizationList,
					compose(
							equalTo(orgCode),
							GwtOrganization.getCode))
					.orNull();
			if (organization == null) {
				display.setAndShowCreateExperimentErrorMessage("Organization not found.");
				display.getCreateExperimentButton().setEnabled(true);
				display.getFileKeyLookupButton().setEnabled(true);
				return;
			}
			final String animalName = orgCode + "_"
					+ parser.getSubjectComponent();
			final GwtRecording recording = new GwtRecording(
					parser.getDir(),
					parser.getMefDir(),
					Long.valueOf(0),
					Long.valueOf(0),
					null,
					null,
					null,
					null,
					null,
					null);
			final String experimentName = animalName + "_"
					+ parser.getDatasetComponent();
			final GwtExperiment experiment = new GwtExperiment(
					experimentName,
					recording,
					false,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
			final GwtAnimal animal = new GwtAnimal(
					animalName,
					organization,
					strain,
					null,
					null);
			service.saveAnimalWithNewExperiment(
					animal,
					experiment,
					channelPrefixToTraces,
					new AsyncCallback<GwtAnimalExperimentPair>() {

						@Override
						public void onFailure(Throwable caught) {
							display.getCreateExperimentButton()
									.setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
							if (caught instanceof DuplicateNameException) {
								final DuplicateNameException dne = (DuplicateNameException) caught;
								Window.alert("Animal " + animal.getLabel()
										+ " already has an experiment "
										+ dne.getName());
							} else if (caught instanceof FileNotFoundException) {
								Window.alert("Could not create Experiment: "
										+ caught.getMessage());
							} else if (caught instanceof BrainTrustUserException) {
								Window.alert("Could not create Experiment: "
										+ caught.getMessage());
							} else {
								Window.alert("An error occurred while attempting to save animal "
										+ animal.getLabel());
							}
						}

						@Override
						public void onSuccess(
								final GwtAnimalExperimentPair result) {

							final int expCount = result.getExperiments().size();
							final String animalName = result.getAnimal()
									.getLabel();
							String msg = null;
							if (expCount == 0) {
								msg = "No experiments found for animal "
										+ animalName
										+ ". Please report this as an error.";
							} else if (expCount == 1) {
								msg = "Created animal " + animalName
										+ " and experiment "
										+ experiment.getLabel();
							} else {
								msg = "Added new experiment "
										+ experiment.getLabel() + " to animal "
										+ animalName;
							}
							display.getCreateExperimentResultsMessage()
									.setHTML(msg);

							display.getGoAgainButton().addClickHandler(
									new ClickHandler() {
										@Override
										public void onClick(ClickEvent event) {
											// Clear the view
											display.getSpeciesView()
													.setSelectedIndex(0);
											display.getSpeciesView()
													.fireChangeEvent();
											display.getFileKeys()
													.setValue(null);
											display.hideCreateExperimentErrorMessage();

											display.showInputHideResults();
										}
									});
							display.setEditAnimalButtonText("Edit animal "
									+ animalName);
							display.getEditAnimalButton().addClickHandler(
									new EditAnimalClickHandler(result));

							display.hideInputShowResults();

						}
					});

		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		final ISingleSelectionList strainView = display
				.getStrainView();
		strainView.clear();
		final int i = display.getSpeciesView().getSelectedIndex();
		if (i < 0 || i >= speciesList.size()) {
			Window
					.alert("There are no species loaded.");
		} else {
			final GwtSpecies species = speciesList.get(i);
			if (species == null) {
				Window.alert("Species not found.");
			} else {
				service.findStrains(species,
						new AsyncCallback<List<GwtStrain>>() {

							@Override
							public void onFailure(Throwable caught) {
								display.setAndShowCreateExperimentErrorMessage(
										"An error occured while attempting to retrieve strains for species: "
												+ species.getLabel()
												+ ".");
							}

							@Override
							public void onSuccess(List<GwtStrain> result) {
								strainList = result;
								final ISingleSelectionList strainView = display
										.getStrainView();
								final String[] strainChoices = new String[result
										.size()];
								int i = 0;
								for (final GwtStrain strain : result) {
									strainChoices[i++] = strain.getLabel();
								}
								strainView.setChoices(strainChoices);
							}
						});
			}

		}

	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		final Object source = event.getSource();
		if (source == display.getFileKeys()) {
			display.hideCreateExperimentErrorMessage();
		} else if (display.getMefDir().isWrapping(source)) {
			display.getMefDir().hideErrorMessage();
		}
	}

	private void onLookupFileKeys() {
		display.getCreateExperimentButton().setEnabled(false);
		display.getFileKeyLookupButton().setEnabled(false);
		final String mefPath = nullToEmpty(display.getMefDir().getValue())
				.trim();
		if (!mefDirRegExp.test(mefPath)) {
			display.getMefDir().setAndShowErrorMessage(
					"MEF directory must be of the form "
							+ mefDirFormatExample);
			display.getCreateExperimentButton().setEnabled(true);
			display.getFileKeyLookupButton().setEnabled(true);
		} else {
			service.retrieveMefFileKeys(mefPath,
					new AsyncCallback<List<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							if (caught instanceof BrainTrustAuthorizationException) {
								Window.alert("You are not authorized to view MEF file keys");
							} else if (caught instanceof BrainTrustUserException) {
								Window.alert(caught.getMessage());
							} else {
								Window.alert("Could not retrieve MEF file keys");
							}
							display.getCreateExperimentButton()
									.setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
						}

						@Override
						public void onSuccess(List<String> result) {
							if (result.isEmpty()) {
								Window.alert("No MEF files found in " + mefPath);
							} else {
								final String fileKeyLines = lineJoiner
										.join(result);
								display.getFileKeys().setValue(fileKeyLines);
							}
							display.getCreateExperimentButton()
									.setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
						}
					});
		}

	}

}
