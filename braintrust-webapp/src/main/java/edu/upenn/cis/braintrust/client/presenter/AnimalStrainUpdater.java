/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.view.ModifyAnimalStrainView;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;

public class AnimalStrainUpdater implements
		FieldUpdater<GwtAnimal, String> {
	private final IAnimalTable display;
	private final BrainTrustServiceAsync service;
	private final List<GwtSpecies> speciesList;

	public AnimalStrainUpdater(IAnimalTable display,
			BrainTrustServiceAsync service,
			List<GwtSpecies> speciesList) {
		this.display = display;
		this.service = service;
		this.speciesList = speciesList;
	}

	@Override
	public void update(final int index, final GwtAnimal object, String value) {
		final IPresenter presenter = new ModifyAnimalStrainPresenter(
				new ModifyAnimalStrainView(), service, object, speciesList,
				display);
		presenter.go(null);
	}
}
