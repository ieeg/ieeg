/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;

/**
 * Fired when a new patient needs to be created.
 * 
 * @author John Frommeyer
 * 
 */
public class CreatePatientEvent extends GwtEvent<CreatePatientEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<CreatePatientEventHandler> TYPE = new Type<CreatePatientEventHandler>();
	
	private final List<GwtOrganization> organizations;

	public CreatePatientEvent(List<GwtOrganization> organizations) {
		this.organizations = ImmutableList.copyOf(organizations);
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreatePatientEventHandler handler) {
		handler.onCreatePatient(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreatePatientEventHandler> getAssociatedType() {
		return TYPE;
	}
	/**
	 * @return the organizations
	 */
	public List<GwtOrganization> getOrganizations() {
		return organizations;
	}

}
