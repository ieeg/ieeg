/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;

import java.util.Collections;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter.IAnimalView;
import edu.upenn.cis.braintrust.client.view.AddElectrodeWidget;
import edu.upenn.cis.braintrust.client.view.CommentaryView;
import edu.upenn.cis.braintrust.client.view.TracesView;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class RecordingPresenter implements IPresenter, ClickHandler,
		IExperimentSelectionHandler {

	private final class RecordingCommentaryUpdater implements
			FieldUpdater<GwtRecording, String> {

		@Override
		public void update(int index, GwtRecording object,
				String value) {
			final IPresenter presenter = new CommentaryPresenter(
					new CommentaryView(),
					service,
					parentPresenter,
					animalDisplay);
			presenter.go(null);
		}
	}

	private final class ContactGroupDeleter implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(final int index,
				final GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			final GwtRecording recording = display.getRecording();
			final GwtExperiment recordingParent = parentPresenter
					.getRecordingParent(recording).orNull();
			service.deleteContactGroup(recordingParent,
					recording, object,
					new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Could not delete electrode "
									+ object.getChannelPrefix());
						}

						@Override
						public void onSuccess(Void result) {
							final IRecordingView display = animalDisplay
									.getRecordingView();
							// The Recording DTO holds onto the
							// Electrodes for historical
							// reasons
							// so we have to keep them up to date.
							display.getRecording().removeContactGroup(
									object);
							display.getContactGroupTable()
									.getEntryList().remove(index);
							Collections
									.sort(display
											.getContactGroupTable()
											.getEntryList(),
											GwtContactGroup.CHANNEL_PREFIX_COMPARATOR);
							display.getContactGroupTable()
									.refreshProvider();
						}
					});

		}
	}

	private final class SideUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final Side newSide = Side.fromString.get(value);
			if (newSide == null) {
				Window.alert("Could not find side " + value);
			} else if (newSide.equals(object.getSide())) {
				return;
			} else {
				object.setSide(newSide);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class LocationUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final Location newLocation = Location.fromString.get(value);
			if (newLocation == null) {
				Window.alert("Could not find location " + value);
			} else if (newLocation.equals(object.getLocation())) {
				return;
			} else {
				object.setLocation(newLocation);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class NotchFilterUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			if (value.equals(BooleanWithUnknownUtil.valueToString(object
					.getNotchFilter()))) {
				return;
			} else {
				object.setNotchFilter(BooleanWithUnknownUtil.stringToValue(
						value)
						.orNull());
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class HffUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			Double newHff = null;
			if ("".equals(value.trim())) {
				newHff = null;
			} else {
				try {
					newHff = Double.valueOf(value);
				} catch (NumberFormatException e) {
					display.getContactGroupTable().clearHffView(object);
					display.getContactGroupTable().redrawTable();
					display.getContactGroupTable()
							.getModifyEntryErrorMessage()
							.setAndShowErrorMessage(
									"Invalid value: " + value);
					return;
				}
			}
			if (equal(newHff, object.getHffSetting().orNull())) {
				return;
			} else {
				object.setHffSetting(newHff);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class LffUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			Double newLff = null;
			if ("".equals(value.trim())) {
				newLff = null;
			} else {
				try {
					newLff = Double.valueOf(value);
				} catch (NumberFormatException e) {
					display.getContactGroupTable().clearLffView(object);
					display.getContactGroupTable().redrawTable();
					display.getContactGroupTable()
							.getModifyEntryErrorMessage()
							.setAndShowErrorMessage(
									"Invalid value: " + value);
					return;
				}
			}
			if (equal(newLff, object.getLffSetting().orNull())) {
				return;
			} else {
				object.setLffSetting(newLff);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class ManufacturerUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			String newManufacturer = value.trim();
			if ("".equals(newManufacturer)) {
				newManufacturer = null;
			}
			if (equal(newManufacturer, object.getManufacturer().orNull())) {
				return;
			} else {
				object.setManufacturer(newManufacturer);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class ModelNoUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			String newModelNo = value.trim();
			if ("".equals(newModelNo)) {
				newModelNo = null;
			}
			if (equal(newModelNo, object.getModelNo().orNull())) {
				return;
			} else {
				object.setModelNo(newModelNo);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class ImpedenceUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			Double newImpedence = null;
			if ("".equals(value.trim())) {
				newImpedence = null;
			} else {
				try {
					newImpedence = Double.valueOf(value);
				} catch (NumberFormatException e) {
					display.getContactGroupTable().clearImpedanceView(object);
					display.getContactGroupTable().redrawTable();
					display.getContactGroupTable()
							.getModifyEntryErrorMessage()
							.setAndShowErrorMessage(
									"Invalid value: " + value);
					return;
				}
			}
			if (equal(newImpedence, object.getImpedance().orNull())) {
				return;
			} else {
				object.setImpedance(newImpedence);
				service.modifyContactGroup(display.getRecording(),
						object, new ModifyElectrodeAsyncCallback(
								index, object));
			}
		}
	}

	private final class SamplingRateUpdater implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay
					.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			Double newSamplingRate = null;
			try {
				newSamplingRate = Double.valueOf(value);
			} catch (NumberFormatException e) {
				// dealt with below
			}
			if (newSamplingRate == null) {
				display.getContactGroupTable().clearSamplingRateView(object);
				display.getContactGroupTable().redrawTable();
				display.getContactGroupTable()
						.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"Invalid value: " + value);
			} else {
				if (newSamplingRate.equals(object.getSamplingRate())) {
					return;
				} else {
					object.setSamplingRate(newSamplingRate);
					service.modifyContactGroup(display.getRecording(),
							object, new ModifyElectrodeAsyncCallback(
									index, object));
				}
			}
		}
	}

	private final class ModifyElectrodeAsyncCallback implements
			AsyncCallback<GwtContactGroup> {
		private final int index;
		private final GwtContactGroup originalElectrode;

		private ModifyElectrodeAsyncCallback(int index,
				GwtContactGroup originalElectrode) {
			this.index = index;
			this.originalElectrode = originalElectrode;
		}

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("An error occurred while attempting to modify electrode.");
		}

		@Override
		public void onSuccess(GwtContactGroup result) {
			final IRecordingView display = animalDisplay.getRecordingView();
			// The Recording DTO holds onto the Electrodes for historical
			// reasons
			// so we have to keep them up to date.
			display.getRecording().removeContactGroup(originalElectrode);
			display.getRecording().addContactGroup(result);
			display.getContactGroupTable().getEntryList()
					.set(index, result);
			Collections
					.sort(display.getContactGroupTable()
							.getEntryList(),
							GwtContactGroup.CHANNEL_PREFIX_COMPARATOR);
			display.getContactGroupTable()
					.refreshProvider();
		}
	}

	private final class ChannelPrefixUpdater implements
			FieldUpdater<GwtContactGroup, String> {

		@Override
		public void update(final int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String newPrefix = value.trim();
			if (newPrefix.equals(object.getChannelPrefix())) {
				return;
			} else if ("".equals(newPrefix)) {
				display.getContactGroupTable().clearChannelPrefixView(object);
				display.getContactGroupTable().redrawTable();
				display.getContactGroupTable()
						.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"You must enter a channel prefix.");
			} else {
				final GwtContactGroup duplicatePrefix = tryFind(
						display.getContactGroupTable().getEntryList(),
						compose(equalTo(newPrefix),
								GwtContactGroup.getChannelPrefix))
						.orNull();
				if (duplicatePrefix != null) {
					display.getContactGroupTable().clearChannelPrefixView(object);
					display.getContactGroupTable().redrawTable();
					display.getContactGroupTable()
							.getModifyEntryErrorMessage()
							.setAndShowErrorMessage(
									"This recording already has a contact group with prefix "
											+ newPrefix);
				} else {
					object.setChannelPrefix(newPrefix);
					service.modifyContactGroup(display.getRecording(), object,
							new ModifyElectrodeAsyncCallback(index, object));
				}
			}
		}
	}

	private final class ElectrodeTypeUpdater implements
			FieldUpdater<GwtContactGroup, String> {

		@Override
		public void update(final int index, GwtContactGroup object,
				String value) {
			final IRecordingView display = animalDisplay.getRecordingView();
			display.getContactGroupTable()
					.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String newType = value.trim();
			if (newType.equals(object.getElectrodeType())) {
				return;
			} else if ("".equals(newType)) {
				display.getContactGroupTable().clearElectrodeTypeView(object);
				display.getContactGroupTable().redrawTable();
				display.getContactGroupTable()
						.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"You must enter an electrode type.");
			} else {
				object.setElectrodeType(newType);
				service.modifyContactGroup(display.getRecording(), object,
						new ModifyElectrodeAsyncCallback(index, object));

			}
		}
	}

	private final class TraceModifier implements
			FieldUpdater<GwtContactGroup, String> {
		@Override
		public void update(int index, final GwtContactGroup object,
				String value) {
			animalDisplay.getRecordingView().getContactGroupTable()
					.getModifyEntryErrorMessage().hideErrorMessage();
			service.findTraces(object,
					new AsyncCallback<List<GwtTrace>>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Unable to load traces for this electrode.");
						}

						@Override
						public void onSuccess(List<GwtTrace> result) {
							final GwtRecording recording = animalDisplay
									.getRecordingView()
									.getRecording();
							final IHasGwtRecording recordingParent = parentPresenter
									.getRecordingParent(recording)
									.orNull();
							if (recordingParent == null) {
								Window.alert("Could not find recording parent.");
							} else {
								IPresenter presenter = new TracesPresenter(
										new TracesView(
												newArrayList(ContactType.FROM_STRING
														.keySet())),
										service,
										object,
										recordingParent,
										true,
										result);
								presenter.go(null);
							}
						}
					});
		}
	}

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IRecordingView extends IView, IHasEnabled {
		public IRecordingTable getRecordingTable();

		GwtRecording getRecording();

		IContactGroupTable getContactGroupTable();

	}

	private final IAnimalView animalDisplay;
	private final IHasRecordingParent<GwtExperiment> parentPresenter;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private final RecordingFieldUpdater<String> dirUpdater;
	private final RecordingFieldUpdater<String> mefDirUpdater;
	private final RecordingFieldUpdater<String> startTimeUpdater;
	private final RecordingFieldUpdater<String> endTimeUpdater;
	private final RecordingFieldUpdater<String> reportFileUpdater;
	private final RecordingFieldUpdater<String> imagesFileUpdater;
	private final RecordingFieldUpdater<String> refElectrodeDescriptionUpdater;

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param animalDisplay
	 * @param rpcService
	 * @param electrodeInfoStore
	 * @param experiment
	 */
	public RecordingPresenter(final HandlerManager eventBus,
			final IAnimalView animalDisplay,
			final BrainTrustServiceAsync rpcService,
			IHasRecordingParent<GwtExperiment> parentPresenter) {
		this.eventBus = eventBus;
		this.animalDisplay = animalDisplay;
		this.service = rpcService;
		this.parentPresenter = parentPresenter;
		final IRecordingView display = animalDisplay.getRecordingView();
		this.dirUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromDir(),
				new RecordingDirValidator(),
				this.parentPresenter);
		this.mefDirUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromMefDir(),
				new RecordingMefDirValidator(),
				this.parentPresenter);
		this.startTimeUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromStartTime(),
				new RecordingStartTimeValidator(),
				this.parentPresenter);
		this.endTimeUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromEndTime(),
				new RecordingEndTimeValidator(),
				this.parentPresenter);
		this.reportFileUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromReportFile(),
				IRecordingFieldValidator.noValidationString,
				this.parentPresenter);
		this.imagesFileUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromImagesFile(),
				IRecordingFieldValidator.noValidationString,
				this.parentPresenter);
		this.refElectrodeDescriptionUpdater = new RecordingFieldUpdater<String>(
				display.getRecordingTable(),
				this.service,
				new RecordingFactoryFromRefElectrodeDescription(),
				IRecordingFieldValidator.noValidationString,
				this.parentPresenter);
		bind();
	}

	private void bind() {
		final IRecordingView display = animalDisplay.getRecordingView();
		// Keep it disabled until an experiment is selected
		display.setEnabled(false);
		display.getRecordingTable().setRecordingDirUpdater(dirUpdater);
		display.getRecordingTable().setRecordingMefDirUpdater(
				mefDirUpdater);
		display.getRecordingTable().setRecordingStartTimeUpdater(
				startTimeUpdater);
		display.getRecordingTable().setRecordingEndTimeUpdater(
				endTimeUpdater);
		display.getRecordingTable().setRecordingReportFileUpdater(
				reportFileUpdater);
		display.getRecordingTable().setRecordingImagesFileUpdater(
				imagesFileUpdater);
		display.getRecordingTable().setRecordingRefElectrodeDescriptionUpdater(
				refElectrodeDescriptionUpdater);
		display.getRecordingTable().setRecordingCommentaryUpdater(
				new RecordingCommentaryUpdater());

		display.getContactGroupTable().getAddEntryButton().addClickHandler(this);
		display.getContactGroupTable().setTracesModifer(
				new TraceModifier());
		display.getContactGroupTable().setChannelPrefixUpdater(
				new ChannelPrefixUpdater());
		display.getContactGroupTable().setElectrodeTypeUpdater(
				new ElectrodeTypeUpdater());
		display.getContactGroupTable().setManufacturerUpdater(
				new ManufacturerUpdater());
		display.getContactGroupTable().setModelNoUpdater(new ModelNoUpdater());
		display.getContactGroupTable().setHffUpdater(
				new HffUpdater());
		display.getContactGroupTable().setLffUpdater(new LffUpdater());
		display.getContactGroupTable().setImpedanceUpdater(new ImpedenceUpdater());
		display.getContactGroupTable().setSamplingRateUpdater(
				new SamplingRateUpdater());
		display.getContactGroupTable().setNotchFilterUpdater(
				new NotchFilterUpdater());
		display.getContactGroupTable().setSideUpdater(
				new SideUpdater());
		display.getContactGroupTable().setLocationUpdater(new LocationUpdater());
		display.getContactGroupTable().setContactGroupDeleter(
				new ContactGroupDeleter());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		final IRecordingView display = animalDisplay.getRecordingView();
		if (source == display.getContactGroupTable()
				.getAddEntryButton()) {
			IPresenter addElectrode = new AddContactGroupPresenter(
					new AddElectrodeWidget(),
					service,
					display.getRecording(),
					animalDisplay);
			addElectrode.go(null);
		}

	}

	@Override
	public void onExperimentSelection(GwtExperiment experiment) {
		final GwtRecording recording = experiment.getRecording();
		final IRecordingView display = animalDisplay.getRecordingView();
		display.setEnabled(true);
		display.getRecordingTable().setRecording(recording);
		display.getContactGroupTable()
				.getEntryList().clear();
		display.getContactGroupTable()
				.getEntryList().addAll(recording.getContactGroups());
		Collections.sort(display.getContactGroupTable()
				.getEntryList(), GwtContactGroup.CHANNEL_PREFIX_COMPARATOR);
		display.getContactGroupTable().refreshProvider();

	}
}
