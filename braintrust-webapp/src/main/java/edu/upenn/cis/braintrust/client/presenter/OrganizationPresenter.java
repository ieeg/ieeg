/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Ordering;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * Handles the logic of the organization table.
 * 
 * @author John Frommeyer
 * 
 */
public class OrganizationPresenter implements IPresenter, ClickHandler,
		KeyDownHandler {

	private final class NameUpdater implements
			FieldUpdater<GwtOrganization, String> {
		@Override
		public void update(int index, GwtOrganization object,
				String value) {
			display.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String newName = value.trim();

			if (newName.equals(object.getName())) {
				return;
			} else if ("".equals(newName)) {
				display.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"Organization name cannot be empty");
				display.clearNameView(
						object);
				display.redrawTable();

			} else {
				final GwtOrganization modifedOrg = new GwtOrganization(
						newName,
						object.getCode(),
						object.getId(),
						object.getVersion());
				service.modifyOrganization(
						modifedOrg,
						new ModifyOrganzationAsyncCallback(index));
			}
		}
	}

	private final class OrganizationDeleter implements
			FieldUpdater<GwtOrganization, String> {
		@Override
		public void update(int index, final GwtOrganization object,
				String value) {
			display.getModifyEntryErrorMessage()
					.hideErrorMessage();
			service.deleteOrganization(object,
					new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							if (caught instanceof BrainTrustAuthorizationException) {
								Window.alert("You are not authorized to delete this organization.");
							} else if (caught instanceof ObjectNotFoundException) {
								Window.alert("This organization cannot found. Please reload page.");
							} else if (caught instanceof StaleObjectException) {
								Window.alert("This organization has been modified elsewhere. Please reload page.");
							} else {
								Window.alert("An error occurred while attempting to delete an organization. Please check that the organization is not assigned to any subject");
							}
						}

						@Override
						public void onSuccess(Void result) {
							display.getEntryList().remove(object);
							display.refreshProvider();
							orgChangeHandler.onOrganizationChange(display
									.getEntryList());
						}
					});
		}
	}

	private final class ModifyOrganzationAsyncCallback implements
			AsyncCallback<GwtOrganization> {
		private final int index;

		private ModifyOrganzationAsyncCallback(
				int index) {
			this.index = index;
		}

		@Override
		public void onFailure(Throwable caught) {
			if (caught instanceof BrainTrustAuthorizationException) {
				Window.alert("You are not authorized to modify this organization.");
			} else if (caught instanceof ObjectNotFoundException) {
				Window.alert("This organization cannot found. Please reload page.");
			} else if (caught instanceof StaleObjectException) {
				Window.alert("This organization has been modified elsewhere. Please reload page.");
			} else {
				Window.alert("An error occurred while attempting to modify organization.");
			}
		}

		@Override
		public void onSuccess(GwtOrganization result) {
			display.getEntryList()
					.set(index, result);
			Collections
					.sort(display.getEntryList(),
							Ordering.usingToString());
			display.refreshProvider();
			orgChangeHandler.onOrganizationChange(display.getEntryList());
		}
	}

	private final IOrganizationTable display;
	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync service;
	private IOrganizationChangeHandler orgChangeHandler;
	private final RegExp newOrgCodePattern = RegExp.compile("^I[0-9]{3}$");

	/**
	 * 
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param organizationList
	 */
	public OrganizationPresenter(
			final HandlerManager eventBus,
			final IOrganizationTable display,
			final BrainTrustServiceAsync rpcService) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = rpcService;
		bind();
	}

	private void bind() {
		display.getAddEntryButton().addClickHandler(this);
		display.addKeyDownHandler(this);
		display.setOrganizationDeleter(
				new OrganizationDeleter());
		display.setNameUpdater(
				new NameUpdater());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {}

	public void populateDisplay(List<GwtOrganization> initialValues) {
		display.getEntryList().addAll(initialValues);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getAddEntryButton()) {
			onAddOrgClick();
		}

	}

	private void onAddOrgClick() {
		boolean canCreate = true;
		final String code = display.getNewOrgCodeView().getValue().trim();
		if (!newOrgCodePattern.test(code)) {
			canCreate = false;
			display.setAndShowNewOrgErrorMessage("Organization code must be of the form Ixyz, where the x, y, & z are in [0-9]");
		}
		final GwtOrganization existingOrg = tryFind(display.getEntryList(),
				compose(equalTo(code), GwtOrganization.getCode)).orNull();
		if (existingOrg != null) {
			canCreate = false;
			display.setAndShowNewOrgErrorMessage("Code " + code
					+ " is already used by " + existingOrg);
		}
		final String name = display.getNewOrgNameView().getValue().trim();
		if ("".equals(name)) {
			canCreate = false;
			display.setAndShowNewOrgErrorMessage("Organization name cannot be empty");
		}
		if (canCreate) {
			final GwtOrganization newOrg = new GwtOrganization(
					name,
					code,
					null,
					null);

			service.createOrganization(newOrg,
					new AsyncCallback<GwtOrganization>() {

						@Override
						public void onFailure(Throwable caught) {
							if (caught instanceof BrainTrustAuthorizationException) {
								Window.alert("You are not authorized to create organizations.");
							} else {
								Window.alert("Could not create organization");
							}
						}

						@Override
						public void onSuccess(GwtOrganization result) {
							display.getEntryList()
									.add(result);
							Collections
									.sort(display.getEntryList(),
											Ordering.usingToString());
							display.refreshProvider();
							orgChangeHandler.onOrganizationChange(display
									.getEntryList());
							display.getNewOrgCodeView().setValue("");
							display.getNewOrgNameView().setValue("");
						}
					});
		}
	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		Object source = event.getSource();
		if (display.getNewOrgCodeView() == source
				|| display.getNewOrgNameView() == source) {
			display.hideNewOrgErrorMessage();
		}
	}

	public void setOrganizationChangeHandler(IOrganizationChangeHandler handler) {
		this.orgChangeHandler = handler;
	}
}
