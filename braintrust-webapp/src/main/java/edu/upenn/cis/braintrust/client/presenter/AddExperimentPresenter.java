/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import java.util.Collections;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter.IAnimalView;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class AddExperimentPresenter implements IPresenter, ClickHandler,
		KeyPressHandler {

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IAddExperimentView extends IView, IHasSaveButton,
			IHasCancelButton, IDialogBox {
		public IHasValueAndErrorMessage<Long> getStartDateView();

		public IHasValueAndErrorMessage<Long> getEndDateView();

		public IHasValueAndErrorMessage<String> getExperimentLabelView();

		public IHasValueAndErrorMessage<String> getExperimentDirView();

		public IHasValueAndErrorMessage<String> getMefDirView();

		public IHasValueAndErrorMessage<String> getReportFileView();

		public IHasValueAndErrorMessage<String> getImagesFileView();

		public HasValue<Boolean> getPublishedView();

	}

	private final IAddExperimentView display;
	private final BrainTrustServiceAsync service;
	private final GwtAnimal parent;
	private final IAnimalView callbackView;

	/**
	 * Creates a presenter with an existing {@code Study}.
	 * 
	 * @param eventBus
	 * @param display
	 * @param rpcService
	 * @param electrodeInfoStore
	 * @param experiment
	 */
	public AddExperimentPresenter(
			final IAddExperimentView display,
			final BrainTrustServiceAsync rpcService,
			final GwtAnimal parent,
			IAnimalView callbackView) {
		this.display = display;
		this.service = rpcService;
		this.parent = parent;
		this.callbackView = callbackView;
		bind();
	}

	private void bind() {
		display.getSaveButton().addClickHandler(this);
		display.getCancelButton().addClickHandler(this);
		display.getStartDateView().addKeyPressHandler(this);
		display.getEndDateView().addKeyPressHandler(this);
		display.getStartDateView().addKeyPressHandler(this);
		display.getExperimentLabelView().addKeyPressHandler(this);
		display.getExperimentDirView().addKeyPressHandler(this);
		display.getMefDirView().addKeyPressHandler(this);
		display.getReportFileView().addKeyPressHandler(this);
		display.getImagesFileView().addKeyPressHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		display.show();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getSaveButton()) {
			onSave();
		} else if (source == display.getCancelButton()) {
			display.hide();
		}
	}

	private void onSave() {

		Long startDate = display.getStartDateView().getValue();
		Long endDate = display.getEndDateView().getValue();
		boolean canSave = true;
		if (startDate == null) {
			canSave = false;
			display.getStartDateView().setAndShowErrorMessage(
					"You must enter a start date.");
		}
		if (endDate == null) {
			canSave = false;
			display.getEndDateView().setAndShowErrorMessage(
					"You must enter an end date.");
		}
		if (startDate != null && endDate != null
				&& startDate.compareTo(endDate) > 0) {
			canSave = false;
			display.getStartDateView().setAndShowErrorMessage(
					"The start date must be earlier than the end date.");
		}

		final String experimentLabel = display.getExperimentLabelView()
				.getValue();
		if ("".equals(experimentLabel)) {
			display.getExperimentLabelView().setAndShowErrorMessage(
					"You must enter a unique experiment label.");
			canSave = false;
		}

		final String experimentDir = display.getExperimentDirView().getValue();
		if ("".equals(experimentDir)) {
			display.getExperimentDirView().setAndShowErrorMessage(
					"You must enter an experiment directory.");
			canSave = false;
		}

		final String mefDir = display.getMefDirView().getValue();
		if ("".equals(mefDir)) {
			display.getMefDirView().setAndShowErrorMessage(
					"You must enter a Mef directory.");
			canSave = false;
		}

		String reportFile = display.getReportFileView().getValue();
		if ("".equals(reportFile)) {
			// Report File is nullable.
			reportFile = null;
		}

		String imagesFile = display.getImagesFileView().getValue();
		if ("".equals(imagesFile)) {
			// Images file is nullable.
			imagesFile = null;
		}

		final Boolean published = display.getPublishedView().getValue();

		if (canSave) {
			final GwtRecording recording = new GwtRecording(
					experimentDir,
					mefDir,
					startDate,
					endDate,
					null,
					imagesFile,
					reportFile,
					null,
					null,
					null);

			final GwtExperiment experiment = new GwtExperiment(
					experimentLabel,
					recording,
					published.booleanValue(),
					null,
					null,
					null,
					null,
					null,
					null,
					null);

			service.createExperiment(parent, experiment,
					new AsyncCallback<GwtExperiment>() {

						@Override
						public void onFailure(Throwable caught) {
							display.hide();
							String message = "Could not create experiment: "
									+ caught.getMessage();
							Window.alert(message);
						}

						@Override
						public void onSuccess(GwtExperiment result) {
							callbackView.getExperimentTable()
									.getEntryList().add(result);
							Collections.sort(callbackView.getExperimentTable()
									.getEntryList(),
									GwtExperiment.LABEL_COMPARATOR);
							callbackView.getExperimentTable()
									.refreshProvider();
							display.hide();
						}
					});
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object source = event.getSource();
		if (display.getStartDateView().isWrapping(source)) {
			display.getStartDateView().hideErrorMessage();
		} else if (display.getEndDateView().isWrapping(source)) {
			display.getEndDateView().hideErrorMessage();
		} else if (display.getExperimentDirView().isWrapping(source)) {
			display.getExperimentDirView().hideErrorMessage();
		} else if (display.getMefDirView().isWrapping(source)) {
			display.getMefDirView().hideErrorMessage();
		} else if (display.getExperimentLabelView().isWrapping(source)) {
			display.getExperimentLabelView().hideErrorMessage();
		} else if (display.getReportFileView().isWrapping(source)) {
			display.getReportFileView().hideErrorMessage();
		} else if (display.getImagesFileView().isWrapping(source)) {
			display.getImagesFileView().hideErrorMessage();
		}
	}
}
