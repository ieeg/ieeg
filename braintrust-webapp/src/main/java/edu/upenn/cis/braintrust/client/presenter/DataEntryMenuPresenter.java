/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.CreatePatientEvent;
import edu.upenn.cis.braintrust.client.event.EditAnimalEvent;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.client.event.EditSpeciesEvent;
import edu.upenn.cis.braintrust.client.presenter.ManagedEnumPresenter.IManagedEnumView;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.OptionsLists;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;

/**
 * Controls the data entry menu screen.
 * 
 * @author John Frommeyer
 * 
 */
public class DataEntryMenuPresenter implements IPresenter, ClickHandler,
		KeyDownHandler, ChangeHandler, IOrganizationChangeHandler {

	/**
	 * Defines the kind of view we can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IDataEntryMenuView extends IView {
		/**
		 * Returns the create patient button.
		 * 
		 * @return the create patient button
		 */
		HasClickHandlers getCreatePatientButton();

		HasClickHandlers getCreateSpeciesButton();

		/**
		 * Returns the retrieve patient button.
		 * 
		 * @return the retrieve patient button
		 */
		HasClickHandlers getRetrievePatientButton();

		/**
		 * Returns the patient id field. The string value should be "" if no
		 * value has been entered.
		 * 
		 * @return the patient id field
		 */
		HasValue<String> getPatientId();

		/**
		 * Sets and shows an error message for the retrieval of a patient.
		 * 
		 * @param message
		 */
		void setAndShowRetrievePatientErrorMessage(String message);

		/**
		 * Clears and hides any patient retrieval error message.
		 * 
		 */
		void hideRetrievePatientErrorMessage();

		/**
		 * Adds a {@code KeyPressHandler} to the retrieve patient field.
		 * 
		 * @param handler
		 */
		void addRetrievePatientKeyDownHandler(KeyDownHandler handler);

		ISingleSelectionListAndErrorMessage getEditSpeciesView();

		HasClickHandlers getCreateAnimalButton();

		HasClickHandlers getRetrieveAnimalButton();

		HasValue<String> getAnimalId();

		IHasValueAndErrorMessage<String> getCreateSpeciesView();

		void addRetrieveAnimalKeyDownHandler(KeyDownHandler handler);

		void setAndShowRetrieveAnimalErrorMessage(String message);

		void hideRetrieveAnimalErrorMessage();

		HasClickHandlers getEditSpeciesButton();

		HasClickHandlers getDeleteSpeciesButton();

		IManagedEnumView<GwtDrug> getDrugView();

		IManagedEnumView<GwtStimRegion> getStimRegionView();

		IManagedEnumView<GwtStimType> getStimTypeView();

		HasValue<String> getNewAnimalLabel();

		void setAndShowCreateAnimalErrorMessage(String string);

		ISingleSelectionList getNewAnimalSpeciesView();

		ISingleSelectionList getNewAnimalStrainView();

		void hideCreateAnimalErrorMessage();

		void addNewAnimalLabelKeyDownHandler(
				KeyDownHandler handler);

		ISingleSelectionList getNewAnimalOrganizationView();

		IOrganizationTable getOrganizationView();
	}

	private final class RetrieveSpeciesCallBack implements
			AsyncCallback<List<GwtSpecies>> {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Failed to load species.");
		}

		@Override
		public void onSuccess(List<GwtSpecies> species) {
			updateSpeciesModel(species);
		}
	}

	private final IManagedEnumFactory<GwtDrug> gwtDrugFactory = new
			IManagedEnumFactory<GwtDrug>() {
				@Override
				public GwtDrug newInstance(String label, Long id,
						Integer version) {
					return new GwtDrug(label, id, version);
				}
			};
	private final IManagedEnumService<GwtDrug> gwtDrugService = new IManagedEnumService<GwtDrug>() {

		@Override
		public void modifyLabel(GwtDrug modifiedValue,
				AsyncCallback<GwtDrug> callback) {
			service.modifyDrugLabel(modifiedValue, callback);
		}

		@Override
		public void createValue(GwtDrug newValue,
				AsyncCallback<GwtDrug> callback) {
			service.createDrug(newValue, callback);
		}

		@Override
		public void deleteValue(GwtDrug value,
				AsyncCallback<Void> callback) {
			service.deleteDrug(value, callback);
		}
	};
	private final IManagedEnumFactory<GwtStimRegion> gwtStimRegionFactory = new
			IManagedEnumFactory<GwtStimRegion>() {
				@Override
				public GwtStimRegion newInstance(String label, Long id,
						Integer version) {
					return new GwtStimRegion(label, id, version);
				}
			};
	private final IManagedEnumService<GwtStimRegion> gwtStimRegionService = new IManagedEnumService<GwtStimRegion>() {

		@Override
		public void modifyLabel(GwtStimRegion modifiedValue,
				AsyncCallback<GwtStimRegion> callback) {
			service.modifyStimRegionLabel(modifiedValue, callback);
		}

		@Override
		public void createValue(GwtStimRegion newValue,
				AsyncCallback<GwtStimRegion> callback) {
			service.createStimRegion(newValue, callback);
		}

		@Override
		public void deleteValue(GwtStimRegion value,
				AsyncCallback<Void> callback) {
			service.deleteStimRegion(value, callback);
		}
	};
	private final IManagedEnumFactory<GwtStimType> gwtStimTypeFactory = new
			IManagedEnumFactory<GwtStimType>() {
				@Override
				public GwtStimType newInstance(String label, Long id,
						Integer version) {
					return new GwtStimType(label, id, version);
				}
			};
	private final IManagedEnumService<GwtStimType> gwtStimTypeService = new IManagedEnumService<GwtStimType>() {

		@Override
		public void modifyLabel(GwtStimType modifiedValue,
				AsyncCallback<GwtStimType> callback) {
			service.modifyStimTypeLabel(modifiedValue, callback);
		}

		@Override
		public void createValue(GwtStimType newValue,
				AsyncCallback<GwtStimType> callback) {
			service.createStimType(newValue, callback);
		}

		@Override
		public void deleteValue(GwtStimType value,
				AsyncCallback<Void> callback) {
			service.deleteStimType(value, callback);
		}
	};

	private final HandlerManager eventBus;
	private final IDataEntryMenuView display;
	private final BrainTrustServiceAsync service;
	private final OrganizationPresenter organizationPresenter;
	private final ManagedEnumPresenter<GwtDrug> drugPresenter;
	private final ManagedEnumPresenter<GwtStimRegion> stimRegionPresenter;
	private final ManagedEnumPresenter<GwtStimType> stimTypePresenter;
	private List<GwtSpecies> speciesList = newArrayList();
	private List<GwtStrain> strainList = newArrayList();

	/**
	 * Create the menu presenter.
	 * 
	 * @param theEventBus
	 * @param theDisplay
	 * @param theService
	 */
	public DataEntryMenuPresenter(HandlerManager theEventBus,
			IDataEntryMenuView theDisplay, BrainTrustServiceAsync theService) {
		this.eventBus = theEventBus;
		this.display = theDisplay;
		this.service = theService;
		organizationPresenter = new OrganizationPresenter(
				eventBus,
				display.getOrganizationView(),
				service);
		drugPresenter = new ManagedEnumPresenter<GwtDrug>(
				display.getDrugView(),
				gwtDrugFactory,
				gwtDrugService,
				"Add drug");
		stimRegionPresenter = new ManagedEnumPresenter<GwtStimRegion>(
				display.getStimRegionView(),
				gwtStimRegionFactory,
				gwtStimRegionService,
				"Add stimulation region");
		stimTypePresenter = new ManagedEnumPresenter<GwtStimType>(
				display.getStimTypeView(),
				gwtStimTypeFactory,
				gwtStimTypeService,
				"Add stimulation type");
		this.service.retrieveOptionsLists(new AsyncCallback<OptionsLists>() {

			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof BrainTrustAuthorizationException) {
					Window.alert("You are not authorized to retrieve options.");
				} else {
					Window.alert("Failed to load options.");
				}

			}

			@Override
			public void onSuccess(OptionsLists result) {

				// Orgs
				final List<GwtOrganization> orgs = result.getOrganizations();
				final String[] organizationChoices = new String[orgs
						.size()];
				for (int i = 0; i < orgs.size(); i++) {
					final GwtOrganization org = orgs.get(i);
					organizationChoices[i] = org.toString();
				}
				display.getNewAnimalOrganizationView().setChoices(
						organizationChoices);
				organizationPresenter.populateDisplay(orgs);

				// Species
				updateSpeciesModel(result.getSpecies());

				// Drugs
				drugPresenter
						.populateDisplay(result.getDrugs());

				// StimRegions
				stimRegionPresenter
						.populateDisplay(result.getStimRegions());

				// StimTypes
				stimTypePresenter
						.populateDisplay(result.getStimTypes());
			}
		});

		bind();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());

	}

	private void bind() {
		organizationPresenter.setOrganizationChangeHandler(this);
		display.getCreatePatientButton().addClickHandler(this);
		display.getCreateAnimalButton().addClickHandler(this);
		display.addNewAnimalLabelKeyDownHandler(this);
		display.getRetrievePatientButton().addClickHandler(this);
		display.getRetrieveAnimalButton().addClickHandler(this);
		display.addRetrievePatientKeyDownHandler(this);
		display.addRetrieveAnimalKeyDownHandler(this);
		display.getCreateSpeciesView().addKeyDownHandler(this);
		display.getCreateSpeciesButton().addClickHandler(this);
		display.getEditSpeciesButton().addClickHandler(this);
		display.getDeleteSpeciesButton().addClickHandler(this);

		// This change handler updates the strain choices when a new species is
		// selected.
		display.getNewAnimalSpeciesView().addChangeHandler(this);

		// This change handler clears any error message associated with new
		// animal creation when the selection of any of the new animal listboxes
		// is changed.
		final ChangeHandler hideNewAnimalErrorMessage = new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				display.hideCreateAnimalErrorMessage();
			}
		};
		display.getNewAnimalOrganizationView().addChangeHandler(
				hideNewAnimalErrorMessage);
		display.getNewAnimalSpeciesView().addChangeHandler(
				hideNewAnimalErrorMessage);
		display.getNewAnimalStrainView().addChangeHandler(
				hideNewAnimalErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getCreatePatientButton()) {
			eventBus.fireEvent(new CreatePatientEvent(display
					.getOrganizationView().getEntryList()));
		} else if (source == display.getCreateAnimalButton()) {
			onCreateAnimalClick();
		} else if (source == display.getRetrievePatientButton()) {
			onRetrievePatientClick();
		} else if (source == display.getRetrieveAnimalButton()) {
			onRetrieveAnimalClick();
		} else if (source == display.getCreateSpeciesButton()) {
			onCreateSpeciesClick();
		} else if (source == display.getEditSpeciesButton()) {
			onEditSpeciesClick();
		} else if (source == display.getDeleteSpeciesButton()) {
			onDeleteSpeciesClick();
		}
	}

	private void onDeleteSpeciesClick() {
		final int i = display.getEditSpeciesView().getSelectedIndex();
		if (i < 0 || i >= speciesList.size()) {
			Window
					.alert("There are no species loaded.");
		} else {
			final GwtSpecies species = speciesList.get(i);
			if (species == null) {
				Window.alert("Species not found.");
			} else {
				service.deleteSpecies(species,
						new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								display.getEditSpeciesView()
										.setAndShowErrorMessage(
												"An error occured while attempting to delete "
														+ species.getLabel()
														+ ": "
														+ caught.getMessage());
							}

							@Override
							public void onSuccess(Void result) {
								service.retrieveSpecies(new RetrieveSpeciesCallBack());
							}
						});
			}

		}
	}

	private void onEditSpeciesClick() {
		final int i = display.getEditSpeciesView().getSelectedIndex();
		if (i < 0 || i >= speciesList.size()) {
			Window
					.alert("There are no species loaded.");
		} else {
			final GwtSpecies species = speciesList.get(i);
			if (species == null) {
				Window.alert("Species not found.");
			} else {
				service.findStrains(species,
						new AsyncCallback<List<GwtStrain>>() {

							@Override
							public void onFailure(Throwable caught) {
								display.getEditSpeciesView()
										.setAndShowErrorMessage(
												"An error occured while attempting to retrieve strains for species: "
														+ species.getLabel()
														+ ".");
							}

							@Override
							public void onSuccess(List<GwtStrain> result) {
								eventBus.fireEvent(new EditSpeciesEvent(
										species, result));
							}
						});
			}

		}
	}

	private void onCreateSpeciesClick() {
		final String label = display.getCreateSpeciesView().getValue().trim();
		if (!"".equals(label)) {
			Optional<GwtSpecies> speciesOpt = tryFind(speciesList,
					compose(equalTo(label), GwtSpecies.getLabel));
			if (!speciesOpt.isPresent()) {
				final GwtSpecies newSpecies = new GwtSpecies(label, null, null);
				final Set<GwtStrain> strains = newHashSet(new GwtStrain("none",
						null, null, label, null));
				service.createSpecies(newSpecies,
						strains,
						new AsyncCallback<GwtSpecies>() {

							@Override
							public void onFailure(Throwable caught) {
								display.getCreateSpeciesView()
										.setAndShowErrorMessage(
												"An error occured while attempting to create species "
														+ label);
							}

							@Override
							public void onSuccess(GwtSpecies result) {
								// Reload the data entry menu
								display.getCreateSpeciesView().setValue("");
								service.retrieveSpecies(new RetrieveSpeciesCallBack());
							}
						});
			} else {
				display.getCreateSpeciesView().setAndShowErrorMessage(
						"The species " + label + " already exists.");
			}
		} else {
			display.getCreateSpeciesView().setAndShowErrorMessage(
					"Species name cannot be empty");
		}
	}

	private void onRetrievePatientClick() {
		final String patientId = display.getPatientId().getValue().trim();
		if ("".equals(patientId)) {
			display
					.setAndShowRetrievePatientErrorMessage("You must enter a patient id.");
		} else {
			service.retrievePatientByLabel(patientId,
					new AsyncCallback<GwtPatient>() {

						@Override
						public void onFailure(Throwable caught) {
							display
									.setAndShowRetrievePatientErrorMessage("An error occured while attempting to retrieve patient: "
											+ patientId + ".");
						}

						@Override
						public void onSuccess(
								final GwtPatient retrievePatientResult) {
							if (retrievePatientResult == null) {
								display
										.setAndShowRetrievePatientErrorMessage("Patient with id "
												+ patientId
												+ " could not be found.");
							} else {
								service.retrieveOrganizations(new AsyncCallback<List<GwtOrganization>>() {

									@Override
									public void onFailure(Throwable caught) {
										if (caught instanceof BrainTrustAuthorizationException) {
											Window.alert("You are not authorized to list organizations.");
										} else {
											Window.alert("Failed to load organizations.");
										}
									}

									@Override
									public void onSuccess(
											List<GwtOrganization> retrieveOrgsResult) {
										eventBus.fireEvent(new EditPatientEvent(
												retrieveOrgsResult,
												retrievePatientResult));
									}
								});

							}
						}
					});

		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.KeyPressHandler#onKeyPress(com.google.gwt.event.dom.client.KeyPressEvent)
	 */
	@Override
	public void onKeyDown(KeyDownEvent event) {
		Object source = event.getSource();
		if (source == display.getPatientId()) {
			display.hideRetrievePatientErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onRetrievePatientClick();
			}
		} else if (source == display.getAnimalId()) {
			display.hideRetrieveAnimalErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onRetrieveAnimalClick();
			}
		} else if (display.getCreateSpeciesView().isWrapping(source)) {
			display.getCreateSpeciesView().hideErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onCreateSpeciesClick();
			}
		} else if (source == display.getNewAnimalLabel()) {
			display.hideCreateAnimalErrorMessage();
		}
	}

	private void onCreateAnimalClick() {
		boolean canCreate = true;
		final List<GwtOrganization> organizationList = display
				.getOrganizationView().getEntryList();
		final int organizationIdx = display.getNewAnimalOrganizationView()
				.getSelectedIndex();
		if (organizationIdx < 0
				|| organizationIdx >= organizationList.size()) {
			canCreate = false;
			display.setAndShowCreateAnimalErrorMessage("There are no organizations loaded");
		}
		final GwtOrganization organization = organizationList
				.get(organizationIdx);
		String label = null;
		if (organization == null) {
			canCreate = false;
			display.setAndShowCreateAnimalErrorMessage("Organization not found.");
		} else {
			final RegExp animalLabelPattern = RegExp.compile("^"
					+ organization.getCode() + "_A[0-9]{4}$");

			label = display.getNewAnimalLabel().getValue().trim();

			if (!animalLabelPattern.test(label)) {
				canCreate = false;
				display.setAndShowCreateAnimalErrorMessage("Animal id must be of the form "
						+ organization.getCode()
						+ "_Auxyz with u, x, y, & z in [0-9] for the chosen organization.");
			}
		}
		final int strainIdx = display.getNewAnimalStrainView()
				.getSelectedIndex();
		if (strainIdx < 0 || strainIdx >= strainList.size()) {
			canCreate = false;
			display.setAndShowCreateAnimalErrorMessage("There are no strains loaded");
		}
		final GwtStrain strain = strainList.get(strainIdx);
		if (strain == null) {
			display.setAndShowCreateAnimalErrorMessage("Strain not found");
		}

		if (canCreate) {
			final GwtAnimal unsavedAnimal = new GwtAnimal(
					label,
					organization,
					strain,
					null,
					null);
			service.saveAnimal(unsavedAnimal,
					new AsyncCallback<GwtAnimal>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("An error occurred while attempting to save animal "
									+ unsavedAnimal.getLabel());
						}

						@Override
						public void onSuccess(GwtAnimal result) {
							final ImmutableList<GwtExperiment> empty = ImmutableList
									.of();
							final GwtAnimalExperimentPair pair = new GwtAnimalExperimentPair(
									result, empty);
							eventBus.fireEvent(new EditAnimalEvent(
									pair,
									display.getOrganizationView()
											.getEntryList(),
									speciesList,
									display.getDrugView()
											.getValueList(),
									display.getStimRegionView()
											.getValueList(),
									display.getStimTypeView()
											.getValueList()));
						}
					});

		}
	}

	private void onRetrieveAnimalClick() {
		final String animalLabel = display.getAnimalId().getValue().trim();
		if ("".equals(animalLabel)) {
			display
					.setAndShowRetrieveAnimalErrorMessage("You must enter a animal id.");
		} else {
			service.retrieveAnimalByLabel(animalLabel,
					new AsyncCallback<GwtAnimalExperimentPair>() {

						@Override
						public void onFailure(Throwable caught) {
							display
									.setAndShowRetrieveAnimalErrorMessage("An error occured while attempting to retrieve animal: "
											+ animalLabel + ".");
						}

						@Override
						public void onSuccess(GwtAnimalExperimentPair result) {
							if (result == null) {
								display
										.setAndShowRetrieveAnimalErrorMessage("Animal with id "
												+ animalLabel
												+ " could not be found.");
							} else {
								eventBus.fireEvent(new EditAnimalEvent(
										result,
										display.getOrganizationView()
												.getEntryList(),
										speciesList,
										display.getDrugView().getValueList(),
										display.getStimRegionView()
												.getValueList(),
										display.getStimTypeView()
												.getValueList()));
							}
						}
					});

		}
	}

	@Override
	public void onChange(ChangeEvent event) {
		final ISingleSelectionList strainView = display
				.getNewAnimalStrainView();
		strainView.clear();
		final int i = display.getNewAnimalSpeciesView().getSelectedIndex();
		if (i < 0 || i >= speciesList.size()) {
			Window
					.alert("There are no species loaded.");
		} else {
			final GwtSpecies species = speciesList.get(i);
			if (species == null) {
				Window.alert("Species not found.");
			} else {
				service.findStrains(species,
						new AsyncCallback<List<GwtStrain>>() {

							@Override
							public void onFailure(Throwable caught) {
								display.setAndShowCreateAnimalErrorMessage(
										"An error occured while attempting to retrieve strains for species: "
												+ species.getLabel()
												+ ".");
							}

							@Override
							public void onSuccess(List<GwtStrain> result) {
								strainList = result;
								final ISingleSelectionList strainView = display
										.getNewAnimalStrainView();
								final String[] strainChoices = new String[result
										.size()];
								int i = 0;
								for (final GwtStrain strain : result) {
									strainChoices[i++] = strain.getLabel();
								}
								strainView.setChoices(strainChoices);
							}
						});
			}

		}

	}

	@Override
	public void onOrganizationChange(List<GwtOrganization> newOrgs) {
		String[] orgChoices = new String[newOrgs.size()];
		for (int i = 0; i < newOrgs.size(); i++) {
			orgChoices[i] = newOrgs.get(i).toString();
		}
		display.getNewAnimalOrganizationView().setChoices(orgChoices);
		if (orgChoices.length > 0) {
			display.getNewAnimalOrganizationView().setSelectedIndex(0);
		}
	}

	private void updateSpeciesModel(List<GwtSpecies> species) {
		speciesList = species;
		String[] speciesChoices = new String[speciesList.size()];
		for (int i = 0; i < speciesList.size(); i++) {
			speciesChoices[i] = speciesList.get(i).getLabel();
		}
		display.getEditSpeciesView().setChoices(speciesChoices);
		display.getNewAnimalSpeciesView().setChoices(speciesChoices);
		if (speciesChoices.length > 0) {
			display.getNewAnimalSpeciesView().setSelectedIndex(0);
			// Fire event to trigger onChange() to load the related strains.
			display.getNewAnimalSpeciesView().fireChangeEvent();
		}
	}

}
