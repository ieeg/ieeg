/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionList;
import edu.upenn.cis.braintrust.client.presenter.ISingleSelectionListAndErrorMessage;

/**
 * A single selection list box with an error message.
 * 
 * @author John Frommeyer
 * 
 */
public class SingleSelectionListBoxAndErrorMessageView implements
		ISingleSelectionListAndErrorMessage {

	private final ISingleSelectionList listBoxView;
	private final Label errorMessageLabel;

	/**
	 * Create a SingleSelectionListBoxAndErrorMessageView.
	 * 
	 * @param listBox
	 * @param errorMessageLabel
	 */
	SingleSelectionListBoxAndErrorMessageView(final ListBox listBox,
			final Label errorMessageLabel) {
		listBoxView = new SingleSelectionListBoxView(listBox);
		this.errorMessageLabel = errorMessageLabel;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#getSelectedIndex()
	 */
	@Override
	public int getSelectedIndex() {
		return listBoxView.getSelectedIndex();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#setSelectedIndex(int)
	 */
	@Override
	public void setSelectedIndex(int i) {
		listBoxView.setSelectedIndex(i);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IListView#setChoices(java.lang.String[])
	 */
	@Override
	public void setChoices(String[] choices) {
		listBoxView.setChoices(choices);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorMessageLabel);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(errorMessageLabel, errorMessage);
	}

	@Override
	public void clear() {
		listBoxView.clear();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.ISingleSelectionListBoxView#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return listBoxView.addChangeHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return listBoxView.isWrapping(o);
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.listBoxView.setEnabled(enabled);
	}

	@Override
	public void fireChangeEvent() {
		listBoxView.fireChangeEvent();
	}

}
