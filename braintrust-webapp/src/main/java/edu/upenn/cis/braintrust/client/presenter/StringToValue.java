/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

/**
 * Methods for converting {@code String}s to other types.
 * 
 * @author John Frommeyer
 * 
 */
public class StringToValue {

	public static Integer toInteger(String s) {
		Integer value = null;
		try {
			value = Integer.valueOf(s);
		} catch (NumberFormatException e) {
			// We just return null.
		}
		return value;
	}

	public static Integer toNonNegativeInteger(String s) {
		Integer value = Integer.valueOf(-1);
		try {
			value = Integer.valueOf(s);
		} catch (NumberFormatException e) {
			// We'll just return null.
		}
		if (Integer.valueOf(0).compareTo(value) <= 0) {
			return value;
		}
		return null;
	}

	public static Long toNonNegativeLong(String s) {
		Long value = Long.valueOf(-1);
		try {
			value = Long.valueOf(s);
		} catch (NumberFormatException e) {
			// Is it written is scientific notation? No regular expressions in
			// 2.0!
			boolean matches = s.contains("e") || s.contains("E");
			if (matches) {
				Double dValue = Double.valueOf(-1.0);
				try {
					dValue = Double.valueOf(s);
				} catch (NumberFormatException de) {
					// we will return null.
				}
				value = Long.valueOf(dValue.longValue());
			}
		}
		if (Long.valueOf(0).compareTo(value) <= 0) {
			return value;
		}
		return null;
	}

	public static Double toDouble(String s) {
		Double value = null;
		try {
			value = Double.valueOf(s);
		} catch (NumberFormatException e) {
			// We just return null.
		}
		return value;
	}

	public static Float toFloat(String s) {
		Float value = null;
		try {
			value = Float.valueOf(s);
		} catch (NumberFormatException e) {
			// We just return null.
		}
		return value;
	}

	public static Long toLong(String s) {
		Long value = null;
		try {
			value = Long.valueOf(s);
		} catch (NumberFormatException e) {
			// We just return null.
		}
		return value;
	}

	/** Prevent inheritance and instantiation. */
	private StringToValue() {
		throw new AssertionError("can't instantiate a " + "StringToValue");
	}
}
