/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtPatient;

/**
 * Fired when a {@code GwtHospitalAdmission} needs to be created.
 * 
 * @author John Frommeyer
 * 
 */
public class CreateHospitalAdmissionEvent extends GwtEvent<CreateHospitalAdmissionEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<CreateHospitalAdmissionEventHandler> TYPE = new Type<CreateHospitalAdmissionEventHandler>();
	private final GwtPatient patient;
	
	/**
	 * Creates an {@code CreateHospitalAdmissionEvent} for the given patient.
	 * 
	 * @param thePatient
	 */
	public CreateHospitalAdmissionEvent(final GwtPatient thePatient) {
		this.patient = thePatient;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreateHospitalAdmissionEventHandler handler) {
		handler.onCreateHospitalAdmission(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateHospitalAdmissionEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Returns the patient for which the study will be created.
	 * @return the patient
	 */
	public GwtPatient getPatient() {
		return patient;
	}

}
