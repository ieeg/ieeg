/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.common.base.Strings;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IExperimentSelectionHandler;
import edu.upenn.cis.braintrust.client.presenter.IExperimentTable;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

/**
 * A table for animal experiments
 * 
 * @author John Frommeyer
 * 
 */
public class ExperimentTableWidget extends Composite implements
		IExperimentTable {
	private static ExperimentViewUiBinder uiBinder = GWT
			.create(ExperimentViewUiBinder.class);

	interface ExperimentViewUiBinder extends
			UiBinder<Widget, ExperimentTableWidget> {}

	// Experiment
	@UiField(provided = true)
	DataEntryTableAndPagerWidget<GwtExperiment> experimentTableWidget;
	@UiField
	Button addEntryButton;

	private static final GlobalCss css;

	private final EditTextCell experimentLabelCell = new EditTextCell();
	private final Column<GwtExperiment, String> experimentLabelColumn = new Column<GwtExperiment, String>(
			experimentLabelCell) {

		@Override
		public String getValue(GwtExperiment object) {
			return object.getLabel();
		}
	};

	private final Column<GwtExperiment, Boolean> isPublishedColumn = new Column<GwtExperiment, Boolean>(
			new CheckboxCell()) {

		@Override
		public Boolean getValue(GwtExperiment object) {
			return Boolean.valueOf(object.isPublished());
		}
	};

	private final Column<GwtExperiment, String> ageColumn = new Column<GwtExperiment, String>(
			new EditTextCell()) {

		@Override
		public String getValue(GwtExperiment object) {
			return Strings.nullToEmpty(object.getAge());
		}
	};

	private final Column<GwtExperiment, String> stimRegionColumn;

	private final Column<GwtExperiment, String> stimTypeColumn;

	private final EditTextCell stimDurationCell = new EditTextCell();
	private final Column<GwtExperiment, String> stimDurationColumn = new Column<GwtExperiment, String>(
			stimDurationCell) {

		@Override
		public String getValue(GwtExperiment object) {
			return object.getStimDurationMs() == null ? "" : object
					.getStimDurationMs().toString();
		}
	};

	private final EditTextCell stimIsiCell = new EditTextCell();
	private final Column<GwtExperiment, String> stimIsiColumn = new Column<GwtExperiment, String>(
			stimIsiCell) {

		@Override
		public String getValue(GwtExperiment object) {
			return object.getStimIsiMs() == null ? "" : object
					.getStimIsiMs().toString();
		}
	};

	private final Column<GwtExperiment, String> deleteColumn = new Column<GwtExperiment, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtExperiment object) {
			return "x";
		}
	};
	private final SingleSelectionModel<GwtExperiment> experimentSelectionModel = new SingleSelectionModel<GwtExperiment>(
			KeyProviderMap.getMap().getProvider(GwtExperiment.class));
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public ExperimentTableWidget(List<String> stimRegions,
			List<String> stimTypes) {
		// Experiment
		final CellTable<GwtExperiment> experimentTable = new CellTable<GwtExperiment>(
				3,
				KeyProviderMap.getMap().getProvider(GwtExperiment.class));
		experimentTable.setWidth("100%", false);
		experimentTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

		experimentTable
				.setSelectionModel(experimentSelectionModel);
		experimentTable.addColumn(experimentLabelColumn, "Experiment Label");
		experimentTable.addColumn(isPublishedColumn, "Visible to Searches?");
		experimentTable.addColumn(ageColumn, "Age");

		stimRegionColumn = new Column<GwtExperiment, String>(
				new SelectionCell(stimRegions)) {

			@Override
			public String getValue(GwtExperiment object) {
				return object.getStimRegion() == null ? "" : object
						.getStimRegion().getLabel();
			}
		};
		experimentTable.addColumn(stimRegionColumn, "Stimulation Region");

		stimTypeColumn = new Column<GwtExperiment, String>(new SelectionCell(
				stimTypes)) {

			@Override
			public String getValue(GwtExperiment object) {
				return object.getStimType() == null ? "" : object
						.getStimType().getLabel();
			}

		};
		experimentTable.addColumn(stimTypeColumn, "Stimulation Type");
		experimentTable.addColumn(stimDurationColumn,
				"Stimulation Duration (ms)");
		experimentTable.addColumn(stimIsiColumn, "Stimulation ISI (ms)");
		experimentTable.addColumn(deleteColumn, "Delete");
		experimentTableWidget = new DataEntryTableAndPagerWidget<GwtExperiment>(
				"Experiments", experimentTable);

		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void addExperimentSelectionHandler(
			IExperimentSelectionHandler handler) {
		experimentSelectionModel
				.addSelectionChangeHandler(new ExperimentSelectionHandler(
						experimentSelectionModel, handler));
	}

	@Override
	public HasClickHandlers getAddEntryButton() {
		return addEntryButton;
	}

	@Override
	public List<GwtExperiment> getEntryList() {
		return experimentTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		experimentTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		experimentTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return experimentTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setExperimentDeleter(FieldUpdater<GwtExperiment, String> deleter) {
		deleteColumn.setFieldUpdater(deleter);
	}

	@Override
	public void setExperimentLabelUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		experimentLabelColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearExperimentLabelView(GwtExperiment experiment) {
		experimentLabelCell.clearViewData(experiment.getId());
	}

	@Override
	public void setExperimentAgeUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		ageColumn.setFieldUpdater(updater);
	}

	@Override
	public void setExperimentStimDurationUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		stimDurationColumn.setFieldUpdater(updater);
	}

	@Override
	public void setExperimentStimIsiUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		stimIsiColumn.setFieldUpdater(updater);
	}

	@Override
	public void setExperimentStimRegionUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		stimRegionColumn.setFieldUpdater(updater);
	}

	@Override
	public void setExperimentStimTypeUpdater(
			FieldUpdater<GwtExperiment, String> updater) {
		stimTypeColumn.setFieldUpdater(updater);
	}

	@Override
	public void setExperimentIsPublishedUpdater(
			FieldUpdater<GwtExperiment, Boolean> updater) {
		isPublishedColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearExperimentStimDurationView(GwtExperiment experiment) {
		stimDurationCell.clearViewData(experiment.getId());
	}

	@Override
	public void clearExperimentStimIsiView(GwtExperiment experiment) {
		stimIsiCell.clearViewData(experiment.getId());
	}

}
