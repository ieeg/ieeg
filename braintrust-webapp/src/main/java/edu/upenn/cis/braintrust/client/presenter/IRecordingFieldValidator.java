/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

public interface IRecordingFieldValidator<T> {
	static final IRecordingFieldValidator<String> noValidationString = new IRecordingFieldValidator<String>() {

		@Override
		public boolean isValidFieldValue(String fieldValue) {
			return true;
		}

		@Override
		public void clearFieldView(IRecordingTable display,
				GwtRecording recording) {

		}
	};

	boolean isValidFieldValue(T fieldValue);

	void clearFieldView(IRecordingTable display, GwtRecording recording);
}
