/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.BrainTrustPresenter.IBrainTrustView;

/**
 * The main BrainTrust interface.
 * 
 * @author Sam Donnelly
 */
public class BrainTrustView
		extends Composite
		implements IBrainTrustView {

	@UiField
	ScrollPanel centerPanel;

	interface BrainTrustViewUiBinder extends UiBinder<Widget, BrainTrustView> {}

	private static BrainTrustViewUiBinder uiBinder = GWT
			.create(BrainTrustViewUiBinder.class);

	/**
	 * Create the study search view.
	 */
	public BrainTrustView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public HasWidgets getMainPanel() {
		return centerPanel;
	}
}
