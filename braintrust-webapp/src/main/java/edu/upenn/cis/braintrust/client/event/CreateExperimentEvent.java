/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter.IAnimalView;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;

/**
 * Fired when an experiment is created.
 * 
 * @author John Frommeyer
 * 
 */
public class CreateExperimentEvent extends
		GwtEvent<CreateExperimentEventHandler> {

	/** The {@code Type} of this event. */
	public static final Type<CreateExperimentEventHandler> TYPE = new Type<CreateExperimentEventHandler>();

	private final IAnimalView animalView;
	private final GwtAnimal animal;

	/**
	 * Create experiment event for the given animal and view for callbacks.
	 * 
	 * @param animal
	 */
	public CreateExperimentEvent(final GwtAnimal animal,
			final IAnimalView animalView) {
		this.animalView = animalView;
		this.animal = animal;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreateExperimentEventHandler handler) {
		handler.onCreateExperiment(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateExperimentEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * @return the callback view
	 */
	public IAnimalView getCallbackView() {
		return animalView;
	}

	/**
	 * @return the animal
	 */
	public GwtAnimal getAnimal() {
		return animal;
	}

}
