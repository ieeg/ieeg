/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.base.Preconditions.checkState;

import com.google.gwt.user.client.ui.ListBox;

import edu.upenn.cis.braintrust.client.presenter.IHasValue;
import edu.upenn.cis.braintrust.client.presenter.BooleanWithUnknownUtil;

/**
 * Order of ListBox needs to be unknown, true, false
 * 
 * @author John Frommeyer
 * 
 */
public class BooleanListBoxView implements IHasValue<Boolean> {

	private final ListBox listBox;
	BooleanListBoxView(ListBox listBox) {
		this.listBox = listBox;
		this.listBox.clear();
		ViewUtil.setListBoxChoices(this.listBox, BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN);
		//Default to Unknown
		this.listBox.setSelectedIndex(BooleanWithUnknownUtil.booleanToIndex(null));
	}

	@Override
	public void setValue(Boolean value) {
		final int index = BooleanWithUnknownUtil.booleanToIndex(value);
		listBox.setSelectedIndex(index);
	}

	@Override
	public Boolean getValue() {
		final int selected = listBox.getSelectedIndex();
		checkState(0 <= selected && selected < BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN.length,
				"Illegal value of selected index: "
						+ selected);
		final String strValue = BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN[selected];
		return BooleanWithUnknownUtil.stringToValue(strValue).orNull();
	}

}
