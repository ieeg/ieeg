/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Strings.nullToEmpty;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.exception.AmbiguousHospitalAdmissionException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.FileNotFoundException;

/**
 * Controls the create new study from file keys screen.
 * 
 * @author John Frommeyer
 * 
 */
public class StudyFromFileKeysPresenter implements IPresenter,
		ClickHandler,
		KeyDownHandler {

	/**
	 * @author John Frommeyer
	 * 
	 */
	private final class EditPatientClickHandler implements ClickHandler {

		private final GwtPatient result;

		/**
		 * 
		 * @param result
		 */
		private EditPatientClickHandler(GwtPatient result) {
			this.result = result;
		}

		@Override
		public void onClick(ClickEvent event) {
			service.retrieveOrganizations(new AsyncCallback<List<GwtOrganization>>() {

				@Override
				public void onFailure(
						Throwable caught) {
					if (caught instanceof BrainTrustAuthorizationException) {
						Window.alert("You are not authorized to retrieve options.");
					} else {
						Window.alert("Failed to load options.");
					}
				}

				@Override
				public void onSuccess(
						List<GwtOrganization> orgsResult) {
					final EditPatientEvent editPatientEvent = new EditPatientEvent(
							orgsResult,
							result);
					eventBus.fireEvent(editPatientEvent);
				}
			});

		}
	}

	/**
	 * Defines the kind of view we can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface IStudyFromFileKeysView extends IView {

		IHasEnabledAndClickHandlers getCreateStudyButton();

		void setAndShowCreateStudyErrorMessageList(List<String> stringList);

		void setAndShowCreateStudyErrorMessage(String string);

		void hideCreateStudyErrorMessage();

		HasValue<String> getFileKeys();

		HandlerRegistration addFileKeyKeyDownHandler(KeyDownHandler handler);

		HasHTML getCreateStudyResultsMessage();

		void hideInputShowResults();

		HasClickHandlers getGoAgainButton();

		HasClickHandlers getEditPatientButton();

		void setEditPatientButtonText(String text);

		void showInputHideResults();

		HasValue<Integer> getAgeAdmission();

		IHasEnabledAndClickHandlers getFileKeyLookupButton();

		IHasValueAndErrorMessage<String> getMefDir();

	}

	private final class RetrieveOrganizationsCallBack implements
			AsyncCallback<List<GwtOrganization>> {
		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Failed to load organizations.");
		}

		@Override
		public void onSuccess(List<GwtOrganization> result) {
			organizationList = result;
		}
	}

	private final HandlerManager eventBus;
	private final IStudyFromFileKeysView display;
	private final BrainTrustServiceAsync service;
	private List<GwtOrganization> organizationList = newArrayList();
	private static final String fileKeyPrefix = "Human_Data";
	private static final String orgPattern = "I\\d{3}";
	private static final RegExp orgRegExp = RegExp
			.compile("^" + orgPattern + "$");
	private static final String orgFormatExample = "Ixyz";
	private static final String subjectPattern = "P\\d{3}";
	private static final RegExp subjectRegExp = RegExp
			.compile("^" + subjectPattern + "$");
	private static final String subjectFormatExample = "Pxyz";
	private static final String datasetPattern = "D\\d{2}";
	private static final RegExp datasetRegExp = RegExp
			.compile("" + datasetPattern + "$");
	private static final String datasetFormatExample = "Dyz";
	private static final RegExp mefDirRegExp = RegExp.compile(
			"^"
					+ fileKeyPrefix + "/" + orgPattern + "/"
					+ subjectPattern + "/"
					+ datasetPattern + "/mef/?$");
	private static final String mefDirFormatExample = fileKeyPrefix + "/"
			+ orgFormatExample + "/" + subjectFormatExample + "/"
			+ datasetFormatExample + "/mef";
	private final Splitter lineSplitter = Splitter.on('\n')
			.trimResults()
			.omitEmptyStrings();

	private final Joiner lineJoiner = Joiner.on('\n').skipNulls();

	/**
	 * Create the menu presenter.
	 * 
	 * @param eventBus
	 * @param display
	 * @param service
	 */
	public StudyFromFileKeysPresenter(
			HandlerManager eventBus,
			IStudyFromFileKeysView display,
			BrainTrustServiceAsync service) {
		this.eventBus = eventBus;
		this.display = display;
		this.service = service;
		this.service.retrieveOrganizations(new RetrieveOrganizationsCallBack());
		bind();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());

	}

	private void bind() {
		display.getCreateStudyButton().addClickHandler(this);
		display.getFileKeyLookupButton().addClickHandler(this);
		display.addFileKeyKeyDownHandler(this);
		display.getMefDir().addKeyDownHandler(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (display.getCreateStudyButton().isWrapping(source)) {
			onCreateStudyClick();
		} else if (display.getFileKeyLookupButton().isWrapping(source)) {
			onLookupFileKeys();
		}
	}

	private void onLookupFileKeys() {
		display.getCreateStudyButton().setEnabled(false);
		display.getFileKeyLookupButton().setEnabled(false);
		final String mefPath = nullToEmpty(display.getMefDir().getValue())
				.trim();
		if (!mefDirRegExp.test(mefPath)) {
			display.getMefDir().setAndShowErrorMessage(
					"MEF directory must be of the form "
							+ mefDirFormatExample);
			display.getCreateStudyButton().setEnabled(true);
			display.getFileKeyLookupButton().setEnabled(true);
		} else {
			service.retrieveMefFileKeys(mefPath,
					new AsyncCallback<List<String>>() {

						@Override
						public void onFailure(Throwable caught) {
							if (caught instanceof BrainTrustAuthorizationException) {
								Window.alert("You are not authorized to view MEF file keys");
							} else if (caught instanceof BrainTrustUserException) {
								Window.alert(caught.getMessage());
							} else {
								Window.alert("Could not retrieve MEF file keys");
							}
							display.getCreateStudyButton().setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
						}

						@Override
						public void onSuccess(List<String> result) {
							if (result.isEmpty()) {
								Window.alert("No MEF files found in " + mefPath);
							} else {
								final String fileKeyLines = lineJoiner
										.join(result);
								display.getFileKeys().setValue(fileKeyLines);
							}
							display.getCreateStudyButton().setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
						}
					});
		}

	}

	private void onCreateStudyClick() {
		display.getCreateStudyButton().setEnabled(false);
		display.getFileKeyLookupButton().setEnabled(false);
		final List<String> fileKeys = lineSplitter.splitToList(display
				.getFileKeys().getValue());
		final FileKeyParser parser = new FileKeyParser(
				fileKeys,
				fileKeyPrefix,
				orgRegExp,
				orgFormatExample,
				subjectRegExp,
				subjectFormatExample,
				datasetRegExp,
				datasetFormatExample);

		boolean canCreate = true;
		Map<String, Set<GwtTrace>> channelPrefixToTraces = null;
		try {
			channelPrefixToTraces = parser
					.parse();
		} catch (FileKeyParserException e) {
			canCreate = false;
			display.setAndShowCreateStudyErrorMessageList(e
					.getParseErrors());
		}

		if (!canCreate) {
			display.getCreateStudyButton().setEnabled(true);
			display.getFileKeyLookupButton().setEnabled(true);
			return;
		} else {
			final String orgCode = parser.getOrgComponent();
			final GwtOrganization organization = tryFind(
					organizationList,
					compose(
							equalTo(orgCode),
							GwtOrganization.getCode))
					.orNull();
			if (organization == null) {
				display.setAndShowCreateStudyErrorMessage("Organization not found.");
				display.getCreateStudyButton().setEnabled(true);
				display.getFileKeyLookupButton().setEnabled(true);
				return;
			}
			final String patientName = orgCode + "_"
					+ parser.getSubjectComponent();
			final GwtRecording recording = new GwtRecording(
					parser.getDir(),
					parser.getMefDir(),
					Long.valueOf(0),
					Long.valueOf(0),
					null,
					null,
					null,
					null,
					null,
					null);
			final String studyName = patientName + "_"
					+ parser.getDatasetComponent();
			final GwtEegStudy study = new GwtEegStudy(
					studyName,
					recording,
					EegStudyType.EMU_RECORDING,
					null,
					null,
					null);
			final Integer ageAtAdmission = display.getAgeAdmission().getValue();
			final GwtHospitalAdmission admission = new GwtHospitalAdmission(
					ageAtAdmission,
					null,
					null);
			final GwtPatient patient = new GwtPatient(
					patientName,
					organization,
					Handedness.UNKNOWN,
					Ethnicity.UNKNOWN,
					Gender.UNKOWN,
					"unknown",
					null,
					null,
					null,
					null,
					null,
					null,
					null);
			service.savePatientWithNewStudy(
					patient,
					admission,
					study,
					channelPrefixToTraces,
					new AsyncCallback<GwtPatient>() {

						@Override
						public void onFailure(Throwable caught) {
							display.getCreateStudyButton()
									.setEnabled(true);
							display.getFileKeyLookupButton().setEnabled(true);
							if (caught instanceof DuplicateNameException) {
								final DuplicateNameException dne = (DuplicateNameException)
										caught;
								Window.alert("Patient " + patient.getLabel()
										+ " already has a study "
										+ dne.getName());
							} else if (caught instanceof AmbiguousHospitalAdmissionException) {
								Window.alert("Could not create study: "
										+ caught.getMessage());
							} else if (caught instanceof FileNotFoundException) {
								Window.alert("Could not create study: "
										+ caught.getMessage());
							} else if (caught instanceof BrainTrustUserException) {
								Window.alert("Could not create study: "
										+ caught.getMessage());
							} else {
								Window.alert("An error occurred while attempting to save patient "
										+ patient.getLabel());
							}
						}

						@Override
						public void onSuccess(
								final GwtPatient result) {
							final int admissionsCount = result.getAdmissions()
									.size();
							final String patientName = result
									.getLabel();
							String msg = null;
							if (admissionsCount == 0) {
								msg = "No admissions found for patient "
										+ patientName
										+ ". Please report this as an error.";
							} else if (admissionsCount == 1) {
								final GwtHospitalAdmission admission = getOnlyElement(result
										.getAdmissions());
								final int studiesCount = admission.getStudies()
										.size();
								if (studiesCount == 0) {
									msg = "No studies found for patient "
											+ patientName
											+ ". Please report this as an error.";
								} else if (studiesCount == 1) {
									msg = "Created patient " + patientName
											+ " and study "
											+ study.getLabel();
								} else {
									msg = "Added new study "
											+ study.getLabel() + " to patient "
											+ patientName;
								}
							} else {
								msg = "Added new study "
										+ study.getLabel() + " to patient "
										+ patientName;
							}
							display.getCreateStudyResultsMessage()
									.setHTML(msg);

							display.getGoAgainButton().addClickHandler(
									new ClickHandler() {
										@Override
										public void onClick(ClickEvent event) {
											// Clear the view
											display.getAgeAdmission().setValue(
													null);
											display.getFileKeys()
													.setValue(null);
											display.hideCreateStudyErrorMessage();

											display.showInputHideResults();
										}
									});
							display.setEditPatientButtonText("Edit patient "
									+ patientName);
							display.getEditPatientButton().addClickHandler(
									new EditPatientClickHandler(result));

							display.hideInputShowResults();

						}
					});

		}
	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		final Object source = event.getSource();
		if (source == display.getFileKeys()) {
			display.hideCreateStudyErrorMessage();
		} else if (display.getMefDir().isWrapping(source)) {
			display.getMefDir().hideErrorMessage();
		}
	}

}
