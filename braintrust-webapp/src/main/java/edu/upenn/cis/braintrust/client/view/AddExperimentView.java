/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.AddExperimentPresenter.IAddExperimentView;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * A screen to enter information about an experiment.
 * 
 * @author John Frommeyer
 * 
 */
public class AddExperimentView extends Composite implements IAddExperimentView {

	private static AddExperimentUiBinder uiBinder = GWT
			.create(AddExperimentUiBinder.class);

	interface AddExperimentUiBinder extends
			UiBinder<DialogBox, AddExperimentView> {}

	@UiField
	UutcDateWidget startDate;

	private final UutcView startDateView;

	@UiField
	UutcDateWidget endDate;
	private final UutcView endDateView;

	@UiField
	CheckBox publishedCheckBox;

	@UiField
	TextBox experimentLabel;
	@UiField
	Label experimentLabelErrorMessage;

	@UiField
	StudyDirectoriesWidget experimentDirs;

	private final IHasValueAndErrorMessage<String> experimentLabelView;
	private final IHasValueAndErrorMessage<String> experimentDirView;
	private final IHasValueAndErrorMessage<String> mefDirView;
	private final IHasValueAndErrorMessage<String> reportFileView;
	private final IHasValueAndErrorMessage<String> imagesFileView;

	@UiField
	Button saveButton;

	@UiField
	Button cancelButton;

	private final DialogBox dialogBox;

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Creates this view.
	 * 
	 */
	public AddExperimentView() {
		dialogBox = uiBinder.createAndBindUi(this);
		// Can access @UiField after calling createAndBindUi
		startDateView = new UutcView(startDate);
		endDateView = new UutcView(endDate);

		experimentLabelView = new TextBoxWithError(experimentLabel,
				experimentLabelErrorMessage);
		experimentDirView = new DirectoryView(experimentDirs.studyDirectory,
				experimentDirs.studyDirectoryErrorMessage);
		mefDirView = new DirectoryView(experimentDirs.mefDirectory,
				experimentDirs.mefDirectoryErrorMessage);
		reportFileView = new DirectoryView(experimentDirs.reportFile,
				experimentDirs.reportFileErrorMessage);
		imagesFileView = new DirectoryView(experimentDirs.imagesFile,
				experimentDirs.imagesFileErrorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasSaveButton#getSaveButton()
	 */
	@Override
	public HasClickHandlers getSaveButton() {
		return saveButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasCancelButton#getCancelButton()
	 */
	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getEndDateView()
	 */
	@Override
	public IHasValueAndErrorMessage<Long> getEndDateView() {
		return endDateView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getStartDateView()
	 */
	@Override
	public IHasValueAndErrorMessage<Long> getStartDateView() {
		return startDateView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getMefDirView()
	 */
	@Override
	public IHasValueAndErrorMessage<String> getMefDirView() {
		return mefDirView;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView#getStudyDirView()
	 */
	@Override
	public IHasValueAndErrorMessage<String> getExperimentDirView() {
		return experimentDirView;
	}

	@Override
	public IHasValueAndErrorMessage<String> getExperimentLabelView() {
		return experimentLabelView;
	}

	@Override
	public HasValue<Boolean> getPublishedView() {
		return publishedCheckBox;
	}

	@Override
	public IHasValueAndErrorMessage<String> getReportFileView() {
		return reportFileView;
	}

	@Override
	public IHasValueAndErrorMessage<String> getImagesFileView() {
		return imagesFileView;
	}

	@Override
	public void show() {
		dialogBox.center();
		dialogBox.show();
	}

	@Override
	public void hide() {
		dialogBox.hide();
	}
}
