/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * An input field for doubles and an associated error message.
 * 
 * @author John Frommeyer
 * 
 */
public class IntegerBoxWithError implements IHasValueAndErrorMessage<Integer> {

	private final IntegerBox integerBox;
	private final Label errorMessage;

	/**
	 * Creates a view from the given {@code IntegerBox} and {@code Label}.
	 * 
	 * @param integerBox
	 * @param errorMessage
	 */
	protected IntegerBoxWithError(final IntegerBox integerBox,
			final Label errorMessage) {
		this.integerBox = integerBox;
		this.errorMessage = errorMessage;
	}

	/**
	 * {@inheritDoc} If the {@code String} value of the wrapped text box
	 * represents an integer, then returns that value. Otherwise returns null.
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#getValue()
	 */
	@Override
	public Integer getValue() {
		final Integer value = integerBox.getValue();
		return value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Integer value) {
		integerBox.setValue(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasKeyPressHandlersView#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return integerBox.addKeyPressHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasErrorMessageView#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessageString) {
		ViewUtil.setAndShowErrorMessage(errorMessage, errorMessageString);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == integerBox;
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return integerBox.addKeyDownHandler(handler);
	}
}
