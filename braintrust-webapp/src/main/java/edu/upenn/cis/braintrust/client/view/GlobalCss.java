/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.gwt.resources.client.CssResource;

/**
 * Interface to {@code edu.upenn.cis.braintrust.client.view.global.css}.
 * 
 * @author John Frommeyer
 * 
 */
public interface GlobalCss extends CssResource {

	String errorMessage();

	String flexTableButtonColumn();

	String important();

	String emphasize();

	String addPanel();

	String contactNumberColumn();

	String disabled();

	@ClassName("gwt-Button-remove")
	String gwtButtonRemove();

	String flexTableHeader();

	String widePanel();

	String fieldHeader();

	String welcomeText();

	String brainTrustHeader();
	
	String cellTable();
	
	String epSubtypeTable();
	
	String epSubtypeTableOddRow();
	
	String epSubtypeTableName();
	
	String code();
	

}
