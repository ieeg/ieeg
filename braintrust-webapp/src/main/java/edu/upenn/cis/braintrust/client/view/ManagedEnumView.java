/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ManagedEnumPresenter.IManagedEnumView;
import edu.upenn.cis.braintrust.shared.dto.IManagedEnum;

public class ManagedEnumView<T extends IManagedEnum> extends Composite
		implements IManagedEnumView<T> {
	private static ManagedEnumWidgetUiBinder uiBinder = GWT
			.create(ManagedEnumWidgetUiBinder.class);

	interface ManagedEnumWidgetUiBinder extends
			UiBinder<Widget, ManagedEnumView<?>> {}

	/**
	 * The strain CellTable.
	 */
	@UiField(provided = true)
	CellTable<T> valueTable;

	@UiField
	Label modifyValueErrorMessage;

	/**
	 * The pager used to change the range of data.
	 */
	@UiField(provided = true)
	SimplePager pager;

	@UiField
	TextBox addValueBox;
	@UiField
	Button addValueButton;
	@UiField
	Label addValueErrorMessage;

	private final IHasValueAndErrorMessage<String> addValueView;
	private final ListDataProvider<T> valueProvider = new ListDataProvider<T>();
	private final EditTextCell valueCell = new EditTextCell();
	private final Column<T, String> valueColumn = new Column<T, String>(
			valueCell) {

		@Override
		public String getValue(T object) {
			return object.getLabel();
		}
	};

	private final Column<T, String> deleteValueColumn = new Column<T, String>(
			new ButtonCell()) {

		@Override
		public String getValue(T object) {
			return "x";
		}
	};
	private final IHasErrorMessage modifyValueLabelError;

	public ManagedEnumView(final CellTable<T> valueTable) {
		// Instantiate provided elements before calling uiBinder.createAndBindUi
		this.valueTable = checkNotNull(valueTable);
		this.valueTable.setWidth("20%", false);
		this.valueTable
				.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		this.valueTable
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

		this.valueTable.addColumn(valueColumn);
		this.valueTable.addColumn(deleteValueColumn);
		valueProvider.addDataDisplay(valueTable);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0,
				true);
		pager.setDisplay(this.valueTable);

		initWidget(uiBinder.createAndBindUi(this));
		addValueBox.setText("");
		addValueView = new TextBoxWithError(addValueBox,
				addValueErrorMessage);
		modifyValueLabelError = new ErrorMessageLabel(
				modifyValueErrorMessage);
	}

	@Override
	public List<T> getValueList() {
		return valueProvider.getList();
	}

	@Override
	public IHasValueAndErrorMessage<String> getAddValueView() {
		return addValueView;
	}

	@Override
	public HasClickHandlers getAddValueButton() {
		return addValueButton;
	}

	@Override
	public void setValueUpdater(FieldUpdater<T, String> valueUpdater) {
		valueColumn.setFieldUpdater(valueUpdater);
	}

	@Override
	public void setValueDeleter(FieldUpdater<T, String> valueDeleter) {
		deleteValueColumn.setFieldUpdater(valueDeleter);
	}

	@Override
	public IHasErrorMessage getModifyValueErrorMessage() {
		return modifyValueLabelError;
	}

	@Override
	public void refresh() {
		valueProvider.refresh();
	}

	@Override
	public void redraw() {
		valueTable.redraw();
	}

	@Override
	public void clearViewData(T object) {
		valueCell.clearViewData(valueTable.getKeyProvider().getKey(object));
	}

	@Override
	public void setAddValueButtonText(String text) {
		addValueButton.setText(text);
	}
}
