/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.base.Splitter;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;

/**
 * Handles the logic of a Study view.
 * 
 * @author John Frommeyer
 * 
 */
public class TracesPresenter implements IPresenter, ClickHandler {

	private final class ContactTypeUpdater implements
			FieldUpdater<GwtTrace, String> {
		@Override
		public void update(
				final int index,
				GwtTrace object,
				String value) {
			final ITraceTable traceTable = display.getTraceTable();
			traceTable.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final ContactType newContactType = ContactType.FROM_STRING
					.get(value);
			if (newContactType == null) {
				Window.alert("Could not find contact type " + value);
			} else if (newContactType.equals(object.getContactType())) {
				return;
			} else {
				final GwtTrace modifiedTrace = new GwtTrace(
						newContactType,
						object.getLabel(),
						object.getFileKey(),
						object.getContactId(),
						object.getContactVersion(),
						object.getId(),
						object.getVersion());
				service.modifyTrace(parent, modifiedTrace,
						new AsyncCallback<GwtTrace>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Could not modify contact type");
							}

							@Override
							public void onSuccess(GwtTrace result) {
								traceTable.getEntryList().set(
										index,
										result);
								Collections.sort(
										traceTable.getEntryList(),
										GwtTrace.LABEL_COMPARATOR);
								traceTable.refreshProvider();

							}
						});
			}
		}
	}

	public final class LabelUpdater implements FieldUpdater<GwtTrace, String> {
		@Override
		public void update(final int index, GwtTrace object,
				String value) {
			final ITraceTable traceTable = display.getTraceTable();
			traceTable.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String newLabel = value.trim();
			if ("".equals(newLabel)) {
				traceTable
						.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"You must enter a label");
				traceTable.clearLabelView(object);
				traceTable.redrawTable();
			} else {
				final GwtTrace modifiedTrace = new GwtTrace(
						object.getContactType(),
						newLabel,
						object.getFileKey(),
						object.getContactId(),
						object.getContactVersion(),
						object.getId(),
						object.getVersion());
				service.modifyTrace(parent, modifiedTrace,
						new AsyncCallback<GwtTrace>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Could not modify trace label");
							}

							@Override
							public void onSuccess(GwtTrace result) {
								traceTable.getEntryList().set(
										index,
										result);
								Collections.sort(
										traceTable.getEntryList(),
										GwtTrace.LABEL_COMPARATOR);
								traceTable.refreshProvider();

							}
						});
			}
		}
	}

	public final class FileKeyUpdater implements FieldUpdater<GwtTrace, String> {
		@Override
		public void update(final int index, GwtTrace object,
				String value) {
			final ITraceTable traceTable = display.getTraceTable();
			traceTable.getModifyEntryErrorMessage()
					.hideErrorMessage();
			final String newFileKey = value.trim();
			if ("".equals(newFileKey)) {
				traceTable
						.getModifyEntryErrorMessage()
						.setAndShowErrorMessage(
								"You must enter a file key");
				traceTable.clearLabelView(object);
				traceTable.redrawTable();
			} else {
				final GwtTrace modifiedTrace = new GwtTrace(
						object.getContactType(),
						object.getLabel(),
						newFileKey,
						object.getContactId(),
						object.getContactVersion(),
						object.getId(),
						object.getVersion());
				service.modifyTrace(parent, modifiedTrace,
						new AsyncCallback<GwtTrace>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Could not modify trace label");
							}

							@Override
							public void onSuccess(GwtTrace result) {
								traceTable.getEntryList().set(
										index,
										result);
								Collections.sort(
										traceTable.getEntryList(),
										GwtTrace.LABEL_COMPARATOR);
								traceTable.refreshProvider();

							}
						});
			}
		}
	}

	public final class TraceDeleter implements

			FieldUpdater<GwtTrace, String> {
		@Override
		public void update(int index, final GwtTrace object,
				String value) {
			service.deleteTrace(recordingParent, parent,
					object, new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Could not delete trace "
									+ object.getLabel());
						}

						@Override
						public void onSuccess(Void result) {
							display.getTraceTable()
									.getEntryList()
									.remove(object);
							display.getTraceTable()
									.refreshProvider();
						}
					});
		}
	}

	/**
	 * The kind of view this presenter can control.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public interface ITracesView extends IView, IDialogBox {

		HasClickHandlers getCreateTracesButton();

		HasValue<String> getFileKeyList();

		void hideTracesCountPanel();

		ITraceTable getTraceTable();

		HasClickHandlers getOkButton();

	}

	private final ITracesView display;
	private final BrainTrustServiceAsync service;
	private final GwtContactGroup parent;
	private final IHasGwtRecording recordingParent;
	private final boolean modifiable;
	private final Splitter lineSplitter = Splitter.on('\n')
			.trimResults()
			.omitEmptyStrings();

	public TracesPresenter(
			final ITracesView display,
			final BrainTrustServiceAsync rpcService,
			final GwtContactGroup parent,
			IHasGwtRecording recordingParent,
			boolean modifiable,
			List<GwtTrace> traces) {
		this.display = display;
		this.service = rpcService;
		this.parent = parent;
		this.recordingParent = recordingParent;
		this.modifiable = modifiable;
		bind();
		populateDisplay(traces);
	}

	private void populateDisplay(List<GwtTrace> traces) {
		if (traces.isEmpty()) {
			// Nothing to do
		} else {
			display.hideTracesCountPanel();
			display.getTraceTable().getEntryList().addAll(traces);
			display.getTraceTable().refreshProvider();
		}
	}

	private void bind() {
		display.getOkButton().addClickHandler(this);
		display.getCreateTracesButton().addClickHandler(this);
		display.getTraceTable().setContactTypeUpdater(new ContactTypeUpdater());
		display.getTraceTable().setLabelUpdater(
				new LabelUpdater());
		display.getTraceTable().setFileKeyUpdater(new FileKeyUpdater());
		if (modifiable) {
			display.getTraceTable().setTraceDeleter(
					new TraceDeleter());
		} else {
			display.getTraceTable().removeDeleteColumn();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		display.show();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		Object source = event.getSource();
		if (source == display.getOkButton()) {
			display.hide();
		} else if (source == display.getCreateTracesButton()) {
			onCreateTracesClick();
		}
	}

	private void onCreateTracesClick() {
		final String fileKeysRaw = display
				.getFileKeyList().getValue();
		if ("".equals(fileKeysRaw)) {
			Window.alert("You must select at least one file key.");
		} else {
			final Set<GwtTrace> newTraces = newHashSet();
			Iterable<String> fileKeys = lineSplitter.split(fileKeysRaw);
			for (final String fileKey : fileKeys) {
				String label = fileKey;
				if (label.toLowerCase().endsWith(".mef")) {
					label = label.substring(0, label.length() - 4);
				}
				int idx = label.lastIndexOf("/");
				if (idx != -1) {
					label = label.substring(idx + 1);
				}
				final GwtTrace trace = new GwtTrace(
						ContactType.MACRO,
						label,
						fileKey,
						null,
						null,
						null,
						null);
				newTraces.add(trace);

			}
			service.createTraces(parent, newTraces,
					new AsyncCallback<List<GwtTrace>>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Could not create traces.");
						}

						@Override
						public void onSuccess(List<GwtTrace> result) {
							display.hideTracesCountPanel();
							display.getTraceTable().getEntryList()
									.addAll(result);
							display.getTraceTable().refreshProvider();
						}
					});
		}
	}
}
