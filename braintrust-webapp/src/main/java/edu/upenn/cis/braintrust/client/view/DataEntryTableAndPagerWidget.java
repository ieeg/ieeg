/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.braintrust.client.presenter.IDataEntryTable;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;

/**
 * A table for data entry
 * 
 * @author John Frommeyer
 * 
 */
public class DataEntryTableAndPagerWidget<T> extends Composite implements
		IDataEntryTable<T> {
	private static DataEntryTableAndPagerWidgetUiBinder uiBinder = GWT
			.create(DataEntryTableAndPagerWidgetUiBinder.class);

	interface DataEntryTableAndPagerWidgetUiBinder extends
			UiBinder<Widget, DataEntryTableAndPagerWidget<?>> {}

	@UiField
	Label tableLabel;
	@UiField(provided = true)
	CellTable<T> entryTable;
	@UiField(provided = true)
	SimplePager entryPager;
	@UiField
	Label modifyEntryErrorMessageLabel;

	private final IHasErrorMessage modifyEntryErrorMessage;
	private static final GlobalCss css;

	private final ListDataProvider<T> entryProvider = new ListDataProvider<T>();

	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the view.
	 * 
	 */
	public DataEntryTableAndPagerWidget(String tableLabel,
			CellTable<T> entryTable) {

		this.entryTable = entryTable;
		this.entryProvider.addDataDisplay(this.entryTable);

		// Create a Pager to control the table.
		SimplePager.Resources pagerResources = GWT
				.create(SimplePager.Resources.class);
		entryPager = new SimplePager(TextLocation.CENTER, pagerResources,
				false, 0,
				true);
		entryPager.setDisplay(entryTable);

		initWidget(uiBinder.createAndBindUi(this));
		this.tableLabel.setText(tableLabel);
		modifyEntryErrorMessage = new ErrorMessageLabel(
				modifyEntryErrorMessageLabel);
	}

	@Override
	public List<T> getEntryList() {
		return entryProvider.getList();
	}

	@Override
	public void redrawTable() {
		entryTable.redraw();
	}

	@Override
	public void refreshProvider() {
		entryProvider.refresh();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return modifyEntryErrorMessage;
	}

}
