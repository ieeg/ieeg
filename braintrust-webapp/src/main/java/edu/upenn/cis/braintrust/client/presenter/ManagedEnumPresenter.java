/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Optional;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.shared.dto.IManagedEnum;

public class ManagedEnumPresenter<T extends IManagedEnum> implements
		IPresenter, ClickHandler, KeyDownHandler {

	public static interface IManagedEnumView<T> extends IView {
		List<T> getValueList();

		IHasValueAndErrorMessage<String> getAddValueView();

		HasClickHandlers getAddValueButton();

		void setAddValueButtonText(String text);

		void setValueUpdater(FieldUpdater<T, String> valueUpdater);

		void setValueDeleter(FieldUpdater<T, String> valueDeleter);

		IHasErrorMessage getModifyValueErrorMessage();

		void refresh();

		void redraw();

		/**
		 * Clears the view corresponding to {@code object}. A subsequent
		 * {@code redraw()} will restore the view from the model.
		 * 
		 * @param object
		 */
		void clearViewData(T object);
	}

	private final IManagedEnumView<T> display;
	private final IManagedEnumFactory<T> factory;
	private final IManagedEnumService<T> service;

	public ManagedEnumPresenter(final IManagedEnumView<T> display,
			IManagedEnumFactory<T> factory,
			final IManagedEnumService<T> service,
			String addValueButtonText) {
		this.display = display;
		this.factory = factory;
		this.service = service;
		bind(addValueButtonText);
	}

	private void bind(String addValueButtonText) {
		display.getAddValueButton().addClickHandler(this);
		display.getAddValueView().addKeyDownHandler(this);
		display.setAddValueButtonText(addValueButtonText);
		display.setValueUpdater(new ManagedEnumUpdater<T>(
				display, service, factory));
		display.setValueDeleter(new ManagedEnumDeleter<T>(display, service));
	}

	public void populateDisplay(List<T> initialValues) {
		display.getValueList().addAll(initialValues);
	}

	@Override
	public void go(HasWidgets container) {
		// Nothing
	}

	@Override
	public void onClick(ClickEvent event) {
		final Object source = event.getSource();
		if (source == display.getAddValueButton()) {
			onAddValueClick();
		}
	}

	private void onAddValueClick() {
		final String label = display.getAddValueView().getValue().trim();
		if ("".equals(label)) {
			display.getAddValueView().setAndShowErrorMessage(
					"Value cannot be empty");
		} else {
			final Optional<T> valueOpt = tryFind(
					display.getValueList(),
					compose(equalTo(label), T.getLabel));
			if (valueOpt.isPresent()) {
				display.getAddValueView().setAndShowErrorMessage(
						"The value " + label
								+ " already exists.");
			} else {
				final T newStrain = factory.newInstance(label, null, null);
				service.createValue(newStrain,
						new AsyncCallback<T>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("An error occurred while attempting to create "
										+ label);
							}

							@Override
							public void onSuccess(T result) {
								display.getAddValueView().setValue("");
								display.getValueList().add(result);
								Collections.sort(display.getValueList(),
										T.LABEL_COMPARATOR);
								display.refresh();
							}
						});
			}
		}
	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		Object source = event.getSource();
		if (display.getAddValueView().isWrapping(source)) {
			display.getAddValueView().hideErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onAddValueClick();
			}
		}
	}

}
