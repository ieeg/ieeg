/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;

public class KeyProviderMap {
	public Map<Class, ProvidesKey> keyProviderMap =
			new HashMap<Class, ProvidesKey>();
	
	static KeyProviderMap theMap = null;
	
	public static synchronized KeyProviderMap getMap() {
		if (theMap == null)
			theMap = new KeyProviderMap();
		return theMap;
	}
	
	KeyProviderMap() {
		keyProviderMap.put(GwtDrugAdmin.class, 
				new ProvidesKey<GwtDrugAdmin>() {

			@Override
			public Object getKey(GwtDrugAdmin item) {
				return item.getId();
			}
		});
		
		keyProviderMap.put(GwtAnimal.class, new ProvidesKey<GwtAnimal>() {

			@Override
			public Object getKey(GwtAnimal item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtContactGroup.class, new ProvidesKey<GwtContactGroup>() {

			@Override
			public Object getKey(GwtContactGroup item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtDrug.class, new ProvidesKey<GwtDrug>() {
	
			@Override
			public Object getKey(GwtDrug item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtExperiment.class, new ProvidesKey<GwtExperiment>() {

			@Override
			public Object getKey(GwtExperiment item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtOrganization.class, new ProvidesKey<GwtOrganization>() {

			@Override
			public Object getKey(GwtOrganization item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtRecording.class, new ProvidesKey<GwtRecording>() {

			@Override
			public Object getKey(GwtRecording item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtStimRegion.class, new ProvidesKey<GwtStimRegion>() {

			@Override
			public Object getKey(GwtStimRegion item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtStimType.class, new ProvidesKey<GwtStimType>() {

			@Override
			public Object getKey(GwtStimType item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtStrain.class, new ProvidesKey<GwtStrain>() {

			@Override
			public Object getKey(GwtStrain item) {
				return item == null ? null : item.getId();
			}
		});
		
		keyProviderMap.put(GwtTrace.class, new ProvidesKey<GwtTrace>() {

			@Override
			public Object getKey(GwtTrace item) {
				return item == null ? null : item.getId();
			}
		});
	}
	
	public ProvidesKey getProvider(Class c) {
		return keyProviderMap.get(c);
	}
}
