/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.List;

import com.google.gwt.user.client.Window;

import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;

public class ExperimentFactoryFromStimRegion implements
		IExperimentFactory<String> {

	private final List<GwtStimRegion> stimRegions;

	public ExperimentFactoryFromStimRegion(List<GwtStimRegion> stimRegions) {
		this.stimRegions = stimRegions;
	}

	@Override
	public GwtExperiment newInstance(GwtExperiment originalExperiment,
			String newValue) {
		final String newStimRegionLabel = newValue.trim();
		GwtStimRegion newStimRegion;
		if ("".equals(newStimRegionLabel)) {
			newStimRegion = null;
		} else {
			newStimRegion = tryFind(
					stimRegions,
					compose(equalTo(newStimRegionLabel), GwtStimRegion.getLabel))
					.orNull();
			if (newStimRegion == null) {
				Window.alert("Stimulation region " + newStimRegionLabel
						+ " could not be found. Setting to null.");
			}
		}
		return new GwtExperiment(
				originalExperiment.getLabel(),
				originalExperiment.getRecording(),
				originalExperiment.isPublished(),
				originalExperiment.getAge(),
				originalExperiment.getStimType(),
				newStimRegion,
				originalExperiment.getStimIsiMs(),
				originalExperiment.getStimDurationMs(),
				originalExperiment.getId(),
				originalExperiment.getVersion());
	}

}
