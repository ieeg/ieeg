/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;

import java.util.List;

import com.google.common.base.Functions;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.event.ExperimentFromFileKeysEvent;
import edu.upenn.cis.braintrust.client.event.ExperimentFromFileKeysEventHandler;
import edu.upenn.cis.braintrust.client.event.CreateContactGroupEvent;
import edu.upenn.cis.braintrust.client.event.CreateContactGroupEventHandler;
import edu.upenn.cis.braintrust.client.event.CreateExperimentEvent;
import edu.upenn.cis.braintrust.client.event.CreateExperimentEventHandler;
import edu.upenn.cis.braintrust.client.event.CreateHospitalAdmissionEvent;
import edu.upenn.cis.braintrust.client.event.CreateHospitalAdmissionEventHandler;
import edu.upenn.cis.braintrust.client.event.CreatePatientCanceledEvent;
import edu.upenn.cis.braintrust.client.event.CreatePatientCanceledEventHandler;
import edu.upenn.cis.braintrust.client.event.CreatePatientEvent;
import edu.upenn.cis.braintrust.client.event.CreatePatientEventHandler;
import edu.upenn.cis.braintrust.client.event.CreateStudyEvent;
import edu.upenn.cis.braintrust.client.event.CreateStudyEventHandler;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent;
import edu.upenn.cis.braintrust.client.event.DataEntryMenuEventHandler;
import edu.upenn.cis.braintrust.client.event.EditAnimalEvent;
import edu.upenn.cis.braintrust.client.event.EditAnimalEventHandler;
import edu.upenn.cis.braintrust.client.event.EditContactGroupEvent;
import edu.upenn.cis.braintrust.client.event.EditContactGroupEventHandler;
import edu.upenn.cis.braintrust.client.event.EditHospitalAdmissionEvent;
import edu.upenn.cis.braintrust.client.event.EditHospitalAdmissionEventHandler;
import edu.upenn.cis.braintrust.client.event.EditPatientEvent;
import edu.upenn.cis.braintrust.client.event.EditPatientEventHandler;
import edu.upenn.cis.braintrust.client.event.EditSpeciesEvent;
import edu.upenn.cis.braintrust.client.event.EditSpeciesEventHandler;
import edu.upenn.cis.braintrust.client.event.EditStudyEvent;
import edu.upenn.cis.braintrust.client.event.EditStudyEventHandler;
import edu.upenn.cis.braintrust.client.event.StudyFromFileKeysEvent;
import edu.upenn.cis.braintrust.client.event.StudyFromFileKeysEventHandler;
import edu.upenn.cis.braintrust.client.presenter.AddExperimentPresenter;
import edu.upenn.cis.braintrust.client.presenter.AnimalPresenter;
import edu.upenn.cis.braintrust.client.presenter.BrainTrustPresenter;
import edu.upenn.cis.braintrust.client.presenter.BrainTrustPresenter.IBrainTrustView;
import edu.upenn.cis.braintrust.client.presenter.ContactGroupPresenter;
import edu.upenn.cis.braintrust.client.presenter.ExperimentFromFileKeysPresenter;
import edu.upenn.cis.braintrust.client.presenter.DataEntryMenuPresenter;
import edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter;
import edu.upenn.cis.braintrust.client.presenter.IPresenter;
import edu.upenn.cis.braintrust.client.presenter.ListChoices;
import edu.upenn.cis.braintrust.client.presenter.PatientPresenter;
import edu.upenn.cis.braintrust.client.presenter.SpeciesPresenter;
import edu.upenn.cis.braintrust.client.presenter.StudyFromFileKeysPresenter;
import edu.upenn.cis.braintrust.client.presenter.StudyPresenter;
import edu.upenn.cis.braintrust.client.view.AddExperimentView;
import edu.upenn.cis.braintrust.client.view.AnimalView;
import edu.upenn.cis.braintrust.client.view.BrainTrustView;
import edu.upenn.cis.braintrust.client.view.ExperimentFromFileKeysView;
import edu.upenn.cis.braintrust.client.view.DataEntryMenuView;
import edu.upenn.cis.braintrust.client.view.ElectrodeView;
import edu.upenn.cis.braintrust.client.view.HospitalAdmissionView;
import edu.upenn.cis.braintrust.client.view.PatientView;
import edu.upenn.cis.braintrust.client.view.SpeciesView;
import edu.upenn.cis.braintrust.client.view.StudyFromFileKeysView;
import edu.upenn.cis.braintrust.client.view.StudyView;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;

/**
 * This is the main {@code IPresenter} for the application. It handles the
 * application level state changes such as history.
 * 
 * @author John Frommeyer
 * 
 */
public class AppController implements
		IPresenter,
		ValueChangeHandler<String>,
		CreatePatientEventHandler,
		CreatePatientCanceledEventHandler,
		EditPatientEventHandler,
		ExperimentFromFileKeysEventHandler,
		EditAnimalEventHandler,
		CreateStudyEventHandler,
		EditStudyEventHandler,
		CreateContactGroupEventHandler,
		EditContactGroupEventHandler,
		DataEntryMenuEventHandler,
		CreateHospitalAdmissionEventHandler,
		EditHospitalAdmissionEventHandler,
		EditSpeciesEventHandler,
		CreateExperimentEventHandler,
		StudyFromFileKeysEventHandler {

	private static final String CREATE_PATIENT_TOKEN = "createPatient";
	private static final String EDIT_PATIENT_TOKEN = "editPatient";
	private final static String STUDY_FROM_FILE_KEYS_TOKEN = "createStudyFromFileKeys";
	private final static String EXPERIMENT_FROM_FILE_KEYS_TOKEN = "createExperimentFromFileKeys";
	private static final String EDIT_ANIMAL_TOKEN = "editAnimal";
	private static final String DATA_ENTRY_MENU_TOKEN = "dataEntryMenu";
	private static final String CREATE_STUDY_TOKEN = "createStudy";
	private static final String EDIT_STUDY_TOKEN = "editStudy";
	private static final String CREATE_CONTACT_GROUP_TOKEN = "createContactGroup";
	private static final String EDIT_CONTACT_GROUP_TOKEN = "editContactGroup";
	private static final String CREATE_ADMISSION_TOKEN = "createHospitalAdmission";
	private static final String EDIT_ADMISSION_TOKEN = "editHospitalAdmission";
	private static final String EDIT_SPECIES_TOKEN = "editSpecies";

	private final HandlerManager eventBus;
	private final BrainTrustServiceAsync rpcService;
	private HasWidgets mainContainer;

	/**
	 * Create the AppPresenter
	 * 
	 * @param eventBus
	 * @param rpcService
	 */
	AppController(final HandlerManager eventBus,
			final BrainTrustServiceAsync rpcService) {
		this.eventBus = eventBus;
		this.rpcService = rpcService;
		bind();
	}

	private void bind() {
		History.addValueChangeHandler(this);

		eventBus.addHandler(CreatePatientEvent.TYPE, this);
		eventBus.addHandler(CreatePatientCanceledEvent.TYPE, this);
		eventBus.addHandler(EditPatientEvent.TYPE, this);
		eventBus.addHandler(ExperimentFromFileKeysEvent.TYPE, this);
		eventBus.addHandler(EditAnimalEvent.TYPE, this);
		eventBus.addHandler(CreateStudyEvent.TYPE, this);
		eventBus.addHandler(EditStudyEvent.TYPE, this);
		eventBus.addHandler(CreateContactGroupEvent.TYPE, this);
		eventBus.addHandler(EditContactGroupEvent.TYPE, this);
		eventBus.addHandler(DataEntryMenuEvent.TYPE, this);
		eventBus.addHandler(CreateHospitalAdmissionEvent.TYPE, this);
		eventBus.addHandler(EditHospitalAdmissionEvent.TYPE, this);
		eventBus.addHandler(EditSpeciesEvent.TYPE, this);
		eventBus.addHandler(CreateExperimentEvent.TYPE, this);
		eventBus.addHandler(StudyFromFileKeysEvent.TYPE, this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IPresenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(final HasWidgets container) {
		checkNotNull(container);

		final IBrainTrustView btv = new BrainTrustView();
		final BrainTrustPresenter brainTrustPresenter = new BrainTrustPresenter(
				eventBus, btv);

		brainTrustPresenter.go(container);

		this.mainContainer = btv.getMainPanel();

		if ("".equals(History.getToken())) {
			History.newItem(DATA_ENTRY_MENU_TOKEN);
		} else {
			History.fireCurrentHistoryState();
		}
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			IPresenter presenter = null;

			if (token.equals(DATA_ENTRY_MENU_TOKEN)) {
				presenter = new DataEntryMenuPresenter(
						eventBus,
						new DataEntryMenuView(),
						rpcService);
			} else if (token.equals(EXPERIMENT_FROM_FILE_KEYS_TOKEN)) {
				presenter = new ExperimentFromFileKeysPresenter(
						eventBus,
						new ExperimentFromFileKeysView(),
						rpcService);
			} else if (token.equals(STUDY_FROM_FILE_KEYS_TOKEN)) {
				presenter = new StudyFromFileKeysPresenter(
						eventBus,
						new StudyFromFileKeysView(),
						rpcService);
			}

			if (presenter != null) {
				presenter.go(mainContainer);
			}
		}
	}

	@Override
	public void onCreatePatient(CreatePatientEvent event) {
		History.newItem(CREATE_PATIENT_TOKEN, false);
		IPresenter presenter = new PatientPresenter(
				eventBus,
				new PatientView(),
				rpcService,
				event.getOrganizations());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.CreatePatientCanceledEventHandler#onCreatePatientCancelled(edu.upenn.cis.braintrust.client.event.CreatePatientCanceledEvent)
	 */
	@Override
	public void onCreatePatientCancelled(CreatePatientCanceledEvent event) {
		History.newItem(DATA_ENTRY_MENU_TOKEN);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.EditPatientEventHandler#onEditPatient(edu.upenn.cis.braintrust.client.event.EditPatientEvent)
	 */
	@Override
	public void onEditPatient(EditPatientEvent event) {
		History.newItem(EDIT_PATIENT_TOKEN, false);
		IPresenter presenter = new PatientPresenter(
				eventBus,
				new PatientView(),
				rpcService,
				event.getOrganizations(),
				event.getPatient());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.CreateStudyEventHandler#onCreateStudy(edu.upenn.cis.braintrust.client.event.CreateStudyEvent)
	 */
	@Override
	public void onCreateStudy(CreateStudyEvent event) {
		History.newItem(CREATE_STUDY_TOKEN, false);
		IPresenter presenter = new StudyPresenter(eventBus, new StudyView(),
				rpcService, event.getHospitalAdmission());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.EditStudyEventHandler#onEditStudy(edu.upenn.cis.braintrust.client.event.EditStudyEvent)
	 */
	@Override
	public void onEditStudy(EditStudyEvent event) {
		History.newItem(EDIT_STUDY_TOKEN, false);
		IPresenter presenter = new StudyPresenter(eventBus, new StudyView(),
				rpcService, event.getStudy());

		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.CreateContactGroupEventHandler#onCreateContactGroup(edu.upenn.cis.braintrust.client.event.CreateContactGroupEvent)
	 */
	@Override
	public void onCreateContactGroup(CreateContactGroupEvent event) {
		History.newItem(CREATE_CONTACT_GROUP_TOKEN, false);
		IPresenter presenter = new ContactGroupPresenter(eventBus,
				new ElectrodeView(), rpcService, event.getStudy());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.EditContactGroupEventHandler#onEditContactGroup(edu.upenn.cis.braintrust.client.event.EditContactGroupEvent)
	 */
	@Override
	public void onEditContactGroup(EditContactGroupEvent event) {
		History.newItem(EDIT_CONTACT_GROUP_TOKEN, false);
		IPresenter presenter = new ContactGroupPresenter(eventBus,
				new ElectrodeView(), rpcService, event.getStudy(),
				event.getContactGroup());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.DataEntryMenuEventHandler#onDataEntryMenu(edu.upenn.cis.braintrust.client.event.DataEntryMenuEvent)
	 */
	@Override
	public void onDataEntryMenu(DataEntryMenuEvent event) {
		History.newItem(DATA_ENTRY_MENU_TOKEN);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.CreateHospitalAdmissionEventHandler#onCreateHospitalAdmission(edu.upenn.cis.braintrust.client.event.CreateHospitalAdmissionEvent)
	 */
	@Override
	public void onCreateHospitalAdmission(CreateHospitalAdmissionEvent event) {
		History.newItem(CREATE_ADMISSION_TOKEN, false);
		IPresenter presenter = new HospitalAdmissionPresenter(eventBus,
				new HospitalAdmissionView(), rpcService, event.getPatient());
		presenter.go(mainContainer);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.event.EditHospitalAdmissionEventHandler#onEditHospitalAdmission(edu.upenn.cis.braintrust.client.event.EditHospitalAdmissionEvent)
	 */
	@Override
	public void onEditHospitalAdmission(EditHospitalAdmissionEvent event) {
		History.newItem(EDIT_ADMISSION_TOKEN, false);
		IPresenter presenter = new HospitalAdmissionPresenter(eventBus,
				new HospitalAdmissionView(), rpcService,
				event.getHospitalAdmission());
		presenter.go(mainContainer);
	}

	@Override
	public void onEditAnimal(EditAnimalEvent event) {
		History.newItem(EDIT_ANIMAL_TOKEN, false);
		final List<String> sides = newArrayList(ListChoices.SIDE_CHOICES);
		final List<String> locations = newArrayList(ListChoices.LOCATION_CHOICES);
		final List<String> drugs = newArrayList(transform(event.getDrugList(),
				GwtDrug.getLabel));
		final List<String> stimRegionListWithEmpty = newArrayList("");
		stimRegionListWithEmpty.addAll(transform(event.getStimRegionList(),
				GwtStimRegion.getLabel));
		final List<String> stimTypeListWithEmpty = newArrayList("");
		stimTypeListWithEmpty.addAll(transform(event.getStimTypeList(),
				GwtStimType.getLabel));
		final List<String> organizations = transform(
				event.getOrganizationList(),
				Functions.toStringFunction());
		IPresenter presenter = new AnimalPresenter(
				eventBus,
				new AnimalView(
						organizations,
						sides,
						locations,
						drugs,
						stimRegionListWithEmpty,
						stimTypeListWithEmpty),
				rpcService,
				event.getOrganizationList(),
				event.getSpeciesList(),
				event.getDrugList(),
				event.getStimRegionList(),
				event.getStimTypeList(),
				event.getAnimalExperimentPair());
		presenter.go(mainContainer);
	}

	@Override
	public void onEditSpecies(EditSpeciesEvent event) {
		History.newItem(EDIT_SPECIES_TOKEN, false);
		IPresenter presenter = new SpeciesPresenter(
				new SpeciesView(),
				rpcService,
				event.getSpecies(),
				event.getStrains());
		presenter.go(mainContainer);
	}

	@Override
	public void onCreateExperiment(CreateExperimentEvent event) {
		// This view is a dialog box, so no history and no container for
		// widgets.
		IPresenter presenter = new AddExperimentPresenter(
				new AddExperimentView(), rpcService, event.getAnimal(),
				event.getCallbackView());
		presenter.go(null);

	}

	@Override
	public void onCreateExperimentFromFileKeys(ExperimentFromFileKeysEvent event) {
		History.newItem(EXPERIMENT_FROM_FILE_KEYS_TOKEN);
	}

	@Override
	public void onCreateStudyFromFileKeys(StudyFromFileKeysEvent event) {
		History.newItem(STUDY_FROM_FILE_KEYS_TOKEN);
	}

}
