/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import com.google.common.annotations.VisibleForTesting;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.Label;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage;

/**
 * A view for a field which is supposed to contain a nonnegative float.
 * 
 * @author John Frommeyer
 */
public class NonNegativeDoubleView implements
		IHasValueAndErrorMessage<Double> {

	private final DoubleBox doubleBox;
	private final Label errorLabel;

	/**
	 * Creates a field which is supposed to contain a nonnegative float.
	 * 
	 * @param doubleBox
	 * @param errorLabel
	 */
	public NonNegativeDoubleView(DoubleBox doubleBox, Label errorLabel) {
		this.doubleBox = doubleBox;
		this.errorLabel = errorLabel;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @returns the nonnegative double value represented by the text value of
	 *          the field, or null it the value does not represent a nonnegative
	 *          double.
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#getValue()
	 */
	@Override
	public Double getValue() {
		Double value = toNonNegativeDoubleOrNull(doubleBox.getValue());
		return value;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(Double value) {
		doubleBox.setValue(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return doubleBox.addKeyPressHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorLabel);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(errorLabel, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IHasValueAndErrorMessage#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == doubleBox;
	}

	@VisibleForTesting
	static Double toNonNegativeDoubleOrNull(final Double value) {
		if (value == null) {
			return null;
		}
		if (value.compareTo(Double.valueOf(0.0)) < 0) {
			return null;
		}
		return value;
	}
	
	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return doubleBox.addKeyDownHandler(handler);
	}

}
