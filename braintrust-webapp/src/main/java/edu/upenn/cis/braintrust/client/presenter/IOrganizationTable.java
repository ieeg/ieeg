/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.HasKeyDownHandlers;
import com.google.gwt.user.client.ui.HasValue;

import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;

/**
 * @author John Frommeyer
 * 
 */
public interface IOrganizationTable extends IDataEntryTable<GwtOrganization>,
		IHasAddEntryButton, HasKeyDownHandlers {
	void setOrganizationDeleter(FieldUpdater<GwtOrganization, String> deleter);

	void setNameUpdater(FieldUpdater<GwtOrganization, String> updater);

	void clearNameView(GwtOrganization org);

	void removeDeleteColumn();

	HasValue<String> getNewOrgCodeView();

	HasValue<String> getNewOrgNameView();

	void setAndShowNewOrgErrorMessage(String message);

	void hideNewOrgErrorMessage();
}
