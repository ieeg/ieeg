/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

/**
 * @author John Frommeyer
 * 
 */
public class FileKeyParserException extends Exception {

	private static final long serialVersionUID = 1L;

	private final List<String> parseErrors = newArrayList();

	public FileKeyParserException(List<String> parseErrors) {
		super("See parseErrors for a list of errors");
		this.parseErrors.addAll(parseErrors);
	}

	/**
	 * @return the validationErrors
	 */
	public List<String> getParseErrors() {
		return parseErrors;
	}
}
