/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import java.util.Collection;
import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Fired when a new arrayInfo needs to be created.
 * 
 * @author John Frommeyer
 * 
 */
public class CreateElectrodeInfoEvent extends
		GwtEvent<CreateElectrodeInfoEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<CreateElectrodeInfoEventHandler> TYPE = new Type<CreateElectrodeInfoEventHandler>();
	private final List<String> manufacturers;
	private final Collection<String> electrodeTypes;

	/**
	 * Creates an {@code CreateElectrodeInfoEvent} for the given arrayInfo.
	 * 
	 * @param theArrayInfo
	 * @param manufacturers
	 */
	public CreateElectrodeInfoEvent(List<String> manufacturers,
			final Collection<String> electrodeTypes) {
		this.manufacturers = manufacturers;
		this.electrodeTypes = electrodeTypes;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreateElectrodeInfoEventHandler handler) {
		handler.onCreateArrayInfo(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateElectrodeInfoEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Returns the list of manufacturers we know about.
	 * 
	 * @return the list of manufacturers we know about
	 */
	public List<String> getManufacturers() {
		return manufacturers;
	}

	/**
	 * DOCUMENT ME
	 * 
	 * @return
	 */
	public Collection<String> getElectrodeTypes() {
		return electrodeTypes;
	}

}
