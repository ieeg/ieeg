/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.IOrganizationTable;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;

/**
 * A table for organizations
 * 
 * @author John Frommeyer
 * 
 */
public class OrganizationTableWidget extends Composite implements
		IOrganizationTable {
	private static OrganizationTableWidgetUiBinder uiBinder = GWT
			.create(OrganizationTableWidgetUiBinder.class);

	interface OrganizationTableWidgetUiBinder extends
			UiBinder<Widget, OrganizationTableWidget> {}

	@UiField(provided = true)
	DataEntryTableAndPagerWidget<GwtOrganization> organizationTableWidget;

	@UiField
	TextBox newOrgCodeBox;

	@UiField
	TextBox newOrgNameBox;

	@UiField
	Button addEntryButton;

	@UiField
	Label newOrgErrorMessage;

	private static final GlobalCss css;

	private final Column<GwtOrganization, String> codeColumn = new Column<GwtOrganization, String>(
			new TextCell()) {

		@Override
		public String getValue(GwtOrganization object) {
			return object.getCode();
		}
	};

	private final EditTextCell nameCell = new EditTextCell();
	private final Column<GwtOrganization, String> nameColumn = new Column<GwtOrganization, String>(
			nameCell) {

		@Override
		public String getValue(GwtOrganization object) {
			return object.getName();
		}
	};

	private final Column<GwtOrganization, String> deleteColumn = new Column<GwtOrganization, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtOrganization object) {
			return "x";
		}
	};
	private CellTable<GwtOrganization> organizationTable;

	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the view.
	 * 
	 */
	public OrganizationTableWidget() {
		organizationTable = new CellTable<GwtOrganization>(
				5,
				KeyProviderMap.getMap().getProvider(GwtOrganization.class));
		organizationTable.setWidth("100%", false);

		organizationTable.addColumn(codeColumn, "Code");
		organizationTable.addColumn(nameColumn, "Name");
		organizationTable.addColumn(deleteColumn, "Delete");
		organizationTableWidget = new DataEntryTableAndPagerWidget<GwtOrganization>(
				"Organizations", organizationTable);

		initWidget(uiBinder.createAndBindUi(this));
		newOrgCodeBox.setText("");
		newOrgNameBox.setText("");
	}

	@Override
	public HasClickHandlers getAddEntryButton() {
		return addEntryButton;
	}

	@Override
	public List<GwtOrganization> getEntryList() {
		return organizationTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		organizationTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		organizationTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return organizationTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setOrganizationDeleter(
			FieldUpdater<GwtOrganization, String> deleter) {
		deleteColumn.setFieldUpdater(deleter);
	}

	@Override
	public void setNameUpdater(
			FieldUpdater<GwtOrganization, String> updater) {
		nameColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearNameView(GwtOrganization organization) {
		nameCell.clearViewData(organization.getId());
	}

	@Override
	public void removeDeleteColumn() {
		organizationTable.removeColumn(deleteColumn);
	}

	@Override
	public HasValue<String> getNewOrgCodeView() {
		return newOrgCodeBox;
	}

	@Override
	public HasValue<String> getNewOrgNameView() {
		return newOrgNameBox;
	}

	@Override
	public void setAndShowNewOrgErrorMessage(String message) {
		ViewUtil.setAndShowErrorMessage(newOrgErrorMessage, message);
	}

	@Override
	public void hideNewOrgErrorMessage() {
		ViewUtil.hideErrorMessage(newOrgErrorMessage);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		newOrgCodeBox.addKeyDownHandler(handler);
		return newOrgNameBox.addKeyDownHandler(handler);
	}
}
