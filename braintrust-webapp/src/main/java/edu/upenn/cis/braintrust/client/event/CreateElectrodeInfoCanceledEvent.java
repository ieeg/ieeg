/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Fired when the creation of a array info has been canceled.
 * 
 * @author John Frommeyer
 * 
 */
public class CreateElectrodeInfoCanceledEvent extends
		GwtEvent<CreateElectrodeInfoCanceledEventHandler> {
	/** The {@code EegStudyType} of this event. */
	public static final Type<CreateElectrodeInfoCanceledEventHandler> TYPE = new Type<CreateElectrodeInfoCanceledEventHandler>();

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(CreateElectrodeInfoCanceledEventHandler handler) {
		handler.onCreateArrayInfoCancelled(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateElectrodeInfoCanceledEventHandler> getAssociatedType() {
		return TYPE;
	}

}
