/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.dto.OptionsLists;
import edu.upenn.cis.braintrust.shared.exception.AmbiguousHospitalAdmissionException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.FileNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("braintrust")
public interface BrainTrustService extends RemoteService {

	/**
	 * Delete the patient.
	 * 
	 * @param patient the patient to be deleted
	 * @throws BrainTrustAuthorizationException
	 * @throws StaleObjectException
	 * @throws ObjectNotFoundException
	 * @throws BrainTrustUserException
	 */
	void deletePatient(GwtPatient patient)
			throws BrainTrustAuthorizationException, StaleObjectException,
			ObjectNotFoundException, BrainTrustUserException;

	/**
	 * 
	 * Returns the list of etiologies found in the database.
	 * 
	 * @return the list of etiologies found in the database
	 */
	List<String> retrieveEtiologies() throws BrainTrustAuthorizationException;

	List<GwtOrganization> retrieveOrganizations()
			throws BrainTrustAuthorizationException;

	/**
	 * Returns a list of all species.
	 * 
	 * @return a list of all species
	 */
	List<GwtSpecies> retrieveSpecies() throws BrainTrustAuthorizationException;

	List<GwtDrug> retrieveDrugs() throws BrainTrustAuthorizationException;

	List<GwtStimRegion> retrieveStimRegions()
			throws BrainTrustAuthorizationException;

	List<GwtStimType> retrieveStimTypes()
			throws BrainTrustAuthorizationException;

	/**
	 * Returns the {@code GwtPatient} with the given user set ID, or
	 * {@code null} if there is no such {@code GwtPatient}.
	 * 
	 * @param patientLabel
	 * @return the {@code GwtPatient} with the given user set ID
	 */
	public GwtPatient retrievePatientByLabel(String patientLabel)
			throws BrainTrustAuthorizationException;

	/**
	 * Returns the {@code GwtAnimal} with the given user set ID, or {@code null}
	 * if there is no such {@code GwtAnimal}.
	 * 
	 * @param animalLabel
	 * @return the {@code GwtAnimal} with the given user set ID
	 */
	public GwtAnimalExperimentPair retrieveAnimalByLabel(String animalLabel)
			throws BrainTrustAuthorizationException;

	/**
	 * Save the patient data.
	 * 
	 * @param patient to be saved
	 * @return the saved patient
	 * @throws BrainTrustAuthorizationException
	 * @throws StaleObjectException
	 * @throws ObjectNotFoundException
	 * @throws BrainTrustUserException
	 */
	GwtPatient savePatient(GwtPatient patient)
			throws BrainTrustAuthorizationException, StaleObjectException,
			ObjectNotFoundException, BrainTrustUserException;

	GwtPatient savePatientWithNewStudy(
			GwtPatient patient,
			GwtHospitalAdmission admission,
			GwtEegStudy study,
			Map<String, Set<GwtTrace>> channelPrefixToTraces)
			throws BrainTrustAuthorizationException,
			DuplicateNameException,
			AmbiguousHospitalAdmissionException,
			FileNotFoundException, 
			BrainTrustUserException;

	GwtSpecies createSpecies(GwtSpecies gwtSpecies,
			Collection<GwtStrain> gwtStrains)
			throws BrainTrustAuthorizationException;

	List<GwtStrain> findStrains(GwtSpecies species)
			throws BrainTrustAuthorizationException;

	GwtStrain createStrain(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException, ObjectNotFoundException;

	GwtSpecies modifySpeciesLabel(GwtSpecies species)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	GwtStrain modifyStrainLabel(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteStrain(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	void deleteSpecies(GwtSpecies species)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtDrug createDrug(GwtDrug drug)
			throws BrainTrustAuthorizationException, ObjectNotFoundException;

	void deleteDrug(GwtDrug drug)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtDrug modifyDrugLabel(GwtDrug drug)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	GwtStimRegion createStimRegion(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException, ObjectNotFoundException;

	void deleteStimRegion(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtStimRegion modifyStimRegionLabel(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	GwtStimType createStimType(GwtStimType stimType)
			throws BrainTrustAuthorizationException, ObjectNotFoundException;

	void deleteStimType(GwtStimType stimType)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtStimType modifyStimTypeLabel(GwtStimType stimType)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	GwtAnimal saveAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException;

	GwtAnimalExperimentPair saveAnimalWithNewExperiment(
			GwtAnimal animal,
			GwtExperiment experiment,
			Map<String, Set<GwtTrace>> channelPrefixToTraces)
			throws BrainTrustAuthorizationException,
			DuplicateNameException,
			FileNotFoundException, 
			BrainTrustUserException;

	GwtAnimal modifyAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtExperiment createExperiment(GwtAnimal parent, GwtExperiment experiment)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			BrainTrustUserException;

	GwtExperiment modifyExperiment(GwtAnimal parent, GwtExperiment experiment)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteExperiment(GwtAnimal parent, GwtExperiment experiment)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtRecording modifyRecording(GwtExperiment parent, GwtRecording recording)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	List<GwtTrace> findTraces(GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException;

	List<GwtTrace> createTraces(GwtContactGroup gwtParent,
			Set<GwtTrace> gwtTrace)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			BrainTrustUserException;

	GwtTrace modifyTrace(GwtContactGroup gwtParent, GwtTrace gwtTrace)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteTrace(IHasGwtRecording recordingParent,
			GwtContactGroup traceParent,
			GwtTrace trace)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtContactGroup createContactGroup(GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			BrainTrustUserException;

	GwtContactGroup modifyContactGroup(GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteContactGroup(IHasGwtRecording recordingParent,
			GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	List<GwtDrugAdmin> findDrugAdmins(GwtExperiment experiment)
			throws BrainTrustAuthorizationException;

	GwtDrugAdmin createDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			BrainTrustUserException;

	GwtDrugAdmin modifyDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException;

	void deleteDrugAdmin(GwtExperiment parent,
			GwtDrugAdmin drugAdmin)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException;

	GwtOrganization modifyOrganization(GwtOrganization modifiedOrg)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException;

	void deleteOrganization(GwtOrganization org)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException;

	GwtOrganization createOrganization(GwtOrganization newOrg)
			throws BrainTrustAuthorizationException;

	OptionsLists retrieveOptionsLists() throws BrainTrustAuthorizationException;

	List<String> retrieveMefFileKeys(String mefPath)
			throws BrainTrustAuthorizationException,
			BrainTrustUserException;
}
