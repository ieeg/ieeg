/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.KeyProviderMap;
import edu.upenn.cis.braintrust.client.presenter.IHasErrorMessage;
import edu.upenn.cis.braintrust.client.presenter.ITraceTable;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;

/**
 * A table for traces
 * 
 * @author John Frommeyer
 * 
 */
public class TraceTableWidget extends Composite implements
		ITraceTable {
	private static TraceTableViewUiBinder uiBinder = GWT
			.create(TraceTableViewUiBinder.class);

	interface TraceTableViewUiBinder extends
			UiBinder<Widget, TraceTableWidget> {}

	@UiField(provided = true)
	DataEntryTableAndPagerWidget<GwtTrace> traceTableWidget;

	private final Column<GwtTrace, String> contactTypeColumn;

	private final EditTextCell labelCell = new EditTextCell();
	private final Column<GwtTrace, String> labelColumn = new Column<GwtTrace, String>(
			labelCell) {

		@Override
		public String getValue(GwtTrace object) {
			return object.getLabel();
		}
	};

	private final EditTextCell fileKeyCell = new EditTextCell();
	private final Column<GwtTrace, String> fileKeyColumn = new Column<GwtTrace, String>(
			fileKeyCell) {

		@Override
		public String getValue(GwtTrace object) {
			return object.getFileKey();
		}
	};

	private final Column<GwtTrace, String> deleteColumn = new Column<GwtTrace, String>(
			new ButtonCell()) {

		@Override
		public String getValue(GwtTrace object) {
			return "x";
		}
	};

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Create the patient view.
	 * 
	 */
	public TraceTableWidget(List<String> contactTypes) {
		// Animal
		final CellTable<GwtTrace> traceTable = new CellTable<GwtTrace>(15,
				KeyProviderMap.getMap().getProvider(GwtTrace.class));
		traceTable.setWidth("100%", false);
		contactTypeColumn = new Column<GwtTrace, String>(
				new SelectionCell(contactTypes)) {

			@Override
			public String getValue(GwtTrace object) {
				return object.getContactType().toString();
			}
		};
		traceTable.addColumn(contactTypeColumn, "Contact Type");
		traceTable.addColumn(labelColumn, "Channel Label");
		traceTable.addColumn(fileKeyColumn, "Channel File Key");
		traceTable.addColumn(deleteColumn, "Delete Channel");

		traceTableWidget = new DataEntryTableAndPagerWidget<GwtTrace>(
				"Channels",
				traceTable);
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public List<GwtTrace> getEntryList() {
		return traceTableWidget.getEntryList();
	}

	@Override
	public void redrawTable() {
		traceTableWidget.redrawTable();
	}

	@Override
	public void refreshProvider() {
		traceTableWidget.refreshProvider();
	}

	@Override
	public IHasErrorMessage getModifyEntryErrorMessage() {
		return traceTableWidget.getModifyEntryErrorMessage();
	}

	@Override
	public void setTraceDeleter(FieldUpdater<GwtTrace, String> deleter) {
		deleteColumn.setFieldUpdater(deleter);
	}

	@Override
	public void setFileKeyUpdater(
			FieldUpdater<GwtTrace, String> updater) {
		fileKeyColumn.setFieldUpdater(updater);
	}

	@Override
	public void setLabelUpdater(
			FieldUpdater<GwtTrace, String> updater) {
		labelColumn.setFieldUpdater(updater);
	}

	@Override
	public void clearFileKeyView(GwtTrace trace) {
		fileKeyCell.clearViewData(trace.getId());
	}

	@Override
	public void clearLabelView(GwtTrace trace) {
		labelCell.clearViewData(trace.getId());

	}

	@Override
	public void removeDeleteColumn() {
		traceTableWidget.entryTable.removeColumn(deleteColumn);
	}

	@Override
	public void setContactTypeUpdater(FieldUpdater<GwtTrace, String> updater) {
		contactTypeColumn.setFieldUpdater(updater);
	}

}
