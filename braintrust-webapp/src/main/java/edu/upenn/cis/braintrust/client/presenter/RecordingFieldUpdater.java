/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

public class RecordingFieldUpdater<T> implements
		FieldUpdater<GwtRecording, T> {
	private final IRecordingTable display;
	private final BrainTrustServiceAsync service;
	private final IRecordingFactory<T> factory;
	private final IRecordingFieldValidator<T> validator;
	private final IHasRecordingParent<GwtExperiment> hasExperiment;

	public RecordingFieldUpdater(IRecordingTable display,
			BrainTrustServiceAsync service, IRecordingFactory<T> factory,
			IRecordingFieldValidator<T> validator,
			IHasRecordingParent<GwtExperiment> hasExperiement) {
		this.display = display;
		this.service = service;
		this.factory = factory;
		this.validator = validator;
		this.hasExperiment = hasExperiement;
	}

	@Override
	public void update(final int index, final GwtRecording object, T value) {
		display.getModifyEntryErrorMessage()
				.hideErrorMessage();
		if (false == validator.isValidFieldValue(value)) {
			display.getModifyEntryErrorMessage().setAndShowErrorMessage(
					"Invalid value: " + value);
			validator.clearFieldView(display, object);
		} else {

			final GwtRecording modifiedValue = factory.newInstance(object,
					value);
			final GwtExperiment parent = hasExperiment.getRecordingParent(object)
					.orNull();
			if (parent == null) {
				Window.alert("Could not find experiment for recording "
						+ object.getDir());
			} else {
				service.modifyRecording(parent, modifiedValue,
						new AsyncCallback<GwtRecording>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("An error occurred while attempting to modify recording.");
								validator.clearFieldView(display, object);
							}

							@Override
							public void onSuccess(GwtRecording result) {
								hasExperiment.updateRecording(result);
								display.setRecording(result);
								display.refreshProvider();
							}
						});
			}
		}

	}
}
