/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.client.presenter.IContactGroupTable;
import edu.upenn.cis.braintrust.client.presenter.IRecordingTable;
import edu.upenn.cis.braintrust.client.presenter.RecordingPresenter.IRecordingView;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

/**
 * A screen to enter information about an experiment.
 * 
 * @author John Frommeyer
 * 
 */
public class RecordingView extends Composite implements IRecordingView {

	private static RecordingUiBinder uiBinder = GWT
			.create(RecordingUiBinder.class);

	interface RecordingUiBinder extends
			UiBinder<Widget, RecordingView> {}

	@UiField
	RecordingTableWidget recordingTableWidget;

	@UiField(provided = true)
	ContactGroupTableWidget contactGroupTableWidget;

	private static final GlobalCss css;
	static {
		ICssResources.INSTANCE.globalCss().ensureInjected();
		css = ICssResources.INSTANCE.globalCss();
	}

	/**
	 * Creates this view.
	 * 
	 */
	public RecordingView(
			List<String> sides,
			List<String> locations) {
		contactGroupTableWidget = new ContactGroupTableWidget(sides, locations);
		initWidget(uiBinder.createAndBindUi(this));
		// Can access @UiField after calling createAndBindUi
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.presenter.IView#asWidget()
	 */
	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public IRecordingTable getRecordingTable() {
		return recordingTableWidget;
	}

	@Override
	public IContactGroupTable getContactGroupTable() {
		return contactGroupTableWidget;
	}

	@Override
	public GwtRecording getRecording() {
		final int count = recordingTableWidget.getEntryList().size();
		if (count != 1) {
			Window.alert("An error has occured in RecordingTableWidget. Please report. Count: "
					+ count);
		}
		return recordingTableWidget.getEntryList().get(0);

	}

	@Override
	public void setEnabled(boolean enabled) {
		contactGroupTableWidget.addEntryButton.setEnabled(enabled);
	}

}
