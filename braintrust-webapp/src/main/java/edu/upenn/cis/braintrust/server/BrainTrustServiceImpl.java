/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.server;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.SecurityUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.client.BrainTrustService;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IContactDAO;
import edu.upenn.cis.braintrust.dao.metadata.IContactGroupDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugAdminDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStrainDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.AnimalDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactGroupDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugAdminDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.OrganizationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.SpeciesDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimRegionDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimTypeDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StrainDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.RecordingDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServiceFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServiceFactory;
import edu.upenn.cis.braintrust.datasnapshot.SearchResultCacheEntry;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.AnimalAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.ContactGroupAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.DrugAdminAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.DrugAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.EegStudyAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.ExperimentAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.HospitalAdmissionAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.OrganizationAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.PatientAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.RecordingAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.SpeciesAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.StimRegionAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.StimTypeAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.StrainAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.TraceAssembler;
import edu.upenn.cis.braintrust.imodel.IHasRecording;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DrugAdminRelationship;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.dto.OptionsLists;
import edu.upenn.cis.braintrust.shared.exception.AmbiguousHospitalAdmissionException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.FileNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.validate.ConstraintViolationMessageUtil;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.TraceServer;
import edu.upenn.cis.db.mefview.server.TraceServerFactory;
import edu.upenn.cis.db.mefview.shared.IUser;

//import edu.upenn.cis.braintrust.security.Role;

/**
 * The BrainTrust servlet.
 * 
 * @author John Frommeyer
 */
public class BrainTrustServiceImpl extends RemoteServiceServlet implements
		BrainTrustService {

	private final transient SpeciesAssembler speciesAssembler = new SpeciesAssembler();
	private final transient StrainAssembler strainAssembler = new StrainAssembler();
	private final transient DrugAssembler drugAssembler = new DrugAssembler();
	private final transient StimRegionAssembler stimRegionAssembler = new StimRegionAssembler();
	private final transient StimTypeAssembler stimTypeAssembler = new StimTypeAssembler();
	private final transient TraceAssembler traceAssembler = new TraceAssembler();
	private final transient DrugAdminAssembler drugAdminAssembler = new DrugAdminAssembler();
	private final transient OrganizationAssembler organizationAssembler = new OrganizationAssembler();

	private final transient IDataSnapshotServiceFactory dsServiceFactory;
	private final transient IUserService userService;
	private final transient TraceServer traceServer;
	private final transient AmazonS3 s3;

	private final boolean usingS3;
	private final String dataBucket;
	private final String sourcePath;

	private Cache<String, DataSnapshotEntity> dataSnapshotShortCache =
			CacheBuilder
					.newBuilder()
					.expireAfterWrite(0, TimeUnit.MINUTES)
					.build();

	private Cache<Long, SearchResultCacheEntry> searchResultsCache =
			CacheBuilder
					.newBuilder()
					.expireAfterWrite(0, TimeUnit.MINUTES)
					.build();

	private static final long serialVersionUID = 1L;
	final static Logger logger = LoggerFactory
			.getLogger(BrainTrustServiceImpl.class);

	public BrainTrustServiceImpl() {
		this(
				new DataSnapshotServiceFactory(),
				UserServiceFactory.getUserService(),
				TraceServerFactory.getTraceServer(),
				AwsUtil.getS3(),
				IvProps.isUsingS3(),
				IvProps.getDataBucket(),
				BtUtil.get(IvProps.getIvProps(), IvProps.SOURCEPATH, ""));
	}

	public BrainTrustServiceImpl(
			IDataSnapshotServiceFactory dsServiceFactory,
			IUserService userService,
			TraceServer traceServer,
			AmazonS3 s3,
			boolean useS3,
			String dataBucket,
			String sourcePath) {
		this.dsServiceFactory = dsServiceFactory;
		this.userService = userService;
		this.traceServer = traceServer;
		this.s3 = s3;
		this.usingS3 = useS3;
		this.dataBucket = dataBucket;
		this.sourcePath = sourcePath;
	}

	@Override
	public void deletePatient(
			final edu.upenn.cis.braintrust.shared.dto.GwtPatient gwtPatient)
			throws BrainTrustAuthorizationException, StaleObjectException,
			ObjectNotFoundException, BrainTrustUserException {
		final String M = "deletePatient(...)";

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Long id = gwtPatient.getId();
			final Integer version = gwtPatient.getVersion();
			final Session session = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(session);
			session.beginTransaction();
			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final Patient patient = patientDAO.findById(id, false);
			if (patient == null) {
				throw new ObjectNotFoundException("Patient "
						+ gwtPatient.getLabel()
						+ " could not be found in the database.");
			}
			final Integer currentVersion = patient.getVersion();
			if (!currentVersion.equals(version)) {
				throw new StaleObjectException(
						"Could not delete patient "
								+ patient.getLabel()
								+ ". This patient has been modified by another user. Please reload.");
			}
			deletePatientOrThrowException(
					patientDAO,
					patient,
					new TsAnnotationDAOHibernate(session, HibernateUtil
							.getConfiguration()),
					dsServiceFactory.newInstance(
							session,
							dataSnapshotShortCache,
							searchResultsCache));
			session.getTransaction().commit();
			logger.info("{}, Deleted patient {}.", M, patient.getLabel());
		} catch (final StaleObjectException soe) {
			handleRollback(M, HibernateUtil.getSessionFactory()
					.getCurrentSession()
					.getTransaction());
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					soe);
			throw soe;
		} catch (final ObjectNotFoundException onfe) {
			handleRollback(M, HibernateUtil.getSessionFactory()
					.getCurrentSession()
					.getTransaction());
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					onfe);
			throw onfe;

		} catch (final ConstraintViolationException cve) {
			handleRollback(M, HibernateUtil.getSessionFactory()
					.getCurrentSession()
					.getTransaction());
			final String message = ConstraintViolationMessageUtil
					.toString(cve);
			logger.error(M + ": " + message, cve);
			throw new IllegalStateException(message);
		} catch (final BrainTrustUserException bte) {
			handleRollback(M, HibernateUtil.getSessionFactory()
					.getCurrentSession()
					.getTransaction());
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					bte);
			throw bte;
		} catch (final Throwable t) {
			handleRollback(M, HibernateUtil.getSessionFactory()
					.getCurrentSession()
					.getTransaction());
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(
					ManagedSessionContext.unbind(
							HibernateUtil.getSessionFactory()));
		}
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	@SuppressWarnings("unused")
	private String escapeHtml(final String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}

	@Override
	public List<String> retrieveEtiologies()
			throws BrainTrustAuthorizationException {
		final String M = "retrieveEtiologies(...)";

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Session session = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(session);
			session.beginTransaction();
			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final List<String> etiologies = patientDAO.retrieveEtiologies();
			session.getTransaction().commit();
			logger.info("{}: Retrieved {} etiologies.", M, etiologies.size());
			return etiologies;
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error(M
						+ ": Could not rollback transaction after exception!",
						rbEx);
			}
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw new IllegalStateException(t);
		} finally {
			PersistenceUtil.close(
					ManagedSessionContext.unbind(
							HibernateUtil.getSessionFactory()));
		}

	}

	@Override
	public edu.upenn.cis.braintrust.shared.dto.GwtPatient retrievePatientByLabel(
			final String patientLabel) throws BrainTrustAuthorizationException {
		final String M = "retrievePatientByLabel(...)";

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Session session = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(session);
			session.beginTransaction();
			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final Patient patient = patientDAO
					.findByLabelEager(patientLabel);
			GwtPatient gwtPatient = null;
			if (patient != null) {
				final PatientAssembler patientAssembler = newPatientAssembler(session);
				gwtPatient = patientAssembler.buildDto(patient);
				logger.info("{}: Retrieved patient {}.", M, patient.getLabel());

			} else {
				logger.info(
						"{}: Patient with userSuppliedId {} could not be found.",
						M,
						patientLabel);
			}
			session.getTransaction().commit();
			return gwtPatient;
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error(M
						+ ": Could not rollback transaction after exception!",
						rbEx);
			}
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(
					ManagedSessionContext.unbind(
							HibernateUtil.getSessionFactory()));
		}
	}

	private PatientAssembler newPatientAssembler(final Session session) {
		final ITsAnnotationDAO annotationDAO = new TsAnnotationDAOHibernate(
				session, HibernateUtil.getConfiguration());
		final IPermissionDAO permissionDAO = new PermissionDAOHibernate(session);
		final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
				session);
		final IDataSnapshotService dsService = dsServiceFactory
				.newInstance(session, dataSnapshotShortCache,
						searchResultsCache);
		final PatientAssembler patientAssembler = new PatientAssembler(
				permissionDAO,
				annotationDAO,
				organizationDAO,
				dsService);
		return patientAssembler;
	}

	@Override
	public edu.upenn.cis.braintrust.shared.dto.GwtPatient savePatient(
			final edu.upenn.cis.braintrust.shared.dto.GwtPatient gwtPatient)
			throws BrainTrustAuthorizationException, StaleObjectException,
			ObjectNotFoundException, BrainTrustUserException {
		final String M = "savePatient(...)";

		Session sess = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Long id = gwtPatient.getId();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			final IPatientDAO patientDAO = new PatientDAOHibernate(sess);
			final PatientAssembler patientAssembler = newPatientAssembler(sess);
			Patient patient = null;
			if (id == null) {// A new patient
				patient = patientAssembler.buildEntity(gwtPatient);
				patientDAO.saveOrUpdate(patient);
			} else {// A modified patient
				patient = patientDAO.findByIdEager(id);
				if (patient == null) {
					throw new ObjectNotFoundException("Patient "
							+ gwtPatient.getLabel()
							+ " could not be found in the database.");
				}
				patientAssembler.modifyEntity(patient, gwtPatient);
			}
			patientDAO.flush();
			final GwtPatient resultPatient = patientAssembler.buildDto(patient);
			trx.commit();

			// session = HibernateUtil.getSessionFactory()
			// .openSession();
			// session.beginTransaction();
			// patient = patientDAO.findByIdEager(id);
			// session.getTransaction().commit();

			logger.info("{}: Saved patient {}.", M, patient.getLabel());
			return resultPatient;
		} catch (final StaleObjectException soe) {
			handleRollback(M, trx);
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					soe);
			throw soe;
		} catch (final ObjectNotFoundException onfe) {
			handleRollback(M, trx);
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					onfe);
			throw onfe;

		} catch (final ConstraintViolationException cve) {
			handleRollback(M, trx);
			final String message = ConstraintViolationMessageUtil
					.toString(cve);
			logger.error(M + ": " + message, cve);
			throw new IllegalStateException(message);
		} catch (final BrainTrustUserException bte) {
			handleRollback(M, trx);
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					bte);
			throw bte;
		} catch (final Throwable t) {
			handleRollback(M, trx);
			logger.error(
					M
							+ ": Exception while handling request. Any active transaction rolled back.",
					t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	private static void handleRollback(String methodName,
			final @Nullable Transaction trx) {
		try {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
		} catch (final Throwable rbEx) {
			logger.error(methodName
					+ ": Could not rollback transaction after exception!",
					rbEx);
		}
	}

	private void authzOrThrowException(final UserId userId)
			throws BrainTrustAuthorizationException {
		final User user = userService.findUserByUid(userId);
		if (user.getStatus().equals(IUser.Status.ENABLED)
				&& user.isAdmin()) {
			return;
		} else {
			throw new BrainTrustAuthorizationException(
					"You are not authorized to perform this action.");
		}
	}

	private static void deletePatientOrThrowException(
			final IPatientDAO patientDAO,
			final Patient patient,
			ITsAnnotationDAO annotationDAO,
			IDataSnapshotService dsService)
			throws BrainTrustUserException {
		for (final HospitalAdmission admission : patient.getAdmissions()) {
			for (final EegStudy study : admission.getStudies()) {
				if (!dsService.isRecordingModifiable(study)) {
					throw new BrainTrustUserException(
							"This patient cannot be deleted. A time series in one of this patient's studies is referenced by a user created data snapshot.");
				}
			}
		}

		// Delete all study annotations before deleting the patient.
		for (final HospitalAdmission admission : patient.getAdmissions()) {
			for (final EegStudy study : admission.getStudies()) {
				annotationDAO.deleteByParent(study);
			}
		}
		patientDAO.delete(patient);
	}

	@Override
	public List<GwtSpecies> retrieveSpecies()
			throws BrainTrustAuthorizationException {
		final String M = "retrieveSpecies(...)";
		Session session = null;
		Transaction trx = null;

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final List<GwtSpecies> gwtSpecies = retrieveSpecies(session);
			trx.commit();
			logger.info(
					"{}: Returned {} species.",
					new Object[] { M, gwtSpecies.size() });
			return gwtSpecies;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtSpecies createSpecies(GwtSpecies gwtSpecies,
			Collection<GwtStrain> gwtStrains)
			throws BrainTrustAuthorizationException {
		final String M = "createSpecies(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());

			Long id = gwtSpecies.getId();
			checkArgument(id == null, "gwtSpecies.id must be null");
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			// Create a new species
			final SpeciesEntity species = speciesAssembler
					.buildEntity(gwtSpecies);
			for (final GwtStrain gwtStrain : gwtStrains) {
				strainAssembler.buildEntity(species, gwtStrain);
			}
			session.saveOrUpdate(species);
			session.flush();
			final GwtSpecies result = speciesAssembler.buildDto(species);
			trx.commit();
			logger.info("{}: Created species {} with {} strains.",
					new Object[] { M,
							result.getLabel(), species.getStrains().size() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public List<GwtStrain> findStrains(GwtSpecies gwtSpecies) {
		final String M = "findStrains(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final Long id = gwtSpecies.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity species = speciesDAO.findById(id, false);
			final IStrainDAO strainDAO = new StrainDAOHibernate(session);
			final List<StrainEntity> strains = strainDAO
					.findByParentOrderedByLabel(species);
			final List<GwtStrain> gwtStrains = newArrayList();
			for (final StrainEntity strain : strains) {
				gwtStrains
						.add(strainAssembler.buildDto(strain,
								species.getLabel(),
								species.getId()));
			}
			trx.commit();
			logger.info("{}: Returning {} strains for {}.",
					new Object[] { M,
							gwtStrains.size(), species.getLabel() });
			return gwtStrains;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtStrain createStrain(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException, ObjectNotFoundException {
		final String M = "createStrain(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Long id = checkNotNull(parent.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity parentEntity = speciesDAO.findById(id, false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Species "
						+ parent.getLabel()
						+ " could not be found in the database.");
			}
			final StrainEntity strainEntity = strainAssembler.buildEntity(
					parentEntity, strain);
			session.saveOrUpdate(parentEntity);
			session.flush();
			final GwtStrain result = strainAssembler.buildDto(strainEntity,
					parentEntity.getLabel(), parentEntity.getId());
			trx.commit();
			logger.info("{}: Created strain {} for species {}.",
					new Object[] { M,
							result.getLabel(), parentEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtSpecies modifySpeciesLabel(GwtSpecies species)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifySpeciesLabel(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = species.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity speciesEntity = speciesDAO.findById(id, false);
			if (speciesEntity == null) {
				throw new ObjectNotFoundException("Species "
						+ species.getLabel()
						+ " could not be found in the database.");
			}
			final String oldLabel = speciesEntity.getLabel();
			speciesAssembler.modifyEntity(speciesEntity, species);
			// Flush to increment version
			session.flush();
			final GwtSpecies result = speciesAssembler.buildDto(speciesEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} changed species from {} to {}.",
					new Object[] { M,
							user.getUsername(),
							oldLabel, result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtStrain modifyStrainLabel(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyStrainLabel(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = parent.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity speciesEntity = speciesDAO.findById(id, false);
			if (speciesEntity == null) {
				throw new ObjectNotFoundException("Species "
						+ parent.getLabel()
						+ " could not be found in the database.");
			}
			final Optional<StrainEntity> strainEntityOpt = tryFind(
					speciesEntity.getStrains(),
					compose(equalTo(strain.getId()), StrainEntity.getId));
			if (!strainEntityOpt.isPresent()) {
				throw new ObjectNotFoundException("Strain "
						+ strain.getLabel()
						+ " could not be found in the database.");
			}
			final StrainEntity strainEntity = strainEntityOpt.get();
			final String oldLabel = strainEntity.getLabel();
			strainAssembler.modifyEntity(strainEntity, strain);
			// Flush to increment version
			session.flush();
			final GwtStrain result = strainAssembler.buildDto(strainEntity,
					speciesEntity.getLabel(), speciesEntity.getId());
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} changed strain from {} to {} for species {}.",
					new Object[] { M,
							user.getUsername(),
							oldLabel, result.getLabel(),
							speciesEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteStrain(GwtSpecies parent, GwtStrain strain)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteStrain(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = checkNotNull(strain.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStrainDAO strainDAO = new StrainDAOHibernate(session);
			final StrainEntity strainEntity = strainDAO.findById(id, false);
			if (strainEntity == null) {
				throw new ObjectNotFoundException("Strain "
						+ strain.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = strain.getVersion();
			final Integer entityV = strainEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete strain "
								+ strain.getLabel()
								+ ". This strain has been modified by another user. Please reload.");
			}
			strainDAO.delete(strainEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted strain {} from species {}.",
					new Object[] { M,
							user.getUsername(),
							strain.getLabel(), parent.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void deleteSpecies(GwtSpecies species)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteSpecies(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = species.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity speciesEntity = speciesDAO.findById(id, false);
			if (speciesEntity == null) {
				throw new ObjectNotFoundException("Species "
						+ species.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = species.getVersion();
			final Integer entityV = speciesEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete species "
								+ species.getLabel()
								+ ". This species has been modified by another user. Please reload.");
			}
			speciesDAO.delete(speciesEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted species {}.",
					new Object[] { M,
							user.getUsername(),
							species.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public List<GwtDrug> retrieveDrugs()
			throws BrainTrustAuthorizationException {
		final String M = "retrieveDrugs(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final List<GwtDrug> gwtDrug = retrieveDrugs(session);
			trx.commit();
			logger.info(
					"{}: Returned {} drugs.",
					new Object[] { M, gwtDrug.size() });
			return gwtDrug;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public List<GwtStimRegion> retrieveStimRegions()
			throws BrainTrustAuthorizationException {
		final String M = "retrieveStimRegions(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final List<GwtStimRegion> gwtStimRegions = retrieveStimRegions(session);
			trx.commit();
			logger.info(
					"{}: Returned {} stimulation regions.",
					new Object[] { M, gwtStimRegions.size() });
			return gwtStimRegions;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public List<GwtStimType> retrieveStimTypes()
			throws BrainTrustAuthorizationException {
		final String M = "retrieveStimTypes(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final List<GwtStimType> gwtStimTypes = retrieveStimTypes(session);
			trx.commit();
			logger.info(
					"{}: Returned {} stimulation Types.",
					new Object[] { M, gwtStimTypes.size() });
			return gwtStimTypes;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtDrug modifyDrugLabel(GwtDrug drug)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyDrugLabel(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IDrugDAO drugDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate(session);
			final DrugEntity drugEntity = drugDAO.findById(drug.getId(),
					false);
			if (drugEntity == null) {
				throw new ObjectNotFoundException("Drug "
						+ drug.getLabel()
						+ " could not be found in the database.");
			}
			final String oldLabel = drugEntity.getLabel();
			drugAssembler.modifyEntity(drugEntity, drug);
			// flush so that version is incremented for the returned dto
			session.flush();
			final GwtDrug result = drugAssembler.buildDto(drugEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} changed drug from {} to {}.",
					new Object[] { M,
							user.getUsername(),
							oldLabel,
							result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtDrug createDrug(GwtDrug drug)
			throws BrainTrustAuthorizationException, ObjectNotFoundException {
		final String M = "createDrug(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IDrugDAO drugDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate(session);
			final DrugEntity drugEntity = drugAssembler.buildEntity(drug);
			drugDAO.saveOrUpdate(drugEntity);
			session.flush();
			final GwtDrug result = drugAssembler.buildDto(drugEntity);
			trx.commit();
			logger.info("{}: Created drug {}.",
					new Object[] { M,
							result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteDrug(GwtDrug drug)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteDrug(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = checkNotNull(drug.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IDrugDAO drugDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate(session);
			final DrugEntity drugEntity = drugDAO.findById(id, false);
			if (drugEntity == null) {
				throw new ObjectNotFoundException("Drug "
						+ drug.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = drug.getVersion();
			final Integer entityV = drugEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete drug "
								+ drug.getLabel()
								+ ". This drug has been modified by another user. Please reload.");
			}
			drugDAO.delete(drugEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted drug {}.",
					new Object[] { M,
							user.getUsername(),
							drugEntity.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtStimRegion createStimRegion(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException, ObjectNotFoundException {
		final String M = "createStimRegion(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimRegionDAO stimRegionDAO = new StimRegionDAOHibernate(
					session);
			final StimRegionEntity stimRegionEntity = stimRegionAssembler
					.buildEntity(stimRegion);
			stimRegionDAO.saveOrUpdate(stimRegionEntity);
			session.flush();
			final GwtStimRegion result = stimRegionAssembler
					.buildDto(stimRegionEntity);
			trx.commit();
			logger.info("{}: Created stimulation region {}.",
					new Object[] { M,
							result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void deleteStimRegion(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteStimRegion(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = stimRegion.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimRegionDAO stimRegionDAO = new StimRegionDAOHibernate(
					session);
			final StimRegionEntity stimRegionEntity = stimRegionDAO.findById(
					id, false);
			if (stimRegionEntity == null) {
				throw new ObjectNotFoundException("Stimulation region "
						+ stimRegion.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = stimRegion.getVersion();
			final Integer entityV = stimRegionEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete stimulation region "
								+ stimRegion.getLabel()
								+ ". This stimulation region has been modified by another user. Please reload.");
			}
			stimRegionDAO.delete(stimRegionEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted stimRegion {}.",
					new Object[] { M,
							user.getUsername(),
							stimRegionEntity.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtStimRegion modifyStimRegionLabel(GwtStimRegion stimRegion)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyStimRegionLabel(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimRegionDAO stimRegionDAO = new StimRegionDAOHibernate(
					session);
			final StimRegionEntity stimRegionEntity = stimRegionDAO.findById(
					stimRegion.getId(),
					false);
			if (stimRegionEntity == null) {
				throw new ObjectNotFoundException("Stimulation region "
						+ stimRegion.getLabel()
						+ " could not be found in the database.");
			}
			final String oldLabel = stimRegionEntity.getLabel();
			stimRegionAssembler.modifyEntity(stimRegionEntity, stimRegion);
			// Flush to increment version
			session.flush();
			final GwtStimRegion result = stimRegionAssembler
					.buildDto(stimRegionEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} changed stimulation region from {} to {}.",
					new Object[] { M,
							user.getUsername(),
							oldLabel, result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtStimType createStimType(GwtStimType stimType)
			throws BrainTrustAuthorizationException, ObjectNotFoundException {
		final String M = "createStimType(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimTypeDAO stimTypeDAO = new StimTypeDAOHibernate(
					session);
			final StimTypeEntity stimTypeEntity = stimTypeAssembler
					.buildEntity(stimType);
			stimTypeDAO.saveOrUpdate(stimTypeEntity);
			session.flush();
			final GwtStimType result = stimTypeAssembler
					.buildDto(stimTypeEntity);
			trx.commit();
			logger.info("{}: Created stimulation type {}.",
					new Object[] { M,
							result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteStimType(GwtStimType stimType)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteStimType(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = stimType.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimTypeDAO stimTypeDAO = new StimTypeDAOHibernate(
					session);
			final StimTypeEntity stimTypeEntity = stimTypeDAO.findById(
					id, false);
			if (stimTypeEntity == null) {
				throw new ObjectNotFoundException("Stimulation type "
						+ stimType.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = stimType.getVersion();
			final Integer entityV = stimTypeEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete stimulation type "
								+ stimType.getLabel()
								+ ". This stimulation type has been modified by another user. Please reload.");
			}
			stimTypeDAO.delete(stimTypeEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted stimType {}.",
					new Object[] { M,
							user.getUsername(),
							stimTypeEntity.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtStimType modifyStimTypeLabel(GwtStimType stimType)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyStimTypeLabel(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IStimTypeDAO stimTypeDAO = new StimTypeDAOHibernate(session);
			final StimTypeEntity stimTypeEntity = stimTypeDAO.findById(
					stimType.getId(),
					false);
			if (stimTypeEntity == null) {
				throw new ObjectNotFoundException("Stimulation type "
						+ stimType.getLabel()
						+ " could not be found in the database.");
			}
			final String oldLabel = stimTypeEntity.getLabel();
			stimTypeAssembler.modifyEntity(stimTypeEntity, stimType);
			// Flush to increment version
			session.flush();
			final GwtStimType result = stimTypeAssembler
					.buildDto(stimTypeEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} changed stimulation type from {} to {}.",
					new Object[] { M,
							user.getUsername(),
							oldLabel, result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtAnimal saveAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException {
		final String M = "saveAnimal(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
					session);
			final IStrainDAO strainDAO = new StrainDAOHibernate(session);
			final AnimalAssembler animalAssembler = new AnimalAssembler(
					organizationDAO,
					strainDAO);
			final AnimalEntity animalEntity = animalAssembler
					.buildEntity(animal);
			animalDAO.saveOrUpdate(animalEntity);
			session.flush();
			final GwtAnimal result = animalAssembler.buildDto(animalEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} created animal {}",
					new Object[] { M,
							user.getUsername(),
							result.getLabel() });
			return result;
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtAnimal modifyAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException, BrainTrustUserException {
		final String M = "modifyAnimal(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
					session);
			final IStrainDAO strainDAO = new StrainDAOHibernate(session);
			final AnimalAssembler animalAssembler = new AnimalAssembler(
					organizationDAO,
					strainDAO);
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			final AnimalEntity animalEntity = animalDAO.findById(
					animal.getId(),
					false);
			if (animalEntity == null) {
				throw new ObjectNotFoundException("Animal "
						+ animal.getLabel()
						+ " could not be found in the database.");
			}
			animalAssembler.modifyEntity(animalEntity, animal);
			// Flush for version increment
			session.flush();
			final GwtAnimal result = animalAssembler.buildDto(animalEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified animal {}.",
					new Object[] { M,
							user.getUsername(),
							result.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtExperiment createExperiment(GwtAnimal parent,
			GwtExperiment experiment) throws BrainTrustAuthorizationException,
			ObjectNotFoundException, BrainTrustUserException {
		final String M = "createExperiment(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = checkNotNull(parent.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			final AnimalEntity parentEntity = animalDAO.findById(id, false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Animal "
						+ parent.getLabel()
						+ " could not be found in the database.");
			}
			ITsAnnotationDAO annotationDAO = new TsAnnotationDAOHibernate(
					session, HibernateUtil.getConfiguration());
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(session),
					new StimTypeDAOHibernate(session),
					new PermissionDAOHibernate(session),
					annotationDAO);
			final ExperimentEntity experimentEntity = experimentAssembler
					.buildEntity(
							parentEntity, experiment);
			session.saveOrUpdate(parentEntity);
			session.flush();
			final GwtExperiment result = experimentAssembler
					.buildDto(experimentEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} created experiment {} for animal {}.",
					new Object[] { M,
							user.getUsername(),
							result.getLabel(),
							parentEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtAnimalExperimentPair retrieveAnimalByLabel(String animalLabel)
			throws BrainTrustAuthorizationException {
		final String M = "retrieveAnimalByLabel(...)";
		Session session = null;
		Transaction trx = null;

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			checkNotNull(animalLabel);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			final AnimalEntity animalEntity = animalDAO
					.findByLabel(animalLabel);
			GwtAnimalExperimentPair result = null;
			if (animalEntity == null) {
				logger.info(
						"{}: Animal with label {} could not be found.",
						M, animalLabel);
			} else {
				final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
						session);
				IStrainDAO strainDAO = new StrainDAOHibernate(
						session);
				final AnimalAssembler animalAssembler = new AnimalAssembler(
						organizationDAO,
						strainDAO);
				ITsAnnotationDAO annotationDAO = new TsAnnotationDAOHibernate(
						session, HibernateUtil.getConfiguration());
				final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
						new StimRegionDAOHibernate(session),
						new StimTypeDAOHibernate(session),
						new PermissionDAOHibernate(session),
						annotationDAO);
				final GwtAnimal animal = animalAssembler
						.buildDto(animalEntity);

				final ImmutableList.Builder<GwtExperiment> experimentListBuilder = ImmutableList
						.builder();
				final List<ExperimentEntity> temp = newArrayList(animalEntity
						.getExperiments());
				Collections.sort(temp, ExperimentEntity.LABEL_COMPARATOR);
				for (final ExperimentEntity experimentEntity : temp) {
					experimentListBuilder.add(experimentAssembler
							.buildDto(experimentEntity));
				}
				result = new GwtAnimalExperimentPair(animal,
						experimentListBuilder.build());
				logger.info("{}: Retrieved animal {}.", M, animal.getLabel());

			}
			trx.commit();
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteAnimal(GwtAnimal animal)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteAnimal(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = animal.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(
					session);
			final AnimalEntity animalEntity = animalDAO.findById(
					id, false);
			if (animalEntity == null) {
				throw new ObjectNotFoundException("Animal "
						+ animal.getLabel()
						+ " could not be found in the database.");
			}
			final Integer dtoV = animal.getVersion();
			final Integer entityV = animalEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete animal "
								+ animal.getLabel()
								+ ". This animal has been modified by another user. Please reload.");
			}
			animalDAO.delete(animalEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted animal {}.",
					new Object[] { M,
							user.getUsername(),
							animalEntity.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtExperiment modifyExperiment(
			GwtAnimal parent,
			GwtExperiment experiment)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyExperiment(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = parent.getId();
			final Long experimentId = experiment.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity experimentEntity = experimentDAO.findById(
					experimentId, false);
			if (experimentEntity == null) {
				throw new ObjectNotFoundException("Experiment "
						+ parent.getLabel()
						+ " could not be found in the database.");
			}
			if (!parentId.equals(experimentEntity.getParent().getId())) {
				throw new IllegalArgumentException("Experiment "
						+ experiment.getLabel()
						+ " is not the child of animal " + parent.getLabel());
			}
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(session),
					new StimTypeDAOHibernate(session),
					new PermissionDAOHibernate(session),
					new TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration()));
			// TODO: check if the experiment is really modifiable. For now just
			// say true.
			experimentAssembler
					.modifyEntity(experimentEntity, experiment, true);
			// Flush to increment version
			session.flush();
			final GwtExperiment result = experimentAssembler
					.buildDto(experimentEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified experiment {}.",
					new Object[] { M,
							user.getUsername(),
							experimentEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteExperiment(GwtAnimal parent,
			GwtExperiment experiment)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteExperiment(...)";

		Session session = null;
		Transaction trx = null;
		try {

			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = parent.getId();
			final Long experimentId = experiment.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity experimentEntity = experimentDAO.findById(
					experimentId, false);
			if (experimentEntity == null) {
				throw new ObjectNotFoundException("Experiment "
						+ experiment.getLabel()
						+ " could not be found in the database.");
			}
			final AnimalEntity parentEntity = experimentEntity.getParent();
			if (!parentId.equals(parentEntity.getId())) {
				throw new IllegalArgumentException("Experiment "
						+ experiment.getLabel()
						+ " is not the child of animal " + parent.getLabel());
			}
			final Integer dtoV = experiment.getVersion();
			final Integer entityV = experimentEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete experiment "
								+ experiment.getLabel()
								+ ". This experiment has been modified by another user. Please reload.");
			}
			ITsAnnotationDAO annotationDAO = new TsAnnotationDAOHibernate(
					session, 
					HibernateUtil.getConfiguration());
			annotationDAO.deleteByParent(experimentEntity);
			parentEntity.removeExperiment(experimentEntity);
			//experimentDAO.delete(experimentEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted experiment {} from animal {}.",
					new Object[] { M,
							user.getUsername(),
							experiment.getLabel(),
							parent.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtRecording modifyRecording(
			GwtExperiment parent,
			GwtRecording recording)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyRecording(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = parent.getId();
			final Long recordingId = recording.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity parentEntity = experimentDAO.findById(
					parentId, false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Parent of recording "
						+ recording.getDir()
						+ " could not be found in the database.");
			}
			final Recording recordingEntity = parentEntity.getRecording();
			if (recordingEntity == null) {
				throw new ObjectNotFoundException("Recording "
						+ recording.getDir()
						+ " could not be found in the database.");
			}
			if (!recording.getId()
					.equals(recordingEntity.getId())) {
				throw new IllegalArgumentException("Recording " + recordingId
						+ " does not belong to experiment " + parentId);
			}
			final RecordingAssembler recordingAssembler = new RecordingAssembler(
					new TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration()));
			// TODO: check if the experiment is really modifiable. For now just
			// say true.
			recordingAssembler
					.modifyEntity(parentEntity, recordingEntity, recording,
							true);
			// Flush to increment version
			session.flush();
			final GwtRecording result = recordingAssembler
					.buildDto(recordingEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified recording {}.",
					new Object[] { M,
							user.getUsername(),
							recordingEntity.getDir() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public List<GwtTrace> createTraces(GwtContactGroup gwtParent,
			Set<GwtTrace> gwtTraces)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			BrainTrustUserException {
		final String M = "createTraces(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			final Long parentId = checkNotNull(gwtParent.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IContactGroupDAO electrodeDAO = new ContactGroupDAOHibernate(
					session);
			final ContactGroup parentEntity = electrodeDAO.findById(parentId,
					false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Electrode "
						+ gwtParent.getChannelPrefix()
						+ " could not be found in the database.");
			}
			checkArgument(parentEntity.getContacts().isEmpty(), "Electrode "
					+ parentEntity.getChannelPrefix()
					+ " already has contacts.");
			final Set<Contact> newContacts = newHashSet();
			for (final GwtTrace gwtTrace : gwtTraces) {
				final Contact contactEntity = traceAssembler.buildEntity(
						gwtTrace);
				parentEntity.addContact(contactEntity);
				newContacts.add(contactEntity);
			}
			session.saveOrUpdate(parentEntity);
			session.flush();
			final List<GwtTrace> result = newArrayList();
			for (final Contact contactEntity : newContacts) {
				result.add(traceAssembler.buildDto(contactEntity));
			}
			trx.commit();
			Collections.sort(result, GwtTrace.LABEL_COMPARATOR);
			logger.info(
					"{}: Created {} contacts and time series for electrode {}.",
					new Object[] { M,
							result.size(), parentEntity.getChannelPrefix() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtTrace modifyTrace(
			GwtContactGroup gwtParent,
			GwtTrace gwtTrace)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyTrace(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = gwtParent.getId();
			final Long contactId = gwtTrace.getContactId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IContactDAO contactDAO = new ContactDAOHibernate(
					session);
			final Contact contactEntity = contactDAO.findById(
					contactId, false);
			if (contactEntity == null) {
				throw new ObjectNotFoundException("Contact "
						+ contactId
						+ " could not be found in the database.");
			}
			if (!parentId.equals(contactEntity.getParent().getId())) {
				throw new IllegalArgumentException("Contact "
						+ contactEntity.getTrace().getLabel()
						+ " is not the child of animal "
						+ gwtParent.getChannelPrefix());
			}
			final TraceAssembler traceAssembler = new TraceAssembler();
			traceAssembler
					.modifyEntity(contactEntity, gwtTrace);
			// Flush to increment version
			session.flush();
			final GwtTrace result = traceAssembler
					.buildDto(contactEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified trace {}.",
					new Object[] { M,
							user.getUsername(),
							contactEntity.getTrace().getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void deleteTrace(IHasGwtRecording recordingParent,
			GwtContactGroup traceParent, GwtTrace trace)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteTrace(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = traceParent.getId();
			final Long contactId = trace.getContactId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IContactDAO contactDAO = new ContactDAOHibernate(
					session);
			final Contact contactEntity = contactDAO.findById(
					contactId, false);
			if (contactEntity == null) {
				throw new ObjectNotFoundException("Contact "
						+ contactId
						+ " could not be found in the database.");
			}
			if (!parentId.equals(contactEntity.getParent().getId())) {
				throw new IllegalArgumentException("Contact "
						+ contactEntity.getTrace().getLabel()
						+ " is not the child of electrode "
						+ traceParent.getChannelPrefix());
			}
			final Integer dtoContactV = trace.getContactVersion();
			final Integer contactEntityV = contactEntity.getVersion();
			if (!dtoContactV.equals(contactEntityV)) {
				throw new StaleObjectException(
						"Could not delete trace "
								+ trace.getLabel()
								+ ". This contact has been modified by another user. Please reload.");
			}
			final Integer dtoTimeSeriesV = trace.getVersion();
			final Integer timeSeriesEntityV = contactEntity.getTrace()
					.getVersion();
			if (!dtoTimeSeriesV.equals(timeSeriesEntityV)) {
				throw new StaleObjectException(
						"Could not delete trace "
								+ trace.getLabel()
								+ ". This contact has been modified by another user. Please reload.");
			}
			final IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(session);
			final DataSnapshotEntity recordingParentEntity = dsDAO.findById(
					recordingParent.getId(), false);
			final ITsAnnotationDAO annotationDOA = new TsAnnotationDAOHibernate(
					session, HibernateUtil.getConfiguration());

			annotationDOA.removeAnnotatedAndDeleteEmptyAnnotations(
					recordingParentEntity,
					contactEntity.getTrace());
			contactDAO.delete(contactEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} deleted contact and time series {} from electrode {}.",
					new Object[] { M,
							user.getUsername(),
							trace.getLabel(),
							traceParent.getChannelPrefix() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public List<GwtTrace> findTraces(GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException {
		final String M = "findTraces(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final IContactGroupDAO electrodeDAO = new ContactGroupDAOHibernate(
					session);
			final ContactGroup electrodeEntity = electrodeDAO.findById(
					gwtContactGroup.getId(), false);

			final List<GwtTrace> gwtTraces = newArrayList();
			for (final Contact contact : electrodeEntity.getContacts()) {
				gwtTraces.add(traceAssembler.buildDto(contact));
			}
			Collections.sort(gwtTraces, GwtTrace.LABEL_COMPARATOR);
			trx.commit();
			logger.info(
					"{}: Returned {} traces for electrode {}.",
					new Object[] { M, gwtTraces.size(),
							electrodeEntity.getChannelPrefix() });
			return gwtTraces;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtContactGroup createContactGroup(
			GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			BrainTrustUserException {
		final String M = "createContactGroup(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long recordingId = checkNotNull(parent.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IRecordingDAO recordingDAO = new RecordingDAOHibernate(
					session);
			final Recording recordingEntity = recordingDAO.findById(
					recordingId, false);
			if (recordingEntity == null) {
				throw new ObjectNotFoundException("Recording "
						+ recordingId
						+ " could not be found in the database.");
			}
			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			final ContactGroup electrodeEntity = electrodeAssembler
					.buildEntity(gwtContactGroup);
			recordingEntity.addContactGroup(electrodeEntity);
			session.saveOrUpdate(recordingEntity);
			session.flush();
			final GwtContactGroup result = electrodeAssembler
					.buildDto(electrodeEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} created electrode {} for recording {}.",
					new Object[] { M,
							user.getUsername(),
							result.getChannelPrefix(), recordingEntity.getDir() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtContactGroup modifyContactGroup(
			GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyElectrode(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long recordingId = parent.getId();
			final Long electrodeId = gwtContactGroup.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IContactGroupDAO electrodeDAO = new ContactGroupDAOHibernate(
					session);
			final ContactGroup electrodeEntity = electrodeDAO.findById(
					electrodeId, false);
			if (electrodeEntity == null) {
				throw new ObjectNotFoundException("Electrode "
						+ electrodeId
						+ " could not be found in the database.");
			}
			if (!recordingId.equals(electrodeEntity.getParent().getId())) {
				throw new IllegalArgumentException("Electrode "
						+ electrodeEntity.getId()
						+ " is not the child of recording "
						+ recordingId);
			}
			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			electrodeAssembler
					.modifyEntity(electrodeEntity, gwtContactGroup);
			// Flush for version increment
			session.flush();
			final GwtContactGroup result = electrodeAssembler
					.buildDto(electrodeEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified electrode {}.",
					new Object[] { M,
							user.getUsername(),
							electrodeEntity.getChannelPrefix() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void deleteContactGroup(IHasGwtRecording recordingParent,
			GwtRecording parent,
			GwtContactGroup gwtContactGroup)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteElectrode(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long recordingId = parent.getId();
			final Long electrodeId = gwtContactGroup.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IContactGroupDAO electrodeDAO = new ContactGroupDAOHibernate(
					session);
			final ContactGroup electrodeEntity = electrodeDAO.findById(
					electrodeId, false);
			if (electrodeEntity == null) {
				throw new ObjectNotFoundException("Electrode "
						+ electrodeId
						+ " could not be found in the database.");
			}
			final IRecordingDAO recordingDAO = new RecordingDAOHibernate(
					session);
			final Recording recordingEntity = recordingDAO.findById(
					recordingId, false);
			if (recordingEntity == null) {
				throw new ObjectNotFoundException("Recording "
						+ electrodeId
						+ " could not be found in the database.");
			}
			if (!recordingEntity.equals(electrodeEntity.getParent())) {
				throw new IllegalArgumentException("Electrode "
						+ electrodeId
						+ " is not the child of recording "
						+ recordingId);
			}
			final Integer dtoV = gwtContactGroup.getVersion();
			final Integer entityV = electrodeEntity
					.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete electrode "
								+ gwtContactGroup.getChannelPrefix()
								+ ". This contact has been modified by another user. Please reload.");
			}
			final IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(session);
			final DataSnapshotEntity recordingParentEntity = dsDAO.findById(
					recordingParent.getId(), false);
			final ITsAnnotationDAO annotationDOA = new TsAnnotationDAOHibernate(
					session, HibernateUtil.getConfiguration());
			for (final Contact contact : electrodeEntity.getContacts()) {
				annotationDOA.removeAnnotatedAndDeleteEmptyAnnotations(
						recordingParentEntity,
						contact.getTrace());
			}
			recordingEntity.removeContactGroup(electrodeEntity);
			electrodeDAO.delete(electrodeEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} deleted electrode {} from recording {}.",
					new Object[] { M,
							user.getUsername(),
							gwtContactGroup.getChannelPrefix(),
							recordingEntity.getDir() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public List<GwtDrugAdmin> findDrugAdmins(GwtExperiment experiment)
			throws BrainTrustAuthorizationException {
		final String M = "findDrugAdmins(...)";

		Session session = null;
		Transaction trx = null;
		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity experimentEntity = experimentDAO.findById(
					experiment.getId(), false);

			final List<GwtDrugAdmin> gwtAdmins = newArrayList();
			for (final DrugAdminRelationship adminEntity : experimentEntity
					.getDrugAdmins()) {
				gwtAdmins.add(drugAdminAssembler.buildDto(adminEntity));
			}
			Collections.sort(gwtAdmins,
					new GwtDrugAdmin.GwtDrugAdminComparator());
			trx.commit();
			logger.info(
					"{}: Returned {} drugAdmins for experiment {}.",
					new Object[] { M, gwtAdmins.size(),
							experimentEntity.getLabel() });
			return gwtAdmins;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public GwtDrugAdmin createDrugAdmin(GwtExperiment parent,
			GwtDrugAdmin gwtAdmin) throws BrainTrustAuthorizationException,
			ObjectNotFoundException, BrainTrustUserException {
		final String M = "createDrugAdmin(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long experimentId = checkNotNull(parent.getId());
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity experimentEntity = experimentDAO.findById(
					experimentId, false);
			if (experimentEntity == null) {
				throw new ObjectNotFoundException("Experiment "
						+ experimentId
						+ " could not be found in the database.");
			}
			final IDrugDAO drugDAO = new DrugDAOHibernate(session);
			final DrugEntity drugEntity = drugDAO.findById(gwtAdmin.getDrug()
					.getId(),
					false);
			if (drugEntity == null) {
				throw new ObjectNotFoundException("Drug "
						+ gwtAdmin.getDrug().getLabel()
						+ " could not be found in the database using id "
						+ gwtAdmin.getDrug().getId() + ".");
			}
			final DrugAdminRelationship adminEntity = drugAdminAssembler.buildEntity(
					experimentEntity, drugEntity, gwtAdmin);
			experimentEntity.getDrugAdmins().add(adminEntity);
			session.saveOrUpdate(experimentEntity);
			session.flush();
			final GwtDrugAdmin result = drugAdminAssembler
					.buildDto(adminEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} created drug admin {} for experiment {}.",
					new Object[] { M,
							user.getUsername(),
							adminEntity.getAdminTime(),
							experimentEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtDrugAdmin modifyDrugAdmin(
			GwtExperiment parent,
			GwtDrugAdmin drugAdmin)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException,
			BrainTrustUserException {
		final String M = "modifyDrugAdmin(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = parent.getId();
			final Long adminId = drugAdmin.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IDrugAdminDAO drugAdminDAO = new DrugAdminDAOHibernate(
					session);
			final DrugAdminRelationship drugAdminEntity = drugAdminDAO.findById(
					adminId, false);
			if (drugAdminEntity == null) {
				throw new ObjectNotFoundException("DrugAdmin "
						+ adminId
						+ " could not be found in the database.");
			}
			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity parentEntity = experimentDAO.findById(
					parentId, false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Experiment "
						+ parentId
						+ " could not be found in the database.");
			}
			if (!parentEntity
					.equals(drugAdminEntity.getParent())) {
				throw new IllegalArgumentException("DrugAdmin " + adminId
						+ " does not belong to experiment " + parentId);
			}
			final IDrugDAO drugDAO = new DrugDAOHibernate(session);
			DrugEntity newDrug = drugDAO.findById(drugAdmin.getDrug().getId(),
					false);
			if (newDrug == null) {
				throw new ObjectNotFoundException("Drug "
						+ drugAdmin.getDrug().getId()
						+ " could not be found in the database.");
			}
			drugAdminAssembler
					.modifyEntity(newDrug, drugAdminEntity, drugAdmin);
			// Flush for version increment
			session.flush();
			final GwtDrugAdmin result = drugAdminAssembler
					.buildDto(drugAdminEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} modified DrugAdmin ({}, {}, {}) for Experiment {}.",
					new Object[] { M,
							user.getUsername(),
							drugAdminEntity.getAdminTime(),
							drugAdminEntity.getDrug().getLabel(),
							drugAdminEntity.getDoseMgPerKg(),
							parentEntity.getLabel() });
			return result;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t,
					BrainTrustAuthorizationException.class);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			Throwables.propagateIfInstanceOf(t, BrainTrustUserException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public void deleteDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String M = "deleteDrugAdmin(...)";

		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long parentId = parent.getId();
			final Long drugAdminId = drugAdmin.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IDrugAdminDAO drugAdminDAO = new DrugAdminDAOHibernate(
					session);
			final DrugAdminRelationship drugAdminEntity = drugAdminDAO.findById(
					drugAdminId, false);
			if (drugAdminEntity == null) {
				throw new ObjectNotFoundException("DrugAdmin "
						+ drugAdminId
						+ " could not be found in the database.");
			}
			final Integer dtoV = drugAdmin.getVersion();
			final Integer entityV = drugAdminEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete DrugAdmin "
								+ drugAdmin.getDrug().getLabel()
								+ ". This strain has been modified by another user. Please reload.");
			}
			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					session);
			final ExperimentEntity parentEntity = experimentDAO.findById(
					parentId, false);
			if (parentEntity == null) {
				throw new ObjectNotFoundException("Experiment "
						+ parentId
						+ " could not be found in the database.");
			}
			final boolean removed = parentEntity.getDrugAdmins().remove(
					drugAdminEntity);
			if (!removed) {
				throw new IllegalArgumentException("DrugAdmin " + drugAdminId
						+ " does not belong to experiment " + parentId);
			}
			drugAdminDAO.delete(drugAdminEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} deleted DrugAdmin ({}, {}, {}) from experiment {}.",
					new Object[] { M,
							user.getUsername(),
							drugAdminEntity.getAdminTime(),
							drugAdminEntity.getDrug().getLabel(),
							drugAdminEntity.getDoseMgPerKg(),
							parent.getLabel() });
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			logger.error(M + ": Transaction rolled back after exception.", t);
			Throwables.propagateIfInstanceOf(t, ObjectNotFoundException.class);
			Throwables.propagateIfInstanceOf(t, StaleObjectException.class);
			propagateIfInstanceOf(t, BrainTrustAuthorizationException.class);
			throw Throwables.propagate(t);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public List<GwtOrganization> retrieveOrganizations()
			throws BrainTrustAuthorizationException {
		final String m = "retrieveOrganizations(...)";
		Session session = null;
		Transaction trx = null;

		try {
			authzOrThrowException((UserId) SecurityUtils.getSubject()
					.getPrincipal());
			session = HibernateUtil.getSessionFactory()
					.openSession();
			trx = session.beginTransaction();

			final List<GwtOrganization> gwtOrganizations = retrieveOrganizations(session);
			trx.commit();
			logger.info(
					"{}: Returned {} organizations.",
					new Object[] { m, gwtOrganizations.size() });
			return gwtOrganizations;
		} catch (BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtOrganization modifyOrganization(GwtOrganization modifiedOrg)
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException {
		final String m = "modifyOrganization(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IOrganizationDAO orgDAO = new OrganizationDAOHibernate(
					session);
			final OrganizationEntity orgEntity = orgDAO.findById(
					modifiedOrg.getId(),
					false);
			if (orgEntity == null) {
				throw new ObjectNotFoundException("Organization "
						+ modifiedOrg
						+ " could not be found in the database.");
			}
			organizationAssembler.modifyEntity(orgEntity, modifiedOrg);
			// Flush to increment version
			session.flush();
			final GwtOrganization result = organizationAssembler
					.buildDto(orgEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info(
					"{}: User {} changed organization to be {}.",
					new Object[] { m,
							user.getUsername(),
							result });
			return result;
		} catch (BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (ObjectNotFoundException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (StaleObjectException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void deleteOrganization(GwtOrganization org)
			throws BrainTrustAuthorizationException, ObjectNotFoundException,
			StaleObjectException {
		final String m = "deleteOrganization(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			final Long id = org.getId();
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IOrganizationDAO orgDAO = new OrganizationDAOHibernate(
					session);
			final OrganizationEntity orgEntity = orgDAO.findById(id, false);
			if (orgEntity == null) {
				throw new ObjectNotFoundException("Organization "
						+ org
						+ " could not be found in the database.");
			}
			final Integer dtoV = org.getVersion();
			final Integer entityV = orgEntity.getVersion();
			if (!dtoV.equals(entityV)) {
				throw new StaleObjectException(
						"Could not delete organization "
								+ org
								+ ". This organization has been modified by another user. Please reload.");
			}
			orgDAO.delete(orgEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} deleted organization {}.",
					new Object[] { m,
							user.getUsername(),
							org });

		} catch (BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (ObjectNotFoundException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (StaleObjectException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtOrganization createOrganization(GwtOrganization newOrg)
			throws BrainTrustAuthorizationException {
		final String m = "createOrganization(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IOrganizationDAO orgDAO = new OrganizationDAOHibernate(
					session);
			final OrganizationEntity orgEntity = organizationAssembler
					.buildEntity(newOrg);
			orgDAO.saveOrUpdate(orgEntity);
			session.flush();
			final GwtOrganization result = organizationAssembler
					.buildDto(orgEntity);
			trx.commit();
			final User user = userService.findUserByUid(userId);
			logger.info("{}: User {} created organization {}.",
					new Object[] {
							m,
							user.getUsername(),
							result });
			return result;
		} catch (final BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public GwtAnimalExperimentPair saveAnimalWithNewExperiment(
			GwtAnimal animal,
			GwtExperiment experiment,
			Map<String, Set<GwtTrace>> channelPrefixToTraces) throws
			BrainTrustAuthorizationException,
			DuplicateNameException,
			FileNotFoundException,
			BrainTrustUserException {
		final String m = "saveAnimalWithNewExperiment(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			final User user = userService.findUserByUid(userId);
			authzOrThrowException(userId);
			checkNotNull(animal);
			checkNotNull(experiment);
			checkNotNull(channelPrefixToTraces);

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final String animalLabel = animal.getLabel();
			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			AnimalEntity animalEntity = animalDAO
					.findByLabel(animalLabel);
			final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
					session);
			final IStrainDAO strainDAO = new StrainDAOHibernate(session);
			final AnimalAssembler animalAssembler = new AnimalAssembler(
					organizationDAO,
					strainDAO);
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(session),
					new StimTypeDAOHibernate(session),
					new PermissionDAOHibernate(session),
					new TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration()));

			if (animalEntity == null) {
				animalEntity = animalAssembler
						.buildEntity(animal);
				logger.info(
						"{}: User {} created animal {}.",
						new Object[] {
								m,
								user.getUsername(),
								animalEntity.getLabel() });
			} else {
				final ExperimentEntity existingExperiment = tryFind(
						animalEntity.getExperiments(),
						compose(equalTo(experiment.getLabel()),
								ExperimentEntity.getLabel)).orNull();
				if (existingExperiment != null) {
					throw new DuplicateNameException(experiment.getLabel());
				}
			}
			final ExperimentEntity experimentEntity = experimentAssembler
					.buildEntity(
							animalEntity,
							experiment);
			createContactGroupsFromFileKeys(channelPrefixToTraces,
					experimentEntity);
			animalDAO.saveOrUpdate(animalEntity);
			session.flush();
			final GwtAnimal resultAnimal = animalAssembler
					.buildDto(animalEntity);

			final ImmutableList.Builder<GwtExperiment> experimentListBuilder = ImmutableList
					.builder();
			final List<ExperimentEntity> experimentEntities = newArrayList(animalEntity
					.getExperiments());
			Collections.sort(experimentEntities,
					ExperimentEntity.LABEL_COMPARATOR);
			for (final ExperimentEntity expEntity : experimentEntities) {
				experimentListBuilder.add(experimentAssembler
						.buildDto(expEntity));
			}
			final GwtAnimalExperimentPair result = new GwtAnimalExperimentPair(
					resultAnimal,
					experimentListBuilder.build());
			trx.commit();

			logger.info(
					"{}: User {} created experiment {} with {} contact groups.",
					new Object[] {
							m,
							user.getUsername(),
							experimentEntity.getLabel(),
							experimentEntity.getRecording().getContactGroups()
									.size() });
			return result;
		} catch (final BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final DuplicateNameException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final FileNotFoundException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (BrainTrustUserException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Override
	public OptionsLists retrieveOptionsLists()
			throws BrainTrustAuthorizationException {
		final String m = "retrieveOptionsLists(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			final User user = userService.findUserByUid(userId);
			authzOrThrowException(userId);
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			// Orgs
			final List<GwtOrganization> gwtOrganizations = retrieveOrganizations(session);

			// Species
			final List<GwtSpecies> gwtSpecies = retrieveSpecies(session);

			// Drugs
			final List<GwtDrug> gwtDrugs = retrieveDrugs(session);

			// StimRegions
			final List<GwtStimRegion> gwtStimRegions = retrieveStimRegions(session);

			// StimTypes
			final List<GwtStimType> gwtStimTypes = retrieveStimTypes(session);

			final OptionsLists result = new OptionsLists(
					gwtOrganizations,
					gwtSpecies,
					gwtDrugs,
					gwtStimRegions,
					gwtStimTypes);
			trx.commit();

			logger.info(
					"{}: User {} retrieved options lists.",
					new Object[] {
							m,
							user.getUsername() });
			return result;
		} catch (final BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	private List<GwtStimType> retrieveStimTypes(Session session) {
		final IStimTypeDAO stimTypeDAO = new StimTypeDAOHibernate(
				session);
		final List<StimTypeEntity> stimTypes = stimTypeDAO
				.findAllOrderBy("label");
		final List<GwtStimType> gwtStimTypes = newArrayList();
		for (final StimTypeEntity sp : stimTypes) {
			gwtStimTypes.add(stimTypeAssembler.buildDto(sp));
		}
		return gwtStimTypes;
	}

	private List<GwtStimRegion> retrieveStimRegions(Session session) {
		final IStimRegionDAO stimRegionDAO = new StimRegionDAOHibernate(
				session);
		final List<StimRegionEntity> stimRegions = stimRegionDAO
				.findAllOrderBy("label");
		final List<GwtStimRegion> gwtStimRegions = newArrayList();
		for (final StimRegionEntity sp : stimRegions) {
			gwtStimRegions.add(stimRegionAssembler.buildDto(sp));
		}
		return gwtStimRegions;
	}

	private List<GwtDrug> retrieveDrugs(Session session) {
		final IDrugDAO drugDAO = new DrugDAOHibernate(
				session);
		final List<DrugEntity> drug = drugDAO
				.findAllOrderBy("label");
		final List<GwtDrug> gwtDrugs = newArrayList();
		for (final DrugEntity sp : drug) {
			gwtDrugs.add(drugAssembler.buildDto(sp));
		}
		return gwtDrugs;
	}

	private List<GwtSpecies> retrieveSpecies(Session session) {
		final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(
				session);
		final List<SpeciesEntity> species = speciesDAO
				.findAllOrderBy("label");
		final List<GwtSpecies> gwtSpecies = newArrayList();
		for (final SpeciesEntity sp : species) {
			gwtSpecies.add(speciesAssembler.buildDto(sp));
		}
		return gwtSpecies;
	}

	private List<GwtOrganization> retrieveOrganizations(Session session) {
		final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
				session);
		final List<OrganizationEntity> organizations = organizationDAO
				.findAllOrderBy("code");
		final List<GwtOrganization> gwtOrganizations = newArrayList();
		for (final OrganizationEntity organization : organizations) {
			gwtOrganizations.add(organizationAssembler
					.buildDto(organization));
		}
		return gwtOrganizations;
	}

	@Override
	public GwtPatient savePatientWithNewStudy(
			GwtPatient patient,
			GwtHospitalAdmission admission,
			GwtEegStudy study,
			Map<String, Set<GwtTrace>> channelPrefixToTraces) throws
			BrainTrustAuthorizationException,
			DuplicateNameException,
			AmbiguousHospitalAdmissionException,
			FileNotFoundException,
			BrainTrustUserException {
		final String m = "savePatientWithNewStudy(...)";
		Session session = null;
		Transaction trx = null;
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			final User user = userService.findUserByUid(userId);
			authzOrThrowException(userId);
			checkNotNull(patient);
			checkArgument(
					patient.getAdmissions().isEmpty(),
					"Patient argument should not have any admissions. Pass in admission as argument.");
			checkNotNull(admission);
			checkArgument(
					admission.getStudies().isEmpty(),
					"HospitalAdmission argument should not have any studies. Pass in study as argument.");
			checkNotNull(study);
			checkNotNull(channelPrefixToTraces);

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final String patientLabel = patient.getLabel();
			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			Patient patientEntity = patientDAO
					.findByLabelEager(patientLabel);
			final ITsAnnotationDAO annotationDAO = new TsAnnotationDAOHibernate(
					session, HibernateUtil.getConfiguration());
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
					session);
			final IDataSnapshotService dsService = dsServiceFactory
					.newInstance(session, dataSnapshotShortCache,
							searchResultsCache);
			final PatientAssembler patientAssembler = new PatientAssembler(
					permissionDAO,
					annotationDAO,
					organizationDAO,
					dsService);
			final HospitalAdmissionAssembler admissionAssembler = new HospitalAdmissionAssembler(
					permissionDAO,
					annotationDAO,
					dsService);
			final EegStudyAssembler studyAssembler = new EegStudyAssembler(
					permissionDAO,
					annotationDAO,
					dsService);
			HospitalAdmission admissionEntity = null;
			if (patientEntity == null) {
				patientEntity = patientAssembler
						.buildEntity(patient);
				admissionEntity = admissionAssembler
						.buildEntity(admission);
				patientEntity.addAdmission(admissionEntity);
				logger.info(
						"{}: User {} created patient {} with a new hospital admission.",
						new Object[] {
								m,
								user.getUsername(),
								patientEntity.getLabel() });
			} else {
				final Set<HospitalAdmission> existingAdmissionEntities = patientEntity
						.getAdmissions();
				final Integer incomingAge = admission
						.getAgeAtAdmission().orNull();
				final Set<HospitalAdmission> matchingAdmissionEntities = newHashSet();

				for (final HospitalAdmission existingAdmissionEntity : existingAdmissionEntities) {
					final EegStudy duplicateStudy = tryFind(
							existingAdmissionEntity.getStudies(),
							compose(equalTo(study.getLabel()),
									EegStudy.getLabel)).orNull();
					if (duplicateStudy != null) {
						throw new DuplicateNameException(study.getLabel());
					}
					if (Objects.equal(
							existingAdmissionEntity.getAgeAtAdmission(),
							incomingAge)) {
						matchingAdmissionEntities
								.add(existingAdmissionEntity);
					}
				}

				if (matchingAdmissionEntities.size() == 0) {
					admissionEntity = admissionAssembler.buildEntity(admission);
					patientEntity.addAdmission(admissionEntity);
					logger.info(
							"{}: User {} added a new hospital admission to patient {}",
							new Object[] {
									m,
									user.getUsername(),
									patientEntity.getLabel() });
				} else if (matchingAdmissionEntities.size() == 1) {
					admissionEntity = getOnlyElement(matchingAdmissionEntities);
					logger.info(
							"{}: User {} attempting to add a new study to an existing hospital admission of patient {}",
							new Object[] {
									m,
									user.getUsername(),
									patientEntity.getLabel() });
				} else {
					throw new AmbiguousHospitalAdmissionException(
							patientLabel,
							incomingAge,
							matchingAdmissionEntities.size());
				}
			}
			final EegStudy studyEntity = studyAssembler.buildEntity(study);
			admissionEntity.addStudy(studyEntity);
			createContactGroupsFromFileKeys(channelPrefixToTraces, studyEntity);
			patientDAO.saveOrUpdate(patientEntity);
			session.flush();
			final GwtPatient resultPatient = patientAssembler
					.buildDto(patientEntity);
			trx.commit();
			logger.info(
					"{}: User {} created study {} with {} contact groups.",
					new Object[] {
							m,
							user.getUsername(),
							studyEntity.getLabel(),
							studyEntity.getRecording().getContactGroups()
									.size() });
			return resultPatient;
		} catch (final BrainTrustAuthorizationException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final FileNotFoundException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final DuplicateNameException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final AmbiguousHospitalAdmissionException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (BrainTrustUserException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	private <T extends IHasRecording & IHasLabel> void createContactGroupsFromFileKeys(
			Map<String, Set<GwtTrace>> channelPrefixToTraces,
			final T hasRecordingAndLabel) throws FileNotFoundException,
			BrainTrustUserException {
		final String m = "createContactGroupsFromFileKeys(...)";
		Optional<Long> firstHeaderStartUutc = Optional.absent();
		Long minHeaderStartUutc = Long.MAX_VALUE;
		Optional<Long> firstHeaderEndUutc = Optional.absent();
		Long maxHeaderEndUutc = Long.MIN_VALUE;
		final List<String> samplingRateErrors = newArrayList();
		for (final Map.Entry<String, Set<GwtTrace>> entry : channelPrefixToTraces
				.entrySet()) {
			final List<Contact> contacts = newArrayList();
			Optional<Double> samplingRateOpt = Optional.absent();
			final Set<String> samplingRateErrorsForGroup = newHashSet();
			for (final GwtTrace trace : entry.getValue()) {
				final Contact contactEntity = traceAssembler.buildEntity(
						trace);
				contacts.add(contactEntity);

				String eTag = null;
				if (usingS3) {
					try {
						eTag = AwsUtil.getETag(
								s3,
								dataBucket,
								trace.getFileKey());
					} catch (AmazonS3Exception e) {
						if (404 == e.getStatusCode()) {
							final FileNotFoundException exception =
									new FileNotFoundException(
											dataBucket,
											trace.getFileKey(),
											e);
							throw exception;
						} else {
							throw e;
						}
					}
				} else {
					eTag = "1";
				}

				final MEFChannelSpecifier channelSpecifier = new MEFChannelSpecifier(
						new MEFSnapshotSpecifier(
								sourcePath,
								""),
						trace.getFileKey(),
						"",
						0,
						eTag);
				if (!samplingRateOpt.isPresent()) {
					samplingRateOpt = Optional.of(Double.valueOf(traceServer
							.getSampleRate(channelSpecifier)));
				} else {
					final Double currentSamplingRate = Double
							.valueOf(traceServer
									.getSampleRate(channelSpecifier));
					if (!currentSamplingRate.equals(samplingRateOpt.get())) {
						samplingRateErrorsForGroup
								.add("Sampling rate ["
										+ currentSamplingRate
										+ "] of ["
										+ trace.getFileKey()
										+ "] does not match first sampling rate of contact group ["
										+ samplingRateOpt.get() + "]");
					}

				}
				final Long currentHeaderStartUutc = Long
						.valueOf(traceServer
								.getStartUutc(channelSpecifier));
				if (!firstHeaderStartUutc.isPresent()) {
					firstHeaderStartUutc = Optional
							.of(currentHeaderStartUutc);
				} else {
					if (!currentHeaderStartUutc.equals(firstHeaderStartUutc
							.get())) {
						logger.warn(
								"{}: file {} start time {} does not match first start time {}",
								new Object[] {
										m,
										trace.getFileKey(),
										currentHeaderStartUutc,
										firstHeaderStartUutc.get()
								});
					}
				}
				minHeaderStartUutc = Math.min(minHeaderStartUutc,
						currentHeaderStartUutc);

				final Long currentHeaderEndUutc = Long.valueOf(traceServer
						.getEndUutc(channelSpecifier));
				if (!firstHeaderEndUutc.isPresent()) {
					firstHeaderEndUutc = Optional.of(currentHeaderEndUutc);
				} else {
					if (!currentHeaderEndUutc.equals(firstHeaderEndUutc
							.get())) {
						logger.warn(
								"{}: file {} end time {} does not match first end time {}",
								new Object[] {
										m,
										trace.getFileKey(),
										currentHeaderEndUutc,
										firstHeaderEndUutc.get()
								});
					}
				}
				maxHeaderEndUutc = Math.max(maxHeaderEndUutc,
						currentHeaderEndUutc);

			}
			final String channelPrefix = entry.getKey();
			if (!samplingRateOpt.isPresent()) {
				samplingRateErrorsForGroup
						.add("No sampling rates found for contact group ["
								+ channelPrefix + "]");
			} else if (!samplingRateErrorsForGroup.isEmpty()) {
				samplingRateErrorsForGroup
						.add("Could not set sampling rate for contact group ["
								+ channelPrefix + "] because of earlier errors");
			} else if (samplingRateOpt.get().isNaN()
					|| samplingRateOpt.get().isInfinite()
					|| samplingRateOpt.get().compareTo(Double.valueOf(0.0)) <= 0) {
				samplingRateErrorsForGroup
						.add("Could not set sampling rate for contact group ["
								+ channelPrefix
								+ "] because of invalid sampling rate ["
								+ samplingRateOpt.get() + "]");
			} else {
				final ContactGroup contactGroup = new ContactGroup(
						channelPrefix,
						"unknown",
						samplingRateOpt.get(),
						null,
						null,
						null,
						Side.UNKNOWN,
						Location.UNKOWN,
						null,
						null,
						null);

				hasRecordingAndLabel.getRecording().addContactGroup(
						contactGroup);
				for (final Contact contact : contacts) {
					contactGroup.addContact(contact);
				}
				logger.info(
						"{}: Created contact group {} for {} with sampling rate {}",
						new Object[] {
								m,
								channelPrefix,
								hasRecordingAndLabel.getLabel(),
								samplingRateOpt });
			}
			samplingRateErrors.addAll(samplingRateErrorsForGroup);
		}
		if (!samplingRateErrors.isEmpty()) {
			final String errorMessage = Joiner.on('\n')
					.join(samplingRateErrors);
			throw new BrainTrustUserException(errorMessage);
		} else {
			hasRecordingAndLabel.getRecording().setStartTimeUutc(
					minHeaderStartUutc);
			hasRecordingAndLabel.getRecording().setEndTimeUutc(
					maxHeaderEndUutc);
			logger.info(
					"{}: Set recording times of {}: {} to {}",
					new Object[] {
							m,
							hasRecordingAndLabel.getLabel(),
							minHeaderStartUutc,
							maxHeaderEndUutc });
		}
	}

	@Override
	public List<String> retrieveMefFileKeys(String mefPath)
			throws BrainTrustAuthorizationException, BrainTrustUserException {
		final String m = "retrieveMefFileKeys(...)";
		try {
			final UserId userId = (UserId) SecurityUtils.getSubject()
					.getPrincipal();
			final User user = userService.findUserByUid(userId);
			authzOrThrowException(userId);
			if (usingS3 == false) {
				throw new BrainTrustUserException(
						"Cannot look up MEF file keys. S3 is not being used.");
			}
			final Set<String> mefFileKeys = AwsUtil.listMefFileKeys(
					s3,
					dataBucket,
					mefPath);
			final List<String> mefFileKeysSorted = newArrayList(mefFileKeys);
			Collections.sort(mefFileKeysSorted);

			logger.info(
					"{}: User {} retrieved {} MEF file keys from {}.",
					new Object[] {
							m,
							user.getUsername(),
							mefFileKeysSorted.size(),
							mefPath });
			return mefFileKeysSorted;
		} catch (final BrainTrustAuthorizationException e) {
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final BrainTrustUserException e) {
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		} catch (final RuntimeException e) {
			logger.error(m + ": Transaction rolled back after exception.", e);
			throw e;
		}

	}

}
