/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtDrugAdmin;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.dto.OptionsLists;

/**
 * The asynchronous version of {@code BrainTrustService}.
 * 
 * @author John Frommeyer
 */
public interface BrainTrustServiceAsync {

	void savePatient(GwtPatient patient, AsyncCallback<GwtPatient> callback);

	void deletePatient(GwtPatient patient, AsyncCallback<Void> callback);

	void retrievePatientByLabel(String patientLabel,
			AsyncCallback<GwtPatient> callback);

	void retrieveEtiologies(AsyncCallback<List<String>> callback);

	void retrieveSpecies(AsyncCallback<List<GwtSpecies>> callback);

	void createSpecies(GwtSpecies gwtSpecies, Collection<GwtStrain> gwtStrains,
			AsyncCallback<GwtSpecies> callback);

	void findStrains(GwtSpecies species, AsyncCallback<List<GwtStrain>> callback);

	void createStrain(GwtSpecies parent, GwtStrain strain,
			AsyncCallback<GwtStrain> callback);

	void modifySpeciesLabel(GwtSpecies species,
			AsyncCallback<GwtSpecies> callback);

	void modifyStrainLabel(GwtSpecies parent, GwtStrain strain,
			AsyncCallback<GwtStrain> callback);

	void deleteStrain(GwtSpecies parent, GwtStrain strain,
			AsyncCallback<Void> callback);

	void deleteSpecies(GwtSpecies species, AsyncCallback<Void> callback);

	void retrieveDrugs(AsyncCallback<List<GwtDrug>> callback);

	void retrieveStimTypes(AsyncCallback<List<GwtStimType>> callback);

	void retrieveStimRegions(AsyncCallback<List<GwtStimRegion>> callback);

	void modifyDrugLabel(GwtDrug drug, AsyncCallback<GwtDrug> callback);

	void createDrug(GwtDrug drug, AsyncCallback<GwtDrug> callback);

	void deleteDrug(GwtDrug drug, AsyncCallback<Void> callback);

	void createStimRegion(GwtStimRegion stimRegion,
			AsyncCallback<GwtStimRegion> callback);

	void deleteStimRegion(GwtStimRegion stimRegion, AsyncCallback<Void> callback);

	void modifyStimRegionLabel(GwtStimRegion stimRegion,
			AsyncCallback<GwtStimRegion> callback);

	void createStimType(GwtStimType stimType,
			AsyncCallback<GwtStimType> callback);

	void deleteStimType(GwtStimType stimType, AsyncCallback<Void> callback);

	void modifyStimTypeLabel(GwtStimType stimType,
			AsyncCallback<GwtStimType> callback);

	void saveAnimal(GwtAnimal animal, AsyncCallback<GwtAnimal> callback);

	void modifyAnimal(GwtAnimal animal, AsyncCallback<GwtAnimal> callback);

	void createExperiment(GwtAnimal parent, GwtExperiment experiment,
			AsyncCallback<GwtExperiment> callback);

	void retrieveAnimalByLabel(String animalLabel,
			AsyncCallback<GwtAnimalExperimentPair> callback);

	void deleteAnimal(GwtAnimal animal, AsyncCallback<Void> callback);

	void modifyExperiment(GwtAnimal parent, GwtExperiment experiment,
			AsyncCallback<GwtExperiment> callback);

	void deleteExperiment(GwtAnimal parent, GwtExperiment experiment,
			AsyncCallback<Void> callback);

	void modifyRecording(GwtExperiment parent, GwtRecording recording,
			AsyncCallback<GwtRecording> callback);

	void createTraces(GwtContactGroup gwtParent, Set<GwtTrace> gwtTrace,
			AsyncCallback<List<GwtTrace>> callback);

	void modifyTrace(GwtContactGroup gwtParent, GwtTrace gwtTrace,
			AsyncCallback<GwtTrace> callback);

	void deleteTrace(IHasGwtRecording recordingParent,
			GwtContactGroup traceParent,
			GwtTrace trace, AsyncCallback<Void> callback);

	void findTraces(GwtContactGroup electrode,
			AsyncCallback<List<GwtTrace>> callback);

	void createContactGroup(GwtRecording parent,
			GwtContactGroup gwtContactGroup,
			AsyncCallback<GwtContactGroup> callback);

	void modifyContactGroup(GwtRecording parent,
			GwtContactGroup gwtContactGroup,
			AsyncCallback<GwtContactGroup> callback);

	void deleteContactGroup(IHasGwtRecording recordingParent, GwtRecording parent,
			GwtContactGroup gwtContactGroup, AsyncCallback<Void> callback);

	void findDrugAdmins(GwtExperiment experiment,
			AsyncCallback<List<GwtDrugAdmin>> callback);

	void createDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin,
			AsyncCallback<GwtDrugAdmin> callback);

	void modifyDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin,
			AsyncCallback<GwtDrugAdmin> callback);

	void deleteDrugAdmin(GwtExperiment parent, GwtDrugAdmin drugAdmin,
			AsyncCallback<Void> callback);

	void retrieveOrganizations(AsyncCallback<List<GwtOrganization>> callback);

	void modifyOrganization(GwtOrganization modifiedOrg,
			AsyncCallback<GwtOrganization> callback);

	void deleteOrganization(GwtOrganization org,
			AsyncCallback<Void> asyncCallback);

	void createOrganization(GwtOrganization newOrg,
			AsyncCallback<GwtOrganization> asyncCallback);

	void saveAnimalWithNewExperiment(GwtAnimal animal,
			GwtExperiment experiment,
			Map<String, Set<GwtTrace>> channelPrefixToTraces,
			AsyncCallback<GwtAnimalExperimentPair> callback);

	void retrieveOptionsLists(AsyncCallback<OptionsLists> callback);

	void savePatientWithNewStudy(
			GwtPatient patient,
			GwtHospitalAdmission admission, 
			GwtEegStudy study,
			Map<String, Set<GwtTrace>> channelPrefixToTraces,
			AsyncCallback<GwtPatient> callback);

	void retrieveMefFileKeys(String mefPath,
			AsyncCallback<List<String>> callback);

}
