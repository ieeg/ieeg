/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.event;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtAnimalExperimentPair;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;

public class EditAnimalEvent extends GwtEvent<EditAnimalEventHandler> {

	public static final Type<EditAnimalEventHandler> TYPE = new Type<EditAnimalEventHandler>();

	private final GwtAnimalExperimentPair pair;
	private final List<GwtOrganization> organizationList;
	private final List<GwtSpecies> speciesList;
	private final List<GwtDrug> drugList;
	private final List<GwtStimRegion> stimRegionList;
	private final List<GwtStimType> stimTypeList;

	/**
	 * Creates a {@code CreateAnimalEvent}
	 */
	public EditAnimalEvent(
			GwtAnimalExperimentPair animalExperimentPair,
			List<GwtOrganization> organizationList,
			List<GwtSpecies> speciesList,
			List<GwtDrug> drugList,
			List<GwtStimRegion> stimRegionList, 
			List<GwtStimType> stimTypeList) {
		this.pair = animalExperimentPair;
		this.organizationList = organizationList;
		this.speciesList = speciesList;
		this.drugList = drugList;
		this.stimRegionList = stimRegionList;
		this.stimTypeList = stimTypeList;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(EditAnimalEventHandler handler) {
		handler.onEditAnimal(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditAnimalEventHandler> getAssociatedType() {
		return TYPE;
	}

	public GwtAnimalExperimentPair getAnimalExperimentPair() {
		return pair;
	}

	public List<GwtSpecies> getSpeciesList() {
		return speciesList;
	}

	public List<GwtDrug> getDrugList() {
		return drugList;
	}

	public List<GwtStimRegion> getStimRegionList() {
		return stimRegionList;
	}

	public List<GwtStimType> getStimTypeList() {
		return stimTypeList;
	}

	/**
	 * @return the organizationList
	 */
	public List<GwtOrganization> getOrganizationList() {
		return organizationList;
	}

}
