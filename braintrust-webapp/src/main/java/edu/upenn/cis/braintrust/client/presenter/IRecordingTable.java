/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;

import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

public interface IRecordingTable extends IDataEntryTable<GwtRecording> {

	void setRecording(GwtRecording recording);

	void setRecordingDirUpdater(
			FieldUpdater<GwtRecording, String> updater);

	void clearRecordingDirView(GwtRecording recording);

	void setRecordingMefDirUpdater(FieldUpdater<GwtRecording, String> updater);

	void clearRecordingMefDirView(GwtRecording recording);

	void setRecordingStartTimeUpdater(FieldUpdater<GwtRecording, String> updater);

	void clearRecordingStartTimeView(GwtRecording recording);

	void setRecordingEndTimeUpdater(FieldUpdater<GwtRecording, String> updater);

	void clearRecordingEndTimeView(GwtRecording recording);

	void setRecordingImagesFileUpdater(
			FieldUpdater<GwtRecording, String> updater);

	void setRecordingReportFileUpdater(
			FieldUpdater<GwtRecording, String> updater);

	void setRecordingCommentaryUpdater(
			FieldUpdater<GwtRecording, String> updater);

	void setRecordingRefElectrodeDescriptionUpdater(
			FieldUpdater<GwtRecording, String> updater);

}
