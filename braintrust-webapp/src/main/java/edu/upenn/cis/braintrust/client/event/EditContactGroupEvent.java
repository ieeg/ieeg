/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.event;

import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;

/**
 * Fired when an array installation is edited.
 * 
 * @author John Frommeyer
 * 
 */
public class EditContactGroupEvent extends
		GwtEvent<EditContactGroupEventHandler> {

	/** The {@code EegStudyType} of this event. */
	public static final Type<EditContactGroupEventHandler> TYPE = new Type<EditContactGroupEventHandler>();

	private final GwtEegStudy parent;
	private final GwtContactGroup contactGroup;

	/**
	 * Edit array installation event for the given study and using the given
	 * array info store.
	 * 
	 * @param parent 
	 * @param contactGroup 
	 */
	public EditContactGroupEvent(
			final GwtEegStudy parent,
			final GwtContactGroup contactGroup) {
		this.contactGroup = contactGroup;
		this.parent = parent;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(EditContactGroupEventHandler handler) {
		handler.onEditContactGroup(this);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditContactGroupEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * @return the array installation
	 */
	public GwtContactGroup getContactGroup() {
		return contactGroup;
	}

	/**
	 * @return the EEG Study
	 */
	public GwtEegStudy getStudy() {
		return parent;
	}
	
}
