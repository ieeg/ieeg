/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Optional;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class SpeciesPresenter implements IPresenter, ClickHandler,
		KeyDownHandler {
	public static interface ISpeciesView extends IView {
		HasClickHandlers getModifySpeciesLabelButton();

		IHasValueAndErrorMessage<String> getModifySpeciesLabelView();

		List<GwtStrain> getStrainList();

		IHasValueAndErrorMessage<String> getAddStrainView();

		HasClickHandlers getAddStrainButton();

		void setStrainUpdater(FieldUpdater<GwtStrain, String> strainUpdater);

		void setStrainDeleter(FieldUpdater<GwtStrain, String> strainDeleter);

		IHasErrorMessage getModifyStrainErrorMessage();

		void refresh();

		void redraw();

		/**
		 * Clears the view corresponding to {@code object}. A subsequent
		 * {@code redraw()} will restore the view from the model.
		 * 
		 * @param object
		 */
		void clearStrainViewData(GwtStrain object);
	}

	private final FieldUpdater<GwtStrain, String> strainLableUpater = new FieldUpdater<GwtStrain, String>() {

		@Override
		public void update(final int index, final GwtStrain object,
				final String value) {
			display.getModifyStrainErrorMessage().hideErrorMessage();
			if ("".equals(value)) {
				display.getModifyStrainErrorMessage()
						.setAndShowErrorMessage(
								"Please enter a name for the strain.");
				display.clearStrainViewData(object);
				display.redraw();
			} else {
				final String origLabel = object.getLabel();
				final Optional<GwtStrain> strainOpt = tryFind(
						display.getStrainList(),
						compose(equalTo(value), GwtStrain.getLabel));
				if (strainOpt.isPresent()) {
					if (!strainOpt.get().getId().equals(object.getId())) {
						display.getModifyStrainErrorMessage()
								.setAndShowErrorMessage(
										"The strain "
												+ value
												+ " already exists for this species.");
						display.clearStrainViewData(object);
						display.redraw();
					}
				} else {
					final GwtStrain modifiedStrain = new GwtStrain(value,
							object.getId(), object.getVersion(),
							species.getLabel(), species.getId());
					service.modifyStrainLabel(species, modifiedStrain,
							new AsyncCallback<GwtStrain>() {

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("An error occurred while attempting to modify strain "
											+ origLabel);
									display.clearStrainViewData(object);
									display.redraw();
								}

								@Override
								public void onSuccess(GwtStrain result) {
									display.getStrainList().set(index, result);
									Collections.sort(display.getStrainList(),
											GwtStrain.LABEL_COMPARATOR);
									display.refresh();

								}
							});
				}
			}
		}

	};

	private final FieldUpdater<GwtStrain, String> strainDeleter = new FieldUpdater<GwtStrain, String>() {

		@Override
		public void update(int index, final GwtStrain object, String value) {
			display.getModifyStrainErrorMessage().hideErrorMessage();
			if (display.getStrainList().size() == 1) {
				display.getModifyStrainErrorMessage().setAndShowErrorMessage(
						"A species must have at least one strain.");
			} else {
				service.deleteStrain(species, object,
						new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Could not delete "
										+ object.getLabel() + ": "
										+ caught.getMessage());
							}

							@Override
							public void onSuccess(Void result) {
								display.getStrainList().remove(object);
								display.refresh();
							}
						});
			}
		}
	};
	private final ISpeciesView display;
	private final BrainTrustServiceAsync service;
	private GwtSpecies species;

	public SpeciesPresenter(final ISpeciesView display,
			final BrainTrustServiceAsync service,
			GwtSpecies species, List<GwtStrain> strains) {
		this.display = display;
		this.service = service;
		this.species = species;
		bind();
		populateDisplay(strains);
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}

	private void bind() {
		display.getModifySpeciesLabelButton().addClickHandler(this);
		display.getAddStrainButton().addClickHandler(this);
		display.getAddStrainView().addKeyDownHandler(this);
		display.getModifySpeciesLabelView().addKeyDownHandler(this);
		display.setStrainUpdater(strainLableUpater);
		display.setStrainDeleter(strainDeleter);
	}

	private void populateDisplay(List<GwtStrain> initialStrains) {
		display.getModifySpeciesLabelView().setValue(species.getLabel());
		display.getStrainList().addAll(initialStrains);
	}

	@Override
	public void onKeyDown(KeyDownEvent event) {
		Object source = event.getSource();
		if (display.getModifySpeciesLabelView().isWrapping(source)) {
			display.getModifySpeciesLabelView().hideErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onModifySpeciesLabelClick();
			}
		} else if (display.getAddStrainView().isWrapping(source)) {
			display.getAddStrainView().hideErrorMessage();
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				onAddStrainClick();
			}
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		final Object source = event.getSource();
		if (source == display.getAddStrainButton()) {
			onAddStrainClick();
		} else if (source == display.getModifySpeciesLabelButton()) {
			onModifySpeciesLabelClick();
		}
	}

	private void onModifySpeciesLabelClick() {
		final String label = display.getModifySpeciesLabelView().getValue()
				.trim();
		if ("".equals(label)) {
			display.getModifySpeciesLabelView().setAndShowErrorMessage(
					"Species name cannot be empty");
			display.getModifySpeciesLabelView().setValue(species.getLabel());
		} else {
			final String oldLabel = species.getLabel();
			final GwtSpecies modifiedSpecies = new GwtSpecies(label,
					species.getId(), species.getVersion());
			service.modifySpeciesLabel(modifiedSpecies,
					new AsyncCallback<GwtSpecies>() {

						@Override
						public void onFailure(Throwable caught) {
							display.getModifySpeciesLabelView().setValue(
									species.getLabel());
							String message = caught.getMessage();
							if (caught instanceof StaleObjectException) {
								message = "This species has changed since your last save. Please reload. ("
										+ message + ")";
							} else {
								message = "Could not change species from "
										+ oldLabel + " to " + label + ": "
										+ message;
							}
							Window.alert(message);
						}

						@Override
						public void onSuccess(GwtSpecies result) {
							species = result;
							display.getModifySpeciesLabelView().setValue(
									result.getLabel());
						}
					});
		}
	}

	private void onAddStrainClick() {
		final String label = display.getAddStrainView().getValue().trim();
		if ("".equals(label)) {
			display.getAddStrainView().setAndShowErrorMessage(
					"Strain name cannot be empty");
		} else {
			final Optional<GwtStrain> strainOpt = tryFind(
					display.getStrainList(),
					compose(equalTo(label), GwtStrain.getLabel));
			if (strainOpt.isPresent()) {
				display.getAddStrainView().setAndShowErrorMessage(
						"The strain " + label
								+ " already exists for this species.");
			} else {
				final GwtStrain newStrain = new GwtStrain(label, null, null,
						species.getLabel(), species.getId());
				service.createStrain(species, newStrain,
						new AsyncCallback<GwtStrain>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("An error occurred while attempting to create strain "
										+ label);
							}

							@Override
							public void onSuccess(GwtStrain result) {
								display.getAddStrainView().setValue("");
								display.getStrainList().add(result);
								Collections.sort(display.getStrainList(),
										GwtStrain.LABEL_COMPARATOR);
								display.refresh();
							}
						});
			}
		}
	}

}
