/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;

public class ExperimentDeleter<T> implements
		FieldUpdater<GwtExperiment, T> {
	private final IExperimentTable display;
	private final BrainTrustServiceAsync service;
	private final IHasAnimal hasAnimal;

	public ExperimentDeleter(IExperimentTable display,
			BrainTrustServiceAsync service,
			IHasAnimal parent) {
		this.display = display;
		this.service = service;
		this.hasAnimal = parent;
	}

	@Override
	public void update(final int index, final GwtExperiment object, T value) {
		display.getModifyEntryErrorMessage()
				.hideErrorMessage();

		service.deleteExperiment(hasAnimal.getAnimal(), object,
				new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Could not delete "
								+ object.getLabel() + ": "
								+ caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						display.getEntryList().remove(object);
						display.refreshProvider();
					}
				});

	}
}
