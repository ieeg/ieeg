/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import java.util.Collection;

import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;

import edu.upenn.cis.braintrust.client.presenter.IHasValueAndSuggestions;

/**
 * A view for a {@code suggestBox} with an associated error message. Not the
 * best design with respect to the {@code SuggestOracle}, but good enough for
 * now.
 * 
 * @author John Frommeyer
 * 
 */
public class SuggestBoxView implements IHasValueAndSuggestions<String> {

	private final SuggestBox suggestBox;
	private final Label errorMessageLabel;

	SuggestBoxView(SuggestBox suggestBox, Label errorMessage) {
		this.suggestBox = suggestBox;
		this.errorMessageLabel = errorMessage;

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#hideErrorMessage()
	 */
	@Override
	public void hideErrorMessage() {
		ViewUtil.hideErrorMessage(errorMessageLabel);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#setAndShowErrorMessage(java.lang.String)
	 */
	@Override
	public void setAndShowErrorMessage(String errorMessage) {
		ViewUtil.setAndShowErrorMessage(errorMessageLabel, errorMessage);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.google.gwt.event.dom.client.HasKeyPressHandlers#addKeyPressHandler(com.google.gwt.event.dom.client.KeyPressHandler)
	 */
	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return suggestBox.addKeyPressHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return suggestBox.addKeyDownHandler(handler);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithSuggestionsView#setSuggestions(java.util.Collection)
	 */
	@Override
	public void setSuggestions(Collection<String> suggestions) {
		SuggestOracle oracle = this.suggestBox.getSuggestOracle();
		if (oracle instanceof MultiWordSuggestOracle) {
			MultiWordSuggestOracle multiWordOracle = (MultiWordSuggestOracle) oracle;
			multiWordOracle.clear();
			multiWordOracle.addAll(suggestions);
		} else {
			throw new IllegalStateException(
					"Can only add suggestions to a MultiWordSuggestOracle.");
		}

	}

	@Override
	public void setValue(String value) {
		suggestBox.setValue(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasValueWithErrorMessageView#getValue()
	 */
	@Override
	public String getValue() {
		return suggestBox.getValue();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.client.view.IHasHandlersView#isWrapping(java.lang.Object)
	 */
	@Override
	public boolean isWrapping(Object o) {
		return o == suggestBox;
	}

}
