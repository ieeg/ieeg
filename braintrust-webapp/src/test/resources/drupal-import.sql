DROP TABLE IF EXISTS `users_roles`;
CREATE TABLE `users_roles` (   `uid` int(10) unsigned NOT NULL default '0',   `rid` int(10) unsigned NOT NULL default '0',   PRIMARY KEY  (`uid`,`rid`));
INSERT INTO `users_roles` VALUES ('1','3');
INSERT INTO `users_roles` VALUES ('2','2');
INSERT INTO `users_roles` VALUES ('3','2');
INSERT INTO `users_roles` VALUES ('4','2');

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (   `rid` int(10) unsigned NOT NULL auto_increment,   `name` varchar(64) NOT NULL default '',   PRIMARY KEY  (`rid`),   UNIQUE KEY `name` (`name`) ); 
INSERT INTO `role` VALUES ('1','anonymous user'),('2','authenticated user'),('3','admin');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (   `uid` int(10) unsigned NOT NULL auto_increment,   `name` varchar(60) NOT NULL default '',   `pass` varchar(32) NOT NULL default '',   `mail` varchar(64) default '',   `mode` tinyint(4) NOT NULL default '0',   `sort` tinyint(4) default '0',   `threshold` tinyint(4) default '0',   `theme` varchar(255) NOT NULL default '',   `signature` varchar(255) NOT NULL default '',   `signature_format` smallint(6) NOT NULL default '0',   `created` int(11) NOT NULL default '0',   `access` int(11) NOT NULL default '0',   `login` int(11) NOT NULL default '0',   `status` tinyint(4) NOT NULL default '0',   `timezone` varchar(8) default NULL,   `language` varchar(12) NOT NULL default '',   `picture` varchar(255) NOT NULL default '',   `init` varchar(64) default '',   `data` longtext,   PRIMARY KEY  (`uid`),   UNIQUE KEY `name_uk` (`name`));
INSERT INTO `users` VALUES ('0','','','','0','0','0','','','0','0','0','0','0',null,'','','',null),
INSERT INTO `users` VALUES ('1','dbgroup-admin','not-a-real-passwrod','nobody@nowhere.com','0','0','0','','','0','1275580773','1296157589','1295968465','1','-14400','','','nobody@nowwhere.com','a:3:{s:14:\"picture_delete\";s:0:\"\";s:14:\"picture_upload\";s:0:\"\";s:13:\"form_build_id\";s:37:\"form-5cd0bb19fcf2f8f2994fa928852c59b3\";}');
INSERT INTO `users` VALUES ('2','disabled-user','not-a-real-passwrod','nobody@nowhere.com','0','0','0','','','0','1275580773','1296157589','1295968465','0','-14400','','','nobody@nowwhere.com','a:3:{s:14:\"picture_delete\";s:0:\"\";s:14:\"picture_upload\";s:0:\"\";s:13:\"form_build_id\";s:37:\"form-5cd0bb19fcf2f8f2994fa928852c59b3\";}');
INSERT INTO `users` VALUES ('3','reg user 1','not-a-real-passwrod','nobody@nowhere.com','0','0','0','','','0','1275580773','1296157589','1295968465','1','-14400','','','nobody@nowwhere.com','a:3:{s:14:\"picture_delete\";s:0:\"\";s:14:\"picture_upload\";s:0:\"\";s:13:\"form_build_id\";s:37:\"form-5cd0bb19fcf2f8f2994fa928852c59b3\";}');
INSERT INTO `users` VALUES ('4','reg user 2','not-a-real-passwrod','nobody@nowhere.com','0','0','0','','','0','1275580773','1296157589','1295968465','1','-14400','','','nobody@nowwhere.com','a:3:{s:14:\"picture_delete\";s:0:\"\";s:14:\"picture_upload\";s:0:\"\";s:13:\"form_build_id\";s:37:\"form-5cd0bb19fcf2f8f2994fa928852c59b3\";}');
