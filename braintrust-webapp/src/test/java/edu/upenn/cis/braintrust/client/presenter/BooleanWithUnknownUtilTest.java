/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import com.google.common.base.Optional;

/**
 * 
 * @author John Frommeyer
 * 
 */
public class BooleanWithUnknownUtilTest {

	@Test
	public void arrayTest() {
		final String[] expected = { "False", "True", "Unknown" };
		assertArrayEquals(expected, BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN);
	}

	@Test
	public void valueToString() {
		final Optional<Boolean> unknown = Optional.absent();
		assertEquals("Unknown",
				BooleanWithUnknownUtil.valueToString(unknown));
		assertEquals("True",
				BooleanWithUnknownUtil.valueToString(Optional.of(Boolean.TRUE)));
		assertEquals(
				"False",
				BooleanWithUnknownUtil.valueToString(Optional.of(Boolean.FALSE)));
	}

	@Test
	public void stringToValue() {
		assertEquals(null, BooleanWithUnknownUtil.stringToValue("Unknown")
				.orNull());
		assertEquals(Boolean.TRUE, BooleanWithUnknownUtil.stringToValue("True")
				.get());
		assertEquals(Boolean.FALSE,
				BooleanWithUnknownUtil.stringToValue("False")
						.get());
	}

	@Test
	public void booleanToIndex() {
		assertEquals(
				Arrays.asList(
						BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN).indexOf(
						"Unknown"),
				BooleanWithUnknownUtil.booleanToIndex(null));
		assertEquals(
				Arrays.asList(
						BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN).indexOf(
						"True"),
				BooleanWithUnknownUtil.booleanToIndex(Boolean.TRUE));
		assertEquals(
				Arrays.asList(
						BooleanWithUnknownUtil.BOOLEAN_WITH_UNKNOWN).indexOf(
						"False"),
				BooleanWithUnknownUtil.booleanToIndex(Boolean.FALSE));
	}
}
