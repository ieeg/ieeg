/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.server.assembler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.ModelDtoCompare;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.HospitalAdmissionAssembler;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Tests for {@code HospitalAdmissionAssembler}.
 * 
 * @author John Frommeyer
 * 
 */
public class HospitalAdmissionAssemblerTest {
	private HospitalAdmissionAssembler assembler;
	private IPermissionDAO permissionDAO;
	private ITsAnnotationDAO annotationDAO;
	private IDataSnapshotService dsService;
	private TstObjectFactory tstObjFac = new TstObjectFactory(false);

	@Before
	public void setup() {
		permissionDAO = mock(IPermissionDAO.class);
		annotationDAO = mock(ITsAnnotationDAO.class);
		dsService = mock(IDataSnapshotService.class);
		when(permissionDAO.findStarCoreReadPerm()).thenReturn(
				tstObjFac.getReadPerm());
		assembler = new HospitalAdmissionAssembler(
				permissionDAO,
				annotationDAO,
				dsService);
	}

	@Test
	public void testBuildDto() {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final HospitalAdmission admission = BuildDb.buildHospitalAdmission00(
				readPerm);
		setStudyIdsAndVersions(admission);
		final GwtHospitalAdmission gwtAdmission = assembler
				.buildDto(admission);
		ModelDtoCompare.compareAdmissions(admission, gwtAdmission);
	}

	@Test
	public void testBuildEntity() {
		// A new admission will have no id, version, or studies.
		final Set<GwtEegStudy> emptyStudies = Collections.emptySet();
		final GwtHospitalAdmission gwtAdmission = BuildDto
				.buildGwtAdmission(emptyStudies);
		gwtAdmission.setId(null);
		gwtAdmission.setVersion(null);
		final HospitalAdmission study = assembler.buildEntity(gwtAdmission);
		ModelDtoCompare.compareAdmissions(study, gwtAdmission);
		assertNull(study.getId());
		assertNull(study.getVersion());
	}

	@Test
	public void testModifyEntitySameStudies() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();
		final HospitalAdmission admission = BuildDb.buildHospitalAdmission00(
				readPerm);
		final GwtHospitalAdmission gwtAdmission = BuildDto
				.buildGwtAdmissionWithMultipleStudies();
		PrepareForModifyTestUtil.prepareAdmissionForModifySameStudies(
				admission, gwtAdmission);
		assembler.modifyEntity(admission, gwtAdmission);
		ModelDtoCompare.compareAdmissions(admission, gwtAdmission);
	}

	@Test
	public void testModifyEntityAddStudy() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final HospitalAdmission admission = BuildDb.buildHospitalAdmission00(
				readPerm);
		final GwtHospitalAdmission gwtAdmission = BuildDto
				.buildGwtAdmissionWithMultipleStudies();
		PrepareForModifyTestUtil.prepareAdmissionForModifyAddStudy(admission,
				gwtAdmission);
		final int origNoStudies = admission.getStudies().size();
		assembler.modifyEntity(admission, gwtAdmission);
		assertEquals(origNoStudies + 1, admission.getStudies().size());
		ModelDtoCompare.compareAdmissions(admission, gwtAdmission);
	}

	@Test
	public void testModifyEntityRemoveStudy() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final HospitalAdmission admission = BuildDb.buildHospitalAdmission00(
				readPerm);
		final GwtHospitalAdmission gwtAdmission = BuildDto
				.buildGwtAdmissionWithMultipleStudies();
		PrepareForModifyTestUtil.prepareAdmissionForModifyRemoveStudy(
				admission,
				gwtAdmission);
		for (EegStudy study : admission.getStudies()) {
			when(dsService.isRecordingModifiable(study)).thenReturn(true);
		}
		final int origNoStudies = admission.getStudies().size();
		assembler.modifyEntity(admission, gwtAdmission);
		assertEquals(origNoStudies - 1, admission.getStudies().size());
		ModelDtoCompare.compareAdmissions(admission, gwtAdmission);
	}

	@Test
	public void testModifyEntityIdMismatch() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final HospitalAdmission a = BuildDb.buildHospitalAdmission00(
				readPerm);
		final GwtHospitalAdmission gwtA = BuildDto
				.buildGwtAdmissionWithSingleStudy();
		a.setId(Long.valueOf(gwtA.getId().longValue() + 1));
		a.setVersion(gwtA.getVersion());
		final Long origId = a.getId();
		final Integer origVer = a.getVersion();
		final Integer origAge = a.getAgeAtAdmission();

		boolean exception = false;
		try {
			assembler.modifyEntity(a, gwtA);
		} catch (IllegalArgumentException iae) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, a.getId());
		assertEquals(origVer, a.getVersion());
		assertEquals(origAge, a.getAgeAtAdmission());
	}

	@Test
	public void testModifyEntityVersionMismatch()
			throws BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final HospitalAdmission a = BuildDb.buildHospitalAdmission00(
				readPerm);
		final GwtHospitalAdmission gwtA = BuildDto
				.buildGwtAdmissionWithSingleStudy();
		a.setId(gwtA.getId());
		a.setVersion(Integer.valueOf(gwtA.getVersion().intValue() + 1));
		final Long origId = a.getId();
		final Integer origVer = a.getVersion();
		final Integer origAge = a.getAgeAtAdmission();

		boolean exception = false;
		try {
			assembler.modifyEntity(a, gwtA);
		} catch (StaleObjectException soe) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, a.getId());
		assertEquals(origVer, a.getVersion());
		assertEquals(origAge, a.getAgeAtAdmission());
	}

	private static void setStudyIdsAndVersions(final HospitalAdmission admission) {
		long studyId = 0;
		int studyVersion = 0;
		for (final EegStudy study : admission.getStudies()) {
			study.setId(Long.valueOf(studyId++));
			study.setVersion(Integer.valueOf(studyVersion++));
			setElectrodeIdsAndVersions(study);
		}
	}

	private static void setElectrodeIdsAndVersions(final EegStudy study) {
		long electrodeId = 0;
		int electrodeVersion = 0;
		for (final ContactGroup electrode : study.getRecording().getContactGroups()) {
			electrode.setId(Long.valueOf(electrodeId++));
			electrode.setVersion(Integer.valueOf(electrodeVersion++));
		}
	}

}
