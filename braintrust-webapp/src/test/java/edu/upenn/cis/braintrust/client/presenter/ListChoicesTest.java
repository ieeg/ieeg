/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static edu.upenn.cis.braintrust.client.presenter.ListChoices.IMAGE_TYPE_CHOICES;
import static edu.upenn.cis.braintrust.client.presenter.ListChoices.SZ_TYPE_CHOICES;
import static edu.upenn.cis.braintrust.client.presenter.ListChoices.enums2Indices;
import static edu.upenn.cis.braintrust.client.presenter.ListChoices.indices2Enums;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.EnumSet;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.SeizureType;

public class ListChoicesTest {
	@Test
	public void indices2EnumsTest() {

		// These positions are defined by teh array we pass into indices2Enums,
		// in this case ListChoices.SZ_TYPE_CHOICES
		final Set<Integer> indices = ImmutableSet.of(0, 3, 9);

		final Set<SeizureType> selectedSzTypes = indices2Enums(indices,
				SZ_TYPE_CHOICES,
				SeizureType.fromString);
		assertEquals(indices.size(), selectedSzTypes.size());
		assertTrue(selectedSzTypes.contains(SeizureType.ABSENCE));
		assertTrue(selectedSzTypes.contains(SeizureType.MYOCLONIC));
		assertTrue(selectedSzTypes.contains(SeizureType.NON_EPILEPTIC));
	}

	@Test
	public void enums2IndicesTest() {
		final Set<ImageType> imageTypes = EnumSet.allOf(ImageType.class);
		final Set<Integer> indices = enums2Indices(imageTypes,
				IMAGE_TYPE_CHOICES, ImageType.fromString);
		assertEquals(imageTypes.size(), indices.size());

		for (int i = 0; i < imageTypes.size(); i++) {
			assertTrue(indices.contains(i));
		}
	}
}
