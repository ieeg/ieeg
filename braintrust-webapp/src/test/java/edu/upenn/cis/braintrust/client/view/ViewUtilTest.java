/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.view;

import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.client.view.ViewUtil.set2Range;
import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.Set;

import org.junit.Test;

/**
 * DOCUMENT ME
 * 
 * @author John Frommeyer
 * 
 */
public class ViewUtilTest {

	@Test
	public void removeLeadingAndTrailingSlashesAndBackSlashes() {
		String dir = "/Xyz_IEEG_001/IC_001/";
		String actual = ViewUtil
				.removeLeadingAndTrailingSlashesAndBackSlashes(dir);
		assertEquals("Xyz_IEEG_001/IC_001", actual);

		dir = "/Xyz_IEEG_001/";
		actual = ViewUtil.removeLeadingAndTrailingSlashesAndBackSlashes(dir);
		assertEquals("Xyz_IEEG_001", actual);

		dir = "\\/Xyz_IEEG_001//IC_001/\\";
		actual = ViewUtil.removeLeadingAndTrailingSlashesAndBackSlashes(dir);
		assertEquals("Xyz_IEEG_001//IC_001", actual);

		dir = "\\////\\";
		actual = ViewUtil.removeLeadingAndTrailingSlashesAndBackSlashes(dir);
		assertEquals("", actual);

		dir = "/";
		actual = ViewUtil.removeLeadingAndTrailingSlashesAndBackSlashes(dir);
		assertEquals("", actual);

	}

	@Test
	public void emptyRange() {
		final Set<Integer> set = Collections.emptySet();
		assertEquals("", set2Range(set));
	}

	@Test
	public void fullRange() {
		final Set<Integer> set = newHashSet(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		assertEquals("1-10", set2Range(set));
	}

	@Test
	public void singleRange() {
		final Set<Integer> set = newHashSet(5);
		assertEquals("5", set2Range(set));
	}

	@Test
	public void doubleConsecutiveRange() {
		final Set<Integer> set = newHashSet(4, 5);
		assertEquals("4-5", set2Range(set));
	}

	@Test
	public void miscRange() {
		final Set<Integer> set = newHashSet(1, 2, 4, 5, 6, 9, 10);
		assertEquals("1-2, 4-6, 9-10", set2Range(set));

		final Set<Integer> set2 = newHashSet(1, 2, 4, 5, 6, 9, 13);
		assertEquals("1-2, 4-6, 9, 13", set2Range(set2));
		
		final Set<Integer> set3 = newHashSet(1, 4, 5, 6, 9, 13);
		assertEquals("1, 4-6, 9, 13", set2Range(set3));

	}

	@Test
	public void doubleNonConsecutiveRange() {
		final Set<Integer> set = newHashSet(4, 10);
		assertEquals("4, 10", set2Range(set));

		final Set<Integer> set2 = newHashSet(4, 6);
		assertEquals("4, 6", set2Range(set2));

	}
	
	@Test
	public void noRanges() {
		final Set<Integer> set = newHashSet(1, 3, 6, 8, 19, 100);
		assertEquals("1, 3, 6, 8, 19, 100", set2Range(set));
	}
}
