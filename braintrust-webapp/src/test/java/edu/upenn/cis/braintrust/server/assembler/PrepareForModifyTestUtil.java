/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.server.assembler;

import static com.google.common.collect.Iterables.getFirst;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;

/**
 * Utility methods that prepare objects before they are used to test
 * modifications.
 * 
 * @author John Frommeyer
 * 
 */
public class PrepareForModifyTestUtil {
	private PrepareForModifyTestUtil() {
		throw new AssertionError("can't instantiate a PrepareForModifyTestUtil");
	}

	static void prepareStudyForModifyAddElectrode(final GwtEegStudy gwtStudy,
			final EegStudy study) {
		prepareStudyForModifySameElectrodes(gwtStudy,
				study);
		final GwtContactGroup gwtE = BuildDto.buildGwtContactGroup();
		gwtE.setId(null);
		gwtE.setVersion(null);
		gwtStudy.getRecording().addContactGroup(gwtE);
	}

	static void prepareStudyForModifyRemoveElectrode(
			final GwtEegStudy gwtStudy,
			final EegStudy study) {
		prepareStudyForModifySameElectrodes(gwtStudy, study);
		final GwtContactGroup removed = getFirst(gwtStudy.getRecording()
				.getContactGroups(), null);
		assertNotNull(removed);
		removed.setDeleted(Boolean.TRUE);
	}

	static void prepareStudyForModifySameElectrodes(final GwtEegStudy gwtStudy,
			final EegStudy study) {
		study.setId(gwtStudy.getId());
		study.setVersion(gwtStudy.getVersion());
		final Recording recording = study.getRecording();
		final GwtRecording gwtRecording = gwtStudy.getRecording();
		recording.setId(gwtRecording.getId());
		recording.setVersion(gwtRecording.getVersion());

		final Iterator<ContactGroup> dbIter = recording.getContactGroups().iterator();
		final Iterator<GwtContactGroup> gwtIter = gwtRecording.getContactGroups()
				.iterator();
		while (dbIter.hasNext() && gwtIter.hasNext()) {
			final ContactGroup dbE = dbIter.next();
			final GwtContactGroup gwtE = gwtIter.next();
			dbE.setId(gwtE.getId());
			dbE.setVersion(gwtE.getVersion());
		}
		while (dbIter.hasNext()) {
			final ContactGroup dbE = dbIter.next();
			dbE.setParent(null);
			dbIter.remove();
		}
		while (gwtIter.hasNext()) {
			final GwtContactGroup gwtE = gwtIter.next();
			gwtE.setParent(null);
			gwtIter.remove();
		}
		assertTrue(recording.getContactGroups().size() == gwtRecording.getContactGroups()
				.size() && !recording.getContactGroups().isEmpty());
	}

	static void prepareAdmissionForModifySameStudies(
			final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission) {
		admission.setId(gwtAdmission.getId());
		admission.setVersion(gwtAdmission.getVersion());

		final Iterator<EegStudy> studyIter = admission.getStudies().iterator();
		final Iterator<GwtEegStudy> gwtStudyIter = gwtAdmission.getStudies()
				.iterator();
		while (studyIter.hasNext() && gwtStudyIter.hasNext()) {
			final EegStudy study = studyIter.next();
			final GwtEegStudy gwtStudy = gwtStudyIter.next();
			study.setId(gwtStudy.getId());
			study.setVersion(gwtStudy.getVersion());
			prepareStudyForModifySameElectrodes(gwtStudy, study);
		}
		while (studyIter.hasNext()) {
			final EegStudy study = studyIter.next();
			study.setParent(null);
			studyIter.remove();
		}
		while (gwtStudyIter.hasNext()) {
			final GwtEegStudy gwtStudy = gwtStudyIter.next();
			gwtStudy.setParent(null);
			gwtStudyIter.remove();
		}
		assertTrue(admission.getStudies().size() == gwtAdmission.getStudies()
				.size() && !admission.getStudies().isEmpty());
	}

	static void prepareAdmissionForModifyAddStudy(
			final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission) {
		prepareAdmissionForModifySameStudies(admission, gwtAdmission);
		Set<GwtContactGroup> noElectrodes = Collections.emptySet();
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy(noElectrodes);
		gwtStudy.setId(null);
		gwtStudy.setVersion(null);
		final GwtRecording gwtRecording = gwtStudy.getRecording();
		gwtRecording.setId(null);
		gwtRecording.setVersion(null);
		gwtAdmission.addStudy(gwtStudy);
	}

	static void prepareAdmissionForModifyRemoveStudy(
			final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission) {
		prepareAdmissionForModifySameStudies(admission, gwtAdmission);
		final GwtEegStudy removed = getFirst(gwtAdmission.getStudies(), null);
		assertNotNull(removed);
		removed.setDeleted(Boolean.TRUE);

	}

	static void preparePatientForModifySameAdmissions(final Patient patient,
			final GwtPatient gwtPatient) {
		patient.setId(gwtPatient.getId());
		patient.setVersion(gwtPatient.getVersion());
		final Iterator<HospitalAdmission> admissionItr = patient
				.getAdmissions().iterator();
		final Iterator<GwtHospitalAdmission> gwtAdmissionItr = gwtPatient
				.getAdmissions().iterator();
		while (admissionItr.hasNext() && gwtAdmissionItr.hasNext()) {
			final HospitalAdmission admission = admissionItr.next();
			final GwtHospitalAdmission gwtAdmission = gwtAdmissionItr.next();
			admission.setId(gwtAdmission.getId());
			admission.setVersion(gwtAdmission.getVersion());
			prepareAdmissionForModifySameStudies(admission, gwtAdmission);
		}
		while (admissionItr.hasNext()) {
			final HospitalAdmission admission = admissionItr.next();
			admission.setParent(null);
			admissionItr.remove();
		}
		while (gwtAdmissionItr.hasNext()) {
			final GwtHospitalAdmission gwtAdmission = gwtAdmissionItr.next();
			gwtAdmission.setParent(null);
			gwtAdmissionItr.remove();
		}
		assertTrue(patient.getAdmissions().size() == gwtPatient.getAdmissions()
				.size() && !patient.getAdmissions().isEmpty());
	}

	static void preparePatientForModifyAddAdmission(final Patient patient,
			final GwtPatient gwtPatient) {
		preparePatientForModifySameAdmissions(patient, gwtPatient);
		final Set<GwtEegStudy> noStudies = Collections.emptySet();
		final GwtHospitalAdmission newAdmission = BuildDto
				.buildGwtAdmission(noStudies);
		newAdmission.setId(null);
		newAdmission.setVersion(null);
		gwtPatient.addAdmission(newAdmission);
	}

	static void preparePatientForModifyRemoveAdmission(final Patient patient,
			final GwtPatient gwtPatient) {
		preparePatientForModifySameAdmissions(patient, gwtPatient);
		final GwtHospitalAdmission removed = getFirst(
				gwtPatient.getAdmissions(), null);
		assertNotNull(removed);
		removed.setDeleted(Boolean.TRUE);
	}
}
