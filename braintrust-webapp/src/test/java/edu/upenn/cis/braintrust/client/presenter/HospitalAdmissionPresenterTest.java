/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.HospitalAdmissionPresenter.IHospitalAdmissionView;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;

/**
 * Tests for {@code StudyPresenter}.
 * 
 * @author John Frommeyer
 * 
 */
public class HospitalAdmissionPresenterTest {

	// Presenter Dependencies
	@Mock
	private HandlerManager eventBus;
	@Mock
	private BrainTrustServiceAsync rpcService;
	@Mock
	private IHospitalAdmissionView display;

	// Sub-views of IHospitalAdmissionView
	@Mock
	private IHasEnabledAndClickHandlers saveButton;
	@Mock
	private IHasEnabledAndClickHandlers cancelButton;
	@Mock
	private IHasEnabledAndClickHandlers addStudyButton;
	@Mock
	private IHasValueAndErrorMessage<Integer> ageAtAdmissionView;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@Ignore("GWT-2.5.1")
	public void testRemoveElectrode() {
		final GwtPatient patient = BuildDto.buildGwtPatientWithSingleAdmission();
		final GwtHospitalAdmission admission = getOnlyElement(patient
				.getAdmissions());
		Map<Long, HasClickHandlers> idToRemoveButton = newHashMap();
		initViewFromAdmission(admission,
				idToRemoveButton);

		new HospitalAdmissionPresenter(eventBus, display,
				rpcService, admission);

		final GwtEegStudy study = getFirst(admission.getStudies(), null);
		final HasClickHandlers removeButton = idToRemoveButton.get(study
				.getId());
		ArgumentCaptor<ClickHandler> handlerArgCaptor = ArgumentCaptor
				.forClass(ClickHandler.class);
		verify(removeButton).addClickHandler(handlerArgCaptor.capture());
		final ClickHandler removeClickHandler = handlerArgCaptor.getValue();
		final ClickEvent clickEvent = mock(ClickEvent.class);
		when(clickEvent.getSource()).thenReturn(removeButton);
		removeClickHandler.onClick(clickEvent);

		assertTrue(admission.getStudies().contains(study));
		assertTrue(study.isDeleted());
		verify(display).removeStudy(study.getId());
	}

	private void initViewFromAdmission(
			final GwtHospitalAdmission admission,
			Map<Long, HasClickHandlers> idToRemoveButton) {

		// Save button
		when(display.getSaveButton()).thenReturn(saveButton);

		// Cancel button
		when(display.getCancelButton()).thenReturn(cancelButton);

		// Add Study Button
		when(display.getAddStudyButton()).thenReturn(addStudyButton);

		// Age at Admission View
		when(display.getAgeAtAdmissionView()).thenReturn(ageAtAdmissionView);

		// Edit and remove study buttons
		for (final GwtEegStudy study : admission.getStudies()) {
			final HasClickHandlers editStudyButton = mock(HasClickHandlers.class);
			final HasClickHandlers removeStudyButton = mock(HasClickHandlers.class);
			final EditRemoveButtonPair buttonPair = new EditRemoveButtonPair(
					editStudyButton, removeStudyButton);
			final String description = study.getLabel();
			when(display.addStudy(study.getId(), description, study.isModifiable()))
					.thenReturn(
							buttonPair);
			if (idToRemoveButton != null) {
				idToRemoveButton.put(study.getId(), removeStudyButton);
			}
		}
	}
}
