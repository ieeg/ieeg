/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.server;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.shiro.subject.Subject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.ModelDtoCompare;
import edu.upenn.cis.braintrust.client.BrainTrustService;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IContactDAO;
import edu.upenn.cis.braintrust.dao.metadata.IContactGroupDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStrainDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.OrganizationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.SpeciesDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimRegionDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimTypeDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StrainDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.TimeSeriesDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServiceFactory;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.AnimalAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.ContactGroupAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.ExperimentAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.RecordingAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.SpeciesAssembler;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustAuthorizationException;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.OrganizationsForTests;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.braintrust.thirdparty.testhelper.AbstractShiroTest;
import edu.upenn.cis.db.mefview.server.TraceServer;

//@Ignore
public class BrainTrustServerImplTest extends AbstractShiroTest {

	private static final TstObjectFactory objFac = new TstObjectFactory(false);
	private static UserEntity creator = objFac.newUserEntity();

	private PermissionEntity readPerm;

	@Before
	public void testSetUp() throws Throwable {
		IvProps.setIvProps(Collections.<String, String> emptyMap());

		final TestDbCleaner cleaner = new TestDbCleaner(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration());
		cleaner.deleteEverything();
		Transaction trx = null;
		Session s = null;
		try {

			s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();

			readPerm = new PermissionDAOHibernate(s).findStarCoreReadPerm();

			trx.commit();

		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
		} finally {
			PersistenceUtil.close(s);
		}

		IUserService userService = Mockito.mock(IUserService.class);
		UserServiceFactory.setUserService(userService);
	}

	private BrainTrustService newServiceWithAdminUser() {
		final User adminEnabled = objFac.newUserAdminEnabled();
		final Subject subject = createSubjectWithId(adminEnabled
				.getUserId());
		setSubject(subject);
		final IUserService userService = mock(IUserService.class);
		when(userService.findUserByUid(adminEnabled.getUserId()))
				.thenReturn(adminEnabled);
		final TraceServer traceServer = mock(TraceServer.class);
		final AmazonS3 s3 = mock(AmazonS3.class);
		final BrainTrustService btService = new BrainTrustServiceImpl(
				new DataSnapshotServiceFactory(),
				userService,
				traceServer,
				s3,
				false,
				"",
				"");
		return btService;
	}

	private BrainTrustService newServiceWithNonDataEntererUser() {
		final User regularEnabled = objFac.newUserRegularEnabled();
		final Subject subject = createSubjectWithId(regularEnabled
				.getUserId());
		setSubject(subject);
		final IUserService userService = mock(IUserService.class);
		when(userService.findUserByUid(regularEnabled.getUserId()))
				.thenReturn(regularEnabled);
		final TraceServer traceServer = mock(TraceServer.class);
		final AmazonS3 s3 = mock(AmazonS3.class);
		final BrainTrustService btService = new BrainTrustServiceImpl(
				new DataSnapshotServiceFactory(),
				userService,
				traceServer,
				s3,
				false,
				"",
				"");
		return btService;
	}

	@Test
	public void retrieveEtiologies() throws Throwable {
		final List<Patient> patients = newArrayList();
		final String etiology1 = "Focal Cortical Dysplasia";
		final String etiology2 = "Meningitis";
		final List<String> etiologies = newArrayList(etiology1, etiology2,
				etiology1);
		try {
			Session sess = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(sess);
			sess.beginTransaction();

			IPermissionDAO permDAO = new PermissionDAOHibernate(sess);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			final Iterator<String> etItr = etiologies.iterator();
			for (final Patient patient : patients) {
				if (etItr.hasNext()) {
					patient.setEtiology(etItr.next());
				}
				HibernateUtil.getSessionFactory().getCurrentSession()
						.save(patient);
			}

			HibernateUtil.getSessionFactory().getCurrentSession()
					.getTransaction().commit();

		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				// logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			PersistenceUtil.close(
					ManagedSessionContext.unbind(
							HibernateUtil.getSessionFactory()));
		}

		final BrainTrustService btService = newServiceWithAdminUser();
		final List<String> actual = btService.retrieveEtiologies();
		assertNotNull(actual);
		assertEquals(2, actual.size());
		assertTrue(actual.contains(etiology1));
		assertTrue(actual.contains(etiology2));
	}

	@Test
	public void retrievePatientByUsersId() throws Throwable {
		final BrainTrustService btService = newServiceWithAdminUser();
		final String userSuppliedId = "0-A";

		final GwtPatient nonexistentPatient = btService
				.retrievePatientByLabel(userSuppliedId);
		// Nothing in DB, so should just get null.
		Assert.assertNull(nonexistentPatient);

		Session setupSession = null;
		Transaction setupTrx = null;

		try {
			setupSession = HibernateUtil.getSessionFactory().openSession();
			setupTrx = setupSession.beginTransaction();
			List<Patient> patients = newArrayList();
			IPermissionDAO permDAO = new PermissionDAOHibernate(setupSession);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			final List<Dataset> datasets = Collections.emptyList();
			final List<TsAnnotationEntity> tsAnnotations = Collections
					.emptyList();
			BrainTrustDb btDb = new BrainTrustDb(setupSession);
			btDb.saveEverything(patients, null, tsAnnotations,
					singleton(creator),
					datasets, null);
			setupTrx.commit();
		} catch (final Throwable t) {
			try {
				if (setupTrx.isActive()) {
					setupTrx.rollback();
				}
			} catch (final Throwable rbEx) {
				// logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (setupSession != null) {
				setupSession.close();
			}
		}

		final GwtPatient retrievedPatient = btService
				.retrievePatientByLabel(userSuppliedId);
		assertNotNull(retrievedPatient);
		assertEquals(userSuppliedId, retrievedPatient.getLabel());
		assertNotNull(retrievedPatient.getId());
		assertNotNull(retrievedPatient.getVersion());

	}

	@Test
	public void savePatientSteps() throws Throwable {
		// The top-down, multi-step way the patient is built up in this test
		// corresponds to how the current UI forces a user to create a patient.

		// Need to setup ref electrode infos and electrode infos in the db to
		// use.
		Session setupSession = null;
		Transaction setupTrx = null;
		try {
			setupSession = HibernateUtil.getSessionFactory().openSession();
			setupTrx = setupSession.beginTransaction();

			final BrainTrustDb btDb = new BrainTrustDb(setupSession);

			Collection<? extends Patient> patients = Collections.emptySet();
			Collection<? extends TsAnnotationEntity> tsAnnotations = Collections
					.emptySet();
			Collection<? extends Dataset> datasets = Collections.emptySet();

			btDb.saveEverything(patients,
					null,
					tsAnnotations,
					singleton(creator),
					datasets,
					null);
			setupTrx.commit();
		} catch (Throwable t) {
			try {
				if (setupTrx != null && setupTrx.isActive()) {
					setupTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (setupSession != null) {
				setupSession.close();
			}
		}

		final BrainTrustService btService = newServiceWithAdminUser();

		// Create a new Patient
		// A new patient has no id, version, or admissions.
		final Set<GwtHospitalAdmission> noAdmissions = Collections.emptySet();
		final GwtOrganization organization = new GwtOrganization(
				OrganizationsForTests.JONES.getName(),
				OrganizationsForTests.JONES.getCode(),
				OrganizationsForTests.JONES.getId(),
				OrganizationsForTests.JONES.getVersion());
		final GwtPatient gwtPatient = BuildDto.buildGwtPatient(
				organization,
				noAdmissions);
		gwtPatient.setId(null);
		gwtPatient.setVersion(null);

		GwtPatient savedPatient = btService.savePatient(gwtPatient);
		assertNotNull(savedPatient.getId());
		assertEquals(Integer.valueOf(0), savedPatient.getVersion());

		// Add a new admission to saved patient.
		// A new admission has no id, version, or studies
		Set<GwtEegStudy> noStudies = Collections.emptySet();
		final GwtHospitalAdmission gwtAdmission = BuildDto
				.buildGwtAdmission(noStudies);
		gwtAdmission.setId(null);
		gwtAdmission.setVersion(null);
		savedPatient.addAdmission(gwtAdmission);

		savedPatient = btService.savePatient(savedPatient);
		final GwtHospitalAdmission savedNewAdmission = getOnlyElement(savedPatient
				.getAdmissions());

		// used to be assertEquals(Integer.valueOf(1),
		// savedPatient.getVersion());
		// hibernate 3->4
		assertEquals(Integer.valueOf(0), savedPatient.getVersion());
		assertNotNull(savedNewAdmission.getId());
		assertEquals(Integer.valueOf(0), savedNewAdmission.getVersion());

		// Add a new study to saved patient.admission.
		// A new Study has no id, version, ref electrodes, or electrodes
		final Set<GwtContactGroup> noElectrodes = Collections.emptySet();
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy(noElectrodes);
		gwtStudy.setId(null);
		gwtStudy.setVersion(null);
		final GwtRecording gwtRecording = gwtStudy.getRecording();
		gwtRecording.setId(null);
		gwtRecording.setVersion(null);
		savedNewAdmission.addStudy(gwtStudy);

		savedPatient = btService.savePatient(savedPatient);
		final GwtHospitalAdmission savedAdmission = getOnlyElement(
				savedPatient.getAdmissions());
		final GwtEegStudy savedNewStudy = getOnlyElement(savedAdmission
				.getStudies());
		assertNotNull(savedPatient.getId());
		assertEquals(Integer.valueOf(0), savedPatient.getVersion());
		assertNotNull(savedAdmission.getId());

		// used to be assertEquals(Integer.valueOf(1),
		// savedAdmission.getVersion());
		// hibernate 3->4
		assertEquals(Integer.valueOf(0), savedAdmission.getVersion());
		assertNotNull(savedNewStudy.getId());
		assertEquals(Integer.valueOf(0), savedNewStudy.getVersion());

		// Add a new electrode to saved patient.admission.study
		// A new Electrode has no id, version and needs an electrode info which
		// already exists in db.
		final GwtContactGroup gwtElectrode = BuildDto.buildGwtContactGroup();
		gwtElectrode.setId(null);
		gwtElectrode.setVersion(null);
		savedNewStudy.getRecording().addContactGroup(gwtElectrode);

		savedPatient = btService.savePatient(savedPatient);
		final GwtHospitalAdmission admission = getOnlyElement(
				savedPatient.getAdmissions());
		final GwtEegStudy study = getOnlyElement(
				admission.getStudies());
		final GwtContactGroup savedElectrode = getOnlyElement(
				study.getRecording().getContactGroups());
		assertNotNull(savedPatient.getId());
		assertEquals(Integer.valueOf(0), savedPatient.getVersion());
		assertNotNull(admission.getId());
		assertEquals(Integer.valueOf(0), admission.getVersion());
		assertNotNull(study.getId());
		assertEquals(Integer.valueOf(0), study.getVersion());
		assertNotNull(savedElectrode.getId());
		assertEquals(Integer.valueOf(0), savedElectrode.getVersion());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					savedPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, savedPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}

	}

	@Test
	public void savePatientModify() throws Throwable {
		// Need to setup objects in the db to
		// use.
		Patient origPatient = createSinglePatientDb();

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		final String newPatientLabel = "A new label";
		gwtPatient.setLabel(newPatientLabel);

		final GwtHospitalAdmission gwtAdmission = getOnlyElement(gwtPatient
				.getAdmissions());
		assertTrue(gwtAdmission.getAgeAtAdmission().isPresent());
		final Integer newAdmissionAge = Integer.valueOf(gwtAdmission
				.getAgeAtAdmission().get().intValue() + 10);
		gwtAdmission.setAgeAtAdmission(newAdmissionAge);

		final GwtEegStudy gwtStudy = getOnlyElement(gwtAdmission.getStudies());

		final String newStudyDir = "a/new/dir";
		gwtStudy.getRecording().setDir(newStudyDir);

		final GwtContactGroup gwtElectrode = find(gwtStudy.getRecording()
				.getContactGroups(),
				compose(equalTo("RT"), GwtContactGroup.getChannelPrefix));
		final String newPrefix = "NEWRT";
		gwtElectrode.setChannelPrefix(newPrefix);

		// Check results.
		final GwtPatient gwtResultPatient = btService.savePatient(gwtPatient);
		assertEquals(newPatientLabel, gwtResultPatient.getLabel());

		final GwtHospitalAdmission gwtResultAdmission = getOnlyElement(gwtResultPatient
				.getAdmissions());
		assertEquals(newAdmissionAge, gwtResultAdmission.getAgeAtAdmission()
				.orNull());

		final GwtEegStudy gwtResultStudy = getOnlyElement(gwtResultAdmission
				.getStudies());
		assertEquals(newStudyDir, gwtResultStudy.getRecording().getDir());

		final GwtContactGroup gwtResultElectrode = find(
				gwtResultStudy.getRecording().getContactGroups(),
				compose(equalTo(newPrefix), GwtContactGroup.getChannelPrefix),
				null);
		assertNotNull(gwtResultElectrode);

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}

	}

	@Test
	public void savePatientRemoveAnalyzedElectrode() throws Throwable {
		// Need to setup objects in the db to
		// use.
		List<Patient> patients = createMultiPatientDb();
		Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the electrode.
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient
						.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(40))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		final GwtEegStudy gwtStudy = getOnlyElement(gwtAdmission.getStudies());
		final GwtContactGroup gwtElectrode = find(gwtStudy.getRecording()
				.getContactGroups(),
				compose(equalTo("ANALYZED"), GwtContactGroup.getChannelPrefix),
				null);
		gwtElectrode.setDeleted(Boolean.TRUE);
		final Long removedElectrodeId = gwtElectrode.getId();

		// Check results.
		boolean exception = false;
		try {
			btService
					.savePatient(gwtPatient);
		} catch (BrainTrustUserException e) {
			exception = true;
		}
		assertTrue(exception);
		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(origPatient.getLabel());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final ContactGroup removedElectrode = (ContactGroup) verifySession
					.get(
							ContactGroup.class, removedElectrodeId);
			assertNotNull(removedElectrode);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}

	}

	@Test
	public void savePatientRemoveNonAnalyzedElectrode() throws Throwable {
		// Need to setup objects in the db to
		// use.
		List<Patient> patients = createMultiPatientDb();
		Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the electrode.
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient
						.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(40))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		final GwtEegStudy gwtStudy = getOnlyElement(gwtAdmission.getStudies());
		final GwtContactGroup gwtElectrode = find(
				gwtStudy.getRecording().getContactGroups(),
				compose(equalTo("NOTANALYZED"),
						GwtContactGroup.getChannelPrefix),
				null);
		gwtElectrode.setDeleted(Boolean.TRUE);
		final Long removedElectrodeId = gwtElectrode.getId();

		// Check results.
		boolean exception = false;
		try {
			btService
					.savePatient(gwtPatient);
		} catch (BrainTrustUserException e) {
			exception = true;
		}
		assertTrue(exception);
		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(origPatient.getLabel());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final ContactGroup removedElectrode = (ContactGroup) verifySession
					.get(
							ContactGroup.class, removedElectrodeId);
			assertNotNull(removedElectrode);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}
	}

	@Test
	public void savePatientRemoveNonAnalyzedStudy() throws Throwable {
		// Need to setup objects in the db to
		// use.
		List<Patient> patients = createMultiPatientDb();
		Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the study
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient
						.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(50))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		final GwtEegStudy gwtStudy = getOnlyElement(gwtAdmission.getStudies());
		gwtStudy.setDeleted(Boolean.TRUE);
		final Long removedStudyId = gwtStudy.getId();

		int origStudyCount = gwtAdmission.getStudies().size();

		// Check results
		final GwtPatient gwtResultPatient = btService.savePatient(gwtPatient);
		final GwtHospitalAdmission gwtResultAdmission = find(
				gwtResultPatient
						.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(50))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		assertEquals(origStudyCount - 1, gwtResultAdmission.getStudies().size());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final EegStudy removedStudy = (EegStudy) verifySession.get(
					EegStudy.class, removedStudyId);
			assertNull(removedStudy);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}
	}

	@Test
	public void savePatientRemoveAnalyzedStudy() throws Throwable {
		// Need to setup objects in the db to
		// use.
		List<Patient> patients = createMultiPatientDb();
		Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the study
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient
						.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(40))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		final GwtEegStudy gwtStudy = getOnlyElement(gwtAdmission.getStudies());
		gwtStudy.setDeleted(Boolean.TRUE);
		final Long removedStudyId = gwtStudy.getId();

		// Check results
		boolean exception = false;
		try {
			btService
					.savePatient(gwtPatient);
		} catch (BrainTrustUserException e) {
			exception = true;
		}
		assertTrue(exception);

		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(gwtPatient.getLabel());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final EegStudy removedStudy = (EegStudy) verifySession.get(
					EegStudy.class, removedStudyId);
			assertNotNull(removedStudy);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}
	}

	@Test
	public void deletePatientWithNoSharedTraces() throws Throwable {
		// Need to setup objects in the db to
		// use.
		final List<Patient> patients = createMultiPatientDb();
		final Patient origPatient = find(patients,
				compose(equalTo("Not Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Delete the patient
		btService.deletePatient(gwtPatient);

		// Check results
		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(origPatient.getLabel());
		assertNull(gwtResultPatient);
	}

	@Test
	public void deletePatientWithSharedTraces() throws Throwable {
		// Need to setup objects in the db to
		// use.
		final List<Patient> patients = createMultiPatientDb();
		final Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Delete the patient
		boolean exception = false;
		try {
			btService.deletePatient(gwtPatient);
		} catch (BrainTrustUserException e) {
			exception = true;
		}
		assertTrue(exception);
		// Check results
		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(origPatient.getLabel());
		assertNotNull(gwtResultPatient);

	}

	@Test
	public void savePatientRemoveNonAnalyzedAdmission() throws Throwable {
		// Need to setup objects in the db to
		// use.
		final List<Patient> patients = createMultiPatientDb();
		final Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the admission
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(50))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		assertNotNull(gwtAdmission);
		gwtAdmission.setDeleted(Boolean.TRUE);
		final Long removedAdmissionId = gwtAdmission.getId();

		final int origAdmissionCount = gwtPatient.getAdmissions().size();
		// Check results
		final GwtPatient gwtResultPatient = btService.savePatient(gwtPatient);
		assertEquals(origAdmissionCount - 1, gwtResultPatient.getAdmissions()
				.size());

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final HospitalAdmission removedAdmission = (HospitalAdmission) verifySession
					.get(
							HospitalAdmission.class, removedAdmissionId);
			assertNull(removedAdmission);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}
	}

	@Test
	public void savePatientRemoveAnalyzedAdmission() throws Throwable {
		// Need to setup objects in the db to
		// use.
		final List<Patient> patients = createMultiPatientDb();
		final Patient origPatient = find(patients,
				compose(equalTo("Analyzed"), Patient.getLabel), null);
		assertNotNull(origPatient);

		final BrainTrustService btService = newServiceWithAdminUser();

		// Retrieve the Patient
		GwtPatient gwtPatient = btService
				.retrievePatientByLabel(origPatient
						.getLabel());

		// Remove the admission
		final GwtHospitalAdmission gwtAdmission = find(
				gwtPatient.getAdmissions(),
				compose(equalTo(Optional.of(Integer.valueOf(40))),
						GwtHospitalAdmission.getAgeAtAdmission), null);
		assertNotNull(gwtAdmission);
		gwtAdmission.setDeleted(Boolean.TRUE);
		final Long removedAdmissionId = gwtAdmission.getId();

		// Check results
		boolean exception = false;
		try {
			btService.savePatient(gwtPatient);
		} catch (BrainTrustUserException e) {
			exception = true;
		}
		assertTrue(exception);

		// Verify last DTO against database
		Session verifySession = null;
		Transaction verifyTrx = null;
		final GwtPatient gwtResultPatient = btService
				.retrievePatientByLabel(gwtPatient.getLabel());
		try {
			verifySession = HibernateUtil.getSessionFactory().openSession();
			verifyTrx = verifySession.beginTransaction();
			final HospitalAdmission removedAdmission = (HospitalAdmission) verifySession
					.get(
							HospitalAdmission.class, removedAdmissionId);
			assertNotNull(removedAdmission);
			Patient dbPatient = (Patient) verifySession.load(Patient.class,
					gwtResultPatient.getId());
			ModelDtoCompare.comparePatients(dbPatient, gwtResultPatient);
			verifyTrx.commit();
		} catch (Throwable t) {
			try {
				if (verifyTrx != null && verifyTrx.isActive()) {
					verifyTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (verifySession != null) {
				verifySession.close();
			}
		}
	}

	@After
	public void tearDownSubject() {
		clearSubject();
	}

	private static Subject createSubjectWithId(UserId userId) {
		Subject admin = mock(Subject.class);
		when(admin.getPrincipal()).thenReturn(userId);
		return admin;
	}

	/**
	 * Returns the single patient created by this method. The patient will have
	 * an electrode with channel prefix "RT".
	 * 
	 * @return the single patient created by this method.
	 * @throws Throwable
	 */
	private static Patient createSinglePatientDb() throws Throwable {

		Session setupSession = null;
		Transaction setupTrx = null;

		try {
			setupSession = HibernateUtil.getSessionFactory().openSession();
			setupTrx = setupSession.beginTransaction();
			final BrainTrustDb btDb = new BrainTrustDb(setupSession);

			List<Patient> patients = newArrayList();
			List<TsAnnotationEntity> tsAnnotations = newArrayList();

			final ContactType contactType = TstObjectFactory
					.randomEnum(ContactType.class);

			final Patient patient = objFac.newPatient();
			patients.add(patient);

			final HospitalAdmission admission = objFac.newHospitalAdmission();
			patient.addAdmission(admission);
			final EegStudy study = objFac.newEegStudy();
			admission.addStudy(study);
			study.getRecording().setRefElectrodeDescription(newUuid());
			final ContactGroup electrode = objFac.newContactGroup();
			electrode.setChannelPrefix("RT");
			study.getRecording().addContactGroup(electrode);
			final TimeSeriesEntity trace = objFac.newTimeSeries();
			final Contact contact = objFac.newContact(contactType, trace);
			electrode.addContact(contact);
			final TsAnnotationEntity ann = objFac.newTsAnnotation(trace, study,
					creator);
			tsAnnotations.add(ann);
			btDb.saveEverything(
					patients,
					null,
					tsAnnotations,
					singleton(creator),
					null,
					null);
			setupTrx.commit();
			return patient;
		} catch (Throwable t) {
			try {
				if (setupTrx != null && setupTrx.isActive()) {
					setupTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (setupSession != null) {
				setupSession.close();
			}
		}

	}

	/**
	 * Returns a list with the two patients created by this method.
	 * 
	 * One patient has label "Not Analyzed" and has one Admission with one Study
	 * which shares no time series with any other datasnapshots.
	 * 
	 * The other patient has the label "Analyzed" and contains two Hospital
	 * admissions. One an has admission age of 50 and the other has admission
	 * age 40. The HA with admission age 50 contains one study which shares no
	 * time series with any other datasnapshots. The HA with admission age 40
	 * contains a study which shares a time series with a use created dataset.
	 * The shared time series belong to an electrode with prefix "ANALYZED". The
	 * study has a second electrode with prefix "NOTANALYZED" which has no
	 * shared time series.
	 * 
	 * @return the patients created by this method
	 * @throws Throwable
	 */
	private static List<Patient> createMultiPatientDb() throws Throwable {

		Session setupSession = null;
		Transaction setupTrx = null;

		try {
			setupSession = HibernateUtil.getSessionFactory().openSession();
			setupTrx = setupSession.beginTransaction();
			final BrainTrustDb btDb = new BrainTrustDb(setupSession);
			List<Patient> patients = newArrayList();
			List<TsAnnotationEntity> tsAnnotations = newArrayList();
			List<UserEntity> users = newArrayList();
			List<Dataset> datasets = newArrayList();

			final ContactType contactType1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType contactType2 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType contactType3 = TstObjectFactory
					.randomEnum(ContactType.class);

			// Not Analyzed patient
			{
				final Patient patientNotAnalyzed = objFac.newPatient();
				patientNotAnalyzed.setLabel("Not Analyzed");
				patients.add(patientNotAnalyzed);

				final HospitalAdmission admission = objFac
						.newHospitalAdmission();
				patientNotAnalyzed.addAdmission(admission);
				final EegStudy study = objFac.newEegStudy();
				admission.addStudy(study);
				study.getRecording().setRefElectrodeDescription(newUuid());
				final ContactGroup electrode = objFac.newContactGroup();
				electrode.setChannelPrefix("RT");
				study.getRecording().addContactGroup(electrode);
				final TimeSeriesEntity trace = objFac.newTimeSeries();
				final Contact contact = objFac.newContact(contactType1, trace);
				electrode.addContact(contact);
				final TsAnnotationEntity ann = objFac.newTsAnnotation(trace,
						study, creator);
				tsAnnotations.add(ann);
			}

			// Analyzed patient
			final Patient patientAnalyzed = objFac.newPatient();
			patientAnalyzed.setLabel("Analyzed");
			patients.add(patientAnalyzed);

			// Analyzed patient - not analyzed HA
			{
				final HospitalAdmission admissionNotAnalyzed = objFac
						.newHospitalAdmission();
				admissionNotAnalyzed.setAgeAtAdmission(50);
				patientAnalyzed.addAdmission(admissionNotAnalyzed);
				final EegStudy studyNotAnalyzed = objFac.newEegStudy();
				admissionNotAnalyzed.addStudy(studyNotAnalyzed);
				studyNotAnalyzed.getRecording().setRefElectrodeDescription(
						newUuid());
				final ContactGroup electrode = objFac.newContactGroup();
				studyNotAnalyzed.getRecording().addContactGroup(electrode);
				final TimeSeriesEntity trace = objFac.newTimeSeries();
				final Contact contact = objFac.newContact(contactType2, trace);
				electrode.addContact(contact);
				final TsAnnotationEntity ann = objFac.newTsAnnotation(trace,
						studyNotAnalyzed, creator);
				tsAnnotations.add(ann);

			}
			// Analyzed patient - analyzed HA
			{
				final HospitalAdmission admissionAnalyzed = objFac
						.newHospitalAdmission();
				admissionAnalyzed.setAgeAtAdmission(40);
				patientAnalyzed.addAdmission(admissionAnalyzed);
				final EegStudy studyAnalyzed = objFac.newEegStudy();
				admissionAnalyzed.addStudy(studyAnalyzed);
				studyAnalyzed.getRecording().setRefElectrodeDescription(
						newUuid());
				// analyzed study - not analyzed electrode
				{
					final ContactGroup electrode = objFac
							.newContactGroup();
					electrode.setChannelPrefix("NOTANALYZED");
					studyAnalyzed.getRecording().addContactGroup(electrode);
					final TimeSeriesEntity trace = objFac.newTimeSeries();
					final Contact contact = objFac.newContact(contactType3,
							trace);
					electrode.addContact(contact);
					final TsAnnotationEntity ann = objFac.newTsAnnotation(
							trace,
							studyAnalyzed, creator);
					tsAnnotations.add(ann);

				}
				// analyzed study - analyzed electrode
				{
					final ContactGroup electrode = objFac
							.newContactGroup();
					electrode.setChannelPrefix("ANALYZED");
					studyAnalyzed.getRecording().addContactGroup(electrode);
					final TimeSeriesEntity trace = objFac.newTimeSeries();
					final Contact contact = objFac.newContact(contactType1,
							trace);
					electrode.addContact(contact);
					final TsAnnotationEntity ann = objFac.newTsAnnotation(
							trace,
							studyAnalyzed,
							creator);
					tsAnnotations.add(ann);

					final Dataset analysis = objFac.newDataset();
					datasets.add(analysis);
					users.add(analysis.getCreator());
					users.add(creator);
					analysis.getTimeSeries().add(trace);
					final TsAnnotationEntity analysisAnn = objFac
							.newTsAnnotation(trace, analysis, creator);
					tsAnnotations.add(analysisAnn);

				}

			}
			btDb.saveEverything(
					patients,
					null,
					tsAnnotations,
					users,
					datasets,
					null);
			setupTrx.commit();
			return patients;
		} catch (Throwable t) {
			try {
				if (setupTrx != null && setupTrx.isActive()) {
					setupTrx.rollback();
				}
			} catch (final Throwable rbEx) {}
			throw t;
		} finally {
			if (setupSession != null) {
				setupSession.close();
			}
		}

	}

	@Test
	public void retrieveSpecies() throws Exception {
		Session s = null;
		Transaction t = null;
		SpeciesEntity species1 = null;
		SpeciesEntity species2 = null;
		SpeciesEntity species3 = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			species1 = objFac.newSpecies();
			objFac.newStrain(species1);
			s.saveOrUpdate(species1);

			species2 = objFac.newSpecies();
			objFac.newStrain(species2);
			objFac.newStrain(species2);
			s.saveOrUpdate(species2);

			species3 = objFac.newSpecies();
			objFac.newStrain(species3);
			objFac.newStrain(species3);
			objFac.newStrain(species3);
			s.saveOrUpdate(species3);

			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final List<GwtSpecies> actual = service.retrieveSpecies();
		assertEquals(3, actual.size());

		final Optional<GwtSpecies> gwtSpecies1Opt = tryFind(actual,
				compose(equalTo(species1.getId()), GwtSpecies.getId));
		assertTrue(gwtSpecies1Opt.isPresent());
		final GwtSpecies gwtSpecies1 = gwtSpecies1Opt.get();
		assertEquals(species1.getVersion(), gwtSpecies1.getVersion());
		assertEquals(species1.getLabel(), gwtSpecies1.getLabel());

		final Optional<GwtSpecies> gwtSpecies2Opt = tryFind(actual,
				compose(equalTo(species2.getId()), GwtSpecies.getId));
		assertTrue(gwtSpecies2Opt.isPresent());
		final GwtSpecies gwtSpecies2 = gwtSpecies2Opt.get();
		assertEquals(species2.getVersion(), gwtSpecies2.getVersion());
		assertEquals(species2.getLabel(), gwtSpecies2.getLabel());

		final Optional<GwtSpecies> gwtSpecies3Opt = tryFind(actual,
				compose(equalTo(species3.getId()), GwtSpecies.getId));
		assertTrue(gwtSpecies3Opt.isPresent());
		final GwtSpecies gwtSpecies3 = gwtSpecies3Opt.get();
		assertEquals(species3.getVersion(), gwtSpecies3.getVersion());
		assertEquals(species3.getLabel(), gwtSpecies3.getLabel());
	}

	@Test
	public void createSpecies() throws Throwable {
		Session session = null;
		Transaction trx = null;

		final GwtSpecies gwtSpecies = BuildDto.buildGwtSpecies();
		final GwtStrain gwtStrain = BuildDto.buildGwtStrain(
				gwtSpecies.getLabel(), null);

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtSpecies actualGwtSpecies = service.createSpecies(gwtSpecies,
				Collections.singleton(gwtStrain));
		assertNotNull(actualGwtSpecies);
		assertNotNull(actualGwtSpecies.getId());
		assertEquals(Integer.valueOf(0), actualGwtSpecies.getVersion());
		assertEquals(gwtSpecies.getLabel(), actualGwtSpecies.getLabel());

		// Verify DB
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(session);
			final SpeciesEntity actualSpecies = speciesDAO.findById(
					actualGwtSpecies.getId(), false);
			assertEquals(actualGwtSpecies.getVersion(),
					actualSpecies.getVersion());
			assertEquals(actualGwtSpecies.getLabel(), actualSpecies.getLabel());
			assertEquals(1, actualSpecies.getStrains().size());
			final StrainEntity actualStrain = getOnlyElement(actualSpecies
					.getStrains());
			assertNotNull(actualStrain);
			assertEquals(gwtStrain.getLabel(), actualStrain.getLabel());
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void findStrains() throws Exception {
		Session s = null;
		Transaction t = null;
		SpeciesEntity species = null;
		Set<StrainEntity> expectedStrains = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			species = objFac.newSpecies();
			objFac.newStrain(species);
			objFac.newStrain(species);
			objFac.newStrain(species);
			s.saveOrUpdate(species);
			expectedStrains = species.getStrains();
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithNonDataEntererUser();
		final SpeciesAssembler speciesAssembler = new SpeciesAssembler();
		final List<GwtStrain> actualStrains = service
				.findStrains(speciesAssembler
						.buildDto(species));
		assertEquals(3, actualStrains.size());

		for (final StrainEntity expected : expectedStrains) {

			final Optional<GwtStrain> actualOpt = tryFind(actualStrains,
					compose(equalTo(expected.getId()), GwtStrain.getId));
			assertTrue(actualOpt.isPresent());
			final GwtStrain actual = actualOpt.get();
			assertEquals(expected.getVersion(), actual.getVersion());
			assertEquals(expected.getLabel(), actual.getLabel());
		}
	}

	@Test
	public void createStrain() throws Throwable {

		Session s = null;
		Transaction t = null;
		SpeciesEntity species = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			species = objFac.newSpecies();
			objFac.newStrain(species);
			objFac.newStrain(species);
			objFac.newStrain(species);
			s.saveOrUpdate(species);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final GwtStrain expectedStrain = BuildDto.buildGwtStrain(
				species.getLabel(), species.getId());
		final BrainTrustService service = newServiceWithAdminUser();
		final SpeciesAssembler speciesAssembler = new SpeciesAssembler();
		final GwtStrain actualStrain = service
				.createStrain(speciesAssembler
						.buildDto(species), expectedStrain);
		assertNotNull(actualStrain);
		assertEquals(expectedStrain.getLabel(), actualStrain.getLabel());

		// Verify against DB
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();
			final IStrainDAO strainDAO = new StrainDAOHibernate(s);
			final StrainEntity actualStrainEntity = strainDAO.findById(
					actualStrain.getId(), false);
			assertEquals(actualStrain.getVersion(),
					actualStrainEntity.getVersion());
			assertEquals(actualStrain.getLabel(), actualStrainEntity.getLabel());
			t.commit();
		} catch (Throwable e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void modifySpeciesLabel() throws Throwable {

		Session s = null;
		Transaction t = null;
		SpeciesEntity species = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			species = objFac.newSpecies();
			objFac.newStrain(species);
			objFac.newStrain(species);
			objFac.newStrain(species);
			s.saveOrUpdate(species);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtSpecies modifiedDto = new GwtSpecies(newUuid(),
				species.getId(), species.getVersion());
		final GwtSpecies actualSpecies = service
				.modifySpeciesLabel(modifiedDto);
		assertNotNull(actualSpecies);
		assertEquals(modifiedDto.getLabel(), actualSpecies.getLabel());
		assertEquals(modifiedDto.getId(), actualSpecies.getId());
		assertEquals(Integer.valueOf(1 + modifiedDto.getVersion().intValue()),
				actualSpecies.getVersion());

		// Verify against DB
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();
			final ISpeciesDAO speciesDAO = new SpeciesDAOHibernate(s);
			final SpeciesEntity actualSpeciesEntity = speciesDAO.findById(
					actualSpecies.getId(), false);
			assertEquals(actualSpecies.getVersion(),
					actualSpeciesEntity.getVersion());
			assertEquals(actualSpecies.getLabel(),
					actualSpeciesEntity.getLabel());
			t.commit();
		} catch (Throwable e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void retrieveTraces() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup electrode = null;
		Contact c1 = null;
		Contact c2 = null;

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactType ct1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType ct2 = TstObjectFactory
					.randomEnum(ContactType.class);

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final TimeSeriesEntity ts1 = objFac.newTimeSeries();
			c1 = objFac.newContact(ct1, ts1);
			electrodeEntity.addContact(c1);
			final TimeSeriesEntity ts2 = objFac.newTimeSeries();
			c2 = objFac.newContact(ct2, ts2);
			electrodeEntity.addContact(c2);

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment =
					objFac.newExperiment(
							animal,
							new ExtAclEntity(readPerm));
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);

			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			electrode = electrodeAssembler.buildDto(electrodeEntity);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();

		final List<GwtTrace> actual = service.findTraces(electrode);
		assertEquals(2, actual.size());

		final GwtTrace actual1 = actual.get(0);
		final GwtTrace actual2 = actual.get(1);

		assertTrue(GwtTrace.LABEL_COMPARATOR.compare(actual1, actual2) < 0);
		final Set<Contact> expectedSet = newHashSet(c1, c2);

		final Contact expected1 = tryFind(expectedSet,
				compose(equalTo(actual1.getContactId()), Contact.getId))
				.orNull();
		assertNotNull(expected1);
		ModelDtoCompare.compareTraces(expected1, actual1);

		final Contact expected2 = tryFind(expectedSet,
				compose(equalTo(actual2.getContactId()), Contact.getId))
				.orNull();
		assertNotNull(expected2);
		ModelDtoCompare.compareTraces(expected2, actual2);

	}

	@Test
	public void createTraces() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup electrode = null;
		ContactType cInfo1 = TstObjectFactory.randomEnum(ContactType.class);
		ContactType cInfo2 = TstObjectFactory.randomEnum(ContactType.class);
		ContactType cInfo3 = TstObjectFactory.randomEnum(ContactType.class);
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);

			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			electrode = electrodeAssembler.buildDto(electrodeEntity);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtTrace newTrace1 = new GwtTrace(
				cInfo1,
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
		final GwtTrace newTrace2 = new GwtTrace(
				cInfo2,
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
		final GwtTrace newTrace3 = new GwtTrace(
				cInfo3,
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
		final Set<GwtTrace> newTraces = newHashSet(newTrace1, newTrace2,
				newTrace3);
		final List<GwtTrace> actualList = service.createTraces(electrode,
				newTraces);
		final List<GwtTrace> expectedList = newArrayList(newTraces);
		Collections.sort(expectedList, GwtTrace.LABEL_COMPARATOR);
		assertEquals(expectedList.size(), actualList.size());
		for (int i = 0; i < actualList.size(); i++) {
			final GwtTrace actual = actualList.get(i);
			final GwtTrace expected = expectedList.get(i);
			assertEquals(expected.getFileKey(), actual.getFileKey());
			assertEquals(expected.getLabel(), actual.getLabel());
			assertEquals(expected.getContactType(), actual.getContactType());
		}

		final List<GwtTrace> actualListFind = service.findTraces(electrode);
		assertEquals(expectedList.size(), actualListFind.size());
		for (int i = 0; i < actualListFind.size(); i++) {
			final GwtTrace actual = actualListFind.get(i);
			final GwtTrace expected = expectedList.get(i);
			assertEquals(expected.getFileKey(), actual.getFileKey());
			assertEquals(expected.getLabel(), actual.getLabel());
			assertEquals(expected.getContactType(), actual.getContactType());
		}
	}

	@Test
	public void createTracesAddTwice() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup electrode = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);

			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			electrode = electrodeAssembler.buildDto(electrodeEntity);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtTrace newTrace0 = new GwtTrace(
				TstObjectFactory.randomEnum(ContactType.class),
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
		final List<GwtTrace> actualList = service.createTraces(electrode,
				Collections.singleton(newTrace0));
		final GwtTrace actual = getOnlyElement(actualList, null);
		assertNotNull(actual);
		final GwtTrace newTrace1 = new GwtTrace(
				TstObjectFactory.randomEnum(ContactType.class),
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
		boolean exception = false;
		try {
			service.createTraces(electrode, newHashSet(newTrace0, newTrace1));
		} catch (IllegalArgumentException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void deleteTrace() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup gwtElectrode = null;
		GwtExperiment gwtExperiment = null;
		Contact c1 = null;
		Contact c2 = null;
		TsAnnotationEntity tsa12 = null;
		ExperimentEntity experiment = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactType ct1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType ct2 = TstObjectFactory
					.randomEnum(ContactType.class);

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final TimeSeriesEntity ts1 = objFac.newTimeSeries();
			c1 = objFac.newContact(ct1, ts1);
			electrodeEntity.addContact(c1);
			final TimeSeriesEntity ts2 = objFac.newTimeSeries();
			c2 = objFac.newContact(ct2, ts2);
			electrodeEntity.addContact(c2);

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			final TsAnnotationEntity tsa1 = objFac.newTsAnnotation(ts1,
					experiment, creator);
			tsa12 = objFac.newTsAnnotation(
					newHashSet(ts1, ts2), experiment, creator);
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);
			s.saveOrUpdate(tsa1);
			s.saveOrUpdate(tsa12);
			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			gwtElectrode = electrodeAssembler.buildDto(electrodeEntity);
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(s),
					new StimTypeDAOHibernate(s),
					new PermissionDAOHibernate(s),
					new TsAnnotationDAOHibernate(
							s,
							HibernateUtil.getConfiguration()));
			gwtExperiment = experimentAssembler.buildDto(experiment);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtTrace trace = new GwtTrace(
				c1.getContactType(),
				c1.getTrace().getLabel(),
				c1.getTrace().getFileKey(),
				c1.getId(),
				c1.getVersion(),
				c1.getTrace().getId(),
				c1.getTrace().getVersion());
		service.deleteTrace(gwtExperiment, gwtElectrode, trace);

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ITimeSeriesDAO tsDAO = new TimeSeriesDAOHibernate(s);
			final List<TimeSeriesEntity> allTs = tsDAO.findAll();
			assertEquals(1, allTs.size());
			assertEquals(c2.getTrace().getId(), allTs.get(0).getId());

			final IContactDAO contactDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactDAOHibernate(s);
			final List<Contact> allContacts = contactDAO.findAll();
			assertEquals(1, allContacts.size());
			assertEquals(c2.getId(), allContacts.get(0).getId());

			final ITsAnnotationDAO annDAO = new TsAnnotationDAOHibernate(
					s,
					HibernateUtil.getConfiguration());
			final List<TsAnnotationEntity> allAnns = annDAO.findAll();
			assertEquals(1, allAnns.size());
			final TsAnnotationEntity actualTsa12 = allAnns.get(0);
			assertEquals(tsa12.getId(), actualTsa12.getId());
			assertEquals(1, actualTsa12.getAnnotated().size());
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void modifyTrace() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup gwtElectrode = null;
		Contact c1 = null;
		Contact c2 = null;
		TsAnnotationEntity tsa12 = null;
		ExperimentEntity experiment = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactType ct1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType ct2 = TstObjectFactory
					.randomEnum(ContactType.class);

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final TimeSeriesEntity ts1 = objFac.newTimeSeries();
			c1 = objFac.newContact(ct1, ts1);
			electrodeEntity.addContact(c1);
			final TimeSeriesEntity ts2 = objFac.newTimeSeries();
			c2 = objFac.newContact(ct2, ts2);
			electrodeEntity.addContact(c2);

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			final TsAnnotationEntity tsa1 = objFac.newTsAnnotation(ts1,
					experiment, creator);
			tsa12 = objFac.newTsAnnotation(
					newHashSet(ts1, ts2), experiment, creator);
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);
			s.saveOrUpdate(tsa1);
			s.saveOrUpdate(tsa12);
			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			gwtElectrode = electrodeAssembler.buildDto(electrodeEntity);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final ContactType existingContactType = c1.getContactType();
		final ContactType newContactType = (existingContactType == ContactType.MICRO) ? ContactType.MACRO
				: ContactType.MICRO;
		final GwtTrace modifiedTrace = new GwtTrace(
				newContactType,
				newUuid(),
				newUuid(),
				c1.getId(),
				c1.getVersion(),
				c1.getTrace().getId(),
				c1.getTrace().getVersion());
		final GwtTrace actualTrace = service.modifyTrace(gwtElectrode,
				modifiedTrace);
		assertEquals(modifiedTrace.getContactType(),
				actualTrace.getContactType());
		assertEquals(modifiedTrace.getLabel(), actualTrace.getLabel());
		assertEquals(modifiedTrace.getFileKey(), actualTrace.getFileKey());
		assertEquals(modifiedTrace.getId(), actualTrace.getId());
		assertEquals(
				Integer.valueOf(modifiedTrace.getVersion().intValue() + 1),
				actualTrace.getVersion());
		assertEquals(modifiedTrace.getContactId(), actualTrace.getContactId());
		assertEquals(
				Integer.valueOf(modifiedTrace.getContactVersion() + 1),
				actualTrace.getContactVersion());
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final IContactDAO contactDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactDAOHibernate(s);
			final Contact dbContact = contactDAO.findById(
					modifiedTrace.getContactId(), false);
			assertNotNull(dbContact);
			ModelDtoCompare.compareTraces(dbContact, actualTrace);
			assertEquals(
					dbContact.getTrace().getVersion(),
					actualTrace.getVersion());
			assertEquals(dbContact.getTrace().getId(),
					actualTrace.getId());
			assertEquals(
					dbContact.getVersion(),
					actualTrace.getContactVersion());
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void createElectrode() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtRecording gwtRecording = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);

			final ITsAnnotationDAO annDAO = new TsAnnotationDAOHibernate(
					s,
					HibernateUtil.getConfiguration());
			final RecordingAssembler recordingAssembler = new RecordingAssembler(
					annDAO);
			gwtRecording = recordingAssembler.buildDto(experiment
					.getRecording());
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtContactGroup newElectrode = BuildDto
				.buildGwtContactGroup();
		newElectrode.setId(null);
		newElectrode.setVersion(null);
		final GwtContactGroup actualElectrode = service.createContactGroup(
				gwtRecording,
				newElectrode);
		assertEquals(newElectrode.getChannelPrefix(),
				actualElectrode.getChannelPrefix());
		assertEquals(newElectrode.getHffSetting(),
				actualElectrode.getHffSetting());
		assertEquals(newElectrode.getImpedance(),
				actualElectrode.getImpedance());
		assertEquals(newElectrode.getLffSetting(),
				actualElectrode.getLffSetting());
		assertEquals(
				newElectrode.getLocation(),
				actualElectrode.getLocation());
		assertEquals(newElectrode.getNotchFilter(),
				actualElectrode.getNotchFilter());
		assertEquals(
				newElectrode.getSamplingRate(),
				actualElectrode.getSamplingRate());
		assertEquals(
				newElectrode.getSide(),
				actualElectrode.getSide());
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final IContactGroupDAO electrodeDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactGroupDAOHibernate(
					s);
			final ContactGroup dbElectrode = electrodeDAO.findById(
					actualElectrode.getId(), false);
			assertNotNull(dbElectrode);
			ModelDtoCompare.compareContactGroups(dbElectrode, actualElectrode);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void createExperiment() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtAnimal gwtAnimal = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();
			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);
			final IOrganizationDAO organizationDAO = new OrganizationDAOHibernate(
					s);
			final StrainDAOHibernate strainDAO = new StrainDAOHibernate(
					s);
			final AnimalAssembler animalAssembler = new AnimalAssembler(
					organizationDAO,
					strainDAO);
			gwtAnimal = animalAssembler.buildDto(animal);

			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		final GwtExperiment newExperiment = BuildDto
				.buildGwtExperiment();

		final GwtExperiment actualExperiment = service.createExperiment(
				gwtAnimal,
				newExperiment);
		assertEquals(newExperiment.getAge(),
				actualExperiment.getAge());
		assertEquals(newExperiment.getLabel(),
				actualExperiment.getLabel());
		assertEquals(newExperiment.getStimDurationMs(),
				actualExperiment.getStimDurationMs());
		assertEquals(newExperiment.getStimIsiMs(),
				actualExperiment.getStimIsiMs());
		assertEquals(newExperiment.getStimRegion(),
				actualExperiment.getStimRegion());
		assertEquals(
				newExperiment.getStimType(),
				actualExperiment.getStimType());
		assertEquals(newExperiment.getRecording().getDir(),
				actualExperiment.getRecording().getDir());

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final IExperimentDAO experimentDAO = new edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate(s);
			final ExperimentEntity dbExperiment = experimentDAO.findById(
					actualExperiment.getId(), false);
			assertNotNull(dbExperiment);
			ModelDtoCompare.compareExperiments(dbExperiment, actualExperiment);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void deleteElectrode() throws Exception {
		Session s = null;
		Transaction t = null;
		GwtContactGroup gwtElectrode = null;
		GwtRecording gwtRecording = null;
		GwtExperiment gwtExperiment = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactType ct1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType ct2 = TstObjectFactory
					.randomEnum(ContactType.class);

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final TimeSeriesEntity ts1 = objFac.newTimeSeries();
			final Contact c1 = objFac.newContact(ct1, ts1);
			electrodeEntity.addContact(c1);
			final TimeSeriesEntity ts2 = objFac.newTimeSeries();
			final Contact c2 = objFac.newContact(ct2, ts2);
			electrodeEntity.addContact(c2);

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			final TsAnnotationEntity tsa1 = objFac.newTsAnnotation(ts1,
					experiment, creator);
			final TsAnnotationEntity tsa12 = objFac.newTsAnnotation(
					newHashSet(ts1, ts2), experiment, creator);
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);
			s.saveOrUpdate(tsa1);
			s.saveOrUpdate(tsa12);
			final ContactGroupAssembler electrodeAssembler = new ContactGroupAssembler();
			gwtElectrode = electrodeAssembler.buildDto(electrodeEntity);
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(s),
					new StimTypeDAOHibernate(s),
					new PermissionDAOHibernate(s),
					new TsAnnotationDAOHibernate(
							s,
							HibernateUtil.getConfiguration()));
			gwtExperiment = experimentAssembler.buildDto(experiment);
			gwtRecording = gwtExperiment.getRecording();
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}

		final BrainTrustService service = newServiceWithAdminUser();
		service.deleteContactGroup(gwtExperiment, gwtRecording, gwtElectrode);

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final IContactGroupDAO electrodeDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactGroupDAOHibernate(
					s);
			final List<ContactGroup> allElectrodes = electrodeDAO.findAll();
			assertTrue(allElectrodes.isEmpty());

			final ITsAnnotationDAO annDAO = new TsAnnotationDAOHibernate(s,
					HibernateUtil.getConfiguration());
			final List<TsAnnotationEntity> allAnns = annDAO.findAll();
			assertTrue(allAnns.isEmpty());
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void deleteExperiment()
			throws BrainTrustAuthorizationException,
			ObjectNotFoundException,
			StaleObjectException {
		Session s = null;
		Transaction t = null;
		GwtExperiment gwtExperiment = null;
		GwtAnimal gwtAnimal = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final ContactType ct1 = TstObjectFactory
					.randomEnum(ContactType.class);
			final ContactType ct2 = TstObjectFactory
					.randomEnum(ContactType.class);

			final ContactGroup electrodeEntity = objFac
					.newContactGroup();
			final TimeSeriesEntity ts1 = objFac.newTimeSeries();
			final Contact c1 = objFac.newContact(ct1, ts1);
			electrodeEntity.addContact(c1);
			final TimeSeriesEntity ts2 = objFac.newTimeSeries();
			final Contact c2 = objFac.newContact(ct2, ts2);
			electrodeEntity.addContact(c2);

			final SpeciesEntity species = objFac.newSpecies();
			final StrainEntity strain = objFac.newStrain(species);
			final AnimalEntity animal = objFac.newAnimal(strain);
			final ExperimentEntity experiment = objFac.newExperiment(animal,
					new ExtAclEntity(readPerm));
			final TsAnnotationEntity tsa1 = objFac.newTsAnnotation(ts1,
					experiment, creator);
			final TsAnnotationEntity tsa12 = objFac.newTsAnnotation(
					newHashSet(ts1, ts2), experiment, creator);
			experiment.getRecording().addContactGroup(electrodeEntity);
			s.saveOrUpdate(species);
			s.saveOrUpdate(animal);
			s.saveOrUpdate(creator);
			s.saveOrUpdate(tsa1);
			s.saveOrUpdate(tsa12);
			final ExperimentAssembler experimentAssembler = new ExperimentAssembler(
					new StimRegionDAOHibernate(s),
					new StimTypeDAOHibernate(s),
					new PermissionDAOHibernate(s),
					new TsAnnotationDAOHibernate(
							s,
							HibernateUtil.getConfiguration()));
			gwtExperiment = experimentAssembler.buildDto(experiment);
			final AnimalAssembler animalAssembler = new AnimalAssembler(
					new OrganizationDAOHibernate(s), 
					new StrainDAOHibernate(s));
			gwtAnimal = animalAssembler.buildDto(experiment.getParent());
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
		final BrainTrustService service = newServiceWithAdminUser();
		
		service.deleteExperiment(gwtAnimal, gwtExperiment);
		
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			final IExperimentDAO experimentDAO = new edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate(
					s);
			final List<ExperimentEntity> allExperiments = experimentDAO.findAll();
			assertTrue(allExperiments.isEmpty());

			final IAnimalDAO animalDAO = new edu.upenn.cis.braintrust.dao.metadata.hibernate.AnimalDAOHibernate(s);
			final AnimalEntity parent = animalDAO.get(gwtAnimal.getId());
			assertNotNull(parent);
			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}
}
