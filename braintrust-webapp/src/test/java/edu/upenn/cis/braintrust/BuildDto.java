/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust;

import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Collections;
import java.util.Random;
import java.util.Set;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Pathology;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtSpecies;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Build DTOs for tests.
 * 
 * @author John Frommeyer
 * 
 */
public class BuildDto {

	private final static Random RND = new Random();

	private BuildDto() {
		throw new AssertionError("can't instantiate a BuildDto");
	}

	public static GwtContactGroup buildGwtContactGroup() {
		final GwtContactGroup gwtE = new GwtContactGroup(
				"LAG",
				newUuid(),
				Side.LEFT,
				Location.OCCIPITAL,
				Double.valueOf(499.987),
				Boolean.FALSE,
				Double.valueOf(0.1),
				Double.valueOf(9000.0),
				Boolean.FALSE,
				3.4,
				null,
				null,
				null,
				Long.valueOf(3),
				Integer.valueOf(50));
		return gwtE;
	}

	public static GwtContactGroup buildGwtContactGroup(final long id,
			final int version, final String channelPrefix) {
		final GwtContactGroup gwtE = new GwtContactGroup(
				channelPrefix,
				newUuid(),
				Side.LEFT,
				Location.OCCIPITAL,
				RND.nextDouble(),
				Boolean.FALSE,
				RND.nextDouble(),
				RND.nextDouble(),
				RND.nextBoolean(),
				RND.nextDouble(),
				null,
				null,
				null,
				Long.valueOf(id),
				Integer.valueOf(version));
		return gwtE;
	}

	public static GwtEegStudy buildGwtStudy(
			final Set<GwtContactGroup> gwtContactGroups) {
		final GwtEegStudy gwtS = new GwtEegStudy();
		gwtS.setId(Long.valueOf(12));
		gwtS.setVersion(Integer.valueOf(40));
		gwtS.setLabel("Study idx");
		gwtS.setSzCount(null);
		gwtS.setType(EegStudyType.EMU_RECORDING);
		gwtS.setPublished(Boolean.TRUE);
		gwtS.getImageTypes().add(ImageType.CT);
		gwtS.getImageTypes().add(ImageType.MRI);
		gwtS.getImageTypes().add(ImageType.THREE_D_RENDERING);
		gwtS.setRecording(buildGwtRecording(gwtContactGroups));
		return gwtS;
	}

	public static GwtEegStudy buildGwtStudy(final long id, final int version,
			final String label) {
		final GwtEegStudy gwtS = new GwtEegStudy();

		gwtS.setId(Long.valueOf(id));
		gwtS.setVersion(Integer.valueOf(version));
		gwtS.setLabel(label);
		gwtS.setSzCount(null);
		gwtS.setType(EegStudyType.EMU_RECORDING);
		gwtS.setPublished(Boolean.TRUE);
		gwtS.getImageTypes().add(ImageType.CT);
		gwtS.getImageTypes().add(ImageType.MRI);
		gwtS.getImageTypes().add(ImageType.THREE_D_RENDERING);
		gwtS.setRecording(buildGwtRecording(id, version));
		return gwtS;
	}

	public static GwtTrace buildGwtTrace(int contactNo) {
		return new GwtTrace(
				ContactType.MACRO,
				newUuid(),
				newUuid(),
				null,
				null,
				null,
				null);
	}

	public static GwtRecording buildGwtRecording() {
		final int startTime = TstObjectFactory.randomNonnegInt();
		final int endTime = startTime + TstObjectFactory.randomNonnegInt();
		final GwtRecording gwtR = new GwtRecording(
				newUuid(),
				newUuid(),
				Long.valueOf(startTime),
				Long.valueOf(endTime),
				null,
				null,
				null,
				null,
				null,
				null);

		return gwtR;
	}

	public static GwtRecording buildGwtRecording(
			final Set<GwtContactGroup> gwtContactGroups) {
		final GwtRecording gwtR = new GwtRecording(
				"IEED_data/IEED_idx",
				"IC_idx",
				Long.valueOf(4454688654662L),
				Long.valueOf(4999688654662L),
				newUuid(),
				newUuid(),
				newUuid(),
				newUuid(),
				Long.valueOf(12),
				Integer.valueOf(40));
		for (final GwtContactGroup gwtContactGroup : gwtContactGroups) {
			gwtR.addContactGroup(gwtContactGroup);
		}
		return gwtR;
	}

	public static GwtRecording buildGwtRecording(final long id,
			final int version) {
		final GwtRecording gwtR = new GwtRecording(
				"IEED_data/IEED_idx",
				"IC_idx",
				Long.valueOf(4454688654662L),
				Long.valueOf(4999688654662L),
				newUuid(),
				newUuid(),
				newUuid(),
				newUuid(),
				Long.valueOf(id),
				Integer.valueOf(version));

		final Set<GwtContactGroup> contactGroups = buildGwtContactGroups();
		for (final GwtContactGroup contactGroup : contactGroups) {
			gwtR.addContactGroup(contactGroup);
		}
		return gwtR;
	}

	public static GwtEegStudy buildGwtStudy() {
		return buildGwtStudy(buildGwtContactGroups());
	}

	public static Set<GwtContactGroup> buildGwtContactGroups() {
		final Set<String> prefixes = newHashSet("LAG", "AG", "OS", "ROS");
		final Set<GwtContactGroup> gwtEs = newHashSet();
		long id = 13;
		for (final String prefix : prefixes) {
			gwtEs.add(buildGwtContactGroup(id++, 0, prefix));
		}
		return gwtEs;
	}

	public static GwtExperiment buildGwtExperiment() {
		final GwtExperiment experiment = new GwtExperiment(
				newUuid(),
				BuildDto.buildGwtRecording(),
				RND.nextBoolean(),
				null,
				null,
				null,
				null,
				null,
				null,
				null);
		return experiment;
	}

	public static Set<GwtEegStudy> buildGwtStudies() {
		final Set<GwtEegStudy> gwtS = newHashSet();
		final Set<String> labels = newHashSet("Study 001", "Study 005",
				"Study 007", "Study 018");
		long id = 8;
		for (final String label : labels) {
			gwtS.add(buildGwtStudy(id++, 0, label));
		}
		return gwtS;
	}

	public static GwtHospitalAdmission buildGwtAdmission(
			Set<GwtEegStudy> gwtStudies) {
		final GwtHospitalAdmission gwtAd = new GwtHospitalAdmission();
		gwtAd.setId(Long.valueOf(4));
		gwtAd.setVersion(Integer.valueOf(34));
		gwtAd.setAgeAtAdmission(null);
		for (final GwtEegStudy gwtStudy : gwtStudies) {
			gwtAd.addStudy(gwtStudy);
		}
		return gwtAd;
	}

	public static GwtHospitalAdmission buildGwtAdmission(final long id,
			final int version, final Integer ageAtAdmission) {
		final GwtHospitalAdmission gwtAd = new GwtHospitalAdmission();
		gwtAd.setId(id);
		gwtAd.setVersion(version);
		gwtAd.setAgeAtAdmission(ageAtAdmission);
		final Set<GwtEegStudy> gwtStudies = buildGwtStudies();
		for (final GwtEegStudy gwtStudy : gwtStudies) {
			gwtAd.addStudy(gwtStudy);
		}
		return gwtAd;
	}

	public static Set<GwtHospitalAdmission> buildGwtAdmissions() {
		final Set<GwtHospitalAdmission> admissions = newHashSet();
		final Set<Integer> ages = newHashSet(null,
				Integer.valueOf(22), Integer.valueOf(15));
		long id = 32;
		for (final Integer age : ages) {
			admissions.add(buildGwtAdmission(id++, 0, age));
		}
		return admissions;

	}

	public static GwtHospitalAdmission buildGwtAdmissionWithMultipleStudies() {
		final GwtHospitalAdmission gwtAdmission = new GwtHospitalAdmission();
		gwtAdmission.setId(Long.valueOf(12));
		gwtAdmission.setVersion(Integer.valueOf(0));
		gwtAdmission.setAgeAtAdmission(null);
		Set<GwtEegStudy> gwtStudies = buildGwtStudies();
		for (final GwtEegStudy gwtStudy : gwtStudies) {
			gwtAdmission.addStudy(gwtStudy);
		}
		return gwtAdmission;
	}

	public static GwtHospitalAdmission buildGwtAdmissionWithSingleStudy() {
		final GwtHospitalAdmission gwtAdmission = new GwtHospitalAdmission();
		gwtAdmission.setId(Long.valueOf(10));
		gwtAdmission.setVersion(Integer.valueOf(0));
		gwtAdmission.setAgeAtAdmission(null);
		gwtAdmission.addStudy(buildGwtStudy());

		return gwtAdmission;
	}

	static public GwtOrganization buildGwtOrganization() {
		return new GwtOrganization(
				newUuid(),
				String.format("I%03d", RND.nextInt(1000)),
				null,
				null);
	}

	static public GwtPatient buildGwtPatientWithSingleAdmission() {
		Set<GwtHospitalAdmission> admissions = Collections
				.singleton(buildGwtAdmissionWithSingleStudy());
		return buildGwtPatient(
				buildGwtOrganization(),
				admissions);
	}

	static public GwtPatient buildGwtPatient(
			GwtOrganization organization,
			final Set<GwtHospitalAdmission> admissions) {
		final GwtPatient patient = new GwtPatient();

		patient.setId(Long.valueOf(2));
		patient.setVersion(Integer.valueOf(17));
		patient.setLabel("032");
		patient.setEthnicity(Ethnicity.AMERICAN_INDIAN);
		patient.setGender(Gender.FEMALE);
		patient.setHandedness(Handedness.AMBIDEXTROUS);
		patient.setOrganization(organization);
		patient.setAgeOfOnset(12);
		patient.setEtiology(Pathology.FOCAL_CORTICAL_DYSPLASIA.toString());
		patient.setDevelopmentalDelay("A dev delay.");
		patient.setDevelopmentalDisorders(null);
		patient.setTraumaticBrainInjury(Boolean.TRUE);
		patient.setFamilyHistory(null);
		patient.getSzTypes().add(SeizureType.ABSENCE);
		patient.getSzTypes().add(SeizureType.ATONIC);
		patient.getPrecipitants().add(Precipitant.ALCOHOL);
		patient.getPrecipitants().add(Precipitant.DRUGS);
		patient.getPrecipitants().add(Precipitant.FEBRILE);

		for (final GwtHospitalAdmission admission : admissions) {
			patient.addAdmission(admission);
		}

		return patient;
	}

	public static GwtPatient buildGwtPatientWithMultipleAdmissions() {
		return buildGwtPatient(
				buildGwtOrganization(),
				buildGwtAdmissions());
	}

	public static GwtSpecies buildGwtSpecies() {
		return new GwtSpecies(newUuid(), null, null);
	}

	public static GwtStrain buildGwtStrain(@Nullable String speciesLabel,
			@Nullable Long speciesId) {
		return new GwtStrain(newUuid(), null, null, speciesLabel, speciesId);
	}

}
