/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.server.assembler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.ModelDtoCompare;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.PatientAssembler;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Tests for {@code HospitalAdmissionAssembler}.
 * 
 * @author John Frommeyer
 * 
 */
public class PatientAssemblerTest {

	private TstObjectFactory tstObjFac = new TstObjectFactory(false);

	private PatientAssembler assembler;
	@Mock
	private IPermissionDAO permissionDAO;
	@Mock
	private ITsAnnotationDAO annotationDAO;
	@Mock
	private IOrganizationDAO organizationDAO;
	@Mock
	private IDataSnapshotService dsService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		when(permissionDAO.findStarCoreReadPerm()).thenReturn(
				tstObjFac.getReadPerm());
		assembler = new PatientAssembler(
				permissionDAO,
				annotationDAO,
				organizationDAO,
				dsService);
	}

	@Test
	public void testBuildDto() {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		setAdmissionIdsAndVersions(patient);
		final GwtPatient gwtPatient = assembler
				.buildDto(patient);
		ModelDtoCompare.comparePatients(patient, gwtPatient);
	}

	@Test
	public void testBuildEntity() {
		// A new patient will have no id, version, or admissions.
		final Set<GwtHospitalAdmission> emptyAdmissions = Collections
				.emptySet();
		final GwtPatient gwtAdmission = BuildDto
				.buildGwtPatient(
						BuildDto.buildGwtOrganization(),
						emptyAdmissions);
		gwtAdmission.setId(null);
		gwtAdmission.setVersion(null);
		final Patient study = assembler.buildEntity(gwtAdmission);
		ModelDtoCompare.comparePatients(study, gwtAdmission);
		assertNull(study.getId());
		assertNull(study.getVersion());
	}

	@Test
	public void testModifyEntitySameAdmissions() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final GwtPatient gwtPatient = BuildDto
				.buildGwtPatientWithMultipleAdmissions();
		PrepareForModifyTestUtil.preparePatientForModifySameAdmissions(
				patient, gwtPatient);
		assembler.modifyEntity(patient, gwtPatient);
		ModelDtoCompare.comparePatients(patient, gwtPatient);
	}

	@Test
	public void testModifyEntityAddAdmission() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final GwtPatient gwtPatient = BuildDto
				.buildGwtPatientWithMultipleAdmissions();
		PrepareForModifyTestUtil.preparePatientForModifyAddAdmission(patient,
				gwtPatient);
		final int origNoAdmissions = patient.getAdmissions().size();
		assembler.modifyEntity(patient, gwtPatient);
		assertEquals(origNoAdmissions + 1, patient.getAdmissions().size());
		ModelDtoCompare.comparePatients(patient, gwtPatient);
	}

	@Test
	public void testModifyEntityRemoveAdmission() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final GwtPatient gwtPatient = BuildDto
				.buildGwtPatientWithMultipleAdmissions();
		PrepareForModifyTestUtil.preparePatientForModifyRemoveAdmission(
				patient,
				gwtPatient);
		for (HospitalAdmission admission : patient.getAdmissions()) {
			for (EegStudy study : admission.getStudies()) {
				when(dsService.isRecordingModifiable(study)).thenReturn(
						Boolean.TRUE);
			}
		}

		final int origNoAdmissions = patient.getAdmissions().size();
		assembler.modifyEntity(patient, gwtPatient);
		assertEquals(origNoAdmissions - 1, patient.getAdmissions().size());
		ModelDtoCompare.comparePatients(patient, gwtPatient);
	}

	@Test
	public void testModifyEntityIdMismatch() throws StaleObjectException,
			BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final GwtPatient gwtPatient = BuildDto
				.buildGwtPatientWithSingleAdmission();
		patient.setId(Long.valueOf(gwtPatient.getId().longValue() + 1));
		patient.setVersion(gwtPatient.getVersion());
		final Long origId = patient.getId();
		final Integer origVer = patient.getVersion();
		final Integer origAge = patient.getAgeOfOnset();
		final String origDelay = patient.getDevelopmentalDelay();
		final Boolean origDisorders = patient.getDevelopmentalDisorders();
		final Ethnicity origEthnicity = patient.getEthnicity();
		final String origEtiology = patient.getEtiology();
		final Boolean origFamilyHistory = patient.getFamilyHistory();
		final Gender origGender = patient.getGender();
		final Handedness origHandedness = patient.getHandedness();
		final String origInstitution = patient.getOrganization().getCode();
		final Boolean origTraumaticBrainInjury = patient
				.getTraumaticBrainInjury();
		final String origUserSuppliedId = patient.getLabel();

		final int origNoPrecipitants = patient.getPrecipitants().size();
		final int origNoSzTypes = patient.getSzTypes().size();

		boolean exception = false;
		try {
			assembler.modifyEntity(patient, gwtPatient);
		} catch (IllegalArgumentException iae) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, patient.getId());
		assertEquals(origVer, patient.getVersion());
		assertEquals(origAge, patient.getAgeOfOnset());
		assertEquals(origDelay, patient.getDevelopmentalDelay());
		assertEquals(origDisorders, patient.getDevelopmentalDisorders());
		assertEquals(origEthnicity, patient.getEthnicity());
		assertEquals(origEtiology, patient.getEtiology());
		assertEquals(origFamilyHistory, patient.getFamilyHistory());
		assertEquals(origGender, patient.getGender());
		assertEquals(origHandedness, patient.getHandedness());
		assertEquals(origInstitution, patient.getOrganization().getCode());
		assertEquals(origTraumaticBrainInjury, patient
				.getTraumaticBrainInjury());
		assertEquals(origUserSuppliedId, patient.getLabel());

		assertEquals(origNoPrecipitants, patient.getPrecipitants().size());
		assertEquals(origNoSzTypes, patient.getSzTypes().size());

	}

	@Test
	public void testModifyEntityVersionMismatch()
			throws BrainTrustUserException {
		PermissionEntity readPerm = tstObjFac.getReadPerm();
		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final GwtPatient gwtPatient = BuildDto
				.buildGwtPatientWithSingleAdmission();
		patient.setId(gwtPatient.getId());
		patient.setVersion(Integer
				.valueOf(gwtPatient.getVersion().intValue() + 1));
		final Long origId = patient.getId();
		final Integer origVer = patient.getVersion();
		final Integer origAge = patient.getAgeOfOnset();
		final String origDelay = patient.getDevelopmentalDelay();
		final Boolean origDisorders = patient.getDevelopmentalDisorders();
		final Ethnicity origEthnicity = patient.getEthnicity();
		final String origEtiology = patient.getEtiology();
		final Boolean origFamilyHistory = patient.getFamilyHistory();
		final Gender origGender = patient.getGender();
		final Handedness origHandedness = patient.getHandedness();
		final String origInstitution = patient.getOrganization().getCode();
		final Boolean origTraumaticBrainInjury = patient
				.getTraumaticBrainInjury();
		final String origUserSuppliedId = patient.getLabel();

		final int origNoPrecipitants = patient.getPrecipitants().size();
		final int origNoSzTypes = patient.getSzTypes().size();

		boolean exception = false;
		try {
			assembler.modifyEntity(patient, gwtPatient);
		} catch (StaleObjectException soe) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, patient.getId());
		assertEquals(origVer, patient.getVersion());
		assertEquals(origAge, patient.getAgeOfOnset());
		assertEquals(origDelay, patient.getDevelopmentalDelay());
		assertEquals(origDisorders, patient.getDevelopmentalDisorders());
		assertEquals(origEthnicity, patient.getEthnicity());
		assertEquals(origEtiology, patient.getEtiology());
		assertEquals(origFamilyHistory, patient.getFamilyHistory());
		assertEquals(origGender, patient.getGender());
		assertEquals(origHandedness, patient.getHandedness());
		assertEquals(origInstitution, patient.getOrganization().getCode());
		assertEquals(origTraumaticBrainInjury, patient
				.getTraumaticBrainInjury());
		assertEquals(origUserSuppliedId, patient.getLabel());

		assertEquals(origNoPrecipitants, patient.getPrecipitants().size());
		assertEquals(origNoSzTypes, patient.getSzTypes().size());

	}

	private static void setAdmissionIdsAndVersions(final Patient patient) {
		long id = 0;
		int version = 0;
		for (final HospitalAdmission admission : patient.getAdmissions()) {
			admission.setId(Long.valueOf(id++));
			admission.setVersion(Integer.valueOf(version++));
			setStudyIdsAndVersions(admission);
		}
	}

	private static void setStudyIdsAndVersions(final HospitalAdmission admission) {
		long studyId = 0;
		int studyVersion = 0;
		for (final EegStudy study : admission.getStudies()) {
			study.setId(Long.valueOf(studyId++));
			study.setVersion(Integer.valueOf(studyVersion++));
			setElectrodeIdsAndVersions(study);
		}
	}

	private static void setElectrodeIdsAndVersions(final EegStudy study) {
		long electrodeId = 0;
		int electrodeVersion = 0;
		for (final ContactGroup electrode : study.getRecording()
				.getContactGroups()) {
			electrode.setId(Long.valueOf(electrodeId++));
			electrode.setVersion(Integer.valueOf(electrodeVersion++));
		}
	}

}
