/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.PatientAssembler;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.Pathology;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Test the patient presenter.
 * 
 * @author John Frommeyer
 */
public class PatientPresenterTest {

	// Presenter Dependencies
	@Mock
	private BrainTrustServiceAsync theService;
	@Mock
	private PatientPresenter.IPatientView theDisplay;
	@Mock
	private HandlerManager eventBus;

	// SubViews of IPatientPresenter

	@Mock
	private IHasValueAndErrorMessage<Integer> ageAtOnsetView;
	@Mock
	private IHasValueAndSuggestions<String> etiologyView;
	@Mock
	private HasClickHandlers addHospitalAdmissionButton;
	@Mock
	private HasClickHandlers saveButton;
	@Mock
	private HasClickHandlers cancelButton;
	@Mock
	private HasClickHandlers deleteButton;
	@Mock
	private IHasValue<String> ddView;
	@Mock
	private IHasValue<Boolean> disorderView;
	@Mock
	private IHasValue<Boolean> fhView;
	@Mock
	private ISingleSelectionList organizationView;
	@Mock
	private IHasValue<Boolean> tbiView;
	@Mock
	private HasValue<String> userSuppliedIdView;
	@Mock
	private HasClickHandlers editButton;
	@Mock
	private HasClickHandlers removeButton;

	private List<GwtOrganization> organizations = ImmutableList.of(
			new GwtOrganization(
					"AAA",
					"I001",
					Long.valueOf(0),
					Integer.valueOf(1)),
			new GwtOrganization(
					"BBB",
					"I002",
					Long.valueOf(5),
					Integer.valueOf(10)),
			new GwtOrganization(
					"CCC",
					"I015",
					Long.valueOf(3),
					Integer.valueOf(3)));

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@SuppressWarnings("unchecked")
	@Ignore("GWT-2.5.1")
	public void testSaveNewPatient() {
		GwtPatient expected = buildShallowGwtPatient(organizations.get(1));
		initViewFromPatient(expected, null);
		final PatientPresenter pp = new PatientPresenter(
				eventBus,
				theDisplay,
				theService,
				organizations);

		final ClickEvent clickEvent = mock(ClickEvent.class);
		final HasClickHandlers searchButton = theDisplay.getSaveButton();
		when(clickEvent.getSource()).thenReturn(searchButton);
		pp.onClick(clickEvent);

		final ArgumentCaptor<GwtPatient> argument = ArgumentCaptor
				.forClass(GwtPatient.class);

		verify(theService).savePatient(argument.capture(),
				(AsyncCallback<GwtPatient>) Matchers.anyObject());

		final GwtPatient actual = argument.getValue();
		assertNotNull(actual);
		assertEquals(expected.getLabel(), actual.getLabel());
	}

	@Test
	public void testExistingPatient() {
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();
		final Patient patient = BuildDb.buildPatient0(
				readPerm);
		final IPermissionDAO permissionDAO = mock(IPermissionDAO.class);
		final ITsAnnotationDAO annotationDAO = mock(ITsAnnotationDAO.class);
		final IOrganizationDAO organizationDAO = mock(IOrganizationDAO.class);
		final IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		final PatientAssembler assembler = new PatientAssembler(
				permissionDAO,
				annotationDAO,
				organizationDAO,
				dsService);
		GwtPatient gwtPatient = assembler.buildDto(patient);
		gwtPatient.setOrganization(organizations.get(1));
		final GwtHospitalAdmission gwtAdmission = Iterables.get(
				gwtPatient.getAdmissions(), 0);

		initEmptyPatientView();
		new PatientPresenter(
				eventBus,
				theDisplay,
				theService,
				organizations,
				gwtPatient);
		verify(theDisplay.getOrganizationView()).setSelectedIndex(1);

		final String admissionDescFragment = gwtAdmission.getAgeAtAdmission()
				.isPresent() ? gwtAdmission.getAgeAtAdmission().get()
				.toString() : GwtHospitalAdmission.UNKNOWN_ADMISSION_AGE;
		verify(theDisplay).addHospitalAdmission(Matchers.anyLong(),
				Matchers.contains(admissionDescFragment),
				anyBoolean());
	}

	@Test
	@Ignore("GWT-2.5.1")
	public void testRemoveAdmission() {
		final GwtPatient patient = BuildDto
				.buildGwtPatientWithSingleAdmission();
		patient.setOrganization(organizations.get(1));
		Map<Long, HasClickHandlers> idToRemoveButton = newHashMap();
		initViewFromPatient(patient,
				idToRemoveButton);

		new PatientPresenter(
				eventBus,
				theDisplay,
				theService,
				organizations,
				patient);

		final GwtHospitalAdmission admission = getOnlyElement(
				patient.getAdmissions(),
				null);
		final HasClickHandlers removeButton = idToRemoveButton.get(admission
				.getId());
		ArgumentCaptor<ClickHandler> handlerArgCaptor = ArgumentCaptor
				.forClass(ClickHandler.class);
		verify(removeButton).addClickHandler(handlerArgCaptor.capture());
		final ClickHandler removeClickHandler = handlerArgCaptor.getValue();
		final ClickEvent clickEvent = mock(ClickEvent.class);
		when(clickEvent.getSource()).thenReturn(removeButton);
		removeClickHandler.onClick(clickEvent);

		assertTrue(patient.getAdmissions().contains(admission));
		assertTrue(admission.isDeleted());
		verify(theDisplay).removeHospitalAdmission(admission.getId());
	}

	private void initEmptyPatientView() {
		when(theDisplay.getOrganizationView()).thenReturn(organizationView);

		when(theDisplay.getAgeAtOnsetView()).thenReturn(ageAtOnsetView);

		when(theDisplay.getEtiologyView()).thenReturn(etiologyView);

		when(theDisplay.getAddHospitalAdmissionButton()).thenReturn(
				addHospitalAdmissionButton);

		when(theDisplay.getSaveButton()).thenReturn(saveButton);

		when(theDisplay.getCancelButton()).thenReturn(cancelButton);

		when(theDisplay.getDeleteButton()).thenReturn(deleteButton);

		when(theDisplay.getDevelopmentalDelayView()).thenReturn(ddView);

		when(theDisplay.getDevelopmentalDisorderView())
				.thenReturn(disorderView);

		when(theDisplay.getFamilyHistoryView()).thenReturn(fhView);

		when(theDisplay.getTraumaticBrainInjuryView()).thenReturn(tbiView);

		when(theDisplay.getPatientLabel()).thenReturn(userSuppliedIdView);

		EditRemoveButtonPair editRemovePair = new EditRemoveButtonPair(
				editButton, removeButton);
		when(
				theDisplay.addHospitalAdmission(Matchers.anyLong(),
						Matchers.anyString(), anyBoolean()))
				.thenReturn(editRemovePair);
	}

	private void initViewFromPatient(final GwtPatient patient,
			final Map<Long, HasClickHandlers> idToRemoveButton) {

		final GwtOrganization org = patient.getOrganization();
		final int orgIdx = organizations.indexOf(org);

		when(organizationView.getSelectedIndex()).thenReturn(orgIdx);
		when(theDisplay.getOrganizationView()).thenReturn(organizationView);

		when(theDisplay.getAddHospitalAdmissionButton()).thenReturn(
				addHospitalAdmissionButton);

		when(theDisplay.getSaveButton()).thenReturn(saveButton);

		when(theDisplay.getCancelButton()).thenReturn(cancelButton);

		when(theDisplay.getDeleteButton()).thenReturn(deleteButton);

		when(ageAtOnsetView.getValue()).thenReturn(
				patient.getAgeOfOnset().orNull());
		when(theDisplay.getAgeAtOnsetView()).thenReturn(ageAtOnsetView);

		when(ddView.getValue()).thenReturn(patient.getDevelopmentalDelay().orNull());
		when(theDisplay.getDevelopmentalDelayView()).thenReturn(ddView);

		when(disorderView.getValue()).thenReturn(
				patient.getDevelopmentalDisorders().orNull());
		when(theDisplay.getDevelopmentalDisorderView())
				.thenReturn(disorderView);

		final String ethnicity = patient.getEthnicity().toString();
		final int i = Arrays.binarySearch(PatientPresenter.ETHNICITY_CHOICES,
				ethnicity);
		when(theDisplay.getSelectedEthnicity()).thenReturn(Integer.valueOf(i));

		when(etiologyView.getValue()).thenReturn(patient.getEtiology());
		when(theDisplay.getEtiologyView()).thenReturn(etiologyView);

		when(fhView.getValue()).thenReturn(patient.getFamilyHistory().orNull());
		when(theDisplay.getFamilyHistoryView()).thenReturn(fhView);

		final String gender = patient.getGender().toString();
		final int genderIdx = Arrays.binarySearch(
				PatientPresenter.GENDER_CHOICES, gender);
		when(theDisplay.getSelectedGender()).thenReturn(
				Integer.valueOf(genderIdx));

		final String handedness = patient.getHandedness().toString();
		final int handIdx = Arrays.binarySearch(
				PatientPresenter.HANDEDNESS_CHOICES, handedness);
		when(theDisplay.getSelectedHandedness()).thenReturn(
				Integer.valueOf(handIdx));

		Set<Integer> precipitantRows = ListChoices.enums2Indices(
				patient.getPrecipitants(), PatientPresenter.SITUATION_CHOICES,
				Precipitant.fromString);
		when(theDisplay.getSelectedSituationRows()).thenReturn(precipitantRows);

		Set<Integer> szTypeRows = ListChoices.enums2Indices(
				patient.getSzTypes(), ListChoices.SZ_TYPE_CHOICES,
				SeizureType.fromString);
		when(theDisplay.getSelectedSeizureTypeRows()).thenReturn(szTypeRows);

		when(tbiView.getValue()).thenReturn(
				patient.getTraumaticBrainInjury().orNull());
		when(theDisplay.getTraumaticBrainInjuryView()).thenReturn(tbiView);

		when(userSuppliedIdView.getValue()).thenReturn(
				patient.getLabel());
		when(theDisplay.getPatientLabel()).thenReturn(userSuppliedIdView);

		// Edit and remove admission buttons
		for (final GwtHospitalAdmission admission : patient.getAdmissions()) {
			final HasClickHandlers editAdmissionButton = mock(HasClickHandlers.class);
			final HasClickHandlers removeAdmissionButton = mock(HasClickHandlers.class);
			final EditRemoveButtonPair buttonPair = new EditRemoveButtonPair(
					editAdmissionButton, removeAdmissionButton);
			final String admissionAgeStr = admission.getAgeAtAdmission()
					.isPresent() ? admission.getAgeAtAdmission().get()
					.toString() : GwtHospitalAdmission.UNKNOWN_ADMISSION_AGE;
			final String description = "Age at admission: "
					+ admissionAgeStr;
			when(
					theDisplay.addHospitalAdmission(admission.getId(),
							description, admission.isRemovable()))
					.thenReturn(
							buttonPair);
			if (idToRemoveButton != null) {
				idToRemoveButton.put(admission.getId(), removeAdmissionButton);
			}
		}

	}

	static private GwtPatient buildShallowGwtPatient(GwtOrganization organization) {
		final GwtPatient patient = new GwtPatient();

		patient.setEthnicity(Ethnicity.AMERICAN_INDIAN);
		patient.setGender(Gender.FEMALE);
		patient.setHandedness(Handedness.AMBIDEXTROUS);
		patient.setOrganization(organization);
		patient.setLabel(organization.getCode() + "_P001");
		patient.setAgeOfOnset(12);
		patient.setEtiology(Pathology.FOCAL_CORTICAL_DYSPLASIA.toString());
		patient.setDevelopmentalDelay("A dev delay.");
		patient.setDevelopmentalDisorders(Boolean.FALSE);
		patient.setTraumaticBrainInjury(Boolean.TRUE);
		patient.setFamilyHistory(Boolean.TRUE);
		patient.getSzTypes().add(SeizureType.ABSENCE);
		patient.getSzTypes().add(SeizureType.ATONIC);
		patient.getPrecipitants().add(Precipitant.ALCOHOL);
		patient.getPrecipitants().add(Precipitant.DRUGS);
		patient.getPrecipitants().add(Precipitant.FEBRILE);

		return patient;
	}
}
