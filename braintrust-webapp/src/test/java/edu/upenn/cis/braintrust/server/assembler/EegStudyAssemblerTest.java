/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.server.assembler;

import static edu.upenn.cis.braintrust.server.assembler.PrepareForModifyTestUtil.prepareStudyForModifyAddElectrode;
import static edu.upenn.cis.braintrust.server.assembler.PrepareForModifyTestUtil.prepareStudyForModifyRemoveElectrode;
import static edu.upenn.cis.braintrust.server.assembler.PrepareForModifyTestUtil.prepareStudyForModifySameElectrodes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.ModelDtoCompare;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.EegStudyAssembler;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Tests for {@code EegStudyAssembler}
 * 
 * @author John Frommeyer
 * 
 */
public class EegStudyAssemblerTest {

	private EegStudyAssembler assembler;
	private IPermissionDAO permissionDAO;
	private ITsAnnotationDAO annotationDAO;
	private IDataSnapshotService dsService;
	private TstObjectFactory objFac = new TstObjectFactory(false);

	@Before
	public void setup() {
		permissionDAO = mock(IPermissionDAO.class);
		annotationDAO = mock(ITsAnnotationDAO.class);
		dsService = mock(IDataSnapshotService.class);
		when(permissionDAO.findStarCoreReadPerm()).thenReturn(
				objFac.getReadPerm());
		assembler = new EegStudyAssembler(
				permissionDAO,
				annotationDAO,
				dsService);
	}

	@Test
	public void testBuildDto() {
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final EegStudy study = BuildDb.buildStudy000(readPerm);
		setElectrodeIdsAndVersions(study);
		when(dsService.isRecordingModifiable(study)).thenReturn(true);
		final GwtEegStudy gwtStudy = assembler
				.buildDto(study);
		ModelDtoCompare.compareStudies(study, gwtStudy);
		assertTrue(gwtStudy.isModifiable());
	}

	@Test
	public void testBuildDtoUnmodifiable() {
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();
		final EegStudy study = BuildDb.buildStudy000(readPerm);
		setElectrodeIdsAndVersions(study);
		when(dsService.isRecordingModifiable(study)).thenReturn(false);
		final GwtEegStudy gwtStudy = assembler
				.buildDto(study);
		ModelDtoCompare.compareStudies(study, gwtStudy);
		assertFalse(gwtStudy.isModifiable());
	}

	private static void setElectrodeIdsAndVersions(final EegStudy study) {
		long electrodeId = 0;
		int electrodeVersion = 0;
		for (final ContactGroup electrode : study.getRecording()
				.getContactGroups()) {
			electrode.setId(Long.valueOf(electrodeId++));
			electrode.setVersion(Integer.valueOf(electrodeVersion++));
		}
	}

	@Test
	public void testBuildEntityPublished() {
		// A new study will have no electrodes, id, or version.
		final Set<GwtContactGroup> emptyElectrodes = Collections.emptySet();
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy(emptyElectrodes);
		gwtStudy.setId(null);
		gwtStudy.setVersion(null);
		final GwtRecording gwtRecording = gwtStudy.getRecording();
		gwtRecording.setId(null);
		gwtRecording.setVersion(null);
		final EegStudy study = assembler.buildEntity(gwtStudy);
		assertNotNull(study.getPubId());
		ModelDtoCompare.compareStudies(study, gwtStudy);
		assertNull(study.getId());
		assertNull(study.getVersion());
	}

	@Test
	public void testBuildEntityUnpublished() {
		// A new study will have no electrodes, id, or version.
		final Set<GwtContactGroup> emptyElectrodes = Collections.emptySet();
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy(emptyElectrodes);
		gwtStudy.setId(null);
		gwtStudy.setVersion(null);
		gwtStudy.setPublished(false);

		final GwtRecording gwtRecording = gwtStudy.getRecording();
		gwtRecording.setId(null);
		gwtRecording.setVersion(null);
		final EegStudy study = assembler.buildEntity(gwtStudy);
		assertNotNull(study.getPubId());
		ModelDtoCompare.compareStudies(study, gwtStudy);
		assertNull(study.getId());
		assertNull(study.getVersion());
	}

	@Test
	public void testModifyEntitySameElectrodes() throws StaleObjectException,
			BrainTrustUserException {
		// DB objects
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();
		final EegStudy study = BuildDb.buildStudy000(readPerm);

		// DTO
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy();

		prepareStudyForModifySameElectrodes(gwtStudy, study);
		final int origNoElectrodes = study.getRecording().getContactGroups()
				.size();
		assembler.modifyEntity(study, gwtStudy, true);
		assertEquals(origNoElectrodes, study.getRecording().getContactGroups()
				.size());
		ModelDtoCompare.compareStudies(study, gwtStudy);
	}

	@Test
	public void testModifyEntityAddElectrode() throws StaleObjectException,
			BrainTrustUserException {
		// DB objects

		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final EegStudy study = BuildDb.buildStudy000(readPerm);

		// DTO
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy();

		prepareStudyForModifyAddElectrode(gwtStudy, study);
		final int origNoElectrodes = study.getRecording().getContactGroups()
				.size();
		assembler.modifyEntity(study, gwtStudy, true);
		assertEquals(origNoElectrodes + 1, study.getRecording()
				.getContactGroups().size());
		ModelDtoCompare.compareStudies(study, gwtStudy);
	}

	@Test
	public void testModifyEntityRemoveElectrode() throws StaleObjectException,
			BrainTrustUserException {
		// DB objects
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final EegStudy study = BuildDb.buildStudy000(readPerm);

		// DTO
		final GwtEegStudy gwtStudy = BuildDto.buildGwtStudy();

		prepareStudyForModifyRemoveElectrode(gwtStudy, study);
		final int origNoElectrodes = study.getRecording().getContactGroups()
				.size();
		assembler.modifyEntity(study, gwtStudy, true);
		assertEquals(origNoElectrodes - 1, study.getRecording()
				.getContactGroups().size());
		ModelDtoCompare.compareStudies(study, gwtStudy);
	}

	@SuppressFBWarnings(
			value = "ES_COMPARING_STRINGS_WITH_EQ",
			justification = "we really are testing that the strings are the same")
	@Test
	public void testModifyEntityIdMismatch() throws StaleObjectException,
			BrainTrustUserException {
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final EegStudy s = BuildDb.buildStudy000(readPerm);
		final GwtEegStudy gwtS = BuildDto.buildGwtStudy();
		s.setId(Long.valueOf(gwtS.getId().longValue() + 1));
		s.setVersion(gwtS.getVersion());
		final Long origId = s.getId();
		final Integer origVer = s.getVersion();
		final String origDir = s.getDir();
		final Long origEndTime = s.getEndTimeUutc();
		final Long origStartTime = s.getStartTimeUutc();
		final String origLabel = s.getLabel();
		final String origMefDir = s.getMefDir();
		final String origRefElectrodeDescription = s.getRecording()
				.getRefElectrodeDescription();
		final String origStudyId = s.getPubId();
		final Integer origSzCount = s.getSzCount();
		final EegStudyType origType = s.getType();

		final int origNoElectrodes = s.getRecording().getContactGroups().size();
		final int origNoImages = s.getImages().size();
		final int origNoTs = s.getTimeSeries().size();

		boolean exception = false;
		try {
			assembler.modifyEntity(s, gwtS, true);
		} catch (IllegalArgumentException iae) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, s.getId());
		assertEquals(origVer, s.getVersion());
		assertEquals(origDir, s.getDir());
		assertEquals(origEndTime, s.getEndTimeUutc());
		assertEquals(origStartTime, s.getStartTimeUutc());
		assertEquals(origLabel, s.getLabel());
		assertEquals(origMefDir, s.getMefDir());
		assertEquals(origStudyId, s.getPubId());
		assertEquals(origSzCount, s.getSzCount());
		assertEquals(origType, s.getType());
		assertSame(origRefElectrodeDescription, s.getRecording()
				.getRefElectrodeDescription());

		assertEquals(origNoElectrodes, s.getRecording().getContactGroups()
				.size());
		assertEquals(origNoImages, s.getImages().size());
		assertEquals(origNoTs, s.getTimeSeries().size());

	}

	@SuppressFBWarnings(
			value = "ES_COMPARING_STRINGS_WITH_EQ",
			justification = "we really are testing that the strings are the same")
	@Test
	public void testModifyEntityVersionMismatch()
			throws BrainTrustUserException {
		TstObjectFactory tstObjFac = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjFac.getReadPerm();

		final EegStudy s = BuildDb.buildStudy000(readPerm);
		final GwtEegStudy gwtS = BuildDto.buildGwtStudy();
		s.setId(gwtS.getId());
		s.setVersion(Integer.valueOf(gwtS.getVersion().intValue() + 1));
		final Long origId = s.getId();
		final Integer origVer = s.getVersion();
		final String origDir = s.getDir();
		final Long origEndTime = s.getEndTimeUutc();
		final Long origStartTime = s.getStartTimeUutc();
		final String origLabel = s.getLabel();
		final String origMefDir = s.getMefDir();
		final String origReferenceElectrode = s.getRecording()
				.getRefElectrodeDescription();
		final String origStudyId = s.getPubId();
		final Integer origSzCount = s.getSzCount();
		final EegStudyType origType = s.getType();

		final int origNoElectrodes = s.getRecording().getContactGroups().size();
		final int origNoImages = s.getImages().size();
		final int origNoTs = s.getTimeSeries().size();

		boolean exception = false;
		try {
			assembler.modifyEntity(s, gwtS, true);
		} catch (StaleObjectException soe) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, s.getId());
		assertEquals(origVer, s.getVersion());
		assertEquals(origDir, s.getDir());
		assertEquals(origEndTime, s.getEndTimeUutc());
		assertEquals(origStartTime, s.getStartTimeUutc());
		assertEquals(origLabel, s.getLabel());
		assertEquals(origMefDir, s.getMefDir());
		assertEquals(origStudyId, s.getPubId());
		assertEquals(origSzCount, s.getSzCount());
		assertEquals(origType, s.getType());
		assertSame(origReferenceElectrode, s.getRecording()
				.getRefElectrodeDescription());

		assertEquals(origNoElectrodes, s.getRecording().getContactGroups()
				.size());
		assertEquals(origNoImages, s.getImages().size());
		assertEquals(origNoTs, s.getTimeSeries().size());
	}
}
