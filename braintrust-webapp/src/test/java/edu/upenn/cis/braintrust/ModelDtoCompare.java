/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.tryFind;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Assert;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;

import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtMriCoordinates;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;

/**
 * Compare values from model entities and DTOs.
 * <p>
 * The methods usually don't compare all fields, just enough to convince us the
 * persisting is working.
 * 
 * @author Sam Donnelly
 */
public class ModelDtoCompare {

	public static void comparePatients(final Patient patient,
			final GwtPatient gwtPatient) {
		Collection<GwtHospitalAdmission> undeletedAdmissions = filter(
				gwtPatient.getAdmissions(),
				new Predicate<GwtHospitalAdmission>() {

					@Override
					public boolean apply(GwtHospitalAdmission input) {
						return !input.isDeleted();
					}
				});
		Assert.assertEquals(patient.getAdmissions().size(),
				undeletedAdmissions.size());
		for (final HospitalAdmission expectedCase : patient
				.getAdmissions()) {
			final GwtHospitalAdmission actualCase = find(undeletedAdmissions,
					compose(equalTo(expectedCase.getId()),
							IHasLongId.getId));
			compareAdmissions(expectedCase, actualCase);
		}
		Assert.assertEquals(patient.getLabel(), gwtPatient
				.getLabel());
		Assert.assertEquals(patient.getEthnicity(), gwtPatient
				.getEthnicity());
		Assert.assertEquals(patient.getGender(), gwtPatient
				.getGender());
		Assert.assertEquals(patient.getEtiology(), gwtPatient
				.getEtiology());
		Assert.assertEquals(patient.getAgeOfOnset(), gwtPatient
				.getAgeOfOnset().orNull());
		assertEquals(patient.getDevelopmentalDelay(), gwtPatient
				.getDevelopmentalDelay().orNull());
		assertEquals(patient.getDevelopmentalDisorders(), gwtPatient
				.getDevelopmentalDisorders().orNull());
		assertEquals(patient.getTraumaticBrainInjury(), gwtPatient
				.getTraumaticBrainInjury().orNull());
		assertEquals(patient.getFamilyHistory(), gwtPatient
				.getFamilyHistory().orNull());
		Assert.assertEquals(patient.getHandedness(), gwtPatient
				.getHandedness());
		Assert.assertEquals(patient.getSzTypes().size(), gwtPatient
				.getSzTypes().size());
	}

	public static void compareAdmissions(
			final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission) {
		assertEquals(admission.getAgeAtAdmission(), gwtAdmission
				.getAgeAtAdmission().orNull());
		Collection<GwtEegStudy> undeletedGwtStudies = filter(
				gwtAdmission.getStudies(), new Predicate<GwtEegStudy>() {

					@Override
					public boolean apply(GwtEegStudy input) {
						return !input.isDeleted();
					}
				});
		Assert.assertEquals(admission.getStudies().size(),
				undeletedGwtStudies.size());
		for (final EegStudy study : admission.getStudies()) {
			final GwtEegStudy gwtStudy = find(undeletedGwtStudies,
					compose(equalTo(study.getId()), IHasLongId.getId));
			compareStudies(study, gwtStudy);
		}
	}

	public static void compareStudies(final EegStudy study,
			final GwtEegStudy gwtStudy) {
		compareRecordings(study.getRecording(), gwtStudy.getRecording());
		if (gwtStudy.isPublished()) {
			final Optional<PermissionEntity> readPerm = tryFind(
					study.getExtAcl().getWorldAce(),
					compose(equalTo(CorePermDefs.READ_PERMISSION_NAME),
							PermissionEntity.getName));
			assertTrue(readPerm.isPresent());
		} else {
			study.getExtAcl().getWorldAce().isEmpty();
		}
		assertEquals(study.getLabel(), gwtStudy.getLabel());

		assertEquals(study.getSzCount(), gwtStudy.getSzCount().orNull());
		assertEquals(study.getType(), gwtStudy.getType());

	}

	public static void compareRecordings(final Recording recording,
			final GwtRecording gwtRecording) {
		Collection<GwtContactGroup> undeletedGwtContactGroups = filter(
				gwtRecording.getContactGroups(), new Predicate<GwtContactGroup>() {

					@Override
					public boolean apply(GwtContactGroup input) {
						return !input.isDeleted();
					}
				});
		assertEquals(recording.getContactGroups().size(),
				undeletedGwtContactGroups.size());
		assertEquals(recording.getDir(), gwtRecording.getDir());
		assertEquals(recording.getMefDir(), gwtRecording.getMefDir());
		assertEquals(recording.getReportFile(), gwtRecording.getReportFile());
		assertEquals(recording.getImagesFile(), gwtRecording.getImagesFile());
		assertEquals(recording.getRefElectrodeDescription(), gwtRecording
				.getRefElectrodeDescription().orNull());

		for (final ContactGroup contactGroup : recording.getContactGroups()) {
			final Long dbId = contactGroup.getId();
			GwtContactGroup gwtContactGroup = find(undeletedGwtContactGroups,
					compose(equalTo(dbId), GwtContactGroup.getId));
			compareContactGroups(contactGroup, gwtContactGroup);
		}
	}

	private ModelDtoCompare() {
		throw new AssertionError("Can't instantiate a ModelTestAssert");
	}

	public static void compareContactGroups(ContactGroup contactGroup,
			GwtContactGroup gwtContactGroup) {
		assertEquals(contactGroup.getSide(),
				gwtContactGroup.getSide());
		assertEquals(contactGroup.getLocation(),
				gwtContactGroup.getLocation());
		assertEquals(contactGroup.getImpedance(),
				gwtContactGroup.getImpedance().orNull());
		assertEquals(contactGroup.getChannelPrefix(),
				gwtContactGroup.getChannelPrefix());
		assertEquals(contactGroup.getHffSetting(), gwtContactGroup.getHffSetting()
				.orNull());
		assertEquals(contactGroup.getLffSetting(), gwtContactGroup.getLffSetting()
				.orNull());
		assertEquals(contactGroup.getNotchFilter(), gwtContactGroup.getNotchFilter()
				.orNull());
		assertEquals(contactGroup.getSamplingRate(),
				gwtContactGroup.getSamplingRate());
		assertEquals(contactGroup.getElectrodeType(),
				gwtContactGroup.getElectrodeType());
		assertEquals(contactGroup.getManufacturer(),
				gwtContactGroup.getManufacturer().orNull());
		assertEquals(contactGroup.getModelNo(), gwtContactGroup.getModelNo().orNull());
	}

	public static void compareMriCoordinates(final MriCoordinates mriCoords,
			final GwtMriCoordinates gwtMriCoords) {

		if (mriCoords != null && gwtMriCoords != null) {
			assertEquals(mriCoords.getX(), gwtMriCoords.getX());
			assertEquals(mriCoords.getY(), gwtMriCoords.getY());
			assertEquals(mriCoords.getZ(), gwtMriCoords.getZ());
		} else if (mriCoords == null && gwtMriCoords == null) {
			return;
		} else if (mriCoords == null && gwtMriCoords != null) {
			throw new AssertionError("Model null, but DTO not null");
		} else {
			throw new AssertionError("Model not null, but DTO null");
		}
	}

	public static void compareTraces(Contact contact, GwtTrace trace) {
		assertEquals(contact.getContactType(), trace.getContactType());
		assertEquals(contact.getTrace().getFileKey(), trace.getFileKey());
		assertEquals(contact.getTrace().getLabel(), trace.getLabel());
		assertEquals(contact.getId(), trace.getContactId());
		assertEquals(contact.getVersion(), trace.getContactVersion());
		assertEquals(contact.getTrace().getId(), trace.getId());
		assertEquals(contact.getTrace().getVersion(), trace.getVersion());
	}

	public static void compareExperiments(ExperimentEntity experiment,
			GwtExperiment gwtExperiment) {
		assertEquals(experiment.getAge(),
				gwtExperiment.getAge());
		assertEquals(experiment.getLabel(),
				gwtExperiment.getLabel());
		assertEquals(experiment.getStimDurationMs(),
				gwtExperiment.getStimDurationMs());
		assertEquals(experiment.getStimIsiMs(),
				gwtExperiment.getStimIsiMs());
		if (experiment.getStimRegion() != null
				&& gwtExperiment.getStimRegion() != null) {
			compareStimRegions(experiment.getStimRegion(),
					gwtExperiment.getStimRegion());
		} else {
			assertTrue(experiment.getStimRegion() == null
					&& gwtExperiment.getStimRegion() == null);
		}
		if (experiment.getStimType() != null
				&& gwtExperiment.getStimType() != null) {
			compareStimTypes(
					experiment.getStimType(),
					gwtExperiment.getStimType());
		} else {
			assertTrue(experiment.getStimType() == null
					&& gwtExperiment.getStimType() == null);
		}
		compareRecordings(experiment.getRecording(),
				gwtExperiment.getRecording());

	}

	public static void compareStimRegions(StimRegionEntity stimRegion,
			GwtStimRegion gwtStimRegion) {
		assertEquals(stimRegion.getLabel(), gwtStimRegion.getLabel());
	}

	public static void compareStimTypes(StimTypeEntity stimType,
			GwtStimType gwtStimType) {
		assertEquals(stimType.getLabel(), gwtStimType.getLabel());
	}
}
