/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.TracesPresenter.FileKeyUpdater;
import edu.upenn.cis.braintrust.client.presenter.TracesPresenter.ITracesView;
import edu.upenn.cis.braintrust.client.presenter.TracesPresenter.LabelUpdater;
import edu.upenn.cis.braintrust.client.presenter.TracesPresenter.TraceDeleter;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;

public class TracesPresenterTest {

	@Mock
	private ITracesView display;
	@Mock
	private HasClickHandlers okButton;
	@Mock
	private HasClickHandlers createTracesButton;
	@Mock
	private ITraceTable traceTable;
	@Mock
	private HasValue<String> fileKeyList;
	@Mock
	private BrainTrustServiceAsync service;
	@Mock
	private IHasGwtRecording recordingParent;
	@Captor
	private ArgumentCaptor<Set<GwtTrace>> traceCaptor;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		when(display.getOkButton()).thenReturn(okButton);
		when(display.getCreateTracesButton()).thenReturn(createTracesButton);
		when(display.getTraceTable()).thenReturn(traceTable);
		when(display.getFileKeyList()).thenReturn(
				fileKeyList);
	}

	@Test
	public void constructorTestModifiableNoTraces() {
		final GwtContactGroup electrode = BuildDto.buildGwtContactGroup();
		final TracesPresenter presenter = new TracesPresenter(
				display,
				service,
				electrode,
				recordingParent,
				true,
				Collections.<GwtTrace> emptyList());

		// Verify bind with modifiable = true
		verify(okButton).addClickHandler(presenter);
		verify(createTracesButton).addClickHandler(presenter);
		verify(traceTable).setFileKeyUpdater(isA(FileKeyUpdater.class));
		verify(traceTable).setLabelUpdater(isA(LabelUpdater.class));
		verify(traceTable).setTraceDeleter(isA(TraceDeleter.class));

	}

	@SuppressWarnings("unchecked")
	@Test
	@Ignore("GWT-2.5.1")
	public void testDefaultFileKeys() {
		final GwtContactGroup electrode = BuildDto.buildGwtContactGroup();
		final String prefix = "x/y/z";
		final TracesPresenter presenter = new TracesPresenter(
				display,
				service,
				electrode,
				recordingParent,
				true,
				Collections.<GwtTrace> emptyList());
		final String fileKeyListRaw = "x/y/z/" + electrode.getChannelPrefix()
				+ "2.mef\r\nx/y/z/" + electrode.getChannelPrefix() + "4.mef";
		when(fileKeyList.getValue()).thenReturn(
				fileKeyListRaw);
		final ClickEvent createTracesEvent = mock(ClickEvent.class);
		when(createTracesEvent.getSource()).thenReturn(createTracesButton);
		presenter.onClick(createTracesEvent);
		verify(service).createTraces(eq(electrode), traceCaptor.capture(),
				any(AsyncCallback.class));
		final Set<GwtTrace> traces = traceCaptor.getValue();
		assertEquals(2, traces.size());
		final String label2 = electrode.getChannelPrefix() + "2";
		final GwtTrace trace2 = tryFind(traces,
				compose(equalTo(label2),
						GwtTrace.getLabel)).orNull();
		assertNotNull(trace2);
		assertEquals(prefix + "/" + label2 + ".mef", trace2.getFileKey());

		final String label4 = electrode.getChannelPrefix() + "4";
		final GwtTrace trace4 = tryFind(traces,
				compose(equalTo(label4),
						GwtTrace.getLabel)).orNull();
		assertNotNull(trace4);
		assertEquals(prefix + "/" + label4 + ".mef", trace4.getFileKey());
	}
}
