/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.client.presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import edu.upenn.cis.braintrust.client.presenter.StringToValue;

/**
 * Tests for the {@code StringToValue} utility class.
 * @author John Frommeyer
 *
 */
public class StringToValueTest {
	
	@Test
	public void toNonNegativeLongWithSciNotation() {
		final String str = "1.0774e+15";
		final Long expected = Long.valueOf(1077400000000000L);
		Long actual = StringToValue.toNonNegativeLong(str);
		assertEquals(expected, actual);
	}
	
	
	@Test
	public void toNonNegativeLong() {
		final String str = "1010";
		final Long expected = Long.valueOf(1010L);
		Long actual = StringToValue.toLong(str);
		assertEquals(expected, actual);
		
	}
	@Test
	public void toLong() {
		final String str = "101026564462";
		final Long expected = Long.valueOf(101026564462L);
		Long actual = StringToValue.toLong(str);
		assertEquals(expected, actual);
	}
	
	@Test
	public void badToLong() {
		//The letter O at the end
		final String str = "101O";
		Long actual = StringToValue.toLong(str);
		assertNull(actual);
		
	}
	
	@Test
	public void toNonNegativeInteger() {
		final String str = "1010";
		final Integer expected = Integer.valueOf(1010);
		Integer actual = StringToValue.toInteger(str);
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void toNonNegativeIntegerWithNegativeInput() {
		final String str = "-27237";
		Integer actual = StringToValue.toNonNegativeInteger(str);
		assertNull("Actual value = " + actual, actual);
		
	}
	
	@Test
	public void toNonNegativeIntegerWithZeroInput() {
		final String str = "0";
		Integer actual = StringToValue.toNonNegativeInteger(str);
		assertEquals(Integer.valueOf(0), actual);
		
	}
	@Test
	public void toNonNegativeIntegerWithBadInput() {
		final String str = "typo";
		Integer actual = StringToValue.toNonNegativeInteger(str);
		assertNull("Actual value = " + actual, actual);
		
	}
}
