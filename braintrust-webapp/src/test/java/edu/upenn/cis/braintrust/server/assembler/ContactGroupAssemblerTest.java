/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.server.assembler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.ModelDtoCompare;
import edu.upenn.cis.braintrust.datasnapshot.assembler.webapp.ContactGroupAssembler;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;
import edu.upenn.cis.braintrust.testhelper.BuildDb;

public class ContactGroupAssemblerTest {

	private ContactGroupAssembler assembler;

	@Before
	public void setup() {
		assembler = new ContactGroupAssembler();
	}

	@Test
	public void testBuildDto() {
		final ContactGroup electrode = BuildDb.buildElectrode0000();
		final GwtContactGroup gwtElectrode = assembler
				.buildDto(electrode);
		ModelDtoCompare.compareContactGroups(electrode, gwtElectrode);
	}

	@Test
	public void testBuildEntity() {
		final GwtContactGroup gwtE = BuildDto
				.buildGwtContactGroup();
		// buildEntity should only get called for creating a brand new object,
		// so Id and version should be null.
		gwtE.setId(null);
		gwtE.setVersion(null);

		final ContactGroup e = assembler.buildEntity(gwtE);
		ModelDtoCompare.compareContactGroups(e, gwtE);
		assertNull(e.getId());
		assertNull(e.getVersion());
	}

	@Test
	public void testModifyEntity() throws StaleObjectException {
		final ContactGroup e = BuildDb
				.buildElectrode0000();
		final GwtContactGroup gwtE = BuildDto
				.buildGwtContactGroup();
		e.setId(gwtE.getId());
		e.setVersion(gwtE.getVersion());
		assembler.modifyEntity(e, gwtE);
		assertEquals(gwtE.getId(), e.getId());
		assertEquals(gwtE.getVersion(), e.getVersion());
		ModelDtoCompare.compareContactGroups(e, gwtE);
	}

	@Test
	public void testModifyEntityIdMismatch() throws StaleObjectException {
		final ContactGroup e = BuildDb.buildElectrode0000();

		final GwtContactGroup gwtE = BuildDto
				.buildGwtContactGroup();
		e.setId(Long.valueOf(gwtE.getId().longValue() + 1));
		e.setVersion(gwtE.getVersion());
		final Long origId = e.getId();
		final Integer origVer = e.getVersion();
		final String origPrefix = e.getChannelPrefix();
		final Double origHffSetting = e.getHffSetting();
		final Double origImpedence = e.getImpedance();
		final Double origLffSetting = e.getLffSetting();
		final Location origLobe = e.getLocation();
		final Boolean origNotchFilter = e.getNotchFilter();
		final Double origSamplingRate = e.getSamplingRate();
		final Side origSide = e.getSide();
		boolean exception = false;
		try {
			assembler.modifyEntity(e, gwtE);
		} catch (IllegalArgumentException iae) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, e.getId());
		assertEquals(origVer, e.getVersion());
		assertEquals(origHffSetting, e.getHffSetting());
		assertEquals(origPrefix, e.getChannelPrefix());
		assertEquals(origImpedence, e.getImpedance());
		assertEquals(origLffSetting, e.getLffSetting());
		assertEquals(origLobe, e.getLocation());
		assertEquals(origNotchFilter, e.getNotchFilter());
		assertEquals(origSamplingRate, e.getSamplingRate());
		assertEquals(origSide, e.getSide());
	}

	@Test
	public void testModifyEntityVersionMismatch() {
		final ContactGroup e = BuildDb.buildElectrode0000();

		final GwtContactGroup gwtE = BuildDto
				.buildGwtContactGroup();

		e.setId(gwtE.getId());
		e.setVersion(Integer.valueOf(gwtE.getVersion().intValue() + 1));
		Long origId = e.getId();
		Integer origVer = e.getVersion();
		final String origPrefix = e.getChannelPrefix();
		final Double origHffSetting = e.getHffSetting();
		final Double origImpedence = e.getImpedance();
		final Double origLffSetting = e.getLffSetting();
		final Location origLobe = e.getLocation();
		final Boolean origNotchFilter = e.getNotchFilter();
		final Double origSamplingRate = e.getSamplingRate();
		final Side origSide = e.getSide();

		boolean exception = false;
		try {
			assembler.modifyEntity(e, gwtE);
		} catch (StaleObjectException soe) {
			exception = true;
		}
		assertTrue(exception);
		assertEquals(origId, e.getId());
		assertEquals(origVer, e.getVersion());
		assertEquals(origHffSetting, e.getHffSetting());
		assertEquals(origPrefix, e.getChannelPrefix());
		assertEquals(origImpedence, e.getImpedance());
		assertEquals(origLffSetting, e.getLffSetting());
		assertEquals(origLobe, e.getLocation());
		assertEquals(origNotchFilter, e.getNotchFilter());
		assertEquals(origSamplingRate, e.getSamplingRate());
		assertEquals(origSide, e.getSide());

	}
}
