/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.client.presenter;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasValue;

import edu.upenn.cis.braintrust.BuildDto;
import edu.upenn.cis.braintrust.client.BrainTrustServiceAsync;
import edu.upenn.cis.braintrust.client.presenter.StudyPresenter.IStudyView;
import edu.upenn.cis.braintrust.client.view.EditRemoveButtonPair;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;

/**
 * Tests for {@code StudyPresenter}.
 * 
 * @author John Frommeyer
 * 
 */
public class StudyPresenterTest {

	// Presenter Dependencies
	@Mock
	private HandlerManager eventBus;
	@Mock
	private BrainTrustServiceAsync rpcService;
	@Mock
	private IStudyView display;

	// Sub Views of IStudyView
	@Mock
	private HasClickHandlers saveButton;
	@Mock
	private HasClickHandlers cancelButton;
	@Mock
	private IHasValueAndErrorMessage<Long> startDateView;
	@Mock
	private IHasValueAndErrorMessage<Long> endDateView;
	@Mock
	private HasClickHandlers addElectrodeButton;
	@Mock
	private IHasValueAndErrorMessage<String> studyLabelView;
	@Mock
	private IHasValueAndErrorMessage<String> studyDirView;
	@Mock
	private IHasValueAndErrorMessage<String> mefDirView;
	@Mock
	private IHasValueAndErrorMessage<String> mafFileView;
	@Mock
	private IHasValueAndErrorMessage<String> reportFileView;
	@Mock
	private IHasValueAndErrorMessage<String> imagesFileView;
	@Mock
	private IHasValueAndErrorMessage<String> refElectrodeDescriptionView;
	@Mock
	private HasValue<Integer> szCountView;
	@Mock
	private ISingleSelectionList studyTypeView;
	@Mock
	private HasValue<Boolean> publishedView;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	@Ignore("GWT-2.5.1")
	public void testRemoveElectrode() {
		final GwtPatient patient = BuildDto
				.buildGwtPatientWithSingleAdmission();
		final GwtHospitalAdmission admission = getOnlyElement(patient
				.getAdmissions());
		final GwtEegStudy study = getOnlyElement(admission.getStudies());
		Map<Long, HasClickHandlers> idToRemoveButton = newHashMap();
		initViewFromStudy(study, idToRemoveButton);
		new StudyPresenter(
				eventBus, 
				display,
				rpcService, 
				study);
		final GwtContactGroup electrode = getFirst(study.getRecording().getContactGroups(), null);
		final HasClickHandlers removeButton = idToRemoveButton.get(electrode
				.getId());
		ArgumentCaptor<ClickHandler> handlerArgCaptor = ArgumentCaptor
				.forClass(ClickHandler.class);
		verify(removeButton).addClickHandler(handlerArgCaptor.capture());
		final ClickHandler removeClickHandler = handlerArgCaptor.getValue();
		final ClickEvent clickEvent = mock(ClickEvent.class);
		when(clickEvent.getSource()).thenReturn(removeButton);
		removeClickHandler.onClick(clickEvent);
		assertTrue(study.getRecording().getContactGroups().contains(electrode));
		assertTrue(electrode.isDeleted());
		verify(display).removeContactGroup(electrode.getId());
	}

	@Test
	@Ignore("We don't yet support locking down studies.")
	public void testUnmodifiableStudy() {
		final GwtPatient patient = BuildDto
				.buildGwtPatientWithSingleAdmission();
		final GwtHospitalAdmission admission = getOnlyElement(patient
				.getAdmissions());
		final GwtEegStudy study = getOnlyElement(admission.getStudies());
		study.setModifiable(false);
		Map<Long, HasClickHandlers> idToRemoveButton = newHashMap();
		initViewFromStudy(study, idToRemoveButton);
		new StudyPresenter(
				eventBus, 
				display,
				rpcService, 
				study);
		verify(display).setUnmodifiable();
	}

	private void initViewFromStudy(final GwtEegStudy study,
			Map<Long, HasClickHandlers> idToRemoveButton) {

		// Save button
		when(display.getSaveButton()).thenReturn(saveButton);

		// Cancel button
		when(display.getCancelButton()).thenReturn(cancelButton);

		// Start Date View
		when(startDateView.getValue()).thenReturn(study.getRecording().getStartTimeUutc());
		when(display.getStartDateView()).thenReturn(startDateView);

		// End Date View
		when(endDateView.getValue()).thenReturn(study.getRecording().getEndTimeUutc());
		when(display.getEndDateView()).thenReturn(endDateView);

		// Add Electrode Button
		when(display.getAddContactGroupButton()).thenReturn(addElectrodeButton);

		// Study Label box
		when(studyLabelView.getValue()).thenReturn(study.getLabel());
		when(display.getStudyLabelView()).thenReturn(studyLabelView);

		// Study dir box
		when(studyDirView.getValue()).thenReturn(study.getRecording().getDir());
		when(display.getStudyDirView()).thenReturn(studyDirView);

		// Mef dir box
		when(mefDirView.getValue()).thenReturn(study.getRecording().getMefDir());
		when(display.getMefDirView()).thenReturn(mefDirView);

		// Report file box
		when(reportFileView.getValue()).thenReturn(study.getRecording().getReportFile());
		when(display.getReportFileView()).thenReturn(reportFileView);

		// Images file box
		when(imagesFileView.getValue()).thenReturn(study.getRecording().getImagesFile());
		when(display.getImagesFileView()).thenReturn(imagesFileView);
		
		//reference electrode description box
		when(display.getRefElectrodeDescriptionView()).thenReturn(refElectrodeDescriptionView);

		// Sz count
		when(szCountView.getValue()).thenReturn(study.getSzCount().orNull());
		when(display.getSeizureCount()).thenReturn(szCountView);

		// Study Type list
		when(display.getStudyTypeView()).thenReturn(studyTypeView);

		// Published CheckBox
		when(display.getPublishedView()).thenReturn(publishedView);

		// Edit and remove electrode buttons
		for (final GwtContactGroup electrode : study.getRecording().getContactGroups()) {
			final HasClickHandlers editElectrodeButton = mock(HasClickHandlers.class);
			final HasClickHandlers removeElectrodeButton = mock(HasClickHandlers.class);
			final EditRemoveButtonPair buttonPair = new EditRemoveButtonPair(
					editElectrodeButton, removeElectrodeButton);
			final String description = electrode.getChannelPrefix() + ": "
					+ electrode.getSide().toString() + " "
					+ electrode.getLocation().toString();
			when(
					display.addContactGroup(electrode.getId(), description,
							study.isModifiable()))
					.thenReturn(
							buttonPair);
			if (idToRemoveButton != null) {
				idToRemoveButton.put(electrode.getId(), removeElectrodeButton);
			}
		}
	}
}
