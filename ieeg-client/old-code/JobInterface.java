/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;

import com.google.common.base.Throwables;

public class JobInterface extends ServerInterface {
	private IJobTracker jobTracker;
	private static IJobTracker lastJobTracker;
	private static String lastURL = null;
	private static String lastPassword = null;
	private static String lastUserID = null;
	private static String workerID = null;

	/**
	 * Constructor for local services
	 * 
	 * @param url
	 * @param userid
	 * @param password
	 */
	public JobInterface(String userid, String password, IJobTracker tracker) {
		super(userid, password);

		jobTracker = tracker;

		if (workerID == null) {
			try {
				InetAddress addr = InetAddress.getLocalHost();

				workerID = addr.getCanonicalHostName();
			} catch (UnknownHostException e) {
				throw Throwables.propagate(e);
			}
		}
	}

	/**
	 * Constructor for remote server via Web services
	 * 
	 * @param url
	 * @param userid
	 * @param password
	 */
	public JobInterface(String url, String userid, String password) {
		super(url, userid, password);
		// Reuse lastTimeSeries only if it is set and is for the same
		// uid/password/url
		if (lastJobTracker == null
				||
				!(url.equals(lastURL))
				||
				!(userid.equals(lastUserID) || !(password.equals(lastPassword)))) {
			jobTracker = ProxyFactory.create(
					IJobTracker.class,
					url,
					new ApacheHttpClient4Executor(httpClient));

			lastPassword = password;
			lastUserID = userid;
			lastURL = url;
			lastJobTracker = jobTracker;
		} else
			jobTracker = lastJobTracker;

		if (workerID == null) {
			try {
				InetAddress addr = InetAddress.getLocalHost();

				workerID = addr.getCanonicalHostName();
			} catch (UnknownHostException e) {
				throw Throwables.propagate(e);
			}
		}
	}

	/**
	 * Request a snapshot ID of a job that still has work to be done: used by
	 * worker nodes
	 * 
	 * @return
	 */
	public String getNewJob() {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, workerID);

		return jobTracker.getNewJob(username, timestamp, signature, workerID);
	}

	/**
	 * Request a snapshot ID of a job that still has work to be done: used by
	 * worker nodes
	 * 
	 * @return
	 */
	public String getNewJobForTool(final String toolRevID) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, toolRevID, workerID);

		return jobTracker.getNewJobForTool(username, timestamp, signature,
				toolRevID, workerID);
	}

	public boolean scheduleJob(final String snapshotID) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, snapshotID);

		return jobTracker.scheduleJob(username, timestamp, signature, snapshotID);
	}

	/**
	 * Requests an unprocessed task ID for the data snapshot
	 * 
	 * @param dataSnapshotRevId
	 * @return
	 */
	public TimeSeriesTask getFirstTimeSeriesTask(String dataSnapshotRevId) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				workerID);

		TimeSeriesTask task = jobTracker.getFirstTimeSeriesTask(username,
				timestamp, signature, dataSnapshotRevId, workerID);

		return task;

	}

	/**
	 * Marks that one task has been done, and requestsan unprocessed task ID for
	 * the data snapshot
	 * 
	 * @param dataSnapshotRevId
	 * @return
	 */
	public TimeSeriesTask getNextTimeSeriesTask(String dataSnapshotRevId,
			String completedTask) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				workerID, completedTask);

		TimeSeriesTask task = jobTracker.getNextTimeSeriesTask(username,
				timestamp, signature, dataSnapshotRevId, workerID,
				completedTask);

		return task;
	}

	/**
	 * Returns the full XML descriptor of the data snapshot (and optionally the
	 * task)
	 * 
	 * @param dataSnapshotRevId
	 * @param taskId
	 * @return
	 */
	public String getTaskXML(String dataSnapshotRevId) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId);

		return jobTracker.getTaskXML(username, timestamp, signature,
				dataSnapshotRevId);
	}

	public String addOutputTimeSeries(String dataSnapshotRevId, String channelId) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				channelId);

		return jobTracker.addOutputTimeSeries(username, timestamp, signature,
				dataSnapshotRevId, channelId);
	}

	public String addOutputAnnotations(String dataSnapshotRevId,
			TimeSeriesAnnotationList annotations) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				annotations);

		return jobTracker.addOutputAnnotations(username, timestamp, signature,
				dataSnapshotRevId, annotations);
	}

	public String removeOutputTimeSeries(String dataSnapshotRevId,
			String channelId) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				channelId);

		return jobTracker.removeOutputTimeSeries(username, timestamp, signature,
				dataSnapshotRevId, channelId);
	}

	public String removeOutputAnnotations(String dataSnapshotRevId,
			String[] annotations) {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp, dataSnapshotRevId,
				Arrays.toString(annotations));

		return jobTracker.removeOutputAnnotations(username, timestamp, signature,
				dataSnapshotRevId, annotations);
	}

	public JobInformation[] getActiveJobs()
			throws IllegalArgumentException {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp);

		return jobTracker.getActiveJobs(username, timestamp, signature)
				.getArray();
	}

	public boolean terminateJob(final String snapshotID)
			throws IllegalArgumentException {
		long timestamp = System.currentTimeMillis();

		String signature = getRequestSignature(timestamp);

		return jobTracker
				.terminateJob(username, timestamp, signature, snapshotID)
				.getResult();
	}
}
