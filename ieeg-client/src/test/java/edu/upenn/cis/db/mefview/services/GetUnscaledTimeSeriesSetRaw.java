/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.services;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.getOnlyElement;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

/**
 * Arguments are: url (e.g. https://view.host), username, configDir, password,
 * studyName, startTimeUsec, durationUsec, outfile
 * 
 * @author John Frommeyer
 * 
 */
public class GetUnscaledTimeSeriesSetRaw {
	private static String url;
	private static String username;
	private static String password;
	private static String configDir;
	private static long startTimeMicros;
	private static String outfile;

	public static void usage() {
		System.out
				.println("Usage: url username password configDir studyName startTimeUsec durationUsec outfile [channel]");

	}

	private static long durationMicros;
	private static String studyName;
	private static String channelLabel;

	public static void main(final String[] args) throws IOException {
		if (args.length > 0
				&& (args[0].equals("-h") || args[0].equals("/h")
						|| args[0].equals("--h") ||
				args[0].equals("--help"))) {
			usage();
		}

		if (args.length >= 1)
			url = args[0];
		if (args.length >= 2)
			username = args[1];
		if (args.length >= 3)
			password = args[2];

		if (password.length() == 0) {
			usage();
			throw new RuntimeException("Password not specified");
		}
		if (args.length >= 4)
			configDir = args[3];
		if (args.length >= 5) {
			studyName = args[4];
		}

		if (args.length >= 6) {
			startTimeMicros = Long.valueOf(args[5]);
		}

		if (args.length >= 7) {
			durationMicros = Long.valueOf(args[6]);
		}
		if (args.length >= 8) {
			outfile = args[7];
		}
		if (args.length >= 9) {
			channelLabel = args[8];
		}
		System.out.println("Connecting to Web Service, user = " + username);
		final TimeSeriesInterface tsInterface = TimeSeriesInterface
				.newInstance(
						url + "/services",
						username,
						password,
						configDir);
		PrintWriter writer = new PrintWriter(new FileWriter(outfile));
		processTimeSeries(tsInterface, writer);
		writer.close();
	}

	private static void processTimeSeries(
			final TimeSeriesInterface tsInterface, PrintWriter writer) {

		String studyId = tsInterface.getDataSnapshotIdByName(studyName);

		final List<TimeSeriesDetails> datasetTimeSeriesDetails =
				tsInterface.getDataSnapshotTimeSeriesDetails(studyId);
		final ImmutableMap.Builder<String, TimeSeriesDetails> labelToTsDetailBuilder = ImmutableMap
				.builder();
		Optional<Double> periodMicrosOpt = Optional.absent();
		for (final TimeSeriesDetails tsDetail : datasetTimeSeriesDetails) {
			if (channelLabel == null
					|| channelLabel.equals(tsDetail.getLabel())) {
				labelToTsDetailBuilder.put(tsDetail.getLabel(), tsDetail);
				periodMicrosOpt = Optional.of(1e6 / tsDetail.getSampleRate());
			}
		}
		final ImmutableMap<String, TimeSeriesDetails> labelToTsDetail = labelToTsDetailBuilder
				.build();
		double reqStartMicros = startTimeMicros;

		final ImmutableCollection<TimeSeriesDetails> requestedLabels = labelToTsDetail
				.values();
		final long requestMicros = (long) Math.floor(130e6 / requestedLabels
				.size());
		while (reqStartMicros - startTimeMicros < durationMicros) {
			UnscaledTimeSeriesSegment[][] segments =
					tsInterface
							.getUnscaledTimeSeriesSetRaw(
									studyId,
									TimeSeriesDetails
											.toTimeSeriesIdAndDCheck(requestedLabels),
									reqStartMicros,
									requestMicros,
									1);
			for (int i = 0; i < segments.length; i++) {
				final List<UnscaledTimeSeriesSegment> segmentList = Arrays
						.asList(segments[i]);
				UnscaledTimeSeriesSegment segment = getOnlyElement(segmentList);
				Integer[] series = segment.toArray();
				if (!periodMicrosOpt.isPresent()) {
					writer.print(Iterables.get(labelToTsDetail.keySet(), i)
							+ ", ");
					final String asArray = Arrays.toString(series);
					writer.println(asArray.substring(1,
							asArray.length() - 1));
				} else {
					final Double periodMicros = periodMicrosOpt.get();
					for (int s = 0; s < series.length; s++) {
						double offsetMicros = reqStartMicros + s
								* periodMicros;
						if (series[s] == null) {
							// writer.print(offsetMicros + "Nan\n");
						} else {
							writer.print(offsetMicros + " " + series[s]
									+ (s == 0 ? " 1" : " 0") + "\n");
						}
					}
				}

			}
		}
		reqStartMicros += requestMicros;

	}
}
