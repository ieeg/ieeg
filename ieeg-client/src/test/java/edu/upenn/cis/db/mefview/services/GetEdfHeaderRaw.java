/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.services;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Iterables.getOnlyElement;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import edu.upenn.cis.eeg.edf.EDFHeader;

/**
 * Arguments are: url (e.g. https://view.host), username, configDir, password,
 * studyName
 * 
 * @author John Frommeyer
 * 
 */
public class GetEdfHeaderRaw {
	private static String url;
	private static String username;
	private static String password;
	private static String configDir;

	public static void usage() {
		System.out
				.println("Usage: url username password configDir studyName");

	}

	private static String studyName;

	public static void main(final String[] args) throws IOException {
		if (args.length > 0
				&& (args[0].equals("-h") || args[0].equals("/h")
						|| args[0].equals("--h") ||
				args[0].equals("--help"))) {
			usage();
		}

		if (args.length >= 1)
			url = args[0];
		if (args.length >= 2)
			username = args[1];
		if (args.length >= 3)
			password = args[2];

		if (password.length() == 0) {
			usage();
			throw new RuntimeException("Password not specified");
		}
		if (args.length >= 4)
			configDir = args[3];
		if (args.length >= 5) {
			studyName = args[4];
		}

		System.out.println("Connecting to Web Service, user = " + username);
		final TimeSeriesInterface tsInterface = TimeSeriesInterface
				.newInstance(
						url + "/services",
						username,
						password,
						configDir);
		processTimeSeries(tsInterface);
	}

	private static void processTimeSeries(
			final TimeSeriesInterface tsInterface) {

		String studyId = tsInterface.getDataSnapshotIdByName(studyName);

		final EDFHeader edfHeader = tsInterface.getEdfHeaderRaw(studyId);
		
		System.out.println(edfHeader.toString());

	}
}
