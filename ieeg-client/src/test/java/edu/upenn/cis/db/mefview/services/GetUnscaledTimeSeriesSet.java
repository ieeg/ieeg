/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.services;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Arguments are: url (e.g. https://view.host), username, configDir password, studyPubId, startTimeUsec, durationUsec, outfile
 * @author John Frommeyer
 *
 */
public class GetUnscaledTimeSeriesSet {
	private static String url;
	private static String configDir;
	private static String username;
	private static String password;
	private static int startTimeUsec;
	private static String outfile;

	public static void usage() {
		System.out
				.println("Usage: url username password configDir datasetName startTimeUsec durationUsec outfile");

	}

	private static long durationUsec;
	private static String datasetName;

	public static void main(final String[] args) throws IOException {
		if (args.length > 0
				&& (args[0].equals("-h") || args[0].equals("/h")
						|| args[0].equals("--h") ||
				args[0].equals("--help"))) {
			usage();
		}

		if (args.length >= 1)
			url = args[0];
		if (args.length >= 2)
			username = args[1];
		if (args.length >= 3)
			password = args[2];

		if (password.length() == 0) {
			usage();
			throw new RuntimeException("Password not specified");
		}
		if (args.length >= 4)
			configDir = args[3];
		if (args.length >= 5) {
			datasetName = args[4];
		}

		if (args.length >= 6) {
			startTimeUsec = Integer.valueOf(args[5]);
		}

		if (args.length >= 7) {
			durationUsec = Long.valueOf(args[6]);
		}
		if (args.length >= 8) {
			outfile = args[7];
		}
		System.out.println("Connecting to Web Service, user = " + username);
		final TimeSeriesInterface tsInterface = TimeSeriesInterface.newInstance(
				url + "/services",
				username,
				password,
				configDir);
		PrintWriter writer = new PrintWriter(new FileWriter(outfile));
		processTimeSeries(tsInterface, writer);
		writer.close();
	}

	private static void processTimeSeries(final TimeSeriesInterface tsInterface, 
			PrintWriter writer) {
		
		String datasetId = tsInterface.getDataSnapshotIdByName(datasetName);

		final List<TimeSeriesDetails> datasetTimeSeriesDetails = tsInterface
				.getDataSnapshotTimeSeriesDetails(datasetId);
		final List<String> tsIds = newArrayList();
		final List<String> labels = newArrayList();
		final List<FilterSpec> filters = newArrayList();
		for (final TimeSeriesDetails tsDetail : datasetTimeSeriesDetails) {
			tsIds.add(tsDetail.getRevId());
			labels.add(tsDetail.getLabel());
			filters.add(new FilterSpec());
		}


		UnscaledTimeSeriesSegment[][] segments = tsInterface
				.getUnscaledTimeSeriesSet(
						datasetId,
						tsIds.toArray(new String[0]),
						filters.toArray(new FilterSpec[0]),
						startTimeUsec,
						durationUsec,
						200,
						null);

		for (int i = 0; i < segments.length; i++) {
			final List<UnscaledTimeSeriesSegment> segmentList = Arrays
					.asList(segments[i]);
			UnscaledTimeSeriesSegment segment = getOnlyElement(segmentList);
			Integer[] series = segment.toArray();
			writer.print(labels.get(i) + ", ");
			final String asArray = Arrays.toString(series);
			writer.println(asArray.substring(1, asArray.length() - 1));

		}

	}

}
