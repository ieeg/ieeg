/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.db.mefview.testhelper.ClientTstObjectFactory;

/**
 * @author John Frommeyer
 *
 */
@GwtIncompatible("A test")
public class EEGMontageTest {

	private final ClientTstObjectFactory tstObjFac = new ClientTstObjectFactory();

	@Test
	public void jaxbMarshallAndUnmarshall() throws JAXBException {
		final EEGMontage expectedMontage = tstObjFac.newEEGMontage();
		expectedMontage.setServerId((long) tstObjFac.randomNonnegInt());
		expectedMontage.setIsPublic(true);

		for (int i = 0; i < 3; i++) {
			expectedMontage.getPairs().add(tstObjFac.newEEGMontagePair());
		}
		expectedMontage.getPairs().add(
				new EEGMontagePair(tstObjFac.newUuid(), null));

		final JAXBContext jaxbContext = JAXBContext
				.newInstance(EEGMontage.class);

		final Marshaller marshaller = jaxbContext.createMarshaller();
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		final StringWriter stringWriter = new StringWriter();
		marshaller.marshal(expectedMontage, stringWriter);

		final EEGMontage actualMontage = (EEGMontage) unmarshaller
				.unmarshal(new StringReader(stringWriter.toString()));

		assertEquals(
				expectedMontage.getName(),
				actualMontage.getName());
		assertEquals(
				expectedMontage.getServerId(),
				actualMontage.getServerId());
		assertEquals(
				expectedMontage.getIsPublic(),
				actualMontage.getIsPublic());
		assertEquals(
				expectedMontage.getPairs().size(),
				actualMontage
						.getPairs().size());

		for (int i = 0; i < expectedMontage.getPairs().size(); i++) {
			final EEGMontagePair expectedPair = expectedMontage.getPairs().get(
					i);
			final EEGMontagePair actualPair = actualMontage.getPairs().get(
					i);
			assertEquals(
					expectedPair.getEl1(),
					actualPair.getEl1());
			assertEquals(
					expectedPair.getEl2(),
					actualPair.getEl2());
		}
	}
}
