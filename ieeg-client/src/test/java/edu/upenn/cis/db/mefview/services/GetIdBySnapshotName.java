/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.services;

import java.io.IOException;

/**
 * Arguments are: url (e.g. https://view.host), username, password, configDir,
 * snapshotName
 * 
 * @author John Frommeyer
 * 
 */
public class GetIdBySnapshotName {
	private static String url;
	private static String configDir;
	private static String username;
	private static String password;

	public static void usage() {
		System.out
				.println("Usage: url username configDir password snapshotName");

	}

	private static String snapshotName;

	public static void main(final String[] args) throws IOException {
		if (args.length != 5 ||
				(args.length > 0
				&& (args[0].equals("-h") || args[0].equals("/h")
						|| args[0].equals("--h") ||
				args[0].equals("--help")))) {
			usage();
			System.exit(0);
		}

		if (args.length >= 1)
			url = args[0];
		if (args.length >= 2)
			username = args[1];
		if (args.length >= 3)
			configDir = args[2];
		if (args.length >= 4)
			password = args[3];

		if (password.length() == 0) {
			usage();
			throw new RuntimeException("Password not specified");
		}
		if (args.length >= 5) {
			snapshotName = args[4];
		}

		System.out.println("Connecting to Web Service, user = " + username);
		final TimeSeriesInterface tsInterface = TimeSeriesInterface
				.newInstance(
						url + "/services",
						username,
						password,
						configDir);
		String snapshotId = tsInterface.getDataSnapshotIdByName(snapshotName);
		System.out.println(snapshotName + "'s id is " + snapshotId);
	}
}
