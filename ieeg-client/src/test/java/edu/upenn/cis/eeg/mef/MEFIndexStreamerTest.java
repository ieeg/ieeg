/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.mef;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

public class MEFIndexStreamerTest {

	private static final int[] ch06FirstSampleInBlocks = {
			14, 124, -207, -135, 149, 148, -31, -62, 69, -160, 1, -196, -233,
			-216, 46, -208, 451, -28, -318, 232, -84, -350, 534 };

	@Test
	public void testDecompress() throws Exception {
		MEFIndexStreamer mefStreamer = null;
		try {
			File mefFile =
					new File("src/test/resources/ch_06.mef");
			mefStreamer = new MEFIndexStreamer(mefFile, true);
			List<TimeSeriesPage> pages;
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(23, pages.size());
			for (int i = 0; i < 23; i++) {
				assertEquals(ch06FirstSampleInBlocks[i], pages.get(i).values[0]);
			}
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(0, pages.size());
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(0, pages.size());
			return;
		} finally {
			if (mefStreamer != null) {
				mefStreamer.close();
			}
		}
	}

	@Test
	public void testNoDecompress() throws Exception {
		MEFIndexStreamer mefStreamer = null;
		try {
			File mefFile =
					new File("src/test/resources/ch_06.mef");
			mefStreamer = new MEFIndexStreamer(mefFile, false);
			List<TimeSeriesPage> pages;
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(23, pages.size());
			for (TimeSeriesPage page : pages) {
				assertEquals(0, page.values.length);
			}
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(0, pages.size());
			pages = mefStreamer.getNextBlocks(90);
			assertEquals(0, pages.size());
			return;
		} finally {
			if (mefStreamer != null) {
				mefStreamer.close();
			}
		}
	}
}
