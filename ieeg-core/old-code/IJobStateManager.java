/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import edu.upenn.cis.braintrust.datasnapshot.JobNotFoundException;
import edu.upenn.cis.braintrust.datasnapshot.TaskNotFoundException;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.braintrust.security.UserId;
import edu.upenn.cis.db.mefview.server.jobs.JobCoordinator.UserJobs;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotationList;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;

public interface IJobStateManager {
	/**
	 * Assigns new job with no tasks.
	 * 
	 * @param u
	 * @param j
	 */
	public void assignNewJob(User u, Job j);

	/**
	 * Add the new {@code TimeSeriesTask}s {@code tasks} to {@code j}.
	 * 
	 * @param j
	 * @param tasks
	 * @throws JobNotFoundException if no job matching {@code j} can be found
	 * @throws IllegalArgumentException if the job already has tasks associated
	 *             with it
	 */
	public void initializeJob(Job j, List<TimeSeriesTask> tasks);

	/**
	 * 
	 * Updates the status only of task in job {@code j} matching {@code task}.
	 * 
	 * @param u
	 * @param j
	 * @param task
	 * @throws JobNotFoundException if no job matching {@code j} can be found
	 * @throws TaskNotFoundException if there is no task matching {@code task}
	 */
	public void updateTaskStatus(User u, Job j, TimeSeriesTask task);

	/**
	 * 
	 * Modify {@code j}'s tasks to match {@code tasks}. No changes to {@code j}.
	 * 
	 * @param u
	 * @param j
	 * @param tasks
	 * @throws JobNotFoundException if no job matching {@code j} can be found
	 * @throws TaskNotFoundException if some task in {@code tasks} cannot be
	 *             found for the given job
	 */
	public void updateJob(User u, Job j, List<TimeSeriesTask> tasks);

	/**
	 * Returns a list of userIds with active jobs.
	 * 
	 * @return a list of userIds with active jobs
	 */
	public List<UserId> getUserIdsWithActiveJobs();

	/**
	 * Return unscheduled and running jobs for {@code u}.
	 * 
	 * @param u
	 * @return
	 */
	public List<Job> getJobsForUser(User u);

	/**
	 * Return all tasks of any status for {@code j}.
	 * 
	 * @param u
	 * @param j
	 * @return all tasks of any status for {@code j}
	 * @throws JobNotFoundException if no job matching {@code j} can be found
	 */
	public List<TimeSeriesTask> getTasksForJob(User u, Job j);

	/**
	 * Removes job {@code j}
	 * 
	 * @param u
	 * @param j
	 * @throws JobNotFoundException if no job matching {@code j} can be found
	 */
	public void removeCompletedJob(User u, Job j);

	/**
	 * Returns all active jobs along with their tasks, grouped by UserId.
	 * 
	 * @return all active jobs along with their tasks, grouped by UserId
	 */
	public Map<UserId, UserJobs> getAllJobs();
	
	public String logAddedTimeSeries(User u, String dataSnapshotRevId,
			String channelId)
			throws AuthorizationException, SAXException, IOException;

	public String logRemovedTimeSeries(User u, String dataSnapshotRevId,
			String channelId)
			throws AuthorizationException, SAXException, IOException;

	public String logAddedAnnotations(User u, String dataSnapshotRevId,
			TimeSeriesAnnotationList annotations)
			throws AuthorizationException, SAXException, IOException;

	public String logRemovedAnnotations(User u, String dataSnapshotRevId,
			String[] annotations)
			throws AuthorizationException, SAXException, IOException;

}
