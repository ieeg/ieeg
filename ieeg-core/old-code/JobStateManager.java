/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static edu.upenn.cis.braintrust.BtUtil.diffNowMsThenSeconds;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.IJobDAO;
import edu.upenn.cis.braintrust.dao.IJobHistoryDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.JobNotFoundException;
import edu.upenn.cis.braintrust.datasnapshot.JobStatus;
import edu.upenn.cis.braintrust.datasnapshot.ParallelDto;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.JobHistory;
import edu.upenn.cis.braintrust.model.Task;
import edu.upenn.cis.braintrust.model.TaskHistory;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.braintrust.security.UserId;
import edu.upenn.cis.braintrust.validate.ConstraintViolationMessageUtil;
import edu.upenn.cis.db.mefview.server.jobs.JobCoordinator.UserJobs;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotationList;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;

public final class JobStateManager implements IJobStateManager {
	private final IDataSnapshotServer dsServer;
	private final SessionFactory jobDbSessFac;
	private final IDAOFactory daoFac;
	private final static Logger logger =
			LoggerFactory.getLogger(JobStateManager.class);

	public JobStateManager(
			final IDataSnapshotServer dsServer) {
		this(dsServer, HibernateUtil.getSessionFactory(),
				new HibernateDAOFactory());
	}

	public JobStateManager(
			final IDataSnapshotServer dsServer,
			SessionFactory jobDbSessFac,
			IDAOFactory daoFac) {
		this.dsServer = checkNotNull(dsServer);
		this.jobDbSessFac = jobDbSessFac;
		this.daoFac = daoFac;
	}

	@Override
	public void assignNewJob(User u, Job j) {
		dsServer.createJobForUser(
				j.getInitiatorId(),
				j.getDataSnapshotId(),
				j.getToolRunningId(),
				JobStatus.UNSCHEDULED,
				ParallelDto.SERIAL,
//				ToolInfoAssembler.toParallelDto(j.getParallelism()),
				j.getCreateTime());
	}
//
	@Override
	public void initializeJob(Job j, List<TimeSeriesTask> tasks) {
		dsServer.initializeJob(j.getDataSnapshotId(),
				TaskAssembler.toTaskSet(tasks));
	}

	@Override
	public void updateTaskStatus(User u, Job j, TimeSeriesTask task) {
		dsServer.updateTaskStatus(u, j.getDataSnapshotId(),
				task.getName(), TaskAssembler.toTaskStatus(task.getStatus()));
	}

	@Override
	public void updateJob(User u, Job j, List<TimeSeriesTask> tasks) {
		dsServer.updateTasks(u, j.getDataSnapshotId(),
				TaskAssembler.toTaskSet(tasks));
	}

	@Override
	public List<UserId> getUserIdsWithActiveJobs() {
		return dsServer.getUserIdsWithActiveJobs();
	}

	@Override
	public List<Job> getJobsForUser(User u) {
		Date in = new Date();
		final String M = "getJobsForUser(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			checkNotNull(u);
			sess = jobDbSessFac.openSession();
			trx = sess.beginTransaction();
			final IJobDAO jobDAO = daoFac.getJobDAO(sess);
			final List<JobEntity> entities = jobDAO.findByUser(u);
			final List<Job> jobs = newArrayList();
			for (final JobEntity entity : entities) {
				jobs.add(new Job(
						entity.getDatasetPubId(),
						new UserId(entity.getUserId()),
						entity.getToolPubId()
//						ToolInfoAssembler.toParallel(entity.getParallel())
						));
			}
			trx.commit();
			return jobs;
		} catch (final Throwable t) {
			PersistenceUtil.rollback(trx);
			ConstraintViolationMessageUtil.propagateCveAsBtcve(t);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}

	}

	@Override
	public List<TimeSeriesTask> getTasksForJob(User u, Job j) {
		final String M = "getTasksForJob(...)";
		long inTime = System.currentTimeMillis();
		Session sess = null;
		Transaction trx = null;
		try {
			sess = jobDbSessFac.openSession();
			trx = sess.beginTransaction();
			List<TimeSeriesTask> timeSeriesTasks = newArrayList();
			IJobDAO jobDAO = daoFac.getJobDAO(sess);
			JobEntity jobEntity = jobDAO.findByNaturalId(
					j.getDataSnapshotId());
			if (jobEntity == null) {
				throw new JobNotFoundException(
						j.getDataSnapshotId());
			}
			for (Task task : jobEntity.getTasks()) {
				timeSeriesTasks.add(
						new TimeSeriesTask(
								task.getName(),
								task.getTsPubIds().toArray(
										new String[task.getTsPubIds().size()]),
								task.getStartTimeMicros(),
								task.getEndTimeMicros(),
								TaskAssembler.of(task.getStatus()))
						);
			}
			trx.commit();
			return timeSeriesTasks;
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			ConstraintViolationMessageUtil.propagateCveAsBtcve(e);
			throw propagate(e);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(inTime));
		}
	}

	@Override
	public void removeCompletedJob(User u, Job j) {
		final String M = "removeCompletedJob(...)";
		long inTime = System.currentTimeMillis();
		Session sess = null;
		Transaction trx = null;
		try {
			sess = jobDbSessFac.openSession();
			trx = sess.beginTransaction();
			j.getDataSnapshotId();
			IJobDAO jobDAO = daoFac.getJobDAO(sess);
			JobEntity jobEntity =
					jobDAO.findByNaturalId(
							j.getDataSnapshotId());
			if (jobEntity == null) {
				throw new JobNotFoundException(
						j.getDataSnapshotId(),
						u.getUsername());
			}
			JobHistory jobHistEntity = new JobHistory(
					jobEntity.getUserId(),
					jobEntity.getDatasetPubId(),
					jobEntity.getToolPubId(),
					jobEntity.getStatus(),
					jobEntity.getParallel(),
					jobEntity.getCreateTime(),
					jobEntity.getClob());
			for (Task taskEntity : jobEntity.getTasks()) {
				jobHistEntity.getTasks().add(
						new TaskHistory(
								jobHistEntity,
								taskEntity.getName(),
								taskEntity.getStatus(),
								taskEntity.getStartTimeMicros(),
								taskEntity.getEndTimeMicros(),
								taskEntity.getTsPubIds(),
								taskEntity.getWorker(),
								taskEntity.getRunStartTime()));
			}
			jobDAO.delete(jobEntity);
			IJobHistoryDAO jobHistoryDAO = daoFac.getJobHistoryDAO(sess);
			jobHistoryDAO.saveOrUpdate(jobHistEntity);
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			ConstraintViolationMessageUtil.propagateCveAsBtcve(e);
			throw propagate(e);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(inTime));
		}
	}

	public Map<UserId, UserJobs> getAllJobs() {
		final String M = "getAllJobs(...)";
		long inTime = System.currentTimeMillis();
		Session sess = null;
		Transaction trx = null;
		try {
			final Map<UserId, UserJobs> userIdToUserJobs = newHashMap();
			sess = jobDbSessFac.openSession();
			trx = sess.beginTransaction();
			IJobDAO jobDAO = daoFac.getJobDAO(sess);
			final List<JobEntity> allJobs = jobDAO.findAll();
			for (final JobEntity jobEntity : allJobs) {
				final UserId userId = new UserId(jobEntity.getUserId());
				UserJobs userJobs = userIdToUserJobs.get(userId);
				if (userJobs == null) {
					userJobs = new UserJobs();
					userIdToUserJobs.put(userId, userJobs);
				}
				final Job job = new Job(
						jobEntity.getDatasetPubId(),
						userId,
						jobEntity.getToolPubId()
//						ToolInfoAssembler.toParallel(jobEntity.getParallel()
						    );
				final List<TimeSeriesTask> timeSeriesTasks = newArrayList();
				for (final Task task : jobEntity.getTasks()) {
					timeSeriesTasks.add(
							new TimeSeriesTask(
									task.getName(),
									task.getTsPubIds().toArray(
											new String[task.getTsPubIds()
													.size()]),
									task.getStartTimeMicros(),
									task.getEndTimeMicros(),
									TaskAssembler.of(task.getStatus()))
							);
				}
				userJobs.addJob(job, timeSeriesTasks);
			}
			trx.commit();
			return userIdToUserJobs;
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			ConstraintViolationMessageUtil.propagateCveAsBtcve(e);
			throw propagate(e);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(inTime));
		}
	}

	@Override
	public String logAddedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		return null;
	}

	@Override
	public String logRemovedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		return null;
	}

	@Override
	public String logAddedAnnotations(User u, String dataSnapshotRevId,
			TimeSeriesAnnotationList annotations)
			throws AuthorizationException, SAXException, IOException {
		return null;
	}

	@Override
	public String logRemovedAnnotations(User u, String dataSnapshotRevId,
			String[] annotations) throws AuthorizationException, SAXException,
			IOException {
		return null;
	}

}
