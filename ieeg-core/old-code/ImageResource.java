/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.db.mefview.services.IImageResource;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetails;

public class ImageResource extends AuthenticatingService implements
		IImageResource {
	static ImageServer images = null;
	static IDataSnapshotServer datasets = null;
	ServletContext ctx;

	public static String sourcePath = "traces/";
	public final String path = "traces/Mayo_IEED_data/Mayo_IEED_";
	public static Map<String, TimeSeriesDetails> traceDetails = Collections
			.synchronizedMap(new LruCache<String, TimeSeriesDetails>(500));

	private static final Logger logger = LoggerFactory
			.getLogger(ImageResource.class);

	/**
	 * Constructor: initialize parameters and MEF page server, based on servlet
	 * context parameters
	 * 
	 * @param context
	 */
	public ImageResource(
			@Context ServletContext context// ,
	// @Context HttpServletRequest req
	) {
		ctx = context;

		boolean prefetch = false;

		if (images == null) {
			@SuppressWarnings("unchecked")
			Map<String, String> ivProps = ((Map<String, String>)
					context.getAttribute(IvProps.ATTR_KEY));

			sourcePath = BtUtil.get(ivProps, IvProps.SOURCEPATH, sourcePath);
			prefetch = BtUtil.getBoolean(ivProps, IvProps.PREFETCH,	prefetch);
			final String imagesBucket = BtUtil.get(ivProps,
					IvProps.IMAGES_BUCKET, IvProps.IMAGES_BUCKET_DFLT);
			final String imagesPath = BtUtil.get(ivProps, IvProps.IMAGES_PATH,
					IvProps.IMAGES_PATH_DFLT);
			final int imagesExpHrs = BtUtil.getInt(ivProps,
					IvProps.IMAGES_EXP_HRS, IvProps.IMAGES_EXP_HRS_DFLT);


			if (datasets == null)
				datasets = DataSnapshotServer.getInstance();
			IUrlFactory imageUrlFactory = new S3PresignedUrlFactory(
					imagesBucket, imagesPath, imagesExpHrs);

			images = new ImageServer(datasets, imageUrlFactory);
		}
	}

	@Override
	public Response getDataSnapshotImage(
			final String userid,
			final long timestamp,
			final String signature,
			final String dataSnapshot,
			final String imageName)
			throws IllegalArgumentException {

		try {
			if (isValid(userid, timestamp, signature, dataSnapshot, imageName)) {
				try {
					return Response.ok(images.getPng(imageName, -1, -1)).
							build();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return Response.noContent().build();
				} catch (IOException e1) {
					e1.printStackTrace();
					return Response.noContent().build();
				}
			} else
				throw new IllegalStateException("Invalid signature");
		} catch (Throwable t) {
			logger.error("Caught exeption", t);
			throw new IllegalStateException(t);
		}
	}

	@Override
	public Response getDataSnapshotScaledImage(
			final String userid,
			final long timestamp,
			final String signature,
			final String dataSnapshot,
			final String imageName,
			final int width,
			final int height)
			throws IllegalArgumentException {

		try {
			if (isValid(userid, timestamp, signature, dataSnapshot, imageName,
					width, height)) {
				try {
					return Response.ok(images.getPng(imageName, width, height))
							.
							build();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return Response.noContent().build();
				} catch (IOException e1) {
					e1.printStackTrace();
					return Response.noContent().build();
				}
			} else
				throw new IllegalStateException("Invalid signature");
		} catch (Throwable t) {
			logger.error("Caught exeption", t);
			throw new IllegalStateException(t);
		}
	}

	@Override
	public String addImageToDataSnapshot(
			String dataSnapshot,
			String imageName) {
		// TODO Auto-generated method stub
		return null;
	}

}
