/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * 
 * 
 * @author John Frommeyer
 * 
 */
public class JobXmlTest {
	@Mock
	private IDataSnapshotServer server;

	private final TstObjectFactory objFac = new TstObjectFactory(true);

	@Before
	public void setUpMock() {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getDuration() throws AuthorizationException, SAXException,
			IOException {
		final User u = objFac.newUserRegularEnabled();
		final String snapshotId = "123";
		final String clobPath = JobXmlTest.class
				.getClassLoader()
				.getResource("dsClob.xml")
				.getPath();
		final File clobFile = new File(clobPath);
		final String xml = Files.toString(clobFile, Charsets.UTF_8);
		when(server.getClob(u, snapshotId)).thenReturn(xml);
		final Document dom = JobXML.getXML(u, snapshotId, server);

		final Node n = JobXML.getChildElement(dom.getDocumentElement(),
				"input-parameters");

		// Test clob has 500 blocks of 15,000,000 microseconds each.
		long duration = JobXML.getDuration(n);
		assertEquals(7500000000L, duration);
	}
}
