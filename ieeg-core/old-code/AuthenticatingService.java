/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.drupal.DrupalUserService;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.db.mefview.services.SignatureGenerator;

public class AuthenticatingService {
	protected final IUserService dus;

	private static final SignatureGenerator sigGen = new SignatureGenerator(
			"MD5");

	public AuthenticatingService() {
		this(DrupalUserService.getInstance());
	}

	@VisibleForTesting
	AuthenticatingService(IUserService userService) {
		this.dus = userService;
	}

	/**
	 * Check that the signature matches
	 * 
	 * @param username
	 * @param timestamp
	 * @param signature
	 * @param params
	 * @return
	 * @throws IncorrectCredentialsException if no such user exists
	 * @throws DisabledAccountException if the user's account is disabled
	 */
	public boolean isValid(final String username, long timestamp,
			final String signature,
			final Object... params) {
		final User user = dus.findUserByUsername(username);

		if (user == null) {
			throw new IncorrectCredentialsException("Username " + username
					+ " not found");
		}
		if (user.getStatus() == User.Status.DISABLED) {
			throw new DisabledAccountException("User " + user.getUsername()
					+ " disabled");
		}
		String password = user.getPassword();
		String sig2 = sigGen.getRequestSignature(username, password,
				timestamp, params);
		return signature.equals(sig2);
	}


}
