/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.braintrust.security.UserId;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotationList;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;

public class InMemJobCoordinator extends JobCoordinator {

	public InMemJobCoordinator(IDataSnapshotServer datasets) {
		super(datasets);
	}

	@Override
	public void assignNewJob(User u, Job j) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initializeJob(Job j, List<TimeSeriesTask> tasks) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTaskStatus(User u, Job j, TimeSeriesTask task) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateJob(User u, Job j, List<TimeSeriesTask> tasks) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UserId> getUserIdsWithActiveJobs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Job> getJobsForUser(User u) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TimeSeriesTask> getTasksForJob(User u, Job j) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeCompletedJob(User u, Job j) {
		// TODO Auto-generated method stub

	}

	@Override
	public String logAddedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String logRemovedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String logAddedAnnotations(User u, String dataSnapshotRevId,
			TimeSeriesAnnotationList annotations)
			throws AuthorizationException, SAXException, IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String logRemovedAnnotations(User u, String dataSnapshotRevId,
			String[] annotations) throws AuthorizationException, SAXException,
			IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<UserId, UserJobs> getAllJobs() {
		final String M = "getAllJobs(...)";
		// TODO Auto-generated method stub
		return Collections.emptyMap();
	}

}
