/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.braintrust.security.UserId;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotationList;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;

/**
 * A {@code JobCoordinator} which uses a database.
 * 
 * @author John Frommeyer
 * 
 */
public class DbJobCoordinator extends JobCoordinator {

	private static final Logger logger = LoggerFactory
			.getLogger(DbJobCoordinator.class);

	private final IJobStateManager jobStateManager;
	private final IUserService userService;

	public DbJobCoordinator(IUserService userService, IDataSnapshotServer datasets) {
		super(datasets);
		this.jobStateManager = new JobStateManager(datasets);
		this.userService = userService;
		initialize();
	}

	@Override
	public void assignNewJob(User u, Job j) {
		jobStateManager.assignNewJob(u, j);
	}

	@Override
	public void initializeJob(Job j, List<TimeSeriesTask> tasks) {
		jobStateManager.initializeJob(j, tasks);
	}

	@Override
	public void updateTaskStatus(User u, Job j, TimeSeriesTask task) {
		jobStateManager.updateTaskStatus(u, j, task);
	}

	@Override
	public void updateJob(User u, Job j, List<TimeSeriesTask> tasks) {
		jobStateManager.updateJob(u, j, tasks);
	}

	@Override
	public List<UserId> getUserIdsWithActiveJobs() {
		return jobStateManager.getUserIdsWithActiveJobs();
	}

	@Override
	public List<Job> getJobsForUser(User u) {
		return jobStateManager.getJobsForUser(u);
	}

	@Override
	public List<TimeSeriesTask> getTasksForJob(User u, Job j) {
		return jobStateManager.getTasksForJob(u, j);
	}

	@Override
	public void removeCompletedJob(User u, Job j) {
		jobStateManager.removeCompletedJob(u, j);
	}

	@Override
	public String logAddedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		return jobStateManager.logAddedTimeSeries(u, dataSnapshotRevId,
				channelId);
	}

	@Override
	public String logRemovedTimeSeries(User u, String dataSnapshotRevId,
			String channelId) throws AuthorizationException, SAXException,
			IOException {
		return jobStateManager.logRemovedTimeSeries(u, dataSnapshotRevId,
				channelId);
	}

	@Override
	public String logAddedAnnotations(User u, String dataSnapshotRevId,
			TimeSeriesAnnotationList annotations)
			throws AuthorizationException, SAXException, IOException {
		return jobStateManager.logAddedAnnotations(u, dataSnapshotRevId,
				annotations);
	}

	@Override
	public String logRemovedAnnotations(User u, String dataSnapshotRevId,
			String[] annotations) throws AuthorizationException, SAXException,
			IOException {
		return jobStateManager.logRemovedAnnotations(u, dataSnapshotRevId,
				annotations);
	}

	@Override
	public Map<UserId, UserJobs> getAllJobs() {
		final String M = "getAllJobs(...)";
		return jobStateManager.getAllJobs();
	}

	private void initialize() {
		final String M = "initialize(...)";
		final Map<UserId, UserJobs> userIdtoUserJobs = getAllJobs();
		final Map<UserId, Optional<User>> idToUser = userService
				.findUsersByUid(userIdtoUserJobs.keySet());
		for (Map.Entry<UserId, UserJobs> entry : userIdtoUserJobs.entrySet()) {
			final UserId userId = entry.getKey();
			final Optional<User> optUser = idToUser.get(userId);
			if (optUser.isPresent()) {
				final User user = optUser.get();
				logger.info(
						"{}: Initalizing JobTracker with {} jobs for user {}",
						new Object[] { M, entry.getValue().getJobs().size(),
								user.getUsername() });
				putUserJobsAndTasks(user, entry.getValue());
			} else {
				logger.warn(
						"{}: UserId [{}] has an active job, but no longer exists.",
						M, userId);
			}

		}
	}

}
