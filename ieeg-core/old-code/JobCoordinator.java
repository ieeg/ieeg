/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfPossible;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newConcurrentMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshot;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.TimeSeriesDto;
import edu.upenn.cis.braintrust.datasnapshot.ToolDto;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.Role;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.db.mefview.services.JobInformation;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;
import edu.upenn.cis.db.mefview.services.TimeSeriesTaskList;

public abstract class JobCoordinator implements IJobStateManager {
	private static final Logger logger = LoggerFactory
	.getLogger(JobCoordinator.class);

	private IDataSnapshotServer datasets;
	
	private final Map<User,UserJobs> tasksRemaining = newConcurrentMap();
	
//	static final JobXML xml = new JobXML();
	
	public JobCoordinator(IDataSnapshotServer datasets) {
		this.datasets = datasets;
	}

	IDataSnapshotServer getDataSnapshotServer() { return datasets; }
	
	public static class UserJobs {
		public Map<Job,List<TimeSeriesTask>> tasks = newConcurrentMap();
		
		public UserJobs() {
			
		}
		
		public UserJobs(Job j, Collection<TimeSeriesTask> tList) {
			tasks.put(j, new ArrayList<TimeSeriesTask>());
			tasks.get(j).addAll(tList);
		}
		
		public UserJobs(Job j) {
			tasks.put(j, new ArrayList<TimeSeriesTask>());
		}
		
		public synchronized List<TimeSeriesTask> getTasks(Job j) {
			return tasks.get(j);
		}
		
		public synchronized Set<Job> getJobs() {
			return tasks.keySet();
		}
		
		public synchronized void addJob(Job j) {
			tasks.put(j, new ArrayList<TimeSeriesTask>());
		}
		
		public synchronized void addJob(Job j, TimeSeriesTask t) {
			tasks.put(j, new ArrayList<TimeSeriesTask>());
			tasks.get(j).add(t);
		}
		
		public synchronized void addJob(Job j, Collection<TimeSeriesTask> tList) {
			tasks.put(j, new ArrayList<TimeSeriesTask>());
			tasks.get(j).addAll(tList);
		}
		
		public synchronized Job getJobForTool(String jobID) {
			for (Job j : tasks.keySet())
				if (j.getToolRunningId().equals(jobID))
					return j;
			return null;
		}
		
		public synchronized Job getJobWritingTo(String dataSnapshotID) {
			for (Job j : tasks.keySet())
				if (j.getDataSnapshotId().equals(dataSnapshotID))
					return j;
			return null;
		}
		
		public synchronized TimeSeriesTask requestTask(Job j) {
			if (tasks.get(j) == null)
				return null;
			
			for (TimeSeriesTask t: tasks.get(j)) {
				if (t.getStatus() != TimeSeriesTask.STATUS.COMPLETE) {
					t.setStatus(TimeSeriesTask.STATUS.RUNNING);
					return t;
				}
			}
			return null;
		}
		
		public synchronized void killTask(Job j, TimeSeriesTask t) {
			if (tasks.get(j) != null)
				if (tasks.get(j).contains(t))
					t.setStatus(TimeSeriesTask.STATUS.KILLED);
						
		}
		
		public synchronized void setTaskComplete(Job j, TimeSeriesTask t) {
			if (tasks.get(j) != null)
				if (tasks.get(j).contains(t))
					t.setStatus(TimeSeriesTask.STATUS.COMPLETE);
						
		}
		
		public boolean removeTask(Job j, TimeSeriesTask t) {
			if (tasks.containsKey(j)) {
				synchronized (tasks.get(j)) {
					return tasks.get(j).remove(t);
				}
			}
			return false;
		}
		
		public synchronized boolean removeJob(Job j) {
			if (!tasks.containsKey(j))
				return false;
			
			tasks.remove(j);
			return true;
		}
		
		public synchronized boolean containsJob(Job j) {
			return tasks.containsKey(j);
		}

		public synchronized boolean containsJob(String jobID) {
			for (Job j : tasks.keySet())
				if (j.getDataSnapshotId().equals(jobID))
					return true;
			return false;
		}
		
		public synchronized boolean removeJob(String jobID) {
			for (Job j : tasks.keySet())
				if (j.getDataSnapshotId().equals(jobID)) {
					tasks.remove(j);
					return true;
				}
			return false;
		}
		
	}
	
//	public abstract JobInformation getJobInfo(User u, Job j);
	
//	public abstract void addJob(User u, Job j);
	
	// get total tasks
//	public abstract List<TimeSeriesTask> getTasks(User u, Job j); 

	// get total tasks
//	public abstract List<TimeSeriesTask> getTasksForTool(User u, ToolDto t); 

	public List<TimeSeriesTask> getUnscheduledTasks(User u, Job j) {
		List<TimeSeriesTask> ret = new ArrayList<TimeSeriesTask>();
		
		ret.addAll(getTasks(u, j));
		
		for (int i = ret.size() - 1; i >= 0; i--) {
			if (ret.get(i).getStatus() != TimeSeriesTask.STATUS.UNSCHEDULED)
				ret.remove(i);
		}
		
		return ret;
	}

	public List<TimeSeriesTask> getRunningTasks(User u, Job j) {
		List<TimeSeriesTask> ret = new ArrayList<TimeSeriesTask>();
		
		ret.addAll(getTasks(u, j));
		
		for (int i = ret.size() - 1; i >= 0; i--) {
			if (ret.get(i).getStatus() != TimeSeriesTask.STATUS.RUNNING)
				ret.remove(i);
		}
		
		return ret;
	}

	public List<TimeSeriesTask> getOverdueTasks(User u, Job j, int duration) {
		List<TimeSeriesTask> ret = new ArrayList<TimeSeriesTask>();
		
		ret.addAll(getTasks(u, j));
		
		for (int i = ret.size() - 1; i >= 0; i--) {
			if (!(ret.get(i).getStatus() == TimeSeriesTask.STATUS.RUNNING &&
					new Date().getTime() - ret.get(i).getStartedRunning() > duration))
				ret.remove(i);
		}
		
		return ret;
	}

	public List<TimeSeriesTask> getCompletedTasks(User u, Job j) {
		List<TimeSeriesTask> ret = new ArrayList<TimeSeriesTask>();
		
		ret.addAll(getTasks(u, j));
		
		for (int i = ret.size() - 1; i >= 0; i--) {
			if (ret.get(i).getStatus() != TimeSeriesTask.STATUS.COMPLETE)
				ret.remove(i);
		}
		
		return ret;
	}

	
	// terminate job
//	public abstract void terminate(User u, Job j);
	
	/**
	 * Retrieves the next available job, restricted by the user ID (admin users get all jobs)
	 */
//	public synchronized Job getNewJob(User thisUser, String workerId) 
//	throws IllegalArgumentException {
//		final String M = "getNewJob()";
//		logger.trace(M + ": Received job worker request for " + workerId + " from " + thisUser.getUsername());
//
//			for (User u : tasksRemaining.keySet()) {
//				if (logger.isTraceEnabled()) {
//					logger.trace(
//							"{}: tasksRemaining state: User {} has {} jobs remaining.",
//							new Object[] {
//									M,
//									u.getUsername(),
//									Integer.valueOf(tasksRemaining.get(
//											u).getJobs().size()) });
//				}
//
//				if (u.equals(thisUser) || thisUser.getRoles().contains(Role.ADMIN)) {
//
//					logger.trace(M + ": Checking jobs for "
//							+ thisUser.getUsername() + "("
//							+ thisUser.getUserId().getValue() + ")");
//					UserJobs userMap = tasksRemaining.get(u);
//
//					if (userMap != null && userMap.getJobFor(workerId) != null) {
//						Job j = userMap.getJobFor(workerId);
//						logger.trace(M + ": Allocating job " + j + " to worker " + workerId);
//						this.addJob(u, j);
//						initializeTasksForJob(u, j, getDataSnapshotServer());
//						return j;
//					}
//				}					
//			}
//		return null;
//	}
	
	/**
	 * Retrieves the next available job for the specified tool, restricted by the user ID 
	 * (admin users get all jobs)
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws AuthorizationException 
	 */
	public synchronized Job getNewJobForTool(User thisUser, String toolId, String workerId) 
	throws AuthorizationException, SAXException, IOException {
		final String M = "getNewJobForTool()";
		logger.trace(M + ": Received job worker request for " + workerId + " from " + thisUser.getUsername());

			for (User u : tasksRemaining.keySet()) {
				if (logger.isTraceEnabled()) {
					logger.trace(
							"{}: tasksRemaing state: User {} has {} tasks remaining.",
							new Object[] {
									M,
									u.getUsername(),
									Integer.valueOf(tasksRemaining.get(
											u).getJobs().size()) });
				}
				if (u.getUsername().equals(thisUser.getUsername()) || thisUser.getRoles().contains(Role.ADMIN)) {

					logger.trace(M + ": Checking jobs for " + thisUser.getUsername() + " and tool " + toolId);
					UserJobs userMap = tasksRemaining.get(u);

					if (userMap != null && userMap.getJobs().size() > 0) {
						logger.trace(M + ": Keys: " + userMap.getJobs());
						for (Job job: userMap.getJobs()) {
							Document dom = JobXML.getXML(thisUser, job.getDataSnapshotId(), getDataSnapshotServer());
							Node n = JobXML.getChildElement(dom.getDocumentElement(), "input-parameters");

							Node child = JobXML.getChildElement(n, "revID");
							if (child != null)
								logger.trace(M + ": Checking against document " + JobXML.getTextChild(child));
							if (child != null && toolId.equals(JobXML.getTextChild(child))) {
								if (userMap.getTasks(job).size() >= 0) {
									logger.trace(M + ": Allocating job " + job + " to worker " + workerId);
									
//									this.addJob(u, job);
//									initializeTasksForJob(u, job, getDataSnapshotServer());
									return job;
								}
							}
						}
					}
				}
			}

		return null;
	}

	/**
	 * Retrieves the next available job for the specified tool, restricted by the user ID 
	 * (admin users get all jobs)
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws AuthorizationException 
	 */
	public synchronized JobInformation[] getActiveJobs(User thisUser) 
	throws IllegalArgumentException, AuthorizationException, SAXException, IOException {
		final String M = "getActiveJobs()";
		ArrayList<JobInformation> retList = new ArrayList<JobInformation>();
		logger.trace(M + ": Received jobs info request for " + thisUser.getUsername());

		for (User u : tasksRemaining.keySet()) {
			if (logger.isTraceEnabled()) {
				logger.trace(
						"{}: tasksRemaing state: User {} has {} tasks remaining.",
						new Object[] {
								M,
								u.getUsername(),
								Integer.valueOf(tasksRemaining.get(
										u).getJobs().size()) });
			}
			if (u.getUserId().equals(thisUser.getUserId()) || 
					thisUser.getRoles().contains(Role.ADMIN)) {

				logger.trace(M + ": Checking jobs for " + u.getUsername());
				UserJobs userMap = this.getUserJobsAndTasks(u);

				if (userMap != null && userMap.getJobs().size() > 0) {
					logger.trace(M + ": Keys: " + userMap.getJobs());
					final List<String> toolIds = newArrayList(); 
					for (Job job: userMap.getJobs()) {
						toolIds.add(job.getToolRunningId());
					}
					final List<ToolDto> toolDtos = datasets.getTools(u, toolIds);
					int i = 0;
					for (Job job: userMap.getJobs()) {
						final String jobToolId = job.getToolRunningId();
						final ToolDto jobTool = toolDtos.get(i++);
						if (jobTool != null) {
							retList.add(new JobInformation(job.getDataSnapshotId(), jobToolId, jobTool.getDescription(), getTasks(u, job).size() - getCompletedTasks(u, job).size()));
						}
					}
				}
			}
		}
		JobInformation[] ret = new JobInformation[retList.size()];
		
		for (int i = 0; i < ret.length; i++)
			ret[i] = retList.get(i);
		
		return ret;
	}
	
	public synchronized void initializeJob(User u, String snapshotId,
			IDataSnapshotServer server) throws AuthorizationException {
		Document dom;
		try {
			dom = JobXML.getXML(u, snapshotId, server);

			Node n = JobXML.getChildElement(dom.getDocumentElement(),
					"input-parameters");

			Node child = JobXML.getChildElement(n, "revID");

			String toolId = "";
			if (child != null)
				toolId = JobXML.getTextChild(child);

			for (ToolDto t : server.getTools(u)) {
				if (t.getId().equals(toolId))
					this.addJob(u,
							new Job(snapshotId, u.getUserId(), t.getId()
									));
			}
		} catch (SAXException e) {
			propagate(e);
		} catch (IOException e) {
			propagate(e);
		}

	}	


	/**
	 * Reads the tasks from the WS XML parameters, then allocates them and persists them
	 * 
	 * @param u
	 * @param j
	 * @param server
	 * @throws AuthorizationException 
	 */
	public synchronized void initializeTasksForJob(User u, Job j,
			IDataSnapshotServer server) throws AuthorizationException {
		Document dom;
		try {
			dom = JobXML.getXML(u, j.getDataSnapshotId(), server);

			Node n = JobXML.getChildElement(dom.getDocumentElement(),
					"input-parameters");

			List<TimeSeriesTask> remainingTasks = determineTasksForJob(u, j,
					dom, n, server);

//			if (j.getParallelism() == ToolInfo.PARALLEL.SERIAL) {
//				String[] channels = new String[remainingTasks.size()];
//
//				for (int i = 0; i < remainingTasks.size(); i++)
//					channels[i] = remainingTasks.get(i).getChannels()[0];
//
//				TimeSeriesTask combinedTask = new TimeSeriesTask(remainingTasks
//						.get(0).getName(),
//						channels, remainingTasks.get(0).getStartTime(),
//						remainingTasks.get(0).getEndTime());
//				remainingTasks.clear();
//				remainingTasks.add(combinedTask);
//			}

			allocateTasksToJob(u, j, remainingTasks);
		} catch (SAXException e) {
			propagate(e);
		} catch (IOException e) {
			propagate(e);
		}
	}

	/**
	 * Reads the tasks out of the XML
	 * @param user
	 * @param dataSnapshotRevId
	 * @param dom
	 * @param n
	 * @param server
	 * @return
	 * @throws AuthorizationException
	 */
	private List<TimeSeriesTask> determineTasksForJob(final User user, final Job job, 
			final Document dom, final Node n, IDataSnapshotServer server) throws AuthorizationException {
		
		final String dataSnapshotRevId = job.getDataSnapshotId();
		// Look up the XML channel list and call up the rev IDs
		logger.trace("Initializing list of tasks for job " + dataSnapshotRevId);
		
		for (Node el = JobXML.getChildElement(n, "parameter"); el != null; el = el.getNextSibling()) {
			if (el.getNodeType() == Node.ELEMENT_NODE && el.getNodeName().equals("parameter")) {
				Node k = JobXML.getChildElement(el, "key");
				
				Node v = JobXML.getChildElement(el, "value");
				
				if (k != null && v != null) {
					if (JobXML.getTextChild(k) != null && JobXML.getTextChild(k).equals("channelNames")) {
						DataSnapshot ds = server.getDataSnapshot(user, dataSnapshotRevId);
						
						Set<TimeSeriesDto> channels = ds.getTimeSeries();
						
						List<TimeSeriesTask> ret = new ArrayList<TimeSeriesTask>();

						// Extract the channel names
						for (Node ch = v; ch != null; ch = ch.getNextSibling()) {

							String channel = JobXML.getTextChild(ch);
							if (channel != null) {
								// Add <channel-remaining>channel</channel-remaining> tags for each channel
								for (TimeSeriesDto c : channels) {
									if (c.getLabel().equals(channel)) {
										
//										n.appendChild(createJobSpecifier(dom, "task-remaining", c.getPubId(), getStartTime(n), getDuration(n)));
										logger.trace("Initial task for job " + dataSnapshotRevId + ": channel " + c.getId());
										ret.add(new TimeSeriesTask(c.getId(), c.getId(), JobXML.getStartTime(n), 
												JobXML.getStartTime(n) + JobXML.getDuration(n)));
									}
								}
							}
						}
						return ret;
					}
				}
			}
		}
		return new ArrayList<TimeSeriesTask>();
	}

	/**
	 * Assigns the tasks to the job
	 * 
	 * @param u
	 * @param remainingTasks
	 * @param dataSnapshotRevId
	 * @param workerId
	 * @param dom
	 * @param n
	 * @param server
	 */
	public void allocateTasksToJob(final User u, final Job job, final List<TimeSeriesTask> remainingTasks) {
		if (assignTasks(u, job, remainingTasks))
			this.updateJob(u, job, remainingTasks);
	}
	
	public void assignTaskToWorker(final User u, final Job job, final TimeSeriesTask task, final String workerId) {
		task.setStartedRunning(new Date().getTime(), workerId);
	}
	
	
	/**
	 * Returns the tasks for this job
	 * 
	 * @param userid
	 * @param toolRevId
	 * @return -1 if job is unscheduled, else number of remaining tasks
	 * @throws AuthorizationException
	 */
	public List<TimeSeriesTask> getTasks(User u, String toolRevId) throws AuthorizationException {
		UserJobs map = tasksRemaining.get(u);
		
		if (map != null) {
			Job j = map.getJobForTool(toolRevId);
			
			if (j != null)
			return getTasks(u, j);
		}
		return none;
	}
	
//	private List<TimeSeriesTask> getJobTasks(User u, Job j) {
//		if (tasksRemaining.get(u) == null)
//			return null;
//		else
//			return tasksRemaining.get(u).getTasks(j);
//	}
	
	UserJobs getUserJobsAndTasks(User u) {
		return tasksRemaining.get(u);
	}
	
	UserJobs getUserJobsAndTasksForAdmin(User admin, Job j) {
		checkArgument(admin.getRoles().contains(Role.ADMIN),
				"User " + admin.getUsername() + " is not an admin.");
		for (final Map.Entry<User, UserJobs> entry : tasksRemaining.entrySet()) {
			UserJobs userJobs = entry.getValue();
			for (final Job job : userJobs.getJobs()) {
				if (job.getDataSnapshotId().equals(j.getDataSnapshotId())) {
					return userJobs;
				}
			}
		}
		return null;
	}

	void putUserJobsAndTasks(User u, UserJobs ut) {
		tasksRemaining.put(u, ut);
	}
	
	public Job getUserJob(User u, String dataSnapshot) {
		if (tasksRemaining.get(u) == null)
			return null;
		
		return tasksRemaining.get(u).getJobWritingTo(dataSnapshot);
	}
	
	public Job getUserJobForAdmin(User admin, String dataSnapshotId) {
		checkArgument(admin.getRoles().contains(Role.ADMIN),
				"User " + admin.getUsername() + " is not an admin.");
		for (final Map.Entry<User, UserJobs> entry : tasksRemaining.entrySet()) {
			UserJobs userJobs = entry.getValue();
			for (final Job job : userJobs.getJobs()) {
				if (job.getDataSnapshotId().equals(dataSnapshotId)) {
					return job;
				}
			}
		}
		return null;
	}
	
	boolean assignTasks(User u, Job j, List<TimeSeriesTask> tasks) {
		UserJobs uj = getUserJobsAndTasks(u);
		if (uj == null) {
			uj = new UserJobs(j);
			putUserJobsAndTasks(u, uj);
		
			uj.addJob(j, tasks);
			initializeJob(j, tasks);
			return true;
		} else {
			if (uj.containsJob(j) && (uj.getTasks(j) != null && !uj.getTasks(j).isEmpty()))
				return false;
			else {
				uj.addJob(j, tasks);
				initializeJob(j, tasks);
				return true;
			}
		}
	}
	
//	private List<TimeSeriesTask> getJobTasks(User u, String jobName) {
//		UserJobs uj = getUserJobsAndTasks(u);
//		for (Job j : uj.getJobs())
//			if (j.getToolRunning().equals(jobName))
//				return uj.getTasks(j);
//
//		return none;
//	}
	
	private Job getJobWritingTo(User u, String snapshot) {
		for (Job j : tasksRemaining.get(u).getJobs())
			if (j.getDataSnapshotId().equals(snapshot))
				return j;

		return null;
	}
	
	private void removeActiveJob(User u, Job j) {
		UserJobs uj = tasksRemaining.get(u);
		
		if (uj != null) {
			uj.removeJob(j);
			removeCompletedJob(u, j);
		}
	}

	
	/**
	 * Returns the tasks for this job
	 * 
	 * @param userid
	 * @param dataSnapshotRevId
	 * @return -1 if job is unscheduled, else number of remaining tasks
	 * @throws AuthorizationException
	 */
	public List<TimeSeriesTask> getTasks(User u, Job j) {
		UserJobs userMap = getUserJobsAndTasks(u);

		if (userMap == null) {
			return none; 
		}
		return userMap.getTasks(j);
	}


	public JobInformation getJobInfo(User u, Job j) {
		// TODO Auto-generated method stub
		return null;
	}
	
	final static List<TimeSeriesTask> none = new ArrayList<TimeSeriesTask>();

	public List<TimeSeriesTask> getTasksForTool(User u, ToolDto t) {
		return null;
	}

	public void terminate(User u, Job j) {
		UserJobs userMap = getUserJobsAndTasks(u);
		if (userMap == null && u.getRoles().contains(Role.ADMIN)) {
			userMap = getUserJobsAndTasksForAdmin(u, j);
		}
		if (userMap != null) {
			userMap.removeJob(j);
			removeCompletedJob(u, j);
		}
	}
	
	/**
	 * Add a job into the scheduled list
	 * 
	 * @param userid
	 * @param dataSnapshotRevId
	 * @throws AuthorizationException 
	 */
	public void addJob(User u, Job j) throws IllegalArgumentException, AuthorizationException {
		UserJobs userMap = getUserJobsAndTasks(u);

		if (userMap == null) {
			userMap = new UserJobs();
			tasksRemaining.put(u, userMap);
		}
		
		if (userMap.containsJob(j)) {
			logger.trace("Not able to schedule job " + j.getDataSnapshotId() + " due to existing run of the same job");
			throw new IllegalArgumentException("An existing job already has this ID!");
		}
		
		userMap.addJob(j);
		
		assignNewJob(u, j);
		logger.trace("Scheduling job " + j.getDataSnapshotId() + " as requested by " + u.getUsername());
	}
	
	public TimeSeriesTask getFirstTimeSeriesTask(User userid, 
			String dataSnapshotRevId, String workerId) throws IllegalArgumentException {
		final String M = "getFirstTimeSeriesTask()";
		try {
			return getFirstTimeSeriesTask(userid, dataSnapshotRevId, workerId, getDataSnapshotServer());
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfPossible(t);
			throw new IllegalStateException(t);
		}
	}

	public synchronized TimeSeriesTask getFirstTimeSeriesTask(User u, 
			String dataSnapshotRevId, String workerId, IDataSnapshotServer server) 
	throws IllegalArgumentException, AuthorizationException, SAXException, IOException, TransformerException {
		
		return getNextTimeSeriesTask(u, dataSnapshotRevId, workerId, "");
	}
	
	public synchronized TimeSeriesTask getNextTimeSeriesTask(User u, 
			String dataSnapshotRevId, String workerId, String completedTask)
			throws IllegalArgumentException, AuthorizationException, SAXException, IOException, TransformerException {

		Job j = getJobWritingTo(u, dataSnapshotRevId);
		
		if (j == null)
			return null;
		
		List<TimeSeriesTask> remainingTasks = getUnscheduledTasks(u, j);
		
//		ToolInfo.PARALLEL type = j.getParallelism();
//		if (type == ToolInfo.PARALLEL.SERIAL) {
//
//			if (remainingTasks.size() > 0) {
//				allocateTasksToJob(u, j, remainingTasks);
//				
//				String[] tasks = new String[remainingTasks.size()];
//				int i = 0;
//				for (TimeSeriesTask task: remainingTasks)
//					tasks[i++] = task.getChannels()[0];
//
//				logger.trace("Returning task list with " + tasks.length + " channels in one task");
//
//
//				TimeSeriesTask task = new TimeSeriesTask(remainingTasks.get(0).getName(), tasks, remainingTasks.get(0).getStartTime(), 
//						remainingTasks.get(0).getEndTime());
//				
//				// actually assign the tasks and their status
//				this.assignTaskToWorker(u, j, task, workerId);
//				return task;
//			} else {
//				return new TimeSeriesTask();
//			}
//			// ToolInfo.PARALLEL.CHANNEL
//		} else {
//			if (remainingTasks.size() > 0) {
//				List<TimeSeriesTask> oneTask = new ArrayList<TimeSeriesTask>();
//				oneTask.add(remainingTasks.get(0));
//				
//				allocateTasksToJob(u, j, remainingTasks);
//
//				TimeSeriesTask task = new TimeSeriesTask(remainingTasks.get(0).getName(), oneTask.get(0).getChannels()[0], 
//						oneTask.get(0).getStartTime(), oneTask.get(0).getEndTime());
//				
//				// actually assign the tasks and their status
//				this.assignTaskToWorker(u, j, task, workerId);
//				return task;
//			}
//		}

		if (remainingTasks.size() > 0) {
			List<TimeSeriesTask> oneTask = new ArrayList<TimeSeriesTask>();
			TimeSeriesTask task = remainingTasks.get(0);
			oneTask.add(remainingTasks.get(0));

//			allocateTasksToJob(u, j, oneTask);

//			TimeSeriesTask task = new TimeSeriesTask(remainingTasks.get(0).getName(), oneTask.get(0).getChannels()[0], 
//					oneTask.get(0).getStartTime(), oneTask.get(0).getEndTime());

			// actually assign the tasks and their status
			this.assignTaskToWorker(u, j, task, workerId);
			return task;
		}
		return new TimeSeriesTask();
	}
	
	/**
	 * Returns the full set of named tasks
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws AuthorizationException 
	 */
	public TimeSeriesTaskList getTaskList(User u, String dataSnapshotRevId)
	throws IllegalArgumentException, AuthorizationException, SAXException, IOException {
		
		Job j = this.getJobWritingTo(u, dataSnapshotRevId);
		
		List<TimeSeriesTask> tasks;
		
		if (j != null)
			tasks = this.getTasks(u, j);
		else
			tasks = none;
		
			
		final String M = "getTaskList()";
		return new TimeSeriesTaskList(tasks);
	}

	public void markJobAsDone(User u, Job j) {
		removeActiveJob(u, j);
	}
}
