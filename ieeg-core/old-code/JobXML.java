/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.User;
import edu.upenn.cis.db.mefview.server.LruCache;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;

public class JobXML {
	private static final Logger logger = LoggerFactory
	.getLogger(JobXML.class);
	
	static Map<String,Set<User>> authCache = Collections
	.synchronizedMap(new LruCache<String, Set<User>>(500));
	
	

	protected static void initIfNecessary() {
		if (transformer == null)
			try {
				transformer = TransformerFactory.newInstance().newTransformer();
			} catch (TransformerConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerFactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (dbf == null) {
				dbf = DocumentBuilderFactory.newInstance();
				dbf.setIgnoringElementContentWhitespace(true);
				try {
					db = dbf.newDocumentBuilder();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}
	
	
	private static DocumentBuilderFactory dbf;
	static DocumentBuilder db;
    static Transformer transformer = null;
    
	static Map<String, Document> xmlCache = Collections
	.synchronizedMap(new LruCache<String, Document>(500));
	
	/**
	 * Returns the full set of named tasks, as an XML string
	 * @throws AuthorizationException 
	 */
	public String getTaskXML(User user, long timestamp,
			String signature, String dataSnapshotRevId, IDataSnapshotServer server)
			throws IllegalArgumentException, AuthorizationException {
		String xml = server.getClob(user, dataSnapshotRevId);

		return xml;
	}
	
	protected static String getXMLFrom(Document dom) throws TransformerException {
		DOMSource domSource = new DOMSource(dom);

		initIfNecessary();
		
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    StringWriter sw = new StringWriter();
	    StreamResult sr = new StreamResult(sw);
	    transformer.transform(domSource, sr);

//	    logger.trace("Writing: " + sw.toString());
	    return sw.toString();		
	}
	
//	static ToolInfo.PARALLEL getParallelismType(Node n) {
//		Node parallelism = getChildElement(n, "parallelism");
//		
//		String parType = parallelism.getAttributes().getNamedItem("type").getNodeValue();
//		
//		if (parType.equals("channel"))
//			return ToolInfo.PARALLEL.PAR_CHANNEL;
//		else if (parType.equals("none"))
//			return ToolInfo.PARALLEL.SERIAL;
//		else
//			return ToolInfo.PARALLEL.PAR_BLOCK;
//	}


	static Node getChildElement(final Node n, final String name) {
		for (Node c = n.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c.getNodeType() == Node.ELEMENT_NODE && c.getNodeName().equals(name))
				return c;
		}
		return null;
	}
	
	static String getTextChild(final Node n) {
		for (Node c = n.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c.getNodeType() == Node.TEXT_NODE) {
				String str = c.getNodeValue().trim(); 
				if (!str.isEmpty())
					return str;
			}
		}
		return "";
	}
	
	static long getStartTime(Node n) {
		for (Node el = getChildElement(n, "parameter"); el != null; el = el.getNextSibling()) {
			if (el.getNodeType() == Node.ELEMENT_NODE && el.getNodeName().equals("parameter")) {
				Node k = getChildElement(el, "key");
				
				Node v = getChildElement(el, "value");
				
				if (k != null && v != null) {
					if (getTextChild(k) != null && getTextChild(k).equals("startTime")) {
						String val = getTextChild(v);
						
						if (val != null)
							return Long.valueOf(val); 
					}
				}
			}
		}
		return 0;
	}
	
	static long getDuration(Node n) {
		int blockSize = 0;
		int numBlocks = 0;
		for (Node el = getChildElement(n, "parameter"); el != null; el = el.getNextSibling()) {
			if (el.getNodeType() == Node.ELEMENT_NODE && el.getNodeName().equals("parameter")) {
				Node k = getChildElement(el, "key");
				
				Node v = getChildElement(el, "value");
				
				if (k != null && v != null) {
					if (getTextChild(k) != null && getTextChild(k).equals("blockSize")) {
						String val = getTextChild(v);
						
						if (val != null)
							blockSize = Integer.valueOf(val); 
					} else if (getTextChild(k) != null && getTextChild(k).equals("numBlocks")) {
						String val = getTextChild(v);
						
						if (val != null)
							numBlocks = Integer.valueOf(val); 
					}
				}
			}
		}
		return blockSize * (long) numBlocks;
	}

	
	public static Document getXML(final User user, final String snapshotID, final IDataSnapshotServer server) 
	throws AuthorizationException, SAXException, IOException {
		synchronized (xmlCache) {
			if (xmlCache.containsKey(snapshotID) && authCache.get(snapshotID).contains(user)) {
				return xmlCache.get(snapshotID);
			} else {
				initIfNecessary();
	
				org.xml.sax.InputSource inStream = new org.xml.sax.InputSource();
				
				logger.trace("Loading XML clob for " + snapshotID);
				String xml = server.getClob(user, snapshotID);
				inStream.setCharacterStream(new StringReader(xml));
		
				Document doc = db.parse(inStream);
				
				xmlCache.put(snapshotID, doc);
				
				Set<User> users = new HashSet<User>();
				users.add(user);
				authCache.put(snapshotID, users);
				
				return doc;
			}
		}
	}

	public static void cacheXML(String id, Document dom) {
		xmlCache.put(id, dom);
	}
	
	public static boolean authCacheContains(String id) {
		return authCache.containsKey(id);
	}
	
	public static Set<User> getUsersWithAuth(String id) {
		return authCache.get(id);
	}
	
	public static void cacheAuthTo(String id, Set<User> users) {
		authCache.put(id, users);
	}
	
	public static DocumentBuilder getBuilder() {
		return db;
	}
}
