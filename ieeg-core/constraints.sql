ALTER TABLE `ieegqa`.`eeg_study` 
DROP FOREIGN KEY `FK6C1FC751D94C0966`,
DROP FOREIGN KEY `fk_eeg_study_recording`;
ALTER TABLE `ieegqa`.`eeg_study` 
CHANGE COLUMN `hospital_admission_id` `hospital_admission_id` BIGINT(20) NULL ,
CHANGE COLUMN `recording_id` `recording_id` BIGINT(20) NULL ;
ALTER TABLE `ieegqa`.`eeg_study` 
ADD CONSTRAINT `FK6C1FC751D94C0966`
  FOREIGN KEY (`hospital_admission_id`)
  REFERENCES `ieegqa`.`hospital_admission` (`hospital_admission_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_eeg_study_recording`
  FOREIGN KEY (`recording_id`)
  REFERENCES `ieegqa`.`recording` (`recording_id`);ALTER TABLE `ieegqa`.`eeg_study` 
DROP FOREIGN KEY `FK6C1FC751D94C0966`,
DROP FOREIGN KEY `fk_eeg_study_recording`;
ALTER TABLE `ieegqa`.`eeg_study` 
CHANGE COLUMN `type` `type` INT(11) NULL ,
CHANGE COLUMN `hospital_admission_id` `hospital_admission_id` BIGINT(20) NULL ,
CHANGE COLUMN `recording_id` `recording_id` BIGINT(20) NULL ;
ALTER TABLE `ieegqa`.`eeg_study` 
ADD CONSTRAINT `FK6C1FC751D94C0966`
  FOREIGN KEY (`hospital_admission_id`)
  REFERENCES `ieegqa`.`hospital_admission` (`hospital_admission_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_eeg_study_recording`
  FOREIGN KEY (`recording_id`)
  REFERENCES `ieegqa`.`recording` (`recording_id`)
  ON DELETE CASCADE;
ALTER TABLE `ieegqa`.`experiment` 
DROP FOREIGN KEY `fk_experiment_parent`,
DROP FOREIGN KEY `fk_experiment_recording`;
ALTER TABLE `ieegqa`.`experiment` 
CHANGE COLUMN `parent_id` `parent_id` BIGINT(20) NULL ,
CHANGE COLUMN `recording_id` `recording_id` BIGINT(20) NULL ;
ALTER TABLE `ieegqa`.`experiment` 
ADD CONSTRAINT `fk_experiment_parent`
  FOREIGN KEY (`parent_id`)
  REFERENCES `ieegqa`.`animal` (`animal_id`),
ADD CONSTRAINT `fk_experiment_recording`
  FOREIGN KEY (`recording_id`)
  REFERENCES `ieegqa`.`recording` (`recording_id`)
  ON DELETE CASCADE;
ALTER TABLE `ieegqa`.`animal` 
DROP FOREIGN KEY `fk_animal_strain`;
ALTER TABLE `ieegqa`.`animal` 
CHANGE COLUMN `strain_id` `strain_id` BIGINT(20) NULL ;
ALTER TABLE `ieegqa`.`animal` 
ADD CONSTRAINT `fk_animal_strain`
  FOREIGN KEY (`strain_id`)
  REFERENCES `ieegqa`.`strain` (`strain_id`);
ALTER TABLE `ieegqa`.`patient` 
CHANGE COLUMN `etiology` `etiology` VARCHAR(255) NULL ;

insert into ieegqa.recording_object (obj_version, create_time, description, file_name, internet_media_type, md5_hash, size_bytes, creator_id, parent_id)
select 0, ds.create_time, "Image ZIP", images_file, "application/zip", 0, 0, 118, ir.recording_id
from ieegqa.recording ir, ieegqa.eeg_study es, ieegqa.data_snapshot ds
where ir.recording_id = es.recording_id and es.eeg_study_id = ds.data_snapshot_id and images_file is not null;

insert into ieegqa.recording_object (obj_version, create_time, description, file_name, internet_media_type, md5_hash, size_bytes, creator_id, parent_id)
select 0, ds.create_time, "Documentation", report_file, "application/pdf", 0, 0, 118, ir.recording_id
from ieegqa.recording ir, ieegqa.eeg_study es, ieegqa.data_snapshot ds
where ir.recording_id = es.recording_id and es.eeg_study_id = ds.data_snapshot_id and report_file is not null;
