/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.RoundingMode;

import com.google.common.math.DoubleMath;

import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

/**
 * 
 * usage: command sineAmpMicroV sineHz samplingHz durationMin
 * 
 * @author John Frommeyer
 *
 */
public class MEFSine {

	public static void main(String[] args) throws FileNotFoundException,
			IOException {
		final int sineAmpMicroV = Integer.valueOf(args[0]);

		final double sineHz = Double.valueOf(args[1]);
		final double samplingHz = Double.valueOf(args[2]);
		final int durationMin = Integer.valueOf(args[3]);
		writeSine(sineAmpMicroV,
				sineHz,
				samplingHz,
				durationMin);
	}

	private static void writeSine(
			int sineAmpMicroV,
			double sineHz,
			double samplingHz,
			int durationMin)
			throws FileNotFoundException, IOException {
		final int sineDigitalAmp = 10000;
		checkArgument(sineAmpMicroV % sineDigitalAmp == 0,
				"The microvolt amplitude must be divisable by "
						+ sineDigitalAmp);
		MefHeader2 meh = new MefHeader2();

		final String channelName = "sine_" + sineHz + "Hz-samp_" + samplingHz
				+ "Hz";
		meh.setChannelName(channelName);
		meh.setSamplingFrequency(samplingHz);
		meh.setHeaderLength((short) 1024);

		// Set voltage conversion factor
		meh.setVoltageConversionFactor(sineAmpMicroV / sineDigitalAmp);

		double secPerMEFBlock = 2500 >= samplingHz ? Math
				.floor(2500.0 / samplingHz)
				: 1;
		long discontinuity_threshold = 10000;

		try (FileOutputStream os = new FileOutputStream(channelName + ".mef")) {
			MefWriter mefWriter = new MefWriter(
					os,
					channelName,
					meh,
					secPerMEFBlock,
					samplingHz,
					discontinuity_threshold);

			final long durationMicros = durationMin * 60L * 1000000L;
			final double samplingPeriodMicros = (1e6 / samplingHz);
			final int writeBlockLength = 2500;
			final long fileStartUutc = 946684800L * 1000000L;
			double blockStartUutc = fileStartUutc;
			while (blockStartUutc - fileStartUutc < durationMicros) {
				int[] samps = new int[writeBlockLength];
				long[] timestamps = new long[writeBlockLength];
				for (int i = 0; i < writeBlockLength; i++) {
					timestamps[i] = DoubleMath.roundToLong(
							blockStartUutc + i * samplingPeriodMicros,
							RoundingMode.HALF_UP);
					samps[i] = DoubleMath.roundToInt(
							sineDigitalAmp
									* Math.sin(2 * Math.PI * ((sineHz / 1e6)
											* timestamps[i])),
							RoundingMode.HALF_UP);
				}
				mefWriter.writeData(samps, timestamps);
				blockStartUutc = timestamps[timestamps.length - 1]
						+ samplingPeriodMicros;
			}
			mefWriter.close();
		}
	}
}
