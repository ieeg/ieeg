/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.processing;

import java.util.List;

import org.junit.Test;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.parameters.LineLengthEchauzParameters;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm.AnnotatedTimeSeries;

public class TestLineLength {
	@Test
	public void testNoGap() throws Exception {
		ISignalProcessingAlgorithm ll = 
				SignalProcessingFactory.getAlgorithm(LineLengthEchauz.NAME, 
						new LineLengthEchauzParameters(10, 100, 400, 25));
//						"{\"threshold\": 10, \"samplingFrequency\": 100, \"windowSize\": 400, \"stepSize\": 25 }");

		int[] values = {1,2,3,4,5,6,100,8,2,3,4,5,6};
		int[] values2 = new int[31400];
		for (int i = 0; i < values2.length; i++)
			values2[i] = (int)(Math.sin(i / 1000.) * 100);

		ITimeSeries series =
				new TimeSeries(0, 1e5, 1, values2, new int[0], new int[0]);


		List<AnnotatedTimeSeries> sList = ll.process("X", "Y",
				0, 100, series);
		
		for (AnnotatedTimeSeries ann: sList) {
			List<IDetection> results = ann.getAnnotations();
		
			for (IDetection ts: results)
				System.out.println("Annotation at " + ts.getStart());
		}
	}

	@Test
	public void testGap() throws Exception {
		ISignalProcessingAlgorithm ll = 
				SignalProcessingFactory.getAlgorithm(LineLengthEchauz.NAME, 
						new LineLengthEchauzParameters(10, 100, 400, 25));
//						"{\"threshold\": 10, \"samplingFrequency\": 100, \"windowSize\": 400, \"stepSize\": 25 }");

		int[] values2 = new int[31400];
		for (int i = 0; i < values2.length; i++)
			values2[i] = (int)(Math.sin(i / 1000.) * 100);

		int[] gapStart = new int[1];
		gapStart[0] = 100;
		int[] gapEnd = new int[1];
		gapEnd[0] = 101;
		ITimeSeries series2 =
				new TimeSeries(0, 1e5, 1, values2, gapStart, gapEnd);
		List<AnnotatedTimeSeries> annList = ll.process("X", "Y", 0,
				100, series2);
		for (AnnotatedTimeSeries ann: annList) {
			List<IDetection> results2 = ann.getAnnotations();
			
			for (IDetection ts: results2)
				System.out.println("Annotation at " + ts.getStart());
		}
	}
}
