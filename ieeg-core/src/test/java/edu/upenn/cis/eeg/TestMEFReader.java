/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.server.PageReader.PageList;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.eeg.mef.MEFObjectReader;

public class TestMEFReader {
	
	@BeforeClass
	public static void init() {
		IvProps.init("src/test/resources");
		IObjectServer fso = StorageFactory.getStagingServer(".");
	}
	
	@Test
	public void testReader() {
		MEFObjectReader mr;
		try {
//			long start = System.currentTimeMillis();

			DirectoryBucketContainer db = new DirectoryBucketContainer("src/test/resources", "src/test/resources");
			IStoredObjectReference handle = 
					new FileReference(db, "I010_A0001_D001_CH2.mef", "I010_A0001_D001_CH2.mef");
//			DirectoryBucketContainer db = new DirectoryBucketContainer("Mayo_IEED_data/Mayo_IEED_005/IC_005", "Mayo_IEED_data/Mayo_IEED_005/IC_005");
//			IStoredObjectReference handle = 
//							new FileReference(db, "LTD2.mef", "LTD2.mef");
			
			mr = new MEFObjectReader(handle);//"traces/Mayo_IEED_data/Mayo_IEED_005/IC_005/LTD1.mef");
			
			System.out.println("Index with " + mr.getTimeIndex().length + " entries");
			long offset = mr.getStartTime();//mr.getTimeIndex()[0];
			
			System.out.println("Start time: " + offset);
			System.out.println("End time: " + mr.getEndTime());//mr.getTimeIndex()[mr.getTimeIndex().length - 1]);
			
			
			System.out.println(mr.getEndTime() - mr.getStartTime());
			
			System.out.println(mr.getHeader());
			
//			for (int i = 0; i < 1000000; i++) {
//				PageList firstPage = mr.getPages(offset, offset + 1000000,1);
//				System.out.println("Timestamps " + (offset) + " from page " + firstPage.startPage + ":" + firstPage.startOffset + 
//						" to " + firstPage.endPage + ":" + firstPage.endOffset);
//				for (long j = firstPage.startPage; j <= firstPage.endPage; j++)
//					mr.readContents(j, 1);
//				offset += 1000000;
//				//System.out.println(firstPage.values);
//			}
			// Error in reading traces/Mayo_IEED_data/Mayo_IEED_002/IC_002/LG1.mef page 233 (time 1077368822621929)
//Error in reading traces/Mayo_IEED_data/Mayo_IEED_002/IC_002/LG10.mef page 31 (time 1077367812622010)
//			Seek to 66864, read 2136, decode 2136
			
//			for (int i = 0; i < 1000; i++) {
//				PageList next = mr.getPages(offset + i * 30000000, offset + 1000000 + i * 30000000,1);
//				for (long j = next.startPage; j < next.endPage; j++)
//					mr.readContents(j, 1);
//			}
//			
//			System.out.println("Finished in " + (System.currentTimeMillis() - start));

			for (int i = 0; i < 100; i++) {
				PageList next = mr.getPages(offset + i * 300000, offset + 10000 + i * 300000,1);
				mr.readPages(next.startPage, (int)(next.endPage - next.startPage + 1), true);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertTrue(false);
		}
	}


}
