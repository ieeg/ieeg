/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

/**
 * @author John Frommeyer
 *
 */
public class DiscontinuousMEFMaker {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		checkArgument(mefFile.getName().endsWith(".mef"));
		final String outFilename = mefFile.getName()
				.replace(
						".mef",
						"_discont.mef");

		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						true)) {
			final MefHeader2 originalMefHeader = streamer.getMEFHeader();

			MefWriter mefWriter = new MefWriter(
					outFilename,
					originalMefHeader.getBlockInterval() / 1000000.0,
					originalMefHeader.getSamplingFrequency(),
					(long) streamer.getSamplingPeriodMicros());
			int blockNo = 1;
			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {

					if (blockNo != 6) {
						final long startUutc = block.timeStart;
						long[] timestamps = new long[block.values.length];
						for (int i = 0; i < timestamps.length; i++) {
							timestamps[i] = (long) (startUutc + i
									* streamer.getSamplingPeriodMicros());
						}
						mefWriter.writeData(block.values, timestamps);
					}

					blockNo++;
				}
			}
			mefWriter.close();
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}
}
