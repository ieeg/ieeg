/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Iterables.getOnlyElement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFIndexStreamer;
import edu.upenn.cis.eeg.mef.MefWriter;

/**
 * Creates a MEF file from given MEF using blocks which do not throw exceptions
 * when decoded. Block numbers start at 0.
 * 
 * @author John Frommeyer
 *
 */
public class MEFDropBadBlocks {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		checkArgument(mefFile.getName().endsWith(".mef"));
		final String outFilename = mefFile.getPath()
				.replace(
						".mef",
						"-fixed"
								+ ".mef");
		try (final MEFIndexStreamer streamer = new MEFIndexStreamer(
				mefFile,
				true)) {
			final MefWriter writer = newMefWriter(
					outFilename,
					streamer);
			int blockNo = -1;
			long previousEndUutc = -1;
			while (streamer.moreBlocks()) {
				blockNo++;
				List<TimeSeriesPage> blocks = null;
				try {
					blocks = streamer.getNextBlocks(1);
				} catch (IOException | RuntimeException e) {
					System.err.println("Cannot read block " + blockNo
							+ ", skipping: " + e.getClass().getSimpleName()
							+ " (" + e.getMessage() + ")");
					continue;
				}
				final TimeSeriesPage block = getOnlyElement(blocks);
				final boolean discontinuity = previousEndUutc == -1
						|| (block.timeStart - previousEndUutc > streamer
								.getSamplingPeriodMicros());
				writer.writeDataBlock(
						block.values,
						block.timeStart,
						block.values.length,
						discontinuity);
				previousEndUutc = block.timeEnd;

			}

			writer.close();
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}

	private static MefWriter newMefWriter(String filename, MEFIndexStreamer streamer) {
		final MefWriter writer = new MefWriter(
				filename,
				streamer.getMEFHeader().getBlockInterval() / 1e6,
				streamer.getMEFHeader().getSamplingFrequency(),
				(long) streamer.getSamplingPeriodMicros());
		writer.setInstitution(streamer.getMEFHeader().getInstitution());
		writer.setUnencryptedTextField(streamer.getMEFHeader()
				.getUnencryptedTextField());
		writer.setFirstName(streamer.getMEFHeader().getSubjectFirstName());
		writer.setSecondName(streamer.getMEFHeader().getSubjectSecondName());
		writer.setThirdName(streamer.getMEFHeader().getSubjectThirdName());
		writer.setSubjectID(streamer.getMEFHeader().getSubjectId());
		writer.setChannelName(streamer.getMEFHeader().getChannelName());
		writer.setLowFrequencyFilterSetting(streamer.getMEFHeader()
				.getLowFrequencyFilterSetting());
		writer.setHighFrequencyFilterSetting(streamer.getMEFHeader()
				.getHighFrequencyFilterSetting());
		writer.setNotchFrequencyFilterSetting(streamer.getMEFHeader()
				.getNotchFilterFrequency());
		writer.setVoltageConversionFactor(streamer.getMEFHeader()
				.getVoltageConversionFactor());
		writer.setAcquisitionSystem(streamer.getMEFHeader()
				.getAcquisitionSystem());
		writer.setChannelComments(streamer.getMEFHeader().getChannelComments());
		writer.setStudyComments(streamer.getMEFHeader().getStudyComments());
		writer.setPhysicalChannelNumber(streamer.getMEFHeader()
				.getPhysicalChannelNumber());
		return writer;
	}
}
