/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MEFStreamer;
import edu.upenn.cis.eeg.mef.MefWriter;

/**
 * Creates a MEF file from given MEF using only startBlockNo through endBlockNo.
 * Block numbers start at 1.
 * 
 * @author John Frommeyer
 *
 */
public class MEFSliceMaker {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		final File mefFile = new File(args[0]);
		final int startBlockNo = Integer.valueOf(args[1]);
		final int endBlockNo = Integer.valueOf(args[2]);
		checkArgument(startBlockNo <= endBlockNo);
		checkArgument(mefFile.getName().endsWith(".mef"));
		final String outFilename = mefFile.getName()
				.replace(
						".mef",
						"-blocks-"
								+ startBlockNo
								+ "-"
								+ endBlockNo
								+ ".mef");
		try (final FileInputStream mefInput = new FileInputStream(
				mefFile);
				final MEFStreamer streamer = new MEFStreamer(
						mefInput,
						true)) {
			checkArgument(endBlockNo <= streamer.getNumberOfBlocks());
			final MefWriter writer = new MefWriter(
					outFilename,
					streamer.getMEFHeader().getBlockInterval() / 1e6,
					streamer.getMEFHeader().getSamplingFrequency(),
					(long) streamer.getSamplingPeriodMicros());
			int blockNo = 1;
			long previousEndUutc = -1;
			while (streamer.moreBlocks()) {
				for (TimeSeriesPage block : streamer.getNextBlocks(1)) {
					if (startBlockNo <= blockNo && blockNo <= endBlockNo) {
						final boolean discontinuity = previousEndUutc == -1
								|| (block.timeStart - previousEndUutc > streamer
										.getSamplingPeriodMicros());
						writer.writeDataBlock(
								block.values,
								block.timeStart,
								block.values.length,
								discontinuity);
					}
					blockNo++;
					previousEndUutc = block.timeEnd;
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			System.err.println(
					"Could not find file ["
							+ args[0]
							+ "]");
			e.printStackTrace();
		}

	}

	/**
	 * DOCUMENT ME
	 * 
	 * @param blockNo
	 * @param previousEndUutc
	 * @param startUutc
	 * @param gapMicros
	 */
	private static String getSegmentReportString(
			int segmentNo,
			TimeSeriesPage startBlock,
			int startBlockNo,
			TimeSeriesPage endBlock,
			int segmentLength,
			long startOffsetMicros) {
		final DateTime endDateTime = uutcToDateTime(endBlock.timeEnd);
		final DateTime startDateTime = uutcToDateTime(startBlock.timeStart);
		long segmentMicros = endBlock.timeEnd - startBlock.timeStart;
		return "Segment "
				+ (segmentNo)
				+ ": start: "
				+ startDateTime
				+ " ("
				+ startBlock.timeStart
				+ "), end: "
				+ endDateTime
				+ " ("
				+ endBlock.timeEnd
				+ "), length: "
				+ segmentMicros
				+ " usec ("
				+ segmentMicros / 1e6
				+ ") sec, block length: "
				+ (segmentLength)
				+ ", block "
				+ startBlockNo
				+ " through block "
				+ (startBlockNo + segmentLength - 1)
				+ ", starting at "
				+ startOffsetMicros
				+ " usec ("
				+ startOffsetMicros / 1e6
				+ " sec) from beginning of file";
	}

	private static DateTime uutcToDateTime(long uutc) {
		final long mutc = TimeUnit.MILLISECONDS.convert(
				uutc,
				TimeUnit.MICROSECONDS);
		final DateTime dateTime = new DateTime(mutc,
				DateTimeZone.UTC);
		return dateTime;
	}
}
