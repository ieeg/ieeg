/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Subregion;

/**
 * @author samd
 * 
 */
public class SubregionTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (Subregion enumValue : Subregion.values()) {
			switch (enumValue) {
				case NONE:
					assertEquals("db enums must not change name!", "NONE",
							enumValue.name());
					break;
				case POLAR:
					assertEquals("db enums must not change name!", "POLAR",
							enumValue.name());
					break;
				case BASAL:
					assertEquals("db enums must not change name!", "BASAL",
							enumValue.name());
					break;
				case LATERAL:
					assertEquals("db enums must not change name!", "LATERAL",
							enumValue.name());
					break;
				case MESIAL:
					assertEquals("db enums must not change name!", "MESIAL",
							enumValue.name());
					break;
				default:
					assertFalse("unknown enum: " + enumValue, true);
					break;
			}
		}
	}
}
