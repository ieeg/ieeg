/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.datasnapshot.assembler.DsSearchResultAssembler;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.EpilepsySubtypeEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.ContactGroupSearch;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.EpilepsySubtypeSearch;
import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.IHasStringId;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.PatientSearch;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

public class DataSnapshotDAOHibernateSearchTest {

	private static EegStudy study000;

	private static EegStudy study100;

	private static TimeSeriesEntity trace00000;
	private static TimeSeriesEntity trace10000;
	private static TimeSeriesEntity trace10001;

	private static Logger logger = LoggerFactory
			.getLogger(DataSnapshotDAOHibernateSearchTest.class);

	static TstUsers tstUsers;

	@BeforeClass
	static public void classSetUp() throws Throwable {

		Transaction trx = null;
		Session s = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory().openSession();

			ManagedSessionContext.bind(s);

			trx = s.beginTransaction();

			tstUsers = new TstUsers(s);

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			// patient 0
			{
				final Patient patient0 = BuildDb.buildShallowPatient(UUID
						.randomUUID().toString());
				patient0.getSzTypes().clear();
				patient0.getSzTypes().addAll(EnumSet.of(
						SeizureType.ATONIC,
						SeizureType.MYOCLONIC));
				patient0.getPrecipitants().addAll(
						EnumSet.of(Precipitant.ALCOHOL,
								Precipitant.CATAMENIAL,
								Precipitant.STRESS));
				patient0.setAgeOfOnset(54);
				// patient0.setDevelopmentalDelay(null);
				patient0.setDevelopmentalDisorders(true);
				patient0.setEtiology("anoxia");
				patient0.setFamilyHistory(false);

				final ImageEntity p0Image0 = new ImageEntity();
				p0Image0.setType(ImageType.CT);
				p0Image0.setFileKey("0/ct/02");

				final ImageEntity image01 = new ImageEntity();
				image01.setType(ImageType.DIGITAL_PICTURES);
				image01.setFileKey("0/digital/01");

				final HospitalAdmission admission00 = new HospitalAdmission();
				patient0.addAdmission(admission00);
				admission00.setAgeAtAdmission(34);

				study000 = BuildDb.buildStudyNoElectrodesNoImages(
						"Xyz_IEED_Study0",
						readPerm);
				admission00.addStudy(study000);
				study000.setSzCount(3);

				new EpilepsySubtypeEntity(
						study000,
						EpilepsyType.FRONTAL_LOBE_BILATERAL,
						Etiology.STRUCTURAL_METABOLIC);

				new EpilepsySubtypeEntity(
						study000,
						EpilepsyType.GENERALIZED,
						Etiology.UNKNOWN);

				final ImageEntity ctImage0000 = new ImageEntity();
				study000.getImages().add(ctImage0000);
				ctImage0000.setFileKey("0/ct/01");
				ctImage0000.setType(ImageType.CT);

				final ImageEntity megImage0000 = new ImageEntity();
				study000.getImages().add(megImage0000);
				megImage0000.setFileKey("0/meg/01");
				megImage0000.setType(ImageType.MEG);

				final HospitalAdmission admission01 = new HospitalAdmission();
				patient0.addAdmission(admission01);
				admission01.setAgeAtAdmission(21);
				patient0.setTraumaticBrainInjury(false);

				final ContactGroup electrode0000 = new ContactGroup(
						"LAG",
						"Depth",
						53898.389,
						0.53,
						55.0,
						false,
						Side.LEFT,
						Location.PARIETAL,
						5.0,
						"Ad-Tech",
						newUuid());
				study000.getRecording().addContactGroup(electrode0000);

				// trace0.setChannel("channel-39");
				trace00000 = new TimeSeriesEntity();
				trace00000.setFileKey("x/y/z");
				trace00000.setLabel("z");
				s.save(trace00000);

				final Contact contact00000 = new Contact(
						ContactType.MACRO,
						new MriCoordinates(444.9, -87.7,
								5626.456),
						new TalairachCoordinates(
								4.5,
								-98.5, 8384.093),
						trace00000);
				electrode0000.addContact(contact00000);

				Set<TsAnnotationEntity> tsAnns = newHashSet();
				TsAnnotationEntity tsAnnotation000000 = new TsAnnotationEntity();
				tsAnnotation000000.getAnnotated().add(trace00000);
				tsAnnotation000000.setParent(study000);
				tsAnnotation000000.setType("seizure");
				tsAnnotation000000.setAnnotator("the-tool");
				tsAnnotation000000.setCreator(tstUsers.btRegUser1);
				tsAnnotation000000.setStartOffsetUsecs(0L);
				tsAnnotation000000.setEndOffsetUsecs(1L);
				tsAnnotation000000.setLayer(study000.getLabel() + " base");
				tsAnns.add(tsAnnotation000000);

				TsAnnotationEntity tsAnnotation000001 = new TsAnnotationEntity();
				tsAnnotation000001.getAnnotated().add(trace00000);
				tsAnnotation000001.setParent(study000);
				tsAnnotation000001.setType("seizure");
				tsAnnotation000001.setAnnotator("the-tool");
				tsAnnotation000001.setCreator(tstUsers.btRegUser1);
				tsAnnotation000001.setStartOffsetUsecs(3L);
				tsAnnotation000001.setEndOffsetUsecs(5L);
				tsAnnotation000001.setLayer(study000.getLabel() + " base");
				tsAnns.add(tsAnnotation000001);

				TsAnnotationEntity tsAnnotation000002 = new TsAnnotationEntity();
				tsAnnotation000002.getAnnotated().add(trace00000);
				tsAnnotation000002.setParent(study000);
				tsAnnotation000002.setType("seizure");
				tsAnnotation000002.setAnnotator("the-tool");
				tsAnnotation000002.setCreator(tstUsers.btRegUser1);
				tsAnnotation000002.setStartOffsetUsecs(20L);
				tsAnnotation000002.setEndOffsetUsecs(25L);
				tsAnnotation000002.setLayer(study000.getLabel() + " base");
				tsAnns.add(tsAnnotation000002);

				patient0.getPrecipitants().addAll(
						EnumSet.of(Precipitant.ALCOHOL,
								Precipitant.CATAMENIAL,
								Precipitant.STRESS));

				s.save(patient0);
				for (TsAnnotationEntity tsAnn : tsAnns) {
					s.saveOrUpdate(tsAnn);
				}
			}

			// patient 1
			{

				final Patient patient1 = BuildDb.buildShallowPatient(UUID
						.randomUUID().toString());
				patient1.setGender(Gender.MALE);
				patient1.getSzTypes().clear();
				patient1.getSzTypes()
						.addAll(EnumSet
								.of(SeizureType.COMPLEX,
										SeizureType.PARTIAL_WITH_2NDARY_GENERALIZATION));

				final HospitalAdmission admission10 = new HospitalAdmission();
				patient1.addAdmission(admission10);
				admission10.setAgeAtAdmission(30);
				study100 = BuildDb.buildStudyNoElectrodesNoImages(
						"Xyz_IEED_Study1",
						readPerm);
				admission10.addStudy(study100);
				study100.setSzCount(2);

				final ImageEntity ctImage1000 = new ImageEntity();
				study100.getImages().add(ctImage1000);
				ctImage1000.setFileKey("1/ct/01");
				ctImage1000.setType(ImageType.CT);

				final ImageEntity mriImage1000 = new ImageEntity();
				study100.getImages().add(mriImage1000);
				mriImage1000.setFileKey("1/mri/01");
				mriImage1000.setType(ImageType.MRI);

				final ImageEntity image10 = new ImageEntity();
				image10.setType(ImageType.CT);
				image10.setFileKey("1/ct/02");

				final HospitalAdmission admission11 = new HospitalAdmission();
				patient1.addAdmission(admission11);
				admission11.setAgeAtAdmission(21);
				patient1.setAgeOfOnset(4);
				// patient1.setDevelopmentalDelay(null);
				patient1.setDevelopmentalDisorders(true);
				patient1.setEtiology("anoxia");
				patient1.setFamilyHistory(false);
				patient1.setTraumaticBrainInjury(false);

				final ContactGroup electrode1000 = new ContactGroup(
						"AG",
						"Depth",
						600.3,
						5.3,
						55.0,
						false,
						Side.RIGHT,
						Location.OCCIPITAL,
						5.0,
						"Ad-Tech",
						newUuid());
				study100.getRecording().addContactGroup(electrode1000);

				trace10000 = new TimeSeriesEntity();

				trace10000.setFileKey("v/e/h");
				trace10000.setLabel("h");
				s.save(trace10000);

				final Contact contact10000 = new Contact(
						ContactType.MICRO,
						new MriCoordinates(34.9,
								-987.7, 35.5),
						new TalairachCoordinates(
								4.5,
								-98.4, 8384.093),
						trace10000);

				electrode1000.addContact(contact10000);

				Set<TsAnnotationEntity> tsAnns = newHashSet();

				TsAnnotationEntity tsAnnotation100000 = new TsAnnotationEntity();
				tsAnnotation100000.getAnnotated().add(trace10000);
				tsAnnotation100000.setParent(study100);
				tsAnnotation100000.setType("seizure");
				tsAnnotation100000.setAnnotator("the-tool");
				tsAnnotation100000.setCreator(tstUsers.btRegUser1);
				tsAnnotation100000.setStartOffsetUsecs(0L);
				tsAnnotation100000.setEndOffsetUsecs(1L);
				tsAnnotation100000.setLayer(study100 + " base");
				tsAnns.add(tsAnnotation100000);

				TsAnnotationEntity tsAnnotation100001 = new TsAnnotationEntity();
				tsAnnotation100001.getAnnotated().add(trace10000);
				tsAnnotation100001.setParent(study100);
				tsAnnotation100001.setType("seizure");
				tsAnnotation100001.setAnnotator("the-tool");
				tsAnnotation100001.setCreator(tstUsers.btRegUser1);
				tsAnnotation100001.setStartOffsetUsecs(3L);
				tsAnnotation100001.setEndOffsetUsecs(5L);
				tsAnnotation100001.setLayer(study100 + " base");
				tsAnns.add(tsAnnotation100001);

				trace10001 = new TimeSeriesEntity();

				trace10001.setFileKey("v/e/i");
				trace10001.setLabel("h");

				final Contact contact10001 = new Contact(
						ContactType.MACRO,
						new MriCoordinates(34.9,
								-987.7, 35.5),
						new TalairachCoordinates(
								4.5,
								-98.4, 8384.093),
						trace10001);

				electrode1000.addContact(contact10001);

				TsAnnotationEntity tsAnnotation100002 = new TsAnnotationEntity();
				tsAnnotation100002.getAnnotated().add(trace10001);
				tsAnnotation100002.setParent(study100);
				tsAnnotation100002.setType("soz");
				tsAnnotation100002.setAnnotator("the-tool");
				tsAnnotation100002.setCreator(tstUsers.btRegUser1);
				tsAnnotation100002.setStartOffsetUsecs(3L);
				tsAnnotation100002.setEndOffsetUsecs(6L);
				tsAnnotation100002.setLayer(study100.getLabel() + " base");
				tsAnns.add(tsAnnotation100002);

				s.save(patient1);

				for (TsAnnotationEntity tsAnn : tsAnns) {
					s.saveOrUpdate(tsAnn);
				}
			}

			trx.commit();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
		}
	}

	@Test
	public void epSubtypes() throws Exception {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(sess);
			EegStudySearch studySearch = new EegStudySearch();

			Set<EegStudy> results = dsDAO.findBySearch(studySearch,
					tstUsers.btRegUser1, tstUsers.regUser1);
			assertEquals(2, results.size());

			EpilepsySubtypeSearch epSearch =
					new EpilepsySubtypeSearch();
			studySearch.setEpilepsySubtypeSearch(epSearch);
			epSearch.getEtiologies().add(Etiology.STRUCTURAL_METABOLIC);
			results = dsDAO.findBySearch(studySearch,
					tstUsers.btRegUser1, tstUsers.regUser1);
			assertEquals(1, results.size());

			epSearch.getTypes().add(EpilepsyType.FRONTAL_LOBE_BILATERAL);
			results =
					dsDAO.findBySearch(studySearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			assertEquals(1, results.size());

			epSearch.getTypes().add(EpilepsyType.GENERALIZED);
			results =
					dsDAO.findBySearch(studySearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			assertEquals(1, results.size());

			epSearch.getTypes().remove((EpilepsyType.FRONTAL_LOBE_BILATERAL));
			results =
					dsDAO.findBySearch(studySearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			assertEquals(0, results.size());

			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void findByAgeOfOnset() throws Exception {

		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch EegStudySearch = new EegStudySearch();
			final PatientSearch patientSearch = new PatientSearch();
			EegStudySearch.setPatientSearch(patientSearch);

			patientSearch.setAgeOfOnsetMin(13);
			patientSearch.setAgeOfOnsetMax(60);

			Set<EegStudy> preStudies =
					dsDAO.findBySearch(EegStudySearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			assertTrue(any(
					preStudies,
					compose(equalTo(study000.getPubId()),
							IHasPubId.getPubId)));

			assertEquals("expected 1 found " + preStudies.size(),
					1, preStudies.size());

			trx.commit();

		} catch (Exception t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void bySzCount() throws Throwable {
		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			DataSnapshotDAOHibernate dsDAO =
					new DataSnapshotDAOHibernate(session);
			EegStudySearch dsSearch = new EegStudySearch();
			dsSearch.setMinSzCount(2);

			Set<EegStudy> preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			assertEquals(2, preResults.size());

			DataSnapshotEntity dsResult1 =
					find(preResults,
							compose(equalTo(study100.getPubId()),
									IHasPubId.getPubId), null);
			assertNotNull(dsResult1);

			dsSearch.setMinSzCount(3);

			preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			assertEquals(1, preResults.size());

			dsSearch.setMinSzCount(null);
			dsSearch.setMaxSzCount(5);

			preResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);

			assertEquals(2, preResults.size());

			dsSearch.setMaxSzCount(2);

			preResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			assertEquals(1, preResults.size());

			dsSearch.setMinSzCount(2);
			dsSearch.setMaxSzCount(2);

			preResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			assertEquals(1, preResults.size());

			dsSearch.setMinSzCount(null);
			dsSearch.setMaxSzCount(null);
			preResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			assertEquals(2, preResults.size());

			dsSearch.setMinSzCount(0);
			dsSearch.setMaxSzCount(null);
			preResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			assertEquals(2, preResults.size());

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findByEegStudyElectrodeSearch() throws Throwable {

		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			EegStudySearch dsSearch = new EegStudySearch();

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);

			Set<EegStudy> dsResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			assertEquals(2, dsResults.size());

			assertTrue(any(dsResults, compose(equalTo(study000.getPubId()),
					IHasPubId.getPubId)));
			assertTrue(any(dsResults, compose(equalTo(study100.getPubId()),
					IHasPubId.getPubId)));

			dsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);

			assertEquals(2, dsResults.size());

			assertTrue(any(dsResults, compose(equalTo(study000.getPubId()),
					IHasPubId.getPubId)));
			assertTrue(any(dsResults, compose(equalTo(study100.getPubId()),
					IHasPubId.getPubId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findByGender() throws Throwable {

		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();
			final PatientSearch patientSearch = new PatientSearch();
			dsSearch.setPatientSearch(patientSearch);

			patientSearch.getGenders().add(Gender.MALE);

			final Set<EegStudy> preSearchResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			Set<DataSnapshotSearchResult> searchResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preSearchResults);

			assertEquals(1, searchResults.size());

			assertTrue(any(searchResults, compose(equalTo(study100.getPubId()),
					IHasStringId.getId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void findByDataSnapshotImageTypes() throws Throwable {

		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO studyDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch EegStudySearch = new EegStudySearch();

			EegStudySearch.getImageTypes()
					.add(ImageType.DIGITAL_PICTURES);
			EegStudySearch.getImageTypes()
					.add(ImageType.ELECTRODE_MAP);

			Set<EegStudy> preStudies = studyDAO
					.findBySearch(EegStudySearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			assertEquals(0, preStudies.size());

			EegStudySearch.getImageTypes().clear();
			EegStudySearch.getImageTypes().add(ImageType.MRI);

			preStudies = studyDAO.findBySearch(EegStudySearch,
					tstUsers.btRegUser1, tstUsers.regUser1);

			assertEquals(1, preStudies.size());

			assertTrue(any(preStudies,
					compose(equalTo(study100.getPubId()),
							IHasPubId.getPubId)));

			EegStudySearch.getImageTypes().add(ImageType.CT);

			preStudies = studyDAO.findBySearch(EegStudySearch,
					tstUsers.btRegUser1, tstUsers.regUser1);
			assertEquals(1, preStudies.size());

			assertTrue(any(preStudies,
					compose(equalTo(study100.getPubId()),
							IHasPubId.getPubId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findStudiesByEegStudyElectrodeSearches() throws Throwable {

		Session session = null;
		Transaction trx = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);

			electrodeSearch.setSamplingRateMin(400.35);
			electrodeSearch.setSamplingRateMax(500000.351);
			electrodeSearch.getSides().add(Side.LEFT);
			electrodeSearch.getLocations().add(Location.FRONTAL);

			Set<EegStudy> preDsResults =
					dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			Set<DataSnapshotSearchResult> dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);

			assertEquals(0, dsResults.size());

			electrodeSearch.getLocations().add(Location.PARIETAL);

			preDsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);
			assertEquals(1, dsResults.size());
			assertTrue(any(dsResults,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));

			electrodeSearch.getSides().add(Side.RIGHT);
			electrodeSearch.getLocations().add(Location.OCCIPITAL);

			preDsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);
			assertEquals(2, dsResults.size());

			assertTrue(any(dsResults,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));
			assertTrue(any(dsResults,
					compose(equalTo(study100.getPubId()),
							IHasStringId.getId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void findStudiesByComplexSearches() throws Throwable {

		Session session = null;

		Transaction trx = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			Set<EegStudy> preDsResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			assertEquals(2, preDsResults.size());

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);

			electrodeSearch.setSamplingRateMin(400.0);
			electrodeSearch.setSamplingRateMax(500000.3);

			preDsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			Set<DataSnapshotSearchResult> dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);

			assertEquals(2, dsResults.size());
			assertTrue(any(dsResults,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));
			assertTrue(any(dsResults,
					compose(equalTo(study100.getPubId()),
							IHasStringId.getId)));

			electrodeSearch.setSamplingRateMax(600.35);

			preDsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);

			assertEquals(1, dsResults.size());
			assertTrue(any(dsResults,
					compose(equalTo(study100.getPubId()),
							IHasStringId.getId)));

			electrodeSearch.setSamplingRateMax(500000.353);

			final PatientSearch pSearch = new PatientSearch();
			dsSearch.setPatientSearch(pSearch);
			pSearch.getSzTypes().add(SeizureType.ATONIC);

			preDsResults = dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
					tstUsers.regUser1);
			dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);
			assertEquals(1, dsResults.size());
			assertTrue(any(dsResults,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));
			trx.commit();
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void findStudiesByElectrodeLocationAndSamplingRate()
			throws Throwable {
		Session session = null;
		Transaction trx = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);

			electrodeSearch.setSamplingRateMin(400.45);
			electrodeSearch.setSamplingRateMax(500000.35);
			electrodeSearch.getSides().add(Side.LEFT);

			Set<EegStudy> preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			Set<DataSnapshotSearchResult> results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);

			assertEquals(1, results.size());
			assertTrue(any(results,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findStudiesByPatientSearch() throws Throwable {

		Session session = null;
		Transaction trx = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			final PatientSearch pSearch = new PatientSearch();
			dsSearch.setPatientSearch(pSearch);

			pSearch.getSzTypes().add(SeizureType.COMPLEX);

			Set<EegStudy> preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			Set<DataSnapshotSearchResult> results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);

			assertEquals(1, results.size());

			assertTrue(any(
					results,
					compose(equalTo(study100.getPubId()),
							IHasStringId.getId)));

			assertNull(getOnlyElement(results).getTimeSeries());

			pSearch.getSzTypes().clear();
			pSearch.getSzTypes().add(SeizureType.MYOCLONIC);
			pSearch.getSzTypes().add(SeizureType.ATONIC);

			preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);
			assertEquals(1, results.size());
			assertTrue(any(results,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));

			pSearch.getSzTypes().clear();
			pSearch.getSzTypes().add(SeizureType.MYOCLONIC);
			pSearch.getSzTypes().add(SeizureType.ATONIC);
			pSearch.getSzTypes().add(SeizureType.ABSENCE);

			preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);
			assertEquals(0, results.size());

			pSearch.getSzTypes().clear();
			pSearch.setGeneralizedSzTypes();

			preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);
			assertEquals(1, results.size());
			assertEquals(study000.getPubId(),
					getOnlyElement(results).getId());

			pSearch.unsetGeneralizedSzTypes();
			pSearch.setPartialSzTypes();
			preResults = dsDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);
			assertEquals(1, results.size());
			assertEquals(study100.getPubId(),
					getOnlyElement(results).getId());

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	// @Test
	// public void findStudiesByShallowStudySearch() throws Throwable {
	//
	// Session session = null;
	// Transaction trx = null;
	//
	// try {
	// session = HibernateUtil.getSessionFactory().openSession();
	// trx = session.beginTransaction();
	//
	// final IDataSnapshotDAO dsDAO =
	// new DataSnapshotDAOHibernate(session);
	//
	// final EegStudySearch studySearch = new EegStudySearch();
	// studySearch.setSzCountMin(0);
	// studySearch.setSzCountMax(10000);
	//
	// final Set<EegStudy> studies = dsDAO
	// .findBySearch(studySearch);
	//
	// assertEquals(2, studies.size());
	//
	// studySearch.setSzCountMin(study000.getSzCount() + 1);
	// studySearch.setSzCountMax(study000.getSzCount() + 1);
	//
	// final Set<EegStudy> studies3 = dsDAO
	// .findByStudySearch(studySearch);
	// assertEquals(0, studies3.size());
	//
	// studySearch.setSzCountMin(study000.getSzCount() - 2);
	// studySearch.setSzCountMax(study000.getSzCount() - 1);
	//
	// final Set<EegStudy> studies4 = studyDAO
	// .findByStudySearch(studySearch);
	// assertEquals(0, studies4.size());
	//
	// trx.commit();
	// } catch (final Throwable t) {
	// try {
	// if (HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().isActive()) {
	// HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().rollback();
	// }
	// } catch (final Throwable rbEx) {
	// logger.error("exception while rolling back", rbEx);
	// }
	// throw t;
	// }
	// }

	// @Test
	// public void findStudiesBySurgicalIntervention() throws Throwable {
	//
	// Session session = null;
	// Transaction trx = null;
	// try {
	//
	// final EegStudySearch sSearch = new EegStudySearch();
	//
	// final HospitalAdmissionSearch aSearch = new HospitalAdmissionSearch();
	// sSearch.setAdmissionSearch(aSearch);
	//
	// final TreatmentSearch treatmentSearch = new TreatmentSearch();
	// aSearch.setTreatmentSearch(treatmentSearch);
	//
	// final SurgicalInterventionSearch siSearch = new
	// SurgicalInterventionSearch();
	// treatmentSearch.setSurgicalInterventionSearch(siSearch);
	// siSearch.getSides().add(Side.RIGHT);
	// siSearch.getLobes().add(Lobe.TEMPORAL);
	//
	// session = HibernateUtil.getSessionFactory().openSession();
	//
	// trx = session.beginTransaction();
	//
	// final IEegStudyDAO studyDAO = new EegStudyDAOHibernate(session);
	//
	// Set<EegStudy> studies = studyDAO.findByStudySearch(sSearch);
	//
	// assertEquals(0, studies.size());
	//
	// siSearch.getSides().add(Side.LEFT);
	// siSearch.getLobes().add(Lobe.FRONTAL);
	//
	// studies = studyDAO.findByStudySearch(sSearch);
	//
	// assertEquals(1, studies.size());
	//
	// siSearch.getPostopRatingScales().add(
	// SurgicalIntervention.PostoperativeRatingScale.CLASS_4);
	//
	// studies = studyDAO.findByStudySearch(sSearch);
	//
	// assertEquals(1, studies.size());
	// assertTrue(any(studies, compose(equalTo(study100.getId()),
	// IHasLongId.getId)));
	//
	// siSearch.getPostopRatingScales().add(
	// SurgicalIntervention.PostoperativeRatingScale.CLASS_1);
	//
	// studies = studyDAO.findByStudySearch(sSearch);
	//
	// assertEquals(2, studies.size());
	// assertTrue(any(studies, compose(equalTo(study000.getId()),
	// IHasLongId.getId)));
	// assertTrue(any(studies, compose(equalTo(study100.getId()),
	// IHasLongId.getId)));
	//
	// siSearch.getTypes().add(SurgicalIntervention.Type.HEMISPHERECTOMY);
	// studies = studyDAO.findByStudySearch(sSearch);
	// assertEquals(0, studies.size());
	//
	// siSearch.getTypes().add(SurgicalIntervention.Type.TUMOR_RESECTION);
	// studies = studyDAO.findByStudySearch(sSearch);
	// assertEquals(0, studies.size());
	//
	// siSearch.getLobes().add(Lobe.PARIETAL);
	// studies = studyDAO.findByStudySearch(sSearch);
	// assertEquals(0, studies.size());
	//
	// siSearch.getSides().add(Side.RIGHT);
	// studies = studyDAO.findByStudySearch(sSearch);
	// assertEquals(0, studies.size());
	//
	// siSearch.getPostopRatingScales().add(
	// PostoperativeRatingScale.CLASS_5);
	// studies = studyDAO.findByStudySearch(sSearch);
	// assertEquals(1, studies.size());
	// assertTrue(any(studies, compose(equalTo(study000.getId()),
	// IHasLongId.getId)));
	//
	// trx.commit();
	//
	// } catch (final Throwable t) {
	// try {
	// if (trx != null && trx.isActive()) {
	// trx.rollback();
	// }
	// } catch (final Throwable rbEx) {
	// logger.error("exception while rolling back", rbEx);
	// }
	// throw t;
	// } finally {
	// if (session != null) {
	// session.close();
	// }
	// }
	// }

	@Test
	public void findStudiesByElectrodeType()
			throws Throwable {
		Session session = null;
		Transaction trx = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dataSnapshotDAO =
					new DataSnapshotDAOHibernate(session);

			// An existing type
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);

				electrodeSearch.setTypes(newHashSet("Depth"));

				final Set<EegStudy> dsResults = dataSnapshotDAO
						.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);

				assertEquals(2, dsResults.size());
			}

			// An non-existing type
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);

				electrodeSearch.setTypes(newHashSet("not a type"));

				final Set<EegStudy> dsResults = dataSnapshotDAO
						.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);

				assertEquals(0, dsResults.size());
			}

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findStudiesByElectrodeTypeAndLocationAndSamplingRate()
			throws Throwable {
		Session session = null;
		Transaction trx = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();

			final IDataSnapshotDAO dataSnapshotDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);

			electrodeSearch.setTypes(newHashSet("Depth"));
			electrodeSearch.setSamplingRateMin(400.45);
			electrodeSearch.setSamplingRateMax(500000.35);
			electrodeSearch.getSides().add(Side.LEFT);

			final Set<EegStudy> preDsResults = dataSnapshotDAO
					.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);
			Set<DataSnapshotSearchResult> dsResults =
					DsSearchResultAssembler.assembleDsSearchResults(
							preDsResults);

			assertEquals(1, dsResults.size());
			assertTrue(any(dsResults,
					compose(equalTo(study000.getPubId()),
							IHasStringId.getId)));

			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	// @Test
	// public void countStudiesByImageTypes() throws Throwable {
	// try {
	// final Transaction trx = HibernateUtil.getSessionFactory()
	// .getCurrentSession().beginTransaction();
	//
	// final IDAOFactory daoFac = DAOFactories
	// .instance(HibernateDAOFactory.class);
	// final IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
	//
	// final IEegStudySearch studySearch = new EegStudySearch();
	// studySearch.getImageTypes().add(Image.Type.MRI);
	//
	// long count = studyDAO
	// .countStudies(studySearch);
	//
	// assertEquals(1, count);
	//
	// studySearch.getImageTypes().add(Image.Type.CT);
	//
	// count = studyDAO.countStudies(studySearch);
	//
	// assertEquals(2, count);
	//
	// trx.commit();
	//
	// } catch (final Throwable t) {
	// try {
	// if (HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().isActive()) {
	// HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().rollback();
	// }
	// } catch (final Throwable rbEx) {
	// logger.error("exception while rolling back", rbEx);
	// }
	// throw t;
	// }
	// }

	@Test
	public void findbyContactTypeAndLocation()
			throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			DataSnapshotDAOHibernate dsDAO =
					new DataSnapshotDAOHibernate(session);

			// Patient 0 has a macro in the left parietal
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);
				electrodeSearch.setContactTypes(newHashSet(ContactType.MACRO));
				electrodeSearch.getSides().add(Side.LEFT);
				electrodeSearch.getLocations().add(Location.PARIETAL);

				final Set<EegStudy> preResults =
						dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);
				Set<DataSnapshotSearchResult> results =
						DsSearchResultAssembler.assembleDsSearchResults(
								preResults);
				assertEquals(1, results.size());
				// final DataSnapshotSearchResult result =
				// getOnlyElement(results);

				// assertNotNull(result.getTimeSeries());
				//
				// assertEquals(1, result.getTimeSeries().size());
				//
				// final TimeSeriesSearchResult tsResult = getOnlyElement(result
				// .getTimeSeries());
				// assertEquals(trace00000.getRevId(), tsResult.getRevId());
			}

			// No patient has a micro in the left parietal
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);
				electrodeSearch.setContactTypes(newHashSet(ContactType.MICRO));
				electrodeSearch.getSides().add(Side.LEFT);
				electrodeSearch.getLocations().add(Location.PARIETAL);

				final Set<EegStudy> results =
						dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);
				assertEquals(0, results.size());
			}

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	// @Test
	// public void countStudiesByImageTypes() throws Throwable {
	// try {
	// final Transaction trx = HibernateUtil.getSessionFactory()
	// .getCurrentSession().beginTransaction();
	//
	// final IDAOFactory daoFac = DAOFactories
	// .instance(HibernateDAOFactory.class);
	// final IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
	//
	// final IEegStudySearch studySearch = new EegStudySearch();
	// studySearch.getImageTypes().add(Image.Type.MRI);
	//
	// long count = studyDAO
	// .countStudies(studySearch);
	//
	// assertEquals(1, count);
	//
	// studySearch.getImageTypes().add(Image.Type.CT);
	//
	// count = studyDAO.countStudies(studySearch);
	//
	// assertEquals(2, count);
	//
	// trx.commit();
	//
	// } catch (final Throwable t) {
	// try {
	// if (HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().isActive()) {
	// HibernateUtil.getSessionFactory().getCurrentSession()
	// .getTransaction().rollback();
	// }
	// } catch (final Throwable rbEx) {
	// logger.error("exception while rolling back", rbEx);
	// }
	// throw t;
	// }
	// }

	@Test
	public void findbyContactTypeAndAnnotationTypeAndLocation()
			throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			DataSnapshotDAOHibernate dsDAO =
					new DataSnapshotDAOHibernate(session);

			final EegStudySearch dsSearch = new EegStudySearch();

			final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
			dsSearch.setContactGroupSearch(electrodeSearch);
			electrodeSearch.setContactTypes(newHashSet(ContactType.MACRO));
			electrodeSearch.getSides().add(Side.LEFT);
			electrodeSearch.getLocations().add(Location.PARIETAL);

			final Set<EegStudy> preResults =
					dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
							tstUsers.regUser1);

			Set<DataSnapshotSearchResult> results =
					DsSearchResultAssembler.assembleDsSearchResults(
							preResults);

			assertEquals(1, results.size());
			// final DataSnapshotSearchResult result = getOnlyElement(results);
			//
			// assertNotNull(result.getTimeSeries());
			//
			// assertEquals(1, result.getTimeSeries().size());
			//
			// final TimeSeriesSearchResult tsResult = getOnlyElement(result
			// .getTimeSeries());
			// assertEquals(trace00000.getRevId(), tsResult.getRevId());

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Test
	public void findbyContactType() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			DataSnapshotDAOHibernate dsDAO =
					new DataSnapshotDAOHibernate(session);

			// Macro only search
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);
				electrodeSearch.setContactTypes(newHashSet(ContactType.MACRO));

				final Set<EegStudy> preResults =
						dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);

				Set<DataSnapshotSearchResult> results =
						DsSearchResultAssembler.assembleDsSearchResults(
								preResults);

				assertEquals(2, results.size());
			}

			// Micro only search
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);
				electrodeSearch.setContactTypes(newHashSet(ContactType.MICRO));

				final Set<EegStudy> preResults =
						dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);

				Set<DataSnapshotSearchResult> results =
						DsSearchResultAssembler.assembleDsSearchResults(
								preResults);

				assertEquals(1, results.size());
			}

			// Macro and Micro only search
			{
				final EegStudySearch dsSearch = new EegStudySearch();

				final ContactGroupSearch electrodeSearch = new ContactGroupSearch();
				dsSearch.setContactGroupSearch(electrodeSearch);
				electrodeSearch.setContactTypes(newHashSet(ContactType.MACRO,
						ContactType.MICRO));

				final Set<EegStudy> preResults =
						dsDAO.findBySearch(dsSearch, tstUsers.btRegUser1,
								tstUsers.regUser1);

				Set<DataSnapshotSearchResult> results =
						DsSearchResultAssembler.assembleDsSearchResults(
								preResults);

				assertEquals(1, results.size());
			}

			// Results from Study000
			// final DataSnapshotSearchResult resultStudy000 = find(
			// results,
			// compose(equalTo(study000.getRevId()),
			// IHasRevId.getRevId));

			// assertNotNull(resultStudy000.getTimeSeries());
			//
			// assertEquals(1, resultStudy000.getTimeSeries().size());

			// final TimeSeriesSearchResult tsResultStudy000 =
			// getOnlyElement(resultStudy000
			// .getTimeSeries());
			// assertEquals(trace00000.getRevId(), tsResultStudy000.getRevId());

			// Results from Study100
			// final DataSnapshotSearchResult resultStudy100 = find(
			// results,
			// compose(equalTo(study100.getRevId()),
			// IHasRevId.getRevId));

			// assertNotNull(resultStudy100.getTimeSeries());
			//
			// assertEquals(1, resultStudy100.getTimeSeries().size());
			//
			// final TimeSeriesSearchResult tsResultStudy100 =
			// getOnlyElement(resultStudy100
			// .getTimeSeries());
			// assertEquals(trace10001.getRevId(), tsResultStudy100.getRevId());

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				trx.rollback();
			}
			throw t;
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
