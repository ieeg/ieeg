/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Pathology;

public class PathologyTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (Pathology etiology : Pathology.values()) {
			switch (etiology) {
				case TUMOR:
					assertEquals("db enums must not change order!", 0,
							etiology.ordinal());
					break;
				case STROKE:
					assertEquals("db enums must not change order!", 1,
							etiology.ordinal());
					break;
				case BRAIN_HEMORRHAGE:
					assertEquals("db enums must not change order!", 2,
							etiology.ordinal());
					break;
				case FOCAL_CORTICAL_DYSPLASIA:
					assertEquals("db enums must not change order!", 3,
							etiology.ordinal());
					break;
				case MESIAL_TEMPORAL_SCLEROSIS:
					assertEquals("db enums must not change order!", 4,
							etiology.ordinal());
					break;
				case ANOXIA:
					assertEquals("db enums must not change order!", 5,
							etiology.ordinal());
					break;
				case AVM:
					assertEquals("db enums must not change order!", 6,
							etiology.ordinal());
					break;
				case ENCEPHALITIS:
					assertEquals("db enums must not change order!", 7,
							etiology.ordinal());
					break;
				case MENINGITIS:
					assertEquals("db enums must not change order!", 8,
							etiology.ordinal());
					break;
				case DEMENTIA:
					assertEquals("db enums must not change order!", 9,
							etiology.ordinal());
					break;
				case HEMISPHERIC_ATROPHY:
					assertEquals("db enums must not change order!", 10,
							etiology.ordinal());
					break;
				case HAMARTOMA:
					assertEquals("db enums must not change order!", 11,
							etiology.ordinal());
					break;
				case CORTICAL_DYSPLASIA:
					assertEquals("db enums must not change order!", 12,
							etiology.ordinal());
					break;
				case CEREBRAL_PALSY:
					assertEquals("db enums must not change order!", 13,
							etiology.ordinal());
					break;
				case DOWN_SYNDROME:
					assertEquals("db enums must not change order!", 14,
							etiology.ordinal());
					break;
				case CRYPTOGENIC:
					assertEquals("db enums must not change order!", 15,
							etiology.ordinal());
					break;
				case TOXIC_OR_METABOLIC:
					assertEquals("db enums must not change order!", 16,
							etiology.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + etiology, true);
					break;
			}
		}
	}
}
