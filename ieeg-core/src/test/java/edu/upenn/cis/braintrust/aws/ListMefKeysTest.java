/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.aws;

import java.util.Set;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * @author John Frommeyer
 * 
 */
public class ListMefKeysTest {
	private static class ClArgs {
		@Parameter(
				names = "--bucketName",
				required = true,
				description = "S3 bucket")
		private String bucketName;

		@Parameter(names = "--mefDir",
				required = true,
				description = "MEF directory")
		private String mefDir;
	}

	public static void main(String[] args) {
		final String m = "main(...)";
		final ClArgs clArgs = new ClArgs();
		new JCommander(clArgs, args);
		final Set<String> fileKeys = AwsUtil.listMefFileKeys(
				AwsUtil.getS3(),
				clArgs.bucketName,
				clArgs.mefDir);
		System.out.println(fileKeys.size());
		for (final String fileKey : fileKeys) {
			System.out.println(fileKey);
		}
	}

}
