/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.NewProjectAce;
import edu.upenn.cis.braintrust.shared.NewUserAce;
import edu.upenn.cis.braintrust.shared.RemoveUserAce;
import edu.upenn.cis.braintrust.shared.UpdateUserAce;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class AclEditorTest {

	@Mock
	private IDataSnapshotDAO dataSnapshotDAO;
	@Mock
	private IUserDAO userDAO;
	@Mock
	private IProjectDAO projectDAO;
	@Mock
	private IPermissionDAO permissionDAO;

	private AclEditor editor;

	private TstObjectFactory tstObjectFactory =
			new TstObjectFactory(true);

	@Before
	public void setupMocks() {
		MockitoAnnotations.initMocks(this);
		// As long as we are just testing shortenActionList() we don't need a
		// populated or persisted acl.
		editor = new AclEditor(
				new ExtAclEntity(),
				CorePermDefs.CORE_MODE_NAME,
				userDAO,
				projectDAO, 
				permissionDAO);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShortenNewUserThenNewUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());
		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);
		final IEditAclAction<?> existingAction = new NewUserAce(firstUserAce);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.READ_PERM);
		final IEditAclAction<?> newAction = new NewUserAce(secondUserAce);

		@SuppressWarnings("unchecked")
		List<IEditAclAction<?>> actions = newArrayList(existingAction,
				newAction);
		editor.shortenActionList(actions);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShortenUpdateUserThenNewUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.READ_PERM);
		final IEditAclAction<?> existingAction = new UpdateUserAce(
				firstUserAce,
				CorePermDefs.OWNER_PERM);
		final IEditAclAction<?> newAction = new NewUserAce(secondUserAce);
		@SuppressWarnings("unchecked")
		List<IEditAclAction<?>> actions = newArrayList(existingAction,
				newAction);
		editor.shortenActionList(actions);
	}

	@Test
	public void testShortenRemoveUserThenNewUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.READ_PERM);
		final RemoveUserAce existingAction = new RemoveUserAce(firstUserAce);
		final NewUserAce newAction = new NewUserAce(secondUserAce);
		List<IEditAclAction<?>> actions = newArrayList();
		actions.add(existingAction);
		actions.add(newAction);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(actions);
		assertEquals(1, shortened.size());
		IEditAclAction<?> resultingAction = getOnlyElement(shortened, null);
		assertTrue(resultingAction instanceof UpdateUserAce);
		final UpdateUserAce updateAction = (UpdateUserAce) resultingAction;
		assertEquals(existingAction.getAce().getAceId(), updateAction.getAce()
				.getAceId());
		assertEquals(existingAction.getAce().getPerms(), updateAction
				.getAce().getPerms());
		assertEquals(1, newAction.getAce().getPerms().size());
		assertEquals(getOnlyElement(newAction.getAce().getPerms()),
				updateAction.getNewPerms());
	}

	@Test
	public void testShortenNewUserThenRemoveUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final NewUserAce existingAction = new NewUserAce(firstUserAce);

		final RemoveUserAce newAction = new RemoveUserAce(firstUserAce);

		List<IEditAclAction<?>> actions = newArrayList();
		actions.add(existingAction);
		actions.add(newAction);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(actions);
		assertEquals(0, shortened.size());
	}

	@Test
	public void testShortenUpdateUserThenRemoveUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.OWNER_PERM);
		final UpdateUserAce existingAction = new UpdateUserAce(firstUserAce,
				CorePermDefs.OWNER_PERM);
		final RemoveUserAce newAction = new RemoveUserAce(secondUserAce);

		List<IEditAclAction<?>> actions = newArrayList();
		actions.add(existingAction);
		actions.add(newAction);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(actions);
		assertEquals(1, shortened.size());
		IEditAclAction<?> resultingAction = getOnlyElement(shortened, null);
		assertTrue(resultingAction instanceof RemoveUserAce);
		final RemoveUserAce resultingRemove = (RemoveUserAce) resultingAction;
		assertEquals(existingAction.getAce().getAceId(), resultingRemove
				.getAce().getAceId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShortenRemoveUserThenRemoveUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.READ_PERM);
		final IEditAclAction<?> existingAction = new RemoveUserAce(firstUserAce);

		final IEditAclAction<?> newAction = new RemoveUserAce(secondUserAce);

		@SuppressWarnings("unchecked")
		List<IEditAclAction<?>> actions = newArrayList(existingAction,
				newAction);
		editor.shortenActionList(actions);
	}

	@Test
	public void testShortenNewUserThenUpdateUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.EDIT_PERM);
		final NewUserAce existingAction = new NewUserAce(firstUserAce);
		final UpdateUserAce newAction = new UpdateUserAce(secondUserAce,
				CorePermDefs.OWNER_PERM);
		List<IEditAclAction<?>> actions = newArrayList();
		actions.add(existingAction);
		actions.add(newAction);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(actions);
		assertEquals(1, shortened.size());
		IEditAclAction<?> resultingAction = getOnlyElement(shortened, null);
		assertTrue(resultingAction instanceof NewUserAce);
		final NewUserAce resultingNew = (NewUserAce) resultingAction;
		assertEquals(existingAction.getAce().getAceId(), resultingNew.getAce()
				.getAceId());
		assertEquals(1, resultingNew.getAce().getPerms().size());
		assertEquals(newAction.getNewPerms(),
				getOnlyElement(resultingNew.getAce().getPerms()));
	}

	@Test
	public void testShortenUpdateUserThenUpdateUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.READ_PERM);
		final UpdateUserAce existingAction = new UpdateUserAce(firstUserAce,
				CorePermDefs.READ_PERM);
		final UpdateUserAce newAction = new UpdateUserAce(secondUserAce,
				CorePermDefs.OWNER_PERM);
		List<IEditAclAction<?>> actions = newArrayList();
		actions.add(existingAction);
		actions.add(newAction);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(actions);
		assertEquals(1, shortened.size());
		IEditAclAction<?> resultingAction = getOnlyElement(shortened, null);
		assertTrue(resultingAction instanceof UpdateUserAce);
		UpdateUserAce resultingUpdate = (UpdateUserAce) resultingAction;
		assertEquals(existingAction.getAce(),
				resultingUpdate.getAce());
		assertEquals(newAction.getNewPerms(),
				resultingUpdate.getNewPerms());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShortenRemoveUserThenUpdateUser() {
		final UserId userId = tstObjectFactory.newUserId();
		final String username = newUuid();
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce firstUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		firstUserAce.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtUserAce secondUserAce = new ExtUserAce(
				userId,
				username,
				targetId,
				aclVersion,
				null,
				null);
		secondUserAce.getPerms().add(CorePermDefs.EDIT_PERM);
		final IEditAclAction<?> existingAction = new RemoveUserAce(firstUserAce);
		final IEditAclAction<?> newAction = new UpdateUserAce(firstUserAce,
				CorePermDefs.OWNER_PERM);
		@SuppressWarnings("unchecked")
		List<IEditAclAction<?>> actions = newArrayList(existingAction,
				newAction);
		editor.shortenActionList(actions);
	}

	@Test
	public void testShortenNewProjectAce() {
		final String projectId = newUuid();
		final String projectName = newUuid();
		final ProjectGroupType groupType = TstObjectFactory
				.randomEnum(ProjectGroupType.class);
		final String targetId = newUuid();
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());
		final ExtProjectAce newAce = new ExtProjectAce(
				projectId,
				projectName,
				groupType,
				targetId,
				aclVersion,
				null,
				null);
		final NewProjectAce newProjectAce = new NewProjectAce(newAce);
		final Set<IEditAclAction<?>> shortened = editor
				.shortenActionList(Collections
						.<IEditAclAction<?>> singletonList(newProjectAce));
		assertTrue(shortened.size() == 1);
		final IEditAclAction<?> actualAction = getOnlyElement(shortened);
		assertEquals(newProjectAce, actualAction);
	}

}
