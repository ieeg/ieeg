/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.google.common.cache.Cache;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.datasnapshot.assembler.ImageAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TimeSeriesAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TsAnnotationAssembler;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.shared.IUser.Status;

/**
 * Test that sessions are closed by {@code DataSnapshotServer}.
 * 
 * @author John Frommeyer
 * 
 */
public class DataSnapshotServerTest {
	private static User authedUser;
	private TsAnnotationDto tsAnn;
	private DataSnapshot dataSnapshot;
	private TstObjectFactory tstObjFac = new TstObjectFactory(true);

	@Mock
	private IDAOFactory daoFac;

	@Before
	public void entitySetup() {
		// DataSnapshotServer has a static initializer that looks at IvProps, so
		// we need to initialize it.
		IvProps.setIvProps(Collections.<String, String> emptyMap());

		IUserService userService = Mockito.mock(IUserService.class);
		UserServiceFactory.setUserService(userService);

		final ImageEntity imageEntity = new ImageEntity();
		imageEntity
				.setFileKey("Xyz_IEED_data/Xyz_001_data/images/IMG-000-0001.jpg");
		imageEntity.setPubId((UUID.randomUUID().toString()));
		imageEntity.setType(ImageType.CT);

		final TimeSeriesEntity tsEntity = new TimeSeriesEntity();
		tsEntity.setPubId((UUID.randomUUID().toString()));
		tsEntity.setLabel("RIS1");
		tsEntity.setFileKey("Xyz_IEED_data/Xyz_001_data/IC_001/RIS1.mef");

		final TsAnnotationEntity tsAnnEntity = new TsAnnotationEntity();
		tsAnnEntity.getAnnotated().add(tsEntity);
		tsAnnEntity.setAnnotator("A tool");
		tsAnnEntity.setCreator(tstObjFac.newUserEntity());
		tsAnnEntity.setType("seizure");
		tsAnnEntity.setStartOffsetUsecs(Long.valueOf(100000000000000L));
		tsAnnEntity.setEndOffsetUsecs(Long.valueOf(110000000000000L));

		dataSnapshot = new DataSnapshot("test dataSnapshot", false);
		dataSnapshot.setRevId(UUID.randomUUID().toString());
		dataSnapshot.getImages().add(ImageAssembler.assemble(imageEntity));
		dataSnapshot.getTimeSeries()
				.add(TimeSeriesAssembler.assemble(tsEntity));
		tsAnn = TsAnnotationAssembler.assemble(tsAnnEntity);
		dataSnapshot.getTsAnnotations().add(tsAnn);

	}

	@Test
	public void testGetDataSnapshot() throws AuthorizationException {
		Transaction transaction = mock(Transaction.class);
		when(transaction.isActive()).thenReturn(true);
		Session session = makeMockSession(transaction);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		when(dsService.getDataSnapshot((User) anyObject(), anyString()))
				.thenReturn(dataSnapshot);

		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer =
				new DataSnapshotServer(
						sessionFac,
						config,
						dsServiceFac,
						daoFac);

		DataSnapshot actualDataSnapshot = dsServer.getDataSnapshot(
				authedUser, "123");
		assertEquals(dataSnapshot, actualDataSnapshot);
		verify(transaction).commit();
		verify(session).close();
	}

	@Test
	public void testGetMEFPath() throws AuthorizationException {
		Transaction trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(true);
		Session session = makeMockSession(trx);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		final String mefPath = "Xyz/012/GAF1.mef";

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		when(
				dsService.getMEFPaths((User) anyObject(), anyString(),
						anyListOf(String.class))).thenReturn(
				singletonList(mefPath));

		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);

		String actualMefPath =
				getOnlyElement(
				dsServer.getMEFPaths(
						authedUser,
						"123",
						singletonList("xyz")));
		assertEquals(mefPath, actualMefPath);
		verify(trx).commit();
		verify(session).close();
	}

	@Test
	public void testGetNonexistentDataSnapshot() throws AuthorizationException {
		Transaction trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(true);
		Session session = makeMockSession(trx);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		when(dsService.getDataSnapshot((User) anyObject(), anyString()))
				.thenReturn(null);

		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);

		DataSnapshot actualDataSnapshot = dsServer.getDataSnapshot(
				authedUser, "123");
		assertNull(actualDataSnapshot);
		verify(trx).commit();
		verify(session).close();
	}

	@Test
	public void testGetDataSnapshotWithException()
			throws AuthorizationException {
		Transaction transaction = mock(Transaction.class);
		Session session = makeMockSession(transaction);
		when(transaction.isActive()).thenReturn(Boolean.TRUE);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		when(dsService.getDataSnapshot((User) anyObject(), anyString()))
				.thenThrow(new HibernateException("Constraint violation"));

		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);
		boolean exception = false;
		try {
			dsServer.getDataSnapshot(authedUser, "123");
		} catch (Throwable e) {
			exception = true;
		}
		assertTrue(exception);
		verify(transaction).rollback();
		verify(session).close();
	}

	@Test
	public void testGetMEFPathWithException() throws AuthorizationException {
		Transaction transaction = mock(Transaction.class);
		when(transaction.isActive()).thenReturn(Boolean.TRUE);
		Session session = makeMockSession(transaction);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		when(
				dsService.getMEFPaths((User) anyObject(), anyString(),
						anyListOf(String.class))).thenThrow(
				new NullPointerException());

		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);

		boolean exception = false;
		try {
			getOnlyElement(dsServer.getMEFPaths(authedUser, "123",
					singleton("xyz")));
		} catch (Throwable e) {
			exception = true;
		}
		assertTrue(exception);
		verify(session).close();
		verify(transaction).rollback();
	}

	@Test
	public void storeClobClosesSession() throws AuthorizationException {
		Transaction transaction = mock(Transaction.class);

		Session session = makeMockSession(transaction);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		DataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);
		when(dsService.storeClob(
				any(User.class),
				anyString(), anyString())).thenReturn("rev-id");
		dsServer.storeClob(authedUser, "123", "clob");
		verify(transaction).commit();
		verify(session).close();
	}

	@Test
	public void storeClobClosesSessionWException()
			throws AuthorizationException {
		Transaction transaction = mock(Transaction.class);
		when(transaction.isActive()).thenReturn(Boolean.TRUE);

		Session session = makeMockSession(transaction);

		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		DataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);
		when(dsService.storeClob(
				any(User.class),
				anyString(), anyString()))
				.thenThrow(new RuntimeException());
		boolean caught = false;
		try {
			dsServer.storeClob(authedUser, "123", "clob");
		} catch (Throwable t) {
			caught = true;
		}
		assertTrue(caught);
		verify(transaction).rollback();
		verify(session).close();
	}

	private static IDataSnapshotServiceFactory makeMockDataSnapshotServiceFactory(
			Session session, IDataSnapshotService dsService) {
		when(session.isOpen()).thenReturn(true);
		IDataSnapshotServiceFactory dsServiceFac = mock(IDataSnapshotServiceFactory.class);
		when(
				dsServiceFac.newInstance(
						eq(session), anyInt(),
						Matchers.<Cache<String, DataSnapshotEntity>> any(),
						Matchers.<Cache<Long, SearchResultCacheEntry>> any()))
				.thenReturn(
						dsService);
		return dsServiceFac;
	}

	private static Configuration makeMockConfig() {
		Configuration config = mock(Configuration.class);
		when(config.getProperty(anyString())).thenReturn("false");
		return config;
	}

	private static SessionFactory makeMockSessionFactory(final Session session) {
		SessionFactory sessionFac = mock(SessionFactory.class);
		when(sessionFac.openSession()).thenReturn(session);
		return sessionFac;
	}

	private static Session makeMockSession(final Transaction transaction) {
		Session session = mock(Session.class);
		when(session.beginTransaction()).thenReturn(transaction);
		when(session.isOpen()).thenReturn(true);
		return session;
	}

	private static Transaction makeMockTransaction() {
		Transaction trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(true);
		return trx;
	}

	@BeforeClass
	public static void classSetUp() throws Throwable {

		final Set<Role> authedUsersRoles = Collections.emptySet();
		authedUser = new User(
				new UserId(0),
				"authedUser",
				"authedPasswd",
				Status.ENABLED,
				authedUsersRoles,
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

	}

	@Test
	public void getRelatedAnalyses() throws AuthorizationException {
		Transaction trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(true);
		Session session = makeMockSession(trx);
		SessionFactory sessionFac = makeMockSessionFactory(session);

		IDataSnapshotService dsService = mock(IDataSnapshotService.class);

		Set<DatasetAndTool> expectedResults = newHashSet();
		when(dsService
				.getRelatedAnalyses(
						any(User.class),
						any(String.class)))
				.thenReturn(expectedResults);

		IDataSnapshotServiceFactory dsServiceFac =
				makeMockDataSnapshotServiceFactory(session, dsService);
		Configuration config = makeMockConfig();
		IDataSnapshotServer dsServer =
				new DataSnapshotServer(sessionFac,
						config,
						dsServiceFac,
						daoFac);

		final String revId = "dont-care-revid";

		Set<DatasetAndTool> actualResults = dsServer
				.getRelatedAnalyses(authedUser, revId);

		assertSame(expectedResults, actualResults);
		verify(dsService).getRelatedAnalyses(authedUser, revId);
		verify(trx).commit();
		verify(session).close();
	}

	@Test
	public void testStoreTsAnnotations() throws Exception {
		final Transaction transaction = mock(Transaction.class);
		when(transaction.isActive()).thenReturn(true);
		final Session session = makeMockSession(transaction);
		when(session.isOpen()).thenReturn(true);

		final SessionFactory sessionFac = makeMockSessionFactory(session);

		final IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		final Set<TsAnnotationDto> annotations = newHashSet();

		final IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		final IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);

		dsServer.storeTsAnnotations(
				authedUser, "snapshotId", annotations);
		verify(transaction).commit();
		verify(session).close();
	}

	@Test
	public void testRemoveTsAnnotations() throws AuthorizationException {
		final Transaction transaction = mock(Transaction.class);
		final Session session = makeMockSession(transaction);

		final SessionFactory sessionFac = makeMockSessionFactory(session);

		final IDataSnapshotService dsService = mock(IDataSnapshotService.class);
		final String revId = "store-ann-revId";
		final Set<String> annotations = newHashSet("ann-to_delete");
		when(
				dsService.removeTsAnnotations(
						authedUser, revId,
						annotations))
				.thenReturn(revId);

		final IDataSnapshotServiceFactory dsServiceFac = makeMockDataSnapshotServiceFactory(
				session, dsService);
		Configuration config = makeMockConfig();
		final IDataSnapshotServer dsServer = new DataSnapshotServer(sessionFac,
				config,
				dsServiceFac, daoFac);

		final String actualRevId = dsServer.removeTsAnnotations(
				authedUser, revId, annotations);
		assertEquals(revId, actualRevId);
		verify(transaction).commit();
		verify(session).close();
	}
}
