/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.upenn.cis.braintrust.model.ImagedContact.Id;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class ImagedContactTest {
	
	private final TstObjectFactory objFac = new TstObjectFactory(false);
	
	@Test
	public void constructor() {

		final ImageEntity imageEntity = new ImageEntity();
		imageEntity.setId(3L);

		final Contact trace = new Contact(
				TstObjectFactory.randomEnum(ContactType.class),
				objFac.newMriCoordinates(),
				objFac.newTalairachCoordinates(),
				objFac.newTimeSeries());
		trace.setId(1L);

		final ImagedContact imagedTrace = new ImagedContact(
				new PixelCoordinates(5, 6),
				imageEntity, trace);
		assertTrue(imageEntity.getImagedContacts().contains(imagedTrace));
		assertTrue(trace.getImagedContacts().contains(imagedTrace));
		assertEquals(imagedTrace.getId().getImageId(), imageEntity.getId());
		assertEquals(imagedTrace.getId().getContactId(), trace.getId());
	}

	@Test
	public void idEquals() {
		final Id id0 = new Id(3L, 4L);
		final Id id1 = new Id(3L, 4L);
		final Id id2 = new Id(6L, 1L);

		assertTrue(id0.equals(id1));
		assertTrue(id0.equals(id0));
		assertFalse(id0.equals(id2));
		assertFalse(id0.equals(new Object()));
	}

	@Test
	public void idHashCode() {
		final Id id0 = new Id(3L, 4L);
		assertEquals(id0.hashCode(), 3L + 4L);
	}

	@Test
	public void equals() {
		final ImageEntity image0 = new ImageEntity();
		image0.setId(3L);

		final Contact trace0 = new Contact(
				TstObjectFactory.randomEnum(ContactType.class),
				objFac.newMriCoordinates(),
				objFac.newTalairachCoordinates(),
				objFac.newTimeSeries());
		trace0.setId(1L);

		final ImagedContact imagedTrace0 = new ImagedContact(
				new PixelCoordinates(5, 6),
				image0, trace0);

		assertTrue(imagedTrace0.equals(imagedTrace0));
		assertFalse(imagedTrace0.equals(new Object()));

		final ImageEntity image1 = new ImageEntity();
		image1.setId(3L);

		final Contact trace1 = new Contact(
				TstObjectFactory.randomEnum(ContactType.class),
				objFac.newMriCoordinates(),
				objFac.newTalairachCoordinates(),
				objFac.newTimeSeries());
		trace1.setId(1L);

		final ImagedContact imagedTrace1 = new ImagedContact(
				new PixelCoordinates(5, 6),
				image1, trace1);

		assertTrue(imagedTrace0.equals(imagedTrace1));

		final ImageEntity image2 = new ImageEntity();
		image2.setId(2L);

		final Contact trace2 = new Contact(
				TstObjectFactory.randomEnum(ContactType.class),
				objFac.newMriCoordinates(),
				objFac.newTalairachCoordinates(),
				objFac.newTimeSeries());
		trace2.setId(1L);

		final ImagedContact imagedTrace2 = new ImagedContact(
				new PixelCoordinates(5, 6),
				image2, trace2);

		assertFalse(imagedTrace0.equals(imagedTrace2));
	}
}
