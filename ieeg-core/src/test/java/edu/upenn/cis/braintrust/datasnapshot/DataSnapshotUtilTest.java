/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.Test;

import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.PixelCoordinates;
import edu.upenn.cis.braintrust.shared.ImageType;

public class DataSnapshotUtilTest {
	@Test
	public void imagedContacts2ImageForTrace() {
		final ImagedContact imagedContact0 = mock(ImagedContact.class);
		when(imagedContact0.getPxCoordinates()).thenReturn(
				new PixelCoordinates(34, 34));
		final ImageEntity image0 = mock(ImageEntity.class);
		when(imagedContact0.getImage()).thenReturn(image0);

		String pubId0 = newUuid();
		when(image0.getPubId()).thenReturn(pubId0);
		when(image0.getFileKey())
				.thenReturn(
						"Xyz_IEED_data/Xyz_IEED_001/Images_001_jpg/3DRenderings/R_Ant-Lat.jpg");
		when(image0.getType()).thenReturn(ImageType.THREE_D_RENDERING);

		final ImagedContact imagedContact1 = mock(ImagedContact.class);
		when(imagedContact1.getPxCoordinates()).thenReturn(
				new PixelCoordinates(34, 34));
		final ImageEntity image1 = mock(ImageEntity.class);

		when(imagedContact1.getImage()).thenReturn(image1);
		String pubId1 = newUuid();
		when(image1.getPubId()).thenReturn(pubId1);
		when(image1.getFileKey())
				.thenReturn(
						"Xyz_IEED_data/Xyz_IEED_001/Images_001_jpg/3DRenderings/L_Ant-Lat.jpg");
		when(image1.getType()).thenReturn(ImageType.THREE_D_RENDERING);

		final Set<ImagedContact> imagedContacts = newHashSet(imagedContact0,
				imagedContact1);

		final Set<ImageForTrace> imagesForTrace =
				DataSnapshotUtil
						.imagedContacts2ImageForTraces(imagedContacts);

		assertEquals(imagedContacts.size(), imagesForTrace.size());

		final ImageForTrace imageForTrace0 = find(
				imagesForTrace,
				compose(equalTo(image0.getFileKey()),
						ImageForTrace.getImageFileKey), null);
		assertNotNull(imageForTrace0);
		assertEquals(pubId0, imageForTrace0.getImagePubId());
		assertEquals(image0.getType().name(), imageForTrace0.getImageType());
		assertEquals(image0.getType(), imageForTrace0.getImageTypeEnum());
		assertEquals(imagedContact0.getPxCoordinates().getX(),
				Integer.valueOf(imageForTrace0.getPixelCoordX()));
		assertEquals(imagedContact0.getPxCoordinates().getY(),
				Integer.valueOf(imageForTrace0.getPixelCoordY()));

		final ImageForTrace imageForTrace1 = find(
				imagesForTrace,
				compose(equalTo(image1.getFileKey()),
						ImageForTrace.getImageFileKey), null);
		assertNotNull(imageForTrace1);
		assertEquals(pubId1, imageForTrace1.getImagePubId());
		assertEquals(image1.getType().name(), imageForTrace1.getImageType());
		assertEquals(image1.getType(), imageForTrace1.getImageTypeEnum());
		assertEquals(imagedContact1.getPxCoordinates().getX(),
				Integer.valueOf(imageForTrace1.getPixelCoordX()));
		assertEquals(imagedContact1.getPxCoordinates().getY(),
				Integer.valueOf(imageForTrace1.getPixelCoordY()));
	}
}
