/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Precipitant;

public class PrecipitantTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (Precipitant precipitant : Precipitant.values()) {
			switch (precipitant) {
				case STRESS:
					assertEquals("db enums must not change order!", 0,
							precipitant.ordinal());
					break;
				case CATAMENIAL:
					assertEquals("db enums must not change order!", 1,
							precipitant.ordinal());
					break;
				case DRUGS:
					assertEquals("db enums must not change order!", 2,
							precipitant.ordinal());
					break;
				case ALCOHOL:
					assertEquals("db enums must not change order!", 3,
							precipitant.ordinal());
					break;
				case SLEEP_DEPRIVATION:
					assertEquals("db enums must not change order!", 4,
							precipitant.ordinal());
					break;
				case NOCTURNAL:
					assertEquals("db enums must not change order!", 5,
							precipitant.ordinal());
					break;
				case UPON_AWAKENING:
					assertEquals("db enums must not change order!", 6,
							precipitant.ordinal());
					break;
				case FEBRILE:
					assertEquals("db enums must not change order!", 7,
							precipitant.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + precipitant, true);
					break;
			}
		}
	}
}
