/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.security;

public class AuthzHandlerTest {

	// private TstUsers users = new TstUsers();
	//
	// private IUserDAO userDAO = new TstUserDAO(users.idsToUsers);
	//
	// private AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// private TstObjectFactory tstObjFac = new TstObjectFactory(true);
	//
	// @Test
	// public void adminCanDoAnything() throws AuthorizationException {
	//
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser1,
	// tstObjFac.newAclEntity(),
	// tstObjFac.newExtAclEntity());
	//
	// IAuthzHandler authzHandler = new AuthzHandler(userDAO);
	// authzHandler.checkReadPermitted(
	// users.adminUser,
	// ds);
	//
	// authzHandler.checkUpdatePermitted(users.adminUser, ds);
	//
	// authzHandler.checkDeletePermitted(users.adminUser, ds);
	//
	// EegStudy study =
	// new EegStudy(
	// new AclEntity(WorldPermissions.READ_ONLY),
	// new ExtAclEntity());
	//
	// authzHandler.checkReadPermitted(users.adminUser, study);
	//
	// authzHandler.checkUpdatePermitted(users.adminUser, study);
	//
	// authzHandler.checkDeletePermitted(users.adminUser, study);
	//
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void moreSpecificWins() throws AuthorizationException {
	// EegStudy study =
	// new EegStudy(
	// tstObjFac.newAclEntityWorldEditable(),
	// tstObjFac.newExtAclEntity());
	// study.getAcl()
	// .getUserAces()
	// .add(tstObjFac.newUserAceReadOnly(study.getAcl(),
	// users.btRegUser1));
	// authzHandler.checkUpdatePermitted(users.regUser1, study);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void regUserCantUpdateStudyWithNoPG() throws
	// AuthorizationException {
	// EegStudy study =
	// new EegStudy(
	// new AclEntity(WorldPermissions.READ_ONLY),
	// new ExtAclEntity());
	//
	// authzHandler.checkUpdatePermitted(users.regUser1, study);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void regUserCantDeleteStudyWithNoPG() throws
	// AuthorizationException {
	// EegStudy study =
	// new EegStudy(
	// new AclEntity(WorldPermissions.READ_ONLY),
	// new ExtAclEntity());
	// authzHandler.checkDeletePermitted(users.regUser1, study);
	// }
	//
	// @Test
	// public void regUser1CanReadRegUser2() throws AuthorizationException {
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	//
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// authzHandler.checkReadPermitted(users.regUser1, ds);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void regUser1CantUpdateRegUser2() throws AuthorizationException {
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	//
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// authzHandler.checkUpdatePermitted(users.regUser1, ds);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void regUser1CantDeleteRegUser2() throws AuthorizationException {
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	//
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// authzHandler.checkDeletePermitted(users.regUser1, ds);
	// }
	//
	// @Test
	// public void regUser2CanReadReadRegUser2() throws AuthorizationException {
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	//
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// // Don't want to read it because of world permissions
	// ds.getAcl().setWorldAce(WorldPermissions.NONE);
	// authzHandler.checkReadPermitted(users.regUser2, ds);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void regUser1CantReadRegUser2NoWorldPerm()
	// throws AuthorizationException {
	//
	// // Don't want to read it because of world permissions
	//
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	// ds.getAcl().setWorldAce(WorldPermissions.NONE);
	//
	// authzHandler.checkReadPermitted(users.regUser1, ds);
	// }
	//
	// @Test(expected = UnauthorizedException.class)
	// public void nonexistentUserNoWorld() throws AuthorizationException {
	// userDAO = mock(IUserDAO.class);
	// when(userDAO.get(any(UserId.class))).thenReturn(null);
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	// AclEntity acl = tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2);
	// acl.setWorldAce(WorldPermissions.NONE);
	// authzHandler.checkReadPermitted(
	// users.regUser1,
	// new Dataset(
	// "some-label",
	// users.btRegUser2,
	// acl,
	// tstObjFac.newExtAclEntity()));
	// }
	//
	// @Test
	// public void nonexistentUserWithWorld() throws AuthorizationException {
	// userDAO = mock(IUserDAO.class);
	// when(userDAO.get(any(UserId.class))).thenReturn(null);
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	// AclEntity acl = tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2);
	// acl.setWorldAce(WorldPermissions.READ_ONLY);
	// authzHandler.checkReadPermitted(
	// users.regUser1,
	// new Dataset(
	// "some-label",
	// users.btRegUser2,
	// acl,
	// tstObjFac.newExtAclEntity()));
	// }
	//
	// @Test
	// public void checkManageAclPermittedStudy() {
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	// EegStudy study = tstObjFac.newEegStudy();
	// authzHandler.checkManageAclPermitted(users.adminUser, study);
	// }
	//
	// @Test
	// public void checkManageAclPermittedDataset() {
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// AclEntity acl = tstObjFac.newAclEntity();
	// new UserAceEntity(acl, users.btRegUser1, Permissions.OWNER_EDIT);
	// new UserAceEntity(acl, users.btRegUser2, Permissions.EDIT);
	// Dataset ds = new Dataset("checkManageAclPermittedDataset",
	// users.btRegUser1, acl, tstObjFac.newExtAclEntity());
	//
	// authzHandler.checkManageAclPermitted(users.regUser1, ds);
	// boolean exception = false;
	// try {
	// authzHandler.checkManageAclPermitted(users.regUser2, ds);
	// } catch (AuthorizationException e) {
	// exception = true;
	// }
	// assertTrue(
	// "regUser2 should not be able to modify ACL with only EDIT perms.",
	// exception);
	// }
	//
	// @Test
	// public void checkManageAclPermittedStudyAcl() {
	// AuthzHandler authzHandler = new AuthzHandler(userDAO);
	//
	// AclEntity acl = tstObjFac.newAclEntity();
	// new UserAceEntity(acl, users.btRegUser1, Permissions.OWNER_EDIT);
	// new UserAceEntity(acl, users.btRegUser2, Permissions.EDIT);
	// EegStudy study = new EegStudy(acl, tstObjFac.newExtAclEntity());
	//
	// authzHandler.checkManageAclPermitted(users.regUser1, study);
	// boolean exception = false;
	// try {
	// authzHandler.checkManageAclPermitted(users.regUser2, study);
	// } catch (AuthorizationException e) {
	// exception = true;
	// }
	// assertTrue(
	// "regUser2 should not be able to modify ACL with only EDIT perms.",
	// exception);
	// }
	//
	// public void regUser1CanCRUDRegUser2WWordPerms()
	// throws AuthorizationException {
	//
	// DataSnapshotEntity ds = new Dataset(
	// "some-label",
	// users.btRegUser2,
	// tstObjFac
	// .newAclEntityUserOwnedAndWorldReadable(users.btRegUser2),
	// tstObjFac.newExtAclEntity());
	//
	// ds
	// .getAcl()
	// .setWorldAce(WorldPermissions.EDIT);
	// authzHandler.checkReadPermitted(users.regUser1, ds);
	// authzHandler.checkReadPermitted(users.regUser1, ds);
	// authzHandler.checkReadPermitted(users.regUser1, ds);
	//
	// }

}
