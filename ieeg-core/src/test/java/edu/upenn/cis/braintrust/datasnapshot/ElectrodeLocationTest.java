/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Location;

public class ElectrodeLocationTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (Location location : Location.values()) {
			switch (location) {
				case FRONTAL:
					assertEquals("db enums must not change order!", 0,
							location.ordinal());
					break;
				case TEMPORAL:
					assertEquals("db enums must not change order!", 1,
							location.ordinal());
					break;
				case PARIETAL:
					assertEquals("db enums must not change order!", 2,
							location.ordinal());
					break;
				case OCCIPITAL:
					assertEquals("db enums must not change order!", 3,
							location.ordinal());
					break;
				case INTERHEMISPHERIC:
					assertEquals("db enums must not change order!", 4,
							location.ordinal());
					break;
				case UNKOWN:
					assertEquals("db enums must not change order!", 5,
							location.ordinal());
					break;
				case NA:
					assertEquals("db enums must not change order!", 6,
							location.ordinal());
					break;
				case AMYGDALA:
					assertEquals("db enums must not change order!", 7,
							location.ordinal());
					break;
				case HIPPOCAMPUS:
					assertEquals("db enums must not change order!", 8,
							location.ordinal());
					break;
				case THALAMUS:
					assertEquals("db enums must not change order!", 9,
							location.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + location, true);
					break;
			}
		}
	}
}
