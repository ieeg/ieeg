/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;

import java.util.Date;
import java.util.Set;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.TaskStatus;

/**
 * @author Sam Donnelly
 * 
 */
public class TaskHistoryTest {
	@Test
	public void ctor() {
		JobHistory jHist = new JobHistory();
		Set<String> tsPubIds = newHashSet("1", "2");
		TaskHistory tHist =
				new TaskHistory(
						jHist,
						newUuid(),
						TaskStatus.COMPLETE,
						1L,
						2L,
						tsPubIds,
						"worker",
						new Date());
		assertNotNull(tHist.getTsPubIds());
		assertNotSame(tsPubIds, tHist.getTsPubIds());
		assertEquals(tsPubIds, tHist.getTsPubIds());
	}
}
