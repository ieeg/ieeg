/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author John Frommeyer
 * 
 */
public class SessionDAOHibernateTest {

	private final TstObjectFactory objFac = new TstObjectFactory(false);
	private String activeSessionToken;

	@Before
	public void setUp() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();

			final UserEntity user = objFac.newUserEntity();
			session.save(user);

			final SessionEntity activeSession = objFac.newSession(user);
			activeSessionToken = activeSession.getToken();
			session.save(activeSession);

			final Calendar sixtyDaysAgo = Calendar.getInstance();
			sixtyDaysAgo.add(Calendar.DAY_OF_MONTH, -60);
			final SessionEntity sixtyDaySession = objFac.newSession(user);
			sixtyDaySession.setLastAccessTime(sixtyDaysAgo.getTime());
			session.save(sixtyDaySession);

			final Calendar thirtyDaysAgo = Calendar.getInstance();
			thirtyDaysAgo.add(Calendar.DAY_OF_MONTH, -30);
			thirtyDaysAgo.add(Calendar.HOUR, -1);
			final SessionEntity thirtyDaySession = objFac.newSession(user);
			thirtyDaySession.setLastAccessTime(thirtyDaysAgo.getTime());
			session.save(thirtyDaySession);

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void deleteOldSessions() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);

			final Calendar thirtyDaysAgoCal = Calendar.getInstance();
			thirtyDaysAgoCal.add(Calendar.DAY_OF_MONTH, -30);
			final Date thirtyDaysAgo = thirtyDaysAgoCal.getTime();
			final int deleted = sessionDAO
					.deleteSessionsOlderThan(thirtyDaysAgo);
			assertEquals(2, deleted);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final List<SessionEntity> allSessions = sessionDAO.findAll();
			assertEquals(1, allSessions.size());
			final SessionEntity actualActiveSession = getOnlyElement(allSessions);
			assertEquals(activeSessionToken, actualActiveSession.getToken());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}
}
