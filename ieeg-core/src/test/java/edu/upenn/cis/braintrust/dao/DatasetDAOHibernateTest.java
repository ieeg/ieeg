/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DatasetDAOHibernate;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.db.mefview.shared.IUser.Status;

/**
 * Tests for dataset dao methods.
 * 
 * @author John Frommeyer
 * 
 */
public class DatasetDAOHibernateTest {

	private static Logger logger = LoggerFactory
			.getLogger(DatasetDAOHibernateTest.class);
	private static EegStudy study;
	private static Dataset dataset;
	private static Set<TsAnnotationEntity> datasetTsAnns = newHashSet();
	private static User authedUser;
	private static TimeSeriesEntity timeSeries;
	private static final TsAnnotationEntity studyTsAnn = new TsAnnotationEntity();
	private static final TsAnnotationEntity datasetTsAnn = new TsAnnotationEntity();
	private static TstUsers users;

	@BeforeClass
	public static void classSetUp() throws Throwable {

		final Set<Role> authedUsersRoles = Collections.emptySet();
		authedUser = new User(
				new UserId(0),
				"authedUser",
				"authedPasswd",
				Status.ENABLED,
				authedUsersRoles,
				null,
				"email@nowhere.com",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

		Transaction trx = null;
		try {

			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			
			Session s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();
			final BrainTrustDb db = new BrainTrustDb(s);
			users = new TstUsers(s);

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = newArrayList();
			final List<TsAnnotationEntity> tsAnnotations = newArrayList();
			final List<SnapshotUsage> usages = newArrayList();

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permissionDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			final Patient patient0 = patients.get(0);

			study = getOnlyElement(
					getOnlyElement(patient0.getAdmissions()).getStudies());

			// Create dataset
			dataset = new Dataset(
					"My dataset",
					users.btRegUser1,
					SecurityUtil.createUserOwnedWorldReadableAcl(
							users.btRegUser1,
							permissionDAO));
			SnapshotUsage testUsage = new SnapshotUsage(dataset,
					users.btRegUser1,
					// SnapshotUsage.USAGE_TYPE.VIEWS,
					ProvenanceLogEntry.UsageType.VIEWS.ordinal(),
					null,
					null);

			usages.add(testUsage);

			final ImageEntity image = find(study.getImages(),
					compose(equalTo("0001"), ImageEntity.getPubId));
			dataset.getImages().add(image);

			final TimeSeriesEntity tSeries = find(
					study.getTimeSeries(),
					compose(equalTo("0002-1------------------------------"),
							TimeSeriesEntity.getPubId));

			timeSeries = tSeries;
			dataset.getTimeSeries().add(tSeries);

			studyTsAnn.getAnnotated().add(timeSeries);
			datasetTsAnn.getAnnotated().add(timeSeries);

			studyTsAnn.setAnnotator("my-annotation-tool");
			datasetTsAnn.setAnnotator("my-annotation-tool");

			studyTsAnn.setCreator(users.btRegUser1);
			datasetTsAnn.setCreator(users.btRegUser1);

			studyTsAnn.setDescription("A description");
			datasetTsAnn.setDescription("A description");

			studyTsAnn.setStartOffsetUsecs(Long
					.valueOf(0L));
			datasetTsAnn.setStartOffsetUsecs(Long
					.valueOf(0L));

			studyTsAnn.setEndOffsetUsecs(Long
					.valueOf(1000000L));
			datasetTsAnn.setEndOffsetUsecs(Long
					.valueOf(1000000L));

			studyTsAnn.setType("seizure");
			datasetTsAnn.setType("seizure");

			studyTsAnn.setLayerDefault(study);
			datasetTsAnn.setLayerDefault(dataset);

			studyTsAnn.setParent(study);

			datasetTsAnn.setParent(dataset);
			datasetTsAnns.add(datasetTsAnn);

			datasets.add(dataset);
			tsAnnotations.add(studyTsAnn);
			tsAnnotations.add(datasetTsAnn);
			db.saveEverything(patients, null,
					tsAnnotations,
					null, datasets, usages);
			trx.commit();
			s.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void findNonExistentDatasetByRevIdTest() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			DatasetDAOHibernate dsDAO = new DatasetDAOHibernate(s);

			final DataSnapshotEntity ds = dsDAO
					.findByPubId("does-not-exist");
			assertNull(ds);
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}

	}

	@Test
	public void findDatasetByRevIdTest() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();

			DatasetDAOHibernate dsDAO = new DatasetDAOHibernate(s);

			final DataSnapshotEntity actualDs = dsDAO
					.findByPubId(dataset.getPubId());

			assertNotNull(actualDs);
			assertEquals(dataset.getLabel(), actualDs.getLabel());
			assertEquals(
					dataset.getImages().size(),
					actualDs.getImages().size());

			assertEquals(
					dataset.getTimeSeries().size(),
					actualDs.getTimeSeries().size());
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}

	}

}
