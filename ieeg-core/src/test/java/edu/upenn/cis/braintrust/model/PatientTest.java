/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

public class PatientTest {
	@Test
	public void addCase() {
		final Patient patient = new Patient();
		final HospitalAdmission epilepsyCase = new HospitalAdmission();

		patient.addAdmission(epilepsyCase);
		assertEquals(1, patient.getAdmissions().size());
		assertSame(epilepsyCase, getOnlyElement(patient.getAdmissions()));
		assertSame(patient, epilepsyCase.getParent());
	}

	@Test
	public void addAdmission() {
		final Patient patient = new Patient();
		final HospitalAdmission admission = new HospitalAdmission();

		patient.addAdmission(admission);

		assertEquals(1, patient.getAdmissions().size());
		assertSame(admission, getOnlyElement(patient.getAdmissions()));
		assertSame(patient, admission.getParent());
	}

	@Test
	public void removeCase() {
		final Patient patient = new Patient();
		final HospitalAdmission epilepsyCase = new HospitalAdmission();

		patient.addAdmission(epilepsyCase);
		patient.removeAdmission(epilepsyCase);
		assertEquals(0, patient.getAdmissions().size());
		assertNull(epilepsyCase.getParent());
	}

	@Test
	public void removeAdmission() {
		final Patient patient = new Patient();
		final HospitalAdmission admission = new HospitalAdmission();

		patient.addAdmission(admission);
		patient.removeAdmission(admission);
		assertEquals(0, patient.getAdmissions().size());
		assertNull(admission.getParent());
	}
}
