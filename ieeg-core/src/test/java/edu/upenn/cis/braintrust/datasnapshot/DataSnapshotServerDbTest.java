/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.in;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.removeIf;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static edu.upenn.cis.braintrust.CompareUtil.areEqual;
import static edu.upenn.cis.braintrust.CompareUtil.compare;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotSame;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import edu.upenn.cis.braintrust.CompareUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.SecurityUtil;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.AnimalDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.TimeSeriesDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.JobDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TsAnnotationAssembler;
import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.model.AnalysisTask;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UnauthorizedException;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.AnimalMetadata;
import edu.upenn.cis.braintrust.shared.ContactGroupMetadata;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.DatasetIdAndVersion;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.IHasName;
import edu.upenn.cis.braintrust.shared.IHasOptionalStringId;
import edu.upenn.cis.braintrust.shared.IHasStringId;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetConflictException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TooManyTsAnnsException;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.OrganizationsForTests;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser.Status;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.testhelper.ClientTstObjectFactory;

/**
 * Test that sessions are closed by {@code DataSnapshotServer}.
 * 
 * @author John Frommeyer
 */
public class DataSnapshotServerDbTest {
	private static final String USER_CREATED_TS_REV_ID = "user-created-ts-001-----------------";
	private SessionFactory sessFac = HibernateUtil.getSessionFactory();
	private User authedUser;
	private Dataset dataset;
	private EegStudy study;
	private EegStudy deletableStudy;
	private TimeSeriesEntity timeSeries;
	private TimeSeriesEntity timeSeries2;
	private TsAnnotationEntity datasetTsAnn0;
	private TsAnnotationEntity datasetTsAnn1;
	private TsAnnotationEntity datasetTsAnn2;
	private TstObjectFactory tstObjectFactory;
	private TstUsers dbUsers;
	private ToolEntity tool;
	private ToolEntity tool2;
	private JobEntity toBeInitializedJob;
	private JobEntity initializedJob;
	private SnapshotUsage testUsage;
	private AnalysisTask unscheduledTask;
	private Set<TsAnnotationEntity> studyTsAnns = newHashSet();
	private Set<TsAnnotationEntity> datasetTsAnns = newHashSet();
	private User initializedJobUser;
	private ExperimentEntity experiment0;
	private SessionToken activeSessToken;
	private Date activeSessLastAccess;
	private SessionToken thirtyDaySessToken;
	private Date thirtyDaySessLastAccess;
	private SessionToken expiredSessToken;
	private Date expiredSessLastAccess;
	private SessionToken loggedOutSessToken;
	private Date loggedOutSessLastAccess;
	private SessionToken disabledAcctSessToken;
	private Date disabledAcctSessLastAccess;
	private ImagedContact imagedContact00;
	private ImagedContact imagedContact11;
	private ImagedContact imagedContact21;

	private ClientTstObjectFactory clientTstObjectFactory = new ClientTstObjectFactory();

	@Mock
	private IObjectServer mockPersistentServer;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	// Clear out any mocks used after each test.
	@After
	public void clearPersistentStorageServer() {
		StorageFactory.setPersistentServer(null);
	}

	private static DataSnapshotServer getDataSnapshotServer() {
		return new DataSnapshotServer(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration(),
				new DataSnapshotServiceFactory(),
				100,
				new HibernateDAOFactory());
	}

	private static DataSnapshotServer getDataSnapshotServer(
			int maxAnnCnt) {
		return new DataSnapshotServer(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration(),
				new DataSnapshotServiceFactory(),
				maxAnnCnt,
				new HibernateDAOFactory());
	}

	@Test
	public void datasetGetDataSnapshotTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		DataSnapshot actualDs = dsServer.getDataSnapshot(
				authedUser, dataset.getPubId());
		assertNotNull(actualDs);
		assertEquals(dataset.getLabel(), actualDs.getLabel());
		assertEquals(dataset.getImages().size(), actualDs.getImages().size());
		assertEquals(
				datasetTsAnns.size(),
				actualDs.getTsAnnotations().size());
		assertEquals(
				dataset.getTimeSeries().size(),
				actualDs.getTimeSeries().size());
	}

	@Test
	public void eegStudyGetDataSnapshotTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		DataSnapshot actualDs = dsServer.getDataSnapshot(
				authedUser, study.getPubId());
		assertNotNull(actualDs);
		assertEquals(study.getLabel(), actualDs.getLabel());
		assertEquals(study.getImages().size(), actualDs.getImages().size());
		assertEquals(
				studyTsAnns.size(),
				actualDs.getTsAnnotations().size());
		assertEquals(
				study.getTimeSeries().size(),
				actualDs.getTimeSeries().size());
	}

	@Test(expected = DataSnapshotNotFoundException.class)
	public void nonExistentDataSnapshotTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		dsServer.getDataSnapshot(authedUser,
				"does-not-exist");
	}

	@Test
	public void datasetGetMEFPathsSingletonTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		String actualTs = getOnlyElement(dsServer.getMEFPaths(authedUser,
				dataset
						.getPubId(), singleton(timeSeries
						.getPubId())));
		assertNotNull(actualTs);
		assertEquals(timeSeries.getFileKey(), actualTs);
	}

	@Test
	public void datasetGetMEFPathsTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();

		List<String> actualTss =
				dsServer.getMEFPaths(
						authedUser,
						study.getPubId(),
						newArrayList(
								timeSeries.getPubId(),
								timeSeries2.getPubId()));
		assertEquals(2, actualTss.size());

		assertEquals(timeSeries.getFileKey(), actualTss.get(0));
		assertEquals(timeSeries2.getFileKey(), actualTss.get(1));

	}

	@Test
	public void eegStudyGetMEFPathTest() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		String actualTs =
				getOnlyElement(
				dsServer.getMEFPaths(authedUser, study
						.getPubId(),
						singleton(timeSeries
								.getPubId())));
		assertNotNull(actualTs);
		assertEquals(timeSeries.getFileKey(), actualTs);

	}

	@Test
	public void eegStudyGetMEFPathManagedSessionTest() throws Exception {
		for (int i = 0; i < 100; i++) {
			// loop so we make sure we're not occuyping jdbc connections
			Session s = null;
			try {
				s = HibernateUtil.getSessionFactory().openSession();
				ManagedSessionContext.bind(s);
				IDataSnapshotServer dsServer = getDataSnapshotServer();

				dsServer.getDataSnapshot(authedUser, study.getPubId());

				List<String> actualTss =
						dsServer.getMEFPaths(
								authedUser,
								study.getPubId(),
								newArrayList(
										timeSeries.getPubId(),
										timeSeries2.getPubId()));
				assertEquals(2, actualTss.size());

				assertEquals(timeSeries.getFileKey(), actualTss.get(0));
				assertEquals(timeSeries2.getFileKey(), actualTss.get(1));

				actualTss =
						dsServer.getMEFPaths(
								authedUser,
								study.getPubId(),
								newArrayList(
										timeSeries.getPubId(),
										timeSeries2.getPubId()));
				assertEquals(2, actualTss.size());

				assertEquals(timeSeries.getFileKey(), actualTss.get(0));
				assertEquals(timeSeries2.getFileKey(), actualTss.get(1));
			} catch (Exception e) {
				throw e;
			} finally {
				PersistenceUtil.close(s);
				ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
			}
		}
	}

	@Before
	public void testSetUp() throws Exception {
		// we need to initialize IvProps for DataSnapshotServerFactory
		IvProps.setIvProps(Collections.<String, String> emptyMap());

		Set<Role> authedUsersRoles = newHashSet(Role.USER);

		authedUser = new User(
				new UserId(3),
				"authedUsername",
				"authedPasswd",
				Status.ENABLED,
				authedUsersRoles,
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null,
						null, null, null, null, null, null, null));

		IUserService userService = Mockito.mock(IUserService.class);
		when(userService.findUserByUid(any(UserId.class))).thenReturn(
				authedUser);
		UserServiceFactory.setUserService(userService);

		SessionAndTrx sessAndTrx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(sess);
			tstObjectFactory = new TstObjectFactory(
					permissionDAO.findStarCoreReadPerm(),
					permissionDAO.findStarCoreOwnerPerm());

			PermissionEntity readPerm = tstObjectFactory.getReadPerm();

			final BrainTrustDb db = new BrainTrustDb(sess);

			dbUsers = new TstUsers(sess);

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = newArrayList();
			final List<TsAnnotationEntity> tsAnnotations = newArrayList();

			BuildDb.buildDb(
					patients,
					readPerm);

			final Patient patient0 = patients.get(0);

			study = getOnlyElement(
					getOnlyElement(patient0.getAdmissions())
							.getStudies());

			deletableStudy = getOnlyElement(
					getOnlyElement(patients.get(1).getAdmissions())
							.getStudies());

			// Create dataset

			dataset = new Dataset(
					"My dataset",
					dbUsers.btRegUser1,
					SecurityUtil.createUserOwnedWorldReadableAcl(
							dbUsers.btRegUser1,
							new PermissionDAOHibernate(sess)));

			testUsage = new SnapshotUsage(dataset,
					dbUsers.btRegUser1,
					// SnapshotUsage.USAGE_TYPE.VIEWS,
					ProvenanceLogEntry.UsageType.VIEWS.ordinal(),
					null,
					null);

			List<SnapshotUsage> usages = Lists.newArrayList();
			usages.add(testUsage);

			timeSeries =
					find(study.getTimeSeries(),
							compose(equalTo("0000-0------------------------------"),
									TimeSeriesEntity.getPubId));
			assertNotNull(timeSeries);

			timeSeries2 =
					find(study.getTimeSeries(),
							compose(equalTo("0000-1------------------------------"),
									TimeSeriesEntity.getPubId));

			assertNotNull(timeSeries2);

			dataset.getTimeSeries().add(timeSeries);
			dataset.getTimeSeries().add(timeSeries2);

			TsAnnotationEntity studyTsAnn = new TsAnnotationEntity();

			studyTsAnn.setAnnotator("my-annotation-tool");
			studyTsAnn.setCreator(dbUsers.btRegUser1);
			studyTsAnn.setDescription("A description");
			studyTsAnn.setStartOffsetUsecs(Long
					.valueOf(0L));
			studyTsAnn.setEndOffsetUsecs(Long
					.valueOf(1000000L));

			studyTsAnn.setType("seizure");

			studyTsAnn.getAnnotated().add(timeSeries);
			studyTsAnn.getAnnotated().add(timeSeries2);
			studyTsAnn.setParent(study);
			studyTsAnn.setLayerDefault(study);
			studyTsAnns.add(studyTsAnn);

			TsAnnotationEntity studyAnn2 = new TsAnnotationEntity();

			studyAnn2.setAnnotator("my-annotation-tool");
			studyAnn2.setCreator(dbUsers.btRegUser1);

			studyAnn2.setDescription("A description 2");
			studyAnn2.setStartOffsetUsecs(Long
					.valueOf(9999876543210000L));
			studyAnn2.setEndOffsetUsecs(Long
					.valueOf(9999996543210000L));

			studyAnn2.setType("seizure");

			studyAnn2.getAnnotated().add(timeSeries);
			studyAnn2.getAnnotated().add(timeSeries2);
			studyAnn2.setParent(study);
			studyAnn2.setLayerDefault(study);
			studyTsAnns.add(studyAnn2);

			datasetTsAnn0 = new TsAnnotationEntity();

			datasetTsAnn0.setAnnotator("datasetTsAnn");
			datasetTsAnn0.setCreator(dbUsers.btRegUser1);
			datasetTsAnn0.setDescription("an annotation for datasetTsAnn");
			datasetTsAnn0.setStartOffsetUsecs(Long
					.valueOf(0L));
			datasetTsAnn0.setEndOffsetUsecs(Long
					.valueOf(1L));
			datasetTsAnn0.setType("seizure");
			datasetTsAnn0.getAnnotated().add(timeSeries);
			datasetTsAnn0.getAnnotated().add(timeSeries2);
			datasetTsAnn0.setParent(dataset);
			datasetTsAnn0.setLayerDefault(dataset);
			datasetTsAnns.add(datasetTsAnn0);

			datasetTsAnn1 = tstObjectFactory
					.newTsAnnotation(
							newHashSet(timeSeries, timeSeries2),
							dataset,
							dbUsers.btRegUser1);
			datasetTsAnn1.setStartOffsetUsecs(2L);
			datasetTsAnn1.setEndOffsetUsecs(3L);
			datasetTsAnns.add(datasetTsAnn1);

			datasetTsAnn2 = tstObjectFactory
					.newTsAnnotation(
							newHashSet(timeSeries, timeSeries2),
							dataset,
							dbUsers.btRegUser1);
			datasetTsAnn2.setLayer("layer 2");
			datasetTsAnn2.setStartOffsetUsecs(2L);
			datasetTsAnn2.setEndOffsetUsecs(3L);
			datasetTsAnns.add(datasetTsAnn2);

			TimeSeriesEntity datasetOnlyTimeSeries = new TimeSeriesEntity();
			datasetOnlyTimeSeries.setPubId((
					USER_CREATED_TS_REV_ID));
			datasetOnlyTimeSeries.setFileKey("path/to/my/LL_LAG1.mef");
			datasetOnlyTimeSeries.setLabel("LL_LAG1");

			dataset.getTimeSeries().add(datasetOnlyTimeSeries);

			Set<TimeSeriesEntity> extraTimeSeries = newHashSet(datasetOnlyTimeSeries);

			tsAnnotations.add(studyTsAnn);
			tsAnnotations.add(studyAnn2);
			tsAnnotations.add(datasetTsAnn0);
			tsAnnotations.add(datasetTsAnn1);
			tsAnnotations.add(datasetTsAnn2);

			datasets.add(dataset);

			{// Create tool ACL and tool

				tool = tstObjectFactory.newTool(dbUsers.btRegUser1);

				ExtAclEntity toolExtAcl =
						SecurityUtil.createUserOwnedWorldReadableAcl(
								dbUsers.btRegUser1,
								permissionDAO);

				tool.setExtAcl(toolExtAcl);

				ExtUserAceEntity readAce =
						new ExtUserAceEntity(
								dbUsers.btRegUser2,
								toolExtAcl,
								readPerm);
				toolExtAcl.getUserAces().add(readAce);

				sess.saveOrUpdate(tool);
			}
			// A Job with no tasks that we can initialize in a test
			toBeInitializedJob = tstObjectFactory.newJob();
			sess.saveOrUpdate(toBeInitializedJob);

			// A Job that has been initialized with one task.
			initializedJob = tstObjectFactory.newJob();
			unscheduledTask = tstObjectFactory.newTask(initializedJob);
			unscheduledTask.setStatus(TaskStatus.UNSCHEDULED);
			initializedJob.getTasks().add(unscheduledTask);
			sess.saveOrUpdate(initializedJob);
			initializedJobUser =
					new User(
							new UserId(initializedJob.getUserId()),
							"initializedJobUser", "initializedJobUser",
							Status.ENABLED,
							singleton(Role.USER),
							null,
							"email",
							new UserProfile("x", "y", "z", "w", "v", null,
									null, null, null, null, null, null, null,
									null, null));

			{ // Create a second tool ACL and tool

				tool2 = tstObjectFactory.newTool(dbUsers.btRegUser1);

				ExtAclEntity toolExtAcl =
						SecurityUtil.createUserOwnedWorldReadableAcl(
								dbUsers.btRegUser1,
								permissionDAO);

				tool2.setExtAcl(toolExtAcl);

				ExtUserAceEntity readAce =
						new ExtUserAceEntity(
								dbUsers.btRegUser2,
								toolExtAcl,
								readPerm);
				toolExtAcl.getUserAces().add(readAce);

				sess.saveOrUpdate(tool2);
			}

			// Create some Sessions
			final SessionEntity activeSession = tstObjectFactory
					.newSession(dbUsers.btRegUser1);
			activeSessToken = new SessionToken(activeSession.getToken());
			final Calendar oneHourAgo = Calendar.getInstance();
			oneHourAgo.add(Calendar.HOUR_OF_DAY, -1);
			activeSession.setLastAccessTime(oneHourAgo.getTime());
			activeSessLastAccess = activeSession.getLastAccessTime();
			sess.save(activeSession);

			final Calendar sixtyDaysAgo = Calendar.getInstance();
			sixtyDaysAgo.add(Calendar.DAY_OF_MONTH, -60);

			// A logged out session
			final SessionEntity loggedOutSession = tstObjectFactory
					.newSession(dbUsers.btRegUser2);
			loggedOutSession.setLastAccessTime(sixtyDaysAgo.getTime());
			loggedOutSession.setStatus(SessionStatus.LOGGED_OUT);
			loggedOutSessToken = new SessionToken(loggedOutSession.getToken());
			loggedOutSessLastAccess = loggedOutSession.getLastAccessTime();
			sess.save(loggedOutSession);

			// A disabled account session
			final SessionEntity disabledAccountSession = tstObjectFactory
					.newSession(dbUsers.btRegUser2);
			disabledAccountSession.setLastAccessTime(sixtyDaysAgo.getTime());
			disabledAccountSession.setStatus(SessionStatus.ACCOUNT_DISABLED);
			disabledAcctSessToken = new SessionToken(
					disabledAccountSession.getToken());
			disabledAcctSessLastAccess = disabledAccountSession
					.getLastAccessTime();
			sess.save(disabledAccountSession);

			// An expired session
			final SessionEntity expiredSession = tstObjectFactory
					.newSession(dbUsers.btRegUser2);
			expiredSession.setLastAccessTime(sixtyDaysAgo.getTime());
			expiredSession.setStatus(SessionStatus.EXPIRED);
			expiredSessToken = new SessionToken(expiredSession.getToken());
			expiredSessLastAccess = expiredSession.getLastAccessTime();
			sess.save(expiredSession);

			final Calendar thirtyDaysAgo = Calendar.getInstance();
			thirtyDaysAgo.add(Calendar.DAY_OF_MONTH, -30);
			thirtyDaysAgo.add(Calendar.HOUR, -1);
			final SessionEntity thirtyDaySession = tstObjectFactory
					.newSession(dbUsers.btRegUser1);
			thirtyDaySession.setLastAccessTime(thirtyDaysAgo.getTime());
			thirtyDaySessToken = new SessionToken(
					thirtyDaySession.getToken());
			sess.save(thirtyDaySession);
			thirtyDaySessLastAccess = thirtyDaySession
					.getLastAccessTime();

			db.saveEverything(
					patients,
					extraTimeSeries,
					tsAnnotations,
					null,
					datasets,
					usages);

			// sess.saveOrUpdate(montagedChAnn1);
			// sess.saveOrUpdate(montagedChAnn2);
			// sess.saveOrUpdate(montagedChAnn3);

			SpeciesEntity species0 = new SpeciesEntity("species0");
			StrainEntity strain0 = new StrainEntity(species0, "strain0");
			sess.saveOrUpdate(species0);

			AnimalEntity animal0 = new AnimalEntity("animal0",
					OrganizationsForTests.SPIKE, strain0);

			ExtAclEntity experiment0Acl = SecurityUtil
					.createUserOwnedWorldReadableAcl(
							dbUsers.btRegUser1,
							permissionDAO);
			experiment0 = tstObjectFactory
					.newExperiment(
							animal0,
							experiment0Acl);
			final TimeSeriesEntity exp0Ts0 = tstObjectFactory.newTimeSeries();
			final Contact exp0Contact0 = tstObjectFactory.newContact(exp0Ts0);
			final TimeSeriesEntity exp0Ts1 = tstObjectFactory.newTimeSeries();
			final Contact exp0Contact1 = tstObjectFactory.newContact(exp0Ts1);
			final ContactGroup exp0Cg0 = tstObjectFactory.newContactGroup();
			exp0Cg0.addContact(exp0Contact0);
			exp0Cg0.addContact(exp0Contact1);
			experiment0.getRecording().addContactGroup(exp0Cg0);
			sess.save(animal0);

			// ImagedContacts created after everything is saved.
			assertTrue(study.getImages().size() >= 3);
			final ImageEntity[] studyImages = new ImageEntity[3];
			for (int i = 0; i < 3; i++) {
				studyImages[i] = get(study.getImages(), i);
			}
			dataset.getImages().add(studyImages[0]);

			final ContactGroup electrode = getFirst(study.getRecording()
					.getContactGroups(), null);
			assertNotNull(electrode);

			assertTrue(electrode.getContacts().size() >= 2);
			final Contact[] contacts = new Contact[2];
			for (int i = 0; i < 2; i++) {
				contacts[i] = get(electrode.getContacts(), i);
			}

			imagedContact00 = tstObjectFactory.newImagedContact(studyImages[0],
					contacts[0]);
			imagedContact11 = tstObjectFactory.newImagedContact(studyImages[1],
					contacts[1]);
			imagedContact21 = tstObjectFactory.newImagedContact(studyImages[2],
					contacts[1]);

			PersistenceUtil.commit(sessAndTrx);
		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}
	}

	@Test
	public void storeClob() throws Exception {

		IDataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer();

		String dsRevId = dataset.getPubId();

		String clob = "some-clob";
		dataSnapshotServer.storeClob(authedUser, dsRevId, clob);

		String actualClob =
				dataSnapshotServer.getClob(
						authedUser,
						dsRevId);
		assertEquals(clob, actualClob);

		String clob1 = "some-clob-1";

		String dsRevId1 = dataSnapshotServer.storeClob(
				authedUser, dsRevId, "some-clob-1");

		actualClob = dataSnapshotServer.getClob(
				authedUser, dsRevId1);

		assertEquals(clob1, actualClob);

	}

	@Test
	public void deriveDatasetIncludeEverything() throws Exception {

		IDataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer();

		String toolLabel = newUuid();
		String dsLabel = newUuid();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				null,
				null);
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(dsLabel, actualDatasnapshot.getLabel());

		assertEquals(
				study.getTimeSeries().size(),
				actualDatasnapshot.getTimeSeries().size());
		for (TimeSeriesDto ts : actualDatasnapshot.getTimeSeries()) {
			assertNotNull(find(study.getTimeSeries(),
					compose(equalTo(ts.getId()), IHasPubId.getPubId),
					null));
		}

		assertEquals(
				studyTsAnns.size(),
				actualDatasnapshot.getTsAnnotations().size());

		for (TsAnnotationDto actualTsa : actualDatasnapshot
				.getTsAnnotations()) {
			boolean foundMatch = false;
			for (TsAnnotationDto expectedTsa : studyAsDs
					.getTsAnnotations()) {
				if (actualTsa.hasValuesEqualTo(expectedTsa)) {
					foundMatch = true;
					break;
				}
			}
			assertTrue(foundMatch);
		}

		Set<DatasetAndTool> datasetAndTools = dataSnapshotServer
				.getRelatedAnalyses(authedUser, study.getPubId());

		DatasetAndTool datasetAndTool = find(
				datasetAndTools,
				compose(equalTo(toolLabel), DatasetAndTool.getToolLabel),
				null);
		assertNotNull(datasetAndTool);
		assertEquals(dsRevId, datasetAndTool.getDsRevId());
	}

	@Test
	public void deriveDatasetIncludeTimeSeries() throws Exception {
		TimeSeriesEntity timeSeries = find(
				study.getTimeSeries(),
				compose(equalTo("0000-0------------------------------"),
						TimeSeriesEntity.getPubId));
		String dsLabel = newUuid();
		String toolLabel = newUuid();
		IDataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				singleton(timeSeries.getPubId()),
				null);
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(authedUser,
				study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(dsLabel, actualDatasnapshot.getLabel());

		assertEquals(
				actualDatasnapshot.getTimeSeries().size(), 1);
		assertEquals(timeSeries.getPubId(),
				getOnlyElement(actualDatasnapshot.getTimeSeries())
						.getId());

		assertEquals(
				actualDatasnapshot.getTsAnnotations().size(),
				studyTsAnns.size());

		for (TsAnnotationDto actualTsa : actualDatasnapshot
				.getTsAnnotations()) {
			boolean foundMatch = false;
			for (TsAnnotationDto expectedTsa : studyAsDs
					.getTsAnnotations()) {
				if (actualTsa.hasValuesEqualTo(expectedTsa,
						singleton(timeSeries.getPubId()))) {
					foundMatch = true;
					break;
				}
			}
			System.out.println("ts ann: " + actualTsa);
			assertTrue(foundMatch);
		}

		Set<DatasetAndTool> datasetAndTools = dataSnapshotServer
				.getRelatedAnalyses(authedUser, study.getPubId());

		DatasetAndTool datasetAndTool = find(datasetAndTools,
				compose(equalTo(toolLabel), DatasetAndTool.getToolLabel), null);
		assertNotNull(datasetAndTool);
		assertEquals(dsRevId, datasetAndTool.getDsRevId());
	}

	@Test
	public void deriveDatasetIncludeTimeSeriesAndTsAnnotations()
			throws Exception {
		TimeSeriesEntity timeSeries = find(
				study.getTimeSeries(),
				compose(equalTo("0000-0------------------------------"),
						TimeSeriesEntity.getPubId));
		TsAnnotationEntity tsAnn = get(studyTsAnns, 0);
		String dsLabel = newUuid();
		String toolLabel = newUuid();
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				singleton(timeSeries.getPubId()),
				singleton(tsAnn.getPubId()));
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());
		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(dsLabel, actualDatasnapshot.getLabel());

		assertEquals(
				actualDatasnapshot.getTimeSeries().size(), 1);
		assertEquals(timeSeries.getPubId(),
				getOnlyElement(actualDatasnapshot.getTimeSeries())
						.getId());

		assertEquals(
				actualDatasnapshot.getTsAnnotations().size(), 1);

		for (TsAnnotationDto actualTsa : actualDatasnapshot
				.getTsAnnotations()) {
			boolean foundMatch = false;
			for (TsAnnotationDto excpectedTsa : studyAsDs
					.getTsAnnotations()) {
				if (actualTsa.hasValuesEqualTo(excpectedTsa,
						singleton(timeSeries.getPubId()))) {
					foundMatch = true;
					break;
				}
			}
			assertTrue(foundMatch);
		}

		Set<DatasetAndTool> datasetAndTools = dataSnapshotServer
				.getRelatedAnalyses(authedUser, study.getPubId());

		DatasetAndTool datasetAndTool = find(datasetAndTools,
				compose(equalTo(toolLabel), DatasetAndTool.getToolLabel), null);
		assertNotNull(datasetAndTool);
		assertEquals(dsRevId, datasetAndTool.getDsRevId());
	}

	@Test
	public void deriveDatasetIncludeTsAnnotations()
			throws Exception {

		TsAnnotationEntity tsAnn = get(studyTsAnns, 0);
		String toolLabel = newUuid();
		String dsLabel = newUuid();
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				null,
				newHashSet(tsAnn.getPubId()));
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());
		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(dsLabel, actualDatasnapshot.getLabel());

		assertEquals(
				actualDatasnapshot.getTimeSeries().size(), study
						.getTimeSeries().size());
		for (TimeSeriesDto ts : actualDatasnapshot.getTimeSeries()) {
			assertNotNull(find(study.getTimeSeries(),
					compose(equalTo(ts.getId()), IHasPubId.getPubId),
					null));
		}

		assertEquals(
				actualDatasnapshot.getTsAnnotations().size(), 1);
		for (TsAnnotationDto actualTsa : actualDatasnapshot
				.getTsAnnotations()) {
			boolean foundMatch = false;
			for (TsAnnotationDto excpectedTsa : studyAsDs
					.getTsAnnotations()) {
				if (actualTsa.hasValuesEqualTo(excpectedTsa)) {
					foundMatch = true;
					break;
				}
			}
			assertTrue(foundMatch);
		}

		Set<DatasetAndTool> datasetAndTools = dataSnapshotServer
				.getRelatedAnalyses(authedUser, study.getPubId());

		DatasetAndTool datasetAndTool = find(datasetAndTools,
				compose(equalTo(toolLabel), DatasetAndTool.getToolLabel), null);
		assertNotNull(datasetAndTool);
		assertEquals(dsRevId, datasetAndTool.getDsRevId());
	}

	// regression test: we had a bug where if you deriving annotations was
	// ignoring whether or not the
	// time series that was annotated was included
	@Test
	public void deriveDatasetIncludeTimeSeriesAndAllTsAnnotations()
			throws Exception {

		TimeSeriesEntity timeSeries =
				find(study.getTimeSeries(),
						compose(equalTo("0000-0------------------------------"),
								TimeSeriesEntity.getPubId));
		String dsLabel = newUuid();
		String toolLabel = newUuid();
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				singleton(timeSeries.getPubId()),
				null);
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());
		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(dsLabel, actualDatasnapshot.getLabel());

		assertEquals(1, actualDatasnapshot.getTimeSeries().size());
		assertEquals(timeSeries.getPubId(),
				getOnlyElement(actualDatasnapshot.getTimeSeries())
						.getId());

		assertEquals(2, actualDatasnapshot.getTsAnnotations().size());

		for (TsAnnotationDto actualTsa : actualDatasnapshot
				.getTsAnnotations()) {
			boolean foundMatch = false;
			for (TsAnnotationDto expectedTsa : studyAsDs
					.getTsAnnotations()) {
				if (actualTsa.hasValuesEqualTo(
						expectedTsa,
						singleton(timeSeries.getPubId()))) {
					foundMatch = true;
					break;
				}
			}
			assertTrue(foundMatch);
		}

		Set<DatasetAndTool> datasetAndTools = dataSnapshotServer
				.getRelatedAnalyses(authedUser, study.getPubId());

		DatasetAndTool datasetAndTool = find(datasetAndTools,
				compose(equalTo(toolLabel), DatasetAndTool.getToolLabel), null);
		assertNotNull(datasetAndTool);
		assertEquals(dsRevId, datasetAndTool.getDsRevId());
	}

	// @Test
	// public void deleteLayer() throws Exception {
	// IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();
	//
	// dataSnapshotServer.deleteLayer(
	// authedUser,
	// new LayerId(layer.getId()));
	//
	// SessionAndTrx sessAndTrx = null;
	// try {
	//
	// sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
	// Session sess = sessAndTrx.session;
	// LayerEntity actualLayer = (LayerEntity) sess.get(LayerEntity.class,
	// layer.getId());
	// assertNull(actualLayer);
	// PersistenceUtil.commit(sessAndTrx);
	// } catch (Exception e) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw e;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// }
	//
	// }

	@Test
	public void deleteDataSnapshot() throws Exception {
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String toolLabel = newUuid();
		String dsLabel = newUuid();

		String dsRevId = dataSnapshotServer.deriveDataset(authedUser,
				study.getPubId(), dsLabel, toolLabel, null, null);
		DataSnapshot actualDatasnapshot = dataSnapshotServer.getDataSnapshot(
				authedUser,
				dsRevId);

		// Add in a clob for good measure
		dsRevId = dataSnapshotServer
				.storeClob(authedUser, dsRevId, "some-clob");

		// And a montage
		final EEGMontage montage = clientTstObjectFactory.newEEGMontage();
		for (final TimeSeriesDto ts : actualDatasnapshot.getTimeSeries()) {
			montage.getPairs().add(new EEGMontagePair(ts.getLabel(), null));
		}
		dataSnapshotServer.createEditEegMontage(authedUser, montage,
				dsRevId);

		dataSnapshotServer.deleteDataset(
				authedUser,
				new DatasetIdAndVersion(dsRevId, -1),
				false,
				false);
		boolean exception = false;
		try {
			dataSnapshotServer.getDataSnapshot(authedUser, dsRevId);
		} catch (DataSnapshotNotFoundException e) {
			exception = true;
		}
		assertTrue(exception);

		// Let's make sure we didn't delete every annotation in the db
		assertNotNull(dataSnapshotServer.getDataSnapshot(authedUser,
				study.getPubId()));
		DataSnapshot studyAsDs =
				dataSnapshotServer
						.getDataSnapshot(authedUser, study.getPubId());
		assertEquals(2, studyAsDs.getTsAnnotations().size());

		// Verify that the attached annotation is deleted
		Session session =
				null;
		Transaction trx = null;
		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();
			ITsAnnotationDAO tsAnnotationDAO = new
					TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration());

			List<TsAnnotationEntity> annotations = tsAnnotationDAO.findAll();

			for (TsAnnotationDto tsAnn : actualDatasnapshot.getTsAnnotations())
			{
				assertNull(find(annotations, compose(equalTo(tsAnn.getId()),
						IHasPubId.getPubId), null));
			}

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void deleteStudy() throws Exception {
		// Cleared by @After method
		StorageFactory.setPersistentServer(mockPersistentServer);

		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String undeletableStudyId = study.getPubId();

		boolean authzException = false;
		try {
			// authedUser is not an owner, so should get authz exception.
			dataSnapshotServer.deleteDataset(
					authedUser,
					new DatasetIdAndVersion(undeletableStudyId, -1),
					false,
					false);
		} catch (UnauthorizedException e) {
			authzException = true;
		}
		assertTrue(authzException);
		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));
		boolean conflictException = false;
		try {
			// adminUser okay authz-wise, but the study has a derived
			// dataset so
			// is undeletable.
			dataSnapshotServer.deleteDataset(
					dbUsers.adminUser,
					new DatasetIdAndVersion(undeletableStudyId, -1),
					false,
					false);
		} catch (DatasetConflictException e) {
			conflictException = true;
		}
		assertTrue(conflictException);
		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));

		final String deletableStudyId = deletableStudy.getPubId();
		final Long subjectId = deletableStudy.getParent().getParent()
				.getId();

		// Add some annotations
		final DataSnapshot beforeAnn = dataSnapshotServer.getDataSnapshot(
				authedUser,
				deletableStudyId);
		final Set<TimeSeriesDto> tsDtos = beforeAnn.getTimeSeries();
		final TimeSeriesDto tsDto = tsDtos.iterator().next();
		final TsAnnotationDto ann1 = tstObjectFactory
				.newTsAnnotationDto(newHashSet(tsDto));
		ann1.setExternalId(null);
		ann1.setId(null);
		final TsAnnotationDto ann2 = tstObjectFactory
				.newTsAnnotationDto(tsDtos);
		ann2.setExternalId(null);
		ann2.setId(null);
		dataSnapshotServer.storeTsAnnotations(
				dbUsers.adminUser,
				deletableStudyId,
				newHashSet(ann1, ann2));
		DataSnapshot actualStudy = dataSnapshotServer.getDataSnapshot(
				authedUser,
				deletableStudyId);

		// And a montage
		final EEGMontage montage = clientTstObjectFactory.newEEGMontage();
		for (final TimeSeriesDto ts : actualStudy.getTimeSeries()) {
			montage.getPairs().add(new EEGMontagePair(ts.getLabel(), null));
		}
		dataSnapshotServer.createEditEegMontage(authedUser, montage,
				actualStudy.getId());

		dataSnapshotServer.deleteDataset(
				dbUsers.adminUser,
				new DatasetIdAndVersion(deletableStudyId, -1),
				false,
				false);

		verify(mockPersistentServer, times(tsDtos.size())).deleteItem(
				any(IStoredObjectReference.class));
		boolean exception = false;
		try {
			dataSnapshotServer.getDataSnapshot(
					authedUser,
					deletableStudyId);
		} catch (DataSnapshotNotFoundException e) {
			exception = true;
		}
		assertTrue(exception);

		// Let's make sure we didn't delete every annotation in the db
		assertNotNull(dataSnapshotServer.getDataSnapshot(authedUser,
				undeletableStudyId));
		DataSnapshot undeletableStudyAsDs =
				dataSnapshotServer
						.getDataSnapshot(authedUser, undeletableStudyId);
		assertEquals(2, undeletableStudyAsDs.getTsAnnotations().size());

		// Verify that the attached annotation is deleted
		Session session =
				null;
		Transaction trx = null;
		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();

			final IEegStudyDAO studyDOA = new EegStudyDAOHibernate(session);
			final EegStudy deletedStudy = studyDOA
					.findByNaturalId(deletableStudyId);
			assertNull(deletedStudy);

			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final Patient subject = patientDAO.get(subjectId);
			assertNotNull(subject);
			final ITsAnnotationDAO tsAnnotationDAO = new
					TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration());

			final List<TsAnnotationEntity> annotations = tsAnnotationDAO
					.findAll();

			for (TsAnnotationDto tsAnn : actualStudy.getTsAnnotations())
			{
				assertNull(find(annotations,
						compose(equalTo(tsAnn.getId()),
								IHasPubId.getPubId), null));
			}

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void deleteStudyAndEmptySubject() throws Exception {
		StorageFactory.setPersistentServer(mockPersistentServer);
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String studyId = study.getPubId();
		final int studyTsCount = study.getTimeSeries().size();
		final Long subjectId = study.getParent().getParent().getId();
		final String derivedId = dataset.getPubId();

		dataSnapshotServer.deleteDataset(
				authedUser,
				new DatasetIdAndVersion(derivedId, -1),
				false,
				true);

		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));
		Session session =
				null;
		Transaction trx = null;
		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();
			final IDataSnapshotDAO datasnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity deletedDataset = datasnapshotDAO
					.findByNaturalId(derivedId);
			assertNull(deletedDataset);

			final IEegStudyDAO studyDOA = new EegStudyDAOHibernate(session);
			final EegStudy undeletedStudy = studyDOA
					.findByNaturalId(studyId);
			assertNotNull(undeletedStudy);

			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final Patient undeletedPatient = patientDAO.get(subjectId);
			assertNotNull(undeletedPatient);

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

		// Now delete study with deleteEmptySubject == true. Should remove
		// patient
		dataSnapshotServer.deleteDataset(
				dbUsers.adminUser,
				new DatasetIdAndVersion(studyId, -1),
				false,
				true);

		verify(mockPersistentServer, times(studyTsCount)).deleteItem(
				any(IStoredObjectReference.class));

		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();

			final IEegStudyDAO studyDOA = new EegStudyDAOHibernate(session);
			final EegStudy deletedStudy = studyDOA
					.findByNaturalId(studyId);
			assertNull(deletedStudy);

			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			final Patient deletedPatient = patientDAO.get(subjectId);
			assertNull(deletedPatient);

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void deleteExperiment() throws Exception {
		StorageFactory.setPersistentServer(mockPersistentServer);
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String expId = experiment0.getPubId();
		final Long subjectId = experiment0.getParent().getId();

		boolean authzException = false;
		try {
			// regUser2 is not an owner, so should get authz exception.
			dataSnapshotServer.deleteDataset(
					dbUsers.regUser2,
					new DatasetIdAndVersion(expId, -1),
					false,
					false);
		} catch (UnauthorizedException e) {
			authzException = true;
		}
		assertTrue(authzException);
		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));

		// Add some annotations
		final DataSnapshot beforeAnn = dataSnapshotServer.getDataSnapshot(
				dbUsers.regUser1,
				expId);
		final Set<TimeSeriesDto> tsDtos = beforeAnn.getTimeSeries();
		final TimeSeriesDto tsDto = tsDtos.iterator().next();
		final TsAnnotationDto ann1 = tstObjectFactory
				.newTsAnnotationDto(newHashSet(tsDto));
		ann1.setExternalId(null);
		ann1.setId(null);
		final TsAnnotationDto ann2 = tstObjectFactory
				.newTsAnnotationDto(tsDtos);
		ann2.setExternalId(null);
		ann2.setId(null);
		dataSnapshotServer.storeTsAnnotations(
				dbUsers.regUser1,
				expId,
				newHashSet(ann1, ann2));
		DataSnapshot actualExp = dataSnapshotServer.getDataSnapshot(
				dbUsers.regUser1,
				expId);

		// And a montage
		final EEGMontage montage = clientTstObjectFactory.newEEGMontage();
		for (final TimeSeriesDto ts : actualExp.getTimeSeries()) {
			montage.getPairs().add(new EEGMontagePair(ts.getLabel(), null));
		}
		dataSnapshotServer.createEditEegMontage(
				dbUsers.regUser1,
				montage,
				actualExp.getId());

		dataSnapshotServer.deleteDataset(
				dbUsers.regUser1,
				new DatasetIdAndVersion(expId, -1),
				false,
				false);
		verify(mockPersistentServer, times(tsDtos.size())).deleteItem(
				any(IStoredObjectReference.class));
		boolean exception = false;
		try {
			dataSnapshotServer.getDataSnapshot(
					authedUser,
					expId);
		} catch (DataSnapshotNotFoundException e) {
			exception = true;
		}
		assertTrue(exception);

		// Let's make sure we didn't delete every annotation in the db
		assertNotNull(dataSnapshotServer.getDataSnapshot(authedUser,
				study.getPubId()));
		DataSnapshot undeletableStudyAsDs =
				dataSnapshotServer
						.getDataSnapshot(authedUser, study.getPubId());
		assertEquals(2, undeletableStudyAsDs.getTsAnnotations().size());

		// Verify that the attached annotation is deleted
		Session session =
				null;
		Transaction trx = null;
		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();

			final IExperimentDAO expDOA = new ExperimentDAOHibernate(session);
			final ExperimentEntity deletedExp = expDOA
					.findByNaturalId(expId);
			assertNull(deletedExp);

			final IAnimalDAO animalDAO = new AnimalDAOHibernate(session);
			final AnimalEntity subject = animalDAO.get(subjectId);
			assertNotNull(subject);

			final ITsAnnotationDAO tsAnnotationDAO = new
					TsAnnotationDAOHibernate(session,
							HibernateUtil.getConfiguration());

			final List<TsAnnotationEntity> annotations = tsAnnotationDAO
					.findAll();

			for (TsAnnotationDto tsAnn : actualExp.getTsAnnotations())
			{
				assertNull(find(annotations, compose(equalTo(tsAnn.getId()),
						IHasPubId.getPubId), null));
			}

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void deleteExperimentAndEmptySubject() throws Exception {
		StorageFactory.setPersistentServer(mockPersistentServer);
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String expId = experiment0.getPubId();
		final Long subjectId = experiment0.getParent().getId();
		final String derivedId = dataSnapshotServer.deriveDataset(
				authedUser,
				expId,
				newUuid(),
				newUuid(),
				null,
				null);

		// Shouldn't be able to delete experiment with derived dataset
		boolean conflictException = false;
		try {
			dataSnapshotServer.deleteDataset(
					dbUsers.regUser1,
					new DatasetIdAndVersion(expId, -1),
					false,
					true);
		} catch (DatasetConflictException e) {
			conflictException = true;
		}
		assertTrue(conflictException);
		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));

		dataSnapshotServer.deleteDataset(
				authedUser,
				new DatasetIdAndVersion(derivedId, -1),
				false,
				true);
		verify(mockPersistentServer, never()).deleteItem(
				any(IStoredObjectReference.class));
		Session session =
				null;
		Transaction trx = null;
		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();
			final IDataSnapshotDAO datasnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity deletedDataset = datasnapshotDAO
					.findByNaturalId(derivedId);
			assertNull(deletedDataset);

			final IExperimentDAO expDAO = new ExperimentDAOHibernate(session);
			final ExperimentEntity undeletedExp = expDAO
					.findByNaturalId(expId);
			assertNotNull(undeletedExp);

			final IAnimalDAO subjectDAO = new AnimalDAOHibernate(session);
			final AnimalEntity undeletedSubject = subjectDAO.get(subjectId);
			assertNotNull(undeletedSubject);

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

		// Now delete experiment with deleteEmptySubject == true. Should remove
		// animal
		final int tsCount = experiment0.getTimeSeries().size();
		dataSnapshotServer.deleteDataset(
				dbUsers.regUser1,
				new DatasetIdAndVersion(expId, -1),
				false,
				true);
		verify(mockPersistentServer, times(tsCount)).deleteItem(
				any(IStoredObjectReference.class));

		try {
			session =
					HibernateUtil.getSessionFactory().openSession();
			trx =
					session.beginTransaction();

			final IExperimentDAO expDAO = new ExperimentDAOHibernate(session);
			final ExperimentEntity deletedExp = expDAO
					.findByNaturalId(expId);
			assertNull(deletedExp);

			final IAnimalDAO subjectDAO = new AnimalDAOHibernate(session);
			final AnimalEntity deletedSubject = subjectDAO.get(subjectId);
			assertNull(deletedSubject);

			trx.commit();
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void removeTimeSeriesDatasetStudy()
			throws Exception {
		final int origTsCount = study.getTimeSeries().size();

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		boolean exception = false;
		try {
			dataSnapshotServer.removeTimeSeriesFromDataset(
					authedUser,
					study.getPubId(),
					newHashSet(timeSeries.getPubId()));
		} catch (DatasetNotFoundException x) {
			exception = true;
		}
		assertTrue(exception);
		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						dbUsers.adminUser,
						study.getPubId());
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount, actualDatasnapshot.getTimeSeries().size());
	}

	@Test
	public void removeTimeSeriesDataset()
			throws Exception {
		final int origTsCount = dataset.getTimeSeries().size();
		final int origAnnCount = datasetTsAnns.size();
		final int toBeRemovedAnnCount =
				size(filter(
						datasetTsAnns,
						compose(equalTo(singleton(timeSeries)),
								TsAnnotationEntity.getAnnotated)));

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String dsRevId = dataSnapshotServer.removeTimeSeriesFromDataset(
				authedUser,
				dataset.getPubId(),
				singleton(timeSeries.getPubId()));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount - 1, actualDatasnapshot.getTimeSeries().size());
		assertEquals(origAnnCount - toBeRemovedAnnCount,
				actualDatasnapshot
						.getTsAnnotations()
						.size());

		// now check to make sure that the annotations only annotate what we'd
		// like
		for (TsAnnotationDto tsAnn : actualDatasnapshot.getTsAnnotations()) {
			assertNotNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries2.getPubId()),
							IHasStringId.getId),
					null));
			assertNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries.getPubId()),
							IHasStringId.getId),
					null));
		}
	}

	@Test
	public void removeLayer()
			throws Exception {
		final int origTsCount = dataset.getTimeSeries().size();
		final int origAnnCount = datasetTsAnns.size();
		final int toBeRemovedAnnCount =
				size(filter(
						datasetTsAnns,
						compose(equalTo(TsAnnotationEntity
								.getLayerDefault(dataset)),
								TsAnnotationEntity.getLayer)));

		DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		int removed = dataSnapshotServer.removeTsAnnotationsByLayer(
				authedUser,
				dataset.getPubId(),
				TsAnnotationEntity.getLayerDefault(dataset));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dataset.getPubId());
		assertNotNull(actualDatasnapshot);
		int size = actualDatasnapshot
				.getTsAnnotations()
				.size();
		assertEquals(origAnnCount - toBeRemovedAnnCount,
				actualDatasnapshot
						.getTsAnnotations()
						.size());

		// now check to make sure that the annotations only annotate what we'd
		// like
		for (TsAnnotationDto tsAnn : actualDatasnapshot.getTsAnnotations()) {
			assertNotNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries.getPubId()),
							IHasStringId.getId),
					null));

			assertNotNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries2.getPubId()),
							IHasStringId.getId),
					null));
		}
	}

	@Test
	public void removeTimeSeriesDatasetAll() throws Exception {
		final int origTsCount = dataset.getTimeSeries().size();
		final int origAnnCount = datasetTsAnns.size();
		final int toBeRemovedAnnCount =
				size(filter(
						datasetTsAnns,
						compose(equalTo((Set<TimeSeriesEntity>) newHashSet(
								timeSeries, timeSeries2)),
								TsAnnotationEntity.getAnnotated)));

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String dsRevId = dataSnapshotServer
				.removeTimeSeriesFromDataset(
						authedUser,
						dataset.getPubId(),
						newHashSet(timeSeries.getPubId(),
								timeSeries2.getPubId()));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount - 2, actualDatasnapshot.getTimeSeries().size());
		assertEquals(origAnnCount - toBeRemovedAnnCount,
				actualDatasnapshot
						.getTsAnnotations()
						.size());

		// now check to make sure that the annotations only annotate what we'd
		// like
		for (TsAnnotationDto tsAnn : actualDatasnapshot.getTsAnnotations()) {
			assertNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries.getPubId()),
							IHasStringId.getId),
					null));
			assertNull(find(tsAnn.getAnnotated(),
					compose(equalTo(timeSeries2.getPubId()),
							IHasStringId.getId),
					null));
		}
	}

	@Test
	public void removeTimeSeriesDatasetLastReference()
			throws Exception {

		final int origTsCount = dataset.getTimeSeries().size();
		final int origAnnCount = datasetTsAnns.size();
		final int toBeRemovedAnnCount =
				size(filter(
						datasetTsAnns,
						compose(equalTo(singleton(timeSeries)),
								TsAnnotationEntity.getAnnotated)));

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String dsRevId = dataSnapshotServer.removeTimeSeriesFromDataset(
				authedUser,
				dataset.getPubId(),
				newHashSet(timeSeries.getPubId(), USER_CREATED_TS_REV_ID));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount - 2, actualDatasnapshot.getTimeSeries().size());
		assertEquals(origAnnCount - toBeRemovedAnnCount, actualDatasnapshot
				.getTsAnnotations()
				.size());

		// Verify that time series is deleted.
		Session sess = null;
		Transaction trx = null;
		try {
			Session s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			final ITimeSeriesDAO timeSeriesDAO = new TimeSeriesDAOHibernate(s);
			final TimeSeriesEntity userCreatedTimeSeries = timeSeriesDAO
					.findByNaturalId(USER_CREATED_TS_REV_ID);
			assertNull(userCreatedTimeSeries);
			final TimeSeriesEntity studyTimeSeries = timeSeriesDAO
					.findByNaturalId(timeSeries.getPubId());
			assertNotNull(studyTimeSeries);
			trx.commit();
			s.close();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void addTimeSeriesToDatasetStudy()
			throws Exception {
		final int origTsCount = study.getTimeSeries().size();
		final TimeSeriesEntity newTs = find(
				study.getTimeSeries(),
				compose(equalTo("0000-1------------------------------"),
						TimeSeriesEntity.getPubId));
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		boolean exception = false;
		try {
			dataSnapshotServer.addTimeSeriesToDataset(
					authedUser,
					study.getPubId(),
					newHashSet(newTs.getPubId()));
		} catch (DatasetNotFoundException _) {
			exception = true;
		}
		assertTrue(exception);
		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						study.getPubId());
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount, actualDatasnapshot.getTimeSeries().size());
	}

	@Test
	public void addTimeSeriesToDataset()
			throws Exception {

		final int origTsCount = dataset.getTimeSeries().size();
		final TimeSeriesEntity newTs = find(
				study.getTimeSeries(),
				compose(equalTo("0000-2------------------------------"),
						TimeSeriesEntity.getPubId));

		final DataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer();

		final String dsRevId = dataSnapshotServer.addTimeSeriesToDataset(
				authedUser,
				dataset.getPubId(),
				newHashSet(newTs.getPubId()));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount + 1, actualDatasnapshot.getTimeSeries().size());
	}

	@Test
	public void addTimeSeriesToDatasetAlreadyAdded()
			throws Exception {
		final int origTsCount = dataset.getTimeSeries().size();
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String dsRevId = dataSnapshotServer.addTimeSeriesToDataset(
				authedUser,
				dataset.getPubId(),
				newHashSet(timeSeries.getPubId()));

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);
		assertNotNull(actualDatasnapshot);
		assertEquals(origTsCount, actualDatasnapshot.getTimeSeries().size());
	}

	@Test
	public void storeGetRemoveToolsSingletons() throws AuthorizationException {
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		Set<ToolDto> toolDtos = dataSnapshotServer.getTools(authedUser);
		assertEquals(2, toolDtos.size());

		ToolDto tool0 = tstObjectFactory.newToolDto(authedUser.getUserId());
		List<String> revIds0 =
				dataSnapshotServer.storeTools(authedUser, singleton(tool0));
		Set<ToolDto> actualTools = dataSnapshotServer.getTools(authedUser);
		assertEquals(1, revIds0.size());

		ToolDto tool1 = tstObjectFactory.newToolDto(authedUser.getUserId());
		List<String> revIds1 =
				dataSnapshotServer.storeTools(authedUser, singleton(tool1));
		assertEquals(1, revIds1.size());

		actualTools = dataSnapshotServer.getTools(authedUser);

		assertEquals(4, actualTools.size());

		ToolDto toolDto0 =
				find(
						actualTools,
						compose(equalTo(
								Optional.of(getOnlyElement(revIds0))),
								IHasOptionalStringId.getId));
		CompareUtil.compare(tool0, toolDto0);

		ToolDto toolDto1 = find(
				actualTools,
				compose(equalTo(
						Optional.of(getOnlyElement(revIds1))),
						IHasOptionalStringId.getId));
		CompareUtil.compare(tool1, toolDto1);

		dataSnapshotServer.removeTools(
				authedUser,
				singleton(getOnlyElement(revIds0)));

		actualTools = dataSnapshotServer.getTools(authedUser);

		assertEquals(3, actualTools.size());

		any(actualTools,
				compose(equalTo(
						Optional.of(getOnlyElement(revIds1))),
						IHasOptionalStringId.getId));

		Set<ToolDto> tools = dataSnapshotServer.getTools(authedUser);
		ToolDto tool = get(tools, 0);

		ToolDto editedTool = new ToolDto(
				tool.getLabel(),
				tool.getAuthorId().get(),
				tool.getAuthorName(),
				tool.getSourceUrl(),
				tool.getBinUrl() + "something",
				tool.getId().get(),
				tool.getDescription().orNull());
		String editedToolRevId =
				getOnlyElement(dataSnapshotServer.storeTools(
						authedUser,
						singleton(editedTool)));
		Set<ToolDto> editedTools = dataSnapshotServer.getTools(authedUser);
		ToolDto actualEditedTool =
				find(editedTools,
						compose(equalTo(Optional.of(editedToolRevId)),
								IHasOptionalStringId.getId));
		CompareUtil.compare(editedTool, actualEditedTool);
	}

	@Test
	public void storeTsAnnotationsAllNew() throws Exception {
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final int origAnnCount = datasetTsAnns.size();
		final Set<TsAnnotationDto> annotations = newHashSet();
		final TimeSeriesDto annotated = tstObjectFactory.newTimeSeriesDto(
				timeSeries.getLabel(), timeSeries.getPubId());
		TsAnnotationDto new1 = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		new1.setDescription("new1");
		new1.setStartOffsetMicros(1L); // for visual inspection of reoffset
										// program
		new1.setEndOffsetMicros(2L);
		// new1.setLayer( // don't set the layer
		annotations.add(new1);
		TsAnnotationDto new2 = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		new2.setDescription("new2");
		new2.setStartOffsetMicros(3L);
		new2.setEndOffsetMicros(4L);
		new2.setLayer("layser2");
		annotations.add(new2);
		final DataSnapshotIds datasetRevId = dataSnapshotServer
				.storeTsAnnotations(
						authedUser, dataset.getPubId(), annotations);

		final DataSnapshot actualDataSnapshot = dataSnapshotServer
				.getDataSnapshot(
						authedUser, datasetRevId.getDsRevId());
		final List<TsAnnotationDto> actualAnnotations = actualDataSnapshot
				.getTsAnnotations();
		assertEquals(
				origAnnCount + annotations.size(),
				actualAnnotations.size());
		int new1Count = 0;
		int new2Count = 0;
		for (final TsAnnotationDto actualAnn : actualAnnotations) {
			final TsAnnotationEntity origAnnEntity = find(
					datasetTsAnns,
					compose(equalTo(
							actualAnn.getId()),
							IHasPubId.getPubId), null);
			if (origAnnEntity == null) {
				// One of the new annotations.
				if (areEqual(new1, actualAnn)) {
					// layer should have been set by the db
					assertEquals(
							TsAnnotationEntity.getLayerDefault(dataset),
							actualAnn.getLayer());
					new1Count++;
				}
				if (areEqual(new2, actualAnn)) {
					new2Count++;
				}
			}
		}
		assertEquals(1, new1Count);
		assertEquals(1, new2Count);
	}

	@Test
	public void storeTsAnnotationsReachMaxTsAnns()
			throws Exception {
		final int origAnnCount = datasetTsAnns.size();
		final DataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer(origAnnCount + 1);
		final Set<TsAnnotationDto> annotations = newHashSet();
		final TimeSeriesDto annotated = tstObjectFactory.newTimeSeriesDto(
				timeSeries.getLabel(), timeSeries.getPubId());
		TsAnnotationDto new1 = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		new1.setDescription("new1");
		new1.setStartOffsetMicros(1L); // for visual inspection of reoffset
										// program
		new1.setEndOffsetMicros(2L);
		annotations.add(new1);
		final DataSnapshotIds datasetRevId = dataSnapshotServer
				.storeTsAnnotations(
						authedUser, dataset.getPubId(), annotations);

		final DataSnapshot actualDataSnapshot = dataSnapshotServer
				.getDataSnapshot(
						authedUser, datasetRevId.getDsRevId());
		final List<TsAnnotationDto> actualAnnotations = actualDataSnapshot
				.getTsAnnotations();
		assertEquals(origAnnCount + annotations.size(),
				actualAnnotations.size());
		int new1Count = 0;
		for (final TsAnnotationDto actualAnn : actualAnnotations) {
			final TsAnnotationEntity origAnnEntity = find(
					datasetTsAnns,
					compose(equalTo(actualAnn.getId()),
							IHasPubId.getPubId), null);
			if (origAnnEntity == null) {
				// One of the new annotations.
				if (areEqual(new1, actualAnn)) {
					new1Count++;
				}
			}
		}
		assertEquals(1, new1Count);
	}

	@Test(expected = TooManyTsAnnsException.class)
	public void storeTsAnnotationsExceedMaxTsAnns()
			throws Exception {
		final int origAnnCount = datasetTsAnns.size();
		final DataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer(origAnnCount + 1);

		final Set<TsAnnotationDto> annotations = newHashSet();
		final TimeSeriesDto annotated = tstObjectFactory.newTimeSeriesDto(
				timeSeries.getLabel(), timeSeries.getPubId());
		TsAnnotationDto new1 = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		new1.setDescription("new1");
		new1.setStartOffsetMicros(1L); // for visual inspection of reoffset
										// program
		new1.setEndOffsetMicros(2L);
		annotations.add(new1);
		TsAnnotationDto new2 = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		new2.setDescription("new2");
		new2.setStartOffsetMicros(3L);
		new2.setEndOffsetMicros(4L);
		annotations.add(new2);
		dataSnapshotServer.storeTsAnnotations(
				authedUser, dataset.getPubId(), annotations);
	}

	@Test
	public void storeTsAnnotationsModifyAndNew() throws Exception {
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final int origAnnCount = datasetTsAnns.size();
		final Set<TsAnnotationDto> annotations = newHashSet();
		final Set<TsAnnotationDto> modifiedAnnotations = newHashSet();

		// A new annotation
		final TimeSeriesDto annotated = tstObjectFactory.newTimeSeriesDto(
				timeSeries.getLabel(), timeSeries.getPubId());
		final TsAnnotationDto newAnn = tstObjectFactory
				.newTsAnnDtoNoPubIdNoLayer(annotated);
		annotations.add(newAnn);

		// Modifying an existing annotation
		final TsAnnotationDto modAnn =
				TsAnnotationAssembler.assemble(datasetTsAnn0);
		modAnn.setType("Type of mod1");
		modAnn.setDescription("Description of mod1");
		modAnn.setLayer(null);
		removeIf(modAnn.getAnnotated(),
				compose(equalTo(timeSeries.getPubId()),
						IHasStringId.getId));
		modifiedAnnotations.add(modAnn);

		// Modifying an existing annotation
		final TsAnnotationDto modAnn2 =
				TsAnnotationAssembler.assemble(datasetTsAnn1);
		// to make sure that we're using the tsRevId->id cache in
		// service.storeTsAnns
		modAnn2.setAnnotated(modAnn.getAnnotated());
		modAnn2.setType("Type of mod2");
		modAnn2.setDescription("Description of mod2");
		modAnn2.setLayer("modlayer2");
		modifiedAnnotations.add(modAnn2);

		annotations.addAll(modifiedAnnotations);
		final DataSnapshotIds datasetRevId = dataSnapshotServer
				.storeTsAnnotations(
						authedUser, dataset.getPubId(), annotations);

		final DataSnapshot actualDataSnapshot = dataSnapshotServer
				.getDataSnapshot(
						authedUser, datasetRevId.getDsRevId());
		final List<TsAnnotationDto> actualAnnotations = actualDataSnapshot
				.getTsAnnotations();
		assertEquals(
				origAnnCount + annotations.size() - modifiedAnnotations.size(),
				actualAnnotations.size());

		// Verify new annotation
		for (final TsAnnotationDto actualAnn : actualAnnotations) {
			final TsAnnotationEntity origAnnEntity = find(
					datasetTsAnns,
					compose(equalTo(actualAnn.getId()),
							IHasPubId.getPubId), null);
			if (origAnnEntity == null) {
				// The new annotations.
				compare(newAnn, actualAnn);
			}
		}

		// Verify modification
		TsAnnotationDto actualModAnn = find(
				actualAnnotations,
				compose(equalTo(
						datasetTsAnn0.getPubId()),
						IHasStringId.getId),
				null);
		assertNotNull(actualModAnn);
		assertTrue(actualModAnn.getType().equals("Type of mod1"));
		assertTrue(actualModAnn.getDescription().equals(
				"Description of mod1"));
		assertEquals(TsAnnotationEntity.getLayerDefault(dataset),
				actualModAnn.getLayer());
		assertEquals(1, actualModAnn.getAnnotated().size());
		assertEquals(
				getOnlyElement(actualModAnn.getAnnotated()).getId(),
				timeSeries2.getPubId());

		TsAnnotationDto actualModAnn2 = find(
				actualAnnotations,
				compose(equalTo(
						datasetTsAnn1.getPubId()),
						IHasStringId.getId),
				null);
		assertNotNull(actualModAnn2);
		assertTrue(actualModAnn2.getType().equals("Type of mod2"));
		assertTrue(actualModAnn2.getDescription().equals(
				"Description of mod2"));
		assertEquals(1, actualModAnn2.getAnnotated().size());
		assertEquals(modAnn2.getLayer(), actualModAnn2.getLayer());
		assertEquals(
				getOnlyElement(actualModAnn2.getAnnotated()).getId(),
				timeSeries2.getPubId());
	}

	// Which annotation gets saved is now undefined if there are dups
	// @Test
	// public void storeTsAnnotationsModifyDuplicateRevIds()
	// throws AuthorizationException {
	// final DataSnapshotServer dataSnapshotServer = new DataSnapshotServer(
	// HibernateUtil.getSessionFactory(),
	// new DataSnapshotServiceFactory());
	// final int origAnnCount = datasetTsAnns.size();
	// // A list so that we can verify that the first mod for a given revId
	// // wins.
	// final Set<TsAnnotationDto> modifiedAnnotations = newHashSet();
	//
	// // Modifying an existing annotation
	// final TsAnnotationDto mod1 = TsAnnotationAssembler
	// .assemble(datasetTsAnn0);
	// mod1.setType("Type of mod1");
	// mod1.setDescription("Description of mod1");
	// modifiedAnnotations.add(mod1);
	//
	// // Modifying the same existing annotation in a different way
	// final TsAnnotationDto mod2 = TsAnnotationAssembler
	// .assemble(datasetTsAnn0);
	// mod2.setType("Type of mod2");
	// mod2.setDescription("Description of mod2");
	// modifiedAnnotations.add(mod2);
	//
	// final String datasetRevId = dataSnapshotServer.storeTsAnnotations(
	// authedUser, dataset.getRevId(), modifiedAnnotations);
	//
	// final DataSnapshot actualDataSnapshot = dataSnapshotServer
	// .getDataSnapshot(
	// authedUser, datasetRevId);
	// final Set<TsAnnotationDto> actualAnnotations = actualDataSnapshot
	// .getTsAnnotations();
	// assertEquals(
	// origAnnCount,
	// actualAnnotations.size());
	//
	// TsAnnotationDto modifiedAnn = find(
	// actualAnnotations,
	// compose(equalTo(datasetTsAnn0.getRevId()),
	// IHasRevId.getRevId),
	// null);
	// assertTrue(modifiedAnn.getType().equals("Type of mod1"));
	// assertTrue(modifiedAnn.getDescription().equals(
	// "Description of mod1"));
	// }

	@Test
	public void removeTsAnnotations() throws AuthorizationException {
		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final int origAnnCount = datasetTsAnns.size();
		final Set<TsAnnotationEntity> toBeRemoved =
				newHashSet(datasetTsAnn0, datasetTsAnn1);
		final Set<String> tsAnnotationRevIds = newHashSet(transform(
				toBeRemoved, IHasPubId.getPubId));
		final String actualDataSnapshotRevId = dataSnapshotServer
				.removeTsAnnotations(authedUser, dataset.getPubId(),
						tsAnnotationRevIds);

		final DataSnapshot actualDataSnapshot = dataSnapshotServer
				.getDataSnapshot(authedUser, actualDataSnapshotRevId);

		final List<TsAnnotationDto> actualAnnotations = actualDataSnapshot
				.getTsAnnotations();
		assertEquals(
				origAnnCount - toBeRemoved.size(),
				actualAnnotations.size());
		final Iterable<String> actualAnnRevIds = transform(actualAnnotations,
				IHasStringId.getId);
		assertTrue(!any(actualAnnRevIds,
				in(tsAnnotationRevIds)));
	}

	@Test
	public void removeTimeSeriesSingleton() throws Exception {

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String toolLabel = newUuid();
		String dsLabel = newUuid();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				null,
				null);

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);

		Set<TimeSeriesDto> origTs = actualDatasnapshot.getTimeSeries();
		int origTsSize = origTs.size();
		TimeSeriesDto oneTs = get(origTs, 0);

		String dsRevId2 = dataSnapshotServer.removeTimeSeriesFromDataset(
				authedUser,
				dsRevId,
				singleton(oneTs.getId()));

		DataSnapshot actualDataSnapshot2 = dataSnapshotServer.getDataSnapshot(
				authedUser, dsRevId2);
		Set<TimeSeriesDto> actualTs2 = actualDataSnapshot2.getTimeSeries();
		assertEquals(
				origTsSize - 1,
				actualTs2.size());

		for (TimeSeriesDto origT : origTs) {
			if (origT == oneTs) {
				assertNull(find(actualTs2,
						compose(equalTo(origT.getId()),
								IHasStringId.getId), null));
			} else {
				assertNotNull(find(actualTs2,
						compose(equalTo(origT.getId()),
								IHasStringId.getId), null));
			}
		}

		// make sure we left the annotations on the study (which we weren't
		// doing at one point
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());

		assertEquals(2, studyAsDs.getTsAnnotations().size());

	}

	/**
	 * No point to this test other than to make sure that seizure counts are
	 * being filled in with the debugger.
	 * 
	 * @throws AuthorizationException
	 */
	@Test
	public void basicSearch() throws AuthorizationException {
		final DataSnapshotServer dsServer = getDataSnapshotServer();

		Set<DataSnapshotSearchResult> results =
				dsServer.getDataSnapshots(authedUser, new EegStudySearch());
		assertTrue(results.size() > 0);
	}

	/**
	 * Remove two time series from a dataset.
	 * 
	 * @throws AuthorizationException
	 */
	@Test
	public void removeTimeSeriesTwo() throws Exception {

		final DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String toolLabel = newUuid();
		String dsLabel = newUuid();

		String dsRevId = dataSnapshotServer.deriveDataset(
				authedUser,
				study.getPubId(),
				dsLabel,
				toolLabel,
				null,
				null);

		DataSnapshot actualDatasnapshot =
				dataSnapshotServer.getDataSnapshot(
						authedUser,
						dsRevId);

		Set<TimeSeriesDto> origTs = actualDatasnapshot.getTimeSeries();
		int origTsSize = origTs.size();
		TimeSeriesDto oneTs = get(origTs, 0);
		TimeSeriesDto twoTs = null;

		for (TimeSeriesDto tsDto : origTs) {
			if (tsDto != oneTs) {
				twoTs = tsDto;
				break;
			}
		}
		assertNotSame(oneTs, twoTs); // this was a problem
		assertNotNull(twoTs);

		String dsRevId2 = dataSnapshotServer.removeTimeSeriesFromDataset(
				authedUser,
				dsRevId,
				newHashSet(oneTs.getId(), twoTs.getId()));

		DataSnapshot actualDataSnapshot2 = dataSnapshotServer.getDataSnapshot(
				authedUser, dsRevId2);
		Set<TimeSeriesDto> actualTs2 = actualDataSnapshot2.getTimeSeries();

		assertEquals(
				origTsSize - 2,
				actualTs2.size());

		for (TimeSeriesDto origT : origTs) {
			if (origT == oneTs || origT == twoTs) {
				assertNull(find(actualTs2,
						compose(equalTo(origT.getId()),
								IHasStringId.getId), null));
			} else {
				assertNotNull(find(actualTs2,
						compose(equalTo(origT.getId()),
								IHasStringId.getId), null));
			}
		}

		// make sure we left the annotations on the study (which we weren't
		// doing at one point
		DataSnapshot studyAsDs = dataSnapshotServer.getDataSnapshot(
				authedUser,
				study.getPubId());

		assertEquals(2, studyAsDs.getTsAnnotations().size());

	}

	@Test
	public void datasetGetDataSnapshotByStableIdSingle() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		DataSnapshotSearchResult actualDs = dsServer
				.getLatestSnapshots(
						authedUser, singletonList(dataset.getPubId()))
				.get(0);
		assertNotNull(actualDs);
		assertEquals(dataset.getLabel(), actualDs.getLabel());
		assertEquals(dataset.getImages().size(), actualDs.getImageCount());
		assertEquals(
				datasetTsAnns.size(),
				actualDs.getTsAnnCount());
		assertEquals(dataset.getTimeSeries().size(),
				actualDs.getTimeSeries().size());
	}

	@Test
	public void eegStudyGetLatestSnapshotsSingle() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		DataSnapshotSearchResult actualDs = dsServer
				.getLatestSnapshots(
						authedUser, singletonList(study.getPubId())).get(0);

		assertNotNull(actualDs);
		assertEquals(study.getLabel(), actualDs.getLabel());
		assertEquals(study.getImages().size(), actualDs.getImageCount());
		assertEquals(studyTsAnns.size(), actualDs.getTsAnnCount());
		assertEquals(study.getTimeSeries().size(), actualDs.getTimeSeries()
				.size());
	}

	public void nonExistentGetDataSnapshotByStableIdSingle() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();
		DataSnapshotSearchResult actualDs = dsServer
				.getLatestSnapshots(
						authedUser, singletonList("does-not-exist")).get(0);
		assertNull(actualDs);
	}

	@Test
	public void storeUserClob() throws Exception {

		DataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		String clob = "some-clob";
		dataSnapshotServer.storeUserClob(authedUser, clob);

		String actualClob =
				dataSnapshotServer.getUserClob(authedUser);

		assertEquals(clob, actualClob);

		String clob1 = "some-clob-1";

		dataSnapshotServer.storeUserClob(
				authedUser, "some-clob-1");

		actualClob = dataSnapshotServer.getUserClob(
				authedUser);

		assertEquals(clob1, actualClob);

	}

	@Test
	public void getExperimentMetadata() throws Exception {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		ExperimentMetadata experimentMd = dsServer.getExperimentMetadata(
				authedUser, experiment0.getPubId());
		assertNotNull(experimentMd);
		assertEquals(experiment0.getLabel(), experimentMd.getLabel());
		assertNull(experimentMd.getStimType());
		assertNull(experimentMd.getStimRegion());
		assertNull(experimentMd.getStimMs());
		assertNull(experimentMd.getStimIsiMs());

		AnimalEntity animalEntity = experiment0.getParent();
		AnimalMetadata animalMd = experimentMd.getAnimal();
		assertNotNull(animalMd);
		assertEquals(animalEntity.getLabel(), animalMd.getLabel());
		assertEquals(animalEntity.getStrain().getLabel(), animalMd.getStrain());
		assertEquals(
				animalEntity.getStrain().getParent().getLabel(),
				animalMd.getSpecies());

	}

	@Test
	public void getEegStudyMetadata() throws Exception {
		DataSnapshotServer dataSnapshotServer =
				getDataSnapshotServer();

		EegStudyMetadata studyMd =
				dataSnapshotServer.getEegStudyMetadata(authedUser,
						study.getPubId());
		assertEquals(study.getParent().getParent().getLabel(),
				studyMd.getPatientLabel());
		assertEquals(study.getParent().getParent().getAgeOfOnset(),
				Integer.valueOf(studyMd.getAgeAtOnset()));
		assertEquals(study.getParent().getParent().getGender(),
				studyMd.getGender());
		assertEquals(study.getParent().getParent().getEtiology(),
				studyMd.getEtiology());
		assertEquals(study.getParent().getParent().getDevelopmentalDisorders(),
				studyMd.getDevelopmentalDisorders());
		assertEquals(study.getParent().getParent().getTraumaticBrainInjury(),
				studyMd.getTraumaticBrainInjury());
		assertEquals(study.getParent().getParent().getFamilyHistory(),
				studyMd.getFamilyHistory());
		assertEquals(study.getParent().getParent().getSzTypes(),
				studyMd.getSzHistory());
		assertEquals(study.getParent().getParent().getPrecipitants(),
				studyMd.getPreciptants());
		assertEquals(study.getParent().getAgeAtAdmission(),
				Integer.valueOf(studyMd.getAgeAtAdmission()));
		assertEquals(study.getRecording().getRefElectrodeDescription(),
				studyMd.getRefElectrodeDescription());
		assertEquals(study.getRecording().getContactGroups().size(), studyMd
				.getContactGroups()
				.size());
		Set<ContactGroup> candidates = newHashSet(study.getRecording()
				.getContactGroups());
		for (ContactGroupMetadata electrodeMd : studyMd.getContactGroups()) {
			ContactGroup match = null;
			for (ContactGroup electrode : candidates) {
				if (electrode.getChannelPrefix().equals(
						electrodeMd.getChannelPrefix())
						&& electrode.getLocation().equals(
								electrodeMd.getLocation())
						&& electrode.getSide().equals(electrodeMd.getSide())
						&& electrode.getSamplingRate().equals(
								electrodeMd.getSamplingRate())
						&& electrode.getLffSetting().equals(
								electrodeMd.getLffSetting())
						&& electrode.getHffSetting().equals(
								electrodeMd.getHffSetting())) {
					match = electrode;
					break;
				}
			}
			assertNotNull("couldn't find a match for electrode md", match);
			candidates.remove(match);
		}
	}

	@Test
	public void frozenTests() throws Exception {
		IDataSnapshotServer dsServer = getDataSnapshotServer();

		boolean frozen = dsServer.isFrozen(authedUser, dataset.getPubId());
		assertFalse("all snapshots should be unfrozen up on creation", frozen);

		DataSnapshotSearchResult dsSr = getOnlyElement(dsServer
				.getLatestSnapshots(authedUser,
						singleton(dataset.getPubId())));
		assertFalse(dsSr.isFrozen());

		DataSnapshot ds = dsServer.getDataSnapshot(authedUser,
				dataset.getPubId());
		assertFalse(ds.isFrozen());

		dsServer.setFrozen(authedUser,
				dataset.getPubId(), true);
		frozen = dsServer.isFrozen(authedUser, dataset.getPubId());
		assertTrue(frozen);

		dsSr = getOnlyElement(dsServer
				.getLatestSnapshots(authedUser,
						singleton(dataset.getPubId())));
		assertTrue(dsSr.isFrozen());

		ds = dsServer.getDataSnapshot(authedUser,
				dataset.getPubId());
		assertTrue(ds.isFrozen());
	}

	@Test
	// @Ignore
	public void createJobForUser() throws Exception {

		final IDataSnapshotServer service =
				getDataSnapshotServer();
		Date createTime = new Date();
		service.createJobForUser(authedUser.getUserId(), dataset.getPubId(),
				tool.getPubId(), JobStatus.UNSCHEDULED, ParallelDto.SERIAL,
				createTime);
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IJobDAO jobDAO = new JobDAOHibernate(session);
			final JobEntity job = jobDAO.findByNaturalId(dataset.getPubId());
			assertNotNull(job);
			assertEquals(0, job.getTasks().size());
			assertEquals(dataset.getPubId(), job.getDatasetPubId());
			assertEquals(JobStatus.UNSCHEDULED, job.getStatus());
			assertEquals(tool.getPubId(), job.getToolPubId());
			assertEquals(authedUser.getUserId().getValue(), job.getUserId());
			assertEquals(ParallelDto.SERIAL, job.getParallel());
			long diff = createTime.getTime() - job.getCreateTime().getTime();
			assertTrue(Math.abs(diff) <= 1000);
			assertNull(job.getClob());

			trx.commit();
		} catch (final Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void initializeJob() throws Exception {
		final IDataSnapshotServer service =
				getDataSnapshotServer();
		Set<TaskDto> taskDtos = newHashSet(
				tstObjectFactory.newTaskDtoUnscheduledNoClob(),
				tstObjectFactory.newTaskDtoUnscheduledNoClob(),
				tstObjectFactory.newTaskDtoUnscheduledNoClob());
		service.initializeJob(toBeInitializedJob.getDatasetPubId(), taskDtos);
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IJobDAO jobDAO = new JobDAOHibernate(session);
			final JobEntity job = jobDAO.findByNaturalId(
					toBeInitializedJob
							.getDatasetPubId());
			assertEquals(taskDtos.size(), job.getTasks().size());

			for (final TaskDto taskDto : taskDtos) {
				final Set<AnalysisTask> matchingTasks = newHashSet(filter(
						job.getTasks(),
						compose(equalTo(taskDto.getName()), IHasName.getName)));
				assertTrue(matchingTasks.size() == 1);
				final AnalysisTask actual = getOnlyElement(matchingTasks);
				assertEquals(taskDto.getName(), actual.getName());
				assertEquals(taskDto.getStartTimeMicros(), actual
						.getStartTimeMicros().longValue());
				assertEquals(taskDto.getEndTimeMicros(), actual
						.getEndTimeMicros().longValue());
				assertEquals(taskDto.getTimeSeriesIds(), actual.getTsPubIds());
				assertEquals(TaskStatus.UNSCHEDULED, actual.getStatus());
				assertNull(actual.getRunStartTime());
				assertNull(actual.getWorker());

			}
			trx.commit();
		} catch (final Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
		boolean exception = false;
		try {
			service.initializeJob(toBeInitializedJob.getDatasetPubId(),
					taskDtos);
		} catch (IllegalArgumentException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void updateTaskStatus() throws Exception {
		final IDataSnapshotServer service =
				getDataSnapshotServer();
		service.updateTaskStatus(
				initializedJobUser,
				initializedJob.getDatasetPubId(),
				unscheduledTask.getName(),
				TaskStatus.RUNNING);
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IJobDAO jobDAO = new JobDAOHibernate(session);
			final JobEntity job = jobDAO.findByNaturalId(
					initializedJob
							.getDatasetPubId());
			final AnalysisTask actual = getOnlyElement(job.getTasks(), null);
			assertNotNull(actual);
			assertEquals(TaskStatus.RUNNING, actual.getStatus());
			trx.commit();
		} catch (final Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void updateTasks() throws Exception {
		final Date now = new Date();
		final IDataSnapshotServer service =
				getDataSnapshotServer();
		final TaskDto updatedTask = new TaskDto(
				unscheduledTask.getName(),
				TaskStatus.RUNNING,
				unscheduledTask.getStartTimeMicros().longValue(),
				unscheduledTask.getEndTimeMicros().longValue(),
				Long.valueOf(now.getTime()),
				"worker123");
		service.updateTasks(
				initializedJobUser,
				initializedJob.getDatasetPubId(),
				singleton(updatedTask));
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final IJobDAO jobDAO = new JobDAOHibernate(session);
			final JobEntity job = jobDAO.findByNaturalId(
					initializedJob
							.getDatasetPubId());
			final AnalysisTask actual = getOnlyElement(job.getTasks(), null);
			assertNotNull(actual);
			assertEquals(TaskStatus.RUNNING, actual.getStatus());
			long diff = now.getTime() - actual.getRunStartTime().getTime();
			assertTrue(Math.abs(diff) <= 1000);
			assertEquals("worker123", actual.getWorker());
			trx.commit();
		} catch (final Exception t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void getUserIdsWithActiveJobs() throws Exception {
		final Long[] ids = new Long[] { toBeInitializedJob.getUserId(),
				initializedJob.getUserId() };
		Arrays.sort(ids);
		final List<UserId> expected = newArrayList();
		for (Long id : ids) {
			expected.add(new UserId(id));
		}
		final IDataSnapshotServer service =
				getDataSnapshotServer();
		List<UserId> actual = service.getUserIdsWithActiveJobs();
		assertEquals(expected, actual);
	}

	@Test
	public void getTools() throws AuthorizationException {
		final IDataSnapshotServer service =
				getDataSnapshotServer();
		final List<ToolDto> actuals = service.getTools(
				dbUsers.regUser2,
				newArrayList(
						tool.getPubId(),
						tool2.getPubId()));
		assertEquals(2, actuals.size());
		ToolDto actual1 = actuals.get(0);
		assertEquals(tool.getAuthor().getId(), actual1.getAuthorId().get());
		assertEquals(tool.getBinUrl(), actual1.getBinUrl());
		assertEquals(tool.getDescription(), actual1.getDescription().orNull());
		assertEquals(tool.getLabel(), actual1.getLabel());
		assertEquals(tool.getPubId(), actual1.getId().get());
		assertEquals(tool.getSourceUrl(), actual1.getSourceUrl());

		ToolDto actual2 = actuals.get(1);
		assertEquals(tool2.getAuthor().getId(), actual2.getAuthorId().get());
		assertEquals(tool2.getBinUrl(), actual2.getBinUrl());
		assertEquals(tool2.getDescription(), actual2.getDescription().orNull());
		assertEquals(tool2.getLabel(), actual2.getLabel());
		assertEquals(tool2.getPubId(), actual2.getId().get());
		assertEquals(tool2.getSourceUrl(), actual2.getSourceUrl());

	}

	@Test
	public void createSession() throws Throwable {
		final IDataSnapshotServer service = getDataSnapshotServer();
		final SessionToken token = service.createSession(authedUser);
		assertNotNull(token);

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final SessionEntity actualSession = sessionDAO
					.findByNaturalId(token.getId());
			assertNotNull(actualSession);
			assertEquals(
					authedUser.getUserId(),
					actualSession.getUser().getId());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void cleanupOldSessions() throws Throwable {

		final IDataSnapshotServer service = getDataSnapshotServer();
		final Calendar thirtyDaysAgoCal = Calendar.getInstance();
		thirtyDaysAgoCal.add(Calendar.DAY_OF_MONTH, -30);
		final Date thirtyDaysAgo = thirtyDaysAgoCal.getTime();
		final int deleted = service
				.cleanupOldSessions(thirtyDaysAgo);
		assertEquals(4, deleted);

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final List<SessionEntity> allSessions = sessionDAO.findAll();
			assertEquals(1, allSessions.size());
			final SessionEntity actualActiveSession = getOnlyElement(allSessions);
			assertEquals(activeSessToken.getId(),
					actualActiveSession.getToken());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void getUserIdSessionStatus() throws Throwable {
		final Calendar oneDayAgoCal = Calendar.getInstance();
		oneDayAgoCal.add(Calendar.DAY_OF_MONTH, -1);
		final Date oneDayAgo = oneDayAgoCal.getTime();

		final IDataSnapshotServer service = getDataSnapshotServer();
		final UserIdSessionStatus userIdStatus = service
				.getUserIdSessionStatus(activeSessToken,
						oneDayAgo);
		final UserId userId = userIdStatus.getUserId();
		assertNotNull(userId);
		assertEquals(dbUsers.btRegUser1.getId(), userId);
		assertEquals(SessionStatus.ACTIVE, userIdStatus.getSessionStatus());

		// This call should change the status from active to expired
		UserIdSessionStatus expiredUserIdStatus = service
				.getUserIdSessionStatus(thirtyDaySessToken,
						oneDayAgo);
		assertEquals(SessionStatus.EXPIRED,
				expiredUserIdStatus.getSessionStatus());

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			// Check that the access time of the active session changed.
			final SessionEntity activeSession = sessionDAO
					.findByNaturalId(activeSessToken.getId());
			assertTrue(activeSession.getLastAccessTime().after(
					activeSessLastAccess));

			// Make sure we did not update the last access time of the session
			// we expired.
			final SessionEntity expiredSession = sessionDAO
					.findByNaturalId(thirtyDaySessToken.getId());
			assertNotNull(expiredSession);

			// Compare formatted strings instead of dates. DB may return
			// java.sql.Timestamp which never compares equal to a Date.
			DateFormat df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
					DateFormat.SHORT);
			assertEquals(df.format(thirtyDaySessLastAccess),
					df.format(expiredSession.getLastAccessTime()));
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

		final SessionToken nonExistent = tstObjectFactory.newSessionToken();
		boolean noSessionException = false;
		try {
			service.getUserIdSessionStatus(nonExistent,
					oneDayAgo);
		} catch (SessionNotFoundException e) {
			noSessionException = true;
		}
		assertTrue(noSessionException);
	}

	@Test
	public void getUserIdSessionStatusNonActive() throws Throwable {
		final Calendar oneDayAgoCal = Calendar.getInstance();
		oneDayAgoCal.add(Calendar.DAY_OF_MONTH, -1);
		final Date oneDayAgo = oneDayAgoCal.getTime();

		final DataSnapshotServer service = getDataSnapshotServer();

		// We'll access the non-active sessions and check in the database that
		// they were not updated.

		// Expired
		UserIdSessionStatus userIdStatus = service
				.getUserIdSessionStatus(expiredSessToken,
						oneDayAgo);
		UserId userId = userIdStatus.getUserId();
		assertNotNull(userId);
		assertEquals(dbUsers.btRegUser2.getId(), userId);
		assertEquals(SessionStatus.EXPIRED, userIdStatus.getSessionStatus());

		// Logged out
		userIdStatus = service
				.getUserIdSessionStatus(loggedOutSessToken,
						oneDayAgo);
		userId = userIdStatus.getUserId();
		assertNotNull(userId);
		assertEquals(dbUsers.btRegUser2.getId(), userId);
		assertEquals(SessionStatus.LOGGED_OUT, userIdStatus.getSessionStatus());

		// Account disabled
		userIdStatus = service
				.getUserIdSessionStatus(disabledAcctSessToken,
						oneDayAgo);
		userId = userIdStatus.getUserId();
		assertNotNull(userId);
		assertEquals(dbUsers.btRegUser2.getId(), userId);
		assertEquals(SessionStatus.ACCOUNT_DISABLED,
				userIdStatus.getSessionStatus());
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			// Make sure we did not update the last access time.

			final SessionEntity expiredSession = sessionDAO
					.findByNaturalId(expiredSessToken.getId());
			assertNotNull(expiredSession);
			// Compare formatted strings instead of dates. DB may return
			// java.sql.Timestamp which never compares equal to a Date.
			DateFormat df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
					DateFormat.SHORT);
			assertEquals(df.format(expiredSessLastAccess),
					df.format(expiredSession.getLastAccessTime()));

			final SessionEntity loggedOutSession = sessionDAO
					.findByNaturalId(loggedOutSessToken.getId());
			assertNotNull(loggedOutSession);
			assertEquals(df.format(loggedOutSessLastAccess),
					df.format(loggedOutSession.getLastAccessTime()));

			final SessionEntity disabledAcctSession = sessionDAO
					.findByNaturalId(disabledAcctSessToken.getId());
			assertNotNull(disabledAcctSession);
			assertEquals(df.format(disabledAcctSessLastAccess),
					df.format(disabledAcctSession.getLastAccessTime()));
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void getUserIdSessionStatusNoReactivation() throws Throwable {
		// Make sure that nonactive sessions are not reactivated if the
		// expiration time is increased.
		final IDataSnapshotServer service = getDataSnapshotServer();

		// We'll access the non-active sessions and check in the database that
		// they were not updated.

		// Expired
		{
			final Calendar minLastAccessCal = Calendar.getInstance();
			minLastAccessCal.setTime(expiredSessLastAccess);
			minLastAccessCal.add(Calendar.DAY_OF_MONTH, -10);
			final Date minLastAccess = minLastAccessCal.getTime();
			final UserIdSessionStatus userIdStatus = service
					.getUserIdSessionStatus(expiredSessToken,
							minLastAccess);
			final UserId userId = userIdStatus.getUserId();
			assertNotNull(userId);
			assertEquals(dbUsers.btRegUser2.getId(), userId);
			assertEquals(SessionStatus.EXPIRED, userIdStatus.getSessionStatus());
		}
		// Logged out
		{
			final Calendar minLastAccessCal = Calendar.getInstance();
			minLastAccessCal.setTime(loggedOutSessLastAccess);
			minLastAccessCal.add(Calendar.DAY_OF_MONTH, -10);
			final Date minLastAccess = minLastAccessCal.getTime();
			final UserIdSessionStatus userIdStatus = service
					.getUserIdSessionStatus(loggedOutSessToken,
							minLastAccess);
			final UserId userId = userIdStatus.getUserId();
			assertNotNull(userId);
			assertEquals(dbUsers.btRegUser2.getId(), userId);
			assertEquals(SessionStatus.LOGGED_OUT,
					userIdStatus.getSessionStatus());
		}
		// Account disabled
		{
			final Calendar minLastAccessCal = Calendar.getInstance();
			minLastAccessCal.setTime(disabledAcctSessLastAccess);
			minLastAccessCal.add(Calendar.DAY_OF_MONTH, -10);
			final Date minLastAccess = minLastAccessCal.getTime();
			final UserIdSessionStatus userIdStatus = service
					.getUserIdSessionStatus(disabledAcctSessToken,
							minLastAccess);
			final UserId userId = userIdStatus.getUserId();
			assertNotNull(userId);
			assertEquals(dbUsers.btRegUser2.getId(), userId);
			assertEquals(SessionStatus.ACCOUNT_DISABLED,
					userIdStatus.getSessionStatus());
		}
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			// Make sure we did not update the last access time.
			final SessionEntity expiredSession = sessionDAO
					.findByNaturalId(expiredSessToken.getId());
			assertNotNull(expiredSession);
			// Compare formatted strings instead of dates. DB may return
			// java.sql.Timestamp which never compares equal to a Date.
			DateFormat df = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
					DateFormat.SHORT);
			assertEquals(df.format(expiredSessLastAccess),
					df.format(expiredSession.getLastAccessTime()));

			final SessionEntity loggedOutSession = sessionDAO
					.findByNaturalId(loggedOutSessToken.getId());
			assertNotNull(loggedOutSession);
			assertEquals(df.format(loggedOutSessLastAccess),
					df.format(loggedOutSession.getLastAccessTime()));

			final SessionEntity disabledAcctSession = sessionDAO
					.findByNaturalId(disabledAcctSessToken.getId());
			assertNotNull(disabledAcctSession);
			assertEquals(df.format(disabledAcctSessLastAccess),
					df.format(disabledAcctSession.getLastAccessTime()));
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}

	@Test
	public void setSessionStatus() throws Throwable {

		final IDataSnapshotServer service = getDataSnapshotServer();
		service
				.setSessionStatus(activeSessToken, SessionStatus.EXPIRED);

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final SessionEntity actualSess = sessionDAO
					.findByNaturalId(activeSessToken.getId());
			assertEquals(SessionStatus.EXPIRED, actualSess.getStatus());
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	/**
	 * This test because I needed to verify that the new managed session work
	 * was not causing staleobjectexception's.
	 */
	@Test
	public void getUserIdSessionStatusManagedSessionsNoStale() throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		try {
			Session s = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(s);
			for (int i = 0; i < 3; i++) {
				Thread.sleep(1 * 1000);
				final Calendar oneDayAgoCal = Calendar.getInstance();
				oneDayAgoCal.add(Calendar.DAY_OF_MONTH, -1);
				final Date oneDayAgo = oneDayAgoCal.getTime();

				final IDataSnapshotServer service = getDataSnapshotServer();
				service
						.getUserIdSessionStatus(activeSessToken,
								oneDayAgo);
				assertFalse(s.getTransaction().isActive());
			}
		} finally {
			ManagedSessionContext.unbind(sf).close();
		}
	}

	@Test
	public void getTsAnnotations() throws Exception {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		List<TsAnnotationDto> tsAnns =
				dsServer.getTsAnnotations(
						authedUser,
						dataset.getPubId(),
						0L,
						TsAnnotationEntity.getLayerDefault(dataset),
						0,
						100);
		assertNotNull(tsAnns);
		assertEquals(2, tsAnns.size());
		assertTrue(tryFind(
				tsAnns,
				compose(equalTo(String.valueOf(datasetTsAnn0.getId())),
						TsAnnotationDto.getId)).isPresent());
		assertTrue(tryFind(
				tsAnns,
				compose(equalTo(String.valueOf(datasetTsAnn1.getId())),
						TsAnnotationDto.getId)).isPresent());
	}

	@Test
	public void getTsAnnotationsLtStartTime() throws Exception {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		List<TsAnnotationDto> tsAnns =
				dsServer.getTsAnnotationsLtStartTime(
						authedUser,
						dataset.getPubId(),
						datasetTsAnn1.getStartOffsetUsecs(),
						TsAnnotationEntity.getLayerDefault(dataset),
						0,
						100);
		assertNotNull(tsAnns);
		assertEquals(1, tsAnns.size());
		assertTrue(tryFind(
				tsAnns,
				compose(equalTo(String.valueOf(datasetTsAnn0.getId())),
						TsAnnotationDto.getId)).isPresent());
	}

	@Test
	public void getImageForTimeSeries() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<ImageForTrace> imagesForTrace = dsServer.getImagesForTimeSeries(
				authedUser, study.getPubId(), imagedContact00.getContact()
						.getTrace().getPubId());

		assertEquals(1, imagesForTrace.size());
		assertEquals(imagedContact00.getImage().getType().name(),
				getOnlyElement(imagesForTrace).getImageType());
		assertEquals(imagedContact00.getImage().getType(),
				getOnlyElement(imagesForTrace).getImageTypeEnum());
		assertEquals(imagedContact00.getImage().getFileKey(),
				getOnlyElement(imagesForTrace).getImageFileKey());
		assertEquals(imagedContact00.getPxCoordinates().getX().longValue(),
				getOnlyElement(imagesForTrace).getPixelCoordX());
		assertEquals(imagedContact00.getPxCoordinates().getY().longValue(),
				getOnlyElement(imagesForTrace).getPixelCoordY());
	}

	@Test
	public void getImageForTimeSeries2() {
		DataSnapshotServer dsServer = getDataSnapshotServer();

		Set<ImageForTrace> imagesForTrace = dsServer.getImagesForTimeSeries(
				authedUser,
				study.getPubId(),
				imagedContact11.getContact().getTrace().getPubId());

		assertEquals(2, imagesForTrace.size());

		final ImageForTrace imageForTrace11 = find(
				imagesForTrace,
				compose(equalTo(imagedContact11.getImage().getFileKey()),
						ImageForTrace.getImageFileKey),
				null);
		assertNotNull(imageForTrace11);
		assertEquals(imagedContact11.getImage().getType().name(),
				imageForTrace11.getImageType());
		assertEquals(imagedContact11.getImage().getType(),
				imageForTrace11.getImageTypeEnum());
		assertEquals(imagedContact11.getImage().getFileKey(),
				imageForTrace11.getImageFileKey());
		assertEquals(imagedContact11.getPxCoordinates().getX().longValue(),
				imageForTrace11.getPixelCoordX());
		assertEquals(imagedContact11.getPxCoordinates().getY().longValue(),
				imageForTrace11.getPixelCoordY());

		final ImageForTrace imageForTrace21 = find(
				imagesForTrace,
				compose(equalTo(imagedContact21.getImage().getFileKey()),
						ImageForTrace.getImageFileKey),
				null);
		assertNotNull(imageForTrace21);
		assertEquals(imagedContact21.getImage().getType().name(),
				imageForTrace21.getImageType());
		assertEquals(imagedContact21.getImage().getType(),
				imageForTrace21.getImageTypeEnum());
		assertEquals(imagedContact21.getImage().getFileKey(),
				imageForTrace21.getImageFileKey());
		assertEquals(imagedContact21.getPxCoordinates().getX().longValue(),
				imageForTrace21.getPixelCoordX());
		assertEquals(imagedContact21.getPxCoordinates().getY().longValue(),
				imageForTrace21.getPixelCoordY());
	}

	@Test
	public void createRecordingObject() throws
			RecordingNotFoundException,
			BadRecordingObjectNameException,
			DuplicateNameException,
			BadDigestException {
		final RecordingObject expectedRecordingObject = clientTstObjectFactory
				.newRecordingObject(experiment0.getPubId());
		expectedRecordingObject.setCreator(dbUsers.regUser1.getUsername());
		RecordingObjectProviderFactory
				.setRecordingObjectProvider(new AssertionRecordingObjectProvider(
						experiment0,
						expectedRecordingObject));
		DataSnapshotServer dsServer = getDataSnapshotServer();

		final InputStream inputStream = new ByteArrayInputStream(
				"recording object content".getBytes());
		RecordingObject actualRecordingObject = null;
		try {

			actualRecordingObject = dsServer
					.createRecordingObject(
							dbUsers.regUser1,
							expectedRecordingObject,
							inputStream,
							false);
		} finally {
			RecordingObjectProviderFactory.setRecordingObjectProvider(null);
		}
		assertNotNull(actualRecordingObject);
		assertEquals(
				expectedRecordingObject.getName(),
				actualRecordingObject.getName());
		assertEquals(
				expectedRecordingObject.getCreator(),
				actualRecordingObject.getCreator());
		assertEquals(
				expectedRecordingObject.getDatasetId(),
				actualRecordingObject.getDatasetId());
		assertEquals(
				expectedRecordingObject.getDescription(),
				actualRecordingObject.getDescription());
		assertEquals(
				expectedRecordingObject.getInternetMediaType(),
				actualRecordingObject.getInternetMediaType());
		assertEquals(
				expectedRecordingObject.getMd5Hash(),
				actualRecordingObject.getMd5Hash());
		assertEquals(
				expectedRecordingObject.getSizeBytes(),
				actualRecordingObject.getSizeBytes());
	}

	@Test
	public void createEditEegMontage() throws Exception {
		IDataSnapshotServer dataSnapshotServer = getDataSnapshotServer();

		final String studyId = study.getPubId();
		DataSnapshot actualStudy = dataSnapshotServer.getDataSnapshot(
				authedUser,
				studyId);

		EEGMontage montage = clientTstObjectFactory.newEEGMontage();
		for (final TimeSeriesDto ts : actualStudy.getTimeSeries()) {
			montage.getPairs().add(new EEGMontagePair(ts.getLabel(), null));
		}
		montage = dataSnapshotServer.createEditEegMontage(
				authedUser, montage,
				actualStudy.getId());

		List<EEGMontage> montages = dataSnapshotServer.getMontages(
				authedUser, studyId);
		assertEquals(1, montages.size());
		EEGMontage actualMontage = getOnlyElement(montages);
		assertEquals(montage.getServerId(), actualMontage.getServerId());
		assertEquals(montage.getPairs().size(), actualMontage.getPairs().size());

		String newReference = null;
		for (int i = 0; i < actualMontage.getPairs().size(); i++) {
			final EEGMontagePair pair = montage.getPairs().get(i);
			final EEGMontagePair actualPair = actualMontage.getPairs().get(i);
			assertEquals(pair.getEl1(), actualPair.getEl1());
			assertEquals(pair.getEl2(), actualPair.getEl2());
			if (i == 0) {
				newReference = pair.getEl1();
			}
		}

		final List<EEGMontagePair> newPairs = new ArrayList<>();
		final ListIterator<EEGMontagePair> reverseIter = montage.getPairs()
				.listIterator(montage.getPairs().size());
		while (reverseIter.hasPrevious()) {
			final EEGMontagePair oldPair = reverseIter.previous();
			newPairs.add(new EEGMontagePair(oldPair.getEl1(), newReference));
		}
		montage.getPairs().clear();
		montage.getPairs().addAll(newPairs);

		montage = dataSnapshotServer.createEditEegMontage(
				authedUser,
				montage,
				studyId);
		montages = dataSnapshotServer.getMontages(
				authedUser, studyId);
		assertEquals(1, montages.size());
		actualMontage = getOnlyElement(montages);
		assertEquals(montage.getServerId(), actualMontage.getServerId());
		assertEquals(montage.getPairs().size(), actualMontage.getPairs().size());

		for (int i = 0; i < actualMontage.getPairs().size(); i++) {
			final EEGMontagePair pair = montage.getPairs().get(i);
			final EEGMontagePair actualPair = actualMontage.getPairs().get(i);
			assertEquals(pair.getEl1(), actualPair.getEl1());
			assertEquals(pair.getEl2(), actualPair.getEl2());

		}

	}

	@Test
	public void createControlFileRegistration() throws AuthorizationException,
			DuplicateNameException {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		final String expectedBucket = newUuid();
		final String expectedFileKey = newUuid();

		final ControlFileRegistration registration = new ControlFileRegistration(
				expectedBucket,
				expectedFileKey);

		final ControlFileRegistration actualRegistration = dsServer
				.createControlFileRegistration(
						authedUser,
						registration);
		assertEquals(expectedBucket, actualRegistration.getBucket());
		assertEquals(expectedFileKey, actualRegistration.getFileKey());
		assertEquals(authedUser.getUsername(), actualRegistration.getCreator());
		assertNotNull(actualRegistration.getCreateTime());
		assertNotNull(actualRegistration.getETag());
		assertNotNull(actualRegistration.getId());
		assertEquals("NEW", actualRegistration.getStatus());

		boolean gotException = false;
		try {
			dsServer
					.createControlFileRegistration(
							authedUser,
							registration);
		} catch (DuplicateNameException e) {
			gotException = true;
		}
		assertTrue(gotException);
	}

	/**
	 * This is maybe lame - this code should be moved into annotation removal
	 * tests.
	 */
	@After
	public void findBereavedAnnotations() {
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();

			Long count = (Long) session
					.createQuery(
							"select count(*) from TsAnnotation tsa where tsa.annotated.size = 0")
					.uniqueResult();
			assertEquals(Long.valueOf(0), count);

			trx.commit();
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
		} finally {
			PersistenceUtil.close(session);
		}
	}
}
