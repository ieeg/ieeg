/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.any;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.AnalyzedDsDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

public class EegStudyDAOHibernateTest {
	private SessionFactory sessFac = HibernateUtil.getSessionFactory();
	private static EegStudy study000;

	private static EegStudy study100;

	private static TstUsers users;

	private static Logger logger = LoggerFactory
			.getLogger(EegStudyDAOHibernateTest.class);

	private static TimeSeriesEntity trace00000;

	@BeforeClass
	static public void classSetUp() throws Throwable {

		Transaction trx = null;
		Session s = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();

			users = new TstUsers(s);

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			// patient 0
			{
				final Patient patient0 = BuildDb.buildShallowPatient(UUID
						.randomUUID().toString());
				patient0.getSzTypes().clear();
				patient0.getSzTypes().addAll(EnumSet.of(SeizureType.ATONIC,
						SeizureType.MYOCLONIC));
				patient0.getPrecipitants().addAll(
						EnumSet.of(Precipitant.ALCOHOL,
								Precipitant.CATAMENIAL,
								Precipitant.STRESS));
				patient0.setAgeOfOnset(54);
				// patient0.setDevelopmentalDelay(null);
				patient0.setDevelopmentalDisorders(true);
				patient0.setEtiology("anoxia");
				patient0.setFamilyHistory(false);

				s.save(patient0);

				final ImageEntity p0Image0 = new ImageEntity();
				p0Image0.setPubId(randomUUID()
						.toString());
				p0Image0.setType(ImageType.CT);
				p0Image0.setFileKey("0/ct/02");

				final ImageEntity image01 = new ImageEntity();
				image01.setPubId(randomUUID()
						.toString());
				image01.setType(ImageType.DIGITAL_PICTURES);
				image01.setFileKey("0/digital/01");

				final HospitalAdmission admission00 = new HospitalAdmission();
				patient0.addAdmission(admission00);
				admission00.setAgeAtAdmission(34);

				study000 = BuildDb.buildStudyNoElectrodesNoImages(
						"Xyz_IEED_Study0",
						readPerm);
				admission00.addStudy(study000);

				final ImageEntity ctImage0000 = new ImageEntity();
				ctImage0000.setPubId(randomUUID()
						.toString());
				study000.getImages().add(ctImage0000);
				ctImage0000.setFileKey("0/ct/01");
				ctImage0000.setType(ImageType.CT);

				final ImageEntity megImage0000 = new ImageEntity();
				megImage0000.setPubId((randomUUID()
						.toString()));
				study000.getImages().add(megImage0000);
				megImage0000.setFileKey("0/meg/01");
				megImage0000.setType(ImageType.MEG);

				final HospitalAdmission admission01 = new HospitalAdmission();
				patient0.addAdmission(admission01);
				admission01.setAgeAtAdmission(21);
				patient0.setTraumaticBrainInjury(false);

				final ContactGroup electrode0000 = new ContactGroup(
						"LAG",
						"Depth",
						53898.389,
						0.53,
						55.0,
						false,
						Side.LEFT,
						Location.PARIETAL,
						5.0,
						"Ad-Tech",
						newUuid());
				study000.getRecording().addContactGroup(electrode0000);

				trace00000 = new TimeSeriesEntity();
				trace00000.setPubId((randomUUID()
						.toString()));

				trace00000.setFileKey("x/y/z");
				trace00000.setLabel("z");
				final Contact contact00000 = new Contact(
						ContactType.MACRO,
						new MriCoordinates(444.9, -87.7,
								5626.456),
						new TalairachCoordinates(
								4.5,
								-98.5, 8384.093),
						trace00000);
				electrode0000.addContact(contact00000);

				patient0.getPrecipitants().addAll(
						EnumSet.of(
								Precipitant.ALCOHOL,
								Precipitant.CATAMENIAL,
								Precipitant.STRESS));

				Dataset dataset000 = new Dataset(
						"dataset000",
						users.btRegUser1,
						SecurityUtil.createUserOwnedWorldReadableAcl(
								users.btRegUser1,
								permDAO));

				s.save(dataset000);
				s.save(admission00);
				AnalyzedDataSnapshot analyzedEegStudy000000 =
						new AnalyzedDataSnapshot("dcn", dataset000, study000);
				s.save(analyzedEegStudy000000);

				Dataset dataset001 = new Dataset("dataset001",
						users.btRegUser1,
						SecurityUtil.createUserOwnedWorldReadableAcl(
								users.btRegUser1,
								permDAO));

				new AnalyzedDataSnapshot(
						"line length",
						dataset001,
						study000);
				s.save(dataset001);
			}

			// patient 1
			{
				final Patient patient1 = BuildDb.buildShallowPatient(UUID
						.randomUUID().toString());
				patient1.setGender(Gender.MALE);
				patient1.getSzTypes().clear();
				patient1.getSzTypes().addAll(EnumSet.of(SeizureType.COMPLEX,
						SeizureType.PARTIAL_WITH_2NDARY_GENERALIZATION));
				s.save(patient1);

				final HospitalAdmission admission10 = new HospitalAdmission();
				patient1.addAdmission(admission10);
				admission10.setAgeAtAdmission(30);
				study100 = BuildDb.buildStudyNoElectrodesNoImages(
						"Xyz_IEED_Study1",
						readPerm);
				admission10.addStudy(study100);

				final ImageEntity ctImage1000 = new ImageEntity();
				ctImage1000.setPubId((randomUUID()
						.toString()));
				study100.getImages().add(ctImage1000);
				ctImage1000.setFileKey("1/ct/01");
				ctImage1000.setType(ImageType.CT);

				final ImageEntity mriImage1000 = new ImageEntity();
				mriImage1000.setPubId((randomUUID()
						.toString()));
				study100.getImages().add(mriImage1000);
				mriImage1000.setFileKey("1/mri/01");
				mriImage1000.setType(ImageType.MRI);

				final ImageEntity image10 = new ImageEntity();
				image10.setPubId((randomUUID()
						.toString()));
				image10.setType(ImageType.CT);
				image10.setFileKey("1/ct/02");

				final HospitalAdmission admission11 = new HospitalAdmission();
				patient1.addAdmission(admission11);
				admission11.setAgeAtAdmission(21);
				patient1.setAgeOfOnset(4);
				patient1.setDevelopmentalDisorders(true);
				patient1.setEtiology("anoxia");
				patient1.setFamilyHistory(false);
				patient1.setTraumaticBrainInjury(false);

				final ContactGroup electrode1000 = new ContactGroup(
						"AG",
						"Depth",
						600.3,
						5.3,
						55.0,
						false,
						Side.RIGHT,
						Location.OCCIPITAL,
						5.0,
						"Ad-Tech",
						newUuid());
				study100.getRecording().addContactGroup(electrode1000);

				TimeSeriesEntity trace10000 = new TimeSeriesEntity();
				trace10000.setPubId((randomUUID().toString()));
				trace10000.setFileKey("v/e/h");
				trace10000.setLabel("h");

				final Contact contact10000 = new Contact(
						ContactType.MICRO,
						new MriCoordinates(34.9,
								-987.7, 35.5),
						new TalairachCoordinates(
								4.5,
								-98.4, 8384.093),
						trace10000);

				electrode1000.addContact(contact10000);

			}

			trx.commit();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	@Test
	public void findAnalyzedStudyByStudyRevId() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = sessFac.openSession();
			trx = s.beginTransaction();
			AnalyzedDsDAOHibernate analyzedDsDAO =
					new AnalyzedDsDAOHibernate(s);
			Set<AnalyzedDataSnapshot> analyzedEegStudies =
					analyzedDsDAO.findByDsPubId(study000.getPubId());
			assertEquals(2, analyzedEegStudies.size());

			assertTrue(any(analyzedEegStudies,
					compose(equalTo("dcn"), AnalyzedDataSnapshot.getToolLabel)));
			assertTrue(any(
					analyzedEegStudies,
					compose(equalTo("line length"),
							AnalyzedDataSnapshot.getToolLabel)));

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	@Test
	public void findByTimeSeries() {
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IEegStudyDAO studyDAO = new EegStudyDAOHibernate(sessAndTrx.session);
			EegStudy actualStudy = studyDAO.findByTimeSeries(trace00000);
			assertNotNull(actualStudy);
			assertEquals(
					study000.getId(),
					actualStudy.getId());
			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}
	}
}
