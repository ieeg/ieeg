/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Ethnicity;

public class EthnicityTest {

	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (Ethnicity eth : Ethnicity.values()) {
			switch (eth) {
				case HISPANIC:
					assertEquals("db enums must not change order!", 0,
							eth.ordinal());

					break;
				case AMERICAN_INDIAN:
					assertEquals("db enums must not change order!", 1,
							eth.ordinal());
					break;
				case ASIAN:
					assertEquals("db enums must not change order!", 2,
							eth.ordinal());
					break;
				case PACIFIC_ISLANDER:
					assertEquals("db enums must not change order!", 3,
							eth.ordinal());
					break;
				case BLACK:
					assertEquals("db enums must not change order!", 4,
							eth.ordinal());
					break;
				case WHITE:
					assertEquals("db enums must not change order!", 5,
							eth.ordinal());
					break;
				case OTHER:
					assertEquals("db enums must not change order!", 6,
							eth.ordinal());
					break;
				case UNKNOWN:
					assertEquals("db enums must not change order!", 7,
							eth.ordinal());
					break;
				default:
					assertFalse("unknown Ethnicity: " + eth, true);
					break;
			}
		}
	}
}
