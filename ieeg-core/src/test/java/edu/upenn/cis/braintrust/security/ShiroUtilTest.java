/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.security;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.security.ShiroUtil.ShiroAction;
import edu.upenn.cis.braintrust.thirdparty.testhelper.AbstractShiroTest;

/**
 * Test the Shiro Utility methods.
 * 
 * @author John Frommeyer
 * 
 */
public class ShiroUtilTest extends AbstractShiroTest {

	private static String permissionString;
	private static String createPermissionString;

	@BeforeClass
	public static void setPermissionString() {
		permissionString = clazz.getSimpleName() + ":" + action.toString()
				+ ":" + id.toString();
		createPermissionString = clazz.getSimpleName() + ":"
				+ ShiroAction.CREATE.toString();
	}

	private static final ShiroAction action = ShiroAction.UPDATE;
	private static final Class<?> clazz = Patient.class;
	private static final Long id = Long.valueOf(1273729290L);

	@Test
	public void isAutcAndHasPermissionNoAuthc() {
		setSubject(createNonAuthcSubject());
		assertFalse(ShiroUtil.isAuthcAndHasPermission(
				SecurityUtils.getSubject(), clazz, action, id));
	}

	@Test
	public void isAutcAndHasPermissionNoPerm() {
		setSubject(createAuthcSubjectWithoutPermission());
		assertFalse(ShiroUtil.isAuthcAndHasPermission(
				SecurityUtils.getSubject(), clazz, action, id));
	}

	@Test
	public void isAutcAndHasPermission() {
		setSubject(createAuthcSubjectWithPermission());
		assertTrue(ShiroUtil.isAuthcAndHasPermission(
				SecurityUtils.getSubject(), clazz, action, id));
	}

	@Test
	public void isAutcAndHasCreatePermissionNoAuthc() {
		setSubject(createNonAuthcSubject());
		assertFalse(ShiroUtil.isAuthcAndHasCreatePermission(
				SecurityUtils.getSubject(), clazz));
	}

	@Test
	public void isAutcAndHasCreatePermissionNoPerm() {
		setSubject(createAuthcSubjectWithoutCreatePermission());
		assertFalse(ShiroUtil.isAuthcAndHasCreatePermission(
				SecurityUtils.getSubject(), clazz));
	}

	@Test
	public void isAutcAndHasCreatePermission() {
		setSubject(createAuthcSubjectWithCreatePermission());
		assertTrue(ShiroUtil.isAuthcAndHasCreatePermission(
				SecurityUtils.getSubject(), clazz));
	}

	@After
	public void tearDownSubject() {
		clearSubject();
	}

	private Subject createNonAuthcSubject() {
		Subject subject = mock(Subject.class);
		when(subject.isAuthenticated()).thenReturn(Boolean.FALSE);
		return subject;
	}

	private Subject createAuthcSubjectWithPermission() {
		Subject subject = mock(Subject.class);
		when(subject.isAuthenticated()).thenReturn(Boolean.TRUE);
		when(subject.isPermitted(permissionString)).thenReturn(Boolean.TRUE);
		return subject;
	}

	private Subject createAuthcSubjectWithoutPermission() {
		Subject subject = mock(Subject.class);
		when(subject.isAuthenticated()).thenReturn(Boolean.TRUE);
		when(subject.isPermitted(permissionString)).thenReturn(Boolean.FALSE);
		return subject;
	}

	private Subject createAuthcSubjectWithCreatePermission() {
		Subject subject = mock(Subject.class);
		when(subject.isAuthenticated()).thenReturn(Boolean.TRUE);
		when(subject.isPermitted(createPermissionString)).thenReturn(
				Boolean.TRUE);
		return subject;
	}

	private Subject createAuthcSubjectWithoutCreatePermission() {
		Subject subject = mock(Subject.class);
		when(subject.isAuthenticated()).thenReturn(Boolean.TRUE);
		when(subject.isPermitted(createPermissionString)).thenReturn(
				Boolean.FALSE);
		return subject;
	}

}
