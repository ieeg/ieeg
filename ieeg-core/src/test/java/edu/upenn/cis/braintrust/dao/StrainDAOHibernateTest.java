/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.metadata.hibernate.StrainDAOHibernate;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class StrainDAOHibernateTest {
	private final TstObjectFactory objFac = new TstObjectFactory(false);
	private SpeciesEntity species;

	@Before
	public void setUp() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			species = objFac.newSpecies();
			objFac.newStrain(species);
			objFac.newStrain(species);
			objFac.newStrain(species);
			sess.saveOrUpdate(species);

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void findByParentOrderedByLabel() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			final StrainDAOHibernate strainDAO = new StrainDAOHibernate(sess);
			List<StrainEntity> actualStrains = strainDAO
					.findByParentOrderedByLabel(species);
			assertEquals(3, actualStrains.size());
			StrainEntity prev = null;
			for (final StrainEntity curr : actualStrains) {
				if (prev != null) {
					assertTrue(prev.getLabel().compareTo(curr.getLabel()) < 0);
				}
				prev = curr;
			}
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
}
