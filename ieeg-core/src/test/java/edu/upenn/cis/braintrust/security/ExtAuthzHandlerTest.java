/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.security;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import org.junit.Test;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class ExtAuthzHandlerTest {

	private TstUsers users = new TstUsers();

	private ExtAuthzHandler authzHandler = AuthCheckFactory.getHandler();

	private TstObjectFactory tstObjFac = new TstObjectFactory(true);

	@Test
	public void adminCanDoAnything() throws AuthorizationException {

		authzHandler
				.checkPermitted(
						new ExtPermissions(
								users.adminUser,
								newUuid(),
								newUuid(),
								null,
								null),
						CorePermDefs.READ,
						null);
	}

	@Test(expected = UnauthorizedException.class)
	public void regUserCantUpdateWithNoUserAce()
			throws AuthorizationException {
		final String targetId = newUuid();
		final ExtWorldAce worldAce = tstObjFac.newExtWorldAce(targetId);
		final ExtPermission worldReadOnly = CorePermDefs.READ_PERM;
		worldAce.getPerms().add(worldReadOnly);

		final ExtPermissions permissions = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				worldAce);

		authzHandler.checkPermitted(permissions, CorePermDefs.EDIT, targetId);
	}

	@Test
	public void checkManageAclPermittedOwner() {
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce userAceOwner = new ExtUserAce(
				users.regUser1.getUserId(),
				users.regUser1.getUsername(),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		final ExtPermission ownerPerm = CorePermDefs.OWNER_PERM;
		userAceOwner.getPerms().add(ownerPerm);

		final ExtWorldAce readOnlyWorldAce = new ExtWorldAce(targetId, aclId,
				aclVersion);
		readOnlyWorldAce.getPerms().add(CorePermDefs.READ_PERM);

		final ExtPermissions userPerms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				userAceOwner,
				readOnlyWorldAce);

		authzHandler.checkPermitted(userPerms, CorePermDefs.EDIT_ACL, targetId);
	}

	@Test(expected = UnauthorizedException.class)
	public void checkManageAclPermittedEditor() {
		final String targetId = newUuid();
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());

		final ExtUserAce userAceEdit = new ExtUserAce(
				users.regUser1.getUserId(),
				users.regUser1.getUsername(),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		userAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtWorldAce readOnlyWorldAce = new ExtWorldAce(targetId, aclId,
				aclVersion);
		readOnlyWorldAce.getPerms().add(CorePermDefs.READ_PERM);

		final ExtPermissions userPerms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				userAceEdit,
				readOnlyWorldAce);

		authzHandler.checkPermitted(userPerms, CorePermDefs.EDIT_ACL, targetId);
	}

	@Test(expected = UnauthorizedException.class)
	public void moreSpecificWinsWorldVsUser() {
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());
		final String targetId = newUuid();

		final ExtUserAce userAceReadOnly = new ExtUserAce(
				users.regUser1.getUserId(),
				users.regUser1.getUsername(),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		userAceReadOnly.getPerms().add(CorePermDefs.READ_PERM);

		final ExtWorldAce worldAceEdit = new ExtWorldAce(targetId, aclId,
				aclVersion);
		worldAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtPermissions worldEditableUserReadOnly = new ExtPermissions(
				users.regUser1, newUuid(), targetId, userAceReadOnly,
				worldAceEdit);

		authzHandler.checkPermitted(worldEditableUserReadOnly,
				CorePermDefs.EDIT, targetId);
	}

	@Test(expected = UnauthorizedException.class)
	public void moreSpecificWins() {
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());
		final String targetId = newUuid();

		final ExtUserAce userAceReadOnly = new ExtUserAce(
				users.regUser1.getUserId(),
				users.regUser1.getUsername(),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		userAceReadOnly.getPerms().add(CorePermDefs.READ_PERM);

		final ExtWorldAce worldAceEdit = new ExtWorldAce(targetId, aclId,
				aclVersion);
		worldAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtPermissions worldEditableProjectOwnedUserReadOnly = new ExtPermissions(
				users.regUser1, newUuid(), targetId, userAceReadOnly,
				worldAceEdit);
		final ExtProjectAce projectAceOwner = new ExtProjectAce(
				newUuid(),
				newUuid(),
				TstObjectFactory.randomEnum(ProjectGroupType.class),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		projectAceOwner.getPerms().add(CorePermDefs.OWNER_PERM);
		worldEditableProjectOwnedUserReadOnly.getProjectAces().add(
				projectAceOwner);

		authzHandler.checkPermitted(worldEditableProjectOwnedUserReadOnly,
				CorePermDefs.EDIT, targetId);
	}
	
	@Test(expected = UnauthorizedException.class)
	public void moreSpecificWinsWorldvsProject() {
		final Long aclId = Long.valueOf(TstObjectFactory.randomNonnegInt());
		final Integer aclVersion = Integer.valueOf(TstObjectFactory
				.randomNonnegInt());
		final String targetId = newUuid();

		final ExtWorldAce worldAceEdit = new ExtWorldAce(targetId, aclId,
				aclVersion);
		worldAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtPermissions worldEditableProjectReadOnly = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				worldAceEdit);
		final ExtProjectAce projectAceReadOnly = new ExtProjectAce(
				newUuid(),
				newUuid(),
				TstObjectFactory.randomEnum(ProjectGroupType.class),
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		projectAceReadOnly.getPerms().add(CorePermDefs.READ_PERM);
		worldEditableProjectReadOnly.getProjectAces().add(
				projectAceReadOnly);

		authzHandler.checkPermitted(worldEditableProjectReadOnly,
				CorePermDefs.EDIT, targetId);
	}

	public void regUserCanRUWWordPerms() {
		final String targetId = newUuid();
		final ExtWorldAce worldAceEdit = tstObjFac.newExtWorldAce(targetId);
		worldAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				worldAceEdit);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);

	}

	@Test(expected = UnauthorizedException.class)
	public void regUserCantDelete() throws AuthorizationException {
		final String targetId = newUuid();
		final ExtWorldAce worldAceRead = tstObjFac.newExtWorldAce(targetId);
		worldAceRead.getPerms().add(CorePermDefs.READ_PERM);

		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				targetId,
				targetId,
				null,
				worldAceRead);
		authzHandler.checkPermitted(perms, CorePermDefs.DELETE, targetId);
	}

	@Test(expected = UnauthorizedException.class)
	public void regUserCantRead() {
		String targetId = newUuid();
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				targetId,
				targetId,
				null,
				null);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
	}

	@Test(expected = UnauthorizedException.class)
	public void regUserCantUpdate() {
		final String targetId = newUuid();
		final ExtWorldAce worldAceRead = tstObjFac.newExtWorldAce(targetId);
		worldAceRead.getPerms().add(CorePermDefs.READ_PERM);

		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				targetId,
				targetId,
				null,
				worldAceRead);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);
	}

	@Test
	public void regUserCanReadWUserAce() {
		final String targetId = newUuid();
		final ExtUserAce userAceRead = tstObjFac.newExtUserAce(users.regUser1,
				targetId);
		userAceRead.getPerms().add(CorePermDefs.READ_PERM);
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				userAceRead,
				null);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
	}

	@Test
	public void regUserCanRUWUserAce() {
		final String targetId = newUuid();
		final ExtUserAce userAceEdit = tstObjFac.newExtUserAce(users.regUser1,
				targetId);
		userAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				userAceEdit,
				null);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);
	}

	@Test
	public void regUserCanCRUDEditAclWUserAce() {
		final String targetId = newUuid();
		final ExtUserAce userAceOwn = tstObjFac.newExtUserAce(users.regUser1,
				targetId);
		userAceOwn.getPerms().add(CorePermDefs.OWNER_PERM);
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				userAceOwn,
				null);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.DELETE, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT_ACL, targetId);
	}

	@Test
	public void regUserCanReadWProjectAce() {
		final String targetId = newUuid();
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				null);
		final ExtProjectAce projAceRead = tstObjFac.newExtProjectAce(
				targetId);
		projAceRead.getPerms().add(CorePermDefs.READ_PERM);
		perms.getProjectAces().add(projAceRead);
		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
	}

	@Test
	public void regUserCanRUWProjectAce() {
		final String targetId = newUuid();

		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				null);
		final ExtProjectAce projAceEdit = tstObjFac.newExtProjectAce(
				targetId);
		projAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);
		perms.getProjectAces().add(projAceEdit);

		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);
	}

	@Test
	public void regUserCanCRUDEditAclWProjectAce() {
		final String targetId = newUuid();

		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				null);
		final ExtProjectAce projAceOwn = tstObjFac.newExtProjectAce(targetId);
		projAceOwn.getPerms().add(CorePermDefs.OWNER_PERM);
		perms.getProjectAces().add(projAceOwn);

		authzHandler.checkPermitted(perms, CorePermDefs.READ, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.DELETE, targetId);
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT_ACL, targetId);
	}

	@Test
	public void mostPermissiveProjectGroupWinsDiffProj() {
		final String targetId = newUuid();
		
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				null);
		
		final ExtProjectAce projAceRead = tstObjFac.newExtProjectAce(targetId);
		projAceRead.getPerms().add(CorePermDefs.READ_PERM);
		perms.getProjectAces().add(projAceRead);
		
		final ExtProjectAce projAceOwn = tstObjFac.newExtProjectAce(targetId);
		projAceOwn.getPerms().add(CorePermDefs.OWNER_PERM);
		perms.getProjectAces().add(projAceOwn);
		
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT_ACL, targetId);
	}
	
	@Test
	public void mostPermissiveProjectGroupWinsSameProj() {
		final String targetId = newUuid();
		
		final String projectId = newUuid();
		final String projectName = newUuid();
		final Integer aclVersion = Integer.valueOf(TstObjectFactory.randomNonnegInt());
		
		final ExtPermissions perms = new ExtPermissions(
				users.regUser1,
				newUuid(),
				targetId,
				null,
				null);
		
		final ExtProjectAce projAdminOwner = new ExtProjectAce(
				projectId,
				projectName,
				ProjectGroupType.ADMINS,
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		
		projAdminOwner.getPerms().add(CorePermDefs.OWNER_PERM);
		perms.getProjectAces().add(projAdminOwner);
		
		final ExtProjectAce projTeamRead = new ExtProjectAce(
				projectId,
				projectName,
				ProjectGroupType.TEAM,
				targetId,
				aclVersion,
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
		projTeamRead.getPerms().add(CorePermDefs.READ_PERM);
		perms.getProjectAces().add(projTeamRead);
		
		authzHandler.checkPermitted(perms, CorePermDefs.EDIT_ACL, targetId);
	}
}
