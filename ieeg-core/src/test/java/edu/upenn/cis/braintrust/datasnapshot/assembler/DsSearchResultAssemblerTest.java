/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Set;

import org.junit.Test;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.TimeSeriesSearchResult;

public class DsSearchResultAssemblerTest {

	@Test
	public void assembleDsSearchResultWithTimeSeries() {
		DataSnapshotEntity dsEntity =
				new EegStudy(
						new ExtAclEntity());

		dsEntity.setLabel("label");
		dsEntity.setPubId("anId");

		TimeSeriesEntity ts0 = new TimeSeriesEntity();
		ts0.setPubId((newUuid()));
		ts0.setLabel("ts0");

		TimeSeriesEntity ts1 = new TimeSeriesEntity();
		ts1.setPubId((newUuid()));
		ts1.setLabel("ts1");
		Set<TimeSeriesEntity> tsSet = newHashSet(ts0, ts1);

		DataSnapshotSearchResult dsResult = DsSearchResultAssembler
				.assembleDsSearchResult(dsEntity, tsSet, 5, 4);
		assertEquals(dsEntity.getPubId(), dsResult.getId());
		assertEquals(dsEntity.getLabel(), dsResult.getLabel());
		assertEquals(tsSet.size(), dsResult.getTimeSeries().size());
		assertNotNull(find(
				dsResult.getTimeSeries(),
				compose(equalTo(ts0.getLabel()),
						TimeSeriesSearchResult.getLabel),
				null));
		assertNotNull(find(
				dsResult.getTimeSeries(),
				compose(equalTo(ts1.getLabel()),
						TimeSeriesSearchResult.getLabel),
				null));
		assertEquals(dsResult.getImageCount(), 5);
		assertEquals(dsResult.getTsAnnCount(), 4);
	}
}
