/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ExperimentNameTest {

	@Test(expected = NullPointerException.class)
	public void isExperimentNameNull() {
		ExperimentName.isExperimentName(null);
	}

	@Test
	public void isExperimentName() {
		assertTrue(ExperimentName.isExperimentName("I001_A0001_D001"));
		assertFalse(ExperimentName.isExperimentName("I001A0001D001"));
		assertFalse(ExperimentName.isExperimentName("I001_P001_D001"));
		assertFalse(ExperimentName.isExperimentName("xI001_A001_D001"));
		assertFalse(ExperimentName.isExperimentName("I01_A0001_D021"));
		assertFalse(ExperimentName.isExperimentName("I001_A000b_D013"));
		assertFalse(ExperimentName.isExperimentName("I001_A0001xD014"));
		assertFalse(ExperimentName.isExperimentName("I001_A0001_D01y"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorFail() {
		new ExperimentName("I001_A001_D01");
	}

	@Test
	public void constructor() {
		final ExperimentName name = new ExperimentName("I001_A0123_D199");
		assertEquals("I001", name.getOrganizationPart());
		assertEquals("A0123", name.getAnimalPart());
		assertEquals("D199", name.getExperimentPart());
	}
}
