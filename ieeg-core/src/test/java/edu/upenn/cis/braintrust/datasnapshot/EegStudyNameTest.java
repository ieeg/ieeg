/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EegStudyNameTest {

	@Test(expected = NullPointerException.class)
	public void isEegStudyNameNull() {
		EegStudyName.isEegStudyName(null);
	}

	@Test
	public void isEegStudyName() {
		assertTrue(EegStudyName.isEegStudyName("I001_P001_D01"));
		assertFalse(EegStudyName.isEegStudyName("I001P001D01"));
		assertFalse(EegStudyName.isEegStudyName("I001_A001_D01"));
		assertFalse(EegStudyName.isEegStudyName("xI001_P001_D01"));
		assertFalse(EegStudyName.isEegStudyName("I01_P001_D01"));
		assertFalse(EegStudyName.isEegStudyName("I001_P00b_D01"));
		assertFalse(EegStudyName.isEegStudyName("I001_P001xD01"));
		assertFalse(EegStudyName.isEegStudyName("I001_P001_D01y"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructorFail() {
		new EegStudyName("I001_A001_D01");
	}

	@Test
	public void constructor() {
		final EegStudyName name = new EegStudyName("I001_P023_D99");
		assertEquals("I001", name.getOrganizationPart());
		assertEquals("P023", name.getPatientPart());
		assertEquals("D99", name.getEegStudyPart());
	}
}
