/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.JobStatus;

public class JobStatusTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (JobStatus status : JobStatus.values()) {
			switch (status) {
				case UNSCHEDULED:
					assertEquals("db enums must not change order!", 0,
							status.ordinal());
					break;
				case RUNNING:
					assertEquals("db enums must not change order!", 1,
							status.ordinal());
					break;
				case KILLED:
					assertEquals("db enums must not change order!", 2,
							status.ordinal());
					break;
				case COMPLETE:
					assertEquals("db enums must not change order!", 3,
							status.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + status, true);
					break;
			}
		}
	}
}
