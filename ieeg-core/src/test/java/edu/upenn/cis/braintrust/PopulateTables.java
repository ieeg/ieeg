/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust;

import static com.google.common.collect.Lists.newArrayList;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.drupal.persistence.DrupalHibernateUtil;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.testhelper.BuildDb;

public class PopulateTables {

	public static void main(String[] args) throws Throwable {
		SchemaExport schemaExport1 = new SchemaExport(
				DrupalHibernateUtil.getConfiguration());
		schemaExport1.create(false, true);

		List<Patient> patients = newArrayList();

		Session session;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();

			IPermissionDAO permDAO = new PermissionDAOHibernate(session);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(
					patients,
					readPerm);

			for (Patient patient : patients) {
				session.save(patient);
			}

			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (Throwable rbEx) {
					rbEx.printStackTrace();
				}
				throw t;
			}
		}
	}
}
