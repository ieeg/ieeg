/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.persistence;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

public class PersistenceUtilTest {

	@Test
	public void rollBackTrx() {
		Transaction trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(Boolean.TRUE);
		PersistenceUtil.rollback(trx);
		verify(trx).rollback();

		trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(Boolean.FALSE);
		PersistenceUtil.rollback(trx);
		verify(trx, never()).rollback();

		trx = mock(Transaction.class);
		when(trx.isActive()).thenReturn(Boolean.TRUE);
		doThrow(new IllegalStateException()).when(trx).rollback();
		PersistenceUtil.rollback(trx);
		verify(trx).rollback();

		PersistenceUtil.rollback((Transaction) null);
	}

	@Test
	public void closeSession() {
		Session sess = mock(Session.class);
		when(sess.isOpen()).thenReturn(Boolean.TRUE);
		PersistenceUtil.close(sess);
		verify(sess).close();

		sess = mock(Session.class);
		when(sess.isOpen()).thenReturn(Boolean.FALSE);
		PersistenceUtil.close(sess);
		verify(sess, never()).close();

		sess = mock(Session.class);
		when(sess.isOpen()).thenReturn(Boolean.TRUE);
		doThrow(new IllegalStateException()).when(sess).close();
		PersistenceUtil.close(sess);
		verify(sess).close();

		PersistenceUtil.close((Session) null);
	}
}
