/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author John Frommeyer
 * 
 */
public class RecordingObjectTest {

	private SessionFactory sessFac = HibernateUtil.getSessionFactory();
	private TstObjectFactory tstObjectFactory;
	private String authedUserPlaintextPassword = newUuid();
	private User authedUser;
	private Long recordingId;

	@Before
	public void testSetUp() throws Exception {

		// we need to initialize IvProps for DataSnapshotServerFactory
		IvProps.setIvProps(Collections.<String, String> emptyMap());

		SessionAndTrx sessAndTrx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(sess);
			tstObjectFactory = new TstObjectFactory(
					permissionDAO.findStarCoreReadPerm(),
					permissionDAO.findStarCoreOwnerPerm());

			authedUser = tstObjectFactory
					.newUserRegularEnabled(authedUserPlaintextPassword);

			IUserService userService = Mockito.mock(IUserService.class);
			when(userService.findUserByUsername(authedUser.getUsername()))
					.thenReturn(
							authedUser);
			when(userService.findExistingEnabledUser(authedUser.getUsername()))
					.thenReturn(authedUser);
			UserServiceFactory.setUserService(userService);

			UserEntity authedUserEntity = tstObjectFactory
					.newUserEntity(authedUser.getUserId());
			sess.saveOrUpdate(authedUserEntity);
			ExtAclEntity studyExtAcl = tstObjectFactory
					.newExtAclEntityUserOwnedAndWorldReadable(authedUserEntity);

			Patient patient = tstObjectFactory.newPatient();
			sess.saveOrUpdate(patient);
			HospitalAdmission admission = tstObjectFactory
					.newHospitalAdmission();
			patient.addAdmission(admission);
			EegStudy study = tstObjectFactory.newEegStudy(studyExtAcl);
			admission.addStudy(study);

			Recording recording = study.getRecording();
			ContactGroup contactGroup = tstObjectFactory.newContactGroup();
			recording.addContactGroup(contactGroup);

			TimeSeriesEntity ts1 = tstObjectFactory.newTimeSeries();
			Contact contact1 = tstObjectFactory.newContact(ts1);
			contactGroup.addContact(contact1);

			TimeSeriesEntity ts2 = tstObjectFactory.newTimeSeries();
			Contact contact2 = tstObjectFactory.newContact(ts2);
			contactGroup.addContact(contact2);

			TimeSeriesEntity ts3 = tstObjectFactory.newTimeSeries();
			Contact contact3 = tstObjectFactory.newContact(ts3);
			contactGroup.addContact(contact3);

			PersistenceUtil.commit(sessAndTrx);

			recordingId = recording.getId();
		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}
	}

	@Test
	public void eTagTest() throws Exception {
		Long recordingObjectId = null;
		Integer recordingObjectVersion = null;
		SessionAndTrx sessAndTrx = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			Recording recordingEntity = (Recording) sess.load(
					Recording.class,
					recordingId);
			UserEntity creator = (UserEntity) sess.load(
					UserEntity.class,
					authedUser.getUserId());
			RecordingObjectEntity recordingObjectEntity = new RecordingObjectEntity(
					recordingEntity,
					"test.txt",
					"text/plain",
					100,
					"06b474b3d97eb1af065831293fc3b541",
					creator,
					"A text file");
			recordingEntity.getObjects().add(recordingObjectEntity);

			PersistenceUtil.commit(sessAndTrx);
			recordingObjectId = recordingObjectEntity.getId();
			recordingObjectVersion = recordingObjectEntity.getVersion();

		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}

		assertNotNull(recordingObjectVersion);

		Integer recordingObjectVersion2 = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			sess.flush();
			RecordingObjectEntity recordingObjectEntity =
					(RecordingObjectEntity) sess
							.load(
									RecordingObjectEntity.class,
									recordingObjectId);

			recordingObjectEntity
					.setJson("{ \"key\" : \"value1\" }");

			if (sess.isDirty()) {
				sess.buildLockRequest(
						new LockOptions(LockMode.OPTIMISTIC_FORCE_INCREMENT))
						.lock(recordingObjectEntity);
			}

			PersistenceUtil.commit(sessAndTrx);

			recordingObjectVersion2 = recordingObjectEntity.getVersion();

		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}

		assertEquals(
				recordingObjectVersion.intValue() + 1,
				recordingObjectVersion2.intValue());

		Integer recordingObjectVersion3 = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			sess.flush();
			RecordingObjectEntity recordingObjectEntity = (RecordingObjectEntity) sess
					.load(RecordingObjectEntity.class, recordingObjectId);


			recordingObjectEntity
					.setJson("{ \"key\" : \"value1\" }");

			if (sess.isDirty()) {
				sess.buildLockRequest(
						new LockOptions(LockMode.OPTIMISTIC_FORCE_INCREMENT))
						.lock(recordingObjectEntity);
			}

			PersistenceUtil.commit(sessAndTrx);
			recordingObjectVersion3 = recordingObjectEntity.getVersion();

		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}

		assertEquals(
				recordingObjectVersion2.intValue(),
				recordingObjectVersion3.intValue());

		Integer recordingObjectVersion4 = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			sess.flush();
			RecordingObjectEntity recordingObjectEntity = (RecordingObjectEntity) sess
					.load(RecordingObjectEntity.class, recordingObjectId);

			recordingObjectEntity
					.setJson("{ \"key\" : \"value2\" }");

			if (sess.isDirty()) {
				sess.buildLockRequest(
						new LockOptions(LockMode.OPTIMISTIC_FORCE_INCREMENT))
						.lock(recordingObjectEntity);
			}

			PersistenceUtil.commit(sessAndTrx);
			recordingObjectVersion4 = recordingObjectEntity.getVersion();

		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}

		assertEquals(
				recordingObjectVersion3.intValue() + 1,
				recordingObjectVersion4.intValue());
	}
}
