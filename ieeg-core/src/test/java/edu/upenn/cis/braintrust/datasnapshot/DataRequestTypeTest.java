/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.DataRequestType;

/**
 * @author Sam Donnelly
 */
public class DataRequestTypeTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (DataRequestType enumValue : DataRequestType.values()) {
			switch (enumValue) {
				case CSV_DOWNLOAD:
					assertEquals("db enums must not change name!",
							"CSV_DOWNLOAD",
							enumValue.name());
					break;
				case CSV_DOWNLOAD_RAW:
					assertEquals("db enums must not change name!",
							"CSV_DOWNLOAD_RAW",
							enumValue.name());
					break;
				case VIEWER:
					assertEquals("db enums must not change name!", "VIEWER",
							enumValue.name());
					break;
				case WS_UNSCALED_RAW_BINARY:
					assertEquals("db enums must not change name!",
							"WS_UNSCALED_RAW_BINARY",
							enumValue.name());
					break;
				case WS_UNSCALED_RAW_RED:
					assertEquals("db enums must not change name!",
							"WS_UNSCALED_RAW_RED",
							enumValue.name());
					break;
				case WS_TIME_SERIES_DETAILS:
					assertEquals("db enums must not change name!",
							"WS_TIME_SERIES_DETAILS",
							enumValue.name());
					break;
				case WS_DIRECT_DATA:
					assertEquals("db enums must not change name!",
							"WS_DIRECT_DATA",
							enumValue.name());
					break;
				case WS_RECORDING_OBJECT:
					assertEquals("db enums must not change name!",
							"WS_RECORDING_OBJECT",
							enumValue.name());
					break;
				default:
					assertFalse("unknown enum: " + enumValue, true);
					break;
			}
		}
	}
}
