/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IToolDAO;
import edu.upenn.cis.braintrust.dao.tools.ToolDAOHibernate;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * 
 * @author John Frommeyer
 *
 */
public class ToolDAOHibernateTest {
	private TstObjectFactory objFac;
	private UserId ownerUserId;
	private UserId regUserId;
	@Before
	public void setup() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();

			final IPermissionDAO permDAO = new PermissionDAOHibernate(sess);
			objFac = new TstObjectFactory(
					permDAO.findStarCoreReadPerm(),
					permDAO.findStarCoreOwnerPerm());

			final UserEntity ownerUser = objFac.newUserEntity();
			sess.save(ownerUser);
			ownerUserId = ownerUser.getId();

			final UserEntity regUser = objFac.newUserEntity();
			sess.save(regUser);
			regUserId = regUser.getId();

			final ToolEntity tool = objFac.newTool(ownerUser);
			sess.save(tool);
			final ExtUserAceEntity userAce = new ExtUserAceEntity(
					ownerUser,
					tool.getExtAcl(),
					objFac.getOwnerPerm());
			tool.getExtAcl().getUserAces().add(userAce);

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void authzTest() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			final IToolDAO toolDAO = new ToolDAOHibernate(sess);

			// ownerUser will get its tool back.
			final UserEntity ownerUser = (UserEntity) sess.load(
					UserEntity.class, ownerUserId);
			List<ToolEntity> ownerTools = toolDAO.findAll(ownerUser);
			assertEquals(1, ownerTools.size());

			// regUser will get no tools
			final UserEntity regUser = (UserEntity) sess.load(
					UserEntity.class, regUserId);
			List<ToolEntity> regTools = toolDAO.findAll(
					regUser);
			assertTrue(regTools.isEmpty());
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
}
