/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.services;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dto.TraceForImage;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.PixelCoordinates;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

/**
 * DOCUMENT ME
 * 
 * @author John Frommeyer
 * 
 */
@RunWith(Theories.class)
public class TracesForImageResourceTest {
	private static Logger logger = LoggerFactory
			.getLogger(TracesForImageResourceTest.class);
	private static ImagedContact imagedContact00;
	private static ImagedContact imagedContact11;
	private static ImagedContact imagedContact10;

	private static Function<TraceForImage, String> getChannel = new Function<TraceForImage, String>() {
		@Override
		public String apply(final TraceForImage input) {
			return input.getChannel();
		}
	};

	@BeforeClass
	static public void classSetUp() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory()
					.openSession();

			trx = s.beginTransaction();

			BrainTrustDb btDb = new BrainTrustDb(s);

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = Collections.emptyList();

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			btDb.saveEverything(patients, null,
					null, null, datasets, null);

			final Patient patient0 = patients.get(0);

			final EegStudy study = getOnlyElement(
					getOnlyElement(patient0.getAdmissions()).getStudies());
			assertTrue(study.getImages().size() >= 2);
			final List<ImageEntity> imageList = newArrayList(study.getImages());
			final ImageEntity image0 = imageList.get(0);
			final ImageEntity image1 = imageList.get(1);

			assertTrue(study.getRecording().getContactGroups().size() >= 2);
			final List<ContactGroup> contactGroupList = newArrayList(study
					.getRecording().getContactGroups());
			final ContactGroup contactGroup0 = contactGroupList.get(0);
			final ContactGroup contactGroup1 = contactGroupList.get(1);

			final Contact contact0 = getFirst(contactGroup0
					.getContacts(), null);
			assertNotNull(contact0);
			//Reset labels to ensure uniqueness for when we find them below.
			contact0.getTrace().setLabel(newUuid());

			final Contact contact1 = getFirst(contactGroup1
					.getContacts(), null);
			assertNotNull(contact1);
			contact1.getTrace().setLabel(newUuid());
			
			imagedContact00 = new ImagedContact(new PixelCoordinates(3, 5),
					image0,
					contact0);
			
			imagedContact11 = new ImagedContact(new PixelCoordinates(1, 2),
					image1,
					contact1);

			imagedContact10 = new ImagedContact(new PixelCoordinates(5, 5),
					image1,
					contact0);
			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	@Test
	public void getTracesForImage() throws Throwable {
		ITracesForImageResource tracesForImageResource = new TracesForImageResource(
				new HibernateDAOFactory());

		Set<TraceForImage> tracesForImage0 = tracesForImageResource
				.getTracesForImage(imagedContact00.getImage().getFileKey());
		assertEquals(1, tracesForImage0.size());
		TraceForImage trace0ForImage0 = getOnlyElement(tracesForImage0);
		assertEquals(imagedContact00.getPxCoordinates().getX().intValue(),
				trace0ForImage0.getPixelCoordX());
		assertEquals(imagedContact00.getPxCoordinates().getY().intValue(),
				trace0ForImage0.getPixelCoordY());
		assertEquals(imagedContact00.getContact().getTrace().getLabel(),
				trace0ForImage0.getChannel());

		Set<TraceForImage> tracesForImage1 = tracesForImageResource
				.getTracesForImage(imagedContact11.getImage().getFileKey());
		assertEquals(2, tracesForImage1.size());

		TraceForImage trace1ForImage1 = Iterables
				.find(tracesForImage1,
						compose(equalTo(imagedContact11.getContact().getTrace()
								.getLabel()),
								getChannel));
		assertEquals(imagedContact11.getPxCoordinates().getX().intValue(),
				trace1ForImage1.getPixelCoordX());
		assertEquals(imagedContact11.getPxCoordinates().getY().intValue(),
				trace1ForImage1.getPixelCoordY());

		TraceForImage trace0ForImage1 = Iterables
				.find(tracesForImage1,
						compose(equalTo(imagedContact10.getContact().getTrace()
								.getLabel()),
								getChannel));
		assertEquals(imagedContact10.getPxCoordinates().getX().intValue(),
				trace0ForImage1.getPixelCoordX());
		assertEquals(imagedContact10.getPxCoordinates().getY().intValue(),
				trace0ForImage1.getPixelCoordY());
	}

	/**
	 * Make sure that getTracesForImage closing its session.
	 * 
	 * @throws Throwable
	 */
	@Test
	@Ignore("can't test for a closed session like that with managed session context")
	public void getTracesForImageClosesSession() throws Throwable {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(session);
			final TracesForImageResource tracesForImageResource = new TracesForImageResource(
					new HibernateDAOFactory());
			tracesForImageResource
					.getTracesForImage(imagedContact00.getImage().getFileKey());
			assertFalse(session.isOpen());
		} finally {
			if (session != null) {
				session.getTransaction().rollback();
				ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
				session.close();
			}
		}
	}

	@Test
	@Ignore("can't test for a closed session like that with managed session context")
	public void getTracesForImageWErrorClosesSession() throws Throwable {
		final IDAOFactory daoFac = mock(IDAOFactory.class);
		final IImageDAO imageDAO = mock(IImageDAO.class);
		when(daoFac.getImageDAO()).thenReturn(imageDAO);
		when(imageDAO.findByFileKey(anyString())).thenThrow(
				new IllegalStateException());

		Session session = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(session);

			final TracesForImageResource tracesForImageResource = new TracesForImageResource(
					daoFac);
			try {
				tracesForImageResource.getTracesForImage("");
				fail("no exception was thrown but was expected");
			} catch (IllegalStateException e) {

			}
			assertFalse(session.isOpen());
		} finally {
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
			PersistenceUtil.close(session);
		}
	}
}
