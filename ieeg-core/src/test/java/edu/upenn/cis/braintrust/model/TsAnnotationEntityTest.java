/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class TsAnnotationEntityTest {

	private TstObjectFactory tstObjFac = new TstObjectFactory(true);

	@Test
	public void constructorAnnParent() {
		Dataset sourceDs = tstObjFac.newDataset();
		TimeSeriesEntity ts = tstObjFac.newTimeSeries(sourceDs);

		Dataset targetDs = tstObjFac.newDataset();

		TsAnnotationEntity expectedTsAnn =
				tstObjFac.newTsAnnotation(ts, sourceDs,
						tstObjFac.newUserEntity());

		TsAnnotationEntity actualTsAnn =
				new TsAnnotationEntity(expectedTsAnn, targetDs);
		assertEquals(
				getOnlyElement(expectedTsAnn.getAnnotated()),
				getOnlyElement(actualTsAnn.getAnnotated()));
		assertEquals(expectedTsAnn.getAnnotator(), actualTsAnn.getAnnotator());
		assertEquals(expectedTsAnn.getCreator(), actualTsAnn.getCreator());

		// Make sure tstObjFac actually assigns a value here, because it wasn't
		assertNotNull(expectedTsAnn.getDescription());
		assertEquals(
				expectedTsAnn.getDescription(),
				actualTsAnn.getDescription());

		assertEquals(
				expectedTsAnn.getEndOffsetUsecs(),
				actualTsAnn.getEndOffsetUsecs());

		assertEquals(
				targetDs,
				actualTsAnn.getParent());
		assertEquals(
				expectedTsAnn.getStartOffsetUsecs(),
				actualTsAnn.getStartOffsetUsecs());
		assertEquals(
				expectedTsAnn.getType(),
				actualTsAnn.getType());

	}
}
