/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust;

import static com.google.common.collect.Maps.newHashMap;

import java.util.List;
import java.util.Map;

import org.hibernate.ScrollableResults;

import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.habitat.persistence.Scope;

public class TstUserDAO implements IUserDAO {

	private Map<UserId, UserEntity> idsToUsers = newHashMap();

	public TstUserDAO(Map<UserId, UserEntity> idsToUsers) {
		this.idsToUsers = idsToUsers;
	}

	@Override
	public List<UserEntity> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<UserEntity> findByExample(UserEntity exampleInstance,
			String... excludeProperty) {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserEntity findById(UserId id, boolean lock) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void flush() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void saveOrUpdate(UserEntity entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(UserEntity entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserEntity get(UserId id) {
		return idsToUsers.get(id);
	}

	@Override
	public void setReadOnly(UserEntity entity, boolean readOnly) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void evict(UserEntity entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void forceVersIncrPes(UserEntity entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ScrollableResults findAllScroll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserEntity getOrCreateUser(UserId userId) {
		throw new UnsupportedOperationException("Operation not supported.");

	}

	@Override
	public void lock(UserEntity entity) {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserEntity findByNaturalId(
			String naturalId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<UserEntity> findAllOrderBy(String property) {
		throw new UnsupportedOperationException();
	}

	@Override
	public UserEntity findByNaturalId(String naturalId, boolean upgradeLock) {
		throw new UnsupportedOperationException("Operation not supported.");

	}

	@Override
	public UserEntity load(UserId id) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void refresh(UserEntity entity, boolean upgradeLock) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void evict(Iterable<? extends UserEntity> entities) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void setScope(Scope s) {
		// TODO Auto-generated method stub
		
	}
	
	public Scope getDefaultScope() {
		return null;
	}
}
