/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.sql;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

public class PointSevenToPointEight {

	@SuppressFBWarnings(
			value = {
					"ODR_OPEN_DATABASE_RESOURCE",
					"DMI_EMPTY_DB_PASSWORD"
			},
			justification = "don't care - just a one off program")
	public static void main(String[] args) throws Throwable {
		Connection con7 = null;
		Connection con8 = null;

		try {

			con7 = DriverManager.getConnection(
					"jdbc:mysql://localhost/braintrust7", "root", "");

			Statement stmt7 = con7.createStatement();
			ResultSet rs7 = stmt7
					.executeQuery("select count(*) from eeg_study order by eeg_study_id");
			rs7.first();

			con8 = DriverManager.getConnection(
					"jdbc:mysql://localhost/braintrust8", "root", "");
			Statement stmt8 = con8.createStatement();
			ResultSet rs8 = stmt8
					.executeQuery("select count(*) from eeg_study inner join data_snapshot on eeg_study.eeg_study_id=data_snapshot.data_snapshot_id order by eeg_study.eeg_study_id");
			rs8.first();
			System.out.println("found " + rs7.getLong(1) + " and "
					+ rs8.getLong(1) + " rows");
			assertEquals(rs7.getLong(1), rs8.getLong(1));

			stmt7 = con7.createStatement();
			rs7 = stmt7
					.executeQuery("select * from eeg_study order by eeg_study_id");

			stmt8 = con8.createStatement();
			rs8 = stmt8
					.executeQuery("select * from eeg_study inner join data_snapshot on eeg_study.eeg_study_id=data_snapshot.data_snapshot_id order by eeg_study.eeg_study_id");

			while (rs7.next() && rs8.next()) {
				assertEquals(rs7.getLong("eeg_study_id"),
						rs8.getLong("eeg_study_id"));
				assertEquals(rs7.getLong("eeg_study_id"),
						rs8.getLong("data_snapshot_id"));
				assertEquals(rs7.getString("label"), rs8.getString("label"));
				assertEquals(0, rs8.getLong("protection_group_id"));
				assertEquals(rs7.getString("revision_id"),
						rs8.getString("revision_id"));
				assertEquals(rs7.getDate("revision_timestamp"),
						rs8.getDate("revision_timestamp"));
				assertEquals(rs7.getInt("obj_version"),
						rs8.getInt("obj_version"));
				assertEquals(rs7.getString("dir"), rs8.getString("dir"));
				assertEquals(rs7.getDate("end_time"), rs8.getDate("end_time"));
				assertEquals(rs7.getString("maf_file"),
						rs8.getString("maf_file"));
				assertEquals(rs7.getString("mef_dir"), rs8.getString("mef_dir"));
				assertEquals(rs7.getLong("hospital_admission_id"),
						rs8.getLong("hospital_admission_id"));
				assertEquals(rs7.getDate("start_time"),
						rs8.getDate("start_time"));
				assertEquals(rs7.getInt("seizure_count"),
						rs8.getInt("seizure_count"));
				assertEquals(rs7.getInt("type"), rs8.getInt("type"));
			}

			stmt7 = con7.createStatement();
			rs7 = stmt7
					.executeQuery("select count(*) from eeg_study_time_series_annotation order by eeg_study_id");
			rs7.first();

			stmt8 = con8.createStatement();
			rs8 = stmt8
					.executeQuery("select count(*) from data_snapshot_ts_annotation order by data_snapshot_id");
			rs8.first();

			System.out.println("found " + rs7.getLong(1) + " and "
					+ rs8.getLong(1) + " rows");
			assertEquals(rs7.getLong(1), rs8.getLong(1));

			stmt7 = con7.createStatement();
			rs7 = stmt7
					.executeQuery("select * from eeg_study_time_series_annotation order by eeg_study_id");

			stmt8 = con8.createStatement();
			rs8 = stmt8
					.executeQuery("select * from data_snapshot_ts_annotation order by data_snapshot_id");
			while (rs7.next() && rs8.next()) {
				assertEquals(rs7.getLong("eeg_study_id"),
						rs8.getLong("data_snapshot_id"));
				assertEquals(rs7.getLong("time_series_annotation_id"),
						rs8.getLong("time_series_annotation_id"));
			}

			System.out.println("success");
		} finally {
			if (con7 != null) {
				con7.close();
			}
			if (con8 != null) {
				con8.close();
			}
		}
	}
}
