/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.services;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

public class ImagesForStudyResourceTest {
	private static Logger logger = LoggerFactory
			.getLogger(ImagesForStudyResourceTest.class);
	private static EegStudy study;

	private static ImageEntity image0;
	private static ImageEntity image1;
	private static ImageEntity image2;
	private static ImageEntity image3;

	private static Function<ImageForStudy, String> getFileKey = new Function<ImageForStudy, String>() {
		@Override
		public String apply(final ImageForStudy input) {
			return input.getImageFileKey();
		}
	};

	@BeforeClass
	static public void classSetUp() throws Throwable {

		Session s = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory()
					.openSession();

			trx = s.beginTransaction();

			final BrainTrustDb btDb = new BrainTrustDb(s);

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = Collections.emptyList();

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			btDb.saveEverything(patients, null,
					null, null, datasets, null);

			final Patient patient0 = patients.get(0);

			study = getOnlyElement(getOnlyElement(patient0.getAdmissions())
					.getStudies());

			image0 = get(study.getImages(), 0);
			image1 = get(study.getImages(), 1);
			image2 = get(study.getImages(), 2);
			image3 = new ImageEntity();
			image3.setPubId((randomUUID().toString()));
			study.getImages().add(image3);
			image3
					.setFileKey("Xyz_IEED_data/Xyz_IEED_001/MRI/IM-001-001.jpg");
			image3.setType(ImageType.MRI);
			trx.commit();

		} catch (final Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	private final IDAOFactory daoFac = new HibernateDAOFactory();

	@Test(expected = IllegalArgumentException.class)
	public void badImageType() {
		final IImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
				daoFac);
		imagesForStudyResource.getImagesForStudy(study.getDir(),
				"NOT_AN_IMAGE_TYPE");
	}

	@Test
	public void getAllImagesForStudy() {
		final IImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
				daoFac);

		final Set<ImageForStudy> allImagesForStudy = imagesForStudyResource
				.getAllImagesForStudy(study.getDir());
		assertEquals(4, allImagesForStudy.size());

		final ImageForStudy imageForStudy0 = Iterables.find(allImagesForStudy,
				compose(equalTo(image0.getFileKey()), getFileKey));
		assertEquals(image0.getType().name(), imageForStudy0.getImageType());
		assertEquals(image0.getType(), imageForStudy0.getImageTypeEnum());

		final ImageForStudy imageForStudy1 = Iterables.find(allImagesForStudy,
				compose(equalTo(image1.getFileKey()), getFileKey));
		assertEquals(image1.getType().name(), imageForStudy1.getImageType());
		assertEquals(image1.getType(), imageForStudy1.getImageTypeEnum());

		final ImageForStudy imageForStudy2 = Iterables.find(allImagesForStudy,
				compose(equalTo(image2.getFileKey()), getFileKey));
		assertEquals(image2.getType().name(), imageForStudy2.getImageType());
		assertEquals(image2.getType(), imageForStudy2.getImageTypeEnum());

		final ImageForStudy imageForStudy3 = Iterables.find(allImagesForStudy,
				compose(equalTo(image3.getFileKey()), getFileKey));
		assertEquals(image3.getType().name(), imageForStudy3.getImageType());
		assertEquals(image3.getType(), imageForStudy3.getImageTypeEnum());

	}

	/**
	 * Make sure that getAllImagesForStudy is closing its session.
	 * 
	 * @throws Throwable
	 */
	@Test
	@Ignore("can't test closing with managed current session")
	public void getAllImagesForStudyClosesSession() throws Throwable {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();

			final ImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
					daoFac);
			imagesForStudyResource.getAllImagesForStudy(study.getDir());
			assertFalse(session.isOpen());
		} catch (final Throwable t) {
			try {
				if (session != null && session.isOpen()) {
					session.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while closing session", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void getImagesForStudy() {
		final IImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
				daoFac);
		final Set<ImageForStudy> threeDImagesForStudy = imagesForStudyResource
				.getImagesForStudy(study.getDir(), ImageType.THREE_D_RENDERING
						.name());
		assertEquals(3, threeDImagesForStudy.size());

		final ImageForStudy imageForStudy0 = Iterables.find(
				threeDImagesForStudy,
				compose(equalTo(image0.getFileKey()), getFileKey));
		assertEquals(image0.getType().name(), imageForStudy0.getImageType());
		assertEquals(image0.getType(), imageForStudy0.getImageTypeEnum());

		final ImageForStudy imageForStudy1 = Iterables.find(
				threeDImagesForStudy,
				compose(equalTo(image1.getFileKey()), getFileKey));
		assertEquals(image1.getType().name(), imageForStudy1.getImageType());
		assertEquals(image1.getType(), imageForStudy1.getImageTypeEnum());

		final ImageForStudy imageForStudy2 = Iterables.find(
				threeDImagesForStudy,
				compose(equalTo(image2.getFileKey()), getFileKey));
		assertEquals(image2.getType().name(), imageForStudy2.getImageType());
		assertEquals(image2.getType(), imageForStudy2.getImageTypeEnum());

		final Set<ImageForStudy> ctImagesForStudy = imagesForStudyResource
				.getImagesForStudy(study.getDir(), ImageType.CT.name());
		assertTrue(ctImagesForStudy.isEmpty());

		final Set<ImageForStudy> mriImagesForStudy = imagesForStudyResource
				.getImagesForStudy(study.getDir(), ImageType.MRI.name());
		assertEquals(1, mriImagesForStudy.size());

		final ImageForStudy imageForStudy3 = Iterables.find(mriImagesForStudy,
				compose(equalTo(image3.getFileKey()), getFileKey));
		assertEquals(image3.getType().name(), imageForStudy3.getImageType());
		assertEquals(image3.getType(), imageForStudy3.getImageTypeEnum());

	}

	/**
	 * Make sure that getImagesForStudy is closing its session.
	 * 
	 * @throws Throwable
	 */
	@Test
	@Ignore("can't test closing with managed current session")
	public void getImagesForStudyClosesSession() throws Throwable {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();

			final ImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
					daoFac);
			imagesForStudyResource.getImagesForStudy(study.getDir(),
					ImageType.CT.name());
			assertFalse(session.isOpen());
		} catch (final Throwable t) {
			try {
				if (session != null && session.isOpen()) {
					session.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while closing session", rbEx);
			}
			throw t;
		}
	}

	@Test
	@Ignore("can't test closing with managed current session")
	public void getImagesForStudyWErrorClosesSession() throws Throwable {
		final IDAOFactory daoFac = mock(IDAOFactory.class);
		final IImageDAO imageDAO = mock(IImageDAO.class);

		when(daoFac.getImageDAO()).thenReturn(imageDAO);
		when(imageDAO.findByStudyDirAndImageType(anyString(),
				any(ImageType.class))).thenThrow(new IllegalStateException());

		Session session = null;

		try {

			session = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(session);

			final ImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
					daoFac);

			try {
				imagesForStudyResource.getImagesForStudy(study.getDir(),
						ImageType.CT.name());
				fail("no exception was thrown but was expected");
			} catch (final IllegalStateException e) {

			}

			assertFalse(session.isOpen());
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());

		} finally {
			PersistenceUtil.close(session);
		}
	}
}
