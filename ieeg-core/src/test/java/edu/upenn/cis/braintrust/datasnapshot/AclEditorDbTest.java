/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.ProjectDAOHibernate;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.identity.hibernate.UserDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.ProjectAceEntity;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermissionAssembler;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.NewUserAce;
import edu.upenn.cis.braintrust.shared.RemoveProjectAce;
import edu.upenn.cis.braintrust.shared.RemoveUserAce;
import edu.upenn.cis.braintrust.shared.UpdateUserAce;
import edu.upenn.cis.braintrust.shared.UpdateWorldAce;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class AclEditorDbTest {

	private TstUsers dbUsers;
	private TstObjectFactory tstObjFactory;
	private String dsRevId;
	private Long aclId;
	private Integer aclVersion;
	private PermissionEntity origWorldPerms;
	private PermissionEntity origPerms;

	private static final Logger logger = LoggerFactory
			.getLogger(AclEditorDbTest.class);
	private ExtUserAceEntity regUser1Ace;
	private ExtUserAceEntity regUser2Ace;
	private ProjectAceEntity teamGroupAce;

	@Before
	public void testSetUp() throws Throwable {

		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(session);

			tstObjFactory = new TstObjectFactory(
					permissionDAO.findStarCoreReadPerm(),
					permissionDAO.findStarCoreOwnerPerm());


			dbUsers = new TstUsers(session);

			final PermissionEntity readPerm = tstObjFactory.getReadPerm();
			ExtAclEntity acl = new ExtAclEntity(Collections.singleton(readPerm));

			final PermissionEntity ownerPerm = tstObjFactory.getOwnerPerm();
			origPerms = ownerPerm;

			// Constructor adds UserAceEntity to the acl.
			regUser1Ace = new ExtUserAceEntity(dbUsers.btRegUser1, acl,
					Collections.singleton(origPerms));
			origWorldPerms = getOnlyElement(acl.getWorldAce());

			regUser2Ace = new ExtUserAceEntity(
					dbUsers.btRegUser2,
					acl,
					readPerm);

			final ProjectEntity project = tstObjFactory.newProject(false);
			session.save(project);

			project.getAdmins().add(dbUsers.btRegUser1);
			project.getTeam().add(dbUsers.btRegUser2);

			// Constructor will add ace to acl
			new ProjectAceEntity(
					project,
					ProjectGroupType.ADMINS,
					acl,
					ownerPerm);

			teamGroupAce = new ProjectAceEntity(
					project,
					ProjectGroupType.TEAM,
					acl,
					readPerm);

			Dataset dataset = tstObjFactory.newDataset(dbUsers.btRegUser1, acl);
			project.getSnapshots().add(dataset);

			dsRevId = dataset.getPubId();
			session.save(dataset);
			aclId = dataset.getExtAcl().getId();
			aclVersion = dataset.getExtAcl().getVersion();
			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	@Test
	public void editAclNewUserAce() throws Throwable {
		Transaction trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IUserDAO userDAO = new UserDAOHibernate(session);
			final IProjectDAO projectDAO = new ProjectDAOHibernate(session);
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final ExtAclEntity acl = (ExtAclEntity) session.load(
					ExtAclEntity.class, aclId);
			final AclEditor editor = new AclEditor(
					acl,
					CorePermDefs.CORE_MODE_NAME,
					userDAO,
					projectDAO,
					permissionDAO);
			final ExtUserAce newAce = new ExtUserAce(
					dbUsers.regUser2.getUserId(),
					dbUsers.regUser2.getUsername(),
					dsRevId,
					aclVersion,
					null,
					null);
			newAce.getPerms().add(CorePermDefs.READ_PERM);

			final NewUserAce newUserAction = new NewUserAce(newAce);
			final List<IEditAclAction<?>> actions = newArrayList();
			actions.add(newUserAction);
			List<EditAclResponse> responses = editor.editAcl(dbUsers.regUser1,
					actions);
			EditAclResponse response = getOnlyElement(responses, null);
			assertTrue(response.isSuccess());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

		// Now check on the ACL
		trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dataSnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity actualDs = dataSnapshotDAO
					.findByNaturalId(dsRevId);
			final ExtAclEntity acl = actualDs.getExtAcl();
			final Set<PermissionEntity> actualWorldPerms = acl.getWorldAce();
			assertEquals(1, actualWorldPerms.size());
			assertEquals(origWorldPerms.getName(),
					getOnlyElement(actualWorldPerms).getName());
			final Set<ExtUserAceEntity> userAces = acl.getUserAces();
			assertEquals(2, userAces.size());

			final Optional<ExtUserAceEntity> regUser1Ace = tryFind(
					userAces,
					compose(equalTo(dbUsers.btRegUser1.getId()),
							ExtUserAceEntity.getUserId));
			assertTrue(regUser1Ace.isPresent());
			assertEquals(1, regUser1Ace.get().getPerms().size());
			assertEquals(origPerms.getName(),
					getOnlyElement(regUser1Ace.get().getPerms()).getName());

			final Optional<ExtUserAceEntity> regUser2Ace = tryFind(
					userAces,
					compose(equalTo(dbUsers.btRegUser2.getId()),
							ExtUserAceEntity.getUserId));
			assertTrue(regUser2Ace.isPresent());
			assertEquals(1, regUser2Ace.get().getPerms().size());
			assertEquals(CorePermDefs.READ_PERM.getValue(),
					getOnlyElement(regUser2Ace.get().getPerms()).getName());

			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			assertEquals(2, projectAces.size());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

	}

	@Test
	public void editAclUpdateUserAce() throws Throwable {
		Transaction trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IUserDAO userDAO = new UserDAOHibernate(session);
			final IProjectDAO projectDAO = new ProjectDAOHibernate(session);
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final ExtPermissionAssembler aclAssembler = new ExtPermissionAssembler(
					permissionDAO);
			final ExtAclEntity acl = (ExtAclEntity) session.load(
					ExtAclEntity.class, aclId);
			final AclEditor editor = new AclEditor(
					acl,
					CorePermDefs.CORE_MODE_NAME,
					userDAO,
					projectDAO,
					permissionDAO);
			final ExtUserAce reg1UserAceForUpdate = new ExtUserAce(
					dbUsers.regUser1.getUserId(),
					dbUsers.regUser1.getUsername(),
					dsRevId,
					aclVersion,
					regUser1Ace.getId(),
					regUser1Ace.getVersion());
			reg1UserAceForUpdate.getPerms().addAll(
					aclAssembler.toDtos(regUser1Ace.getPerms()));

			final UpdateUserAce updateUserAction = new UpdateUserAce(
					reg1UserAceForUpdate,
					CorePermDefs.READ_PERM);
			final List<IEditAclAction<?>> actions = newArrayList();
			actions.add(updateUserAction);
			List<EditAclResponse> responses = editor.editAcl(dbUsers.regUser1,
					actions);
			EditAclResponse response = getOnlyElement(responses, null);
			assertTrue(response.isSuccess());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

		// Now check on the ACL
		trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dataSnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity actualDs = dataSnapshotDAO
					.findByNaturalId(dsRevId);
			final ExtAclEntity acl = actualDs.getExtAcl();
			assertEquals(1, acl.getWorldAce().size());
			assertEquals(origWorldPerms.getName(),
					getOnlyElement(acl.getWorldAce()).getName());
			final Set<ExtUserAceEntity> actualUserAces = acl.getUserAces();
			assertEquals(2, actualUserAces.size());
			final ExtUserAceEntity actualRegUser1Ace = find(
					actualUserAces,
					compose(equalTo(regUser1Ace.getId()),
							ExtUserAceEntity.getId), null);
			assertNotNull(actualRegUser1Ace);
			assertEquals(1, actualRegUser1Ace.getPerms().size());
			assertEquals(CorePermDefs.READ_PERM.getValue(),
					getOnlyElement(actualRegUser1Ace.getPerms()).getName());

			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			assertEquals(2, projectAces.size());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

	}

	@Test
	public void editAclRemoveUserAce() throws Throwable {
		Transaction trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IUserDAO userDAO = new UserDAOHibernate(session);
			final IProjectDAO projectDAO = new ProjectDAOHibernate(session);
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final ExtAclEntity acl = (ExtAclEntity) session.load(
					ExtAclEntity.class, aclId);
			final AclEditor editor = new AclEditor(
					acl,
					CorePermDefs.CORE_MODE_NAME,
					userDAO,
					projectDAO,
					permissionDAO);
			final ExtPermissionAssembler extAclAssembler = new ExtPermissionAssembler(
					permissionDAO);
			final ExtUserAce regUser1AceForRemoval = new ExtUserAce(
					dbUsers.regUser1.getUserId(),
					dbUsers.regUser1.getUsername(),
					dsRevId,
					aclVersion,
					regUser1Ace.getId(),
					regUser1Ace.getVersion());
			regUser1AceForRemoval.getPerms().addAll(
					extAclAssembler.toDtos(regUser1Ace.getPerms()));

			final RemoveUserAce removeUser1Action = new RemoveUserAce(
					regUser1AceForRemoval);

			final ExtUserAce regUser2AceForRemoval = new ExtUserAce(
					dbUsers.regUser2.getUserId(),
					dbUsers.regUser2.getUsername(),
					dsRevId,
					aclVersion,
					regUser2Ace.getId(),
					regUser2Ace.getVersion());
			regUser2AceForRemoval.getPerms().addAll(
					extAclAssembler.toDtos(regUser2Ace.getPerms()));
			final RemoveUserAce removeUser2Action = new RemoveUserAce(
					regUser2AceForRemoval);

			final List<IEditAclAction<?>> actions = newArrayList();
			actions.add(removeUser1Action);
			actions.add(removeUser2Action);
			List<EditAclResponse> responses = editor.editAcl(dbUsers.regUser1,
					actions);
			assertEquals(2, responses.size());
			for (final EditAclResponse response : responses) {
				assertTrue(response.isSuccess());
			}

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

		// Now check on the ACL
		trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dataSnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity actualDs = dataSnapshotDAO
					.findByNaturalId(dsRevId);
			final ExtAclEntity acl = actualDs.getExtAcl();
			assertEquals(1, acl.getWorldAce().size());
			assertEquals(origWorldPerms.getName(),
					getOnlyElement(acl.getWorldAce()).getName());
			final Set<ExtUserAceEntity> userAces = acl.getUserAces();
			assertEquals(0, userAces.size());

			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			assertEquals(2, projectAces.size());
			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

	}

	@Test
	public void editAclRemoveProjectAce() throws Throwable {
		Transaction trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IUserDAO userDAO = new UserDAOHibernate(session);
			final IProjectDAO projectDAO = new ProjectDAOHibernate(session);
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final ExtAclEntity acl = (ExtAclEntity) session.load(
					ExtAclEntity.class,
					aclId);
			final AclEditor editor = new AclEditor(
					acl,
					CorePermDefs.CORE_MODE_NAME,
					userDAO,
					projectDAO,
					permissionDAO);
			final ExtPermissionAssembler extAclAssembler = new ExtPermissionAssembler(
					permissionDAO);
			final ExtProjectAce projTeamAceForRemoval = new ExtProjectAce(
					teamGroupAce.getProject().getPubId(),
					teamGroupAce.getProject().getName(),
					teamGroupAce.getProjectGroup(),
					dsRevId,
					aclVersion,
					teamGroupAce.getId(),
					teamGroupAce.getVersion());
			projTeamAceForRemoval.getPerms().addAll(
					extAclAssembler.toDtos(teamGroupAce.getPerms()));

			final RemoveProjectAce removeTeamGroupAceAction = new RemoveProjectAce(
					projTeamAceForRemoval);

			final List<IEditAclAction<?>> actions = newArrayList();
			actions.add(removeTeamGroupAceAction);

			List<EditAclResponse> responses = editor.editAcl(dbUsers.regUser1,
					actions);
			assertEquals(1, responses.size());
			for (final EditAclResponse response : responses) {
				assertTrue(response.isSuccess());
			}

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

		// Now check on the ACL
		trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dataSnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity actualDs = dataSnapshotDAO
					.findByNaturalId(dsRevId);
			final ExtAclEntity acl = actualDs.getExtAcl();
			assertEquals(1, acl.getWorldAce().size());
			assertEquals(origWorldPerms.getName(),
					getOnlyElement(acl.getWorldAce()).getName());
			final Set<ExtUserAceEntity> userAces = acl.getUserAces();
			assertEquals(2, userAces.size());

			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			assertEquals(1, projectAces.size());
			final ProjectAceEntity remainingProjAce = getOnlyElement(projectAces);
			assertEquals(ProjectGroupType.ADMINS,
					remainingProjAce.getProjectGroup());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

	}

	@Test
	public void editAclUpdateWorldAce() throws Throwable {
		Transaction trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IUserDAO userDAO = new UserDAOHibernate(session);
			final IProjectDAO projectDAO = new ProjectDAOHibernate(session);
			final IPermissionDAO permissionDAO = new PermissionDAOHibernate(
					session);
			final ExtPermissionAssembler aclAssembler = new ExtPermissionAssembler(
					permissionDAO);
			final ExtAclEntity acl = (ExtAclEntity) session.load(
					ExtAclEntity.class, aclId);
			final AclEditor editor = new AclEditor(
					acl,
					CorePermDefs.CORE_MODE_NAME,
					userDAO,
					projectDAO,
					permissionDAO);
			final ExtWorldAce worldAceForUpdate = new ExtWorldAce(
					dsRevId,
					aclId,
					aclVersion);
			worldAceForUpdate.getPerms().addAll(
					aclAssembler.toDtos(Collections.singleton(origWorldPerms)));

			final UpdateWorldAce updateWorldAction = new UpdateWorldAce(
					worldAceForUpdate,
					null);
			final List<IEditAclAction<?>> actions = newArrayList();
			actions.add(updateWorldAction);
			List<EditAclResponse> responses = editor.editAcl(dbUsers.regUser1,
					actions);
			EditAclResponse response = getOnlyElement(responses, null);
			assertTrue(response.isSuccess());

			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

		// Now check on the ACL
		trx = null;
		try {

			Session session = HibernateUtil.getSessionFactory()
					.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dataSnapshotDAO = new DataSnapshotDAOHibernate(
					session);
			final DataSnapshotEntity actualDs = dataSnapshotDAO
					.findByNaturalId(dsRevId);
			final ExtAclEntity acl = actualDs.getExtAcl();
			assertEquals(0, acl.getWorldAce().size());
			final Set<ExtUserAceEntity> userAces = acl.getUserAces();
			assertEquals(2, userAces.size());

			final ExtUserAceEntity actualRegUser1Ace = find(
					userAces,
					compose(equalTo(regUser1Ace.getId()),
							ExtUserAceEntity.getId), null);
			assertNotNull(actualRegUser1Ace);
			assertEquals(1, actualRegUser1Ace.getPerms().size());
			assertEquals(origPerms.getName(),
					getOnlyElement(actualRegUser1Ace.getPerms()).getName());

			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			assertEquals(2, projectAces.size());
			trx.commit();
			session.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}

	}
}
