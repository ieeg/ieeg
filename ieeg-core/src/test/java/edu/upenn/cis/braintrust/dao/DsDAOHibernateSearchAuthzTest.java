/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author John Frommeyer
 * 
 */
public class DsDAOHibernateSearchAuthzTest {

	private TstObjectFactory objFac;
	private User ownerUser;
	private User regUser;
	private User adminUser;
	private String publicId;

	@Before
	public void setup() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();

			final IPermissionDAO permDAO = new PermissionDAOHibernate(sess);
			objFac = new TstObjectFactory(
					permDAO.findStarCoreReadPerm(),
					permDAO.findStarCoreOwnerPerm());

			adminUser = objFac.newUserAdminEnabled();
			final UserEntity adminUserEntity = objFac.newUserEntity(adminUser
					.getUserId());
			sess.save(adminUserEntity);

			ownerUser = objFac.newUserRegularEnabled();
			final UserEntity ownerUserEntity = objFac.newUserEntity(ownerUser
					.getUserId());
			sess.save(ownerUserEntity);

			regUser = objFac.newUserRegularEnabled();
			final UserEntity regUserEntity = objFac.newUserEntity(regUser
					.getUserId());
			sess.save(regUserEntity);

			// A patient with a world-readable study
			final Patient publicPatient = objFac.newPatient();
			sess.save(publicPatient);

			final HospitalAdmission publicAdmission = objFac
					.newHospitalAdmission();
			publicPatient.addAdmission(publicAdmission);

			final EegStudy publicStudy = objFac.newEegStudy();
			publicAdmission.addStudy(publicStudy);
			publicStudy.getExtAcl().getWorldAce().add(objFac.getReadPerm());
			publicId = publicStudy.getPubId();

			// A patient with a study only visible to ownerUser
			final Patient privatePatient = objFac.newPatient();
			sess.save(privatePatient);

			final HospitalAdmission privateAdmission = objFac
					.newHospitalAdmission();
			privatePatient.addAdmission(privateAdmission);

			final EegStudy privateStudy = objFac.newEegStudy();
			privateAdmission.addStudy(privateStudy);
			final ExtUserAceEntity userAce = new ExtUserAceEntity(
					ownerUserEntity,
					privateStudy.getExtAcl(),
					objFac.getOwnerPerm());
			privateStudy.getExtAcl().getUserAces().add(userAce);

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void authzTest() throws Throwable {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			final IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(sess);
			// ownerUser will see both studies.
			final UserEntity ownerUserEntity = (UserEntity) sess.load(
					UserEntity.class, ownerUser.getUserId());

			Set<EegStudy> ownerSearchResult = dsDAO.findBySearch(
					new EegStudySearch(),
					ownerUserEntity,
					ownerUser);
			assertEquals(2, ownerSearchResult.size());

			// regUser will only see world-readable study
			final UserEntity regUserEntity = (UserEntity) sess.load(
					UserEntity.class, regUser.getUserId());
			Set<EegStudy> regSearchResult = dsDAO.findBySearch(
					new EegStudySearch(),
					regUserEntity,
					regUser);
			assertEquals(1, regSearchResult.size());
			final DataSnapshotEntity actualResult = getOnlyElement(regSearchResult);
			assertEquals(publicId, actualResult.getPubId());

			// adminUser will see both studies.
			final UserEntity adminUserEntity = (UserEntity) sess.load(
					UserEntity.class, adminUser.getUserId());

			Set<EegStudy> adminSearchResult = dsDAO.findBySearch(
					new EegStudySearch(),
					adminUserEntity,
					adminUser);
			assertEquals(2, adminSearchResult.size());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
}
