/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.UUID.randomUUID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.dto.TraceForImage;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.PixelCoordinates;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

/**
 * DOCUMENT ME
 * 
 * @author John Frommeyer
 * 
 */
public class ImageDAOTest {
	private static Logger logger = LoggerFactory.getLogger(ImageDAOTest.class);
	private static ImagedContact image0Contact00;
	private static ImagedContact image1Contact10;
	private static ImagedContact image1Contact11;
	private static EegStudy study;

	private static ImageEntity image0;
	private static ImageEntity image1;
	private static ImageEntity image2;
	private static ImageEntity image3;

	private final static Function<TraceForImage, String> getChannel = new Function<TraceForImage, String>() {
		@Override
		public String apply(final TraceForImage input) {
			return input.getChannel();
		}
	};
	private final static Function<ImageForStudy, String> getFileKey = new Function<ImageForStudy, String>() {
		@Override
		public String apply(final ImageForStudy input) {
			return input.getImageFileKey();
		}
	};

	@BeforeClass
	static public void classSetUp() throws Throwable {

		Session s = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory().openSession();
			ManagedSessionContext.bind(s);

			trx = s.beginTransaction();

			BrainTrustDb btDb = new BrainTrustDb(s);

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = Collections.emptyList();
			final List<TsAnnotationEntity> tsAnnotations = Collections
					.emptyList();
			BuildDb.buildDb(patients, readPerm);

			final Patient patient0 = patients.get(0);

			study = getOnlyElement(getOnlyElement(patient0.getAdmissions())
					.getStudies());

			image0 = get(study.getImages(), 0);
			image1 = get(study.getImages(), 1);
			image2 = get(study.getImages(), 2);
			image3 = new ImageEntity();
			image3.setPubId((randomUUID().toString()));
			study.getImages().add(image3);
			image3.setFileKey("Xyz_IEED_data/Xyz_IEED_001/MRI/IM-001-001.jpg");
			image3.setType(ImageType.MRI);

			final List<ContactGroup> electrodes = newArrayList(study
					.getRecording().getContactGroups());

			final ContactGroup electrode0 = electrodes.get(0);
			final Contact contact00 = get(electrode0.getContacts(), 0);

			btDb.saveEverything(
					patients,
					null,
					tsAnnotations,
					null,
					datasets,
					null);

			image0Contact00 = new ImagedContact(new PixelCoordinates(1, 2),
					image0, contact00);

			final ContactGroup electrode1 = get(study.getRecording().getContactGroups(), 1);

			final List<Contact> electrode1Contacts = newArrayList(electrode1
					.getContacts());
			final Contact contact10 = electrode1Contacts.get(0);

			image1Contact10 = new ImagedContact(new PixelCoordinates(3, 4),
					image1, contact10);

			final Contact contact11 = electrode1Contacts.get(1);

			image1Contact11 = new ImagedContact(new PixelCoordinates(5, 6),
					image1, contact11);

			trx.commit();

		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory())
					.close();
		}
	}

	@Test
	public void findByFileKey() throws Throwable {
		Transaction trx = null;
		try {

			final Session s = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(s);

			trx = s.beginTransaction();

			final IDAOFactory daoFac = new HibernateDAOFactory();
			final IImageDAO imageDAO = daoFac.getImageDAO();
			final Set<TraceForImage> tracesForImage0 = imageDAO
					.findByFileKey(image0Contact00.getImage().getFileKey());
			assertEquals(1, tracesForImage0.size());
			final TraceForImage trace0ForImage0 = getOnlyElement(tracesForImage0);
			assertEquals(image0Contact00.getPxCoordinates().getX().intValue(),
					trace0ForImage0.getPixelCoordX());
			assertEquals(image0Contact00.getPxCoordinates().getY().intValue(),
					trace0ForImage0.getPixelCoordY());
			assertEquals(image0Contact00.getContact().getTrace().getLabel(),
					trace0ForImage0.getChannel());

			final Set<TraceForImage> tracesForImage1 = imageDAO
					.findByFileKey(image1Contact11.getImage().getFileKey());
			assertEquals(2, tracesForImage1.size());

			final String contact11Channel = image1Contact11.getContact().getTrace().getLabel();
			final Collection<TraceForImage> tracesForImageContact11 = filter(
					tracesForImage1,
					compose(equalTo(contact11Channel), getChannel));

			assertEquals(1, tracesForImageContact11.size());

			final TraceForImage trace1ForImage1 = getOnlyElement(tracesForImageContact11);

			assertEquals(image1Contact11.getPxCoordinates().getX().intValue(),
					trace1ForImage1.getPixelCoordX());
			assertEquals(image1Contact11.getPxCoordinates().getY().intValue(),
					trace1ForImage1.getPixelCoordY());

			final String contact10Channel = image1Contact10.getContact().getTrace().getLabel();

			final Iterable<TraceForImage> contact10ChannelMatches = Iterables
					.filter(tracesForImage1,
							compose(equalTo(contact10Channel), getChannel));
			assertEquals(1, size(contact10ChannelMatches));

			TraceForImage trace10ForImage1 = getOnlyElement(contact10ChannelMatches);

			assertEquals(image1Contact10.getPxCoordinates().getX().intValue(),
					trace10ForImage1.getPixelCoordX());
			assertEquals(image1Contact10.getPxCoordinates().getY().intValue(),
					trace10ForImage1.getPixelCoordY());

			trx.commit();
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory())
					.close();
		}
	}

	@Test
	public void findByStudyDir() throws Throwable {
		Transaction trx = null;
		try {

			final Session s = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(s);
			trx = s.beginTransaction();

			final IDAOFactory daoFac = new HibernateDAOFactory();
			final IImageDAO imageDAO = daoFac.getImageDAO();
			final Set<ImageForStudy> threeDImagesForStudy = imageDAO
					.findByStudyDir(study.getDir());
			assertEquals(4, threeDImagesForStudy.size());

			final ImageForStudy imageForStudy0 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image0.getFileKey()), getFileKey));
			assertEquals(image0.getType().name(), imageForStudy0.getImageType());
			assertEquals(image0.getType(), imageForStudy0.getImageTypeEnum());

			final ImageForStudy imageForStudy1 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image1.getFileKey()), getFileKey));
			assertEquals(image1.getType().name(), imageForStudy1.getImageType());
			assertEquals(image1.getType(), imageForStudy1.getImageTypeEnum());

			final ImageForStudy imageForStudy2 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image2.getFileKey()), getFileKey));
			assertEquals(image2.getType().name(), imageForStudy2.getImageType());
			assertEquals(image2.getType(), imageForStudy2.getImageTypeEnum());

			final ImageForStudy imageForStudy3 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image3.getFileKey()), getFileKey));
			assertEquals(image3.getType().name(), imageForStudy3.getImageType());
			assertEquals(image3.getType(), imageForStudy3.getImageTypeEnum());

			trx.commit();
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory())
					.close();
		}
	}

	@Test
	public void findByStudyDirAndImageType() throws Throwable {
		Transaction trx = null;
		try {

			final Session s = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(s);
			trx = s.beginTransaction();

			final IDAOFactory daoFac = new HibernateDAOFactory();
			final IImageDAO imageDAO = daoFac.getImageDAO();
			final Set<ImageForStudy> threeDImagesForStudy = imageDAO
					.findByStudyDirAndImageType(study.getDir(),
							ImageType.THREE_D_RENDERING);
			assertEquals(3, threeDImagesForStudy.size());

			final ImageForStudy imageForStudy0 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image0.getFileKey()), getFileKey));
			assertEquals(image0.getType().name(), imageForStudy0.getImageType());
			assertEquals(image0.getType(), imageForStudy0.getImageTypeEnum());

			final ImageForStudy imageForStudy1 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image1.getFileKey()), getFileKey));
			assertEquals(image1.getType().name(), imageForStudy1.getImageType());
			assertEquals(image1.getType(), imageForStudy1.getImageTypeEnum());

			final ImageForStudy imageForStudy2 = Iterables.find(
					threeDImagesForStudy,
					compose(equalTo(image2.getFileKey()), getFileKey));
			assertEquals(image2.getType().name(), imageForStudy2.getImageType());
			assertEquals(image2.getType(), imageForStudy2.getImageTypeEnum());

			final Set<ImageForStudy> ctImagesForStudy = imageDAO
					.findByStudyDirAndImageType(study.getDir(), ImageType.CT);
			assertTrue(ctImagesForStudy.isEmpty());

			final Set<ImageForStudy> mriImagesForStudy = imageDAO
					.findByStudyDirAndImageType(study.getDir(), ImageType.MRI);
			assertEquals(1, mriImagesForStudy.size());

			final ImageForStudy imageForStudy3 = Iterables.find(
					mriImagesForStudy,
					compose(equalTo(image3.getFileKey()), getFileKey));
			assertEquals(image3.getType().name(), imageForStudy3.getImageType());
			assertEquals(image3.getType(), imageForStudy3.getImageTypeEnum());

			trx.commit();
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		} finally {
			ManagedSessionContext.unbind(HibernateUtil.getSessionFactory())
					.close();

		}
	}
}
