/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.SeizureType;

public class SeizureTypeTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (SeizureType szType : SeizureType.values()) {
			switch (szType) {
				case ABSENCE:
					assertEquals("db enums must not change order!", 0,
							szType.ordinal());
					break;
				case TONIC:
					assertEquals("db enums must not change order!", 1,
							szType.ordinal());
					break;
				case LONIC:
					assertEquals("db enums must not change order!", 2,
							szType.ordinal());
					break;
				case MYOCLONIC:
					assertEquals("db enums must not change order!", 3,
							szType.ordinal());
					break;
				case ATONIC:
					assertEquals("db enums must not change order!", 4,
							szType.ordinal());
					break;
				case TONIC_CLONIC:
					assertEquals("db enums must not change order!", 5,
							szType.ordinal());
					break;
				case SIMPLE:
					assertEquals("db enums must not change order!", 6,
							szType.ordinal());
					break;
				case COMPLEX:
					assertEquals("db enums must not change order!", 7,
							szType.ordinal());
					break;
				case PARTIAL_WITH_2NDARY_GENERALIZATION:
					assertEquals("db enums must not change order!", 8,
							szType.ordinal());
					break;
				case NON_EPILEPTIC:
					assertEquals("db enums must not change order!", 9,
							szType.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + szType, true);
					break;
			}
		}
	}

	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (SeizureType szType : SeizureType.values()) {
			switch (szType) {
				case ABSENCE:
					assertEquals("db enums must not change name!", "ABSENCE",
							szType.name());
					break;
				case TONIC:
					assertEquals("db enums must not change name!", "TONIC",
							szType.name());
					break;
				case LONIC:
					assertEquals("db enums must not change name!", "LONIC",
							szType.name());
					break;
				case MYOCLONIC:
					assertEquals("db enums must not change name!",
							"MYOCLONIC",
							szType.name());
					break;
				case ATONIC:
					assertEquals("db enums must not change name!", "ATONIC",
							szType.name());
					break;
				case TONIC_CLONIC:
					assertEquals("db enums must not change name!",
							"TONIC_CLONIC",
							szType.name());
					break;
				case SIMPLE:
					assertEquals("db enums must not change name!", "SIMPLE",
							szType.name());
					break;
				case COMPLEX:
					assertEquals("db enums must not change name!", "COMPLEX",
							szType.name());
					break;
				case PARTIAL_WITH_2NDARY_GENERALIZATION:
					assertEquals("db enums must not change name!",
							"PARTIAL_WITH_2NDARY_GENERALIZATION",
							szType.name());
					break;
				case NON_EPILEPTIC:
					assertEquals("db enums must not change name!",
							"NON_EPILEPTIC",
							szType.name());
					break;
				default:
					assertFalse("unknown enum: " + szType, true);
					break;
			}
		}
	}
}
