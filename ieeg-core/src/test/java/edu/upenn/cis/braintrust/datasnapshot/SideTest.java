/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Side;

public class SideTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (Side side : Side.values()) {
			switch (side) {
				case LEFT:
					assertEquals("db enums must not change order!", 0,
							side.ordinal());
					break;
				case RIGHT:
					assertEquals("db enums must not change order!", 1,
							side.ordinal());
					break;
				case UNKNOWN:
					assertEquals("db enums must not change order!", 2,
							side.ordinal());
					break;
				case NA:
					assertEquals("db enums must not change order!", 3,
							side.ordinal());
					break;
				default:
					assertFalse("unknown enum: " + side, true);
					break;
			}
		}
	}
}
