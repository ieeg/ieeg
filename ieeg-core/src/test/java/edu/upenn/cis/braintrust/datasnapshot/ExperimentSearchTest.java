/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singleton;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.DrugAdminRelationship;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.OrganizationsForTests;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author Sam Donnelly
 */
public class ExperimentSearchTest {

	private TstObjectFactory tstFac = new TstObjectFactory(false);

	private User user;
	private SpeciesEntity species0;
	private DrugEntity drug0;
	private DrugEntity drug1;
	private StimTypeEntity stimType0;
	private StimRegionEntity stimRegion0;

	private static DataSnapshotServer getDataSnapshotServer() {
		return new DataSnapshotServer(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration(),
				new DataSnapshotServiceFactory(),
				100,
				new HibernateDAOFactory());
	}
	
	@Before
	public void setUp() throws Exception {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();

			user = tstFac.newUserRegularEnabled();
			species0 = tstFac.newSpecies();
			drug0 = tstFac.newDrug();
			drug1 = tstFac.newDrug();
			stimType0 = tstFac.newStimType();
			stimRegion0 = tstFac
					.newStimRegion();
			// Setup IvProps
			IvProps.setIvProps(Collections.singletonMap(IvProps.MAX_DS_TS_ANNS,
					Integer.toString(IvProps.MAX_DS_TS_ANNS_DFLT)));
			
			IUserService userService = Mockito.mock(IUserService.class);
			UserServiceFactory.setUserService(userService);

			StrainEntity strain = tstFac.newStrain(species0);
			sess.saveOrUpdate(species0);

			AnimalEntity animal = tstFac.newAnimal(strain);
			animal.setOrganization(OrganizationsForTests.SPIKE);
			sess.saveOrUpdate(animal);

			IPermissionDAO permDAO = new PermissionDAOHibernate(sess);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			ExperimentEntity experiment = tstFac.newExperiment(
					animal,
					new ExtAclEntity(singleton(readPerm)));
			sess.saveOrUpdate(experiment);

			sess.saveOrUpdate(drug0);
			sess.saveOrUpdate(drug1);

			DrugAdminRelationship drugAdmin0 =
					tstFac.newDrugAdmin(experiment, drug0);
			DrugAdminRelationship drugAdmin1 =
					tstFac.newDrugAdmin(experiment, drug1);
			sess.saveOrUpdate(drugAdmin0);
			sess.saveOrUpdate(drugAdmin1);

			experiment.setStimRegion(stimRegion0);
			experiment.setStimType(stimType0);

			sess.saveOrUpdate(stimRegion0);
			sess.saveOrUpdate(stimType0);

			SpeciesEntity species1 = tstFac.newSpecies();

			StrainEntity strain1 = tstFac.newStrain(species1);
			sess.saveOrUpdate(species1);
			sess.saveOrUpdate(strain1);

			AnimalEntity animal1 = tstFac.newAnimal(strain1);
			animal1.setOrganization(OrganizationsForTests.JONES);
			sess.saveOrUpdate(animal1);

			ExperimentEntity experiment1 = tstFac.newExperiment(
					animal1,
					new ExtAclEntity(singleton(readPerm)));
			sess.saveOrUpdate(experiment1);

			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void getAll() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));
		assertNotNull(results);
		assertEquals(2, results.size());
	}

	@Test
	public void species() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						newHashSet(species0.getId()),
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertNotNull(results1);
		assertEquals(1, results1.size());

		Set<DataSnapshotSearchResult> results2 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						newHashSet(species0.getId(), 532352L),
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertNotNull(results2);
		assertEquals(1, results2.size());
	}

	@Test
	public void drugs() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						newHashSet(drug0.getId()),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertNotNull(results);
		assertEquals(1, results.size());

		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						newHashSet(drug0.getId(), 583758L),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertNotNull(results1);
		assertEquals(0, results1.size());

		Set<DataSnapshotSearchResult> results2 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						newHashSet(drug0.getId(), drug1.getId()),
						new HashSet<Long>(),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertNotNull(results2);
		assertEquals(1, results2.size());
	}

	@Test
	public void stimRegion() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						new HashSet<Long>(),
						newHashSet(stimRegion0.getId()),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertEquals(1, results.size());

		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						new HashSet<Long>(),
						newHashSet(stimRegion0.getId(), 35532632L),
						new HashSet<Long>(),
						Collections.<String> emptySet()));

		assertEquals(1, results1.size());

	}

	@Test
	public void stimType() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						newHashSet(stimType0.getId()),
						Collections.<String> emptySet()));

		assertEquals(1, results.size());

		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						newHashSet(stimType0.getId(), 35532632L),
						Collections.<String> emptySet()));

		assertEquals(1, results1.size());

	}

	@Test
	public void organizations() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						newHashSet(OrganizationsForTests.SPIKE.getCode())));

		assertNotNull(results1);
		assertEquals(1, results1.size());
		
		Set<DataSnapshotSearchResult> results2 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						Collections.<Long> emptySet(),
						newHashSet(OrganizationsForTests.JONES.getCode())));

		assertNotNull(results2);
		assertEquals(1, results2.size());
	}

	@Test
	public void all() {
		DataSnapshotServer dsServer = getDataSnapshotServer();
		Set<DataSnapshotSearchResult> results1 = dsServer.getExperiments(
				user,
				new ExperimentSearch(
						newHashSet(species0.getId()),
						newHashSet(drug0.getId(), drug1.getId()),
						newHashSet(stimRegion0.getId()),
						newHashSet(stimType0.getId()),
						Collections.<String> emptySet()));

		assertNotNull(results1);
		assertEquals(1, results1.size());
	}
}
