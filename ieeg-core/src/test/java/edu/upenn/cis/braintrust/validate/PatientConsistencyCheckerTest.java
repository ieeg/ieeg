/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.validate;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Tests for the patient consistency checker.
 * 
 * @author John Frommeyer
 */
public class PatientConsistencyCheckerTest {

	private Patient patient;

	@Before
	public void initPatient() {
		final List<Patient> patients = newArrayList();
		TstObjectFactory tstObjectFactory = new TstObjectFactory(false);
		PermissionEntity readPerm = tstObjectFactory.getReadPerm();
		BuildDb.buildDb(
				patients,
				readPerm);
		patient = patients.get(0);
	}

	@Test
	public void checkPatientTest() {
		final EegStudy studyBeforeCheck = getOnlyElement(getOnlyElement(
				patient.getAdmissions()).getStudies());
		boolean badFileKey = false;
		for (final TimeSeriesEntity ts : studyBeforeCheck.getTimeSeries()) {
			final String expectedFileKey = studyBeforeCheck.getDir() + "/"
					+ studyBeforeCheck.getMefDir() + "/" + ts.getLabel()
					+ ".mef";
			if (!expectedFileKey.equals(ts.getFileKey())) {
				badFileKey = true;
				break;
			}
		}
		assertTrue(badFileKey);
		PatientConsistencyChecker checker = new PatientConsistencyChecker();
		checker.checkPatient(patient);
		final EegStudy studyAfterCheck = getOnlyElement(getOnlyElement(
				patient.getAdmissions()).getStudies());
		for (final TimeSeriesEntity ts : studyAfterCheck.getTimeSeries()) {
			final String expectedFileKey = studyAfterCheck.getDir() + "/"
					+ studyAfterCheck.getMefDir() + "/" + ts.getLabel()
					+ ".mef";
			assertEquals(expectedFileKey, ts.getFileKey());
		}
	}
}
