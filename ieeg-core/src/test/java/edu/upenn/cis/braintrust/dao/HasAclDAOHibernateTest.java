/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertNotNull;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.permissions.IHasAclDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.HasAclDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author John Frommeyer
 * 
 */
public class HasAclDAOHibernateTest {
	private final TstObjectFactory objFac = new TstObjectFactory(true);
	private String studyId;
	private final SessionFactory sessFac = HibernateUtil.getSessionFactory();

	@Before
	public void setUp() throws Throwable {
		SessionAndTrx sessAndTrx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			final Patient patient = objFac.newPatient();
			final HospitalAdmission admission = objFac.newHospitalAdmission();
			patient.addAdmission(admission);

			final Recording recording = objFac.newRecording();
			final ExtAclEntity acl = new ExtAclEntity();
			final EegStudy study = new EegStudy(
					newUuid(),
					acl,
					TstObjectFactory.randomEnum(EegStudyType.class),
					0,
					recording,
					null);
			admission.addStudy(study);
			studyId = study.getPubId();

			session.saveOrUpdate(patient);
			PersistenceUtil.commit(sessAndTrx);
		} catch (Throwable t) {
			PersistenceUtil.rollback(sessAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}
	}

	@Test
	public void findByNaturalId() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			IHasAclDAO hasAclDao = new HasAclDAOHibernate(session);
			final IHasExtAcl actualStudy = hasAclDao.findByNaturalId(
					HasAclType.DATA_SNAPSHOT,
					studyId);
			assertNotNull(actualStudy);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}
}
