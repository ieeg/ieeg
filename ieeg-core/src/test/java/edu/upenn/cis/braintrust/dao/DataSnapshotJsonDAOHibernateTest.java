/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotJsonDAOHibernate;
import edu.upenn.cis.braintrust.model.DataSnapshotJson;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.StringKeyValue;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.IUser;

public class DataSnapshotJsonDAOHibernateTest {
	ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();
	
	private static Logger logger = LoggerFactory
			.getLogger(DatasetDAOHibernateTest.class);
	private static DataSnapshotJson clob;
	private static Dataset dataset;
	private static User authedUser;
	private static TstUsers users;
	
//	/**
//	 * Dummy class for testing serialization
//	 * @author zives
//	 *
//	 */
//	public static class SimpleValue implements JsonTyped {
//		String id;
//		
//		public SimpleValue() {
//			
//		}
//		
//		public SimpleValue(String id) {
//			this.id = id;
//		}
//
//		public String getId() {
//			return id;
//		}
//		
//		public void setId(String id) {
//			this.id = id;
//		}
//		
//		public String toString() {
//			return getId();
//		}
//		
//	}

	@BeforeClass
	public static void classSetUp() throws Throwable {

		final Set<Role> authedUsersRoles = Collections.emptySet();
		authedUser = new User(
				new UserId(0),
				"authedUser",
				"authedPasswd",
				IUser.Status.ENABLED,
				authedUsersRoles,
				null,
				"email@nowhere.com",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			Session s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();
			final BrainTrustDb db = new BrainTrustDb(s);

			users = new TstUsers(s);

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = newArrayList();
			final List<TsAnnotationEntity> tsAnnotations = newArrayList();
			final List<SnapshotUsage> usages = newArrayList();

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permissionDAO.findStarCoreReadPerm();
			DataSnapshotJsonDAOHibernate dscDAO = new DataSnapshotJsonDAOHibernate(s);

			BuildDb.buildDb(patients, readPerm);

			// Create dataset
			dataset = new Dataset(
					"My dataset",
					users.btRegUser1,
					SecurityUtil.createUserOwnedWorldReadableAcl(
							users.btRegUser1,
							permissionDAO));
			
			clob = new DataSnapshotJson("", dataset);
			
			datasets.add(dataset);
			
			StringKeyValue t = new StringKeyValue("one", "two");
			StringKeyValue t2 = new StringKeyValue("two", "three");
			
			JsonKeyValueSet tset = new JsonKeyValueSet();
			
			tset.add(t);
			tset.add(t2);

			db.saveEverything(patients, null,
					tsAnnotations,
					null, datasets, usages);
			dscDAO.setKeyValues(dataset.getPubId(), tset);
			trx.commit();
			s.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}
	
	public String getMappedTriple(IJsonKeyValue t) throws JsonProcessingException {
		return mapper.writeValueAsString(t);
	}
	
	public String getMappedTripleSet(JsonKeyValueSet t) throws JsonProcessingException {
		return mapper.writeValueAsString(t);
	}

	@Test
	public void testTriple() throws Throwable {
		StringKeyValue t = new StringKeyValue("one", "two");
		
		System.out.println(getMappedTriple(t));
	}
	
	@Test
	public void testTripleSet() throws Throwable {
		StringKeyValue t = new StringKeyValue("one", "two");
		StringKeyValue t2 = new StringKeyValue("two", "three");
		
		JsonKeyValueSet tset = new JsonKeyValueSet();
		
		tset.add(t);
		tset.add(t2);
		
		String ret = getMappedTripleSet(tset);
		System.out.println(ret);
	}

	@Test
	public void testTripleRoundTrip() throws Throwable {
		StringKeyValue t = new StringKeyValue("one", "two");
		
		String str = getMappedTriple(t);
		
		IJsonKeyValue t2 = mapper.readValue(str, IJsonKeyValue.class);
	}

	/**
	 * Round-trip the stirng for a triple set
	 * 
	 * @throws Throwable
	 */
	@Test
	public void testTripleSetRoundTrip() throws Throwable {
		StringKeyValue t = new StringKeyValue("one", "two");
		StringKeyValue t2 = new StringKeyValue("two", "three");
		
		JsonKeyValueSet tset = new JsonKeyValueSet();
		
		tset.add(t);
		tset.add(t2);
		
		String str = getMappedTripleSet(tset);
		
		System.out.println("KeyValue set: " + str);
		
		JsonKeyValueSet tset2 = mapper.readValue(str, JsonKeyValueSet.class);
		
		int matches = 0;
		for (IJsonKeyValue tm: tset2) {
			if (tm.getKey().equals("one") || tm.getKey().equals("two"))
				matches++;
			
		}
		assertEquals(matches, 2);
	}
	
	@Test
	public void testWriteTriple() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			DataSnapshotJsonDAOHibernate dscDAO = new DataSnapshotJsonDAOHibernate(s);
			
			dscDAO.addKeyValue(dataset.getPubId(), new StringKeyValue("three", "four"));

			Set<IJsonKeyValue> triples = dscDAO.getAllKeyValues(dataset.getPubId());
			
			boolean found = false;
			for (IJsonKeyValue t : triples)
				if (t.getKey().equals("three"))
					found = true;
			System.out.println("Triples: " + triples);
			trx.commit();
			assertEquals(found, true);
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}
}
