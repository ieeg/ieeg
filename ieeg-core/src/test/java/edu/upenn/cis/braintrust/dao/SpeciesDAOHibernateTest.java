/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;

import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class SpeciesDAOHibernateTest {
	private final TstObjectFactory tstFac = new TstObjectFactory(false);

	@Before
	public void setUp() throws Exception {
		Session s = null;
		Transaction t = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			s = HibernateUtil.getSessionFactory().openSession();
			t = s.beginTransaction();

			SpeciesEntity species1 = tstFac.newSpecies();
			tstFac.newStrain(species1);
			s.saveOrUpdate(species1);

			SpeciesEntity species2 = tstFac.newSpecies();
			tstFac.newStrain(species2);
			tstFac.newStrain(species2);
			s.saveOrUpdate(species2);

			SpeciesEntity species3 = tstFac.newSpecies();
			tstFac.newStrain(species3);
			tstFac.newStrain(species3);
			tstFac.newStrain(species3);
			s.saveOrUpdate(species3);

			t.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(t);
			throw e;
		} finally {
			PersistenceUtil.close(s);
		}
	}

}
