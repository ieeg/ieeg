/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust;

import java.util.Collections;

import org.hibernate.Session;

import com.google.common.collect.ImmutableMap;

import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.identity.hibernate.UserDAOHibernate;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;

/**
 * For testing, get some users in the db. The id's are a superset of what's in
 * drupal-import.sql.
 * 
 * @author Sam Donnelly
 */
public class TstUsers {

	private IUserDAO btUserDAO;

	public final UserEntity btRegUser1;
	public final User regUser1;

	public final UserEntity btRegUser2;
	public final User regUser2;

	public final UserEntity btAdminUser;
	public final User adminUser;

	public final ImmutableMap<UserId, UserEntity> idsToUsers;

	public TstUsers() {
		btRegUser1 = buildBtRegularUser1();
		regUser1 = buildRegularUser1();

		btRegUser2 = buildBtRegularUser2();
		regUser2 = buildRegularUser2();

		btAdminUser = buildBtAdminUser();
		adminUser = buildAdminUser();

		idsToUsers = new ImmutableMap.Builder<UserId, UserEntity>()
				.put(btAdminUser.getId(), btAdminUser)
				.put(btRegUser1.getId(), btRegUser1)
				.put(btRegUser2.getId(), btRegUser2)
				.build();
	}

	public TstUsers(final Session session) {
		btUserDAO = new UserDAOHibernate(session);

		btRegUser1 = buildBtRegularUser1();
		regUser1 = buildRegularUser1();

		btRegUser2 = buildBtRegularUser2();
		regUser2 = buildRegularUser2();

		btAdminUser = buildBtAdminUser();
		adminUser = buildAdminUser();

		idsToUsers = new ImmutableMap.Builder<UserId, UserEntity>()
				.put(btAdminUser.getId(), btAdminUser)
				.put(btRegUser1.getId(), btRegUser1)
				.put(btRegUser2.getId(), btRegUser2)
				.build();
	}

	private User buildAdminUser() {
		User user = new User(
				new UserId(1),
				"admin user",
				"xyz&123",
				User.Status.ENABLED,
				Collections.singleton(Role.ADMIN),
				null,
				"email@nowhere.com",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
		return user;
	}

	private User buildRegularUser1() {
		User user = new User(new UserId(3), "reg user 1", "123xyz?",
				User.Status.ENABLED,
				Collections.singleton(Role.USER),
				null,
				"email@nowhere.com",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
		return user;
	}

	private User buildRegularUser2() {
		User user = new User(new UserId(4), "reg user 2", "123xyz?",
				User.Status.ENABLED,
				Collections.singleton(Role.USER),
				null,
				"email@nowhere.com",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
		return user;
	}

	private UserEntity buildBtAdminUser() {

		final UserEntity btUser = new UserEntity(new UserId(1));

		if (btUserDAO != null) {
			btUserDAO.saveOrUpdate(btUser);
		}

		return btUser;
	}

	private UserEntity buildBtRegularUser1() {

		UserEntity user = new UserEntity(new UserId(3));

		if (btUserDAO != null) {
			btUserDAO.saveOrUpdate(user);
		}

		return user;
	}

	private UserEntity buildBtRegularUser2() {
		final UserEntity btUser = new UserEntity(new UserId(4));

		if (btUserDAO != null) {
			btUserDAO.saveOrUpdate(btUser);
		}

		return btUser;
	}
}
