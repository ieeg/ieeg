/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * @author Sam Donnelly
 * 
 */
public class StrainTest {

	private TstObjectFactory tstObjFac = new TstObjectFactory(false);

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		final TestDbCleaner cleaner = new TestDbCleaner(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration());
		cleaner.deleteEverything();
	}

	// there was a liquibase script bug
	@Test
	public void labelsNotUniqueAcrossSpecies() throws Exception {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			SpeciesEntity species = tstObjFac.newSpecies();
			StrainEntity strain = tstObjFac.newStrain(species);

			SpeciesEntity species1 = tstObjFac.newSpecies();
			StrainEntity strain1 = tstObjFac.newStrain(species1);
			strain1.setLabel(strain.getLabel());
			sess.save(species);
			sess.save(species1);
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	// there was a liquibase script bug
	@Test
	public void labelsUniqueWithinSpecies() throws Exception {
		thrown.expect(ConstraintViolationException.class);
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			SpeciesEntity species = tstObjFac.newSpecies();
			StrainEntity strain = tstObjFac.newStrain(species);
			StrainEntity strain1 = tstObjFac.newStrain(species);
			strain1.setLabel(strain.getLabel());
			sess.save(species);
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
}
