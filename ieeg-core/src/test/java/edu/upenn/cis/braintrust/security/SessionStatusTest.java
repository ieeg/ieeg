/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

import org.junit.Test;

public class SessionStatusTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (SessionStatus status : SessionStatus.values()) {
			switch (status) {
				case ACCOUNT_DISABLED:
					assertEquals("db enums must not change names!",
							"ACCOUNT_DISABLED",
							status.name());
					break;
				case ACTIVE:
					assertEquals("db enums must not change names!",
							"ACTIVE",
							status.name());
					break;
				case EXPIRED:
					assertEquals("db enums must not change names!",
							"EXPIRED",
							status.name());
					break;
				case LOGGED_OUT:
					assertEquals("db enums must not change names!",
							"LOGGED_OUT",
							status.name());
					break;
				default:
					assertFalse("unknown SessionStatus: " + status, true);
					break;
			}
		}
	}
}
