/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.datasnapshot.assembler.DataSnapshotAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.ImageAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TsAnnotationAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TimeSeriesAssembler;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;

/**
 * For tests which need to covert a {@code DataSnapshotEntity} and a collection
 * of {@code TsAnnotationEntity} into a single {@code DataSnapshot}.
 * 
 * @author John Frommeyer
 * 
 */
public class TstDataSnapshotAssembler {

	/**
	 * Returns a {@code DataSnapshot} version of {@code dsEntity} which has had
	 * its {@code Image}, {@code TimeSeries}, and {@code TimeSeriesAnnotation}
	 * collections assembled as well. Returns null if {@code dsEntity} is null.
	 * 
	 * @param dsEntity
	 * @param annEntities
	 * @return a {@code DataSnapshot} version of {@code dsEntity} which has had
	 *         its {@code Image}, {@code TimeSeries}, and
	 *         {@code TimeSeriesAnnotation} collections assembled as well.
	 *         Returns null if {@code dsEntity} is null.
	 */
	public static DataSnapshot assembleDeepForTests(
			@Nullable final DataSnapshotEntity dsEntity,
			final Set<TsAnnotationEntity> annEntities) {
		if (dsEntity == null) {
			return null;
		}
		DataSnapshot ds = DataSnapshotAssembler.assembleWNoChildren(dsEntity);
		if (ds != null) {
			// Images
			final Set<Image> images = ds.getImages();
			for (ImageEntity iEntity : dsEntity.getImages()) {
				images.add(ImageAssembler.assemble(iEntity));
			}
			// Annotations
			final List<TsAnnotationDto> annotations = ds
					.getTsAnnotations();
			for (TsAnnotationEntity annEntity : annEntities) {
				annotations
						.add(TsAnnotationAssembler.assemble(annEntity));
			}

			// Time Series
			final Set<TimeSeriesDto> timeSeries = ds.getTimeSeries();
			for (TimeSeriesEntity tsEntity : dsEntity.getTimeSeries()) {
				timeSeries.add(TimeSeriesAssembler.assemble(tsEntity));
			}
		}
		return ds;
	}

}
