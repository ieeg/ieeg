/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertSame;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class ElectrodeTest {

	private TstObjectFactory objFac = new TstObjectFactory(false);

	@Test
	public void addContact() {
		final ContactGroup ai = objFac.newContactGroup();
		final Contact contact = new Contact(
				TstObjectFactory.randomEnum(ContactType.class),
				objFac.newMriCoordinates(),
				objFac.newTalairachCoordinates(),
				objFac.newTimeSeries());
		ai.addContact(contact);
		assertSame(contact, getOnlyElement(ai.getContacts()));
		assertSame(ai, contact.getParent());

	}
}
