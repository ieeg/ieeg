/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.ImageType;

public class ImageTypeTest {
	/**
	 * The db depends on thee staying the same.
	 */
	@Test
	public void checkOrdinalValues() {
		for (ImageType dbEnum : ImageType.values()) {
			switch (dbEnum) {
				case MRI:
					assertEquals("db enums must not change order!", 0,
							dbEnum.ordinal());
					break;
				case FMRI:
					assertEquals("db enums must not change order!", 1,
							dbEnum.ordinal());
					break;
				case MRS:
					assertEquals("db enums must not change order!", 2,
							dbEnum.ordinal());
					break;
				case ICTAL_SPECT:
					assertEquals("db enums must not change order!", 3,
							dbEnum.ordinal());
					break;
				case CT:
					assertEquals("db enums must not change order!", 4,
							dbEnum.ordinal());
					break;
				case PET:
					assertEquals("db enums must not change order!", 5,
							dbEnum.ordinal());
					break;
				case MEG:
					assertEquals("db enums must not change order!", 6,
							dbEnum.ordinal());
					break;
				case DIGITAL_PICTURES:
					assertEquals("db enums must not change order!", 7,
							dbEnum.ordinal());
					break;
				case ELECTRODE_MAP:
					assertEquals("db enums must not change order!", 8,
							dbEnum.ordinal());
					break;
				case THREE_D_RENDERING:
					assertEquals("db enums must not change order!", 9,
							dbEnum.ordinal());
					break;
				default:
					fail("unknown db enum: " + dbEnum);
					break;
			}
		}
	}
}
