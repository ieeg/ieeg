/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Etiology;

public class EtiologyTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (Etiology etiology : Etiology.values()) {
			switch (etiology) {
				case GENETIC:
					assertEquals(
							"db enums must not change order!",
							"GENETIC",
							etiology.name());
					break;
				case STRUCTURAL_METABOLIC:
					assertEquals(
							"db enums must not change order!",
							"STRUCTURAL_METABOLIC",
							etiology.name());
					break;
				case UNKNOWN:
					assertEquals(
							"db enums must not change order!",
							"UNKNOWN",
							etiology.name());
					break;
				default:
					assertFalse("unknown Etiology: " + etiology, true);
					break;
			}
		}
	}
}
