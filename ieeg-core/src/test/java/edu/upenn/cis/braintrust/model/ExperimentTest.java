/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class ExperimentTest {

	private final TstObjectFactory objFac = new TstObjectFactory(false);
	private AnimalEntity animal;
	private StrainEntity strain1;
	private StrainEntity strain2;
	private StimRegionEntity stimRegion;
	private StimTypeEntity stimType;
	private PermissionEntity readPerm;

	@Before
	public void setUp() throws Exception {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			strain1 = objFac.newStrain(objFac.newSpecies());
			sess.saveOrUpdate(strain1.getParent());

			strain2 = objFac.newStrain(objFac.newSpecies());
			sess.saveOrUpdate(strain2.getParent());
			animal = objFac.newAnimal(strain1);
			sess.saveOrUpdate(animal);

			stimRegion = objFac.newStimRegion();
			sess.save(stimRegion);
			stimType = objFac.newStimType();
			sess.save(stimType);

			readPerm = new PermissionDAOHibernate(sess).findStarCoreReadPerm();

			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void testSave() throws Exception {
		final ExperimentEntity expected1 = objFac.newExperiment(
				animal,
				new ExtAclEntity(singleton(readPerm)));
		expected1.setStimRegion(stimRegion);
		expected1.setStimType(stimType);
		expected1.getRecording().setRefElectrodeDescription(newUuid());

		Session sess = null;
		Transaction trx = null;
		Long expected1Id = null;
		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			sess.saveOrUpdate(expected1);
			expected1Id = expected1.getId();
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}

		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			ExperimentEntity actual = (ExperimentEntity) sess.get(
					ExperimentEntity.class,
					expected1Id);
			assertNotNull(actual);

			assertEquals(expected1.getLabel(), actual.getLabel());
			assertEquals(expected1.getPubId(), actual.getPubId());
			assertEquals(expected1.getParent().getLabel(), actual.getParent()
					.getLabel());
			assertEquals(expected1.getStimRegion().getLabel(), actual
					.getStimRegion().getLabel());
			assertEquals(expected1.getStimType().getLabel(), actual
					.getStimType().getLabel());
			assertEquals(expected1.getStimDurationMs(),
					actual.getStimDurationMs());
			assertEquals(expected1.getStimIsiMs(),
					actual.getStimIsiMs());
			assertEquals(expected1.getRecording().getRefElectrodeDescription(),
					actual.getRecording()
							.getRefElectrodeDescription());
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}

		// Add a second experiment to the animal
		final ExperimentEntity expected2 =
				objFac.newExperiment(
						animal,
						new ExtAclEntity(singleton(readPerm)));
		expected2.setStimRegion(stimRegion);
		expected2.setStimType(stimType);
		expected2.getRecording().setRefElectrodeDescription(newUuid());

		Session sess2 = null;
		Transaction trx2 = null;
		Long expected2Id = null;
		try {
			sess2 =
					HibernateUtil.getSessionFactory().openSession();

			trx2 = sess2.beginTransaction();

			sess2.saveOrUpdate(expected2);
			expected2Id = expected2.getId();
			trx2.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx2);
			throw e;
		} finally {
			PersistenceUtil.close(sess2);
		}

		try {
			sess2 =
					HibernateUtil.getSessionFactory().openSession();

			trx2 = sess2.beginTransaction();
			AnimalEntity actualAnimal = (AnimalEntity) sess2.get(
					AnimalEntity.class,
					animal.getId());
			assertEquals(2, actualAnimal.getExperiments().size());
			ExperimentEntity actual2 = (ExperimentEntity) sess2.get(
					ExperimentEntity.class,
					expected2Id);
			assertNotNull(actual2);

			assertEquals(expected2.getLabel(), actual2.getLabel());
			assertEquals(expected2.getPubId(), actual2.getPubId());
			assertEquals(expected2.getParent().getLabel(), actual2.getParent()
					.getLabel());
			assertEquals(expected2.getStimRegion().getLabel(), actual2
					.getStimRegion().getLabel());
			assertEquals(expected2.getStimType().getLabel(), actual2
					.getStimType().getLabel());
			assertEquals(expected2.getStimDurationMs(),
					actual2.getStimDurationMs());
			assertEquals(expected2.getStimIsiMs(),
					actual2.getStimIsiMs());
			assertEquals(expected2.getRecording().getRefElectrodeDescription(),
					actual2.getRecording()
							.getRefElectrodeDescription());
			trx2.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx2);
			throw e;
		} finally {
			PersistenceUtil.close(sess2);
		}
	}
}
