/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.junit.Test;

import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.shared.IUser;

public class BrainTrustRealmTest {
	@SuppressWarnings("unchecked")
	@Test
	public void doGetAuthenticationInfo() {

		User user = new User(
				new UserId(1),
				"test-user",
				"test-password",
				IUser.Status.ENABLED,
				Collections.<Role> emptySet(),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null,
						null, null, null, null, null, null));

		IUserService userService = mock(IUserService.class);

		when(userService.findUserByUsername(anyString())).thenReturn(user);

		BrainTrustRealm btRealm = new BrainTrustRealm(userService);

		UsernamePasswordToken upToken = new UsernamePasswordToken();

		AuthenticationInfo authcInfo = btRealm.doGetAuthenticationInfo(upToken);
		assertEquals(
				user.getUserId(),
				getOnlyElement(authcInfo.getPrincipals().fromRealm(
						BrainTrustRealm.class.getSimpleName())));
		assertEquals(user.getPassword(),
				authcInfo.getCredentials());
		assertEquals(user.getUserId(),
				(UserId) getOnlyElement(authcInfo.getPrincipals()));

	}

	@Test(expected = DisabledAccountException.class)
	public void doGetAuthenticationInfoDisabled() {
		User user = new User(
				new UserId(1),
				"test-user",
				"test-password",
				IUser.Status.DISABLED,
				new HashSet<Role>(),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

		IUserService userService = mock(IUserService.class);

		when(userService.findUserByUsername(anyString())).thenReturn(user);

		BrainTrustRealm btRealm = new BrainTrustRealm(userService);

		UsernamePasswordToken upToken = new UsernamePasswordToken();

		btRealm.doGetAuthenticationInfo(upToken);

	}

	@Test
	public void doGetAuthorizationInfoAdmin() {

		User user = new User(
				new UserId(1),
				"test-user",
				"test-password",
				IUser.Status.ENABLED,
				newHashSet(Role.ADMIN),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

		IUserService userService = mock(IUserService.class);

		when(userService.findUserByUid(any(UserId.class))).thenReturn(user);

		BrainTrustRealm btRealm = new BrainTrustRealm(userService);

		PrincipalCollection pCollection =
				new SimplePrincipalCollection(new UserId(1),
						BrainTrustRealm.class.getSimpleName());

		AuthorizationInfo authzInfo = btRealm
				.doGetAuthorizationInfo(pCollection);

		assertEquals(1, authzInfo.getRoles().size());
		assertEquals(Role.ADMIN.getName(), getOnlyElement(authzInfo.getRoles()));
		assertEquals(1, authzInfo.getStringPermissions().size());
		assertEquals("*", getOnlyElement(authzInfo.getStringPermissions()));
	}

	@Test
	public void doGetAuthorizationInfoUser() {

		User user = new User(
				new UserId(1),
				"test-user",
				"test-password",
				IUser.Status.ENABLED,
				newHashSet(Role.USER),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

		IUserService userService = mock(IUserService.class);

		when(userService.findUserByUid(any(UserId.class))).thenReturn(user);

		BrainTrustRealm btRealm = new BrainTrustRealm(userService);

		PrincipalCollection pCollection =
				new SimplePrincipalCollection(new UserId(1),
						BrainTrustRealm.class.getSimpleName());

		AuthorizationInfo authzInfo = btRealm
				.doGetAuthorizationInfo(pCollection);

		assertEquals(1, authzInfo.getRoles().size());
		assertEquals(Role.USER.getName(), getOnlyElement(authzInfo.getRoles()));
		assertEquals(1, authzInfo.getStringPermissions().size());
		assertEquals(BrainTrustRealm.USER_PERMISSIONS,
				getOnlyElement(authzInfo.getStringPermissions()));
	}

}
