/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.model.ModeEntity;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;

/**
 * @author John Frommeyer
 * 
 * This test relies on the contents of import.sql
 * 
 */
public class PermissionDAOHibernateTest {

	@Test
	public void findByDomainModeAndName() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			PermissionDAOHibernate permDao = new PermissionDAOHibernate(session);
			final PermissionEntity coreRead = permDao
					.findByDomainModeAndName(
							PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
							CorePermDefs.CORE_MODE_NAME,
							CorePermDefs.READ_PERMISSION_NAME);
			assertNotNull(coreRead);
			assertEquals(CorePermDefs.READ_PERMISSION_NAME, coreRead.getName());

			final ModeEntity actualMode = coreRead.getMode();
			assertNotNull(actualMode);
			assertEquals(CorePermDefs.CORE_MODE_NAME, actualMode.getName());

			final PermissionDomainEntity actualTarget = actualMode.getDomain();
			assertNotNull(actualTarget);
			assertEquals(PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
					actualTarget.getName());
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void findByDomainAndMode() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			PermissionDAOHibernate permDao = new PermissionDAOHibernate(session);
			final List<PermissionEntity> corePerms = permDao
					.findByDomainAndMode(
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME);
			assertNotNull(corePerms);
			assertFalse(corePerms.isEmpty());
			Integer prevDisplayOrder = Integer.MIN_VALUE;
			for (final PermissionEntity actualPerm : corePerms) {
				assertTrue(prevDisplayOrder.compareTo(actualPerm
						.getDisplayOrder()) < 0);
				prevDisplayOrder = actualPerm.getDisplayOrder();
				assertEquals(CorePermDefs.CORE_MODE_NAME, actualPerm
						.getMode().getName());
				final String actualDomainName = actualPerm.getMode()
						.getDomain().getName();
				assertTrue(HasAclType.DATA_SNAPSHOT.getEntityName().equals(
						actualDomainName)
						|| PermissionDomainEntity.PERM_DOMAIN_WILDCARD
								.equals(actualDomainName));
			}

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void findByDtos() throws Throwable {
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			PermissionDAOHibernate permDao = new PermissionDAOHibernate(session);

			final Set<PermissionEntity> permSet = permDao
					.findByDtos(newHashSet(
							CorePermDefs.READ_PERM,
							new ExtPermission(
									"Non-existent domain",
									CorePermDefs.CORE_MODE_NAME,
									"Non-existent perm"),
							CorePermDefs.OWNER_PERM));
			assertNotNull(permSet);
			assertEquals(2, permSet.size());

			// Check for read
			{
				final Optional<PermissionEntity> coreRead = tryFind(
						permSet,
						compose(equalTo(CorePermDefs.READ_PERMISSION_NAME),
								PermissionEntity.getName));
				assertTrue(coreRead.isPresent());

				final ModeEntity actualMode = coreRead.get().getMode();
				assertNotNull(actualMode);
				assertEquals(CorePermDefs.CORE_MODE_NAME,
						actualMode.getName());

				final PermissionDomainEntity actualTarget = actualMode
						.getDomain();
				assertNotNull(actualTarget);
				assertEquals(PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
						actualTarget.getName());
			}
			// Check for own
			{
				final Optional<PermissionEntity> coreOwner = tryFind(
						permSet,
						compose(equalTo(CorePermDefs.OWNER_PERMISSION_NAME),
								PermissionEntity.getName));
				assertTrue(coreOwner.isPresent());

				final ModeEntity actualMode = coreOwner.get().getMode();
				assertNotNull(actualMode);
				assertEquals(CorePermDefs.CORE_MODE_NAME,
						actualMode.getName());

				final PermissionDomainEntity actualTarget = actualMode
						.getDomain();
				assertNotNull(actualTarget);
				assertEquals(PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
						actualTarget.getName());
			}

			// Want to return empty set for empty set
			final Set<PermissionEntity> emptySet = permDao
					.findByDtos(Collections.<ExtPermission> emptySet());
			assertNotNull(emptySet);
			assertTrue(emptySet.isEmpty());

			// Return empty set for non-sense
			final Set<PermissionEntity> nonExistentPerms = permDao
					.findByDtos(newHashSet(
							new ExtPermission(
									"Non-existent domain",
									CorePermDefs.CORE_MODE_NAME,
									"Non-existent perm"),
							new ExtPermission(
									newUuid(),
									newUuid(),
									newUuid())));
			assertNotNull(nonExistentPerms);
			assertTrue(nonExistentPerms.isEmpty());
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

	}
}
