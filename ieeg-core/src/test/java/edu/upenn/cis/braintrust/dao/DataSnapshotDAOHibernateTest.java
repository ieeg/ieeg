/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.TstUsers;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;

/**
 * Test {@code DataSnapshotDAOHibernate}.
 * 
 * @author John Frommeyer
 * 
 */
public class DataSnapshotDAOHibernateTest {

	private final Logger logger = LoggerFactory
			.getLogger(getClass());
	private EegStudy study;
	private Dataset dataset;

	/**
	 * A time series in both the study and the dataset.
	 */
	private TimeSeriesEntity studyAndDsTimeSeries;

	/**
	 * A time series just in the dataset.
	 */
	private TimeSeriesEntity datasetTimeSeries;

	/**
	 * An image in both the study and dataset.
	 */
	private ImageEntity studyAndDsImageEntity;

	/**
	 * An image in the dataset.
	 */
	private ImageEntity datasetImageEntity;
	private final TsAnnotationEntity studyTsAnn = new TsAnnotationEntity();
	private final TsAnnotationEntity datasetTsAnn = new TsAnnotationEntity();
	private TstUsers users;

	@Before
	public void classSetUp() throws Throwable {

		Session s = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(), 
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			
			s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();
			final BrainTrustDb db = new BrainTrustDb(s);

			users = new TstUsers(s);

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			final List<Patient> patients = newArrayList();
			final List<Dataset> datasets = newArrayList();
			final List<TsAnnotationEntity> tsAnnotations = newArrayList();
			final List<SnapshotUsage> usages = newArrayList();

			BuildDb.buildDb(patients, readPerm);

			final Patient patient0 = patients.get(0);

			study = getOnlyElement(
					getOnlyElement(patient0.getAdmissions()).getStudies());

			dataset = new Dataset(
					"My dataset", users.btRegUser1,
					SecurityUtil.createUserOwnedWorldReadableAcl(
							users.btRegUser1, permDAO));
			SnapshotUsage testUsage = new SnapshotUsage(
					dataset,
					users.btRegUser1,
					ProvenanceLogEntry.UsageType.VIEWS.ordinal(),
					null,
					null);
			usages.add(testUsage);
			SnapshotUsage derivedUsage = new SnapshotUsage(
					study,
					users.btRegUser1,
					ProvenanceLogEntry.UsageType.DERIVES.ordinal(),
					dataset,
					null);
			usages.add(derivedUsage);

			studyAndDsImageEntity = find(study.getImages(),
					compose(equalTo("0002"), IHasPubId.getPubId));
			dataset.getImages().add(studyAndDsImageEntity);

			datasetImageEntity = new ImageEntity();
			datasetImageEntity.setFileKey("x/y");
			datasetImageEntity.setType(ImageType.DIGITAL_PICTURES);
			s.save(datasetImageEntity);
			dataset.getImages().add(datasetImageEntity);

			studyAndDsTimeSeries =
					find(study.getTimeSeries(),
							compose(equalTo("0000-3------------------------------"),
									TsAnnotationEntity.getPubId));

			dataset.getTimeSeries().add(studyAndDsTimeSeries);

			datasetTimeSeries = new TimeSeriesEntity();
			dataset.getTimeSeries().add(datasetTimeSeries);
			datasetTimeSeries.setLabel("x");
			datasetTimeSeries.setFileKey("s/y.jpg");
			s.save(datasetTimeSeries);

			studyTsAnn.getAnnotated().add(studyAndDsTimeSeries);
			datasetTsAnn.getAnnotated().add(studyAndDsTimeSeries);

			studyTsAnn.setAnnotator("my-annotation-tool");
			datasetTsAnn.setAnnotator("my-annotation-tool");

			studyTsAnn.setCreator(users.btRegUser1);
			datasetTsAnn.setCreator(users.btRegUser1);

			studyTsAnn.setDescription("A description");
			datasetTsAnn.setDescription("A description");

			studyTsAnn.setStartOffsetUsecs(Long
					.valueOf(0L));
			datasetTsAnn.setStartOffsetUsecs(Long
					.valueOf(0L));

			studyTsAnn.setEndOffsetUsecs(Long
					.valueOf(1000000L));
			datasetTsAnn.setEndOffsetUsecs(Long
					.valueOf(1000000L));

			studyTsAnn.setType("seizure");
			datasetTsAnn.setType("seizure");

			studyTsAnn.setParent(study);
			datasetTsAnn.setParent(dataset);

			studyTsAnn.setLayerDefault(study);
			datasetTsAnn.setLayerDefault(dataset);

			datasets.add(dataset);
			tsAnnotations.add(studyTsAnn);
			tsAnnotations.add(datasetTsAnn);

			SnapshotUsage annotateUsage = new SnapshotUsage(
					dataset,
					users.btRegUser1,
					ProvenanceLogEntry.UsageType.ANNOTATES.ordinal(),
					null,
					datasetTsAnn);
			usages.add(annotateUsage);

			db.saveEverything(patients,
					null,
					tsAnnotations,
					null,
					datasets,
					usages);
			trx.commit();
		} catch (Throwable t) {
			if (t instanceof ConstraintViolationException) {
				ConstraintViolationException cve = (ConstraintViolationException) t;
				for (ConstraintViolation<?> cv : cve.getConstraintViolations()) {
					logger.error(cv.getMessage());
				}
			}
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(s);
		}
	}

	@Test
	public void findTsAnnotationByRevIdFromEegStudyTest() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(s);

			TsAnnotationEntity nonExistentTsAnnotation = dsDAO
					.findTsAnn(
							study,
							"3523523");

			assertNull(nonExistentTsAnnotation);

			TsAnnotationEntity actualTsAnnotation =
					dsDAO
							.findTsAnn(
									study,
									studyTsAnn.getPubId());
			System.out.println("it is studyTsAnn.getRevId(): "
					+ studyTsAnn.getPubId());
			assertNotNull(actualTsAnnotation);
			assertEquals(studyTsAnn.getType(),
					actualTsAnnotation.getType());
			assertEquals(
					getOnlyElement(studyTsAnn.getAnnotated()).getPubId(),
					getOnlyElement(actualTsAnnotation.getAnnotated())
							.getPubId());
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	@Test
	public void findTsAnnotationByRevIdFromDatasetTest() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();

			IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(s);

			TsAnnotationEntity nonExistentTsAnnotation =
					dsDAO.findTsAnn(
							dataset,
							"3523532");
			assertNull(nonExistentTsAnnotation);

			TsAnnotationEntity actualTsAnnotation = dsDAO
					.findTsAnn(
							dataset,
							datasetTsAnn.getPubId());
			assertNotNull(actualTsAnnotation);
			assertEquals(datasetTsAnn.getType(),
					actualTsAnnotation.getType());
			assertEquals(
					getOnlyElement(datasetTsAnn.getAnnotated()).getPubId(),
					getOnlyElement(actualTsAnnotation.getAnnotated())
							.getPubId());
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}
	}

	@Test
	public void deleteWithSnapshotUsage() throws Throwable {
		Session s = null;
		Transaction trx = null;
		final Long datasetId = dataset.getId();
		final Long studyId = study.getId();
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			ITsAnnotationDAO annDAO = new TsAnnotationDAOHibernate(s,
					HibernateUtil.getConfiguration());
			annDAO.deleteByParent(dataset);
			annDAO.deleteByParent(study);

			IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(s);
			dsDAO.delete(dataset);
			dsDAO.delete(study);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(s);
		}

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(s);
			final DataSnapshotEntity actualDataset = dsDAO.get(datasetId);
			assertNull(actualDataset);
			final DataSnapshotEntity actualStudy = dsDAO.get(studyId);
			assertNull(actualStudy);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(s);
		}
	}
	
	@Test
	public void isInProject() throws Throwable {
		Session s = null;
		Transaction trx = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			final ProjectEntity project = new ProjectEntity();
			project.setName(BtUtil.newUuid());
			project.getSnapshots().add(dataset);
			IProjectDAO dsDAO = new ProjectDAOHibernate(s);
			dsDAO.saveOrUpdate(project);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(s);
		}

		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();
			IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(s);
			final boolean studyInProject = dsDAO.isInAProject(study);
			assertFalse(studyInProject);
			final boolean datasetInProject = dsDAO.isInAProject(dataset);
			assertTrue(datasetInProject);
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(s);
		}
	}
}
