/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;

public class CompareUtil {

	public static void compare(ToolDto expected, ToolDto actual) {
		assertEquals(expected.getAuthorId(), actual.getAuthorId());
		assertEquals(expected.getBinUrl(), actual.getBinUrl());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getLabel(), actual.getLabel());
		assertTrue(actual.getId().isPresent());
		assertEquals(expected.getSourceUrl(), actual.getSourceUrl());
	}

	/**
	 * Compares labels and revIds. Cannot compare fileKey, since this is not set
	 * by the client-side dto.
	 * 
	 * @param expected
	 * @param actual
	 */
	public static void compare(
			Set<TimeSeriesDto> expected,
			Set<TimeSeriesDto> actual) {
		assertEquals(expected.size(), actual.size());
		for (final TimeSeriesDto expectedTs : expected) {
			TimeSeriesDto actualTs = null;
			for (TimeSeriesDto ts : actual) {
				if (ts.getId().equals(expectedTs.getId()) &&
						ts.getLabel().equals(expectedTs.getLabel())) {
					actualTs = ts;
					break;
				}

			}
			assertNotNull(
					"Expected time series with revId ["
							+ expectedTs.getId()
							+ "] and label = [" + expectedTs.getLabel()
							+ "] but found none.",
					actualTs);
		}
	}

	/**
	 * Compares all fields but revId, since this is not set on the client-side
	 * for new annotations.
	 * 
	 * @param expected
	 * @param actual
	 */
	public static void compare(
			TsAnnotationDto expected,
			TsAnnotationDto actual) {
		compare(expected.getAnnotated(), actual.getAnnotated());
		assertEquals(expected.getAnnotator(), actual.getAnnotator());
		assertEquals(expected.getDescription(), actual.getDescription());
		assertEquals(expected.getEndOffsetMicros(), actual.getEndOffsetMicros());
		assertEquals(expected.getStartOffsetMicros(),
				actual.getStartOffsetMicros());
		assertEquals(expected.getType(), actual.getType());
	}

	/**
	 * Returns true if the DTOs compare equal on all fields but revId. RevId is
	 * not checked because it is not set for new annotations.
	 * <p>
	 * Only compares layers if both are not-null. Because a null layer will be
	 * modified when sent to the database.
	 * 
	 * @param dto1
	 * @param dto2
	 * @return true if the DTOs compare equal on all fields but revId
	 */
	public static boolean areEqual(
			TsAnnotationDto dto1,
			TsAnnotationDto dto2) {
		if (dto1.getLayer() != null && dto2.getLayer() != null) {
			if (!dto1.getLayer().equals(dto2.getLayer())) {
				return false;
			}
		}
		return areEqual(dto1.getAnnotated(), dto2.getAnnotated())
				&& dto1.getAnnotator().equals(dto2.getAnnotator())
				&& ((dto1.getDescription() == null && dto2.getDescription() == null) || (dto1
						.getDescription().equals(dto2.getDescription())))
				&& dto1.getEndOffsetMicros().equals(dto2.getEndOffsetMicros())
				&& dto1.getStartOffsetMicros().equals(
						dto2.getStartOffsetMicros())
				&& dto1.getType().equals(dto2.getType());
	}

	/**
	 * Returns true if the dto sets compare equal on label and revid. Cannot
	 * compare fileKey, since this is not set by the client-side dto.
	 * 
	 * @param dto1
	 * @param dto2
	 * @return true if the dto sets compare equal on label and revid
	 */
	public static boolean areEqual(
			Set<TimeSeriesDto> dtos1,
			Set<TimeSeriesDto> dtos2) {
		boolean equal = dtos1.size() == dtos2.size();
		if (equal) {
			for (final TimeSeriesDto tsDto1 : dtos1) {
				boolean foundTsDto2 = false;
				for (TimeSeriesDto tsDto2 : dtos2) {
					if (tsDto2.getId().equals(tsDto1.getId()) &&
							tsDto2.getLabel().equals(tsDto1.getLabel())) {
						foundTsDto2 = true;
						break;
					}
				}
				if (!foundTsDto2) {
					equal = false;
					break;
				}
			}
		}
		return equal;
	}

}
