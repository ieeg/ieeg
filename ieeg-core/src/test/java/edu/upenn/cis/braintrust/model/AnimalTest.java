/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class AnimalTest {

	private final TstObjectFactory objFac = new TstObjectFactory(false);
	private StrainEntity strain1;
	private StrainEntity strain2;

	@Before
	public void setUp() throws Exception {
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			strain1 = objFac.newStrain(objFac.newSpecies());
			sess.saveOrUpdate(strain1.getParent());

			strain2 = objFac.newStrain(objFac.newSpecies());
			sess.saveOrUpdate(strain2.getParent());

			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void testSave() throws Exception {
		final AnimalEntity expected = objFac.newAnimal(strain1);
		Session sess = null;
		Transaction trx = null;
		Long expectedId = null;
		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			sess.saveOrUpdate(expected);
			expectedId = expected.getId();
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}

		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			AnimalEntity actual = (AnimalEntity) sess.get(AnimalEntity.class, expectedId);
			assertNotNull(actual);
			assertEquals(expected.getOrganization(), actual.getOrganization());
			assertEquals(expected.getLabel(), actual.getLabel());
			assertEquals(expected.getStrain().getLabel(), actual.getStrain()
					.getLabel());
			assertEquals(expected.getStrain().getParent().getLabel(), actual
					.getStrain().getParent().getLabel());

			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void testSaveTwo() throws Exception {
		final AnimalEntity expected1 = objFac.newAnimal(strain1);
		final AnimalEntity expected2 = objFac.newAnimal(strain1);
		Session sess = null;
		Transaction trx = null;
		Long expected1Id = null;
		Long expected2Id = null;
		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			sess.saveOrUpdate(expected1);
			expected1Id = expected1.getId();
			sess.saveOrUpdate(expected2);
			expected2Id = expected2.getId();
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}

		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			AnimalEntity actual1 = (AnimalEntity) sess.get(AnimalEntity.class, expected1Id);
			assertNotNull(actual1);
			assertEquals(expected1.getOrganization(), actual1.getOrganization());
			assertEquals(expected1.getLabel(), actual1.getLabel());
			assertEquals(expected1.getStrain().getLabel(), actual1.getStrain()
					.getLabel());
			assertEquals(expected1.getStrain().getParent().getLabel(),
					expected1
							.getStrain().getParent().getLabel());

			AnimalEntity actual2 = (AnimalEntity) sess.get(AnimalEntity.class, expected2Id);
			assertNotNull(actual2);
			assertEquals(expected2.getOrganization(), actual2.getOrganization());
			assertEquals(expected2.getLabel(), actual2.getLabel());
			assertEquals(expected2.getStrain().getLabel(), actual2.getStrain()
					.getLabel());
			assertEquals(expected2.getStrain().getParent().getLabel(), actual2
					.getStrain().getParent().getLabel());

			trx.commit();

		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test(expected = ConstraintViolationException.class)
	public void testSaveTwoSameLabel() throws Exception {
		final AnimalEntity expected1 = objFac.newAnimal(strain1);
		final AnimalEntity expected2 = objFac.newAnimal(strain2);
		expected2.setLabel(expected1.getLabel());
		Session sess = null;
		Transaction trx = null;
		try {
			sess =
					HibernateUtil.getSessionFactory().openSession();
	
			trx = sess.beginTransaction();
	
			sess.saveOrUpdate(expected1);
			sess.saveOrUpdate(expected2);
			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	
	}
}
