/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.isEmpty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

public class RecordingTest {

	private final TstObjectFactory objFac = new TstObjectFactory();
	
	@Test
	public void addElectrode() {
		final Recording study = new Recording();
		final ContactGroup electrode = objFac.newContactGroup();
		study.addContactGroup(electrode);

		assertEquals(1, study.getContactGroups().size());
		assertSame(electrode, getOnlyElement(study.getContactGroups()));
		assertSame(electrode.getParent(), study);
	}

	@Test
	public void removeElectrode() {
		final Recording study = new Recording();
		final ContactGroup electrode = objFac.newContactGroup();
		study.addContactGroup(electrode);

		study.removeContactGroup(electrode);
		assertTrue(isEmpty(study.getContactGroups()));
		assertNull(electrode.getParent());
	}

}
