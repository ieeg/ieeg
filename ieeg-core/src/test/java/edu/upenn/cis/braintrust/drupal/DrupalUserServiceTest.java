/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import edu.upenn.cis.braintrust.drupal.model.DrupalProfileValue;
import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.drupal.persistence.DrupalHibernateUtil;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.cache.MapCacheWrapper;
import edu.upenn.cis.db.mefview.shared.IUser.Status;

public class DrupalUserServiceTest {

	@BeforeClass
	public static void setUpClass() throws Throwable {
		final TestDbCleaner cleaner = new TestDbCleaner(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration());
		cleaner.deleteEverything();
	}

	// We had a bug where we were holding onto a closed session
	// plus we need to make sure that local user creation does not occur the
	// second time.
	@Test
	public void callServicesTwice() {

		final DrupalUserService userService = newDrupalUserService();

		assertNotNull(userService.findUserByUsername("dbgroup-admin"));
		assertNotNull(userService.findUserByUsername("dbgroup-admin"));

		assertNotNull(userService.findUserByUid(new UserId(1)));
		assertNotNull(userService.findUserByUid(new UserId(1)));

	}

	@Test
	public void findUserByUsernameNonUser() {

		final DrupalUserService userService = newDrupalUserService();

		final User user = userService.findUserByUsername("non user");

		assertNull(user);
	}

	@Test
	public void findUserByUidNonUser() {

		final DrupalUserService userService = newDrupalUserService();

		final User user = userService.findUserByUid(new UserId(87598087));

		assertNull(user);
	}

	@Test
	public void findUidString() {

		final DrupalUserService userService = newDrupalUserService();

		final User user = userService.findUserByUid(new UserId(436346));

		assertNull(user);
	}

	@Test
	public void findStatesCountries() {

		final DrupalUserService userService = newDrupalUserService();

		final List<String> locales = userService.findUserStates();
		
		assertNotNull(locales);
		System.out.println(locales);
		
	}

	private static final Logger logger = LoggerFactory
			.getLogger(DrupalUserServiceTest.class);

	@Test
	public void getEnabledUserTest() throws Throwable {

		final DrupalUserService userService = newDrupalUserService();

		final User user = userService.findUserByUsername("dbgroup-admin");
		assertNotNull(user);
		assertEquals("dbgroup-admin", user.getUsername());
		assertEquals(new UserId(1), user.getUserId());
		assertNotNull(user.getPassword());
		assertEquals(Status.ENABLED, user.getStatus());

		assertTrue(user.getRoles().size() == 2);

		// final Role role = getOnlyElement(user.getRoles());
		// assertEquals(Role.ADMIN, role);
		assertTrue(user.getRoles().contains(Role.ADMIN));
		assertTrue(user.getRoles().contains(Role.USER));

		// Session session = null;
		// Transaction trx = null;
		// try {
		// session = HibernateUtil.getSessionFactory().openSession();
		// trx = session.beginTransaction();
		// IUserDAO userDAO = new UserDAOHibernate(session);
		// UserEntity btUser = userDAO.get(user.getUserId().getValue());
		// assertNotNull(btUser);
		//
		// assertNotNull(btUser.getDefaultProtectionGroup());
		//
		// UserAce pgUser =
		// find(btUser.getDefaultProtectionGroup()
		// .getUserAces(),
		// compose(equalTo(btUser),
		// UserAce.getUser), null);
		// assertNotNull(pgUser);
		//
		// assertEquals(
		// EnumSet.allOf(Permission.class),
		// pgUser.getPermissions());
		//
		// AclEntity defPg = btUser.getDefaultProtectionGroup();
		// assertEquals(
		// EnumSet.allOf(Permission.class),
		// defPg.getWorldAce());
		// // TODO: check for A permission
		// // assertEquals(1, defPg.getOwners().size());
		// // assertEquals(btUser, getOnlyElement(defPg.getOwners()));
		//
		// trx.commit();
		// } catch (Throwable t) {
		// try {
		// if (trx != null && trx.isActive()) {
		// trx.rollback();
		// }
		// } catch (Throwable rbEx) {
		// logger.error("exception while rolling back", rbEx);
		// }
		// throw t;
		//
		// } finally {
		// if (session != null) {
		// session.close();
		// }
		// }
	}

	@Test
	public void getDisabledUserTest() {
		final DrupalUserService userService = newDrupalUserService();

		final User user2 = userService
				.findUserByUsername("disabled-user");

		assertNotNull(user2);
		assertEquals("disabled-user", user2.getUsername());
		assertEquals(new UserId(2), user2.getUserId());
		assertNotNull(user2.getPassword());
		assertEquals(Status.DISABLED, user2.getStatus());

		assertEquals(2, user2.getRoles().size());

		assertTrue(user2.getRoles().contains(Role.USER));
		assertTrue(user2.getRoles().contains(new Role("authenticated user")));
	}

	@Mock
	private SessionFactory btSessionFac;

	@Mock
	private Session btSession;

	@Mock
	private Transaction btTransaction;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(btSessionFac.openSession()).thenReturn(btSession);
		when(btSession.beginTransaction()).thenReturn(btTransaction);
	}

	@Test
	public void byUserNameSessionClosedOnSuccess() {

		SessionFactory drupalSessionFac = mock(SessionFactory.class);
		Session drupalSession = mock(Session.class);
		when(drupalSession.isOpen()).thenReturn(true);
		Transaction drupalTrx = mock(Transaction.class);
		Query query = mock(Query.class);
		DrupalUser user = mock(DrupalUser.class);
		when(drupalSessionFac.openSession()).thenReturn(drupalSession);
		when(drupalSession.beginTransaction()).thenReturn(drupalTrx);
		when(drupalSession.getNamedQuery(anyString()))
				.thenReturn(query);
		when(query.setReadOnly(anyBoolean())).thenReturn(query);
		when(query.setParameter(anyString(), anyObject())).thenReturn(query);
		when(query.uniqueResult()).thenReturn(user);
		when(user.getName()).thenReturn("fake-username");
		when(user.getPass()).thenReturn("fake-pass");
		when(user.getStatus()).thenReturn(Byte.valueOf((byte) 1));
		when(user.getUid()).thenReturn(345L);

		final DrupalUserService userService = newDrupalUserService(drupalSessionFac);

		userService.findUserByUsername("don't care");

		verify(drupalTrx).commit();
		verify(drupalSession).close();

	}

	@Test
	public void byUserNameSessionClosedOnException() {

		Session session = mock(Session.class);
		SessionFactory sessionFac = mock(SessionFactory.class);
		when(session.isOpen()).thenReturn(true);
		when(sessionFac.openSession()).thenReturn(session);
		when(session.getNamedQuery(anyString()))
				.thenThrow(new RuntimeException());

		final DrupalUserService userService = newDrupalUserService(sessionFac);
		boolean exception = false;

		try {
			userService.findUserByUsername("don't care");
		} catch (RuntimeException e) {
			exception = true;
		}
		assertTrue(exception);

		verify(session).close();

	}

	@Test
	public void byUidSessionClosedOnSuccess() {

		SessionFactory sessionFac = mock(SessionFactory.class);
		Session session = mock(Session.class);
		when(session.isOpen()).thenReturn(true);
		Transaction trx = mock(Transaction.class);
		DrupalUser user = mock(DrupalUser.class);
		List<DrupalProfileValue> list = Lists.newArrayList();
		Query query = mock(Query.class);
		when(sessionFac.openSession()).thenReturn(session);
		when(session.beginTransaction()).thenReturn(trx);
		when(session.get(any(Class.class), anyInt())).thenReturn(user);
		when(session.getNamedQuery(anyString())).thenReturn(query);
		when(user.getName()).thenReturn("fake-username");
		when(user.getPass()).thenReturn("fake-pass");
		when(user.getStatus()).thenReturn(Byte.valueOf((byte) 1));
		when(user.getUid()).thenReturn(345L);
		when(query.setReadOnly(anyBoolean())).thenReturn(query);
		when(query.setParameter(anyString(), anyObject())).thenReturn(query);
		when(query.uniqueResult()).thenReturn(user);
		when(query.list()).thenReturn(list);

		final DrupalUserService userService = newDrupalUserService(sessionFac);

		userService.findUserByUid(new UserId(244));

		verify(trx).commit();
		verify(session).close();

	}

	@Test
	public void byUidSessionClosedOnException() {

		Session session = mock(Session.class);
		when(session.isOpen()).thenReturn(true);
		SessionFactory sessionFac = mock(SessionFactory.class);
		when(sessionFac.openSession()).thenReturn(session);

		when(session.get(any(Class.class), any(Serializable.class)))
				.thenThrow(new RuntimeException());

		final DrupalUserService userService = newDrupalUserService(sessionFac);

		boolean exception = false;

		try {
			userService.findUserByUid(new UserId(244));
		} catch (RuntimeException e) {
			exception = true;
		}
		assertTrue(exception);

		verify(session).close();
	}

	private void testFindUsersByUid(DrupalUserService service,
			Set<UserId> existentUserIds,
			Set<UserId> nonExistentUserIds) {

		final Set<UserId> userIds = newHashSet(existentUserIds);
		userIds.addAll(nonExistentUserIds);

		final Map<UserId, Optional<User>> idToUser = service
				.findUsersByUid(userIds);
		assertEquals(userIds.size(), idToUser.size());

		for (final UserId userId : existentUserIds) {
			Optional<User> user = idToUser.get(userId);
			assertTrue(user.isPresent());
			assertTrue(userId.equals(user.get().getUserId()));
		}

		for (final UserId nonUserId : nonExistentUserIds) {
			assertFalse(idToUser.get(nonUserId).isPresent());
		}

	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void findUsersByUidMultiple() {
		final DrupalUserService service = newDrupalUserService();
		final UserId userId0 = new UserId(0);
		final UserId userId3 = new UserId(3);
		final UserId userId4 = new UserId(4);
		final UserId nonUserId = new UserId(254645);

		// None cached
		testFindUsersByUid(
				service,
				newHashSet(userId0, userId3, userId4),
				newHashSet(nonUserId));

		// All cached
		testFindUsersByUid(
				service,
				newHashSet(userId0, userId3, userId4),
				newHashSet(nonUserId));

		// Some cached
		testFindUsersByUid(
				service,
				newHashSet(new UserId(2), new UserId(1), userId4),
				Collections.EMPTY_SET);

	}

	private DrupalUserService newDrupalUserService(
			SessionFactory sessionFactory) {
		return new DrupalUserService(
				sessionFactory,
				new MapCacheWrapper<UserId, User>(),
				new MapCacheWrapper<String, User>());
	}

	private DrupalUserService newDrupalUserService() {
		return new DrupalUserService(
				DrupalHibernateUtil.getSessionFactory(),
				new MapCacheWrapper<UserId, User>(),
				new MapCacheWrapper<String, User>());
	}
}
