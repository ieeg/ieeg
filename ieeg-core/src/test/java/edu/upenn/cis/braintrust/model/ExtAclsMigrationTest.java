/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

/**
 * @author Sam Donnelly
 */
public class ExtAclsMigrationTest {

	// private static PermissionEntity read;
	// private static PermissionEntity edit;
	// private static PermissionEntity owner;
	//
	// public static void checkPerms(IHasAcl hasAcl, IHasExtAcl hasExtAcl) {
	// System.out.println("looking at [" +
	// hasAcl.getLabel() + "]");
	// AclEntity acl = hasAcl.getAcl();
	// ExtAclEntity extAcl = hasExtAcl.getExtAcl();
	// switch (acl.getWorldAce()) {
	// case NONE:
	// System.out.println("world read");
	// assertTrue(extAcl.getWorldAce().isEmpty());
	// break;
	// case READ_ONLY:
	// System.out.println("world edit");
	// assertTrue(getOnlyElement(extAcl.getWorldAce()) == read);
	// break;
	// case EDIT:
	// assertTrue(getOnlyElement(extAcl.getWorldAce()) == edit);
	// break;
	// default:
	// assertFalse("unknown world perm", true);
	// break;
	// }
	// Set<UserAceEntity> userAces = acl.getUserAces();
	// Set<ExtUserAceEntity> extUserAces = extAcl.getUserAces();
	//
	// for (UserAceEntity userAce : userAces) {
	// UserEntity user = userAce.getUser();
	// System.out.println("user: [" + user.getId() + "]");
	// Optional<ExtUserAceEntity> extUserAceOpt = tryFind(
	// extUserAces,
	// compose(equalTo(user), ExtUserAceEntity.getUser));
	// assertTrue("missing user ace", extUserAceOpt.isPresent());
	// ExtUserAceEntity extUserAce = extUserAceOpt.get();
	// Permissions permissions = userAce.getPermsEntity();
	// switch (permissions) {
	// case READ_ONLY:
	// System.out.println("user read");
	// assertTrue(getOnlyElement(extUserAce.getPerms()) == read);
	// break;
	// case EDIT:
	// System.out.println("user edit");
	// assertTrue(getOnlyElement(extUserAce.getPerms()) == edit);
	// break;
	// case OWNER_EDIT:
	// System.out.println("user owner");
	// assertTrue(getOnlyElement(extUserAce.getPerms()) == owner);
	// break;
	// default:
	// assertFalse("unknown user perm", true);
	// break;
	// }
	// }
	//
	// System.out.println("project aces");
	// assertTrue(extAcl.getProjectAces().isEmpty());
	//
	// }
	//
	// public static void main(String[] args) throws Exception {
	// System.out.println("starting "
	// + ExtAclsMigrationTest.class.getSimpleName() + "...");
	//
	// checkArgument(args.length == 1);
	//
	// FileInputStream hibernateProps = null;
	// Properties props = null;
	// try {
	// hibernateProps = new FileInputStream(args[0]);
	// props = new Properties();
	// props.load(hibernateProps);
	// } finally {
	// BtUtil.close(hibernateProps);
	// }
	//
	// if (props.containsKey("hibernate.hbm2ddl.auto")
	// &&
	// !props.getProperty("hibernate.hbm2ddl.auto").equalsIgnoreCase(
	// "validate")) {
	// System.err
	// .println("you have non-validate hibernate.hbm2ddl.auto!!! stopping...");
	// System.exit(1);
	// }
	// HibernateUtil.putConfigProps(props);
	//
	// Session session = null;
	// Transaction trx = null;
	//
	// try {
	// session = HibernateUtil.getSessionFactory().openSession();
	// trx = session.beginTransaction();
	//
	// IPermissionDAO permissionDAO = new PermissionDAOHibernate(session);
	// read = permissionDAO.findStarCoreReadPerm();
	// edit = permissionDAO.findStarCoreEditPerm();
	// owner = permissionDAO.findStarCoreOwnerPerm();
	// @SuppressWarnings("unchecked")
	// Iterator<DataSnapshotEntity> dataSnapshotItr =
	// session
	// .createQuery("from DataSnapshot")
	// .iterate();
	// while (dataSnapshotItr.hasNext()) {
	// DataSnapshotEntity dataSnapshot = dataSnapshotItr.next();
	// checkPerms(dataSnapshot, dataSnapshot);
	// }
	//
	// @SuppressWarnings("unchecked")
	// Iterator<ToolEntity> toolItr =
	// session
	// .createQuery("from Tool")
	// .iterate();
	// while (toolItr.hasNext()) {
	// ToolEntity tool = toolItr.next();
	// checkPerms(tool, tool);
	// }
	//
	// trx.commit();
	// System.out.println("everything looks good");
	// } catch (Exception e) {
	// e.printStackTrace();
	// PersistenceUtil.rollback(trx);
	// } finally {
	// PersistenceUtil.close(session);
	// }
	// }
}
