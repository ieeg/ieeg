/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.testhelper.BrainTrustDb;
import edu.upenn.cis.braintrust.testhelper.BuildDb;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;

/**
 * Test the time series DAO methods.
 * 
 * @author John Frommeyer
 * 
 */
public class TimeSeriesDAOHibernateTest {

	private static Logger logger = LoggerFactory
			.getLogger(TimeSeriesDAOHibernateTest.class);

	private static TimeSeriesEntity tsEntity;
	private static TimeSeriesEntity datasetTsEntity;
	private static final TstObjectFactory tstObjectFactory = new TstObjectFactory(
			false);

	@BeforeClass
	public static void setupDb() throws Throwable {
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			Session s = HibernateUtil.getSessionFactory().openSession();

			trx = s.beginTransaction();
			final BrainTrustDb db = new BrainTrustDb(s);

			final List<Patient> patients = newArrayList();
			final List<UserEntity> userEntities = newArrayList();
			final List<Dataset> datasets = newArrayList();
			final List<TsAnnotationEntity> tsAnnotations = newArrayList();
			final List<TimeSeriesEntity> timeSeries = newArrayList();
			final List<SnapshotUsage> usages = newArrayList();

			IPermissionDAO permDAO = new PermissionDAOHibernate(s);
			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();

			BuildDb.buildDb(patients, readPerm);

			final Patient patient0 = patients.get(0);

			final EegStudy study = getOnlyElement(
					getOnlyElement(
							patient0.getAdmissions())
							.getStudies());
			tsEntity = find(
					study.getTimeSeries(),
					compose(equalTo("0000-0------------------------------"),
							IHasPubId.getPubId));
			final Dataset dataset = tstObjectFactory.newDataset();
			userEntities.add(dataset.getCreator());
			datasets.add(dataset);

			datasetTsEntity = tstObjectFactory.newTimeSeries(dataset);
			timeSeries.add(datasetTsEntity);

			db.saveEverything(patients, timeSeries,
					tsAnnotations,
					userEntities, datasets, usages);
			trx.commit();
			s.close();
		} catch (Throwable t) {
			try {
				if (trx != null && trx.isActive()) {
					trx.rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
			throw t;
		}
	}

	// TODO: move this into CantactDAOHibernateTest
	@Test
	public void findContact() throws Throwable {
		Session s = null;
		Transaction trx = null;
		Contact actualContact = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();

			ContactDAOHibernate tsDAO = new ContactDAOHibernate(s);

			actualContact = tsDAO.findByTrace(tsEntity);
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}

		assertNotNull(actualContact);
		assertEquals(new MriCoordinates(34.9, -987.7, -3452.56),
				actualContact.getMriCoordinates());
		assertEquals(new TalairachCoordinates(4.5, -98.3,
				8384.093), actualContact.getTalairachCoordinates());

	}

	// TODO: move this to ContactDAOHibernateDAO
	@Test
	public void findContactForNonStudyTs() throws Throwable {
		Session s = null;
		Transaction trx = null;
		Contact actualContact = null;
		try {
			s = HibernateUtil.getSessionFactory().openSession();
			trx = s.beginTransaction();

			ContactDAOHibernate tsDAO = new ContactDAOHibernate(s);

			actualContact = tsDAO
					.findByTrace(datasetTsEntity);
			trx.commit();
		} catch (Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("rollback exception", rbEx);
					throw rbEx;
				}
			}
			throw t;
		} finally {
			if (s != null) {
				s.close();
			}
		}

		assertNull(actualContact);
	}

}
