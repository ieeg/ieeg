/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.EpilepsyType;

public class EpilepsyTypeTest {
	/**
	 * The db depends on these staying the same.
	 */
	@Test
	public void checkNames() {
		for (EpilepsyType epType : EpilepsyType.values()) {
			switch (epType) {
				case FRONTAL_LOBE:
					assertEquals(
							"db enums must not change order!",
							"FRONTAL_LOBE",
							epType.name());
					break;
				case FRONTAL_LOBE_BILATERAL:
					assertEquals(
							"db enums must not change order!",
							"FRONTAL_LOBE_BILATERAL",
							epType.name());
					break;
				case FRONTAL_LOBE_LEFT:
					assertEquals(
							"db enums must not change order!",
							"FRONTAL_LOBE_LEFT",
							epType.name());
					break;
				case FRONTAL_LOBE_RIGHT:
					assertEquals(
							"db enums must not change order!",
							"FRONTAL_LOBE_RIGHT",
							epType.name());
					break;
				case GENERALIZED:
					assertEquals(
							"db enums must not change order!",
							"GENERALIZED",
							epType.name());
					break;
				case INSULA:
					assertEquals(
							"db enums must not change order!",
							"INSULA",
							epType.name());
					break;
				case INSULA_BILATERAL:
					assertEquals(
							"db enums must not change order!",
							"INSULA_BILATERAL",
							epType.name());
					break;
				case INSULA_LEFT:
					assertEquals(
							"db enums must not change order!",
							"INSULA_LEFT",
							epType.name());
					break;
				case INSULA_RIGHT:
					assertEquals(
							"db enums must not change order!",
							"INSULA_RIGHT",
							epType.name());
					break;
				case OCCIPITAL:
					assertEquals(
							"db enums must not change order!",
							"OCCIPITAL",
							epType.name());
					break;
				case OCCIPITAL_BILATERAL:
					assertEquals(
							"db enums must not change order!",
							"OCCIPITAL_BILATERAL",
							epType.name());
					break;
				case OCCIPITAL_LEFT:
					assertEquals(
							"db enums must not change order!",
							"OCCIPITAL_LEFT",
							epType.name());
					break;
				case OCCIPITAL_RIGHT:
					assertEquals(
							"db enums must not change order!",
							"OCCIPITAL_RIGHT",
							epType.name());
					break;
				case PARIETAL:
					assertEquals(
							"db enums must not change order!",
							"PARIETAL",
							epType.name());
					break;
				case PARIETAL_BILATERAL:
					assertEquals(
							"db enums must not change order!",
							"PARIETAL_BILATERAL",
							epType.name());
					break;
				case PARIETAL_LEFT:
					assertEquals(
							"db enums must not change order!",
							"PARIETAL_LEFT",
							epType.name());
					break;
				case PARIETAL_RIGHT:
					assertEquals(
							"db enums must not change order!",
							"PARIETAL_RIGHT",
							epType.name());
					break;
				case TEMPORAL_LOBE:
					assertEquals(
							"db enums must not change order!",
							"TEMPORAL_LOBE",
							epType.name());
					break;
				case TEMPORAL_LOBE_BILATERAL:
					assertEquals(
							"db enums must not change order!",
							"TEMPORAL_LOBE_BILATERAL",
							epType.name());
					break;
				case TEMPORAL_LOBE_LEFT:
					assertEquals(
							"db enums must not change order!",
							"TEMPORAL_LOBE_LEFT",
							epType.name());
					break;
				case TEMPORAL_LOBE_RIGHT:
					assertEquals(
							"db enums must not change order!",
							"TEMPORAL_LOBE_RIGHT",
							epType.name());
					break;
				case UNKNOWN:
					assertEquals(
							"db enums must not change order!",
							"UNKNOWN",
							epType.name());
					break;
				default:
					assertFalse("unknown EpilepsyType: " + epType, true);
					break;
			}
		}
	}
}
