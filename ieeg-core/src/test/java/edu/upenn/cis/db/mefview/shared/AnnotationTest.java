/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.testhelper.CoreTstObjectFactory;

/**
 * @author John Frommeyer
 *
 */
public class AnnotationTest {
	private final static CoreTstObjectFactory objFac = new CoreTstObjectFactory();
	@Test
	public void compareToConsistentWithEqualsRevId() {
		final TraceInfo ti1 = objFac.newTraceInfo();
		final TraceInfo ti2 = objFac.newTraceInfo();
		final TraceInfo ti3 = objFac.newTraceInfo();
		
		final Annotation ann1 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		final Annotation ann2 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		
		final Annotation ann3 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		
		ann2.setCreator(ann1.getCreator());
		ann3.setCreator(ann1.getCreator());
		ann2.setDescription(ann1.getDescription());
		ann3.setDescription(ann1.getDescription());
		ann2.setStart(ann1.getStart());
		ann3.setStart(ann1.getStart());
		ann2.setEnd(ann1.getEnd());
		ann3.setEnd(ann1.getEnd());
		
		ann1.setRevAndInternalId(BtUtil.newUuid());
		ann2.setRevAndInternalId(ann1.getRevId());
		ann3.setRevAndInternalId(BtUtil.newUuid());
		
		boolean cmpZero12 = ann2.compareTo(ann1) == 0;
		boolean areEqual12 = ann2.equals(ann1);
		assertTrue("cmpZero12: " + cmpZero12 + " and areEqual12: " + areEqual12, cmpZero12 == areEqual12);
		
		boolean cmpZero13 = ann3.compareTo(ann1) == 0;
		boolean areEqual13 = ann3.equals(ann1);
		assertTrue("cmpZero13: " + cmpZero13 + " and areEqual13: " + areEqual13, cmpZero13 == areEqual13);
		
		boolean cmpZero23 = ann3.compareTo(ann2) == 0;
		boolean areEqual23 = ann3.equals(ann2);
		assertTrue("cmpZero23: " + cmpZero23 + " and areEqual23: " + areEqual23, cmpZero23 == areEqual23);
		
		ann1.setRevId(BtUtil.newUuid());
		ann2.setRevId(ann1.getRevId());
		ann3.setRevId(BtUtil.newUuid());
		
		cmpZero12 = ann2.compareTo(ann1) == 0;
		areEqual12 = ann2.equals(ann1);
		assertTrue("cmpZero12: " + cmpZero12 + " and areEqual12: " + areEqual12, cmpZero12 == areEqual12);
		
		cmpZero13 = ann3.compareTo(ann1) == 0;
		areEqual13 = ann3.equals(ann1);
		assertTrue("cmpZero13: " + cmpZero13 + " and areEqual13: " + areEqual13, cmpZero13 == areEqual13);
		
		cmpZero23 = ann3.compareTo(ann2) == 0;
		areEqual23 = ann3.equals(ann2);
		assertTrue("cmpZero23: " + cmpZero23 + " and areEqual23: " + areEqual23, cmpZero23 == areEqual23);
	}
	
	@Test
	public void compareToConsistentWithEqualsInternalId() {
		final TraceInfo ti1 = objFac.newTraceInfo();
		final TraceInfo ti2 = objFac.newTraceInfo();
		final TraceInfo ti3 = objFac.newTraceInfo();
		
		final Annotation ann1 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		final Annotation ann2 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		
		final Annotation ann3 = objFac.newAnnotation(newArrayList(ti1, 
				ti2, 
				ti3));
		
		ann2.setCreator(ann1.getCreator());
		ann3.setCreator(ann1.getCreator());
		ann2.setDescription(ann1.getDescription());
		ann3.setDescription(ann1.getDescription());
		ann2.setStart(ann1.getStart());
		ann3.setStart(ann1.getStart());
		ann2.setEnd(ann1.getEnd());
		ann3.setEnd(ann1.getEnd());
		
		ann1.setInternalId(BtUtil.newUuid());
		ann2.setInternalId(ann1.getInternalId());
		ann3.setInternalId(BtUtil.newUuid());
		
		final boolean cmpZero12 = ann2.compareTo(ann1) == 0;
		final boolean areEqual12 = ann2.equals(ann1);
		assertTrue("cmpZero12: " + cmpZero12 + " and areEqual12: " + areEqual12, cmpZero12 == areEqual12);
		
		final boolean cmpZero13 = ann3.compareTo(ann1) == 0;
		final boolean areEqual13 = ann3.equals(ann1);
		assertTrue("cmpZero13: " + cmpZero13 + " and areEqual13: " + areEqual13, cmpZero13 == areEqual13);
		
		final boolean cmpZero23 = ann3.compareTo(ann2) == 0;
		final boolean areEqual23 = ann3.equals(ann2);
		assertTrue("cmpZero23: " + cmpZero23 + " and areEqual23: " + areEqual23, cmpZero23 == areEqual23);
	}
}
