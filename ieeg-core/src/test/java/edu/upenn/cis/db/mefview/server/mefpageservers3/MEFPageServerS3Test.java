/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Objects.equal;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static edu.upenn.cis.braintrust.BtUtil.getBoolean;
import static edu.upenn.cis.braintrust.BtUtil.getDouble;
import static edu.upenn.cis.braintrust.BtUtil.getInt;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import net.sf.ehcache.CacheManager;

import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.PageReader.PageList;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.Decimate;
import edu.upenn.cis.db.mefview.services.Downsample;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.testhelper.S3FromFileSystem;
import edu.upenn.cis.eeg.filters.TimeSeriesFilterButterworth;
import edu.upenn.cis.eeg.mef.MEFReader;

/**
 * @author Sam Donnelly
 * 
 */
public class MEFPageServerS3Test {

	@BeforeClass
	public static void classSetup() {
		Map<String, String> props = newHashMap();
		props.put(IvProps.MEF_INDEX_SLICE_NO_ENTRIES, "100");
		IvProps.setIvProps(props);
	}

	private static final URL MEF_FILE_URL = MEFPageServerS3Test.class
			.getClassLoader()
			.getResource("I010_A0001_D001_CH2.mef");

	private static final int MEF_FILE_SAMPLING_RATE_HZ = 200;

	private static final User NULL_USER = new User(
			new UserId(Long.MAX_VALUE),
			"null-user",
			"",
			IUser.Status.DISABLED,
			Collections.<Role> emptySet(),
			"",
			"email@nowhere.com",
			new UserProfile("x", // first
					"y", // last
					"z", // inst
					"w", // title
					"v", // job
					null, // aws
					null,
					null, // dropbox
					null, null, // geo
					null, // street
					null, // city
					null, // state
					null, // zip
					null // country
			));

	private static MEFPageServerS3 makeOne(IDataSnapshotServer dsServer)
			throws AuthorizationException {
		AmazonS3 s3 = new S3FromFileSystem();
		return new MEFPageServerS3(
				new S3FromFileSystem(),
				CacheManager.newInstance(
						MEFPageServerS3.class.getClassLoader()
								.getResource("ieeg-ehcache.xml"))
						.getEhcache(
								"MEFPageServerS3.header"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.data"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.decompressedDataCache"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.postProcDataCache"),
				dsServer,
				"doesn't-matter-data-bucket",
				getInt(
						IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ,
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ_DFLT),
				IvProps.getMefPageServerS3CpuThrdPoolSz(),
				getBoolean(IvProps.getIvProps(), IvProps.DOWNSAMPLED, false),
				getDouble(
						IvProps.getIvProps(),
						IvProps.DOWNSAMPLED_RATE_HZ,
						IvProps.DOWNSAMPLED_RATE_HZ_DFLT),
				1024 * getInt(
						IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB,
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB_DFLT),
				IvProps.getPaddingSamples(),
				new MEFIndexService(
						s3,
						CacheManager
								.getCacheManager("ieeg")
								.getEhcache(MEFIndexService.CACHE_NAME),
						IvProps.getMefIndexSliceSize(),
						"doesn't-matter-data-bucket"),
				IvProps.getMEFPageServerS3MaxRequestsPerUserRawRed(),
				IvProps.getMEFPageServerS3MaxRequestsTotal(),
				IvProps.getMEFPageServerS3TimeoutSecs(),
				IvProps.getMEFPageServerS3MaxReqHzChsSecs(),
				IvProps.getMEFPageServerS3MaxRedReqHzChsSecs());
	}

	@Test
	public void requestMultiChannelReadBlock() throws Exception {
		IDataSnapshotServer dsServer = mock(IDataSnapshotServer.class);
		// when(dsServer.getMEFPaths(
		// any(User.class),
		// anyString(),
		// any(Iterable.class)))
		// .thenReturn(
		// newArrayList(MEF_FILE, MEF_FILE));

		MEFSnapshotSpecifier mefSnapshotSpec = new MEFSnapshotSpecifier("",
				"");

		MEFChannelSpecifier chSpec = new MEFChannelSpecifier(
				mefSnapshotSpec,
				MEF_FILE_URL.getPath(),
				"doesnt-matter",
				MEF_FILE_SAMPLING_RATE_HZ,
				"fake-sig");

		MEFPageServerS3 mefPageServer = makeOne(dsServer);
		long beginOffsetWereLookingFor = (7 * 60 * 60 * (long) 1E6);
		long endOffsetWereLookingFor =
				beginOffsetWereLookingFor + (3 * 60 * 60 * (long) 1E6);
		List<List<TimeSeriesData>> actualTsdss = mefPageServer
				.requestMultiChannelRead(
						newArrayList(chSpec, chSpec),
						beginOffsetWereLookingFor,
						endOffsetWereLookingFor,
						new PostProcessor[] {
								new Downsample(
										MEF_FILE_SAMPLING_RATE_HZ,
										1),
								new Downsample(
										MEF_FILE_SAMPLING_RATE_HZ,
										1)
						},
						NULL_USER,
						null);
		List<List<Integer>> mergedActualTsds = newArrayList();
		for (List<TimeSeriesData> actualTsds : actualTsdss) {
			List<Integer> merged = newArrayList();
			mergedActualTsds.add(merged);
			for (TimeSeriesData actualTsd : actualTsds) {
				Integer[] asArray = actualTsd.toArray();
				merged.addAll(Arrays.asList(asArray));
			}
		}

		long blockSize = 100 * (long) 1E6;

		long blockStart = beginOffsetWereLookingFor;
		long blockEnd = endOffsetWereLookingFor > blockStart + blockSize ? blockStart
				+ blockSize
				: endOffsetWereLookingFor;

		MEFReader mefReader = new MEFReader(MEF_FILE_URL.toURI().getPath());

		long realStartTime = mefReader.getStartTime();

		PageList expectedPageLists = mefReader.getPages(
				realStartTime + blockStart,
				realStartTime + blockEnd,
				Integer.MIN_VALUE);
		TimeSeriesPage[] expectedPages = mefReader
				.readPages(
						expectedPageLists.startPage,
						Ints.checkedCast(expectedPageLists.endPage)
								- Ints.checkedCast(expectedPageLists.startPage)
								+ 1,
						true);
		PostProcessor ds =
				new Downsample(
						MEF_FILE_SAMPLING_RATE_HZ,
						1);
		// new Decimate(
		// MEF_FILE_SAMPLING_RATE_HZ,
		// new FilterSpec(0, 0, -1, -1, -1, -1), new int[0]);

		List<TimeSeriesData> expectedTsds =
				ds.process(
						blockStart + mefReader.getStartTime(),
						blockEnd + mefReader.getStartTime(),
						mefReader.getSampleRate(),
						blockStart,
						mefReader.getVoltageConversionFactor(),
						expectedPages,
						chSpec,
						false, 0, 0
						);
		blockStart += blockSize;// (blockEnd - blockStart);
		// while (blockEnd < endTime) {
		while (blockStart < endOffsetWereLookingFor) {
			blockEnd = endOffsetWereLookingFor > blockStart + blockSize
					? blockStart + blockSize
					: endOffsetWereLookingFor;

			PageList newExpectedPageLists = mefReader.getPages(
					realStartTime + blockStart,
					realStartTime + blockEnd,
					Integer.MIN_VALUE);
			TimeSeriesPage[] newExpectedPages = mefReader
					.readPages(
							newExpectedPageLists.startPage,
							Ints.checkedCast(newExpectedPageLists.endPage)
									- Ints.checkedCast(newExpectedPageLists.startPage)
									+ 1,
							true);

			List<TimeSeriesData> newSegment =
					ds.process(
							blockStart + mefReader.getStartTime(),
							blockEnd + mefReader.getStartTime(),
							mefReader.getSampleRate(),
							blockStart,
							mefReader.getVoltageConversionFactor(),
							newExpectedPages,
							chSpec,
							false, 0, 0
							);
			expectedTsds.addAll(newSegment);
			blockStart += blockSize;// (blockEnd - blockStart);
		}

		List<Integer> mergedExpectedTsds = newArrayList();
		for (TimeSeriesData expectedTsd : expectedTsds) {
			Integer[] asArray = expectedTsd.toArray();
			mergedExpectedTsds.addAll(Arrays.asList(asArray));
		}

		for (int i = 0; i < mergedActualTsds.size(); i++) {
			List<Integer> actualTsd = mergedActualTsds.get(i);
			System.out.println(mergedExpectedTsds.size() + " "
					+ actualTsd.size());
			assertEquals(mergedExpectedTsds.size(), actualTsd.size());
			for (int j = 0; j < mergedExpectedTsds.size(); j++) {
				if (!equal(mergedExpectedTsds.get(j), actualTsd.get(j))) {
					// System.out.println("--" + j + " "
					// + mergedExpectedTsds.get(j)
					// + " "
					// + actualTsd.get(j));
				} else {
					// System.out.println("++" + j + " "
					// + mergedExpectedTsds.get(j) +
					// " "
					// + actualTsd.get(j));
				}
			}
			assertEquals(mergedExpectedTsds, actualTsd);
		}
	}

	@Test
	public void requestMultiChannelReadBlockWithFilter() throws Exception {
		IDataSnapshotServer dsServer = mock(IDataSnapshotServer.class);
		// when(dsServer.getMEFPaths(
		// any(User.class),
		// anyString(),
		// any(Iterable.class)))
		// .thenReturn(
		// newArrayList(MEF_FILE, MEF_FILE));

		MEFSnapshotSpecifier mefSnapshotSpec = new MEFSnapshotSpecifier("",
				"");

		MEFChannelSpecifier chSpec = new MEFChannelSpecifier(
				mefSnapshotSpec,
				MEF_FILE_URL.getPath(),
				"fake-id",
				MEF_FILE_SAMPLING_RATE_HZ,
				"fake-sig");

		MEFReader mefReader = new MEFReader(MEF_FILE_URL.toURI().getPath());
		MEFPageServerS3 mefPageServer = makeOne(dsServer);
		long beginOffsetWereLookingFor = (7 * 60 * 60 * (long) 1E6);
		long endOffsetWereLookingFor =
				beginOffsetWereLookingFor + (180 * 60 * (long) 1E6);
		List<List<TimeSeriesData>> actualTsdss = mefPageServer
				.requestMultiChannelRead(
						newArrayList(chSpec, chSpec),
						beginOffsetWereLookingFor,
						endOffsetWereLookingFor,
						new PostProcessor[] {
								new Decimate(
										50.0,
										new FilterSpec(
												TimeSeriesFilterButterworth.NAME,
												FilterSpec.NO_FILTER, 3, 0.0,
												100.0,
												0.0,
												0.0),
										new int[0], false),
								new Decimate(
										50.0,
										new FilterSpec(
												TimeSeriesFilterButterworth.NAME,
												FilterSpec.NO_FILTER, 3, 0.0,
												100.0,
												0.0,
												0.0),
										new int[0], false),
						},
						NULL_USER,
						null);
		List<List<Integer>> mergedActualTsds = newArrayList();
		for (List<TimeSeriesData> actualTsds : actualTsdss) {
			List<Integer> merged = newArrayList();
			mergedActualTsds.add(merged);
			for (TimeSeriesData actualTsd : actualTsds) {
				Integer[] asArray = actualTsd.toArray();
				merged.addAll(Arrays.asList(asArray));
			}
		}

		long realStartTime = mefReader.getStartTime();

		long blockSize = 720 * (long) 1E6;

		long blockStart = beginOffsetWereLookingFor;
		long blockEnd = endOffsetWereLookingFor > blockStart + blockSize ? blockStart
				+ blockSize
				: endOffsetWereLookingFor;

		PageList expectedPageLists = mefReader.getPages(
				realStartTime + blockStart,
				realStartTime + blockEnd,
				Integer.MIN_VALUE);
		TimeSeriesPage[] expectedPages = mefReader
				.readPages(
						expectedPageLists.startPage,
						Ints.checkedCast(expectedPageLists.endPage)
								- Ints.checkedCast(expectedPageLists.startPage)
								+ 1,
						true);
		Decimate ds = new Decimate(50.0,
				new FilterSpec(TimeSeriesFilterButterworth.NAME,
						FilterSpec.NO_FILTER, 3, 0.0, 100.0, 0.0,
						0.0),
				new int[0], false);

		long paddingMicros = (long) Math.ceil((1E6 / mefReader
				.getSampleRate())
				* IvProps.getPaddingSamples());

		ArrayList<TimeSeriesData> expectedTsds =
				ds.process(
						realStartTime + blockStart,
						realStartTime + blockEnd,
						mefReader.getSampleRate(),
						blockStart,
						mefReader.getVoltageConversionFactor(),
						expectedPages,
						chSpec,
						false,
						paddingMicros,
						paddingMicros);
		blockStart += blockSize;// (blockEnd - blockStart);
		// while (blockEnd < endTime) {
		while (blockStart < endOffsetWereLookingFor) {
			blockEnd = endOffsetWereLookingFor > blockStart + blockSize
					? blockStart + blockSize
					: endOffsetWereLookingFor;

			PageList newExpectedPageLists = mefReader.getPages(
					realStartTime + blockStart,
					realStartTime + blockEnd,
					Integer.MIN_VALUE);
			TimeSeriesPage[] newExpectedPages = mefReader
					.readPages(
							newExpectedPageLists.startPage,
							Ints.checkedCast(newExpectedPageLists.endPage)
									- Ints.checkedCast(newExpectedPageLists.startPage)
									+ 1,
							true);

			ArrayList<TimeSeriesData> newSegment =
					ds.process(
							realStartTime + blockStart,
							realStartTime + blockEnd,
							mefReader.getSampleRate(),
							blockStart,
							mefReader.getVoltageConversionFactor(),
							newExpectedPages,
							chSpec,
							false,
							paddingMicros,
							paddingMicros);
			expectedTsds.addAll(newSegment);
			blockStart += blockSize;// (blockEnd - blockStart);
		}

		List<Integer> mergedExpectedTsds = newArrayList();
		for (TimeSeriesData expectedTsd : expectedTsds) {
			Integer[] asArray = expectedTsd.toArray();
			mergedExpectedTsds.addAll(Arrays.asList(asArray));
		}

		for (int i = 0; i < mergedActualTsds.size(); i++) {
			List<Integer> actualTsd = mergedActualTsds.get(i);
			assertEquals(mergedExpectedTsds.size(), actualTsd.size());
			// for (int j = 0; j < mergedExpectedTsds.size(); j++) {
			// if (!(j < mergedExpectedTsds.size())) {
			// System.out.println("--" + j + " "
			// + "oor"
			// + " "
			// + actualTsd.get(j));
			// } else if (!(j < actualTsd.size())) {
			// System.out.println("--" + j + " "
			// + mergedExpectedTsds.get(j)
			// + " "
			// + "oor");
			// } else if (!equal(mergedExpectedTsds.get(j),
			// actualTsd.get(j))) {
			// System.out.println("--" + j + " "
			// + mergedExpectedTsds.get(j)
			// + " "
			// + actualTsd.get(j));
			// } else {
			// System.out.println("++" + j + " "
			// + mergedExpectedTsds.get(j) +
			// " "
			// + actualTsd.get(j));
			// }
			// }
			// System.out.println(mergedExpectedTsds.get(i));
			// System.out.println(actualTsd);
			assertEquals(mergedExpectedTsds, actualTsd);
		}
	}
}
