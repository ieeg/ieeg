/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.math.DoubleMath;

import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

/**
 * A reference implementation of Downsample
 * 
 * @author John Frommeyer
 * 
 */
public class DownsampleRef implements PostProcessor {

	private static final class TimeSeriesPageWrapper {
		final int startIdx;
		final int length;
		final TimeSeriesPage page;
		private final double startUutc;
		private final double endUutc;
		private final Optional<Integer> gapBefore;

		TimeSeriesPageWrapper(
				double periodMicros,
				TimeSeriesPage page,
				double startUutc,
				int startIdx,
				@Nullable Integer gapBefore) {
			this.page = checkNotNull(page);
			checkArgument(startIdx >= 0);
			this.startIdx = startIdx;
			this.length = this.page.values.length - startIdx;
			checkState(this.length >= 0);
			if (gapBefore == null) {
				this.gapBefore = Optional.absent();
			} else {
				checkArgument(gapBefore.intValue() >= 0);
				this.gapBefore = Optional.of(gapBefore);
			}
			checkArgument(startUutc >= 0);
			;
			checkArgument(periodMicros >= 0);
			this.startUutc = startUutc;
			this.endUutc = this.startUutc + this.length * periodMicros;

		}

		public int getLength() {
			return length;
		}

		public int get(int idx) {
			checkArgument(idx >= 0);
			return page.values[startIdx + idx];
		}

		/**
		 * @return the startUutc
		 */
		public double getStartUutc() {
			return startUutc;
		}

		/**
		 * @return the endUutc
		 */
		public double getEndUutc() {
			return endUutc;
		}

		/**
		 * @return the gapBefore
		 */
		public Optional<Integer> getGapBefore() {
			return gapBefore;
		}
	}

	private static final Logger logger = LoggerFactory
			.getLogger(DownsampleRef.class);
	private final double sampleRate;

	/**
	 * 
	 * 
	 * @param sampleRate only used by {@link getSampleRate()}
	 */
	public DownsampleRef(double sampleRate) {
		this.sampleRate = sampleRate;
	}

	// @Override
	public ArrayList<TimeSeriesData> process(
			double reqStartUutc,
			double reqEndUutc,
			double trueFreqHz,
			long startTimeOffsetMicros,
			double voltageScale,
			TimeSeriesPage[] pageList,
			@Nullable ChannelSpecifier path,
			boolean minValIsNull,
			double padBefore,
			double padAfter) {
		final String m = "process(...)";
		checkArgument(minValIsNull == false, "minValIsNull must be false");
		final long reqStartLongUutc = DoubleMath.roundToLong(reqStartUutc,
				RoundingMode.DOWN);
		final long reqEndLongUutc = DoubleMath.roundToLong(reqEndUutc,
				RoundingMode.DOWN);
		checkArgument(reqStartLongUutc <= reqEndLongUutc,
				"startTimeUutc is greater than endTimeUutc");
		checkArgument(trueFreqHz > 0);
		final double periodMicros = 1E6 / trueFreqHz;
		final int responseLength = DoubleMath.roundToInt(
				((reqEndLongUutc - reqStartLongUutc)
				* trueFreqHz)
				/ 1E6,
				RoundingMode.CEILING);
		logger.debug(
				"{}: Will return {} samples from {} pages for request from {} to {} uUTC at {} Hz",
				new Object[] {
						m,
						responseLength,
						pageList.length,
						reqStartLongUutc,
						reqEndLongUutc,
						trueFreqHz
				});

		TimeSeriesData tsd = new TimeSeriesData(
				startTimeOffsetMicros,
				periodMicros,
				voltageScale,
				responseLength);

		int nullsAdded = 0;
		int nonNullsAdded = 0;
		int samplesAdded = 0;

		// Use the wrappers to normalize all pages so that all spaces are
		// multiples of the period.
		final TimeSeriesPageWrapper[] pageWrappers = new TimeSeriesPageWrapper[pageList.length];
		for (int pageIdx = 0; pageIdx < pageList.length; pageIdx++) {
			final TimeSeriesPage page = pageList[pageIdx];
			final long pageStartUutc = page.timeStart;
			TimeSeriesPageWrapper wrapper = null;
			if (pageIdx == 0) {
				wrapper = new TimeSeriesPageWrapper(
						periodMicros,
						page,
						page.timeStart,
						0,
						null);
			} else {
				final TimeSeriesPage prevPage = pageList[pageIdx - 1];
				final long prevPageEndUutc = prevPage.timeEnd;
				final double spacePeriodsActual = (pageStartUutc - prevPageEndUutc)
						/ periodMicros;
				final int spacePeriods = DoubleMath.roundToInt(
						spacePeriodsActual,
						RoundingMode.HALF_UP);
				logger.debug(
						"{}: Actual periods between pages {} and {}: {}. Number of periods {}",
						m,
						pageToString(prevPage),
						pageToString(page),
						spacePeriodsActual,
						spacePeriods);
				if (spacePeriods >= 0) {
					final double shiftedStartUutc = prevPageEndUutc
							+ spacePeriods * periodMicros;
					wrapper = new TimeSeriesPageWrapper(
							periodMicros,
							page,
							shiftedStartUutc,
							0,
							spacePeriods);
				} else {
					// Overlap
					final double shiftedStartUutc = prevPageEndUutc;
					wrapper = new TimeSeriesPageWrapper(
							periodMicros,
							page,
							shiftedStartUutc,
							-spacePeriods,
							0);
				}
			}
			logger.debug("{}: Shifted orginal page {} to {}. A shift of {} usec and discarding {} samples",
					m,
					pageToString(page),
					pageWrapperToString(wrapper),
					wrapper.getStartUutc() - page.timeStart,
					wrapper.startIdx);
			pageWrappers[pageIdx] = wrapper;

		}

		// Find where to start
		int startPage = -1;
		int startIdx = -1;
		double prevEndUutc = Double.MIN_VALUE;

		for (int i = 0; i < pageWrappers.length; i++) {
			final TimeSeriesPageWrapper wrapper = pageWrappers[i];
			if (reqStartUutc >= prevEndUutc
					&& reqStartUutc < wrapper.getStartUutc()) {
				// reqStartUutc is between page[i-1] and page[i]
				startPage = i;
				startIdx = 0;
				// Add any required nulls if we start with a gap
				final double spaceMicros = wrapper.getStartUutc()
						- reqStartUutc;
				final int spacePeriods = DoubleMath.roundToInt(
						spaceMicros / periodMicros,
						RoundingMode.HALF_UP);
				for (int j = 0; j < spacePeriods && j < responseLength; j++) {
					tsd.addSample(null);
					samplesAdded++;
					nullsAdded++;

				}
			} else if (reqStartUutc >= wrapper.getStartUutc()
					&& reqStartUutc < wrapper.getEndUutc()) {
				// reqStartUutc is in page[i]
				startPage = i;
				for (int j = 0; j < wrapper.getLength(); j++) {
					final double sampleUutc = wrapper.getStartUutc() + j
							* periodMicros;
					if (reqStartUutc < sampleUutc) {
						double toSampleMicros = sampleUutc - reqStartUutc;
						double toPrevSampleMicros = reqStartUutc
								- wrapper.getStartUutc() + (j - 1)
								* periodMicros;
						startIdx = (toSampleMicros <= toPrevSampleMicros) ? j
								: j - 1;
						break;
					}
				}
				checkState(startIdx >= 0);
			}
			prevEndUutc = wrapper.getEndUutc();
		}

		// If we didn't find a start then our whole response should be a gap
		if (startPage < 0) {
			logger.debug("{}: No starting point found. Full gap response", m);
			while (samplesAdded < responseLength) {
				tsd.addSample(null);
				samplesAdded++;
				nullsAdded++;
			}
		} else {
			logger.debug("{}: Starting at page {} at index {}", m,
					pageToString(pageWrappers[startPage].page), startIdx
							+ pageWrappers[startPage].startIdx);
		}
		// Now fill in the response
		for (int pageIdx = startPage; pageIdx < pageWrappers.length
				&& samplesAdded < responseLength; pageIdx++) {
			final TimeSeriesPageWrapper wrapper = pageWrappers[pageIdx];

			// Take care of gap between pages
			if (pageIdx == startPage) {
				// Do not add a gap. We already took care of this when finding
				// the first page
			} else if (wrapper.getGapBefore().isPresent()) {
				final int gapLength = wrapper.getGapBefore().get().intValue();
				for (int g = 0; g < gapLength && samplesAdded < responseLength; g++) {
					tsd.addSample(null);
					samplesAdded++;
					nullsAdded++;
				}
			}

			// Now the values
			for (int s = startIdx; s < wrapper.getLength()
					&& samplesAdded < responseLength; s++) {
				tsd.addSample(wrapper.get(s));
				samplesAdded++;
				nonNullsAdded++;
			}
			startIdx = 0;

		}

		// And any trailing gap
		while (samplesAdded < responseLength) {
			tsd.addSample(null);
			samplesAdded++;
			nullsAdded++;
		}

		logger.debug(
				"{}: Returning {} samples with {} non-null and {} null for [{}, {})",
				new Object[] {
						m,
						samplesAdded,
						nonNullsAdded,
						nullsAdded,
						reqStartLongUutc,
						reqEndLongUutc
				});
		return newArrayList(tsd);

	}

/**
	 * Returns the number of nulls added to {@code tsd). Adds nulls to cover the half open interval [startUutc, endUutc).
	 * 
	 * @param startUutc
	 * @param endUutc
	 * @param periodMicros
	 * @param tsd
	 * @return
	 */
	private int addNulls(final long startUutc, final long endUutc,
			double periodMicros, TimeSeriesData tsd) {
		double nextNullUutc = startUutc;
		int nullCount = 0;
		while (nextNullUutc < endUutc) {
			tsd.addSample(null);
			nextNullUutc += periodMicros;
			nullCount++;
		}
		return nullCount;
	}

	private long findInitialNullStartUutc(long reqStartUutc,
			long pageStartUutc, double periodMicros) {
		double nullsStartUutc = pageStartUutc;
		while (reqStartUutc <= nullsStartUutc) {
			nullsStartUutc -= periodMicros;
		}
		// So now prevPageEndUutc > nullsStartUutc. Add back a period so
		// that prevPageEndUutc <= nullsStartUutc
		return DoubleMath.roundToLong(nullsStartUutc + periodMicros,
				RoundingMode.CEILING);
	}

	@Override
	public boolean isFiltered() {
		throw new UnsupportedOperationException("Operation not supported.");

	}

	@Override
	public double getSampleRate() {
		return sampleRate;

	}

	@Override
	public boolean needsWorkBuffer(int sampleCount) {
		return false;

	}

	@Override
	public void createWorkBuffer(int sampleCount) {
		throw new UnsupportedOperationException("Operation not supported.");

	}

	@Override
	public void setWorkBuffer(int[] buffer) {
		throw new UnsupportedOperationException("Operation not supported.");

	}

	public ArrayList<TimeSeriesData> process2(
			double reqStartUutc,
			double reqEndUutc,
			double trueFreqHz,
			long startTimeOffsetMicros,
			double voltageScale,
			TimeSeriesPage[] pageList,
			@Nullable ChannelSpecifier path,
			boolean minValIsNull,
			double padBefore,
			double padAfter) {
		final String m = "process(...)";
		checkArgument(minValIsNull == false, "minValIsNull must be false");
		checkArgument(reqStartUutc <= reqEndUutc,
				"startTimeUutc is greater than endTimeUutc");
		final double periodMicros = 1E6 / trueFreqHz;
		final int responseLength = DoubleMath.roundToInt(
				((reqEndUutc - reqStartUutc)
				* trueFreqHz)
				/ 1E6,
				RoundingMode.CEILING);
		logger.debug(
				"{}: Will return {} samples from {} pages for request [{}, {}) uUTC at {} Hz",
				new Object[] {
						m,
						responseLength,
						pageList.length,
						reqStartUutc,
						reqEndUutc,
						trueFreqHz
				});

		TimeSeriesData tsd = new TimeSeriesData(
				startTimeOffsetMicros,
				periodMicros,
				voltageScale,
				responseLength);

		final ImmutableSortedMap.Builder<Double, Optional<Integer>> builder = ImmutableSortedMap
				.naturalOrder();

		// Fill in any gaps before and after the pages we have
		for (double gapUutc = reqStartUutc; gapUutc < pageList[0].timeStart; gapUutc += periodMicros) {
			builder.put(
					Double.valueOf(gapUutc),
					Optional.<Integer> absent());
		}
		for (double gapUutc = pageList[pageList.length - 1].timeEnd; gapUutc < reqEndUutc; gapUutc += periodMicros) {
			builder.put(
					Double.valueOf(gapUutc),
					Optional.<Integer> absent());
		}
		double runningUutc = pageList[0].timeStart;
		for (int pageNo = 0; pageNo < pageList.length; pageNo++) {
			final TimeSeriesPage page = pageList[pageNo];
			final int[] values = page.values;
			double pageUutc = page.timeStart;
			logger.debug(
					"{}: At start of page: running uUTC: {}, page uUTC: {}, running - page: {}",
					new Object[] {
							m,
							runningUutc,
							pageUutc,
							runningUutc - pageUutc
					});
			for (int i = 0; i < values.length; i++) {
				builder.put(Double.valueOf(pageUutc),
						Optional.of(Integer.valueOf(values[i])));
				pageUutc += periodMicros;
				runningUutc += periodMicros;
			}
			logger.debug(
					"{}: At end of page: running uUTC: {}, page uUTC: {}, running - page: {}",
					new Object[] {
							m,
							runningUutc,
							pageUutc,
							runningUutc - pageUutc
					});

			if (pageNo < pageList.length - 1) {
				final TimeSeriesPage nextPage = pageList[pageNo + 1];
				final double pageEndUutc = page.timeEnd;
				final double nextPageStartUutc = nextPage.timeStart;
				logger.debug(
						"{}: Page end from page uUTC: {}, page end calculated from page start uUTC: {}, page - calculated: {}",
						new Object[] {
								m,
								pageEndUutc,
								pageUutc,
								pageEndUutc - pageUutc
						});

				if (nextPageStartUutc - pageEndUutc >= periodMicros) {
					double gapUutc = pageEndUutc;
					while (gapUutc < nextPageStartUutc) {
						builder.put(Double.valueOf(gapUutc),
								Optional.<Integer> absent());
						gapUutc += periodMicros;
						runningUutc += periodMicros;
					}
					logger.debug(
							"{}: Gap between pages {} and {} from [{}, {})",
							new Object[] {
									m,
									pageNo,
									pageNo + 1,
									pageEndUutc,
									gapUutc
							});
				} else if (nextPageStartUutc < pageEndUutc) {
					throw new IllegalArgumentException(
							"Overlapping pages: pageEndUutc: "
									+ pageEndUutc + ", nextPageStartUutc: "
									+ nextPageStartUutc + ", overlap of "
									+ (pageEndUutc - nextPageStartUutc));
				} else {
					// Do nothing. Consider the pages continuous.
					logger.debug("{}: Pages {} and {} continous", new Object[] {
							m,
							pageNo,
							pageNo + 1
					});
				}
			}
		}
		final ImmutableSortedMap<Double, Optional<Integer>> uUtcToValue = builder
				.build();
		final SortedMap<Double, Optional<Integer>> responseMap = uUtcToValue
				.subMap(Double.valueOf(reqStartUutc),
						Double.valueOf(reqEndUutc));
		int samplesAdded = 0;
		int nullsAdded = 0;

		checkState(responseMap.size() <= responseLength,
				"Found more samples than we wanted");
		for (Map.Entry<Double, Optional<Integer>> entry : responseMap
				.entrySet()) {
			final Optional<Integer> value = entry.getValue();
			if (value.isPresent()) {
				tsd.addSample(value.get());
			} else {
				tsd.addSample(null);
				nullsAdded++;
			}
			samplesAdded++;
		}
		int endNulls = responseLength - samplesAdded;
		for (int k = 0; k < endNulls; k++) {
			tsd.addSample(null);
			samplesAdded++;
			nullsAdded++;
		}
		if (endNulls > 0) {
			logger.debug(
					"{}: Added {} nulls to end of response. Request endpoints: [{}, {}). responseMap endpoints: [{}, {})",
					m,
					endNulls,
					reqStartUutc,
					reqEndUutc,
					responseMap.firstKey(),
					responseMap.lastKey());
		}
		logger.debug(
				"{}: Returning {} samples with {} null for [{}, {})",
				new Object[] {
						m,
						samplesAdded,
						nullsAdded,
						reqStartUutc,
						reqEndUutc
				});

		return newArrayList(tsd);

	}

	@Override
	public int getWorkBufferSize() {
		return 0;
	}

	// @Override
	public ArrayList<TimeSeriesData> processAllSamplesInClosedOpenInterval(
			double reqStartUutc,
			double reqEndUutc,
			double trueFreqHz,
			long startTimeOffsetMicros,
			double voltageScale,
			TimeSeriesPage[] pageList,
			@Nullable ChannelSpecifier path,
			boolean minValIsNull,
			double padBefore,
			double padAfter) {
		final String m = "processAllSamplesInClosedOpenInterval(...)";
		checkArgument(minValIsNull == false, "minValIsNull must be false");
		final long reqStartLongUutc = DoubleMath.roundToLong(reqStartUutc,
				RoundingMode.DOWN);
		final long reqEndLongUutc = DoubleMath.roundToLong(reqEndUutc,
				RoundingMode.DOWN);
		checkArgument(reqStartLongUutc <= reqEndLongUutc,
				"startTimeUutc is greater than endTimeUutc");
		final double periodMicros = 1E6 / trueFreqHz;
		final int responseLength = DoubleMath.roundToInt(
				((reqEndLongUutc - reqStartLongUutc)
				* trueFreqHz)
				/ 1E6,
				RoundingMode.CEILING);
		logger.debug(
				"{}: Will return {} samples from {} pages for request [{}, {}) uUTC at {} Hz",
				new Object[] {
						m,
						responseLength,
						pageList.length,
						reqStartLongUutc,
						reqEndLongUutc,
						trueFreqHz
				});

		TimeSeriesData tsd = new TimeSeriesData(
				startTimeOffsetMicros,
				periodMicros,
				voltageScale,
				responseLength);

		int nullsAdded = 0;
		int nonNullsAdded = 0;
		int samplesAdded = 0;
		long firstSampleUutc = -1;

		long prevPageEndUutc = -1;

		// Find the first page we need and the first index in that page.
		int firstPage = -1;
		int startIdxInPage = -1;

		for (int i = 0; i < pageList.length; i++) {
			final TimeSeriesPage page = pageList[i];
			final long pageStartUutc = page.timeStart;
			final long pageEndUutc = page.timeEnd;

			if ((prevPageEndUutc == -1 || prevPageEndUutc <= reqStartLongUutc)
					&& reqStartLongUutc < pageStartUutc) {
				// Request starts between page i-1 and page i. Need to start
				// with nulls.
				firstPage = i;
				startIdxInPage = 0;
				final long nullsStartUutc = findInitialNullStartUutc(
						reqStartLongUutc, pageStartUutc, periodMicros);
				firstSampleUutc = nullsStartUutc;
				final long nullsEndUutc = Math.min(pageStartUutc,
						reqEndLongUutc);
				logger.debug(
						"{}: Found gap for [{}, {})",
						new Object[] {
								m,
								nullsStartUutc,
								nullsEndUutc
						});
				final int nullCount = addNulls(
						nullsStartUutc,
						nullsEndUutc,
						periodMicros,
						tsd);
				nullsAdded += nullCount;
				samplesAdded += nullCount;

				logger.debug(
						"{}: Added {} nulls to start of response for section [{}, {})",
						new Object[] {
								m,
								nullCount,
								nullsStartUutc,
								nullsEndUutc
						});
				break;
			} else if (pageStartUutc <= reqStartLongUutc
					&& reqStartLongUutc < pageEndUutc) {
				// Request starts in page i
				firstPage = i;
				// Use ceiling because if we land strictly between samples we
				// want to take the one above us as the beginning.
				startIdxInPage = DoubleMath
						.roundToInt(
								((reqStartLongUutc - pageStartUutc) * trueFreqHz) / 1E6,
								RoundingMode.CEILING);
				firstSampleUutc = DoubleMath.roundToLong(pageStartUutc
						+ startIdxInPage * periodMicros, RoundingMode.CEILING);
				break;
			}
			prevPageEndUutc = pageEndUutc;
		}
		logger.debug(
				"{}: Calculated uUTC timestamp of first sample in response is {}",
				m, firstSampleUutc);

		if (firstPage == -1) {
			// The request must start after all of the pages, so return a gap.
			final int nullCount = addNulls(reqStartLongUutc, reqEndLongUutc,
					periodMicros, tsd);
			nullsAdded += nullCount;
			samplesAdded += nullCount;

			logger.debug(
					"{}: Pages do not contain request range. Returning a gap of length {} for [{}, {})",
					new Object[] {
							m,
							samplesAdded,
							reqStartLongUutc,
							reqEndLongUutc
					});
			return newArrayList(tsd);
		}
		if (samplesAdded == responseLength) {// We are done. Whole request was a
												// gap.
			logger.debug("{}: Returning a gap of length {} for [{}, {})",
					new Object[] {
							m,
							responseLength,
							reqStartLongUutc,
							reqEndLongUutc });
			return newArrayList(tsd);
		}

		double nextSampleUutc = firstSampleUutc + periodMicros * samplesAdded;
		for (int i = firstPage; samplesAdded < responseLength
				&& i < pageList.length; i++) {
			final TimeSeriesPage page = pageList[i];
			final int pageLength = page.values.length;
			final long pageStartUutc = page.timeStart;
			final long pageEndUutc = page.timeEnd;

			final long sampleStartInPageUutc = DoubleMath.roundToLong(
					pageStartUutc + periodMicros * startIdxInPage,
					RoundingMode.CEILING);
			final long calcedPageStartOverallUutc = DoubleMath.roundToLong(
					firstSampleUutc + periodMicros * samplesAdded,
					RoundingMode.CEILING);
			logger.debug(
					"{}: calculated first sample time of page from page start: {}, calculated first sample time from first response sample: {}, overall - page: {}",
					new Object[] {
							m,
							sampleStartInPageUutc,
							calcedPageStartOverallUutc,
							calcedPageStartOverallUutc - sampleStartInPageUutc });

			int j = startIdxInPage;
			for (; j < page.values.length && samplesAdded < responseLength; j++) {
				tsd.addSample(page.values[j]);
				samplesAdded++;
				nonNullsAdded++;
				nextSampleUutc += periodMicros;
			}
			logger.debug(
					"{}: Adding values from page {} of length {}. Indices [{}, {}]",
					new Object[] {
							m,
							i,
							pageLength,
							startIdxInPage,
							j - 1
					});

			if (i < pageList.length - 1) {// Not last page
				final TimeSeriesPage nextPage = pageList[i + 1];
				final long nextPageStartUutc = nextPage.timeStart;
				startIdxInPage = 0;
				if (pageEndUutc > nextPageStartUutc) {
					logger.warn(
							"{}: Page {}"
									+ " end uUTC: {}"
									+ " is after next page's start uUTC: {}. Next sample uUTC: {}",
							new Object[] {
									m,
									i,
									pageEndUutc,
									nextPageStartUutc,
									nextSampleUutc
							});
					double realPageStartUutc = nextPageStartUutc;
					while (nextSampleUutc > realPageStartUutc) {
						startIdxInPage++;
						realPageStartUutc += periodMicros;
					}
				} else if ((nextPageStartUutc - pageEndUutc) >= periodMicros) {// A
																				// gap
					final long nullsEndUutc = Math.min(nextPageStartUutc,
							reqEndLongUutc);
					final int nullCount = addNulls(
							pageEndUutc,
							nullsEndUutc,
							periodMicros,
							tsd);
					nullsAdded += nullCount;
					samplesAdded += nullCount;
					nextSampleUutc += (periodMicros * nullCount);
					logger.debug(
							"{}: Added {} nulls to response for section [{}, {})",
							new Object[] {
									m,
									nullCount,
									pageEndUutc,
									nullsEndUutc
							});
				}
			}
		}
		int endNulls = responseLength - samplesAdded;
		for (int k = 0; k < endNulls; k++) {
			tsd.addSample(null);
			samplesAdded++;
			nullsAdded++;
			samplesAdded += periodMicros;
		}
		if (endNulls > 0) {
			logger.debug("{}: Added {} nulls to end of response", m, endNulls);
		}
		logger.debug(
				"{}: Returning {} samples with {} non-null and {} null for [{}, {})",
				new Object[] {
						m,
						samplesAdded,
						nonNullsAdded,
						nullsAdded,
						reqStartLongUutc,
						reqEndLongUutc
				});
		return newArrayList(tsd);

	}

	private static String pageToString(TimeSeriesPage page) {
		return "{ ["
				+ page.timeStart
				+ ", "
				+ page.timeEnd
				+ ") "
				+ page.values.length
				+ " values }";
	}

	private static String pageWrapperToString(TimeSeriesPageWrapper wrapper) {
		return "{ ["
				+ wrapper.getStartUutc()
				+ ", "
				+ wrapper.getEndUutc()
				+ ") "
				+ wrapper.getLength()
				+ " values }";
	}

}
