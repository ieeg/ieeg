/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.server.FilterManager.FilterBuffer;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

/**
 * @author John Frommeyer
 *
 */
public class FilterManagerLinearInterpolateTest {

	private final double trueFreqHz = 2500;
	private final int pageSize = (int) trueFreqHz;
	private final double scale = 1;
	private final TimeSeriesPage[] pageList = new TimeSeriesPage[2];
	private ChannelSpecifier path;
	
	//A < 1Hz set of pages
	private final double smallHzTrueFreqHz = 1.0/15;
	private final int smallHzPageSize = 1;
	private final TimeSeriesPage[] smallHzPageList = new TimeSeriesPage[2];
	private ChannelSpecifier smallHzPath;

	@Before
	public void createPages() {

		// 2500 Hz, one second per page. Value == index for ease of debugging.
		pageList[0] = new TimeSeriesPage();
		pageList[0].timeStart = 1_000_000;
		pageList[0].timeEnd = 2_000_000;
		pageList[0].values = new int[pageSize];
		int i = 0;
		while (i < pageList[0].values.length) {
			pageList[0].values[i] = i;
			i++;
		}
		pageList[1] = new TimeSeriesPage();
		pageList[1].timeStart = 2_000_000;
		pageList[1].timeEnd = 3_000_000;
		pageList[1].values = new int[pageSize];
		for (int j = 0; j < pageList[1].values.length; j++) {
			pageList[1].values[j] = i++;
		}

		final MEFSnapshotSpecifier ss = new MEFSnapshotSpecifier(
				"linearInt-1",
				BtUtil.newUuid());
		path = new MEFPageServer.MEFChannelSpecifier(
				ss,
				"linearInt-1-Channel1",
				BtUtil.newUuid(),
				0,
				"1");
	}
	
	@Before
	public void createSmallHzPages() {

		// 2500 Hz, one second per page. Value == index for ease of debugging.
		smallHzPageList[0] = new TimeSeriesPage();
		smallHzPageList[0].timeStart = 15_000_000;
		smallHzPageList[0].timeEnd = 30_000_000;
		smallHzPageList[0].values = new int[smallHzPageSize];
		int i = 0;
		while (i < smallHzPageList[0].values.length) {
			smallHzPageList[0].values[i] = i;
			i++;
		}
		smallHzPageList[1] = new TimeSeriesPage();
		smallHzPageList[1].timeStart = 30_000_000;
		smallHzPageList[1].timeEnd = 45_000_000;
		smallHzPageList[1].values = new int[smallHzPageSize];
		for (int j = 0; j < smallHzPageList[1].values.length; j++) {
			smallHzPageList[1].values[j] = i++;
		}

		final MEFSnapshotSpecifier ss = new MEFSnapshotSpecifier(
				"linearInt-smallHz",
				BtUtil.newUuid());
		smallHzPath = new MEFPageServer.MEFChannelSpecifier(
				ss,
				"linearInt-smallHz-Channel1",
				BtUtil.newUuid(),
				0,
				"1");
	}

	@Test
	public void smallHzDecimateLinearInterpolate() {
		// request start time == page start time 
		double startTimeUutc = 15e6;
		double endTimeUutc = 37500000;
		double samplingPeriodMicros = 10000;

		testFirstSample(startTimeUutc, endTimeUutc, samplingPeriodMicros, smallHzTrueFreqHz, smallHzPageList, smallHzPath);
	}
	
	@Test
	public void decimateLinearInterpolate() {
		// request start time == page start time and request period a multiple
		// of the recording period
		double startTimeUutc = 1e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 1600;

		testFirstSample(startTimeUutc, endTimeUutc, samplingPeriodMicros, trueFreqHz, pageList, path);
	}

	@Test
	public void decimateLinearInterpolateNonMultiplePeriod() {
		// request start time == page start time and request period not a
		// multiple of the recording period
		double startTimeUutc = 1e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 900;

		testFirstSample(startTimeUutc, endTimeUutc, samplingPeriodMicros, trueFreqHz, pageList, path);
	}

	@Test
	public void decimateLinearInterpolateMiddleOfPage() {
		// request start time > page start time and request period a multiple
		// of the recording period
		double startTimeUutc = 1.5e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 800;

		testFirstSample(startTimeUutc, endTimeUutc, samplingPeriodMicros, trueFreqHz, pageList, path);
	}

	@Test
	public void decimateLinearInterpolateMiddleOfPageNonMultiplePeriod() {
		// request start time > page start time and request period not a
		// multiple of the recording period
		double startTimeUutc = 1.5e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 900;

		testFirstSample(startTimeUutc, endTimeUutc, samplingPeriodMicros, trueFreqHz, pageList, path);
	}

	private void testFirstSample(
			double startTimeUutc,
			double endTimeUutc,
			double samplingPeriodMicros, 
			double recordingFreqHz, 
			TimeSeriesPage[] pageList, 
			ChannelSpecifier chSpec) {
		final FilterSpec filter = new FilterSpec();
		long startTimeOffsetMicros = (long) startTimeUutc;
		double filterPad = (long) Math.ceil(samplingPeriodMicros
				* IvProps.PADDING_SAMPLES_DFLT);
		FilterBuffer workSpace = new FilterManager.FilterBuffer(
				(int) ((endTimeUutc
						- startTimeUutc + filterPad + filterPad) / recordingFreqHz) + 1);

		final ArrayList<TimeSeriesData> actualList = FilterManager
				.decimateLinearInterpolate(
						startTimeUutc,
						endTimeUutc,
						recordingFreqHz,
						startTimeOffsetMicros,
						samplingPeriodMicros,
						scale,
						pageList,
						filter,
						chSpec,
						false,
						workSpace,
						filterPad,
						filterPad);
		assertEquals(1, actualList.size());
		final TimeSeriesData actual = actualList.get(0);
		assertEquals(
				(int) Math.ceil((endTimeUutc - startTimeUutc)
						/ samplingPeriodMicros), actual.getSeriesLength());

		int indexOfExpected = (int) Math
				.ceil(((startTimeUutc - pageList[0].timeStart) * recordingFreqHz) / 1e6);
		int[] pageAndIndexOfExpected = getPageAndIndexOfExpected(0,
				indexOfExpected);

		int pageOfExpected = pageAndIndexOfExpected[0];
		indexOfExpected = pageAndIndexOfExpected[1];

		final int expectedValue = pageList[pageOfExpected].values[indexOfExpected];
		final int actualValue = actual.getAt(0);
		assertEquals(expectedValue,
				actualValue);
	}

	private int[] getPageAndIndexOfExpected(int currentPage,
			int unnormalizedIndex) {
		assertTrue(currentPage == 0 || currentPage == 1);
		assertTrue(unnormalizedIndex < pageSize * 2);
		final int[] pageAndIndex = new int[2];
		if (unnormalizedIndex < pageSize) {
			pageAndIndex[0] = currentPage;
			pageAndIndex[1] = unnormalizedIndex;
			return pageAndIndex;
		} else {
			if (currentPage == 0) {
				pageAndIndex[0] = 1;
				pageAndIndex[1] = unnormalizedIndex % pageSize;
				return pageAndIndex;
			} else {
				throw new AssertionError("Number of pages exceeded");
			}
		}
	}
}
