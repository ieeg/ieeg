/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newTreeSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;
import org.jboss.resteasy.test.TestPortProvider;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import edu.upenn.cis.CompareUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.server.IEEGServices;
import edu.upenn.cis.db.mefview.shared.IHasName;

public class TimeSeriesInterfaceTest {
	private static UndertowJaxrsServer server;

	private SessionFactory sessFac = HibernateUtil.getSessionFactory();
	private TstObjectFactory tstObjectFactory;
	private String authedUserPlaintextPassword = newUuid();
	private User authedUser;
	private String studyId;
	private String channel1Id;
	private String channel2Id;
	private String channel3Id;

	@BeforeClass
	public static void startServer() throws Exception
	{
		server = new UndertowJaxrsServer().start();
	}

	@AfterClass
	public static void stopServer() throws Exception
	{
		server.stop();
	}

	@Before
	public void testSetUp() throws Exception {

		// we need to initialize IvProps for DataSnapshotServerFactory
		IvProps.setIvProps(Collections.<String, String> emptyMap());

		SessionAndTrx sessAndTrx = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session sess = sessAndTrx.session;

			IPermissionDAO permissionDAO = new PermissionDAOHibernate(sess);
			tstObjectFactory = new TstObjectFactory(
					permissionDAO.findStarCoreReadPerm(),
					permissionDAO.findStarCoreOwnerPerm());

			final TestDbCleaner cleaner = new TestDbCleaner(
					sessFac,
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();

			authedUser = tstObjectFactory
					.newUserRegularEnabled(authedUserPlaintextPassword);

			IUserService userService = Mockito.mock(IUserService.class);
			when(userService.findUserByUsername(authedUser.getUsername()))
					.thenReturn(
							authedUser);
			when(userService.findExistingEnabledUser(authedUser.getUsername()))
					.thenReturn(authedUser);
			UserServiceFactory.setUserService(userService);

			UserEntity authedUserEntity = tstObjectFactory
					.newUserEntity(authedUser.getUserId());
			sess.saveOrUpdate(authedUserEntity);
			ExtAclEntity studyExtAcl = tstObjectFactory
					.newExtAclEntityUserOwnedAndWorldReadable(authedUserEntity);

			Patient patient = tstObjectFactory.newPatient();
			sess.saveOrUpdate(patient);
			HospitalAdmission admission = tstObjectFactory
					.newHospitalAdmission();
			patient.addAdmission(admission);
			EegStudy study = tstObjectFactory.newEegStudy(studyExtAcl);
			admission.addStudy(study);
			studyId = study.getPubId();

			Recording recording = study.getRecording();
			ContactGroup contactGroup = tstObjectFactory.newContactGroup();
			recording.addContactGroup(contactGroup);

			TimeSeriesEntity ts1 = tstObjectFactory.newTimeSeries();
			Contact contact1 = tstObjectFactory.newContact(ts1);
			contactGroup.addContact(contact1);
			channel1Id = ts1.getPubId();

			TimeSeriesEntity ts2 = tstObjectFactory.newTimeSeries();
			Contact contact2 = tstObjectFactory.newContact(ts2);
			contactGroup.addContact(contact2);
			channel2Id = ts2.getPubId();

			TimeSeriesEntity ts3 = tstObjectFactory.newTimeSeries();
			Contact contact3 = tstObjectFactory.newContact(ts3);
			contactGroup.addContact(contact3);
			channel3Id = ts3.getPubId();

			PersistenceUtil.commit(sessAndTrx);
		} catch (Exception e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
		}
	}

//	@Test
//	public void createMontageDataSnapshotNotFound() throws URISyntaxException {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//		Set<MontagedChannel> montagedChannels = newHashSet();
//		Montage montage = new Montage(
//				newUuid(),
//				montagedChannels);
//		IeegWsRemoteAppException expectedException = null;
//		try {
//			tsi.createMontage(
//					newUuid(),
//					montage);
//		} catch (IeegWsRemoteAppException e) {
//			expectedException = e;
//		}
//		assertNotNull(expectedException);
//		assertEquals(
//				IeegWsError.NO_SUCH_DATA_SNAPSHOT.getCode(),
//				expectedException.getErrorCode());
//
//	}
//
//	@Test
//	public void createMontage() throws URISyntaxException {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Montage expectedMontage = newMontageWithChannels();
//
//		Montage actualMontage = tsi.createMontage(
//				studyId,
//				expectedMontage);
//
//		assertEquals(
//				studyId,
//				actualMontage.getDatasetId());
//		compareRequestResponseMontages(
//				expectedMontage,
//				actualMontage);
//	}
//
//	@Test
//	public void getMontageInfos() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Set<Montage> expectedByName = newTreeSet(new IHasName.NameComparator());
//		Montage expectedMontage1 = newRefMontageAllChannels();
//		expectedByName.add(expectedMontage1);
//		Montage expectedMontage2 = newMontageWithChannels();
//		expectedByName.add(expectedMontage2);
//
//		tsi.createMontage(
//				studyId,
//				expectedMontage1);
//		tsi.createMontage(
//				studyId,
//				expectedMontage2);
//
//		MontageInfos actualMontageInfos = tsi.getMontageInfos(studyId);
//		assertEquals(2, actualMontageInfos.getMontageInfos().size());
//		int i = -1;
//		for (Montage expectedMontage : expectedByName) {
//			i++;
//			MontageInfo actualMontageInfo = get(
//					actualMontageInfos.getMontageInfos(),
//					i,
//					null);
//			assertNotNull(actualMontageInfo);
//			assertEquals(
//					expectedMontage.getName(),
//					actualMontageInfo.getName());
//			assertEquals(
//					studyId,
//					actualMontageInfo.getDatasetId());
//			assertNotNull(actualMontageInfo.getId());
//			assertNotNull(actualMontageInfo.getETag());
//		}
//	}
//
//	@Test
//	public void getMontageInfosDataSnapshotNotFound() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//		IeegWsRemoteAppException expectedException = null;
//		try {
//			tsi.getMontageInfos("xyz");
//		} catch (IeegWsRemoteAppException e) {
//			expectedException = e;
//			assertEquals(
//					IeegWsError.NO_SUCH_DATA_SNAPSHOT.getCode(),
//					e.getErrorCode());
//		}
//		assertNotNull(expectedException);
//	}
//
	@Test
	public void getDataSnapshotIdByNameDataSnapshotNotFound() {
		TimeSeriesInterface tsi = setUpServerGetTsi();
		IeegWsRemoteAppException expectedException = null;
		try {
			tsi.getDataSnapshotIdByName("xyz");
		} catch (IeegWsRemoteAppException e) {
			expectedException = e;
			assertEquals(
					IeegWsError.NO_SUCH_DATA_SNAPSHOT.getCode(),
					e.getErrorCode());
		}
		assertNotNull(expectedException);
	}
//
//	@Test
//	public void getMontage() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Montage expectedMontage1 = newRefMontageAllChannels();
//		Montage expectedMontage2 = newMontageWithChannels();
//		Montage expectedMontage3 = newMontageWithChannels();
//
//		Montage createMontage1Resp = tsi.createMontage(
//				studyId,
//				expectedMontage1);
//		Montage createMontage2Resp = tsi.createMontage(
//				studyId,
//				expectedMontage2);
//		Montage createMontage3Resp = tsi.createMontage(
//				studyId,
//				expectedMontage3);
//
//		Montage actualMontage1 = tsi.getMontage(createMontage1Resp.getId());
//		assertEquals(
//				createMontage1Resp.getId(),
//				actualMontage1.getId());
//		assertNotNull(actualMontage1.getETag());
//		assertEquals(
//				createMontage1Resp.getETag(),
//				actualMontage1.getETag());
//		assertEquals(
//				studyId,
//				actualMontage1.getDatasetId());
//		compareRequestResponseMontages(
//				expectedMontage1,
//				actualMontage1);
//
//		Montage actualMontage2 = tsi.getMontage(createMontage2Resp.getId());
//		assertEquals(
//				createMontage2Resp.getId(),
//				actualMontage2.getId());
//		assertNotNull(actualMontage2.getETag());
//		assertEquals(
//				createMontage2Resp.getETag(),
//				actualMontage2.getETag());
//		assertEquals(
//				studyId,
//				actualMontage2.getDatasetId());
//		compareRequestResponseMontages(
//				expectedMontage2,
//				actualMontage2);
//
//		Montage actualMontage3 = tsi.getMontage(createMontage3Resp.getId());
//		assertEquals(
//				createMontage3Resp.getId(),
//				actualMontage3.getId());
//		assertNotNull(actualMontage3.getETag());
//		assertEquals(
//				createMontage3Resp.getETag(),
//				actualMontage3.getETag());
//		assertEquals(
//				studyId,
//				actualMontage3.getDatasetId());
//		compareRequestResponseMontages(
//				expectedMontage3,
//				actualMontage3);
//
//	}
//
//	@Test
//	public void deleteMontage() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Montage montage1 = newRefMontageAllChannels();
//		Montage montage2 = newMontageWithChannels();
//		Montage montage3 = newMontageWithChannels();
//		String toBeDeletedName = montage2.getName();
//
//		tsi.createMontage(
//				studyId,
//				montage1);
//		tsi.createMontage(
//				studyId,
//				montage2);
//		tsi.createMontage(
//				studyId,
//				montage3);
//
//		MontageInfos beforeDeleteInfos = tsi.getMontageInfos(studyId);
//		MontageInfo toBeDeletedInfo = null;
//		for (MontageInfo info : beforeDeleteInfos.getMontageInfos()) {
//			if (info.getName().equals(toBeDeletedName)) {
//				toBeDeletedInfo = info;
//				tsi.deleteMontage(
//						toBeDeletedInfo.getId(),
//						toBeDeletedInfo.getETag());
//			}
//		}
//
//		MontageInfos afterDeleteInfos = tsi.getMontageInfos(studyId);
//		assertEquals(
//				beforeDeleteInfos.getMontageInfos().size() - 1,
//				afterDeleteInfos.getMontageInfos().size());
//
//		for (MontageInfo info : afterDeleteInfos.getMontageInfos()) {
//			assertTrue(!toBeDeletedInfo.getId().equals(info.getId()));
//		}
//
//	}
//
//	@Test
//	public void deleteMontageNoETag() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Montage montage1 = newRefMontageAllChannels();
//		Montage montage2 = newMontageWithChannels();
//		Montage montage3 = newMontageWithChannels();
//		String toBeDeletedName = montage2.getName();
//
//		tsi.createMontage(
//				studyId,
//				montage1);
//		tsi.createMontage(
//				studyId,
//				montage2);
//		tsi.createMontage(
//				studyId,
//				montage3);
//
//		MontageInfos beforeDeleteInfos = tsi.getMontageInfos(studyId);
//		MontageInfo toBeDeletedInfo = null;
//		for (MontageInfo info : beforeDeleteInfos.getMontageInfos()) {
//			if (info.getName().equals(toBeDeletedName)) {
//				toBeDeletedInfo = info;
//				tsi.deleteMontage(
//						toBeDeletedInfo.getId(),
//						null);
//			}
//		}
//
//		MontageInfos afterDeleteInfos = tsi.getMontageInfos(studyId);
//		assertEquals(
//				beforeDeleteInfos.getMontageInfos().size() - 1,
//				afterDeleteInfos.getMontageInfos().size());
//
//		for (MontageInfo info : afterDeleteInfos.getMontageInfos()) {
//			assertTrue(!toBeDeletedInfo.getId().equals(info.getId()));
//		}
//
//	}
//
//	@Test
//	public void createLayer() {
//		TimeSeriesInterface tsi = setUpServerGetTsi();
//
//		Montage montage = newMontageWithChannels();
//		Montage createMontageResp = tsi.createMontage(
//				studyId,
//				montage);
//
//		String layerName = newUuid();
//		Layer createLayerResp = tsi.createLayer(
//				createMontageResp.getId(),
//				layerName);
//
//		assertNotNull(createLayerResp.getId());
//		assertEquals(
//				layerName,
//				createLayerResp.getName());
//		assertEquals(
//				0,
//				createLayerResp.getAnnotationCount().intValue());
//
//	}

//	private void compareRequestResponseMontages(
//			Montage request,
//			Montage response) {
//		assertEquals(
//				request.getName(),
//				response.getName());
//		assertEquals(
//				request.getMontagedChannels().size(),
//				response.getMontagedChannels().size());
//		assertNotNull(response.getId());
//		assertNotNull(response.getETag());
//		CompareUtil compareUtil = new CompareUtil();
//
//		// Montage orders its channels by name.
//		int i = -1;
//		for (MontagedChannel requestMontagedCh : request
//				.getMontagedChannels()) {
//			i++;
//			MontagedChannel reponseMontagedCh = get(
//					response.getMontagedChannels(),
//					i,
//					null);
//			assertNotNull(reponseMontagedCh);
//			assertNotNull(reponseMontagedCh.getId());
//			compareUtil
//					.compareNonNullable(
//							requestMontagedCh,
//							reponseMontagedCh);
//		}
//	}

//	private Montage newMontageWithChannels() {
//		final String montageName = newUuid();
//
//		Set<MontagedChannel> montagedChannels = newHashSet();
//		MontagedChannel montagedCh1 = new MontagedChannel(
//				newUuid(),
//				channel1Id,
//				Collections.<ReferenceChannel> emptySet());
//		montagedChannels.add(montagedCh1);
//
//		MontagedChannel montagedCh2 = new MontagedChannel(
//				newUuid(),
//				channel1Id,
//				newHashSet(
//				new ReferenceChannel(
//						channel3Id,
//						-1.0)));
//		montagedChannels.add(montagedCh2);
//
//		MontagedChannel montagedCh3 = new MontagedChannel(
//				newUuid(),
//				channel2Id,
//				newHashSet(
//				new ReferenceChannel(
//						channel3Id,
//						-1.0)));
//		montagedChannels.add(montagedCh3);
//
//		MontagedChannel montagedCh4 = new MontagedChannel(
//				newUuid(),
//				channel3Id,
//				newHashSet(
//						new ReferenceChannel(
//								channel1Id,
//								tstObjectFactory.randomDouble()),
//						new ReferenceChannel(
//								channel2Id,
//								tstObjectFactory.randomDouble())));
//		montagedChannels.add(montagedCh4);
//
//		Montage montage = new Montage(
//				montageName,
//				montagedChannels);
//
//		return montage;
//	}
//
//	private Montage newRefMontageAllChannels() {
//		final String montageName = newUuid();
//
//		Set<MontagedChannel> montagedChannels = newHashSet();
//		MontagedChannel montagedCh1 = new MontagedChannel(
//				newUuid(),
//				channel1Id,
//				Collections.<ReferenceChannel> emptySet());
//		montagedChannels.add(montagedCh1);
//
//		MontagedChannel montagedCh2 = new MontagedChannel(
//				newUuid(),
//				channel2Id,
//				Collections.<ReferenceChannel> emptySet());
//		montagedChannels.add(montagedCh2);
//
//		MontagedChannel montagedCh3 = new MontagedChannel(
//				newUuid(),
//				channel3Id,
//				Collections.<ReferenceChannel> emptySet());
//		montagedChannels.add(montagedCh3);
//
//		Montage montage = new Montage(
//				montageName,
//				montagedChannels);
//
//		return montage;
//	}

	private TimeSeriesInterface setUpServerGetTsi() {
		server.deploy(IEEGServices.class);
		String url = TestPortProvider.generateBaseUrl();

		TimeSeriesInterface tsi = new TimeSeriesInterface(
				url,
				authedUser.getUsername(),
				authedUserPlaintextPassword);
		return tsi;
	}
}
