/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Objects.equal;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static edu.upenn.cis.braintrust.BtUtil.getBoolean;
import static edu.upenn.cis.braintrust.BtUtil.getDouble;
import static edu.upenn.cis.braintrust.BtUtil.getInt;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import net.sf.ehcache.CacheManager;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.Downsample;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.shared.IUser.Status;
import edu.upenn.cis.db.mefview.testhelper.S3FromFileSystem;

/**
 * @author Sam Donnelly
 * 
 */
@Ignore("Requires MEF file")
public class MEFPageServerS3LongDownsampleTest {

	@BeforeClass
	public static void classSetup() {
		Map<String, String> props = newHashMap();
		props.put(IvProps.MEF_INDEX_SLICE_NO_ENTRIES, "100");
		IvProps.setIvProps(props);
	}

	private static final String MEF_FILE = MEFPageServerS3LongDownsampleTest.class
			.getClassLoader()
			.getResource("LTD1.mef")
			.getPath();

	private static MEFPageServerS3 makeOne(IDataSnapshotServer dsServer)
			throws AuthorizationException {
		AmazonS3 s3 = new S3FromFileSystem();
		return new MEFPageServerS3(
				new S3FromFileSystem(),
				CacheManager.newInstance(
						MEFPageServerS3.class.getClassLoader()
								.getResource("ieeg-ehcache.xml"))
						.getEhcache(
								"MEFPageServerS3.header"),
				CacheManager.getCacheManager("ieeg").getEhcache(
						"MEFPageServerS3.data"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.decompressedDataCache"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.postProcDataCache"),
				dsServer,
				"doesn't-matter-data-bucket",
				getInt(
						IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ,
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ_DFLT),
				IvProps.getMefPageServerS3CpuThrdPoolSz(),
				getBoolean(IvProps.getIvProps(), IvProps.DOWNSAMPLED, false),
				getDouble(
						IvProps.getIvProps(),
						IvProps.DOWNSAMPLED_RATE_HZ,
						IvProps.DOWNSAMPLED_RATE_HZ_DFLT),
				1024 * getInt(
						IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB,
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB_DFLT),
				IvProps.getPaddingSamples(),
				new MEFIndexService(
						s3,
						CacheManager
								.getCacheManager("ieeg")
								.getEhcache(MEFIndexService.CACHE_NAME),
						IvProps.getMefIndexSliceSize(),
						"doesn't-matter-data-bucket"),
				IvProps.getMEFPageServerS3MaxRequestsPerUserRawRed(),
				IvProps.getMEFPageServerS3MaxRequestsTotal(),
				// Long timeout for stepping through code
				3600,
				IvProps.getMEFPageServerS3MaxReqHzChsSecs(),
				IvProps.getMEFPageServerS3MaxRedReqHzChsSecs());
	}

	private boolean almostEqual(List<Integer> expected, List<Integer> actual,
			int index) {
		return (index < expected.size() - 1
				&&
				equal(expected.get(index + 1),
						actual.get(index)))
				||
				(index == expected.size() - 1)
				||
				(index < actual.size() - 1
				&&
				equal(expected.get(index),
						actual.get(index + 1)))
				||
				(index == actual.size() - 1)
				||
				(index < expected.size() - 2
				&&
				equal(expected.get(index + 2),
						actual.get(index)))
				||
				(index == expected.size() - 2);
	}

	@Test
	public void requestMultiChannelReadBlock() throws Exception {
		IDataSnapshotServer dsServer = Mockito.mock(IDataSnapshotServer.class);

		MEFSnapshotSpecifier mefSnapshotSpec =
				new MEFSnapshotSpecifier("", "");

		MEFChannelSpecifier chSpec = new MEFChannelSpecifier(
				mefSnapshotSpec,
				MEF_FILE,
				UUID.randomUUID().toString(),
				0,
				"1");

		MEFPageServerS3 mefPageServer = makeOne(dsServer);
		double samplingRateHz = mefPageServer.getSampleRate(chSpec);
		double periodMicros = 1E6 / samplingRateHz;
		long fileStartUutc = mefPageServer.getStartUutc(chSpec);
		long beginOffsetWereLookingForMicros = 0;// 48600 * (long) 1E6;
		long endOffsetWereLookingForMicros = // (long) (3 * 1E6);
		beginOffsetWereLookingForMicros
				+ (3 * 24 * 60 * 60 * (long) 1E6);
		long intervalMicros = 30 * (long) 1E6;
		// 3 days
		long bigJ = 0;
		for (long offsetMicros = beginOffsetWereLookingForMicros; offsetMicros < endOffsetWereLookingForMicros; offsetMicros += intervalMicros) {
			List<List<TimeSeriesData>> actualTsdss = mefPageServer
					.requestMultiChannelRead(
							Lists.newArrayList(chSpec),
							offsetMicros,
							offsetMicros + intervalMicros,
							new PostProcessor[] {
							new Downsample(
									samplingRateHz,
									1) },
							new User(
									new UserId(1),
									"no",
									"no",
									Status.ENABLED,
									Sets.<Role> newHashSet(),
									"photo",
									"me@me.com",
									new UserProfile("x", "y", "z", "w", "v",
											null, null, null, null, null, null,
											null, null, null, null)),
							null
					);

			List<List<Integer>> mergedActualTsds = Lists.newArrayList();
			for (List<TimeSeriesData> actualTsds : actualTsdss) {
				List<Integer> merged = newArrayList();
				mergedActualTsds.add(merged);
				for (TimeSeriesData actualTsd : actualTsds) {
					Integer[] asArray = actualTsd.toArray();
					merged.addAll(Arrays.asList(asArray));
				}
			}

			List<List<TimeSeriesData>> expectedTsdss = mefPageServer
					.requestMultiChannelRead(
							newArrayList(chSpec),
							offsetMicros,
							offsetMicros + intervalMicros,
							new PostProcessor[] {
							new DownsampleRef(samplingRateHz)
							},
							new User(
									new UserId(1),
									"no",
									"no",
									Status.ENABLED,
									Sets.<Role> newHashSet(),
									"photo",
									"me@me.com",
									new UserProfile("x", // first 
											"y", // last 
											"z", // inst 
											"w", // title
											"v", // job
											 null, // aws
											 null, 
											 null, // dropbox 
											 null, null, // geo 
											 null, // street 
											 null, // city
											 null, // state 
											 null, // zip 
											 null // country 
											 )),
							null
					);

			List<List<Integer>> mergedExpectedTsds = newArrayList();
			for (List<TimeSeriesData> expectedTsds : expectedTsdss) {
				List<Integer> merged = newArrayList();
				mergedExpectedTsds.add(merged);
				for (TimeSeriesData expectedTsd : expectedTsds) {
					Integer[] asArray = expectedTsd.toArray();
					merged.addAll(Arrays.asList(asArray));
				}
			}
			assertEquals(mergedExpectedTsds.size(), mergedActualTsds.size());
			for (int i = 0; i < mergedActualTsds.size(); i++) {
				List<Integer> actualTsd = mergedActualTsds.get(i);
				List<Integer> expectedTsd = mergedExpectedTsds.get(i);
				assertEquals(expectedTsd.size(), actualTsd.size());
				for (int j = 0; j < expectedTsd.size(); j++) {
					if (equal(expectedTsd.get(j), actualTsd.get(j))
							||
							 //expectedTsd.equals(actualTsd)) {
							almostEqual(expectedTsd, actualTsd, j)) {
						// System.out
						// .println("++"
						// + (j + bigJ)
						// + " "
						// + (long) (((j + bigJ) * 2000.37209) +
						// 946706400000000L)
						// + " "
						// + expectedTsd.get(j) +
						// " "
						// + actualTsd.get(j));
					} else {
						System.out
								.println("--"
										+ (j + bigJ)
										+ " "
										+ (long) (((j + bigJ) * periodMicros) + fileStartUutc)
										+ " "
										+ expectedTsd.get(j)
										+ " "
										+ actualTsd.get(j));
						System.out.println("Context:");
						System.out.println("Expected Actual");
						for (int c = Math.max(0, j - 10); c < Ints.min(j + 11,
								expectedTsd.size(), actualTsd.size()); c++) {
							System.out.println((c == j ? "**" : "")
									+ expectedTsd.get(c) + " "
									+ actualTsd.get(c));
						}
						throw new AssertionError();
					}
					if (j == expectedTsd.size() - 1) {
						bigJ += j + 1;
					}
				}
			}
		}
	}
}
