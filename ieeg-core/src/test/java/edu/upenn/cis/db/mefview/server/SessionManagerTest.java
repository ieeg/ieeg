/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServiceFactory;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.shared.IUser;

/**
 * 
 * @author John Frommeyer
 * 
 */
public class SessionManagerTest {
	private User authedUser;
	private final static TstObjectFactory tstObjectFactory =
			new TstObjectFactory(false);
	private SessionToken activeSessionToken;
	private SessionToken tenHoursOldSessionToken;

	@Before
	public void testSetUp() throws Exception {
		// DataSnapshotServer needs IvProps initialized
		IvProps.setIvProps(Collections.<String, String> emptyMap());
		IUserService userService = Mockito.mock(IUserService.class);
		UserServiceFactory.setUserService(userService);
		Session sess = null;
		Transaction trx = null;
		try {
			final TestDbCleaner cleaner = new TestDbCleaner(
					HibernateUtil.getSessionFactory(),
					HibernateUtil.getConfiguration());
			cleaner.deleteEverything();
			sess =
					HibernateUtil.getSessionFactory().openSession();

			trx = sess.beginTransaction();

			final UserEntity user = tstObjectFactory.newUserEntity();
			sess.saveOrUpdate(user);
			final Set<Role> authedUsersRoles = Collections.emptySet();
			authedUser = new User(
					user.getId(),
					"authedUser",
					"authedPasswd",
					IUser.Status.ENABLED,
					authedUsersRoles,
					"",
					"email@nowhere.com",
					new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

			// Create some Sessions
			final SessionEntity activeSession = tstObjectFactory
					.newSession(user);
			activeSessionToken = new SessionToken(activeSession.getToken());
			sess.save(activeSession);

			final Calendar sixtyDaysAgo = Calendar.getInstance();
			sixtyDaysAgo.add(Calendar.DAY_OF_MONTH, -60);
			final SessionEntity sixtyDaySession = tstObjectFactory
					.newSession(user);
			sixtyDaySession.setLastAccessTime(sixtyDaysAgo.getTime());
			sess.save(sixtyDaySession);

			final Calendar tenHoursAgo = Calendar.getInstance();
			tenHoursAgo.add(Calendar.HOUR_OF_DAY, -10);
			final SessionEntity tenHoursOldSession = tstObjectFactory
					.newSession(user);
			tenHoursOldSession.setLastAccessTime(tenHoursAgo.getTime());
			tenHoursOldSessionToken = new SessionToken(
					tenHoursOldSession.getToken());
			sess.save(tenHoursOldSession);

			trx.commit();
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Test
	public void createSession() throws Throwable {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				24, 
				24, 
				100);
		final SessionToken token = service.createSession(authedUser);
		assertNotNull(token);

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final SessionEntity actualSession = sessionDAO
					.findByNaturalId(token.getId());
			assertNotNull(actualSession);
			assertEquals(authedUser.getUserId(),
					actualSession.getUser().getId());
			assertEquals(SessionStatus.ACTIVE, actualSession.getStatus());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void cleanupOldSessions() throws Throwable {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				30, 
				30, 
				2);
		final int deleted = service
				.cleanupOldSessions();
		assertEquals(2, deleted);

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();

			trx = session.beginTransaction();
			final ISessionDAO sessionDAO = new SessionDAOHibernate(session);
			final List<SessionEntity> allSessions = sessionDAO.findAll();
			assertEquals(1, allSessions.size());
			final SessionEntity actualActiveSession = getOnlyElement(allSessions);
			assertEquals(activeSessionToken.getId(),
					actualActiveSession.getToken());

			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}

		// We set sessionCleanupDenomintor to 2, so second call should not run
		// cleanup.
		final int secondRun = service.cleanupOldSessions();
		assertEquals(-1, secondRun);

		// On the third run cleanup should run, but not find any expired
		// sessions.
		final int thirdRun = service.cleanupOldSessions();
		assertEquals(0, thirdRun);
	}

	@Test
	public void authenticateSession() throws Throwable {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				30, 
				30, 
				100);
		
		final UserId userId = service.authenticateSession(activeSessionToken);
		assertNotNull(userId);
		assertEquals(authedUser.getUserId(), userId);

		boolean timeOutException = false;
		try {
			service.authenticateSession(tenHoursOldSessionToken);
		} catch (SessionExpirationException e) {
			timeOutException = true;
		}
		assertTrue(timeOutException);

		final SessionToken nonExistent = tstObjectFactory.newSessionToken();
		boolean noSessionException = false;
		try {
			service.authenticateSession(nonExistent);
		} catch (SessionNotFoundException e) {
			noSessionException = true;
		}
		assertTrue(noSessionException);
	}

	@Test
	public void expireSession() {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				30, 
				30, 
				100);
		service
				.logoutSession(activeSessionToken);

		final UserIdSessionStatus userStatus = service
				.getUserIdSessionStatus(activeSessionToken);

		assertEquals(SessionStatus.LOGGED_OUT, userStatus.getSessionStatus());
	}

	@Test
	public void disableAcctSession() {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				30, 
				30, 
				100);
		service
				.disableAcctSession(activeSessionToken);

		final UserIdSessionStatus userStatus = service
				.getUserIdSessionStatus(activeSessionToken);

		assertEquals(SessionStatus.ACCOUNT_DISABLED,
				userStatus.getSessionStatus());
	}

	@Test
	public void getUserIdSessionStatus() {
		final SessionManager service = new SessionManager(
				getDataSnapshotServer(),
				30, 
				30, 
				100);
		UserIdSessionStatus userStatus = service
				.getUserIdSessionStatus(activeSessionToken);
		assertEquals(authedUser.getUserId(), userStatus.getUserId());
		assertEquals(SessionStatus.ACTIVE, userStatus.getSessionStatus());

		service
				.logoutSession(activeSessionToken);

		userStatus = service.getUserIdSessionStatus(activeSessionToken);
		assertEquals(authedUser.getUserId(), userStatus.getUserId());
		assertEquals(SessionStatus.LOGGED_OUT, userStatus.getSessionStatus());
	}
	
	private static DataSnapshotServer getDataSnapshotServer() {
		return new DataSnapshotServer(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration(),
				new DataSnapshotServiceFactory(),
				100,
				new HibernateDAOFactory());
	}
}
