/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 * @author John Frommeyer
 *
 */
public class DisplayConfigurationTest {

	@Test
	public void testEquals() {
		final DisplayConfiguration butterworth = new DisplayConfiguration(
				"butterworth", 
				DisplayConfiguration.BANDPASS_FILTER, 
				4, 
				0.1,
				4999, 
				0, 
				0);
		
		final DisplayConfiguration bessel = new DisplayConfiguration(
				"bessel", 
				DisplayConfiguration.BANDPASS_FILTER, 
				4, 
				0.1,
				4999, 
				0, 
				0);
		
		assertFalse(butterworth.equals(bessel));
		
		final DisplayConfiguration butterworth2 = new DisplayConfiguration(
				"butterworth", 
				DisplayConfiguration.BANDPASS_FILTER, 
				4, 
				0.1,
				4999, 
				0, 
				0);
		
		assertEquals(butterworth, butterworth2);
		assertEquals(butterworth.hashCode(), butterworth2.hashCode());
	}
}
