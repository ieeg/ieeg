/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.amazonaws.auth.BasicAWSCredentials;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.server.AwsCredentialsMixIn;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.AWSContainerCredentials;
import edu.upenn.cis.db.mefview.shared.ContainerCredentials;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * 
 * @author John Frommeyer
 *
 */
public class ObjectMapperFactoryTest {
	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();
	private final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();
	private FileReference expectedFileReference;
	private AWSContainerCredentials expectedContainerCredentials;

	@Before
	public void setupExpectedFileReference() {
		final String expectedObjectKey = BtUtil.newUuid();
		final String expectedFilePath = BtUtil.newUuid();
		final String expectedBucket = BtUtil.newUuid();
		final String expectedFileDirectory = BtUtil.newUuid();
		final String expectedAwsAccessKeyId = BtUtil.newUuid();
		final String expectedAwsSecretKey = BtUtil.newUuid();

		expectedContainerCredentials = new AWSContainerCredentials(
				new BasicAWSCredentials(
						expectedAwsAccessKeyId,
						expectedAwsSecretKey));
		final CredentialedDirectoryBucketContainer expectedContainer = new CredentialedDirectoryBucketContainer(
				expectedBucket,
				expectedFileDirectory,
				expectedContainerCredentials);
		expectedFileReference = new FileReference(
				expectedContainer,
				expectedObjectKey, expectedFilePath);
	}

	@Test
	public void testLogging() throws JsonParseException, JsonMappingException,
			IOException {
		toAndfromJsonTest();
		final ObjectWriter passwdFilteredWriter = ObjectMapperFactory
				.newPasswordFilteredWriter();
		String filteredValue = passwdFilteredWriter
				.writeValueAsString(expectedFileReference);
		assertFalse(filteredValue.toLowerCase()
				.contains(AwsCredentialsMixIn.AWS_SECRET_KEY_PROPERTY.toLowerCase()));
		toAndfromJsonTest();

	}

	private void toAndfromJsonTest() throws JsonParseException,
			JsonMappingException,
			IOException {

		final File toJsonFile = testFolder.newFile();
		mapper.writeValue(toJsonFile, expectedFileReference);

		//System.out.println(mapper.writeValueAsString(expectedFileReference));

		final InputStream jsonStream = new FileInputStream(toJsonFile);

		final FileReference fromJson = mapper.readValue(jsonStream,
				FileReference.class);

		assertEquals(expectedFileReference.getFilePath(),
				fromJson.getFilePath());
		assertEquals(expectedFileReference.getObjectKey(),
				fromJson.getObjectKey());
		assertEquals(expectedFileReference.isContainer(),
				fromJson.isContainer());

		final DirectoryBucketContainer containerFromJson = fromJson
				.getDirBucketContainer();
		assertTrue(containerFromJson instanceof CredentialedDirectoryBucketContainer);
		final CredentialedDirectoryBucketContainer credentialedContainerFromJson = (CredentialedDirectoryBucketContainer) containerFromJson;
		assertEquals(
				expectedFileReference
						.getDirBucketContainer()
						.getBucket(),
				credentialedContainerFromJson.getBucket());
		assertEquals(
				expectedFileReference
						.getDirBucketContainer()
						.getFileDirectory(),
				credentialedContainerFromJson.getFileDirectory());

		final ContainerCredentials containerCredentialsFromJson = credentialedContainerFromJson
				.getCredentials();
		assertTrue(containerCredentialsFromJson instanceof AWSContainerCredentials);
		final AWSContainerCredentials awsContainerCredentialsFromJson = (AWSContainerCredentials) containerCredentialsFromJson;
		assertEquals(expectedContainerCredentials.getCredentials()
				.getAWSAccessKeyId(),
				awsContainerCredentialsFromJson
						.getCredentials().getAWSAccessKeyId());
		assertEquals(expectedContainerCredentials.getCredentials()
				.getAWSSecretKey(), awsContainerCredentialsFromJson
				.getCredentials().getAWSSecretKey());
	}
}
