package edu.upenn.cis.db.mefview.server.mappings;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.StringKeyValue;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.Scope;
import edu.upenn.cis.db.habitat.persistence.mapping.UserTaskMapper;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.HibernateScope;
import edu.upenn.cis.db.mefview.server.mapping.database.EntityLookup;
import edu.upenn.cis.db.mefview.server.mapping.database.EntityLookup.ScopeRetriever;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Task;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class TestMappings {
	private TstObjectFactory tstFac = new TstObjectFactory(false);

	private User user;
//	private SpeciesEntity species0;
//	private DrugEntity drug0;
//	private DrugEntity drug1;
//	private StimTypeEntity stimType0;
//	private StimRegionEntity stimRegion0;
//	private IDataSnapshotServer dsServer;
	Session sess = null;
	Transaction trx = null;

	private IPermissionDAO permDAO;
	
	PersistentObjectFactory perf = PersistentObjectFactory.getFactory();

	@Before
	public void setUp() throws Exception {
		try {
//			final TestDbCleaner cleaner = new TestDbCleaner(
//					HibernateUtil.getSessionFactory(),
//					HibernateUtil.getConfiguration());
//			cleaner.deleteEverything();
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();

			user = tstFac.newUserRegularEnabled();
			user.setUserId(new UserId(1));

			// Setup IvProps
			IvProps.setIvProps(Collections.singletonMap(IvProps.MAX_DS_TS_ANNS,
					Integer.toString(IvProps.MAX_DS_TS_ANNS_DFLT)));
			IUserService userService = Mockito.mock(IUserService.class);
			when(userService.findUserByUid((UserId)anyObject())).thenReturn(user);			
			UserServiceFactory.setUserService(userService);

//			dsServer = DataSnapshotServerFactory.getDataSnapshotServer();
			
			permDAO = new PermissionDAOHibernate(sess);
//			PermissionEntity readPerm = permDAO.findStarCoreReadPerm();
			
			IDataSnapshotDAO snapshotDAO = new DataSnapshotDAOHibernate(sess);
			
			final Scope newScope = new HibernateScope(sess, trx);
			new EntityLookup(//snapshotDAO, 
					permDAO, 
					userService, new ScopeRetriever() {

						@Override
						public Scope getScope() {
							return newScope;
						}
				
			});

			EntityPersistence<PermissionEntity,String,Void> ePersistence =
					(EntityPersistence)perf.getPersistenceManager(PermissionEntity.class);
			
			PermissionEntity readPerm = ePersistence.createTransient("Test", null);
//			
//			UserEntity entity = EntityLookup.getSingleton().getUserEntityFor(user.getUsername());
			
			sess.saveOrUpdate(tstFac.newUserEntity(user.getUserId()));
			
//			sess.saveOrUpdate(entity);

//			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		}
	}

	
	
//	@Test
//	public void testUserTaskToTask() {
//		UserEntity bob = new UserEntity(new UserId(3));
//		List<UserEntity> testUser = new ArrayList<UserEntity>();
////		testUser.add(bob);
//		List<DataSnapshotEntity> resources = new ArrayList<DataSnapshotEntity>();
//		List<String> submissionsAllowed = new ArrayList<String>(); 
//		List<String> patternsAllowed = new ArrayList<String>();
//	
//		UserTaskEntity myTE = new UserTaskEntity(
//				new Long(123),
//				null,//ExtAclEntity extAcl,
//				testUser, 
//				"taskName", 
//				BtUtil.newUuid(), // pubId
//				new Date(),	// active
//				new Date(), // due
//				new Date(), // final
//				"description",
//				"url",
//				"project",
//				resources,
//				submissionsAllowed, 
//				patternsAllowed,
//				"configuration", 
//				"validationOperation",
//				"formParameters");
//		
//		Task task = UserTaskMapper.INSTANCE.userTaskEntityToTask(myTE);
//		
//		UserTaskEntity te = UserTaskMapper.INSTANCE.taskToUserTaskEntity(task);
//	}
	
	@After
	public void close() {
		trx.rollback();
//	} finally {
		PersistenceUtil.close(sess);
	}

	@Test
	public void testTaskToUserTask() {
		Set<Role> roles = new HashSet<Role>();
		roles.add(Role.USER);
		List<PresentableMetadata> resources = new ArrayList<PresentableMetadata>();
		List<String> submissionsAllowed = new ArrayList<String>(); 
		List<String> patternsAllowed = new ArrayList<String>();
		WebLink url = new WebLink("1", "url", "http://u.rl");
		JsonKeyValueSet configuration = new JsonKeyValueSet();
		configuration.add(new StringKeyValue("config", "value"));
		JsonKeyValueSet validation = new JsonKeyValueSet();
		validation.add(new StringKeyValue("validation", "http://validation"));
		JsonKeyValueSet parms = new JsonKeyValueSet();
		parms.add(new StringKeyValue("validation", "http://parms"));
		
		Task myTask = new Task(
				BtUtil.newUuid(), // pubId
				"taskName", 
				new Date(),	// active
				new Date(), // due
				new Date(), // final
				"description",
				url,
				"project",
				resources,
				submissionsAllowed, 
				patternsAllowed,
				configuration, 
				validation,
				parms);
		myTask.setUser(user.getUserId());
		
		System.out.println(myTask);
	
		UserTaskEntity te = UserTaskMapper.INSTANCE.taskToUserTaskEntity(myTask);
		
		System.out.println(te);

		Task task2 = UserTaskMapper.INSTANCE.userTaskEntityToTask(te);
		
		
	}
}
