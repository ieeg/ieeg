/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.google.common.math.DoubleMath;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.server.FilterManager.FilterBuffer;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

/**
 * @author John Frommeyer
 *
 */
public class FilterManagerMinMaxTest {

	private final double trueFreqHz = 2500;
	private final int pageLength = (int) trueFreqHz;
	private final double scale = 1;
	private final TimeSeriesPage[] pageList = new TimeSeriesPage[2];
	private ChannelSpecifier path;

	@Before
	public void createPages() {

		// 2500 Hz, one second per page. Value == index for ease of debugging.
		pageList[0] = new TimeSeriesPage();
		pageList[0].timeStart = 1_000_000;
		pageList[0].timeEnd = 2_000_000;
		pageList[0].values = new int[pageLength];
		int i = 0;
		while (i < pageList[0].values.length) {
			pageList[0].values[i] = i;
			i++;
		}
		pageList[1] = new TimeSeriesPage();
		pageList[1].timeStart = 2_000_000;
		pageList[1].timeEnd = 3_000_000;
		pageList[1].values = new int[pageLength];
		for (int j = 0; j < pageList[1].values.length; j++) {
			pageList[1].values[j] = i++;
		}

		final MEFSnapshotSpecifier ss = new MEFSnapshotSpecifier(
				"minMax-1",
				BtUtil.newUuid());
		path = new MEFPageServer.MEFChannelSpecifier(
				ss,
				"minMax-1-Channel1",
				BtUtil.newUuid(),
				0,
				"1");
	}

	@Test
	public void decimateMinMax() {
		// request start time == page start time and request period a multiple
		// of the recording period
		double startTimeUutc = 1e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 1600;

		testRequest(startTimeUutc, endTimeUutc, samplingPeriodMicros);
	}

	@Test
	public void decimateMinMaxNonMultiplePeriod() {
		// request start time == page start time and request period not a
		// multiple of the recording period
		double startTimeUutc = 1e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 900;

		testRequest(startTimeUutc, endTimeUutc, samplingPeriodMicros);
	}

	@Test
	public void decimateMinMaxMiddleOfPage() {
		// request start time > page start time and request period a multiple
		// of the recording period
		double startTimeUutc = 1.5e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 800;

		testRequest(startTimeUutc, endTimeUutc, samplingPeriodMicros);
	}

	@Test
	public void decimateMinMaxMiddleOfPageNonMultiplePeriod() {
		// request start time > page start time and request period not a
		// multiple of the recording period
		double startTimeUutc = 1.5e6;
		double endTimeUutc = 2.5e6;
		double samplingPeriodMicros = 900;

		testRequest(startTimeUutc, endTimeUutc, samplingPeriodMicros);
	}

	private void testRequest(
			double startTimeUutc,
			double endTimeUutc,
			double samplingPeriodMicros) {
		final FilterSpec filter = new FilterSpec();
		long startTimeOffsetMicros = (long) startTimeUutc;
		double filterPad = (long) Math.ceil(samplingPeriodMicros
				* IvProps.PADDING_SAMPLES_DFLT);
		FilterBuffer workSpace = new FilterManager.FilterBuffer(
				(int) ((endTimeUutc
						- startTimeUutc + filterPad + filterPad) / trueFreqHz) + 1);

		final ArrayList<TimeSeriesData> actualList = FilterManager
				.decimateMinMax(
						startTimeUutc,
						endTimeUutc,
						trueFreqHz,
						startTimeOffsetMicros,
						samplingPeriodMicros,
						scale,
						pageList,
						filter,
						path,
						false,
						workSpace,
						filterPad,
						filterPad);
		assertEquals(1, actualList.size());
		final TimeSeriesData actual = actualList.get(0);
		assertEquals(
				2 * (int) Math.ceil((endTimeUutc - startTimeUutc)
						/ samplingPeriodMicros), actual.getSeriesLength());

		final double recordingPeriodMicros = 1e6 / trueFreqHz;
		for (int samplePeriod = 0; samplePeriod < actual.getSeriesLength() / 2; samplePeriod++) {
			// We consider the sample period to be a half open interval
			// [thisPeriodStartUUtc, thisPeriodEndUutc) so if thisPeriodEndUutc
			// happens to fall exactly on a sample we take the one before it.

			// The min
			final double thisPeriodStartUUtc = startTimeUutc
					+ samplingPeriodMicros * samplePeriod;
			int indexOfExpectedMin = (int) Math
					.ceil((thisPeriodStartUUtc - pageList[0].timeStart)
							/ recordingPeriodMicros);
			final int[] pageAndIndexOfExpectedMin = getPageAndIndexOfExpected(
					indexOfExpectedMin);
			final int pageOfExpectedMin = pageAndIndexOfExpectedMin[0];
			indexOfExpectedMin = pageAndIndexOfExpectedMin[1];
			final int indexOfActualMin = 2 * samplePeriod;
			assertEquals(
					"at index: " + indexOfActualMin,
					pageList[pageOfExpectedMin].values[indexOfExpectedMin],
					actual.getAt(indexOfActualMin));

			// The max
			final double thisPeriodEndUutc = startTimeUutc
					+ samplingPeriodMicros * (samplePeriod + 1);
			final double indexOfExpectedMaxDbl = (thisPeriodEndUutc - pageList[0].timeStart)
					/ recordingPeriodMicros;
			int indexOfExpectedMax = (int) (DoubleMath
					.isMathematicalInteger(indexOfExpectedMaxDbl) ?
					indexOfExpectedMaxDbl - 1
					: Math.floor(indexOfExpectedMaxDbl));
			final int[] pageAndIndexOfExpectedMax = getPageAndIndexOfExpected(
					indexOfExpectedMax);
			final int pageOfExpectedMax = pageAndIndexOfExpectedMax[0];
			indexOfExpectedMax = pageAndIndexOfExpectedMax[1];
			final int indexOfActualMax = indexOfActualMin + 1;
			assertEquals("at index: " + indexOfActualMax,
					pageList[pageOfExpectedMax].values[indexOfExpectedMax],
					actual.getAt(indexOfActualMax));
		}
	}

	private int[] getPageAndIndexOfExpected(
			int unnormalizedIndex) {
		assertTrue(unnormalizedIndex < pageLength * 2);
		final int[] pageAndIndex = new int[2];
		if (unnormalizedIndex < pageLength) {
			pageAndIndex[0] = 0;
			pageAndIndex[1] = unnormalizedIndex;
			return pageAndIndex;
		} else {
			pageAndIndex[0] = 1;
			pageAndIndex[1] = unnormalizedIndex % pageLength;
			return pageAndIndex;
		}
	}
}
