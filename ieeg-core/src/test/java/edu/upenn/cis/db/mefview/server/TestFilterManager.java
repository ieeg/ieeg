/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.server.FilterManager.FilterBuffer;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.Downsample;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

public class TestFilterManager {
	static TimeSeriesPage[] samplePages = new TimeSeriesPage[3];

	static ChannelSpecifier spec;

	@BeforeClass
	public static void classSetup() {

		IvProps.setIvProps(new HashMap<String, String>());
		samplePages[0] = new TimeSeriesPage();
		samplePages[0].timeStart = 12;
		samplePages[0].timeEnd = samplePages[0].timeStart + 1023;
		samplePages[0].values = new int[1024];
		for (int i = 0; i < 1024; i++)
			samplePages[0].values[i] = i + 12;

		// Deliberate gap
		samplePages[1] = new TimeSeriesPage();
		samplePages[1].timeStart = 1060;
		samplePages[1].timeEnd = samplePages[1].timeStart + 1023;
		samplePages[1].values = new int[1024];
		for (int i = 0; i < 1024; i++)
			samplePages[1].values[i] = (int) samplePages[1].timeStart + i;

		// Deliberate gap
		samplePages[2] = new TimeSeriesPage();
		samplePages[2].timeStart = 100000;
		samplePages[2].timeEnd = samplePages[2].timeStart + 1023;
		samplePages[2].values = new int[1024];
		for (int i = 0; i < 1024; i++)
			samplePages[2].values[i] = (int) Math.sin(i / 102.4 * 2 * Math.PI) * 20;

		MEFSnapshotSpecifier ss = new MEFSnapshotSpecifier("path", "1");

		spec = new MEFPageServer.MEFChannelSpecifier(ss, "Channel1",
				"123-45-6789", 1000, "1");
	}

	@Test
	public void requestIntDownsample() throws Exception {
		ArrayList<TimeSeriesData> results = Downsample.downsample(0, 1024,
				1.e6, 0, 8, 1, samplePages, spec, false);

		assertEquals(results.size(), 1);
		System.out.println("Result size: " + results.get(0).getSeriesLength());
		for (int i = 0; i < results.get(0).getSeriesLength(); i++)
			System.out.print(results.get(0).getSeries()[i] + ",");
		System.out.println();
		assertEquals(results.get(0).getSeriesLength(), 128);
		for (int i = 0; i < results.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results.get(0).getGapStart().get(i)
					+ "-" + results.get(0).getGapEnd().get(i));
		}
		for (int i = 3; i < results.get(0).getSeriesLength(); i++)
			assertTrue(results.get(0).getSeries()[i]
					- results.get(0).getSeries()[i - 1] == 8);
	}

	@Test
	public void requestIntDownsampleMiddle() throws Exception {
		ArrayList<TimeSeriesData> results2 = Downsample.downsample(10, 3160,
				1.e6, 0, 10, 1, samplePages, spec, false);
		assertEquals(results2.size(), 1);
		System.out.println("Result size: " + results2.get(0).getSeriesLength());
		for (int i = 0; i < results2.get(0).getSeriesLength(); i++)
			System.out.print(results2.get(0).getSeries()[i] + ",");
		System.out.println();

		for (int i = 0; i < results2.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results2.get(0).getGapStart().get(i)
					+ "-" + results2.get(0).getGapEnd().get(i));
			for (int j = results2.get(0).getGapStart().get(i); j < results2
					.get(0).getGapEnd().get(i); j++)
				assertEquals(results2.get(0).getSeries()[j], 0);
		}
		assertEquals(results2.get(0).getSeriesLength(), 315);
	}

	@Test
	public void requestUnfiltered() throws Exception {
		FilterSpec fs = new FilterSpec();

		fs.setFilterType(0);
		ArrayList<TimeSeriesData> results = FilterManager.decimateLinearInterpolate(0, 1024,
				1.e6, 0, 10, 1, samplePages, fs, spec, false, new FilterBuffer(
						4096), 0, 0);

		assertEquals(results.size(), 1);
		System.out.println("Result size: " + results.get(0).getSeriesLength());
		for (int i = 0; i < results.get(0).getSeriesLength(); i++)
			System.out.print(results.get(0).getSeries()[i] + ",");
		System.out.println();
		assertEquals(results.get(0).getSeriesLength(), 103);
		for (int i = 0; i < results.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results.get(0).getGapStart().get(i)
					+ "-" + results.get(0).getGapEnd().get(i));
		}
		for (int i = 3; i < results.get(0).getSeriesLength(); i++)
			if (results.get(0).getSeries()[i]
					- results.get(0).getSeries()[i - 1] != 10) {
				System.err.println("Failure at " + i);
				assert (false);
			}
	}

	@Test
	public void requestUnfilteredMiddle() throws Exception {
		FilterSpec fs = new FilterSpec();
		fs.setFilterType(0);

		ArrayList<TimeSeriesData> results2 = FilterManager.decimateLinearInterpolate(10, 3160,
				1.e6, 0, 10, 1, samplePages, fs, spec, false, new FilterBuffer(
						4096), 0, 0);
		assertEquals(results2.size(), 1);
		System.out.println("Result size: " + results2.get(0).getSeriesLength());
		for (int i = 0; i < results2.get(0).getSeriesLength(); i++)
			System.out.print(results2.get(0).getSeries()[i] + ",");
		System.out.println();

		for (int i = 0; i < results2.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results2.get(0).getGapStart().get(i)
					+ "-" + results2.get(0).getGapEnd().get(i));
			for (int j = results2.get(0).getGapStart().get(i); j < results2
					.get(0).getGapEnd().get(i); j++)
				assertEquals(results2.get(0).getSeries()[j], 0);
		}
		assertEquals(results2.get(0).getSeriesLength(), 315);
	}

	@Test
	public void requestFilter() throws Exception {
		ArrayList<TimeSeriesData> results = FilterManager.decimateLinearInterpolate(0, 1024,
				1.e6, 0, 10, 1, samplePages, new FilterSpec(), spec, false,
				new FilterBuffer(4096), 0, 0);

		assertEquals(results.size(), 1);
		System.out.println("Result size: " + results.get(0).getSeriesLength());
		for (int i = 0; i < results.get(0).getSeriesLength(); i++)
			System.out.print(results.get(0).getSeries()[i] + ",");
		System.out.println();
		assertEquals(results.get(0).getSeriesLength(), 103);
		for (int i = 0; i < results.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results.get(0).getGapStart().get(i)
					+ "-" + results.get(0).getGapEnd().get(i));
		}
	}

	@Test
	public void requestFilterOnEmpty() throws Exception {
		ArrayList<TimeSeriesData> results = FilterManager.decimateLinearInterpolate(5000, 10000,
				1.e6, 0, 10, 1, samplePages, new FilterSpec(), spec, false,
				new FilterBuffer(4096), 0, 0);

		assertEquals(results.size(), 1);
		System.out.println("Result size: " + results.get(0).getSeriesLength());
		for (int i = 0; i < results.get(0).getSeriesLength(); i++)
			System.out.print(results.get(0).getSeries()[i] + ",");
		System.out.println();
		assertEquals(results.get(0).getSeriesLength(), 500);
		for (int i = 0; i < results.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results.get(0).getGapStart().get(i)
					+ "-" + results.get(0).getGapEnd().get(i));
		}
	}

	@Test
	public void requestFilterMiddle() throws Exception {
		ArrayList<TimeSeriesData> results2 = FilterManager.decimateLinearInterpolate(10, 3160,
				1.e6, 0, 10, 1, samplePages, new FilterSpec(), spec, false,
				new FilterBuffer(4096), 0, 0);
		assertEquals(results2.size(), 1);
		System.out.println("Result size: " + results2.get(0).getSeriesLength());
		for (int i = 0; i < results2.get(0).getSeriesLength(); i++)
			System.out.print(results2.get(0).getSeries()[i] + ",");
		System.out.println();

		for (int i = 0; i < results2.get(0).getGapStart().size(); i++) {
			System.out.println("Gap " + results2.get(0).getGapStart().get(i)
					+ "-" + results2.get(0).getGapEnd().get(i));
			for (int j = results2.get(0).getGapStart().get(i); j < results2
					.get(0).getGapEnd().get(i); j++)
				assertEquals(results2.get(0).getSeries()[j], 0);
		}
		assertEquals(results2.get(0).getSeriesLength(), 315);
	}

	@Test
	public void segmentsSameAsAll() {
		FilterSpec fs = new FilterSpec();
		fs.setFilterType(0);

		// FilterSpec fs = new FilterSpec(FilterSpec.NO_FILTER, 3, 0.0, 1E6,
		// 0.0,
		// 0.0);

		// A
		{
			ArrayList<TimeSeriesData> results = FilterManager.decimateLinearInterpolate(
					samplePages[0].timeStart,
					samplePages[0].timeEnd,
					1.e6, 0, 10, 1, samplePages, fs, spec, false,
					new FilterBuffer(4096), 150E6, 150E6);
			for (int i = 0; i < results.get(0).getSeriesLength(); i++)
				System.out.print(results.get(0).getValue(i) + ",");
			System.out.println();
		}

		// B
		{
			ArrayList<TimeSeriesData> results2 = FilterManager.decimateLinearInterpolate(
					samplePages[1].timeStart, samplePages[1].timeEnd,
					1.e6, 0, 10, 1, samplePages, fs, spec, false,
					new FilterBuffer(4096), 150E6, 150E6);
			for (int i = 0; i < results2.get(0).getSeriesLength(); i++)
				System.out.print(results2.get(0).getValue(i) + ",");
			System.out.println();
		}

		// C
		{
			ArrayList<TimeSeriesData> results3 = FilterManager.decimateLinearInterpolate(
					samplePages[0].timeStart, samplePages[1].timeEnd,
					1.e6, 0, 10, 1, samplePages, fs, spec, false,
					new FilterBuffer(4096), 150E6, 150E6);

			for (int i = 0; i < results3.get(0).getSeriesLength(); i++) {
				System.out.print(results3.get(0).getValue(i) + ",");
			}
		}
	}

}
