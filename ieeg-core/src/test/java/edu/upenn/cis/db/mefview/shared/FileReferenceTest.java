/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.amazonaws.auth.BasicAWSCredentials;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;

/**
 * 
 * @author John Frommeyer
 *
 */
@GwtIncompatible("A test")
public class FileReferenceTest {
	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Test
	public void toAndfromJsonTest() throws JsonParseException,
			JsonMappingException,
			IOException {
		final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

		final String expectedObjectKey = BtUtil.newUuid();
		final String expectedFilePath = BtUtil.newUuid();
		final String expectedBucket = BtUtil.newUuid();
		final String expectedFileDirectory = BtUtil.newUuid();
		final String expectedAwsAccessKeyId = BtUtil.newUuid();
		final String expectedAwsSecretKey = BtUtil.newUuid();

		final AWSContainerCredentials expectedContainerCredentials = new AWSContainerCredentials(
				new BasicAWSCredentials(
						expectedAwsAccessKeyId,
						expectedAwsSecretKey));
		final CredentialedDirectoryBucketContainer expectedContainer = new CredentialedDirectoryBucketContainer(
				expectedBucket,
				expectedFileDirectory,
				expectedContainerCredentials);
		final FileReference expectedFileReference = new FileReference(
				expectedContainer,
				expectedObjectKey, expectedFilePath);

		final File toJsonFile = testFolder.newFile();
		mapper.writeValue(toJsonFile, expectedFileReference);

		//System.out.println(mapper.writeValueAsString(expectedFileReference));
		
		final InputStream jsonStream = new FileInputStream(toJsonFile);

		final FileReference fromJson = mapper.readValue(jsonStream,
				FileReference.class);

		assertEquals(expectedFileReference.getFilePath(),
				fromJson.getFilePath());
		assertEquals(expectedFileReference.getObjectKey(),
				fromJson.getObjectKey());
		assertEquals(expectedFileReference.isContainer(),
				fromJson.isContainer());

		final DirectoryBucketContainer containerFromJson = fromJson
				.getDirBucketContainer();
		assertTrue(containerFromJson instanceof CredentialedDirectoryBucketContainer);
		final CredentialedDirectoryBucketContainer credentialedContainerFromJson = (CredentialedDirectoryBucketContainer) containerFromJson;
		assertEquals(expectedBucket, credentialedContainerFromJson.getBucket());
		assertEquals(expectedFileDirectory,
				credentialedContainerFromJson.getFileDirectory());

		final ContainerCredentials containerCredentialsFromJson = credentialedContainerFromJson
				.getCredentials();
		assertTrue(containerCredentialsFromJson instanceof AWSContainerCredentials);
		final AWSContainerCredentials awsContainerCredentialsFromJson = (AWSContainerCredentials) containerCredentialsFromJson;
		assertEquals(expectedAwsAccessKeyId, awsContainerCredentialsFromJson
				.getCredentials().getAWSAccessKeyId());
		assertEquals(expectedAwsSecretKey, awsContainerCredentialsFromJson
				.getCredentials().getAWSSecretKey());
	}
}
