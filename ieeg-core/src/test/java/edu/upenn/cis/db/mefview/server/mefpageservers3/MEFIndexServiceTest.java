/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;

import org.junit.Before;
import org.junit.Test;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.base.Optional;
import com.google.gwt.thirdparty.guava.common.primitives.Ints;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.testhelper.S3FromFileSystem;
import edu.upenn.cis.eeg.mef.MEFStreamer;
import edu.upenn.cis.eeg.mef.MefHeader2;

public class MEFIndexServiceTest {

	private static final URL MEF_FILE_URL = MEFPageServerS3Test.class
			.getClassLoader()
			.getResource("I010_A0001_D001_CH2.mef");
	
	private static final int MEF_FILE_SAMPLING_RATE_HZ = 200;
	
	@Before
	public void before() {
		Map<String, String> props = newHashMap();
		props.put(IvProps.MEF_INDEX_SLICE_NO_ENTRIES, "100");
		IvProps.setIvProps(props);

		InputStream cacheConfig =
				getClass()
						.getClassLoader()
						.getResourceAsStream("ieeg-ehcache.xml");
		CacheManager.newInstance(cacheConfig);
	}

	@Test
	public void randomFromStartToFinishNoCache()
			throws FileNotFoundException, IOException, StaleMEFException, URISyntaxException {
		MEFStreamer mefStreamer = null;
		try {
			mefStreamer = new MEFStreamer(
					new FileInputStream(MEF_FILE_URL.toURI().getPath()),
					false);
			AmazonS3 s3 = new S3FromFileSystem();
			Ehcache cache = CacheManager
					.getCacheManager("ieeg")
					.getEhcache(MEFIndexService.CACHE_NAME);
			cache.removeAll();
			MEFIndexService idxService = new MEFIndexService(
					new S3FromFileSystem(),
					cache,
					IvProps.getMefIndexSliceSize(),
					IvProps.getDataBucket());
			MEFSnapshotSpecifier mefSnapshotSpec =
					new MEFSnapshotSpecifier("", "");
			MEFChannelSpecifier chSpec = new MEFChannelSpecifier(
					mefSnapshotSpec,
					MEF_FILE_URL.getPath(),
					"fake-id",
					MEF_FILE_SAMPLING_RATE_HZ,
					"1");

			MefHeader2 header = new MefHeader2(
					MEFPageServerS3Util.getHeader(
							s3,
							"doesnt-matter-bucket",
							chSpec));
			Optional<long[]> idxEntries =
					idxService.getStartAndEndPageNos(
							header,
							chSpec,
							0,
							10,
							false);
			assertFalse(idxEntries.isPresent());

			for (long startTimeMicros = header.getRecordingStartTime(); startTimeMicros < header
					.getRecordingEndTime(); startTimeMicros += 30000000) {
				long endTimeMicros = startTimeMicros + 30000000;
				idxEntries =
						idxService.getStartAndEndPageNos(
								header,
								chSpec,
								startTimeMicros,
								endTimeMicros,
								false);
				assertFalse(idxEntries.isPresent());
			}

		} finally {
			mefStreamer.close();
		}
	}

	@Test
	public void readFromStartToFinishTest()
			throws
			FileNotFoundException,
			IOException,
			StaleMEFException, URISyntaxException {
		MEFStreamer mefStreamer = null;
		try {
			mefStreamer = new MEFStreamer(
					new FileInputStream(MEF_FILE_URL.toURI().getPath()),
					false);
			long totalBlocks = mefStreamer.getNumberOfBlocks();
			AmazonS3 s3 = new S3FromFileSystem();
			List<TimeSeriesPage> pages =
					mefStreamer.getNextBlocks(
							Ints.checkedCast(totalBlocks));
			Ehcache cache = CacheManager
					.getCacheManager("ieeg")
					.getEhcache(MEFIndexService.CACHE_NAME);
			MEFIndexService idxService = new MEFIndexService(
					new S3FromFileSystem(),
					cache,
					IvProps.getMefIndexSliceSize(),
					IvProps.getDataBucket());
			MEFSnapshotSpecifier mefSnapshotSpec =
					new MEFSnapshotSpecifier("", "");
			MEFChannelSpecifier chSpec = new MEFChannelSpecifier(
					mefSnapshotSpec,
					MEF_FILE_URL.getPath(),
					"fake-id",
					MEF_FILE_SAMPLING_RATE_HZ,
					"1");

			MefHeader2 header = new MefHeader2(
					MEFPageServerS3Util.getHeader(
							s3,
							"doesnt-matter-bucket",
							chSpec));
			long[] idxEntries =
					idxService.getStartAndEndPageNos(
							header,
							chSpec,
							0,
							10,
							true)
							.get();

			assertEquals(0, idxEntries[0]);
			assertEquals(0, idxEntries[1]);
			for (long startTimeMicros = header.getRecordingStartTime(); startTimeMicros < header
					.getRecordingEndTime(); startTimeMicros += 30000000) {
				long endTimeMicros = startTimeMicros + 30000000;
				idxEntries =
						idxService.getStartAndEndPageNos(
								header,
								chSpec,
								startTimeMicros,
								endTimeMicros,
								true).get();

				assertNotNull(idxEntries);
				long firstEntry = idxEntries[0];
				long secondEntry = idxEntries[1];
				TimeSeriesPage oneAfterFirstPage = null;
				if (firstEntry < pages.size() - 1) {
					oneAfterFirstPage =
							pages.get((int) firstEntry + 1);
				}
				TimeSeriesPage firstPage = pages.get((int) firstEntry);
				TimeSeriesPage secondPage = pages.get((int) secondEntry);
				TimeSeriesPage oneBeforeSecondPage = null;
				if (secondEntry > 0) {
					oneBeforeSecondPage = pages.get((int) secondEntry - 1);
				}
				if (oneAfterFirstPage != null) {
					assertTrue(oneAfterFirstPage.timeStart > startTimeMicros);
				}
				assertTrue(firstPage.timeStart <= startTimeMicros);
				if (!(endTimeMicros < secondPage.timeEnd)) {
					// see if we're in a gap or we fell off of the end
					if (secondEntry + 1 < pages.size()) {
						TimeSeriesPage oneAfterSecondPage =
								pages.get((int) secondEntry + 1);
						assertTrue(endTimeMicros < oneAfterSecondPage.timeStart);
					}
				}

				if (oneBeforeSecondPage != null) {
					assertTrue(endTimeMicros >= oneBeforeSecondPage.timeEnd);
				}

			}

			long startTimeMicros = header.getRecordingStartTime();
			long endTimeMicros = startTimeMicros + (long) (60 * 1E6);

			idxEntries = idxService.getStartAndEndPageNos(
					header, chSpec, startTimeMicros, endTimeMicros, true).get();
			assertEquals(0, idxEntries[0]);
			assertEquals(3, idxEntries[1]);

			startTimeMicros = endTimeMicros;
			endTimeMicros = startTimeMicros + (long) (60 * 1E6);
			idxEntries = idxService.getStartAndEndPageNos(
					header, chSpec, startTimeMicros, endTimeMicros, true).get();
			assertEquals(3, idxEntries[0]);
			assertEquals(6, idxEntries[1]);

			startTimeMicros = header.getRecordingStartTime();
			endTimeMicros = header.getRecordingEndTime();

			idxEntries = idxService.getStartAndEndPageNos(
					header, chSpec, startTimeMicros, endTimeMicros, true).get();

			startTimeMicros = header.getRecordingStartTime() - 100;
			endTimeMicros = header.getRecordingEndTime() + 100;

			idxEntries = idxService.getStartAndEndPageNos(
					header, chSpec, startTimeMicros, endTimeMicros, true).get();

			idxEntries = idxService.getStartAndEndPageNos(
					header, chSpec, startTimeMicros, endTimeMicros, true).get();

			startTimeMicros = header.getRecordingStartTime()
					+ (long) (20 * 60 * 60 * 1E6);
			endTimeMicros = startTimeMicros + (long) (10000 * 60 * 60 * 1E6);
			idxEntries =
					idxService.getStartAndEndPageNos(
							header,
							chSpec,
							startTimeMicros,
							endTimeMicros,
							true).get();

			assertNotNull(idxEntries);
			long firstEntry = idxEntries[0];
			long secondEntry = idxEntries[1];
			TimeSeriesPage oneBeforeFirstPage = pages.get((int) firstEntry - 1);
			TimeSeriesPage firstPage = pages.get((int) firstEntry);
			TimeSeriesPage secondPage = pages.get((int) secondEntry);
			assertEquals(pages.size() - 1, secondEntry); // assert that it's the
															// last one

			assertTrue(oneBeforeFirstPage.timeEnd < startTimeMicros);
			assertTrue(firstPage.timeStart <= startTimeMicros);
			assertTrue(endTimeMicros >= secondPage.timeEnd);

			startTimeMicros = header.getRecordingStartTime();

			endTimeMicros = startTimeMicros + (long) (40 * 60 * 60 * 1E6);
			idxEntries =
					idxService.getStartAndEndPageNos(
							header,
							chSpec,
							startTimeMicros,
							endTimeMicros,
							true).get();
			assertNotNull(idxEntries);
		} finally {
			mefStreamer.close();
		}
	}
}
