/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis;

import static com.google.common.collect.Iterables.get;
import static org.junit.Assert.assertEquals;
import edu.upenn.cis.db.mefview.services.MontagedChannel;
import edu.upenn.cis.db.mefview.services.ReferenceChannel;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class CompareUtil {

	/**
	 * Compare the non-nullable fiels of {@code expected} and {@code actual} and
	 * also their {@code ReferenceChannel}s.
	 * 
	 * @param expected
	 * @param actual
	 */
	public void compareNonNullable(
			MontagedChannel expected,
			MontagedChannel actual) {
		assertEquals(
				expected.getName(),
				actual.getName());
		assertEquals(
				expected.getChannelId(),
				actual.getChannelId());
		assertEquals(
				expected.getReferenceChannels().size(),
				actual.getReferenceChannels().size());
		// Reference Channels are ordered by channelId
		int j = -1;
		for (ReferenceChannel expectedRefCh : expected
				.getReferenceChannels()) {
			j++;
			ReferenceChannel actualRefCh = get(
					actual.getReferenceChannels(),
					j,
					null);
			compare(
					expectedRefCh,
					actualRefCh);
		}
	}

	/**
	 * Compared channelId and coefficient of expected and actual.
	 * 
	 * @param expected
	 * @param actual
	 */
	public void compare(
			ReferenceChannel expected,
			ReferenceChannel actual) {
		assertEquals(
				expected.getChannelId(),
				actual.getChannelId());
		assertEquals(
				expected.getCoefficient(),
				actual.getCoefficient(),
				0.0001);
	}
}
