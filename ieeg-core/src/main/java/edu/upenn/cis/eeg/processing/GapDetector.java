/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.parameters.GapDetectorParameters;

public class GapDetector extends SignalAnnotator {
	GapDetectorParameters parameters;
//	ObjectMapper mapper = new ObjectMapper();
	
	public static final String NAME = "Gapper";
	
	GapDetector() {
	}
	
	public GapDetector(GapDetectorParameters parametersJson) 
			throws JsonParseException, IOException {
		this.parameters = parametersJson;//parameters = mapper.readValue(parametersJson, Parameters.class);
	}

	@Override
	public ISignalProcessingAlgorithm create(String name, JsonTyped parmJson) throws JsonParseException, IOException {
		return new GapDetector((GapDetectorParameters)parmJson);
	}

	@Override
	public int[] processSegment(AnnotatedTimeSeries workingSeries,int startIndex,
			int endIndex, double start, double period) {
		
		List<String> channels = new ArrayList<String>();
		List<Annotation> ret = new ArrayList<Annotation>();
		Annotation a = new Annotation(GapDetector.NAME, "Valid_data", 
				start + period * startIndex, start + period * endIndex, channels);
		workingSeries.getAnnotations().addAll(ret);
		return null;
	}
	
	public boolean isGapSensitive() {
		return true;
	}

	@Override
	public JsonTyped getParameters() {
		return parameters;
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public List<IDetection> annotate(String channel, String revId, double start, double period, int startIndex, int endIndex,
			ITimeSeries series) {
		List<String> channels = new ArrayList<String>();
		List<IDetection> ret = new ArrayList<IDetection>();
		Annotation a = new Annotation(GapDetector.NAME, "Valid_data", 
				start + period * startIndex, start + period * endIndex, channels);

		return ret;
	}

}
