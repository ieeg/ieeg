/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.filters;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.Maps;

import edu.upenn.cis.db.mefview.server.FilterManager;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm;
import edu.upenn.cis.eeg.processing.SignalProcessingFactory;

public class TimeSeriesFilterFactory {
	public static final String FILTERS="filters";
	
	static Map<FilterSpec, TimeSeriesFilter> filterCache = new ConcurrentHashMap<FilterSpec, TimeSeriesFilter>();
	
	static Map<String, Class<? extends TimeSeriesFilter>> classMap = Maps.newConcurrentMap();
	
	static Properties props = new Properties();
	
	static {
		if (TimeSeriesFilterFactory
				.class
				.getResourceAsStream("filters.properties") != null) {
			try {
				props.load(TimeSeriesFilterFactory
						.class
						.getResourceAsStream("filters.properties"));

				if (props.get(FILTERS) != null) {
					String[] items = props.getProperty(FILTERS).split(",");

					for (String str: items) {
						try {
							TimeSeriesFilter filt = (TimeSeriesFilter) Class.forName(str).newInstance();

							registerFilter(filt.getFilterType(), filt.getClass());		
							
							// Also register with the signal processing factory
							SignalProcessingFactory.register(new SignalProcessingStep(filt.getName(), 
									filt.getParameters()), 
									(Class<? extends ISignalProcessingAlgorithm>) Class.forName(str));
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			registerFilter(TimeSeriesFilterButterworth.NAME, TimeSeriesFilterButterworth.class);
			registerFilter(TimeSeriesFilterButter2.NAME, TimeSeriesFilterButter2.class);
		}
	}

	static void registerFilter(final String filterType, Class<? extends TimeSeriesFilter> c) {
		if (classMap.get(filterType) != null)
			throw new RuntimeException("Attempting to register a non-unique class " + filterType);

		classMap.put(filterType, c);
	}

	public static TimeSeriesFilter getFilterFor(double targetFrequency,
			FilterSpec fspec, double sampleRate) throws IllegalFilterException, InstantiationException, IllegalAccessException {
		FilterSpec fs = FilterManager.getFilterSpecFor(targetFrequency, fspec, sampleRate);

		fs.setAt((int) targetFrequency);
		TimeSeriesFilter ret = filterCache.get(fs);
		String filterType = fs.getFilterName();
		if (filterType == null || filterType.length() == 0)
			filterType = TimeSeriesFilterButterworth.NAME;
		
		if (ret == null) {
//			ret = new TimeSeriesFilterButter2(sampleRate/*targetFrequency*/, fs);
			ret = (TimeSeriesFilter)classMap.get(filterType).newInstance();
			ret.init(sampleRate, fs);
			filterCache.put(fs, ret);
		} // else
			// System.out.println("Found filter");

		return ret;
	}

	public static Set<String> getFilterTypes() {
		return classMap.keySet();
	}
}
