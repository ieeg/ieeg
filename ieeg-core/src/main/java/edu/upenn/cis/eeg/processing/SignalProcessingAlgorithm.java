/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.util.List;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.shared.IDetection;

/**
 * Basic signal processing algorithm abstract class.
 * Details at @link https://code.google.com/p/braintrust/wiki/UploadPipeline#Time_Series_Workflow_Steps
 * 
 * @author zives
 *
 */
public abstract class SignalProcessingAlgorithm 
implements ISignalProcessingAlgorithm {
	@Override
	public List<AnnotatedTimeSeries> process(String channel, String revId, double start, double period, 
			ITimeSeries series) {
		return process(new AnnotatedTimeSeries(channel, revId, start, period, series));
	}

	public abstract List<AnnotatedTimeSeries> process(AnnotatedTimeSeries series);
	
	public abstract List<IDetection> annotate(String channel, String revId, double start, double period, 
			int startIndex, int endIndex, ITimeSeries series);
	
	/**
	 * Process a non-gap segment
	 * 
	 * @param workingSeries
	 * @param startIndex
	 * @param endIndex
	 * 
	 * @return null if no change to segment, else updated sample values for startIndex/endIndex
	 */
	public abstract int[] processSegment(AnnotatedTimeSeries workingSeries,
			int startIndex, int endIndex, double start, double period);

//	@Override
//	public Annotation findFirst(String channel, String revId, double start,
//			double period, ITimeSeries series) {
//		return findFirst(new AnnotatedTimeSeries(channel, revId,
//				start, period, series));
//	}
//
//	@Override
//	public Annotation findFirst(AnnotatedTimeSeries series) {
//		List<Annotation> found = process(series).getAnnotations();
//		if (found.isEmpty())
//			return null;
//		else
//			return found.get(0);
//	}

	@Override
	public boolean isStateful() {
		return false;
	}

//	@Override
//	public JsonTyped getStateInJson() {
//		return "";
//	}
}
