/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.eeg;

import static com.google.common.base.Preconditions.checkState;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class XREDEReader {
	public static final DocumentBuilderFactory dbf = DocumentBuilderFactory
			.newInstance();

	public static Map<String, String> getTraces(String study)
			throws IllegalArgumentException {
		HashMap<String, String> channelSources = new HashMap<String, String>();

		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			Document doc = db.parse(study);

			NodeList list = doc.getElementsByTagName("Source");

			for (int i = 0; i < list.getLength(); i++) {
				String channelName = list.item(i).getAttributes()
						.getNamedItem("label").getNodeValue();
				String fileName = list.item(i).getAttributes()
						.getNamedItem("name").getNodeValue();
				checkState(fileName.equals(channelName + ".mef"), "Label: ["
						+ channelName + "] is not compatible with filename: ["
						+ fileName + "].");
				channelSources.put(channelName, fileName);
			}

		} catch (Throwable t) {
			throw new IllegalStateException(t);
		}

		return channelSources;
	}
}
