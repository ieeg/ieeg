/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.collect.Maps;

import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;

/**
 * Factory for creating new signal processing algorithm steps.
 * Details at @link https://code.google.com/p/braintrust/wiki/UploadPipeline#Time_Series_Workflow_Steps
 *
 * @author zives
 *
 */
public class SignalProcessingFactory {
	static Map<String, Class<? extends ISignalProcessingAlgorithm>> registry = 
			Maps.newHashMap();
	static List<SignalProcessingStep> spec = new ArrayList<SignalProcessingStep>();
	
	static Properties props = new Properties();
	static String ANNOTATORS = "annotators";
	
	static {
		if (SignalProcessingFactory
				.class
				.getResourceAsStream("processors.properties") != null) {
			try {
				props.load(SignalProcessingFactory
						.class
						.getResourceAsStream("processors.properties"));

				if (props.get(ANNOTATORS) != null) {
					String[] items = props.getProperty(ANNOTATORS).split(",");

					for (String str: items) {
						try {
							ISignalProcessingAlgorithm filt = 
									(ISignalProcessingAlgorithm) Class.forName(str).newInstance();

							System.out.println("Registering " + str);
							
							register(new SignalProcessingStep(filt.getName(), filt.getParameters()), 
									(Class<? extends ISignalProcessingAlgorithm>) Class.forName(str));
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	public static void register(SignalProcessingStep details, 
			Class<? extends ISignalProcessingAlgorithm> algo) {
		if (registry.get(details.getStepName()) != null)
			throw new RuntimeException("Attempting to register a non-unique class " + details.getStepName());

		registry.put(details.getStepName(), algo);
		spec.add(details);
	}
	
	public static List<SignalProcessingStep> getRegisteredAlgorithms() {
		return spec;
	}
	

	public static ISignalProcessingAlgorithm getAlgorithm(SignalProcessingStep stepDef) throws JsonParseException, IOException {
		if (stepDef.getNextStep() != null)
			throw new RuntimeException("Multi-stage processing not yet implemented");
		
		return getAlgorithm(stepDef.getStepName(), stepDef.getParametersJson());
	}
	
	public static ISignalProcessingAlgorithm getAlgorithm(String type, JsonTyped parms) throws JsonParseException, IOException {
		if (registry.get(type) != null) {
			try {
				return registry.get(type).newInstance().create(type, parms);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
