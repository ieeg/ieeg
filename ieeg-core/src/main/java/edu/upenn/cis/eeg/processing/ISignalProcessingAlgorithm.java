/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Signal processing algorithm interface for single signals.
 * We can have signal processing routines that are 
 * annotators or transcoders, or both.
 * Details at @link https://code.google.com/p/braintrust/wiki/UploadPipeline#Time_Series_Workflow_Steps
 * 
 * @author zives
 *
 */
public interface ISignalProcessingAlgorithm {

	/**
	 * Time series with annotations
	 * 
	 * @author zives
	 *
	 */
	public class AnnotatedTimeSeries implements ITimeSeries {
		ITimeSeries series;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getStart() {
			return start;
		}
		public void setStart(double start) {
			this.start = start;
		}
		public void setSeries(ITimeSeries series) {
			this.series = series;
		}
		public void setPeriod(double period) {
			this.period = period;
		}
		List<IDetection> annotations;
		String name;
		String revId;
		double start;
		double period;
		
		public ITimeSeries getTimeSeries() {
			return series;
		}
		public void setTimeSeries(ITimeSeries series) {
			this.series = series;
		}
		public List<IDetection> getAnnotations() {
			return annotations;
		}
		public void setAnnotations(List<IDetection> annotations) {
			this.annotations = annotations;
		}
		public AnnotatedTimeSeries(String channel, String revId, ITimeSeries series,
				double start, double period, List<IDetection> annotations) {
			super();
			this.name = channel;
			this.revId = revId;
			this.series = series;
			this.annotations = annotations;
			this.start = start;
			this.period = period;
		}
		
		public AnnotatedTimeSeries(String channel, String revId, double start, double period,
				ITimeSeries series) {
			super();
			this.name = channel;
			this.revId = revId;
			this.series = series;
			this.annotations = new ArrayList<IDetection>();
			this.start = start;
			this.period = period;
		}
		public String getChannelName() {
			return name;
		}
		public void setChannelName(String name) {
			this.name = name;
		}
		@Override
		public List<Integer> getGapStart() {
			return series.getGapStart();
		}
		@Override
		public List<Integer> getGapEnd() {
			return series.getGapEnd();
		}
		@Override
		public double getScale() {
			return series.getScale();
		}
		@Override
		public double getPeriod() {
			return series.getPeriod();
		}
		@Override
		public long getStartTime() {
			return series.getStartTime();
		}
		@Override
		public boolean isInGap(int i) {
			return series.isInGap(i);
		}
		@Override
		public Integer getValue(int i) {
			return series.getValue(i);
		}
		@Override
		public int getAt(int i) {
			return series.getAt(i);
		}
		@Override
		public int getSeriesLength() {
			return series.getSeriesLength();
		}
		@Override
		public int[] getSeries() {
			return series.getSeries();
		}
		public String getRevId() {
			return revId;
		}
		public void setRevId(String revId) {
			this.revId = revId;
		}
	}
	
	/**
	 * Construct a new signal processing algorithm with the parameters
	 * captured in the JSON string
	 * 
	 * @param name
	 * @param parmJson
	 * @return
	 */
	public ISignalProcessingAlgorithm create(String name, JsonTyped parmJson)
			throws JsonParseException, IOException;
	
	/**
	 * Process a raw time series
	 * 
	 * @param series
	 * @return
	 */
	public List<AnnotatedTimeSeries> process(String channel, String revId, double startTime,
			double period, ITimeSeries series);
	
	/**
	 * Process an annotated time series
	 * 
	 * @param series
	 * @return
	 */
	public List<AnnotatedTimeSeries> process(AnnotatedTimeSeries series);

//	/**
//	 * Find the first annotation over the raw time series
//	 * 
//	 * @param series
//	 * @return
//	 */
//	public Annotation findFirst(String channel, String revId, double start,
//			double period, ITimeSeries series);
//	
//	/**
//	 * Find the first annotation over the annotated time series
//	 * 
//	 * @param series
//	 * @return
//	 */
//	public Annotation findFirst(AnnotatedTimeSeries series);

	/**
	 * Does this algorithm keep state around?
	 * 
	 * @return
	 */
	public boolean isStateful();
	
	/**
	 * Does this algorithm care about gaps?  Else they are zeroes.
	 * 
	 * @return
	 */
	public boolean isGapSensitive();
	
//	public JsonTyped getStateInJson();
	
	/**
	 * Is this a routine that produces annotations?
	 * 
	 * @return
	 */
	public boolean isAnnotator();
	
	/**
	 * Is this a routine that transcodes the signal?
	 * 
	 * @return
	 */
	public boolean isTranscoder();
	
	/**
	 * Capture the state in a JSON string that will be passed back
	 * in as a parameter in construction.
	 * 
	 * @return
	 */
	public JsonTyped getParameters();
	
	public String getName();

}
