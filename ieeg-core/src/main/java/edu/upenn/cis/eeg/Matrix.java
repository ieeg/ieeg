/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import org.apache.commons.math.complex.Complex;
import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.LUDecompositionImpl;
import org.apache.commons.math.linear.RealMatrix;

////////////////Matrix functions start
public class Matrix {
	static final double EPS_SF8 =        2.22045e-16;
	static final double EPS_SF16 =       1.0842e-19;
	static final int RADIX = 2;
	static final double ZERO = 0.0;
	static final double ONE = 1.0;

	private static double SIGN(double x, double y) {
		return (y >= ZERO) ? Math.abs(x) : -Math.abs(x);
	}

	public static void multiply(double[][] a, double[][] b, double[][] product, int outerDim1, int innerDim, int outerDim2)
	{
        int	i, j, k;
		double	sum, t1, t2;
		
		if (a.length != outerDim1)
			throw new RuntimeException("Size of array a does not agree with parameter outerDim1");
		if (a[0].length != innerDim)
			throw new RuntimeException("Size of array a does not agree with parameter innerDim");
		if (b.length != innerDim)
			throw new RuntimeException("Size of array b does not agree with parameter innerDim");
		if (b[0].length != outerDim2)
			throw new RuntimeException("Size of array b does not agree with parameter outerDim2");
		if (product.length != outerDim1)
			throw new RuntimeException("Bad size");
		if (product[0].length != outerDim2)
			throw new RuntimeException("Bad size");

		// Rows in a
		for (i = 0; i < outerDim1; ++i) {
			// For each row in b
			for (j = 0; j < outerDim2; ++j) {
				sum = 0.0;
				for (k = 0; k < innerDim; ++k) {
					t1 = a[i][k];
					t2 = b[k][j];					
					sum += t1 * t2;
				}
				product[i][j] = sum;
			}
		}
	}

	public static void invert(double[][] a, double[][] inv_a, int order)  // done in place if a == inv_a
	{
		RealMatrix aMatrix = new Array2DRowRealMatrix(a);
		
		RealMatrix aInv = new LUDecompositionImpl(aMatrix).getSolver().getInverse();
		
		for (int i = 0; i < inv_a.length; i++)
			for (int j = 0; j < inv_a[i].length; j++)
				inv_a[i][j] = aInv.getData()[i][j];
		/*
		int	i, icol, irow, j, k, l, ll;
		double	big, dum, pivinv, temp;
	        
		int[] indxc = new int[order];
		int[] indxr = new int[order];
		int[] ipiv = new int[order];
	        
        if (inv_a != a) {
        	for (i = 0; i < order; i++)
        		for (j = 0; j < order; j++)
        			inv_a[i][j] = a[i][j];
        }
        icol = 0;
        irow = 0;
		
		for (i = 0; i < order; i++) {
			big = ZERO;
			for (j = 0; j < order; j++)
				if (ipiv[j] != 1)
					for (k = 0; k < order; k++) {
						if (ipiv[k] == 0) {
							if (Math.abs(inv_a[j][k]) >= big) {
								big = Math.abs(inv_a[j][k]);
								irow = j;
								icol = k;
							}
						}
					}
			++ipiv[icol];
			if (irow != icol) {
				for (l = 0; l < order; l++) {
					temp = inv_a[irow][l];
					inv_a[irow][l] = inv_a[icol][l];
					inv_a[icol][l] = temp;
				}
			}
			indxr[i] = irow;
			indxc[i] = icol;
			if (inv_a[icol][icol] == ZERO) {
				logger.error("invert_matrix: Singular Matrix\n");
				throw new RuntimeException();
	        }
			pivinv = ONE / inv_a[icol][icol];
			inv_a[icol][icol] = ONE;
			for (l = 0; l < order; l++)
				inv_a[icol][l] *= pivinv;
			for (ll = 0; ll < order; ll++) {
				if (ll != icol) {
					dum = inv_a[ll][icol];
					inv_a[ll][icol] = ZERO;
					for (l = 0; l < order;l++)
						inv_a[ll][l] -= inv_a[icol][l] * dum;
				}
			}
		}
	        
		for (l = (order - 1);l >= 0; l--) {
			if (indxr[l] != indxc[l]) {
				for (k = 0; k < order; k++) {
					temp = inv_a[k][indxr[l]];
					inv_a[k][indxr[l]] = inv_a[k][indxc[l]];
					inv_a[k][indxc[l]] = temp;
				}
			}
		}*/
	}


	public static void unsymmeig(double[][] a, int order, /*LONG_COMPLEX*/Complex[] eigs)
	{
	        int     i, j;
		
	
	        balance(a, order);  
	        elmhes(a, order);
	        hqr(a, order, eigs);
		
	        return;
	}


	public static void balance(double[][] a, int order)
	{
		double    radix, sqrdx, c, r, g, f, s, t;
		int     i, j;
		boolean done = false;


		radix = (double) RADIX;
		sqrdx = radix * radix;

		while (!done) {
			done = true;
			for (i = 0; i < order; i++) {
				r = c = ZERO;
				for (j = 0; j < order; j++)
					if (j != i) {
						c += Math.abs(a[j][i]);
						r += Math.abs(a[i][j]);
					}
				if (c != ZERO && r != ZERO) {
					g = r / radix;
					f = ONE;
					s = c + r;
					while (c < g) {
						f *= radix;
						c *= sqrdx;
					}
					g = r * radix;
					while (c > g) {
						f /= radix;
						c /= sqrdx;
					}
					if (((c + r) / f) < ((double) 0.95 * s)) {
						done = false;
						g = 1.0 / f;
						for (j = 0; j < order; j++)
							a[i][j] *= g;
						for (j = 0; j < order; j++)
							a[j][i] *= f;
					}
				}
			}
		}
	}


	public static void elmhes(double[][] a, int order)
	{
		int     i, j, m;
		double    x, y, t1;
	
	
		for (m = 1; m < (order - 1); m++) {
			x = ZERO;
			i = m;
			for (j = m; j < order; j++) {
				if (Math.abs(a[j][m-1]) > Math.abs(x)) {
					x = a[j][m - 1];
					i = j;
				}
			}
			if (i != m) {
				for (j = m - 1; j < order; j++) {
					t1 = a[i][j];
					a[i][j] = a[m][j];
					a[m][j] = t1;
				}
				for (j = 0; j < order; j++) {
					t1 = a[j][i];
					a[j][i] = a[j][m];
					a[j][m] = t1;
				}
			}
			if (x != ZERO) {
				for (i = m + 1; i < order; i++) {
					y = a[i][m - 1];
					if (y != ZERO) {
						y /= x;
						a[i][m - 1] = y;
						for (j = m; j < order; j++)
							a[i][j] -= (y * a[m][j]);
						for (j = 0; j < order; j++)
							a[j][m] += (y * a[j][i]);
					}
				}
			}
		}
	
		return;
	}


	public static void hqr(double[][] a, int order, /*LONG_COMPLEX*/Complex[] eigs)
	{
		int     nn, m, l, k, j, its, i, mmin, max;
		double    z, y, x, w, v, u, t, s, r, q, p, anorm, eps, t1, t2, t3, t4;
	
		p = 0;
		q = 0;
		r = 0;
	
		anorm = ZERO;
		eps = (double) EPS_SF8;
	
		for (i = 0; i < order; i++) {
			max = ((i - 1) > 0) ? (i - 1) : 0;
			for (j = max; j < order; j++) {
				anorm += Math.abs(a[i][j]);
			}
		}
	
		nn = order - 1;
		t = ZERO;
		while (nn >= 0) {
			its = 0;
			do { 
				for (l = nn; l > 0; l--) {
					t1 = Math.abs(a[l - 1][l - 1]);
					t2 = Math.abs(a[l][l]);
					s = t1 + t2;
					if (s == ZERO)
						s = anorm;
					t1 = Math.abs(a[l][l - 1]);
					if (t1 <= (eps * s)) {
						a[l][l - 1] = ZERO;
						break;
					}
				}
				x = a[nn][nn];
				if (l == nn) {
					eigs[nn--] = new Complex(x+t, ZERO);
//					eigs[nn].real = x + t;
//					eigs[nn--].imag = ZERO;
				} else {
					y = a[nn - 1][nn - 1];
					w = a[nn][nn - 1] * a[nn - 1][nn];
					if (l == (nn - 1)) {
						p = (double) 0.5 * (y - x);
						q = (p * p) + w;
						z = Math.sqrt(Math.abs(q));
						x += t;
						if (q >= ZERO) {
							t1 = SIGN(z, p);
							z = p + t1;
							eigs[nn - 1] = new Complex(x + z, eigs[nn - 1].getImaginary());
							eigs[nn] = new Complex(x + z, eigs[nn - 1].getImaginary());
//							eigs[nn - 1].real = eigs[nn].real = x + z;
							if (z != ZERO)
//								eigs[nn].real = x - w / z;
								eigs[nn] = new Complex(x - w / z, eigs[nn].getImaginary());
						} else {
							eigs[nn] = new Complex(x+p, -z);
//							eigs[nn].real = x + p;
//							eigs[nn].imag = -z;
							eigs[nn - 1] = new Complex(eigs[nn].getReal(), -eigs[nn].getImaginary());
//							eigs[nn - 1].real = eigs[nn].real;
//							eigs[nn - 1].imag = -eigs[nn].imag;
						}
						nn -= 2;
					} else {
						if (its == 30) {
							throw new RuntimeException("Too many iterations in hqr\n");
						}
						if (its == 10 || its == 20) {
							t += x;
							for (i = 0; i < nn + 1; i++)
								a[i][i] -= x;
							t1 = Math.abs(a[nn][nn - 1]);
							t2 = Math.abs(a[nn - 1][nn - 2]);
							s = t1 + t2;
							y = x = (double) 0.75 * s;
							w = (double) -0.4375 * s * s;
						}
						++its;
						for (m = nn - 2; m >= l; m--) {
							z = a[m][m];
							r = x - z;
							s = y - z;
							p = ((r * s - w) / a[m + 1][m]) + a[m][m + 1];
							q = a[m + 1][m + 1] - z - r - s;
							r = a[m + 2][m + 1];
							t1 = Math.abs(p);
							t2 = Math.abs(q);
							t3 = Math.abs(r);
							s = t1 + t2 + t3;
							p /= s;
							q /= s;
							r /= s;
							if (m == l)
								break;
							t1 = Math.abs(a[m][m - 1]);
							t2 = Math.abs(q);
							t3 = Math.abs(r);
							u = t1 * (t2 + t3);
							t1 = Math.abs(p);
							t2 = Math.abs(a[m - 1][m - 1]);
							t3 = Math.abs(z);
							t4 = Math.abs(a[m + 1][m + 1]);
							v = t1 * (t2 + t3 + t4);
							if (u <= (eps * v))
								break;
						}
						for (i = m; i < (nn - 1); i++) {
							a[i + 2][i] = ZERO;
							if (i != m)
								a[i + 2][i - 1] = ZERO;
						}
						for (k = m; k < nn; k++) {
							if (k != m) {
								p = a[k][k - 1];
								q = a[k + 1][k - 1];
								r = ZERO;
								if (k + 1 != nn)
									r = a[k + 2][k - 1];
								t1 = Math.abs(p);
								t2 = Math.abs(q);
								t3 = Math.abs(r);
								if ((x = t1 + t2 + t3) != ZERO) {
									p /= x;
									q /= x;
									r /= x;
								}
							}
							t1 = Math.sqrt((p * p) + (q * q) + (r * r));
							s = SIGN(t1, p);
							if (s != 0.0) {
								if (k == m) {
									if (l != m)
										a[k][k - 1] = -a[k][k - 1];
								} else
									a[k][k - 1] = -s * x;
								p += s;
								x = p / s;
								y = q / s;
								z = r / s;
								q /= p;
								r /= p;
								for (j = k; j < (nn + 1); j++) {
									p = a[k][j] + (q * a[k + 1][j]);
									if ((k + 1) != nn) {
										p += r * a[k + 2][j];
										a[k + 2][j] -= p * z;
									}
									a[k + 1][j] -= p * y;
									a[k][j] -= p * x;
								}
								mmin = nn < (k + 3) ? nn : k + 3;
								for (i = l; i < (mmin + 1); i++) {
									p = (x * a[i][k]) + (y * a[i][k + 1]);
									if ((k + 1) != nn) {
										p += z * a[i][k + 2];
										a[i][k + 2] -= p * r;
									}
									a[i][k + 1] -= p * q;
									a[i][k] -= p;
								}
							}
						}
					}
				}
			} while ((l + 1) < nn);
		}
	}


}
