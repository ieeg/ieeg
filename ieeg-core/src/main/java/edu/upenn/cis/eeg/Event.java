/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a set of timestamp ranges that together define an event
 * @author zives
 *
 */
public class Event {
	
	public Event(String typName) {
		typeName = typName;
		timestampId = new ArrayList<Integer>();
		sourceId = new ArrayList<Integer>();
		onset = new ArrayList<Integer>();
		offset = new ArrayList<Integer>();
	}
	
	public void addTimestamp(int id, int source, int onset, int offset) {
		timestampId.add(id);
		sourceId.add(source);
		this.onset.add(onset);
		this.offset.add(offset);
	}
	
	public int getNumTimestamps() {
		return timestampId.size();
	}
	
	public int getTimestampId(int inx) {
		return timestampId.get(inx);
	}
	
	public int getSourceId(int inx) {
		return sourceId.get(inx);
	}
	
	public int getOnset(int inx) {
		return offset.get(inx);
	}
	
	public int getOffset(int inx) {
		return offset.get(inx);
	}
	
	String typeName;
	private List<Integer> timestampId;
	private List<Integer> sourceId;
	private List<Integer> onset;
	private List<Integer> offset;
}

