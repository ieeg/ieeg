/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.filters;

import java.io.IOException;
import java.util.List;

import org.apache.commons.math.complex.Complex;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.collect.Lists;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.Matrix;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm;

/**
 * Butterworth filter based on Mayo C code by Matt Stead
 * (translated to Java and thus losing FP precision...)
 * 
 * @author zives, stead
 *
 */
public class TimeSeriesFilterButterworth extends TimeSeriesFilter {
	public static String NAME = "Butterworth-Stead";

	final double ZERO = 0.0;
	final double ONE = 1.0;
	final int BAD_BAD_FILTER = -1;
	
	TimeSeriesFilterButterworth second = null;
	
	double[] d_num;
	double[] d_den;
	
	double sample;
	double lo;
	double hi;

	int butterSize;
	
	/////////////
	public static final int NO_FILTER = (1 << 0);
	public static final int LOWPASS_FILTER = (1 << 1);
	public static final int HIGHPASS_FILTER = (1 << 2);
	public static final int BANDPASS_FILTER = (1 << 3);
	public static final int BANDSTOP_FILTER = (1 << 12);

	public static final int DEFAULT_FILTER_POLES = 3;//32;
	public static final int MAX_FILTER_POLES = 8;//128;
	public static final double DEFAULT_LOW_FC = 0.0;
	/////////////////
	
	public TimeSeriesFilterButterworth() {}
	
	public TimeSeriesFilterButterworth(double sampleFreq, FilterSpec fs) throws IllegalFilterException {
		init(sampleFreq, fs);
	}
	
	public void init(double sampleFreq, FilterSpec fs) throws IllegalFilterException {
		init(fs.getNumPoles(), fs.getFilterType(), sampleFreq, fs.getBandpassLowCutoff(), fs.getBandpassHighCutoff(),
				fs.getBandstopLowCutoff(), fs.getBandstopHighCutoff());
	}

//	public TimeSeriesFilterButterworth
	public void init(int nPoles, int filterType, double samplingFrequency, double bandPassLo, 
			double bandPassHi, double bandStopLo, double bandStopHi) 
	throws IllegalArgumentException
	{
		String typ = "";
		
		if ((filterType & LOWPASS_FILTER) != 0) {
			bandPassLo = bandPassHi;
			typ = "lowpass";
		} else if ((filterType & HIGHPASS_FILTER) != 0) {
			bandPassHi = bandPassLo;
			typ = "highpass";
		} else if ((filterType & BANDPASS_FILTER) != 0) {
			typ = "bandpass";
		} else if ((filterType & NO_FILTER) != 0) {
			typ = "(none)";
		}
		
		if ((filterType & BANDSTOP_FILTER) != 0) {
			if ((filterType != BANDSTOP_FILTER)) {
				second = new TimeSeriesFilterButterworth();
				second.init(nPoles, BANDSTOP_FILTER, samplingFrequency,
						bandStopLo, bandStopHi, 0, 0);
			}
			typ = typ + " bandstop";
		}
		
		lo = bandPassLo;
		hi = bandPassHi;
		sample = samplingFrequency;
		
		butterSize = butter(filterType, (float)samplingFrequency, (int)nPoles, (float)bandPassLo, (float)bandPassHi);
		
//		for (int i = 0; i <= butterSize; i++) {
//			System.out.println(String.valueOf(d_num[i]) + "\t" + String.valueOf(d_den[i]));
//		}
//		System.out.println("Generating " + typ + " filter for " + sample + " with " + lo + "," + hi);
		
        z = generate_initial_conditions(d_num, d_den, butterSize);
        
//        for (int i = 0; i < z.length; i++)
//        	System.out.println(z[i]);
	}

	public double[] apply(IntArrayWrapper data, int[] buffer) {
//		System.out.println("Applying filter to " + lo + "-" + hi + " from " + sample);
		return apply(data, 0, 0, data.length(), buffer);
	}
	
    double[] filt_data = null;//new double[nSamples + 6 * butterSize];
    int[] buf2 = null;
    int[][] toWrap = new int[1][];
    
	public synchronized double[] apply(IntArrayWrapper data, int bufferStart, int dataStart, 
			int nSamples, int[] buffer) {
//        double[] filt_data = new double[nSamples + 6 * butterSize];
		if (filt_data == null || filt_data.length < nSamples + 6 * butterSize)
			filt_data = new double[nSamples + 6 * butterSize];
		
        filtfilt(d_num, d_den, butterSize, data, dataStart, nSamples, filt_data);
        
        if (second == null) {
	        for (int i = bufferStart; i < bufferStart + nSamples; i++)
	        	buffer[i] = (int)filt_data[i - bufferStart];
        } else {
        	if (buf2 == null || buf2.length < nSamples)
        		buf2 = new int[nSamples];
	        for (int i = bufferStart; i < bufferStart + nSamples; i++)
	        	buf2[i] = (int)filt_data[i - bufferStart];
	        toWrap[0] = buf2;
        	IntArrayWrapper wrap = new IntArrayWrapper(toWrap);
        	
        	second.apply(wrap, bufferStart, dataStart, nSamples, buffer);
        }

        return filt_data;
	}
	
	public String toString() {
		return sample + " to " + lo + "-" + hi;
	}

	/////////////////////// Filter construction
	
	
	int butter(int type, float d_samp_freq, int n_poles, float fc1, float fc2)
	{
		int	i, j, n_fcs, offset, idx, arr_size;
		double		samp_freq, fcs[], den[], sum_num, sum_den;
		double		u[], half_pi, bw, wn, w, r[], num[], ratio;
	        double            a[][], inv_a[][], ta1[][], ta2[][], b[][], bt[][], c[][], d, t;
	    Complex csum_num, csum_den, cratio, ckern[];
	    Complex p[], tc, eigs[], cden[], rc[], cnum[];
		
		u = new double[2];
		fcs = new double[2];
		tc = new Complex(0,0);
		offset = 0;
		bw = 0;
	    wn = 0;
	    w = 0;
	    csum_num = new Complex(0,0);
	    csum_den = new Complex(0,0);
	    cratio = new Complex(0,0);
	    tc = new Complex(0,0);
	        
		samp_freq = (double) d_samp_freq;
		fcs[0] = (double) fc1;
		n_fcs = (int)((((type & LOWPASS_FILTER) != 0) || 
				((type & HIGHPASS_FILTER) != 0)) ? 1 : 2);
		if (n_fcs == 2) {
			fcs[1] = fc2;
		}
		arr_size = (int)(n_fcs * n_poles);
		boolean is_odd = (n_poles % 2) != 0;
		
		// step 1: get analog, pre-warped frequencies
	    half_pi = Math.PI / (double) 2.0;
	    for (i = 0; i < n_fcs; ++i) 
	            u[i] = (double) 4.0 * Math.tan((Math.PI * fcs[i]) / samp_freq);

//		System.out.print("u:  ");
//		for (i = 0; i < n_fcs; i++)
//			System.out.print(u[i] + " ");
//	    System.out.println();
	    
		// step 2: convert to low-pass prototype estimate
	    if ((type & LOWPASS_FILTER) != 0)
			wn = u[0];
	    
	    if ((type & BANDPASS_FILTER) != 0) {
			bw = u[1] - u[0];
			wn = Math.sqrt(u[0] * u[1]);
	    }
	    
	    if ((type & HIGHPASS_FILTER) != 0)
			wn = u[0];
	
	    if ((type & BANDSTOP_FILTER) != 0) {
			bw = u[1] - u[0];
			wn = Math.sqrt(u[0] * u[1]);
		}
		
		// step 3: Get N-th order Butterworth analog LOWPASS_FILTER prototype
		p = new Complex[n_poles];//LONG_COMPLEX[n_poles];
		for (i = 1; i < n_poles; i += 2) {
			p[i - 1] = new Complex(0,((Math.PI * (double) i) / (double) (2 * n_poles)) + half_pi);//LONG_COMPLEX();
//	        System.out.println("p[" + (i-1) + "] init = " + p[i-1].getReal() + ";" + p[i-1].getImaginary());
	                p[i - 1] = complex_expl(p[i - 1]);
//	        System.out.println("p[" + (i-1) + "] = " + p[i-1].getReal() + ";" + p[i-1].getImaginary());
		}
	        for (i = 1; i < n_poles; i += 2) {
	    			p[i] = new Complex(p[i - 1].getReal(), -p[i - 1].getImaginary());//new LONG_COMPLEX();
//	    	        System.out.println("p[" + i + "] = " + p[i].getReal() + ";" + p[i].getImaginary());
	        }
	        if (is_odd)
	        	p[n_poles - 1] = new Complex(-1.0, 0);//p[n_poles-1].getImaginary());//.real = (double) -1.0;
	        
	        j = (int)(n_poles - 1);  // sort into ascending order, by real values
	        if (is_odd) --j;
	        for (i = 0; j > i; ++i, --j) {
	                tc = p[i];
	                p[i] = p[j];
	                p[j] = tc;
	        }

//			System.out.print("p:  ");
//			for (i = 0; i < n_poles; i++)
//				System.out.print("(" + p[i].getReal() + "," + p[i].getImaginary() + ") \n");
//		    System.out.println();
		    
	        // Transform to state-space
	        a = new double[arr_size][];
	        inv_a = new double[arr_size][];
	        ta1 = new double[arr_size][];
	        ta2 = new double[arr_size][];
	
	        for (i = 0; i < arr_size; ++i) {
	        	a[i] = new double[arr_size];
	        	inv_a[i] = new double[arr_size];
	        	ta1[i] = new double[arr_size];
	        	ta2[i] = new double[arr_size];
	        }
	        
	        
	        b = new double[arr_size][1];
	        bt = new double[1][arr_size];
	        c = new double[1][arr_size];
		
		if (is_odd) {
			a[0][0] = (double) -1.0;
			offset = 1;
		} else
			offset = 0;
		for (i = 0; i < n_poles - 1; i += 2) {
			idx = (int)(i + offset);//((is_odd) ? 1 : 0));
			if (idx != 0)
				a[i + offset][idx - 1] = ONE;
			a[i + offset][i + offset] = p[i].getReal() + p[i + 1].getReal();//real;
			a[i + offset][i + offset + 1] = (double) -1.0;
			a[i + offset + 1][i + offset] = ONE;		
		}
		b[n_poles - 1][0] = ONE;
		c[0][n_poles - 1] = ONE;
		d = ZERO;

//		System.out.println("a:  ");
//		for (i = 0; i < n_poles; i++) {
//			for (j = 0; j < n_poles; j++)
//				System.out.print(a[i][j] + " ");
//		    System.out.println();
//		}
		
		// step 4: Transform to LOWPASS_FILTER, FilterSpec.BANDPASS_FILTER, FilterSpec.HIGHPASS_FILTER, or FilterSpec.BANDSTOP_FILTER of desired Wn
		switch (type) {
			case LOWPASS_FILTER:
				for (i = 0; i < n_poles; ++i) {
					for (j = 0; j < n_poles; ++j)
						a[i][j] *= wn;
					b[i][0] *= wn;
				}
				break;
			case BANDPASS_FILTER:
				for (i = 0; i < n_poles; ++i) {
					for (j = 0; j < n_poles; ++j) {
						a[i][j] *= bw;
					}
					a[i][i + n_poles] = wn;
					a[i + n_poles][i] = -wn;
				}
				b[0][0] = bw;
				break;	
			case HIGHPASS_FILTER:
				for (i = 0; i < n_poles; ++i) {
					c[0][i] = (double) -1.0;
					b[i][0] = wn;
				}
				for (i = (int)((is_odd) ? 1 : 0); i < n_poles; i += 2) {
					c[0][i + 1] = a[i][i];
					b[i][0] = ZERO;
				}
				d = ONE;
				Matrix.invert(a, inv_a, n_poles);
				
				for (i = 0; i < n_poles; ++i)
					for (j = 0; j < n_poles; ++j)
						a[i][j] = wn * inv_a[i][j];			
				break;
			case BANDSTOP_FILTER:
				for (i = 0; i < n_poles; ++i) {
					c[0][i] = (double) -1.0;
					b[i][0] = bw;
				}
				for (i = (int)((is_odd) ? 1 : 0); i < n_poles; i += 2) {
					c[0][i + 1] = a[i][i];
					b[i][0] = ZERO;
				}
				Matrix.invert(a, inv_a, n_poles);
				for (i = 0; i < n_poles; ++i) {
					for (j = 0; j < n_poles; ++j) {
						a[i][j] = bw * inv_a[i][j];
					}
					a[i][i + n_poles] = wn;
					a[i + n_poles][i] = -wn;
				}
				d = ONE;
				break;	
		}
		
		// step 5: Use bilinear transformation to find discrete equivalent
		t = (double) 0.25;	
		for (i = 0; i < arr_size; ++i) {
			for (j = 0; j < arr_size; ++j) {
				ta1[i][j] = t * a[i][j];
				ta2[i][j] = -ta1[i][j];
			}
			ta1[i][i] += ONE;
			ta2[i][i] += ONE;
		}
	
		Matrix.invert(ta2, inv_a, arr_size);
		
		Matrix.multiply(inv_a, ta1, a, arr_size, arr_size, arr_size);
	
		Matrix.multiply(c, inv_a, bt, 1, arr_size, arr_size);
		t = Math.sqrt((double) 0.5);
		for (i = 0; i < arr_size; ++i)
			c[0][i] = bt[0][i] * t;
		
		double[][] tArr = new double[1][1];
		tArr[0][0] = t;
		
		Matrix.multiply(bt, b, tArr, 1, arr_size, 1);
		d += (t * 0.25);
		
		double[][] btArr2 = new double[arr_size][1];
		for (i = 0; i < arr_size; i++)
			btArr2[i][0] = bt[0][i];

		Matrix.multiply(inv_a, b, btArr2, arr_size, arr_size, 1);
		t = ONE / Math.sqrt(2.0);
		for (i = 0; i < arr_size; ++i)
			b[i][0] = btArr2[i][0] * t;
	        
		// Transform to zero-pole-gain and polynomial forms
		eigs = new Complex[arr_size];//LONG_COMPLEX[arr_size];
		for (i = 0; i < arr_size; i++)
			eigs[i] = new Complex(0,0);//LONG_COMPLEX();
		Matrix.unsymmeig(a, arr_size, eigs);
	
		den = new double[arr_size + 1];
		cden = new Complex[arr_size + 1];//LONG_COMPLEX[arr_size + 1];
		for (i = 1; i < arr_size + 1; i++)
			cden[i] = new Complex(0,0);//LONG_COMPLEX();
		cden[0] = new Complex(ONE, 0);//.real = ONE;
		for (i = 0; i < arr_size; ++i) {
			for (j = (int)(i + 1); j-- > 0; ) {
				tc = eigs[i].multiply(cden[j]);
//				tc = complex_multl(eigs[i], cden[j]);
				cden[j + 1] = new Complex(cden[j + 1].getReal() - tc.getReal(),
										cden[j + 1].getImaginary() - tc.getImaginary());
//				cden[j + 1].real -= tc.real;
//				cden[j + 1].imag -= tc.imag;
			}
		}
		for (i = 0; i <= arr_size; ++i)
			den[i] = cden[i].getReal();
		
		// generate numerator
		r = new double[arr_size + 1];
		rc = new Complex[arr_size + 1];//LONG_COMPLEX[arr_size + 1];
		for (i = 0; i < arr_size + 1; i++)
			rc[i] = new Complex(0,0);//LONG_COMPLEX();
		wn = (double) 2.0 * Math.atan2(wn, 4.0);
		
		switch (type) {
			case LOWPASS_FILTER:
				for (i = 0; i < arr_size; ++i)
					r[i] = (double) -1.0;
				break;
			case BANDPASS_FILTER:
				for (i = 0; i < n_poles; ++i) {
					r[i] = ONE;
					r[i + n_poles] = (double) -1.0;
				}
				w = -wn;
				break;	
			case HIGHPASS_FILTER:
				for (i = 0; i < arr_size; ++i)
					r[i] = ONE;
				w = -Math.PI;
				break;
			case BANDSTOP_FILTER:
				tc = new Complex(ZERO, wn);
//				tc.real = ZERO;
//				tc.imag = wn;
				tc = complex_expl(tc);
				for (i = 0; i < arr_size; i += 2) {
					rc[i] = tc;
//					rc[i].real = tc.real;
//					rc[i].imag = tc.imag;
					rc[i + 1] = new Complex(tc.getReal(), -tc.getImaginary());
//					rc[i + 1].real = tc.real;
//					rc[i + 1].imag = -tc.imag;				
				}
				break;	
		}
	
		num = new double[arr_size + 1];
		cnum = new Complex[arr_size + 1];//LONG_COMPLEX[arr_size + 1];
		for (i = 0; i < arr_size + 1; i++)
			cnum[i] = new Complex(0,0);//LONG_COMPLEX();
		if (type == BANDSTOP_FILTER) {
			cnum[0] = new Complex(ONE, 0);
//			cnum[0].real = ONE;
			for (i = 0; i < arr_size; ++i) {
				for (j = (int)(i + 1); j >= 0; j--) {
//					tc = complex_multl(rc[i], cnum[j]);
					tc = rc[i].multiply(cnum[j]);
					cnum[j + 1] = tc;
//					cnum[j + 1].real -= tc.real;
//					cnum[j + 1].imag -= tc.imag;
				}
			}
			for (i = 0; i <= arr_size; ++i)
				num[i] = cnum[i].getReal();//.real;
		} else {
			num[0] = ONE;
			for (i = 0; i < arr_size; ++i)
				for (j = (int)(i + 1); j-- > 0; )
					num[j + 1] -= r[i] * num[j];
		}
		
//		System.out.println("---");
//		for (i = 0; i < arr_size + 1; i++)
//			System.out.println(num[i] + " " + den[i] + " ");
//		System.out.println("---");
	
		// normalize
		ckern = new Complex[arr_size + 1];//LONG_COMPLEX[arr_size + 1];
		for (i = 0; i < arr_size + 1; i++)
			ckern[i] = new Complex(0,0);//LONG_COMPLEX();
		if ((type == LOWPASS_FILTER) || (type == BANDSTOP_FILTER)) {
			sum_num = ZERO;
			sum_den = ZERO;
			for (i = 0; i <= arr_size; ++i) {
				sum_num += num[i];
				sum_den += den[i];
			}
			ratio = sum_den / sum_num;
//			System.out.println("Ratio = " + ratio + " from " + sum_num + " " + sum_den);
			for (i = 0; i <= arr_size; ++i)
				num[i] *= ratio;
		} else {
//			tc.real = ZERO;
			tc = new Complex(ZERO, tc.getImaginary());
			for (i = 0; i <= arr_size; ++i) {
//				tc.imag = w * (double) i;
				tc = new Complex(tc.getReal(), w * i);
				ckern[i] = complex_expl(tc);
				cnum[i] = new Complex(num[i], ZERO);
//				cnum[i].real = num[i];
//				cnum[i].imag = ZERO;
				cden[i] = new Complex(den[i], ZERO);
//				cden[i].real = den[i];
//				cden[i].imag = ZERO;
			}
			csum_num = new Complex(ZERO, ZERO);
			csum_den = new Complex(ZERO, ZERO);
			for (i = 0; i <= arr_size; ++i) {
				tc = ckern[i].multiply(cnum[i]);
//				tc = complex_multl(ckern[i], cnum[i]);
//				csum_num.real += tc.real;
//				csum_num.imag += tc.imag;
				csum_num = csum_num.add(tc);
				tc = ckern[i].multiply(cden[i]);
//				tc = complex_multl(ckern[i], cden[i]);
//				csum_den.real += tc.real;
//				csum_den.imag += tc.imag;
				csum_den = csum_den.add(tc);

			}
			//complex_divl(&csum_den, &csum_num, &cratio);
//			cratio = complex_divl(csum_den, csum_num);
			cratio = csum_den.divide(csum_num);
			for (i = 0; i <= arr_size; ++i) {
//				tc = complex_multl(cnum[i], cratio);
				tc = cnum[i].multiply(cratio);
//				num[i] = tc.real;
				num[i] = tc.getReal();
			}			
		}
		
//		System.out.println("---");
//		for (i = 0; i < arr_size; i++)
//			System.out.println(num[i] + " " + den[i]);
//		System.out.println("---");
	
		// set & check output
		d_num = new double[arr_size + 1];
		d_den = new double[arr_size + 1];
		for (i = 0; i <= arr_size; ++i) {
			d_num[i] = num[i];
			if (Double.isNaN(d_num[i]) || Double.isInfinite(d_num[i]))
			  {
			    throw new RuntimeException("isNaN");
			  }
			d_den[i] = den[i];
			if (Double.isNaN(d_den[i]) || Double.isInfinite(d_den[i]))
			  {
			    throw new RuntimeException("isNaN");
			
			  }
		}
	
//	    System.exit(1);
	    
	    return(arr_size);
	}

//////////////////////// Complex math functions start

	Complex complex_expl(Complex exponent)
	{
		double            c;
	
		c = Math.exp(exponent.getReal());//.real);
	
		Complex  ans = new Complex(c * Math.cos(exponent.getImaginary()), c * Math.sin(exponent.getImaginary()));
	
		return ans;
	}

///////////////////////////////////////////////////
	
	public static final int MAX_ORDER = 50;
	
	double[] z = null;
	
    double[] filt_buf;
    double zc[] = new double[MAX_ORDER];

	void filtfilt(double[] num, double[] den, int order, IntArrayWrapper data, int start, int samples,
			double[] filt_data)
	{
	        int		i, j, k, m, padded_data_len, pad_len, pad_lenx2;
	        double		dx2, t1, t2;

	        
	        pad_len = order * 3;
	        pad_lenx2 = pad_len * 2;
	        
	        // front pad
	        dx2 = (double) data.get(start + 0) * 2.0;
	        i = 0;
	        for (j = pad_len; j != 0; --j) {
	           filt_data[i++] = dx2 - (double) data.get(start + j);
	        }
	        // copy data
	        i = 0;
	        for (j = pad_len; i < samples; ++j)
	                filt_data[j] = data.get(start + i++);
	        // back pad        
	        padded_data_len = samples + pad_lenx2;
	        dx2 = (double) data.get(start + samples - 1) * 2.0;
	        for (i = samples + (pad_len), j = samples - 2; i < padded_data_len; ++i, --j)
	                filt_data[i] = dx2 - (double) data.get(start + j);
		        
	        // check arguments & allocate as needed
	        if (z == null) {
                z = generate_initial_conditions(num, den, order);
	        }
	        
	        if (filt_buf == null || filt_buf.length < samples + pad_lenx2)
	        	filt_buf = new double[samples + pad_lenx2];

	        // copy and initialize initial conditions
	        for (i = 0; i < order; ++i)
	                zc[i] = z[i] * filt_data[0];
	        // forward filter from filt_data to filt_buf
	        for (i = 0; i < padded_data_len; ++i) {
	                t1 = filt_data[i];
	                t2 = (num[0] * t1) + zc[0];
	                for (j = 1; j < order; ++j) {
	                        zc[j - 1] = (num[j] * t1) - (den[j] * t2) + zc[j];
	                }
	                zc[order - 1] = (num[order] * t1) - (den[order] * t2);
	                filt_buf[i] = t2;
	        }
		
	        // copy and initialize initial conditions
	        for (i = 0; i < order; ++i)
	                zc[i] = z[i] * filt_buf[padded_data_len - 1];
	        // reverse filter from filt_buf to filt_data
	        i = padded_data_len - 1;
	        for (k = pad_len; k != 0; k--) {
	                t1 = filt_buf[i--];
	                t2 = (num[0] * t1) + zc[0];
	                for (j = 1; j < order; ++j) {
	                        zc[j - 1] = (num[j] * t1) - (den[j] * t2) + zc[j];
	                }
	                zc[order - 1] = (num[order] * t1) - (den[order] * t2);
	        }
	        m = i - pad_len;
	        for (k = samples; k != 0; k--) {
	                t1 = filt_buf[i--];
	                t2 = (num[0] * t1) + zc[0];
	                for (j = 1; j < order; ++j) {
	                        zc[j - 1] = (num[j] * t1) - (den[j] * t2) + zc[j];
	                }
	                zc[order - 1] = (num[order] * t1) - (den[order] * t2);
	                filt_data[m--] = t2;
	        }
	}


	double[] generate_initial_conditions(double[] num, double[] den, int order)
	{
	        int     i, j;
	  
	        double[][] rhs;
	        double[][] z;
	        double[][] q;
	        double[] out_z;

	        q = new double[order][];

	        for (i = 0; i < order; ++i)
	        	q[i] = new double[order];

	        rhs = new double[order][1];
	        z = new double[order][1];
	        out_z = new double[order];
	        
	        q[0][0] = (double) 1.0 + (double) den[1];
	        j = 2;
	        for (i = 1; i < order; ++i){
	                q[i][0] = (double) den[j];
	                j++;
	        }
	        for (i = 1; i < order; ++i) {
	                q[i - 1][i] = (double) -1.0;
	                q[i][i] = (double) 1.0;
	        }
	        j = 1;
	        for (i = 0; i < order; ++i){
	            rhs[i][0] = (double) num[j] - ((double) num[0] * (double) den[j]);
	            j++;
	        }
	        
//	        System.out.println("Dump of q:");
//	        for (i = 0; i < order; i++) {
//	        	for (j = 0; j < order; j++)
//	        		System.out.print(q[i][j] + " ");
//	        	System.out.println();
//	        }
//	        
	        double[][] qInv = new double[q.length][q.length];
	       
	        Matrix.invert(q, qInv, order);
	        
//	        System.out.println("Dump of q inverted:");
//	        for (i = 0; i < order; i++) {
//	        	for (j = 0; j < order; j++)
//	        		System.out.print(qInv[i][j] + " ");
//	        	System.out.println();
//	        }
	        
//	        double[][] qRes = new double[q.length][q.length];
//	        mat_multl(q, qInv, qRes, order, order, order);

//	        System.out.println("Dump of product with inverse:");
//	        for (i = 0; i < order; i++) {
//	        	for (j = 0; j < order; j++)
//	        		System.out.print(qRes[i][j] + " ");
//	        	System.out.println();
//	        }
	        
	        Matrix.multiply(qInv, rhs, z, order, order, 1);
	        
	        for (i = 0; i < order; ++i)
	                out_z[i] = z[i][0];
	        
	        return(out_z);
	}

	@Override
	public String getFilterType() {
		return NAME;
	}

	@Override
	public ISignalProcessingAlgorithm create(String name, JsonTyped parmJson)
			throws JsonParseException, IOException {
		TimeSeriesFilterButterworth me = new TimeSeriesFilterButterworth();
		me.setParameters(parmJson);
		try {
			me.init(me.parameters.getSampleFrequency(), me.parameters.getFilterSpec());
		} catch (IllegalFilterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JsonParseException(e.getMessage(), null);
		}
		return me;
	}

	@Override
	public List<IDetection> annotate(String channel, String revId, double start, double period, int startIndex,
			int endIndex, ITimeSeries series) {		// TODO Auto-generated method stub
		return Lists.newArrayList();
	}
}
