/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.maf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import edu.upenn.cis.db.mefview.shared.Annotation;

public class MAFReader {
	public static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();	

	public static Map<String,String> getTraces(String study) throws IllegalArgumentException {
		HashMap<String,String> channelSources = new HashMap<String,String>();
		
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			Document doc = db.parse(study);
			
			NodeList list = doc.getElementsByTagName("Source");
			
			for (int i = 0; i < list.getLength(); i++) {
				String channelName = list.item(i).getAttributes().getNamedItem("label").getNodeValue();
				String fileName = list.item(i).getAttributes().getNamedItem("name").getNodeValue();
				channelSources.put(channelName, fileName);
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return channelSources;
	}
	
	public static List<Annotation> getAnnotations(String study, String type) {
		HashMap<String,String> channelSources = new HashMap<String,String>();
		
		List<Annotation> events = new ArrayList<Annotation>();
		
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			Document doc = db.parse(study);

			NodeList list = doc.getElementsByTagName("Episode");

			long start = Long.valueOf(list.item(0).getAttributes().getNamedItem("recording_start_time").getNodeValue());
			System.out.println("* Found Episode at " + start);
			
			list = doc.getElementsByTagName("Source");
			
			for (int i = 0; i < list.getLength(); i++) {
				String channelId = list.item(i).getAttributes().getNamedItem("id").getNodeValue();
				String channelName = list.item(i).getAttributes().getNamedItem("label").getNodeValue();
				//String fileName = list.item(i).getAttributes().getNamedItem("name").getNodeValue();
				channelSources.put(channelId, channelName);
//				System.out.println("* Found Source at " + channelName + ":" + channelId);
			}
			
			list = doc.getElementsByTagName("Event");
			
			for (int i = 0; i < list.getLength(); i++) {
				if (type.isEmpty() || 
						list.item(i).getAttributes().getNamedItem("type").getNodeValue().equals(type)) {
					
					Node eventType = list.item(i).getAttributes().getNamedItem("type");
					
//					a.type = eventType.getNodeValue();
					
//					a.creator = study;
					
					Annotation a = new Annotation(study, eventType.getNodeValue(), 0, 0, new HashSet<String>());
					events.add(a);
					
					NodeList eventList = list.item(i).getChildNodes();
					for (int j = 0; j < eventList.getLength(); j++) {
						if (eventList.item(j).getNodeName().equals("Timestamp")) {
							
							Double onset = Double.valueOf(eventList.item(j).getAttributes().getNamedItem("onset").getNodeValue()) - start;
							String sourceId;
							// Skip if no source, e.g., for XlTek annotations
							if (eventList.item(j).getAttributes().getNamedItem("SourceID") == null)
								continue;//sourceId = "all";
							else
								sourceId = eventList.item(j).getAttributes().getNamedItem("SourceID").getNodeValue();
							onset += start;
//							System.out.println("* Found event at " + onset);
							
							a.setStart(onset);
							
							if (!sourceId.equals("all"))
								a.getChannels().add(channelSources.get(sourceId));
							else
								a.getChannels().add("all");
							
							Node n = eventList.item(j).getAttributes().getNamedItem("offset"); 
							if (n != null) {
								Double offset = Double.valueOf(n.getNodeValue()) - start;
								offset += start;
								
								a.setEnd(offset);
							}
						}
					}
//					System.out.println("Added annotation " + a.toJSON(false));
				}
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return events;
	}
	
	public static SortedMap<Double,HashMap<String,Set<String>>> getEvents(String study, String type) {
		HashMap<String,String> channelSources = new HashMap<String,String>();
		TreeMap<Double,HashMap<String,Set<String>>> events = new TreeMap<Double,HashMap<String,Set<String>>>();
		
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			Document doc = db.parse(study);

			NodeList list = doc.getElementsByTagName("Episode");

			long start = Long.valueOf(list.item(0).getAttributes().getNamedItem("recording_start_time").getNodeValue());
			System.out.println("* Found Episode at " + start);
			
			list = doc.getElementsByTagName("Source");
			
			for (int i = 0; i < list.getLength(); i++) {
				String channelId = list.item(i).getAttributes().getNamedItem("id").getNodeValue();
				String channelName = list.item(i).getAttributes().getNamedItem("label").getNodeValue();
				//String fileName = list.item(i).getAttributes().getNamedItem("name").getNodeValue();
				channelSources.put(channelId, channelName);
				System.out.println("* Found Source at " + channelName + ":" + channelId);
			}
			
			list = doc.getElementsByTagName("Event");
			
			for (int i = 0; i < list.getLength(); i++) {
				if (type.isEmpty() || list.item(i).getAttributes().getNamedItem("type").getNodeValue().equals(type)) {
					
					Node eventType = list.item(i).getAttributes().getNamedItem("type");
					NodeList eventList = list.item(i).getChildNodes();
					for (int j = 0; j < eventList.getLength(); j++) {
						if (eventList.item(j).getNodeName().equals("Timestamp")) {
							
							Double onset = Double.valueOf(eventList.item(j).getAttributes().getNamedItem("onset").getNodeValue()) - start;
							String sourceId;
							// Skip if no source, e.g., for XlTek annotations
							if (eventList.item(j).getAttributes().getNamedItem("SourceID") == null)
								continue;//sourceId = "all";
							else
								sourceId = eventList.item(j).getAttributes().getNamedItem("SourceID").getNodeValue();
							onset += start;
							System.out.println("* Found event at " + onset);
							
							HashMap<String,Set<String>> sources = events.get(onset);
							if (sources == null) {
								sources = new HashMap<String,Set<String>>();
								events.put(onset, sources);
							}
							Set<String> sourceSet;
							sourceSet = sources.get(eventType.getNodeValue()+".on");
							if (sourceSet == null) {
								sourceSet = new HashSet<String>();
								sources.put(eventType.getNodeValue() + ".on", sourceSet);
							}
							
							if (!sourceId.equals("all"))
								sourceSet.add(channelSources.get(sourceId));
							else
								sourceSet.add("all");
							
							Node n = eventList.item(j).getAttributes().getNamedItem("offset"); 
							if (n != null) {
								Double offset = Double.valueOf(n.getNodeValue()) - start;
								offset += start;
								sources = events.get(offset);
								if (sources == null) {
									sources = new HashMap<String,Set<String>>();
									events.put(offset, sources);
								}
								sourceSet = sources.get(eventType.getNodeValue()+".off");
								if (sourceSet == null) {
									sourceSet = new HashSet<String>();
									sources.put(eventType.getNodeValue() + ".off", sourceSet);
								}
								if (!sourceId.equals("all"))
									sourceSet.add(channelSources.get(sourceId));
								else
									sourceSet.add("all");
								
							}
						}
					}
				}
			}
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return events;
	}
}
