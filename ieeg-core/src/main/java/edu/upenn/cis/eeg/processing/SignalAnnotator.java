/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import edu.upenn.cis.db.mefview.shared.IDetection;

public abstract class SignalAnnotator extends SignalProcessingAlgorithm {

	@Override
	public boolean isAnnotator() {
		return true;
	}

	@Override
	public boolean isTranscoder() {
		return false;
	}

	@Override
	public List<AnnotatedTimeSeries> process(AnnotatedTimeSeries series) {
		List<IDetection> newAnn = new ArrayList<IDetection>();
		newAnn.addAll(series.getAnnotations());
		
		AnnotatedTimeSeries ret = 
				new AnnotatedTimeSeries(series.getChannelName(), series.getRevId(), 
						series.getTimeSeries(), series.getStart(), series.getPeriod(),
						newAnn);
		
		if (isGapSensitive()) {
			int start = 0;
			do {
				while (series.isInGap(start) && series.getSeriesLength() > start)
					start++;
				if (start == series.getSeriesLength())
					break;
				
				int end = start + 1;
				while (series.getSeriesLength() > end && !series.isInGap(end))
					end++;
				
				try {
					processSegment(ret, start, end-1, series.getStart() +
							start * series.getPeriod(), series.getPeriod());
				} catch (RuntimeException re) {
					re.printStackTrace();
				}
				start = end;
			} while (start < series.getSeriesLength());
		} else {
			processSegment(ret, 0, series.getSeriesLength() - 1, series.getStart(), series.getPeriod());
		}
		return Lists.newArrayList(ret);
	}
}
