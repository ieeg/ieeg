/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;

/**
 * Spline approximation to a curve / waveform
 * 
 * @author zives, stead
 *
 */
public class Spline {

	public static final int TAIL_LEN = 6;

//	void spline_splint_array(si4 *inArr, si4 inArr.length, si4 *out_arr, si4 outArr.length)
	public static void splineSplintArray(final IntArrayWrapper inArr, int[] outArr)
	{
//		double	*prev_y, *next_y, *ty, out_x, out_x_inc, a, b;
//		double	*td2y, *prev_d2y, *next_d2y, *tu, *prev_u, p, sig;
//		int		*tin, *tout, lo_pt, hi_pt;
		int i;
		double h;
		double a;
		double b;
		int prevD2y, tD2y, nextD2y;
		int prevY, tY, nextY;
		int prevU, tU;
		double p;
		double outX, outXinc;
		int hiPt, lowPt;
		int tOut;
			
		// passed storage would be more efficient here
		double[] y = new double[inArr.length() + TAIL_LEN];
		double[] d2y = new double[inArr.length() + TAIL_LEN];
		double[] u = new double[inArr.length() + TAIL_LEN];

		for (i = 0; i < inArr.length(); i++)
			y[i] = inArr.get(i);
			
		h = 2.0 * y[inArr.length() - 1];
		for (i = 0; i < TAIL_LEN; i++) {
			y[inArr.length() + i] = h - y[inArr.length() - 2 - i];
		}
		
		// spline
		d2y[0] = u[0] = 0.0;	
		prevD2y = 0;
		tD2y = 1;
//		prev_d2y = d2y;
//		td2y = prev_d2y + 1;
		prevY = 0;
		tY = 1;
		nextY = tY + 1;
		prevU = 0;
		tU = 1;
//		prev_y = y;
//		ty = prev_y + 1;
//		next_y = ty + 1;
//		prev_u = u;
//		tu = prev_u + 1;
		
//		inArr.length += TAIL_LEN;
//		for (i = inArr.length - 2; i--;) {
		for (i = 0; i < inArr.length() + TAIL_LEN - 2; i++) {
//			p = (*prev_d2y++ * 0.5) + 2.0;
			p = d2y[prevD2y++] * 0.5 + 2.0;
			
//			*td2y++ = -0.5 / p;
			d2y[tD2y++] = -0.5 / p;
			
			u[tU] = y[nextY++] - y[tY] - (y[tY] - y[prevY++]);
//			*tu = (*next_y++ - *ty) - (*ty - *prev_y++);
//			++ty;
			tY++;
			u[tU++] = 3.0 * u[tU] - 0.5 * u[prevU++] / p;
//			*tu++ = (3.0 * *tu - 0.5 * *prev_u++) / p;
		}	
//		*td2y = 0.0;
		y[tD2y] = 0.0;

//		next_d2y = d2y + inArr.length;
		nextD2y = inArr.length() + TAIL_LEN;
		tD2y = nextD2y - 1;
//		td2y = next_d2y - 1;
		tU = inArr.length() - 1;
//		tu = u + inArr.length - 1;
//		for (i = inArr.length - 2; i--;)
		for (i = 0; i < inArr.length() - 2; i++)
//			*td2y-- = (*td2y * *next_d2y--) + *tu--;
			d2y[tD2y--] = d2y[tD2y] * d2y[nextD2y--] + u[tU--];
//		inArr.length -= TAIL_LEN;
		
		// splint
//		out_x_inc = (sf8) inArr.length / (sf8) outArr.length;
		outXinc = inArr.length() / (double) outArr.length;
//		out_x = -out_x_inc;
		outX = -outXinc;
//		tout = out_arr;
		tOut = 0;
		
		
//		for (i = outArr.length; i--;) {
		for (i = 0; i < outArr.length; i++) {
			outX += outXinc;
			lowPt = (int) outX;
			hiPt = lowPt + 1;
			a = hiPt - outX;
			b = 1.0 - a;
			outArr[tOut++] = (int) ((a * y[lowPt] + b * y[hiPt] + 
					((a * a * a - a) * d2y[lowPt] + (b * b * b - b) * d2y[hiPt]) / 6.0) + 0.5);
		}
		
		return;
	}
}
