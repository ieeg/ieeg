/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.mef;

import static com.google.common.base.Throwables.propagate;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.server.PageReader;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.eeg.TimeSeriesFileReader;
import edu.upenn.cis.thirdparty.RED;

public class MEFReader extends TimeSeriesFileReader {
	String filename;
//	IObjectHandle file;
	
	boolean lazy = false;
	
	long[] timeIndex = null; 
	long[] offsetIndex = null; 
	long[] sampleIdxIndex = null; 
	int[] blockLengthsIndex = null; 
	byte[] blockFlagsIndex = null;

	static RED redDecoder = new RED();
//	static KeyGenerator kgen = null;
//	String encryptionKey = null;
	
	private final static Logger logger = LoggerFactory.getLogger(MEFReader.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time." + MEFReader.class.getName());
	
	ByteBuffer buf = null;
	
	public MEFReader(String filename) throws IOException {
		this(filename, false);
	}
	
	public MEFReader(String filename, boolean lazy) throws IOException {
		super(filename, 0);
		this.lazy = lazy;

		init();
		
	}

	public MEFReader(IStoredObjectReference handle) throws IOException {
		this(handle, false);
	}
	
	public MEFReader(IStoredObjectReference handle, boolean lazy) throws IOException {
		super((handle.getDirBucketContainer() == null ? "" : handle.getDirBucketContainer().getFileDirectory() + "/") + 
				handle.getFilePath(), 0);
		this.lazy = lazy;

		init();
		
	}
	/**
	 * Open file and FileChannel, read the header
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException {
		super.init();
		/*
		try {
			infile = new RandomAccessFile(filename, "r");
			fc = infile.getChannel();
		} catch (FileNotFoundException f) {
			File d = new File(".");
			logger.error("Cannot find " + filename + " in current dir: " + d.getCanonicalPath());
			System.err.println("Cannot find " + filename + " in current dir: " + d.getCanonicalPath());
			f.printStackTrace();
			throw f;
		}
		bb = ByteBuffer.allocate(1024);
		pos = readHeader();*/
		if (!getCompressionScheme().equals("Range Encoded Differences (RED)"))
			throw new IOException("Unsupported compression format: must be RED");
		if (!isLittleEndian())
			throw new IOException("Unsupported endian-ness: must be little-endian");
		
//		if (!lazy)
//			buildIndex();
		if (lazy) {
			buf  = ByteBuffer.allocate(16);//4 * (Long.SIZE / 8));
			buf.order(ByteOrder.LITTLE_ENDIAN);
		}
		
//		System.out.println("File " + filename + " initialized");
		//infileStream.close();
	}

	public void close() {
		super.close();
//		bb = null;
		timeIndex = null;
		offsetIndex = null;
		sampleIdxIndex = null;
		blockLengthsIndex = null;
		blockFlagsIndex = null;
	}
	
	private MefHeader2 getMEFHeader() {
		return (MefHeader2)header;
	}
	
	/**
	 * Read the index structures into memory
	 * 
	 * @throws IOException
	 */
	 public synchronized void buildIndex() throws IOException {
		/*
		if (hdr_info.data_encryption_used) {
			AES_KeyExpansion(4, 10, encryptionKey, hdr_info.session_password); 
		}
		 */
		
		if (getMEFHeader().getDataEncryptionUsed() != 0) {
//			AES_KeyExpansion(4, 10, encryptionKey, hdr_info.session_password); 
		}
		
		int count = (int)getNumIndexEntries();
		
		ByteBuffer bbI = ByteBuffer.allocate(count * 3 * (Long.SIZE / 8));
		bbI.order(ByteOrder.LITTLE_ENDIAN);
		
		logger.debug("There are " + count + " entries in the index...");
		timeIndex = new long[count]; 
		offsetIndex = new long[count]; 
		sampleIdxIndex = new long[count]; 
//		indexLength = count; 
		
		getFileChannel().read(bbI, getMEFHeader().getIndexDataOffset());
		bbI.rewind();
		
		// Read the index data
		for (int i = 0; i < count; i++) {
			
			timeIndex[i] = bbI.getLong();
			offsetIndex[i] = bbI.getLong();
			//Need to read past the sample number index entry
			sampleIdxIndex[i] = bbI.getLong();
		}
	}
	
	public long[] getTimeIndex() throws IOException{
		if (timeIndex == null)
			buildIndex();
		return timeIndex;
	}
	
	@Override
	public long getStartTime() {
		try {
			return getTimeIndex(0);
		} catch (IOException ioe) {
			throw propagate(ioe);
		}
	}
	
	public long[] getOffsetIndex() throws IOException {
		if (timeIndex == null)
			buildIndex();
		return offsetIndex;
	}
	
	public long[] getSampleIdxIndex() throws IOException {
		if (timeIndex == null)
			buildIndex();
		return sampleIdxIndex;
	}
		
	public int[] getBlockLengthsIndex() throws IOException {  // WATCH It was supposed to be uint32
		if (timeIndex == null)
			buildIndex();  // For offsetIndex
		if (blockLengthsIndex == null) {
			int count = (int)getNumIndexEntries();
			ByteBuffer bbRED = ByteBuffer.allocate(31);  // Will hold the 1st 31 bytes of every RED block small header
														 // (covers all except block stats & var-length compressed data)
			bbRED.order(ByteOrder.LITTLE_ENDIAN);
			
			blockLengthsIndex = new int[count];
			blockFlagsIndex = new byte[count];
		    int bytesSkip = 4 + 4 + 8 + 4;  // # of bytes inserted at start of each RED block's header before getting to block length 4-byte word
			// Read from block headers
			for (int b = 0; b < count; b++) {
				getFileChannel().read(bbRED,offsetIndex[b]);
				bbRED.rewind();
				blockLengthsIndex[b] = bbRED.getInt(bytesSkip);  // Reads 4 bytes at absolute pos from beginning. WATCH It was supposed to be uint32
	//			bbRED.position(bytesSkip);  // The above doesn't move position
	//			bbRED.getInt(); bbRED.getShort(); // Skip 6 more bytes, here 4+2 but representing 3+3 ...
	//	        blockFlagsIndex[b] = bbRED.get();  // Read 1 byte. WATCH It was supposed to be uint8
				blockFlagsIndex[b] = bbRED.get(bytesSkip+10);
		          // (int)(blockFlags[b] & 0x01) gives the discontinuity bit
			}
		}
		return blockLengthsIndex;
	}
	
	public long getMaximumBlockLength() throws IOException {
		return header.getMaximumBlockLength();
	}
	
	public byte[] getBlockFlagsIndex() throws IOException {
		if (blockLengthsIndex == null)
			getBlockLengthsIndex();
		return blockFlagsIndex;
	}
	
//	public double getSampleRate() {
//		return header.getSamplingFrequency();
//	}

	@Override
	public long readHeader() throws IOException {
		header = new MefHeader2(getFileChannel(), bb);//getFile());
//		header.print();
		return getMEFHeader().getBytesRead();
	}
	
	public String getCompressionScheme() {
		return (getMEFHeader().getCompressionAlgorithm());
	}
	
	private boolean isLittleEndian() {
		return getMEFHeader().getByteOrderCode() == 1;
	}
	
	public long getNumIndexEntries() {
		return getMEFHeader().getNumberOfIndexEntries();
	}
	
	private synchronized long retrieveTimeEntryFromIndex(int inx) throws IOException {
		
		@SuppressWarnings("unused")
		long size = getFileChannel().size();
		
		buf.rewind();
		int bytes = getFileChannel().read(buf, getMEFHeader().getIndexDataOffset() + 3 * inx * (Long.SIZE / 8));
		while (bytes < Long.SIZE / 8) {
			int b = getFileChannel().read(buf);
			if (b == -1)
				break;
			bytes += b;
		}
		buf.rewind();
		return buf.getLong();
	}

	private synchronized long retrieveOffsetEntryFromIndex(int inx) throws IOException {
		buf.rewind();
		int bytes = getFileChannel().read(buf, getMEFHeader().getIndexDataOffset() + 3 * inx * (Long.SIZE / 8) + (Long.SIZE / 8));
		while (bytes < Long.SIZE / 8) {
			int b = getFileChannel().read(buf);
			if (b == -1)
				break;
			bytes += b;
		}
		buf.rewind();
		return buf.getLong();
	}
	
	private int binSearchTimeEntry(long value, int start, int end) throws IOException {
		if (start > end || start < 0 || end > getTimeIndexLength())
			throw new IllegalArgumentException("Sorry, illegal bounds");
		
		int mid = 0;
		while (!(start > end)) {
			mid = (start + end) / 2;
			
			long entry = retrieveTimeEntryFromIndex(mid);
			if (entry == value)
				return mid;
			else if (entry > value)
				end = mid - 1;
			else  {
				start = mid + 1;
				mid++;
			}
		}
		return -mid - 1;
	}
	
	private long getTimeIndex(int inx) throws IOException {
		if (lazy)
			return retrieveTimeEntryFromIndex(inx);
		
		if (timeIndex == null)
			buildIndex();
		return timeIndex[inx];
	}
	
	private int getTimeIndexOf(long time) throws IOException {
		if (!lazy) {
			if (timeIndex == null)
				buildIndex();
			return Arrays.binarySearch(timeIndex, time);
		} else
			return binSearchTimeEntry(time, 0, getTimeIndexLength() - 1);
	}
	
	private int getTimeIndexLength() throws IOException {
		return (int)getNumIndexEntries();
	}
	
	private long getOffsetIndex(int inx) throws IOException {
		if (lazy)
			return retrieveOffsetEntryFromIndex(inx);
		
		if (offsetIndex == null)
			buildIndex();

		return offsetIndex[inx];
	}
	
	private int getOffsetIndexLength() throws IOException {
		return (int)getNumIndexEntries();
	}

	@Override
	public PageReader.PageList getPages(long startTime, long endTime, int resolution) throws IOException {
		PageReader.PageList ret = new PageReader.PageList();
		
		pos = 0;
		long st = startTime;//(long)(startTime * 1000);
		long et = endTime;//(long)(endTime * 1000);
		
		if (st > header.getRecordingEndTime())
			throw new IOException("Request for time past end");
		if (st < header.getRecordingStartTime()) {
			st = header.getRecordingStartTime();
			et = st + endTime - startTime;
		}

		if (et > header.getRecordingEndTime())
			et = header.getRecordingEndTime();
		
		// Find start index
		int i = getTimeIndexOf(st);//Arrays.binarySearch(timeIndex, st);
		if (i < 0)
			i = -i - 2;

		int len = getTimeIndexLength();
		for (; i < len - 1; i++)
			if (getTimeIndex(i+1) > st)
				break;
		
		// Period in usec
		double period = 1000000. / header.getSamplingFrequency();
		
		// Find end index
		int j = getTimeIndexOf(et);//Arrays.binarySearch(timeIndex, et); // i
		if (j < 0)
			j = -j - 1;
		for (; j < len - 1; j++)
			if (getTimeIndex(j+1) > et)
				break;
		
		ret.startPage = i;
		ret.startOffset = (long)((st - getTimeIndex(i)) / period);
		ret.endPage = j;
		if (j < len)
			ret.endOffset = (long)((et - getTimeIndex(j)) / period + 1);
		else
			ret.endOffset = 0;
		
		ret.startTime = st;
		ret.endTime = et;
		
		// This is the time index for the *next* page, in case
		// this one doesn't have any relevant samples.  (We can't
		// tell without reading it.)
		
		if (i < len - 1)
			ret.nextStart = getTimeIndex(i+1);
		else
			ret.nextStart = 0;
		
		return ret;
	}
	
	private byte[] mainBuf = new byte[10240];
	
	private ByteBuffer bb2 = ByteBuffer.wrap(mainBuf);//ByteBuffer.allocate(10240);

	@Override
	public synchronized //ArrayList<MEFReader.MEFPage> 
	TimeSeriesPage[] readPages(long pageId, int numPages, boolean doDecode)
	throws IOException {
		
		final String M = "readPages(long, int, boolean)";
		
		if (pageId >= getOffsetIndexLength())
			return new TimeSeriesPage[0];//>();
		
		long startFileOffset = getOffsetIndex((int)pageId);

		// Period in usec
		double period = 1000000. / header.getSamplingFrequency();
		
		int amount;
		if (pageId + numPages >= getNumIndexEntries()) {
			amount = (int)(getMEFHeader().getIndexDataOffset() - getOffsetIndex((int)pageId));
			numPages = (int)(getNumIndexEntries() - pageId);
		} else {
			amount = (int)(getOffsetIndex((int)pageId+numPages) - getOffsetIndex((int)pageId));
		}

		if (amount > mainBuf.length) {
			logger.trace(
					"{}: resizing from bb2 from {} to {}", 
					new Object[] {M, 
					mainBuf.length,
					amount});
			mainBuf = new byte[amount];
			bb2 = ByteBuffer.wrap(mainBuf);
		}
		bb2.order(ByteOrder.LITTLE_ENDIAN);

		bb2.rewind();
		
		long bytes;
		
		long readIn = System.nanoTime();
		
		try {
			bytes = getFileChannel().read(bb2, startFileOffset);
			
			// If there is a file I/O error, try re-reading once more
		} catch (IOException e) {
			infile[0] = new RandomAccessFile(filename, "r");
			setFileChannel(0, infile[0].getChannel());
			bytes = getFileChannel().read(bb2, startFileOffset);
		}
		
		timeLogger.trace("{}: read {} seconds", M, BtUtil.diffNowThenSeconds(readIn));
		
		if (bytes < amount)
			throw new RuntimeException("Unable to read full amount " + amount + "(" + bytes +")");
		
		bb2.rewind();

		TimeSeriesPage[] results = new TimeSeriesPage[numPages];//ArrayList<MEFReader.MEFPage>(numPages); 
		for (int i = 0; i < numPages; i++) {
			TimeSeriesPage p = new TimeSeriesPage();
//			results.add(p);
			results[i] = p;
	
			p.timeStart = getTimeIndex((int)pageId + i);
			
			long startInx = getOffsetIndex((int)(pageId + i)) - getOffsetIndex((int)pageId);  
			int len;
			if (pageId + i == getNumIndexEntries() - 1) {
				len = (int)(getMEFHeader().getIndexDataOffset() - getOffsetIndex((int)pageId + i));
			} else {
				len = (int)(getOffsetIndex((int)pageId+1+i) - getOffsetIndex((int)pageId+i));
			}

			try {
				if (doDecode) {
					p.values = redDecoder.decode(mainBuf, (int)startInx, (int)(len));//buffer.length);
				} else {
					p.values = new int[(int)(len - startInx)];
					for (int x = (int)startInx; x < (int)len; x++)
						p.values[x] = mainBuf[x];
				}
			} catch (RuntimeException rt) {
				logger.error("Error in reading " + filename + " page " + pageId + " (time " + getTimeIndex((int)pageId) +")");
				logger.error("Seek to " + startFileOffset + ", read " + amount + ", decode " + (len - startInx));////(pos - startFileOffset));
				rt.printStackTrace();
				throw rt;
			}
	
			// Use # samples to determine end time
			p.timeEnd = (long)(p.timeStart + period * p.values.length);
		}
		return results;
	}
}
