/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg;

/**
 * Savgol smoothing routine
 * 
 * @author stead
 *
 */
public class SavgolSmoother {
//	void	savgol(c, np, nl, nr, ld, m)
//	double	*c;
//	int	np, nl, nr, ld, m;
//	{
//		int	imj, ipj, j, k, kk, mm, *indx;
//		double	d, fac, sum, **a, *b;
//		void	lubksb(), ludcmp();
//
//		if ((np < nl + nr + 1) || (nl < 0) || (nr < 0) || (ld > m) || (m > nl + nr)) {
//			(void) fprintf(stderr, "%cbad arguments to savgol\n", 7);
//			exit(1);
//		}
//
//		a = (double **) calloc((size_t) (m + 1), sizeof(double *));
//		b = (double *) calloc((size_t) (m + 1), sizeof(double));
//		indx = (int *) calloc((size_t) (m + 1), sizeof(int));
//		if ((a == NULL) || (b == NULL) || (indx == NULL)) {
//			(void) fprintf(stderr, "%cnot enough memory to run savgol\n", 7);
//			exit(1);
//		}
//		for (j = 0; j < m + 1; ++j) {
//			a[j] = (double *) calloc((size_t) (m + 1), sizeof(double));
//			if (a[j] == NULL) {
//				(void) fprintf(stderr, "%cnot enough memory to run savgol\n", 7);
//				exit(1);
//			}
//		}
//
//		for (j = 0; j < m + 1; ++j)
//			--a[j];
//		--a; --b; --indx;
//
//		for (ipj = 0; ipj <= (m << 1); ++ipj) {
//			sum = (ipj ? 0.0 : 1.0);
//			for (k = 1; k <= nr; ++k)
//				sum += pow((double) k, (double) ipj);
//			for (k = 1; k <= nl; ++k)
//				sum += pow((double) -k, (double) ipj);
//			mm = (2 * m) - ipj;
//			if (ipj < mm)
//				mm = ipj;
//			for (imj = -mm; imj <= mm; imj += 2)
//				a[1 + ((ipj + imj) / 2)][1 + ((ipj - imj) / 2)] = sum;
//		}
//		ludcmp(a, m + 1, indx, &d);
//		for (j = 1; j <= (m + 1); ++j)
//			b[j] = 0.0;
//		lubksb(a, m + 1, indx, b);
//		for (kk = 1; kk <= np; ++kk)
//			c[kk] = 0.0;
//		for (k = -nl; k <= nr; ++k) {
//			sum = b[1];
//			fac = 1.0;
//			for (mm = 1; mm <= m; ++mm)
//				sum += b[mm + 1] * (fac *= k);
//			kk = ((np -k) % np) + 1;
//			c[kk] = sum;
//		}
//
//		++a; ++b; ++indx;
//		for (j = 0; j < m + 1; ++j) {
//			++a[j];
//			free(a[j]);
//		}
//		free(a);
//		free(b);
//		free(indx);
//
//		return;
//	}
//
//
//	void	ludcmp(a, n, indx, d)
//	double	**a, *d;
//	int	n, *indx;
//	{
//		int	i, imax, j, k;
//		double	big, dum, sum, temp, *vv;
//
//		vv = (double *) calloc((size_t) n, sizeof(double));
//		if (vv == NULL) {
//			(void) fprintf(stderr, "%cnot enough memory to run ludcmp\n", 7);
//			exit(1);
//		}
//		--vv;
//
//		*d = 1.0;
//		for (i = 1; i <= n; ++i) {
//			big = 0.0;
//			for (j = 1; j <= n; ++j) {
//				temp = a[i][j];
//				if (temp < 0.0)
//					temp = -temp;
//				if (temp > big)
//					big = temp;
//			}
//			if (big == 0.0) {
//				(void) fprintf(stderr, "%csingular matrix in routine ludcmp\n", 7);
//				exit(1);
//			}
//			vv[i] = 1.0 / big;
//		}
//		for (j = 1; j <= n; ++j) {
//			for (i = 1; i < j; ++i) {
//				sum = a[i][j];
//				for (k = 1; k < i; ++k)
//					sum -= a[i][k] * a[k][j];
//				a[i][j] = sum;
//			}
//			big = 0.0;
//			for (i = j; i <= n; ++i) {
//				sum = a[i][j];
//				for (k = 1; k < j; ++k)
//					sum -= a[i][k] * a[k][j];
//				a[i][j] = sum;
//				dum = vv[i] * sum;
//				if (dum < 0.0)
//					dum = -dum;
//				if (dum >= big) {
//					big = dum;
//					imax = i;
//				}
//			}
//			if (j != imax) {
//				for (k = 1; k <= n; ++k) {
//					dum = a[imax][k];
//					a[imax][k] = a[j][k];
//					a[j][k] = dum;
//				}
//				*d = -(*d);
//				vv[imax] = vv[j];
//			}
//			indx[j] = imax;
//			if (a[j][j] == 0.0)
//				a[j][j] = MINDOUBLE;
//			if (j != n) {
//				dum = 1.0 / a[j][j];
//				for (i = j + 1; i <= n; ++i)
//					a[i][j] *= dum;
//			}
//		}
//
//		++vv;
//		free(vv);
//
//		return;
//	}
//
//
//	void	lubksb(a, n, indx, b)
//	double	**a, *b;
//	int	n, *indx;
//	{
//		int	i, ii, ip, j;
//		double	sum;
//
//		ii = 0;
//		for (i = 1; i <= n; ++i) {
//			ip = indx[i];
//			sum = b[ip];
//			b[ip] = b[i];
//			if (ii)
//				for (j = ii; j <= (i - 1); ++j)
//					sum -= a[i][j] * b[j];
//			else if (sum)
//				ii = i;
//			b[i] = sum;
//		}
//		for (i = n; i >= 1; --i) {
//			sum = b[i];
//			for (j = i + 1; j <= n; ++j)
//				sum -= a[i][j] * b[j];
//			b[i] = sum / a[i][i];
//		}
//
//		return;
//	}
//
//
//	void	convolve(a1, a2, n1, n2, c)
//	double	*a1, *a2, *c;
//	int	n1, n2;
//	{
//		int	i, j, nc;
//
//		--n1; --n2;
//		nc = n1 + n2;
//
//		for (i = 0; i <= nc; ++i) {
//			c[i] = 0;
//			if (n1 >= n2) {
//				if (i <= n2)
//					for (j = 0; j <= i; ++j)
//						c[i] += a1[j] * a2[i - j]; 
//				else if (i <= n1)
//					for (j = i - n2; j <= i; ++j)
//						c[i] += a1[j] * a2[i - j];
//				else
//					for (j = i - n2; j <= n1; ++j)
//						c[i] += a1[j] * a2[i - j];
//			}
//			else {
//				if (i <= n1)
//					for (j = 0; j <= i; ++j)
//						c[i] += a1[j] * a2[i - j]; 
//				else if (i <= n2)
//					for (j = 0; j <= n1; ++j)
//						c[i] += a1[j] * a2[i - j];
//				else 
//					for (j = i - n2; j <= n1; ++j)
//						c[i] += a1[j] * a2[i - j];
//			}
//		}
//
//		return;
//	}
	
}
