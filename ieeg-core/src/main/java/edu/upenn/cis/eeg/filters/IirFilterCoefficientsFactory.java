/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.filters;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import biz.source_code.dsp.filter.FilterCharacteristicsType;
import biz.source_code.dsp.filter.FilterPassType;
import biz.source_code.dsp.filter.IirFilterCoefficients;
import biz.source_code.dsp.filter.IirFilterDesignFisher;
import edu.upenn.cis.db.mefview.services.FilterSpec;

/**
 * @author John Frommeyer
 *
 */
public class IirFilterCoefficientsFactory {

	public static List<IirFilterCoefficients> getCoefficients(
			double sampleFreq,
			FilterSpec fs,
			FilterCharacteristicsType filterCharType) {
		final List<IirFilterCoefficients> coefficients = new ArrayList<>();
		List<FilterPassType> fp = Lists.newArrayList();
		if ((fs.getFilterType() & FilterSpec.LOWPASS_FILTER) != 0) {
			fp.add(FilterPassType.lowpass);
			if (fs.getBandpassHighCutoff() == 0)
				throw new RuntimeException(
						"Lowpass filter needs high frequency cutoff");
		} else if ((fs.getFilterType() & FilterSpec.HIGHPASS_FILTER) != 0) {
			fp.add(FilterPassType.highpass);
			if (fs.getBandpassLowCutoff() == 0)
				throw new RuntimeException(
						"Highpass filter needs low frequency cutoff");
		} else if ((fs.getFilterType() & FilterSpec.BANDPASS_FILTER) != 0) {
			fp.add(FilterPassType.bandpass);
			if (fs.getBandpassLowCutoff() == 0)
				throw new RuntimeException(
						"Bandpass filter needs low frequency cutoff");
			if (fs.getBandpassHighCutoff() == 0)
				throw new RuntimeException(
						"Bandpass filter needs high frequency cutoff");
		}

		// Bandstop is independent of the above and might need to be chained
		if ((fs.getFilterType() & FilterSpec.BANDSTOP_FILTER) != 0) {
			fp.add(FilterPassType.bandstop);
			if (fs.getBandstopLowCutoff() == 0)
				throw new RuntimeException(
						"Bandstop filter needs low frequency cutoff");
			if (fs.getBandstopHighCutoff() == 0)
				throw new RuntimeException(
						"Bandstop filter needs high frequency cutoff");
		}

		for (FilterPassType ft : fp) {
			double fcf1 = -1;
			double fcf2 = -1;
			switch (ft) {
				case lowpass:
					fcf1 = fs.getBandpassHighCutoff() / sampleFreq;
					// fcf2 is irrelevant
					break;
				case highpass:
					fcf1 = fs.getBandpassLowCutoff() / sampleFreq;
					// fcf2 is irrelevant
					break;
				case bandpass:
					fcf1 = fs.getBandpassLowCutoff() / sampleFreq;
					fcf2 = fs.getBandpassHighCutoff() / sampleFreq;
					break;
				case bandstop:
					fcf1 = fs.getBandstopLowCutoff() / sampleFreq;
					fcf2 = fs.getBandstopHighCutoff() / sampleFreq;
					break;
				default:
					throw new IllegalArgumentException(
							"Unknown FilterPassType: " + ft);
			}
			final IirFilterCoefficients coeff = IirFilterDesignFisher.design(
					ft,
					filterCharType,
					fs.getNumPoles()/* order */,
					0/* ripple / ignore for non-Chebyshev */,
					fcf1/* fcf1 */,
					fcf2/* fcf2 */);
			coefficients.add(coeff);
		}
		return coefficients;

	}
}
