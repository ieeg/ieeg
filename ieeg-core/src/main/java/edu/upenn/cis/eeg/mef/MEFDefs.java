/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.mef;

public final class MEFDefs {
	/************* header version & constants *******************/

	public static final int HEADER_MAJOR_VERSION = 2;
	public static final int HEADER_MINOR_VERSION = 0;
	public static final int MEF_HEADER_LENGTH = 1024;
	public static final int DATA_START_OFFSET = MEF_HEADER_LENGTH;
	public static final int UNENCRYPTED_REGION_OFFSET = 0;
	public static final int UNENCRYPTED_REGION_LENGTH = 176;
	public static final int SUBJECT_ENCRYPTION_OFFSET = 176;
	public static final int SUBJECT_ENCRYPTION_LENGTH = 160;
	public static final int SESSION_ENCRYPTION_OFFSET = 352;
	public static final int SESSION_ENCRYPTION_LENGTH = 496; // maintain
																// multiple of
																// 16
	public static final int ENCRYPTION_BLOCK_BITS = 128;
	public static final int ENCRYPTION_BLOCK_BYTES = (ENCRYPTION_BLOCK_BITS / 8);

	/******************** header fields *************************/

	// Begin Unencrypted Block
	public static final int INSTITUTION_OFFSET = 0;
	public static final int INSTITUTION_LENGTH = 64; // $(63)
	public static final int UNENCRYPTED_TEXT_FIELD_OFFSET = 64;
	public static final int UNENCRYPTED_TEXT_FIELD_LENGTH = 64; // $(63)
	public static final int ENCRYPTION_ALGORITHM_OFFSET = 128;
	public static final int ENCRYPTION_ALGORITHM_LENGTH = 32; // $(29)
	public static final int SUBJECT_ENCRYPTION_USED_OFFSET = 160;
	public static final int SUBJECT_ENCRYPTION_USED_LENGTH = 1; // ui1
	public static final int SESSION_ENCRYPTION_USED_OFFSET = 161;
	public static final int SESSION_ENCRYPTION_USED_LENGTH = 1; // ui1
	public static final int DATA_ENCRYPTION_USED_OFFSET = 162;
	public static final int DATA_ENCRYPTION_USED_LENGTH = 1; // ui1
	public static final int BYTE_ORDER_CODE_OFFSET = 163;
	public static final int BYTE_ORDER_CODE_LENGTH = 1; // ui1
	public static final int HEADER_MAJOR_VERSION_OFFSET = 164;
	public static final int HEADER_MAJOR_VERSION_LENGTH = 1; // ui1
	public static final int HEADER_MINOR_VERSION_OFFSET = 165;
	public static final int HEADER_MINOR_VERSION_LENGTH = 1; // ui1
	public static final int HEADER_LENGTH_OFFSET = 166;
	public static final int HEADER_LENGTH_LENGTH = 2; // ui2
	public static final int SESSION_UNIQUE_ID_OFFSET = 168;
	public static final int SESSION_UNIQUE_ID_LENGTH = 8; // ui1
	// End Unencrypted Block

	// Begin Subject Encrypted Block
	public static final int SUBJECT_FIRST_NAME_OFFSET = 176;
	public static final int SUBJECT_FIRST_NAME_LENGTH = 32; // $(31)
	public static final int SUBJECT_SECOND_NAME_OFFSET = 208;
	public static final int SUBJECT_SECOND_NAME_LENGTH = 32; // $(31)
	public static final int SUBJECT_THIRD_NAME_OFFSET = 240;
	public static final int SUBJECT_THIRD_NAME_LENGTH = 32; // $(31)
	public static final int SUBJECT_ID_OFFSET = 272;
	public static final int SUBJECT_ID_LENGTH = 32; // $(31)
	public static final int SESSION_PASSWORD_OFFSET = 304;
	public static final int SESSION_PASSWORD_LENGTH = ENCRYPTION_BLOCK_BYTES; // $(15)
	public static final int SUBJECT_VALIDATION_FIELD_OFFSET = 320;
	public static final int SUBJECT_VALIDATION_FIELD_LENGTH = 16;
	// End Subject Encrypted Block

	// Begin Protected Block
	public static final int PROTECTED_REGION_OFFSET = 336;
	public static final int PROTECTED_REGION_LENGTH = 16;
	// End Protected Block

	// Begin Session Encrypted Block
	public static final int SESSION_VALIDATION_FIELD_OFFSET = 352;
	public static final int SESSION_VALIDATION_FIELD_LENGTH = 16; // ui1
	public static final int NUMBER_OF_SAMPLES_OFFSET = 368;
	public static final int NUMBER_OF_SAMPLES_LENGTH = 8; // ui8
	public static final int CHANNEL_NAME_OFFSET = 376;
	public static final int CHANNEL_NAME_LENGTH = 32; // $(31)
	public static final int RECORDING_START_TIME_OFFSET = 408;
	public static final int RECORDING_START_TIME_LENGTH = 8; // ui8
	public static final int RECORDING_END_TIME_OFFSET = 416;
	public static final int RECORDING_END_TIME_LENGTH = 8; // ui8
	public static final int SAMPLING_FREQUENCY_OFFSET = 424;
	public static final int SAMPLING_FREQUENCY_LENGTH = 8; // sf8
	public static final int LOW_FREQUENCY_FILTER_SETTING_OFFSET = 432;
	public static final int LOW_FREQUENCY_FILTER_SETTING_LENGTH = 8; // sf8
	public static final int HIGH_FREQUENCY_FILTER_SETTING_OFFSET = 440;
	public static final int HIGH_FREQUENCY_FILTER_SETTING_LENGTH = 8; // sf8
	public static final int NOTCH_FILTER_FREQUENCY_OFFSET = 448;
	public static final int NOTCH_FILTER_FREQUENCY_LENGTH = 8; // sf8
	public static final int VOLTAGE_CONVERSION_FACTOR_OFFSET = 456;
	public static final int VOLTAGE_CONVERSION_FACTOR_LENGTH = 8; // sf8
	public static final int ACQUISITION_SYSTEM_OFFSET = 464;
	public static final int ACQUISITION_SYSTEM_LENGTH = 32; // $(31)
	public static final int CHANNEL_COMMENTS_OFFSET = 496;
	public static final int CHANNEL_COMMENTS_LENGTH = 128; // $(127)
	public static final int STUDY_COMMENTS_OFFSET = 624;
	public static final int STUDY_COMMENTS_LENGTH = 128; // $(127)
	public static final int PHYSICAL_CHANNEL_NUMBER_OFFSET = 752;
	public static final int PHYSICAL_CHANNEL_NUMBER_LENGTH = 4; // si4
	public static final int COMPRESSION_ALGORITHM_OFFSET = 756;
	public static final int COMPRESSION_ALGORITHM_LENGTH = 32; // $(31)
	public static final int MAXIMUM_COMPRESSED_BLOCK_SIZE_OFFSET = 788;
	public static final int MAXIMUM_COMPRESSED_BLOCK_SIZE_LENGTH = 4; // ui4
	public static final int MAXIMUM_BLOCK_LENGTH_OFFSET = 792;
	public static final int MAXIMUM_BLOCK_LENGTH_LENGTH = 8; // ui8
	public static final int BLOCK_INTERVAL_OFFSET = 800;
	public static final int BLOCK_INTERVAL_LENGTH = 8; // sf8
	public static final int MAXIMUM_DATA_VALUE_OFFSET = 808;
	public static final int MAXIMUM_DATA_VALUE_LENGTH = 4; // si4
	public static final int MINIMUM_DATA_VALUE_OFFSET = 812;
	public static final int MINIMUM_DATA_VALUE_LENGTH = 4; // si4
	public static final int INDEX_DATA_OFFSET_OFFSET = 816;
	public static final int INDEX_DATA_OFFSET_LENGTH = 8; // ui8
	public static final int NUMBER_OF_INDEX_ENTRIES_OFFSET = 824;
	public static final int NUMBER_OF_INDEX_ENTRIES_LENGTH = 8; // ui8
	public static final int BLOCK_HEADER_LENGTH_OFFSET = 832;
	public static final int BLOCK_HEADER_LENGTH_LENGTH = 2; // ui2
	public static final int UNUSED_HEADER_SPACE_OFFSET = 834;
	public static final int UNUSED_HEADER_SPACE_LENGTH = 190;

	// End Session Encrypted Block
	private MEFDefs() {}
}
