/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.mefview.server.PageReader;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

public abstract class TimeSeriesFileReader implements PageReader<TimeSeriesPage> {
	protected RandomAccessFile[] infile;
	private FileChannel[] fc;
	public TimeSeriesChannel header;
	int channel;
	String[] filenames;
	protected ByteBuffer bb;
	
	protected long pos;
	
	final static Logger logger = LoggerFactory
	.getLogger(TimeSeriesFileReader.class);
	final static Logger timeLogger = LoggerFactory.getLogger("time." + TimeSeriesFileReader.class.getName());
	
	ByteBuffer buf = null;
	
	public TimeSeriesFileReader(String[] filename, int channels) throws IOException {
		final String M = "TimeSeriesFileReader(...)";
		this.filenames = filename;
		this.infile = new RandomAccessFile[filename.length];
		this.fc = new FileChannel[filename.length];

		logger.debug("{}: Creating TimeSeriesFileReader for {}", M, filename);
	}
	
	public TimeSeriesFileReader(String filename, int channels) throws IOException {
		final String M = "TimeSeriesFileReader(...)";
		this.filenames = new String[1];
		this.filenames[0] = filename;
		this.infile = new RandomAccessFile[filenames.length];
		this.fc = new FileChannel[filenames.length];

		logger.debug("{}: Creating TimeSeriesFileReader for {}", M, filename);
	}

	protected FileChannel getChannel(int inx) {
		return fc[inx];
	}
	
	protected FileChannel getChannel() {
		return fc[0];
	}
	
	/**
	 * Open file and FileChannel, read the header
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException {
		int i = 0;
		try {
			for (String file: filenames) {
				infile[i] = new RandomAccessFile(file, "r");
				setFileChannel(i, infile[i++].getChannel());
			}
		} catch (FileNotFoundException f) {
			File d = new File(".");
			logger.error("Cannot find " + filenames[i] + " in current dir: " + d.getCanonicalPath());
			System.err.println("Cannot find " + filenames[i] + " in current dir: " + d.getCanonicalPath());
			f.printStackTrace();
			throw f;
		}
		bb = ByteBuffer.allocate(1024);
		pos = readHeader();
	}

	public void close() {
		bb = null;
		try {
			for (int i = 0; i < filenames.length; i++) {
				if (getFileChannel(i).isOpen())
					getFileChannel(i).close();
				
				infile[i].close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int read(RandomAccessFile r, byte[] arr) throws IOException {
		return r.read(arr);
	}

	/**
	 * Read the index structures into memory
	 * 
	 * @throws IOException
	 */
	public abstract void buildIndex() throws IOException;
	
	public abstract long getStartTime() throws IOException;
	
	public double getSampleRate() {
		return header.getSamplingFrequency();
	}
	
	public abstract long readHeader() throws IOException;
	
	public TimeSeriesChannel getHeader() {
		return header;
	}
	
	/*
	public InputStream getStream() {
		return infileStream;
	}*/
	
	public RandomAccessFile getFile() {
		return infile[0];
	}
	
	public RandomAccessFile getFile(int inx) {
		return infile[inx];
	}
	
	public abstract String getCompressionScheme();
	
//	public long getNumIndexEntries() {
//		return header.getNumberOfIndexEntries();
//	}
	
	public long getNumSamples() {
		return header.getNumberOfSamples();
	}
	
	public long getEndTime() {
		return header.getRecordingEndTime();
	}
	
	public int getMaxSampleValue() {
		return header.getMaximumDataValue();
	}
	
	public static String fromCString(byte[] str) {
		int i = 0;
		for (; i < str.length; i++)
			if (str[i] == 0)
				break;
		
		return new String(str, 0, i);
	}
	
	public int getMinSampleValue() {
		return header.getMinimumDataValue();
	}
	
	public String getInstitution() {
		return (header.getInstitution());
	}
	
	public String getAcquisitionSystem() {
		return (header.getAcquisitionSystem());
	}
	
	public String getChannelName() {
		return header.getChannelName();
	}
	
	public String getChannelComments() {
		return (header.getChannelComments());
	}
	
	public String getSubjectId() {
		return (header.getSubjectId());
	}
	
	/**
	 * Microvolts per sample unit
	 * 
	 * @return
	 */
	public double getVoltageConversionFactor() {
		return header.getVoltageConversionFactor();
	}

	
	public abstract PageReader.PageList getPages(long startTime, long endTime, int resolution) throws IOException;
	

	public abstract  TimeSeriesPage[] readPages(long pageId, int numPages, boolean doDecode) throws IOException;

	public FilterSpec getFilterSpec() {
		return getFilterSpecFor(header);
	}

	public static FilterSpec getFilterSpecFor(TimeSeriesChannel header) {
		FilterSpec ret = new FilterSpec();
		
		ret.setBandpassLowCutoff(header.getLowFrequencyFilterSetting());
		ret.setBandpassHighCutoff(header.getHighFrequencyFilterSetting());
		ret.setBandstopLowCutoff(header.getNotchFilterFrequency());
		ret.setBandstopHighCutoff(header.getNotchFilterFrequency());
		ret.setNumPoles(FilterSpec.DEFAULT_FILTER_POLES);

		return ret;
	}

	public FileChannel getFileChannel(int inx) {
		return fc[inx];
	}

	public void setFileChannel(int inx, FileChannel fc) {
		this.fc[inx] = fc;
	}
	
	public FileChannel getFileChannel() {
		return getFileChannel(0);
	}
	
	public void setFileChannel(FileChannel fc) {
		this.fc[0] = fc;
	}

}
