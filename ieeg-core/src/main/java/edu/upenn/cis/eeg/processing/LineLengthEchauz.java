/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.parameters.LineLengthEchauzParameters;
import edu.upenn.cis.eeg.processing.MathLib.LambdaVecScalarInt;

public class LineLengthEchauz extends SignalAnnotator {
	LineLengthEchauzParameters parameters;
	double trendLength;
	double tumble;
	
	public static final String NAME = "LL_echauz";
	
	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public JsonTyped getParameters() {
		return parameters;
	}
	
	public static class LL implements LambdaVecScalarInt {
		public double compute(double[] values) {
			// Computes mean of difference between values in entire vector.
			return MathLib.mean(MathLib.abs(MathLib.diff(values)));
		}
	}
	
	LineLengthEchauz() {
		
	}
	
	public LineLengthEchauz(LineLengthEchauzParameters parametersJson) throws JsonParseException, IOException {
//		parameters = mapper.readValue(parametersJson, Parameters.class);
		this.parameters = parametersJson;
//		trendLength = parameters.getWindowSize() * 2;//getSamplingFrequency() * 60;
//		tumble = parameters.getStepSize() * 2; //.getSamplingFrequency() * 20;
		trendLength = parameters.getSamplingFrequency() * 10;
		tumble = parameters.getSamplingFrequency() * 5;
	}

	@Override
	public ISignalProcessingAlgorithm create(String name, JsonTyped parmJson) throws JsonParseException, IOException {
		return new LineLengthEchauz((LineLengthEchauzParameters)parmJson);
	}

	@Override
	public int[] processSegment(AnnotatedTimeSeries series, int startIndex,
			int endIndex, double startPos, double period) throws RuntimeException {
//		System.out.println("From " + startIndex + " to " + endIndex);
		int[] work = new int[endIndex - startIndex + 1];
		for (int i = 0; i < work.length; i++)
			work[i] = series.getSeries()[i + startIndex];
		
		FeaturesReturned f1 = rfeat(new LL(), work, 
				(int)parameters.getWindowSize(), 
				(int)parameters.getStepSize());
		
		double[] rf = new double[(int)parameters.getWindowSize() + f1.numberIgnored + 
		                         (f1.featureSeries.length-1) * (int)parameters.getStepSize()];
		
//		System.out.println("rf length = " + rf.length);
		double[] zoh1 = MathLib.zohinterp(f1.featureSeries, 
				0, f1.featureSeries.length - 1, (int)parameters.getStepSize());
//		System.out.println("zoh1 length = " + zoh1.length);
		int start = (int) (parameters.getWindowSize() - 1);
		for (int i = 0; i < start; i++)
			rf[i] = Double.NaN;
		for (int i = 0; i < zoh1.length; i++)
			rf[start + i] = zoh1[i]; 
		rf[rf.length - 1] = f1.featureSeries[f1.featureSeries.length - 1];
//		System.out.println("zoh1 pad length = " + start);
		
		FeaturesReturned f2 = rfeat(new LL(), work, (int)trendLength, 
				(int)tumble);
		
		double [] rf2 = new double[(int)trendLength + f2.numberIgnored + 
		                           (f2.featureSeries.length - 1) * (int)tumble];
		double[] zoh2 = MathLib.zohinterp(f2.featureSeries, 
				0, f2.featureSeries.length - 1, (int)tumble);
		
//		start = (int)trendLength - 1;
		start = (int) (parameters.getWindowSize() - 1);
		for (int i = 0; i < start; i++)
			rf2[i] = Double.NaN;
		for (int i = 0; i < zoh2.length; i++)
			rf2[start + i] = zoh2[i]; 
		
		rf2[rf2.length - 1] = f2.featureSeries[f2.featureSeries.length - 1];
		
//		System.out.println(rf.length + ", " + rf2.length);
		
		List<String> channels = new ArrayList<String>();
		channels.add(series.getChannelName());
		List<TraceInfo> ids = new ArrayList<TraceInfo>();
		ids.add(new TraceInfo(null, 0, 1, 1, 1, 1, 1, "", "", "", "", series.getChannelName(), 
				series.getRevId()));
		List<Annotation> ret = new ArrayList<Annotation>();
		for (int i = 0; i < rf.length; i++) {
			if (rf[i] - rf2[i] >= parameters.getThreshold()) {
				double startAnn = startPos + 
						i * period;//1.e6/ parameters.getSamplingFrequency();
				double endAnn = startAnn;
				
				while (++i < rf.length && rf[i] - rf2[i] >= parameters.getThreshold())
					endAnn = startPos + i * period;//1.e6/ parameters.getSamplingFrequency();
				
				Annotation a = new Annotation(LineLengthEchauz.NAME, "Seizure", 
						startAnn, endAnn, channels);
				
				a.setLayer("Linelength");
				
				a.getChannelInfo().add(new TraceInfo(null, series.getRevId()));
				
//				a.setRevId(series.getRevId());
				ret.add(a);
			}
		}
		
		series.getAnnotations().addAll(ret);
		return null;
	}
	
	@Override
	public boolean isStateful() {
		return false;
	}
	
	public boolean isGapSensitive() {
		return false;
	}

	public static class FeaturesReturned {
		double[] featureSeries;
		int numberIgnored;
	}
	
	public FeaturesReturned rfeat(LambdaVecScalarInt fn, int[] values, int windowLength, int step) {
		FeaturesReturned f = new FeaturesReturned();
		
		if (windowLength > values.length) {
			throw new RuntimeException("Cannot run feature detector when window length (" +
					windowLength + ") > values (" + values.length + ")");
//			return f;
		}
		
		// NOTE: ZI - this seems to be over-optimistic in cases where we have an exact multiple
		// of the tumble size
		// Number of sliding obs windows that fit within the length of signal
		int numSeg = (values.length - windowLength) / step + 1;

		// Number of points ignored from the beginning of signal
		f.numberIgnored = values.length - windowLength + step - numSeg * step;
		
		// To run once the 1st time to test if feature is uni/multivariate (single output per time sample) or multiunivariate (multiple outputs per time sample)
		f.featureSeries = new double[numSeg];
		for (int i = 0; i < numSeg; i++)
			f.featureSeries[i] = Double.NaN;
		for (int n = 0; n < numSeg; n++) {
			int start = f.numberIgnored + n * step;
			int end = f.numberIgnored + n * step + windowLength - 1;
			double[] overthis = new double[end - start + 1];
			for (int j = start; j <= end; j++)
				overthis[j - start] = values[j]; 
			f.featureSeries[n] = fn.compute(overthis);
		}
	    
		return f;
	}

	@Override
	public List<IDetection> annotate(String channel, String revId, double startPos, double period, int startIndex,
			int endIndex, ITimeSeries series) {
		int[] work = new int[endIndex - startIndex + 1];
		for (int i = 0; i < work.length; i++)
			work[i] = series.getSeries()[i + startIndex];
		
		FeaturesReturned f1 = rfeat(new LL(), work, 
				(int)parameters.getWindowSize(), 
				(int)parameters.getStepSize());
		
		double[] rf = new double[(int)parameters.getWindowSize() + f1.numberIgnored + 
		                         (f1.featureSeries.length-1) * (int)parameters.getStepSize()];
		
//		System.out.println("rf length = " + rf.length);
		double[] zoh1 = MathLib.zohinterp(f1.featureSeries, 
				0, f1.featureSeries.length - 1, (int)parameters.getStepSize());
//		System.out.println("zoh1 length = " + zoh1.length);
		int start = (int) (parameters.getWindowSize() - 1);
		for (int i = 0; i < start; i++)
			rf[i] = Double.NaN;
		for (int i = 0; i < zoh1.length; i++)
			rf[start + i] = zoh1[i]; 
		rf[rf.length - 1] = f1.featureSeries[f1.featureSeries.length - 1];
//		System.out.println("zoh1 pad length = " + start);
		
		FeaturesReturned f2 = rfeat(new LL(), work, (int)trendLength, 
				(int)tumble);
		
		double [] rf2 = new double[(int)trendLength + f2.numberIgnored + 
		                           (f2.featureSeries.length - 1) * (int)tumble];
		double[] zoh2 = MathLib.zohinterp(f2.featureSeries, 
				0, f2.featureSeries.length - 1, (int)tumble);
		
//		start = (int)trendLength - 1;
		start = (int) (parameters.getWindowSize() - 1);
		for (int i = 0; i < start; i++)
			rf2[i] = Double.NaN;
		for (int i = 0; i < zoh2.length; i++)
			rf2[start + i] = zoh2[i]; 
		
		rf2[rf2.length - 1] = f2.featureSeries[f2.featureSeries.length - 1];
		
//		System.out.println(rf.length + ", " + rf2.length);
		
		List<String> channels = new ArrayList<String>();
		channels.add(channel);
		List<TraceInfo> ids = new ArrayList<TraceInfo>();
		ids.add(new TraceInfo(null, 0, 1, 1, 1, 1, 1, "", "", "", "", channel, 
				revId));
		List<IDetection> ret = new ArrayList<IDetection>();
		for (int i = 0; i < rf.length; i++) {
			if (rf[i] - rf2[i] >= parameters.getThreshold()) {
				double startAnn = startPos + 
						i * period;//1.e6/ parameters.getSamplingFrequency();
				double endAnn = startAnn;
				
				while (++i < rf.length && rf[i] - rf2[i] >= parameters.getThreshold())
					endAnn = startPos + i * period;//1.e6/ parameters.getSamplingFrequency();
				
				Annotation a = new Annotation(LineLengthEchauz.NAME, "Seizure", 
						startAnn, endAnn, channels);
				
				a.setLayer("Linelength");
				
				a.getChannelInfo().add(new TraceInfo(null, revId));
				
//				a.setRevId(series.getRevId());
				ret.add(a);
			}
		}
		
		return ret;
	}

	/*
function [rf,ni] = rfeat(feat,y,L,d,varargin)
% [rf,ni] = rfeat(@feature, y, L, d, additionalFeatureParameters, ...)
%
% "Running feature" version 2. Generate time-series of univariate, multivariate, and "multiunivariate" features.
% Univariate = single signal in (y=vector), single feature stream out (rf=vector)
% Multivariate = multiple signals in (y=matrix), single feature stream out (rf=vector)
% Multiunivariate = multiple signals in (y=matrix), multiple feature stream out (rf=matrix),
%   where each rf output channel is simply the same univariate feature applied to each
%   corresponding input channel (e.g., with feature=@mean)
% Vector-valued features (rf=3D array) not supported. To do that, compose multiple rf streams.
%
% INPUTS
% @feature = Handle to a function implementing the feature, e.g., built-ins like @mean, user-defined (@myFeature),
%   or anonymous @(X)sum(X,2). Can also be passed as string '<featureName>' or object inline('<feature expression>').
% y = Input data to be processed; can be a vector or in general a rectangular matrix.
%   By default, the largest dimension of y is associated to "time samples" while the other to "signals."
%   E.g., [1:12] has 1 row-signal whereas reshape([1:12],4,3) has 3 column-signals.
%   In case of y = square matrix, orientation will resolve to signals-are-columns.
%   When input data was stored at less than double precision and the feature admits it, go native. For example,
%   rfeat('LineLength',int16(y),800,25) is MUCH faster than rfeat('LineLength',y,800,25).
% L = Observation window length
% d = Displacement between consecutive slides of the window = L - overlap
%   L,d > 1 cause a compression in the time axis: length(rf) < length(y).
% additionalFeatureParameters = Parameters other than L,d particular of a feature, passed as comma-separated list.
%
% OUTPUTS
% rf = Running feature series, with one column or row per feature time.
%    The running feature is "right aligned" so that the last value of a feature always
%    corresponds in time to the last value of y (e.g., the time of UEO).
%    The right-alignment is such that the command
%		[NaN(ni+L-1,1); zohinterp(rf(1:end-1),d); rf(end)]  (causal, zero-order held, y=column)
%       [NaN(1,ni+L-1) zohinterp(rf(1:end-1),d) rf(end)]    (causal, zero-order held, y=row)
%          or
%       [NaN(1,ni+L-1) interp1(ni+L:d:length(y),rf,ni+L:length(y),'*linear')] (non-causal version)
%    is exactly time-synchronized with and has the same length as the original data sequence y.
%    Orientation of rf will follow that of y (signals-are-columns vs. signals-are-rows).
%    If you have a case of rectangular input w/more columns than rows but still want it processed as
%    signals-are-columns, deal with it yourself applying pre- and/or post-transposes.
% ni = Number ignored: The right-aligned convention implies that some values of y may be ignored/dropped from the
%    beginning of the record (instead of from the end, or part-beginning-part-end).

% Resolve orientation & sizes
[r,c] = size(y);
if r>=c
    Ly = r;  % Column-signal(s) length (time goes down)
else
    Ly = c;  % Row-signal(s) length (time goes sideways)
end

numSeg = floor((Ly-L)/d + 1);   % Number of sliding obs windows that fit within the length of signal
ni = Ly-L+d - numSeg*d;   % Number of points ignored from the beginning of signal

n = 1;  % To run once the 1st time to test if feature is uni/multivariate (single output per time sample) or multiunivariate (multiple outputs per time sample)
if Ly==r  % For functions that take column-signals and return a row of features (e.g., @mean )
    rf(n,:) = feval(feat,y(ni+1+(n-1)*d:ni+(n-1)*d+L,:),varargin{:});
    rf = [rf; NaN(numSeg-1,size(rf,2))];
    for n=2:numSeg
        rf(n,:) = feval(feat,y(ni+1+(n-1)*d:ni+(n-1)*d+L,:),varargin{:});
    end
else  % For functions that take row-signals and return a column of features (e.g., @(X)mean(X,2) )
    n = 1;
    rf(:,n) = feval(feat,y(:,ni+1+(n-1)*d:ni+(n-1)*d+L),varargin{:});
    rf = [rf NaN(size(rf,1),numSeg-1)];
    for n=2:numSeg
        rf(:,n) = feval(feat,y(:,ni+1+(n-1)*d:ni+(n-1)*d+L),varargin{:});
    end
end

	 */
}
