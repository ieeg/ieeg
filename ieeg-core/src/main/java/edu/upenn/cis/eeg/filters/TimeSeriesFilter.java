/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.filters;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;

import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.processing.SignalTranscoder;

/**
 * Basic time series filter interface.  
 * See @link https://code.google.com/p/braintrust/wiki/DevPlatform#Filter_development
 * 
 * TODO: this should really subclass ...eeg.processing.SignalTranscoder
 * @see edu.upenn.cis.eeg.processing.SignalTranscoder
 * 
 * @author zives
 *
 */
public abstract class TimeSeriesFilter extends SignalTranscoder {
	public abstract void init(double sampleFreq, FilterSpec fs)
			 throws IllegalFilterException;
	public abstract String getFilterType();
	
	public abstract double[] apply(IntArrayWrapper data, int[] buffer);
	public abstract double[] apply(IntArrayWrapper data, int bufferStart, int dataStart, 
			int nSamples, int[] buffer);
	
	public static class Parameters implements JsonTyped {
		public static enum FilterType {NONE, DECIMATE, LOWPASS, HIGHPASS, BANDPASS, BANDSTOP};
		
		String name;
		double sampleFrequency;
		FilterType filterType;
		int numberOfPoles;
		double bandpassLowCutoffFrequency;
		double bandpassHighCutoffFrequency;
		double bandstopLowCutoffFrequency;
		double bandstopHighCutoffFrequency;
		
		public FilterSpec getFilterSpec() {
			int filterType2 = 0;
			filterType2 += (filterType == FilterType.NONE) ? FilterSpec.NO_FILTER : 0;
			filterType2 += (filterType == FilterType.BANDPASS) ? FilterSpec.BANDPASS_FILTER : 0;
			filterType2 += (filterType == FilterType.BANDSTOP) ? FilterSpec.BANDSTOP_FILTER : 0;
			filterType2 += (filterType == FilterType.DECIMATE) ? FilterSpec.DECIMATE_FILTER : 0;
			filterType2 += (filterType == FilterType.LOWPASS) ? FilterSpec.LOWPASS_FILTER : 0;
			filterType2 += (filterType == FilterType.HIGHPASS) ? FilterSpec.HIGHPASS_FILTER : 0;
			return new FilterSpec(name, filterType2, numberOfPoles, bandpassLowCutoffFrequency,
					bandpassHighCutoffFrequency, bandstopLowCutoffFrequency,
					bandstopHighCutoffFrequency);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public double getSampleFrequency() {
			return sampleFrequency;
		}

		public void setSampleFrequency(double sampleFrequency) {
			this.sampleFrequency = sampleFrequency;
		}

		public FilterType getFilterType() {
			return filterType;
		}

		public void setFilterType(FilterType filterType) {
			this.filterType = filterType;
		}

		public int getNumberOfPoles() {
			return numberOfPoles;
		}

		public void setNumberOfPoles(int numberOfPoles) {
			this.numberOfPoles = numberOfPoles;
		}

		public double getBandpassLowCutoffFrequency() {
			return bandpassLowCutoffFrequency;
		}

		public void setBandpassLowCutoffFrequency(double bandpassLowCutoffFrequency) {
			this.bandpassLowCutoffFrequency = bandpassLowCutoffFrequency;
		}

		public double getBandpassHighCutoffFrequency() {
			return bandpassHighCutoffFrequency;
		}

		public void setBandpassHighCutoffFrequency(double bandpassHighCutoffFrequency) {
			this.bandpassHighCutoffFrequency = bandpassHighCutoffFrequency;
		}

		public double getBandstopLowCutoffFrequency() {
			return bandstopLowCutoffFrequency;
		}

		public void setBandstopLowCutoffFrequency(double bandstopLowCutoffFrequency) {
			this.bandstopLowCutoffFrequency = bandstopLowCutoffFrequency;
		}

		public double getBandstopHighCutoffFrequency() {
			return bandstopHighCutoffFrequency;
		}

		public void setBandstopHighCutoffFrequency(double bandstopHighCutoffFrequency) {
			this.bandstopHighCutoffFrequency = bandstopHighCutoffFrequency;
		}
		
	}
	
	
	Parameters parameters;
	
	@Override
	public String getName() {
		return getFilterType();
	}
	
	@Override
	public JsonTyped getParameters() {
		return parameters;
	}
	
	public void setParameters(JsonTyped parametersJson)  
			throws JsonParseException, IOException {
		parameters = (Parameters)parametersJson;		
	}
	
	@Override
	public double getOutputFrequency() {
		return parameters.sampleFrequency;
	}


	@Override
	public boolean isGapSensitive() {
		return true;
	}

	int [] buffer;

	@Override
	public int[] processSegment(AnnotatedTimeSeries workingSeries, int startIndex,
			int endIndex, double start, double period) {
		if (buffer == null || buffer.length < (endIndex - startIndex + 1))
			buffer = new int[endIndex - startIndex + 1];
		
		IntArrayWrapper wrap = new IntArrayWrapper(1);
		wrap.addArray(workingSeries.getSeries());
		
		double[] dbl = apply(wrap, startIndex, endIndex, 0, buffer);
		
		int[] ret = new int[dbl.length];
		int i = 0;
		for (double d: dbl)
			ret[i++] = (int)d;
		
		return ret;
	}

}
