package edu.upenn.cis.eeg;


public interface ITimeSeriesFileWriter {
	
	public void setInstitution(String new_institution);
	
	public void setChannelName(String new_channel_name);
	
	public void setSamplingFrequency(double new_freq);
	
	public void setLowFrequencyFilterSetting(double new_filter);
	
	public void setHighFrequencyFilterSetting(double new_filter);
	
	public void setNotchFrequencyFilterSetting(double new_filter);
	
	public void setVoltageConversionFactor(double new_factor);
	
	public void setAcquisitionSystem(String new_system);
	
	public void setChannelComments(String comments);
	
	public void setPhysicalChannelNumber(int num);
	
	public void setGMToffset(float offset);
	
	public void writeData(int[] samps, long[] timestamps,
			int n_packets_to_process);

	public void writeData(int[] samps, long[] timestamps,
			int n_packets_to_process, boolean skipOne);

	/**
	 * Write the samples at the listed timestamp
	 * @param samps
	 * @param timestamp
	 * @param n_packets_to_process
	 * @param discontinuity
	 */
	public void writeDataBlock(int[] samps, long timestamp,
			int n_packets_to_process, boolean discontinuity);

	public void close();
}
