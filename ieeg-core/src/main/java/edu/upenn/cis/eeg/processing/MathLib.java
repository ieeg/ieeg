/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.processing;

import java.util.Collection;

public class MathLib {
	public static interface LambdaVecScalar<T> {
		public T compute(Collection<? extends T> values);
	}

	/**
	 * Lambda function between array (vector) of ints
	 * and an int value
	 *  
	 * @author zives
	 *
	 */
	public static interface LambdaVecScalarInt {
		public double compute(double[] values);
	}
	
	/**
	 * Return the mean value of a vector
	 * 
	 * @param values
	 * @return
	 */
	public static int mean(int[] values) {
		int meanVal = 0;
		for (int i = 0; i < values.length; i++) {
			meanVal += values[i]; 
		}
		
		if (values.length > 0)
			return meanVal / values.length;
		else
			return -1;
	}
	
	public static double mean(double[] values) {
		double meanVal = 0;
		for (int i = 0; i < values.length; i++) {
			meanVal += values[i]; 
		}
		
		if (values.length > 0)
			return meanVal / values.length;
		else
			return -1;
	}

	/**
	 * Given an n-element vector, return an (n-1) element vector
	 * with the difference in values between successive elements
	 * 
	 * @param values
	 * @return
	 */
	public static int[] diff(int[] values) {
		int[] ret = new int[values.length - 1];
		
		for (int i = 1; i < values.length; i++) {
			ret[i - 1] = values[i] - values[i-1];
		}
		
		return ret;
	}
	
	public static double[] diff(double[] values) {
		double[] ret = new double[values.length - 1];
		
		for (int i = 1; i < values.length; i++) {
			ret[i - 1] = values[i] - values[i-1];
		}
		
		return ret;
	}

	/**
	 * Absolute value of vector
	 * 
	 * @param values
	 * @return
	 */
	public static int[] abs(int[] values) {
		int[] ret = new int[values.length];
		
		for (int i = 0; i < values.length; i++) {
			ret[i] = (values[i] >= 0) ? values[i] : -values[i];
		}
		
		return ret;
	}

	public static double[] abs(double[] values) {
		double[] ret = new double[values.length];
		
		for (int i = 0; i < values.length; i++) {
			ret[i] = (values[i] >= 0) ? values[i] : -values[i];
		}
		
		return ret;
	}

	/**
	 * Expands the vector y with zero-order hold samples by an upsampling factor uf.
	 * The resulting vector is uf times longer.
	 * @param vector
	 * @param upsamplingFactor
	 * @return
	 */
	public static int[] zohinterp(int[] vector, int upsamplingFactor) {
		int[] ret = new int[vector.length * upsamplingFactor];
		
		for (int i = 0; i < vector.length * upsamplingFactor; i++)
			ret[i] = vector[i / upsamplingFactor];
		
		return ret;
	}

	public static int[] zohinterp(int[] vector, int start, int end, int upsamplingFactor) {
		int[] ret = new int[(end - start) * upsamplingFactor];
		
		for (int i = start; i < end * upsamplingFactor; i++)
			ret[i] = vector[start + (i - start) / upsamplingFactor];
		
		return ret;
	}
	
	public static double[] zohinterp(double[] vector, int start, int end, int upsamplingFactor) {
		double[] ret = new double[(end - start) * upsamplingFactor];
		
		for (int i = start; i < end * upsamplingFactor; i++)
			ret[i] = vector[start + (i - start) / upsamplingFactor];
		
		return ret;
	}
	
	/*
function yi = zohinterp(y,uf)
% ZOHINTERP(y,uf)
%							J. Echauz 9/00, Mod R. Esteller 02-09-01, Remod JE 3/11 
% Expands the vector y with zero-order hold samples by an upsampling factor uf.
% The resulting vector is uf times longer.
% Example:
% [1 3 2 -1] with uf = 3 becomes [1 1 1 3 3 3 2 2 2 -1 -1 -1]
% Input y can also be a matrix of signals (rows or columns).
% Note: If y is so short that it's a single scalar, will output zoh as row.

% Force same orientation as input & allow matrix too
[r,c] = size(y);
if r>c
    Ly = r;  % Column-signal(s) length (time goes down)
    yi = reshape(repmat(y',uf,1),c,Ly*uf)';
else
    Ly = c;  % Row-signal(s) length (time goes sideways)
    yi = reshape(repmat(y,uf,1),r,Ly*uf);
end

	 */
	
	/**
	 * Expands the vector y with zero-order hold samples by an upsampling factor uf.
	 * The resulting vector is uf times longer.
	 * @param vector
	 * @param upsamplingFactor
	 * @return
	 */
	public int[][] zohinterp(int[][] matrix, int upsamplingFactor) {
		if (matrix.length > matrix[0].length) {
			int[][] ret = new int[matrix.length * upsamplingFactor][matrix[0].length];
			
			for (int i = 0; i < matrix.length * upsamplingFactor; i++)
				for (int j = 0; j < matrix[0].length; j++)
					ret[i][j] = matrix[i / upsamplingFactor][j];
			
			return ret;
		} else {
			int[][] ret = new int[matrix.length][matrix[0].length * upsamplingFactor];
			
			for (int i = 0; i < matrix.length; i++)
				for (int j = 0; j < matrix[0].length * upsamplingFactor; j++)
					ret[i][j] = matrix[i][j / upsamplingFactor];
			
			return ret;
		}
	}

}
