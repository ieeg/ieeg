/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer.ICursor;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.server.PageReader;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

public abstract class TimeSeriesObjectReader implements PageReader<TimeSeriesPage> {
	protected IStoredObjectReference[] handles;
	protected IInputStream[] sources;
	public TimeSeriesChannel header;
	int channel;
	protected ByteBuffer bb;
	
	protected long pos;
	
	final static Logger logger = LoggerFactory
	.getLogger(TimeSeriesObjectReader.class);
	final static Logger timeLogger = LoggerFactory.getLogger("time." + TimeSeriesObjectReader.class.getName());
	
	ByteBuffer buf = null;
	
	public TimeSeriesObjectReader(IStoredObjectReference[] handles, int channels) throws IOException {
		final String M = "TimeSeriesFileReader(...)";
		this.handles = handles;
		this.sources = new IInputStream[handles.length];

		logger.debug("{}: Creating TimeSeriesObjectReader for {}", M, handles);
	}
	
	public TimeSeriesObjectReader(IStoredObjectReference handle, int channels) throws IOException {
		final String M = "TimeSeriesObjectReader(...)";
		this.handles = new IStoredObjectReference[1];
		this.handles[0] = handle;
		this.sources = new IInputStream[handles.length];

		logger.debug("{}: Creating TimeSeriesObjectReader for {}", M, handle);
	}

	protected IInputStream getStream(int inx) {
		return sources[inx];
	}
	
	protected IInputStream getStream() {
		return sources[0];
	}
	
	/**
	 * Open file and FileChannel, read the header
	 * 
	 * @throws IOException
	 */
	public void init() throws IOException {
		int i = 0;
		try {
			for (IStoredObjectReference handle: handles) {
				sources[i] = StorageFactory.getBestServerFor(handle).openForInput(handle);
			}
		} catch (IOException f) {
			logger.error("Cannot find " + handles[i]);
			System.err.println("Cannot find " + handles[i]);
			f.printStackTrace();
			throw f;
		}
		bb = ByteBuffer.allocate(1024);
		pos = readHeader();
	}

	public void close() {
		bb = null;
		try {
			for (int i = 0; i < sources.length; i++) {
				getStream(i).close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ICursor read(IInputStream r, byte[] arr) throws IOException {
		ByteBuffer buf = ByteBuffer.wrap(arr);
		return r.getBytes(buf, arr.length);
	}

	public ICursor read(IInputStream r, ByteBuffer buf, long length) throws IOException {
		return r.getBytes(buf, length);
	}

	public ICursor read(IInputStream r, ICursor pos, ByteBuffer buf, long length) throws IOException {
		return r.getBytes(pos, buf, length);
	}

	/**
	 * Read the index structures into memory
	 * 
	 * @throws IOException
	 */
	public abstract void buildIndex() throws IOException;
	
	public abstract long getStartTime() throws IOException;
	
	public double getSampleRate() {
		return header.getSamplingFrequency();
	}
	
	public abstract long readHeader() throws IOException;
	
	public TimeSeriesChannel getHeader() {
		return header;
	}
	
	public abstract String getCompressionScheme();
	
//	public long getNumIndexEntries() {
//		return header.getNumberOfIndexEntries();
//	}
	
	public long getNumSamples() {
		return header.getNumberOfSamples();
	}
	
	public long getEndTime() {
		return header.getRecordingEndTime();
	}
	
	public int getMaxSampleValue() {
		return header.getMaximumDataValue();
	}
	
	public static String fromCString(byte[] str) {
		int i = 0;
		for (; i < str.length; i++)
			if (str[i] == 0)
				break;
		
		return new String(str, 0, i);
	}
	
	public int getMinSampleValue() {
		return header.getMinimumDataValue();
	}
	
	public String getInstitution() {
		return (header.getInstitution());
	}
	
	public String getAcquisitionSystem() {
		return (header.getAcquisitionSystem());
	}
	
	public String getChannelName() {
		return header.getChannelName();
	}
	
	public String getChannelComments() {
		return (header.getChannelComments());
	}
	
	public String getSubjectId() {
		return (header.getSubjectId());
	}
	
	/**
	 * Microvolts per sample unit
	 * 
	 * @return
	 */
	public double getVoltageConversionFactor() {
		return header.getVoltageConversionFactor();
	}

	
	public abstract PageReader.PageList getPages(long startTime, long endTime, int resolution) throws IOException;
	

	public abstract  TimeSeriesPage[] readPages(long pageId, int numPages, boolean doDecode) throws IOException;

	public FilterSpec getFilterSpec() {
		return getFilterSpecFor(header);
	}

	public static FilterSpec getFilterSpecFor(TimeSeriesChannel header) {
		FilterSpec ret = new FilterSpec();
		
		ret.setBandpassLowCutoff(header.getLowFrequencyFilterSetting());
		ret.setBandpassHighCutoff(header.getHighFrequencyFilterSetting());
		ret.setBandstopLowCutoff(header.getNotchFilterFrequency());
		ret.setBandstopHighCutoff(header.getNotchFilterFrequency());
		ret.setNumPoles(FilterSpec.DEFAULT_FILTER_POLES);

		return ret;
	}
}
