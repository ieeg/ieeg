/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Copyright 2013 Christian d'Heureuse, Inventec Informatik AG, Zurich, Switzerland
// www.source-code.biz, www.inventec.ch/chdh
//
// This module is multi-licensed and may be used under the terms
// of any of the following licenses:
//
//  EPL, Eclipse Public License, V1.0 or later, http://www.eclipse.org/legal
//  LGPL, GNU Lesser General Public License, V2.1 or later, http://www.gnu.org/licenses/lgpl.html
//
// Please contact the author if you need another license.
// This module is provided "as is", without warranties of any kind.
package edu.upenn.cis.eeg.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import biz.source_code.dsp.filter.FilterCharacteristicsType;
import biz.source_code.dsp.filter.FilterPassType;
import biz.source_code.dsp.filter.IirFilter;
import biz.source_code.dsp.filter.IirFilterCoefficients;
import biz.source_code.dsp.filter.IirFilterDesignFisher;

import com.fasterxml.jackson.core.JsonParseException;
import com.google.common.collect.Lists;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm;

/**
 * Butterworth filter based on the Fisher code in the biz.source_code.dsp IIR filter libraries.
 * 
 * @author zives
 *
 */
public class TimeSeriesFilterButter2 extends TimeSeriesFilter {
	public static String NAME = "Butterworth-Fisher";
	/**
	 * We may need to chain multiple filters, so we have a list 
	 */
	private List<IirFilterCoefficients> coeffList = new ArrayList<>();
	
	public TimeSeriesFilterButter2() {
		
	}
	
	
	public TimeSeriesFilterButter2(double sampleFreq, FilterSpec fs) throws IllegalFilterException {
		init(sampleFreq, fs);
	}
	
	public void init(double sampleFreq, FilterSpec fs) throws IllegalFilterException {
		final List<IirFilterCoefficients> coefficients = IirFilterCoefficientsFactory.getCoefficients(
				sampleFreq, 
				fs, 
				FilterCharacteristicsType.butterworth);
		coeffList.addAll(coefficients);
	}

	@Override
	public double[] apply(IntArrayWrapper data, int[] buffer) {
		return apply(data, 0, 0, data.length(), buffer);
	}

	@Override
	public double[] apply(IntArrayWrapper data, int bufferStart, int dataStart,
			int nSamples, int[] buffer) {
		final double[] filt_data = new double[nSamples];
		
		//Filters don't provide way to clear buffers, so create new
		for (IirFilterCoefficients coeff : coeffList) {
			IirFilter filt = new IirFilter(coeff);
			for (int i = dataStart; i < dataStart + nSamples; i++) {
				if (coeff == coeffList.get(0))
					filt_data[i - dataStart] = filt.step(data.get(i));
				else
					filt_data[i - dataStart] = filt.step(filt_data[i - dataStart]);
			}
			filt = new IirFilter(coeff);
			for (int i = dataStart + nSamples - 1; i >= dataStart; i--) {
				filt_data[i - dataStart] = filt.step(filt_data[i - dataStart]);
			}
		}
		for (int i = 0; i < filt_data.length; i++) {
			buffer[bufferStart + i] = (int)filt_data[i];
		}
		
		return filt_data;
	}


	@Override
	public String getFilterType() {
		return NAME;
	}

	@Override
	public ISignalProcessingAlgorithm create(String name, JsonTyped parmJson)
			throws JsonParseException, IOException {
		TimeSeriesFilterButter2 me = new TimeSeriesFilterButter2();
		me.setParameters((Parameters)parmJson);
		try {
			me.init(me.parameters.getSampleFrequency(), me.parameters.getFilterSpec());
		} catch (IllegalFilterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JsonParseException(e.getMessage(), null);
		}
		return me;
	}

	@Override
	public List<IDetection> annotate(String channel, String revId, double start, double period, int startIndex,
			int endIndex, ITimeSeries series) {		// TODO Auto-generated method stub
		return Lists.newArrayList();
	}
}
