/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.eeg.processing;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegment;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.IDetection;

/**
 * Abstract class for transcoding algorithms for signal data
 * Details at @link https://code.google.com/p/braintrust/wiki/UploadPipeline#Time_Series_Workflow_Steps
 * 
 * 
 * @author zives
 *
 */
public abstract class SignalTranscoder extends SignalProcessingAlgorithm {

	@Override
	public boolean isAnnotator() {
		return false;
	}

	@Override
	public boolean isTranscoder() {
		return true;
	}
	
	public abstract double getOutputFrequency();

	@Override
	public List<AnnotatedTimeSeries> process(AnnotatedTimeSeries series) {
		List<IDetection> newAnn = new ArrayList<IDetection>();
		newAnn.addAll(series.getAnnotations());
		
		AnnotatedTimeSeries source = 
				new AnnotatedTimeSeries(series.getChannelName(), series.getRevId(), 
						series.getTimeSeries(), series.getStart(), series.getPeriod(),
						newAnn);
		
		List<AnnotatedTimeSeries> segments = new ArrayList<AnnotatedTimeSeries>();

		int[] outputData;
		
		if (isGapSensitive()) {
			int start = 0;
			do {
				while (series.isInGap(start) && series.getSeriesLength() > start)
					start++;
				if (start == series.getSeriesLength())
					break;
				
				int end = start + 1;
				while (series.getSeriesLength() > end && !series.isInGap(end))
					end++;
				
				try {
					outputData = processSegment(source, start, end-1, series.getStart() +
							start * series.getPeriod(), series.getPeriod());
					
					ITimeSeries newSeries = new UnscaledTimeSeriesSegment(series.getStart() + start * series.getPeriod(), 
							outputData);
					double period = ((double)outputData.length / end - start) * series.getPeriod();
					segments.add(
							new AnnotatedTimeSeries(series.getChannelName(), series.getRevId(), 
									newSeries, newSeries.getStartTime(), 
									period, series.getAnnotations()));
				} catch (RuntimeException re) {
					re.printStackTrace();
				}
				start = end;
			} while (start < series.getSeriesLength());
		} else {
			outputData = processSegment(source, 0, series.getSeriesLength() - 1, 
					series.getStart(), series.getPeriod());
			ITimeSeries newSeries = new UnscaledTimeSeriesSegment(series.getStart(), 
					outputData);
			double period = ((double)outputData.length / series.getSeriesLength()) * series.getPeriod();
			segments.add(
					new AnnotatedTimeSeries(series.getChannelName(), series.getRevId(), 
							newSeries, newSeries.getStartTime(), 
							period, series.getAnnotations()));
		}
		
		return segments;
	}
}
