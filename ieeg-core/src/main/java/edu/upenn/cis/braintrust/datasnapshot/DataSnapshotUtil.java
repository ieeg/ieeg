/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import org.hibernate.Hibernate;

import com.google.common.cache.Cache;

import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.ProjectAceEntity;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;

public class DataSnapshotUtil {

	public static Set<ImageForTrace> imagedContacts2ImageForTraces(
			final Set<ImagedContact> imagedContacts) {
		final Set<ImageForTrace> imageForTraces = newHashSet();
		for (final ImagedContact imagedTrace : imagedContacts) {
			final ImageEntity image = imagedTrace.getImage();
			final ImageForTrace imageForTrace = new ImageForTrace();
			imageForTraces.add(imageForTrace);
			imageForTrace.setImagePubId(image.getPubId());
			imageForTrace.setImageFileKey(image.getFileKey());
			imageForTrace.setImageTypeEnum(image.getType());
			imageForTrace.setPixelCoordX(imagedTrace.getPxCoordinates().getX());
			imageForTrace.setPixelCoordY(imagedTrace.getPxCoordinates().getY());
		}
		return imageForTraces;
	}

	public static DataSnapshotEntity getFromCache(
			String datasetPubId,
			IDataSnapshotDAO dataSnapshotDAO,
			Cache<String, DataSnapshotEntity> datasetCache) {
		DataSnapshotEntity dataset =
				datasetCache.getIfPresent(datasetPubId);
		if (dataset == null) {
			dataset = dataSnapshotDAO.findByNaturalId(datasetPubId);
			if (dataset == null) {
				throw new DataSnapshotNotFoundException(datasetPubId);
			}
			for (ProjectAceEntity projectAce : dataset.getExtAcl()
					.getProjectAces()) {
				Hibernate.initialize(projectAce.getProject().getAdmins());
				Hibernate.initialize(projectAce.getProject().getTeam());
			}
			datasetCache.put(datasetPubId, dataset);
		}
		return dataset;
	}
}
