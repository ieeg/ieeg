/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.proxy.HibernateProxy;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;

@NamedQueries({
		@NamedQuery(
				name = DataSnapshotEntity.FIND_TS_ANNOTATION,
				query = "select tsa "
						+ "from TsAnnotation tsa "
						+ "where tsa.id = :tsAnnRevId "
						+ "and tsa.parent = :ds"),
		@NamedQuery(
				name = DataSnapshotEntity.COUNT_ALL_TS_ANNS,
				query = "select count(tsa.id) from TsAnnotation tsa"),
		@NamedQuery(
				name = DataSnapshotEntity.COUNT_ALL_TS_DS_ANNS,
				query = "select count(tsa.id) from TsAnnotation tsa where tsa.parent = :ds"),
		@NamedQuery(
				name = DataSnapshotEntity.FIND_BY_LABEL,
				query = "from DataSnapshot ds where ds.label = :dsLabel"),
		@NamedQuery(
				name = DataSnapshotEntity.COUNT_TS_ANNS_ON_TS,
				query = "select count(*) from TsAnnotation tsa where :ts in elements(tsa.annotated) and tsa.parent = :ds"),
		@NamedQuery(
				name = DataSnapshotEntity.COUNT_ACTIVE_DATASNAPSHOTS,
				query = "select count(ds.id) from DataSnapshot ds where frozen = 1"),
		@NamedQuery(
				name = DataSnapshotEntity.COUNT_ALL_DATASNAPSHOTS,
				query = "select count(ds.id) from DataSnapshot ds"),
		@NamedQuery(
					name = DataSnapshotEntity.IS_IN_A_PROJECT,
					query = "select 1 "
							+ "from Project p "
							+ "join p.snapshots ds "
							+ "where ds = :ds")
})
@Entity(name = "DataSnapshot")
@Table(name = DataSnapshotEntity.TABLE)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class DataSnapshotEntity
		implements IHasPubId, IHasLongId, IHasExtAcl, IEntity {

	public static final String TABLE = "data_snapshot";

	public static final String UPPER_CASE_TABLE = "DATA_SNAPSHOT";

	public static final String ID_COLUMN = TABLE + "_id";

	public static final String FIND_TS_ANNOTATION = "DataSnapshot-findTsAnn";
	public static final String COUNT_ALL_TS_ANNS = "DataSnapshot-countAllAnns";
	public static final String COUNT_ALL_TS_DS_ANNS = "DataSnapshot-countAllDsAnns";
	public static final String COUNT_TS_ANNS_ON_TS = "DataSnapshot-countTsAnnsOnTs";
	public static final String COUNT_ACTIVE_DATASNAPSHOTS = "DataSnapshot-countActiveSnapshots";
	public static final String COUNT_ALL_DATASNAPSHOTS = "DataSnapshot-countAllSnapshots";
	public static final String FIND_BY_LABEL = "DataSnapshot-findByLabel";
	public static final String IS_IN_A_PROJECT = "DataSnapshot-isInAProject";

	private Long id;
	private Integer version;
	String pubId = newUuid();
	private Date createTime = new Date();
	private String label;
	private ExtAclEntity extAcl;
	private Set<DataSnapshotClob> dsClob = newHashSet();
	private Set<DataSnapshotJson> dsJson = newHashSet();
	private Set<SnapshotApplicationClob> snapshotApplicationClob = newHashSet();
	private Set<AnalyzedDataSnapshot> analysisOutputs = newHashSet();
	private Set<SnapshotUsage> usagesAsPrimary = newHashSet();
	private Set<SnapshotUsage> usagesAsDerived = newHashSet();
	private Boolean frozen = Boolean.FALSE;
	
	@ManyToMany(mappedBy="snapshots")
	private Set<EegMontageEntity> montages = new HashSet<>();
	
	public DataSnapshotEntity() {}

	protected DataSnapshotEntity(
			ExtAclEntity extAcl) {
		this.extAcl = checkNotNull(extAcl);
	}

	protected DataSnapshotEntity(
			String label,
			ExtAclEntity extAcl,
			@Nullable String clob) {
		this(extAcl);
		this.label = checkNotNull(label);
		setClob(clob);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DataSnapshotEntity)) {
			return false;
		}
		DataSnapshotEntity other = (DataSnapshotEntity) obj;
		if (!equal(pubId, other.getPubId())) {
			return false;
		}
		return true;
	}

	/**
	 * Many-to-many with additional columns on join table, intermediate entity
	 * class. To create a link, instantiate an {@code AnalyzedDataSnapshot} with
	 * the right constructor. To remove a link, use
	 * {@code getAnalyzedDataSnapshots().remove(...)}.
	 * 
	 * @return the join-table entity
	 */
	@OneToMany(
			mappedBy = "analysis",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	@Nonnull
	public Set<AnalyzedDataSnapshot> getAnalysisOutputs() {
		return analysisOutputs;
	}

	@Transient
	public String getClob() {
		if (dsClob.isEmpty()) {
			return null;
		}
		return getOnlyElement(dsClob).getValue();
	}

	/**
	 * @return the timestamp
	 */
	@Column(name = "create_time")
	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@Transient
	public abstract DataSnapshotType getDataSnapshotType();

	/**
	 * Arbitrary character data associated with this data snapshot.
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than buildtime
	 * bytecode instr. because that loads all lazy props when you hit any of
	 * them.
	 * 
	 * @return the arbitrary character data associated with this data snapshot
	 */
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,//FetchType.LAZY,
			orphanRemoval = true)
	private Set<DataSnapshotClob> getDsClob() {
		return dsClob;
	}

	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,//LAZY,
			orphanRemoval = true)
	private Set<DataSnapshotJson> getDsJson() {
		return dsJson;
	}
	
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,//LAZY,
			orphanRemoval = true)
	Set<SnapshotApplicationClob> getSnapshotApplicationClob() {
		return snapshotApplicationClob;
	}

	@Override
	@OneToOne(
			fetch = FetchType.EAGER,//LAZY,
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getExtAcl() {
		return extAcl;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = DataSnapshotEntity.ID_COLUMN)
	public Long getId() {
		return id;
	}

	/**
	 * @return the images
	 */
	@Transient
	public abstract Set<ImageEntity> getImages();

	@Override
//	@Column(unique = true)
	@NotNull
	@Size(min = 1, max = 255)
	public String getLabel() {
		return label;
	}

//	@OneToMany(
//			mappedBy = "parent",
//			cascade = CascadeType.ALL,
//			orphanRemoval = true)
//	public Set<MontageEntity> getMontages() {
//		return montages;
//	}

	/**
	 * @return the id
	 */
	@Override
	@NaturalId
	@NotNull
	@Size(min = 36, max = 36)
	public String getPubId() {
		return pubId;
	}

	/**
	 * The number of seizures the admin entered for the study.
	 * 
	 * Quick hack.
	 * 
	 * @return the number of seizures the admin entered for the study
	 */
	@Transient
	public Integer getSzCount() {
		return null;
	}

	/**
	 * Will be modifiable if and only if modifying the collection will have an
	 * effect on the db.
	 * 
	 * @return the time series in this snapshot
	 */
	@Transient
	public abstract Set<TimeSeriesEntity> getTimeSeries();

	@OneToMany(mappedBy = "derivedSnapshot")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<SnapshotUsage> getUsagesAsDerived() {
		return usagesAsDerived;
	}

	@OneToMany(mappedBy = "snapshot")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<SnapshotUsage> getUsagesAsPrimary() {
		return usagesAsPrimary;
	}
	
//    @ManyToMany(mappedBy="montages")
//    @JoinTable(
//            name = TABLE + "_" + EegMontageEntity.TABLE,
//            joinColumns = @JoinColumn(name = ID_COLUMN),
//            inverseJoinColumns = @JoinColumn(name = EegMontageEntity.ID_COLUMN))
//    public Set<EegMontageEntity> getMontages() {
//        return montages;
//    }

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pubId == null) ? 0 : pubId.hashCode());
		return result;
	}

	@NotNull
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean isFrozen() {
		return frozen;
	}

	@SuppressWarnings("unused")
	private void setAnalysisOutputs(
			final Set<AnalyzedDataSnapshot> analysisOutputs) {
		this.analysisOutputs = analysisOutputs;
	}

	public void setClob(@Nullable String clob) {
		if (clob == null) {
			dsClob.clear();
		} else {
			if (dsClob.isEmpty()) {
				dsClob.add(new DataSnapshotClob(clob, this));
			} else {
				getOnlyElement(dsClob).setValue(clob);
			}
		}
	}

	/**
	 * @param createTime the timestamp to set
	 */
	@SuppressWarnings("unused")
	private void setCreateTime(final Date createTime) {
		this.createTime = createTime;
	}

	@SuppressWarnings("unused")
	private void setDsClob(@Nullable Set<DataSnapshotClob> dsClob) {
		this.dsClob = dsClob;
	}

	@SuppressWarnings("unused")
	private void setDsJson(Set<DataSnapshotJson> dsJson) {
		this.dsJson = dsJson;
	}
	
	@SuppressWarnings("unused")
	private void setSnapshotApplicationClob(Set<SnapshotApplicationClob> snapshotApplicationClob) {
		this.snapshotApplicationClob = snapshotApplicationClob;
	}

	public void setExtAcl(final ExtAclEntity extAcl) {
		this.extAcl = extAcl;
	}

	public void setFrozen(Boolean frozen) {
		this.frozen = frozen;
	}

	@VisibleForTesting
	public void setId(final Long id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@SuppressWarnings("unused")
	private void setMontages(Set<EegMontageEntity> montages) {
		this.montages = montages;
	}

	/**
	 * @param id the id to set
	 */
	@VisibleForTesting
	public void setPubId(final String pubId) {
		this.pubId = pubId;
	}

	@SuppressWarnings("unused")
	private void setUsagesAsDerived(Set<SnapshotUsage> usagesAsDerived) {
		this.usagesAsDerived = usagesAsDerived;
	}

	@SuppressWarnings("unused")
	private void setUsagesAsPrimary(Set<SnapshotUsage> usagesAsPrimary) {
		this.usagesAsPrimary = usagesAsPrimary;
	}

	@VisibleForTesting
	public void setVersion(final Integer version) {
		this.version = version;
	}
	
	
	@Override
	public String toString() {
		return "DataSnapshotEntity [id="
				+ id
				+ ", label=" + label
				+ "]";
	}

	   /**
	    * Utility method that tries to properly initialize the Hibernate CGLIB
	    * proxy.
	    *
	    * @param <T>
	    * @param maybeProxy -- the possible Hibernate generated proxy
	    * @param baseClass -- the resulting class to be cast to.
	    * @return the object of a class <T>
	    * @throws ClassCastException
	    */
	   public static <T> T deproxy(Object maybeProxy, Class<T> baseClass) throws ClassCastException {
	      if (maybeProxy instanceof HibernateProxy) {
	         return baseClass.cast(((HibernateProxy) maybeProxy).getHibernateLazyInitializer().getImplementation());
	      }
	      return baseClass.cast(maybeProxy);
	   }
}
