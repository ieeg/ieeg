package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.List;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * An m-to-n mapping from source objects to target objects,
 * with an optional transform descriptor and metadata.
 * 
 * @author Zack Ives
 *
 * @param <T>
 */
@GwtCompatible(serializable = true)
public class FileMapping<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	List<IStoredObjectReference> sourceFiles;
	List<T> targetObjects;
	List<PresentableMetadata> targetDescriptors;
	
	String jsonTransformDescriptor;
	String jsonTransformInverseDescriptor;
	
	PresentableMetadata groupDescriptor;
	
	
}
