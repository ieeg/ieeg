/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * Intended to represent the state of a patient's case during a single
 * admission. {@link GwtPatient} contains general information which is constant
 * across admissions.
 * 
 * @author Sam Donnelly
 */
public class GwtHospitalAdmission implements IHasLongId, IHasVersion,
		Serializable {

	private static final long serialVersionUID = 1L;

	public static final Function<GwtHospitalAdmission, Optional<Integer>> getAgeAtAdmission = new Function<GwtHospitalAdmission, Optional<Integer>>() {

		@Override
		public Optional<Integer> apply(GwtHospitalAdmission input) {
			return input.getAgeAtAdmission();
		}
	};

	public static final String UNKNOWN_ADMISSION_AGE = "Unknown";

	private Long id;
	private Integer version;
	private GwtPatient parent;
	private Optional<Integer> ageAtAdmission;
	private Boolean isDeleted = Boolean.FALSE;
	private Set<GwtEegStudy> studies = newHashSet();

	public GwtHospitalAdmission() {}

	public GwtHospitalAdmission(
			@Nullable Integer ageAtAdmission,
			@Nullable Long id,
			@Nullable Integer version) {
		this.ageAtAdmission = Optional.fromNullable(ageAtAdmission);
		this.id = id;
		this.version = version;
	}

	/**
	 * For convenience, handle both sides of the {@code GwtPatient<->Study}
	 * relationship.
	 * 
	 * @param study to be added
	 */
	public void addStudy(final GwtEegStudy study) {
		checkNotNull(study);
		checkArgument(study.getParent() == null, "study already has a parent");
		checkArgument(!studies.contains(study),
				"study is already contained in this admission");
		study.setParent(this);
		studies.add(study);
	}

	/**
	 * @return the ageAtAdmission
	 */
	public Optional<Integer> getAgeAtAdmission() {
		return ageAtAdmission;
	}

	public Long getId() {
		return id;
	}

	public GwtPatient getParent() {
		return parent;
	}

	/**
	 * @return the studies
	 */
	public Set<GwtEegStudy> getStudies() {
		return studies;
	}

	public Integer getVersion() {
		return version;
	}

	public void removeStudy(final GwtEegStudy study) {
		checkNotNull(study);
		checkArgument(studies.contains(study),
				"this patient does not contain study");
		studies.remove(study);
		study.setParent(null);
	}

	/**
	 * @param ageAtAdmission the ageAtAdmission to set
	 */
	public void setAgeAtAdmission(@Nullable final Integer ageAtAdmission) {
		this.ageAtAdmission = Optional.fromNullable(ageAtAdmission);
	}

	public void setId(@Nullable final Long id) {
		this.id = id;
	}

	public void setParent(@Nullable final GwtPatient parent) {
		this.parent = parent;
	}

	/**
	 * @param studies the studies to set
	 */
	@SuppressWarnings("unused")
	private void setStudies(final Set<GwtEegStudy> studies) {
		this.studies = studies;
	}

	public void setVersion(@Nullable final Integer version) {
		this.version = version;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @return the isDeleted
	 */
	public Boolean isDeleted() {
		return isDeleted;
	}

	public boolean isRemovable() {
		boolean removable = true;
		for (GwtEegStudy study : studies) {
			if (!study.isModifiable()) {
				removable = false;
				break;
			}
		}
		return removable;
	}

}
