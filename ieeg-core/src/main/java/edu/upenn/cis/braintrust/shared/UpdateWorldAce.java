/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtWorldAce;

public class UpdateWorldAce implements IEditAclAction<ExtWorldAce> {

	private static final long serialVersionUID = 1L;
	private ExtWorldAce ace;
	private Optional<ExtPermission> newPerms;

	// For GWT
	@SuppressWarnings("unused")
	private UpdateWorldAce() {}

	public UpdateWorldAce(ExtWorldAce ace, @Nullable ExtPermission newPerms) {
		this.ace = checkNotNull(ace);
		this.newPerms = Optional.fromNullable(newPerms);
	}

	@Override
	public ExtWorldAce getAce() {
		return ace;
	}

	public Optional<ExtPermission> getNewPerms() {
		return newPerms;
	}

	public void setNewPerms(@Nullable ExtPermission newPerms) {
		this.newPerms = Optional.fromNullable(newPerms);
	}

}
