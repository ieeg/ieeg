/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.importer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ImageDAOHibernate;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;

/**
 * Import image info from spreadsheets and add in coord info for contacts.
 * Contacts must already exist in database.
 * 
 * @author John Frommeyer
 * 
 */
public class ImportContactImages {
	private static class CommandLineArgs {

		@Parameter(names = "-path", required = true,
				description = "Path to the spreadsheet")
		public String path;

		@Parameter(names = "-patientLabel", required = true,
				description = "The patient label")
		public String patientLabel;

		@Parameter(
				names = "-studyDir",
				required = true,
				description = "The study directory as entered into the data-entry app")
		public String studyDir;

		@Parameter(
				names = "-sheetNo",
				description = "The sheet containing the coordinate and image information.")
		public Integer sheetNo = 0;

	}

	private final static Logger logger = LoggerFactory
			.getLogger(ImportContactImages.class);

	public static void main(String[] args) {
		final CommandLineArgs clArgs = new CommandLineArgs();
		new JCommander(clArgs, args);
		final String spreadsheetPath = clArgs.path;
		final String patientId = clArgs.patientLabel;
		final String studyDir = clArgs.studyDir;
		final int sheetNo = clArgs.sheetNo.intValue();

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(spreadsheetPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		try {

			Session session = HibernateUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			IPatientDAO patientDAO = new PatientDAOHibernate(session);
			EegStudy study = ImporterUtil.findStudy(patientDAO, patientId,
					studyDir);
			IImageDAO imageDAO = new ImageDAOHibernate(session);
			ContactImageImporter converter = new ContactImageImporter(
					study, imageDAO, sheetNo, fis);
			converter.convertAndImport();
			session.getTransaction().commit();
		} catch (Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (Throwable rbEx) {
				logger.error("Could not rollback transaction after exception!",
						rbEx);
			}
			throw new IllegalStateException(t);
		}

	}

}
