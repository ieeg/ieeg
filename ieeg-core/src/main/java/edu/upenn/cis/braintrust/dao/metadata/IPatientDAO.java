/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.metadata;

import java.util.List;

import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IPatientDAO extends IDAO<Patient, Long> {
	/**
	 * Returns the {@code Patient} with the given label.
	 * <p>
	 * Eagerly fetches all objects in the patient tree.
	 * 
	 * @param patientLabel the id we're querying for
	 * 
	 * @return the {@code Patient} with the given label
	 */
	public Patient findByLabelEager(String patientLabel);
	
	/**
	 * Returns the {@code Patient} with the given id.
	 * <p>
	 * Eagerly fetches all objects in the patient tree which are needed for the GWT DTOs.
	 * 
	 * @param id the id we're querying for
	 * 
	 * @return the {@code Patient} with the given id
	 */
	public Patient findByIdEager(Long id);

	/**
	 * Returns a list of the etiologies found in the database.
	 * 
	 * @return a list of the etiologies found in the database
	 */
	public List<String> retrieveEtiologies();
}
