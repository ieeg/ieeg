/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = EventLogEntity.TABLE)
public class EventLogEntity {
	public static final String TABLE = "event_log";
	public static final String UPPER_CASE_TABLE = "EVENT_LOG";
	public static final String FIND_BY_ID = "EventLog-findByID";

	private Long eventId;
	private UserEntity notifiedUser;
	private UserEntity sourceUser;

	private Integer version;
	private Integer visibility;

	private String event;
	private Timestamp time;

	private String messageIdentifier;
	private int messageType;

	/**
	 * For JPA.
	 */
	EventLogEntity() {}

	public EventLogEntity(
			UserEntity notifiedUser,
			UserEntity sourceUser,
			int visibility,
			final String event) {
		this.notifiedUser = notifiedUser;
		this.sourceUser = sourceUser;
		this.visibility = visibility;
		this.event = event;

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	public EventLogEntity(
			UserEntity notifiedUser,
			UserEntity sourceUser,
			int visibility,
			final String event,
			final Timestamp time) {
		this.notifiedUser = notifiedUser;
		this.sourceUser = sourceUser;
		this.visibility = visibility;
		this.event = event;

		this.time = time;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "event_id")
	public Long getEventId() {
		return eventId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_el_source")
	@JoinColumn(name = "source_user")
	@NotNull
	public UserEntity getSourceUser() {
		return sourceUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_el_notified")
	@JoinColumn(name = "notified_user")
	@NotNull
	public UserEntity getNotifiedUser() {
		return notifiedUser;
	}

	@Lob
	@NotNull
	public String getEvent() {
		return event;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setEventId(final Long id) {
		this.eventId = id;
	}

	public void setVersion(final Integer version) {
		this.version = version;
	}

	@NotNull
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Integer getVisibility() {
		return visibility;
	}

	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

	public void setNotifiedUser(UserEntity notifiedUser) {
		this.notifiedUser = notifiedUser;
	}

	public void setSourceUser(UserEntity sourceUser) {
		this.sourceUser = sourceUser;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getMessageIdentifier() {
		return messageIdentifier;
	}

	public void setMessageIdentifier(String messageIdentifier) {
		this.messageIdentifier = messageIdentifier;
	}

	@Column(nullable = false)
	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

}
