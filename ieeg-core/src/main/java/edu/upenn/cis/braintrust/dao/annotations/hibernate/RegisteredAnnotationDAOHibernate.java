/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.annotations.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.annotations.IRegisteredAnnotationDAO;
import edu.upenn.cis.braintrust.model.RegisteredAnnotation;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class RegisteredAnnotationDAOHibernate extends
		GenericHibernateDAO<RegisteredAnnotation, Long>
		implements IRegisteredAnnotationDAO {
	public RegisteredAnnotationDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public List<RegisteredAnnotation> getRegisteredAnnotations(String viewer) {
		final Session s = getSession();
		Query query = s.getNamedQuery(RegisteredAnnotation.FIND_BY_TYPE)
				.setParameter("viewer", viewer);
		@SuppressWarnings("unchecked")
		List<RegisteredAnnotation> l = query.list();
		return l;
	}

}
