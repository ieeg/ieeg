/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public enum Pathology {
	TUMOR("Tumor"), STROKE("Stroke"), BRAIN_HEMORRHAGE("Brain Hemorrhage"), FOCAL_CORTICAL_DYSPLASIA(
			"Focal Cortical Dysplasia"), MESIAL_TEMPORAL_SCLEROSIS(
			"Mesial Temporal Sclerosis"), ANOXIA("Anoxia"), AVM("AVM"), ENCEPHALITIS(
			"Encephalitis"), MENINGITIS("Meningitis"), DEMENTIA("Dementia"), HEMISPHERIC_ATROPHY(
			"Hemispheric Atrophy"), HAMARTOMA("Hamartoma"), CORTICAL_DYSPLASIA(
			"Cortical Dysplasia"), CEREBRAL_PALSY("Cerebral Palsy"), DOWN_SYNDROME(
			"Down Syndrome"), CRYPTOGENIC("Cryptogenic"), TOXIC_OR_METABOLIC(
			"Toxic or Metabolic");

	private final String displayString;

	/**
	 * Maps a {@code Etiology}'s toString representation to the {@code Etiology}
	 * itself.
	 */
	public static final SortedMap<String, Pathology> fromString;
	static {
		final SortedMap<String, Pathology> temp = newTreeMap();
		for (final Pathology etiologyy : values()) {
			temp.put(etiologyy.toString(), etiologyy);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	private Pathology(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
