/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class TraceSearch implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer minInterannMicrosecs;
	private Integer maxInterannMicrosecs;
	private String annotationType;
	private Integer minAnnotationCount;
	private Integer maxAnnotationCount;

	/**
	 * @return the annotationType
	 */
	public String getAnnotationType() {
		return annotationType;
	}

	/**
	 * @return the maxCount
	 */
	public Integer getMaxAnnotationCount() {
		return maxAnnotationCount;
	}

	/**
	 * @return the maxInterictalSeconds
	 */
	public Integer getMaxInterannMicrosecs() {
		return maxInterannMicrosecs;
	}

	/**
	 * @return the count
	 */
	public Integer getMinAnnotationCount() {
		return minAnnotationCount;
	}

	/**
	 * @return the interictalRegionSeconds
	 */
	public Integer getMinInterannMicrosecs() {
		return minInterannMicrosecs;
	}

	public boolean isSomethingNotNull() {
		if (minInterannMicrosecs != null
				|| maxInterannMicrosecs != null
				|| annotationType != null
				|| minAnnotationCount != null
				|| maxAnnotationCount != null) {
			return true;
		}
		return false;
	}

	/**
	 * @param annotationType the annotationType to set
	 */
	public void setAnnotationType(@Nullable final String annotationType) {
		this.annotationType = annotationType;
	}

	/**
	 * @param maxCount the maxCount to set
	 * 
	 * @throws IllegalArgumentException if max count is less than 0
	 */
	public void setMaxAnnotationCount(@Nullable final Integer maxCount) {
		if (maxCount != null) {
			checkArgument(maxCount >= 0);
		}
		this.maxAnnotationCount = maxCount;
	}

	/**
	 * @param maxInterictalSeconds the maxInterictalSeconds to set
	 */
	public void setMaxInterannMicrosecs(
			@Nullable final Integer maxInterictalSeconds) {
		this.maxInterannMicrosecs = maxInterictalSeconds;
	}

	/**
	 * @param count the count to set
	 * 
	 * @throws IllegalArgumentException if count is less than 0
	 */
	public void setMinAnnotationCount(@Nullable final Integer count) {
		if (count != null) {
			checkArgument(count >= 0);
		}
		this.minAnnotationCount = count;
	}

	/**
	 * @param interictalRegionSeconds the interictalRegionSeconds to set
	 */
	public void setMinInterannMicrosecs(
			@Nullable final Integer interictalRegionSeconds) {
		this.minInterannMicrosecs = interictalRegionSeconds;
	}

	@Override
	public String toString() {
		return "TraceSearch [minInterannMicrosecs=" + minInterannMicrosecs
				+ ", maxInterannMicrosecs=" + maxInterannMicrosecs
				+ ", annotationType=" + annotationType
				+ ", minAnnotationCount=" + minAnnotationCount
				+ ", maxAnnotationCount=" + maxAnnotationCount + "]";
	}
}
