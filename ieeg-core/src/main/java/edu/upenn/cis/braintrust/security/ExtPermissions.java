/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.User;

/**
 * 
 * @author John Frommeyer
 */
public final class ExtPermissions {

	private User user;
	private String targetName;
	private String targetId;
	private Optional<ExtWorldAce> worldAce;
	private Optional<ExtUserAce> userAce;
	private Set<ExtProjectAce> projectAces = newHashSet();

	public ExtPermissions(
			User user,
			String targetName,
			String targetId,
			@Nullable ExtUserAce userAce,
			@Nullable ExtWorldAce worldAce) {
		this.user = checkNotNull(user);
		this.targetName = checkNotNull(targetName);
		this.targetId = checkNotNull(targetId);
		this.userAce = Optional.fromNullable(userAce);
		this.worldAce = Optional.fromNullable(worldAce);
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @return the targetName
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * @return the targetId
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * @return the worldAce
	 */
	public Optional<ExtWorldAce> getWorldAce() {
		return worldAce;
	}

	/**
	 * @return the userAce
	 */
	public Optional<ExtUserAce> getUserAce() {
		return userAce;
	}

	/**
	 * @return the projectAces
	 */
	public Set<ExtProjectAce> getProjectAces() {
		return projectAces;
	}

}
