/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Semaphore;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.User;

/**
 * An filter which adds {@code dataUsage.getDataSize()} and
 * {@code dataUsage.getReponseTimeNanos()} to {@code dataUsage} and saves it.
 * 
 * @author John Frommeyer
 */
public class IeegFilter implements Filter {

	private Logger logger = LoggerFactory.getLogger(IeegFilter.class);

	public static final String TRANSFER_ALL_USERS_SEMPAHORE_KEY =
			IeegFilter.class
					.getPackage()
					.toString()
					.toLowerCase()
					.concat(".transfer-all-users-semaphore");

	public static final String TRANSFER_PER_USER_SEMPAHORE_KEY =
			IeegFilter.class
					.getPackage()
					.toString()
					.toLowerCase()
					.concat(".transfer-per-user-semaphore");
	public static final String UPLOAD_IN_PROGRESS_SEMPAHORE_KEY =
			IeegFilter.class
					.getPackage()
					.toString()
					.toLowerCase()
					.concat(".upload-in-progress-semaphore");
	public static final String DATA_USAGE_ATTR_KEY = DataUsageEntity.class
			.getName();
	public static final String USER_ATTR_KEY = User.class.getName();

	private final IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();

	@Override
	public void destroy() {
		// noop
	}

	@Override
	public void doFilter(
			ServletRequest request,
			ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		String m = "doFilter(...)";
		final Date startTime = new Date();
		final long startTimeNanos = System.nanoTime();
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		try {
			final CountingServletResponseWrapper responseWrapper = new CountingServletResponseWrapper(
					(HttpServletResponse)
					response);
			filterChain.doFilter(httpServletRequest, responseWrapper);
			final DataUsageEntity dataUsage =
					(DataUsageEntity) httpServletRequest
							.getAttribute(DATA_USAGE_ATTR_KEY);
			if (dataUsage != null) {
				final User user = (User) httpServletRequest
						.getAttribute(USER_ATTR_KEY);
				if (dataUsage.getRequestType().equals(
						DataRequestType.WS_TIME_SERIES_DETAILS)) {
					dataUsage.setDataSizeBytes(0L);
				} else {
					dataUsage.setDataSizeBytes(responseWrapper
							.getByteCount());
				}
				dataUsage.setTimeOfRequest(startTime);
				dataUsage.setResponseTimeNanos(System.nanoTime()
						- startTimeNanos);
				dataUsage.setRemoteHost(httpServletRequest.getRemoteHost());
				dataUsage.setRemoteAddr(httpServletRequest.getRemoteAddr());
				String userAgent = httpServletRequest
						.getHeader("User-Agent");
				if (userAgent == null) {
					userAgent = "UNKNOWN";
				}
				dataUsage.setUserAgentHeader(userAgent);
				dataUsage.setRequestUrl(httpServletRequest.getRequestURL()
						.toString());
				dataUsage.setQueryString(httpServletRequest
						.getQueryString());
				dsServer.createDataUsage(user, dataUsage);
			}
		} finally {
			Semaphore allUsersSemaphore =
					(Semaphore) httpServletRequest
							.getAttribute(TRANSFER_ALL_USERS_SEMPAHORE_KEY);
			if (allUsersSemaphore != null) {
				logger.debug("yes release all-users semaphore");
				allUsersSemaphore.release();
			} else {
				logger.debug("not release all-users semaphore");
			}
			Semaphore perUserSemaphore =
					(Semaphore) httpServletRequest
							.getAttribute(TRANSFER_PER_USER_SEMPAHORE_KEY);
			if (perUserSemaphore != null) {
				logger.debug("yes release per-user semaphore");
				perUserSemaphore.release();
			} else {
				logger.debug("not release per-user semaphore");
			}
			Semaphore uploadInProgressSemaphore =
					(Semaphore) httpServletRequest
							.getAttribute(UPLOAD_IN_PROGRESS_SEMPAHORE_KEY);
			if (uploadInProgressSemaphore != null) {
				logger.debug("yes release upload-in-progress semaphore");
				uploadInProgressSemaphore.release();
			} else {
				logger.debug("not release upload-in-progress semaphore");
			}
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// noop
	}

	public static void setUserAndDataUsage(
			HttpServletRequest httpServletRequest,
			User user,
			DataUsageEntity dataUsage) {
		httpServletRequest.setAttribute(USER_ATTR_KEY, user);
		httpServletRequest.setAttribute(DATA_USAGE_ATTR_KEY, dataUsage);
	}

	public static void setTransferPerUserSemaphore(
			HttpServletRequest httpServletRequest,
			Semaphore semaphore) {
		httpServletRequest.setAttribute(
				TRANSFER_PER_USER_SEMPAHORE_KEY,
				semaphore);
	}

	public static void setTransferAllUsersSemaphore(
			HttpServletRequest httpServletRequest,
			Semaphore semaphore) {
		httpServletRequest.setAttribute(
				TRANSFER_ALL_USERS_SEMPAHORE_KEY,
				semaphore);
	}

	public static void seUploadInProgressSemaphore(
			HttpServletRequest httpServletRequest,
			Semaphore inProgressSemaphore) {
		httpServletRequest.setAttribute(
				UPLOAD_IN_PROGRESS_SEMPAHORE_KEY,
				inProgressSemaphore);
	}
}
