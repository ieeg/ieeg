/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

/**
 * A Strain of a certain species
 * 
 * @author John Frommeyer
 */
public class GwtStrain implements IManagedEnum, Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer version;
	private String label;
	private Long speciesId;
	private String speciesLabel;

	@SuppressWarnings("unused")
	private GwtStrain() {}

	public GwtStrain(String label,
			@Nullable Long id,
			@Nullable Integer version,
			@Nullable String speciesLabel,
			@Nullable Long speciesId) {
		this.id = id;
		this.version = version;
		this.label = checkNotNull(label);
		this.speciesLabel = speciesLabel;
		this.speciesId = speciesId;
	}

	public Long getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public Integer getVersion() {
		return version;
	}

	public Long getSpeciesId() {
		return speciesId;
	}
	
	public String getSpeciesLabel() {
		return speciesLabel;
	}
	
	public String getDisplayLabel() {
		return speciesLabel + "/" + label;
	}
}
