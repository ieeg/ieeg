/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

public enum IlaeRating {
	CLASS_1("Class 1"),
	CLASS_1A("Class 1A"),
	CLASS_2("Class 2"),
	CLASS_3("Class 3"),
	CLASS_4("Class 4"),
	CLASS_5("Class 5"),
	CLASS_6("Class 6");

	private final String displayString;

	private IlaeRating(final String displayString) {
		this.displayString = displayString;
	}

	/**
	 * Maps a postopratingscale's toString representation to the type
	 * itself.
	 */
	public static final SortedMap<String, IlaeRating> FROM_STRING;
	static {
		final SortedMap<String, IlaeRating> temp = newTreeMap();
		for (final IlaeRating type : values()) {
			temp.put(type.toString(), type);
		}
		FROM_STRING = Collections.unmodifiableSortedMap(temp);
	}

	@Override
	public String toString() {
		return displayString;
	}
}
