/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import net.sf.ehcache.CacheManager;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

import edu.upenn.cis.braintrust.drupal.DrupalUserService;
import edu.upenn.cis.braintrust.drupal.persistence.DrupalHibernateUtil;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.thirdparty.cache.EhcacheWrapper;

@ThreadSafe
public class UserServiceFactory {

	private static class DrupalUserServiceSupplier
			implements Supplier<IUserService> {

		@Override
		public IUserService get() {
			return new DrupalUserService(
					HibernateUtil.getSessionFactory(DrupalHibernateUtil.DRUPAL_SESS_FAC_KEY),
					new EhcacheWrapper<UserId, User>(
							"DrupalUserService.idToUser",
							CacheManager.getCacheManager("ieeg")),
					new EhcacheWrapper<String, User>(
							"DrupalUserService.nameToUser",
							CacheManager.getCacheManager("ieeg")));
		}
	}

	private static Supplier<IUserService> userServiceSupplier =
			Suppliers.memoize(new DrupalUserServiceSupplier());

	public static IUserService getUserService() {
		return userServiceSupplier.get();
	}

	@VisibleForTesting
	public static void setUserService(final @Nullable IUserService userService) {
		userServiceSupplier = new Supplier<IUserService>() {

			@Override
			public IUserService get() {
				return userService;
			}
		};
	}

	private UserServiceFactory() {}
}
