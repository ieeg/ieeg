/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

/**
 * Animal.
 * 
 * @author Sam Donnelly
 */
@Immutable
@GwtCompatible(serializable = true)
@JsonIgnoreProperties(ignoreUnknown=true)
public final class AnimalMetadata implements SerializableMetadata {

	private static final long serialVersionUID = 1L;

	private String label;
	private String species;
	private String strain;

	private SerializableMetadata parent = null;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("label", VALUE_TYPE.STRING);
		valueMap.put("species", VALUE_TYPE.STRING);
		valueMap.put("strain", VALUE_TYPE.STRING);
	}

	/** For GWT. */
	@SuppressWarnings("unused")
	private AnimalMetadata() {}

	public AnimalMetadata(
			String label,
			String species,
			String strain) {
		this.label = checkNotNull(label);
		this.species = checkNotNull(species);
		this.strain = checkNotNull(strain);
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public String getId() {
		return getLabel();
	}

	@Override
	public void setId(String id) {}

	public String getSpecies() {
		return species;
	}
	
	public void setSpecies(String species) {
		this.species = species;
	}

	public String getStrain() {
		return strain;
	}
	
	public void setStrain(String strain) {
		this.strain = strain;
	}

	@Override
	@JsonIgnore
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@Override
	@JsonIgnore
	public String getStringValue(String key) {
		if (key.equals("label"))
			return this.getLabel();
		if (key.equals("species"))
			return this.getSpecies();
		if (key.equals("strain"))
			return this.getStrain();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	@JsonIgnore
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonIgnore
	public SerializableMetadata getParent() {
		return parent;
	}

//	@Override
//	public void setParent(SerializableMetadata p) {
//		parent = p;
//	}

	@Override
	@JsonIgnore
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Metadata.name();
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p == null || p instanceof SerializableMetadata)
			parent = (SerializableMetadata) p;
		else
			throw new RuntimeException(
					"Attempted to set non-serializable parent");
	}
	
	
}
