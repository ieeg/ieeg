/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.testhelper;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * Compare values of model entities.
 * <p>
 * The methods usually don't compare all fields, just enough to convince us the
 * persisting is working.
 * 
 * @author Sam Donnelly
 */
public class ModelAssert {

	public static void assertEqualsPatients(final Patient expectedPatient,
			final Patient actualPatient) {
		Assert.assertEquals(expectedPatient.getAdmissions().size(),
				actualPatient.getAdmissions().size());
		for (final HospitalAdmission expectedCase : expectedPatient
				.getAdmissions()) {
			final HospitalAdmission actualCase = find(actualPatient
					.getAdmissions(), compose(equalTo(expectedCase.getId()),
					IHasLongId.getId));
			assertEqualsHospitalAdmissions(expectedCase, actualCase);
		}
		Assert.assertEquals(expectedPatient.getLabel(), actualPatient
				.getLabel());
		Assert.assertEquals(expectedPatient.getEthnicity(), actualPatient
				.getEthnicity());
		Assert.assertEquals(expectedPatient.getGender(), actualPatient
				.getGender());
		Assert.assertEquals(expectedPatient.getEtiology(), actualPatient
				.getEtiology());
		Assert.assertEquals(expectedPatient.getAgeOfOnset(), actualPatient
				.getAgeOfOnset());
		assertEquals(expectedPatient.getDevelopmentalDelay(), actualPatient
				.getDevelopmentalDelay());
		assertEquals(expectedPatient.getDevelopmentalDisorders(), actualPatient
				.getDevelopmentalDisorders());
		assertEquals(expectedPatient.getTraumaticBrainInjury(), actualPatient
				.getTraumaticBrainInjury());
		assertEquals(expectedPatient.getFamilyHistory(), actualPatient
				.getFamilyHistory());
		Assert.assertEquals(expectedPatient.getHandedness(), actualPatient
				.getHandedness());
		Assert.assertEquals(expectedPatient.getSzTypes().size(), actualPatient
				.getSzTypes().size());
		Assert.assertEquals(expectedPatient.getAdmissions().size(),
				actualPatient.getAdmissions().size());
		for (final HospitalAdmission expectedAdmission : expectedPatient
				.getAdmissions()) {
			final HospitalAdmission actualAdmission = find(actualPatient
					.getAdmissions(), compose(
					equalTo(expectedAdmission.getId()), IHasLongId.getId));
			assertEqualsAdmissions(expectedAdmission, actualAdmission);
		}

	}

	public static void assertEqualsAdmissions(
			final HospitalAdmission expectedAdmission,
			final HospitalAdmission actualAdmission) {
		assertEquals(expectedAdmission.getAgeAtAdmission(), actualAdmission
				.getAgeAtAdmission());
		Assert.assertEquals(expectedAdmission.getStudies().size(),
				actualAdmission.getStudies().size());
		for (final EegStudy expectedStudy : expectedAdmission.getStudies()) {
			final EegStudy actualStudy = find(actualAdmission.getStudies(),
					compose(equalTo(expectedStudy.getId()), IHasLongId.getId));
			assertEqualsStudies(expectedStudy, actualStudy);
		}
	}

	public static void assertEqualsStudies(final EegStudy expectedStudy,
			final EegStudy actualStudy) {
		assertEquals(
				expectedStudy.getRecording().getRefElectrodeDescription(),
				actualStudy.getRecording().getRefElectrodeDescription());
		assertEquals(expectedStudy.getRecording().getContactGroups().size(), actualStudy
				.getRecording().getContactGroups().size());
	}

	/**
	 * Compares everything except id's.
	 * 
	 * @param expectedCase expected epilepsy case
	 * @param actualCase actual epilepsy case
	 */
	public static void assertEqualsHospitalAdmissions(
			final HospitalAdmission expectedCase,
			final HospitalAdmission actualCase) {
		assertEquals(expectedCase.getAgeAtAdmission(), actualCase
				.getAgeAtAdmission());
	}

	private ModelAssert() {
		throw new AssertionError("Can't instantiate a ModelTestAssert");
	}
}
