/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.validate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;

/**
 * Reports on, and possibly fixes, consistency problems in a {@code Patient}.
 * 
 * @author John Frommeyer
 * 
 */
final class PatientConsistencyChecker {

	private final static Logger logger = LoggerFactory
			.getLogger(PatientConsistencyChecker.class);

	public void checkPatient(final Patient patient) {
		for (final HospitalAdmission admission : patient.getAdmissions()) {
			checkAdmission(admission);
		}

	}

	private void checkAdmission(final HospitalAdmission admission) {
		for (final EegStudy study : admission.getStudies()) {
			checkStudy(study);
		}
	}

	private void checkStudy(final EegStudy study) {
		final String mefPathPrefix = study.getDir() + "/" + study.getMefDir();
		for (final ContactGroup electrode : study.getRecording().getContactGroups()) {
			checkElectrode(electrode, mefPathPrefix);
		}
	}

	private void checkElectrode(final ContactGroup electrode,
			final String mefPathPrefix) {
		for (final Contact contact : electrode.getContacts()) {
			checkContact(contact, mefPathPrefix);
		}
	}

	private void checkContact(final Contact contact, final String mefPathPrefix) {
		final TimeSeriesEntity trace = contact.getTrace();
		checkTrace(trace, mefPathPrefix);
	}

	private void checkTrace(final TimeSeriesEntity trace,
			final String mefPathPrefix) {
		final String M = "checkTrace(...)";
		final String expectedFileKey = mefPathPrefix + "/" + trace.getLabel()
				+ ".mef";
		final String actualFileKey = trace.getFileKey();
		if (!actualFileKey.equals(expectedFileKey)) {
			trace.setFileKey(expectedFileKey);
			logger.info("{}: Replacing fileKey: {} with fileKey: {}.",
					new Object[] { M, actualFileKey, expectedFileKey });
		}
	}
}
