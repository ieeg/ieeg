/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
		@NamedQuery(
				name = "Image-findByFileKey",
				query = "from Image i "
						+ "where i.fileKey=:imageFileKey"),
		@NamedQuery(
				name = "Image-findByStudyDirAndImageType",
				query = "select i "
						+ "from EegStudy s "
						+ "join s.recording r "
						+ "join r.images i "
						+ "where r.dir=:studyDir "
						+ "and i.type=:imageType"),
		@NamedQuery(
				name = "Image-findByStudyDir",
				query = "select i "
						+ "from EegStudy s "
						+ "join s.recording r "
						+ "join r.images i "
						+ "where r.dir=:studyDir") })
@Entity(name = "Image")
@Table(name = ImageEntity.TABLE,
		uniqueConstraints = { @UniqueConstraint(
				columnNames = { ImageEntity.FILE_KEY_COLUMN }) })
public class ImageEntity implements IHasPubId, IHasLongId, IEntity {

	public static final String TABLE = "image";
	public static final String UPPER_CASE_TABLE = "IMAGE";

	public static final String ID_COLUMN = TABLE + "_id";
	public static final String FILE_KEY_COLUMN = "file_key";

	private Long id;
	private Integer version;
	private String pubId = newUuid();
	private Date createTime = new Date();

	private ImageType type;

	// start at Xyz_IEED_data with no leading slash.
	private String fileKey;

	private Set<ImagedContact> imagedContacts = newHashSet();

	public ImageEntity() {}

	public ImageEntity(
			final String fileKey,
			final ImageType type) {
		this.fileKey = checkNotNull(fileKey);
		this.type = checkNotNull(type);
	}

	/**
	 * How to find the file. Could be an amazon s3 key for a file URL (without
	 * the "file://" part.)
	 * 
	 * @return the fileUri
	 */
	@Column(name = ImageEntity.FILE_KEY_COLUMN, unique = true)
	@NotNull
	public String getFileKey() {
		return fileKey;
	}

	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	/**
	 * Many-to-many with additional columns on join table, intermediate entity
	 * class. To create a link, instantiate an {@code ImagedContact} with the
	 * right constructor. To remove a link, use
	 * {@code getImagecContacts().remove(...)}.
	 * 
	 * @return the join-table entity
	 */
	@OneToMany(mappedBy = "image", cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<ImagedContact> getImagedContacts() {
		return imagedContacts;
	}

	/**
	 * @return the id
	 */
	@Override
	@NaturalId
	@Column(name = "pub_id", length = 36, nullable = false)
	@NotNull
	public String getPubId() {
		return pubId;
	}

	/**
	 * @return the timestamp
	 */
	@Column(name = "create_time", nullable = false)
	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @return the type
	 */
	@NotNull
	public ImageType getType() {
		return type;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	public void setFileKey(final String fileKey) {
		this.fileKey = fileKey;
	}

	@VisibleForTesting
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @param imagedContacts the imagedContacts to set
	 */
	@SuppressWarnings("unused")
	private void setImagedContacts(final Set<ImagedContact> imagedContacts) {
		this.imagedContacts = imagedContacts;
	}

	/**
	 * @param pubId the id to set
	 */
	@VisibleForTesting
	public void setPubId(final String pubId) {
		this.pubId = pubId;
	}

	/**
	 * @param createTime the timestamp to set
	 */
	@SuppressWarnings("unused")
	private void setCreateTime(final Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final ImageType type) {
		this.type = type;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

	public static final EntityConstructor<ImageEntity, String, Void> CONSTRUCTOR =
		new EntityConstructor<ImageEntity,String,Void>(ImageEntity.class,
				new IPersistentObjectManager.IPersistentKey<ImageEntity,String>() {

			@Override
			public String getKey(ImageEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(ImageEntity o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<ImageEntity,String,Void>() {

			@Override
			public ImageEntity create(String label, Void optParent) {
				// TODO: no acl?
				// null image type
				return new ImageEntity(label, null);
			}
		}
				);

	@Override
	public EntityConstructor<ImageEntity, String, Void> tableConstructor() {
		return CONSTRUCTOR;
	}

}
