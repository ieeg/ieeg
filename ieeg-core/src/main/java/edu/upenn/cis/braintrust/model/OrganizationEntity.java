/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.google.common.base.Objects;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@Entity(name = OrganizationEntity.ENTITY_NAME)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(name = OrganizationEntity.ID_COLUMN))
})
@NamedQueries({
	@NamedQuery(name = OrganizationEntity.ORGANIZATION_BY_NAME,
			query = "from Organization o "
					+ "where o.name = :name ")})
public class OrganizationEntity
		extends LongIdAndVersion
		implements IHasExtAcl, IEntity {

	public static final String ID_COLUMN = "organization_id";
	public static final String ENTITY_NAME = "Organization";
	
	public static final String ORGANIZATION_BY_NAME = "OrganizationByName";

	private String name;
	private String code;
	private ExtAclEntity acl;

	public OrganizationEntity() {}

	public OrganizationEntity(
			String name,
			String code,
			ExtAclEntity acl) {
		this.name = checkNotNull(name);
		this.code = checkNotNull(code);
		this.acl = checkNotNull(acl);
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof OrganizationEntity)) {
			return false;
		}
		OrganizationEntity other = (OrganizationEntity) obj;
		if (!Objects.equal(this.code, other.getCode())) {
			return false;
		}
		return true;
	}

	@NaturalId
	@Column(unique = true)
	@Size(min = 1, max = 10)
	@Nonnull
	public String getCode() {
		return code;
	}

	@Override
	@OneToOne(
			fetch = FetchType.EAGER,
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getExtAcl() {
		return acl;
	}

	@Transient
	@Override
	public String getLabel() {
		return name;
	}

	@Column(unique = true)
	@Size(min = 1, max = 255)
	@Nonnull
	public String getName() {
		return name;
	}

	@Transient
	@Override
	public String getPubId() {
		if (getId() == null) {
			return null;
		}
		return getId().toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	private void setCode(String code) {
		this.code = code;
	}

	public void setExtAcl(ExtAclEntity acl) {
		this.acl = acl;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Organization [name=" + name + ", code=" + code
				+ ", getVersion()=" + getVersion() + "]";
	}

	public static final EntityConstructor<OrganizationEntity, String, String> CONSTRUCTOR =
		new EntityConstructor<OrganizationEntity,String,String>(OrganizationEntity.class,
				new IPersistentObjectManager.IPersistentKey<OrganizationEntity,String>() {

			@Override
			public String getKey(OrganizationEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(OrganizationEntity o, String newKey) {
				o.setId(Long.valueOf(newKey));
			}

		},
		new EntityPersistence.ICreateObject<OrganizationEntity,String,String>() {

			@Override
			public OrganizationEntity create(String label, String orgCode) {
				return new OrganizationEntity(label, orgCode, 
						PersistentObjectFactory.getFactory().getEmptyWorldAcl());
			}
		}
				);
	
	@Override
	public EntityConstructor<OrganizationEntity, String, String> tableConstructor() {
		return CONSTRUCTOR;
	}

}
