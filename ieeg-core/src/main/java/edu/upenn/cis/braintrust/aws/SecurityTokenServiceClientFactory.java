/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

import edu.upenn.cis.braintrust.IvProps;

/**
 * 
 * @author John Frommeyer
 *
 */
public final class SecurityTokenServiceClientFactory {

	private static class SecurityTokenServiceClientSupplier implements
			Supplier<AWSSecurityTokenServiceClient> {

		@Override
		public AWSSecurityTokenServiceClient get() {
			final AWSCredentials credentials = new BasicAWSCredentials(
					IvProps.getInboxAccessKeyId(), 
					IvProps.getInboxSecretKey());
			return new AWSSecurityTokenServiceClient(credentials);
		}

	}

	private static final Supplier<AWSSecurityTokenServiceClient> securityTokenServiceClientSupplier = Suppliers
			.memoize(new SecurityTokenServiceClientSupplier());

	public static AWSSecurityTokenServiceClient getSecurityTokenServiceClient() {
		return securityTokenServiceClientSupplier.get();
	}

}
