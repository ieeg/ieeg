/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

/**
 * Information for a time series.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public class TimeSeriesDto implements IHasLabel, IHasStringId, Serializable {

	private static final long serialVersionUID = 1L;
	private String label;
	private String fileKey;
	private String id;

	/** For GWT. */
	@SuppressWarnings("unused")
	private TimeSeriesDto() {}

	public TimeSeriesDto(
			final String id,
			final String label,
			@Nullable final String fileKey) {
		this.id = checkNotNull(id);
		this.label = checkNotNull(label);
		this.fileKey = fileKey;
	}

	/**
	 * @return the fileKey
	 */
	public String getFileKey() {
		return fileKey;
	}

	/**
	 * @return the label
	 */
	@Override
	@Nonnull
	public String getLabel() {
		return label;
	}

	/**
	 * @return the revId
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * @param fileKey the fileKey to set
	 */
	public void setFileKey(@Nullable final String fileKey) {
		this.fileKey = fileKey;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = checkNotNull(label);
	}

	/**
	 * @param revId the revId to set
	 */
	public void setId(final String revId) {
		this.id = checkNotNull(revId);
	}

	@Override
	public String toString() {
		return "TimeSeriesDto [label=" + label + ", fileKey=" + fileKey
				+ ", revId=" + id + "]";
	}

}
