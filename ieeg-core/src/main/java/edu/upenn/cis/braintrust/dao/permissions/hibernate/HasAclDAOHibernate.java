/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.permissions.hibernate;

import static com.google.common.base.Preconditions.checkNotNull;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.permissions.IHasAclDAO;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;

public class HasAclDAOHibernate implements IHasAclDAO {

	private final Session session;

	public HasAclDAOHibernate(final Session session) {
		this.session = checkNotNull(session);
	}

	@Override
	public IHasExtAcl findByNaturalId(HasAclType entityType, String targetId) {
		return (IHasExtAcl) session.bySimpleNaturalId(
				entityType.getEntityClass().getCanonicalName())
				.load(targetId);

	}

}
