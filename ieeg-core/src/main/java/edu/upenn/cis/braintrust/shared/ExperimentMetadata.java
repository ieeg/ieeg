/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

/**
 * An experiment.
 * 
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ExperimentMetadata implements CaseMetadata, SerializableMetadata {

	private static final long serialVersionUID = 1L;

	private String id;
	private String label;
	private AnimalMetadata animal;
	private int imageCount;
	private String stimulationRegion;
	private String stimulationType;
	private Integer stimulationMs;
	private Integer stimulationIsiMs;
	private Set<DrugAdminMetadata> drugAdmins;
	private String refElectrodeDescription;
	private Set<ContactGroupMetadata> contactGroups;
	private String age;

	SerializableMetadata parent = null;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("animal", VALUE_TYPE.META);
		valueMap.put("imageCount", VALUE_TYPE.INT);
		valueMap.put("stimulationRegion", VALUE_TYPE.STRING);
		valueMap.put("stimulationType", VALUE_TYPE.STRING);
		valueMap.put("stimulationMs", VALUE_TYPE.INT);
		valueMap.put("stimulationIsiMs", VALUE_TYPE.INT);
		valueMap.put("drugAdmins", VALUE_TYPE.META_SET);
		valueMap.put("refElectrodeDescription", VALUE_TYPE.STRING_SET);

		valueMap.put("contactGroups", VALUE_TYPE.META_SET);

		valueMap.put("ageAtOnset", VALUE_TYPE.STRING);
	}
	
	/** For GWT. */
	@SuppressWarnings("unused")
	private ExperimentMetadata() {}

	public ExperimentMetadata(
			String id,
			String label,
			AnimalMetadata animal,
			int imageCount,
			@Nullable String stimulationRegion,
			@Nullable String stimulationType,
			@Nullable Integer stimulationMs,
			@Nullable Integer stimulationIsiMs,
			Set<DrugAdminMetadata> drugAdmins,
			@Nullable String refElectrodeDescription,
			Set<ContactGroupMetadata> contactGroups,
			@Nullable String age) {
		this.id = checkNotNull(id);
		this.label = checkNotNull(label);
		this.animal = checkNotNull(animal);
		this.imageCount = imageCount;
		this.stimulationRegion = stimulationRegion;
		this.stimulationType = stimulationType;
		this.stimulationMs = stimulationMs;
		this.stimulationIsiMs = stimulationIsiMs;
		this.drugAdmins = ImmutableSet.copyOf(drugAdmins);
		this.refElectrodeDescription = refElectrodeDescription;
		this.contactGroups = ImmutableSet.copyOf(contactGroups);
		this.age = age;
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExperimentMetadata)) {
			return false;
		}
		ExperimentMetadata other = (ExperimentMetadata) obj;
		if (!Objects.equal(id, other.id)) {
			return false;
		}
		return true;
	}

	public String getAge() {
		return age;
	}

	public AnimalMetadata getAnimal() {
		return animal;
	}

	public Set<ContactGroupMetadata> getContactGroups() {
		return contactGroups;
	}

	public Set<DrugAdminMetadata> getDrugAdmins() {
		return drugAdmins;
	}

	public String getId() {
		return id;
	}

	public int getImageCount() {
		return imageCount;
	}

	public String getLabel() {
		return label;
	}

	public String getRefElectrodeDescription() {
		return refElectrodeDescription;
	}

	public Integer getStimIsiMs() {
		return stimulationIsiMs;
	}

	public Integer getStimMs() {
		return stimulationMs;
	}

	public String getStimRegion() {
		return stimulationRegion;
	}

	public String getStimType() {
		return stimulationType;
	}
	
	

	public void setLabel(String label) {
		this.label = label;
	}

	public void setAnimal(AnimalMetadata animal) {
		this.animal = animal;
	}

	public void setImageCount(int imageCount) {
		this.imageCount = imageCount;
	}

	public void setStimulationRegion(String stimulationRegion) {
		this.stimulationRegion = stimulationRegion;
	}

	public void setStimulationType(String stimulationType) {
		this.stimulationType = stimulationType;
	}

	public void setStimulationMs(Integer stimulationMs) {
		this.stimulationMs = stimulationMs;
	}

	public void setStimulationIsiMs(Integer stimulationIsiMs) {
		this.stimulationIsiMs = stimulationIsiMs;
	}

	public void setDrugAdmins(Set<DrugAdminMetadata> drugAdmins) {
		this.drugAdmins = drugAdmins;
	}

	public void setRefElectrodeDescription(String refElectrodeDescription) {
		this.refElectrodeDescription = refElectrodeDescription;
	}

	public void setContactGroups(Set<ContactGroupMetadata> contactGroups) {
		this.contactGroups = contactGroups;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Metadata.name();
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return getId();
		if (key.equals("stimulationRegion"))
			return getStimRegion();
		if (key.equals("stimulationType"))
			return getStimType();
		if (key.equals("ageAtOnset"))
			return getAge();

		return null;
	}

	@JsonIgnore
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		return null;
	}

	@JsonIgnore
	@Override
	public Integer getIntegerValue(String key) {
		if (key.equals("imageCount"))
			return getImageCount();
		else if (key.equals("stimulationMs"))
			return getStimMs();
		else if (key.equals("stimulationIsiMs"))
			return getStimIsiMs();
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		if (key.equals("animal"))
			return getAnimal();
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		List<String> ret = new ArrayList<String>();
		if (key.equals("refElectrodeDescription")) {
			ret.add(getRefElectrodeDescription());
			Collections.sort(ret);
			return ret;
		}
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		List<GeneralMetadata> ret = new ArrayList<GeneralMetadata>();
		if (key.equals("drugAdmins")) {
			ret.addAll(getDrugAdmins());
			return ret;
		} else if (key.equals("contactGroups")) {
			ret.addAll(getContactGroups());
			return ret;
		}
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		parent = (SerializableMetadata)p;
	}

	@JsonIgnore
	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	@JsonIgnore
	@Override
	public boolean isHumanPatient() {
		return false;
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		return "";
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Metadata";
	}
}
