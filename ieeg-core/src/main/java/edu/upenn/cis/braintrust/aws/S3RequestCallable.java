/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.aws;

import java.io.IOException;
import java.util.concurrent.Callable;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;

/**
 * @author Sam Donnelly
 */
public final class S3RequestCallable implements Callable<byte[]> {

	private final AmazonS3 s3;
	private final GetObjectRequest getObjectRequest;
	private final String requestType;

	public S3RequestCallable(
			AmazonS3 s3,
			GetObjectRequest getObjectRequest,
			String requestType) {
		this.s3 = s3;
		this.getObjectRequest = getObjectRequest;
		this.requestType = requestType;
	}

	@Override
	public byte[] call() throws IOException {
		final String M = "call()";
		return AwsUtil.request(s3, getObjectRequest, requestType);
	}
}
