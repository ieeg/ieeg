/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.Serializable;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

@GwtCompatible(serializable = true)
public final class MontageDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Optional<MontageIdAndVersion> id;
	private DataSnapshotId parentId;
	private String name;
	private Set<MontagedChannelDto> montagedChannels = newLinkedHashSet();

	@SuppressWarnings("unused")
	private MontageDto() {}

	public MontageDto(
			DataSnapshotId parentId,
			String name,
			Set<MontagedChannelDto> montagedChannels) {
		this.parentId = checkNotNull(parentId);
		this.name = checkNotNull(name);
		this.montagedChannels = newLinkedHashSet(montagedChannels);
		this.id = Optional.absent();
	}

	public MontageDto(
			DataSnapshotId parentId,
			String name,
			Set<MontagedChannelDto> montagedChannels,
			MontageIdAndVersion id) {
		this(parentId, name, montagedChannels);
		this.id = Optional.of(id);
	}

	public Optional<MontageIdAndVersion> getId() {
		return id;
	}

	public Set<MontagedChannelDto> getMontagedChannels() {
		return montagedChannels;
	}

	public String getName() {
		return name;
	}

	public DataSnapshotId getParentId() {
		return parentId;
	}

	public void setId(MontageIdAndVersion montageId) {
		this.id = Optional.of(montageId);
	}

	public void setName(String name) {
		this.name = checkNotNull(name);
	}

	public void setParentId(DataSnapshotId parentId) {
		this.parentId = parentId;
	}
}
