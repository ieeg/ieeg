/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.cli;

public class ReoffsetAnnotations {
	// public static void main(String[] args) throws IOException {
	//
	// checkArgument(args.length == 1);
	//
	// FileInputStream hibernateProps = null;
	// Properties props = new Properties();
	// try {
	// hibernateProps = new FileInputStream(args[0]);
	// props.load(hibernateProps);
	// } finally {
	// try {
	// if (hibernateProps != null) {
	// hibernateProps.close();
	// }
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// if (props.containsKey("hibernate.hbm2ddl.auto")) {
	// System.err
	// .println("you have an hibernate.hbm2ddl.auto!!! stopping....");
	// System.exit(1);
	// }
	//
	// HibernateUtil.getConfiguration().setProperties(props);
	//
	// Session sess = null;
	// Transaction trx = null;
	// try {
	// sess = HibernateUtil.getSessionFactory().openSession();
	// trx = sess.beginTransaction();
	// int totalAnns = 0;
	// IEegStudyDAO studyDAO = new EegStudyDAOHibernate(sess);
	// for (EegStudy study : newHashSet(studyDAO.findAll())) {
	// System.out.println("looking at study " + study.getLabel());
	// // if (study.getId().equals(35L)) {
	// // System.out.println("passing over study " + study.getLabel()
	// // + " " + study.getId());
	// // continue;
	// // }
	// for (Electrode electrode : study.getElectrodes()) {
	// for (Contact contact : electrode.getContacts()) {
	// System.out.println("looking at trace "
	// + contact.getTrace().getLabel());
	// String hqlVersionedUpdate =
	// "update versioned TsAnnotation set startTimeUutc = startTimeUutc - :studyStartTimeUutc, endTimeUutc = endTimeUutc - :studyStartTimeUutc "
	// + "where annotated = :annotated "
	// + "and startTimeUutc >= :studyStartTimeUutc";
	// int updatedEntities = sess
	// .createQuery(hqlVersionedUpdate)
	// .setLong("studyStartTimeUutc",
	// study.getStartTimeUutc())
	// .setParameter("annotated", contact.getTrace())
	// .executeUpdate();
	// totalAnns += updatedEntities;
	// System.out.println("updated " + updatedEntities
	// + " annotations, total " + totalAnns);
	// }
	// }
	// studyDAO.evict(study);
	// }
	// trx.commit();
	// } catch (Throwable t) {
	// PersistenceUtil.rollBackTrx(trx);
	// } finally {
	// PersistenceUtil.closeSession(sess);
	// }
	// }
}
