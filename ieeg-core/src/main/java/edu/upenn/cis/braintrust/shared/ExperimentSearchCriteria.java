/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableMap;

/**
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
@Immutable
public final class ExperimentSearchCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<Long, String> species;
	private Map<Long, String> drugs;
	private Map<Long, String> stimTypes;
	private Map<Long, String> stimRegions;
	private Map<String, String> organizations;

	@SuppressWarnings("unused")
	private ExperimentSearchCriteria() {}

	/**
	 * All values must be in alphabetical iterator-order.
	 * 
	 * @param species the species
	 * @param drugs the drugs
	 * @param stimTypes the stim types
	 * @param stimRegions the stim regions
	 */
	public ExperimentSearchCriteria(
			Map<Long, String> species,
			Map<Long, String> drugs,
			Map<Long, String> stimTypes,
			Map<Long, String> stimRegions,
			Map<String, String> organizations) {
		this.species = ImmutableMap.copyOf(species);
		this.drugs = ImmutableMap.copyOf(drugs);
		this.stimTypes = ImmutableMap.copyOf(stimTypes);
		this.stimRegions = ImmutableMap.copyOf(stimRegions);
		this.organizations = ImmutableMap.copyOf(organizations);
	}

	/**
	 * Entries in alphabetical order.
	 * 
	 * @return the drugs
	 */
	public Map<Long, String> getDrugs() {
		return drugs;
	}

	/**
	 * Entries in alphabetical order.
	 * 
	 * @return the organizations
	 */
	public Map<String, String> getOrganizations() {
		return organizations;
	}

	/**
	 * Entries in alphabetical order.
	 * 
	 * @return the species
	 */
	public Map<Long, String> getSpecies() {
		return species;
	}

	/**
	 * Entries in alphabetical order.
	 * 
	 * @return the stim regions
	 */
	public Map<Long, String> getStimRegions() {
		return stimRegions;
	}

	/**
	 * Entries in alphabetical order.
	 * 
	 * @return the stim types
	 */
	public Map<Long, String> getStimTypes() {
		return stimTypes;
	}

}
