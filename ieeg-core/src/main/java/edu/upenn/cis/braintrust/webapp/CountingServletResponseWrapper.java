/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * A response wrapper which will count the number of bytes sent.
 * 
 * @author John Frommeyer
 */
public class CountingServletResponseWrapper extends HttpServletResponseWrapper {

	private final CountingServletOutputStream cos;

	public CountingServletResponseWrapper(HttpServletResponse response)
			throws IOException {
		super(response);
		cos = new CountingServletOutputStream(super.getOutputStream());
	}

	@Override
	public ServletOutputStream getOutputStream() {
		return cos;
	}

	public long getByteCount() {
		return cos.getByteCount();
	}

}
