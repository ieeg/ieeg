/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import com.google.common.annotations.GwtCompatible;

/**
 * For missing S3 files.
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class FileNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	private String s3Bucket;
	private String fileKey;

	// For GWT
	@SuppressWarnings("unused")
	private FileNotFoundException() {}

	public FileNotFoundException(String bucket, String fileKey) {
		super("Could not find file [" + fileKey + "] in bucket [" + bucket
				+ "]");
		this.s3Bucket = bucket;
		this.fileKey = fileKey;
	}

	public FileNotFoundException(String bucket, String fileKey, Throwable cause) {
		super("Could not find file [" + fileKey + "] in bucket [" + bucket
				+ "]", cause);
		this.s3Bucket = bucket;
		this.fileKey = fileKey;
	}

	public String getS3Bucket() {
		return s3Bucket;
	}

	public String getFileKey() {
		return fileKey;
	}
}
