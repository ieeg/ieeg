/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import java.util.Map;

import edu.upenn.cis.braintrust.shared.ContactGroupSearch;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.EpilepsySubtypeSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.HospitalAdmissionSearch;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.PatientSearch;
import edu.upenn.cis.braintrust.shared.SeizureType;

public class Search2Hql {

	public static void admissionSearch2Query(
			final HospitalAdmissionSearch admissionSearch,
			final StringBuilder fromPart,
			final StringBuilder wherePart,
			final Map<String, Object> params) {
		if (admissionSearch.getPatientSearch() != null) {
			// fromPart.append("join admission.parent patient ");
			patientSearch2Query(
					admissionSearch.getPatientSearch(),
					fromPart,
					wherePart,
					params);
		}
	}

	public static void epilepsySubtypeSearch2Query(
			EpilepsySubtypeSearch epilepsySubtypeSearch,
			StringBuilder fromPart,
			StringBuilder wherePart,
			Map<String, Object> params) {
		if (epilepsySubtypeSearch.getTypes().size() > 0) {
			wherePart
					.append("and epSubtype.epilepsyType in (:epilepsyTypes) ");
			params.put("epilepsyTypes", epilepsySubtypeSearch.getTypes());
		}
		if (epilepsySubtypeSearch.getEtiologies().size() > 0) {
			wherePart
					.append("and epSubtype.etiology in (:etiologies) ");
			params.put("etiologies", epilepsySubtypeSearch.getEtiologies());
		}
	}

	public static void experimentSearch2Query(
			final ExperimentSearch experimentSearch,
			final StringBuilder fromPart,
			final StringBuilder wherePart,
			final Map<String, Object> params) {
		if (experimentSearch.getSpeciesIds().size() > 0) {
			wherePart.append("and animal.strain.parent.id "
					+ "in (:speciesIds) ");
			params.put("speciesIds", experimentSearch.getSpeciesIds());
		}
		if (experimentSearch.getDrugIds().size() > 0) {
			int i = -1;
			for (Long drugId : experimentSearch.getDrugIds()) {
				i++;
				wherePart
						.append("and experiment "
								+ "in (select e from Experiment e join e.drugAdmins das where das.drug.id = :drugId"
								+ i + ") ");
				params.put("drugId" + i, drugId);
			}
		}
		if (experimentSearch.getStimTypeIds().size() > 0) {
			wherePart
					.append("and experiment.stimType.id in (:stimTypeIds) ");
			params.put("stimTypeIds",
					experimentSearch.getStimTypeIds());
		}
		if (experimentSearch.getStimRegionIds().size() > 0) {
			wherePart
					.append("and experiment.stimRegion.id in (:stimRegionIds) ");
			params.put("stimRegionIds",
					experimentSearch.getStimRegionIds());
		}
		if (experimentSearch.getOrganizationCodes().size() > 0) {
			wherePart
					.append("and organization.code in (:organizationCodes) ");
			params.put(
					"organizationCodes",
					experimentSearch.getOrganizationCodes());
		}
	}

	public static void eegStudySearch2Query(
			final EegStudySearch studySearch,
			final StringBuilder fromPart,
			final StringBuilder wherePart,
			final Map<String, Object> params) {
		if (studySearch.getAdmissionSearch() != null) {
			// fromPart.append("join study.parent admission ");
			admissionSearch2Query(
					studySearch.getAdmissionSearch(),
					fromPart,
					wherePart,
					params);
		}
		if (studySearch.getContactGroupSearch() != null) {
			fromPart.append("join study.recording.contactGroups contactGroup ");
			contactGroupSearch2Query(studySearch.getContactGroupSearch(),
					fromPart,
					wherePart, params);
		}
		if (studySearch.getEpilepsySubtypeSearch() != null) {
			fromPart.append("join study.epilepsySubtypes epSubtype ");
			epilepsySubtypeSearch2Query(studySearch.getEpilepsySubtypeSearch(),
					fromPart, wherePart, params);
		}
		if (studySearch.getImageTypes() != null
				&& studySearch.getImageTypes().size() > 0) {
			int i = -1;
			for (final ImageType imageType : studySearch.getImageTypes()) {
				i++;
				wherePart
						.append("and study "
								+ "in (select s from EegStudy s join s.recording r join r.images i where i.type = :imageType"
								+ i + ") ");
				params.put("imageType" + i, imageType);
			}
		}

		String type = "seizure";
		Long tsAnnMin = 0L, tsAnnMax = Long.MAX_VALUE;
		boolean searchSzs = false;
		if (studySearch.getMinSzCount() != null
				&& studySearch.getMinSzCount() > 0) {
			tsAnnMin = Long
					.valueOf(studySearch.getMinSzCount());
			searchSzs = true;
		}

		if (studySearch.getMaxSzCount() != null) {
			tsAnnMax = Long.valueOf(studySearch.getMaxSzCount());
			searchSzs = true;
		}

		if (searchSzs) {
			wherePart
					.append(
					"and ("
							+ "select count(*) "
							+ "from TsAnnotation tsa "
							+ "where tsa.parent = study "
							+ (type == null ? ""
									: "and lower(tsa.type) = lower(:tsAnnType) ")
							+
							") between :tsAnnMin and :tsAnnMax ");

			if (tsAnnMin != null) {
				params.put("tsAnnMin", tsAnnMin);
			}
			if (tsAnnMax != null) {
				params.put("tsAnnMax", tsAnnMax);
			}
			if (type != null) {
				params.put("tsAnnType", type);
			}
		}

		// if (studySearch.getSurgeryTypes() != null) {
		// int i = 0;
		// for (final SurgeryType surgeryType : studySearch
		// .getSurgeryTypes()) {
		// wherePart
		// .append("and study "
		// +
		// "in (select s from EegStudy s join s.surgery su where su.type = :surgeryType"
		// + i + ") ");
		// params.put("surgeryType" + i, surgeryType);
		// i++;
		// }
		// }
		// if (studySearch.getIlaeRatings() != null) {
		// int i = 0;
		// for (final IlaeRating ilaeRating : studySearch
		// .getIlaeRatings()) {
		// wherePart
		// .append("and study "
		// +
		// "in (select s from EegStudy s join s.surgery su join su.followUps f where f.outcome = :outcome"
		// + i + ") ");
		// params.put("outcome" + i, ilaeRating);
		// i++;
		// }
		// }

		// if (studySearch.getElectrodeSearch() != null) {
		// TraceSearch traceSearch = studySearch.getElectrodeSearch()
		// .getTraceSearch();
		// if (traceSearch != null
		// && (traceSearch.getAnnotationType() != null
		// || (traceSearch.getMinAnnotationCount() != null && traceSearch
		// .getMinAnnotationCount() >= 0) || traceSearch
		// .getMaxAnnotationCount() != null)) {
		// fromPart.append("join study.electrodes studyElectrode join studyElectrode.contacts studyContact join studyContact.trace studyTrace ");
		//
		// String type = null;
		// Long tsAnnMin = 0L, tsAnnMax = Long.MAX_VALUE;
		//
		// if (traceSearch.getAnnotationType() != null) {
		// type = traceSearch.getAnnotationType();
		// }
		//
		// if (traceSearch.getMinAnnotationCount() != null
		// && traceSearch.getMinAnnotationCount() > 0) {
		// tsAnnMin = Long
		// .valueOf(traceSearch.getMinAnnotationCount());
		// }
		//
		// if (traceSearch.getMaxAnnotationCount() != null) {
		// tsAnnMax = Long
		// .valueOf(traceSearch.getMaxAnnotationCount());
		// }
		//
		// if (tsAnnMin != null && tsAnnMax != null) {
		// wherePart
		// .append(
		// "and ("
		// + "select count(*) "
		// + "from TsAnnotation tsa "
		// + "where tsa.parent = study "
		// + (type == null ? ""
		// : "and tsa.type = :tsAnnType ")
		// +
		// "and tsa.annotated = studyTrace) between :tsAnnMin and :tsAnnMax ");
		// }
		// if (tsAnnMin != null) {
		// params.put("tsAnnMin", tsAnnMin);
		// }
		// if (tsAnnMax != null) {
		// params.put("tsAnnMax", tsAnnMax);
		// }
		// if (type != null) {
		// params.put("tsAnnType", type);
		// }
		// }
		// }
	}

	public static void contactGroupSearch2Query(
			final ContactGroupSearch contactGroupSearch,
			final StringBuilder fromPart,
			final StringBuilder wherePart,
			final Map<String, Object> params) {
		if (contactGroupSearch.getSamplingRateMin() != null) {
			wherePart
					.append("and contactGroup.samplingRate >= :samplingRateMin ");
			params.put("samplingRateMin",
					contactGroupSearch.getSamplingRateMin());
		}
		if (contactGroupSearch.getSamplingRateMax() != null) {
			wherePart
					.append("and contactGroup.samplingRate <= :samplingRateMax ");
			params.put("samplingRateMax",
					contactGroupSearch.getSamplingRateMax());
		}
		if (contactGroupSearch.getTypes().size() > 0) {
			wherePart
					.append("and contactGroup.electrodeType in (:electrodeTypes) ");
			params.put("electrodeTypes", contactGroupSearch.getTypes());
		}
		if (contactGroupSearch.getSides().size() > 0) {
			wherePart
					.append("and contactGroup.side in (:contactGroupSides) ");
			params.put("contactGroupSides", contactGroupSearch.getSides());

		}
		if (contactGroupSearch.getLocations().size() > 0) {
			wherePart
					.append("and contactGroup.location in (:contactGroupLocations) ");
			params.put("contactGroupLocations",
					contactGroupSearch.getLocations());
		}
		// if (electrodeSearch.getContactTypes().size() > 0) {
		// fromPart.append("join electrode.contacts contact ");
		// wherePart
		// .append("and contact.info.contactType in (:contactTypes) ");
		// params.put("contactTypes", electrodeSearch.getContactTypes());
		// }
		if (contactGroupSearch.getContactTypes().size() > 0) {
			int i = -1;
			for (ContactType contactType : contactGroupSearch.getContactTypes()) {
				i++;
				wherePart
						.append("and contactGroup in (select contactGroup from ContactGroup contactGroup join contactGroup.contacts contact where contact.contactType = :contactType"
								+ i + ") ");
				params.put("contactType" + i, contactType);
			}
		}
	}

	public static void patientSearch2Query(
			final PatientSearch patientSearch,
			final StringBuilder fromPart,
			final StringBuilder wherePart,
			final Map<String, Object> params) {
		if (patientSearch.getSzTypes().size() > 0) {
			int i = -1;
			for (final SeizureType szType : patientSearch.getSzTypes()) {
				i++;
				wherePart
						.append("and patient in (select patient from Patient patient join patient.szTypes szType where szType = :szType"
								+ i + ") ");
				params.put("szType" + i, szType);
			}
		}

		boolean addedSzTypesJoin = false;

		if (patientSearch.getGeneralizedSzTypes() != null
				&& patientSearch.getGeneralizedSzTypes() == true) {
			fromPart.append("join patient.szTypes patientSzType ");
			addedSzTypesJoin = true;
			wherePart
					.append("and patientSzType in (:genSzTypes) ");
			params.put("genSzTypes", SeizureType.GENERALIZED_SZ_TYPES);
		}
		if (patientSearch.getPartialSzTypes() != null
				&& patientSearch.getPartialSzTypes() == true) {
			if (!addedSzTypesJoin) {
				fromPart.append("join patient.szTypes patientSzType ");
			}
			wherePart
					.append("and patientSzType in (:partSzTypes) ");
			params.put("partSzTypes", SeizureType.PARTIAL_SZ_TYPES);
		}
		if (patientSearch.getAgeOfOnsetMin() != null) {
			wherePart.append("and patient.ageOfOnset >= :ageOfOnsetMin ");
			params.put("ageOfOnsetMin", patientSearch.getAgeOfOnsetMin());
		}

		if (patientSearch.getAgeOfOnsetMax() != null) {
			wherePart.append("and patient.ageOfOnset <= :ageOfOnsetMax ");
			params.put("ageOfOnsetMax", patientSearch.getAgeOfOnsetMax());
		}

		if (patientSearch.getGenders().size() > 0) {
			wherePart.append("and patient.gender in :genders ");
			params.put("genders", patientSearch.getGenders());
		}
	}
}
