/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class StrainAssembler {

	public GwtStrain buildDto(
			final StrainEntity strain,
			String speciesLabel, Long speciesId) {
		checkNotNull(strain);
		checkNotNull(speciesLabel);
		checkNotNull(speciesId);
		GwtStrain gwtStrain = new GwtStrain(
				strain.getLabel(),
				strain.getId(),
				strain.getVersion(),
				speciesLabel, speciesId);
		return gwtStrain;
	}

	public StrainEntity buildEntity(
			final SpeciesEntity parentEntity,
			final GwtStrain gwtStrain) {
		checkNotNull(parentEntity);
		checkNotNull(gwtStrain);
		checkArgument(gwtStrain.getId() == null);
		checkArgument(gwtStrain.getVersion() == null);
		final StrainEntity strain = new StrainEntity(
				parentEntity,
				gwtStrain.getLabel());
		return strain;
	}

	public void modifyEntity(
			final StrainEntity strain,
			final GwtStrain gwtStrain)
			throws StaleObjectException {
		checkNotNull(gwtStrain);
		checkNotNull(strain);
		final Long dtoId = gwtStrain.getId();
		final Long entityId = strain.getId();
		if (!dtoId.equals(entityId)) {
			throw new IllegalArgumentException("DTO id: [" + dtoId
					+ "] does not match entity id: [" + entityId + "].");
		}
		final Integer dtoV = gwtStrain.getVersion();
		final Integer entityV = strain.getVersion();
		if (!dtoV.equals(entityV)) {
			throw new StaleObjectException(
					"Version mismatch for strain ["
							+ entityId + "]. DTO version: [" + dtoV
							+ "], DB version: [" + entityV + "].");
		}
		copyToEntity(strain, gwtStrain);
	}

	private void copyToEntity(
			final StrainEntity strain,
			final GwtStrain gwtStrain) {
		strain.setLabel(gwtStrain.getLabel());
	}
}
