/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.RelationshipConstructor;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence;

/**
 * @author John Frommeyer
 * 
 */
@Entity(name = "DrugAdmin")
@Table(name = DrugAdminRelationship.TABLE)
public class DrugAdminRelationship implements IHasLongId, IRelationship {

	/**
	 * This comparator is not compatible with equals. So use caution. For
	 * example, don't use it to create a {@link SortedSet}.
	 */
	public static class DrugAdminRelationshipComparator
			implements Comparator<DrugAdminRelationship> {

		@Override
		public int compare(DrugAdminRelationship arg0, DrugAdminRelationship arg1) {
			if (arg0.getAdminTime() == null && arg1.getAdminTime() == null) {
				return 0;
			}
			if (arg0.getAdminTime() == null) {
				return -1;
			}
			if (arg1.getAdminTime() == null) {
				return 1;
			}
			return arg0.getAdminTime().compareTo(arg1.getAdminTime());
		}

	}

	public static final String TABLE = "drug_admin";
	public static final String ID_COLUMN = TABLE + "_id";
	private Long id;
	private Integer version;
	private DrugEntity drug;
	private Date adminTime;
	private BigDecimal doseMgPerKg;
	private ExperimentEntity parent;

	public DrugAdminRelationship() {}

	public DrugAdminRelationship(
			ExperimentEntity parent,
			DrugEntity drug,
			@Nullable Date adminTime,
			@Nullable BigDecimal doseMgPerKg) {
		this.parent = checkNotNull(parent);
		this.parent.getDrugAdmins().add(this);
		this.drug = checkNotNull(drug);
		this.adminTime = adminTime;
		this.doseMgPerKg = doseMgPerKg;
	}

	public Date getAdminTime() {
		return adminTime;
	}

	@Min(0)
	@Digits(integer = 8, fraction = 2)
	public BigDecimal getDoseMgPerKg() {
		return doseMgPerKg;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = DrugEntity.ID_COLUMN)
	@NotNull
	public DrugEntity getDrug() {
		return drug;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public ExperimentEntity getParent() {
		return parent;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setAdminTime(Date adminTime) {
		this.adminTime = adminTime;
	}

	public void setDoseMgPerKg(BigDecimal doseMgPerKg) {
		this.doseMgPerKg = doseMgPerKg;
	}

	public void setDrug(DrugEntity drug) {
		this.drug = drug;
	}

	@VisibleForTesting
	public void setId(Long id) {
		this.id = id;
	}

	public void setParent(ExperimentEntity parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

	@Override
	public RelationshipConstructor<? extends IRelationship, ?, ? extends IEntity, ? extends IEntity> tableConstructor() {
		// DrugAdminEntity
		return new RelationshipConstructor<DrugAdminRelationship,Long,ExperimentEntity,DrugEntity>(DrugAdminRelationship.class,
				new IPersistentObjectManager.IPersistentKey<DrugAdminRelationship,Long>() {

					@Override
					public Long getKey(DrugAdminRelationship o) {
						return o.getId();
					}

					@Override
					public void setKey(DrugAdminRelationship o, Long newKey) {
						o.setId(newKey);
					}

				},
				new RelationshipPersistence.ICreateRelationship<DrugAdminRelationship,Long,ExperimentEntity,DrugEntity>() {

					@Override
					public DrugAdminRelationship create(Long id, ExperimentEntity e1, DrugEntity e2) {
						// Null adminTime, dose
						return new DrugAdminRelationship(e1, e2, null, null);
					}
				}
		);
	}
}
