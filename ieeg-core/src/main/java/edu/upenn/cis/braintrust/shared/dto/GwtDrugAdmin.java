/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 *
 * 
 */
public class GwtDrugAdmin implements IHasLongId, IHasVersion, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * This comparator is not compatible with equals. So use caution. For
	 * example, don't use it to create a {@link SortedSet}.
	 */
	public static class GwtDrugAdminComparator
			implements Comparator<GwtDrugAdmin> {

		@Override
		public int compare(GwtDrugAdmin arg0, GwtDrugAdmin arg1) {
			if (arg0.getAdminTime() == null && arg1.getAdminTime() == null) {
				return 0;
			}
			if (arg0.getAdminTime() == null) {
				return -1;
			}
			if (arg1.getAdminTime() == null) {
				return 1;
			}
			return arg0.getAdminTime().compareTo(arg1.getAdminTime());
		}

	}

	private Long id;
	private Integer version;
	private GwtDrug drug;
	private Date adminTime;
	private BigDecimal doseMgPerKg;

	@SuppressWarnings("unused")
	private GwtDrugAdmin() {}

	public GwtDrugAdmin(
			GwtDrug drug,
			@Nullable Date adminTime,
			@Nullable BigDecimal doseMgPerKg,
			@Nullable Long id,
			@Nullable Integer version) {
		this.drug = checkNotNull(drug);
		this.adminTime = adminTime;
		this.doseMgPerKg = doseMgPerKg;
		this.id = id;
		this.version = version;
	}

	public Date getAdminTime() {
		return adminTime;
	}

	public BigDecimal getDoseMgPerKg() {
		return doseMgPerKg;
	}

	public GwtDrug getDrug() {
		return drug;
	}

	public Long getId() {
		return id;
	}

	public Integer getVersion() {
		return version;
	}
}
