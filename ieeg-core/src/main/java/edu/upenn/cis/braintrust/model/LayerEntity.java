/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import edu.upenn.cis.braintrust.shared.IHasName;

/**
 * No version field because all a layer has is a name and we don't care about
 * locking that down.
 * 
 * @author Sam Donnelly
 */
@Entity(name = "Layer")
@Table(name = LayerEntity.TABLE,
		uniqueConstraints = {
				@UniqueConstraint(
						columnNames = {
								"parent_id",
								LayerEntity.NAME_COLUMN
						},
						name = "uq_layer_parent_id_name")
		})
@AttributeOverrides({
	@AttributeOverride(
			name = "id",
			column = @Column(
					name = LayerEntity.ID_COLUMN)
	)
})
public class LayerEntity
		extends LongId
		implements IHasName {

	public static final String TABLE = "layer";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String NAME_COLUMN = "name";

	private MontageEntity parent;
	private String name;
	private Set<MontagedChAnnotationEntity> annotations = newHashSet();

	/** For JPA. */
	LayerEntity() {}

	public LayerEntity(
			MontageEntity montage,
			String name) {
		this.parent = checkNotNull(montage);
		this.name = checkNotNull(name);
	}

	public int countAnnotations() {
		return annotations.size();
	}

	/**
	 * Extra lazy since there could be too many to load up at one time.
	 * <p>
	 * Private because you shouldn't iterate over them - too inefficient to load
	 * them up individually.
	 * <p>
	 * This is really only good for {@link #countAnnotations()}.
	 */
	@OneToMany
	@JoinColumn(name = "parent_id", insertable = false, updatable = false)
	@LazyCollection(LazyCollectionOption.EXTRA)
	private Set<MontagedChAnnotationEntity> getAnnotations() {
		return annotations;
	}

	@Override
	@Size(min = 1, max = 255)
	@NotNull
	public String getName() {
		return name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public MontageEntity getParent() {
		return parent;
	}

	@SuppressWarnings("unused")
	private void setAnnotations(
			Set<MontagedChAnnotationEntity> annotations) {
		this.annotations = annotations;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setParent(MontageEntity montage) {
		this.parent = montage;
	}

}
