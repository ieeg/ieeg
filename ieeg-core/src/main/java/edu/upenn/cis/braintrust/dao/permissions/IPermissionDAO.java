/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.permissions;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IPermissionDAO extends IDAO<PermissionEntity, Long> {

	PermissionEntity findByDomainModeAndName(
			String permissionTarget,
			String mode,
			String name);

	PermissionEntity findStarCoreReadPerm();

	PermissionEntity findStarCoreEditPerm();

	PermissionEntity findStarCoreOwnerPerm();

	/**
	 * Return the set of {@code PermissionEntity}s which match the
	 * {@code permDtos}.
	 * 
	 * @param permDtos a set of {@code ExtPermission}.
	 * @return the set of {@code PermissionEntity}s which match the
	 *         {@code permDtos}
	 */
	Set<PermissionEntity> findByDtos(Set<ExtPermission> permDtos);

	/**
	 * Returns the list of {@code PermissionEntity}s with the given domain and
	 * mode including any with the domain wildcard ordered by
	 * {@code displayOrder} field.
	 * 
	 * @param permissionDomain
	 * @param mode
	 * @return the list of {@code PermissionEntity}s with the given domain and
	 *         mode including any with the domain wildcard ordered by
	 *         {@code displayOrder} field
	 */
	List<PermissionEntity> findByDomainAndMode(HasAclType permissionDomain,
			String mode);
}
