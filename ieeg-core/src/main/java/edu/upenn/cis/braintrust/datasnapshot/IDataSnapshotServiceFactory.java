/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.google.common.cache.Cache;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;

/**
 * A factory for making {@code IDataSnaphotService} instances.
 * 
 * @author John Frommeyer
 */
public interface IDataSnapshotServiceFactory {

	final static int DEFAULT_MAX_TS_ANNS = 100000;

	IDataSnapshotService newInstance(
			Session session,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srCacheEntry);

	/**
	 * Returns a new instance of a {@code IDataSnapshotService}.
	 * 
	 * @param session
	 * 
	 * @return a new instance of a {@code IDataSnapshotService}
	 */
	IDataSnapshotService newInstance(
			Session session,
			Configuration config,
			int maxTsAnns,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srCacheEntry);

	/**
	 * Shorthand for
	 * {@code newInstance(session, HibernateUtil.getConfiguration()}.
	 * 
	 * @param session
	 * @return
	 */
	IDataSnapshotService newInstance(
			Session session,
			int maxTsAnns,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srCacheEntry);
}
