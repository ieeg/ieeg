/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "RecordingObjectTaskMetadata")
@Table(name = "recording_object_task_metadata")
public class RecordingObjectTaskMetadataEntity {

	private Long id;
	private Integer version;
	private RecordingObjectTaskEntity parent;
	private String value;

	/**
	 * For JPA.
	 */
	RecordingObjectTaskMetadataEntity() {}

	public RecordingObjectTaskMetadataEntity(
			String value,
			RecordingObjectTaskEntity parent) {
		this.value = checkNotNull(value);
		checkArgument(value.length() > 1);
		this.parent = checkNotNull(parent);
	}

	@Id
	public Long getId() {
		return id;
	}

	@MapsId
	// this is really one-to-one, but we need to make it many-to-one since its
	// one-to-many on the other side. See the other side for why.
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	public RecordingObjectTaskEntity getParent() {
		return parent;
	}

	@Lob
	@NotNull
	public String getValue() {
		return value;
	}

	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setParent(RecordingObjectTaskEntity parent) {
		this.parent = parent;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}
}