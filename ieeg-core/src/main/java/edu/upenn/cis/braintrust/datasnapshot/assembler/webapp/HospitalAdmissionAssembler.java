/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class HospitalAdmissionAssembler {

	private final EegStudyAssembler studyAssembler;
	private final ITsAnnotationDAO annotationDAO;
	private final IDataSnapshotService dsService;

	public HospitalAdmissionAssembler(
			IPermissionDAO permissionDAO,
			ITsAnnotationDAO annotationDAO,
			IDataSnapshotService dsService) {
		checkNotNull(permissionDAO);
		this.annotationDAO = checkNotNull(annotationDAO);
		this.dsService = checkNotNull(dsService);
		studyAssembler = new EegStudyAssembler(
				permissionDAO,
				annotationDAO,
				dsService);
	}

	public GwtHospitalAdmission buildDto(final HospitalAdmission admission) {
		checkNotNull(admission);
		final GwtHospitalAdmission gwtAdmission = new GwtHospitalAdmission();
		gwtAdmission.setId(admission.getId());
		gwtAdmission.setVersion(admission.getVersion());
		gwtAdmission.setAgeAtAdmission(admission.getAgeAtAdmission());
		for (final EegStudy study : admission.getStudies()) {
			gwtAdmission.addStudy(studyAssembler.buildDto(study));
		}
		return gwtAdmission;
	}

	public HospitalAdmission buildEntity(final GwtHospitalAdmission gwtAdmission) {
		checkNotNull(gwtAdmission);
		checkArgument(gwtAdmission.getId() == null);
		checkArgument(gwtAdmission.getVersion() == null);
		// A new admission should have no studies.
		checkArgument(gwtAdmission.getStudies().isEmpty(),
				"A new admission should have no studies.");
		final HospitalAdmission admission = new HospitalAdmission();
		copyToEntity(admission, gwtAdmission);
		return admission;
	}

	private void copyToEntity(final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission) {
		admission.setAgeAtAdmission(gwtAdmission.getAgeAtAdmission().orNull());
	}

	public void modifyEntity(final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission)
			throws StaleObjectException, BrainTrustUserException {
		final Long gwtId = gwtAdmission.getId();
		final Long dbId = admission.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtAdmission.getVersion();
		final Integer dbVer = admission.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for admission ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		handleStudies(admission, gwtAdmission);
		copyToEntity(admission, gwtAdmission);
	}

	private void handleStudies(final HospitalAdmission admission,
			final GwtHospitalAdmission gwtAdmission)
			throws StaleObjectException, BrainTrustUserException {
		for (final GwtEegStudy gwtStudy : gwtAdmission.getStudies()) {
			final Long gwtId = gwtStudy.getId();
			if (gwtId == null) {
				final EegStudy study = studyAssembler.buildEntity(
						gwtStudy);
				admission.addStudy(study);
			} else {

				final EegStudy study = find(admission.getStudies(),
						compose(equalTo(gwtId), EegStudy.getId));
				final boolean modifiable = dsService.isRecordingModifiable(study);
				if (!gwtStudy.isDeleted()) {
					studyAssembler.modifyEntity(study, gwtStudy, modifiable);
				} else {
					if (!modifiable) {
						throw new BrainTrustUserException(
								"Study cannot be deleted. A time series in this study is referenced by a user created data snapshot.");
					} else {
						final Integer dbVersion = study.getVersion();
						final Integer gwtVersion = gwtStudy.getVersion();
						if (!dbVersion.equals(gwtVersion)) {
							throw new StaleObjectException(
									"Version mismatch for study ["
											+ gwtId + "]. DTO version: ["
											+ gwtVersion
											+ "], DB version: [" + dbVersion
											+ "].");
						}
						annotationDAO.deleteByParent(study);
						admission.removeStudy(study);
					}
				}
			}
		}
	}
}
