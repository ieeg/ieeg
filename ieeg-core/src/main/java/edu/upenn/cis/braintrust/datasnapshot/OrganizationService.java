/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.collect.Sets.newLinkedHashSet;

import java.util.Set;

import javax.annotation.concurrent.ThreadSafe;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.shared.Organization;

@ThreadSafe
public final class OrganizationService {

	private static Logger logger =
			LoggerFactory.getLogger(OrganizationService.class);

	private final SessionFactory sessFac;
	private final IDAOFactory daoFac;

	public OrganizationService() {
		sessFac = HibernateUtil.getSessionFactory();
		daoFac = new HibernateDAOFactory();
	}

	// public OrganizationEntity
	// getOrganization(String organizationCode) {
	// SessionAndTrx sessAndTrx = null;
	// try {
	// sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
	// IOrganizationDAO orgDAO = daoFac.getOrganizationDAO();
	// OrganizationEntity organization =
	// orgDAO.findByNaturalId(organizationCode);
	// PersistenceUtil.commit(sessAndTrx);
	// return organization;
	// } catch (RuntimeException re) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw re;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// }
	// }

	/**
	 * Will be ordered by the organizations' codes.
	 */
	public Set<Organization> getOrganizations() {
		long inNanos = System.nanoTime();
		String m = "getOrganizations()";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IOrganizationDAO orgDAO = daoFac.getOrganizationDAO();
			Set<OrganizationEntity> organizationEntities =
					newLinkedHashSet(orgDAO.findAllOrderBy("code"));
			Set<Organization> organizations = newLinkedHashSet();
			for (OrganizationEntity orgEntity : organizationEntities) {
				organizations.add(
						new Organization(
								orgEntity.getCode(),
								orgEntity.getName()));
			}
			PersistenceUtil.commit(sessAndTrx);
			return organizations;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	// public void save(
	// Set<? extends OrganizationEntity> organizations)
	// throws StaleDataException {
	// long inNanos = System.nanoTime();
	// String m = "updateOrganizations(...)";
	// SessionAndTrx sessAndTrx = null;
	// try {
	// sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
	// IOrganizationDAO orgDAO = daoFac.getOrganizationDAO();
	// for (OrganizationEntity organization : organizations) {
	// if (organization.getVersion() == null) {
	// orgDAO.saveOrUpdate(organization);
	// } else {
	// OrganizationEntity persistedOrg =
	// orgDAO.findByNaturalId(organization.getCode());
	// if (!persistedOrg.getVersion().equals(
	// organization.getVersion())) {
	// throw new StaleDataException("stale data "
	// + organization);
	// } else {
	// persistedOrg.setName(organization.getName());
	// }
	// }
	// }
	// PersistenceUtil.commit(sessAndTrx);
	// return;
	// } catch (RuntimeException re) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw re;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// logger.info(
	// "{} : {} seconds",
	// m,
	// BtUtil.diffNowThenSeconds(inNanos));
	// }
	// }
}
