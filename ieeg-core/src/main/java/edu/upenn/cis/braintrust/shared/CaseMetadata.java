package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.Previewable;

public interface CaseMetadata extends Serializable, Previewable {
	public String getAge();
	
	// TODO: need to regularize the useless DTOs first...
	public Set<ContactGroupMetadata> getContactGroups();
	
	public String getId();
	
	public int getImageCount();
	
	public String getLabel();
	
	public String getRefElectrodeDescription();
	
	public boolean isHumanPatient();
}
