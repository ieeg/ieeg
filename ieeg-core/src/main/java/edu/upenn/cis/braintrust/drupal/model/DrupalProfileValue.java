/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;

@Entity
@Table(name = "profile_values")
//@Immutable
public class DrupalProfileValue {
	@Embeddable
	public static class Id implements java.io.Serializable {

		private static final long serialVersionUID = 1L;

		public static final String FIELD_ID_COLUMN = "fid";

		private Long fid;
		private Long uid;

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Id)) {
				return false;
			} else {
				Id two = (Id) o;

				return two.fid.equals(fid)
						&& two.getUid().equals(getUid());
			}
		}

		@Column(name = FIELD_ID_COLUMN, columnDefinition = "INT UNSIGNED")
		public Long getFid() {
			return fid;
		}

		@Column(columnDefinition = "INT UNSIGNED")
		public Long getUid() {
			return uid;
		}

		@Override
		public int hashCode() {
			return Long.valueOf(fid + uid).hashCode();
		}

		@SuppressWarnings("unused")
		public void setFid(Long fid) {
			this.fid = fid;
		}

		@SuppressWarnings("unused")
		public void setUid(Long uid) {
			this.uid = uid;
		}

	}

	public final static String TABLE = "profile_values";

	public final static String FIND_BY_UID = "DrupalProfile-findByUserId";

	private Id id;

	private String value;

	private DrupalUser user;

	public DrupalProfileValue() {}
	
	public DrupalProfileValue(DrupalUser user, String value) {
		this.user = user;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DrupalProfileValue)) {
			return false;
		}
		DrupalProfileValue other = (DrupalProfileValue) obj;
		if (!Objects.equal(id, other.id)) {
			return false;
		}
		return true;
	}

	@EmbeddedId
	public Id getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
			name = DrupalUser.ID_COLUMN,
			insertable = false,
			updatable = false)
	public DrupalUser getUser() {
		return user;
	}

	@Column(columnDefinition = "text")
	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	public void setId(Id id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	public void setUser(DrupalUser user) {
		this.user = user;
	}

	@SuppressWarnings("unused")
	public void setValue(String value) {
		this.value = value;
	}

}
