/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.cli;

import java.io.IOException;

public class MigrateDot10ToDot11 {
	public static void main(String[] args) throws IOException {

		// System.out.println("starting "
		// + MigrateDot10ToDot11.class.getSimpleName() + "...");
		//
		// checkArgument(args.length == 1);
		//
		// FileInputStream hibernateProps = null;
		// Properties props = null;
		// try {
		// hibernateProps = new FileInputStream(args[0]);
		// props = new Properties();
		// props.load(hibernateProps);
		// } finally {
		// try {
		// if (hibernateProps != null) {
		// hibernateProps.close();
		// }
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		//
		// if (props.containsKey("hibernate.hbm2ddl.auto")) {
		// System.err
		// .println("you have an hibernate.hbm2ddl.auto!!! stopping...");
		// System.exit(1);
		// }
		//
		// HibernateUtil.getConfiguration().setProperties(props);
		//
		// Session sess = null;
		// Transaction trx = null;
		//
		// try {
		// sess = HibernateUtil.getSessionFactory().openSession();
		// IEegStudyDAO studyDao = new EegStudyDAOHibernate(sess);
		// IDatasetDAO datasetDao = new DatasetDAOHibernate(sess);
		// ITsAnnotationDAO tsAnnDao = new TsAnnotationDAOHibernate(sess,
		// HibernateUtil.getConfiguration());
		// ITsAnnotationOldDAO tsAnnOldDao = new TsAnnotationOldDAOHibernate(
		// sess,
		// HibernateUtil.getConfiguration());
		// IUserDAO userDao = new UserDAOHibernate(sess);
		// List<DataSnapshotEntity> dss = newArrayList();
		// dss.addAll(studyDao.findAll());
		// dss.addAll(datasetDao.findAll());
		// int totalOld = 0;
		// int totalNew = 0;
		// System.out.println("processing " + dss.size() + " snapshots");
		// int dsCounter = 1;
		// for (DataSnapshotEntity ds : dss) {
		// System.out.println("snapshot #" + dsCounter++ + ", "
		// + ds.getLabel());
		// trx = sess.beginTransaction();
		// while (true) {
		// TsAnnotationOldEntity tsAnnOld = (TsAnnotationOldEntity) sess
		// .createQuery(
		// "select tsa "
		// + "from TsAnnotationOld tsa "
		// + "where tsa.parent = :parent "
		// + "and tsa.id = "
		// + "(select min(id) "
		// + "from TsAnnotationOld tsa where tsa.parent = :parent)")
		// .setParameter("parent", ds)
		// .uniqueResult();
		// if (tsAnnOld == null) {
		// System.out.println("found null...");
		// break;
		// }
		//
		// System.out.println("handling [" + ds.getLabel()
		// + "]'s old ts ann " + tsAnnOld);
		// String q = "from TsAnnotationOld tso "
		// + "where "
		// + "tso.parent = :parent "
		// + "and tso.startTimeUutc = :startTimeUutc "
		// + "and tso.endTimeUutc = :endTimeUutc "
		// + "and tso.creator = :creator "
		// + "and tso.annotator = :annotator "
		// + (tsAnnOld.getDescription() == null
		// ? "and tso.description is null "
		// : "and tso.description = :description ")
		// + "and tso.type = :type ";
		//
		// Query query = sess
		// .createQuery(q)
		// .setParameter("parent", ds)
		// .setLong("startTimeUutc",
		// tsAnnOld.getStartTimeUutc())
		// .setLong("endTimeUutc", tsAnnOld.getEndTimeUutc())
		// .setLong("creator", tsAnnOld.getCreator())
		// .setString("annotator", tsAnnOld.getAnnotator())
		// .setString("type", tsAnnOld.getType());
		// if (tsAnnOld.getDescription() != null) {
		// query.setString("description",
		// tsAnnOld.getDescription());
		// }
		//
		// @SuppressWarnings("unchecked")
		// List<TsAnnotationOldEntity> matches =
		// (List<TsAnnotationOldEntity>) query.list();
		//
		// System.out.println("found " + (matches.size() - 1)
		// + " matches");
		// totalOld += matches.size();
		// Set<TimeSeriesEntity> annotated = newHashSet();
		//
		// // This will delete the original annotation too
		// for (TsAnnotationOldEntity match : matches) {
		// annotated.add(match.getAnnotated());
		// tsAnnOldDao.delete(match);
		// }
		//
		// System.out.println("added " + annotated.size()
		// + " time series");
		//
		// TsAnnotationEntity tsAnn =
		// new TsAnnotationEntity(
		// tsAnnOld.getType(),
		// tsAnnOld.getStartTimeUutc(),
		// tsAnnOld.getEndTimeUutc(),
		// annotated,
		// tsAnnOld.getAnnotator(),
		// userDao.getOrCreateUser(new UserId(tsAnnOld
		// .getCreator())),
		// tsAnnOld.getParent(),
		// TsAnnotationEntity.getLayerDefault(ds),
		// tsAnnOld.getDescription());
		// totalNew++;
		// tsAnnDao.saveOrUpdate(tsAnn);
		// sess.flush();
		// tsAnnDao.evict(tsAnn);
		// System.out.println("total old: " + totalOld);
		// System.out.println("total new: " + totalNew);
		// }
		// System.out.println("committing trx");
		// trx.commit();
		// }
		// } catch (Throwable t) {
		// t.printStackTrace();
		// PersistenceUtil.rollBackTrx(trx);
		// } finally {
		// PersistenceUtil.closeSession(sess);
		// }
	}
}
