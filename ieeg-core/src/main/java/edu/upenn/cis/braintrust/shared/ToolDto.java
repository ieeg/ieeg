/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

/**
 * A tool.
 * 
 * Description is always possibly not present.
 * <p>
 * Outgoing tools will have all values present, excepting description.
 * <p>
 * Incoming tools may (already saved) or may not (new tools) have an id.
 * <p>
 * Incoming tools may or may not have an author id, but will always have an
 * author name.
 * 
 * @author Sam Donnelly
 * @author Joost Wagenaar
 */
@GwtCompatible(serializable = true)
public final class ToolDto
		implements IHasOptionalStringId, Serializable {

	private static final long serialVersionUID = 1L;

	private Optional<String> id;
	private String label;
	private Optional<UserId> authorId;
	private String authorName;
	private Optional<String> description;
	private String sourceUrl;
	private String binUrl;

	/** For GWT. */
	@SuppressWarnings("unused")
	private ToolDto() {}

	public ToolDto(
			String label,
			@Nullable UserId authorId,
			String authorName,
			String sourceUrl,
			String binUrl,
			@Nullable String id,
			@Nullable String description) {
		this.label = checkNotNull(label);
		this.authorId = Optional.fromNullable(authorId);
		this.authorName = checkNotNull(authorName);
		this.sourceUrl = checkNotNull(sourceUrl);
		this.binUrl = checkNotNull(binUrl);
		this.id = Optional.fromNullable(id);
		this.description = Optional.fromNullable(description);
	}

	public Optional<UserId> getAuthorId() {
		return authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public String getBinUrl() {
		return binUrl;
	}

	public Optional<String> getDescription() {
		return description;
	}

	@Override
	public Optional<String> getId() {
		return id;
	}

	public String getLabel() {
		return label;
	}

	public ToolLoc getLocation() {
		// TODO Parse URL to find tool location
		return ToolLoc.GITHUB;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setAuthorName(String authorName) {
		this.authorName = checkNotNull(authorName);
	}

	public void setBinUrl(String binUrl) {
		this.binUrl = checkNotNull(binUrl);
	}

	public void setDescription(@Nullable String description) {
		this.description = Optional.fromNullable(description);
	}

	public void setId(String id) {
		this.id = Optional.of(id);
	}

	public void setLabel(String label) {
		this.label = checkNotNull(label);
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = checkNotNull(sourceUrl);
	}

	@Override
	public String toString() {
		return "ToolDto [label=" + label +
				", authorName=" + authorName + "]";
	}

}
