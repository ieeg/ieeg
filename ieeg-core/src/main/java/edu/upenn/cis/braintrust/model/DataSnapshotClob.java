/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
public class DataSnapshotClob {

	private Long id;
	private Integer version;
	private DataSnapshotEntity parent;
	private String value;

	// private boolean addsChannels = false;
	// private boolean addsAnnotations = false;
	// private boolean derivesData = false;
	// private DataSnapshotEntity supersededBy;
	// private String descriptor;
	// private Timestamp creation;
	// private Timestamp update;
	// private String creator;
	// private String url;

	/**
	 * For JPA.
	 */
	DataSnapshotClob() {}

	public DataSnapshotClob(
			final String value, 
			final DataSnapshotEntity parent) {
		this.value = checkNotNull(value);
		this.parent = checkNotNull(parent);
	}

	@Id
	public Long getId() {
		return id;
	}

	@MapsId
	// Really it's @OneToOne but we need to make it @ManyToOne since it's
	// @OneToMany on the other side
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "FK_DS_CLOB_PARENT")
	@JoinColumn(name = "parent_id")
	@NotNull
	public DataSnapshotEntity getParent() {
		return parent;
	}

	@Lob
	@NotNull
	public String getValue() {
		return value;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setParent(final DataSnapshotEntity parent) {
		this.parent = parent;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

	// @OneToOne
	// @JoinColumn(name="superseded_by")
	// @Nullable
	// public DataSnapshotEntity getSupersededBy() {
	// return supersededBy;
	// }
	//
	// public void setSupersededBy(DataSnapshotEntity supersededBy) {
	// this.supersededBy = supersededBy;
	// }
	//
	// public String getDescriptor() {
	// return descriptor;
	// }
	//
	// public void setDescriptor(String descriptor) {
	// this.descriptor = descriptor;
	// }
	//
	// public Timestamp getCreation() {
	// return creation;
	// }
	//
	// public void setCreation(Timestamp creation) {
	// this.creation = creation;
	// }
	//
	// public Timestamp getUpdate() {
	// return update;
	// }
	//
	// public void setUpdate(Timestamp update) {
	// this.update = update;
	// }

	// public String getCreator() {
	// return creator;
	// }
	//
	// public void setCreator(String creator) {
	// this.creator = creator;
	// }

	// public String getUrl() {
	// return url;
	// }
	//
	// public void setUrl(String url) {
	// this.url = url;
	// }

}
