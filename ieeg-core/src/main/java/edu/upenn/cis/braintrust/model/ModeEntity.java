/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.IHasName;

/**
 * * @author John Frommeyer
 * 
 */
@Entity(name = "Mode")
@Table(name = ModeEntity.TABLE,
		uniqueConstraints =
		@UniqueConstraint(columnNames = {
				ModeEntity.NAME_COLUMN, PermissionDomainEntity.ID_COLUMN }))
public class ModeEntity implements IHasLongId, IHasName {
	public static final String TABLE = "mode";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String NAME_COLUMN = "name";

	private Long id;
	private Integer version;
	private String name;
	private String description;
	private PermissionDomainEntity domain;
	private Set<PermissionEntity> permissions = newHashSet();

	// For JPA
	ModeEntity() {}

	/**
	 * This constructor sets both sides of the PermissionTarget/Mode
	 * relationship.
	 * 
	 * @param parent
	 * @param name
	 */
	public ModeEntity(
			PermissionDomainEntity parent,
			String name,
			@Nullable String description) {
		this.domain = checkNotNull(parent);
		this.domain.getModes().add(this);
		this.name = checkNotNull(name);
		this.description = description;

	}

	@Column
	@Size(max = 4000)
	public String getDescription() {
		return description;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = PermissionDomainEntity.ID_COLUMN)
	@NotNull
	public PermissionDomainEntity getDomain() {
		return domain;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(name = NAME_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getName() {
		return name;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "mode",
			orphanRemoval = true)
	@Size(min = 1)
	public Set<PermissionEntity> getPermissions() {
		return permissions;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setDescription(String description) {
		this.description = description;
	}

	@SuppressWarnings("unused")
	private void setDomain(PermissionDomainEntity domain) {
		this.domain = domain;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setPermissions(Set<PermissionEntity> permissions) {
		this.permissions = permissions;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
