/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name=ImageBookmarkEntity.TABLE)
public class ImageBookmarkEntity {
	public final static String TABLE = "image_bookmark";
	
	private Long bookmarkId;
	
	private DataSnapshotEntity snapshotId;
	
	private int imageMode;
	private String channelSelected;
	
	private int xOrigin;
	private int yOrigin;
	private int xWidth;
	private int yWidth;
	private int zCoordinate;
	
	public ImageBookmarkEntity() { }
	
	
	
	public ImageBookmarkEntity(DataSnapshotEntity snapshotId, int imageMode,
			String channelSelected, int xOrigin, int yOrigin, int xWidth,
			int yWidth, int zCoordinate) {
		super();
		this.snapshotId = snapshotId;
		this.imageMode = imageMode;
		this.channelSelected = channelSelected;
		this.xOrigin = xOrigin;
		this.yOrigin = yOrigin;
		this.xWidth = xWidth;
		this.yWidth = yWidth;
		this.zCoordinate = zCoordinate;
	}



	public ImageBookmarkEntity(Long bookmarkId, DataSnapshotEntity snapshotId,
			int imageMode, String channelSelected, int xOrigin, int yOrigin,
			int xWidth, int yWidth, int zCoordinate) {
		super();
		this.bookmarkId = bookmarkId;
		this.snapshotId = snapshotId;
		this.imageMode = imageMode;
		this.channelSelected = channelSelected;
		this.xOrigin = xOrigin;
		this.yOrigin = yOrigin;
		this.xWidth = xWidth;
		this.yWidth = yWidth;
		this.zCoordinate = zCoordinate;
	}



	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "bookmark_id", unique = true, nullable = false)
	public Long getBookmarkId() {
		return bookmarkId;
	}
	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "FK_BOOKMARK_SNAPSHOT_IMG")
	@JoinColumn(name = "snapshot_id")
	@NotNull
	public DataSnapshotEntity getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(DataSnapshotEntity snapshotId) {
		this.snapshotId = snapshotId;
	}

	public int getImageMode() {
		return imageMode;
	}

	public void setImageMode(int imageMode) {
		this.imageMode = imageMode;
	}

	public String getChannelSelected() {
		return channelSelected;
	}

	public void setChannelSelected(String channelSelected) {
		this.channelSelected = channelSelected;
	}

	public int getxOrigin() {
		return xOrigin;
	}

	public void setxOrigin(int xOrigin) {
		this.xOrigin = xOrigin;
	}

	public int getyOrigin() {
		return yOrigin;
	}

	public void setyOrigin(int yOrigin) {
		this.yOrigin = yOrigin;
	}

	public int getxWidth() {
		return xWidth;
	}

	public void setxWidth(int xWidth) {
		this.xWidth = xWidth;
	}

	public int getyWidth() {
		return yWidth;
	}

	public void setyWidth(int yWidth) {
		this.yWidth = yWidth;
	}

	public int getzCoordinate() {
		return zCoordinate;
	}

	public void setzCoordinate(int zCoordinate) {
		this.zCoordinate = zCoordinate;
	}


}
