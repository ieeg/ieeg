/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

/**
 * 
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
public class PatientSearch implements Serializable {

	private static final long serialVersionUID = 1L;

	private Set<SeizureType> szTypes = newHashSet();
	private Set<Gender> genders = newHashSet();
	private Boolean generalizedSzTypes;
	private Boolean partialSzTypes;
	private Integer ageOfOnsetMin;
	private Integer ageOfOnsetMax;

	public Integer getAgeOfOnsetMax() {
		return ageOfOnsetMax;
	}

	public Integer getAgeOfOnsetMin() {
		return ageOfOnsetMin;
	}

	public Set<Gender> getGenders() {
		return genders;
	}

	/**
	 * Get patients that have <em>any</em> of
	 * {@link SeizureType#GENERALIZED_SZ_TYPES}.
	 * 
	 * @return the generalizedSzTypes
	 */
	public Boolean getGeneralizedSzTypes() {
		return generalizedSzTypes;
	}

	/**
	 * Get patients that have <em>any</em> of
	 * {@link SeizureType#PARTIAL_SZ_TYPES}.
	 * 
	 * @return the partialSzTypes
	 */
	public Boolean getPartialSzTypes() {
		return partialSzTypes;
	}

	/**
	 * Get patients that have <em>all</em> of the seizure types.
	 * 
	 * @return the seizure types we're looking for in the patient
	 */
	public Set<SeizureType> getSzTypes() {
		return szTypes;
	}

	public void setAgeOfOnsetMax(@Nullable Integer ageOfOnsetMax) {
		this.ageOfOnsetMax = ageOfOnsetMax;
	}

	public void setAgeOfOnsetMin(@Nullable Integer ageOfOnsetMin) {
		this.ageOfOnsetMin = ageOfOnsetMin;
	}

	public void setGenders(final Set<Gender> genders) {
		this.genders = checkNotNull(genders);
	}

	/**
	 * @param generalizedSzTypes the generalizedSzTypes to set
	 */
	public void setGeneralizedSzTypes() {
		this.generalizedSzTypes = true;
	}

	/**
	 * @param partialSzSzTypes the partialSzSzTypes to set
	 */
	public void setPartialSzTypes() {
		this.partialSzTypes = true;
	}

	public void setSzTypes(final Set<SeizureType> szTypes) {
		this.szTypes = checkNotNull(szTypes);
	}

	@Override
	public String toString() {
		return "PatientSearch [szTypes=" + szTypes + ", genders=" + genders
				+ ", generalizedSzTypes=" + generalizedSzTypes
				+ ", partialSzTypes=" + partialSzTypes + ", ageOfOnsetMin="
				+ ageOfOnsetMin + ", ageOfOnsetMax=" + ageOfOnsetMax + "]";
	}

	public void unsetGeneralizedSzTypes() {
		this.generalizedSzTypes = null;
	}

	public void unsetPartialSzTypes() {
		this.partialSzTypes = null;
	}
}
