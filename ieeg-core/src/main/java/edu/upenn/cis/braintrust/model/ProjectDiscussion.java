/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.sql.Timestamp;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;

@Entity
@Table
@NamedQueries({
		@NamedQuery(
				name = ProjectDiscussion.FIND_BY_PROJECT,
				query = "from ProjectDiscussion pd "
						+ "where pd.project = :project "// and sd.refersTo is
														// null "
						+ "order by time desc"),

		// Useful for threads
		@NamedQuery(
				name = ProjectDiscussion.FIND_REPLY_TO,
				query = "from ProjectDiscussion pd "
						+ "where pd.project = :projectId and (pd.refersTo is not null and pd.refersTo.pubId = :pubId) "
						+ "order by time desc")
})
public class ProjectDiscussion implements IHasPubId {

	public static final String FIND_BY_PROJECT = "ProjectDiscussion-findByDataSetID";
	public static final String FIND_REPLY_TO = "ProjectDiscussion-findByDataSetReplyID";

	private Long discussId;

	private String pubId;
	private UserEntity user;
	private Integer version;
	private ProjectEntity project;
	private String title;
	private String posting;
	private Timestamp time;

	private ProjectDiscussion refersTo;

	/**
	 * For JPA.
	 */
	ProjectDiscussion() {
		pubId = newUuid();
	}

	public ProjectDiscussion(final String title,
			final String value,
			final ProjectEntity project,
			final UserEntity user,
			final ProjectDiscussion refs) {
		this.title = title;
		this.posting = value;
		this.project = project;

		this.user = user;
		this.refersTo = refs;
		if (pubId == null)
			pubId = newUuid();

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	public ProjectDiscussion(final String title,
			final String value,
			final ProjectEntity project,
			final UserEntity user,
			final ProjectDiscussion refs,
			final String id) {
		this.title = title;
		this.posting = value;
		this.project = project;
		this.pubId = id;

		this.user = user;
		this.refersTo = refs;
		if (pubId == null)
			pubId = newUuid();

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "post_id")
	public Long getDiscussId() {
		return discussId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_ps_discuss_proj")
	@JoinColumn(name = "project_id")
	@NotNull
	public ProjectEntity getProject() {
		return project;
	}

	@Lob
	@Column
	@NotNull
	public String getPosting() {
		return posting;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setDiscussId(final Long id) {
		this.discussId = id;
	}

	public void setProject(final ProjectEntity parent) {
		this.project = parent;
	}

	public void setPosting(final String value) {
		this.posting = value;
	}

	public void setVersion(final Integer version) {
		this.version = version;
	}

	@NotNull
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_proj_discuss_project_parent")
	@JoinColumn(name = "refers_id")
	public ProjectDiscussion getRefersTo() {
		return refersTo;
	}

	public void setRefersTo(ProjectDiscussion refers) {
		refersTo = refers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_project_discuss")
	@JoinColumn(name = "user_id")
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	/**
	 * @return the id
	 */
	@NaturalId
	@Override
	@Size(min = 36, max = 36)
	@NotNull
	public String getPubId() {
		return pubId;
	}

	public void setPubId(String id) {
		pubId = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
