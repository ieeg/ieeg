/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public enum Ethnicity {
	HISPANIC("Hispanic or Latino"), AMERICAN_INDIAN(
			"American Indian/Alaska Native"), ASIAN("Asian"), PACIFIC_ISLANDER(
			"Native Hawaiian or Other Pacific Islander"), BLACK(
			"Black or African American"), WHITE("White"), OTHER(
			"Other"), UNKNOWN("Unknown");
	private final String displayString;

	/**
	 * Maps a {@code Ethnicity}'s toString representation to the
	 * {@code Ethnicity} itself.
	 */
	public static final SortedMap<String, Ethnicity> fromString;
	static {
		final SortedMap<String, Ethnicity> temp = newTreeMap();
		for (final Ethnicity ethnicity : values()) {
			temp.put(ethnicity.toString(), ethnicity);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	private Ethnicity(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
