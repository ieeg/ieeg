/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.ExtPermissions;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.exception.AclTargetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;
import edu.upenn.cis.braintrust.shared.exception.JobNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TaskNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.RecordingObject;

/**
 * DAO-injectable {@link DataSnapshotServer} guts.
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public interface IDataSnapshotService {

	/**
	 * Logs an activity
	 * 
	 * @param user
	 * @param log
	 * @return
	 * @throws AuthorizationException
	 */
	public Long addLogEntry(
			final User user,
			final LogMessage log)
			throws AuthorizationException;

	/**
	 * Posts a message to the forum associated with the data snapshot
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @param post
	 * @return
	 * @throws AuthorizationException
	 * @throws DataSnapshotNotFoundException TODO
	 */
	public Long addPosting(
			final User user,
			final String dataSnapshotRevId,
			final Post post)
			throws AuthorizationException, DataSnapshotNotFoundException;

	public String addProjectPosting(User user, String projectId,
			Post post) throws AuthorizationException;

	/**
	 * Adds an entry describing data usage
	 * 
	 * @param user
	 * @param log
	 * @return
	 * @throws AuthorizationException
	 */
	public Long addProvenanceEntry(
			final User user,
			final ProvenanceLogEntry log)
			throws AuthorizationException;

	String addTimeSeriesToDataset(User user, String datasetRevId,
			Set<String> timeSeriesIds) throws AuthorizationException,
			DatasetNotFoundException;

	/**
	 * Returns the number of deleted sessions. Sessions older than
	 * {@minLastActive}.
	 * 
	 * @param minLastActive
	 * @return
	 */
	int cleanupOldSessions(Date minLastActive);

	void createDataUsage(DataUsageEntity dataUsage);

	void createJobForUser(
			UserId userId,
			String datasetId,
			String toolId,
			JobStatus status,
			ParallelDto parallelism,
			Date createTime);

	/**
	 * Defines a new project
	 * 
	 * @param user
	 * @param project
	 * @return
	 * @throws AuthorizationException
	 */
	public String createProject(
			final User user,
			final EegProject project)
			throws AuthorizationException;

	/**
	 * Returns a token for the newly created session for {@code user}.
	 * 
	 * @param user
	 * @return
	 */
	SessionToken createSession(User user);

	String createSnapshot(User user, String containerName,
			IHasGwtRecording recInfo);

	void createUserIfNecessary(User user);

	/**
	 * 
	 * 
	 * @param user
	 * @param sourceDataSnapshotRevId
	 * @param timeSeriesRevIds {@code null} means include all time series
	 * @param tsAnnotationRevIds {@code null} means include all time series
	 *            annotations
	 * @return the rev id of the derived dataset
	 * @throws AuthorizationException
	 * @throws DuplicateNameException
	 */
	String deriveDataset(
			User user,
			String sourceDataSnapshotRevId,
			String derivedDsRevIdLabel,
			String toolName,
			@Nullable Set<String> timeSeriesRevIds,
			@Nullable Set<String> tsAnnotationRevIds)
			throws DuplicateNameException;

	List<EditAclResponse> editAcl(
			User user,
			HasAclType entityType,
			String mode,
			List<IEditAclAction<?>> actions) throws AuthorizationException;

	GetAcesResponse getAcesResponse(
			User user,
			HasAclType entityType,
			String mode,
			String targetId);

	public Integer getAnnotationsByUser(
			final User user)
			throws AuthorizationException;

	String getClob(User user, String dsRevId)
			throws AuthorizationException;

	/**
	 * Returns the {@code DataSnapshot} for the given dataSnapshot revision id.
	 * 
	 * @param user the {@code UserId} of the user requesting the data snapshot
	 * @param dataSnapshotRevId the revision id of the desired data snapshot.
	 * 
	 * 
	 * @return the {@code DataSnapshot} for the given data snapshot revision id.
	 * 
	 * @throws AuthorizationException if the user does not have read access to
	 *             the data snapshot.
	 * @throws DataSnapshotNotFoundException if no DataSnapshot exists with the
	 *             given revision id.,
	 */
	DataSnapshot getDataSnapshot(
			User user,
			String dataSnapshotRevId) throws AuthorizationException;

	Set<DataSnapshotSearchResult> getDataSnapshots(
			User user,
			EegStudySearch dsSearch);

	EegStudyMetadata getEegStudyMetadata(User user, String studyId)
			throws AuthorizationException;

	ExperimentMetadata getExperimentMetadata(User user, String experimentId)
			throws AuthorizationException;

	Set<Image> getImages(User user, String dsId)
			throws AuthorizationException;

	/**
	 * Will accept a revision id or a stable id.
	 * 
	 * @param user
	 * @param dsStableId
	 * @return
	 * @throws AuthorizationException
	 */
	List<DataSnapshotSearchResult> getLatestSnapshots(
			User user,
			Iterable<String> dsStableIds)
			throws AuthorizationException;

	public List<LogMessage> getLogEntries(
			final User user,
			int startIndex,
			int count)
			throws AuthorizationException;

	public List<LogMessage> getLogEntriesSince(
			final User user,
			Timestamp sinceWhen)
			throws AuthorizationException;

	Map<String, TimeSeriesDto> getTimeSeries(User user, String dsId)
			throws AuthorizationException;

	/**
	 * Returns the path of the mef file with the given {@code traceRevId} as its
	 * revision id, or null if no such trace exists.
	 * 
	 * @param user the {@code UserId} of the user requesting the mef path
	 * @param dataSnapshotRevId the revision id of a data snapshot containing
	 *            the time series
	 * @param traceRevId the revision id of of the desired time series
	 * 
	 * @return the path of the mef file with the given {@code traceRevId} as its
	 *         revision id, or null if no such trace exists
	 * 
	 * @throws AuthorizationException if user does not have read access to the
	 *             data snapshot with revision id {@code dataSnapshotRevId}
	 */
	List<String> getMEFPaths(final User user, final String dsRevId,
			final Iterable<String> tsRevIds)
			throws AuthorizationException;

	List<Post> getPostings(final User user, final String dsRevId,
			int postIndex, int numPosts) throws AuthorizationException;

	public EegProject getProject(
			final User user,
			final String id)
			throws AuthorizationException;

	public List<Post> getProjectDiscussions(User user,
			String projectId, int postIndex, int numPosts)
			throws AuthorizationException;

	public Set<EegProject> getProjectsForUser(
			final User user)
			throws AuthorizationException;

	public List<ProvenanceLogEntry> getProvenanceEntriesByUser(
			final User user)
			throws AuthorizationException;

	public List<ProvenanceLogEntry> getProvenanceEntriesForSnapshot(
			User user,
			String dataSnapshotRevId) throws AuthorizationException;

	public List<ProvenanceLogEntry> getProvenanceEntriesForUser(
			final User user)
			throws AuthorizationException;

	public Set<DatasetAndTool> getRelatedAnalyses(
			User user,
			String studyRevId) throws AuthorizationException;

	public Integer getSnapshotsByUser(
			final User user)
			throws AuthorizationException;

	Set<ToolDto> getTools(User user);

	List<ToolDto> getTools(User user, Iterable<String> toolIds);

	String getUserClob(User user);

	/**
	 * Returns a {@code UserIdSessionStatus} with the userId and session status
	 * of the session with the given token. If the session's date of last
	 * activity is after {@code minLastActive} then session's last activity date
	 * is updated. Otherwise, the status is updated to {@code EXPIRED}.
	 * 
	 * @param token
	 * @return the {@code UserId} of the user who owns the active session with
	 *         the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 */
	UserIdSessionStatus getUserIdSessionStatus(SessionToken token,
			Date minLastActive);

	public List<UserId> getUserIdsWithActiveJobs();

	/**
	 * Adds {@code tasks} to the job working on the dataset with id equal to
	 * {@code dataSnapshotId}.
	 * 
	 * @param datasetId
	 * @param tasks
	 * @throws JobNotFoundException if there is no job associated with
	 *             {@code datasetId}
	 * @throws IllegalArgumentException if the job already has tasks associated
	 *             with it
	 */
	public void initializeJob(String datasetId, Set<TaskDto> tasks);

	public abstract boolean isFrozen(User user, String dsId)
			throws AuthorizationException;

	boolean isOwnerOfDataset(User user, String datasetId);

	/**
	 * Returns {@code true} if {@code recording} is modifiable, and {@code false}
	 * otherwise. Modifiable means that none of the recording's time series are
	 * shared with a {@link edu.upenn.cis.braintrust.model.Dataset}.
	 * 
	 * {@code recording} may be a {@link edu.upenn.cis.braintrust.model.Dataset} but it probably doesn't make sense.
	 * 
	 * @return {@code true} if {@code recording} is modifiable
	 */
	boolean isRecordingModifiable(DataSnapshotEntity recording);

	String removeTimeSeriesFromDataset(
			User user,
			String dataSnapshotRevId,
			Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException;

	void removeTools(User user, Set<String> revIds)
			throws AuthorizationException;

	/**
	 * Returns the revision id of the revised data snapshot.
	 * <p>
	 * For each {@code tsAnnotationRevId} in {@code tsAnnotationRevIds}, removes
	 * the annotation with revision id {@code tsAnnotationRevId} from the data
	 * snapshot with rev id {@code dataSnapshotRevId}.
	 * </p>
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @param tsAnnotationRevIds
	 * @return the revision id of the revised data snapshot
	 * @throws AuthorizationException if user does not have modify access to the
	 *             data snapshot with revision id {@code dataSnapshotRevId}
	 * @throws IllegalArgumentException if no data snapshot with the given rev
	 *             id exists
	 * @throws IllegalArgumentException if some {@code tsAnnotationRevId} is not
	 *             found in the database
	 */
	String removeTsAnnotations(final User user,
			final String dataSnapshotRevId,
			final Set<String> tsAnnotationRevIds)
			throws AuthorizationException;

	void setFrozen(User user, String dsId, boolean frozen)
			throws AuthorizationException;

	/**
	 * Sets the {@link SessionStatus} of the given session to {@code status}.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	void setSessionStatus(SessionToken token, SessionStatus status);

	String storeClob(User user, String studyRevId, String clob)
			throws AuthorizationException;

	List<String> storeTools(User user, Iterable<? extends ToolDto> tool)
			throws AuthorizationException;

	DataSnapshotIds storeTsAnnotations(
			User user,
			String dataSnapshotRevId,
			Iterable<? extends TsAnnotationDto> tsAnnDtos)
			throws
			AuthorizationException,
			TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException;

	void storeUserClob(User user, String clob);

	public void updateProject(
			final User admin,
			final String projectId,
			final IProjectUpdater updater)
			throws AuthorizationException;

	/**
	 * 
	 * Updates the status, runStartTime, and worker of each passed in task.
	 * 
	 * @param user
	 * @param datasetId
	 * @param taskSet * @throws JobNotFoundException if there is no job
	 *            associated with {@code datasetId} for the given {@code user}
	 * @throws TaskNotFoundException if some task in {@code taskSet} cannot be
	 *             found for the given job
	 */
	void updateTasks(User user, String datasetId, Set<TaskDto> taskSet);

	/**
	 * Updates the status of task {@code taskId} in job {@code datasetId} to
	 * {@code newStatus}.
	 * 
	 * @param user the user that started the job
	 * 
	 * @throws JobNotFoundException if there is no job associated with
	 *             {@code datasetId} for the given {@code user}
	 * @throws TaskNotFoundException if there is no task with {@code taskId}
	 */
	void updateTaskStatus(User user, String datasetId,
			String taskId, TaskStatus newStatus);

	UserEntity getUser(UserId userId);

	/**
	 * Returns the set of defined annotations and their schemas
	 * 
	 * @param user
	 * @param viewerType the viewer for whom we are looking up annotations
	 * @return
	 */
	public List<AnnotationDefinition> getAnnotationDefinitions(User user,
			String viewerType);

	/**
	 * Returns a {@code ExtPermissions} for the given user and instance.
	 * 
	 * @param user
	 * @param entityType the entity type of the instance
	 * @param mode
	 * @param targetId
	 * @param useDatasetShortCache TODO
	 * @return a {@code ExtPermissions} for the given user and instance
	 * @throws AclTargetNotFoundException if there is no {@code entity} instance
	 *             with natural id equal to {@code targetId}
	 */
	public ExtPermissions getPermissions(User user, HasAclType entityType,
			String mode, String targetId, boolean useDatasetShortCache);

	/**
	 * Returns the snapshot ID for a particular snapshot based on its name...
	 * unless we don't have permissions. Returns null for doesn't exist.
	 * 
	 * @param user
	 * @param ssName Snapshot ID, null if not found
	 * @return
	 */
	public String getSnapshotIDFromName(
			final User user,
			final String ssName);

	public List<ChannelInfoDto> writeChannelInfos(String snapshotId,
			List<ChannelInfoDto> info);

	public List<ChannelInfoDto> getChannelInfos(String dataSnapshotId);

	String storeJsonKeyValues(User user, String dataSnapshotRevId,
			Collection<? extends IJsonKeyValue> keyValues)
			throws AuthorizationException,
			JsonProcessingException;

	Set<IJsonKeyValue> getJsonKeyValues(User user, String dataSnapshotId)
			throws AuthorizationException, IOException;

	public String addMEFPath(User user, String mefPath,
			ChannelInfoDto chInfoDto);

	public String addTimeSeriesToSnapshot(final User user,
			final String datasetRevId,
			final List<ChannelInfoDto> channelInfos,
			final List<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException;

	void setAdmissionInfo(User user, String snapshotId, GwtPatient origPatient,
			int ageAtAdmission) throws AuthorizationException,
			DatasetNotFoundException;

	OrganizationEntity getOrganization(String orgName, String code);

	public List<EEGMontage> getMontages(User user, String snapshotID);

	public Set<DataSnapshotSearchResult> getDataSnapshotsForUser(User user);

	Set<DataSnapshotSearchResult> getDataSnapshotsNoCounts(User user, EegStudySearch studySearch);

	public String getParentSnapshot(User authenticatedUser, String studyRevId);

	public DataSnapshotSearchResult getDataSnapshotForId(User user, String snapshotID);

//	IStoredObjectReference getRecordingObjectHandle(User user, RecordingObject object)
//			throws RecordingObjectNotFoundException;
}
