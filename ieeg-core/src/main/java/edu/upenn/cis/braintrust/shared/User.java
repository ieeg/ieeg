/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;

import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * A user.
 * 
 * @author Sam Donnelly
 */
@Immutable
@GwtCompatible(serializable = true)
public final class User implements IUser, Serializable, JsonTyped {

	private UserId userId;
	private String username;
	private String password;
	private Optional<String> email;
	private String photo;
	private Status status;
	private Set<Role> roles;
	private UserProfile profile;

	public User(
			final UserId userId,
			final String username,
			final String password,
			final Status status,
			final Set<Role> roles,
			@Nullable final String photo,
			@Nullable final String email,
			UserProfile userProfile) {
		this.userId = checkNotNull(userId);
		this.username = checkNotNull(username);
		this.password = checkNotNull(password);
		this.status = checkNotNull(status);
		this.roles = ImmutableSet.copyOf(checkNotNull(roles));
		this.photo = (photo == null) ? "" : photo;
		this.email = Optional.fromNullable(email);
		this.profile = checkNotNull(userProfile);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (!Objects.equal(userId, other.userId)) {
			return false;
		}
		return true;
	}

	public Optional<String> getEmail() {
		return email;
	}

	/**
	 * @return the pass
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the path to the photo, blank string if none
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * This collection should not be modified.
	 * 
	 * @return the roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @return the status
	 */
	@Override
	public Status getStatus() {
		return status;
	}

	/**
	 * @return the uid
	 */
	public UserId getUserId() {
		return userId;
	}

	/**
	 * @return the name
	 */
	public String getUsername() {
		return username;
	}

	public UserProfile getProfile() {
		return profile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	public boolean isAdmin() {
		if (getRoles().contains(Role.ADMIN)) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "User{" +
				"email=" + email +
				", userId=" + userId +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", photo='" + photo + '\'' +
				", status=" + status +
				", roles=" + roles +
				", profile=" + profile +
				'}';
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(Optional<String> email) {
		this.email = email;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}

}
