/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.provenance.hibernate;

import java.util.List;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.provenance.IProvenanceDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class ProvenanceDAOHibernate extends
		GenericHibernateDAO<SnapshotUsage, Long>
		implements IProvenanceDAO {
	public ProvenanceDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public List<SnapshotUsage> findAllProps(DataSnapshotEntity entity) {
		@SuppressWarnings("unchecked")
		List<SnapshotUsage> l = getSession()
				.createQuery(
						"select s from SnapshotUsage s "
								+ "where s.snapshot = :snap "
								+ "order by s.time desc")
				.setParameter("snap", entity)
				.list();

		return l;
	}

	@Override
	public List<SnapshotUsage> findRelevantToUser(UserEntity user) {
		@SuppressWarnings("unchecked")
		List<SnapshotUsage> l = getSession()
				.createQuery(
						"from SnapshotUsage u "
								+ "where exists (from Dataset ds where ds = u.snapshot and ds.creator = :user) "
								+ "order by u.time desc")
				.setParameter("user", user)
				.list();

		return l;
	}

	@Override
	public List<SnapshotUsage> findCreatedByUser(UserEntity user) {
		@SuppressWarnings("unchecked")
		List<SnapshotUsage> l = getSession()
				.createQuery(
						"select u from SnapshotUsage u "
								+ "where u.userId = :user "
								+ "order by u.time desc")
				.setParameter("user", user)
				.list();

		return l;
	}
}
