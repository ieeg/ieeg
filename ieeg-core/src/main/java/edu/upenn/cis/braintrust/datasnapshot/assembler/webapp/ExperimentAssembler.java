/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.dto.GwtStimType;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class ExperimentAssembler {

	private final RecordingAssembler recordingAssembler;
	private final IStimRegionDAO stimRegionDAO;
	private final IStimTypeDAO stimTypeDAO;
	private final IPermissionDAO permissionDAO;
	private final StimRegionAssembler stimRegionAssembler = new StimRegionAssembler();
	private final StimTypeAssembler stimTypeAssembler = new StimTypeAssembler();

	public ExperimentAssembler(
			IStimRegionDAO stimRegionDAO,
			IStimTypeDAO stimTypeDAO,
			IPermissionDAO permissionDAO,
			ITsAnnotationDAO annotationDAO) {
		this.stimRegionDAO = checkNotNull(stimRegionDAO);
		this.stimTypeDAO = checkNotNull(stimTypeDAO);
		this.permissionDAO = checkNotNull(permissionDAO);
		this.recordingAssembler = new RecordingAssembler(annotationDAO);
	}

	public GwtExperiment buildDto(final ExperimentEntity experiment) {
		checkNotNull(experiment);
		final GwtStimType gwtStimType = experiment.getStimType() == null ? null
				: stimTypeAssembler.buildDto(experiment.getStimType());
		final GwtStimRegion gwtStimRegion = experiment.getStimRegion() == null ? null
				: stimRegionAssembler
						.buildDto(experiment.getStimRegion());
		final PermissionEntity readPerm = permissionDAO.findStarCoreReadPerm();
		final GwtExperiment gwtExperiment = new GwtExperiment(
				experiment.getLabel(),
				recordingAssembler.buildDto(experiment.getRecording()),
				experiment.getExtAcl().getWorldAce().contains(readPerm),
				experiment.getAge(),
				gwtStimType,
				gwtStimRegion,
				experiment.getStimIsiMs(),
				experiment.getStimDurationMs(),
				experiment.getId(), experiment.getVersion());
		return gwtExperiment;
	}

	public ExperimentEntity buildEntity(AnimalEntity parent,
			GwtExperiment gwtExperiment) {
		checkNotNull(gwtExperiment);
		checkArgument(gwtExperiment.getId() == null);
		checkArgument(gwtExperiment.getVersion() == null);

		final StimTypeEntity stimType = findStimTypeOrNull(gwtExperiment
				.getStimType());
		final StimRegionEntity stimRegion = findStimRegionOrNull(gwtExperiment
				.getStimRegion());

		ExtAclEntity acl = new ExtAclEntity();
		if (gwtExperiment.isPublished()) {
			final PermissionEntity readPerm = permissionDAO
					.findStarCoreReadPerm();
			acl.getWorldAce().add(readPerm);
		}
		final ExperimentEntity experiment = new ExperimentEntity(
				parent,
				gwtExperiment.getLabel(),
				acl,
				recordingAssembler.buildEntity(gwtExperiment.getRecording()),
				gwtExperiment.getAge(),
				stimType,
				stimRegion,
				gwtExperiment.getStimIsiMs(),
				gwtExperiment.getStimDurationMs(), null);
		return experiment;
	}

	private void copyToEntity(final ExperimentEntity experiment,
			final GwtExperiment gwtExperiment) {
		experiment.setLabel(gwtExperiment.getLabel());
		experiment.setAge(gwtExperiment.getAge());
		final StimTypeEntity stimTypeEntity = findStimTypeOrNull(gwtExperiment
				.getStimType());
		experiment.setStimType(stimTypeEntity);
		final StimRegionEntity stimRegionEntity = findStimRegionOrNull(gwtExperiment
				.getStimRegion());
		experiment.setStimRegion(stimRegionEntity);
		experiment.setStimIsiMs(gwtExperiment.getStimIsiMs());
		experiment.setStimDurationMs(gwtExperiment.getStimDurationMs());
		final PermissionEntity readPerm = permissionDAO.findStarCoreReadPerm();
		if (gwtExperiment.isPublished()) {
			experiment.getExtAcl().getWorldAce().add(readPerm);
		} else {
			experiment.getExtAcl().getWorldAce().clear();
		}
	}

	public void modifyEntity(final ExperimentEntity experiment,
			final GwtExperiment gwtExperiment,
			boolean modifiable)
			throws StaleObjectException, BrainTrustUserException {
		final Long gwtId = gwtExperiment.getId();
		final Long dbId = experiment.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtExperiment.getVersion();
		final Integer dbVer = experiment.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for experiment ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		recordingAssembler.modifyEntity(experiment,
				experiment.getRecording(), gwtExperiment.getRecording(),
				modifiable);
		copyToEntity(experiment, gwtExperiment);

	}

	private StimRegionEntity findStimRegionOrNull(GwtStimRegion gwtStimRegion) {
		if (gwtStimRegion == null) {
			return null;
		} else {
			final Long id = gwtStimRegion.getId();
			final StimRegionEntity stimRegionEntity = stimRegionDAO.findById(
					id, false);
			if (stimRegionEntity == null) {
				throw new IllegalArgumentException(
						"Stimulation region with id " + id
								+ " cannot be found in database");
			}
			return stimRegionEntity;

		}
	}

	private StimTypeEntity findStimTypeOrNull(GwtStimType gwtStimType) {
		if (gwtStimType == null) {
			return null;
		} else {
			final Long id = gwtStimType.getId();
			final StimTypeEntity stimTypeEntity = stimTypeDAO.findById(
					id, false);
			if (stimTypeEntity == null) {
				throw new IllegalArgumentException(
						"Stimulation type with id " + id
								+ " cannot be found in database");
			}
			return stimTypeEntity;

		}
	}
}
