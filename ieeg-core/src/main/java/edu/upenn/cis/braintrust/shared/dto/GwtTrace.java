/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Comparator;

import javax.annotation.Nullable;

import com.google.common.collect.ComparisonChain;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;

public class GwtTrace implements Serializable, IHasLongId, IHasVersion,
		IHasLabel {

	/**
	 * Compare GwtTraces by label. Warning: this is not compatible with equals.
	 */
	public static final Comparator<GwtTrace> LABEL_COMPARATOR = new Comparator<GwtTrace>() {

		@Override
		public int compare(GwtTrace o1, GwtTrace o2) {
			return ComparisonChain
					.start()
					.compare(o1.getLabel(), o2.getLabel())
					.compare(o1.getId(), o2.getId())
					.result();
		}
	};

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer version;
	private Long contactId;
	private Integer contactVersion;
	private ContactType contactType;
	private String label;
	private String fileKey;

	@SuppressWarnings("unused")
	private GwtTrace() {}

	public GwtTrace(
			ContactType contactType,
			String label,
			String fileKey,
			@Nullable Long contactId,
			@Nullable Integer contactVersion,
			@Nullable Long id,
			@Nullable Integer version) {
		this.id = id;
		this.version = version;
		this.contactId = contactId;
		this.contactVersion = contactVersion;
		this.contactType = checkNotNull(contactType);
		this.label = checkNotNull(label);
		this.fileKey = checkNotNull(fileKey);
	}

	/**
	 * @return the time series label
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/**
	 * @return the time series version
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/**
	 * @return the time series id
	 */
	@Override
	public Long getId() {
		return id;
	}

	public Long getContactId() {
		return contactId;
	}

	public Integer getContactVersion() {
		return contactVersion;
	}

	public ContactType getContactType() {
		return contactType;
	}

	public String getFileKey() {
		return fileKey;
	}

}
