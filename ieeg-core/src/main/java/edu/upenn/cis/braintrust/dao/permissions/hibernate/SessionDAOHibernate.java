/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.permissions.hibernate;

import java.util.Date;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.model.SessionEntity;

/**
 * @author John Frommeyer
 */
public class SessionDAOHibernate
		extends GenericHibernateDAOLongId<SessionEntity>
		implements ISessionDAO {
	public SessionDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public int deleteSessionsOlderThan(Date minLastAccess) {
		return getSession().getNamedQuery(SessionEntity.DELETE_OLD_SESSIONS)
				.setTimestamp("minLastAccess", minLastAccess)
				.executeUpdate();
	}
	
	public SessionEntity findToken(String sessionID) {
		return (SessionEntity) getSession().getNamedQuery(SessionEntity.SESSION_TOKEN)
				.setString("token", sessionID).uniqueResult();
	}
}
