/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStrainDAO;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class AnimalAssembler {
	private final OrganizationAssembler organizationAssembler;
	private final StrainAssembler strainAssembler;
	private final IOrganizationDAO organizationDAO;
	private final IStrainDAO strainDAO;

	public AnimalAssembler(IOrganizationDAO organizationDAO,
			IStrainDAO strainDAO) {
		this.organizationDAO = checkNotNull(organizationDAO);
		this.strainDAO = checkNotNull(strainDAO);
		strainAssembler = new StrainAssembler();
		organizationAssembler = new OrganizationAssembler();
	}

	public GwtAnimal buildDto(final AnimalEntity animal) {
		final StrainEntity strain = animal.getStrain();
		final SpeciesEntity species = strain.getParent();
		final GwtAnimal gwtAnimal =
				new GwtAnimal(
						animal.getLabel(),
						organizationAssembler.buildDto(animal.getOrganization()),
						strainAssembler.buildDto(
								strain,
								species.getLabel(),
								species.getId()),
						animal.getId(),
						animal.getVersion());
		return gwtAnimal;
	}

	public AnimalEntity buildEntity(
			final GwtAnimal gwtAnimal) {
		checkNotNull(gwtAnimal);
		checkArgument(gwtAnimal.getId() == null);
		checkArgument(gwtAnimal.getVersion() == null);
		final OrganizationEntity organization = organizationDAO.findById(
				gwtAnimal.getOrganization().getId(), false);
		final Long strainId = gwtAnimal.getStrain().getId();
		final StrainEntity strain = strainDAO.findById(strainId, false);
		final AnimalEntity patient = new AnimalEntity(
				gwtAnimal.getLabel(),
				organization,
				strain);
		return patient;
	}

	public void modifyEntity(
			final AnimalEntity animal,
			final GwtAnimal gwtAnimal) throws StaleObjectException,
			BrainTrustUserException {
		checkNotNull(animal);
		checkNotNull(gwtAnimal);
		final Long gwtId = gwtAnimal.getId();
		final Long dbId = animal.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtAnimal.getVersion();
		final Integer dbVer = animal.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for animal ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		copyToEntity(animal, gwtAnimal);
	}

	private void copyToEntity(final AnimalEntity animal,
			final GwtAnimal gwtAnimal) {
		final Long organizationId = gwtAnimal.getOrganization().getId();
		final OrganizationEntity organization = organizationDAO.findById(
				organizationId, false);
		animal.setOrganization(organization);
		animal.setLabel(gwtAnimal.getLabel());
		final Long strainId = gwtAnimal.getStrain().getId();
		final StrainEntity strain = strainDAO.findById(strainId, false);
		animal.setStrain(strain);
	}

}
