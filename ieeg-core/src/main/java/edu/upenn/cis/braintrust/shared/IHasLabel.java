/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.util.Comparator;

import com.google.common.base.Function;

public interface IHasLabel {

	public static final Function<IHasLabel, String> getLabel = new Function<IHasLabel, String>() {

		@Override
		public String apply(final IHasLabel input) {
			return input.getLabel();
		}
	};

	/**
	 * Uses String's Comparator on labels. Warning: This Comparator is not
	 * consistent with equals.
	 */
	public static final Comparator<IHasLabel> LABEL_COMPARATOR = new Comparator<IHasLabel>() {

		@Override
		public int compare(IHasLabel arg0, IHasLabel arg1) {
			return arg0.getLabel().compareTo(arg1.getLabel());
		}
	};

	String getLabel();
}
