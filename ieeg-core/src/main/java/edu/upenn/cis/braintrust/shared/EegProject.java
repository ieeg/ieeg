/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Sets;

@GwtCompatible(serializable = true)
public class EegProject implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long projectId = null;

	private String pubId;

	private String name;

	private Set<String> tags = Sets.newHashSet();

	private Set<String> credits = Sets.newHashSet();

	private Set<UserId> admins = Sets.newHashSet();

	private Set<UserId> team = Sets.newHashSet();

	private Set<String> snapshots = Sets.newHashSet();

	private Set<String> tools = Sets.newHashSet();

	private boolean complete = false;

	public EegProject() {

	}

	public EegProject(
			final String pubId,
			final String name,
			final Set<String> tags,
			final Set<String> credits,
			boolean isComplete) {
		this.projectId = null;
		this.pubId = pubId;
		this.name = name;
		this.tags = Sets.newHashSet(tags);
		this.credits = Sets.newHashSet(credits);
	}

	public EegProject(
			final Long id,
			final String pubId,
			final String name,
			final Set<String> tags,
			final Set<String> credits,
			boolean isComplete) {
		this.projectId = id;
		this.pubId = pubId;
		this.name = name;
		this.tags = Sets.newHashSet(tags);
		this.credits = Sets.newHashSet(credits);
	}
	
	public EegProject(
			final Long id,
			final String pubId,
			final String name,
			final Set<String> tags,
			final Set<String> credits,
			boolean isComplete,
			final Set<UserId> admins,
			final Set<UserId> team,
			final Set<String> datasetIds,
			final Set<String> toolIds) {
		this.projectId = id;
		this.pubId = pubId;
		this.name = name;
		this.tags = Sets.newHashSet(tags);
		this.credits = Sets.newHashSet(credits);
		this.admins = newHashSet(admins);
		this.team = newHashSet(team);
		this.snapshots = newHashSet(datasetIds);
		this.tools = newHashSet(toolIds);
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getPubId() {
		return pubId;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Set<String> getCredits() {
		return credits;
	}

	public void setCredits(Set<String> credits) {
		this.credits = credits;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public Set<UserId> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<UserId> admins) {
		this.admins = admins;
	}

	public Set<UserId> getTeam() {
		return team;
	}

	public void setTeam(Set<UserId> team) {
		this.team = team;
	}

	public Set<String> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<String> snapshots) {
		this.snapshots = snapshots;
	}

	public Set<String> getTools() {
		return tools;
	}

	public void setTools(Set<String> tools) {
		this.tools = tools;
	}

}
