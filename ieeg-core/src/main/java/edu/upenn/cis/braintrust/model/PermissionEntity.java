/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.IHasName;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * * @author John Frommeyer
 * 
 */
@NamedQueries({
		@NamedQuery(name = PermissionEntity.BY_DOMAIN_MODE_AND_NAME,
				query = "select p "
						+ "from Permission p "
						+ "left join fetch p.mode mode "
						+ "left join fetch mode.domain domain "
						+ "where p.name = :name "
						+ "and mode.name = :mode "
						+ "and domain.name = :domain"),
		@NamedQuery(name = PermissionEntity.BY_DOMAIN_AND_MODE,
				query = "select p "
						+ "from Permission p "
						+ "left join fetch p.mode mode "
						+ "left join fetch mode.domain domain "
						+ "where mode.name = :mode "
						+ "and domain.name in (:domain, '"
						+ PermissionDomainEntity.PERM_DOMAIN_WILDCARD + "') "
						+ "order by p.displayOrder ") })
@Entity(name = "Permission")
@Table(name = PermissionEntity.TABLE,
		uniqueConstraints =
		{
				@UniqueConstraint(columnNames = {
						PermissionEntity.NAME_COLUMN,
						ModeEntity.ID_COLUMN }),
				@UniqueConstraint(columnNames = {
						PermissionEntity.DISPLAY_STR_COLUMN,
						ModeEntity.ID_COLUMN }),
				@UniqueConstraint(columnNames = {
						PermissionEntity.DISPLAY_ORDER_COLUMN,
						ModeEntity.ID_COLUMN }) })
public class PermissionEntity implements IHasLongId, IHasName, IEntity {
	@Transient
	private static PermissionDomainEntity domain =
			new PermissionDomainEntity(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD);
	@Transient
	private static ModeEntity modeEntity =
			new ModeEntity(domain, CorePermDefs.CORE_MODE_NAME, null);


	public static Function<PermissionEntity, String> getModeName = new Function<PermissionEntity, String>() {

		@Override
		public String apply(PermissionEntity input) {
			return input.getMode().getName();
		}
	};

	public static Function<PermissionEntity, String> getDomainName = new Function<PermissionEntity, String>() {

		@Override
		public String apply(PermissionEntity input) {
			return input.getMode().getDomain().getName();
		}
	};

	public static final String TABLE = "permission";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String NAME_COLUMN = "name";
	public static final String DISPLAY_STR_COLUMN = "display_string";
	public static final String DISPLAY_ORDER_COLUMN = "display_order";

	public static final String BY_DOMAIN_MODE_AND_NAME = "Permission-findByDomainModeAndName";
	public static final String BY_DOMAIN_AND_MODE = "Permission-findByDomainAndMode";

	private Long id;
	private Integer version;
	private String name;
	private String displayString;
	private Integer displayOrder;
	private Boolean worldPerm;
	private Boolean projectGroupPerm;
	private Boolean userPerm;
	private String description;
	private ModeEntity mode;

	// For JPA
	public PermissionEntity() {}

	/**
	 * This constructor sets both sides of the Mode/Permission relationship.
	 * 
	 * @param parent
	 * @param name
	 * @param displayString
	 * @param displayOrder
	 * @param worldPerm
	 * @param projectGroupPerm
	 * @param userPerm
	 */
	public PermissionEntity(
			ModeEntity parent,
			String name,
			String displayString,
			Integer displayOrder,
			Boolean worldPerm,
			Boolean projectGroupPerm,
			Boolean userPerm,
			@Nullable String description) {
		this.mode = checkNotNull(parent);
		this.mode.getPermissions().add(this);
		this.name = checkNotNull(name);
		this.displayString = checkNotNull(displayString);
		this.displayOrder = checkNotNull(displayOrder);
		this.worldPerm = checkNotNull(worldPerm);
		this.projectGroupPerm = checkNotNull(projectGroupPerm);
		this.userPerm = checkNotNull(userPerm);

		this.description = description;

	}

	@Column
	@Size(max = 4000)
	public String getDescription() {
		return description;
	}

	@Column(name = DISPLAY_ORDER_COLUMN)
	@NotNull
	@VisibleForTesting
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	@Column(name = DISPLAY_STR_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getDisplayString() {
		return displayString;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(name = NAME_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getName() {
		return name;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = ModeEntity.ID_COLUMN)
	@NotNull
	public ModeEntity getMode() {
		return mode;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@NotNull
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean isWorldPerm() {
		return worldPerm;
	}

	@NotNull
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean isProjectGroupPerm() {
		return projectGroupPerm;
	}

	@NotNull
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean isUserPerm() {
		return userPerm;
	}

	@SuppressWarnings("unused")
	private void setDescription(String description) {
		this.description = description;
	}

	@SuppressWarnings("unused")
	private void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	@SuppressWarnings("unused")
	private void setDisplayString(String displayString) {
		this.displayString = displayString;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setMode(ModeEntity mode) {
		this.mode = mode;
	}

	@SuppressWarnings("unused")
	private void setWorldPerm(Boolean isWorldPerm) {
		this.worldPerm = isWorldPerm;
	}

	@SuppressWarnings("unused")
	private void setProjectGroupPerm(Boolean isProjectGroupPerm) {
		this.projectGroupPerm = isProjectGroupPerm;
	}

	@SuppressWarnings("unused")
	private void setUserPerm(Boolean isUserPerm) {
		this.userPerm = isUserPerm;
	}

	@VisibleForTesting
	public void setVersion(Integer version) {
		this.version = version;
	}

	public static final EntityConstructor<PermissionEntity, String, Void> CONSTRUCTOR = 
		new EntityConstructor<PermissionEntity,String,Void>(PermissionEntity.class,
				new IPersistentObjectManager.IPersistentKey<PermissionEntity,String>() {

			@Override
			public String getKey(PermissionEntity o) {
				return o.getName();
			}

			@Override
			public void setKey(PermissionEntity o, String newKey) {
				o.setName(newKey);
			}

		},
		new EntityPersistence.ICreateObject<PermissionEntity,String,Void>() {
			@Override
			public PermissionEntity create(String id, Void optParent) {
				return new PermissionEntity(modeEntity, id, id, -1, true, false, false, id);
			}
		}
				);

		@Override
	public EntityConstructor<PermissionEntity, String, Void> tableConstructor() {
		return CONSTRUCTOR;
	}

}
