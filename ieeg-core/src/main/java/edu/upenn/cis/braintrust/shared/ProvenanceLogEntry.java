/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class ProvenanceLogEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum UsageType {
		CREATES, ADDSTO, VIEWS, ANNOTATES, DERIVES, DISCUSSES, FAVORITES, SHARES
	};

//	private UserId userId;
	private String userId;
	private UsageType usage;
	private String snapshot;
	private Timestamp time;
	private String derivedSnapshot;

	private String annotation;
	
	public ProvenanceLogEntry() {}

	public ProvenanceLogEntry(String userId, UsageType usage,
			String snapshot, Timestamp time, @Nullable String derivedSnapshot,
			@Nullable String annotation) {
		super();
		this.userId = userId;
		this.usage = usage;
		this.snapshot = snapshot;
		this.time = time;
		this.derivedSnapshot = derivedSnapshot;
		this.annotation = annotation;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public UsageType getUsage() {
		return usage;
	}

	public void setUsage(UsageType usage) {
		this.usage = usage;
	}

	public String getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(String snapshot) {
		this.snapshot = snapshot;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getDerivedSnapshot() {
		return derivedSnapshot;
	}

	public void setDerivedSnapshot(String derivedSnapshot) {
		this.derivedSnapshot = derivedSnapshot;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

}
