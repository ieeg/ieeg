/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SimpleNaturalIdLoadAccess;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotJsonDAO;
import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataSnapshotJson;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValue;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

/**
 * JSON list of types
 * 
 * @author zives
 *
 */
public class DataSnapshotJsonDAOHibernate 
	extends GenericHibernateDAO<DataSnapshotJson, Long>
	implements IDataSnapshotJsonDAO {
	
	JsonKeyValueMapper theMapper;

	public DataSnapshotJsonDAOHibernate(Session session) {
		super.setSession(session);
		
		theMapper = JsonKeyValueMapper.getMapper();
	}
	
	/**
	 * Look up the parent DataSnapshotEntity based on the pub ID
	 * 
	 * @param snapshotId
	 * @return
	 */
	protected DataSnapshotEntity getParentEntity(String snapshotId) {
		SimpleNaturalIdLoadAccess la =
				getSession().bySimpleNaturalId(DataSnapshotEntity.class);
		return (DataSnapshotEntity) la.load(snapshotId);
	}

	protected DataSnapshotJson getClob(String snapshotId) {
		DataSnapshotJson clob = (DataSnapshotJson) getSession().
				getNamedQuery(DataSnapshotJson.BY_SNAPSHOT_ID).
				setParameter("pubId", snapshotId).
				uniqueResult();

		if (clob == null) {
			clob = new DataSnapshotJson("", getParentEntity(snapshotId));
		}
		return clob;
	}

	

	@Override
	public Set<IJsonKeyValue> getAllKeyValues(String snapshotId) throws JsonProcessingException, IOException {
		checkNotNull(snapshotId);
		DataSnapshotJson clob = (DataSnapshotJson) getSession().
				getNamedQuery(DataSnapshotJson.BY_SNAPSHOT_ID).
				setParameter("pubId", snapshotId).
				uniqueResult();

		if (clob == null || clob.getValue() == null)
			return new HashSet<IJsonKeyValue>();
		return theMapper.getKeyValueSet(clob.getValue());
	}

	@Override
	public void addKeyValues(String snapshotId, Collection<? extends IJsonKeyValue> triples) throws IOException {
		checkNotNull(snapshotId);
		checkNotNull(triples);
		DataSnapshotJson clob = getClob(snapshotId);

		Set<IJsonKeyValue> tset = theMapper.getKeyValueSet(clob.getValue());
		if (tset == null) {
			tset = new HashSet<IJsonKeyValue>();
		}
		tset.addAll(triples);
		clob.setValue(theMapper.getKeyValueSet(tset));
		
		getSession().save(clob);
	}
	
	@Override
	public void setKeyValues(String snapshotId, Collection<? extends IJsonKeyValue> triples) throws JsonProcessingException {
		checkNotNull(snapshotId);
		checkNotNull(triples);
		DataSnapshotJson clob = getClob(snapshotId);
		clob.setValue(theMapper.getKeyValueSet(triples));
		
		getSession().save(clob);
	}

	@Override
	public void addKeyValue(String snapshotId, IJsonKeyValue triple) throws JsonProcessingException, IOException {
		checkNotNull(snapshotId);
		checkNotNull(triple);
		DataSnapshotJson clob = getClob(snapshotId);

		Set<IJsonKeyValue> tset = theMapper.getKeyValueSet(clob.getValue());
		if (tset == null) {
			tset = new HashSet<IJsonKeyValue>();
		}
		tset.add(triple);
		clob.setValue(theMapper.getKeyValueSet(tset));
		
		getSession().save(clob);
	}

	@Override
	public void removeKey(String snapshotId, String key) throws JsonProcessingException, IOException {
		checkNotNull(snapshotId);
		checkNotNull(key);
		DataSnapshotJson clob = getClob(snapshotId);

		Set<IJsonKeyValue> tset = theMapper.getKeyValueSet(clob.getValue());
		
		IJsonKeyValue toDelete = null;
		for (IJsonKeyValue t: tset)
			if (t.getKey().equals(key)) {
				toDelete = t;
				break;
			}
		if (toDelete != null) {
			clob.setValue(theMapper.getKeyValueSet(tset));
			getSession().save(clob);
		}
	}

}
