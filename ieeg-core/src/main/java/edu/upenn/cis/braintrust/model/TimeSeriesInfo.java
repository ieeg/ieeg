/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

@Entity
@Table(name = TimeSeriesInfo.TABLE)
public class TimeSeriesInfo {
	public static final String TABLE = "time_series_info";

	private Long id;

	private TimeSeriesEntity timeSeries;
	private String institution;
	private Integer headerLength;
	private String subjectFirstName;
	private String subjectSecondName;
	private String subjectThirdName;
	private String subjectId;
	private Long numberOfSamples;
	private String channelName;
	private Long recordingStartTime;
	private Long recordingEndTime;
	private Double samplingFrequency;
	private Double lowFrequencyFilterSetting;
	private Double highFrequencyFilterSetting;
	private Double notchFilterFrequency;
	private String acquisitionSystem;
	private String channelComments;
	private String studyComments;
	private Integer physicalChannelNumber;
	private Long maximumBlockLength;
	private Long blockInterval;
	private Integer blockHeaderLength;
	private Integer maximumDataValue;
	private Integer minimumDataValue;
	
	private Double baseline;
	private Double multiplier;
	private String units;
	private String modality;

	
	private Double voltageConversionFactor;
	
	private Integer channelNumber;

	public TimeSeriesInfo() {}

	public TimeSeriesInfo(TimeSeriesEntity timeSeries, String institution,
			Integer headerLength, String subjectFirstName,
			String subjectSecondName, String subjectThirdName,
			String subjectId, Long numberOfSamples, String channelName,
			Long recordingStartTime, Long recordingEndTime,
			Double samplingFrequency, Double lowFrequencyFilterSetting,
			Double highFrequencyFilterSetting, Double notchFilterFrequency,
			String acquisitionSystem, String channelComments,
			String studyComments, Integer physicalChannelNumber,
			Long maximumBlockLength, Long blockInterval,
			Integer blockHeaderLength, Integer maximumDataValue,
			Integer minimumDataValue, Double voltageConversionFactor,
			Integer channelNumber) {
		super();
		this.timeSeries = timeSeries;
		this.institution = institution;
		this.headerLength = headerLength;
		this.subjectFirstName = subjectFirstName;
		this.subjectSecondName = subjectSecondName;
		this.subjectThirdName = subjectThirdName;
		this.subjectId = subjectId;
		this.numberOfSamples = numberOfSamples;
		this.channelName = channelName;
		this.recordingStartTime = recordingStartTime;
		this.recordingEndTime = recordingEndTime;
		this.samplingFrequency = samplingFrequency;
		this.lowFrequencyFilterSetting = lowFrequencyFilterSetting;
		this.highFrequencyFilterSetting = highFrequencyFilterSetting;
		this.notchFilterFrequency = notchFilterFrequency;
		this.acquisitionSystem = acquisitionSystem;
		this.channelComments = channelComments;
		this.studyComments = studyComments;
		this.physicalChannelNumber = physicalChannelNumber;
		this.maximumBlockLength = maximumBlockLength;
		this.blockInterval = blockInterval;
		this.blockHeaderLength = blockHeaderLength;
		this.maximumDataValue = maximumDataValue;
		this.minimumDataValue = minimumDataValue;
		this.voltageConversionFactor = voltageConversionFactor;
		
		this.channelNumber = channelNumber;
	}

	public TimeSeriesInfo(Long id, TimeSeriesEntity timeSeries,
			String institution, Integer headerLength, String subjectFirstName,
			String subjectSecondName, String subjectThirdName,
			String subjectId, Long numberOfSamples, String channelName,
			Long recordingStartTime, Long recordingEndTime,
			Double samplingFrequency, Double lowFrequencyFilterSetting,
			Double highFrequencyFilterSetting, Double notchFilterFrequency,
			String acquisitionSystem, String channelComments,
			String studyComments, Integer physicalChannelNumber,
			Long maximumBlockLength, Long blockInterval,
			Integer blockHeaderLength, Integer maximumDataValue,
			Integer minimumDataValue, Double voltageConversionFactor,
			Integer channelNumber) {
		super();
		this.id = id;
		this.timeSeries = timeSeries;
		this.institution = institution;
		this.headerLength = headerLength;
		this.subjectFirstName = subjectFirstName;
		this.subjectSecondName = subjectSecondName;
		this.subjectThirdName = subjectThirdName;
		this.subjectId = subjectId;
		this.numberOfSamples = numberOfSamples;
		this.channelName = channelName;
		this.recordingStartTime = recordingStartTime;
		this.recordingEndTime = recordingEndTime;
		this.samplingFrequency = samplingFrequency;
		this.lowFrequencyFilterSetting = lowFrequencyFilterSetting;
		this.highFrequencyFilterSetting = highFrequencyFilterSetting;
		this.notchFilterFrequency = notchFilterFrequency;
		this.acquisitionSystem = acquisitionSystem;
		this.channelComments = channelComments;
		this.studyComments = studyComments;
		this.physicalChannelNumber = physicalChannelNumber;
		this.maximumBlockLength = maximumBlockLength;
		this.blockInterval = blockInterval;
		this.blockHeaderLength = blockHeaderLength;
		this.maximumDataValue = maximumDataValue;
		this.minimumDataValue = minimumDataValue;
		this.voltageConversionFactor = voltageConversionFactor;
		this.channelNumber = channelNumber;
	}

	@GeneratedValue
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(optional = true)
	@ForeignKey(name = "fk_info_to_ts")
	@JoinColumn(name = "ts_id")
	public TimeSeriesEntity getTimeSeries() {
		return timeSeries;
	}

	public void setTimeSeries(TimeSeriesEntity timeSeries) {
		this.timeSeries = timeSeries;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Integer getHeaderLength() {
		return headerLength;
	}

	public void setHeaderLength(Integer headerLength) {
		this.headerLength = headerLength;
	}

	public String getSubjectFirstName() {
		return subjectFirstName;
	}

	public void setSubjectFirstName(String subjectFirstName) {
		this.subjectFirstName = subjectFirstName;
	}

	public String getSubjectSecondName() {
		return subjectSecondName;
	}

	public void setSubjectSecondName(String subjectSecondName) {
		this.subjectSecondName = subjectSecondName;
	}

	public String getSubjectThirdName() {
		return subjectThirdName;
	}

	public void setSubjectThirdName(String subjectThirdName) {
		this.subjectThirdName = subjectThirdName;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public Long getNumberOfSamples() {
		return numberOfSamples;
	}

	public void setNumberOfSamples(Long numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Long getRecordingStartTime() {
		return recordingStartTime;
	}

	public void setRecordingStartTime(Long recordingStartTime) {
		this.recordingStartTime = recordingStartTime;
	}

	public Long getRecordingEndTime() {
		return recordingEndTime;
	}

	public void setRecordingEndTime(Long recordingEndTime) {
		this.recordingEndTime = recordingEndTime;
	}

	public Double getSamplingFrequency() {
		return samplingFrequency;
	}

	public void setSamplingFrequency(Double samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public Double getLowFrequencyFilterSetting() {
		return lowFrequencyFilterSetting;
	}

	public void setLowFrequencyFilterSetting(Double lowFrequencyFilterSetting) {
		this.lowFrequencyFilterSetting = lowFrequencyFilterSetting;
	}

	public Double getHighFrequencyFilterSetting() {
		return highFrequencyFilterSetting;
	}

	public void setHighFrequencyFilterSetting(Double highFrequencyFilterSetting) {
		this.highFrequencyFilterSetting = highFrequencyFilterSetting;
	}

	public Double getNotchFilterFrequency() {
		return notchFilterFrequency;
	}

	public void setNotchFilterFrequency(Double notchFilterFrequency) {
		this.notchFilterFrequency = notchFilterFrequency;
	}

	public String getAcquisitionSystem() {
		return acquisitionSystem;
	}

	public void setAcquisitionSystem(String acquisitionSystem) {
		this.acquisitionSystem = acquisitionSystem;
	}

	public String getChannelComments() {
		return channelComments;
	}

	public void setChannelComments(String channelComments) {
		this.channelComments = channelComments;
	}

	public String getStudyComments() {
		return studyComments;
	}

	public void setStudyComments(String studyComments) {
		this.studyComments = studyComments;
	}

	public Integer getPhysicalChannelNumber() {
		return physicalChannelNumber;
	}

	public void setPhysicalChannelNumber(Integer physicalChannelNumber) {
		this.physicalChannelNumber = physicalChannelNumber;
	}

	public Long getMaximumBlockLength() {
		return maximumBlockLength;
	}

	public void setMaximumBlockLength(Long maximumBlockLength) {
		this.maximumBlockLength = maximumBlockLength;
	}

	public Long getBlockInterval() {
		return blockInterval;
	}

	public void setBlockInterval(Long blockInterval) {
		this.blockInterval = blockInterval;
	}

	public Integer getBlockHeaderLength() {
		return blockHeaderLength;
	}

	public void setBlockHeaderLength(Integer blockHeaderLength) {
		this.blockHeaderLength = blockHeaderLength;
	}

	public Integer getMaximumDataValue() {
		return maximumDataValue;
	}

	public void setMaximumDataValue(Integer maximumDataValue) {
		this.maximumDataValue = maximumDataValue;
	}

	public Integer getMinimumDataValue() {
		return minimumDataValue;
	}

	public void setMinimumDataValue(Integer minimumDataValue) {
		this.minimumDataValue = minimumDataValue;
	}

	public Double getVoltageConversionFactor() {
		return voltageConversionFactor;
	}

	public void setVoltageConversionFactor(Double voltageConversionFactor) {
		this.voltageConversionFactor = voltageConversionFactor;
	}
	
	public Integer getChannelNumber() {
		return channelNumber;
	}
	
	public void setChannelNumber(Integer num) {
		channelNumber = num;
	}

	public Double getBaseline() {
		if (baseline == null)
			return Double.valueOf(0);
		return baseline;
	}

	public void setBaseline(Double baseline) {
		this.baseline = baseline;
	}

	public Double getMultiplier() {
		if (multiplier == null)
			return Double.valueOf(0.001);
		
		return multiplier;
	}

	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}

	public String getUnits() {
		if (units == null)
			return "V";
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getDataModality() {
		if (modality == null)
			return INamedTimeSegment.EEG;
		return modality;
	}

	public void setDataModality(String modality) {
		this.modality = modality;
	}


}
