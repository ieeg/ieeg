/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.aws;

import com.amazonaws.services.s3.transfer.TransferManager;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

/**
 * 
 * @author John Frommeyer
 *
 */
public final class TransferManagerFactory {

	private static class TransferManagerSupplier implements
			Supplier<TransferManager> {

		@Override
		public TransferManager get() {
			return new TransferManager(AwsUtil.getS3());
		}

	}

	private static final Supplier<TransferManager> transferManagerSupplier = Suppliers
			.memoize(new TransferManagerSupplier());

	public static TransferManager getTransferManager() {
		return transferManagerSupplier.get();
	}

}
