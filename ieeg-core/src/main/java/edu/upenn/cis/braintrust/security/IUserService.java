/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.shared.LatLon;

/**
 * Implementations must be thread-safe.
 * 
 * @author Sam Donnelly
 */
@ThreadSafe
public interface IUserService {

	@Immutable
	static final class UsersAndTotalCount {

		public final List<User> users;
		public final long totalCount;

		public UsersAndTotalCount(List<User> users, long totalCount) {
			this.users = ImmutableList.copyOf(users);
			this.totalCount = totalCount;
		}
	}

	long countEnabledUsers();

	UsersAndTotalCount findEnabledUsers(
			int start,
			int length,
			Set<UserId> excludes);

	/**
	 * Returns the active user with the given username.
	 * 
	 * @param username the name of the user we want to look up
	 * 
	 * @return the active user with the given username
	 * 
	 * @throws IncorrectCredentialsExceptionif no such user exists
	 * @throws DisabledAccountException if the user is disabled
	 */
	User findExistingEnabledUser(String username);

	/**
	 * Get the user with the given email address. Return {@code null} if there
	 * is no such user.
	 * 
	 * @param email the email address
	 * 
	 * @return the user with the given username or {@code null} if there is no
	 *         such user
	 */
	User findUserByEmail(String email);

	/**
	 * Get the user with the given user id. Return {@code null} if there is no
	 * such user. NOTE: user id is not necessarily the same as the user name.
	 * 
	 * @param uid the user id
	 * 
	 * @return the user with the given user id or {@code null} if there is no
	 *         such user
	 */
	User findUserByUid(UserId uid);

	/**
	 * Get the user with the given username. Return {@code null} if there is no
	 * such user.
	 * 
	 * @param username the username
	 * 
	 * @return the user with the given username or {@code null} if there is no
	 *         such user
	 */
	User findUserByUsername(String username);

	/**
	 * Get the users with the given user ids. NOTE: user id is not necessarily
	 * the same as the user name.
	 * 
	 * @param userIds the user ids
	 * 
	 * @return the users with the given user ids
	 */
	Map<UserId, Optional<User>> findUsersByUid(
			Iterable<UserId> userIds);

	public List<LatLon> findLatLon();

	User createOrUpdateUser(User newUser);
}