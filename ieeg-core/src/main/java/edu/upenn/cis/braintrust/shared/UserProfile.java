/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

import edu.upenn.cis.db.mefview.shared.IUserProfile;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

@GwtCompatible(serializable = true)
@Immutable
public final class UserProfile implements IUserProfile, Serializable, JsonTyped {
	private static final long serialVersionUID = 1L;

	private Optional<String> firstName;

	private Optional<String> lastName;

	private Optional<String> institutionName;

	private Optional<String> academicTitle;

	private Optional<String> inboxBucket;

	private Optional<String> inboxAwsAccessKeyId;
	
	private Optional<Double> latitude;

	private Optional<Double> longitude;
	
	private Optional<String> street;
	private Optional<String> city;
	private Optional<String> state;
	private Optional<String> zip;
	private Optional<String> country;
	
	private Optional<String> job;
	
	private Optional<String> dropBoxCredential;

	/** For GWT. */
	@SuppressWarnings("unused")
	private UserProfile() {}

	public UserProfile(
			@Nullable String firstName,
			@Nullable String lastName,
			@Nullable String institutionName,
			@Nullable String academicTitle,
			@Nullable String job,
			@Nullable String inboxBucket, 
			@Nullable String inboxAwsAccessKeyId,
			@Nullable String dropBoxCredential,
			@Nullable Double latitude,
			@Nullable Double longitude,
			@Nullable String street,
			@Nullable String city,
			@Nullable String state,
			@Nullable String zip,
			@Nullable String country) {
		this.firstName = Optional.fromNullable(firstName);
		this.lastName = Optional.fromNullable(lastName);
		this.institutionName = Optional.fromNullable(institutionName);
		this.academicTitle = Optional.fromNullable(academicTitle);
		this.job = Optional.fromNullable(job);
		this.inboxBucket = Optional.fromNullable(inboxBucket);
		this.inboxAwsAccessKeyId = Optional.fromNullable(inboxAwsAccessKeyId);
		this.latitude = Optional.fromNullable(latitude);
		this.longitude = Optional.fromNullable(longitude);
		
		this.street = Optional.fromNullable(street);
		this.city = Optional.fromNullable(city);
		this.state = Optional.fromNullable(state);
		this.zip = Optional.fromNullable(zip);
		this.country = Optional.fromNullable(country);
		this.dropBoxCredential = Optional.fromNullable(dropBoxCredential);
	}

	public Optional<String> getAcademicTitle() {
		return academicTitle;
	}

	public Optional<String> getFirstName() {
		return firstName;
	}

	public Optional<String> getInstitutionName() {
		return institutionName;
	}

	public Optional<String> getLastName() {
		return lastName;
	}

	
	public Optional<String> getInboxBucket() {
		return inboxBucket;
	}

	public Optional<String> getInboxAwsAccessKeyId() {
		return inboxAwsAccessKeyId;
	}

	
	public Optional<Double> getLatitude() {
		return latitude;
	}

	public void setLatitude(Optional<Double> latitude) {
		this.latitude = latitude;
	}

	public Optional<Double> getLongitude() {
		return longitude;
	}

	public void setLongitude(Optional<Double> longitude) {
		this.longitude = longitude;
	}

	public Optional<String> getStreet() {
		return street;
	}

	public void setStreet(Optional<String> street) {
		this.street = street;
	}

	public Optional<String> getCity() {
		return city;
	}

	public void setCity(Optional<String> city) {
		this.city = city;
	}

	public Optional<String> getState() {
		return state;
	}

	public void setState(Optional<String> state) {
		this.state = state;
	}

	public Optional<String> getZip() {
		return zip;
	}

	public void setZip(Optional<String> zip) {
		this.zip = zip;
	}

	public Optional<String> getCountry() {
		return country;
	}

	public void setCountry(Optional<String> country) {
		this.country = country;
	}

	public void setFirstName(Optional<String> firstName) {
		this.firstName = firstName;
	}

	public void setLastName(Optional<String> lastName) {
		this.lastName = lastName;
	}

	public void setInstitutionName(Optional<String> institutionName) {
		this.institutionName = institutionName;
	}

	public void setAcademicTitle(Optional<String> academicTitle) {
		this.academicTitle = academicTitle;
	}

	public void setInboxBucket(Optional<String> inboxBucket) {
		this.inboxBucket = inboxBucket;
	}

	public void setInboxAwsAccessKeyId(Optional<String> inboxAwsAccessKeyId) {
		this.inboxAwsAccessKeyId = inboxAwsAccessKeyId;
	}

	
	public Optional<String> getJob() {
		return job;
	}

	public void setJob(Optional<String> job) {
		this.job = job;
	}

	public Optional<String> getDropBoxCredential() {
		return dropBoxCredential;
	}

	public void setDropBoxCredential(Optional<String> dropBoxCredential) {
		this.dropBoxCredential = dropBoxCredential;
	}

	@Override
	public String toString() {
		return "UserProfile [firstName="
				+ firstName
				+ ", lastName="
				+ lastName
				+ ", institutionName="
				+ institutionName
				+ ", academicTitle="
				+ academicTitle
				+ ", inboxBucket="
				+ inboxBucket
				+ ", inboxAwsAccessKeyId="
				+ inboxAwsAccessKeyId
				+ "]";
	}

}
