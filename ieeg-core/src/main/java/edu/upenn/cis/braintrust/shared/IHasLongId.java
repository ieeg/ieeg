/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.util.Comparator;

import com.google.common.base.Function;

/**
 * Has a long id.
 * 
 * @author Sam Donnelly
 */
public interface IHasLongId {

	public static final class HasLongIdComparator implements
			Comparator<IHasLongId> {

		@Override
		public int compare(IHasLongId o1, IHasLongId o2) {
			return o1.getId().compareTo(o2.getId());
		}

	}

	/**
	 * {@link Function} wrapper of {@link #getId()}.
	 */
	public static final Function<IHasLongId, Long> getId = new Function<IHasLongId, Long>() {

		@Override
		public Long apply(final IHasLongId hasLongId) {
			return hasLongId.getId();
		}
	};

	/**
	 * Get the id.
	 * 
	 * @return the id
	 */
	Long getId();

}
