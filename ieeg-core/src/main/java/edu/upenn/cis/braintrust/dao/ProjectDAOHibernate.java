/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SimpleNaturalIdLoadAccess;

import edu.upenn.cis.braintrust.model.ProjectDiscussion;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class ProjectDAOHibernate extends
		GenericHibernateDAO<ProjectEntity, Long>
		implements IProjectDAO {

	public ProjectDAOHibernate() {}
	
	public ProjectDAOHibernate(Session session) {
		setSession(session);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectDiscussion> findDiscussionByProjectID(
			ProjectEntity entity, int start,
			int count) {

		final Session s = getSession();
		Query query =
				s.getNamedQuery(ProjectDiscussion.FIND_BY_PROJECT)
						.setParameter("project", entity)
						.setFirstResult(start)
						.setMaxResults(count);
		return query.list();
	}

	@Override
	public void saveOrUpdateDiscussion(final ProjectDiscussion entity) {
		getSession().saveOrUpdate(entity);
	}

	@Override
	public ProjectDiscussion getDiscussionByItsID(String discussionID) {
		final Session s = getSession();
		SimpleNaturalIdLoadAccess la =
				s.bySimpleNaturalId(ProjectDiscussion.class);
		return (ProjectDiscussion) la.load(discussionID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectEntity> getProjectsForUser(UserEntity user) {
		List<ProjectEntity> p = (List<ProjectEntity>) getSession()
				.getNamedQuery(ProjectEntity.BY_USER)
				.setParameter("user", user)
				.list();

		return p;
	}

	@Override
	public Set<EegProject> getEegProjectsForUser(UserEntity user) {
		@SuppressWarnings("unchecked")
		List<Object[]> p = (List<Object[]>) getSession()
				.getNamedQuery(ProjectEntity.TO_EEG_PROJECT)
				.setParameter("user", user)
				.list();
		
		final Map<Long, EegProject> idToProject = newHashMap();
		for (Object[] row : p) {
			final Long id = (Long) row[0];
			EegProject eegProject = idToProject.get(id);
			if (eegProject == null) {
				final String pubId = (String) row[1];
				final String name = (String) row[2];
				final boolean isComplete = (Boolean) row[3];
				eegProject = new EegProject(
						id,
						pubId,
						name,
						new HashSet<String>(),
						new HashSet<String>(),
						isComplete);
				idToProject.put(id, eegProject);
			}
			final String tag = (String) row[4];
			if (tag != null) {
				eegProject.getTags().add(tag);
			}
			final String credit = (String) row[5];
			if (credit != null) {
				eegProject.getCredits().add(credit);
			}
			final UserId adminId = (UserId) row[6];
			if (adminId != null) {
				eegProject.getAdmins().add(adminId);
			}
			final UserId teamId = (UserId) row[7];
			if (teamId != null) {
				eegProject.getTeam().add(teamId);
			}
			final String datasetId = (String) row[8];
			if (datasetId != null) {
				eegProject.getSnapshots().add(datasetId);
			}
			final String toolId = (String) row[9];
			if (toolId != null) {
				eegProject.getTools().add(toolId);
			}
		}
		return newHashSet(idToProject.values());
	}
	
	@Override
	public List<String> findIdsByName(UserEntity user, String projectName) {
		@SuppressWarnings("unchecked")
		List<String> p = (List<String>) getSession()
				.getNamedQuery(ProjectEntity.IDS_BY_NAME)
				.setParameter("user", user)
				.setParameter("name", projectName)
				.list();
		
		return p;
	}

}
