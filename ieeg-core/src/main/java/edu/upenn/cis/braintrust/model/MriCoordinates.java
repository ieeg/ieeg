/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MriCoordinates implements Serializable {

	public static final String X_COLUMN = "mri_x";

	public static final String Y_COLUMN = "mri_y";

	public static final String Z_COLUMN = "mri_z";

	private static final long serialVersionUID = 1L;

	private Double x;

	private Double y;

	private Double z;

	MriCoordinates() {}

	public MriCoordinates(final Double x, final Double y, final Double z) {
		this.x = checkNotNull(x);
		this.y = checkNotNull(y);
		this.z = checkNotNull(z);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MriCoordinates)) {
			return false;
		}
		final MriCoordinates other = (MriCoordinates) obj;
		if (x == null) {
			if (other.x != null) {
				return false;
			}
		} else if (!x.equals(other.x)) {
			return false;
		}
		if (y == null) {
			if (other.y != null) {
				return false;
			}
		} else if (!y.equals(other.y)) {
			return false;
		}
		if (z == null) {
			if (other.z != null) {
				return false;
			}
		} else if (!z.equals(other.z)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the x
	 */
	@Column(name = X_COLUMN, nullable = false)
	public Double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	@Column(name = Y_COLUMN, nullable = false)
	public Double getY() {
		return y;
	}

	/**
	 * @return the z
	 */
	@Column(name = Z_COLUMN, nullable = false)
	public Double getZ() {
		return z;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		result = prime * result + ((z == null) ? 0 : z.hashCode());
		return result;
	}

	/**
	 * @param x the x to set
	 */
	@SuppressWarnings("unused")
	private void setX(final Double x) {
		this.x = x;
	}

	/**
	 * @param y the y to set
	 */
	@SuppressWarnings("unused")
	private void setY(final Double y) {
		this.y = y;
	}

	/**
	 * @param z the z to set
	 */
	@SuppressWarnings("unused")
	private void setZ(final Double z) {
		this.z = z;
	}

}
