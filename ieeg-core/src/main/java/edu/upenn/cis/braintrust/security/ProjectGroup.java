/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
@Immutable
public class ProjectGroup implements Serializable {

	private static final long serialVersionUID = 1L;
	private String projectId;
	private String projectName;
	private ProjectGroupType type;

	// For Gwt
	@SuppressWarnings("unused")
	private ProjectGroup() {}

	public ProjectGroup(String projectId,
			String projectName,
			ProjectGroupType type) {
		this.projectId = checkNotNull(projectId);
		this.projectName = checkNotNull(projectName);
		this.type = checkNotNull(type);
	}

	public String getProjectId() {
		return projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public ProjectGroupType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projectId == null) ? 0 : projectId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ProjectGroup)) {
			return false;
		}
		ProjectGroup other = (ProjectGroup) obj;
		if (projectId == null) {
			if (other.projectId != null) {
				return false;
			}
		} else if (!projectId.equals(other.projectId)) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ProjectGroup [projectId=" + projectId + ", projectName="
				+ projectName + ", type=" + type + "]";
	}

}