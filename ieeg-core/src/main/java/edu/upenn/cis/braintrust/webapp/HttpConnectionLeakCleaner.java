/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.lang.reflect.Field;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.net.www.http.HttpClient;
import sun.net.www.http.KeepAliveCache;

@SuppressWarnings("restriction")
public final class HttpConnectionLeakCleaner implements
		ServletContextListener {

	Logger logger = LoggerFactory.getLogger(HttpConnectionLeakCleaner.class);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		try {
			logger.info("cleaning up sun.net.www.http.HttpClient.kac.keepAliveTimer.contextClassLoader permgen leak");

			final Field kac = HttpClient.class.getDeclaredField("kac");
			kac.setAccessible(true);
			final Field keepAliveTimer = KeepAliveCache.class
					.getDeclaredField("keepAliveTimer");
			keepAliveTimer.setAccessible(true);

			final Thread t = (Thread) keepAliveTimer.get(kac.get(null));
			final ClassLoader tContextClassLoader = t.getContextClassLoader();
			final ClassLoader thisClassLoader = getClass().getClassLoader();

			if (tContextClassLoader == thisClassLoader) {
				logger.info("setting keepAliveTimer thread to the system class loader");
				t.setContextClassLoader(ClassLoader.getSystemClassLoader());
			}
		} catch (final Throwable ex) {
			logger.error("LeakPreventor failed", ex);
		}
	}

	/**
	 * NOOP.
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {

	}

}
