/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Date;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.shared.IHasLongId;

@Entity(name = "Tool")
@Table(name = ToolEntity.TABLE,
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {
						ToolEntity.LABEL_COLUMN },
						name = "uq_tool_label")
		})
public class ToolEntity
		implements IHasLongId, IHasPubId, IHasExtAcl {

	public static final String TABLE = "tool";

	public static final String ID_COLUMN = "tool_id";
	public static final String LABEL_COLUMN = "label";

	private Long id;
	private Integer version;
	private String pubId = newUuid();
	private Date createTime = new Date();
	private ExtAclEntity extAcl;
	private String label;
	private UserEntity author;
	private String description;
	private String sourceUrl;
	private String binUrl;

	ToolEntity() {}

	/**
	 * User-supplied-field total constructor.
	 * 
	 * @param acl
	 * @param extAcl TODO
	 * @param label
	 * @param author
	 * @param toolLoc
	 * @param parallel
	 * @param sourceUrl
	 * @param binUrl
	 * @param description
	 */
	public ToolEntity(
			ExtAclEntity extAcl,
			String label,
			UserEntity author,
			String sourceUrl,
			@Nullable String binUrl,
			@Nullable String description) {
		this.extAcl = checkNotNull(extAcl);
		this.label = checkNotNull(label);
		this.author = checkNotNull(author);
		this.sourceUrl = checkNotNull(sourceUrl);
		this.binUrl = binUrl;
		this.description = description;
	}

	@Override
	@OneToOne(
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getExtAcl() {
		return extAcl;
	}

	@ManyToOne
	// not lazy on purpose
	@JoinColumn(name = "author_id")
	@ForeignKey(name = "FK_TOOL_AUTHOR")
	@NotNull
	public UserEntity getAuthor() {
		return author;
	}

	@NotNull
	public String getBinUrl() {
		return binUrl;
	}

	/**
	 * @return the timestamp
	 */
	@Column(name = "create_time", nullable = false)
	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	public String getDescription() {
		return description;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(name = LABEL_COLUMN)
	@NotNull
	public String getLabel() {
		return label;
	}

	/**
	 * @return the id
	 */
	@Override
	@NaturalId
	@Column(name = "pub_id")
	@Size(min = 36, max = 36)
	@NotNull
	public String getPubId() {
		return pubId;
	}

	@NotNull
	public String getSourceUrl() {
		return sourceUrl;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	public void setExtAcl(final ExtAclEntity extAcl) {
		this.extAcl = extAcl;
	}

	public void setAuthor(final UserEntity author) {
		this.author = author;
	}

	public void setBinUrl(final String binUrl) {
		this.binUrl = binUrl;
	}

	/**
	 * @param createTime the timestamp to set
	 */
	@SuppressWarnings("unused")
	private void setCreateTime(final Date createTime) {
		this.createTime = createTime;
	}

	public void setDescription(@Nullable final String description) {
		this.description = description;
	}

	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @param pubId the id to set
	 */
	@SuppressWarnings("unused")
	private void setPubId(final String pubId) {
		this.pubId = pubId;
	}

	public void setSourceUrl(final String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

}
