/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.braintrust.dao;

import java.util.Collections;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;

public final class SecurityUtil {

	public static ExtAclEntity createUserOwnedAcl(
			UserEntity user,
			IPermissionDAO permissionDAO) {
		ExtAclEntity acl = new ExtAclEntity();
		PermissionEntity ownerPerm =
				permissionDAO.findByDomainModeAndName(
						PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
						CorePermDefs.CORE_MODE_NAME,
						CorePermDefs.OWNER_PERMISSION_NAME);

		// Constructor will add user ace to acl
		new ExtUserAceEntity(user, acl, ownerPerm);
		return acl;
	}

	public static ExtAclEntity createUserOwnedWorldReadableAcl(
			UserEntity user,
			IPermissionDAO permissionDAO) {
		PermissionEntity readPerm =
				permissionDAO.findByDomainModeAndName(
						PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
						CorePermDefs.CORE_MODE_NAME,
						CorePermDefs.READ_PERMISSION_NAME);
		ExtAclEntity acl = new ExtAclEntity(Collections.singleton(readPerm));
		PermissionEntity ownerPerm =
				permissionDAO.findByDomainModeAndName(
						PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
						CorePermDefs.CORE_MODE_NAME,
						CorePermDefs.OWNER_PERMISSION_NAME);
		// Constructor will add user ace to acl
		new ExtUserAceEntity(user, acl, Collections.singleton(ownerPerm));

		return acl;
	}

	private SecurityUtil() {
		throw new AssertionError(
				"can't instantiate a " + SecurityUtil.class.getSimpleName());
	}
}
