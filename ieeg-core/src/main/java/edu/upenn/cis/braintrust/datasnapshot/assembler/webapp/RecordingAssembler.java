/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class RecordingAssembler {

	private final ContactGroupAssembler electrodeAssembler;
	private final ITsAnnotationDAO annotationDAO;

	public RecordingAssembler(
			ITsAnnotationDAO annotationDAO) {
		this.annotationDAO = checkNotNull(annotationDAO);
		this.electrodeAssembler = new ContactGroupAssembler();
	}

	public GwtRecording buildDto(final Recording recording) {
		checkNotNull(recording);
		final GwtRecording gwtRecording = new GwtRecording(
				recording.getDir(),
				recording.getMefDir(),
				recording.getStartTimeUutc(),
				recording.getEndTimeUutc(),
				recording.getRefElectrodeDescription(),
				recording.getImagesFile(),
				recording.getReportFile(),
				recording.getCommentary(),
				recording.getId(),
				recording.getVersion());

		for (final ContactGroup electrode : recording.getContactGroups()) {
			gwtRecording.addContactGroup(electrodeAssembler.buildDto(electrode));
		}
		return gwtRecording;
	}

	public Recording buildEntity(GwtRecording gwtRecording) {
		checkNotNull(gwtRecording);
		checkArgument(gwtRecording.getId() == null);
		checkArgument(gwtRecording.getVersion() == null);
		// A new study should have no electrodes
		checkArgument(gwtRecording.getContactGroups().isEmpty(),
				"New recordings should have no electrodes.");
		final Recording recording = new Recording(
				gwtRecording.getDir(),
				gwtRecording.getMefDir(),
				gwtRecording.getStartTimeUutc(),
				gwtRecording.getEndTimeUutc(),
				gwtRecording.getRefElectrodeDescription().orNull(),
				gwtRecording.getImagesFile(),
				gwtRecording.getReportFile(),
				gwtRecording.getCommentary());
		return recording;
	}

	private void copyToEntity(final Recording recording,
			final GwtRecording gwtRecording) {
		recording.setDir(gwtRecording.getDir());
		recording.setMefDir(gwtRecording.getMefDir());
		recording.setStartTimeUutc(gwtRecording.getStartTimeUutc());
		recording.setEndTimeUutc(gwtRecording.getEndTimeUutc());
		recording.setReportFile(gwtRecording.getReportFile());
		recording.setImagesFile(gwtRecording.getImagesFile());
		recording.setCommentary(gwtRecording.getCommentary());
		recording.setRefElectrodeDescription(gwtRecording
				.getRefElectrodeDescription().orNull());
	}

	public void modifyEntity(DataSnapshotEntity recordingParent,
			final Recording recording,
			final GwtRecording gwtRecording, boolean modifiable)
			throws StaleObjectException, BrainTrustUserException {
		final Long gwtId = gwtRecording.getId();
		final Long dbId = recording.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtRecording.getVersion();
		final Integer dbVer = recording.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for study ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		handleContactGroups(recordingParent, recording, gwtRecording, modifiable);
		copyToEntity(recording, gwtRecording);

	}

	private void handleContactGroups(DataSnapshotEntity recordingParent,
			final Recording recording,
			final GwtRecording gwtRecording, boolean modifiable)
			throws StaleObjectException, BrainTrustUserException {

		for (final GwtContactGroup gwtElectrode : gwtRecording.getContactGroups()) {
			final Long gwtId = gwtElectrode.getId();
			if (gwtId == null) {
				final ContactGroup electrode = electrodeAssembler.buildEntity(
						gwtElectrode);
				recording.addContactGroup(electrode);
			} else {
				final ContactGroup electrode = find(recording.getContactGroups(),
						compose(equalTo(gwtId), ContactGroup.getId));
				if (!gwtElectrode.isDeleted()) {
					electrodeAssembler.modifyEntity(electrode, gwtElectrode);
				} else {
					if (!modifiable) {
						throw new BrainTrustUserException(
								"Electrode cannot be deleted. A time series in this recording is referenced by a user created data snapshot.");
					} else {
						final Integer dbVersion = electrode.getVersion();
						final Integer gwtVersion = gwtElectrode.getVersion();
						if (!dbVersion.equals(gwtVersion)) {
							throw new StaleObjectException(
									"Version mismatch for electrode ["
											+ gwtId + "]. DTO version: ["
											+ gwtVersion
											+ "], DB version: [" + dbVersion
											+ "].");
						}
						for (final Contact contact : electrode.getContacts()) {
							annotationDAO
									.removeAnnotatedAndDeleteEmptyAnnotations(
											recordingParent, contact.getTrace());
						}
						recording.removeContactGroup(electrode);
					}
				}
			}
		}
	}
}
