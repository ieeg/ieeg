/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.db.mefview.shared.JsonTyped;


@GwtCompatible(serializable = true)

/**
 * Simple class for associating named JSON-compatible datatypes
 * 
 * @author zives
 *
 */
public class JsonKeyValue implements IJsonKeyValue {
	String key;
	JsonTyped json;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public JsonTyped getJson() {
		return json;
	}
	public void setJson(JsonTyped  json) {
		this.json = json;
	}
	
	public String toString() {
		return key + ": '" + json + "'";
	}
	
	public JsonKeyValue () {
		
	}
	
	public JsonKeyValue(String key, JsonTyped  json) {
		super();
		this.key = key;
		this.json = json;
	}
	
//	@Override
//	public String getValueAsString() {
//		try {
//			return JsonKeyValueMapper.getMapper().createValue(json);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//	}
	
	
}