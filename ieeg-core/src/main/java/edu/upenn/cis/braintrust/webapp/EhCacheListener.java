/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.InputStream;
import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.constructs.web.ShutdownListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;

/**
 * @author Sam Donnelly
 */
public class EhCacheListener implements ServletContextListener {

	private static Logger logger = LoggerFactory
			.getLogger(EhCacheListener.class);

	private ShutdownListener shutdownListener = new ShutdownListener();

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		final String M = "contextDestroyed(...)";
		final long in = System.nanoTime();
		try {
			shutdownListener.contextDestroyed(sce);
		} finally {
			logger.info(
					"{}: ehcache shutdown took {}",
					M,
					BtUtil.diffNowThenSeconds(in));
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		final String M = "contextInitialized(...)";
		InputStream is = null;
		try {
			if (AwsUtil.isConfigFromS3()) {
				logger.info("reading ieeg-ehcache.xml from S3");
				String configKey = null;
				configKey = AwsUtil.getConfigS3Dir() + "/" +
						"ieeg-ehcache.xml";
				GetObjectRequest getObjectRequest =
						new GetObjectRequest(
								AwsUtil.getConfigS3Bucket(),
								configKey);
				logger.info("reading ieeg-ehcache.xml from ["
						+ AwsUtil.getConfigS3Bucket() + "] ["
						+ configKey + "]");
				S3Object s3Object = AwsUtil.getS3().getObject(
						getObjectRequest);
				is = s3Object.getObjectContent();
			} else {
				String configName = BtUtil.getConfigName(
						sce.getServletContext(),
						"ieeg-ehcache.xml");
				logger.info("{}: looking for ieeg-ehcache.xml at [{}]", M,
						configName);
				URL url = getClass().getClassLoader().getResource(configName);
				if (url == null) {
					logger.info(
							"{}: looking for ieeg-ehcache.xml at class path root",
							M);
					url = getClass().getClassLoader()
							.getResource("ieeg-ehcache.xml");
				}

				logger.info("{}: reading ieeg-ehcache.xml from {}", M, url);

				is = url.openStream();

			}
			CacheManager.newInstance(is);
		} catch (Throwable t) {
			logger.error("caught exception", t);
			throw new ExceptionInInitializerError(t);
		} finally {
			BtUtil.close(is);
		}
	}
}
