/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.persistence;

import javax.annotation.Nullable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.BtUtil;

public class PersistenceUtil {

	public static class SessionAndTrx {
		public final Session session;
		public final Optional<Transaction> trx;
		public final boolean localSession;

		public SessionAndTrx(
				Session session,
				@Nullable Transaction trx,
				boolean localSession) {
			this.session = session;
			this.trx = Optional.fromNullable(trx);
			this.localSession = localSession;
		}
	}

	private static Logger logger = LoggerFactory
			.getLogger(PersistenceUtil.class);
	private static Logger timeLogger =
			LoggerFactory.getLogger("time." + PersistenceUtil.class.getName());

	/**
	 * Take care of closing a session. Logs any {@code Exception} that occurs
	 * during close.
	 * <p>
	 * Does nothing if the session is null or closed.
	 * <p>
	 * Guaranteed to not throw an {@code Exception}.
	 * 
	 * @param session the session to close
	 */
	public static void close(@Nullable Session session) {
		if (session != null && session.isOpen()) {
			try {
				session.close();
			} catch (Exception t) {
				logger.error("exception while closing session", t);
			}
		}
	}

	public static void close(
			@Nullable SessionAndTrx sessionAndTrx) {
		if (sessionAndTrx != null) {
			closeAndUnbind(sessionAndTrx.session, sessionAndTrx.localSession);
		}
	}

	public static void closeAndUnbind(
			Session session,
			boolean closeIt) {
		if (closeIt) {
			SessionFactory sessionFac =
					session.getSessionFactory();
			if (ManagedSessionContext.hasBind(sessionFac)) {
				ManagedSessionContext.unbind(sessionFac);
			}
			session.close();
		}
	}

	public static void commit(@Nullable SessionAndTrx sessAndTrx) {
		if (sessAndTrx != null) {
			commit(sessAndTrx.trx.orNull());
		}
	}

	/**
	 * Shorthand for: <code>
	 * 		if (trx != null && trx.isActive()) {
	 * 			trx.commit();
	 * 		}
	 * </code>
	 * 
	 * @param trx transaction to close
	 */
	public static void commit(@Nullable Transaction trx) {
		if (trx != null && trx.isActive()) {
			trx.commit();
		}
	}

	public static SessionAndTrx getSessAndTrx(
			SessionFactory sessFac) {
		String m = "getSessAndTrx(...)";
		double a = -1;
		double b = -1;
		double c = -1;
		boolean localSess = !ManagedSessionContext.hasBind(sessFac);
		Session sess;
		Transaction trx;
		logger.debug("{}: localSess {}", m, localSess);
		if (localSess) {
			sess = sessFac.openSession();
			ManagedSessionContext.bind(sess);
			trx = sess.beginTransaction();
		} else {
			long aBegin = System.nanoTime();
			sess = sessFac.getCurrentSession();
			a = BtUtil.diffNowThenSeconds(aBegin);
			if (sess.getTransaction().isActive()) {
				trx = null;
			} else {
				long bBegin = System.nanoTime();
				trx = sess.getTransaction();
				b = BtUtil.diffNowThenSeconds(bBegin);
				long cBegin = System.nanoTime();
				trx.begin();
				c = BtUtil.diffNowThenSeconds(cBegin);
			}
		}
		timeLogger.trace("{}: {} {} {}", new Object[] { m, a, b, c });
		return new SessionAndTrx(sess, trx, localSess);
	}

	public static void rollback(@Nullable SessionAndTrx sessAndTrx) {
		if (sessAndTrx != null) {
			rollback(sessAndTrx.trx.orNull());
		}
	}

	/**
	 * Take care of rolling back a transaction. Logs any rollback
	 * {@code Exception}
	 * <p>
	 * Does nothing if the transaction is null or not active.
	 * <p>
	 * Guaranteed to not throw an {@code Exception}.
	 * 
	 * @param trx the transaction to roll back
	 */
	public static void rollback(@Nullable Transaction trx) {
		if (trx != null && trx.isActive()) {
			try {
				trx.rollback();
			} catch (Exception rbEx) {
				logger.error("exception while rolling back", rbEx);
			}
		}
	}

	private PersistenceUtil() {}

}
