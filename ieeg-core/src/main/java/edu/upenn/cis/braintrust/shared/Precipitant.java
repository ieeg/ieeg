/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.io.Serializable;
import java.util.Collections;
import java.util.SortedMap;

import com.google.common.annotations.GwtCompatible;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
@GwtCompatible(serializable=true)
public enum Precipitant implements Serializable {
	STRESS("Stress"), CATAMENIAL("Catamenial"), DRUGS("Drugs"), ALCOHOL(
			"Alcohol"), SLEEP_DEPRIVATION("Sleep Deprivation"), NOCTURNAL(
			"Nocturnal"), UPON_AWAKENING("Upon Awakening"), FEBRILE(
			"Febrile");

	private String displayString;

	/**
	 * Maps a {@code Situation}'s toString representation to the
	 * {@code Situation} itself.
	 */
	public static final SortedMap<String, Precipitant> fromString;
	static {
		final SortedMap<String, Precipitant> temp = newTreeMap();
		for (final Precipitant situation : values()) {
			temp.put(situation.toString(), situation);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}
	
	private Precipitant() {}

	private Precipitant(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
