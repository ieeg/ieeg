/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;

public final class Log4JListener implements ServletContextListener {

	/**
	 * NOOP.
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		final String CLASS_NAME = getClass().getName();
		Properties props = null;
		try {
			if (AwsUtil.isConfigFromS3()) {

				String configKey =
						AwsUtil.getConfigS3Dir() + "/"
								+ "log4j.properties";

				System.out
						.println(
						CLASS_NAME
								+ ": reading log4j.properties from s3 ["
								+ AwsUtil.getConfigS3Bucket() + "] ["
								+ configKey + "]");

				GetObjectRequest getObjectRequest =
						new GetObjectRequest(
								AwsUtil.getConfigS3Bucket(),
								configKey);

				S3Object s3Object =
						AwsUtil.getS3().getObject(getObjectRequest);

				props = new Properties();
				props.load(s3Object.getObjectContent());
			} else {
				String configName = BtUtil.getConfigName(
						sce.getServletContext(),
						"log4j.properties");
				System.out.println(
						CLASS_NAME
								+ ": looking for log4j.properties at ["
								+ configName + "]");
				URL url = getClass().getClassLoader().getResource(configName);
				if (url == null) {
					System.out
							.println(
							CLASS_NAME
									+ ": reading log4j.properties from default location");
				} else {
					System.out.println(
							CLASS_NAME
									+ ": reading log4j.properties from "
									+ url);
					props = BtUtil.loadProps(url);
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

		if (props != null) {
			LogManager.resetConfiguration();
			PropertyConfigurator.configure(props);
		}
	}
}
