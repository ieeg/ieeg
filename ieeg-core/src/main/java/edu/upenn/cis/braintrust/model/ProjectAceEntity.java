/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing perms and
 * limitations undeimportimport javax.annotation.ParametersAreNonnullByDefault;



 ***************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.braintrust.imodel.IHasPerms;
import edu.upenn.cis.braintrust.security.ProjectGroupType;

@Entity(name = "ProjectAce")
@Table(
		name = ProjectAceEntity.TABLE,
		uniqueConstraints = @UniqueConstraint(
				columnNames = {
						ProjectEntity.ID_COLUMN,
						ProjectAceEntity.PROJ_GROUP_COLUMN,
						ExtAclEntity.ID_COLUMN }))
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(
		name = ProjectAceEntity.ID_COLUMN)) })
public class ProjectAceEntity extends LongIdAndVersion implements IHasPerms {

	public static final String TABLE = "project_ace";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String PROJ_GROUP_COLUMN = "project_group";

	private ProjectEntity project;
	private ProjectGroupType projGroup;
	private ExtAclEntity acl;
	private Set<PermissionEntity> perms = newHashSet();

	/**
	 * For JPA.
	 */
	ProjectAceEntity() {}

	/**
	 * This constructor adds the new instance to {@code extAcl}'s set of project
	 * aces.
	 * 
	 * @param project
	 * @param projectGrp
	 * @param extAcl
	 */
	public ProjectAceEntity(
			ProjectEntity project,
			ProjectGroupType projectGrp,
			ExtAclEntity extAcl,
			PermissionEntity permission) {
		this.acl = checkNotNull(extAcl);
		this.project = checkNotNull(project);
		this.projGroup = checkNotNull(projectGrp);
		this.perms.add(checkNotNull(permission));
		this.acl.getProjectAces().add(this);
	}
	
	/**
	 * This constructor adds the new instance to {@code extAcl}'s set of project
	 * aces.
	 * 
	 * @param project
	 * @param projectGrp
	 * @param extAcl
	 */
	public ProjectAceEntity(
			ProjectEntity project,
			ProjectGroupType projectGrp,
			ExtAclEntity extAcl,
			Set<PermissionEntity> permissions) {
		this.acl = checkNotNull(extAcl);
		this.project = checkNotNull(project);
		this.projGroup = checkNotNull(projectGrp);
		this.perms.addAll(checkNotNull(permissions));
		this.acl.getProjectAces().add(this);
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	public ExtAclEntity getAcl() {
		return acl;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = TABLE + "_" + PermissionEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = PermissionEntity.ID_COLUMN))
	@Size(min = 1)
	@Override
	public Set<PermissionEntity> getPerms() {
		return perms;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(
			name = ProjectEntity.ID_COLUMN)
	@NotNull
	public ProjectEntity getProject() {
		return project;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = PROJ_GROUP_COLUMN)
	@NotNull
	public ProjectGroupType getProjectGroup() {
		return projGroup;
	}

	@SuppressWarnings("unused")
	private void setAcl(ExtAclEntity acl) {
		this.acl = acl;
	}

	@SuppressWarnings("unused")
	private void setPerms(Set<PermissionEntity> perms) {
		this.perms = perms;
	}

	@SuppressWarnings("unused")
	private void setProject(ProjectEntity project) {
		this.project = project;
	}

	@SuppressWarnings("unused")
	private void setProjectGroup(ProjectGroupType projectGrp) {
		this.projGroup = projectGrp;
	}

}
