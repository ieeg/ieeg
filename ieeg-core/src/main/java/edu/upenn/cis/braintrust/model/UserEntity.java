/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.habitat.persistence.IPersistenceManager;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IUserProfile;

@Entity(name = "User")
@Table(name = UserEntity.TABLE)
@Immutable
public class UserEntity implements IUser, IEntity {
	public static final Function<UserEntity, UserId> getId = new Function<UserEntity, UserId>() {

		@Override
		public UserId apply(UserEntity input) {
			return input.getId();
		}
	};
	public static final String FIND_BY_USER_ID = "User-findByUserId";

	private Set<EegMontageEntity> montages = newHashSet(); // For cascading on
															// delete

	public static final String ID_COLUMN = "user_id";
	public static final String TABLE = "user";
	public static final String UPPER_CASE_TABLE = "USER";

	private UserId id;
	private Integer version;

	/** no lazy -to-one work-around */
	private Set<UserClob> userClob = newHashSet();

	/** For Hibernate. */
	public UserEntity() {}

	public UserEntity(UserId id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserEntity)) {
			return false;
		}
		UserEntity other = (UserEntity) obj;
		if (id == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!id.equals(other.getId())) {
			return false;
		}
		return true;
	}

	@Transient
	public String getClob() {
		if (userClob.isEmpty()) {
			return null;
		}
		return getOnlyElement(userClob).getValue();
	}

	@Id
	public UserId getId() {
		return id;
	}

	/**
	 * Arbitrary character data associated with this user.
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than buildtime
	 * bytecode instr. because that loads all lazy props when you hit any of
	 * them.
	 * 
	 * @return the arbitrary character data associated with this user
	 */
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,// LAZY,
			orphanRemoval = true)
	private Set<UserClob> getUserClob() {
		return userClob;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public void setClob(@Nullable String clob) {
		if (clob == null) {
			this.userClob.clear();
		} else {
			if (userClob.isEmpty()) {
				this.userClob.add(new UserClob(clob, this));
			} else {
				getOnlyElement(this.userClob).setValue(clob);
			}
		}
	}

	@SuppressWarnings("unused")
	public void setId(UserId id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	public void setUserClob(final Set<UserClob> userClob) {
		this.userClob = userClob;
	}

	@SuppressWarnings("unused")
	public void setVersion(final Integer version) {
		this.version = version;
	}

	// ///////////////////////////////////
	// TODO: all of these can fetch the results
	// by querying against the Drupal data

	@Transient
	@Override
	public String getUsername() {
		throw new RuntimeException(
				"Unsupported - need to cross into Drupal database");
	}

	@Transient
	@Override
	public boolean isAdmin() {
		throw new RuntimeException(
				"Unsupported - need to cross into Drupal database");
	}

	@Transient
	@Override
	public Status getStatus() {
		throw new RuntimeException(
				"Unsupported - need to cross into Drupal database");
	}

	@Transient
	@Override
	public IUserProfile getProfile() {
		throw new RuntimeException(
				"Unsupported - need to cross into Drupal database");
	}

	public static final EntityConstructor<UserEntity, UserId, DrupalUser> CONSTRUCTOR =
			new EntityConstructor<UserEntity, UserId, DrupalUser>(
					UserEntity.class,
					new IPersistentKey<UserEntity, UserId>() {

						@Override
						public UserId getKey(UserEntity o) {
							return o.getId();
						}

				@Override
				public void setKey(UserEntity o, UserId newKey) {
					o.setId(newKey);
				}

			},
			new EntityPersistence.ICreateObject<UserEntity,UserId,DrupalUser>() {
						@Override
						public UserEntity create(UserId id, DrupalUser optParent) {
							return new UserEntity(id);
						}
					}
			);

	@Override
	public EntityConstructor<UserEntity, UserId, DrupalUser> tableConstructor() {
		return CONSTRUCTOR;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "user",
			orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Set<EegMontageEntity> getMontages() {
		return montages;
	}

	@SuppressWarnings("unused")
	private void setMontages(Set<EegMontageEntity> montages) {
		this.montages = montages;
	}
}
