/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

@Entity(name = "SubmittedItem")
@Table(name = SubmittedItemEntity.TABLE)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = SubmittedItemEntity.ID_COLUMN)
		)
})
public class SubmittedItemEntity extends LongIdAndVersion implements IWorkItem {

	public final static String TABLE = "submitted_item";
	public final static String ID_COLUMN = TABLE + "_id";

	private String parentId;

	private String container;
	private String path;
	private String fileName;
	private boolean inS3;

	private String mediaType;
	private UserEntity creator;
	private String description;
	private Date createTime = new Date();
	private JsonTyped json;

	SubmittedItemEntity() {}

	public SubmittedItemEntity(
			String parent,
			String fileName,
			String internetMediaType,
			long sizeBytes,
			String md5,
			UserEntity creator) {
		this.parentId = checkNotNull(parent);

		this.fileName = checkNotNull(fileName);
		checkArgument(
				fileName.length() >= 1,
				"fileName too short");
		checkArgument(
				fileName.length() <= 255,
				"fileName too long");

		this.mediaType = checkNotNull(internetMediaType);
		checkArgument(
				internetMediaType.length() >= 1,
				"internetMediaType too short");
		checkArgument(
				internetMediaType.length() <= 255,
				"internetMediaType too long");

		this.creator = checkNotNull(creator);
	}

	public SubmittedItemEntity(
			String parent,
			String fileName,
			String internetMediaType,
			long sizeBytes,
			String md5,
			UserEntity creator,
			String description) {
		this(
				parent,
				fileName,
				internetMediaType,
				sizeBytes,
				md5,
				creator);
		this.description = checkNotNull(description);
		checkArgument(
				description.length() >= 1,
				"description too short");
		checkArgument(
				description.length() <= 4000,
				"description too long");
	}

	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id")
	@NotNull
	public UserEntity getCreator() {
		return creator;
	}

	@Size(min = 1, max = 4000)
	public String getDescription() {
		return description;
	}

	@Transient
	public String getFileKey() {
		return parentId
				+ "/objects/"
				+ fileName;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getFileName() {
		return fileName;
	}

	@Size(min = 1, max = 255)
	@NotNull
	public String getMediaType() {
		return mediaType;
	}

	/**
	 * Arbitrary json associated with this recording object
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than build time
	 * bytecode instrumentation because that loads all lazy props when you hit
	 * any of them.
	 * 
	 * @return the arbitrary json associated with this object
	 */
	public JsonTyped getJson() {
		return json;
	}

	public String getParentId() {
		return parentId;
	}

	@SuppressWarnings("unused")
	private void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@SuppressWarnings("unused")
	private void setCreator(UserEntity creator) {
		this.creator = creator;
	}

	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	@SuppressWarnings("unused")
	private void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setMediaType(String internetMediaType) {
		this.mediaType = internetMediaType;
	}

	@SuppressWarnings("unused")
	public void setJson(JsonTyped value) {
		this.json = value;
	}

	public void setParentId(String parent) {
		this.parentId = parent;
	}

	@Transient
	@Override
	public String getItemId() {
		return getFileKey();
	}

	@Override
	public IStoredObjectReference getMainHandle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<IStoredObjectReference> getOtherHandles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transient
	@Override
	public Long getTimestamp() {
		return getCreateTime().getTime();
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isInS3() {
		return inS3;
	}

	public void setInS3(boolean inS3) {
		this.inS3 = inS3;
	}


}
