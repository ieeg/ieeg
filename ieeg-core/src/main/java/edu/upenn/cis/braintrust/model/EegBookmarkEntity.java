/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import com.google.common.collect.Sets;

@Entity
@Table(name=EegBookmarkEntity.TABLE)
public class EegBookmarkEntity {
	public final static String TABLE = "eeg_bookmark";
	
	private Long bookmarkId;
	
	private DataSnapshotEntity snapshotId;
	
	private Long timeOffset;
	
	private Set<String> channelsSelected = Sets.newHashSet();
	
	private long viewWidth;
	
	private TsAnnotationEntity annotation;
	
	private int filterType;
	private double passLow;
	private double passHigh;
	private double stopLow;
	private double stopHigh;
	
	private boolean auto = false;
	
	public EegBookmarkEntity() { }
	
	public EegBookmarkEntity(DataSnapshotEntity snapshotId,
			Long timeOffset, Set<String> channelsSelected, long viewWidth,
			TsAnnotationEntity annotation, int filterType, double passLow,
			double passHigh, double stopLow, double stopHigh, boolean auto) {
		super();
		this.snapshotId = snapshotId;
		this.timeOffset = timeOffset;
		this.channelsSelected = channelsSelected;
		this.viewWidth = viewWidth;
		this.annotation = annotation;
		this.filterType = filterType;
		this.passLow = passLow;
		this.passHigh = passHigh;
		this.stopLow = stopLow;
		this.stopHigh = stopHigh;
		this.auto = auto;
	}
	
	
	public EegBookmarkEntity(Long bookmarkId, DataSnapshotEntity snapshotId,
			Long timeOffset, Set<String> channelsSelected, long viewWidth,
			TsAnnotationEntity annotation, int filterType, double passLow,
			double passHigh, double stopLow, double stopHigh, boolean auto) {
		super();
		this.bookmarkId = bookmarkId;
		this.snapshotId = snapshotId;
		this.timeOffset = timeOffset;
		this.channelsSelected = channelsSelected;
		this.viewWidth = viewWidth;
		this.annotation = annotation;
		this.filterType = filterType;
		this.passLow = passLow;
		this.passHigh = passHigh;
		this.stopLow = stopLow;
		this.stopHigh = stopHigh;
		this.auto = auto;
	}



	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "bookmark_id", unique = true, nullable = false)
	public Long getBookmarkId() {
		return bookmarkId;
	}
	public void setBookmarkId(Long bookmarkId) {
		this.bookmarkId = bookmarkId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "fk_bookmark_snapshot")
	@JoinColumn(name = "snapshot_id")
	@NotNull
	public DataSnapshotEntity getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(DataSnapshotEntity snapshotId) {
		this.snapshotId = snapshotId;
	}

	@NotNull
	public Long getTimeOffset() {
		return timeOffset;
	}
	public void setTimeOffset(Long timeOffset) {
		this.timeOffset = timeOffset;
	}

	@ElementCollection
	@CollectionTable(name="bookmark_channels")
	public Set<String> getChannelsSelected() {
		return channelsSelected;
	}
	public void setChannelsSelected(Set<String> channelsSelected) {
		this.channelsSelected = channelsSelected;
	}
	public long getViewWidth() {
		return viewWidth;
	}
	public void setViewWidth(long timeWidth) {
		this.viewWidth = timeWidth;
	}
	public int getFilterType() {
		return filterType;
	}
	public void setFilterType(int filterType) {
		this.filterType = filterType;
	}
	public double getPassLow() {
		return passLow;
	}
	public void setPassLow(double passLow) {
		this.passLow = passLow;
	}
	public double getPassHigh() {
		return passHigh;
	}
	public void setPassHigh(double passHigh) {
		this.passHigh = passHigh;
	}
	public double getStopLow() {
		return stopLow;
	}
	public void setStopLow(double stopLow) {
		this.stopLow = stopLow;
	}
	public double getStopHigh() {
		return stopHigh;
	}
	public void setStopHigh(double stopHigh) {
		this.stopHigh = stopHigh;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "FK_BOOKMARK_ANNOTATION")
	@JoinColumn(name = "annotation_id")
	@Nullable
	public TsAnnotationEntity getAnnotation() {
		return annotation;
	}

	public void setAnnotation(TsAnnotationEntity annotation) {
		this.annotation = annotation;
	}

	@Column(columnDefinition = "BIT", length = 1)
	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

}
