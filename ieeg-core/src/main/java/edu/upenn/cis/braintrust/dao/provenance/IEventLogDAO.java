/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.provenance;

import java.sql.Timestamp;
import java.util.List;

import edu.upenn.cis.braintrust.model.EventLogEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IEventLogDAO extends IDAO<EventLogEntity, Long> {
	/**
	 * All log entries directed to the user
	 * @param user
	 * @param start
	 * @param count
	 * @return
	 */
	List<EventLogEntity> find(UserEntity user, int start, int count);
	
	List<EventLogEntity> findSince(UserEntity user, Timestamp time);

	/**
	 * All log entries originating from a different user
	 * @param user
	 * @param start
	 * @param count
	 * @return
	 */
	List<EventLogEntity> findAllEntriesFrom(UserEntity user, int start, int count);
	
	/**
	 * All log entries originating from someone else, but directed to the user
	 * @param user
	 * @param start
	 * @param count
	 * @return
	 */
	List<EventLogEntity> findAllEntriesFromOthers(UserEntity user, int start, int count);
}
