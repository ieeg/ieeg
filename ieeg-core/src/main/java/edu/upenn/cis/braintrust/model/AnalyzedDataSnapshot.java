/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Immutable;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.RelationshipConstructor;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence;

/**
 * Inputs-to-outputs of a tool or manual derivation.
 * 
 * @author Sam Donnelly
 */
@Entity
@Table(name = AnalyzedDataSnapshot.TABLE)
@Immutable
@NamedQueries({ 
	@NamedQuery(
		name = AnalyzedDataSnapshot.ANALYZED_DS_BY_DS_ID,
		query = "select a "
				+ "from AnalyzedDataSnapshot a "
				+ "where a.dataSnapshot.pubId = :dataSnapshotPubId"),
	 @NamedQuery(
			name = AnalyzedDataSnapshot.ANALYZED_PARENT,
			query = "select a "
					+ "from AnalyzedDataSnapshot a "
					+ "where a.analysis.pubId = :dataSnapshotPubId") 
	})
public class AnalyzedDataSnapshot implements IRelationship {

	public static final String ANALYZED_DS_BY_DS_ID = "AnalyzedDataSnapshot-findByDsPubId";
	public static final String ANALYZED_PARENT = "AnalyzedDataSnapshot-findParent";

	public static Function<AnalyzedDataSnapshot, String> getToolLabel = new Function<AnalyzedDataSnapshot, String>() {

		@Override
		public String apply(final AnalyzedDataSnapshot input) {
			return input.getToolLabel();
		}

	};

	public static final String TABLE =
			"analyzed_" + DataSnapshotEntity.TABLE;
	public static final String UPPER_CASE_TABLE =
			"ANALYZED_" + DataSnapshotEntity.UPPER_CASE_TABLE;

	private Long id;

	private DataSnapshotEntity dataSnapshot;

	private Dataset dataset;

	private String toolLabel;

	// private ToolEntity tool;

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public AnalyzedDataSnapshot() {}

	// public AnalyzedDataSnapshot(
	// final @Nullable ToolEntity tool,
	// final Dataset dataset,
	// final DataSnapshotEntity dataSnapshot) {
	// this.tool = tool;
	// this.dataset = dataset;
	// this.dataSnapshot = dataSnapshot;
	//
	// checkArgument(dataset.getId() != null, "dataset.getId() is null");
	// checkArgument(dataSnapshot.getId() != null,
	// "dataSnapshot.getId() is null");
	//
	// this.id = new Id(dataset.getId(), dataSnapshot.getId());
	// dataset.getAnalysisInputs().add(this);
	// dataSnapshot.getAnalysisOutputs().add(this);
	// }

	/**
	 * Full constructor, the Image and Contact instances have to have an
	 * identifier value, they have to be in detached or persistent state. This
	 * constructor takes care of the bidirectional relationship by adding the
	 * new instance to the collections on either side of the many-to-many
	 * association (added to the collections).
	 */
	public AnalyzedDataSnapshot(
			final String toolLabel,
			final Dataset dataset,
			final DataSnapshotEntity dataSnapshot) {

		checkNotNull(toolLabel);

		this.toolLabel = toolLabel;

		this.dataset = dataset;
		this.dataSnapshot = dataSnapshot;

		dataset.getAnalysisInputs().add(this);
		dataSnapshot.getAnalysisOutputs().add(this);
	}

	@MapsId
	@ManyToOne
	// really @OneToOne - apparent bug with @MapsId
	@JoinColumn(name = "analysis_id")
	@ForeignKey(name = "FK_" + UPPER_CASE_TABLE + "_ANALYSIS")
	public Dataset getAnalysis() {
		return dataset;
	}

	@ManyToOne
	@JoinColumn(name = DataSnapshotEntity.ID_COLUMN)
	@ForeignKey(name = "FK_" + UPPER_CASE_TABLE + "_SNAPSHOT")
	public DataSnapshotEntity getDataSnapshot() {
		return dataSnapshot;
	}

	@Id
	public Long getId() {
		return id;
	}

	public String getToolLabel() {
		return toolLabel;
	}

	@SuppressWarnings("unused")
	private void setAnalysis(Dataset dataset) {
		this.dataset = dataset;
	}

	@SuppressWarnings("unused")
	private void setDataSnapshot(DataSnapshotEntity dataSnapshot) {
		this.dataSnapshot = dataSnapshot;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setToolLabel(String toolLabel) {
		this.toolLabel = toolLabel;
	}

	@Override
	public RelationshipConstructor<? extends IRelationship, ?, ? extends IEntity, ? extends IEntity> tableConstructor() {
		return new RelationshipConstructor<AnalyzedDataSnapshot,String, DataSnapshotEntity, Dataset>
		(AnalyzedDataSnapshot.class,
				new IPersistentObjectManager.IPersistentKey<AnalyzedDataSnapshot,String>() {

					@Override
					public String getKey(AnalyzedDataSnapshot o) {
						return o.getToolLabel();
					}

					@Override
					public void setKey(AnalyzedDataSnapshot o, String newKey) {
						o.setToolLabel(newKey);
					}

				},
				new RelationshipPersistence.ICreateRelationship<AnalyzedDataSnapshot,String,DataSnapshotEntity, Dataset>() {

					@Override
					public AnalyzedDataSnapshot create(String key, DataSnapshotEntity e1, Dataset e2) {
						// No derived Dataset specified
						// TODO: create the dataset???
						return new AnalyzedDataSnapshot(key, 
								e2, 
								e1);
					}
				}
		);
	}

}
