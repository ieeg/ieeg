/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import edu.upenn.cis.braintrust.shared.UserId;

@Entity
@Table(name = UserClob.TABLE)
public class UserClob {

	public static final String TABLE = "user_clob";

	private UserId id;
	private Integer version;
	private UserEntity parent;
	private String value;

	/**
	 * For Hibernate.
	 */
	UserClob() {}

	public UserClob(String value, UserEntity parent) {
		this.value = value;
		this.parent = parent;
	}

	@Id
	public UserId getId() {
		return id;
	}

	@MapsId
	// this is really one-to-one, but we need to make it many-to-one since its
	// one-to-many on the other side. See the other side for why.
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_user_clob_parent")
	@JoinColumn(name = "parent_id")
	@NotNull
	public UserEntity getParent() {
		return parent;
	}

	@Lob
	@Column
	@NotNull
	public String getValue() {
		return value;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(UserId id) {
		this.id = id;
	}

	public void setParent(final UserEntity parent) {
		this.parent = parent;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}
}