/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.model.SessionEntity;

/**
 * For type safety, a session token for a {@link SessionEntity}.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
@Immutable
public final class SessionToken implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	/** For GWT */
	@SuppressWarnings("unused")
	public SessionToken() {}

	public SessionToken(final String id) {
		checkNotNull(id);
		this.id = id;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(@Nullable final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SessionToken)) {
			return false;
		}
		final SessionToken other = (SessionToken) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/**
	 * Get the value of the session id
	 * 
	 * @return the value of the session id
	 */
	@Nonnull
	public String getId() {
		return id;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "SessionToken [id=" + id + "]";
	}

}
