/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.IHasName;

/**
 * An Organization
 * 
 * @author John Frommeyer
 * 
 */
public class GwtOrganization implements IHasLongId, IHasVersion, IHasName,
		Serializable {
	public static final Function<GwtOrganization, String> getCode = new Function<GwtOrganization, String>() {

		@Override
		@Nullable
		public String apply(GwtOrganization input) {
			return input.getCode();
		}
	};

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer version;
	private String name;
	private String code;

	@SuppressWarnings("unused")
	private GwtOrganization() {}

	public GwtOrganization(
			String name,
			String code,
			@Nullable Long id,
			@Nullable Integer version) {
		this.name = checkNotNull(name);
		this.code = checkNotNull(code);
		this.id = id;
		this.version = version;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return code + ": " + name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof GwtOrganization)) {
			return false;
		}
		GwtOrganization other = (GwtOrganization) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public void setName(String name) {
		this.name = name;
	}

}
