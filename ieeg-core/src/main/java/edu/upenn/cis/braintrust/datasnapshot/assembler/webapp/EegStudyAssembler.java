/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.EpilepsySubtypeEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtEpilepsySubtype;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class EegStudyAssembler {

	private final RecordingAssembler recordingAssembler;
	private final IPermissionDAO permissionDAO;
	private final EpilepsySubtypeAssembler epilepsySubtypeAssembler = new EpilepsySubtypeAssembler();;
	private final IDataSnapshotService dsService;

	public EegStudyAssembler(
			IPermissionDAO permissionDAO,
			ITsAnnotationDAO annotationDAO,
			IDataSnapshotService dsService) {
		this.permissionDAO = checkNotNull(permissionDAO);
		this.dsService = checkNotNull(dsService);
		this.recordingAssembler = new RecordingAssembler(annotationDAO);
	}

	public GwtEegStudy buildDto(final EegStudy study) {
		checkNotNull(study);
		final GwtEegStudy gwtStudy = new GwtEegStudy();
		gwtStudy.setId(study.getId());
		gwtStudy.setVersion(study.getVersion());
		gwtStudy.setLabel(study.getLabel());
		gwtStudy.setSzCount(study.getSzCount());
		gwtStudy.setType(study.getType());
		final PermissionEntity readPerm = permissionDAO
				.findStarCoreReadPerm();
		gwtStudy.setPublished(study.getExtAcl().getWorldAce().contains(readPerm));
		final boolean modifiable = dsService.isRecordingModifiable(study);
		gwtStudy.setModifiable(modifiable);
		gwtStudy.setRecording(recordingAssembler.buildDto(study.getRecording()));
		for (final EpilepsySubtypeEntity subtype : study.getEpilepsySubtypes()) {
			gwtStudy.getEpilepsySubtypes().add(
					epilepsySubtypeAssembler.buildDto(subtype));
		}
		return gwtStudy;
	}

	public EegStudy buildEntity(GwtEegStudy gwtStudy) {
		checkNotNull(gwtStudy);
		checkArgument(gwtStudy.getId() == null);
		checkArgument(gwtStudy.getVersion() == null);

		final EegStudy study = new EegStudy(
				new ExtAclEntity());
		copyToEntity(study, gwtStudy);
		final GwtRecording gwtRecording = gwtStudy.getRecording();
		study.setRecording(recordingAssembler.buildEntity(gwtRecording));
		for (final GwtEpilepsySubtype gwtSubtype : gwtStudy
				.getEpilepsySubtypes()) {
			study.getEpilepsySubtypes().add(
					epilepsySubtypeAssembler.buildEntity(study, gwtSubtype));
		}
		return study;
	}

	private void copyToEntity(final EegStudy study, final GwtEegStudy gwtStudy) {
		study.setLabel(gwtStudy.getLabel());
		study.setSzCount(gwtStudy.getSzCount().orNull());
		study.setType(gwtStudy.getType());
		final PermissionEntity readPerm = permissionDAO.findStarCoreReadPerm();
		if (gwtStudy.isPublished()) {
			study.getExtAcl().getWorldAce().add(readPerm);
		} else {
			study.getExtAcl().getWorldAce().clear();
		}
	}

	public void modifyEntity(final EegStudy study, final GwtEegStudy gwtStudy,
			boolean modifiable)
			throws StaleObjectException, BrainTrustUserException {
		final Long gwtId = gwtStudy.getId();
		final Long dbId = study.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtStudy.getVersion();
		final Integer dbVer = study.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for study ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		recordingAssembler.modifyEntity(study,
				study.getRecording(), gwtStudy.getRecording(), modifiable);
		handleEpilepsySubtypesModification(study, gwtStudy);
		copyToEntity(study, gwtStudy);

	}

	private void handleEpilepsySubtypesModification(EegStudy study,
			GwtEegStudy gwtStudy)
			throws StaleObjectException {
		for (final GwtEpilepsySubtype gwtSubtype : gwtStudy
				.getEpilepsySubtypes()) {
			final Long gwtId = gwtSubtype.getId();
			if (gwtId == null) {
				// buildEntity will add the subtype to the parent.
				epilepsySubtypeAssembler
						.buildEntity(
								study,
								gwtSubtype);
			} else {
				final EpilepsySubtypeEntity subtype = find(
						study.getEpilepsySubtypes(),
						compose(equalTo(gwtId), EpilepsySubtypeEntity.getId));
				if (!gwtSubtype.isDeleted()) {
					epilepsySubtypeAssembler.modifyEntity(subtype, gwtSubtype);
				} else {
					final Integer dbVersion = subtype.getVersion();
					final Integer gwtVersion = gwtSubtype.getVersion();
					if (!dbVersion.equals(gwtVersion)) {
						throw new StaleObjectException(
								"Version mismatch for epilepsy subtype ["
										+ gwtId + "]. DTO version: ["
										+ gwtVersion
										+ "], DB version: [" + dbVersion + "].");
					}
					study.removeEpilepsySubtype(subtype);
				}

			}
		}
	}
}
