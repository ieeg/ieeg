/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

/**
 * @author Sam Donnelly
 */
public class GenericHibernateDAOLongId<T extends IHasLongId>
		extends GenericHibernateDAO<T, Long>
		implements IDAOLongId<T> {}
