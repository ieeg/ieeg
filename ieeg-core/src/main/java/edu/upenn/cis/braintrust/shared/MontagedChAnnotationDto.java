/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newLinkedHashSet;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

@GwtCompatible(serializable = true)
public class MontagedChAnnotationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	public static Set<Long> toLongIds(Set<MontagedChId> montagedChIds) {
		Set<Long> longIds = newLinkedHashSet();
		for (MontagedChId montagedChId : montagedChIds) {
			longIds.add(montagedChId.getId());
		}
		return longIds;
	}

	private Optional<MontagedChAnnotationId> id;
	private LayerId parentId;
	private String type;
	private Optional<String> description;
	private Set<MontagedChId> montagedChIds = newLinkedHashSet();
	private Long startOffsetUsecs;
	private Long endOffsetUsecs;
	private UserId creatorId;
	private Date createTime;
	private Optional<String> color;

	/** For GWT */
	@SuppressWarnings("unused")
	private MontagedChAnnotationDto() {}

	public MontagedChAnnotationDto(
			LayerId parentId,
			String type,
			@Nullable String description,
			long startOffsetUsecs,
			long endOffsetUsecs,
			@Nullable String color,
			UserId creatorId,
			Set<MontagedChId> montagedChIds) {
		this.parentId = checkNotNull(parentId);
		this.type = checkNotNull(type);
		this.description = Optional.fromNullable(description);
		this.startOffsetUsecs = startOffsetUsecs;
		this.endOffsetUsecs = endOffsetUsecs;
		this.color = Optional.fromNullable(color);
		this.creatorId = checkNotNull(creatorId);
		this.montagedChIds.addAll(montagedChIds);
		this.id = Optional.absent();
	}

	public MontagedChAnnotationDto(
			LayerId layerId,
			String type,
			@Nullable String description,
			long startOffsetUsecs,
			long endOffsetUsecs,
			@Nullable String color,
			UserId creatorId,
			Set<MontagedChId> montagedChIds,
			MontagedChAnnotationId id) {
		this(
				layerId,
				type,
				description,
				startOffsetUsecs,
				endOffsetUsecs,
				color,
				creatorId,
				montagedChIds);
		this.id = Optional.of(id);
	}

	public Optional<String> getColor() {
		return color;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public UserId getCreatorId() {
		return creatorId;
	}

	public Optional<String> getDescription() {
		return description;
	}

	public Long getEndOffsetUsecs() {
		return endOffsetUsecs;
	}

	public Optional<MontagedChAnnotationId> getId() {
		return id;
	}

	public Set<MontagedChId> getMontagedChIds() {
		return montagedChIds;
	}

	public LayerId getParentId() {
		return parentId;
	}

	public Long getStartOffsetUsecs() {
		return startOffsetUsecs;
	}

	public String getType() {
		return type;
	}

	public void setColor(@Nullable String color) {
		this.color = Optional.fromNullable(color);
	}

	public void setCreateTime(Date createTime) {
		this.createTime = checkNotNull(createTime);
	}

	public void setCreatorId(UserId creatorId) {
		this.creatorId = checkNotNull(creatorId);
	}

	public void setDescription(@Nullable String description) {
		this.description = Optional.fromNullable(description);
	}

	public void setEndOffsetUsecs(long endOffsetUsecs) {
		this.endOffsetUsecs = endOffsetUsecs;
	}

	public void setMontagedChIds(
			Set<MontagedChId> montagedChIds) {
		this.montagedChIds.clear();
		this.montagedChIds.addAll(montagedChIds);
	}

	public void setStartOffsetUsecs(long startOffsetUsecs) {
		this.startOffsetUsecs = startOffsetUsecs;
	}

	public void setType(String type) {
		this.type = checkNotNull(type);
	}
}
