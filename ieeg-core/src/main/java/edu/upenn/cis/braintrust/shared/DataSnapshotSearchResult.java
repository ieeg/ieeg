/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IFeatureVector;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

/**
 * @author Sam Donnelly
 */
@Immutable
@GwtCompatible(serializable = true)
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NONE,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type"
		)
@JsonIgnoreProperties(ignoreUnknown=true)
public class DataSnapshotSearchResult extends PresentableMetadata
		implements IHasStringId, Serializable, IDataset {
	private static final long serialVersionUID = 1L;
	
	public static final TextColumn<DataSnapshotSearchResult> baseColumn = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			if (object.getBaseFriendly() != null)
				return object.getBaseFriendly();
			else
				return ("-");
		}
	};
	public static final ProvidesKey<DataSnapshotSearchResult> KEY_PROVIDER = new ProvidesKey<DataSnapshotSearchResult>() {

		public Object getKey(DataSnapshotSearchResult item) {
			return (item == null) ? null : item.getDatasetRevId();
		}

	};
	
	public static final TextColumn<DataSnapshotSearchResult> studyAnnotations = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			if (object.getBaseRevId() == null)
				return String.valueOf(object.getAnnotationCount());
			else
				return "-";
		}
	};
	public static final TextColumn<DataSnapshotSearchResult> studyChannels = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			if (object.getBaseRevId() == null)
				return String.valueOf(object.getTimeSeriesRevIds().size());
			else
				return "-";
		}
	};
	public static final TextColumn<DataSnapshotSearchResult> studyColumn = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			if (object.isHighlighted())
				return "*" + object.getFriendlyName() + "*";
			else
				return object.getFriendlyName();
		}
	};
	
	public static final TextColumn<DataSnapshotSearchResult> studyImages = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			return String.valueOf(object.getImageCount());
		}
	};
	public static final ProvidesKey<DataSnapshotSearchResult> studyKey = new ProvidesKey<DataSnapshotSearchResult>() {

		public Object getKey(DataSnapshotSearchResult item) {
			return (item == null) ? null : item.getDatasetRevId();// .getFriendlyName();
		}

	};
	
	public static final TextColumn<DataSnapshotSearchResult> studySeries = new TextColumn<DataSnapshotSearchResult>() {
		@Override
		public String getValue(DataSnapshotSearchResult object) {
			return String.valueOf(object.getDatasetRevId());
		}
	};

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("baseId", VALUE_TYPE.STRING);
		valueMap.put("baseLabel", VALUE_TYPE.STRING);
		// valueMap.put("base", VALUE_TYPE.META);
		valueMap.put("label", VALUE_TYPE.STRING);
		// valueMap.put("series", VALUE_TYPE.META_SET);
		valueMap.put("seriesIds", VALUE_TYPE.STRING_SET);
		valueMap.put("seriesLabels", VALUE_TYPE.STRING_SET);
		valueMap.put("contained", VALUE_TYPE.STRING_SET);
		valueMap.put("type", VALUE_TYPE.STRING);
		valueMap.put("creators", VALUE_TYPE.STRING_SET);

		valueMap.put("annotationCount", VALUE_TYPE.INT);
		valueMap.put("imageCount", VALUE_TYPE.INT);
		valueMap.put("startUutc", VALUE_TYPE.INT);
		valueMap.put("endUutc", VALUE_TYPE.INT);
		valueMap.put("organization", VALUE_TYPE.STRING);
		valueMap.put("derived", VALUE_TYPE.META_SET);
	}
	

	private Long endTimeUutc = null;
	private boolean frozen;
	private String id;
	private long imageCount;
	private String label;
	private String organization = null;
	private UserId owner;
	private String creator;
	private Long startTimeUutc = null;
	private Date createTime;
	boolean highlighted = false;

	private Set<TimeSeriesSearchResult> timeSeries;
	private long tsAnnCount;
	private DataSnapshotType type;

	String baseRevId;
	String baseFriendly;
	private boolean isLeaf = false;
	Boolean isValid = null;
	private boolean knowIfLeaf = false;
	String location = "habitat://main";

	SerializableMetadata parent;
	List<PresentableMetadata> collections = new ArrayList<PresentableMetadata>();
	
	IFeatureVector features;
	double termRate = 0.1;
	double score = 0;
	Set<String> termsMatched = new HashSet<String>();
	
	
	/** For GWT. */
	@SuppressWarnings("unused")
	private DataSnapshotSearchResult() {}

	public DataSnapshotSearchResult(
			DataSnapshotType type,
			String id,
			String label,
			Date createTime,
			@Nullable UserId owner,
			long imageCount,
			long tsAnnCount,
			boolean frozen) {
		this.type = checkNotNull(type);
		this.id = checkNotNull(id);
		this.label = checkNotNull(label);
		this.createTime = checkNotNull(createTime);
		this.owner = owner;
		this.imageCount = imageCount;
		this.tsAnnCount = tsAnnCount;
		this.frozen = frozen;
	}

	public DataSnapshotSearchResult(
			DataSnapshotType type,
			String revId,
			String label,
			Date createTime,
			@Nullable UserId owner,
			long imageCount,
			long tsAnnCount,
			boolean frozen,
			@Nullable Set<TimeSeriesSearchResult> timeSeries) {
		this(type, revId, label, createTime, owner, imageCount, tsAnnCount, frozen);
		this.timeSeries = timeSeries;
	}

	public DataSnapshotSearchResult(
			DataSnapshotType type,
			String id,
			String label,
			Date createTime,
			@Nullable UserId owner,
			long imageCount,
			long tsAnnCount,
			boolean frozen,
			@Nullable final Set<TimeSeriesSearchResult> timeSeries,
			Long startTimeUutc,
			Long endTimeUutc,
			String organization) {
		this(type, id, label, createTime, owner, imageCount, tsAnnCount, frozen);
		this.timeSeries = timeSeries;
		this.startTimeUutc = startTimeUutc;
		this.endTimeUutc = endTimeUutc;
		this.organization = organization;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DataSnapshotSearchResult)) {
			return false;
		}
		DataSnapshotSearchResult other = (DataSnapshotSearchResult) obj;
		return id.equals(other.id);
	}


	public Long getEndTimeUutc() {
		return endTimeUutc;
	}

	/**
	 * The revision id of the data snapshot
	 * 
	 * @return the revision id of the data snapshot
	 */
	@Override
	public String getId() {
		return id;
	}

	public long getImageCount() {
		return imageCount;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	public String getOrganization() {
		return organization;
	}

	@Nullable
	public UserId getOwner() {
		return owner;
	}

	public Long getStartTimeUutc() {
		return startTimeUutc;
	}

	@JsonIgnore
	public Set<TimeSeriesSearchResult> getTimeSeries() {
		return timeSeries;
	}

	public long getTsAnnCount() {
		return tsAnnCount;
	}

	public DataSnapshotType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@JsonIgnore
	@Transient
	public boolean isHighlighted() {
		return highlighted;
	}

	public boolean isFrozen() {
		return frozen;
	}

	public void setEndTimeUutc(Long endTimeUutc) {
		this.endTimeUutc = endTimeUutc;
	}

	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setImageCount(long imageCount) {
		this.imageCount = imageCount;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public void setOwner(UserId owner) {
		this.owner = owner;
	}
	
	public void setStartTimeUutc(Long startTimeUutc) {
		this.startTimeUutc = startTimeUutc;
	}
	
	public void setTimeSeries(Set<TimeSeriesSearchResult> timeSeries) {
		this.timeSeries = timeSeries;
	}
	
	public void setTsAnnCount(long tsAnnCount) {
		this.tsAnnCount = tsAnnCount;
	}
	
	public void setType(DataSnapshotType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "DataSnapshotSearchResult [type=" + type + ", revId=" + id
				+ ", label=" + label + ", timeSeries=" + timeSeries
				+ ", imageCount=" + imageCount + ", tsAnnCount=" + tsAnnCount
				+ ", frozen=" + frozen + ", owner=" + owner + "]";
	}
	
	@JsonIgnore
	@Transient
	public String getBaseRevId() {
		return baseRevId;
	}
	
	@JsonIgnore
	@Transient
	public String getBaseFriendly() {
		return baseFriendly;
	}

	@JsonIgnore
	public String getFriendlyName() {
		return getLabel();
	}
	
	@JsonIgnore
	public String getDatasetRevId() {
		return id;
	}
	
	@JsonIgnore
	public long getAnnotationCount() {
		return getTsAnnCount();
	}

	@JsonIgnore
	@Transient
	public List<String> getTimeSeriesLabels() {
//		return tsLabels;
		List<String> ret = new ArrayList<String>(timeSeries.size());
		for (TimeSeriesSearchResult tsr: timeSeries) {
			ret.add(tsr.getLabel());
		}
		return ret;
	}

	@JsonIgnore
	@Transient
	public List<String> getTimeSeriesRevIds() {
//		return tsRevIds;
		List<String> ret = new ArrayList<String>(timeSeries.size());
		for (TimeSeriesSearchResult tsr: timeSeries) {
			ret.add(tsr.getRevId());
		}
		return ret;
	}


	/////////////////////////////////////
	// Do we want to merge with SearchResult??

	public void addDerived(PresentableMetadata dr) {
		if (dr != this && !dr.getId().equals(getId())) {
			collections.add(dr);
			if (dr instanceof TraceInfo) {
//				tsRevIds.add(dr.getId());
//				tsLabels.add(dr.getLabel());
				timeSeries.add(new TimeSeriesSearchResult(dr.getId(), dr.getLabel()));
			}
		}
	}

	public void addTermMatched(String term) {
		termsMatched.add(term);
	}

	public void addTimeSeries(String revId, String label) {
		timeSeries.add(new TimeSeriesSearchResult(revId, label));
	}

	public void addTimeSeries(TimeSeriesSearchResult ts) {
		timeSeries.add(ts);
	}

//	public int compareTo(DataSnapshotSearchResult o) {
//		if (!(o instanceof DataSnapshotSearchResult))
//			throw new RuntimeException("Incompatible types!");
//		else
//			return (getFriendlyName().compareTo(((DataSnapshotSearchResult) o).getFriendlyName()));
//	}

	@JsonIgnore
	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.EEG, BuiltinDataTypes.MRI, BuiltinDataTypes.CT,
				BuiltinDataTypes.DCN, BuiltinDataTypes.PDF);
	}

	@JsonIgnore
	@Override
	public String getContentDescriptor() {
		return tsAnnCount + " annotations";
	}

	@Override
	public Date getCreationTime() {
		return createTime;
	}

	public String getCreator() {
		return creator;
	}

	@JsonIgnore
	@Override
	public List<String> getCreators() {
		List<String> ret = Lists.newArrayList();

		ret.add(creator);

		return ret;
	}

	@JsonIgnore
	public List<PresentableMetadata> getDerived() {
		return collections;
	}
	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	@JsonIgnore
	public Long getEndUutc() {
		return endTimeUutc;
	}
	@JsonIgnore
	public IFeatureVector getFeatures() {
		return features;
	}

	public boolean getHighlighted() {
		return highlighted;
	}

	@JsonIgnore
	@Override
	public Integer getIntegerValue(String key) {
		if (key.equals("annotationCount"))
			return (int) getAnnotationCount();
		if (key.equals("imageCount"))
			return (int) getImageCount();
		return null;
	}

	@JsonIgnore
	public Boolean getIsValid() {
		return isValid;
	}

	@JsonIgnore
	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return valueMap.keySet();
	}

	@JsonIgnore
	@Override
	public String getLocation() {
		return location;
	}

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Snapshot.name();
	}

	@JsonIgnore
	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		if (key.equals("derived"))
			return this.collections;

		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	@JsonIgnore
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public double getRelevanceScore() {
		return 1;
	}

	@JsonIgnore
	@Transient
	public double getScore() {
		return score + termRate * termsMatched.size();
	}

	@JsonIgnore
	public Long getStartUutc() {
		return startTimeUutc;
	}

	@JsonIgnore
	@Override
	public List<String> getStringSetValue(String key) {
		if (key.equals("creators")) {
			return getCreators();
		}
		if (key.equals("seriesIds"))
			return this.getTimeSeriesRevIds();
		if (key.equals("seriesLabels"))
			return this.getTimeSeriesLabels();

		return null;
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return this.getDatasetRevId();
		if (key.equals("baseId"))
			return this.getBaseRevId();
		if (key.equals("baseLabel"))
			return this.getBaseFriendly();
		if (key.equals("label"))
			return this.getFriendlyName();
		if (key.equals("type"))
			return this.getType().toString();
		if (key.equals("organization"))
			return getOrganization();

		return null;
	}

	/**
	 * How much to weight term matches?
	 * 
	 * @return
	 */
	@JsonIgnore
	@Transient
	public double getTermRate() {
		return termRate;
	}

	@JsonIgnore
	@Transient
	public Set<String> getTermsMatched() {
		return termsMatched;
	}

	@JsonIgnore
//	@Override
	public Date getUpdateTime() {
		return getCreationTime();
	}

	@JsonIgnore
//	@Override
	public Map<String, Set<String>> getUserPermissions() {
		// TODO Auto-generated method stub
		return new HashMap<String,Set<String>>();
	}

	@JsonIgnore
//	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	public void highlight() {
		highlighted = true;
	}

	@JsonIgnore
	@Override
	public boolean isLeaf() {
		return false;
	}
	
	public void setAnnotationCount(long annotationCount) {
		this.tsAnnCount = annotationCount;
	}

	public void setBaseFriendly(String bf) {
		this.baseFriendly = bf;
	}

	public void setBaseRevId(String dsRevId) {
		this.baseRevId = dsRevId;
	}

	public void setCreationTime(Date date) {
		this.createTime = date;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public void setDatasetRevId(String dsRevId) {
		this.id = dsRevId;
	}

	public void setEndUutc(@Nullable Long endUutc) {
		this.endTimeUutc = endUutc;
	}
	
	public void setFeatures(IFeatureVector features) {
		this.features = features;
	}
	
	public void setHighlighted(boolean h) {
		this.highlighted = h;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public void setLeafStatus(boolean isLeaf) {
		this.isLeaf = isLeaf;
		knowIfLeaf = true;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			parent = (SerializableMetadata)p;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public void setStartUutc(@Nullable Long startUutc) {
		this.startTimeUutc = startUutc;
	}
	
	public void setTermRate(double termRate) {
		this.termRate = termRate;
	}
	
	public void setTermsMatched(Set<String> termsMatched) {
		this.termsMatched = termsMatched;
	}
	
	public void setTimeSeries(List<TimeSeriesSearchResult> timeSeries2) {
		timeSeries.addAll(timeSeries2);
	}
	
	public void unhighlight() {
		highlighted = false;
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Dataset";
	}
}
