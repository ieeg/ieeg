/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A name for an {@link Experiment}. Is 15 characters long and has the form
 * "Innn_Annnn_Dnnn" where the n are [0-9].
 * 
 * @author John Frommeyer
 * 
 */
public final class ExperimentName {
	/**
	 * Group 1 is 'I' followed by 3 digits. Group 2 is 'A' followed by 4 digits.
	 * Group 3 is 'D' followed by 3 digits. Groups are separated by '_'
	 */
	private final static Pattern pattern = Pattern
			.compile("^(I\\d{3})_(A\\d{4})_(D\\d{3})$");
	private final String organizationPart;
	private final String animalPart;
	private final String experimentPart;

	public ExperimentName(String name) {
		final Matcher matcher = pattern.matcher(name);
		checkArgument(matcher.matches(), name
				+ " is not a valid Experiment name");
		organizationPart = checkNotNull(matcher.group(1));
		animalPart = checkNotNull(matcher.group(2));
		experimentPart = checkNotNull(matcher.group(3));
	}

	public static boolean isExperimentName(String candidate) {
		final Matcher matcher = pattern.matcher(candidate);
		return matcher.matches();
	}

	/**
	 * Returns the organization part of the name. For example I001.
	 * 
	 * @return the organization part
	 */
	public String getOrganizationPart() {
		return organizationPart;
	}

	/**
	 * Returns the animal part of the name. For example A0001.
	 * 
	 * @return the animalPart
	 */
	public String getAnimalPart() {
		return animalPart;
	}

	/**
	 * Returns the experiment part of the name. For example D01.
	 * 
	 * @return the experimentPart
	 */
	public String getExperimentPart() {
		return experimentPart;
	}

	/**
	 * Returns the directory implied by this name.
	 * 
	 * @return the directory
	 */
	public String getDir() {
		return "Animal_Data/" + organizationPart + "/" + animalPart + "/"
				+ experimentPart;
	}

	/**
	 * Returns the mef directory implied by this name.
	 * 
	 * @return the mef directory
	 */
	public String getMefDir() {
		return getDir() + "/" + "mef";
	}

	/**
	 * Returns the animal name implied by this name. For example I001_A0001
	 * 
	 * @return the animal name
	 */
	public String getAnimalName() {
		return organizationPart + "_" + animalPart;
	}

	@Override
	public String toString() {
		return organizationPart + "_" + animalPart + "_" + experimentPart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((animalPart == null) ? 0 : animalPart.hashCode());
		result = prime * result
				+ ((experimentPart == null) ? 0 : experimentPart.hashCode());
		result = prime
				* result
				+ ((organizationPart == null) ? 0 : organizationPart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExperimentName)) {
			return false;
		}
		ExperimentName other = (ExperimentName) obj;
		if (animalPart == null) {
			if (other.animalPart != null) {
				return false;
			}
		} else if (!animalPart.equals(other.animalPart)) {
			return false;
		}
		if (experimentPart == null) {
			if (other.experimentPart != null) {
				return false;
			}
		} else if (!experimentPart.equals(other.experimentPart)) {
			return false;
		}
		if (organizationPart == null) {
			if (other.organizationPart != null) {
				return false;
			}
		} else if (!organizationPart.equals(other.organizationPart)) {
			return false;
		}
		return true;
	}

}
