/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * * @author John Frommeyer
 * 
 */
@NamedQueries({ @NamedQuery(
		name = StrainEntity.BY_PARENT_ORDER_BY_LABEL,
		query = "select s "
				+ "from Strain s "
				+ "where s.parent = :parent "
				+ "order by s.label asc") })
@Entity(name = "Strain")
@Table(name = StrainEntity.TABLE,
		uniqueConstraints =
		@UniqueConstraint(columnNames = {
				StrainEntity.LABEL_COLUMN, StrainEntity.PARENT_COLUMN }))
public class StrainEntity implements IHasLongId, IHasLabel {
	public static final String TABLE = "strain";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String LABEL_COLUMN = "label";
	public static final String PARENT_COLUMN = "parent_id";
	public static final String BY_PARENT_ORDER_BY_LABEL = "Strain-findByParentOrderedByLabel";
	private Long id;
	private Integer version;
	private String label;
	private SpeciesEntity parent;

	StrainEntity() {}

	/**
	 * This constructor sets both sides of the Species/Strain relationship.
	 * 
	 * @param parent
	 * @param label
	 */
	public StrainEntity(SpeciesEntity parent, String label) {
		this.parent = checkNotNull(parent);
		this.parent.getStrains().add(this);
		this.label = checkNotNull(label);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(name = LABEL_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getLabel() {
		return label;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = PARENT_COLUMN)
	@NotNull
	public SpeciesEntity getParent() {
		return parent;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setParent(SpeciesEntity parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
