/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.annotation.Nullable;
import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class BtUtil {

	private static Logger logger = LoggerFactory
			.getLogger(BtUtil.class);

	public static <K, V> V get(
			final Map<K, V> map,
			final K key,
			@Nullable final V defValue) {
		if (map.containsKey(key)) {
			return map.get(key);
		} else {
			return defValue;
		}
	}

	/**
	 * 
	 * @param props
	 * @param key
	 * @param defValue
	 * @return
	 */
	public static boolean getBoolean(
			final Map<String, String> props,
			final String key,
			final boolean defValue) {
		return Boolean
				.valueOf(BtUtil.get(props, key, String.valueOf(defValue)));
	}

	/**
	 * Will not have a leading "/".
	 * 
	 * @param context
	 * @param configFile
	 * @return
	 */
	public static String getConfigName(
			final ServletContext context,
			final String configFile) {
		return getConfigName(getWebAppName(context), configFile);
	}

	public static String getConfigName(
			String prefix,
			String config) {
		checkArgument(!isNullOrEmpty(prefix), "illegal arg [" + prefix + "]");
		return getConfigPath(prefix) + "/" + config;
	}

	/**
	 * Will not have a leading "/" or a trailing "/".
	 * 
	 * @param context
	 * 
	 * @return
	 */
	public static String getWebAppName(final ServletContext context) {
		String appName = context.getContextPath();
		if (appName.equals("")) {
			appName = "ROOT";
		} else {
			// get rid of leading "/"
			appName = appName.substring(1);
		}
		return appName;
	}

	/**
	 * Will not have a trailing "/".
	 * 
	 * @param context
	 * 
	 * @return
	 */
	public static String getConfigPath(String prefix) {
		return prefix + "/config";
	}

	public static int getInt(
			final Map<String, String> props,
			final String key,
			final int defValue) {
		return Integer
				.valueOf(BtUtil.get(props, key, String.valueOf(defValue)));
	}

	public static long getLong(
			final Map<String, String> props,
			final String key,
			final long defValue) {
		return Long
				.valueOf(BtUtil.get(props, key, String.valueOf(defValue)));
	}

	public static double getDouble(
			final Map<String, String> props,
			final String key,
			final double defValue) {
		return Double.valueOf(BtUtil.get(props, key, String.valueOf(defValue)));
	}

	public static Properties loadProps(final URL url) throws IOException {
		InputStream is = null;
		try {
			is = url.openStream();
			final Properties props = new Properties();
			props.load(is);
			return props;
		} finally {
			close(is);
		}
	}

	/**
	 * Shorthand for:
	 * 
	 * <pre>
	 * UUID.randomUUID().toString();
	 * </pre>
	 * 
	 * @return
	 */
	public static String newUuid() {
		return UUID.randomUUID().toString();
	}

	public static double diffNowMsThenSeconds(Date thenMillis) {
		return (System.currentTimeMillis() - thenMillis.getTime()) / 1000.0;
	}

	public static double diffNowMsThenSeconds(long thenMillis) {
		return (System.currentTimeMillis() - thenMillis) / 1000.0;
	}

	public static double diffNowThenSeconds(long thenNanos) {
		return (System.nanoTime() - thenNanos) / 1E9;
	}

	public static double diffNowThenMillis(long thenNanos) {
		return (System.nanoTime() - thenNanos) / 1E6;
	}

	public static void close(@Nullable Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (Exception e) {
				logger.error("caught exception closing", e);
			}
		}
	}

	public static long secs2Nanos(int secs) {
		return secs * (long) 1E9;
	}

	public static String nullSafeTrim(@Nullable String string) {
		return string == null ? null : string.trim();
	}

	private BtUtil() {
		throw new AssertionError("can't instantiate a "
				+ BtUtil.class.getSimpleName());
	}

}
