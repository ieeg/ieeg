package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity(name = "EegMontagePair")
@Table(name = "eeg_montage_pair",
    uniqueConstraints = @UniqueConstraint(
        columnNames = {
            EegMontagePairEntity.PARENT_ID_COLUMN,
            EegMontagePairEntity.INDEX_COLUMN
        }
        ))
@AttributeOverrides({
  @AttributeOverride(
      name = "id",
      column = @Column(
          name = EegMontagePairEntity.ID_COLUMN)
      )
})
public class EegMontagePairEntity extends LongIdAndVersion{
  
  public final static String TABLE = "eeg_montage_pair";
  public final static String ID_COLUMN = TABLE + "_id";
  
  public final static String PARENT_ID_COLUMN = "parent_id";
  public final static String INDEX_COLUMN = "idx";
  
  private EegMontageEntity parent;
   String el1;
   String el2;
   Integer index;

   EegMontagePairEntity(){}
   
   public EegMontagePairEntity(EegMontageEntity parent, String el1, @Nullable String el2, Integer index){
     
     
     this.parent = checkNotNull(parent);
     this.el1 = checkNotNull(el1);
     this.el2 = el2;
     this.index = checkNotNull(index);
     
     checkArgument(
         el1.length() >=1,
         "Montage channelname too short.");
     checkArgument(
         el1.length() < 256,
         "Montage channelname too long.");
     
     if(el2 != null){
       checkArgument(
           el2.length() >=1,
           "Montage channelname too short.");
       checkArgument(
           el2.length() < 256,
           "Montage channelname too long.");
     }
   }

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = PARENT_ID_COLUMN)
   @NotNull
  private EegMontageEntity getParent() {
    return parent;
  }

  @SuppressWarnings("unused")
  private void setParent(EegMontageEntity parent) {
    this.parent = parent;
  }

  @NotNull
  @Size(min = 1, max = 255)
  public String getEl1() {
    return el1;
  }

  @SuppressWarnings("unused")
  private void setEl1(String el1) {
    this.el1 = el1;
  }

  @Size(min = 1, max = 255)
  public String getEl2() {
    return el2;
  }

  @SuppressWarnings("unused")
  private void setEl2(String el2) {
    this.el2 = el2;
  }

  @NotNull
  @Column(name = INDEX_COLUMN)
  public Integer getIndex() {
    return index;
  }

  @SuppressWarnings("unused")
  private void setIndex(Integer index) {
    this.index = index;
  }
   
   
}
