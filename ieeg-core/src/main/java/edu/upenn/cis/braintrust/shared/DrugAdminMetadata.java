/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.VALUE_TYPE;

/**
 * Drug admin.
 * 
 * @author Sam Donnelly
 */
@Immutable
@GwtCompatible(serializable = true)
public final class DrugAdminMetadata implements Serializable, SerializableMetadata {

	public final static Function<DrugAdminMetadata, String> getDrug = new Function<DrugAdminMetadata, String>() {

		@Override
		public String apply(DrugAdminMetadata input) {
			return input.getDrug();
		}
	};

	private static final long serialVersionUID = 1L;

	private String drug;

	private Date adminTime;
	private BigDecimal doseMgPerKg;
	private Long id;

	SerializableMetadata parent = null;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("drug", VALUE_TYPE.STRING);
		valueMap.put("adminTime", VALUE_TYPE.STRING);
		valueMap.put("doseMgPerKg", VALUE_TYPE.STRING);
	}

	/** For GWT. */
	@SuppressWarnings("unused")
	private DrugAdminMetadata() {}

	public DrugAdminMetadata(
			Long id,
			String drug,
			@Nullable Date adminTime,
			@Nullable BigDecimal doseMgPerKg) {
		this.id = checkNotNull(id);
		this.drug = checkNotNull(drug);
		this.adminTime = adminTime;
		this.doseMgPerKg = doseMgPerKg;
	}

	public Date getAdminTime() {
		return adminTime;
	}

	public BigDecimal getDoseMgPerKg() {
		return doseMgPerKg;
	}

	public String getDrug() {
		return drug;
	}

	public Long getIdValue() {
		return id;
	}

	@Override
	public String toString() {
		return "DrugAdminMetadata [drug=" + drug + ", adminTime=" + adminTime
				+ ", doseMgPerKg=" + doseMgPerKg + ", id=" + id + "]";
	}

	@Override
	public String getMetadataCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("drug"))
			return getDrug();
		else if (key.equals("adminTime"))
			return getAdminTime().toGMTString();
		else if (key.equals("doseMgPerKg"))
			return getDoseMgPerKg().toString();

		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		parent = (SerializableMetadata)p;
	}

	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

}
