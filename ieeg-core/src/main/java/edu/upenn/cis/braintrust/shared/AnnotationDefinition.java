/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class AnnotationDefinition implements Serializable {
	/**
'1','HFO','clinical','0','1','eeg'
'2','Spike','clinical','0','1','eeg'
'3','Low Quality','portal','0','2','eeg'
'4','Standard Quality','portal','0','2','eeg'
'5','High Quality','portal','0','2','eeg'
'6','Note','clinical','1','1','eeg'
'7','Artifact','clinical','0','2','eeg'
'8','Time Segment','portal','0','2','eeg'
'9','Bookmark','clinical','1','1','eeg'
'10','Time Span of Interest','clinical','1','2','eeg'
'11','Valid Data','portal','0','2','eeg'
'12','Talking','clinical','1','2','eeg'
'13','Movement','clinical','1','2','eeg'
'14','Clenched Fist','clinical','1','2','eeg'
'15','Discussion Ref Time','portal','1','1','eeg'
'16','Frequency','portal','1','1','eeg'
'17','Filter Type','portal','1','1','eeg'
'18','Filter Pass','portal','1','2','eeg'
'19','Filter Stop','portal','1','2','eeg'
'20','Seizure','clinical','0','2','eeg'
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long shortId;

	private String viewer;
	private String annotationName;
	
	private int numberOfPoints;
	
	private String category;
	
	private Boolean global;


	public AnnotationDefinition() {
	}

	

	public AnnotationDefinition(Long shortId, String viewer,
			String annotationName, int numberOfPoints, String category,
			Boolean global) {
		super();
		this.shortId = shortId;
		this.viewer = viewer;
		this.annotationName = annotationName;
		this.numberOfPoints = numberOfPoints;
		this.category = category;
		this.global = global;
	}



	public Long getShortId() {
		return shortId;
	}


	public void setShortId(Long shortId) {
		this.shortId = shortId;
	}


	public String getViewer() {
		return viewer;
	}


	public void setViewer(String viewer) {
		this.viewer = viewer;
	}


	public String getAnnotationName() {
		return annotationName;
	}


	public void setAnnotationName(String annotationName) {
		this.annotationName = annotationName;
	}


	public int getNumberOfPoints() {
		return numberOfPoints;
	}


	public void setNumberOfPoints(int numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public Boolean getGlobal() {
		return global;
	}


	public void setGlobal(Boolean global) {
		this.global = global;
	}
	

}
