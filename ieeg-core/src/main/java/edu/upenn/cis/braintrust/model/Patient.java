/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * Contains information about a patient which is constant across multiple
 * admissions. Information specific to a particular admission goes in
 * {@link HospitalAdmission}.
 * 
 * @author Sam Donnelly
 */
@NamedQueries({
		@NamedQuery(name = Patient.PATIENT_BY_LABEL_EAGER,
				query = "from Patient p "
						+ "left join fetch p.admissions c "
						+ "left join fetch c.studies s "
						+ "left join fetch s.recording rec "
						+ "left join fetch rec.contactGroups g "
						+ "left join fetch p.precipitants "
						+ "left join fetch p.szTypes "
						+ "where p.label = :label "),
		@NamedQuery(name = Patient.PATIENT_BY_ID_EAGER,
				query = "from Patient p "
						+ "left join fetch p.admissions c "
						+ "left join fetch c.studies s "
						+ "left join fetch s.recording rec "
						+ "left join fetch rec.contactGroups g "
						+ "left join fetch p.precipitants "
						+ "left join fetch p.szTypes "
						+ "where p.id = :id "),
		@NamedQuery(name = "Patient-retrieveEtiologies",
				query = "select distinct p.etiology from Patient p") })
@Entity
@Table(name = Patient.TABLE)
@PrimaryKeyJoinColumn(name = Patient.ID_COLUMN)
public class Patient extends Subject {

	public static final String TABLE = "patient";
	public static final String PATIENT_BY_ID_EAGER = "Patient-findByIdEager";
	public static final String PATIENT_BY_LABEL_EAGER = "Patient-retrieveByUserSuppliedIdEager";

	public static final String ID_COLUMN = TABLE + "_id";

	private Handedness handedness;
	private Ethnicity ethnicity;
	private Gender gender;
	private String etiology;
	private String developmentalDelay;
	private Boolean developmentalDisorders;
	private Boolean traumaticBrainInjury;
	private Boolean familyHistory;
	private Integer ageOfOnset;
	private Set<HospitalAdmission> admissions = newHashSet();
	private Set<Precipitant> precipitants = newHashSet();
	private Set<SeizureType> szTypes = newHashSet();

	public Patient() {}

	public Patient(
			String label,
			OrganizationEntity institution,
			Handedness handedness,
			Ethnicity ethnicity,
			Gender gender,
			String etiology,
			@Nullable Boolean developmentalDisorders,
			@Nullable Boolean traumaticBrainInjury,
			@Nullable Boolean familyHistory,
			@Nullable Integer ageOfOnset,
			@Nullable String developmentalDelay) {
		super(label, institution);
		setHandedness(handedness);
		setEthnicity(ethnicity);
		setGender(gender);
		this.etiology = checkNotNull(etiology);
		this.developmentalDisorders = developmentalDisorders;
		this.traumaticBrainInjury = traumaticBrainInjury;
		this.familyHistory = familyHistory;
		this.ageOfOnset = ageOfOnset;
		this.developmentalDelay = developmentalDelay;
	}

	public Patient(
			String label,
			OrganizationEntity institution,
			String etiology) {
		super(label, institution);
		setHandedness(Handedness.UNKNOWN);
		setEthnicity(Ethnicity.UNKNOWN);
		setGender(Gender.UNKOWN);
		this.etiology = checkNotNull(etiology);
		this.developmentalDisorders = null;
		this.traumaticBrainInjury = null;
		this.familyHistory = null;
		this.ageOfOnset = null;
		this.developmentalDelay = null;
	}

	/**
	 * Add the admission to this patient and set this patient as the admission's
	 * parent.
	 * 
	 * @param admission case to be added to this patient case and have its
	 *            parent set to this patient
	 * 
	 * @throws IllegalArgumentException if {@code admission} already has a
	 *             parent
	 * @throws IllegalArgumentException if this patient already contains
	 *             {@code admission}
	 */
	public void addAdmission(final HospitalAdmission admission) {
		checkNotNull(admission);
		checkArgument(admission.getParent() == null,
				"epilepsyCase already has a parent");
		checkArgument(!admissions.contains(admission),
				"epilepsyCase is already contained in this patient");
		admissions.add(admission);
		admission.setParent(this);
	}

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<HospitalAdmission> getAdmissions() {
		return admissions;
	}

	/**
	 * @return the ageOfOnset
	 */
	public Integer getAgeOfOnset() {
		return ageOfOnset;
	}

	/**
	 * @return the developmentalDelay
	 */
	public String getDevelopmentalDelay() {
		return developmentalDelay;
	}

	/**
	 * Only {@code null} until set. Won't be {@code null} for a persistent case.
	 * 
	 * @return the developmentalDisorders
	 */
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean getDevelopmentalDisorders() {
		return developmentalDisorders;
	}

	@NotNull
	public Ethnicity getEthnicity() {
		return ethnicity;
	}

	/**
	 * @return the etiology
	 */
//	@NotNull
	public String getEtiology() {
		return etiology;
	}

	/**
	 * @return the familyHistory
	 */
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean getFamilyHistory() {
		return familyHistory;
	}

	@NotNull
	public Gender getGender() {
		return gender;
	}

	@NotNull
	public Handedness getHandedness() {
		return handedness;
	}

	/**
	 * @return the situations
	 */
	@ElementCollection
	@CollectionTable(
			name = TABLE + "_precipitant",
			joinColumns = @JoinColumn(name = ID_COLUMN))
	@Column(name = "precipitant", nullable = false)
	public Set<Precipitant> getPrecipitants() {
		return precipitants;
	}

	/**
	 * @return the szTypes
	 */
	@ElementCollection
	@CollectionTable(name = TABLE + "_seizure_type",
			joinColumns = @JoinColumn(name = ID_COLUMN))
	@Column(name = "seizure_type", nullable = false)
	public Set<SeizureType> getSzTypes() {
		return szTypes;
	}

	/**
	 * @return the traumaticBrainInjury
	 */
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean getTraumaticBrainInjury() {
		return traumaticBrainInjury;
	}

	/**
	 * Remove {@code epilepsyCase} from this patient and {@code null} out
	 * {@code epilepsyCase}'s parent.
	 * 
	 * @param epilepsyCase to be removed and deparented
	 * 
	 * @throws IllegalArgumentException if this patient is not the cases's
	 *             parent
	 * @throws IllegalArgumentException if the case is not contained in this
	 *             patient
	 */
	public void removeAdmission(final HospitalAdmission admission) {
		checkNotNull(admission);
		checkArgument(equals(admission.getParent()),
				"this patient is not admission's parent");
		checkArgument(admissions.contains(admission),
				"this patient does not contain admission");
		admissions.remove(admission);
		admission.setParent(null);
	}

	@SuppressWarnings("unused")
	private void setAdmissions(final Set<HospitalAdmission> cases) {
		this.admissions = cases;
	}

	/**
	 * @param ageOfOnset the ageOfOnset to set
	 */
	public void setAgeOfOnset(@Nullable Integer ageOfOnset) {
		this.ageOfOnset = ageOfOnset;
	}

	/**
	 * @param developmentalDelay the developmentalDelay to set
	 */
	public void setDevelopmentalDelay(
			@Nullable String developmentalDelay) {
		this.developmentalDelay = developmentalDelay;
	}

	/**
	 * @param developmentalDisorders the developmentalDisorders to set
	 */
	public void setDevelopmentalDisorders(final Boolean developmentalDisorders) {
		this.developmentalDisorders = developmentalDisorders;
	}

	public void setEthnicity(final Ethnicity ethnicity) {
		this.ethnicity = (ethnicity != null) ? ethnicity: Ethnicity.UNKNOWN;
	}

	/**
	 * @param etiology the etiology to set
	 */
	public void setEtiology(final String etiology) {
		this.etiology = etiology;
	}

	/**
	 * @param familyHistory the familyHistory to set
	 */
	public void setFamilyHistory(final Boolean familyHistory) {
		this.familyHistory = familyHistory;
	}

	public void setGender(final Gender gender) {
		this.gender = (gender != null) ? gender: Gender.UNKOWN;
	}

	public void setHandedness(final Handedness handedness) {
		this.handedness = (handedness != null) ? handedness : Handedness.UNKNOWN;
	}

	/**
	 * @param situations the situations to set
	 */
	@SuppressWarnings("unused")
	private void setPrecipitants(final Set<Precipitant> situations) {
		this.precipitants = situations;
	}

	/**
	 * @param szTypes the szTypes to set
	 */
	@SuppressWarnings("unused")
	private void setSzTypes(final Set<SeizureType> szTypes) {
		this.szTypes = szTypes;
	}

	/**
	 * @param traumaticBrainInjury the traumaticBrainInjury to set
	 */
	public void setTraumaticBrainInjury(final Boolean traumaticBrainInjury) {
		this.traumaticBrainInjury = traumaticBrainInjury;
	}

	public static final EntityConstructor<Patient, String, OrganizationEntity> CONSTRUCTOR =
		new EntityConstructor<Patient,String,OrganizationEntity>(Patient.class,
				new IPersistentObjectManager.IPersistentKey<Patient,String>() {

			@Override
			public String getKey(Patient o) {
				return o.getLabel();
			}

			@Override
			public void setKey(Patient o, String newKey) {
				o.setLabel(newKey);
			}

		},
		new EntityPersistence.ICreateObject<Patient,String,OrganizationEntity>() {

			@Override
			public Patient create(String label, OrganizationEntity optParent) {
				// Null etiology
				return new Patient(label, optParent, null);
			}
		}
				);

		@Override
	public EntityConstructor<Patient, String, OrganizationEntity> tableConstructor() {
		return CONSTRUCTOR;
	}

}
