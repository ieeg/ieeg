/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

import java.util.Set;

import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotService;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.dto.GwtHospitalAdmission;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.exception.BrainTrustUserException;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class PatientAssembler {

	private final OrganizationAssembler organizationAssembler;
	private final HospitalAdmissionAssembler admissionAssembler;
	private final ITsAnnotationDAO annotationDAO;
	private final IOrganizationDAO organizationDAO;
	private final IDataSnapshotService dsService;

	public PatientAssembler(
			IPermissionDAO permissionDAO,
			ITsAnnotationDAO annotationDAO,
			IOrganizationDAO organizationDAO, IDataSnapshotService dsService) {
		checkNotNull(permissionDAO);
		this.annotationDAO = checkNotNull(annotationDAO);
		this.organizationDAO = checkNotNull(organizationDAO);
		this.dsService = checkNotNull(dsService);
		organizationAssembler = new OrganizationAssembler();
		admissionAssembler = new HospitalAdmissionAssembler(
				permissionDAO,
				annotationDAO,
				dsService);
	}

	public GwtPatient buildDto(final Patient patient) {
		final GwtPatient gwtPatient = new GwtPatient();
		gwtPatient.setId(patient.getId());
		gwtPatient.setVersion(patient.getVersion());
		gwtPatient.setAgeOfOnset(patient.getAgeOfOnset());
		gwtPatient.setDevelopmentalDelay(patient.getDevelopmentalDelay());
		gwtPatient.setDevelopmentalDisorders(patient
				.getDevelopmentalDisorders());
		gwtPatient.setEthnicity(patient.getEthnicity());
		gwtPatient.setEtiology(patient.getEtiology());
		gwtPatient.setFamilyHistory(patient.getFamilyHistory());
		gwtPatient.setGender(patient.getGender());
		gwtPatient.setHandedness(patient.getHandedness());
		final GwtOrganization gwtOrganization = organizationAssembler
				.buildDto(patient.getOrganization());
		gwtPatient.setOrganization(gwtOrganization);
		gwtPatient.setTraumaticBrainInjury(patient.getTraumaticBrainInjury());
		gwtPatient.setLabel(patient.getLabel());
		final Set<Precipitant> precipitants = patient.getPrecipitants();
		gwtPatient.getPrecipitants().addAll(precipitants);

		final Set<SeizureType> szTypes = patient.getSzTypes();
		gwtPatient.getSzTypes().addAll(szTypes);

		final Set<HospitalAdmission> admissions = patient.getAdmissions();
		for (HospitalAdmission admission : admissions) {
			gwtPatient.addAdmission(admissionAssembler.buildDto(admission));
		}

		return gwtPatient;
	}

	public Patient buildEntity(
			final GwtPatient gwtPatient) {
		checkNotNull(gwtPatient);
		checkArgument(gwtPatient.getId() == null);
		checkArgument(gwtPatient.getVersion() == null);
		// A new patient should have no admissions.
		checkArgument(gwtPatient.getAdmissions().isEmpty(),
				"A new patient should have no admissions.");
		final Patient patient = new Patient();
		copyToEntity(patient, gwtPatient);
		return patient;
	}

	public void modifyEntity(
			final Patient patient,
			final GwtPatient gwtPatient) throws StaleObjectException,
			BrainTrustUserException {
		checkNotNull(patient);
		checkNotNull(gwtPatient);
		final Long gwtId = gwtPatient.getId();
		final Long dbId = patient.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtPatient.getVersion();
		final Integer dbVer = patient.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for patient ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		handleAdmisssions(patient, gwtPatient);
		copyToEntity(patient, gwtPatient);
	}

	private void handleAdmisssions(Patient patient, GwtPatient gwtPatient)
			throws StaleObjectException, BrainTrustUserException {
		for (final GwtHospitalAdmission gwtAdmission : gwtPatient
				.getAdmissions()) {
			final Long gwtId = gwtAdmission.getId();
			if (gwtId == null) {
				final HospitalAdmission admission = admissionAssembler
						.buildEntity(
						gwtAdmission);
				patient.addAdmission(admission);
			} else {
				final HospitalAdmission admission = find(
						patient.getAdmissions(),
						compose(equalTo(gwtId), HospitalAdmission.getId));
				if (!gwtAdmission.isDeleted()) {
					admissionAssembler.modifyEntity(admission, gwtAdmission);
				} else {
					final Integer dbVersion = admission.getVersion();
					final Integer gwtVersion = gwtAdmission.getVersion();
					if (!dbVersion.equals(gwtVersion)) {
						throw new StaleObjectException(
								"Version mismatch for hospital admission ["
										+ gwtId + "]. DTO version: ["
										+ gwtVersion
										+ "], DB version: [" + dbVersion + "].");
					}
					removeAdmissionOrThrowException(patient, admission,
							annotationDAO, dsService);
				}

			}
		}
	}

	private void copyToEntity(final Patient patient, final GwtPatient gwtPatient) {
		patient.setAgeOfOnset(gwtPatient.getAgeOfOnset().orNull());
		patient.setDevelopmentalDelay(gwtPatient.getDevelopmentalDelay().orNull());
		patient.setDevelopmentalDisorders(gwtPatient
				.getDevelopmentalDisorders().orNull());
		patient.setEthnicity(gwtPatient.getEthnicity());
		patient.setEtiology(gwtPatient.getEtiology());
		patient.setFamilyHistory(gwtPatient.getFamilyHistory().orNull());
		patient.setGender(gwtPatient.getGender());
		patient.setHandedness(gwtPatient.getHandedness());
		final Long organizationId = gwtPatient.getOrganization().getId();
		final OrganizationEntity organization = organizationDAO.findById(organizationId, false);
		patient.setOrganization(organization);
		patient.setTraumaticBrainInjury(gwtPatient.getTraumaticBrainInjury()
				.orNull());
		patient.setLabel(gwtPatient.getLabel());
		for (final Precipitant p : gwtPatient.getPrecipitants()) {
			patient.getPrecipitants().add(p);
		}
		for (final SeizureType s : gwtPatient.getSzTypes()) {
			patient.getSzTypes().add(s);
		}
	}

	private static void removeAdmissionOrThrowException(
			final Patient patient,
			final HospitalAdmission admission,
			ITsAnnotationDAO annotationDAO,
			IDataSnapshotService dsService)
			throws BrainTrustUserException {

		for (final EegStudy study : admission.getStudies()) {
			if (!dsService.isRecordingModifiable(study)) {
				throw new BrainTrustUserException(
						"Hospital admission cannot be deleted. A time series in one of this admission's studies is referenced by a user created data snapshot.");
			}
		}

		// Delete all study annotations before deleting the patient.
		for (final EegStudy study : admission.getStudies()) {
			annotationDAO.deleteByParent(study);
		}
		patient.removeAdmission(admission);
	}

}
