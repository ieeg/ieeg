/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.validate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;

public class CheckPatientConsistency {

	private static final Logger logger = LoggerFactory
			.getLogger(CheckPatientConsistency.class);

	/**
	 * Runs a {@code PatientConsistencyChecker} on all patients in the database.
	 * 
	 * @param args
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable {
		Transaction trx = null;
		try {
			final PatientConsistencyChecker checker = new PatientConsistencyChecker();
			final Session session = HibernateUtil.getSessionFactory()
					.getCurrentSession();
			final IPatientDAO patientDAO = new PatientDAOHibernate(session);
			trx = session.beginTransaction();
			final List<Patient> patients = patientDAO.findAll();
			for (final Patient patient : patients) {
				checker.checkPatient(patient);
			}
			trx.commit();
		} catch (final Throwable t) {
			if (trx != null && trx.isActive()) {
				try {
					trx.rollback();
				} catch (final Throwable rbEx) {
					logger.error("Rollback exception", rbEx);
				}
			}
			throw t;
		}
	}
}
