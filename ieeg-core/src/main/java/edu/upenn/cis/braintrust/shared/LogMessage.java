/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.sql.Timestamp;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class LogMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	private UserId recipient;
	private UserId sender;

	private String message;
	private Timestamp time;
	private int visibility;

	private int messageType;
	private String identifier;

	private Long eventId;

	public LogMessage(UserId recipient, UserId sender, String message,
			Timestamp time, int visibility, Long eventId, int messageType,
			String identifier) {
		super();
		this.recipient = recipient;
		this.sender = sender;
		this.message = message;
		this.time = time;
		this.visibility = visibility;
		this.eventId = eventId;
		this.messageType = messageType;
		this.identifier = identifier;
	}

	public LogMessage(UserId recipient, UserId sender, String message,
			Timestamp time, int visibility, int messageType, String identifier) {
		super();
		this.recipient = recipient;
		this.sender = sender;
		this.message = message;
		this.time = time;
		this.visibility = visibility;
		this.messageType = messageType;
		this.identifier = identifier;
	}

	public UserId getRecipient() {
		return recipient;
	}

	public void setRecipient(UserId recipient) {
		this.recipient = recipient;
	}

	public UserId getSender() {
		return sender;
	}

	public void setSender(UserId sender) {
		this.sender = sender;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

}
