package edu.upenn.cis.braintrust.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.RelationshipConstructor;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

@Entity(name = "UserSubmission")
@Table(name = UserSubmissionRelationship.TABLE)
public class UserSubmissionRelationship
implements IHasLongId, IHasPubId, IHasExtAcl, IRelationship {
	public final static String TABLE = "user_submission";

	private Long id;
	private String pubId;

	private UserTaskEntity task;

	private ExtAclEntity extAcl;

	List<UserEntity> creators;

	Date submissionDate;
	String submissionComments;

	List<String> submissionNames;
	List<JsonTyped> submissionValues;

	JsonTyped submissionParameters;

	public UserSubmissionRelationship() {}

	public UserSubmissionRelationship(Long id, String pubId,
			UserTaskEntity task, ExtAclEntity extAcl,
			List<UserEntity> creators, Date submissionDate,
			String submissionComments, List<String> submissionNames,
			List<JsonTyped> submissionValues, JsonTyped submissionParameters) {
		super();
		this.id = id;
		if (pubId != null)
			this.pubId = pubId;
		else
			this.pubId = BtUtil.newUuid();
		this.task = task;
		this.extAcl = extAcl;
		this.creators = creators;
		this.submissionDate = submissionDate;
		this.submissionComments = submissionComments;
		this.submissionNames = submissionNames;
		this.submissionValues = submissionValues;
		this.submissionParameters = submissionParameters;
	}




	@OneToOne(
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public UserTaskEntity getTask() {
		return task;
	}

	public void setTask(UserTaskEntity task) {
		this.task = task;
	}

	@Override
	@OneToOne(
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getExtAcl() {
		return extAcl;
	}

	@ManyToOne
	// not lazy on purpose
	@JoinColumn(name = "creators_list_id")
	@ForeignKey(name = "FK_TASK_CREATORS")
	@NotNull
	public List<UserEntity> getCreators() {
		return creators;
	}

	@Override
	public Long getId() {
		return id;
	}



	public String getPubId() {
		return pubId;
	}



	public void setPubId(String pubId) {
		this.pubId = pubId;
	}



	public Date getSubmissionDate() {
		return submissionDate;
	}



	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}



	public String getSubmissionComments() {
		return submissionComments;
	}



	public void setSubmissionComments(String submissionComments) {
		this.submissionComments = submissionComments;
	}



	public List<String> getSubmissionNames() {
		return submissionNames;
	}



	public void setSubmissionNames(List<String> submissionNames) {
		this.submissionNames = submissionNames;
	}



	public List<JsonTyped> getSubmissionValues() {
		return submissionValues;
	}



	public void setSubmissionValues(List<JsonTyped> submissionValues) {
		this.submissionValues = submissionValues;
	}



	public JsonTyped getSubmissionParameters() {
		return submissionParameters;
	}



	public void setSubmissionParameters(JsonTyped submissionParameters) {
		this.submissionParameters = submissionParameters;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public void setExtAcl(ExtAclEntity extAcl) {
		this.extAcl = extAcl;
	}



	public void setCreators(List<UserEntity> creators) {
		this.creators = creators;
	}



	@JsonIgnore
	@Transient
	@Override
	public String getLabel() {
		return null;
	}

	@Override
	public RelationshipConstructor<? extends IRelationship, ?, ? extends IEntity, ? extends IEntity> tableConstructor() {
		return new RelationshipConstructor<UserSubmissionRelationship,String,UserEntity,UserTaskEntity>(UserSubmissionRelationship.class,
				new IPersistentObjectManager.IPersistentKey<UserSubmissionRelationship,String>() {

			@Override
			public String getKey(UserSubmissionRelationship o) {
				return o.getPubId();
			}

			@Override
			public void setKey(UserSubmissionRelationship o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new RelationshipPersistence.ICreateRelationship<UserSubmissionRelationship,String,UserEntity,UserTaskEntity>() {

			@Override
			public UserSubmissionRelationship create(String label, UserEntity e1, UserTaskEntity e2) {
				List<UserEntity> creators = new ArrayList<UserEntity>();
				creators.add(e1);

				return new UserSubmissionRelationship(null, null, e2, PersistentObjectFactory.getFactory().getEmptyWorldAcl(), creators, null, null, null, null, null);
			}
		}
				);
	}



}
