/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.model.PermissionDomainEntity;

/**
 * Constants for core permissions.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public final class CorePermDefs implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public static final String CORE_MODE_NAME = "CORE";

	public static final String READ_PERMISSION_NAME = "READ";
	public static final String EDIT_PERMISSION_NAME = "EDIT";
	public static final String OWNER_PERMISSION_NAME = "OWNER";

	public static final ExtPermission READ_METADATA_PERM =
			new ExtPermission(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
					CORE_MODE_NAME,
					READ_PERMISSION_NAME);
	public static final ExtPermission READ_PERM =
			new ExtPermission(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
					CORE_MODE_NAME,
					READ_PERMISSION_NAME);
	public static final ExtPermission EDIT_PERM =
			new ExtPermission(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
					CORE_MODE_NAME,
					EDIT_PERMISSION_NAME);
	public static final ExtPermission OWNER_PERM =
			new ExtPermission(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
					CORE_MODE_NAME,
					OWNER_PERMISSION_NAME);

	public static final AllowedAction READ =
			new AllowedAction(READ_PERMISSION_NAME);
	public static final AllowedAction EDIT =
			new AllowedAction("EDIT");
	public static final AllowedAction DELETE =
			new AllowedAction("DELETE");
	public static final AllowedAction EDIT_ACL =
			new AllowedAction("EDIT_ACL");

	private CorePermDefs() {
		throw new AssertionError("CorePermDefs should not be instantiated.");
	}
}
