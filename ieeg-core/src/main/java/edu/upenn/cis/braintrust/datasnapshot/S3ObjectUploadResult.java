/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.aws.AwsUtil;

/**
 * @author John Frommeyer
 *
 */
public final class S3ObjectUploadResult implements IRecObjectUploadResult {

	private final String bucket;
	private final String fileKey;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public S3ObjectUploadResult(
			String bucket,
			String fileKey) {
		this.bucket = checkNotNull(bucket);
		this.fileKey = checkNotNull(fileKey);
	}

	@Override
	public void deleteObject() {
		final String m = "deleteObject()";
		AwsUtil.getS3().deleteObject(bucket, fileKey);
		logger.info(
				"{}: Deleted S3 object {} from bucket {}",
				m,
				fileKey,
				bucket);

	}

}
