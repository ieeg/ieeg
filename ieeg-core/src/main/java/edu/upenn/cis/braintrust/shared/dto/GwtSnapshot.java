/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class GwtSnapshot implements IHasGwtRecording, JsonTyped, Serializable {
	
	GwtSnapshot() {
		
	}

	public GwtSnapshot(
			String label,
			@Nullable GwtRecording recording,
			@Nullable Long id,
			@Nullable Integer version) {
		this.recording = recording;//checkNotNull(recording);
		this.label = checkNotNull(label);
		this.id = id;
		this.version = version;
	}	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer version;
	private String label;
	private GwtRecording recording;

	@Nullable
	public Long getId() {
		return id;
	}

	@Nullable
	public Integer getVersion() {
		return version;
	}

	public void setId(@Nullable final Long id) {
		this.id = id;
	}

	public void setVersion(@Nullable final Integer version) {
		this.version = version;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	@Nullable
	public GwtRecording getRecording() {
		return recording;
	}

	public void setRecording(@Nullable GwtRecording recording) {
		this.recording = recording;
	}

}
