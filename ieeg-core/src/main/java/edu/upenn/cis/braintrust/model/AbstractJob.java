/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import javax.annotation.Nullable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.ParallelDto;

/**
 * A user's job.
 * 
 * @author John Frommeyer
 */
@MappedSuperclass
public abstract class AbstractJob implements IHasLongId {
	public static final String TABLE = "job";
	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private Long userId;
	private String datasetPubId;
	private String toolPubId;
	private JobStatus status;
	private Date createTime;
	private ParallelDto parallel;

	protected AbstractJob() {}

	protected AbstractJob(
			Long user,
			String datasetPubId,
			String tool,
			JobStatus status,
			ParallelDto parallel,
			Date createTime) {
		this.userId = checkNotNull(user);
		this.datasetPubId = checkNotNull(datasetPubId);
		this.toolPubId = checkNotNull(tool);
		this.status = checkNotNull(status);
		this.parallel = checkNotNull(parallel);
		this.createTime = checkNotNull(createTime);
	}

	@Transient
	public abstract String getClob();

	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@NaturalId
	@Size(min = 36, max = 36)
	@NotNull
	public String getDatasetPubId() {
		return datasetPubId;
	}

	@Override
	@Transient
	public Long getId() {
		return id;
	}

	@NotNull
	public ParallelDto getParallel() {
		return parallel;
	}

	@NotNull
	public JobStatus getStatus() {
		return status;
	}

	@NotNull
	@Size(min = 36, max = 36)
	public String getToolPubId() {
		return toolPubId;
	}

	@NotNull
	public Long getUserId() {
		return userId;
	}

	public abstract void setClob(@Nullable final String clob);

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setDatasetPubId(String datasetPubId) {
		this.datasetPubId = datasetPubId;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public void setParallel(ParallelDto parallel) {
		this.parallel = parallel;
	}

	public void setStatus(JobStatus status) {
		this.status = status;
	}

	public void setToolPubId(String toolPubId) {
		this.toolPubId = toolPubId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
