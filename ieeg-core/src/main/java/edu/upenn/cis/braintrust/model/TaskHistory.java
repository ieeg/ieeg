/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.shared.TaskStatus;

/**
 * A completed task.
 * 
 * @author John Frommeyer
 */
@Entity
@Table(name = TaskHistory.TABLE)
public class TaskHistory extends AbstractTask<JobHistory> {
	public static final String TABLE = "task_history";
	public static final String ID_COLUMN = TABLE + "_id";

	TaskHistory() {}

	public TaskHistory(
			JobHistory parent,
			String pubId,
			TaskStatus status,
			Long startTimeMicrosec,
			Long endTimeMicrosec,
			@Nullable String worker,
			@Nullable Date runStartTime) {
		super(parent, pubId, status, startTimeMicrosec, endTimeMicrosec,
				worker, runStartTime);
	}

	public TaskHistory(
			JobHistory parent,
			String pubId,
			TaskStatus status,
			Long startTimeMicrosec,
			Long endTimeMicrosec,
			Set<String> tsPubIds,
			@Nullable String worker,
			@Nullable Date runStartTime) {
		super(parent, pubId, status, startTimeMicrosec, endTimeMicrosec,
				tsPubIds,
				worker, runStartTime);
	}

	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	@Override
	public Long getId() {
		return super.getId();
	}

	@Override
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public JobHistory getParent() {
		return super.getParent();
	}

	@Override
	@ElementCollection
	@CollectionTable(
			name = TABLE + "_ts_pub_id",
			joinColumns = @JoinColumn(name = ID_COLUMN))
	@Column(name = "ts_pub_id", nullable = false)
	@Nonnull
	public Set<String> getTsPubIds() {
		return super.getTsPubIds();
	}
}
