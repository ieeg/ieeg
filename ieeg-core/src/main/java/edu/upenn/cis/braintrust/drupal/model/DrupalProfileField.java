/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import static com.google.common.base.Objects.equal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.NaturalId;

@Immutable
@Entity
@Table(name = "profile_fields")
public class DrupalProfileField {

	public final static String ID_COLUMN = "fid";

	public final static String FIRSTNAME_NAME = "profile_firstName";

	public final static String LASTNAME_NAME = "profile_surName";
	public final static String INSTITUTION_NAME = "profile_affiliation";
	public final static String ACADEMIC_TITLE_NAME = "profile_title";
	public final static String JOB_NAME = "profile_position";
	public final static String INBOX_BUCKET = "profile_inbox_bucket";
	public final static String INBOX_AWS_ACCESS_KEY_ID = "profile_inbox_access_key_id";
	public final static String DROPBOX_KEY = "profile_dropbox_key_id";
	public final static String LAT = "profile_latitude";
	public final static String LON = "profile_longitude";

	public final static String STREET_NAME = "profile_workAddress";
	public final static String CITY_NAME = "profile_workTown";
	public final static String STATE_NAME = "profile_workState";
	public final static String ZIP_NAME = "profile_workZip";
	public final static String COUNTRY_NAME = "profile_country";
	
	private Integer fid;
	private String name;

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DrupalProfileField)) {
			return false;
		}
		DrupalProfileField other = (DrupalProfileField) obj;
		if (!equal(name, other.getName())) {
			return false;
		}
		return true;
	}

	@Id
	@Column(name = ID_COLUMN)
	public Integer getFid() {
		return fid;
	}

	@NaturalId
	@Size(max = 128)
	@NotNull
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	private void setFid(Integer id) {
		fid = id;
	}

	@SuppressWarnings("unused")
	private void setName(String name) {
		this.name = name;
	}

}
