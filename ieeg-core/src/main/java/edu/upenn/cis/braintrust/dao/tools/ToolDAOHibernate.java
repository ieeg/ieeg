/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.tools;

import java.util.List;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class ToolDAOHibernate
		extends GenericHibernateDAO<ToolEntity, Long>
		implements IToolDAO {

	public ToolDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ToolEntity> findAll(UserEntity user) {
		return (List<ToolEntity>) getSession()
				.createQuery(
						// "from Tool t left join fetch t.dialogXml join fetch t.acl a left join fetch a.userAces ua join fetch ua.user")
						"select distinct t from Tool t "
								+ "join t.extAcl acl "
								+ ExtAclEntity.FROM_PERMISSIONS
								+ "where "
								+ ExtAclEntity.WHERE_PERMISSIONS)
				.setParameter("user", user)
				.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<ToolEntity> findAllForAdmin() {
		return (List<ToolEntity>) getSession()
				.createQuery(
						"select t from Tool t ")
				.list();
	}

}
