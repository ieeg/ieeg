/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public final class EegStudySearch implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer minSzCount;
	private Integer maxSzCount;
	private ContactGroupSearch contactGroupSearch;
	private HospitalAdmissionSearch admissionSearch;
	private Set<ImageType> imageTypes = newHashSet();
	private Set<IlaeRating> ilaeRatings = newHashSet();
	private EpilepsySubtypeSearch epilepsySubtypeSearch;

	public HospitalAdmissionSearch getAdmissionSearch() {
		return this.admissionSearch;
	}

	public ContactGroupSearch getContactGroupSearch() {
		return contactGroupSearch;
	}

	public EpilepsySubtypeSearch getEpilepsySubtypeSearch() {
		return epilepsySubtypeSearch;
	}

	public Set<IlaeRating> getIlaeRatings() {
		return ilaeRatings;
	}

	/**
	 * Get studies that have <em>all</em> of the image types.
	 * 
	 * @return image types we're looking for in the study
	 */
	public Set<ImageType> getImageTypes() {
		return imageTypes;
	}

	public Integer getMaxSzCount() {
		return maxSzCount;
	}

	public Integer getMinSzCount() {
		return minSzCount;
	}

	public void setAdmissionSearch(
			@Nullable HospitalAdmissionSearch admissionSearch) {
		this.admissionSearch = admissionSearch;
	}

	public void setContactGroupSearch(
			@Nullable final ContactGroupSearch contactGroupSearch) {
		this.contactGroupSearch = contactGroupSearch;
	}

	public void setEpilepsySubtypeSearch(
			EpilepsySubtypeSearch epilepsySubtypeSearch) {
		this.epilepsySubtypeSearch = epilepsySubtypeSearch;
	}

	public void setIlaeRatings(Set<IlaeRating> ilaeRatingScales) {
		this.ilaeRatings = ilaeRatingScales;
	}

	public void setImageTypes(Set<ImageType> imageTypes) {
		this.imageTypes = imageTypes;
	}

	public void setMaxSzCount(@Nullable final Integer szCountMax) {
		this.maxSzCount = szCountMax;
	}

	public void setMinSzCount(@Nullable final Integer szCountMin) {
		this.minSzCount = szCountMin;
	}

	public void setPatientSearch(PatientSearch patientSearch) {
		if (getAdmissionSearch() == null) {
			setAdmissionSearch(new HospitalAdmissionSearch());
		}
		getAdmissionSearch().setPatientSearch(patientSearch);
	}

	@Override
	public String toString() {
		return "EegStudySearch [minSzCount=" + minSzCount + ", maxSzCount="
				+ maxSzCount + ", electrodeSearch=" + contactGroupSearch
				+ ", admissionSearch=" + admissionSearch + ", imageTypes="
				+ imageTypes + ", ilaeRatings=" + ilaeRatings
				+ ", epilepsySubtypeSearch="
				+ epilepsySubtypeSearch + "]";
	}

}
