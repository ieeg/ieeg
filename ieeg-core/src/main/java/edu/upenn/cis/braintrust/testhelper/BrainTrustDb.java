/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.testhelper;

import javax.annotation.Nullable;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.identity.hibernate.UserDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugAdminDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.AnimalDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugAdminDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.SpeciesDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimRegionDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimTypeDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.dao.provenance.IProvenanceDAO;
import edu.upenn.cis.braintrust.dao.provenance.hibernate.ProvenanceDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.AnalyzedDsDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DatasetDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ImageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.TimeSeriesDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.IToolDAO;
import edu.upenn.cis.braintrust.dao.tools.JobDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.ToolDAOHibernate;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;

public class BrainTrustDb {

	private final IPatientDAO patientDAO;
	private final IAnimalDAO animalDAO;
	private final IDatasetDAO datasetDAO;
	private final ITsAnnotationDAO tsAnnotationDAO;
	private final IAnalyzedDataSnapshotDAO analyzedDataSnapshotDAO;
	private final ITimeSeriesDAO timeSeriesDAO;
	private final IUserDAO userDAO;
	private final IImageDAO imageDAO;
	private final IToolDAO toolDAO;
	private final IJobDAO jobDAO;
	private final ISpeciesDAO speciesDAO;
	private final IStimRegionDAO stimRegionDAO;
	private final IStimTypeDAO stimTypeDAO;
	private final IDrugDAO drugDAO;
	private Session session;
	private final IDrugAdminDAO drugAdminDAO;
	private final ISessionDAO sessionDAO;
	private final IProvenanceDAO provenanceDAO;
	private IDAOFactory daoFactory;

	public BrainTrustDb(
			IDAOFactory daoFactory,
			final IPatientDAO patientDAO,
			final IAnimalDAO animalDAO,
			final ITimeSeriesDAO timeSeriesDAO,
			final ITsAnnotationDAO tsAnnotation2DAO,
			final IDatasetDAO datasetDAO,
			final IAnalyzedDataSnapshotDAO analyzedDsDAO,
			final IUserDAO userDAO,
			final IImageDAO imageDAO,
			final IToolDAO toolDAO,
			final IJobDAO jobDAO,
			final ISpeciesDAO speciesDAO,
			final IStimRegionDAO stimRegionDAO,
			final IStimTypeDAO stimTypeDAO,
			final IDrugDAO drugDAO,
			final IDrugAdminDAO drugAdminDAO,
			final ISessionDAO sessionDAO,
			final IProvenanceDAO provenanceDAO) {
		this.daoFactory = daoFactory;
		this.patientDAO = patientDAO;
		this.provenanceDAO = provenanceDAO;
		this.animalDAO = animalDAO;
		this.timeSeriesDAO = timeSeriesDAO;
		this.tsAnnotationDAO = tsAnnotation2DAO;
		this.datasetDAO = datasetDAO;
		this.analyzedDataSnapshotDAO = analyzedDsDAO;
		this.userDAO = userDAO;
		this.imageDAO = imageDAO;
		this.toolDAO = toolDAO;
		this.jobDAO = jobDAO;
		this.speciesDAO = speciesDAO;
		this.stimRegionDAO = stimRegionDAO;
		this.stimTypeDAO = stimTypeDAO;
		this.drugDAO = drugDAO;
		this.drugAdminDAO = drugAdminDAO;
		this.sessionDAO = sessionDAO;
	}

	public BrainTrustDb(final Session session) {
		this(session, HibernateUtil.getConfiguration());
	}

	public BrainTrustDb(
			final Session session, Configuration config) {
		this(
				new HibernateDAOFactory(),
				new PatientDAOHibernate(session),
				new AnimalDAOHibernate(session),
				new TimeSeriesDAOHibernate(session),
				new TsAnnotationDAOHibernate(session, config),
				new DatasetDAOHibernate(session),
				new AnalyzedDsDAOHibernate(session),
				new UserDAOHibernate(session),
				new ImageDAOHibernate(session),
				new ToolDAOHibernate(session),
				new JobDAOHibernate(session),
				new SpeciesDAOHibernate(session),
				new StimRegionDAOHibernate(session),
				new StimTypeDAOHibernate(session),
				new DrugDAOHibernate(session),
				new DrugAdminDAOHibernate(session),
				new SessionDAOHibernate(session),
				new ProvenanceDAOHibernate(session));
		this.session = session;
	}

	public void saveEverything(
			@Nullable final Iterable<? extends Patient> patients,
			@Nullable final Iterable<? extends TimeSeriesEntity> timeSeries,
			@Nullable final Iterable<? extends TsAnnotationEntity> tsAnnotations,
			@Nullable final Iterable<? extends UserEntity> users,
			@Nullable final Iterable<? extends Dataset> datasets,
			@Nullable final Iterable<? extends SnapshotUsage> usages) {

		if (patients != null) {
			for (Patient patient : patients) {
				patientDAO.saveOrUpdate(patient);
			}
		}
		if (timeSeries != null) {
			for (TimeSeriesEntity ts : timeSeries) {
				timeSeriesDAO.saveOrUpdate(ts);
			}
		}

		if (users != null) {
			for (UserEntity user : users) {
				userDAO.saveOrUpdate(user);
			}
		}
		if (datasets != null) {
			for (Dataset ds : datasets) {
				datasetDAO.saveOrUpdate(ds);
			}
		}
		if (tsAnnotations != null) {
			for (TsAnnotationEntity annotation : tsAnnotations) {
				tsAnnotationDAO.saveOrUpdate(annotation);
			}
		}

		if (usages != null) {
			for (SnapshotUsage usage : usages) {
				provenanceDAO.saveOrUpdate(usage);
			}
		}
	}
}
