/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.ImageType;

public class GwtEegStudy extends GwtSnapshot {

	private static final long serialVersionUID = 1L;

	private GwtHospitalAdmission parent;
	private boolean published = false;

	// private Set<Image.Type> imageTypes = newHashSet();
	private EegStudyType type;

	/** The number of seizures observed during the course of this study. */
	private Optional<Integer> szCount;

	private Set<ImageType> imageTypes = newHashSet();
	private Set<GwtEpilepsySubtype> epilepsySubtypes = newHashSet();

	private boolean deleted = false;

	private boolean modifiable = true;

	public GwtEegStudy() {}

	public GwtEegStudy(
			String label,
			@Nullable GwtRecording recording,
			EegStudyType type,
			@Nullable Integer szCount,
			@Nullable Long id,
			@Nullable Integer version) {
		super(label, recording, id, version);
		this.type = checkNotNull(type);
		this.szCount = Optional.fromNullable(szCount);
	}

	public Set<GwtEpilepsySubtype> getEpilepsySubtypes() {
		return epilepsySubtypes;
	}

	/**
	 * @return the imageTypes
	 */
	public Set<ImageType> getImageTypes() {
		return imageTypes;
	}

	/**
	 * The {@code GwtHospitalAdmission} when this monitoring was performed.
	 * 
	 * @return {@code GwtHospitalAdmission} when this monitoring was performed
	 */
	@Nullable
	public GwtHospitalAdmission getParent() {
		return parent;
	}

	/**
	 * @return the szCount
	 */
	public Optional<Integer> getSzCount() {
		return szCount;
	}

	public EegStudyType getType() {
		return type;
	}

	/**
	 * @param hospitalAdmission the {@code GwtHospitalAdmission} which owns this
	 *            {@code GwtEegStudy}
	 */
	public void setParent(
			@Nullable final GwtHospitalAdmission hospitalAdmission) {
		this.parent = hospitalAdmission;
	}

	/**
	 * @param szCount the seizureCount to set
	 */
	public void setSzCount(@Nullable final Integer szCount) {
		this.szCount = Optional.fromNullable(szCount);
	}

	public void setType(final EegStudyType studyType) {
		this.type = studyType;
	}

	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setDeleted(Boolean isDeleted) {
		this.deleted = isDeleted;
	}

	/**
	 * @return the isDeleted
	 */
	public Boolean isDeleted() {
		return deleted;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(final boolean published) {
		this.published = published;
	}

	public boolean isModifiable() {
		return modifiable;
	}

	public void setModifiable(boolean modifiable) {
		this.modifiable = modifiable;
	}

}
