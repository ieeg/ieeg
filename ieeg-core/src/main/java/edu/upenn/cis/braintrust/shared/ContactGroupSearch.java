/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class ContactGroupSearch implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double samplingRateMin;

	private Double samplingRateMax;
	// remember: EnumSet's are gwt serializable, so use hashset's
	private Set<Side> sides = newHashSet();

	private Set<Location> locations = newHashSet();
	private Set<String> types = newHashSet();
	private Set<ContactType> contactTypes = newHashSet();
	private TraceSearch traceSearch;

	public ContactGroupSearch() {}

	public Set<ContactType> getContactTypes() {
		return contactTypes;
	}

	public Set<Location> getLocations() {
		return this.locations;
	}

	public Double getSamplingRateMax() {
		return samplingRateMax;
	}

	public Double getSamplingRateMin() {
		return samplingRateMin;
	}

	public Set<Side> getSides() {
		return this.sides;
	}

	public TraceSearch getTraceSearch() {
		return traceSearch;
	}

	public Set<String> getTypes() {
		return types;
	}

	public void setContactTypes(Set<ContactType> contactTypes) {
		this.contactTypes = checkNotNull(contactTypes);
	}

	public void setLocations(Set<Location> locations) {
		this.locations = checkNotNull(locations);
	}

	public void setSamplingRateMax(@Nullable Double samplingRateHigh) {
		this.samplingRateMax = samplingRateHigh;
	}

	public void setSamplingRateMin(@Nullable Double samplingRateLow) {
		this.samplingRateMin = samplingRateLow;
	}

	public void setSides(Set<Side> sides) {
		this.sides = checkNotNull(sides);
	}

	/**
	 * @param timeSeriesSearch the timeSeriesSearch to set
	 */
	public void setTraceSearch(
			@Nullable TraceSearch timeSeriesSearch) {
		this.traceSearch = timeSeriesSearch;
	}

	public void setTypes(Set<String> types) {
		this.types = checkNotNull(types);
	}

	@Override
	public String toString() {
		return "ContactGroupSearch [samplingRateMin=" + samplingRateMin
				+ ", samplingRateMax=" + samplingRateMax + ", sides=" + sides
				+ ", locations=" + locations + ", types=" + types
				+ ", contactTypes="
				+ contactTypes + ", traceSearch=" + traceSearch + "]";
	}

}
