/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;

/**
 * Enums for entities with ACLs
 * 
 * @author John Frommeyer
 */
public enum HasAclType {

	DATA_SNAPSHOT(DataSnapshotEntity.class, "DataSnapshot"),
	TOOL(ToolEntity.class, "Tool"),
	ORGANIZATION(OrganizationEntity.class, "Organization");

	HasAclType(Class<? extends IHasExtAcl> clazz, String entityName) {
		this.clazz = clazz;
		this.entityName = entityName;
	}

	private final Class<? extends IHasExtAcl> clazz;
	private final String entityName;

	public Class<? extends IHasExtAcl> getEntityClass() {
		return clazz;
	}

	public String getEntityName() {
		return entityName;
	}

	public static HasAclType fromString(String entityType) {
		checkNotNull(entityType);
		if (entityType.equals("DataSnapshot")) {
			return DATA_SNAPSHOT;
		}
		if (entityType.equals("Tool")) {
			return TOOL;
		}
		if (entityType.equals("Organization")) {
			return ORGANIZATION;
		}
		throw new IllegalArgumentException("Illegal HasAclType: " + entityType);
	}
}
