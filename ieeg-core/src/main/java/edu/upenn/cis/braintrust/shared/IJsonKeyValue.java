package edu.upenn.cis.braintrust.shared;

import edu.upenn.cis.db.mefview.shared.JsonTyped;


public interface IJsonKeyValue extends JsonTyped {
	public String getKey();
	
	public void setKey(String key);
	
//	public String getValueAsString();
	
	public String toString();
}
