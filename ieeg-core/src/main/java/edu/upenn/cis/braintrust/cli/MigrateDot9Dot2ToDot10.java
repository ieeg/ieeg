/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.cli;

public class MigrateDot9Dot2ToDot10 {
	// public static void main(String[] args) throws IOException {
	//
	// checkArgument(args.length == 1);
	//
	// FileInputStream hibernateProps = null;
	// Properties props = null;
	// try {
	// hibernateProps = new FileInputStream(args[0]);
	// props = new Properties();
	// props.load(hibernateProps);
	// } finally {
	// try {
	// hibernateProps.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// HibernateUtil.getConfiguration().setProperties(props);
	//
	// Session sess = null;
	// Transaction trx = null;
	//
	// try {
	// sess = HibernateUtil.getSessionFactory().openSession();
	// trx = sess.beginTransaction();
	//
	// IAclDAO aclDAO = new AclDAOHibernate(sess);
	// IUserAceDAO userAceDAO = new UserAceDAOHibernate(sess);
	//
	// IEegStudyDAO studyDAO = new EegStudyDAOHibernate(sess);
	//
	// Set<EegStudy> studies = newHashSet(studyDAO.findAll());
	//
	// for (EegStudy study : studies) {
	// checkState(study.getAcl() == null,
	// "this program was already run! Found non-null acl in a study");
	// if (study.isPublished()) {
	// study.setAcl(new AclEntity(WorldPermissions.READ_ONLY));
	// } else {
	// study.setAcl(new AclEntity());
	// }
	// }
	//
	// IDatasetDAO dsDAO = new DatasetDAOHibernate(sess);
	//
	// Set<Dataset> dss = newHashSet(dsDAO.findAll());
	//
	// for (Dataset ds : dss) {
	// checkState(ds.getAcl() == null,
	// "this program was already run! Found non-null acl in a dataset");
	// UserEntity user = ds.getCreator();
	// AclEntity acl = SecurityUtil.createDefUserOwnedAcl(user,
	// aclDAO,
	// userAceDAO);
	// ds.setAcl(acl);
	// }
	//
	// IToolDAO toolDAO = new ToolDAOHibernate(sess);
	//
	// Set<ToolEntity> tools = newHashSet(toolDAO.findAll());
	//
	// for (ToolEntity tool : tools) {
	// checkState(tool.getAcl() == null,
	// "this program was already run! Found non-null acl in a tool");
	// UserEntity user = tool.getAuthor();
	// AclEntity acl = SecurityUtil.createDefUserOwnedAcl(user,
	// aclDAO, userAceDAO);
	// tool.setAcl(acl);
	// }
	// trx.commit();
	// } catch (Throwable t) {
	// t.printStackTrace();
	// PersistenceUtil.rollBackTrx(trx);
	// } finally {
	// PersistenceUtil.closeSession(sess);
	// }
	// }
}
