/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * 
 * @author John Frommeyer
 * 
 */
@Entity(name = "Subject")
@Table(name = Subject.TABLE)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Subject
		implements IHasLabel, IHasLongId, IEntity {
	public static final String TABLE = "subject";
	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private String label;
	private Integer version;
	private OrganizationEntity organization;

	protected Subject() {}

	protected Subject(String label, OrganizationEntity organization) {
		this.label = checkNotNull(label);
		this.organization = checkNotNull(organization);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = Subject.ID_COLUMN)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.EAGER)//LAZY)
	@JoinColumn(name = OrganizationEntity.ID_COLUMN)
	@NotNull
	public OrganizationEntity getOrganization() {
		return organization;
	}

	@Override
	@Column(unique = true)
	@Size(min = 1, max = 255)
	@NotNull
	public String getLabel() {
		return label;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@VisibleForTesting
	public void setId(Long id) {
		this.id = id;
	}

	public void setOrganization(OrganizationEntity organization) {
		this.organization = organization;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@VisibleForTesting
	public void setVersion(Integer version) {
		this.version = version;
	}
}
