/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.collect.ImmutableList;

public class GwtAnimalExperimentPair implements Serializable {

	private static final long serialVersionUID = 1L;

	private GwtAnimal animal;
	private ImmutableList<GwtExperiment> experiments;

	@SuppressWarnings("unused")
	private GwtAnimalExperimentPair() {}

	public GwtAnimalExperimentPair(GwtAnimal animal,
			ImmutableList<GwtExperiment> experiments) {
		this.animal = checkNotNull(animal);
		this.experiments = checkNotNull(experiments);
	}

	public GwtAnimal getAnimal() {
		return animal;
	}

	public ImmutableList<GwtExperiment> getExperiments() {
		return experiments;
	}

}
