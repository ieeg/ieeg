/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import java.io.InputStream;

import javax.annotation.concurrent.ThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.google.common.hash.HashCode;
import com.google.common.io.BaseEncoding;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.aws.TransferManagerFactory;
import edu.upenn.cis.braintrust.model.RecordingObjectEntity;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;

/**
 * @author John Frommeyer
 *
 */
@ThreadSafe
public final class TransferManagerRecObjectWriter implements
		IRecordingObjectWriter {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final TransferManager transferManager = TransferManagerFactory
			.getTransferManager();

	@Override
	public IRecObjectUploadResult writeRecordingObject(
			InputStream inputStream,
			String fileKey,
			String md5Hash,
			Long inputLength,
			String contentType)
			throws BadDigestException,
			DuplicateNameException {

		final String m = "writeRecordingObject(...)";
		logger.debug(
				"{}: Uploading file to S3.",
				m);
		final String dataBucket = IvProps.getDataBucket();
		final PutObjectRequest putRequest = AwsUtil.createPutObjectRequest(
				dataBucket,
				fileKey,
				inputStream,
				inputLength);
		putRequest.setStorageClass(StorageClass.ReducedRedundancy);
		final String md5Base64 = BaseEncoding.base64().encode(
				HashCode.fromString(md5Hash).asBytes());
		putRequest.getMetadata().setContentMD5(md5Base64);
		putRequest.getMetadata().setContentType(contentType);
		long startTimeNanos = System.nanoTime();
		try {
			final Upload upload = transferManager.upload(putRequest);
			upload.waitForCompletion();
		} catch (AmazonS3Exception e) {
			if (e.getErrorCode().equals("BadDigest")) {
				throw new BadDigestException(e.getMessage());
			}
			throw e;
		} catch (AmazonServiceException e) {
			throw e;
		} catch (AmazonClientException e) {
			throw e;
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			logger.error("Caught interruption, reasserting", e);
		} finally {
			logger.info(
					"{}: AWS write time for {}: {} seconds",
					m,
					fileKey,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
		return new S3ObjectUploadResult(
				dataBucket,
				fileKey);
	}

	@Override
	public void deleteRecordingObject(RecordingObjectEntity recordingObject) {
		final String m = "deleteRecordingObject(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

}
