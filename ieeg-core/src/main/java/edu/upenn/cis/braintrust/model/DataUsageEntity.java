/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Immutable;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
		@NamedQuery(
				name = DataUsageEntity.COUNT_BYTES,
				query = "select sum(du.dataSizeBytes) "
						+ "from DataUsage du")
})
@Entity(name = "DataUsage")
@Table(name = DataUsageEntity.TABLE)
@Immutable
public class DataUsageEntity implements IHasLongId, IEntity {

	public static final String TABLE = "data_usage";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String COUNT_BYTES = "DataUsage-countBytes";

	private Long id;
	// no version field because immutable
	private UserEntity user;
	private DataRequestType requestType;
	private Long dataSizeBytes;
	private Date timeOfRequest;
	private Long responseTimeNanos;
	private Long dataSnapshotId;
	private String dataSnapshotPubId;
	private String dataSnapshotName;
	private Long dataStartUutc;
	private Long dataLengthMicros;
	private String remoteHost;
	private String remoteAddr;
	private String userAgentHeader;
	private String requestUrl;
	private String queryString;

	/** For JPA. */
	public DataUsageEntity() {}

	public DataUsageEntity(
			DataRequestType requestType,
			String dataSnapshotPubId,
			long dataStartUutc,
			long dataLengthMicros) {
		maximalInit(
				null,
				requestType,
				null,
				null,
				null,
				null,
				dataSnapshotPubId,
				null,
				dataStartUutc,
				dataLengthMicros,
				null,
				null,
				null,
				null,
				null);
	}

	public DataUsageEntity(
			DataRequestType requestType,
			String dataSnapshotPubId) {
		maximalInit(
				null,
				requestType,
				null,
				null,
				null,
				null,
				dataSnapshotPubId,
				null,
				-1,
				-1,
				null,
				null,
				null,
				null,
				null);
	}

	private void maximalInit(
			@Nullable UserEntity user,
			DataRequestType requestType,
			@Nullable Long dataSize,
			@Nullable Date timeOfRequest,
			@Nullable Long responseTimeNanos,
			@Nullable Long dataSnapshotId,
			String dataSnapshotPubId,
			@Nullable String dataSnapshotName,
			long dataStartUutc,
			long dataLengthMicros,
			@Nullable String remoteHost,
			@Nullable String remoteAddr,
			@Nullable String userAgentHeader,
			@Nullable String requestUrl,
			@Nullable String queryString) {
		this.user = user;
		this.requestType = checkNotNull(requestType);
		this.dataSizeBytes = dataSize;
		this.timeOfRequest = timeOfRequest;
		this.responseTimeNanos = responseTimeNanos;
		this.dataSnapshotPubId = checkNotNull(dataSnapshotPubId);
		this.dataSnapshotName = dataSnapshotName;
		this.dataStartUutc = dataStartUutc;
		this.dataLengthMicros = dataLengthMicros;

		if (remoteHost != null && remoteHost.length() > 255) {
			remoteHost = remoteHost.substring(0, 254);
		}
		this.remoteHost = remoteHost;

		this.remoteAddr = remoteAddr;

		if (userAgentHeader != null && userAgentHeader.length() > 255) {
			userAgentHeader = userAgentHeader.substring(0, 254);
		}
		this.userAgentHeader = userAgentHeader;

		if (requestUrl != null && requestUrl.length() > 2000) {
			requestUrl = requestUrl.substring(0, 1999);
		}
		this.requestUrl = requestUrl;

		if (queryString != null && queryString.length() > 2000) {
			queryString = queryString.substring(0, 1999);
		}
		this.queryString = queryString;
	}

	@NotNull
	public Long getDataLengthMicros() {
		return dataLengthMicros;
	}

	@NotNull
	@Min(0)
	@Column(name = "data_size")
	// really should be changed to data_size_bytes, but would be more
	// inconvenient than payoff
	public Long getDataSizeBytes() {
		return dataSizeBytes;
	}

	@NotNull
	public Long getDataSnapshotId() {
		return dataSnapshotId;
	}

	/**
	 * Hold on to this since the data snapshot could get deleted and it would be
	 * nice to have the name of it.
	 * 
	 * @return the name of the data snapshot usage data was used
	 */
	@NotNull
	@Size(min = 1, max = 255)
	public String getDataSnapshotName() {
		return dataSnapshotName;
	}

	/**
	 * Use the pub id instead of a {@link DataSnapshotEntity} since a data
	 * snapshot can get deleted.
	 */
	@NotNull
	@Size(min = 36, max = 36)
	public String getDataSnapshotPubId() {
		return dataSnapshotPubId;
	}

	@NotNull
	@Min(-1)
	public Long getDataStartUutc() {
		return dataStartUutc;
	}

	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Size(max = 2000)
	public String getQueryString() {
		return queryString;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getRemoteAddr() {
		return remoteAddr;
	}

	@NotNull
	@Size(min = 7 /* 0.0.0.0 */, max = 255 /* allow for IPv6 */)
	public String getRemoteHost() {
		return remoteHost;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	public DataRequestType getRequestType() {
		return requestType;
	}

	@NotNull
	@Size(min = 1, max = 2000)
	public String getRequestUrl() {
		return requestUrl;
	}

	@NotNull
	@Min(0)
	public Long getResponseTimeNanos() {
		return responseTimeNanos;
	}

	@NotNull
	@Past
	public Date getTimeOfRequest() {
		return timeOfRequest;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = UserEntity.ID_COLUMN)
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getUserAgentHeader() {
		return userAgentHeader;
	}

	@SuppressWarnings("unused")
	private void setDataLengthMicros(Long dataLengthMicros) {
		this.dataLengthMicros = dataLengthMicros;
	}

	public void setDataSizeBytes(Long dataSize) {
		this.dataSizeBytes = dataSize;
	}

	public void setDataSnapshotId(Long dataSnapshotId) {
		this.dataSnapshotId = dataSnapshotId;
	}

	public void setDataSnapshotName(String dataSnapshotName) {
		this.dataSnapshotName = dataSnapshotName;
	}

	@SuppressWarnings("unused")
	private void setDataSnapshotPubId(String dataSnapshotPubId) {
		this.dataSnapshotPubId = dataSnapshotPubId;
	}

	@SuppressWarnings("unused")
	private void setDataStartUutc(Long dataStartUutc) {
		this.dataStartUutc = dataStartUutc;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setQueryString(@Nullable String queryString) {
		if (queryString != null && queryString.length() > 2000) {
			queryString = queryString.substring(0, 1999);
		}
		this.queryString = queryString;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public void setRemoteHost(String remoteHost) {
		if (remoteHost.length() > 255) {
			remoteHost = remoteHost.substring(0, 254);
		}
		this.remoteHost = remoteHost;
	}

	@SuppressWarnings("unused")
	private void setRequestType(DataRequestType requestType) {
		this.requestType = requestType;
	}

	public void setRequestUrl(String requestUrl) {
		if (requestUrl.length() > 2000) {
			requestUrl = requestUrl.substring(0, 1999);
		}
		this.requestUrl = requestUrl;
	}

	public void setResponseTimeNanos(Long responseTimeNanos) {
		this.responseTimeNanos = responseTimeNanos;
	}

	public void setTimeOfRequest(Date timeOfRequest) {
		this.timeOfRequest = timeOfRequest;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public void setUserAgentHeader(@Nullable String userAgentHeader) {
		if (userAgentHeader.length() > 255) {
			userAgentHeader = userAgentHeader.substring(0, 254);
		}
		this.userAgentHeader = userAgentHeader;
	}

	public static final EntityConstructor<DataUsageEntity, Long, String> CONSTRUCTOR =
		new EntityConstructor<DataUsageEntity,Long,String>(DataUsageEntity.class,
				new IPersistentObjectManager.IPersistentKey<DataUsageEntity,Long>() {

			@Override
			public Long getKey(DataUsageEntity o) {
				return o.getId();
			}

			@Override
			public void setKey(DataUsageEntity o, Long newKey) {
				o.setId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<DataUsageEntity,Long,String>() {

			@Override
			public DataUsageEntity create(Long key, String snapshotId) {
				// TODO: Populate the DataUsageEntity?
				return new DataUsageEntity(null,snapshotId);
			}
		}
				);
	
	@Override
	public EntityConstructor<DataUsageEntity, Long, String> tableConstructor() {
		return CONSTRUCTOR;
	}
}
