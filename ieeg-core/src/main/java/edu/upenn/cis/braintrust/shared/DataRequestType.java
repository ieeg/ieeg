/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public enum DataRequestType {
	/** Data to be viewed in the viewer. */
	VIEWER,

	/** From the csv download. */
	CSV_DOWNLOAD,

	/** From the csv raw download */
	CSV_DOWNLOAD_RAW,

	/** unscaled raw red web service. */
	WS_UNSCALED_RAW_RED,

	/** unscaled raw binary web service */
	WS_UNSCALED_RAW_BINARY,

	/** get time series details */
	WS_TIME_SERIES_DETAILS,

	/** raw mef. */
	WS_DIRECT_DATA,
	
	/** object file */
	WS_RECORDING_OBJECT;
}
