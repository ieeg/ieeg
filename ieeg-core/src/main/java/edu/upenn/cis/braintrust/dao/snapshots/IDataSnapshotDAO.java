/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots;

import java.util.Set;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

/**
 * {@link DataSnapshotEntity} DAO
 * 
 * @author John Frommeyer
 */
public interface IDataSnapshotDAO extends IDAO<DataSnapshotEntity, Long> {

	Set<EegStudy> findBySearch(EegStudySearch dataSnapshotSearch,
			UserEntity userEntity, User user);
	
	Set<Dataset> findForUser(UserEntity userEntity, User user);

	TsAnnotationEntity findTsAnn(DataSnapshotEntity ds, String tsAnnRevId);

	/**
	 * Returns the total number of annotations in {@code ds}.
	 * 
	 * @param ds
	 * @return the total number of annotations in {@code ds}
	 */
	long countTsAnns(DataSnapshotEntity ds);

	long countTsAnns();

	/**
	 * Returns the total number of snapshots in the database.
	 */
	long countAllSnapshots();

	/**
	 * Returns the total number of active (frozen) snapshots in the database.
	 */
	long countActiveSnapshots();

	/**
	 * Returns the total number of annotations on the time series {@code ts} in
	 * {@code ds}.
	 * 
	 * @param ds
	 * @param ts
	 * @return the total number of annotations on the time series {@code ts} in
	 *         {@code ds}
	 */
	long countTsAnns(DataSnapshotEntity ds, TimeSeriesEntity ts);

	DataSnapshotEntity findByLabel(String label);

	Set<DataSnapshotEntity> findAnyWithLabel(String label);

	boolean isInAProject(DataSnapshotEntity ds);
}
