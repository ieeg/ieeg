/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Objects;

@GwtCompatible(serializable = true)
public abstract class IdDto<T extends Serializable>
		implements Serializable {

	private static final long serialVersionUID = 1L;

	private T id;

	/** For GWT. */
	protected IdDto() {}

	public IdDto(T id) {
		this.id = checkNotNull(id);
	}

	/**
	 * If true then the id's are equal.
	 */
	@Override
	public boolean equals(@Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof IdDto)) {
			return false;
		}

		IdDto<?> other = (IdDto<?>) obj;
		if (!Objects.equal(id, other.id)) {
			return false;
		}

		// Now let's make sure one is an ancestor of the other.
		@SuppressWarnings("rawtypes")
		Class<? extends IdDto> thisClass = getClass();

		@SuppressWarnings("rawtypes")
		Class<? extends IdDto> otherClass = other.getClass();

		if (thisClass.isAssignableFrom(otherClass)
				|| otherClass.isAssignableFrom(thisClass)) {

		} else {
			return false;
		}
		return true;
	}

	/**
	 * {@code final} because it's used in {@link #equals(Object)}.
	 */
	public final T getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "IdDto [id=" + id + "]";
	}

}
