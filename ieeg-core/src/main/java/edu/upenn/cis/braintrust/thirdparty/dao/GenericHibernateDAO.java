/*
 * Copyright (c) 2005, Christian Bauer <christian@hibernate.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the original author nor the names of contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES of MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT MANAGE_ACL OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT of
 * SUBSTITUTE GOODS OR SERVICES; LOSS of USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY of LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT of THE USE of THIS SOFTWARE, EVEN IF ADVISED of
 * THE POSSIBILITY of SUCH DAMAGE.
 */
package edu.upenn.cis.braintrust.thirdparty.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.SimpleNaturalIdLoadAccess;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.shared.StringKeyValue;
import edu.upenn.cis.db.habitat.daemons.HabitatCore;
import edu.upenn.cis.db.habitat.persistence.Scope;
import edu.upenn.cis.db.habitat.persistence.object.HibernateScope;

/**
 * From http://www.hibernate.org/328.html.
 * 
 * @param <T> transfer object type
 * @param <ID> the type of the Hibernate id - usually {@link Long}
 */
public abstract class GenericHibernateDAO<T, ID extends Serializable>
		implements IDAO<T, ID> {

	private final Class<T> persistentClass;
	private Session session;
	private SessionFactory sessFac = HibernateUtil.getSessionFactory();

	@SuppressWarnings("unchecked")
	public GenericHibernateDAO() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public void clear() {
		getSession().clear();
	}

	@Override
	public void delete(final T entity) {
		getSession().delete(entity);
	}

	@Override
	public void evict(T entity) {
		session.evict(entity);
	}
	
	@Override
	public void evict(Iterable<? extends T> entities) { 
		for (T entity : entities) {
			evict(entity);
		}
	}

	@Override
	public List<T> findAll() {
		return findByCriteria();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAllOrderBy(String property) {
		Criteria crit = getSession().createCriteria(getPersistentClass())
				.addOrder(Order.asc(property));
		return crit.list();
	}

	@Override
	public ScrollableResults findAllScroll() {
		return getSession()
				.createCriteria(getPersistentClass())
				.scroll(ScrollMode.FORWARD_ONLY);
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByCriteria(final Criterion... criterion) {
		final Criteria crit = getSession().createCriteria(getPersistentClass());
		for (final Criterion c : criterion) {
			crit.add(c);
		}
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<T> findByExample(final T exampleInstance,
			final String... excludeProperty) {
		final Criteria crit = getSession().createCriteria(getPersistentClass());
		final Example example = Example.create(exampleInstance);
		for (final String exclude : excludeProperty) {
			example.excludeProperty(exclude);
		}
		crit.add(example);
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T findById(final ID id, final boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getSession().load(getPersistentClass(), id,
					LockOptions.UPGRADE);
		} else {
			entity = (T) getSession().load(getPersistentClass(), id);
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findByNaturalId(String naturalId) {
		SimpleNaturalIdLoadAccess la =
				getSession().bySimpleNaturalId(getPersistentClass());
		return (T) la.load(naturalId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findByNaturalId(String naturalId, boolean upgradeLock) {
		if (upgradeLock) {
			SimpleNaturalIdLoadAccess la =
					getSession().bySimpleNaturalId(getPersistentClass()).with(
							LockOptions.UPGRADE);
			return (T) la.load(naturalId);
		} else {
			return findByNaturalId(naturalId);
		}
	}

	@Override
	public void flush() {
		getSession().flush();
	}

	@Override
	public void forceVersIncrPes(T entity) {
		session.buildLockRequest(
				new LockOptions(LockMode.PESSIMISTIC_FORCE_INCREMENT))
				.lock(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T get(final ID id) {
		return (T) getSession().get(getPersistentClass(), id);
	}

	public Class<T> getPersistentClass() {
		return persistentClass;
	}

	protected Session getSession() {
		if (session == null) {
			session = sessFac.getCurrentSession();
		}
		return session;
	}

	@SuppressWarnings("unchecked")
	public T load(ID id) {
		return (T) getSession().load(persistentClass, id);
	}

	@Override
	public void lock(T entity) {
		if (entity == null) {
			return;
		}
		getSession().buildLockRequest(LockOptions.UPGRADE).lock(entity);
	}

	@Override
	public void saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
	}

	@Override
	public void setReadOnly(T entity, boolean readOnly) {
		session.setReadOnly(entity, readOnly);
	}

	public void setSession(Session s) {
		this.session = checkNotNull(s);
	}

	public void refresh(T entity, boolean upgradeLock) {
		if (upgradeLock) {
			session.refresh(entity, LockOptions.UPGRADE);
		} else {
			session.refresh(entity);
		}
	}

	protected static boolean isMysql(Configuration hibernateConfig) {
		boolean isMysql = false;
		if (hibernateConfig.getProperty(Environment.DIALECT).contains("MySQL")) {
			isMysql = true;
		}
		return isMysql;
	}

	public void setScope(Scope s) {
		HibernateScope scope = (HibernateScope)s;
		setSession(scope.getSession());
	}
	
	public synchronized Scope getDefaultScope() {
		try {
			return new HibernateScope(getSession());
		} catch (org.hibernate.HibernateException hbe) {
			SessionAndTrx stx = PersistenceUtil.getSessAndTrx(sessFac);
			
			stx.trx.get().rollback();
			return new HibernateScope(stx.session);
		}
	}
}
