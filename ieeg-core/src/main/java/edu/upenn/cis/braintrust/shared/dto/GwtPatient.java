/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;

/**
 * Contains information about a patient which is constant across multiple
 * admissions. Information specific to a particular admission goes in
 * {@link GwtHospitalAdmission}.
 * 
 * @author Sam Donnelly
 */
public class GwtPatient implements IHasLongId, IHasVersion, Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer version;
	private GwtOrganization organization;
	private String label;
	private Handedness handedness;
	private Ethnicity ethnicity;
	private Gender gender;
	private String etiology;
	private Optional<String> developmentalDelay;
	private Optional<Boolean> developmentalDisorders;
	private Optional<Boolean> traumaticBrainInjury;
	private Optional<Boolean> familyHistory;
	private Optional<Integer> ageOfOnset;
	private Set<GwtHospitalAdmission> admissions = newHashSet();
	private Set<Precipitant> precipitants = newHashSet();
	private Set<SeizureType> szTypes = newHashSet();

	public GwtPatient() {}

	public GwtPatient(
			String label,
			GwtOrganization org,
			Handedness handedness,
			Ethnicity ethnicity,
			Gender gender,
			String etiology,
			@Nullable String developementalDelay,
			@Nullable Boolean developmentalDisorders,
			@Nullable Boolean traumaticBrainInjury,
			@Nullable Boolean familyHistory,
			@Nullable Integer ageOfOnset,
			@Nullable Long id,
			@Nullable Integer version) {
		this.label = checkNotNull(label);
		this.organization = checkNotNull(org);
		this.handedness = checkNotNull(handedness);
		this.ethnicity = checkNotNull(ethnicity);
		this.gender = checkNotNull(gender);
		this.etiology = checkNotNull(etiology);
		this.developmentalDelay = Optional.fromNullable(developementalDelay);
		this.developmentalDisorders = Optional
				.fromNullable(developmentalDisorders);
		this.traumaticBrainInjury = Optional.fromNullable(traumaticBrainInjury);
		this.familyHistory = Optional.fromNullable(familyHistory);
		this.ageOfOnset = Optional.fromNullable(ageOfOnset);
		this.id = id;
		this.version = version;
	}

	/**
	 * Add the admission to this patient and set this patient as the admission's
	 * parent.
	 * 
	 * @param admission case to be added to this patient case and have its
	 *            parent set to this patient
	 * 
	 * @throws IllegalArgumentException if {@code admission} already has a
	 *             parent
	 * @throws IllegalArgumentException if this patient already contains
	 *             {@code admission}
	 */
	public void addAdmission(final GwtHospitalAdmission admission) {
		checkNotNull(admission);
		checkArgument(admission.getParent() == null,
				"epilepsyCase already has a parent");
		checkArgument(!admissions.contains(admission),
				"epilepsyCase is already contained in this patient");
		admissions.add(admission);
		admission.setParent(this);
	}

	public Set<GwtHospitalAdmission> getAdmissions() {
		return admissions;
	}

	/**
	 * @return the ageOfOnset
	 */
	public Optional<Integer> getAgeOfOnset() {
		return ageOfOnset;
	}

	/**
	 * @return the developmentalDelay
	 */
	public Optional<String> getDevelopmentalDelay() {
		return developmentalDelay;
	}

	public Optional<Boolean> getDevelopmentalDisorders() {
		return developmentalDisorders;
	}

	public Ethnicity getEthnicity() {
		return ethnicity;
	}

	/**
	 * @return the etiology
	 */
	public String getEtiology() {
		return etiology;
	}

	/**
	 * @return the familyHistory
	 */
	public Optional<Boolean> getFamilyHistory() {
		return familyHistory;
	}

	public Gender getGender() {
		return gender;
	}

	public Handedness getHandedness() {
		return handedness;
	}

	public Long getId() {
		return id;
	}

	public GwtOrganization getOrganization() {
		return organization;
	}

	/**
	 * @return the situations
	 */
	public Set<Precipitant> getPrecipitants() {
		return precipitants;
	}

	/**
	 * @return the szTypes
	 */
	public Set<SeizureType> getSzTypes() {
		return szTypes;
	}

	/**
	 * @return the traumaticBrainInjury
	 */
	public Optional<Boolean> getTraumaticBrainInjury() {
		return traumaticBrainInjury;
	}

	public String getLabel() {
		return label;
	}

	public Integer getVersion() {
		return version;
	}

	/**
	 * Remove {@code epilepsyCase} from this patient and {@code null} out
	 * {@code epilepsyCase}'s parent.
	 * 
	 * @param admission to be removed and deparented
	 * 
	 * @throws IllegalArgumentException if this patient is not the cases's
	 *             parent
	 * @throws IllegalArgumentException if the case is not contained in this
	 *             patient
	 */
	public void removeAdmission(final GwtHospitalAdmission admission) {
		checkNotNull(admission);
		checkArgument(equals(admission.getParent()),
				"this patient is not admission's parent");
		checkArgument(admissions.contains(admission),
				"this patient does not contain admission");
		admissions.remove(admission);
		admission.setParent(null);
	}

	/**
	 * @param ageOfOnset the ageOfOnset to set
	 */
	public void setAgeOfOnset(@Nullable final Integer ageOfOnset) {
		this.ageOfOnset = Optional.fromNullable(ageOfOnset);
	}

	/**
	 * @param developmentalDelay the developmentalDelay to set
	 */
	public void setDevelopmentalDelay(
			@Nullable final String developmentalDelay) {
		this.developmentalDelay = Optional.fromNullable(developmentalDelay);
	}

	/**
	 * @param developmentalDisorders the developmentalDisorders to set
	 */
	public void setDevelopmentalDisorders(
			@Nullable final Boolean developmentalDisorders) {
		this.developmentalDisorders = Optional
				.fromNullable(developmentalDisorders);
	}

	public void setEthnicity(final Ethnicity ethnicity) {
		this.ethnicity = ethnicity;
	}

	/**
	 * @param etiology the etiology to set
	 */
	public void setEtiology(final String etiology) {
		this.etiology = etiology;
	}

	/**
	 * @param familyHistory the familyHistory to set
	 */
	public void setFamilyHistory(@Nullable final Boolean familyHistory) {
		this.familyHistory = Optional.fromNullable(familyHistory);
	}

	public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public void setHandedness(final Handedness handedness) {
		this.handedness = handedness;
	}

	public void setId(@Nullable final Long id) {
		this.id = id;
	}

	public void setOrganization(final GwtOrganization institution) {
		this.organization = institution;
	}

	/**
	 * @param traumaticBrainInjury the traumaticBrainInjury to set
	 */
	public void setTraumaticBrainInjury(
			@Nullable final Boolean traumaticBrainInjury) {
		this.traumaticBrainInjury = Optional.fromNullable(traumaticBrainInjury);
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public void setVersion(@Nullable final Integer version) {
		this.version = version;
	}

}
