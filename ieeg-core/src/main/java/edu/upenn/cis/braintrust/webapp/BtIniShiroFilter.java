/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.servlet.IniShiroFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.io.CharStreams;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;

/**
 * Wraps {@link IniShiroFilter} so we can get the configuration from
 * {context-path}/config/shiro.ini on the classpath.
 * 
 * @author Sam Donnelly
 */
public class BtIniShiroFilter implements Filter {

	private IniShiroFilter iniShiroFilter;
	private static Logger logger =
			LoggerFactory.getLogger(BtIniShiroFilter.class);

	@Override
	public void destroy() {
		iniShiroFilter.destroy();
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		iniShiroFilter.doFilter(req, resp, chain);
	}

	@Override
	public void init(FilterConfig cfg)
			throws ServletException {
		iniShiroFilter = new IniShiroFilter();
		String m = "init(...)";
		InputStream is = null;
		try {
			if (AwsUtil.isConfigFromS3()) {
				logger.info("reading shiro.ini from S3");
				String configKey = null;
				configKey = AwsUtil.getConfigS3Dir() + "/" +
						"shiro.ini";
				GetObjectRequest getObjectRequest =
						new GetObjectRequest(
								AwsUtil.getConfigS3Bucket(),
								configKey);
				logger.info("reading shiro.ini from ["
						+ AwsUtil.getConfigS3Bucket() + "] ["
						+ configKey + "]");
				S3Object s3Object = AwsUtil.getS3().getObject(
						getObjectRequest);
				is = s3Object.getObjectContent();
			} else {
				String configName = BtUtil.getConfigName(
						cfg.getServletContext(),
						"shiro.ini");
				logger.info("{}: looking for shiro.ini at [{}]", m,
						configName);
				URL url = getClass().getClassLoader().getResource(configName);
				if (url == null) {
					logger.info(
							"{}: looking for shiro.ini at class path root",
							m);
					url = getClass().getClassLoader()
							.getResource("shiro.ini");
				}

				logger.info("{}: reading shiro.ini from {}", m, url);
				is = url.openStream();
			}

			String config = CharStreams.toString(new InputStreamReader(is));

			iniShiroFilter.setConfig(config);
			iniShiroFilter.init(cfg);

		} catch (Throwable t) {
			logger.error("caught exception", t);
			throw new ExceptionInInitializerError(t);
		} finally {
			BtUtil.close(is);
		}
	}
	// iniShiroFilter = new IniShiroFilter();
	//
	// String configName = BtUtil.getConfigName(
	// cfg.getServletContext(),
	// "shiro.ini");
	// logger.info(
	// "looking for shiro.ini at ["
	// + configName
	// + "]");
	// URL url = getClass().getClassLoader().getResource(configName);
	// if (url == null) {
	// logger.info("reading shiro.ini from default location");
	// } else {
	// logger.info("reading shiro.ini from "
	// + url.toString());
	// iniShiroFilter.setConfigPath(
	// ResourceUtils.URL_PREFIX + url.toString());
	// }
	// iniShiroFilter.init(cfg);
	// }
}
