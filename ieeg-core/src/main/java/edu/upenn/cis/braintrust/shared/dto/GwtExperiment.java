/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import javax.annotation.Nullable;

/**
 * @author John Frommeyer
 */
public class GwtExperiment extends GwtSnapshot {
	private static final long serialVersionUID = 1L;
	private String age;
	// private Set<DrugAdminEntity> drugAdmins = newHashSet();
	private GwtStimType stimType;
	private GwtStimRegion stimRegion;
	private Integer stimIsiMs;
	private Integer stimDurationMs;
	private boolean published;

	@SuppressWarnings("unused")
	private GwtExperiment() {}

	/**
	 * This constructor sets both sides of the Animal/Experiment relationship.
	 * 
	 * @param label
	 * @param recording
	 * @param published TODO
	 * @param age
	 * @param stimulationType
	 * @param stimulationRegion
	 * @param stimulationIsiMs
	 * @param stimulationDurationMs
	 * @param id
	 * @param version
	 */
	public GwtExperiment(
			String label,
			@Nullable GwtRecording recording,
			boolean published,
			@Nullable String age,
			@Nullable GwtStimType stimulationType,
			@Nullable GwtStimRegion stimulationRegion,
			@Nullable Integer stimulationIsiMs,
			@Nullable Integer stimulationDurationMs,
			@Nullable Long id,
			@Nullable Integer version) {
		super(label, recording, id, version);

		this.published = published;
		this.age = age;
		this.stimType = stimulationType;
		this.stimRegion = stimulationRegion;
		this.stimIsiMs = stimulationIsiMs;
		this.stimDurationMs = stimulationDurationMs;
	}

	public String getAge() {
		return age;
	}

	// public Set<DrugAdminEntity> getDrugAdmins() {
	// return drugAdmins;
	// }

	public Integer getStimDurationMs() {
		return stimDurationMs;
	}

	public Integer getStimIsiMs() {
		return stimIsiMs;
	}

	public GwtStimRegion getStimRegion() {
		return stimRegion;
	}

	public GwtStimType getStimType() {
		return stimType;
	}

	public boolean isPublished() {
		return published;
	}

}
