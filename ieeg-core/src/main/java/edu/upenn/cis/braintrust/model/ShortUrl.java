/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name=ShortUrl.TABLE)
public class ShortUrl {
	public static final String TABLE = "short_url";
	
	public static enum BOOKMARK_TYPE {IMAGE, EEG};
	
	private Long shortId;
	
	private BOOKMARK_TYPE bookmarkType;
	
	private EegBookmarkEntity eegBookmark;
	private ImageBookmarkEntity imageBookmark;

	public ShortUrl() {}
	
	
	
	public ShortUrl(BOOKMARK_TYPE bookmarkType, EegBookmarkEntity eegBookmark) {
		super();
		this.bookmarkType = bookmarkType;
		this.eegBookmark = eegBookmark;
	}



	public ShortUrl(BOOKMARK_TYPE bookmarkType, ImageBookmarkEntity imageBookmark) {
		super();
		this.bookmarkType = bookmarkType;
		this.imageBookmark = imageBookmark;
	}



	public ShortUrl(Long shortId, BOOKMARK_TYPE bookmarkType,
			ImageBookmarkEntity imageBookmark) {
		super();
		this.shortId = shortId;
		this.bookmarkType = bookmarkType;
		this.imageBookmark = imageBookmark;
	}



	public ShortUrl(Long shortId, BOOKMARK_TYPE bookmarkType,
			EegBookmarkEntity eegBookmark) {
		super();
		this.shortId = shortId;
		this.bookmarkType = bookmarkType;
		this.eegBookmark = eegBookmark;
	}



	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "url_id", unique = true, nullable = false)
	public Long getShortId() {
		return shortId;
	}

	public void setShortId(Long shortId) {
		this.shortId = shortId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "FK_SHORTURL_EEG")
	@JoinColumn(name = "eeg_bookmark")
	@Nullable
	public EegBookmarkEntity getEegBookmark() {
		return eegBookmark;
	}

	public void setEegBookmark(EegBookmarkEntity bookmark) {
		this.eegBookmark = bookmark;
	}

	public BOOKMARK_TYPE getBookmarkType() {
		return bookmarkType;
	}

	public void setBookmarkType(BOOKMARK_TYPE bookmarkType) {
		this.bookmarkType = bookmarkType;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "FK_SHORTURL_IMAGE")
	@JoinColumn(name = "image_bookmark")
	@Nullable
	public ImageBookmarkEntity getImageBookmark() {
		return imageBookmark;
	}

	public void setImageBookmark(ImageBookmarkEntity imageBookmark) {
		this.imageBookmark = imageBookmark;
	}
	
	
}
