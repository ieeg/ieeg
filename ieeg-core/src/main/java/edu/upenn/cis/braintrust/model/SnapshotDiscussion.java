/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.sql.Timestamp;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;

@Entity
@Table(name = SnapshotDiscussion.TABLE)
@NamedQueries({
		@NamedQuery(
				name = SnapshotDiscussion.FIND_BY_ID,
				query = "from SnapshotDiscussion sd "
						+ "where sd.pubId = :pubId"),
		@NamedQuery(
				name = SnapshotDiscussion.FIND_BY_DATASET,
				query = "from SnapshotDiscussion sd "
						+ "where sd.snapshot = :snapId "// and sd.refersTo is
														// null "
						+ "order by time desc"),

		// Useful for threads
		@NamedQuery(
				name = SnapshotDiscussion.FIND_REPLY_TO,
				query = "from SnapshotDiscussion sd "
						+ "where sd.snapshot = :snapId and (sd.refersTo is not null and sd.refersTo.pubId = :pubId) "
						+ "order by time desc")
})
public class SnapshotDiscussion implements Comparable<SnapshotDiscussion>,
		IHasPubId {
	public static final String TABLE = "snapshot_discussion";
	public static final String UPPER_CASE_TABLE = "SNAPSHOT_DISCUSSION";
	public static final String FIND_BY_ID = "SnapshotDiscussion-findByID";
	public static final String FIND_BY_DATASET = "SnapshotDiscussion-findByDataSetID";
	public static final String FIND_REPLY_TO = "SnapshotDiscussion-findByDataSetReplyID";

	private Long discussId;

	private String pubId;
	private UserEntity user;
	private Integer version;
	private DataSnapshotEntity snapshot;
	private String title;
	private String posting;
	private Timestamp time;

	private SnapshotDiscussion refersTo;
	private TsAnnotationEntity annotation;

	/**
	 * For JPA.
	 */
	SnapshotDiscussion() {}

	public SnapshotDiscussion(final String title,
			final String posting,
			final DataSnapshotEntity snapshot,
			final UserEntity user,
			@Nullable final SnapshotDiscussion refersTo,
			@Nullable final TsAnnotationEntity annotation) {
		this.title = title;
		this.posting = posting;
		this.snapshot = snapshot;

		this.user = user;
		this.refersTo = refersTo;
		this.annotation = annotation;
		if (pubId == null)
			pubId = newUuid();

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	public SnapshotDiscussion(final String title,
			final String value,
			final DataSnapshotEntity snap,
			final UserEntity userId,
			@Nullable final SnapshotDiscussion refersTo,
			final String id,
			@Nullable final TsAnnotationEntity annotation) {
		this.title = title;
		this.posting = value;
		this.snapshot = snap;
		this.pubId = id;

		this.user = userId;
		this.refersTo = refersTo;
		this.annotation = annotation;
		if (pubId == null)
			pubId = newUuid();

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "post_id", unique = true, nullable = false)
	public Long getDiscussId() {
		return discussId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_ds_discuss_snap")
	@JoinColumn(name = "snapshot_id")
	@NotNull
	public DataSnapshotEntity getSnapshot() {
		return snapshot;
	}

	@Lob
	@Column
	@NotNull
	public String getPosting() {
		return posting;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setDiscussId(final Long id) {
		this.discussId = id;
	}

	public void setSnapshot(final DataSnapshotEntity parent) {
		this.snapshot = parent;
	}

	public void setPosting(final String value) {
		this.posting = value;
	}

	public void setVersion(final Integer version) {
		this.version = version;
	}

	@NotNull
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Nullable
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_discuss_post_parent")
	@JoinColumn(name = "refers_id")
	public SnapshotDiscussion getRefersTo() {
		return refersTo;
	}

	public void setRefersTo(SnapshotDiscussion refers) {
		refersTo = refers;
	}

	@Nullable
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_discuss_post_annotation")
	public TsAnnotationEntity getAnnotation() {
		return annotation;
	}

	public void setAnnotation(TsAnnotationEntity annotationId) {
		this.annotation = annotationId;
	}

	@Override
	public int compareTo(SnapshotDiscussion arg0) {
		return time.compareTo(arg0.time);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_discuss")
	@JoinColumn(name = "user_id")
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	/**
	 * @return the id
	 */
	@NaturalId
	@Override
	@Size(min = 36, max = 36)
	@NotNull
	public String getPubId() {
		return pubId;
	}

	public void setPubId(String id) {
		pubId = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
