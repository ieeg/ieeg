/*
 * Copyright (C) 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@Entity(name = "TsAnnotation")
@NamedNativeQueries({
		@NamedNativeQuery(
				name = TsAnnotationEntity.DELETE_BY_PARENT_AND_ID,
				query = "delete from ts_annotation where ts_annotation_id = :tsaId and parent_id = :parentId"),
		@NamedNativeQuery(
				name = TsAnnotationEntity.DELETE_BY_PARENT_AND_IDS,
				query = "delete from ts_annotation where ts_annotation_id IN (:tsaIds) and parent_id = :parentId")
})
@NamedQueries({
		@NamedQuery(
				name = TsAnnotationEntity.FIND_BY_ANNOTATED,
				query = "from TsAnnotation tsa "
						+ "where tsa.parent = :parent "
						+ "and :annotated member of tsa.annotated"),
		@NamedQuery(
				name = TsAnnotationEntity.FIND_BY_DATA_SNAPSHOT,
				query = "from TsAnnotation tsa "
						+ "where tsa.parent = :ds"),
		@NamedQuery(
				name = TsAnnotationEntity.BY_PARENT_ORDER_BY_ID,
				query = "from TsAnnotation tsa "
						+ "where tsa.parent = :parent "
						+ "order by tsa.startOffsetUsecs, tsa.id"),
		@NamedQuery(
				name = TsAnnotationEntity.EXISTS_TS_ANNS,
				query = "select 1 from TsAnnotation tsa where tsa.parent = :ds"),
		@NamedQuery(
				name = TsAnnotationEntity.COUNT_LAYERS,
				query = "select tsa.layer, count(tsa) from TsAnnotation tsa where tsa.parent = :parent group by tsa.layer"),
		@NamedQuery(
				name = TsAnnotationEntity.BY_LAYER_AND_START_TIME,
				query = "select distinct tsa "
						+ "from TsAnnotation tsa "
						+ "where tsa.parent = :parent "
						+ "and tsa.startOffsetUsecs >= :startOffsetUsecs "
						+ "and tsa.layer = :layer "
						+ "order by tsa.startOffsetUsecs, tsa.id"),
		@NamedQuery(
				name = TsAnnotationEntity.BY_LAYER_AND_START_TIME_CONTAINS,
				query = "select distinct tsa "
						+ "from TsAnnotation tsa "
						+ "join tsa.annotated ts "
						+ "where tsa.parent = :parent "
						+ "and tsa.startOffsetUsecs >= :startOffsetUsecs "
						+ "and tsa.layer = :layer "
						+ "and ts in (:timeSeries) "
						+ "order by tsa.startOffsetUsecs, tsa.id"),
		@NamedQuery(
				name = TsAnnotationEntity.BY_LAYER_FROM_ID_AND_START,
				query = "select distinct tsa "
						+ "from TsAnnotation tsa "
						+ "join tsa.annotated ts "
						+ "where tsa.parent = :parent "
						+ "and tsa.id > :lastTsaId "
						+ "and tsa.startOffsetUsecs >= :startOffsetUsecs "
						+ "and tsa.layer = :layer "
						+ "and ts.pubId in (:tsPubIds) "
						+ "order by tsa.startOffsetUsecs, tsa.id"),
		@NamedQuery(
				name = TsAnnotationEntity.LT_START_TIME,
				query = "select distinct tsa "
						+ "from TsAnnotation tsa "
						+ "where tsa.parent = :parent "
						+ "and tsa.startOffsetUsecs < :startOffsetUsecs "
						+ "and tsa.layer = :layer "
						+ "order by tsa.startOffsetUsecs desc, tsa.id asc"),
		@NamedQuery(
				name = TsAnnotationEntity.LT_START_TIME_CONTAINS,
				query = "select distinct tsa "
						+ "from TsAnnotation tsa "
						+ "join tsa.annotated ts "
						+ "where tsa.parent = :parent "
						+ "and tsa.startOffsetUsecs < :startOffsetUsecs "
						+ "and tsa.layer = :layer "
						+ "and ts in (:timeSeries) "
						+ "order by tsa.startOffsetUsecs desc, tsa.id asc"),
		@NamedQuery(
				name = TsAnnotationEntity.COUNT_BY_PARENT_START_TIME,
				query = "select count(*) "
						+ "from TsAnnotation tsa "
						+ "where tsa.parent = :parent "
						+ "and tsa.startOffsetUsecs = :startOffsetUsecs"),
		@NamedQuery(
				name = TsAnnotationEntity.SET_LAYER_NAME,
				query = "update versioned TsAnnotation tsa set tsa.layer = :newName where tsa.parent = :parent and tsa.layer = :originalName"),
		@NamedQuery(
				name = TsAnnotationEntity.FOR_STREAMING_BY_LAYER,
				query = "select tsa "
						+ "from TsAnnotation tsa "
						+ "join fetch tsa.annotated "
						+ "where tsa.parent = :parent "
						+ "order by tsa.layer, tsa.startOffsetUsecs, tsa.id, tsa.parent.id"),
})
@Table(name = TsAnnotationEntity.TABLE)
public class TsAnnotationEntity
		implements IHasLongId, IHasPubId, IEntity {

	public static final Function<TsAnnotationEntity, Set<TimeSeriesEntity>> getAnnotated = new Function<TsAnnotationEntity, Set<TimeSeriesEntity>>() {

		@Override
		public Set<TimeSeriesEntity> apply(TsAnnotationEntity input) {
			return input.getAnnotated();
		}
	};

	public static final Function<TsAnnotationEntity, String> getLayer = new Function<TsAnnotationEntity, String>() {

		@Override
		public String apply(TsAnnotationEntity input) {
			return input.getLayer();
		}

	};

	public static final String TABLE = "ts_annotation";
	public static final String ID_COLUMN =
			TsAnnotationEntity.TABLE + "_id";
	public static final String START_TIME_COLUMN = "start_time";
	public static final String END_TIME_COLUMN = "end_time";

	public static final String COUNT_LAYERS = "TsAnnotation-countLayers";
	public static final String FIND_BY_ANNOTATED = "TsAnnotation-findByAnnotated";
	public static final String FIND_BY_DATA_SNAPSHOT = "TsAnnotation-findByDataSnapshot";
	public static final String INSERT_TS_ANN = "TsAnnotation-insertTsAnn";
	public static final String INSERT_TS_ANNS = "TsAnnotation-insertTsAnns";
	public static final String BY_PARENT_ORDER_BY_ID = "TsAnnotation-byParentOrderById";
	public static final String BY_LAYER_AND_START_TIME = "TsAnnotation-byParentOrderByStartTimeAndId";
	public static final String FOR_STREAMING_BY_LAYER = "TsAnnotation-forStreamingByLayer";
	public static final String BY_LAYER_AND_START_TIME_CONTAINS = "TsAnnotation-byParentOrderByStartTimeAndIdContains";
	public static final String LT_START_TIME = "TsAnnotation-lessThanStart";
	public static final String LT_START_TIME_CONTAINS = "TsAnnotation-lessThanStartContains";
	public static final String BY_LAYER_FROM_ID_AND_START = "TsAnnotation-fromIdAndStart";
	public static final String DELETE_BY_PARENT_AND_ID = "TsAnnotation-deleteByParentAndRevId";
	public static final String DELETE_BY_PARENT_AND_IDS = "TsAnnotation-deleteByParentAndRevIds";
	public static final String COUNT_BY_PARENT_START_TIME = "TsAnnotation-countByParentAndStartTime";
	public static final String SET_LAYER_NAME = "TsAnnotation-setLayerName";
	public static final String EXISTS_TS_ANNS = "TsAnnotation-existsTsAnns";

	public static String getLayerDefault(final DataSnapshotEntity parent) {
		return parent.getLabel() + " base";
	}

	private Long id;

	private Integer version;
	private DataSnapshotEntity parent;
	private Set<TimeSeriesEntity> annotated = newHashSet();
	private String annotator;

	/* The type of this annotation. */
	private String type;

	/* The optional color of this annotation. */
	private String color;

	/* Free text associated with this annotation. */
	private String description;
	private Long startOffsetUsecs;
	private Long endOffsetUsecs;
	private UserEntity creator;
	private Date createTime = new Date();
	private String layer;
	private Long srcTsAnnId;
	private Set<SnapshotUsage> usages = newHashSet();

	public TsAnnotationEntity() {}

	public TsAnnotationEntity(
			String type,
			Long startOffsetUsecs,
			Long endOffsetUsecs,
			Set<TimeSeriesEntity> annotated,
			String annotator,
			UserEntity creator,
			DataSnapshotEntity parent,
			@Nullable String layer,
			@Nullable String description,
			@Nullable String color) {
		this.type = checkNotNull(type);
		this.startOffsetUsecs = checkNotNull(startOffsetUsecs);
		this.endOffsetUsecs = checkNotNull(endOffsetUsecs);
		this.annotated = checkNotNull(newHashSet(annotated));
		this.annotator = checkNotNull(annotator);
		this.creator = checkNotNull(creator);
		this.parent = checkNotNull(parent);
		this.layer = layer;
		this.description = description;
		this.color = color;
	}

	public TsAnnotationEntity(
			final TsAnnotationEntity annotation,
			final DataSnapshotEntity parent) {
		this(
				annotation.getType(),
				annotation.getStartOffsetUsecs(),
				annotation.getEndOffsetUsecs(),
				annotation.getAnnotated(),
				annotation.getAnnotator(),
				annotation.getCreator(),
				parent,
				annotation.getLayer(),
				annotation.getDescription(),
				annotation.getColor());
	}

	/** The time series being annotated. */
	@ManyToMany
	@JoinTable(
			name = TABLE + "_" + TimeSeriesEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = TimeSeriesEntity.ID_COLUMN))
	@ForeignKey(
			name = "fk_ts_ann_ts_ts_ann",
			inverseName = "fk_ts_ann_ts_ts")
	@Size(min = 1)
	@NotNull
	// The join table has "on delete cascade" specified for the annotation id.
	public Set<TimeSeriesEntity> getAnnotated() {
		return annotated;
	}

	/**
	 * The name of the tool or user who created this annotation.
	 * 
	 * @return the annotator
	 */
	@NotNull
	public String getAnnotator() {
		return annotator;
	}

	@Size(max = 45)
	public String getColor() {
		return color;
	}

	/**
	 * @return the createTime
	 */
	@Column(name = "create_time", nullable = false)
	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * The uid of the user who created this annotation.
	 * 
	 * @return the uid
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id")
	@ForeignKey(name = "fk_ts_ann_2_creator")
	@NotNull
	public UserEntity getCreator() {
		return creator;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	@Column(name = TsAnnotationEntity.END_TIME_COLUMN)
	@NotNull
	@Min(0)
	public Long getEndOffsetUsecs() {
		return endOffsetUsecs;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = TsAnnotationEntity.ID_COLUMN)
	public Long getId() {
		return id;
	}

	@NotNull
	public String getLayer() {
		return layer;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@ForeignKey(name = "fk_ts_ann_2_parent")
	public DataSnapshotEntity getParent() {
		return parent;
	}

	/**
	 * Convenience method equivalent to
	 * {@code getId() == null ? null : String.valueOf(getId())}.
	 * 
	 * @return the revision id of this annotation
	 */
	@Override
	@Transient
	public String getPubId() {
		return id == null ? null : String.valueOf(id);
	}

	/**
	 * For efficient copying from one snapshot into another.
	 * <p>
	 * Intentionally not a foreign key - the original could be deleted.
	 * 
	 * @return the id of the annotation this one was derived from
	 */
	@SuppressWarnings("unused")
	private Long getSrcAnnId() {
		return srcTsAnnId;
	}

	@Column(name = TsAnnotationEntity.START_TIME_COLUMN)
	@NotNull
	@Min(0)
	public Long getStartOffsetUsecs() {
		return startOffsetUsecs;
	}

	/**
	 * @return the type
	 */
	@NotNull
	@Size(min = 1, max = 255)
	public String getType() {
		return type;
	}

	@OneToMany(mappedBy = "annotation")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<SnapshotUsage> getUsages() {
		return usages;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setAnnotated(final Set<TimeSeriesEntity> annotated) {
		this.annotated = annotated;
	}

	public void setAnnotator(String annotator) {
		this.annotator = annotator;
	}

	public void setColor(final String color) {
		this.color = color;
	}

	@SuppressWarnings("unused")
	private void setCreateTime(final Date createTime) {
		this.createTime = createTime;
	}

	public void setCreator(UserEntity creator) {
		this.creator = creator;
	}

	public void setDescription(@Nullable final String description) {
		this.description = description;
	}

	public void setEndOffsetUsecs(Long endOffsetUsecs) {
		this.endOffsetUsecs = endOffsetUsecs;
	}

	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}

	public void setLayer(final String layer) {
		this.layer = layer;
	}

	public void setLayerDefault(final DataSnapshotEntity parent) {
		setLayer(getLayerDefault(parent));
	}

	public void setParent(final DataSnapshotEntity parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setSrcAnnId(final Long srcTsAnnId) {
		this.srcTsAnnId = srcTsAnnId;
	}

	public void setStartOffsetUsecs(Long startOffsetUsecs) {
		this.startOffsetUsecs = startOffsetUsecs;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	@SuppressWarnings("unused")
	private void setUsages(Set<SnapshotUsage> usages) {
		this.usages = usages;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "TsAnnotationEntity [id=" + id + ", version=" + version
				+ ", annotated=" + annotated + ", annotator=" + annotator
				+ ", type=" + type + ", description=" + description
				+ ", startOffsetUsecs=" + startOffsetUsecs
				+ ", endOffsetUsecs="
				+ endOffsetUsecs + ", creator=" + creator + ", createTime="
				+ createTime + ", layer=" + layer + ", srcTsAnnId="
				+ srcTsAnnId + ", color="
				+ color
				+ "]";
	}

	public static final EntityConstructor<TsAnnotationEntity, String, DataSnapshotEntity> CONSTRUCTOR =
			new EntityConstructor<TsAnnotationEntity,String,DataSnapshotEntity>(TsAnnotationEntity.class,
					new IPersistentObjectManager.IPersistentKey<TsAnnotationEntity,String>() {

				@Override
				public String getKey(TsAnnotationEntity o) {
					return o.getPubId();
				}

				@Override
				public void setKey(TsAnnotationEntity o, String newKey) {
					o.setId(Long.valueOf(newKey));
				}

			},
			new EntityPersistence.ICreateObject<TsAnnotationEntity,String,DataSnapshotEntity>() {

				@Override
				public TsAnnotationEntity create(String description, DataSnapshotEntity parent) {
					return new TsAnnotationEntity(null, null, null, new HashSet<TimeSeriesEntity>(), 
							null, null, parent, null, description, null);
				}
			}
					);

		@Override
		public EntityConstructor<TsAnnotationEntity, String, DataSnapshotEntity> tableConstructor() {
			return CONSTRUCTOR;
		}

}
