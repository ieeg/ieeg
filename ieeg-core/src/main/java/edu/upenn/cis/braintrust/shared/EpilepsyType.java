/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Sets.immutableEnumSet;

import java.util.Map;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

/**
 * IMPORTANT!: THE {@code .name} VALUES OF THESE MUST NOT CHANGE - THE DB
 * DEPENDS ON THEM.
 * 
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
public enum EpilepsyType {

	GENERALIZED("Generalized"),
	TEMPORAL_LOBE("Temporal Lobe"),
	TEMPORAL_LOBE_LEFT("Left Temporal Lobe"),
	TEMPORAL_LOBE_RIGHT("Right Temporal Lobe"),
	TEMPORAL_LOBE_BILATERAL("Bilateral Temporal Lobe"),
	PARIETAL("Parietal"),
	PARIETAL_LEFT("Left Parietal"),
	PARIETAL_RIGHT("Right Parietal"),
	PARIETAL_BILATERAL("Bilateral Parietal"),
	OCCIPITAL("Occipital"),
	OCCIPITAL_LEFT("Left Occipital"),
	OCCIPITAL_RIGHT("Right Occipital"),
	OCCIPITAL_BILATERAL("Bilateral Occipital"),
	INSULA("Insula"),
	INSULA_LEFT("Left Insula"),
	INSULA_RIGHT("Right Insula"),
	INSULA_BILATERAL("Bilateral Insula"),
	FRONTAL_LOBE("Frontal Lobe"),
	FRONTAL_LOBE_LEFT("Left Frontal Lobe"),
	FRONTAL_LOBE_RIGHT("Right Frontal Lobe"),
	FRONTAL_LOBE_BILATERAL("Bilateral Frontal Lobe"),
	UNKNOWN("Unknown");

	public static final Set<EpilepsyType> FOCALS =
			immutableEnumSet(
					TEMPORAL_LOBE,
					TEMPORAL_LOBE_LEFT,
					TEMPORAL_LOBE_RIGHT,
					TEMPORAL_LOBE_BILATERAL,
					PARIETAL,
					PARIETAL_LEFT,
					PARIETAL_RIGHT,
					PARIETAL_BILATERAL,
					OCCIPITAL,
					OCCIPITAL_LEFT,
					OCCIPITAL_RIGHT,
					OCCIPITAL_BILATERAL,
					INSULA,
					INSULA_LEFT,
					INSULA_RIGHT,
					INSULA_BILATERAL,
					FRONTAL_LOBE,
					FRONTAL_LOBE_LEFT,
					FRONTAL_LOBE_RIGHT,
					FRONTAL_LOBE_BILATERAL);

	public static final Set<EpilepsyType> LEFTS =
			immutableEnumSet(
					TEMPORAL_LOBE_LEFT,
					PARIETAL_LEFT,
					OCCIPITAL_LEFT,
					INSULA_LEFT,
					FRONTAL_LOBE_LEFT);

	public static final Set<EpilepsyType> RIGHTS =
			immutableEnumSet(
					TEMPORAL_LOBE_RIGHT,
					PARIETAL_RIGHT,
					OCCIPITAL_RIGHT,
					INSULA_RIGHT,
					FRONTAL_LOBE_RIGHT);

	public static final Set<EpilepsyType> BILATERALS =
			immutableEnumSet(
					TEMPORAL_LOBE_BILATERAL,
					PARIETAL_BILATERAL,
					OCCIPITAL_BILATERAL,
					INSULA_BILATERAL,
					FRONTAL_LOBE_BILATERAL);

	public static final Set<EpilepsyType> TEMPORAL_LOBES =
			immutableEnumSet(
					TEMPORAL_LOBE,
					TEMPORAL_LOBE_LEFT,
					TEMPORAL_LOBE_RIGHT,
					TEMPORAL_LOBE_BILATERAL);

	public static final Set<EpilepsyType> PARIETAL_LOBES =
			immutableEnumSet(
					PARIETAL,
					PARIETAL_LEFT,
					PARIETAL_RIGHT,
					PARIETAL_BILATERAL);

	public static final Set<EpilepsyType> OCCIPITAL_LOBES =
			immutableEnumSet(
					OCCIPITAL,
					OCCIPITAL_LEFT,
					OCCIPITAL_RIGHT,
					OCCIPITAL_BILATERAL);

	public static final Set<EpilepsyType> INSULA_LOBES =
			immutableEnumSet(
					INSULA,
					INSULA_LEFT,
					INSULA_RIGHT,
					INSULA_BILATERAL);

	public static final Set<EpilepsyType> FRONTAL_LOBES =
			immutableEnumSet(
					FRONTAL_LOBE,
					FRONTAL_LOBE_LEFT,
					FRONTAL_LOBE_RIGHT,
					FRONTAL_LOBE_BILATERAL);

	private String displayString;

	public static final Map<String, EpilepsyType> FROM_STRING;

	static {
		Builder<String, EpilepsyType> temp = ImmutableMap.builder();
		for (final EpilepsyType epilepsy : values()) {
			temp.put(epilepsy.toString(), epilepsy);
		}
		FROM_STRING = temp.build();
	}

	private EpilepsyType(String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
