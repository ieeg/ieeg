/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class TraceForImage {
	private String channel;
	private int pixelCoordX;
	private int pixelCoordY;

	public TraceForImage() {}
	
	/**
	 * @return the channel
	 */
	@XmlElement
	public String getChannel() {
		return channel;
	}

	/**
	 * @return the pixelCoordX
	 */
	@XmlElement
	public int getPixelCoordX() {
		return pixelCoordX;
	}

	/**
	 * @return the pixelCoordY
	 */
	@XmlElement
	public int getPixelCoordY() {
		return pixelCoordY;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(final String channel) {
		this.channel = channel;
	}

	/**
	 * @param pixelCoordX the pixelCoordX to set
	 */
	public void setPixelCoordX(final int pixelCoordX) {
		this.pixelCoordX = pixelCoordX;
	}

	/**
	 * @param pixelCoordY the pixelCoordY to set
	 */
	public void setPixelCoordY(final int pixelCoordY) {
		this.pixelCoordY = pixelCoordY;
	}
}
