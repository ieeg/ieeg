/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.cli;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.IHasLabel;

public class ReoffsetStudies20000101 {

	final static long NEW_DATE = 946684800000000l;

	public static void main(String[] args) throws IOException {
		System.out.println("starting "
				+ ReoffsetStudies20000101.class.getSimpleName() + "...");
		checkArgument(args.length == 1);

		Properties props = null;
		FileInputStream hibernateProps = new FileInputStream(args[0]);
		props = new Properties();
		props.load(hibernateProps);
		hibernateProps.close();

		if (props.containsKey("hibernate.hbm2ddl.auto")) {
			System.err
					.println("you have an hibernate.hbm2ddl.auto!!! stopping...");
			System.exit(1);
		}

		System.out.println("properties: " + props);

		HibernateUtil.putConfigProps(props);
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();

			IPatientDAO pDAO = new PatientDAOHibernate(sess);
			for (Patient p : pDAO.findAll()) {
				System.out.println("patient " + p.getLabel());
				if (p.getAdmissions().size() == 0) {

				} else if (p.getAdmissions().size() == 1
						|| p.getLabel().equals("036")) {
					HospitalAdmission a = null;
					if (p.getLabel().equals("036")) {
						for (HospitalAdmission aHere : p.getAdmissions()) {
							if (aHere.getStudies().size() == 0) {

							} else if (aHere.getStudies().size() == 1) {
								a = aHere;
							} else {
								throw new IllegalStateException("wrong!");
							}
						}
					} else {
						a = getOnlyElement(p.getAdmissions());
					}
					if (a.getStudies().size() > 0) {
						EegStudy s = getOnlyElement(a.getStudies());

						System.out.println("doing study " + s.getLabel());
						if (s.getStartTimeUutc() < NEW_DATE) {
							throw new IllegalStateException(
									"we don't handle studies from before 2000/1/1");
						}
						long shift = s.getStartTimeUutc() - NEW_DATE;
						s.getRecording().setStartTimeUutc(NEW_DATE);
						s.getRecording().setEndTimeUutc(s.getEndTimeUutc() - shift);
					}
				} else if (p.getAdmissions().size() == 2) {
					Set<EegStudy> studies = newHashSet();
					for (HospitalAdmission a : p.getAdmissions()) {
						if (a.getStudies().size() > 0) {
							studies.add(getOnlyElement(a.getStudies()));
						}
					}
					if (studies.size() == 0) {
						continue;
					}
					long shift = -1;
					{
						EegStudy s012_1 = find(
								studies,
								compose(equalTo("Study 012-1"),
										IHasLabel.getLabel));
						System.out.println("doing study "
								+ s012_1.getLabel());
						shift = s012_1.getStartTimeUutc() - NEW_DATE;
						s012_1.getRecording().setStartTimeUutc(NEW_DATE);
						s012_1.getRecording().setEndTimeUutc(s012_1.getEndTimeUutc()
								- shift);
					}
					{
						EegStudy s012_2 = find(
								studies,
								compose(equalTo("Study 012-2"),
										IHasLabel.getLabel));
						System.out.println("doing study "
								+ s012_2.getLabel());
						s012_2.getRecording().setStartTimeUutc(s012_2
								.getStartTimeUutc()
								- shift);
						s012_2.getRecording().setEndTimeUutc(s012_2.getEndTimeUutc()
								- shift);
					}

				} else {
					throw new IllegalStateException(
							"can't handle more than 2 admissions");
				}
			}
			trx.commit();
		} catch (Throwable t) {
			PersistenceUtil.rollback(trx);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(sess);
		}
	}
}
