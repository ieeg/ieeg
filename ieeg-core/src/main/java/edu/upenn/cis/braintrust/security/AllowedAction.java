/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Objects;

/**
 * An action.
 * 
 * @author John Frommeyer
 */
@Immutable
@GwtCompatible(serializable = true)
public final class AllowedAction implements Serializable {

	private static final long serialVersionUID = 1L;
	private String value;

	// For GWT
	@SuppressWarnings("unused")
	private AllowedAction() {}

	public AllowedAction(String value) {
		this.value = checkNotNull(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AllowedAction)) {
			return false;
		}
		AllowedAction other = (AllowedAction) obj;
		if (!Objects.equal(value, other.value)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the permission
	 */
	public String get() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "AllowedAction [value=" + value + "]";
	}

}
