/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.testhelper;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;

public class OrganizationsForTests {
	public final static OrganizationEntity SPIKE;
	public final static OrganizationEntity JONES;
	public final static OrganizationEntity ANOTHER;

	static {
		SessionFactory sessFact = HibernateUtil.getSessionFactory();
		Session session = null;
		Transaction trx = null;
		OrganizationEntity spike = null, jones = null, another = null;
		try {
			session = sessFact.openSession();
			trx = session.beginTransaction();
			spike = (OrganizationEntity) session.get(OrganizationEntity.class,
					1L);
			if (spike == null) {
				throw new AssertionError();
			}
			jones = (OrganizationEntity) session.get(OrganizationEntity.class,
					2L);
			if (jones == null) {
				throw new AssertionError();
			}
			another = (OrganizationEntity) session
					.get(OrganizationEntity.class, 3L);
			if (another == null) {
				throw new AssertionError();
			}
			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw new ExceptionInInitializerError(e);
		} finally {
			PersistenceUtil.close(session);
		}
		SPIKE = spike;
		JONES = jones;
		ANOTHER = another;
	}
}
