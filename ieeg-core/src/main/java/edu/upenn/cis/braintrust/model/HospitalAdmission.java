/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * Intended to represent the state of a patient's case during a single
 * admission. {@link Patient} contains general information which is constant
 * across admissions.
 * 
 * @author Sam Donnelly
 */
@Entity
@Table(name = HospitalAdmission.TABLE)
public class HospitalAdmission implements IHasLongId {

	public static final String TABLE = "hospital_admission";

	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private Integer version;
	private Patient parent;
	private Integer ageAtAdmission;
	private Set<EegStudy> studies = newHashSet();

	public HospitalAdmission() {}

	public HospitalAdmission(@Nullable Integer ageAtAdmission) {
		this.ageAtAdmission = ageAtAdmission;
	}

	/**
	 * For convenience, handle both sides of the {@code Patient<->Study}
	 * relationship.
	 * 
	 * @param study to be added
	 */
	public void addStudy(final EegStudy study) {
		checkNotNull(study);
//		checkArgument(study.getParent() == null,
//				"study already has a parent");
		checkArgument(!studies.contains(study),
				"study is already contained in this hospital admission");
		
		if (study.getParent() != this)
			study.setParent(this);
		studies.add(study);
	}

	/**
	 * @return the ageAtAdmission
	 */
	public Integer getAgeAtAdmission() {
		return ageAtAdmission;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.EAGER)//LAZY)
	@JoinColumn(name = Patient.ID_COLUMN)
	@NotNull
	public Patient getParent() {
		return parent;
	}

	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<EegStudy> getStudies() {
		return studies;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void removeStudy(final EegStudy study) {
		checkNotNull(study);
		checkArgument(studies.contains(study),
				"this patient does not contain study");
		studies.remove(study);
		study.setParent(null);
	}

	/**
	 * @param ageAtAdmission the ageAtAdmission to set
	 */
	public void setAgeAtAdmission(final Integer ageAtAdmission) {
		this.ageAtAdmission = ageAtAdmission;
	}

	@VisibleForTesting
	public void setId(final Long id) {
		this.id = id;
	}

	public void setParent(@Nullable Patient parent) {
		this.parent = parent;
	}

	/**
	 * @param studies the studies to set
	 */
	@SuppressWarnings("unused")
	private void setStudies(final Set<EegStudy> studies) {
		this.studies = studies;
	}

	@VisibleForTesting
	public void setVersion(final Integer version) {
		this.version = version;
	}

}
