/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import edu.upenn.cis.braintrust.shared.ToolDto;


public class ToolNotFoundForDtoException extends ToolNotFoundException {

	private static final long serialVersionUID = 1L;

	private ToolDto toolDto;

	public ToolNotFoundForDtoException(final String msg, final ToolDto toolDto) {
		super(msg, toolDto.getId().orNull());
		this.toolDto = toolDto;
	}

	public ToolDto getToolDto() {
		return toolDto;
	}

}
