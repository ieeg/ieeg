/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao;

import java.util.Iterator;

import org.hibernate.Criteria;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.loader.criteria.CriteriaQueryTranslator;

import edu.umd.cs.findbugs.annotations.Nullable;
import edu.upenn.cis.braintrust.shared.ContactGroupSearch;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.PatientSearch;
import edu.upenn.cis.braintrust.shared.TraceSearch;

/**
 * @author Sam Donnelly
 */
class Search2Criteria {

	// public static final String ELECTRODE_TRACE_0_ALIAS = "et0";

	// public static void admissionSearch2Criteria(
	// final HospitalAdmissionSearch admissionSearch,
	// final Criteria admissionCriteria) {
	// if (admissionSearch.getPatientSearch() != null) {
	// final Criteria patientCriteria = admissionCriteria
	// .createCriteria("parent");
	// patientSearch2Criteria(admissionSearch.getPatientSearch(),
	// patientCriteria);
	// }
	// }

	public static void eegStudySearch2Criteria(
			final EegStudySearch studySearch, final Criteria studyCriteria) {

		// studyCriteria
		// .setFetchMode("electrodes", FetchMode.JOIN)
		// .setFetchMode("electrodes.contacts", FetchMode.JOIN)
		// .setFetchMode("electrodes.contacts.trace", FetchMode.JOIN);

		// if (studySearch.getAdmissionSearch() != null) {
		// final Criteria admissionCriteria = studyCriteria
		// .createCriteria("parent");
		// admissionSearch2Criteria(studySearch.getAdmissionSearch(),
		// admissionCriteria);
		// }

		// if (studySearch.getSzCountMin() != null) {
		// studyCriteria.add(Restrictions.ge("szCount", studySearch
		// .getSzCountMin()));
		// }
		// if (studySearch.getSzCountMax() != null) {
		// studyCriteria.add(Restrictions.le("szCount", studySearch
		// .getSzCountMax()));
		// }
		if (studySearch.getContactGroupSearch() != null) {
			final Criteria electrodeCriteria = studyCriteria
					.createCriteria("recording.contactGroups");
			contactGroupSearch2Criteria(
					studySearch.getContactGroupSearch(),
					electrodeCriteria,
					studyCriteria);
		}

		// if (studySearch.getImageTypes().size() > 0) {
		//
		// final Junction imgTypeConjunction = Restrictions.conjunction();
		// studyCriteria.add(imgTypeConjunction);
		//
		// for (final ImageType imgType : studySearch.getImageTypes()) {
		// imgTypeConjunction.add(Restrictions.sqlRestriction(
		// "{alias}.eeg_study_id "
		// + "in (select s.eeg_study_id "
		// + "from eeg_study s, "
		// + "eeg_study_image si, "
		// + "image i "
		// + "where s.eeg_study_id = si.eeg_study_id "
		// + "and si.image_id = i.image_id "
		// + "and i.type = ?)",
		// imgType.ordinal(), StandardBasicTypes.INTEGER));
		// }
		// }
	}

	public static void contactGroupSearch2Criteria(
			final ContactGroupSearch contactGroupSearch,
			final Criteria contactGroupCriteria,
			final Criteria studyCriteria) {

		// if (electrodeSearch.getSamplingRateMin() != null) {
		// electrodeCriteria.add(Restrictions.ge("samplingRate",
		// electrodeSearch.getSamplingRateMin()));
		// }
		// if (electrodeSearch.getSamplingRateMax() != null) {
		// electrodeCriteria.add(Restrictions.le("samplingRate",
		// electrodeSearch.getSamplingRateMax()));
		// }
		// if (electrodeSearch.getType() != null) {
		// final Criteria electrodeInfoCriteria = electrodeCriteria
		// .createCriteria("info");
		// electrodeInfoCriteria
		// .add(Restrictions.eq("electrodeType",
		// electrodeSearch.getType()));
		// }

		// if (electrodeSearch.getSides().size() > 0) {
		// electrodeCriteria.add(Restrictions.in("side", electrodeSearch
		// .getSides()));
		// }
		// if (electrodeSearch.getLobes().size() > 0) {
		// electrodeCriteria.add(Restrictions.in("lobe", electrodeSearch
		// .getLobes()));
		// }

		// Criteria contactCriteria = null;
		// if (electrodeSearch.getTraceSearch() != null) {
		// contactCriteria = electrodeCriteria
		// .createCriteria("contacts");
		// final Criteria studyTraceCriteria = contactCriteria.createCriteria(
		// "trace");
		// traceSearch2Criteria(
		// studyTraceCriteria,
		// electrodeSearch.getTraceSearch(),
		// studyCriteria);
		// }

		// if (electrodeSearch.getContactType() != null) {
		// if (contactCriteria == null) {
		// contactCriteria = electrodeCriteria
		// .createCriteria("contacts");
		// }
		// final Criteria contactInfoCriteria = contactCriteria
		// .createCriteria("info");
		// contactInfoCriteria.add(Restrictions.eq("contactType",
		// electrodeSearch.getContactType()));
		// }
	}

	public static void patientSearch2Criteria(
			final PatientSearch patientSearch, final Criteria patientCriteria) {

		// if (patientSearch.getSzTypes().size() > 0) {
		// final Junction szTypeConjunction = Restrictions.conjunction();
		// patientCriteria.add(szTypeConjunction);
		// for (final SeizureType szType : patientSearch.getSzTypes()) {
		// szTypeConjunction.add(Restrictions.sqlRestriction(
		// "{alias}.patient_id "
		// + "in (select p.patient_id "
		// + "from patient p, "
		// + "patient_seizure_type pst "
		// + "where p.patient_id = pst.patient_id "
		// + "and pst.seizure_type = ?)",
		// szType.ordinal(), StandardBasicTypes.INTEGER));
		// }
		// }

		// if (patientSearch.getGeneralizedSzTypes() != null) {
		// final Junction genSzTypeDisjunction = Restrictions.disjunction();
		// patientCriteria.add(genSzTypeDisjunction);
		// for (final SeizureType szType : SeizureType.GENERALIZED_SZ_TYPES) {
		// genSzTypeDisjunction.add(Restrictions.sqlRestriction(
		// "{alias}.patient_id "
		// + "in (select p.patient_id "
		// + "from patient p, "
		// + "patient_seizure_type pst "
		// + "where p.patient_id = pst.patient_id "
		// + "and pst.seizure_type = ?)",
		// szType.ordinal(), StandardBasicTypes.INTEGER));
		// }
		// }

		// if (patientSearch.getPartialSzTypes() != null) {
		// final Junction partialSzTypeDisjunction = Restrictions
		// .disjunction();
		// patientCriteria.add(partialSzTypeDisjunction);
		// for (final SeizureType szType : SeizureType.PARTIAL_SZ_TYPES) {
		// partialSzTypeDisjunction.add(Restrictions.sqlRestriction(
		// "{alias}.patient_id "
		// + "in (select p.patient_id "
		// + "from patient p, "
		// + "patient_seizure_type pst "
		// + "where p.patient_id = pst.patient_id "
		// + "and pst.seizure_type = ?)",
		// szType.ordinal(), StandardBasicTypes.INTEGER));
		// }
		// }

		// if (patientSearch.getAgeOfOnsetMin() != null) {
		// patientCriteria.add(Restrictions.ge("ageOfOnset", patientSearch
		// .getAgeOfOnsetMin()));
		// }
		//
		// if (patientSearch.getAgeOfOnsetMax() != null) {
		// patientCriteria.add(Restrictions.le("ageOfOnset", patientSearch
		// .getAgeOfOnsetMax()));
		// }
		//
		// if (patientSearch.getGenders().size() > 0) {
		// patientCriteria.add(Restrictions.in("gender", patientSearch
		// .getGenders()));
		// }
	}

	public static void traceSearch2Criteria(
			final Criteria studyTraceCriteria,
			final TraceSearch traceSearch,
			final Criteria studyCriteria) {
		// if (traceSearch.isSomethingNotNull()) {
		// final Criteria sa0 = studyCriteria
		// .createCriteria("tsAnnotations", "sa0");
		// final Criteria at0 = sa0.createCriteria("annotated", "at0");
		// sa0.add(Restrictions.eqProperty(
		// at0.getAlias() + ".id",
		// studyTraceCriteria.getAlias() + ".id"));
		//
		// if (traceSearch.getAnnotationType() != null) {
		// sa0.add(Restrictions.eq(
		// "type",
		// traceSearch.getAnnotationType()));
		// }
		//
		// if (traceSearch.getMinAnnotationCount() != null) {
		// DetachedCriteria countSubquery = DetachedCriteria
		// .forClass(TsAnnotationEntity.class);
		// countSubquery.setProjection(Projections.rowCount());
		// if (traceSearch.getAnnotationType() != null) {
		// countSubquery
		// .add(
		// Restrictions.eq("type",
		// traceSearch.getAnnotationType()));
		// }
		// countSubquery.add(Restrictions.eqProperty("annotated.id",
		// ELECTRODE_TRACE_0_ALIAS + ".id"));
		// studyCriteria
		// .add(
		// Subqueries.le(
		// Long.valueOf(traceSearch
		// .getMinAnnotationCount()),
		// countSubquery));
		// }
		//
		// if (traceSearch.getMaxAnnotationCount() != null) {
		// DetachedCriteria countSubquery = DetachedCriteria
		// .forClass(TsAnnotationEntity.class);
		// countSubquery.setProjection(Projections.rowCount());
		// if (traceSearch.getAnnotationType() != null) {
		// countSubquery
		// .add(
		// Restrictions.eq("type",
		// traceSearch.getAnnotationType()));
		// }
		// countSubquery.add(Restrictions.eqProperty("annotated.id",
		// ELECTRODE_TRACE_0_ALIAS + ".id"));
		// studyCriteria
		// .add(
		// Subqueries.ge(
		// Long.valueOf(traceSearch
		// .getMaxAnnotationCount()),
		// countSubquery));
		// }
		//
		// if (traceSearch.getMinInterannMicrosecs() != null
		// || traceSearch.getMaxInterannMicrosecs() != null) {
		// DetachedCriteria subSa1 = DetachedCriteria
		// .forClass(TsAnnotationEntity.class, "sa1");
		// studyCriteria
		// .add(Subqueries.exists(subSa1.setProjection(Projections
		// .id())));
		// subSa1.add(Restrictions.eqProperty(
		// "annotated.id",
		// ELECTRODE_TRACE_0_ALIAS + ".id"));
		// subSa1.add(Restrictions.neProperty("id", "sa0.id"));
		//
		// if (traceSearch.getAnnotationType() != null) {
		// subSa1.add(Restrictions.eq("type",
		// traceSearch.getAnnotationType()));
		// }
		// String sa0SqlAlias = getSqlAlias(studyCriteria,
		// sa0.getAlias());
		// if (traceSearch.getMinInterannMicrosecs() != null) {
		// subSa1.add(Restrictions
		// .sqlRestriction(
		// "(({alias}.start_time - " + sa0SqlAlias
		// + ".end_time) >=  "
		// + traceSearch.getMinInterannMicrosecs()
		// + ")"));
		// } else {
		// subSa1.add(Restrictions
		// .geProperty("startTimeUutc",
		// sa0.getAlias() + ".endTimeUutc"));
		// }
		// if (traceSearch.getMaxInterannMicrosecs() != null) {
		// subSa1.add(Restrictions
		// .sqlRestriction(
		// "(({alias}.start_time - " + sa0SqlAlias
		// + ".end_time) <=  "
		// + traceSearch.getMaxInterannMicrosecs()
		// + ")"));
		// }
		//
		// DetachedCriteria subSa2 = DetachedCriteria
		// .forClass(TsAnnotationEntity.class, "sa2");
		// subSa1
		// .add(Subqueries.notExists(subSa2
		// .setProjection(Projections.id())));
		// subSa2.add(Restrictions.eqProperty(
		// "annotated.id",
		// ELECTRODE_TRACE_0_ALIAS + ".id"));
		// subSa2.add(Restrictions.gtProperty(
		// "startTimeUutc",
		// sa0.getAlias() + ".endTimeUutc"));
		// subSa2.add(Restrictions.ltProperty(
		// "startTimeUutc",
		// subSa1.getAlias() + ".startTimeUutc"));
		// }
		// }
	}

	// public static void surgicalInterventionSearch2Criteria(
	// final SurgicalInterventionSearch surgicalInterventionSearch,
	// final Criteria surgicalInterventionCriteria) {
	// checkNotNull(surgicalInterventionSearch);
	// checkNotNull(surgicalInterventionCriteria);
	//
	// if (surgicalInterventionSearch.getLobes().size() > 0) {
	// surgicalInterventionCriteria.add(Restrictions.in("lobe",
	// surgicalInterventionSearch.getLobes()));
	// }
	//
	// if (surgicalInterventionSearch.getPostopRatingScales().size() > 0) {
	// surgicalInterventionCriteria.add(Restrictions.in(
	// "postopRatingScale", surgicalInterventionSearch
	// .getPostopRatingScales()));
	// }
	//
	// if (surgicalInterventionSearch.getSides().size() > 0) {
	// surgicalInterventionCriteria.add(Restrictions.in("side",
	// surgicalInterventionSearch.getSides()));
	// }
	//
	// if (surgicalInterventionSearch.getTypes().size() > 0) {
	// surgicalInterventionCriteria.add(Restrictions.in("type",
	// surgicalInterventionSearch.getTypes()));
	// }
	// }

	// public static void treatmentSearch2Criteria(
	// final TreatmentSearch treatmentSearch,
	// final Criteria treatmentCriteria) {
	// checkNotNull(treatmentSearch);
	// checkNotNull(treatmentCriteria);
	//
	// if (treatmentSearch.getSurgicalInterventionSearch() != null) {
	// final Criteria surgicalInterventionCriteria = treatmentCriteria
	// .createCriteria("surgicalInterventions");
	// surgicalInterventionSearch2Criteria(treatmentSearch
	// .getSurgicalInterventionSearch(),
	// surgicalInterventionCriteria);
	// }
	// }

	private Search2Criteria() {
		throw new AssertionError("can't instantiate a Search2Criteria");
	}

	public static String getSqlAlias(Criteria criteria, String alias) {
		CriteriaImpl criteriaImpl = (CriteriaImpl) criteria;
		SessionImplementor session = ((CriteriaImpl) criteria).getSession();
		SessionFactoryImplementor factory = session.getFactory();
		String[] implementors = factory.getImplementors(criteriaImpl
				.getEntityOrClassName());

		CriteriaQueryTranslator translator = new CriteriaQueryTranslator(
				factory,
				(CriteriaImpl) criteria, implementors[0],
				CriteriaQueryTranslator.ROOT_SQL_ALIAS);

		Criteria aliasCriteria = findCriteriaForAlias(criteria, alias);

		return translator.getSQLAlias(aliasCriteria);
	}

	@Nullable
	public static Criteria findCriteriaForAlias(Criteria criteria, String alias) {
		Iterator<?> subcriterias = ((CriteriaImpl) criteria)
				.iterateSubcriteria();
		while (subcriterias.hasNext()) {
			Criteria subcriteria = (Criteria) subcriterias.next();
			if (subcriteria.getAlias() != null
					&& subcriteria.getAlias().equals(alias)) {
				return subcriteria;
			}
		}
		return null;
	}

}
