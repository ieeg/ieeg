/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;
import com.google.common.collect.Sets;

import edu.upenn.cis.db.mefview.shared.ISecurityCredentials;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * For type safety, an id for a {@link User}.
 * 
 * @author Sam Donnelly
 */
@Immutable
@Embeddable
@GwtCompatible(serializable = true)
public final class UserId implements Serializable, JsonTyped, ISecurityCredentials {

	private static final long serialVersionUID = 1L;
	
	public static final Function<UserId, Long> getValue = new Function<UserId, Long>() {

		@Override
		public Long apply(UserId input) {
			return input.getValue();
		}
	};

	private Long value;

	/** For GWT */
	@SuppressWarnings("unused")
	private UserId() {}

	public UserId(int value) {
		this(Long.valueOf(value));
	}

	public UserId(Long value) {
		checkArgument(value >= -1, "user id must be >= -1");
		this.value = checkNotNull(value);
	}

	@Override
	public boolean equals(@Nullable final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof UserId)) {
			return false;
		}
		final UserId other = (UserId) obj;
		return value.equals(other.value);
	}

	/**
	 * Get the value of the user id
	 * 
	 * @return the value of the user id
	 */
	@Column(name = "user_id")
	@NotNull
	public Long getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	private void setValue(Long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "UserId [value=" + value + "]";
	}

	@Override
	@JsonIgnore
	@Transient
	public Long getIdentifier() {
		return getValue();
	}

	@JsonIgnore
	@Transient
	@Override
	public String getClassification() {
		return "user";
	}

	@JsonIgnore
	@Transient
	@Override
	public String getName() {
		return "";
	}
	
	public void setMembers(Set<String> misc) {}
}
