/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.newLinkedHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static com.google.common.collect.Sets.newTreeSet;
import static edu.upenn.cis.braintrust.BtUtil.diffNowMsThenSeconds;
import static edu.upenn.cis.braintrust.BtUtil.diffNowThenSeconds;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableFutureTask;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IControlFileDAO;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.provenance.IDataUsageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.datasnapshot.assembler.DsSearchResultAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TsAnnotationAssembler;
import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.imodel.IHasRecording;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.model.EegMontageEntity;
import edu.upenn.cis.braintrust.model.EegMontagePairEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.RecordingObjectEntity;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermissions;
import edu.upenn.cis.braintrust.security.IExtAuthzHandler;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.AnnotationCube;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.DatasetIdAndVersion;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.StaleDataException;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetConflictException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInAnyRecordingException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.RecordingObject;

/**
 * A service to provide access to data snapshots and their contents.
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public final class DataSnapshotServer implements IDataSnapshotServer {

	/**
	 * Simple wrapper for action within a transaction.
	 * 
	 * @author zives
	 * 
	 * @param <T>
	 */
	public interface TransAction<T> {
		public abstract T execute(Session session) throws
				AuthorizationException;
	}

	/**
	 * Wraps a TransAction within a Hibernate transaction. On error, rolls back
	 * results. Else returns them.
	 * 
	 * @author zives
	 * 
	 * @param <T> Type of return value.
	 */
	public class TransactionWrap<T> {
		/**
		 * Wrapper to execute an operation within a transaction, with no return
		 * 
		 * @param name
		 * @param action
		 */
		public T execTransaction(String name, TransAction<T> action) {
			long inNanos = System.nanoTime();
			String m = name + "(...)";
			SessionAndTrx sessAndTrx = null;
			try {
				sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);

				T ret = action.execute(sessAndTrx.session);

				PersistenceUtil.commit(sessAndTrx);

				return ret;
			} catch (RuntimeException t) {
				PersistenceUtil.rollback(sessAndTrx);
				throw t;
			} finally {
				PersistenceUtil.close(sessAndTrx);
				logger.info(
						"{} : {} seconds",
						m,
						BtUtil.diffNowThenSeconds(inNanos));
			}

		}
	}

	TransactionWrap<Void> voidTransaction = new TransactionWrap<Void>();
	TransactionWrap<String> stringTransaction = new TransactionWrap<String>();
	TransactionWrap<Long> longTransaction = new TransactionWrap<Long>();

	TransactionWrap<Set<IJsonKeyValue>> keyValueTransaction = new TransactionWrap<Set<IJsonKeyValue>>();

	private Cache<String, DataSnapshotEntity> dataSnapshotShortCache =
			CacheBuilder
					.newBuilder()
					.maximumSize(100)
					.expireAfterWrite(5, TimeUnit.MINUTES)
					.build();

	private final Executor searchResultCachePool =
			Executors.newSingleThreadExecutor();

	private Cache<Long, SearchResultCacheEntry> searchResultCache =
			CacheBuilder
					.newBuilder()
					.refreshAfterWrite(5, TimeUnit.MINUTES)
					.maximumSize(2000)
					.build(
							new CacheLoader<Long, SearchResultCacheEntry>() {
								@Override
								public SearchResultCacheEntry load(Long key) {
									throw new AssertionError("invalid call");
								}

								@Override
								public ListenableFuture<SearchResultCacheEntry> reload(
										final Long key,
										SearchResultCacheEntry prevGraph) {
									ListenableFutureTask<SearchResultCacheEntry> task = ListenableFutureTask
											.create(new Callable<SearchResultCacheEntry>() {
												@Override
												public SearchResultCacheEntry call() {
													SessionAndTrx sessAndTrx = null;
													try {
														sessAndTrx = PersistenceUtil
																.getSessAndTrx(sessFac);
														ITimeSeriesDAO timeSeriesDAO
														= daoFac.getTimeSeriesDAO(sessAndTrx.session);
														IImageDAO imageDAO
														= daoFac.getImageDAO();
														IDataSnapshotDAO dataSnapshotDAO
														= daoFac.getDataSnapshotDAO(sessAndTrx.session);
														IEegStudyDAO studyDAO
														= daoFac.getEegStudyDAO();
														IExperimentDAO experimentDAO
														= daoFac.getExperimentDAO();

														DataSnapshotEntity ds
														= dataSnapshotDAO
																.load(key);

														if (ds == null) {
															throw new DataSnapshotNotFoundException(
																	key.toString());
														}
														IHasRecording rec = null;
														if (HibernateUtil
																.isInstanceOf(
																		ds,
																		ExperimentEntity.class)) {
															ExperimentEntity experiment
															= experimentDAO.load(ds
																	.getId());
															rec = experiment;
														} else if (
														HibernateUtil
																.isInstanceOf(
																		ds,
																		EegStudy.class)) {
															EegStudy study
															= studyDAO.load(ds
																	.getId());
															rec = study;
														}

														if (rec == null) {
															throw new AssertionError();
														}

														int tsCount = timeSeriesDAO
																.countByRecording(rec
																		.getRecording());

														int imgCount = Ints
																.checkedCast(imageDAO
																		.countImagesRecording(rec
																				.getRecording()));

														int tsAnnCount
														= Ints.checkedCast(dataSnapshotDAO
																.countTsAnns(ds));

														SearchResultCacheEntry searchResultCacheEntry =
																new SearchResultCacheEntry(
																		imgCount,
																		tsCount,
																		tsAnnCount,
																		rec.getOrganization()
																				.getName());

														PersistenceUtil
																.commit(sessAndTrx);
														return searchResultCacheEntry;
													} catch (RuntimeException re) {
														PersistenceUtil
																.rollback(sessAndTrx);
														throw re;
													} finally {
														PersistenceUtil
																.close(sessAndTrx);
													}
												}
											});
									searchResultCachePool.execute(task);
									return task;
								}
							});

	private final SessionFactory sessFac;
	private final Configuration sessFacConfig;

	private final IDataSnapshotServiceFactory dsServiceFac;
	private final IDAOFactory daoFac;
	private final IUserService userService =
			UserServiceFactory.getUserService();

	private final IRecordingObjectProvider recObjProvider = RecordingObjectProviderFactory
			.getRecordingObjectProvider();

	private final int MAX_DS_TS_ANNS;

	private final Logger logger =
			LoggerFactory.getLogger(getClass());
	private final Logger timeLogger =
			LoggerFactory.getLogger("time." + getClass());

	@VisibleForTesting
	DataSnapshotServer() {
		this(IvProps.getMaxTsAnnotations());
	}

	@VisibleForTesting
	DataSnapshotServer(int maxTsAnns) {
		this(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration(),
				new DataSnapshotServiceFactory(),
				maxTsAnns,
				new HibernateDAOFactory());
	}

	/**
	 * Inject the factories for testing. One use case is to use mocking to get a
	 * session that throws an exception. So that we can verify that the session
	 * is closed if an exception is thrown.
	 * 
	 * @param sessFac {@code SessionFactory} this service should use for
	 *            creating {@code Session}s.
	 * @param dsServiceFac
	 */
	@VisibleForTesting
	DataSnapshotServer(
			SessionFactory sessionFac,
			Configuration sessFacConfig,
			IDataSnapshotServiceFactory dsServiceFac,
			IDAOFactory daoFac) {
		this(
				sessionFac,
				sessFacConfig,
				dsServiceFac,
				IDataSnapshotServiceFactory.DEFAULT_MAX_TS_ANNS,
				daoFac);
	}

	/**
	 * Inject the factories for testing. One use case is to use mocking to get a
	 * session that throws an exception. So that we can verify that the session
	 * is closed if an exception is thrown.
	 * 
	 * @param sessFac {@code SessionFactory} this service should use for
	 *            creating {@code Session}s.
	 * @param dsServiceFac
	 * @param maxTsAnns the maximum number of annotations which can exist on a
	 *            snapshot
	 */
	@VisibleForTesting
	public DataSnapshotServer(
			SessionFactory sessionFac,
			Configuration sessFacConfig,
			IDataSnapshotServiceFactory dsServiceFac,
			int maxTsAnns,
			IDAOFactory daoFac) {
		this.sessFac = sessionFac;
		this.sessFacConfig = sessFacConfig;
		this.dsServiceFac = dsServiceFac;
		MAX_DS_TS_ANNS = maxTsAnns;
		this.daoFac = daoFac;
	}

	/**************************************************************************/

	// @Override
	public Long addLogEntry(User user, LogMessage log)
			throws AuthorizationException {
		long inNanos = System.nanoTime();
		String m = "addLogEntry(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(
							session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Long ret = service.addLogEntry(user, log);
			PersistenceUtil.commit(sessAndTrx);
			return ret;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public Long addPosting(User user, String dataSnapshotRevId,
			Post post) throws AuthorizationException {
		final String M = "addPosting(User, snapshot, post)";
		long in = System.nanoTime();
		Session sess = null;
		Transaction trx = null;
		try {
			checkNotNull(user);
			checkNotNull(dataSnapshotRevId);
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service =
					dsServiceFac.newInstance(
							sess,
							MAX_DS_TS_ANNS,
							dataSnapshotShortCache,
							searchResultCache);
			Long newPost = service.addPosting(user, dataSnapshotRevId, post);
			trx.commit();
			return newPost;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			propagateIfInstanceOf(e, AuthorizationException.class);
			throw propagate(e);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowThenSeconds(in));
		}
	}

	@Override
	public EegProject addProject(User admin, EegProject project)
			throws AuthorizationException {
		Date in = new Date();
		final String M = "addProject(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			String id = service.createProject(admin, project);

			EegProject ret = service.getProject(admin, id);
			trx.commit();
			return ret;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public String addProjectPosting(User user, String projectId,
			Post post) throws AuthorizationException {
		final String M = "addPosting(User, snapshot, post)";
		long in = System.nanoTime();
		Session sess = null;
		Transaction trx = null;
		try {
			checkNotNull(user);
			checkNotNull(projectId);
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			String newPost = service.addProjectPosting(user, projectId, post);
			trx.commit();
			return newPost;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowThenSeconds(in));
		}
	}

	@Override
	public Long addProvenanceEntry(User user, ProvenanceLogEntry log)
			throws AuthorizationException {
		Date in = new Date();
		final String M = "addProvenanceEntry(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			Long ret = service.addProvenanceEntry(user, log);
			trx.commit();
			return ret;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public String addTimeSeriesToDataset(
			final User user,
			final String datasetId,
			final Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(datasetId);
		checkNotNull(timeSeriesRevIds);
		final String M = "addTimeSeriesToDataset(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final String revId =
					service.addTimeSeriesToDataset(
							user,
							datasetId,
							timeSeriesRevIds);
			trx.commit();
			return revId;
		} catch (DatasetNotFoundException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public int cleanupOldSessions(Date minLastActive) {
		final String M = "cleanupOldSessions(...)";
		long in = System.nanoTime();
		Session sess = null;
		Transaction trx = null;
		try {
			checkNotNull(minLastActive);
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			final IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			int deleted = service.cleanupOldSessions(minLastActive);
			trx.commit();
			return deleted;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowThenSeconds(in));
		}
	}

	// @Override
	// public long countAnnotations() {
	// long inNanos = System.nanoTime();
	// final String m = "countAnnotations(...)";
	// SessionAndTrx sessAndTrx = null;
	// try {
	// sessAndTrx =
	// PersistenceUtil.getSessAndTrx(sessFac);
	// Session session = sessAndTrx.session;
	//
	// IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
	//
	// return dsDAO.countTsAnns();
	// } catch (RuntimeException e) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw e;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// logger.info(
	// "{} : {} seconds",
	// m,
	// BtUtil.diffNowThenSeconds(inNanos));
	// }
	// }

	@Override
	public long countAnnotations() {
		return longTransaction.execTransaction("countAnnotations",
				new TransAction<Long>() {

					@Override
					public Long execute(Session session) {
						IDataSnapshotDAO dsDAO = daoFac
								.getDataSnapshotDAO(session);

						return dsDAO.countTsAnns();
					}

				});
		// long inNanos = System.nanoTime();
		// final String m = "countAnnotations(...)";
		// SessionAndTrx sessAndTrx = null;
		// try {
		// sessAndTrx =
		// PersistenceUtil.getSessAndTrx(sessFac);
		// Session session = sessAndTrx.session;
		//
		// IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
		//
		// return dsDAO.countTsAnns();
		// } catch (RuntimeException e) {
		// PersistenceUtil.rollback(sessAndTrx);
		// throw e;
		// } finally {
		// PersistenceUtil.close(sessAndTrx);
		// logger.info(
		// "{} : {} seconds",
		// m,
		// BtUtil.diffNowThenSeconds(inNanos));
		// }
	}

	@Override
	public List<AnnotationCube> getAnnotationCube(User user) {
		long inNanos = System.nanoTime();

		String m = "getAnnotationCube(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			ITsAnnotationDAO tsAnnDAO =
					daoFac.getTsAnnotationDAO(
							session,
							sessFacConfig);

			List<AnnotationCube> cube = tsAnnDAO.getAnnotationCubeInfo();
			PersistenceUtil.commit(sessAndTrx);

			return cube;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public Map<String, Long> countLayers(
			User user,
			String datasetId) {
		long inNanos = System.nanoTime();
		checkArgument(!datasetId.isEmpty());
		String m = "countLayers(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(datasetId);
			if (ds == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(),
							false),
					CorePermDefs.READ,
					ds.getPubId());

			ITsAnnotationDAO tsAnnDAO =
					daoFac.getTsAnnotationDAO(
							session,
							sessFacConfig);
			Map<String, Long> counts = tsAnnDAO.countLayers(ds);
			PersistenceUtil.commit(sessAndTrx);
			logger.debug("{}: returning {}", m, counts);
			return counts;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public long countSnapshots() {
		long inNanos = System.nanoTime();
		final String m = "countSnapshots(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);

			return dsDAO.countActiveSnapshots();
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public long countUserReadableExperiments() {
		long inNanos = System.nanoTime();
		final String m = "countUserViewableExperiments(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			IExperimentDAO expDAO = daoFac.getExperimentDAO();

			return expDAO.countUserReadableExperiments();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public long countUserReadableStudies() {
		long inNanos = System.nanoTime();
		final String m = "countUserViewableStudies(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();

			return studyDAO.countUserReadableStudies();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public long countWorldReadableExperiments() {
		long inNanos = System.nanoTime();
		final String m = "countWorldReadableExperiments(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			IExperimentDAO expDAO = daoFac.getExperimentDAO();

			return expDAO.countWorldReadableExperiments();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public long countWorldReadableStudies() {
		long inNanos = System.nanoTime();
		final String m = "countWorldReadableStudies(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			IEegStudyDAO expDAO = daoFac.getEegStudyDAO();

			return expDAO.countWorldReadableStudies();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public void createDataUsage(
			User user,
			DataUsageEntity dataUsage) {
		long startTimeNanos = System.nanoTime();
		String m = "createDataUsage(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IUserDAO userDAO = daoFac.getUserDAO(session);
			UserEntity userEntity = userDAO.load(user.getUserId());
			dataUsage.setUser(userEntity);
			IDataUsageDAO dataUsageDAO =
					daoFac.getDataUsageDAO(session);
			DataSnapshotEntity ds =
					DataSnapshotUtil.getFromCache(
							dataUsage.getDataSnapshotPubId(),
							daoFac.getDataSnapshotDAO(session),
							dataSnapshotShortCache);

			dataUsage.setDataSnapshotId(ds.getId());
			dataUsage.setDataSnapshotName(ds.getLabel());
			dataUsageDAO.saveOrUpdate(dataUsage);
			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public void createJobForUser(UserId userId, String datasetId,
			String toolId,
			JobStatus status, ParallelDto parallelism, Date createTime) {
		Date in = new Date();
		final String M = "createJobForUser(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(
					sess,
					MAX_DS_TS_ANNS,
					dataSnapshotShortCache,
					searchResultCache);
			service.createJobForUser(userId, datasetId, toolId, status,
					parallelism,
					createTime);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	private static Optional<TimeSeriesEntity> findTimeSeries(
			String pubId,
			Iterable<TimeSeriesEntity> channels) {
		Optional<TimeSeriesEntity> channel =
				Iterables.tryFind(channels,
						compose(equalTo(pubId),
								IHasPubId.getPubId));
		return channel;
	}

	@Override
	public void deleteEegMontage(
			User user,
			Long montageId) throws EegMontageNotFoundException {
		long startTimeNanos = System.nanoTime();
		String m = "deleteMontage(...)";

		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IEegMontageDAO montageDAO =
					daoFac.getEegMontageDAO(sessAndTrx.session);
			EegMontageEntity montage = montageDAO.get(montageId);
			if (montage == null) {
				throw new EegMontageNotFoundException(montageId);
			}

			UserEntity montageUser = montage.getUser();

			IUserDAO userDAO = daoFac.getUserDAO(sessAndTrx.session);
			UserEntity callingUser = userDAO.getOrCreateUser(user.getUserId());
			if (callingUser != montageUser) {
				throw new EegMontageNotFoundException(montageId);
			}

			montageUser.getMontages().remove(montage);

			PersistenceUtil.commit(sessAndTrx);

			return;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public EEGMontage createEditEegMontage(
			User user,
			EEGMontage montage, String datsetId)
			throws EegMontageNotFoundException
	{

		long startTimeNanos = System.nanoTime();
		String m = "createEegMontage(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);

			UserEntity userEntity;
			DataSnapshotEntity datasetEntity;

			if (user != null) {
				final IExtAuthzHandler extAuthzHandler = AuthCheckFactory
						.getHandler();
				extAuthzHandler.checkPermitted(
						getPermissions(
								user,
								HasAclType.DATA_SNAPSHOT,
								CorePermDefs.CORE_MODE_NAME,
								datsetId,
								false),
						CorePermDefs.READ,
						datsetId);
				IUserDAO userDAO = daoFac.getUserDAO(sessAndTrx.session);
				IDataSnapshotDAO snapshotDAO = daoFac
						.getDataSnapshotDAO(sessAndTrx.session);

				userEntity = userDAO.getOrCreateUser(user.getUserId());
				datasetEntity = snapshotDAO.findByNaturalId(datsetId);

			} else {
				userEntity = null;
				datasetEntity = null;
			}

			EegMontageEntity entity;
			Long montageId = montage.getServerId();
			if (montageId == null) {
				entity = new EegMontageEntity(montage.getName(),
						userEntity);
				sessAndTrx.session.saveOrUpdate(entity);
			} else {
				IEegMontageDAO montageDAO =
						daoFac.getEegMontageDAO(sessAndTrx.session);
				entity = montageDAO.get(montageId);
				if (entity == null) {
					throw new EegMontageNotFoundException(montageId);
				}

				if (!entity.getUser().equals(userEntity)) {
					throw new AuthorizationException(
							"Not allowed to change snapshot with different owener.");
				}

				entity.setName(montage.getName());
				// Clear out existing pairs
				entity.getPairs().clear();
				sessAndTrx.session.flush();
			}

			if (datasetEntity != null) {
				Set<DataSnapshotEntity> snapshotsForMontage = entity
						.getSnapshots();
				snapshotsForMontage.add(datasetEntity);
			}

			int index = 0;
			for (EEGMontagePair pair : montage.getPairs()) {
				entity.getPairs().add(
						new EegMontagePairEntity(entity, pair.getEl1(), pair
								.getEl2(), index));
				index++;
			}

			entity.setIsPublic(montage.getIsPublic());

			PersistenceUtil.commit(sessAndTrx);

			montage.setServerId(entity.getId());
			return montage;
		} catch (EegMontageNotFoundException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

	@Override
	public SessionToken createSession(User user) {
		long startTimeNanos = System.nanoTime();
		String m = "createSession(User)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			SessionToken token = service.createSession(user);
			PersistenceUtil.commit(sessAndTrx);
			return token;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public void createUserIfNecessary(User user) {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service =
					dsServiceFac.newInstance(sess, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			service.createUserIfNecessary(user);
			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Override
	public void deleteDatasetChannels(
			User user,
			DatasetIdAndVersion datasetId,
			Collection<INamedTimeSegment> channels)
			throws DatasetNotFoundException, DatasetConflictException {
		long startTimeNanos = System.nanoTime();
		String m = "deleteDatasetChannel(...)";

		Set<String> channelUUIDs = new HashSet<String>();

		for (INamedTimeSegment ch : channels) {
			if (channelUUIDs.contains(ch.getId()))
				throw new DatasetConflictException(
						"Trying delete multiple channels with same UIUD "
								+ ch.getId());

			channelUUIDs.add(ch.getId());
		}

		removeTimeSeriesFromDataset(user, datasetId.getId(), channelUUIDs);

		// Redundant so disabled
		// SessionAndTrx sessAndTrx = null;
		// try {
		// sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
		//
		// IDataSnapshotDAO dataSnapshotDAO = daoFac
		// .getDataSnapshotDAO(sessAndTrx.session);
		// final DataSnapshotEntity ds =
		// dataSnapshotDAO.findByNaturalId(datasetId.getId());
		//
		// if (ds == null) {
		// throw new DataSnapshotNotFoundException(
		// datasetId.asDataSnapshotId());
		// }
		//
		// AuthCheckFactory.getHandler().checkPermitted(
		// getPermissions(
		// user,
		// HasAclType.DATA_SNAPSHOT,
		// CorePermDefs.CORE_MODE_NAME,
		// ds.getPubId(), false),
		// CorePermDefs.EDIT,
		// ds.getPubId());
		//
		// searchResultCache.invalidate(ds.getId());
		// dataSnapshotShortCache.invalidate(ds.getPubId());
		//
		// IObjectServer persistentStore = StorageFactory.getPersistentServer();
		//
		// Set<TimeSeriesEntity> del = new HashSet<TimeSeriesEntity>();
		// // Time series channels
		// for (TimeSeriesEntity tse: ds.getTimeSeries()) {
		// if (channelUUIDs.contains(tse.getPubId())) {
		// del.add(tse);
		// FileReference ref = new
		// FileReference((DirectoryBucketContainer)persistentStore.getDefaultContainer(),
		// tse.getFileKey(), tse.getFileKey());
		//
		// try {
		// persistentStore.deleteItem(ref);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }
		// }
		//
		// for (TimeSeriesEntity d: del) {
		// ds.getTimeSeries().remove(d);
		// }
		//
		// dataSnapshotDAO.saveOrUpdate(ds);
		//
		// PersistenceUtil.commit(sessAndTrx);
		// logger.info("{}: User {} deleted dataset {} channels {}",
		// m,
		// user.getUsername(),
		// ds.getLabel(),
		// channelUUIDs);
		// return;
		// } catch (RuntimeException e) {
		// PersistenceUtil.rollback(sessAndTrx);
		// throw e;
		// } finally {
		// PersistenceUtil.close(sessAndTrx);
		// logger.info(
		// "{} : {} seconds",
		// m,
		// BtUtil.diffNowThenSeconds(startTimeNanos));
		// }
	}

	@Override
	public void deleteDatasetObjects(
			User user,
			DatasetIdAndVersion datasetId,
			Collection<RecordingObject> objects)
			throws FailedVersionMatchException,
			DatasetConflictException {
		long startTimeNanos = System.nanoTime();
		String m = "deleteDatasetChannel(...)";

		Set<Long> objectIDs = new HashSet<Long>();

		for (RecordingObject obj : objects) {
			objectIDs.add(obj.getId());
		}

		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);

			IDataSnapshotDAO dataSnapshotDAO = daoFac
					.getDataSnapshotDAO(sessAndTrx.session);
			final DataSnapshotEntity ds =
					dataSnapshotDAO.findByNaturalId(datasetId.getId());

			if (ds == null) {
				throw new DataSnapshotNotFoundException(
						datasetId.asDataSnapshotId());
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.EDIT,
					ds.getPubId());

			searchResultCache.invalidate(ds.getId());
			dataSnapshotShortCache.invalidate(ds.getPubId());

			IObjectServer persistentStore = StorageFactory
					.getPersistentServer();

			Optional<Recording> recordingOpt = getRecording(ds);
			if (recordingOpt.isPresent()) {
				Recording recording = recordingOpt.get();

				Set<RecordingObjectEntity> del = new HashSet<RecordingObjectEntity>();
				for (final RecordingObjectEntity objEntity : recording
						.getObjects()) {
					if (objectIDs.contains(objEntity.getId())) {
						del.add(objEntity);
						FileReference ref = new FileReference(
								(DirectoryBucketContainer) persistentStore.getDefaultContainer(),
								objEntity.getFileName(), objEntity
										.getFileName());
						try {
							persistentStore.deleteItem(ref);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				// Other objects
				if (HibernateUtil.isInstanceOf(
						ds,
						EegStudy.class)) {
					IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
					EegStudy study = studyDAO.load(ds.getId());
					for (RecordingObjectEntity d : del)
						study.getRecording().getObjects().remove(d);
					studyDAO.saveOrUpdate(study);
				} else if (HibernateUtil.isInstanceOf(
						ds,
						ExperimentEntity.class)) {
					IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
					ExperimentEntity experiment = experimentDAO
							.load(ds.getId());
					for (RecordingObjectEntity d : del)
						experiment.getRecording().getObjects().remove(d);
					experimentDAO.saveOrUpdate(experiment);
				}

				dataSnapshotDAO.saveOrUpdate(ds);
			}

			PersistenceUtil.commit(sessAndTrx);
			logger.info("{}: User {} deleted dataset {} objects {}",
					m,
					user.getUsername(),
					ds.getLabel(),
					objectIDs);
			return;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public void deleteDataset(
			User user,
			DatasetIdAndVersion datasetId,
			boolean checkVersion,
			boolean deleteEmptySubject)
			throws FailedVersionMatchException,
			DatasetConflictException {
		long startTimeNanos = System.nanoTime();
		String m = "deleteDataset(...)";
		Set<String> fileKeysToDelete = Collections.emptySet();
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);

			IDataSnapshotDAO dataSnapshotDAO = daoFac
					.getDataSnapshotDAO(sessAndTrx.session);
			final DataSnapshotEntity ds =
					dataSnapshotDAO.findByNaturalId(datasetId.getId());

			if (ds == null) {
				throw new DataSnapshotNotFoundException(
						datasetId.asDataSnapshotId());
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.DELETE,
					ds.getPubId());

			if (checkVersion
					&& !ds.getVersion().equals(datasetId.getVersion())) {
				throw new FailedVersionMatchException(
						"dataset version does not match, refresh");
			}

			searchResultCache.invalidate(ds.getId());
			dataSnapshotShortCache.invalidate(ds.getPubId());

			// TODO: why isn't this a simple SQL cascade?

			ITsAnnotationDAO tsAnnotationDAO = daoFac.getTsAnnotationDAO(
					sessAndTrx.session,
					sessFacConfig);
			tsAnnotationDAO.deleteByParent(ds);

			IEegMontageDAO montageDAO = daoFac
					.getEegMontageDAO(sessAndTrx.session);
			List<EegMontageEntity> montages = montageDAO.findForSnapshot(ds);
			for (EegMontageEntity montage : montages) {
				montage.getSnapshots().remove(ds);
				logger.info("{}: Removing {} from montage {}",
						m,
						ds.getLabel(),
						montage.getName());
				if (montage.getSnapshots().isEmpty()) {
					montageDAO.delete(montage);
					logger.info("{}: Deleting montage {}",
							m,
							montage.getName());
				}
			}
			if (HibernateUtil.isInstanceOf(
					ds,
					EegStudy.class)) {
				final IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
				final EegStudy study = studyDAO.load(ds.getId());
				fileKeysToDelete = deleteStudy(
						sessAndTrx.session,
						study,
						deleteEmptySubject);
			} else if (HibernateUtil.isInstanceOf(
					ds,
					ExperimentEntity.class)) {
				final IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
				final ExperimentEntity experiment = experimentDAO.load(ds
						.getId());
				fileKeysToDelete = deleteExperiment(
						sessAndTrx.session,
						experiment,
						deleteEmptySubject);

			} else {
				final IDatasetDAO datasetDAO = daoFac.getDatasetDAO();
				final Dataset dataset = datasetDAO.load(ds.getId());
				deleteDerivedDataset(
						sessAndTrx.session,
						dataset);
			}
			PersistenceUtil.commit(sessAndTrx);
			logger.info("{}: User {} deleted dataset {}",
					m,
					user.getUsername(),
					ds.getLabel());
			// Don't delete files unless trx succeeds.
			if (!fileKeysToDelete.isEmpty()) {
				deleteContents(fileKeysToDelete);
			}
			return;
		} catch (FailedVersionMatchException fvme) {
			PersistenceUtil.rollback(sessAndTrx);
			throw fvme;
		} catch (DatasetConflictException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	/**
	 * Delete the actual files corresponding to the given file keys
	 * 
	 * @param fileKeysToDelete
	 */
	private void deleteContents(Set<String> fileKeysToDelete) {
		final String m = "deleteContents(...)";
		IObjectServer persistentStore = StorageFactory.getPersistentServer();

		for (String fileKey : fileKeysToDelete) {
			FileReference ref = new FileReference(
					(DirectoryBucketContainer) persistentStore.getDefaultContainer(),
					fileKey, fileKey);

			try {
				persistentStore.deleteItem(ref);
			} catch (IOException e) {
				logger.error(m + ": Error deleting " + ref.getFilePath(), e);
			}
		}

	}

	/**
	 * Gather the actual files corresponding to the data contents
	 * 
	 * @param ds
	 */
	private Set<String> gatherContents(DataSnapshotEntity ds) {
		final Set<String> fileKeys = new HashSet<>();

		// Time series channels
		for (TimeSeriesEntity tse : ds.getTimeSeries()) {
			fileKeys.add(tse.getFileKey());
		}

		// Other objects
		Optional<Recording> recordingOpt = getRecording(ds);
		if (recordingOpt.isPresent()) {
			Recording recording = recordingOpt.get();

			for (final RecordingObjectEntity objEntity : recording
					.getObjects()) {
				fileKeys.add(objEntity.getFileName());
			}
		}
		return fileKeys;

	}

	/**
	 * Returns set of filekeys that may be deleted from storage.
	 * 
	 * @param session
	 * @param study
	 * @param deleteEmptySubject
	 * @return
	 * @throws DatasetConflictException
	 */
	private Set<String> deleteStudy(
			Session session,
			EegStudy study,
			boolean deleteEmptySubject)
			throws DatasetConflictException {
		final IDataSnapshotService service = dsServiceFac.newInstance(
				session,
				MAX_DS_TS_ANNS,
				dataSnapshotShortCache,
				searchResultCache);
		Set<String> fileKeysToDelete = Collections.emptySet();
		final boolean modifiable = service.isRecordingModifiable(study);
		if (modifiable) {
			final HospitalAdmission admission = study.getParent();
			admission.removeStudy(study);
			fileKeysToDelete = gatherContents(study);
			if (deleteEmptySubject) {
				final Patient patient = admission.getParent();
				boolean empty = true;
				for (final HospitalAdmission add : patient.getAdmissions()) {
					if (!add.getStudies().isEmpty()) {
						empty = false;
						break;
					}
				}
				if (empty) {
					final IPatientDAO patientDAO = daoFac.getPatientDAO();
					patientDAO.delete(patient);
				}
			}
		} else {
			throw new DatasetConflictException(
					"Study "
							+ study.getLabel()
							+ " cannot be deleted until it is removed from all projects and all datasets derived from it are deleted.");
		}
		return fileKeysToDelete;
	}

	/**
	 * Returns set of filekeys that may be deleted from storage.
	 * 
	 * @param session
	 * @param experiment
	 * @param deleteEmptySubject
	 * @return
	 * @throws DatasetConflictException
	 */
	private Set<String> deleteExperiment(
			Session session,
			ExperimentEntity experiment,
			boolean deleteEmptySubject)
			throws DatasetConflictException {
		Set<String> fileKeysToDelete = Collections.emptySet();
		final IDataSnapshotService service = dsServiceFac.newInstance(
				session,
				MAX_DS_TS_ANNS,
				dataSnapshotShortCache,
				searchResultCache);
		final boolean modifiable = service.isRecordingModifiable(experiment);
		if (modifiable) {
			final AnimalEntity animal = experiment.getParent();
			if (animal != null) {
				animal.removeExperiment(experiment);
				fileKeysToDelete = gatherContents(experiment);
				if (deleteEmptySubject) {
					if (animal.getExperiments().isEmpty()) {
						final IAnimalDAO animalDAO = daoFac.getAnimalDAO(session);
						animalDAO.delete(animal);
					}
				}
			}
		} else {
			throw new DatasetConflictException(
					"Experiment "
							+ experiment.getLabel()
							+ " cannot be deleted until it is removed from all projects and all datasets derived from it are deleted.");
		}
		return fileKeysToDelete;
	}

	private boolean deleteDerivedDataset(
			Session session,
			Dataset dataset) throws DatasetConflictException {
		final IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
		final boolean deletable = !dsDAO.isInAProject(dataset);
		if (deletable) {
			dsDAO.delete(dataset);
			return true;
		} else {
			throw new DatasetConflictException(
					"Dataset "
							+ dataset.getLabel()
							+ " cannot be deleted until it is removed from all projects.");
		}
	}

	@Nonnull
	@Override
	public String deriveDataset(
			final User user,
			final String sourceRevId,
			final String derivedDsLabel,
			final String toolLabel,
			@Nullable final Set<String> timeSeriesRevIds,
			@Nullable final Set<String> tsAnnotationRevIds)
			throws DuplicateNameException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(sourceRevId);
		final String M = "deriveDataset(...)";

		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final String revId = service.deriveDataset(
					user,
					sourceRevId,
					derivedDsLabel,
					toolLabel,
					timeSeriesRevIds,
					tsAnnotationRevIds);
			trx.commit();
			return revId;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public void doNothing() {
		long startTimeNanos = System.nanoTime();
		String m = "doNothing()";
		SessionAndTrx sessAndTrx = null;
		double a = -1;
		double b = -1;
		double c = -1;
		try {
			long aStart = System.nanoTime();
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			a = BtUtil.diffNowThenSeconds(aStart);
			long bStart = System.nanoTime();
			PersistenceUtil.commit(sessAndTrx);
			b = BtUtil.diffNowThenSeconds(bStart);
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			long cStart = System.nanoTime();
			PersistenceUtil.close(sessAndTrx);
			c = BtUtil.diffNowThenSeconds(cStart);
			logger.info(
					"{} : {} seconds {} {} {}",
					new Object[] {
							m,
							BtUtil.diffNowThenSeconds(startTimeNanos),
							a,
							b,
							c });
		}
	}

	@Override
	public List<EditAclResponse> editAcl(User user,
			HasAclType entityType,
			String mode,
			List<IEditAclAction<?>> actions) throws AuthorizationException {
		final long inTime = new Date().getTime();
		final String M = "editAcl(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = HibernateUtil.getSessionFactory().openSession();
			trx = sess.beginTransaction();
			final IDataSnapshotService dsService =
					dsServiceFac.newInstance(sess, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final List<EditAclResponse> responses = dsService
					.editAcl(
							user, entityType,
							mode,
							actions);
			trx.commit();
			return responses;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public GetAcesResponse getAcesResponse(
			User user,
			HasAclType entityType,
			String mode,
			String targetId) {
		final long inTime = new Date().getTime();
		final String M = "getAcesResponse(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			final IDataSnapshotService dsService =
					dsServiceFac.newInstance(
							sess,
							MAX_DS_TS_ANNS,
							dataSnapshotShortCache,
							searchResultCache);
			GetAcesResponse getAcesResponse = dsService
					.getAcesResponse(
							user,
							entityType,
							mode,
							targetId);
			trx.commit();
			return getAcesResponse;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<AnnotationDefinition> getAnnotationDefinitions(User user,
			String viewerType) {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getAnnotationDefinitions(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final List<AnnotationDefinition> annotations =
					service.getAnnotationDefinitions(user, viewerType);
			trx.commit();
			return annotations;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public Integer getAnnotationsByUser(User user)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);
		final String M = "getAnnotationsByUser(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final Integer count =
					service.getAnnotationsByUser(user);
			PersistenceUtil.commit(sessAndTrx);
			return count;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(sessAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<ChannelInfoDto> getChannelInfo(String dataSnapshotId) {
		Date in = new Date();
		final String M = "getChannelInfo(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			List<ChannelInfoDto> ret = service.getChannelInfos(dataSnapshotId);
			trx.commit();
			return ret;
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public String getClob(
			final User user,
			final String dsRevId) throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dsRevId);
		final String M = "getClob(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final String clob =
					service.getClob(user, dsRevId);
			trx.commit();
			return clob;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public Set<IJsonKeyValue> getJsonKeyValues(
			final User user,
			final String dsRevId) throws AuthorizationException {

		return keyValueTransaction.execTransaction("getJsonKeyValues",
				new TransAction<Set<IJsonKeyValue>>() {

					@Override
					public Set<IJsonKeyValue> execute(Session session) {
						final IDataSnapshotService service =
								dsServiceFac.newInstance(session,
										MAX_DS_TS_ANNS,
										dataSnapshotShortCache,
										searchResultCache);
						try {
							return service.getJsonKeyValues(user, dsRevId);
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
					}

				});
		// final long inTime = new Date().getTime();
		// checkNotNull(user);
		// checkNotNull(dsRevId);
		// final String M = "getJsonKeyValues(...)";
		// Session session = null;
		// Transaction trx = null;
		// try {
		// session = sessFac.openSession();
		// trx = session.beginTransaction();
		// final IDataSnapshotService service =
		// dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
		// dataSnapshotShortCache, searchResultCache);
		// final Set<JsonKeyValue> values =
		// service.getJsonKeyValues(user, dsRevId);
		// trx.commit();
		// return values;
		// } catch (RuntimeException t) {
		// PersistenceUtil.rollback(trx);
		// throw t;
		// } finally {
		// PersistenceUtil.close(session);
		// logger.info(
		// "{} : {} seconds",
		// M,
		// (new Date().getTime() - inTime) / 1000.0);
		// }
	}

	@Override
	public String getDatasetPortion(final String dataSnapshotRevId) {
		return dataSnapshotRevId;
	}

	@Override
	public DataSnapshot getDataSnapshot(
			final User user,
			final String dataSnapshotId) throws AuthorizationException {
		long startTimeNanos = System.nanoTime();
		String m = "getDataSnapshot(User, String)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			DataSnapshot ds = service.getDataSnapshot(user, dataSnapshotId);
			PersistenceUtil.commit(sessAndTrx);
			return ds;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public DataSnapshotEntity getDataSnapshotFromCache(
			User user,
			String dsId) {
		long startTimeNanos = System.nanoTime();
		String m = "getDataSnapshotEntity(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			DataSnapshotEntity ds =
					DataSnapshotUtil.getFromCache(
							dsId,
							daoFac.getDataSnapshotDAO(session),
							dataSnapshotShortCache);

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(),
							false),
					CorePermDefs.READ,
					ds.getPubId());

			PersistenceUtil.commit(sessAndTrx);
			return ds;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

	@Override
	public String getDataSnapshotId(
			User user,
			String dsName,
			boolean willUpdate) throws DataSnapshotNotFoundException {
		long startTimeNanos = System.nanoTime();
		String m = "getIdForDataSnapshot(...)";
		SessionAndTrx sessAndTrx = null;
		String dsNameTrimmed = dsName.trim();
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);

			String dsId = null;

			Set<DataSnapshotEntity> dsSet = dsDAO
					.findAnyWithLabel(dsNameTrimmed);
			// First see if there's one where we have EDIT permissions; if so we
			// get the first one
			for (DataSnapshotEntity ds : dsSet) {
				if (AuthCheckFactory.getHandler().isPermitted(
						getPermissions(
								user,
								HasAclType.DATA_SNAPSHOT,
								CorePermDefs.CORE_MODE_NAME,
								ds.getPubId(),
								false),
						CorePermDefs.EDIT,
						ds.getPubId())) {
					dsId = ds.getPubId();
					break;
				}
			}

			// Didn't find anything editable
			if (dsId == null) {
				// If we were requesting just a READ then see if we can find a
				// match
				// satisfying that. If so we get the first one.
				if (!willUpdate) {
					for (DataSnapshotEntity ds : dsSet) {
						if (AuthCheckFactory.getHandler().isPermitted(
								getPermissions(
										user,
										HasAclType.DATA_SNAPSHOT,
										CorePermDefs.CORE_MODE_NAME,
										ds.getPubId(),
										false),
								CorePermDefs.READ,
								ds.getPubId())) {
							dsId = ds.getPubId();
							break;
						}
					}
				}
				if (dsId == null)
					throw new DataSnapshotNotFoundException(dsNameTrimmed);
			}

			PersistenceUtil.commit(sessAndTrx);
			return dsId;
		} catch (DataSnapshotNotFoundException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public Set<String> getDataSnapshotNames(User user, User optUser)
			throws AuthorizationException {
		long startTimeNanos = System.nanoTime();
		String m = "getDataSnapshotNames(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Set<DataSnapshotSearchResult> results =
					service.getDataSnapshotsNoCounts(
							user,
							new EegStudySearch());

			PersistenceUtil.commit(sessAndTrx);
			Set<String> ret = new HashSet<String>();
			for (DataSnapshotSearchResult res : results)
				if (optUser == null)
					ret.add(res.getLabel());
				else if (res.getOwner() != null &&
						res.getOwner().equals(optUser.getUserId()))
					ret.add(res.getLabel());
			return ret;
		} catch (DataSnapshotNotFoundException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException e) {
			e.printStackTrace();
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@SuppressWarnings("all")
	public String getDataSnapshotIdByLabel(
			final User user,
			final String label) throws AuthorizationException {
		// throw new UnsupportedOperationException();
		long startTimeNanos = System.nanoTime();
		String m = "getDataSnapshot(User, String)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service = dsServiceFac.newInstance(
					session,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			String ds = service.getSnapshotIDFromName(user, label);
			PersistenceUtil.commit(sessAndTrx);
			return ds;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshots(
			final User user,
			final EegStudySearch studySearch)
			throws AuthorizationException {
		long inTime = System.nanoTime();
		String m = "getDataSnapshots(User, EegStudySearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Set<DataSnapshotSearchResult> results =
					service.getDataSnapshots(
							user,
							studySearch);
			PersistenceUtil.commit(sessAndTrx);
			return results;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			if (sessFac.getStatistics().isStatisticsEnabled()) {
				logger.info(
						"{} : {} seconds, {} sess opened {} sess closed",
						new Object[] { m,
								BtUtil.diffNowThenSeconds(inTime),
								sessFac.getStatistics().getSessionOpenCount(),
								sessFac.getStatistics().getSessionCloseCount() });
			} else {
				logger.info(
						"{} : {} seconds",
						m,
						BtUtil.diffNowThenSeconds(inTime));
			}
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshotsNoCounts(
			final User user,
			final EegStudySearch studySearch)
			throws AuthorizationException {
		long inTime = System.nanoTime();
		String m = "getDataSnapshots(User, EegStudySearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Set<DataSnapshotSearchResult> results =
					service.getDataSnapshotsNoCounts(
							user,
							studySearch);
			PersistenceUtil.commit(sessAndTrx);
			return results;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			if (sessFac.getStatistics().isStatisticsEnabled()) {
				logger.info(
						"{} : {} seconds, {} sess opened {} sess closed",
						new Object[] { m,
								BtUtil.diffNowThenSeconds(inTime),
								sessFac.getStatistics().getSessionOpenCount(),
								sessFac.getStatistics().getSessionCloseCount() });
			} else {
				logger.info(
						"{} : {} seconds",
						m,
						BtUtil.diffNowThenSeconds(inTime));
			}
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshotsForUser(
			final User user)
			throws AuthorizationException {
		long inTime = System.nanoTime();
		String m = "getDataSnapshots(User, EegStudySearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Set<DataSnapshotSearchResult> results =
					service.getDataSnapshotsForUser(
							user);
			PersistenceUtil.commit(sessAndTrx);
			return results;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			if (sessFac.getStatistics().isStatisticsEnabled()) {
				logger.info(
						"{} : {} seconds, {} sess opened {} sess closed",
						new Object[] { m,
								BtUtil.diffNowThenSeconds(inTime),
								sessFac.getStatistics().getSessionOpenCount(),
								sessFac.getStatistics().getSessionCloseCount() });
			} else {
				logger.info(
						"{} : {} seconds",
						m,
						BtUtil.diffNowThenSeconds(inTime));
			}
		}
	}

	@Override
	public DataSnapshotSearchResult getDataSnapshotForId(
			final User user, final String snapshotID)
			throws AuthorizationException {
		long inTime = System.nanoTime();
		String m = "getDataSnapshots(User, EegStudySearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			DataSnapshotSearchResult results = service.getDataSnapshotForId(
					user, snapshotID);
			PersistenceUtil.commit(sessAndTrx);
			return results;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			if (sessFac.getStatistics().isStatisticsEnabled()) {
				logger.info(
						"{} : {} seconds, {} sess opened {} sess closed",
						new Object[] { m,
								BtUtil.diffNowThenSeconds(inTime),
								sessFac.getStatistics().getSessionOpenCount(),
								sessFac.getStatistics().getSessionCloseCount() });
			} else {
				logger.info(
						"{} : {} seconds",
						m,
						BtUtil.diffNowThenSeconds(inTime));
			}
		}
	}

	@Override
	public EegStudyMetadata getEegStudyMetadata(User user, String studyId)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		final String M = "getEegStudyMetadata(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(
							session,
							MAX_DS_TS_ANNS,
							dataSnapshotShortCache,
							searchResultCache);
			EegStudyMetadata studyMd = service.getEegStudyMetadata(
					user,
					studyId);
			trx.commit();
			return studyMd;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public ExperimentMetadata getExperimentMetadata(
			User user,
			String experimentId) throws AuthorizationException {
		final long inTime = System.nanoTime();
		final String M = "getExperimentMetadata(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(sess, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			ExperimentMetadata experimentMd = service.getExperimentMetadata(
					user,
					experimentId);
			trx.commit();
			return experimentMd;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{} : {} seconds",
					M,
					BtUtil.diffNowThenSeconds(inTime));
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getExperiments(
			User user,
			ExperimentSearch experimentSearch) {
		final long inTime = new Date().getTime();
		final String M = "getExperiments(User, ExperimentSearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IUserDAO userDAO = daoFac.getUserDAO(sessAndTrx.session);
			IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
			ITimeSeriesDAO timeSeriesDAO = daoFac
					.getTimeSeriesDAO(sessAndTrx.session);
			IDataSnapshotDAO dataSnapshotDAO = daoFac
					.getDataSnapshotDAO(sessAndTrx.session);
			UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

			Set<ExperimentEntity> experiments =
					experimentDAO.findBySearch(
							experimentSearch,
							user,
							userEntity);

			Set<DataSnapshotSearchResult> searchResults = newHashSet();
			for (ExperimentEntity experiment : experiments) {
				// authz done in query

				SearchResultCacheEntry searchResultCacheEntry = null;

				searchResultCacheEntry =
						searchResultCache
								.getIfPresent(experiment.getId());
				Set<TimeSeriesEntity> ts = newHashSet();

				if (searchResultCacheEntry == null) {
					int imgCnt = 0; // there will never be images for
									// experiments

					int tsCount =
							timeSeriesDAO.countByRecording(
									experiment.getRecording());

					int tsAnnCnt = Ints.checkedCast(dataSnapshotDAO
							.countTsAnns(experiment));
					searchResultCacheEntry =
							new SearchResultCacheEntry(
									imgCnt,
									tsCount,
									tsAnnCnt,
									experiment.getOrganization().getName());
					searchResultCache.put(
							experiment.getId(),
							searchResultCacheEntry);
				}

				for (long i = 0; i < searchResultCacheEntry.getTsCount(); i++) {
					TimeSeriesEntity tsEntity = new TimeSeriesEntity();
					tsEntity.setPubId("dummy-pubId-" + i);
					tsEntity.setLabel("dummy-label-" + i);
					ts.add(tsEntity);
				}

				searchResults
						.add(
						DsSearchResultAssembler.assembleDsSearchResult(
								experiment,
								ts,
								searchResultCacheEntry.getImageCount(),
								searchResultCacheEntry.getTsAnnCount(),
								experiment.getRecording().getStartTimeUutc(),
								experiment.getRecording().getEndTimeUutc(),
								experiment.getParent().getOrganization()
										.getName()));
			}
			PersistenceUtil.commit(sessAndTrx);
			return searchResults;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);

		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getExperimentsNoCounts(
			User user,
			ExperimentSearch experimentSearch) {
		final long inTime = new Date().getTime();
		final String M = "getExperiments(User, ExperimentSearch)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IUserDAO userDAO = daoFac.getUserDAO(sessAndTrx.session);
			IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
			ITimeSeriesDAO timeSeriesDAO = daoFac
					.getTimeSeriesDAO(sessAndTrx.session);
			IDataSnapshotDAO dataSnapshotDAO = daoFac
					.getDataSnapshotDAO(sessAndTrx.session);
			UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

			Set<ExperimentEntity> experiments =
					experimentDAO.findBySearch(
							experimentSearch,
							user,
							userEntity);

			Set<DataSnapshotSearchResult> searchResults = newHashSet();
			for (ExperimentEntity experiment : experiments) {
				// authz done in query

				Set<TimeSeriesEntity> ts = newHashSet();
				searchResults
						.add(
						DsSearchResultAssembler.assembleDsSearchResult(
								experiment,
								ts,
								0,
								0,
								experiment.getRecording().getStartTimeUutc(),
								experiment.getRecording().getEndTimeUutc(),
								experiment.getParent().getOrganization()
										.getName()));
			}
			PersistenceUtil.commit(sessAndTrx);
			return searchResults;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);

		}
	}

	@Override
	public ExperimentSearchCriteria getExperimentSearchCriteria() {
		long in = System.nanoTime();
		final String M = "getExperimentSearchCriteria()";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			ISpeciesDAO speciesDAO = daoFac.getSpeciesDAO();
			IStimTypeDAO stimTypeDAO = daoFac.getStimTypeDAO();
			IDrugDAO drugDAO = daoFac.getDrugDAO();
			IStimRegionDAO stimRegionDAO = daoFac.getStimRegionDAO();
			IOrganizationDAO organizationDAO = daoFac.getOrganizationDAO();

			List<SpeciesEntity> species = speciesDAO.findAllOrderBy("label");
			Map<Long, String> speciesCrits = newLinkedHashMap();

			List<StimTypeEntity> stimTypes = stimTypeDAO
					.findAllOrderBy("label");
			Map<Long, String> stimTypeCrits = newLinkedHashMap();

			List<StimRegionEntity> stimRegions = stimRegionDAO
					.findAllOrderBy("label");
			Map<Long, String> stimRegionCrits = newLinkedHashMap();

			List<DrugEntity> drugs = drugDAO.findAllOrderBy("label");
			Map<Long, String> drugCrits = newLinkedHashMap();

			List<OrganizationEntity> organizations = organizationDAO
					.findAllOrderBy("name");
			Map<String, String> orgCrits = newLinkedHashMap();

			for (SpeciesEntity sp : species) {
				speciesCrits.put(sp.getId(), sp.getLabel());
			}

			for (StimTypeEntity stimType : stimTypes) {
				stimTypeCrits.put(stimType.getId(), stimType.getLabel());
			}

			for (StimRegionEntity stimRegion : stimRegions) {
				stimRegionCrits.put(stimRegion.getId(), stimRegion.getLabel());
			}

			for (DrugEntity drug : drugs) {
				drugCrits.put(drug.getId(), drug.getLabel());
			}

			for (OrganizationEntity org : organizations) {
				orgCrits.put(org.getCode(), org.getName());
			}

			PersistenceUtil.commit(sessAndTrx);
			return new ExperimentSearchCriteria(
					speciesCrits,
					drugCrits,
					stimTypeCrits,
					stimRegionCrits,
					orgCrits);

		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					BtUtil.diffNowThenSeconds(in));
		}
	}

	@Override
	public Set<Image> getImages(
			User user,
			String dataSnapshotId)
			throws AuthorizationException {
		final long inTime = System.nanoTime();
		final String M = "getImages(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService dsService = dsServiceFac.newInstance(
					sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			Set<Image> images = dsService.getImages(user, dataSnapshotId);
			trx.commit();
			return images;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{} : {} seconds",
					M,
					BtUtil.diffNowThenSeconds(inTime));
		}
	}

	@Override
	public Set<ImageForTrace> getImagesForTimeSeries(
			final User user,
			final String dataSnapshotId,
			final String timeSeriesId) {
		final long inTime = new Date().getTime();
		checkNotNull(timeSeriesId);

		final String m = "getImagesForTimeSeries(...)";
		Session session = null;
		Transaction trx = null;

		try {
			session = sessFac.openSession();

			trx = session.beginTransaction();
			final IDataSnapshotDAO dsDAO = new DataSnapshotDAOHibernate(session);
			final DataSnapshotEntity dataSnapshot = dsDAO
					.findByNaturalId(dataSnapshotId);
			if (dataSnapshot == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dataSnapshot.getPubId(),
							false),
					CorePermDefs.READ,
					dataSnapshot.getPubId());

			final Optional<TimeSeriesEntity> timeSeries = Iterables.tryFind(
					dataSnapshot.getTimeSeries(),
					compose(equalTo(timeSeriesId), IHasPubId.getPubId));

			checkArgument(timeSeries.isPresent(), "DataSnapshot "
					+ dataSnapshot.getLabel()
					+ " does not contain time series " + timeSeriesId);

			final IEegStudyDAO studyDAO =
					new EegStudyDAOHibernate(session);

			final Contact contact =
					studyDAO.findContactByTracePubId(
							timeSeriesId);
			final Set<ImageForTrace> imageForTraces = newHashSet();
			if (contact != null) {
				imageForTraces.addAll(
						DataSnapshotUtil
								.imagedContacts2ImageForTraces(contact
										.getImagedContacts()));
			}

			trx.commit();
			logger.info(
					"{}: Returning {} images for data snapshot {}, time series {}. {} seconds",
					new Object[] { m, imageForTraces.size(),
							dataSnapshot.getLabel(),
							timeSeries.get().getLabel(),
							(new Date().getTime() - inTime) / 1000.0 });
			return imageForTraces;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public List<DataSnapshotSearchResult> getLatestSnapshots(
			final User user,
			final Iterable<String> dsStableIds) throws AuthorizationException {
		final long inTime = new Date().getTime();
		String m = "getLatestSnapshots(...)";
		SessionAndTrx sessionAndTrx = null;
		try {
			sessionAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessionAndTrx.session;

			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			List<DataSnapshotSearchResult> dsSearchResults = service
					.getLatestSnapshots(
							user,
							dsStableIds);
			PersistenceUtil.commit(sessionAndTrx);
			return dsSearchResults;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(sessionAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessionAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<LogMessage> getLogEntries(User user, int startIndex, int count)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getLogEntries(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final List<LogMessage> entries =
					service.getLogEntries(user, startIndex, count);
			trx.commit();
			return entries;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<LogMessage> getLogEntriesSince(User user, Timestamp time)
			throws AuthorizationException {
		checkNotNull(user);
		checkNotNull(time);
		long inNanos = System.nanoTime();
		String m = "getLogEntriesSince(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			List<LogMessage> entries =
					service.getLogEntriesSince(user, time);
			PersistenceUtil.commit(sessAndTrx);
			return entries;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public List<String> getMEFPaths(
			final User user,
			final String dataSnapshotRevId,
			final Iterable<String> tsIds)
			throws AuthorizationException {
		long inNanos = System.nanoTime();
		checkNotNull(user);
		checkNotNull(dataSnapshotRevId);
		checkNotNull(tsIds);
		String m = "getMEFPaths(User, String, Iterable)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service = dsServiceFac
					.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			List<String> mefPaths = service.getMEFPaths(
					user,
					dataSnapshotRevId,
					tsIds);
			PersistenceUtil.commit(sessAndTrx);
			return mefPaths;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public ExtPermissions getPermissions(User user, HasAclType entityType,
			String mode, String targetId, boolean useDatasetShortCache) {
		long startTimeNanos = System.nanoTime();
		String m = "getPermissions(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService dsService = dsServiceFac
					.newInstance(
							session,
							dataSnapshotShortCache,
							searchResultCache);
			final ExtPermissions permissions = dsService.getPermissions(
					user,
					entityType,
					mode,
					targetId,
					useDatasetShortCache);
			PersistenceUtil.commit(sessAndTrx);
			return permissions;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.debug(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public Set<AllowedAction> getPermittedActionsOnDataset(User user,
			String dsId) {
		long startTimeNanos = System.nanoTime();
		final String m = "getPermittedActionsOnDataSnapshot(...)";
		try {
			final Set<AllowedAction> allowedActions = newHashSet();
			final IExtAuthzHandler authzHandler = AuthCheckFactory.getHandler();
			final ExtPermissions perms = getPermissions(
					user,
					HasAclType.DATA_SNAPSHOT,
					CorePermDefs.CORE_MODE_NAME,
					dsId,
					false);
			authzHandler.checkPermitted(perms, CorePermDefs.READ, dsId);
			// If we made it to here, then read is allowed.
			allowedActions.add(CorePermDefs.READ);
			final Set<AllowedAction> remainingCoreActions = newHashSet(
					CorePermDefs.EDIT,
					CorePermDefs.DELETE,
					CorePermDefs.EDIT_ACL);
			for (final AllowedAction action : remainingCoreActions) {
				if (authzHandler.isPermitted(perms, action, dsId)) {
					allowedActions.add(action);
				}
			}
			return allowedActions;
		} catch (RuntimeException e) {
			throw e;
		} finally {
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public List<Post> getPostings(final User user,
			final String dsRevId,
			int postIndex, int numPosts) throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dsRevId);
		final String M = "getPostings(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final List<Post> posts =
					service.getPostings(user, dsRevId, postIndex, numPosts);
			trx.commit();
			return posts;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public EegProject getProject(User admin, String id)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(admin);

		final String M = "getProject(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final EegProject count =
					service.getProject(admin, id);
			trx.commit();
			return count;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<Post> getProjectDiscussions(User user,
			String projectId, int postIndex, int numPosts)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);
		checkNotNull(projectId);
		final String M = "getProjectDiscussions(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final List<Post> posts =
					service.getProjectDiscussions(user, projectId, postIndex,
							numPosts);
			trx.commit();
			return posts;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<ProjectGroup> getProjectGroups(Set<ProjectGroup> excludedGroups) {
		long startTimeNanos = System.nanoTime();
		final String m = "getProjectGroups(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IProjectDAO projectDAO = daoFac.getProjectDAO(session);
			final List<ProjectEntity> allProjects = projectDAO
					.findAllOrderBy("name");
			final List<ProjectGroup> groups = newArrayList();
			for (final ProjectEntity project : allProjects) {
				final ProjectGroup adminGroup = new ProjectGroup(
						project.getPubId(),
						project.getName(),
						ProjectGroupType.ADMINS);
				if (!excludedGroups.contains(adminGroup)) {
					groups.add(adminGroup);
				}
				final ProjectGroup teamGroup = new ProjectGroup(
						project.getPubId(),
						project.getName(),
						ProjectGroupType.TEAM);
				if (!excludedGroups.contains(teamGroup)) {
					groups.add(teamGroup);
				}
			}
			PersistenceUtil.commit(sessAndTrx);
			return groups;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public Set<EegProject> getProjectsForUser(User user)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getProjectsForUser(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final Set<EegProject> count =
					service.getProjectsForUser(user);
			PersistenceUtil.commit(sessAndTrx);
			return count;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(sessAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<String> getProjectIdsByName(User user, String projectName)
			throws AuthorizationException {
		final long inTime = System.nanoTime();
		checkNotNull(user);
		checkNotNull(projectName);
		final String m = "getProjectIdByName(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			final Session session = sessAndTrx.session;
			final IUserDAO userDAO = daoFac.getUserDAO(session);
			final UserEntity userEntity = userDAO.getOrCreateUser(user
					.getUserId());
			final IProjectDAO projectDAO = daoFac.getProjectDAO(session);

			final List<String> projectIds = projectDAO.findIdsByName(
					userEntity, projectName);
			PersistenceUtil.commit(sessAndTrx);
			return projectIds;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(sessAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inTime));
		}
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesByUser(User user)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getProvenanceEntriesForUser(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final List<ProvenanceLogEntry> entries =
					service.getProvenanceEntriesByUser(user);
			PersistenceUtil.commit(sessAndTrx);
			return entries;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(sessAndTrx);
			throw t;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesForSnapshot(
			final User user,
			final String dataSnapshotRevId)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getLogEntries(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final List<ProvenanceLogEntry> entries =
					service.getProvenanceEntriesForSnapshot(user,
							dataSnapshotRevId);
			trx.commit();
			return entries;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesForUser(User user)
			throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getProvenanceEntriesForUser(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final List<ProvenanceLogEntry> entries =
					service.getProvenanceEntriesForUser(user);
			trx.commit();
			return entries;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public Set<DatasetAndTool> getRelatedAnalyses(
			User user,
			String studyRevId) {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(studyRevId);
		final String M = "getRelatedAnalyses(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			trx.commit();
			final Set<DatasetAndTool> results =
					service.getRelatedAnalyses(user, studyRevId);
			return results;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getRelatedSearchResults(
			User user,
			String inputDatasetId) {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(inputDatasetId);
		final String m = "getRelatedSearchResults(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IAnalyzedDataSnapshotDAO analyzedDsDAO = daoFac
					.getAnalyzedDataSnapshotDAO();
			final Set<AnalyzedDataSnapshot> analyzedSnapshots =
					analyzedDsDAO.findByDsPubId(inputDatasetId);

			final AnalyzedDataSnapshot firstOne = get(analyzedSnapshots, 0,
					null);
			final IExtAuthzHandler extAuthzHandler = AuthCheckFactory
					.getHandler();
			if (firstOne != null) {
				extAuthzHandler.checkPermitted(
						getPermissions(
								user,
								HasAclType.DATA_SNAPSHOT,
								CorePermDefs.CORE_MODE_NAME,
								firstOne.getDataSnapshot().getPubId(), false),
						CorePermDefs.READ,
						firstOne.getDataSnapshot().getPubId());
			}

			final Set<DataSnapshotSearchResult> searchResults = newHashSet();
			final IDataSnapshotDAO dataSnapshotDAO = daoFac
					.getDataSnapshotDAO(sessAndTrx.session);
			for (final AnalyzedDataSnapshot analyzedSnapshot : analyzedSnapshots) {
				if (extAuthzHandler.isPermitted(
						getPermissions(
								user,
								HasAclType.DATA_SNAPSHOT,
								CorePermDefs.CORE_MODE_NAME,
								analyzedSnapshot.getAnalysis().getPubId(),
								false),
						CorePermDefs.READ,
						analyzedSnapshot.getAnalysis().getPubId())) {
					final Dataset analysis = analyzedSnapshot.getAnalysis();

					long imgCnt = analysis.getImages().size();
					final long tsAnnCnt = dataSnapshotDAO.countTsAnns(analysis);

					DataSnapshotSearchResult dsSearchResult = DsSearchResultAssembler
							.assembleDsSearchResult(
									analysis,
									analysis.getTimeSeries(),
									imgCnt,
									tsAnnCnt);
					searchResults.add(dsSearchResult);
				}
			}
			logger.info(
					"{}: Returning {} related search results to user {}.",
					new Object[] { m, searchResults.size(),
							user.getUsername() });
			PersistenceUtil.commit(sessAndTrx);
			return searchResults;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public Integer getSnapshotsByUser(User user) throws AuthorizationException {
		final long inTime = new Date().getTime();
		checkNotNull(user);

		final String M = "getSnapshotsByUser(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final Integer count =
					service.getSnapshotsByUser(user);
			trx.commit();
			return count;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public Map<String, TimeSeriesDto> getTimeSeries(
			User user,
			String dsId) {
		final long inTime = System.nanoTime();

		final String M = "getTimeSeries(User, String)";
		// SessionAndTrx sessAndTrx = null;
		try {
			// sessAndTrx =
			// PersistenceUtil.getSessAndTrx(sessFac);
			// Session session = sessAndTrx.session;
			Session session = sessFac.openSession();
			Transaction trx = session.beginTransaction();

			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			Map<String, TimeSeriesDto> tsIds2FileKeys =
					service.getTimeSeries(user, dsId);

			// PersistenceUtil.commit(sessAndTrx);
			trx.commit();
			return tsIds2FileKeys;
			// } catch (RuntimeException e) {
			// PersistenceUtil.rollback(sessAndTrx);
			// throw e;
		} finally {
			// PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M, BtUtil.diffNowThenSeconds(inTime));
		}
	}

	@Override
	public Set<ToolDto> getTools(User user) {
		long in = new Date().getTime();
		final String M = "getTools(User)";

		Session session = null;
		Transaction trx = null;

		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			Set<ToolDto> tools = service.getTools(user);
			trx.commit();
			return tools;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public List<ToolDto> getTools(User user, Iterable<String> toolIds) {
		final String M = "getTools(User, Iterable)";
		long in = System.nanoTime();
		Session sess = null;
		Transaction trx = null;
		try {
			checkNotNull(user);
			checkNotNull(toolIds);
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			List<ToolDto> toolDtos = service.getTools(user, toolIds);
			trx.commit();
			return toolDtos;
		} catch (final RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowThenSeconds(in));
		}
	}

	@Override
	public List<TsAnnotationDto> getTsAnnotations(
			User user,
			String dataSnapshotId,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults) {
		long inNanos = System.nanoTime();
		String m = "getTsAnnotations(User, String, long, String, int, int)";
		SessionAndTrx sessAndTrx = null;
		try {
			checkArgument(maxResults >= 0);
			checkArgument(maxResults <= 1000);
			checkArgument(!Strings.isNullOrEmpty(dataSnapshotId));
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(dataSnapshotId);
			if (ds == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(),
							false),
					CorePermDefs.READ,
					ds.getPubId());

			ITsAnnotationDAO tsAnnDAO = daoFac.getTsAnnotationDAO(
					session,
					sessFacConfig);

			List<TsAnnotationEntity> tsAnns = null;

			tsAnns = tsAnnDAO.findByParentOrderByStartTimeAndId(
					ds,
					startOffsetUsecs,
					layer,
					firstResult,
					maxResults);

			List<TsAnnotationDto> tsAnnDtos =
					TsAnnotationAssembler.assemble(tsAnns);

			PersistenceUtil.commit(sessAndTrx);

			return tsAnnDtos;

		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : startTimeUutc {} firstResult {} maxResults {}  {} seconds",
					m,
					startOffsetUsecs,
					firstResult,
					maxResults,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public List<TsAnnotationDto> getTsAnnotationsLtStartTime(
			User user,
			String dataSnapshotId,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults) {
		long inNanos = System.nanoTime();
		String m = "getTsAnnotationsLtStartTime(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			checkArgument(!dataSnapshotId.isEmpty());
			checkArgument(maxResults >= 0);
			checkArgument(maxResults <= 1000);
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(dataSnapshotId);
			if (ds == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.READ,
					ds.getPubId());

			ITsAnnotationDAO tsAnnDAO = daoFac.getTsAnnotationDAO(
					session,
					sessFacConfig);

			List<TsAnnotationEntity> tsAnns =
					tsAnnDAO.findLtStart(
							ds,
							startOffsetUsecs,
							layer,
							firstResult,
							maxResults);

			List<TsAnnotationDto> tsAnnDtos =
					TsAnnotationAssembler.assemble(tsAnns);
			PersistenceUtil.commit(sessAndTrx);
			return tsAnnDtos;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : startTimeUutc {} firstResult {} maxResults {}  {} seconds",
					m,
					startOffsetUsecs,
					firstResult,
					maxResults,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	// @Override
	// public Set<MontagedChAnnotationDto> getMontagedChAnnotations(
	// User user,
	// LayerId layerId,
	// long startOffsetUsecs,
	// int firstResult,
	// int maxResults) {
	// long inNanos = System.nanoTime();
	// String m = "getMontagedChAnnotations(User, LayerId, long, int, int)";
	// SessionAndTrx sessAndTrx = null;
	// Integer annCount = null;
	// try {
	// checkArgument(maxResults >= 0);
	// checkArgument(maxResults <= 1000);
	// checkNotNull(layerId);
	// sessAndTrx =
	// PersistenceUtil.getSessAndTrx(sessFac);
	//
	// ILayerDAO layerDAO = daoFac.getLayerDAO(sessAndTrx.session);
	// LayerEntity layerEntity = layerDAO.findById(layerId.getId(), false);
	// if (layerEntity == null) {
	// throw new IllegalArgumentException();
	// }
	// MontageEntity montage = layerEntity.getParent();
	// DataSnapshotEntity ds = montage.getParent();
	//
	// new ExtAuthzHandler().checkPermitted(
	// getPermissions(
	// user,
	// HasAclType.DATA_SNAPSHOT,
	// CorePermDefs.CORE_MODE_NAME,
	// ds.getPubId(),
	// false),
	// CorePermDefs.READ);
	//
	// IMontagedChAnnotationDAO montagedChAnnDAO = daoFac
	// .getMontagedChAnnotationDAO(sessAndTrx.session);
	//
	// Set<MontagedChAnnotationEntity> montagedChAnnEntities = montagedChAnnDAO
	// .findByParentOrderByStartTimeAndId(
	// layerEntity,
	// startOffsetUsecs,
	// firstResult,
	// maxResults);
	// logger.debug("{}: {}", m, montagedChAnnEntities.size());
	// Set<MontagedChAnnotationDto> montagedChAnnDtos = newLinkedHashSet();
	// for (final MontagedChAnnotationEntity montagedChAnnEntity :
	// montagedChAnnEntities) {
	// Set<MontagedChId> montagedChIds = newHashSet();
	// for (final MontagedChannelEntity montagedChEntity : montagedChAnnEntity
	// .getMontagedChannels()) {
	// montagedChIds
	// .add(new MontagedChId(montagedChEntity.getId()));
	// }
	// final MontagedChAnnotationDto montagedChAnnDto = new
	// MontagedChAnnotationDto(
	// layerId,
	// montagedChAnnEntity.getType(),
	// montagedChAnnEntity.getDescription(),
	// montagedChAnnEntity.getStartOffsetUsecs(),
	// montagedChAnnEntity.getEndOffsetUsecs(),
	// montagedChAnnEntity.getColor(),
	// montagedChAnnEntity.getCreator().getId(),
	// montagedChIds,
	// new MontagedChAnnotationId(montagedChAnnEntity.getId()));
	// montagedChAnnDtos.add(montagedChAnnDto);
	// }
	// PersistenceUtil.commit(sessAndTrx);
	// annCount = montagedChAnnDtos.size();
	// return montagedChAnnDtos;
	//
	// } catch (RuntimeException e) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw e;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// logger.info(
	// "{} : startTimeUutc {} firstResult {} maxResults {} annCount {} {} seconds",
	// m,
	// startOffsetUsecs,
	// firstResult,
	// maxResults,
	// annCount,
	// BtUtil.diffNowThenSeconds(inNanos));
	// }
	// }
	//
	// @Override
	// public Set<MontagedChAnnotationDto> getMontagedChAnnotationsLtStartTime(
	// User user,
	// LayerId layerId,
	// long startOffsetUsecs,
	// int firstResult,
	// int maxResults) {
	// long inNanos = System.nanoTime();
	// String m = "getMontagedChAnnotationsLtStartTime(...)";
	// SessionAndTrx sessAndTrx = null;
	// Integer annCount = null;
	// try {
	// checkNotNull(layerId);
	// checkArgument(maxResults >= 0);
	// checkArgument(maxResults <= 1000);
	// sessAndTrx =
	// PersistenceUtil.getSessAndTrx(sessFac);
	// ILayerDAO layerDAO = daoFac.getLayerDAO(sessAndTrx.session);
	// LayerEntity layerEntity = layerDAO.findById(layerId.getId(), false);
	// if (layerEntity == null) {
	// throw new IllegalArgumentException();
	// }
	// final DataSnapshotEntity ds = layerEntity.getParent().getParent();
	//
	// new ExtAuthzHandler().checkPermitted(
	// getPermissions(
	// user,
	// HasAclType.DATA_SNAPSHOT,
	// CorePermDefs.CORE_MODE_NAME,
	// ds.getPubId(),
	// false),
	// CorePermDefs.READ);
	//
	// IMontagedChAnnotationDAO montagedChAnnDAO = daoFac
	// .getMontagedChAnnotationDAO(sessAndTrx.session);
	//
	// Set<MontagedChAnnotationEntity> montagedChAnnEntities =
	// montagedChAnnDAO.findLtStart(
	// layerEntity,
	// startOffsetUsecs,
	// firstResult,
	// maxResults);
	//
	// final Set<MontagedChAnnotationDto> montagedChAnnDtos =
	// newLinkedHashSet();
	// for (final MontagedChAnnotationEntity montagedChAnnEntity :
	// montagedChAnnEntities) {
	// Set<MontagedChId> montagedChIds = newLinkedHashSet();
	// for (final MontagedChannelEntity montagedChEntity : montagedChAnnEntity
	// .getMontagedChannels()) {
	// montagedChIds
	// .add(new MontagedChId(montagedChEntity.getId()));
	// }
	// final MontagedChAnnotationDto montagedChAnnDto = new
	// MontagedChAnnotationDto(
	// layerId,
	// montagedChAnnEntity.getType(),
	// montagedChAnnEntity.getDescription(),
	// montagedChAnnEntity.getStartOffsetUsecs(),
	// montagedChAnnEntity.getEndOffsetUsecs(),
	// montagedChAnnEntity.getColor(),
	// montagedChAnnEntity.getCreator().getId(),
	// montagedChIds,
	// new MontagedChAnnotationId(montagedChAnnEntity.getId()));
	// montagedChAnnDtos.add(montagedChAnnDto);
	// }
	//
	// PersistenceUtil.commit(sessAndTrx);
	// annCount = montagedChAnnDtos.size();
	// return montagedChAnnDtos;
	// } catch (RuntimeException e) {
	// PersistenceUtil.rollback(sessAndTrx);
	// throw e;
	// } finally {
	// PersistenceUtil.close(sessAndTrx);
	// logger.info(
	// "{} : startTimeUutc {} firstResult {} maxResults {} results {} {} seconds",
	// m,
	// startOffsetUsecs,
	// firstResult,
	// maxResults,
	// annCount,
	// BtUtil.diffNowThenSeconds(inNanos));
	// }
	// }

	@Override
	public UserEntity getUser(UserId userId) {
		long startTimeNanos = System.nanoTime();
		String m = "getUser(UserId)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			UserEntity user = service.getUser(userId);
			PersistenceUtil.commit(sessAndTrx);
			return user;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public String getUserClob(final User user) {
		final long inTime = new Date().getTime();
		checkNotNull(user);
		final String M = "getUserClob(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			String clob = service.getUserClob(user);
			PersistenceUtil.commit(sessAndTrx);
			return clob;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M,
					(new Date().getTime() - inTime) / 1000.0);
		}
	}

	@Override
	public UserIdSessionStatus getUserIdSessionStatus(SessionToken token,
			Date minLastActive) {
		long inNanos = System.nanoTime();
		String m = "getUserIdSessionStatus(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			IDataSnapshotService service = dsServiceFac.newInstance(
					session,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			UserIdSessionStatus userIdSessionStatus = service
					.getUserIdSessionStatus(token, minLastActive);
			PersistenceUtil.commit(sessAndTrx);
			return userIdSessionStatus;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			timeLogger.debug(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public List<UserId> getUserIdsWithActiveJobs() {
		Date in = new Date();
		final String M = "getUsersWithActiveJobs(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			final List<UserId> users = service.getUserIdsWithActiveJobs();
			trx.commit();
			return users;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public void initializeJob(String datasetId, Set<TaskDto> tasks) {
		Date in = new Date();
		final String M = "initializeJob(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.initializeJob(datasetId, tasks);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public boolean isFrozen(User user, String dsId)
			throws AuthorizationException {
		Date in = new Date();
		final String M = "isFrozen(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			boolean frozen = service.isFrozen(user, dsId);
			trx.commit();
			return frozen;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info("{}: {} seconds", M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public int moveTsAnnotations(
			User user,
			String datasetId,
			String name,
			String newName) {
		String m = "setLayerName(...)";
		long inNanos = System.nanoTime();
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			IDataSnapshotDAO dataSnapshotDAO =
					daoFac.getDataSnapshotDAO(sessAndTrx.session);

			DataSnapshotEntity dataSnapshot =
					dataSnapshotDAO.findByNaturalId(datasetId);
			if (dataSnapshot == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dataSnapshot.getPubId(), false),
					CorePermDefs.EDIT,
					dataSnapshot.getPubId());

			ITsAnnotationDAO tsAnnotationDAO =
					daoFac.getTsAnnotationDAO(
							sessAndTrx.session,
							sessFacConfig);

			int changed = tsAnnotationDAO.setLayerName(dataSnapshot, name,
					newName);
			PersistenceUtil.commit(sessAndTrx);
			return changed;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : renamed layer from {} to {} {} seconds",
					new Object[] {
							m,
							name,
							newName,
							BtUtil.diffNowThenSeconds(inNanos)
					});
		}
	}

	@Override
	public String removeTimeSeriesFromDataset(
			final User user,
			final String dataSnapshotRevId,
			final Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dataSnapshotRevId);
		checkNotNull(timeSeriesRevIds);
		final String M = "removeTimeSeries(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final String revId =
					service.removeTimeSeriesFromDataset(
							user,
							dataSnapshotRevId,
							timeSeriesRevIds);
			trx.commit();
			return revId;
		} catch (DatasetNotFoundException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);

		}
	}

	@Override
	public void removeTools(User user, Set<String> revIds)
			throws AuthorizationException {
		long in = new Date().getTime();
		final String M = "removeTools(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			service.removeTools(user, revIds);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);

		}
	}

	@Override
	public String removeTsAnnotations(
			final User user,
			final String dataSnapshotRevId,
			final Set<String> tsAnnotationRevIds)
			throws AuthorizationException {
		final long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dataSnapshotRevId);
		checkNotNull(tsAnnotationRevIds);

		String M = "removeTsAnnotations(...)";

		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service = dsServiceFac
					.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			final String revId = service.removeTsAnnotations(user,
					dataSnapshotRevId, tsAnnotationRevIds);
			trx.commit();
			return revId;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public int removeTsAnnotationsByLayer(
			User user,
			String datasetId,
			String layer) {
		long inNanos = System.nanoTime();
		String m = "removeTsAnnotationsByLayer(...)";
		SessionAndTrx sessAndTrx = null;
		int deleted = -1;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(datasetId);
			if (ds == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.EDIT,
					ds.getPubId());
			ITsAnnotationDAO tsAnnotationDAO =
					daoFac.getTsAnnotationDAO(
							session,
							sessFacConfig);
			deleted = tsAnnotationDAO.deleteByLayer(ds, layer);
			PersistenceUtil.commit(sessAndTrx);
			return deleted;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : deleted {} annotations {} seconds",
					m,
					deleted,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public void setDataSnapshotName(
			User user,
			String dataSnapshotId,
			String originalName,
			String newName) throws StaleDataException, DuplicateNameException {
		long inNanos = System.nanoTime();
		String m = "setDataSnapshotName(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(dataSnapshotId);
			if (ds == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.EDIT,
					ds.getPubId());

			DataSnapshotEntity dupCheck = dsDAO.findByLabel(newName);

			if (dupCheck != null) {
				throw new DuplicateNameException(newName);
			}

			if (!ds.getLabel().trim().equals(originalName.trim())) {
				throw new StaleDataException("snapshot " + dataSnapshotId
						+ " has name "
						+ "[" + ds.getLabel().trim()
						+ "] but you passed in original name ["
						+ originalName.trim() + "]");
			}
			ds.setLabel(newName.trim());
			PersistenceUtil.commit(sessAndTrx);
		} catch (StaleDataException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public void setFrozen(User user, String dsId, boolean frozen)
			throws AuthorizationException {
		Date in = new Date();
		final String M = "setFrozen(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.setFrozen(user, dsId, frozen);
			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public void setSessionStatus(SessionToken token, SessionStatus status) {
		checkNotNull(token);
		checkNotNull(status);
		long inNanos = System.nanoTime();
		String m = "setSessionStatus(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;

			IDataSnapshotService service = dsServiceFac.newInstance(
					session,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.setSessionStatus(token, status);
			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Nonnull
	@Override
	public String storeClob(
			User user,
			String dataSnapshotRevId,
			String clob)
			throws AuthorizationException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dataSnapshotRevId);
		checkNotNull(clob);
		final String M = "storeClob(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(
							session,
							MAX_DS_TS_ANNS,
							dataSnapshotShortCache,
							searchResultCache);

			final String revId = service.storeClob(
					user,
					dataSnapshotRevId,
					clob);
			trx.commit();
			return revId;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Nonnull
	@Override
	public String storeJsonKeyValues(
			final User user,
			final String dataSnapshotRevId,
			final Collection<? extends IJsonKeyValue> keyValues) {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(dataSnapshotRevId);
		checkNotNull(keyValues);

		return stringTransaction.execTransaction("storeJsonKeyValues",
				new TransAction<String>() {

					@Override
					public String execute(Session session) {
						final IDataSnapshotService service =
								dsServiceFac.newInstance(
										session,
										MAX_DS_TS_ANNS,
										dataSnapshotShortCache,
										searchResultCache);

						try {
							return service.storeJsonKeyValues(
									user,
									dataSnapshotRevId,
									keyValues);
						} catch (JsonProcessingException e) {
							throw new RuntimeException(e);
						}
					}

				});
		// final String M = "storeJsonKeyValues(...)";
		// Session session = null;
		// Transaction trx = null;
		// try {
		// session = sessFac.openSession();
		// trx = session.beginTransaction();
		// final IDataSnapshotService service =
		// dsServiceFac.newInstance(
		// session,
		// MAX_DS_TS_ANNS,
		// dataSnapshotShortCache,
		// searchResultCache);
		//
		// final String revId = service.storeJsonKeyValues(
		// user,
		// dataSnapshotRevId,
		// keyValues);
		// trx.commit();
		// return revId;
		// } catch (RuntimeException re) {
		// PersistenceUtil.rollback(trx);
		// throw re;
		// } finally {
		// PersistenceUtil.close(session);
		// logger.info(
		// "{} : {} seconds",
		// M, (new Date().getTime() - in) / 1000.0);
		// }
	}

	@Override
	public List<String> storeTools(User user, Iterable<ToolDto> tools)
			throws AuthorizationException {
		long in = new Date().getTime();
		Session session = null;
		Transaction trx = null;
		final String M = "storeTools(...)";
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			List<String> revIds = service.storeTools(user, tools);
			trx.commit();
			return revIds;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);

		}
	}

	@Override
	public DataSnapshotIds storeTsAnnotations(
			User user,
			String dataSnapshotRevId,
			Iterable<? extends TsAnnotationDto> tsAnnDtos)
			throws
			AuthorizationException, TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException {
		long inNanos = System.nanoTime();
		String m = "storeTsAnnotations(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotService service = dsServiceFac
					.newInstance(
							session,
							MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			DataSnapshotIds ids =
					service.storeTsAnnotations(
							user,
							dataSnapshotRevId,
							tsAnnDtos);
			PersistenceUtil.commit(sessAndTrx);
			return ids;
		} catch (TimeSeriesNotFoundInDatasetException tse) {
			PersistenceUtil.rollback(sessAndTrx);
			throw tse;
		} catch (BadTsAnnotationTimeException bte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw bte;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public void storeUserClob(
			User user,
			String clob) {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(clob);
		final String M = "storeUserClob(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			service.storeUserClob(user, clob);
			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(sessAndTrx);
			throw re;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public void updateProject(User admin, String projectId,
			IProjectUpdater updater)
			throws AuthorizationException {
		Date in = new Date();
		final String M = "updateProject(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.updateProject(admin, projectId, updater);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public void updateTasks(User user, String datasetId,
			Set<TaskDto> taskSet) {
		Date in = new Date();
		final String M = "updateTasks(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.updateTasks(user, datasetId, taskSet);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public void updateTaskStatus(User user, String datasetId,
			String taskId, TaskStatus newStatus) {
		Date in = new Date();
		final String M = "updateTaskStatus(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			service.updateTaskStatus(user, datasetId, taskId, newStatus);
			trx.commit();
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}

	}

	@Override
	public List<ChannelInfoDto> writeChannelInfo(String snapshotId,
			List<ChannelInfoDto> info) {
		Date in = new Date();
		final String M = "writeChannelInfo(...)";
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessFac.openSession();
			trx = sess.beginTransaction();
			IDataSnapshotService service = dsServiceFac.newInstance(sess,
					MAX_DS_TS_ANNS, dataSnapshotShortCache, searchResultCache);
			List<ChannelInfoDto> ret = service.writeChannelInfos(snapshotId,
					info);
			trx.commit();
			return ret;
		} catch (Exception t) {
			PersistenceUtil.rollback(trx);
			throw propagate(t);
		} finally {
			PersistenceUtil.close(sess);
			logger.info(
					"{}: {} seconds",
					M,
					diffNowMsThenSeconds(in));
		}
	}

	@Override
	public void streamAnnotations(
			User user,
			DataSnapshotId datasetId,
			IAnnotationWriter annotationWriter) {
		long inNanos = System.nanoTime();
		String m = "streamAnnotations(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);

			Session session = sessAndTrx.session;

			IDataSnapshotDAO dsDAO = daoFac.getDataSnapshotDAO(session);
			ITsAnnotationDAO tsAnnotationDAO =
					daoFac.getTsAnnotationDAO(session, sessFacConfig);
			DataSnapshotEntity ds = dsDAO.findByNaturalId(datasetId.getId());
			if (ds == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getPubId(), false),
					CorePermDefs.READ,
					ds.getPubId());

			tsAnnotationDAO.streamTsAnnotations(
					ds,
					annotationWriter);

			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public Set<DataSnapshotSearchResult> getSearchableDatasetsThatExist(
			User user,
			String projectId) {
		long inNanos = System.nanoTime();
		String m = "getSearchableDatasetsThatExist(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			Set<DataSnapshotSearchResult> searchResults = getProjectDatasetsForUser(
					user,
					projectId,
					session);
			PersistenceUtil.commit(sessAndTrx);
			return searchResults;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public Map<String, Set<DataSnapshotSearchResult>> getSearchableDatasetsThatExist(
			User user,
			Set<String> projectIds) {
		long inNanos = System.nanoTime();
		String m = "getSearchableDatasetsThatExist(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final Map<String, Set<DataSnapshotSearchResult>> projectIdToSearchResults = newHashMap();
			for (String projectId : projectIds) {
				Set<DataSnapshotSearchResult> searchResults = getProjectDatasetsForUser(
						user,
						projectId,
						session);
				projectIdToSearchResults.put(projectId, searchResults);
			}
			PersistenceUtil.commit(sessAndTrx);
			return projectIdToSearchResults;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	private Set<DataSnapshotSearchResult> getProjectDatasetsForUser(
			User user,
			String projectId,
			Session session) throws AssertionError {
		final long startNanos = System.nanoTime();
		final String m = "getProjectDatasetsForUser(...)";
		IProjectDAO projectDAO = daoFac.getProjectDAO(session);
		IDataSnapshotDAO dataSnapshotDAO = daoFac
				.getDataSnapshotDAO(session);
		IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
		IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
		IImageDAO imageDAO = daoFac.getImageDAO();
		ITimeSeriesDAO timeSeriesDAO = daoFac.getTimeSeriesDAO(session);

		ProjectEntity projectEntity = projectDAO.findByNaturalId(projectId);
		if (projectEntity == null) {
			throw new IllegalArgumentException(
					"no such project [" + projectId + "]");
		}

		Set<DataSnapshotSearchResult> searchResults =
				newLinkedHashSet();

		for (DataSnapshotEntity ds : projectEntity.getSnapshots()) {
			boolean isReadPermitted = AuthCheckFactory.getHandler()
					.isPermitted(
							getPermissions(
									user,
									HasAclType.DATA_SNAPSHOT,
									CorePermDefs.CORE_MODE_NAME,
									ds.getPubId(), false),
							CorePermDefs.READ,
							ds.getPubId());
			if (isReadPermitted) {
				if (!HibernateUtil.isInstanceOf(ds, IHasRecording.class)) {
					logger.debug("{}: it's not a recording", m);
					DataSnapshotSearchResult dsSearchResult = DsSearchResultAssembler
							.assembleDsSearchResult(
									ds,
									ds.getTimeSeries(),
									-1, // images are on the way out
									-1); // current use case doesn't need
											// annotations
					searchResults.add(dsSearchResult);
				} else {
					logger.debug("{}: it is a recording.", m);
					IHasRecording rec = null;
					if (HibernateUtil
							.isInstanceOf(
									ds,
									ExperimentEntity.class)) {
						ExperimentEntity experiment =
								experimentDAO.load(ds.getId());
						rec = experiment;
					} else if (HibernateUtil
							.isInstanceOf(
									ds,
									EegStudy.class)) {
						EegStudy study =
								studyDAO.load(ds.getId());
						rec = study;
					}

					if (rec == null) {
						throw new AssertionError();
					}

					SearchResultCacheEntry searchResultCacheEntry =
							searchResultCache.getIfPresent(ds.getId());

					Set<TimeSeriesEntity> ts = newHashSet();

					if (searchResultCacheEntry == null) {
						int imgCnt = Ints.checkedCast(
								imageDAO.countImagesRecording(rec
										.getRecording()));

						int tsCount =
								timeSeriesDAO.countByRecording(rec
										.getRecording());

						int tsAnnCnt = Ints.checkedCast(
								dataSnapshotDAO.countTsAnns(ds));

						searchResultCacheEntry =
								new SearchResultCacheEntry(
										imgCnt,
										tsCount,
										tsAnnCnt,
										rec.getOrganization().getName());
						searchResultCache.put(
								ds.getId(),
								searchResultCacheEntry);
					}

					for (long i = 0; i < searchResultCacheEntry
							.getTsCount(); i++) {
						TimeSeriesEntity tsEntity = new TimeSeriesEntity();
						tsEntity.setPubId("dummy-pubId-" + i);
						tsEntity.setLabel("dummy-label-" + i);
						ts.add(tsEntity);
					}

					DataSnapshotSearchResult dsSearchResult =
							DsSearchResultAssembler
									.assembleDsSearchResult(
											ds,
											ts,
											searchResultCacheEntry
													.getImageCount(),
											searchResultCacheEntry
													.getTsAnnCount(),
											rec.getRecording()
													.getStartTimeUutc(),
											rec.getRecording()
													.getEndTimeUutc(),
											searchResultCacheEntry
													.getOrganizationName());
					searchResults.add(dsSearchResult);
				}
			}
		}
		logger.info("{}: project: {}, total {} seconds",
				m,
				projectEntity.getName(),
				BtUtil.diffNowThenSeconds(startNanos));
		return searchResults;
	}

	@Override
	public DataSnapshotId getRecordingIdForTimeSeries(
			User user,
			String timeSeriesId) throws TimeSeriesNotFoundException {
		long startTimeNanos = System.nanoTime();
		final String m = "getRecordingIdForTimeSeries(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			ITimeSeriesDAO timeSeriesDAO = daoFac
					.getTimeSeriesDAO(sessAndTrx.session);
			TimeSeriesEntity timeSeries = timeSeriesDAO
					.findByNaturalId(timeSeriesId);
			if (timeSeries == null) {
				throw new TimeSeriesNotFoundException(
						"could not find time series ["
								+ timeSeriesId
								+ "]");
			}
			IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
			EegStudy study = studyDAO.findByTimeSeries(timeSeries);
			String datasetId = null;
			if (study != null) {
				datasetId = study.getPubId();
			} else {
				IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
				ExperimentEntity experiment = experimentDAO
						.findByTimeSeries(timeSeries);
				if (experiment == null) {
					throw new TimeSeriesNotFoundInAnyRecordingException(
							timeSeriesId);
				} else {
					datasetId = experiment.getPubId();
				}
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							datasetId,
							false),
					CorePermDefs.READ,
					datasetId);
			PersistenceUtil.commit(sessAndTrx);
			return new DataSnapshotId(datasetId);
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

	@Override
	public String addMEF(User user, String mefPath,
			ChannelInfoDto chInfoDto)
			throws AuthorizationException {
		long inNanos = System.nanoTime();
		checkNotNull(user);
		String m = "addMEFPath(User, String, ChannelInfoDt)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			final IDataSnapshotService service = dsServiceFac
					.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			String tsId = service.addMEFPath(
					user,
					mefPath, chInfoDto);
			PersistenceUtil.commit(sessAndTrx);
			return tsId;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(inNanos));
		}
	}

	@Override
	public String createDataSnapshot(User user, String containerName,
			IHasGwtRecording recInfo)
			throws AuthorizationException {
		long in = new Date().getTime();
		checkNotNull(user);

		final String M = "createDataSnapshot(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			String ret = service.createSnapshot(user, containerName, recInfo);
			trx.commit();
			return ret;
		} catch (RuntimeException t) {
			PersistenceUtil.rollback(trx);
			throw t;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public RecordingObject createRecordingObject(
			User user,
			RecordingObject recordingObject,
			InputStream inputStream,
			boolean test) throws
			RecordingNotFoundException,
			BadRecordingObjectNameException,
			DuplicateNameException,
			BadDigestException {
		final long startNanos = System.nanoTime();
		double writeSecs = -1;
		final String m = "createRecordingObject(...)";
		checkNotNull(user);
		checkNotNull(recordingObject);
		checkNotNull(inputStream);
		SessionAndTrx sessAndTrx = null;
		IRecObjectUploadResult result = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dataSnapshotDAO =
					daoFac.getDataSnapshotDAO(session);
			DataSnapshotId dataSnapshotId = new DataSnapshotId(
					recordingObject.getDatasetId());
			DataSnapshotEntity dsEntity =
					dataSnapshotDAO.findByNaturalId(dataSnapshotId.getId());

			if (dsEntity == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.EDIT,
					dsEntity.getPubId());
			if (recordingObject.getId() != null) {
				throw new IllegalArgumentException();
			}

			Optional<Recording> recordingOpt = getRecording(dsEntity);
			if (!recordingOpt.isPresent()) {
				throw new RecordingNotFoundException(dataSnapshotId);
			}
			Recording recording = recordingOpt.get();
			String simpleName = recordingObject.getName();
			File nameTestFile = new File(simpleName);
			if (!simpleName.equals(nameTestFile.getName())) {
				throw new BadRecordingObjectNameException(
						"Illegal recording object name: ["
								+ simpleName
								+ "]. Name must be a simple name with no path components.");
			}
			Set<RecordingObjectEntity> existingObjects = recording.getObjects();
			for (RecordingObjectEntity existingObject : existingObjects) {
				if (existingObject.getFileName().equals(
						simpleName)) {
					throw new DuplicateNameException(
							simpleName);
				}
			}
			if (test) {
				// Everything we could test without reading from the input
				// stream is okay, so test passed. Return original object with
				// no id to show nothing was created.
				return recordingObject;
			} else {
				IUserDAO userDAO = daoFac.getUserDAO(session);
				UserEntity creator = userDAO.findById(
						user.getUserId(),
						false);
				final String fileKey = recording.getDir() + "/objects/"
						+ simpleName;
				RecordingObjectEntity recordingObjectEntity = new RecordingObjectEntity(
						recording,
						fileKey,
						recordingObject.getInternetMediaType(),
						recordingObject.getSizeBytes(),
						recordingObject.getMd5Hash(),
						creator);
				String description = recordingObject.getDescription();
				if (description != null) {
					recordingObjectEntity.setDescription(description);
				}
				recording.getObjects().add(recordingObjectEntity);
				session.flush();
				final long startWriteNanos = System.nanoTime();
				result = recObjProvider.writeRecordingObject(
						inputStream,
						recordingObjectEntity.getFileName(),
						recordingObject.getMd5Hash(),
						recordingObject.getSizeBytes(),
						recordingObject.getInternetMediaType());
				writeSecs = BtUtil.diffNowThenSeconds(startWriteNanos);
				PersistenceUtil.commit(sessAndTrx);

				RecordingObject newRecordingObject = new RecordingObject(
						simpleName,
						recordingObjectEntity.getMd5Hash(),
						recordingObjectEntity.getSizeBytes(),
						user.getUsername(),
						recordingObjectEntity.getInternetMediaType(),
						dataSnapshotId.getId(),
						recordingObjectEntity.getId(),
						recordingObjectEntity.getVersion().toString(),
						recordingObjectEntity.getCreateTime());
				description = recordingObjectEntity.getDescription();
				if (description != null) {
					newRecordingObject.setDescription(description);
				}

				return newRecordingObject;
			}
		} catch (BadDigestException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RecordingNotFoundException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (BadRecordingObjectNameException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (DuplicateNameException e) {
			PersistenceUtil.rollback(sessAndTrx);
			if (result != null) {
				result.deleteObject();
			}
			throw e;
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			if (result != null) {
				result.deleteObject();
			}
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : test = {}, write: {} seconds, total: {} seconds",
					m,
					test,
					writeSecs,
					BtUtil.diffNowThenSeconds(startNanos));
		}
	}

	private Optional<Recording> getRecording(DataSnapshotEntity dataset) {
		Optional<Recording> recordingOpt = Optional.absent();

		if (HibernateUtil.isInstanceOf(
				dataset,
				EegStudy.class)) {
			IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
			EegStudy study = studyDAO.load(dataset.getId());
			recordingOpt = Optional.of(study.getRecording());
		} else if (HibernateUtil.isInstanceOf(
				dataset,
				ExperimentEntity.class)) {
			IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
			ExperimentEntity experiment = experimentDAO.load(dataset.getId());
			recordingOpt = Optional.of(experiment.getRecording());
		}
		return recordingOpt;
	}

	private DataSnapshotEntity getDataSnapshotFromRecording(Recording recording) {
		checkNotNull(recording);
		IEegStudyDAO studyDAO = daoFac.getEegStudyDAO();
		EegStudy study = studyDAO.findByRecording(recording);
		if (study != null) {
			return study;
		} else {
			IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
			ExperimentEntity experiment = experimentDAO
					.findByRecording(recording);
			if (experiment == null) {
				throw new IllegalStateException("Recording "
						+ recording.getId() + " has no parent");
			}
			return experiment;
		}
	}

	@Override
	public Set<RecordingObject> getRecordingObjects(
			User user,
			DataSnapshotId datasetId) {
		final long startNanos = System.nanoTime();
		final String m = "getRecordingObjects(...)";
		checkNotNull(user);
		checkNotNull(datasetId);
		SessionAndTrx sessAndTrx = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dataSnapshotDAO =
					daoFac.getDataSnapshotDAO(session);
			DataSnapshotEntity dsEntity =
					dataSnapshotDAO.findByNaturalId(datasetId.getId());

			if (dsEntity == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.READ,
					dsEntity.getPubId());

			Set<RecordingObject> objects = newTreeSet(new edu.upenn.cis.db.mefview.shared.IHasName.NameComparator());
			Optional<Recording> recordingOpt = getRecording(dsEntity);
			if (!recordingOpt.isPresent()) {
				logger.debug("{}: No objects. Dataset {} is not a recording",
						m, dsEntity.getLabel());
			} else {
				Recording recording = recordingOpt.get();

				for (final RecordingObjectEntity objEntity : recording
						.getObjects()) {
					User creator = userService.findUserByUid(objEntity
							.getCreator()
							.getId());
					final File path = new File(objEntity.getFileName());
					RecordingObject object = new RecordingObject(
							path.getName(),
							objEntity.getMd5Hash(),
							objEntity.getSizeBytes(),
							(creator != null) ? creator.getUsername() : "",
							objEntity.getInternetMediaType(),
							datasetId.getId(),
							objEntity.getId(),
							objEntity.getVersion().toString(),
							objEntity.getCreateTime());
					if (objEntity.getDescription() != null) {
						object.setDescription(objEntity.getDescription());
					}
					logger.debug("{}: Returning {}", m, object);
					objects.add(object);
				}
			}
			PersistenceUtil.commit(sessAndTrx);

			return objects;
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{}: {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startNanos));
		}

	}

	@Override
	public void deleteRecordingObject(
			User user,
			RecordingObjectIdAndVersion idAndVersion,
			boolean checkVersion)
			throws FailedVersionMatchException,
			RecordingObjectNotFoundException {
		long startTimeNanos = System.nanoTime();
		final String m = "deleteRecordingObject(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IRecordingObjectDAO recObjectDAO =
					daoFac.getRecordingObjectDAO(sessAndTrx.session);
			RecordingObjectEntity recordingObject = recObjectDAO
					.get(idAndVersion.getId());
			if (recordingObject == null) {
				throw new RecordingObjectNotFoundException(
						idAndVersion.asRecordingObjectId());
			}
			Recording recording = recordingObject.getParent();

			DataSnapshotEntity dsEntity = getDataSnapshotFromRecording(recording);

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.EDIT,
					dsEntity.getPubId());

			if (checkVersion
					&& !recordingObject.getVersion().equals(
							idAndVersion.getVersion())) {
				throw new FailedVersionMatchException(
						"recording object version does not match, refresh");
			}
			recording.getObjects().remove(recordingObject);
			PersistenceUtil.commit(sessAndTrx);
			recObjProvider.deleteRecordingObject(recordingObject);
			return;
		} catch (FailedVersionMatchException fvme) {
			PersistenceUtil.rollback(sessAndTrx);
			throw fvme;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}
	}

	@Override
	public IStoredObjectReference getRecordingObjectHandle(
			User user,
			RecordingObject object)
			throws
			RecordingObjectNotFoundException {
		long startTimeNanos = System.nanoTime();
		final String m = "getRecordingObjectContent(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IRecordingObjectDAO recObjectDAO =
					daoFac.getRecordingObjectDAO(sessAndTrx.session);
			RecordingObjectEntity recordingObject = recObjectDAO
					.get(object.getId());
			Recording recording = recordingObject.getParent();

			DataSnapshotEntity dsEntity = getDataSnapshotFromRecording(recording);

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.READ,
					dsEntity.getPubId());

			IStoredObjectReference ref =
					new FileReference(
							null,
							recordingObject.getFileName(),
							recordingObject.getFileName());
			PersistenceUtil.commit(sessAndTrx);

			return ref;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

	@Override
	public RecordingObjectContent getRecordingObjectContent(
			User user,
			RecordingObjectIdAndVersion idAndVersion,
			boolean checkVersion,
			Long startByteOffset,
			Long endByteOffset)
			throws FailedVersionMatchException,
			RecordingObjectNotFoundException,
			InvalidRangeException {
		long startTimeNanos = System.nanoTime();
		final String m = "getRecordingObjectContent(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IRecordingObjectDAO recObjectDAO =
					daoFac.getRecordingObjectDAO(sessAndTrx.session);
			RecordingObjectEntity recordingObject = recObjectDAO
					.get(idAndVersion.getId());
			if (recordingObject == null) {
				throw new RecordingObjectNotFoundException(
						idAndVersion.asRecordingObjectId());
			}
			Recording recording = recordingObject.getParent();

			DataSnapshotEntity dsEntity = getDataSnapshotFromRecording(recording);

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.READ,
					dsEntity.getPubId());

			if (checkVersion
					&& !recordingObject.getVersion().equals(
							idAndVersion.getVersion())) {
				throw new FailedVersionMatchException(
						"recording object version does not match, refresh");
			}
			final InputStream objectInputStream = recObjProvider
					.getRecordingObject(
							recordingObject.getFileName(),
							startByteOffset,
							endByteOffset);
			final File keyAsPath = new File(recordingObject.getFileName());
			RecordingObjectContent content = new RecordingObjectContent(
					new DataSnapshotId(dsEntity.getPubId()),
					objectInputStream,
					keyAsPath.getName(),
					recordingObject.getInternetMediaType(), recordingObject
							.getVersion().toString());
			PersistenceUtil.commit(sessAndTrx);

			return content;
		} catch (FailedVersionMatchException fvme) {
			PersistenceUtil.rollback(sessAndTrx);
			throw fvme;
		} catch (InvalidRangeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

	@Override
	public void addAsRecordingObject(User user,
			String key,
			String description,
			String mimeType,
			int size,
			String snapshotId) throws RecordingNotFoundException,
			DuplicateNameException {

		final String m = "addAsRecordingObject(...)";
		checkNotNull(user);
		checkNotNull(key);
		checkNotNull(snapshotId);
		SessionAndTrx sessAndTrx = null;

		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IDataSnapshotDAO dataSnapshotDAO =
					daoFac.getDataSnapshotDAO(session);
			DataSnapshotId dataSnapshotId = new DataSnapshotId(
					snapshotId);
			DataSnapshotEntity dsEntity =
					dataSnapshotDAO.findByNaturalId(dataSnapshotId.getId());

			if (dsEntity == null) {
				throw new DataSnapshotNotFoundException(dataSnapshotId);
			}

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.EDIT,
					dsEntity.getPubId());

			Optional<Recording> recordingOpt = getRecording(dsEntity);
			if (!recordingOpt.isPresent()) {
				throw new RecordingNotFoundException(dataSnapshotId);
			}
			Recording recording = recordingOpt.get();
			// If no objects have been added yet, set Recording.dir based on
			// this object.
			if (recording.getContactGroups().isEmpty()
					&& recording.getObjects().isEmpty()) {
				final int lastSlashIdx = key.lastIndexOf("/");
				if (lastSlashIdx > 0) {
					final String recDir = key
							.substring(0,
									lastSlashIdx);
					final String oldRecDir = recording.getDir();
					recording.setDir(recDir);
					logger.info(
							"{}: Changing Recording directory from {} to {}",
							m,
							oldRecDir,
							recDir);
				}
			}

			Set<RecordingObjectEntity> existingObjects = recording.getObjects();
			for (RecordingObjectEntity existingObject : existingObjects) {

				// If this already exists, update it
				if (existingObject.getFileName().equals(key)) {
					existingObject.setInternetMediaType(mimeType);
					if (description != null) {
						existingObject.setDescription(description);
					}

					dataSnapshotDAO.saveOrUpdate(dsEntity);
					PersistenceUtil.commit(sessAndTrx);
					return;
				}
			}

			// Doesn't exist
			IUserDAO userDAO = daoFac.getUserDAO(session);
			UserEntity creator = userDAO.findById(
					user.getUserId(),
					false);
			RecordingObjectEntity recordingObjectEntity = new RecordingObjectEntity(
					recording,
					key,
					mimeType,
					(long) size,
					"",
					creator);

			if (description != null) {
				recordingObjectEntity.setDescription(description);
			}
			recording.getObjects().add(recordingObjectEntity);
			dataSnapshotDAO.saveOrUpdate(dsEntity);
			PersistenceUtil.commit(sessAndTrx);
		} catch (RecordingNotFoundException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} done", m);
		}
	}

	@Override
	public String addTimeSeriesToDataSnapshot(User user, String datasetRevId,
			List<ChannelInfoDto> channelInfo, List<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(datasetRevId);
		checkNotNull(timeSeriesRevIds);
		final String M = "addTimeSeriesToDataSnapshot(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			final String revId =
					service.addTimeSeriesToSnapshot(
							user,
							datasetRevId,
							channelInfo,
							timeSeriesRevIds);
			trx.commit();
			return revId;
		} catch (DatasetNotFoundException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public void setAdmissionInfo(User user, String snapshotId,
			GwtPatient origPatient,
			int ageAtAdmission) throws AuthorizationException,
			DatasetNotFoundException {
		long in = new Date().getTime();
		checkNotNull(user);
		checkNotNull(snapshotId);
		final String M = "addTimeSeriesToDataSnapshot(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);

			service.setAdmissionInfo(user, snapshotId, origPatient,
					ageAtAdmission);
			trx.commit();
		} catch (DatasetNotFoundException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public ControlFileRegistration createControlFileRegistration(
			User user,
			ControlFileRegistration registration)
			throws AuthorizationException,
			DuplicateNameException {
		final long startNanos = System.nanoTime();
		final String m = "createControlFileRegistration(...)";
		checkNotNull(user);
		checkNotNull(registration);
		SessionAndTrx sessAndTrx = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			Session session = sessAndTrx.session;
			IUserDAO userDAO = daoFac.getUserDAO(session);
			UserEntity creator = userDAO.findById(
					user.getUserId(),
					false);
			// Remove leading and trailing slashes and backslashes.
			final String cleanedFileKey = registration
					.getFileKey()
					.replaceAll(
							"^[\\\\/]+|[\\\\/]+$", "");
			IControlFileDAO controlFileDAO = daoFac.getControlFileDAO(session);
			final ControlFileEntity existingRegistration = controlFileDAO
					.findByBucketFileKey(
							registration.getBucket(),
							registration.getFileKey());
			if (existingRegistration != null) {
				throw new DuplicateNameException(
						registration.getBucket()
								+ "/"
								+ cleanedFileKey);
			}
			final ControlFileEntity controlFile = new ControlFileEntity(
					registration.getBucket(),
					cleanedFileKey,
					creator);
			if (registration.getMetadata() != null) {
				controlFile.setMetadata(registration.getMetadata());
			}
			controlFileDAO.saveOrUpdate(controlFile);
			PersistenceUtil.commit(sessAndTrx);

			final ControlFileRegistration newControlFileRegistration = new ControlFileRegistration(
					controlFile.getBucket(),
					controlFile.getFileKey(),
					user.getUsername(),
					controlFile.getCreateTime(),
					controlFile.getStatus(),
					controlFile.getId(),
					controlFile.getVersion().toString(),
					controlFile.getMetadata());

			return newControlFileRegistration;
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{}: {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startNanos));
		}
	}

	@Override
	public void setAnimal(
			User user,
			DataSnapshotId datasetId,
			GwtAnimal animal)
			throws AuthorizationException,
			DataSnapshotNotFoundException {
		final long startNanos = System.nanoTime();
		final String m = "setAnimal(...)";
		checkNotNull(user);
		checkNotNull(datasetId);
		checkNotNull(animal);
		SessionAndTrx sessAndTrx = null;
		try {

			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			final IExperimentDAO experimentDAO = daoFac.getExperimentDAO();
			final ExperimentEntity experiment = experimentDAO
					.findByNaturalId(datasetId.getId());
			if (experiment == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}
			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							experiment.getPubId(),
							false),
					CorePermDefs.EDIT,
					experiment.getPubId());
			final Session session = sessAndTrx.session;

			String orgName = "unknown";
			String code = "";

			if (animal.getOrganization() != null &&
					!animal.getOrganization().getName().equals("Temporary")) {
				orgName = animal.getOrganization().getName();
				code = animal.getOrganization().getCode();
			}
			final IDataSnapshotService dsService = dsServiceFac.newInstance(
					session,
					MAX_DS_TS_ANNS,
					dataSnapshotShortCache,
					searchResultCache);
			final OrganizationEntity org = dsService.getOrganization(
					orgName,
					code);
			final GwtStrain strain = animal.getStrain();
			String strainLabel = "none";
			String speciesLabel = "unknown";
			if (strain != null) {
				strainLabel = strain.getLabel();
				if (strain.getSpeciesLabel() != null) {
					speciesLabel = strain.getSpeciesLabel();
				}
			}
			StrainEntity strainEntity = null;
			ISpeciesDAO speciesDAO = daoFac.getSpeciesDAO();
			SpeciesEntity species = speciesDAO
					.findByLabel(speciesLabel);
			if (species == null) {
				species = new SpeciesEntity(speciesLabel);
				strainEntity = new StrainEntity(
						species,
						strainLabel);
				speciesDAO.saveOrUpdate(species);
				logger.info(
						"{}: Created new species/strain {}/{}",
						m,
						speciesLabel,
						strainLabel);
			} else {
				for (final StrainEntity existingStrain : species
						.getStrains()) {
					if (existingStrain.getLabel().equals(strainLabel)) {
						strainEntity = existingStrain;
						break;
					}
				}
				if (strainEntity == null) {
					strainEntity = new StrainEntity(
							species,
							strainLabel);
					// will be persisted on commit.
					logger.info(
							"{}: Added new strain {} to species {}",
							m,
							strainLabel,
							speciesLabel);
				}
			}

			final IAnimalDAO animalDAO = daoFac.getAnimalDAO(session);
			AnimalEntity animalEntity = animalDAO.findByLabel(animal
					.getLabel());
			if (animalEntity == null) {
				animalEntity = new AnimalEntity(
						animal.getLabel(),
						org,
						strainEntity);
				logger.info(
						"{}: Created animal {}",
						m,
						animal.getLabel());
			}

			animalEntity.setOrganization(org);
			animalEntity.setStrain(strainEntity);
			final AnimalEntity existingParent = experiment.getParent();
			if (existingParent == null) {
				animalEntity.addExperiment(experiment);
			} else if (existingParent != animalEntity) {
				throw new IllegalArgumentException("Cannot change animal");
			}

			animalDAO.saveOrUpdate(animalEntity);
			PersistenceUtil.commit(sessAndTrx);
		} catch (RuntimeException rte) {
			PersistenceUtil.rollback(sessAndTrx);
			throw rte;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{}: {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startNanos));
		}
	}

	@Override
	public List<EEGMontage> getMontages(User user, String dataSnapshotID) {
		long in = new Date().getTime();
		final String M = "getMontages(User)";

		Session session = null;
		Transaction trx = null;

		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			List<EEGMontage> montages = service.getMontages(user,
					dataSnapshotID);
			trx.commit();
			return montages;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public String getParentShapshot(User authenticatedUser, String studyRevId) {
		long in = new Date().getTime();
		checkNotNull(authenticatedUser);
		checkNotNull(studyRevId);
		final String M = "getParentSnapshot(...)";
		Session session = null;
		Transaction trx = null;
		try {
			session = sessFac.openSession();
			trx = session.beginTransaction();
			final IDataSnapshotService service =
					dsServiceFac.newInstance(session, MAX_DS_TS_ANNS,
							dataSnapshotShortCache, searchResultCache);
			trx.commit();
			return service.getParentSnapshot(authenticatedUser, studyRevId);
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(session);
			logger.info(
					"{} : {} seconds",
					M, (new Date().getTime() - in) / 1000.0);
		}
	}

	@Override
	public RecordingObject getRecordingObject(User user,
			DataSnapshotId datasetId, String name) {
		long startTimeNanos = System.nanoTime();
		final String m = "getRecordingObjectContent(...)";
		SessionAndTrx sessAndTrx = null;
		try {
			sessAndTrx = PersistenceUtil.getSessAndTrx(sessFac);
			IRecordingObjectDAO recObjectDAO =
					daoFac.getRecordingObjectDAO(sessAndTrx.session);
			RecordingObjectEntity recordingObject = recObjectDAO
					.findByName(name);
			Recording recording = recordingObject.getParent();

			DataSnapshotEntity dsEntity = getDataSnapshotFromRecording(recording);

			AuthCheckFactory.getHandler().checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dsEntity.getPubId(),
							false),
					CorePermDefs.READ,
					dsEntity.getPubId());

			PersistenceUtil.commit(sessAndTrx);
			// Path p = Paths.get(recordingObject.getFileName());
			RecordingObject newRecordingObject = new RecordingObject(
					recordingObject.getFileName(), // p.getFileName().toString(),//recordingObject.getFileName(),
					recordingObject.getMd5Hash(),
					recordingObject.getSizeBytes(),
					user.getUsername(),
					recordingObject.getInternetMediaType(),
					datasetId.getId(),
					recordingObject.getId(),
					recordingObject.getVersion().toString(),
					recordingObject.getCreateTime());

			return newRecordingObject;
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(sessAndTrx);
			throw e;
		} finally {
			PersistenceUtil.close(sessAndTrx);
			logger.info(
					"{} : {} seconds",
					m,
					BtUtil.diffNowThenSeconds(startTimeNanos));
		}

	}

}
