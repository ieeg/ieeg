/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * @author Sam Donnelly
 */
@Entity(name = "EpilepsySubtype")
@Table(name = EpilepsySubtypeEntity.TABLE)
public class EpilepsySubtypeEntity implements IHasLongId {
	public static final String TABLE = "epilepsy_subtype";
	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private Integer version;
	private EegStudy parent;
	private EpilepsyType epilepsyType;
	private Etiology etiology;

	EpilepsySubtypeEntity() {}

	public EpilepsySubtypeEntity(
			EegStudy parent,
			EpilepsyType epilepsyType,
			Etiology etiology) {
		this.parent = parent;
		this.epilepsyType = epilepsyType;
		this.etiology = etiology;

		this.parent.getEpilepsySubtypes().add(this);
	}

	@Enumerated(EnumType.STRING)
	@NotNull
	public EpilepsyType getEpilepsyType() {
		return epilepsyType;
	}

	@Enumerated(EnumType.STRING)
	@NotNull
	public Etiology getEtiology() {
		return etiology;
	}

	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public EegStudy getParent() {
		return parent;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setEpilepsyType(EpilepsyType epilepsyType) {
		this.epilepsyType = epilepsyType;
	}

	public void setEtiology(Etiology etiology) {
		this.etiology = etiology;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setParent(@Nullable EegStudy parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}
}
