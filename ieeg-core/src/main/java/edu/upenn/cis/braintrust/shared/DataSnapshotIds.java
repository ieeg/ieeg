/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableMap;

/**
 * Contains a snapshot rev id + a mapping of ts ann external ids to ts ann ids.
 * 
 * @author Sam Donnelly
 */
@Immutable
@GwtCompatible(serializable = true)
public final class DataSnapshotIds implements Serializable {
	private static final long serialVersionUID = 1L;

	private String dsRevId;

	private Map<String, String> tsAnnExternalIdsToIds;

	@SuppressWarnings("unused")
	private DataSnapshotIds() {}

	public DataSnapshotIds(
			final String dsRevId,
			final Map<String, String> tsAnnExternalIdsToIds) {
		this.dsRevId = checkNotNull(dsRevId);
		this.tsAnnExternalIdsToIds =
				ImmutableMap.copyOf(tsAnnExternalIdsToIds);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DataSnapshotIds other = (DataSnapshotIds) obj;
		if (dsRevId == null) {
			if (other.dsRevId != null) {
				return false;
			}
		} else if (!dsRevId.equals(other.dsRevId)) {
			return false;
		}
		return true;
	}

	public String getDsRevId() {
		return dsRevId;
	}

	public Map<String, String> getTsAnnExternalIdsToIds() {
		return tsAnnExternalIdsToIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dsRevId == null) ? 0 : dsRevId.hashCode());
		return result;
	}
}
