/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.Search2Hql;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class DataSnapshotDAOHibernate
		extends GenericHibernateDAO<DataSnapshotEntity, Long>
		implements IDataSnapshotDAO {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public DataSnapshotDAOHibernate() { }

	public DataSnapshotDAOHibernate(Session session) {
		setSession(checkNotNull(session));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<EegStudy> findBySearch(
			EegStudySearch dataSnapshotSearch,
			UserEntity userEntity,
			User user) {
		String m = "findBySearch(...)";
		checkNotNull(dataSnapshotSearch);
		checkNotNull(userEntity);
		checkNotNull(user);
		checkArgument(userEntity.getId().equals(user.getUserId()),
				"userEntity and user must be the same user");

		final EegStudySearch studySearch =
				dataSnapshotSearch;

		final Session session = getSession();

		StringBuilder fromPart = new StringBuilder(
				"select distinct study "
						+ "from EegStudy study "
						+ "join fetch study.recording recording "
						+ "join fetch study.parent admission "
						+ "join fetch admission.parent patient "
						+ "join fetch patient.organization ");

		// Just so we can start appending and's
		StringBuilder wherePart = new StringBuilder("where 1 = 1 ");

		Map<String, Object> params = newHashMap();

		Search2Hql.eegStudySearch2Query(
				studySearch,
				fromPart,
				wherePart,
				params);

		Query q;

		if (user.getRoles().contains(Role.ADMIN)) {
			// they should see everything
			q = session
					.createQuery(fromPart.append(wherePart).toString());
		} else {
			fromPart.append(
					"join study.extAcl acl "
							+ ExtAclEntity.FROM_PERMISSIONS);

			wherePart.append("and " + ExtAclEntity.WHERE_PERMISSIONS);

			q = session
					.createQuery(fromPart.append(wherePart).toString())
					.setParameter("user", userEntity);
		}

		for (Map.Entry<String, Object> param : params.entrySet()) {
			if (param.getValue() instanceof Collection<?>) {
				q.setParameterList(
						param.getKey(),
						(Collection<?>) param.getValue());
			} else {
				q.setParameter(param.getKey(), param.getValue());
			}
		}

		logger.debug("{}: s: {}", m, session);
		logger.debug("{}: q: {}", m, q);

		Set<EegStudy> results = newHashSet(q.list());

		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Dataset> findForUser(
			UserEntity userEntity,
			User user) {
		String m = "findForUser(...)";
		checkNotNull(userEntity);
		checkNotNull(user);
		checkArgument(userEntity.getId().equals(user.getUserId()),
				"userEntity and user must be the same user");

		final Session session = getSession();

		StringBuilder fromPart = new StringBuilder(
				"select distinct study "
						+ "from Dataset study ");

		// Just so we can start appending and's
		StringBuilder wherePart = new StringBuilder("where 1 = 1 ");

		Map<String, Object> params = newHashMap();

		Query q = session
					.createQuery("from Dataset where creator = :creator")
					.setParameter("creator", userEntity);

		Set<Dataset> results = newHashSet(q.list());

		return results;
	}

	@Override
	public TsAnnotationEntity findTsAnn(DataSnapshotEntity ds, String tsAnnRevId) {
		return (TsAnnotationEntity) getSession()
				.getNamedQuery(DataSnapshotEntity.FIND_TS_ANNOTATION)
				.setParameter("ds", ds)
				.setLong("tsAnnRevId", Long.valueOf(tsAnnRevId))
				.uniqueResult();
	}

	@Override
	public long countTsAnns(DataSnapshotEntity ds) {
		final Long count = (Long) getSession()
				.getNamedQuery(DataSnapshotEntity.COUNT_ALL_TS_DS_ANNS)
				.setParameter("ds", ds)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countTsAnns(
			DataSnapshotEntity ds,
			TimeSeriesEntity ts) {
		final Long count = (Long) getSession()
				.getNamedQuery(DataSnapshotEntity.COUNT_TS_ANNS_ON_TS)
				.setParameter("ds", ds)
				.setParameter("ts", ts)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countActiveSnapshots() {
		final Long count = (Long) getSession()
				.getNamedQuery(DataSnapshotEntity.COUNT_ACTIVE_DATASNAPSHOTS)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countAllSnapshots() {
		final Long count = (Long) getSession()
				.getNamedQuery(DataSnapshotEntity.COUNT_ALL_DATASNAPSHOTS)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public DataSnapshotEntity findByLabel(String dsLabel) {
//		System.err.println("Request for " + dsLabel);
		return (DataSnapshotEntity) getSession()
				.getNamedQuery(DataSnapshotEntity.FIND_BY_LABEL)
				.setString("dsLabel", dsLabel)
				.uniqueResult();
	}

	@Override
	public long countTsAnns() {
		final Long count = (Long) getSession()
				.getNamedQuery(DataSnapshotEntity.COUNT_ALL_TS_ANNS)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public Set<DataSnapshotEntity> findAnyWithLabel(String label) {
		List<?> results = getSession()
				.getNamedQuery(DataSnapshotEntity.FIND_BY_LABEL)
				.setString("dsLabel", label)
				.list();
		
		Set<DataSnapshotEntity> ret = new HashSet<DataSnapshotEntity>();
		
		for (Object r: results)
			ret.add((DataSnapshotEntity)r);
		
		return ret;
	}
	
	@Override
	public boolean isInAProject(DataSnapshotEntity ds) {
		return getSession()
				.getNamedQuery(DataSnapshotEntity.IS_IN_A_PROJECT)
				.setParameter("ds", ds)
				.uniqueResult() != null;
	}

	// @Override
	// public void forceVersIncrPes(DataSnapshotEntity ds) {
	// getSession().createSQLQuery("update data_snapshot set obj_version = "
	// + (ds.getVersion() + 1) + " where data_snapshot_id = "
	// + ds.getId() + " and obj_version = " + ds.getVersion())
	// .executeUpdate();
	// // getSession().getNamedQuery(DataSnapshotEntity.FORCE_VERSION_INCR)
	// // .setParameter("newVersion", ds.getVersion() + 1)
	// // .setParameter("id", ds.getId())
	// // .setParameter("curVersion", ds.getVersion());
	// getSession().refresh(ds);
	// }
}
