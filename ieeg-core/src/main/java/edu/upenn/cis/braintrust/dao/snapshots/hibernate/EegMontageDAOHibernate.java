package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import java.util.List;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegMontageEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class EegMontageDAOHibernate
		extends GenericHibernateDAO<EegMontageEntity, Long>
		implements IEegMontageDAO {

	public EegMontageDAOHibernate() {}

	public EegMontageDAOHibernate(Session session) {
		setSession(session);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EegMontageEntity> findForUser(UserEntity btUser) {
		// TODO Auto-generated method stub
		return getSession().getNamedQuery(EegMontageEntity.BY_USER)
				.setParameter("user", btUser).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<EegMontageEntity> findForUserAndSnapshot(UserEntity btUser,
			DataSnapshotEntity snapshot) {
		// TODO Auto-generated method stub
		return getSession().getNamedQuery(EegMontageEntity.BY_USERSNAPSHOT)
				.setParameter("user", btUser)
				.setParameter("snapshot", snapshot).list();
	}

	@Override
	public List<EegMontageEntity> findForSnapshot(DataSnapshotEntity snapshot) {
		@SuppressWarnings("unchecked")
		final List<EegMontageEntity> montages =
				getSession()
						.getNamedQuery(EegMontageEntity.BY_SNAPSHOT)
						.setParameter("snapshot", snapshot)
						.list();
		return montages;

	}

}
