/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.services;

import java.util.Set;

import org.hibernate.Session;
import org.hibernate.context.internal.ManagedSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.ImageType;

public class ImagesForStudyResource implements IImagesForStudyResource {

	private final static Logger logger = LoggerFactory
			.getLogger(ImagesForStudyResource.class);

	private final IDAOFactory daoFac;

	public ImagesForStudyResource(final IDAOFactory daoFac) {
		this.daoFac = daoFac;
	}

	@Override
	public Set<ImageForStudy> getAllImagesForStudy(final String studyDir) {
		final String M = "getAllImagesForStudy(...)";
		try {

			final Session session = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(session);

			final IImageDAO imageDAO = daoFac.getImageDAO();

			session.beginTransaction();

			final Set<ImageForStudy> imageForStudys = imageDAO
					.findByStudyDir(studyDir);

			session.getTransaction().commit();
			logger.info("{}: Returning {} images for study {}.", new Object[] {
					M, imageForStudys.size(), studyDir });
			return imageForStudys;
		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					logger.debug("Trying to rollback database transaction after exception");
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("Could not rollback transaction after exception!",
						rbEx);
			}
			logger.error(M + ": Rolled back transaction after exception.", t);
			throw new IllegalStateException(t);
		} finally {
			PersistenceUtil.close(ManagedSessionContext.unbind(HibernateUtil
					.getSessionFactory()));
		}

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.services.IImagesForStudyResource#getImagesForStudy(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public Set<ImageForStudy> getImagesForStudy(final String studyDir,
			final String imageType) {
		final String M = "getImagesForStudy(...)";
		ImageType imageTypeValue;

		try {
			imageTypeValue = ImageType.valueOf(imageType);
		} catch (final IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid image type: ["
					+ imageType + "].", e);
		}

		try {

			final Session session = HibernateUtil.getSessionFactory()
					.openSession();
			ManagedSessionContext.bind(session);

			final IImageDAO imageDAO = daoFac.getImageDAO();

			session.beginTransaction();

			final Set<ImageForStudy> imageForStudys = imageDAO
					.findByStudyDirAndImageType(studyDir, imageTypeValue);

			session.getTransaction().commit();
			logger.info("{}: Returning {} {} images for study {}.",
					new Object[] { M, imageForStudys.size(), imageType,
							studyDir });
			return imageForStudys;

		} catch (final Throwable t) {
			try {
				if (HibernateUtil.getSessionFactory().getCurrentSession()
						.getTransaction().isActive()) {
					logger.debug("Trying to rollback database transaction after exception");
					HibernateUtil.getSessionFactory().getCurrentSession()
							.getTransaction().rollback();
				}
			} catch (final Throwable rbEx) {
				logger.error("Could not rollback transaction after exception!",
						rbEx);
			}
			logger.error(M + ": Rolled back transaction after exception.", t);
			throw new IllegalStateException(t);
		} finally {
			PersistenceUtil.close(ManagedSessionContext.unbind(HibernateUtil
					.getSessionFactory()));
		}
	}
}
