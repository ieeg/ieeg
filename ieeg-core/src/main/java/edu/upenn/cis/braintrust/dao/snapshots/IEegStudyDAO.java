/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots;

import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

/**
 * {@link EegStudy} DAO
 * 
 * @author Sam Donnelly
 */
public interface IEegStudyDAO extends IDAO<EegStudy, Long> {

	Contact findContactByTracePubId(String timeSeriesPubId);

	EegStudy findByPubId(String pubId);

	/**
	 * Returns the total number of studies in the database.
	 */
	long countAllStudies();

	/**
	 * Returns the total number of world readable studies in the database.
	 */
	long countWorldReadableStudies();

	/**
	 * 
	 * Returns the number of studies which some non-admin user can read.
	 * 
	 * @return the number of studies which some non-admin user can read
	 */
	long countUserReadableStudies();

	EegStudy findByTimeSeries(TimeSeriesEntity timeSeries);
	
	EegStudy findByRecording(Recording recording);

}
