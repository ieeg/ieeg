/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.sort;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DrugAdminRelationship;
import edu.upenn.cis.braintrust.model.EpilepsySubtypeEntity;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.shared.AnimalMetadata;
import edu.upenn.cis.braintrust.shared.ContactGroupMetadata;
import edu.upenn.cis.braintrust.shared.DrugAdminMetadata;
import edu.upenn.cis.braintrust.shared.EpilepsySubtypeMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;

/**
 * @author Sam Donnelly
 * 
 */
public class Assembler {

	public static Set<ContactGroupMetadata> contactGroupEntities2Metadatas(
			Set<ContactGroup> contactGroups) {
		Set<ContactGroupMetadata> contactGroupMds = newHashSet();
		for (ContactGroup contactGroup : contactGroups) {
			contactGroupMds.add(
					new ContactGroupMetadata(
							contactGroup.getChannelPrefix(),
							contactGroup.getLocation(),
							contactGroup.getSide(),
							contactGroup.getSamplingRate(),
							contactGroup.getLffSetting(),
							contactGroup.getHffSetting()));
		}
		return contactGroupMds;
	}

	public static ExperimentMetadata entity2Metadata(
			ExperimentEntity experimentEntity) {
		AnimalEntity animalEntity = experimentEntity.getParent();
		AnimalMetadata animalMetadata = new AnimalMetadata(
				animalEntity.getLabel(),
				animalEntity.getStrain().getParent().getLabel(),
				animalEntity.getStrain().getLabel());

		ExperimentMetadata experimentMetadata =
				new ExperimentMetadata(
						experimentEntity.getPubId(),
						experimentEntity.getLabel(),
						animalMetadata,
						experimentEntity.getRecording().getImages().size(),
						null == experimentEntity
								.getStimRegion()
								? null
								: experimentEntity.getStimRegion().getLabel(),
						null == experimentEntity.getStimType()
								? null
								: experimentEntity.getStimType().getLabel(),
						experimentEntity.getStimDurationMs(),
						experimentEntity.getStimIsiMs(),
						entity2Metadata(experimentEntity.getDrugAdmins()),
						experimentEntity
								.getRecording()
								.getRefElectrodeDescription(),
						contactGroupEntities2Metadatas(
						experimentEntity
								.getRecording().getContactGroups()),
						null == experimentEntity.getAge()
								? null
								: experimentEntity.getAge());
		return experimentMetadata;

	}

	public static Set<DrugAdminMetadata> entity2Metadata(
			Set<DrugAdminRelationship> drugAdminEntities) {
		Set<DrugAdminMetadata> builder = new LinkedHashSet<DrugAdminMetadata>();
		List<DrugAdminRelationship> sortedDas = newArrayList(drugAdminEntities);
		sort(sortedDas, new DrugAdminRelationship.DrugAdminRelationshipComparator());
		for (DrugAdminRelationship drugAdminEntity : sortedDas) {
			DrugAdminMetadata drugAdminMetadata =
					new DrugAdminMetadata(
							drugAdminEntity.getId(),
							drugAdminEntity.getDrug().getLabel(),
							drugAdminEntity.getAdminTime(),
							drugAdminEntity.getDoseMgPerKg());
			builder.add(drugAdminMetadata);
		}
		return builder;
	}

	public static Set<EpilepsySubtypeMetadata> epilepsySubtypeEntities2Metadatas(
			Set<EpilepsySubtypeEntity> epSubtypeEntities) {
		Set<EpilepsySubtypeMetadata> epSubtypeMds = newHashSet();
		for (EpilepsySubtypeEntity epSubtypeEntity : epSubtypeEntities) {
			EpilepsySubtypeMetadata epSubtypeMd =
					new EpilepsySubtypeMetadata(
							epSubtypeEntity.getEpilepsyType(),
							epSubtypeEntity.getEtiology());
			epSubtypeMds.add(epSubtypeMd);
		}
		return epSubtypeMds;
	}

	private Assembler() {}
}
