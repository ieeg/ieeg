/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import javax.annotation.concurrent.Immutable;

@Immutable
public final class SearchResultCacheEntry {
	private final int imageCount;
	private final int tsCount;
	private final int tsAnnCount;
	private final String organizationName;

	public SearchResultCacheEntry(
			int imageCount,
			int tsCount,
			int tsAnnCount,
			String organizationName) {
		this.imageCount = imageCount;
		this.tsCount = tsCount;
		this.tsAnnCount = tsAnnCount;
		this.organizationName = organizationName;
	}

	public int getImageCount() {
		return imageCount;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public int getTsAnnCount() {
		return tsAnnCount;
	}

	public int getTsCount() {
		return tsCount;
	}

}
