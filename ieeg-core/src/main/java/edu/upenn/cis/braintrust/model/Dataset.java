/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * A data snapshot that can contain data from anywhere: it is not limited like
 * {@link EegStudy}.
 * 
 * @author Sam Donnelly
 */
@NamedQueries({
		@NamedQuery(
				name = Dataset.DATASET_BY_LABEL,
				query = "from Dataset ds "
						+ "where ds.label = :label")
})
@Entity
@Table(name = Dataset.TABLE)
@PrimaryKeyJoinColumn(name = Dataset.ID_COLUMN)
public class Dataset extends DataSnapshotEntity {

	public static final String DATASET_BY_LABEL = "Dataset-findByLabel";

	public static final String TABLE = "dataset";
	public static final String UPPER_CASE_TABLE = "DATASET";
	public static final String ID_COLUMN = TABLE + "_id";

	private Set<TimeSeriesEntity> timeSeries = newHashSet();
	private Set<ImageEntity> images = newHashSet();
	private UserEntity creator;
	private Set<AnalyzedDataSnapshot> analysisInputs = newHashSet();

	/** For Hibernate. */
	public Dataset() {}

	public Dataset(
			String label,
			UserEntity creator,
			ExtAclEntity extAcl) {
		super(label, extAcl, null);
		this.creator = checkNotNull(creator);
	}

	/**
	 * one-to-many with additional columns on join table, intermediate entity
	 * class. To create a link, instantiate an {@code AnalyzedDataSnapshot} with
	 * the right constructor. To remove a link, use
	 * {@code getAnalyzedDataSnapshots().remove(...)}.
	 * 
	 * @return the join-table entity
	 */
	@OneToMany(
			mappedBy = "analysis",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<AnalyzedDataSnapshot> getAnalysisInputs() {
		return analysisInputs;
	}

	/**
	 * The user who created the dataset.
	 * 
	 * @return he user who created the dataset
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id")
	@ForeignKey(name = "FK_DATASET_CREATOR")
	@NotNull
	public UserEntity getCreator() {
		return creator;
	}

	@Override
	@ManyToMany
	@JoinTable(
			name = TABLE + "_" + ImageEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = ImageEntity.ID_COLUMN))
	@ForeignKey(
			name = "FK_DATASET_IMAGE_DATASET",
			inverseName = "FK_DATASET_IMAGE_IMAGE")
	public Set<ImageEntity> getImages() {
		return images;
	}

	@Override
	@ManyToMany
	@JoinTable(
			name = TABLE + "_" + TimeSeriesEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = TimeSeriesEntity.ID_COLUMN))
	public Set<TimeSeriesEntity> getTimeSeries() {
		return timeSeries;
	}

	@SuppressWarnings("unused")
	private void setAnalysisInputs(
			final Set<AnalyzedDataSnapshot> analysisInputs) {
		this.analysisInputs = analysisInputs;
	}

	/**
	 * @param uid the uid of the creator of this annotation.
	 */
	public void setCreator(final UserEntity creator) {
		this.creator = creator;
	}

	public void setTimeSeries(final Set<TimeSeriesEntity> timeSeries) {
		this.timeSeries = timeSeries;
	}

	@SuppressWarnings("unused")
	private void setImages(Set<ImageEntity> images) {
		this.images = images;
	}

	@Transient
	@Override
	public DataSnapshotType getDataSnapshotType() {
		return DataSnapshotType.DATASET;
	}
	
	public static final EntityConstructor<Dataset, String, UserEntity> CONSTRUCTOR =
			new EntityConstructor<Dataset,String,UserEntity>(Dataset.class,
				new IPersistentObjectManager.IPersistentKey<Dataset,String>() {

			@Override
			public String getKey(Dataset o) {
				return o.getPubId();
			}

			@Override
			public void setKey(Dataset o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<Dataset,String,UserEntity>() {

			@Override
			public Dataset create(String label, UserEntity optParent) {
				// TODO: Create extAcl??
				return new Dataset(label, (UserEntity)optParent, null);
			}
		}
					);


	@Override
	public EntityConstructor<Dataset, String, UserEntity> tableConstructor() {
		return CONSTRUCTOR;
	}

}
