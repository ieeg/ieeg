package edu.upenn.cis.braintrust.imodel;

import java.io.Serializable;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.upenn.cis.db.habitat.persistence.constructors.PersistentObjectConstructor;

public interface IPersistentObject extends Serializable {
	@JsonIgnore
	@Transient
	public PersistentObjectConstructor<? extends IPersistentObject, ?> tableConstructor();

}
