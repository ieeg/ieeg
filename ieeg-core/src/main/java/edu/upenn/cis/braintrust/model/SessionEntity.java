/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * An application session.
 * 
 * @author John Frommeyer
 * 
 */
@Entity(name = "Session")
@Table(name = SessionEntity.TABLE)
@NamedQueries({
	@NamedQuery(
			name = SessionEntity.DELETE_OLD_SESSIONS,
			query = "delete from Session where lastAccessTime < :minLastAccess")
//	,
//			@NamedQuery(
//					name = SessionEntity.SESSION_TOKEN,
//					query = "select from Session where token = :token") 
	}
			)
public class SessionEntity implements IHasLongId {
	public static final String TABLE = "session";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String DELETE_OLD_SESSIONS = "SessionEntity-deleteSessionsOlderThan";
	public static final String SESSION_TOKEN = "SessionEntity-sessionToken";

	private Long id;
	private Integer version;
	private String token = newUuid();
	private UserEntity user;
	private Date lastAccessTime = new Date();
	private SessionStatus status = SessionStatus.ACTIVE;

	SessionEntity() {}

	public SessionEntity(UserEntity user) {
		this.user = user;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Column
	@NotNull
	public Date getLastAccessTime() {
		return lastAccessTime;
	}

	@Enumerated(EnumType.STRING)
	@NotNull
	public SessionStatus getStatus() {
		return status;
	}
	
	@Column(unique = true)
	@NotNull
	@NaturalId
	@Size(min = 36, max = 36)
	public String getToken() {
		return token;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = UserEntity.ID_COLUMN)
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

	@SuppressWarnings("unused")
	private void setToken(String token) {
		this.token = token;
	}

	@SuppressWarnings("unused")
	private void setUser(UserEntity user) {
		this.user = user;
	}

	public void setLastAccessTime(Date lastAccess) {
		this.lastAccessTime = lastAccess;
	}
	
	public void setStatus(SessionStatus status) {
		this.status = status;
	}

}
