/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import static edu.upenn.cis.braintrust.BtUtil.close;

import java.io.InputStream;
import java.util.Collections;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;

public class IvPropsListener implements ServletContextListener {

	private static Logger logger = LoggerFactory
			.getLogger(IvPropsListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		final String m = "contextInitialized(...)";
		String configPath;
		if (AwsUtil.isConfigFromS3()) {
			configPath = AwsUtil.getConfigS3Dir();

			InputStream is = null;
			try {
				final String configFileName = configPath.equals("") ? IvProps.DFLT_PROPS
						: configPath
						+ "/" + IvProps.DFLT_PROPS;
				logger.info("looking for ieegview properties on AWS/S3 at ["
						+ AwsUtil.getConfigS3Bucket() + "] ["
						+ configFileName + "]");
				GetObjectRequest getObjectRequest =
						new GetObjectRequest(
								AwsUtil.getConfigS3Bucket(),
								configFileName);

				S3Object s3Object = AwsUtil.getS3().getObject(
						getObjectRequest);
				is = s3Object.getObjectContent();

				IvProps.init(is);
			} finally {
				close(is);
			}

		} else {
			configPath = BtUtil.getConfigPath(
					BtUtil.getWebAppName(sce.getServletContext()));
			IvProps.init(configPath);
		}
		sce.getServletContext().setAttribute(
				IvProps.ATTR_KEY,
				IvProps.getIvProps());

		new Thread() {
			public void run() {
				try {
					IUserService userService = UserServiceFactory.getUserService();
					User dbgroupAdmin = userService.findUserByUid(new UserId(1));
					logger.info("IvPropsListener.run - user:"+dbgroupAdmin);
					if (dbgroupAdmin != null) {
						// don't load data if this user cannot be found
						IDataSnapshotServer dsServer = DataSnapshotServerFactory.getDataSnapshotServer();
						dsServer.getDataSnapshots(dbgroupAdmin, new EegStudySearch());
						dsServer.getExperiments(
                                dbgroupAdmin,
                                new ExperimentSearch(
                                        Collections.<Long> emptySet(),
                                        Collections.<Long> emptySet(),
                                        Collections.<Long> emptySet(),
                                        Collections.<Long> emptySet(),
                                        Collections.<String> emptySet()));
					}
				} catch (RuntimeException re) {
					logger.error(
							m
									+ ": caught exception when initializing search cache",
							re);
				}
			}
		}.start();

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// NOOP
	}
}
