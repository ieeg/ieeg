/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author John Frommeyer
 *
 */
public final class FileObjectUploadResult implements IRecObjectUploadResult {

	private final File objectFile;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public FileObjectUploadResult(File objectFile) {
		this.objectFile = checkNotNull(objectFile);
	}

	@Override
	public void deleteObject() {
		final String m = "deleteObject()";
		if (!objectFile.exists()) {
			logger.debug(
					"{}: Asked to delete file {} but file does not exist.",
					m,
					objectFile.getAbsoluteFile());
		} else {
			final boolean deleted = objectFile.delete();
			if (deleted) {
				logger.info(
						"{}: Deleted file {}",
						m,
						objectFile.getAbsoluteFile());
			} else {
				logger.info(
						"{}: Unsucessful attempt to delete file {}",
						m,
						objectFile.getAbsoluteFile());
			}
		}
	}

}
