/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = ToolDialogXml.TABLE)
public class ToolDialogXml {

	public static final String TABLE = "tool_dialog_xml";

	private Long id;
	private Integer version;
	private ToolEntity parent;
	private String value;

	/**
	 * For JPA.
	 */
	ToolDialogXml() {}

	public ToolDialogXml(final String value, final ToolEntity parent) {
		this.value = value;
		this.parent = parent;
	}

	@Lob
	@Column
	@NotNull
	public String getValue() {
		return value;
	}

	@Id
	public Long getId() {
		return id;
	}

	@MapsId
	// Have to make this many-to-one because it's one-to-many on the parent
	// side (apparantly)
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "FK_DIALOG_XML_PARENT")
	@JoinColumn(name = "parent_id")
	@NotNull
	public ToolEntity getParent() {
		return parent;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}

	public void setParent(final ToolEntity parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}
}
