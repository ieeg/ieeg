/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.tools;

import static com.google.common.collect.Sets.newTreeSet;

import java.util.List;
import java.util.SortedSet;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class JobDAOHibernate
		extends GenericHibernateDAO<JobEntity, Long>
		implements IJobDAO {
	public JobDAOHibernate() {

	}

	public JobDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public SortedSet<Long> listJobUserIds() {
		final Session s = getSession();
		@SuppressWarnings("unchecked")
		List<Long> idList = s
				.createQuery(
						"select distinct j.userId from JobEntity j order by j.userId")
				.list();
		final SortedSet<Long> userIds = newTreeSet(idList);
		return userIds;
	}

	@Override
	public List<JobEntity> findByUser(User user) {
		final Session s = getSession();
		@SuppressWarnings("unchecked")
		List<JobEntity> jobs = s
				.createQuery("From JobEntity j where j.userId = :userId")
				.setParameter("userId", user.getUserId().getValue()).list();
		return jobs;
	}

}