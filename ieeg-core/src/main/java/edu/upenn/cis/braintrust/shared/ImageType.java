/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public enum ImageType {
	MRI("MRI"),
	FMRI("FMRI"),
	MRS("MRS"),
	ICTAL_SPECT("Ictal Spect"),
	CT("CT"),
	PET("PET"),
	MEG("MEG"),
	DIGITAL_PICTURES("Digital Pictures"),
	ELECTRODE_MAP("Electrode Map"),
	THREE_D_RENDERING("3D Rendering");
	private final String displayString;

	/**
	 * Maps a type's toString representation to the type itself.
	 */
	public static final SortedMap<String, ImageType> fromString;
	static {
		final SortedMap<String, ImageType> temp = newTreeMap();
		for (final ImageType type : values()) {
			temp.put(type.toString(), type);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	private ImageType(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}

}
