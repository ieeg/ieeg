/*
 * Copyright (c) 2005, Christian Bauer <christian@hibernate.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the original author nor the names of contributors may be
 * used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES of MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT MANAGE_ACL OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT of
 * SUBSTITUTE GOODS OR SERVICES; LOSS of USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY of LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT of THE USE of THIS SOFTWARE, EVEN IF ADVISED of
 * THE POSSIBILITY of SUCH DAMAGE.
 */
package edu.upenn.cis.braintrust.dao;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import edu.upenn.cis.braintrust.dao.annotations.ILayerDAO;
import edu.upenn.cis.braintrust.dao.annotations.IMontageDAO;
import edu.upenn.cis.braintrust.dao.annotations.IMontagedChAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.LayerDAOHibernate;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.MontageDAOHibernate;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.MontagedChAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.identity.hibernate.UserDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugAdminDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.AnimalDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugAdminDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.OrganizationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.SpeciesDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimRegionDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimTypeDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.dao.provenance.IDataUsageDAO;
import edu.upenn.cis.braintrust.dao.provenance.hibernate.DataUsageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegMontageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectTaskDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.AnalyzedDsDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DatasetDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ImageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.RecordingDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.RecordingObjectDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.RecordingObjectTaskDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.TimeSeriesDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.IJobHistoryDAO;
import edu.upenn.cis.braintrust.dao.tools.JobDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.JobHistoryDAOHibernate;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

/**
 * Returns Hibernate-specific instances of DAOs.
 * <p/>
 * If for a particular DAO there is no additional non-CRUD functionality, we use
 * a nested static class to implement the interface in a generic way. This
 * allows clean refactoring later on, should the interface implement business
 * data access methods at some later time. Then, we would externalize the
 * implementation into its own first-level class.
 * 
 * @author Christian Bauer
 * 
 */
public class HibernateDAOFactory implements IDAOFactory {

	// You could override this if you don't want HibernateUtil for lookup
	protected Session getCurrentSession() {
		return HibernateUtil.getSessionFactory().getCurrentSession();
	}

	@Override
	public IPatientDAO getPatientDAO() {
		return (IPatientDAO) instantiateDAO(PatientDAOHibernate.class);
	}

	@Override
	public IEegStudyDAO getEegStudyDAO() {
		return (IEegStudyDAO) instantiateDAO(EegStudyDAOHibernate.class);
	}

	@Override
	public IExperimentDAO getExperimentDAO() {
		return (IExperimentDAO) instantiateDAO(ExperimentDAOHibernate.class);
	}

	private Object instantiateDAO(final Class<? extends IDAO<?, ?>> daoClass,
			Session sess) {
		try {
			final GenericHibernateDAO<?, ?> dao = (GenericHibernateDAO<?, ?>) daoClass
					.newInstance();
			dao.setSession(sess);
			return dao;
		} catch (final Exception ex) {
			throw new RuntimeException("Can not instantiate DAO: " + daoClass,
					ex);
		}
	}

	/**
	 * 
	 * @param daoClass
	 * @return
	 * 
	 * @deprecated use {@link #instantiateDAO(Class, Session)}
	 */
	@Deprecated
	private Object instantiateDAO(final Class<? extends IDAO<?, ?>> daoClass) {
		try {
			final GenericHibernateDAO<?, ?> dao = (GenericHibernateDAO<?, ?>) daoClass
					.newInstance();
			dao.setSession(getCurrentSession());
			return dao;
		} catch (final Exception ex) {
			throw new RuntimeException("Can not instantiate DAO: " + daoClass,
					ex);
		}
	}

	@Override
	public IImageDAO getImageDAO() {
		return (ImageDAOHibernate) instantiateDAO(ImageDAOHibernate.class);
	}

	@Override
	public IDatasetDAO getDatasetDAO() {
		return (DatasetDAOHibernate) instantiateDAO(DatasetDAOHibernate.class);
	}

	@Override
	public ITsAnnotationDAO getTsAnnotationDAO(
			Session session,
			Configuration config) {
		TsAnnotationDAOHibernate tsAnnDAO = (TsAnnotationDAOHibernate) instantiateDAO(
				TsAnnotationDAOHibernate.class, session);
		tsAnnDAO.setHibernateConfig(config);
		return tsAnnDAO;
	}

	@Override
	public IAnalyzedDataSnapshotDAO getAnalyzedDataSnapshotDAO() {
		return (AnalyzedDsDAOHibernate) instantiateDAO(AnalyzedDsDAOHibernate.class);
	}

	@Override
	public IJobDAO getJobDAO(Session sess) {
		return (JobDAOHibernate) instantiateDAO(JobDAOHibernate.class, sess);
	}

	@Override
	public IJobHistoryDAO getJobHistoryDAO(Session sess) {
		return (JobHistoryDAOHibernate) instantiateDAO(
				JobHistoryDAOHibernate.class, sess);
	}

	@Override
	public IAnimalDAO getAnimalDAO(Session session) {
		return new AnimalDAOHibernate(session);
	}

	@Override
	public ISessionDAO getSessionDAO(Session session) {
		return new SessionDAOHibernate(session);
	}

	@Override
	public IUserDAO getUserDAO(Session session) {
		return new UserDAOHibernate(session);
	}

	@Override
	public IDataUsageDAO getDataUsageDAO(Session session) {
		return new DataUsageDAOHibernate(session);
	}

	@Override
	public IDataSnapshotDAO getDataSnapshotDAO(Session session) {
		return new DataSnapshotDAOHibernate(session);
	}

	@Override
	public ITimeSeriesDAO getTimeSeriesDAO(Session session) {
		return new TimeSeriesDAOHibernate(session);
	}

	@Override
	public IProjectDAO getProjectDAO(Session session) {
		return new ProjectDAOHibernate(session);
	}

	@Override
	public IOrganizationDAO getOrganizationDAO() {
		return new OrganizationDAOHibernate();
	}

	@Override
	public IRecordingDAO getRecordingDAO(Session session) {
		return new RecordingDAOHibernate(session);
	}

	@Override
	public IDrugDAO getDrugDAO() {
		return new DrugDAOHibernate();
	}

	@Override
	public IDrugAdminDAO getDrugAdminDAO() {
		return new DrugAdminDAOHibernate();
	}

	@Override
	public ISpeciesDAO getSpeciesDAO() {
		return new SpeciesDAOHibernate();
	}

	@Override
	public IStimRegionDAO getStimRegionDAO() {
		return new StimRegionDAOHibernate();
	}

	@Override
	public IStimTypeDAO getStimTypeDAO() {
		return new StimTypeDAOHibernate();
	}

	@Override
	public IRecordingObjectTaskDAO getRecordingObjectTaskDAO(Session session) {
		return new RecordingObjectTaskDAOHibernate(session);
	}

	@Override
	public IRecordingObjectDAO getRecordingObjectDAO(Session session) {
		return new RecordingObjectDAOHibernate(session);
	}

	@Override
	public IControlFileDAO getControlFileDAO(Session session) {
		return new ControlFileDAOHibernate(session);
	}

    @Override
    public IEegMontageDAO getEegMontageDAO(Session session) {
      return new EegMontageDAOHibernate(session);
    }
	
	
}
