/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.shared.dto.GwtContactGroup;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class ContactGroupAssembler {

	public GwtContactGroup buildDto(final ContactGroup contactGroup) {
		checkNotNull(contactGroup);
		final GwtContactGroup gwtContactGroup = new GwtContactGroup(
				contactGroup.getChannelPrefix(),
				contactGroup.getElectrodeType(),
				contactGroup.getSide(),
				contactGroup.getLocation(),
				contactGroup.getSamplingRate(),
				Boolean.FALSE,
				contactGroup.getLffSetting(),
				contactGroup.getHffSetting(),
				contactGroup.getNotchFilter(),
				contactGroup.getImpedance(),
				contactGroup.getManufacturer(),
				contactGroup.getModelNo(),
				null,
				contactGroup.getId(),
				contactGroup.getVersion());
		return gwtContactGroup;
	}

	public ContactGroup buildEntity(final GwtContactGroup gwtContactGroup) {
		checkNotNull(gwtContactGroup);
		checkArgument(gwtContactGroup.getId() == null);
		checkArgument(gwtContactGroup.getVersion() == null);
		final ContactGroup contactGroup = new ContactGroup(
				gwtContactGroup.getChannelPrefix(),
				gwtContactGroup.getElectrodeType(),
				gwtContactGroup.getSamplingRate(),
				gwtContactGroup.getLffSetting().orNull(),
				gwtContactGroup.getHffSetting().orNull(),
				gwtContactGroup.getNotchFilter().orNull(),
				gwtContactGroup.getSide(),
				gwtContactGroup.getLocation(),
				gwtContactGroup.getImpedance().orNull(),
				gwtContactGroup.getManufacturer().orNull(),
				gwtContactGroup.getModelNo().orNull());
		return contactGroup;
	}

	public void modifyEntity(
			final ContactGroup contactGroup,
			final GwtContactGroup gwtContactGroup)
			throws StaleObjectException {
		final Long gwtId = gwtContactGroup.getId();
		final Long dbId = contactGroup.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtContactGroup.getVersion();
		final Integer dbVer = contactGroup.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for electrode ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		copyToEntity(contactGroup, gwtContactGroup);
	}

	private void copyToEntity(
			final ContactGroup contactGroup,
			final GwtContactGroup gwtContactGroup) {
		contactGroup.setChannelPrefix(gwtContactGroup.getChannelPrefix());
		contactGroup.setElectrodeType(gwtContactGroup.getElectrodeType());
		contactGroup.setManufacturer(gwtContactGroup.getManufacturer().orNull());
		contactGroup.setModelNo(gwtContactGroup.getModelNo().orNull());
		contactGroup.setHffSetting(gwtContactGroup.getHffSetting().orNull());
		contactGroup.setImpedance(gwtContactGroup.getImpedance().orNull());
		contactGroup.setLffSetting(gwtContactGroup.getLffSetting().orNull());
		contactGroup.setLocation(gwtContactGroup.getLocation());
		contactGroup.setNotchFilter(gwtContactGroup.getNotchFilter().orNull());
		contactGroup.setSamplingRate(gwtContactGroup.getSamplingRate());
		contactGroup.setSide(gwtContactGroup.getSide());
	}
}
