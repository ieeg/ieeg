/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * * @author John Frommeyer
 * 
 */
@NamedQueries({ @NamedQuery(
		name = SpeciesEntity.FIND_BY_LABEL,
		query = "from Species s where s.label = :label") })
@Entity(name = "Species")
@Table(name = SpeciesEntity.TABLE)
public class SpeciesEntity implements IHasLongId, IHasLabel, IEntity {
	public static final String TABLE = "species";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String FIND_BY_LABEL = "Species-findByLabel";
	private Long id;
	private Integer version;
	private String label;
	private Set<StrainEntity> strains = newHashSet();

	public SpeciesEntity() {}

	public SpeciesEntity(String label) {
		this.label = checkNotNull(label);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(unique = true)
	@NotNull
	public String getLabel() {
		return label;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "parent",
			orphanRemoval = true)
	@Size(min = 1)
	public Set<StrainEntity> getStrains() {
		return strains;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@SuppressWarnings("unused")
	private void setStrains(Set<StrainEntity> strains) {
		this.strains = strains;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

	public static final EntityConstructor<SpeciesEntity, String, Void> CONSTRUCTOR =
			new EntityConstructor<SpeciesEntity,String,Void>(SpeciesEntity.class,
					new IPersistentObjectManager.IPersistentKey<SpeciesEntity,String>() {

				@Override
				public String getKey(SpeciesEntity o) {
					return o.getLabel();
				}

				@Override
				public void setKey(SpeciesEntity o, String newKey) {
					o.setLabel(newKey);
				}

			},
			new EntityPersistence.ICreateObject<SpeciesEntity,String,Void>() {

				@Override
				public SpeciesEntity create(String label, Void optParent) {
					return new SpeciesEntity(label);
				}
			}
					);

			@Override
		public EntityConstructor<SpeciesEntity, String, Void> tableConstructor() {
			return CONSTRUCTOR;
		}

}
