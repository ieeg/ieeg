/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import java.sql.Timestamp;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = SnapshotUsage.TABLE)
@NamedQueries({
		@NamedQuery(
				name = SnapshotUsage.FIND_BY_DATASET,
				query = "from SnapshotUsage sd "
						+ "where sd.snapshot = :snapId "// and sd.refersTo is
														// null "
						+ "order by time desc"),

		// Useful for threads
		@NamedQuery(
				name = SnapshotUsage.FIND_BY_DERIVED,
				query = "from SnapshotUsage sd "
						+ "where sd.snapshot = :snapId and (sd.derivedSnapshot is not null and sd.derivedSnapshot = :derivedId) "
						+ "order by time desc")
})
public class SnapshotUsage {
	public static final String TABLE = "snapshot_usage";
	public static final String UPPER_CASE_TABLE = "SNAPSHOT_USAGE";
	public static final String FIND_BY_DATASET = "SnapshotUsage-findByDataSetID";
	public static final String FIND_BY_DERIVED = "SnapshotUsage-findByDataSetDerivedID";

	// public static enum USAGE_TYPE { CREATES, ADDSTO, VIEWS, ANNOTATES,
	// DERIVES, DISCUSSES, FAVORITES };

	private Long usageId;

	private UserEntity user;
	private Integer version;
	private int usageMode;
	private DataSnapshotEntity snapshot;
	private Timestamp time;
	private DataSnapshotEntity derivedSnapshot;
	private TsAnnotationEntity annotation;

	/**
	 * For JPA.
	 */
	SnapshotUsage() {}

	public SnapshotUsage(
			final DataSnapshotEntity snap,
			final UserEntity userId,
			final int usage,
			@Nullable final DataSnapshotEntity derived,
			@Nullable final TsAnnotationEntity annId) {

		this.snapshot = snap;
		this.derivedSnapshot = derived;
		this.usageMode = usage;

		this.user = userId;
		this.annotation = annId;

		java.util.Date now = new java.util.Date();

		this.time = new Timestamp(now.getTime());
	}

	public SnapshotUsage(
			final DataSnapshotEntity snap,
			final UserEntity userId,
			final int usage,
			final DataSnapshotEntity derived,
			final TsAnnotationEntity annId,
			final Timestamp time) {

		this.snapshot = snap;
		this.derivedSnapshot = derived;
		this.usageMode = usage;

		this.user = userId;
		this.annotation = annId;

		this.time = time;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "usage_id")
	public Long getUsageId() {
		return usageId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_usage_snap")
	@JoinColumn(name = "snapshot_id")
	@NotNull
	public DataSnapshotEntity getSnapshot() {
		return snapshot;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setUsageId(final Long id) {
		this.usageId = id;
	}

	public void setSnapshot(final DataSnapshotEntity parent) {
		this.snapshot = parent;
	}

	public void setVersion(final Integer version) {
		this.version = version;
	}

	@NotNull
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_usage_derives")
	@JoinColumn(name = "derives_id")
	public DataSnapshotEntity getDerivedSnapshot() {
		return derivedSnapshot;
	}

	public void setDerivedSnapshot(DataSnapshotEntity derived) {
		derivedSnapshot = derived;
	}

	@Nullable
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_usage_annotation")
	@JoinColumn(name = "annotation_id")
	public TsAnnotationEntity getAnnotation() {
		return annotation;
	}

	public void setAnnotation(TsAnnotationEntity annotationId) {
		this.annotation = annotationId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_usage")
	@JoinColumn(name = "user_id")
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public int getUsageMode() {
		return usageMode;
	}

	public void setUsageMode(int usage) {
		this.usageMode = usage;
	}

}
