/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.metadata.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.metadata.IStrainDAO;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class StrainDAOHibernate extends GenericHibernateDAO<StrainEntity, Long>
		implements IStrainDAO {

	public StrainDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public List<StrainEntity> findByParentOrderedByLabel(SpeciesEntity parent) {
		final Session s = getSession();
		final Query q = s.getNamedQuery(StrainEntity.BY_PARENT_ORDER_BY_LABEL)
				.setParameter("parent", parent);
		@SuppressWarnings("unchecked")
		final List<StrainEntity> strains = q.list();
		return strains;
	}
}
