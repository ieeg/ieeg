/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OptimisticLock;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.MontagedChId;

/**
 * We treat a montage has an atomic unit for locking, with the exception of
 * names which are ignored. Treating it as an atomic unit requires special work
 * for the montaged channels: listeners to detect changes so that we can
 * manually increment the version number of their parent montages.
 * 
 * @author Sam Donnelly
 */
@Entity(name = "Montage")
@Table(name = MontageEntity.TABLE,
		uniqueConstraints = {
				@UniqueConstraint(
						columnNames = {
								MontageEntity.PARENT_ID_COLUMN,
								MontageEntity.NAME_COLUMN
						},
						name = "uq_montage_parent_id_name")
		})
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = MontageEntity.ID_COLUMN)
		)
})
public class MontageEntity extends LongIdAndVersion {

	public static final String TABLE = "montage";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String NAME_COLUMN = "name";
	public static final String PARENT_ID_COLUMN = "parent_id";

	private DataSnapshotEntity parent;
	private String name;
	private Set<MontagedChannelEntity> montagedChannels = newHashSet();
	private Set<LayerEntity> layers = newHashSet();

	/** For JPA. */
	MontageEntity() {}

	public MontageEntity(
			DataSnapshotEntity parent,
			String name) {
		this.parent = checkNotNull(parent);
		this.name = checkNotNull(name);
	}

	@OneToMany(mappedBy = "parent")
	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	public Set<LayerEntity> getLayers() {
		return layers;
	}

	@Transient
	public Optional<MontagedChannelEntity> getMontagedChannel(
			MontagedChId montagedChId) {
		for (MontagedChannelEntity montagedCh : montagedChannels) {
			if (montagedChId.getId().equals(montagedCh.getId())) {
				return Optional.of(montagedCh);
			}
		}
		return Optional.absent();
	}

	@OptimisticLock(excluded = false)
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<MontagedChannelEntity> getMontagedChannels() {
		return montagedChannels;
	}

	@OptimisticLock(excluded = true)
	@Column(name = NAME_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getName() {
		return name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = PARENT_ID_COLUMN)
	@NotNull
	public DataSnapshotEntity getParent() {
		return parent;
	}

	@SuppressWarnings("unused")
	private void setLayers(Set<LayerEntity> layers) {
		this.layers = layers;
	}

	@SuppressWarnings("unused")
	private void setMontagedChannels(Set<MontagedChannelEntity> montagedChannels) {
		this.montagedChannels = montagedChannels;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setParent(DataSnapshotEntity parent) {
		this.parent = parent;
	}
}
