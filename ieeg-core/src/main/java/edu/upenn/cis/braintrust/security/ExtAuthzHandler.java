/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceCannotSerializeException;

public final class ExtAuthzHandler implements IExtAuthzHandler {

	private final Logger logger = LoggerFactory
			.getLogger(ExtAuthzHandler.class);
	
	IGraphServer crService;
	
	public ExtAuthzHandler(IGraphServer cr) {
		crService = cr;
	}

	@Override
	public void checkPermitted(
			ExtPermissions perms,
			AllowedAction attemptedAction,
			String optSnapshotId)
			throws AuthorizationException {
		if (!isPermitted(perms, attemptedAction, optSnapshotId)) {
			throw new UnauthorizedException("user ["
					+ perms.getUser().getUsername()
					+ "] is not permitted [" + attemptedAction + "] on ["
					+ perms.getTargetName() + "]");
		}
	}

	@Override
	public boolean isPermitted(
			ExtPermissions perms,
			AllowedAction action,
			String optSnapshotId) {
		String m = "isPermitted(...)";
		checkNotNull(perms);
		checkNotNull(action);

		final User user = perms.getUser();

		try {
			if (user.getRoles().contains(Role.ADMIN) ||
					(optSnapshotId != null && crService != null && crService.isAdmin(user, optSnapshotId))) {
				logger.debug(
						"{}: user [{}] is allowed to [{}] because they are an admin",
						new Object[] {
								m,
								user.getUsername(),
								action });
				return true;
			}
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		if (!perms.getUserAce().isPresent()) {
			Set<ExtPermission> userPerms = new HashSet<ExtPermission>();
			try {
				if (isActionPermitted(userPerms, action, user, optSnapshotId))
					return true;
			} catch (Throwable t) {
				t.printStackTrace();
			}

			logger.debug(
					"{}: user [{}] has no user aces in acl, falling back to project group perms",
					new Object[] {
							m,
							user.getUsername() });
		} else {
			ExtUserAce userAce = perms.getUserAce().get();
			Set<ExtPermission> userPerms = userAce.getPerms();
			checkArgument(!userPerms.isEmpty(),
					"User ace perms cannot be empty");
			if (isActionPermitted(userPerms, action, user, optSnapshotId))
				return true;
			else
				return false;
		}
		// Shouldn't we fall-through? No, we wanted user ace to be able to override project ace for particular users in a project.

		final Set<ExtProjectAce> projectAces = perms.getProjectAces();
		if (projectAces.isEmpty()) {
			logger.debug(
					"{}: user [{}] has no project aces in acl, falling back to world perms",
					new Object[] {
							m,
							user.getUsername() });
		} else {
			logger.debug("{}: Checking {} project aces", m, projectAces.size());
			for (final ExtProjectAce projectAce : projectAces) {
				final Set<ExtPermission> projectPerms = projectAce.getPerms();
				checkArgument(!projectPerms.isEmpty(),
						"Project ace perms cannot be empty");
				boolean isPermitted = isActionPermitted(projectPerms, action, user, optSnapshotId);
				if (isPermitted) {
					logger.debug("{}: Allowing {} because of {}", new Object[] {
							m,
							action,
							projectAce
					});
					return true;
				}
			}
			logger.debug("{}: Not allowing {} based on project aces: {}",
					new Object[] {
							m,
							action,
							projectAces
					});
			return false;
		}

		if (perms.getWorldAce().isPresent()
				&& user.getRoles().contains(Role.USER)) {
			ExtWorldAce worldAce = perms.getWorldAce().get();
			Set<ExtPermission> worldPerms = worldAce.getPerms();
			checkArgument(!worldPerms.isEmpty(),
					"World ace perms cannot be empty");
			for (ExtPermission worldPerm : worldPerms) {
				checkArgument(
						worldPerm.equals(CorePermDefs.READ_PERM)
								|| worldPerm.equals(CorePermDefs.EDIT_PERM),
						"illegal world perm " + worldPerm.toString());
			}
			return isActionPermitted(worldPerms, action, user, optSnapshotId);
		}

		logger.debug(
				"{}: user [{}] is not allowed to [{}] because don't have the permission in the ACL",
				new Object[] {
						m,
						user.getUsername(),
						action });
		return false;
	}

	private boolean isActionPermitted(Set<ExtPermission> perms, AllowedAction action, User user, String optSnapshotId) {
		if (action.equals(CorePermDefs.READ)) {
			return isReadPermitted(perms, user, optSnapshotId);
		} else if (action.equals(CorePermDefs.EDIT)) {
			return isEditPermitted(perms, user, optSnapshotId);
		} else if (action.equals(CorePermDefs.DELETE)) {
			return isOwner(perms, user, optSnapshotId);
		} else if (action.equals(CorePermDefs.EDIT_ACL)) {
			return isOwner(perms, user, optSnapshotId);
		} else {
			throw new IllegalArgumentException("illegal action "
					+ action.toString());
		}
	}

	private boolean isOwner(Set<ExtPermission> perms, User user, String optSnapshotId) {
		try {
			if ((optSnapshotId != null && crService != null && crService.isAdmin(user, optSnapshotId)))
				return true;
		} catch (PersistenceCannotSerializeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (edu.upenn.cis.db.mefview.shared.UnauthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (ExtPermission perm : perms) {
			if (perm.equals(CorePermDefs.OWNER_PERM)) {
				return true;
			}
		}
		return false;
	}

	private boolean isEditPermitted(Set<ExtPermission> perms, User user, String optSnapshotId) {
		try {
			if ((optSnapshotId != null && crService != null && crService.isUpdater(user, optSnapshotId)))
				return true;
		} catch (PersistenceCannotSerializeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (edu.upenn.cis.db.mefview.shared.UnauthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		for (ExtPermission perm : perms) {
			if (perm.equals(CorePermDefs.EDIT_PERM)
					|| perm.equals(CorePermDefs.OWNER_PERM)) {
				return true;
			}
		}
		return false;
	}

	private boolean isReadPermitted(Set<ExtPermission> perms, User user, String optSnapshotId) {
		try {
			if ((optSnapshotId != null && crService != null && crService.isReader(user, optSnapshotId)))
				return true;
		} catch (PersistenceCannotSerializeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (edu.upenn.cis.db.mefview.shared.UnauthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		for (ExtPermission perm : perms) {
			if (perm.equals(CorePermDefs.READ_PERM)
					|| perm.equals(CorePermDefs.EDIT_PERM)
					|| perm.equals(CorePermDefs.OWNER_PERM)) {
				return true;
			}
		}
		return false;
	}
}
