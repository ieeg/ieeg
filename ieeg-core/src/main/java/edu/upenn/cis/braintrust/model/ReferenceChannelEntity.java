/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Embeddable
@Immutable
public class ReferenceChannelEntity {

	public static final String TABLE = "reference_channel";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String CHANNEL_ID_COLUMN = "channel_id";

	private TimeSeriesEntity channel;
	private Double coefficient;

	/** For JPA. */
	ReferenceChannelEntity() {}

	public ReferenceChannelEntity(
			Double coefficent,
			TimeSeriesEntity channel) {
		this.coefficient = checkNotNull(coefficent);
		this.channel = checkNotNull(channel);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ReferenceChannelEntity)) {
			return false;
		}
		ReferenceChannelEntity other = (ReferenceChannelEntity) obj;
		if (channel == null) {
			if (other.channel != null) {
				return false;
			}
		} else if (!channel.equals(other.channel)) {
			return false;
		}
		if (coefficient == null) {
			if (other.coefficient != null) {
				return false;
			}
		} else if (!coefficient.equals(other.coefficient)) {
			return false;
		}
		return true;
	}

	/**
	 * Need optional = false to get not-null in hbm2ddl table.
	 */
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = CHANNEL_ID_COLUMN)
	@NotNull
	public TimeSeriesEntity getChannel() {
		return channel;
	}

	/**
	 * Need nullable = false to get not-null in hbm2ddl table.
	 */
	@Column(nullable = false)
	@NotNull
	public Double getCoefficient() {
		return coefficient;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((channel == null) ? 0 : channel.hashCode());
		result = prime * result
				+ ((coefficient == null) ? 0 : coefficient.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	private void setChannel(TimeSeriesEntity channel) {
		this.channel = channel;
	}

	@SuppressWarnings("unused")
	private void setCoefficient(Double coefficent) {
		this.coefficient = coefficent;
	}
}
