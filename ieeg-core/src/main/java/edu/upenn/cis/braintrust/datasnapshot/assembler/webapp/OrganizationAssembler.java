/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class OrganizationAssembler {

	public GwtOrganization buildDto(
			final OrganizationEntity organization) {
		checkNotNull(organization);
		GwtOrganization gwtOrganization = new GwtOrganization(
				organization.getName(),
				organization.getCode(),
				organization.getId(),
				organization.getVersion());
		return gwtOrganization;
	}

	public OrganizationEntity buildEntity(
			final GwtOrganization gwtOrganization) {
		checkNotNull(gwtOrganization);
		checkArgument(gwtOrganization.getId() == null);
		checkArgument(gwtOrganization.getVersion() == null);
		final OrganizationEntity organization = new OrganizationEntity(
				gwtOrganization.getName(),
				gwtOrganization.getCode(),
				new ExtAclEntity());
		return organization;
	}

	public void modifyEntity(
			final OrganizationEntity organization,
			final GwtOrganization gwtOrganization)
			throws StaleObjectException {
		checkNotNull(gwtOrganization);
		checkNotNull(organization);
		final Long dtoId = gwtOrganization.getId();
		final Long entityId = organization.getId();
		if (!dtoId.equals(entityId)) {
			throw new IllegalArgumentException("DTO id: [" + dtoId
					+ "] does not match entity id: [" + entityId + "].");
		}
		final Integer dtoV = gwtOrganization.getVersion();
		final Integer entityV = organization.getVersion();
		if (!dtoV.equals(entityV)) {
			throw new StaleObjectException(
					"Version mismatch for strain ["
							+ entityId + "]. DTO version: [" + dtoV
							+ "], DB version: [" + entityV + "].");
		}
		copyToEntity(organization, gwtOrganization);
	}

	private void copyToEntity(
			final OrganizationEntity organization,
			final GwtOrganization gwtOrganization) {
		organization.setName(gwtOrganization.getName());
	}
}
