/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.drupal.model.DrupalProfileField;
import edu.upenn.cis.braintrust.drupal.model.DrupalProfileValue;
import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.thirdparty.cache.CacheWrapper;
import edu.upenn.cis.db.mefview.shared.LatLon;

public final class DrupalUserService implements IUserService {
	
	private final SessionFactory sessionFac;

	private final static Logger logger =
			LoggerFactory.getLogger(DrupalUserService.class);
	private final CacheWrapper<UserId, User> idToUser;

	private final CacheWrapper<String, User> nameToUser;

	/**
	 * Inject the {@code SessionFactory} and caches for testing. One use case is
	 * to use mocking to get a session that throws an exception. So that we can
	 * verify that the session is closed if an exception is thrown.
	 * 
	 * @param drupalSessionFac {@code SessionFactory} this service should use
	 *            for creating {@code Session}s.
	 * @param idToUser
	 * @param nameToUser
	 */
	@VisibleForTesting
	public DrupalUserService(
			final SessionFactory drupalSessionFac,
			final CacheWrapper<UserId, User> idToUser,
			final CacheWrapper<String, User> nameToUser) {
		this.sessionFac = checkNotNull(drupalSessionFac);
		this.idToUser = checkNotNull(idToUser);
		this.nameToUser = checkNotNull(nameToUser);
	}

	@Override
	public long countEnabledUsers() {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessionFac.openSession();
			trx = sess.beginTransaction();
			Long enabledCount = (Long)
					sess
					.createQuery(
							"select count(*) from DrupalUser u where u.status = 1")
							.uniqueResult();
			PersistenceUtil.commit(trx);
			return enabledCount;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
	
//	public static class StateCountry {
//		@Column(name="St")
//		String state;
//		
//		@Column(name="Cou")
//		String country;
//	}
	
	/*
	 * CREATE TABLE `geo` (
  `state` varchar(30) NOT NULL,
  `country` varchar(45) NOT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL
  PRIMARY KEY (`state`,`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	 */
	
	Map<String, LatLon> cache = new HashMap<String, LatLon>();
	
	final Geocoder geocoder = new Geocoder();
	
	public List<LatLon> findLatLon() {
		final String m = "findLatLon()";
		List<String> locations = findUserStates();
		List<LatLon> ret = new ArrayList<LatLon>();
		
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessionFac.openSession();
			trx = sess.beginTransaction();
			
			for (String addr: locations) {
				String[] stCou = addr.split(",");
				
				if (stCou.length < 2)
					continue;
				
				LatLon loc = cache.get(addr);
				if (loc != null)
					ret.add(loc);
				else {
					
					String state = stCou[0];
					String country = stCou[1];
					
					// Skip nonsensical stuff
					if (state.length() < 2)
						state = null;
					
					if (country.length() < 2)
						country = null;

					SQLQuery q = null;
					if (country != null && state != null) {
						q = sess.createSQLQuery("SELECT lat, lon from geo WHERE state=:st AND country=:cou"
							);
						
						q.setParameter("st", state);
						q.setParameter("cou", country);
					
					} else if (country != null) {
						q = sess.createSQLQuery("SELECT lat, lon from geo WHERE country=:cou"
								);
						q.setParameter("cou", country);
						
					}
					try {
						if (q != null) {
							List<Object[]> results = null;
							
							try {
								results = q.list();
							} catch (Exception e) {
								logger.error(m + ": Error looking querying DB for geo info, continuing with empty list", e);
								
								results = new ArrayList<Object[]>();
							}
							
							if (results.isEmpty()) {
								logger.debug("{}: Geocode request for {}", m, addr);
								GeocoderRequest geocoderReq = 
										new GeocoderRequestBuilder().setAddress(addr).setLanguage("en").getGeocoderRequest();
								
								GeocodeResponse resp = geocoder.geocode(geocoderReq);
								
								if (!resp.getResults().isEmpty()) {
									GeocoderResult res = resp.getResults().get(0);
									
									loc = new LatLon(res.getGeometry().getLocation().getLat().doubleValue(),
											res.getGeometry().getLocation().getLng().doubleValue());

									logger.debug("{}: Geocode response for {}: {}", m, addr, loc);
									try {
										SQLQuery q2 = sess.createSQLQuery("insert into geo(state,country,lat,lon) " +
												" values(:st, :cou, :lat, :lon)");
										
										q2.setParameter("st", state);
										q2.setParameter("cou", country);
										q2.setParameter("lat", loc.getLat());
										q2.setParameter("lon", loc.getLon());
										
										q2.executeUpdate();
									} catch (Exception e2) {
										logger.error(m + ": Exception inserting geo info, continuing", e2);
									}
								}
							} else {
								loc = new LatLon((Double)results.get(0)[0], 
										(Double)results.get(0)[1]);
							}
							if (loc != null) {
								cache.put(addr, loc);
								ret.add(loc);
							}
						}
					} catch (Exception e) {
						logger.error(m + ": Exception looking up geo info for [" + addr + "], continuing", e);
						
					}
				}
			}

			trx.commit();
			return ret;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(sess);
		}
	}
	
	public List<String> findUserStates() {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessionFac.openSession();
			trx = sess.beginTransaction();

			SQLQuery q = sess.createSQLQuery("SELECT S.value AS St, C.value AS Cou " +
					"FROM profile_values S, profile_values C, users U " +
					"where S.uid = C.uid and U.status=1 and U.uid = S.uid and S.fid = 13 and C.fid = 7"
					);

			List<Object[]> results = q.list();
			trx.commit();
			
			List<String> resultsString = new ArrayList<String>();
			for (Object[] sc: results)
				resultsString.add(sc[0] + "," + sc[1]);
			return resultsString;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Override
	public UsersAndTotalCount findEnabledUsers(
			int start,
			int length,
			Set<UserId> excludes) {
		Session sess = null;
		Transaction trx = null;
		try {
			sess = sessionFac.openSession();
			trx = sess.beginTransaction();

			final Set<Long> excludedIds =
					newHashSet(transform(excludes, UserId.getValue));

			Query q = null;
			if (excludes.size() == 0) {
				q = sess.createQuery("from DrupalUser u where u.status = 1 order by u.name");
			} else {
				q = sess
						.createQuery(
								"from DrupalUser u where u.status = 1 and u.id not in (:excludes) order by u.name")
								.setParameterList(
										"excludes",
										excludedIds);
			}
			q.setFirstResult(start).setMaxResults(length);

			@SuppressWarnings("unchecked")
			List<DrupalUser> drupalUsers = q.list();

			Query countQ = null;
			if (excludes.size() == 0) {
				countQ = sess
						.createQuery(
								"select count(*) from DrupalUser u where u.status = 1");
			} else {
				countQ = sess
						.createQuery(
								"select count(*) from DrupalUser u where u.status = 1 and u.id not in (:excludes)")
								.setParameterList("excludes", excludedIds);
			}
			Long totalCount = (Long) countQ.uniqueResult();
			trx.commit();
			return new UsersAndTotalCount(
					Drupal2BrainTrust.drupalUsers2BrainTrustUsers(drupalUsers),
					totalCount);
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(trx);
			throw re;
		} finally {
			PersistenceUtil.close(sess);
		}
	}

	@Override
	public User findExistingEnabledUser(String username)
			throws AuthorizationException {
		User user = findUserByUsername(username);
		if (user == null) {
			throw new IncorrectCredentialsException(
					"Username "
							+ username
							+ " not found");
		}
		if (user.getStatus() == User.Status.DISABLED) {
			throw new DisabledAccountException("User "
					+ user.getUsername()
					+ " disabled");
		}
		return user;
	}

	private User findInDbByUid(
			final UserId userId,
			final Session session) {
		DrupalUser drupalUser = (DrupalUser) session.get(
				DrupalUser.class,
				userId.getValue());
		if (drupalUser == null) {
			return null;
		}

		User user = Drupal2BrainTrust.drupalUser2BrainTrustUser(
				drupalUser);

		return user;
	}

	private User findInDbByUsername(final String username) {

		Session drupalSession = null;
		Transaction drupalTrx = null;

		try {
			drupalSession = sessionFac.openSession();
			drupalTrx = drupalSession.beginTransaction();

			final DrupalUser drupalUser =
					(DrupalUser) drupalSession
					.getNamedQuery(DrupalUser.FIND_BY_NAME)
					.setReadOnly(true)
					.setParameter("name", username)
					.uniqueResult();

			if (drupalUser == null) {
				return null;
			}

			User user = Drupal2BrainTrust.drupalUser2BrainTrustUser(
					drupalUser);

			drupalTrx.commit();
			return user;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(drupalTrx);
			throw re;
		} finally {
			PersistenceUtil.close(drupalSession);
		}
	}

	@Override
	public User findUserByEmail(final String email) {

		Session drupalSession = null;
		Transaction drupalTrx = null;

		try {
			drupalSession = sessionFac.openSession();
			drupalTrx = drupalSession.beginTransaction();

			final DrupalUser drupalUser =
					(DrupalUser) drupalSession
					.getNamedQuery(DrupalUser.FIND_BY_EMAIL)
					.setReadOnly(true)
					.setParameter("mail", email)
					.uniqueResult();

			if (drupalUser == null) {
				return null;
			}

			User user = Drupal2BrainTrust.drupalUser2BrainTrustUser(
					drupalUser);

			drupalTrx.commit();
			return user;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(drupalTrx);
			throw re;
		} finally {
			PersistenceUtil.close(drupalSession);
		}
	}

	@Override
	public User findUserByUid(final UserId userId) {
		checkNotNull(userId);
		User user = idToUser.get(userId);
		if (user == null) {
			user = findUserInDbByUid(userId);
			if (user != null) {
				idToUser.put(userId, user);
				nameToUser.put(user.getUsername(), user);
			}
		}
		return user;
	}

	@Override
	public User findUserByUsername(String username) {
		checkNotNull(username);
		User user = nameToUser.get(username);
		if (user == null) {
			user = findInDbByUsername(username);
			if (user != null) {
				nameToUser.put(username, user);
				idToUser.put(user.getUserId(), user);
			}
		}
		return user;
	}

	@Override
	public User createOrUpdateUser(User newUser) {
		final Session drupalSession = sessionFac.openSession();
		final Transaction drupalTrx = drupalSession.beginTransaction();

		try {

			DrupalUser userToUpdate = null;
			if (newUser.getUserId() != null && newUser.getUserId().getValue() > -1) {
				userToUpdate = (DrupalUser) drupalSession.get(DrupalUser.class, newUser.getUserId().getValue());
			}

			if (userToUpdate == null) {
				userToUpdate = new DrupalUser();
			}

			Drupal2BrainTrust.DrupalFields actor = new Drupal2BrainTrust.DrupalFields() {
				@Override
				public DrupalProfileValue createValue(DrupalUser dUser, DrupalProfileField f, String value) {

					if (dUser.getProfile().get(f) != null) {
						dUser.getProfile().get(f).setValue(value);
						drupalSession.update(dUser.getProfile().get(f));
						drupalSession.update(dUser);
						return dUser.getProfile().get(f);
					} else {
						DrupalProfileValue.Id id = new DrupalProfileValue.Id();

						id.setFid(f.getFid().longValue());
						id.setUid(dUser.getUid());
						DrupalProfileValue dpf = new DrupalProfileValue(dUser, value);
						dpf.setId(id);

						dUser.getProfile().put(f, dpf);
						drupalSession.save(dpf);
						drupalSession.update(dUser);
						return dpf;
					}
				}

				@Override
				public DrupalProfileField getField(DrupalUser user, String fieldName) {
					// Try to return from the existing set if possible
					for (DrupalProfileField dpf : user.getProfile().keySet())
						if (dpf.getName().equals(fieldName))
							return dpf;

					// Else query
					Query q = drupalSession.createQuery("from DrupalProfileField f where name = :name")
							.setString("name", fieldName);
					DrupalProfileField ret = (DrupalProfileField) q.uniqueResult();
					return ret;
				}

			};

			Drupal2BrainTrust.userInfo2DrupalUser(newUser,
					userToUpdate, actor, false);

			if (newUser.getUserId() == null || newUser.getUserId().getValue() == -1)
				userToUpdate.setUid(null);

			drupalSession.saveOrUpdate(userToUpdate);

			// Now that we've saved or updated the Drupal user, we need to get
			// it (and its ID) to add the profile info
			DrupalUser theUser = (DrupalUser) drupalSession.createQuery("from DrupalUser u where name=:name")
					.setString("name", newUser.getUsername()).uniqueResult();

			Drupal2BrainTrust.userInfo2DrupalUserProfile(newUser, theUser, actor);

			drupalSession.saveOrUpdate(theUser);
			User user = Drupal2BrainTrust.drupalUser2BrainTrustUser(theUser);

			drupalTrx.commit();
			return user;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(drupalTrx);
			throw re;
		} finally {
			PersistenceUtil.close(drupalSession);
		}
	}

	private User findUserInDbByUid(final UserId userId) {

		Session drupalSession = null;
		Transaction drupalTrx = null;

		try {
			drupalSession = sessionFac.openSession();
			drupalTrx = drupalSession.beginTransaction();

			User user = findInDbByUid(userId, drupalSession);

			drupalTrx.commit();
			return user;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(drupalTrx);
			throw re;
		} finally {
			PersistenceUtil.close(drupalSession);
		}
	}

	@Override
	public Map<UserId, Optional<User>> findUsersByUid(Iterable<UserId> userIds) {
		checkNotNull(userIds);

		Map<UserId, Optional<User>> idToOptUser = newHashMap();
		Set<UserId> needFromDb = newHashSet();
		for (final UserId userId : userIds) {
			final User user = idToUser.get(userId);
			if (user == null) {
				needFromDb.add(userId);
			} else {
				idToOptUser.put(userId, Optional.of(user));
			}
		}

		Map<UserId, Optional<User>> fromDb = findUsersInDbByUid(needFromDb);
		for (final Map.Entry<UserId, Optional<User>> entry : fromDb.entrySet()) {
			idToOptUser.put(entry.getKey(), entry.getValue());
			if (entry.getValue().isPresent()) {
				final User user = entry.getValue().get();
				idToUser.put(entry.getKey(), user);
				nameToUser.put(user.getUsername(), user);
			}
		}

		return idToOptUser;

	}

	private Map<UserId, Optional<User>> findUsersInDbByUid(
			final Iterable<UserId> userIds) {

		Session drupalSession = null;
		Transaction drupalTrx = null;
		Map<UserId, Optional<User>> idToOptUser = newHashMap();
		try {
			drupalSession = sessionFac.openSession();
			drupalTrx = drupalSession.beginTransaction();

			for (final UserId userId : userIds) {
				User user = findInDbByUid(userId, drupalSession);
				idToOptUser.put(userId, Optional.fromNullable(user));
			}
			drupalTrx.commit();
			return idToOptUser;
		} catch (RuntimeException re) {
			PersistenceUtil.rollback(drupalTrx);
			throw re;
		} finally {
			PersistenceUtil.close(drupalSession);
		}
	}

}
