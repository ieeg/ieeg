/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NODE_REVISIONS")
public class DrupalNodeRevision {
	private int nid;
	private int vid;
	private int uid;
	private String title;
	private String body;
	private String teaser;
	private String log;
	private int timestamp;
	private int format;

	@Column(unique = true, columnDefinition = "INT(10)")
	public int getNid() {
		return nid;
	}

	@Id
	@GeneratedValue
	@Column(columnDefinition = "INT(10)")
	public int getVid() {
		return vid;
	}

	@Column(columnDefinition = "INT(11)")
	public int getUid() {
		return uid;
	}

	@Column(length = 255)
	public String getTitle() {
		return title;
	}

	@Column(columnDefinition = "longtext")
	public String getBody() {
		return body;
	}

	@Column(columnDefinition = "longtext")
	public String getTeaser() {
		return teaser;
	}

	@Column(columnDefinition = "longtext")
	public String getLog() {
		return log;
	}

	@Column(columnDefinition = "int(11)")
	public int getTimestamp() {
		return timestamp;
	}

	@Column(columnDefinition = "int(11)")
	public int getFormat() {
		return format;
	}

	public void setNid(int nid) {
		this.nid = nid;
	}

	public void setVid(int vid) {
		this.vid = vid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setTeaser(String teaser) {
		this.teaser = teaser;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public void setFormat(int format) {
		this.format = format;
	}

}
