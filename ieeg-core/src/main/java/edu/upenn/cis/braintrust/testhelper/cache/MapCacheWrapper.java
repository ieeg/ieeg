/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.testhelper.cache;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

import edu.upenn.cis.braintrust.thirdparty.cache.CacheWrapper;

/**
 * A cache wrapper that uses a Map. For testing.
 * 
 * @author John Frommeyer
 * @deprecated Use EhCacheWrapper instead. Eventually the CacheWrapper interface itself will go away.
 */
@Deprecated
public class MapCacheWrapper<K, V> implements CacheWrapper<K, V> {

	private final Map<K, V> cache = newHashMap();

	@Override
	public void put(K key, V value) {
		final String M = "put(...)";
		cache.put(key, value);

	}

	@Override
	public V get(K key) {
		final String M = "get(...)";
		return cache.get(key);
	}

}
