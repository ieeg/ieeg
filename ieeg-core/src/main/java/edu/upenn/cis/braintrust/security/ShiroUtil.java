/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import org.apache.shiro.subject.Subject;

import edu.umd.cs.findbugs.annotations.CheckForNull;

/**
 * Utility methods for dealing with Shiro.
 * 
 * @author John Frommeyer
 * 
 */
public class ShiroUtil {

	/**
	 * The action part of Shiro permissions.
	 * 
	 * @author John Frommeyer
	 * 
	 */
	public enum ShiroAction {
		CREATE("create"), READ("read"), UPDATE("update"), DELETE("delete");
		private final String action;

		private ShiroAction(String action) {
			this.action = action;
		}

		public String toString() {
			return action;
		}
	}

	/**
	 * Returns {@code true} if the current subject is authenticated and has
	 * permission to perform {@code action} on instance {@code id} of type
	 * {@code clazz}. Otherwise returns {@code false}.
	 * 
	 * @param clazz
	 * @param action
	 * @param id
	 * @return {@code true} if the current subject is authenticated and has
	 *         permission to perform {@code action} on instance {@code id} of
	 *         type {@code clazz}
	 */
	public static boolean isAuthcAndHasPermission(Subject subject,
			Class<?> clazz,
			ShiroAction action, Serializable id) {
		checkNotNull(id);
		return isAuthcAndHasPermissionImpl(subject, clazz, action, id);
	}

	/**
	 * Returns {@code true} if the current subject is authenticated and has
	 * permission to create an instance of type {@code clazz}. Otherwise returns
	 * {@code false}.
	 * 
	 * @param clazz
	 * @return {@code true} if the current subject is authenticated and has
	 *         permission to create an instance of type {@code clazz}
	 */
	public static boolean isAuthcAndHasCreatePermission(Subject subject,
			Class<?> clazz) {
		return isAuthcAndHasPermissionImpl(subject, clazz, ShiroAction.CREATE,
				null);
	}

	private static boolean isAuthcAndHasPermissionImpl(
			Subject subject,
			Class<?> clazz,
			ShiroAction action,
			@CheckForNull Serializable id) {
		// Subject currentUser = SecurityUtils.getSubject();
		String permissionString = newPermissionString(clazz, action, id);
		if (subject.isAuthenticated()
				&& subject.isPermitted(permissionString)) {
			return true;
		}
		return false;
	}

	/**
	 * Returns a Shiro-style permission string for performing {@code action} on
	 * an instance of class {@code clazz} with id {@code id}. If {@code id ==
	 * null}, then the permission string contains no id.
	 * 
	 * @param clazz
	 * @param action
	 * @param id
	 * @return a Shiro-style permission string
	 */
	public static String newPermissionString(Class<?> clazz,
			ShiroAction action, @CheckForNull Serializable id) {
		checkNotNull(clazz);
		checkNotNull(action);
		String permissionString = (id == null) ? (clazz.getSimpleName() + ":" + action
				.toString())
				: (clazz.getSimpleName() + ":" + action.toString() + ":" + id);
		return permissionString;
	}

	private ShiroUtil() {
		throw new AssertionError(ShiroUtil.class.getSimpleName()
				+ " should not be instantiated.");
	}
}
