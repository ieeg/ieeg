/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

@GwtCompatible(serializable = true)
public class ChannelInfoDto implements SerializableMetadata, IHasStringId, INamedTimeSegment {

	private static final long serialVersionUID = 1L;

	IDataset parent = null;
	String id;

	// TimeSeriesInfo ID
	private Long internalId;

	private String institution;
	private Integer headerLength;
	private String subjectFirstName;
	private String subjectSecondName;
	private String subjectThirdName;
	private String subjectId;
	private Long numberOfSamples;
	private String channelName;
	private Long recordingStartTime;
	private Long recordingEndTime;
	private Double samplingFrequency;
	private Double lowFrequencyFilterSetting;
	private Double highFrequencyFilterSetting;
	private Double notchFilterFrequency;
	private String acquisitionSystem;
	private String channelComments;
	private String studyComments;
	private Integer physicalChannelNumber;
	private Long maximumBlockLength;
	private Long blockInterval;
	private Integer blockHeaderLength;
	private Integer maximumDataValue;
	private Integer minimumDataValue;
	
	private String fileKey;

	private Double voltageConversionFactor;
	
	private Double baseline;
	private Double multiplier;
	private String units;
	private String modality;
	private long timeOffset = 0;

	@Override
	public String getId() {
		return id;
	}

	public Long getInternalId() {
		return internalId;
	}

	public void setInternalId(Long internalId) {
		this.internalId = internalId;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Integer getHeaderLength() {
		return headerLength;
	}

	public void setHeaderLength(Integer headerLength) {
		this.headerLength = headerLength;
	}

	public String getSubjectFirstName() {
		return subjectFirstName;
	}

	public void setSubjectFirstName(String subjectFirstName) {
		this.subjectFirstName = subjectFirstName;
	}

	public String getSubjectSecondName() {
		return subjectSecondName;
	}

	public void setSubjectSecondName(String subjectSecondName) {
		this.subjectSecondName = subjectSecondName;
	}

	public String getSubjectThirdName() {
		return subjectThirdName;
	}

	public void setSubjectThirdName(String subjectThirdName) {
		this.subjectThirdName = subjectThirdName;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public Long getNumberOfSamples() {
		return numberOfSamples;
	}

	public void setNumberOfSamples(Long numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Long getRecordingStartTime() {
		return recordingStartTime;
	}

	public void setRecordingStartTime(Long recordingStartTime) {
		this.recordingStartTime = recordingStartTime;
	}

	public Long getRecordingEndTime() {
		return recordingEndTime;
	}

	public void setRecordingEndTime(Long recordingEndTime) {
		this.recordingEndTime = recordingEndTime;
	}

	public Double getSamplingFrequency() {
		return samplingFrequency;
	}

	public void setSamplingFrequency(Double samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public Double getLowFrequencyFilterSetting() {
		return lowFrequencyFilterSetting;
	}

	public void setLowFrequencyFilterSetting(Double lowFrequencyFilterSetting) {
		this.lowFrequencyFilterSetting = lowFrequencyFilterSetting;
	}

	public Double getHighFrequencyFilterSetting() {
		return highFrequencyFilterSetting;
	}

	public void setHighFrequencyFilterSetting(Double highFrequencyFilterSetting) {
		this.highFrequencyFilterSetting = highFrequencyFilterSetting;
	}

	public Double getNotchFilterFrequency() {
		return notchFilterFrequency;
	}

	public void setNotchFilterFrequency(Double notchFilterFrequency) {
		this.notchFilterFrequency = notchFilterFrequency;
	}

	public String getAcquisitionSystem() {
		return acquisitionSystem;
	}

	public void setAcquisitionSystem(String acquisitionSystem) {
		this.acquisitionSystem = acquisitionSystem;
	}

	public String getChannelComments() {
		return channelComments;
	}

	public void setChannelComments(String channelComments) {
		this.channelComments = channelComments;
	}

	public String getStudyComments() {
		return studyComments;
	}

	public void setStudyComments(String studyComments) {
		this.studyComments = studyComments;
	}

	public Integer getPhysicalChannelNumber() {
		return physicalChannelNumber;
	}

	public void setPhysicalChannelNumber(Integer physicalChannelNumber) {
		this.physicalChannelNumber = physicalChannelNumber;
	}

	public Long getMaximumBlockLength() {
		return maximumBlockLength;
	}

	public void setMaximumBlockLength(Long maximumBlockLength) {
		this.maximumBlockLength = maximumBlockLength;
	}

	public Long getBlockInterval() {
		return blockInterval;
	}

	public void setBlockInterval(Long blockInterval) {
		this.blockInterval = blockInterval;
	}

	public Integer getBlockHeaderLength() {
		return blockHeaderLength;
	}

	public void setBlockHeaderLength(Integer blockHeaderLength) {
		this.blockHeaderLength = blockHeaderLength;
	}

	public Integer getMaximumDataValue() {
		return maximumDataValue;
	}

	public void setMaximumDataValue(Integer maximumDataValue) {
		this.maximumDataValue = maximumDataValue;
	}

	public Integer getMinimumDataValue() {
		return minimumDataValue;
	}

	public void setMinimumDataValue(Integer minimumDataValue) {
		this.minimumDataValue = minimumDataValue;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	public ChannelInfoDto() {}
	
	public ChannelInfoDto(
			String id,
			@Nullable Long internalId,
			@Nullable String institution,
			@Nullable Integer headerLength,
			@Nullable String subjectFirstName,
			@Nullable String subjectSecondName,
			@Nullable String subjectThirdName,
			@Nullable String subjectId,
			@Nullable Long numberOfSamples,
			@Nullable String channelName,
			@Nullable Long recordingStartTime,
			@Nullable Long recordingEndTime,
			@Nullable Double samplingFrequency,
			@Nullable Double lowFrequencyFilterSetting,
			@Nullable Double highFrequencyFilterSetting,
			@Nullable Double notchFilterFrequency,
			@Nullable String acquisitionSystem,
			@Nullable String channelComments,
			@Nullable String studyComments,
			@Nullable Integer physicalChannelNumber,
			@Nullable Long maximumBlockLength,
			@Nullable Long blockInterval,
			@Nullable Integer blockHeaderLength,
			@Nullable Integer maximumDataValue,
			@Nullable Integer minimumDataValue,
			@Nullable Double voltageConversionFactor,
			@Nullable String fileKey) {
		this.id = id;
		this.internalId = internalId;
		this.institution = institution;
		this.headerLength = headerLength;
		this.subjectFirstName = subjectFirstName;
		this.subjectSecondName = subjectSecondName;
		this.subjectThirdName = subjectThirdName;
		this.subjectId = subjectId;
		this.numberOfSamples = numberOfSamples;
		this.channelName = channelName;
		this.recordingStartTime = recordingStartTime;
		this.recordingEndTime = recordingEndTime;
		this.samplingFrequency = samplingFrequency;
		this.lowFrequencyFilterSetting = lowFrequencyFilterSetting;
		this.highFrequencyFilterSetting = highFrequencyFilterSetting;
		this.notchFilterFrequency = notchFilterFrequency;
		this.acquisitionSystem = acquisitionSystem;
		this.channelComments = channelComments;
		this.studyComments = studyComments;
		this.physicalChannelNumber = physicalChannelNumber;
		this.maximumBlockLength = maximumBlockLength;
		this.blockInterval = blockInterval;
		this.blockHeaderLength = blockHeaderLength;
		this.maximumDataValue = maximumDataValue;
		this.minimumDataValue = minimumDataValue;
		this.voltageConversionFactor = voltageConversionFactor;
		this.fileKey = fileKey;
	}

	public Double getVoltageConversionFactor() {
		return voltageConversionFactor;
	}

	public void setVoltageConversionFactor(Double d) {
		voltageConversionFactor = d;
	}

	@JsonIgnore
	@Override
	public String getLabel() {
		return channelName;
	}

	@JsonIgnore
	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public VALUE_TYPE getValueType(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public IDataset getParent() {
		return parent;
	}

//	@Override
//	public void setParent(SerializableMetadata p) {
//		parent = p;
//	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof IDataset)
			parent = (IDataset) p;
		else
			throw new RuntimeException(
					"Attempted to set non-serializable parent");
	}

	
	
	public String getFileKey() {
		return fileKey;
	}

	public void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Timeseries.name();
	}

	// TODO: add baseline / offset for conversions
	
	// TODO: add units specifier
	
	// TODO: add data modality string
	
	public double getBaseline() {
		if (baseline == null)
			return Double.valueOf(0);
		return baseline;
	}

	public void setBaseline(Double baseline) {
		this.baseline = baseline;
	}

	public double getMultiplier() {
		if (multiplier == null)
			return Double.valueOf(0.001);
		
		return multiplier;
	}

	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}

	public String getUnits() {
		if (units == null)
			return "V";
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getModality() {
		if (modality == null)
			return INamedTimeSegment.EEG;
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	@JsonIgnore
	@Override
	public double getDuration() {
		return recordingEndTime - recordingStartTime;
	}

	@JsonIgnore
	@Override
	public double getSampleRate() {
		return samplingFrequency;
	}

	@JsonIgnore
	@Override
	public long getStartTime() {
		return recordingStartTime;
	}

	@JsonIgnore
	@Override
	public String getDataModality() {
		return getModality();
	}

	@Override
	public long getTimeShift() {
		// TODO Auto-generated method stub
		return timeOffset;
	}

	public void setTimeShift(long timeOffset) {
		this.timeOffset = timeOffset;
	}
	
	@JsonIgnore
	@Override
	public double getScale() {
		return getVoltageConversionFactor();
	}

	@JsonIgnore
	@Override
	public boolean isEventBased() {
		return false;
	}
}
