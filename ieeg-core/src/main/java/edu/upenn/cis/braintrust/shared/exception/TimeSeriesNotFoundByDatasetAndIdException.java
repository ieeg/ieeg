/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import com.google.common.annotations.GwtCompatible;

/**
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
public class TimeSeriesNotFoundByDatasetAndIdException
		extends TimeSeriesNotFoundException {

	private static final long serialVersionUID = 1L;

	private String dataSnapshotId;
	private String dataSnapshotName;
	private String timeSeriesId;

	public TimeSeriesNotFoundByDatasetAndIdException(
			String dataSnapshotId,
			String dataSnapshotName,
			String timeSeriesId) {
		super("could not find time series in snapshot [" + dataSnapshotId
				+ "] [" + dataSnapshotName + "] with id [" + timeSeriesId + "]");
		this.dataSnapshotId = dataSnapshotId;
		this.dataSnapshotName = dataSnapshotName;
		this.timeSeriesId = timeSeriesId;
	}

	public String getDataSnapshotId() {
		return dataSnapshotId;
	}

	public String getDataSnapshotName() {
		return dataSnapshotName;
	}

	public String getTimeSeriesId() {
		return timeSeriesId;
	}

}
