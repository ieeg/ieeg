/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.util.SortedMap;

import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableSortedMap.Builder;

/**
 * IMPORTANT!: THE {@code .name} VALUES OF THESE MUST NOT CHANGE - THE DB
 * DEPENDS ON THEM.
 * 
 * @author Sam Donnelly
 */
public enum Etiology {
	GENETIC("Genetic"),
	STRUCTURAL_METABOLIC("Structural Metabolic"),
	UNKNOWN("Unknown");

	private String displayString;

	public static final SortedMap<String, Etiology> FROM_STRING;

	static {
		Builder<String, Etiology> temp = ImmutableSortedMap.naturalOrder();
		for (final Etiology gender : values()) {
			temp.put(gender.toString(), gender);
		}
		FROM_STRING = temp.build();
	}

	private Etiology(String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
