/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;

/**
 * Assembles a {@code TimeSeries} from a {@code TimeSeriesEntity}.
 * 
 * @author John Frommeyer
 * 
 */
public class TimeSeriesAssembler {
	public static TimeSeriesDto assemble(final TimeSeriesEntity timeSeriesEntity) {
		checkNotNull(timeSeriesEntity);
		TimeSeriesDto ts = new TimeSeriesDto(timeSeriesEntity.getPubId(),
				timeSeriesEntity.getLabel(), timeSeriesEntity.getFileKey());
		return ts;
	}

	public static TimeSeriesEntity buildEntity(final TimeSeriesDto timeSeries) {
		checkNotNull(timeSeries);
		final TimeSeriesEntity theEntity = new TimeSeriesEntity();
		theEntity.setLabel(timeSeries.getLabel());
		theEntity.setFileKey(timeSeries.getFileKey());
		return theEntity;
	}

	public static boolean isModified(final TimeSeriesDto timeSeries,
			final TimeSeriesEntity timeSeriesEntity) {
		final boolean modifiedLabel = !timeSeries.getLabel().equals(
				timeSeriesEntity.getLabel());
		final String gwtFileKey = timeSeries.getFileKey();
		final boolean modifiedFileKey = gwtFileKey == null ? false
				: !gwtFileKey
						.equals(timeSeriesEntity.getFileKey());
		return modifiedFileKey || modifiedLabel;
	}
}
