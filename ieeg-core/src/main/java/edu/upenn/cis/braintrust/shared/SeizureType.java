/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;
import java.util.SortedMap;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableSet;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
@GwtCompatible(serializable=true)
public enum SeizureType implements Serializable {

	// generalized
	ABSENCE("Generalized/Absence"), TONIC("Generalized/Tonic"), LONIC(
			"Generalized/Clonic"), MYOCLONIC("Generalized/Myoclonic"), ATONIC(
			"Generalized/Atonic"), TONIC_CLONIC("Generalized/Tonic-clonic"),

	// partial
	SIMPLE("Partial/Simple"), COMPLEX("Partial/Complex"), PARTIAL_WITH_2NDARY_GENERALIZATION(
			"Partial/With secondary generalization"),

	NON_EPILEPTIC("Non-epileptic");

	private String displayString;

	/**
	 * Maps a {@code SeizureType}'s toString representation to the
	 * {@code SeizureType} itself.
	 */
	public static final SortedMap<String, SeizureType> fromString;
	static {
		final SortedMap<String, SeizureType> temp = newTreeMap();
		for (final SeizureType seizureType : values()) {
			temp.put(seizureType.toString(), seizureType);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	public static final Set<SeizureType> GENERALIZED_SZ_TYPES =
			ImmutableSet.of(ABSENCE, TONIC, LONIC, MYOCLONIC, ATONIC,
					TONIC_CLONIC);

	public static final Set<SeizureType> PARTIAL_SZ_TYPES =
			ImmutableSet
					.of(SIMPLE, COMPLEX, PARTIAL_WITH_2NDARY_GENERALIZATION);

	SeizureType() {}
	
	SeizureType(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}

}
