/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.util.SortedMap;

import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableSortedMap.Builder;

/**
 * @author samd
 * 
 */
public enum Region {
	NONE("None"),
	FRONTAL("FrontaL"),
	TEMPORAL("Temporal"),
	CENTRAL("Central"),
	PARIETAL("Parietal"),
	OCCIPITAL("Occipital"),
	HEMISPHERE("Hemisphere");

	private final String displayString;

	/**
	 * Maps a {@code Side}'s toString representation to the {@code Side} itself.
	 */
	public static final SortedMap<String, Region> FROM_STRING;
	static {
		Builder<String, Region> builder = ImmutableSortedMap.naturalOrder();
		for (final Region region : values()) {
			builder.put(region.toString(), region);
		}
		FROM_STRING = builder.build();
	}

	private Region(String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
