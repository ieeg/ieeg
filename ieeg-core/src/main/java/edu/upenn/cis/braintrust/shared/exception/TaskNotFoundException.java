/*
 * Copyright (C) 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.IeegException;

public class TaskNotFoundException extends IeegException {
	private static final long serialVersionUID = 1L;
	private String datasetPubId;
	private String taskPubId;

	/**
	 * Indicates that there is no task with {@code taskPubId} in job with the
	 * given dataset pub id in the db.
	 * 
	 * @param datasetPubId
	 */
	public TaskNotFoundException(String datasetPubId, String taskPubId) {
		super("could not find task [" + taskPubId
				+ "] in job with with dataset pub id [" + datasetPubId + "]");
		this.datasetPubId = checkNotNull(datasetPubId);
		this.taskPubId = checkNotNull(taskPubId);
	}

	public String getDatasetPubId() {
		return datasetPubId;
	}

	public String getTaskPubId() {
		return taskPubId;
	}
}
