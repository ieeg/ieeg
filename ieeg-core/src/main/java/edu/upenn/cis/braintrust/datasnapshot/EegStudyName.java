/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.upenn.cis.braintrust.model.EegStudy;

/**
 * A name for an {@link EegStudy}. Is 13 characters long and has the form
 * "Innn_Pnnn_Dnn" where the n are [0-9].
 * 
 * @author John Frommeyer
 * 
 */
public final class EegStudyName {
	/**
	 * Group 1 is 'I' followed by 3 digits. Group 2 is 'P' followed by 3 digits.
	 * Group 3 is 'D' followed by 2 digits. Groups are separated by '_'
	 */
	private final static Pattern pattern = Pattern
			.compile("^(I\\d{3})_(P\\d{3})_(D\\d{2})$");
	private final String organizationPart;
	private final String patientPart;
	private final String eegStudyPart;

	public EegStudyName(String name) {
		final Matcher matcher = pattern.matcher(name);
		checkArgument(matcher.matches(), name + " is not a valid EegStudy name");
		organizationPart = checkNotNull(matcher.group(1));
		patientPart = checkNotNull(matcher.group(2));
		eegStudyPart = checkNotNull(matcher.group(3));
	}

	public static boolean isEegStudyName(String candidate) {
		final Matcher matcher = pattern.matcher(candidate);
		return matcher.matches();
	}

	/**
	 * Returns the organization part of the name. For example I001.
	 * 
	 * @return the organization part
	 */
	public String getOrganizationPart() {
		return organizationPart;
	}

	/**
	 * Returns the patient part of the name. For example P001.
	 * 
	 * @return the patientPart
	 */
	public String getPatientPart() {
		return patientPart;
	}

	/**
	 * Returns the study part of the name. For example D01.
	 * 
	 * @return the eegStudyPart
	 */
	public String getEegStudyPart() {
		return eegStudyPart;
	}

	/**
	 * Returns the study directory implied by this name.
	 * 
	 * @return the study directory
	 */
	public String getDir() {
		return "Human_Data/" + organizationPart + "/" + patientPart + "/"
				+ eegStudyPart;
	}

	/**
	 * Returns the mef directory implied by this name.
	 * 
	 * @return the mef directory
	 */
	public String getMefDir() {
		return getDir() + "/" + "mef";
	}

	/**
	 * Returns the patient name implied by this name. For example I001_P001
	 * 
	 * @return the patient name
	 */
	public String getPatientName() {
		return organizationPart + "_" + patientPart;
	}

	@Override
	public String toString() {
		return organizationPart + "_" + patientPart + "_" + eegStudyPart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eegStudyPart == null) ? 0 : eegStudyPart.hashCode());
		result = prime
				* result
				+ ((organizationPart == null) ? 0 : organizationPart.hashCode());
		result = prime * result
				+ ((patientPart == null) ? 0 : patientPart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EegStudyName)) {
			return false;
		}
		EegStudyName other = (EegStudyName) obj;
		if (eegStudyPart == null) {
			if (other.eegStudyPart != null) {
				return false;
			}
		} else if (!eegStudyPart.equals(other.eegStudyPart)) {
			return false;
		}
		if (organizationPart == null) {
			if (other.organizationPart != null) {
				return false;
			}
		} else if (!organizationPart.equals(other.organizationPart)) {
			return false;
		}
		if (patientPart == null) {
			if (other.patientPart != null) {
				return false;
			}
		} else if (!patientPart.equals(other.patientPart)) {
			return false;
		}
		return true;
	}

}
