/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * @author John Frommeyer
 * 
 */
@NamedQueries({
		@NamedQuery(name = AnimalEntity.BY_LABEL,
				query = "from Animal a "
						+ "where a.label = :label ") })
@Entity(name = "Animal")
@Table(name = AnimalEntity.TABLE)
@PrimaryKeyJoinColumn(name = AnimalEntity.ID_COLUMN)
public class AnimalEntity extends Subject {

	public static final String TABLE = "animal";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String BY_LABEL = "Animal-findByLabel";
	private StrainEntity strain;
	private Set<ExperimentEntity> experiments = newHashSet();

	public AnimalEntity() {}

	public AnimalEntity(
			String label,
			OrganizationEntity organization,
			StrainEntity strain) {
		super(label, organization);
		this.strain = strain;
	}

	/**
	 * For convenience, handle both sides of the {@code Animal<->Experiment}
	 * relationship.
	 * 
	 * @param experiment to be added
	 */
	public void addExperiment(final ExperimentEntity experiment) {
		checkNotNull(experiment);
		checkArgument(experiment.getParent() == null,
				"experiment already has a parent");
		checkArgument(!experiments.contains(experiment),
				"experiment is already contained in this animal's experiments");
		experiment.setParent(this);
		experiments.add(experiment);
	}
	
	/**
	 * Remove {@code experiment} from this animal and {@code null} out
	 * {@code experiment}'s parent.
	 * 
	 * @param experiment to be removed and deparented
	 * 
	 * @throws IllegalArgumentException if this animal is not the experiment's
	 *             parent
	 * @throws IllegalArgumentException if the experiment is not contained in this
	 *             animal
	 */
	public void removeExperiment(final ExperimentEntity experiment) {
		checkNotNull(experiment);
		checkArgument(equals(experiment.getParent()),
				"this animal is not experiment's parent");
		checkArgument(experiments.contains(experiment),
				"this animal does not contain experiment");
		experiments.remove(experiment);
		experiment.setParent(null);
	}

	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<ExperimentEntity> getExperiments() {
		return experiments;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = StrainEntity.ID_COLUMN)
//	@NotNull
	public StrainEntity getStrain() {
		return strain;
	}

	@SuppressWarnings("unused")
	private void setExperiments(Set<ExperimentEntity> experiments) {
		this.experiments = experiments;
	}

	public void setStrain(@Nullable StrainEntity strain) {
		this.strain = strain;
	}

	public static final EntityConstructor<AnimalEntity, String, OrganizationEntity> CONSTRUCTOR =
		new EntityConstructor<AnimalEntity,String,OrganizationEntity>(AnimalEntity.class,
				new IPersistentObjectManager.IPersistentKey<AnimalEntity,String>() {

			@Override
			public String getKey(AnimalEntity o) {
				return o.getLabel();
			}

			@Override
			public void setKey(AnimalEntity o, String newKey) {
				o.setLabel(newKey);
			}

		},
		new EntityPersistence.ICreateObject<AnimalEntity,String,OrganizationEntity>() {

			@Override
			public AnimalEntity create(String key, OrganizationEntity optParent) {
				// No OrganizationEntity or StrainEntity
				return new AnimalEntity(key, (OrganizationEntity)optParent, null);
			}
		}
				);

	@Override
	public EntityConstructor<AnimalEntity, String, OrganizationEntity> tableConstructor() {
		return CONSTRUCTOR;
	}
}
