/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.ImageType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class ImageForTrace {

	private ImageType imageType;
	private String imagePubId;
	private String imageFileKey;
	private int pixelCoordX;
	private int pixelCoordY;
	public static Function<ImageForTrace, String> getImageFileKey = new Function<ImageForTrace, String>() {
		@Override
		public String apply(final ImageForTrace input) {
			return input.getImageFileKey();
		}
	};

	public ImageForTrace() {}

	/**
	 * @return the imageFileKey
	 */
	@XmlElement
	public String getImageFileKey() {
		return imageFileKey;
	}

	/**
	 * @return the imageRevId
	 */
	@XmlElement
	public String getImagePubId() {
		return imagePubId;
	}

	/**
	 * @return the imageType
	 * 
	 * @deprecated instead use {@link #getImageTypeEnum()}
	 */
	@Deprecated
	@XmlElement
	public String getImageType() {
		return imageType.name();
	}

	public ImageType getImageTypeEnum() {
		return this.imageType;
	}

	/**
	 * @return the pixelCoordX
	 */
	@XmlElement
	public int getPixelCoordX() {
		return pixelCoordX;
	}

	/**
	 * @return the pixelCoordY
	 */
	@XmlElement
	public int getPixelCoordY() {
		return pixelCoordY;
	}

	/**
	 * @param imageFileKey the imageFileKey to set
	 */
	public void setImageFileKey(final String imageFileKey) {
		this.imageFileKey = imageFileKey;
	}

	/**
	 * @param imagePubId the imagePubId to set
	 */
	public void setImagePubId(final String imagePubId) {
		this.imagePubId = imagePubId;
	}

	@Deprecated
	public void setImageType(final String imageType) {
		this.imageType = ImageType.valueOf(imageType);
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageTypeEnum(final ImageType imageType) {
		this.imageType = imageType;
	}

	/**
	 * @param pixelCoordX the pixelCoordX to set
	 */
	public void setPixelCoordX(final int pixelCoordX) {
		this.pixelCoordX = pixelCoordX;
	}

	/**
	 * @param pixelCoordY the pixelCoordY to set
	 */
	public void setPixelCoordY(final int pixelCoordY) {
		this.pixelCoordY = pixelCoordY;
	}

}
