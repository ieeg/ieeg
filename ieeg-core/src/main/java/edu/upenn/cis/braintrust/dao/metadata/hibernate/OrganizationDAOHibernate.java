/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.metadata.hibernate;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.Patient;

public class OrganizationDAOHibernate extends
		GenericHibernateDAOLongId<OrganizationEntity>
		implements IOrganizationDAO {

	public OrganizationDAOHibernate() {}

	public OrganizationDAOHibernate(Session sess) {
		setSession(sess);
	}

	@Override
	public OrganizationEntity findByOrganization(String organization) {
		final Session theSession = getSession();
		final Query query = theSession.getNamedQuery(
				OrganizationEntity.ORGANIZATION_BY_NAME)
				.setParameter("name", organization);
		return (OrganizationEntity) query.uniqueResult();
	}
}
