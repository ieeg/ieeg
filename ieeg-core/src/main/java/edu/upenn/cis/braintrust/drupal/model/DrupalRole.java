/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "ROLE")
@Immutable
public class DrupalRole {

	public static final String ADMIN = "admin";
	public static final String DATA_ENTERER = "data-enterer";
	public static final String GUEST = "portal-guest";

	private Integer rid;
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the rid
	 */
	@Id
	@Column(columnDefinition = "INT UNSIGNED")
	public Integer getRid() {
		return rid;
	}

	/**
	 * @param name the name to set
	 */
	@SuppressWarnings("unused")
	private void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param rid the rid to set
	 */
	@SuppressWarnings("unused")
	private void setRid(final Integer rid) {
		this.rid = rid;
	}
}
