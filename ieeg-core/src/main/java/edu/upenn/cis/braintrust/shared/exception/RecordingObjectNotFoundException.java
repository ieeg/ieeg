/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import edu.upenn.cis.braintrust.shared.RecordingObjectId;

public class RecordingObjectNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	private RecordingObjectId recordingObjectId;

	public RecordingObjectNotFoundException(RecordingObjectId recordingObjectId) {
		super("could not find recording object with id or name ["
				+ recordingObjectId.getId() + "]");
	}

	public RecordingObjectNotFoundException(Long recordingObjectId) {
		super("could not find recording object with id or name ["
				+ recordingObjectId + "]");
		this.recordingObjectId = new RecordingObjectId(recordingObjectId);
	}

	public RecordingObjectNotFoundException(Long recordingObjectId, String msg) {
		super(msg);
		this.recordingObjectId = new RecordingObjectId(recordingObjectId);
	}

	public RecordingObjectId getRecordingObjectId() {
		return recordingObjectId;
	}
}
