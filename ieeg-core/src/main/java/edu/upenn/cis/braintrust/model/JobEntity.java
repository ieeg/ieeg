/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.ParallelDto;

/**
 * A user's job.
 * 
 * @author John Frommeyer
 */
@Entity
@Table(name = JobEntity.TABLE)
public class JobEntity extends AbstractJob implements IHasLongId {
	public static final String TABLE = "job";
	public static final String ID_COLUMN = TABLE + "_id";

	private Integer version;
	private Set<AnalysisTask> tasks = newHashSet();
	/** no lazy -to-one work-around */
	private Set<JobClob> jobClob = newHashSet();

	JobEntity() {}

	public JobEntity(
			Long user,
			String datasetPubId,
			String tool,
			JobStatus status,
			ParallelDto parallel,
			Date createTime,
			@Nullable String jobClob) {
		super(user, datasetPubId, tool, status, parallel, createTime);
		setClob(jobClob);
	}

	@Transient
	@Override
	public String getClob() {
		if (jobClob.isEmpty()) {
			return null;
		}
		return getOnlyElement(jobClob).getValue();
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return super.getId();
	}

	/**
	 * Arbitrary character data associated with this user job.
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than buildtime
	 * bytecode instr. because that loads all lazy props when you hit any of
	 * them.
	 * 
	 * @return the arbitrary character data associated with this user job
	 */
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			orphanRemoval = true)
	private Set<JobClob> getJobClob() {
		return jobClob;
	}

	/**
	 * @return the active tasks
	 */
	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "parent",
			orphanRemoval = true)
	public Set<AnalysisTask> getTasks() {
		return tasks;
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	@Override
	public void setClob(@Nullable final String clob) {
		if (clob == null) {
			jobClob.clear();
		} else {
			if (jobClob.isEmpty()) {
				this.jobClob.add(new JobClob(clob, this));
			} else {
				getOnlyElement(this.jobClob).setValue(clob);
			}
		}
	}

	@SuppressWarnings("unused")
	private void setJobClob(@Nullable Set<JobClob> jobClob) {
		this.jobClob = jobClob;
	}

	@SuppressWarnings("unused")
	private void setTasks(Set<AnalysisTask> tasks) {
		this.tasks = tasks;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
