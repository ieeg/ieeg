/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

@Immutable
@GwtCompatible(serializable = true)
public final class MontageInfoDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private MontageIdAndVersion idAndVersion;
	private String name;

	/** For GWT. */
	@SuppressWarnings("unused")
	private MontageInfoDto() {}

	public MontageInfoDto(MontageIdAndVersion idAndVersion, String name) {
		this.idAndVersion = checkNotNull(idAndVersion);
		this.name = checkNotNull(name);
	}

	public MontageIdAndVersion getIdAndVersion() {
		return idAndVersion;
	}

	public String getName() {
		return name;
	}

}
