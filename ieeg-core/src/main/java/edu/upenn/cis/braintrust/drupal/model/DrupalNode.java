/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "NODE")
public class DrupalNode {
	@Id @GeneratedValue
	private int nid;
	private int vid;
	private String type;
	private String language;
	private String title;
	private int uid;
	private int status;
	private int created;
	private int changed;
	private int comment;
	private int promote;
	private int moderate;
	private int sticky;
	private int tnid;
	private int translate;
	
	@Column(columnDefinition = "int(10)")
	public int getNid() {
		return nid;
	}
	public void setNid(int nid) {
		this.nid = nid;
	}
	@Column(unique = true, columnDefinition = "int(10)")
	public int getVid() {
		return vid;
	}
	public void setVid(int vid) {
		this.vid = vid;
	}
	@Column(columnDefinition = "varchar(32)")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(columnDefinition = "varchar(12)")
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	@Column(columnDefinition = "varchar(255)")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(columnDefinition = "int(11)")
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	@Column(columnDefinition = "int(11)")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Column(columnDefinition = "int(11)")
	public int getCreated() {
		return created;
	}
	public void setCreated(int created) {
		this.created = created;
	}
	@Column(columnDefinition = "int(11)")
	public int getChanged() {
		return changed;
	}
	public void setChanged(int changed) {
		this.changed = changed;
	}
	@Column(columnDefinition = "int(11)")
	public int getComment() {
		return comment;
	}
	public void setComment(int comment) {
		this.comment = comment;
	}
	@Column(columnDefinition = "int(11)")
	public int getPromote() {
		return promote;
	}
	public void setPromote(int promote) {
		this.promote = promote;
	}
	@Column(columnDefinition = "int(11)")
	public int getModerate() {
		return moderate;
	}
	public void setModerate(int moderate) {
		this.moderate = moderate;
	}
	@Column(columnDefinition = "int(11)")
	public int getSticky() {
		return sticky;
	}
	public void setSticky(int sticky) {
		this.sticky = sticky;
	}
	@Column(unique = true, columnDefinition = "int(11)")
	public int getTnid() {
		return tnid;
	}
	public void setTnid(int tnid) {
		this.tnid = tnid;
	}
	@Column(columnDefinition = "int(11)")
	public int getTranslate() {
		return translate;
	}
	public void setTranslate(int translate) {
		this.translate = translate;
	}

}
