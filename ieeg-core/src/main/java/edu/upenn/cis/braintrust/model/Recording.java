/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * @author John Frommeyer
 * 
 */
@Entity
@Table(name = Recording.TABLE)
public class Recording implements IHasLongId {
	public static final String TABLE = "recording";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String START_TIME_COLUMN = "start_time";
	public static final String END_TIME_COLUMN = "end_time";

	private Long id;
	private Integer version;

	/** The contactGroups used in this recording. */
	private Set<ContactGroup> contactGroups = newHashSet();
	private Set<ImageEntity> images = newHashSet();
	private Set<RecordingObjectEntity> objects = newHashSet();
	private String refElecrodeDescription;
	private String imagesFile;
	private String reportFile;
	/** The relative path that is home to the study */
	// start at Xyz_IEED_data
	private String dir;
	/** The relative path to dir of the mef directory. */
	private String mefDir;
	private Long startTimeUutc;
	private Long endTimeUutc;
	private String commentary;

	Recording() {}

	public Recording(
			String dir,
			String mefDir,
			Long startTimeUutc,
			Long endTimeUutc,
			@Nullable String refElectrodeDescription,
			@Nullable String imagesFile,
			@Nullable String reportFile,
			@Nullable String commentary) {
		this.dir = checkNotNull(dir);
		this.mefDir = checkNotNull(mefDir);
		this.startTimeUutc = checkNotNull(startTimeUutc);
		this.endTimeUutc = checkNotNull(endTimeUutc);
		this.refElecrodeDescription = refElectrodeDescription;
		this.imagesFile = imagesFile;
		this.reportFile = reportFile;
		this.commentary = commentary;
	}

	/**
	 * Add {@code contactGroup} to this recording.
	 * 
	 * @param contactGroup to be added
	 * 
	 * @throws IllegalArgumentException if the contactGroup already has a parent
	 * @throws IllegalArgumentException if this recording already contains
	 *             {@code contactGroup}
	 */
	public void addContactGroup(final ContactGroup contactGroup) {
		checkNotNull(contactGroup);
		checkArgument(contactGroup.getParent() == null,
				"contactGroup already has a parent");
		checkArgument(!contactGroups.contains(contactGroup),
				"this recording's contactGroups already contains contactGroup");
		contactGroups.add(contactGroup);
		contactGroup.setParent(this);
	}

	@Column
	@Size(max = 4000)
	public String getCommentary() {
		return commentary;
	}

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<ContactGroup> getContactGroups() {
		return contactGroups;
	}

	/**
	 * @return the dirUri
	 */
	@Column(unique = true)
	@NotNull
	@Size(min = 1)
	public String getDir() {
		return dir;
	}

	/**
	 * The end time of EEG monitoring.
	 * 
	 * @return the end time of EEG monitoring
	 */
	@Column(name = END_TIME_COLUMN)
	@NotNull
	@Min(0)
	public Long getEndTimeUutc() {
		return endTimeUutc;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = Recording.ID_COLUMN)
	public Long getId() {
		return id;
	}

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = TABLE + "_" + ImageEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = ImageEntity.ID_COLUMN))
	public Set<ImageEntity> getImages() {
		return images;
	}

	/** Relative path from dir. */
	@Size(min = 1)
	public String getImagesFile() {
		return imagesFile;
	}

	/**
	 * The relative path to dir of the mef directory.
	 * 
	 * @return the mefDir
	 */
	@Column
	@NotNull
	@Size(min = 1)
	public String getMefDir() {
		return mefDir;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			mappedBy = "parent")
	public Set<RecordingObjectEntity> getObjects() {
		return objects;
	}

	@Column
	@Size(max = 255)
	public String getRefElectrodeDescription() {
		return refElecrodeDescription;
	}

	/** Relative path from dir. */
	@Size(min = 1)
	public String getReportFile() {
		return reportFile;
	}

	/**
	 * The start time of EEG monitoring.
	 * 
	 * @return the start time of EEG monitoring
	 */
	@Column(name = START_TIME_COLUMN)
	@NotNull
	@Min(0)
	public Long getStartTimeUutc() {
		return startTimeUutc;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	/**
	 * Remove {@code contactGroup} from this recording.
	 * 
	 * @param contactGroup to be removed
	 * 
	 * @throws IllegalArgumentException if this is not the contactGroup's parent
	 * @throws IllegalArgumentException if this recording does not contain
	 *             {@code contactGroup}
	 */
	public void removeContactGroup(final ContactGroup contactGroup) {
		checkNotNull(contactGroup);
		contactGroups.remove(contactGroup);
		contactGroup.setParent(null);
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

	@SuppressWarnings("unused")
	private void setContactGroups(final Set<ContactGroup> contactGroups) {
		this.contactGroups = contactGroups;
	}

	/**
	 * @param dirUri the dirUri to set
	 */
	public void setDir(final String dirUri) {
		this.dir = dirUri;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTimeUutc(final Long endTime) {
		this.endTimeUutc = endTime;
	}

	@VisibleForTesting
	public void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setImages(final Set<ImageEntity> images) {
		this.images = images;
	}

	public void setImagesFile(final String imagesFile) {
		this.imagesFile = imagesFile;
	}

	/**
	 * @param mefDir the mefDir to set
	 */
	public void setMefDir(final String mefDir) {
		this.mefDir = mefDir;
	}

	@SuppressWarnings("unused")
	private void setObjects(Set<RecordingObjectEntity> objects) {
		this.objects = objects;
	}

	public void setRefElectrodeDescription(
			@Nullable String refElectrodeDescription) {
		this.refElecrodeDescription = refElectrodeDescription;
	}

	public void setReportFile(final String reportFile) {
		this.reportFile = reportFile;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTimeUutc(final Long startTime) {
		this.startTimeUutc = startTime;
	}

	@VisibleForTesting
	public void setVersion(final Integer version) {
		this.version = version;
	}
}
