/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots;

import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IDatasetDAO extends IDAO<Dataset, Long> {

	/**
	 * Returns the {@code Dataset} with the given revision id, or null if there
	 * is no such {@code Dataset}.
	 * 
	 * @param datasetPubId
	 * 
	 * @return the {@code Dataset} with the given revision id, or null if there
	 *         is no such {@code Dataset}
	 */
	public Dataset findByPubId(final String datasetPubId);

	public Integer findContributedSnapshotCount(final UserEntity user);
}
