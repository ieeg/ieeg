/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.base.Preconditions.checkNotNull;

import org.hibernate.Session;
import org.hibernate.SimpleNaturalIdLoadAccess;

import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class EegStudyDAOHibernate
		extends GenericHibernateDAO<EegStudy, Long>
		implements IEegStudyDAO {

	public EegStudyDAOHibernate() {

	}

	public EegStudyDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public Contact findContactByTracePubId(
			final String timeSeriesPubId) {
		checkNotNull(timeSeriesPubId);
		return (Contact) getSession()
				.getNamedQuery(EegStudy.CONTACT_BY_TRACE_PUB_ID)
				.setParameter("timeSeriesPubId", timeSeriesPubId)
				.uniqueResult();
	}

	@Override
	public EegStudy findByPubId(String pubId) {
		checkNotNull(pubId);
		SimpleNaturalIdLoadAccess la = getSession()
				.bySimpleNaturalId(EegStudy.class);
		return (EegStudy) la.load(pubId);
	}

	@Override
	public long countWorldReadableStudies() {
		final Long count = (Long) getSession()
				.getNamedQuery(EegStudy.COUNT_WORLD_READABLE_STUDIES)
				.uniqueResult();

		return count.longValue();
	}

	@Override
	public long countAllStudies() {
		final Long count = (Long) getSession()
				.getNamedQuery(EegStudy.COUNT_ALL_STUDIES)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countUserReadableStudies() {
		final Long count = (Long) getSession()
				.getNamedQuery(EegStudy.COUNT_USER_READABLE_STUDIES)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public EegStudy findByTimeSeries(TimeSeriesEntity timeSeries) {
		checkNotNull(timeSeries);
		Object study = getSession().
				getNamedQuery(EegStudy.BY_TIME_SERIES).
				setParameter("timeSeries", timeSeries).
				uniqueResult();
		return (EegStudy) study;
	}

	@Override
	public EegStudy findByRecording(Recording recording) {
		checkNotNull(recording);
		return (EegStudy) getSession()
				.getNamedQuery(EegStudy.BY_RECORDING)
				.setParameter("recording", recording)
				.uniqueResult();
	}

}
