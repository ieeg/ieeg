/*
 * Copyright (C) 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.IAce;
import edu.upenn.cis.braintrust.shared.UserId;

/**
 * Access Control Entry for a user.
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class ExtUserAce implements IAce {

	public static final Function<ExtUserAce, String> getName = new Function<ExtUserAce, String>() {

		@Override
		public String apply(ExtUserAce input) {
			return input.getName();
		}
	};

	public static final Function<ExtUserAce, UserId> getUserId = new Function<ExtUserAce, UserId>() {

		@Override
		public UserId apply(ExtUserAce input) {
			return input.getUserId();
		}
	};
	private static final long serialVersionUID = 1L;
	private UserId userId;
	private String username;
	private String targetId;
	private Integer aclVersion;
	private Optional<Long> aceId;
	private Optional<Integer> aceVersion;

	private Set<ExtPermission> userPerms = newHashSet();

	/**
	 * 
	 * 
	 * @param userId
	 * @param username
	 * @param targetId
	 * @param aclVersion
	 * @param aceId
	 * @param aceVersion
	 */
	public ExtUserAce(
			UserId userId,
			String username,
			String targetId,
			Integer aclVersion,
			@Nullable Long aceId,
			@Nullable Integer aceVersion) {
		this.userId = checkNotNull(userId);
		this.username = checkNotNull(username);
		this.targetId = checkNotNull(targetId);
		this.aclVersion = checkNotNull(aclVersion);
		this.aceId = Optional.fromNullable(aceId);
		this.aceVersion = Optional.fromNullable(aceVersion);
	}

	// For GWT
	@SuppressWarnings("unused")
	private ExtUserAce() {}

	public UserId getUserId() {
		return userId;
	}

	public Optional<Long> getAceId() {
		return aceId;
	}
	
	@Override
	public String getTargetId() {
		return targetId;
	}

	public Integer getAclVersion() {
		return aclVersion;
	}

	public Optional<Integer> getAceVersion() {
		return aceVersion;
	}

	public String getName() {
		return username;
	}

	@Override
	public Set<ExtPermission> getPerms() {
		return userPerms;
	}

	@Override
	public String toString() {
		return "ExtUserAce [userId=" + userId + ", username=" + username
				+ ", targetId=" + targetId + ", aclVersion=" + aclVersion
				+ ", aceId=" + aceId + ", aceVersion=" + aceVersion
				+ ", userPerms=" + userPerms + "]";
	}

}
