/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * An electrode has many contacts.
 * 
 * @author Sam Donnelly
 */
@Entity
@Table(name = Contact.TABLE)
@NamedQueries(@NamedQuery(
		name = Contact.FIND_CONTACT,
		query = "from Contact c " +
				"where c.trace = :trace"))
public class Contact implements IHasLongId {

	public static final String TABLE = "contact";
	public static final String UPPER_CASE_TABLE = "CONTACT";

	public static final String ID_COLUMN = TABLE + "_id";

	public static final String FIND_CONTACT = "Contact-findContact";

	private Long id;

	private Integer version;
	private ContactGroup parent;
	private MriCoordinates mriCoordinates;
	private TalairachCoordinates talairachCoordinates;
	private ContactType contactType;
	private TimeSeriesEntity trace;
	private Set<ImagedContact> imagedContacts = newHashSet();

	public Contact() {}

	public Contact(
			ContactType contactType,
			MriCoordinates mriCoordinates,
			TalairachCoordinates talairachCoordinates,
			TimeSeriesEntity trace) {
		this.contactType = checkNotNull(contactType);
		this.mriCoordinates = checkNotNull(mriCoordinates);
		this.talairachCoordinates = checkNotNull(talairachCoordinates);
		this.trace = checkNotNull(trace);
	}

	/**
	 * @return the contactType
	 */
	@Column
	@Enumerated(EnumType.STRING)
	@NotNull
	public ContactType getContactType() {
		return contactType;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	/**
	 * Many-to-many with additional columns on join table, intermediate entity
	 * class. To create a link, instantiate an {@code ImagedContact} with the
	 * right constructor. To remove a link, use
	 * {@code getImagecContacts().remove(...)}.
	 * 
	 * @return the join-table entity
	 */
	@OneToMany(
			mappedBy = "contact",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<ImagedContact> getImagedContacts() {
		return imagedContacts;
	}

	/**
	 * @return the mriCoordinates
	 */
	@AttributeOverrides({
			@AttributeOverride(name = "x", column = @Column(
					name = MriCoordinates.X_COLUMN, nullable = true)),
			@AttributeOverride(name = "y", column = @Column(
					name = MriCoordinates.Y_COLUMN, nullable = true)),
			@AttributeOverride(name = "z", column = @Column(
					name = MriCoordinates.Z_COLUMN, nullable = true)) })
	public MriCoordinates getMriCoordinates() {
		return mriCoordinates;
	}

	/**
	 * @return the parent
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = ContactGroup.ID_COLUMN)
	@NotNull
	public ContactGroup getParent() {
		return parent;
	}

	/**
	 * @return the talairachCoordinates
	 */
	@AttributeOverrides({
			@AttributeOverride(name = "x", column = @Column(
					name = TalairachCoordinates.X_COLUMN, nullable = true)),
			@AttributeOverride(name = "y", column = @Column(
					name = TalairachCoordinates.Y_COLUMN, nullable = true)),
			@AttributeOverride(name = "z", column = @Column(
					name = TalairachCoordinates.Z_COLUMN, nullable = true)) })
	public TalairachCoordinates getTalairachCoordinates() {
		return talairachCoordinates;
	}

	/**
	 * The time series recorded by this contactInfo
	 * 
	 * @return the trace
	 */
	@OneToOne(
			fetch = FetchType.EAGER,//LAZY,
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			optional = false)
	@JoinColumn(name = TimeSeriesEntity.ID_COLUMN)
	@NotNull
	public TimeSeriesEntity getTrace() {
		return trace;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	public void setContactType(ContactType contactType) {
		this.contactType = checkNotNull(contactType);
	}

	@VisibleForTesting
	public void setId(final Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setImagedContacts(final Set<ImagedContact> imagedContacts) {
		this.imagedContacts = imagedContacts;
	}

	/**
	 * @param mriCoordinates the mriCoordinates to set
	 */
	public void setMriCoordinates(@Nullable MriCoordinates mriCoordinates) {
		this.mriCoordinates = mriCoordinates;
	}

	/**
	 * Set the owning electrode
	 * 
	 * @param parent the owning electrode
	 */
	public void setParent(final ContactGroup parent) {
		this.parent = parent;
	}

	/**
	 * @param talairachCoordinates the talairachCoordinates to set
	 */
	public void setTalairachCoordinates(
			@Nullable TalairachCoordinates talairachCoordinates) {
		this.talairachCoordinates = talairachCoordinates;
	}

	public void setTrace(final TimeSeriesEntity trace) {
		this.trace = trace;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

}
