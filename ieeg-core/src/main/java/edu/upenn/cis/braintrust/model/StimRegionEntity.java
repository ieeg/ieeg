/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * * @author John Frommeyer
 * 
 */
@Entity(name = "StimRegion")
@Table(name = StimRegionEntity.TABLE)
public class StimRegionEntity implements IHasLongId, IHasLabel {
	public static final String TABLE = "stim_region";
	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private Integer version;
	private String label;

	StimRegionEntity() {}

	public StimRegionEntity(String label) {
		this.label = checkNotNull(label);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Override
	@Column(unique = true)
	@NotNull
	public String getLabel() {
		return label;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
