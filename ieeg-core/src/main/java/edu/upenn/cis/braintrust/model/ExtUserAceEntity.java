/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.imodel.IHasPerms;
import edu.upenn.cis.braintrust.shared.UserId;

@Entity(name = "ExtUserAce")
@Table(name = ExtUserAceEntity.TABLE,
		uniqueConstraints = @UniqueConstraint(
				columnNames = {
						UserEntity.ID_COLUMN,
						ExtAclEntity.ID_COLUMN }))
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(
				name = ExtUserAceEntity.ID_COLUMN)) })
public class ExtUserAceEntity extends LongIdAndVersion implements IHasPerms {

	public static final Function<ExtUserAceEntity, UserEntity> getUser = new Function<ExtUserAceEntity, UserEntity>() {

		@Override
		public UserEntity apply(final ExtUserAceEntity input) {
			return input.getUser();
		}

	};

	public static final Function<ExtUserAceEntity, UserId> getUserId = new Function<ExtUserAceEntity, UserId>() {

		@Override
		public UserId apply(final ExtUserAceEntity input) {
			return input.getUser().getId();
		}

	};

	public static final String TABLE = "ext_user_ace";
	public static final String ID_COLUMN = TABLE + "_id";

	private UserEntity user;
	private ExtAclEntity acl;
	private Set<PermissionEntity> perms = newHashSet();

	/**
	 * For JPA.
	 */
	ExtUserAceEntity() {}

	/**
	 * This constructor takes care of the relationship by adding the new
	 * instance to the {@code acl}.
	 */
	public ExtUserAceEntity(
			final UserEntity user,
			final ExtAclEntity acl,
			Set<PermissionEntity> perms) {
		this.acl = checkNotNull(acl);
		this.user = checkNotNull(user);
		checkNotNull(perms);
		checkArgument(!perms.isEmpty());
		this.perms.addAll(perms);
		// Guarantee referential integrity
		acl.getUserAces().add(this);
	}

	/**
	 * This constructor takes care of the relationship by adding the new
	 * instance to the {@code acl}.
	 */
	public ExtUserAceEntity(
			final UserEntity user,
			final ExtAclEntity acl,
			PermissionEntity perm) {
		this.acl = checkNotNull(acl);
		this.user = checkNotNull(user);
		this.perms.add(checkNotNull(perm));
		// Guarantee referential integrity
		acl.getUserAces().add(this);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getAcl() {
		return acl;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = TABLE + "_" + PermissionEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = PermissionEntity.ID_COLUMN))
	@Size(min = 1)
	@Override
	public Set<PermissionEntity> getPerms() {
		return perms;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = UserEntity.ID_COLUMN)
	@NotNull
	public UserEntity getUser() {
		return user;
	}

	@SuppressWarnings("unused")
	private void setAcl(ExtAclEntity acl) {
		this.acl = acl;
	}

	@SuppressWarnings("unused")
	private void setPerms(final Set<PermissionEntity> perms) {
		this.perms = perms;
	}

	@SuppressWarnings("unused")
	private void setUser(final UserEntity user) {
		this.user = user;
	}

}
