/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import javax.annotation.Nullable;

/**
 * A response to an edit acl action
 * 
 * @author John Frommeyer
 * 
 */
public class EditAclResponse implements IResponse {

	
	private static final long serialVersionUID = 1L;
	private boolean success;
	private String userMessage;

	//For GWT
	@SuppressWarnings("unused")
	private EditAclResponse() {}

	
	public EditAclResponse(boolean success, @Nullable String userMessage) {
		this.success = success;
		this.userMessage = userMessage;
	}


	public EditAclResponse(boolean success) {
		this.success = success;
	}


	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(@Nullable String userMessage) {
		this.userMessage = userMessage;
	}
	
	
}
