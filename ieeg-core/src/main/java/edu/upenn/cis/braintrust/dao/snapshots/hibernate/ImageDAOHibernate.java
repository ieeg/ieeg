/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.dto.TraceForImage;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class ImageDAOHibernate
		extends GenericHibernateDAO<ImageEntity, Long>
		implements IImageDAO {

	public ImageDAOHibernate() {}

	public ImageDAOHibernate(Session session) {
		setSession(session);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.dao.snapshots.IImageDAO#findByFileKey(java.lang.String)
	 */
	@Override
	public Set<TraceForImage> findByFileKey(String fileKey) {
		final Query query = getSession().getNamedQuery(
				"Image-findByFileKey").setParameter(
				"imageFileKey", fileKey);

		final ImageEntity imageEntity = (ImageEntity) query.uniqueResult();
		return image2TraceForImages(imageEntity);
	}

	private static Set<TraceForImage> image2TraceForImages(
			final ImageEntity imageEntity) {
		Set<TraceForImage> traceForImages = newHashSet();
		if (imageEntity != null) {
			final Set<ImagedContact> imagedContacts = imageEntity
					.getImagedContacts();
			for (ImagedContact imagedContact : imagedContacts) {
				final Contact contact = imagedContact.getContact();
				final TimeSeriesEntity ts = contact.getTrace();
				TraceForImage traceForImage = new TraceForImage();
				traceForImages.add(traceForImage);
				traceForImage.setChannel(ts.getLabel());
				traceForImage.setPixelCoordX(imagedContact.getPxCoordinates()
						.getX().intValue());
				traceForImage.setPixelCoordY(imagedContact.getPxCoordinates()
						.getY().intValue());
			}
		}
		return traceForImages;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.dao.snapshots.IImageDAO#findByStudyDirAndImageType(java.lang.String,
	 *      edu.upenn.cis.braintrust.model.ImageEntity.Type)
	 */
	@Override
	public Set<ImageForStudy> findByStudyDirAndImageType(String studyDir,
			ImageType imageType) {
		final Query query =
				getSession()
						.getNamedQuery("Image-findByStudyDirAndImageType")
						.setParameter("studyDir", studyDir)
						.setParameter("imageType", imageType);

		@SuppressWarnings("unchecked")
		final List<ImageEntity> imageList = query.list();
		final Set<ImageEntity> imageEntities = newHashSet(imageList);
		return images2ImagesForStudy(imageEntities);
	}

	static private Set<ImageForStudy> images2ImagesForStudy(
			Set<ImageEntity> imageEntities) {
		final Set<ImageForStudy> imagesForStudy = newHashSet();
		for (ImageEntity imageEntity : imageEntities) {
			ImageForStudy imageForStudy = new ImageForStudy();
			imagesForStudy.add(imageForStudy);
			imageForStudy.setImageFileKey(imageEntity.getFileKey());
			imageForStudy.setImageTypeEnum(imageEntity.getType());
		}
		return imagesForStudy;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see edu.upenn.cis.braintrust.dao.snapshots.IImageDAO#findByStudyDir(java.lang.String)
	 */
	@Override
	public Set<ImageForStudy> findByStudyDir(String studyDir) {
		final Query query = getSession().getNamedQuery(
				"Image-findByStudyDir").setParameter(
				"studyDir", studyDir);

		@SuppressWarnings("unchecked")
		final List<ImageEntity> imageList = query.list();
		final Set<ImageEntity> imageEntities = newHashSet(imageList);
		return images2ImagesForStudy(imageEntities);

	}

	@Override
	public long countImagesDataset(Dataset ds) {
		return (Long) getSession()
				.createQuery(
						"select count(*) from Dataset p join p.images i where p = :parent")
				.setParameter("parent", ds).uniqueResult();
	}

	@Override
	public long countImagesRecording(Recording recording) {
		return (Long) getSession()
				.createQuery(
						"select count(*) from Recording r join r.images i where r = :recording")
				.setParameter("recording", recording).uniqueResult();
	}
}
