/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OptimisticLock;

/**
 * This entity should be thought of as a component of its parent montage. We
 * include a version field to leverage Hibernate's versioning - propagating
 * changes up to its parent.
 * 
 * @author Sam Donnelly
 */
@Entity(name = "MontagedChannel")
@Table(name = MontagedChannelEntity.TABLE,
		uniqueConstraints = {
				@UniqueConstraint(
						columnNames = {
								"parent_id",
								MontagedChannelEntity.NAME_COLUMN
						}
				)
		})
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = MontagedChannelEntity.ID_COLUMN)
		)
})
public class MontagedChannelEntity extends LongIdAndVersion {

	public static final String TABLE = "montaged_channel";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String NAME_COLUMN = "name";

	private MontageEntity parent;
	private String name;
	private TimeSeriesEntity channel;
	private Set<ReferenceChannelEntity> referenceChannels = newHashSet();

	/** For JPA. */
	MontagedChannelEntity() {}

	public MontagedChannelEntity(
			MontageEntity parent,
			String name,
			TimeSeriesEntity channel) {
		this.parent = checkNotNull(parent);
		parent.getMontagedChannels().add(this);
		this.name = checkNotNull(name);
		this.channel = checkNotNull(channel);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "channel_id")
	@NotNull
	public TimeSeriesEntity getChannel() {
		return channel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public MontageEntity getParent() {
		return parent;
	}

	@OptimisticLock(excluded = true)
	@Column(name = NAME_COLUMN)
	@Size(min = 1, max = 255)
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * This side is the owner of the collection so that the version number gets
	 * incremented when we add/remove from the collection.
	 */
	@ElementCollection
	@CollectionTable(
			name = "reference_channel",
			joinColumns = @JoinColumn(name = "parent_id"))
	public Set<ReferenceChannelEntity> getReferenceChannels() {
		return referenceChannels;
	}

	@SuppressWarnings("unused")
	private void setChannel(TimeSeriesEntity channel) {
		this.channel = channel;
	}

	@SuppressWarnings("unused")
	private void setParent(MontageEntity montage) {
		this.parent = montage;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setReferenceChannels(
			Set<ReferenceChannelEntity> referenceChannels) {
		this.referenceChannels = referenceChannels;
	}
}
