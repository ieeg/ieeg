/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.identity.hibernate;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class UserDAOHibernate
		extends GenericHibernateDAO<UserEntity, UserId>
		implements IUserDAO {

	public UserDAOHibernate() {
	}
	
	public UserDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public UserEntity getOrCreateUser(UserId userId) {
		UserEntity user = get(userId);
		if (user == null) {
			user = new UserEntity(userId);
			saveOrUpdate(user);
		}
		return user;
	}

	// @Override
	// public UserEntity findUserByUid(UserId userId) {
	// final Session session = getSession();
	// final Query query = session.getNamedQuery(
	// UserEntity.FIND_BY_USER_ID)
	// .setParameter("userId", userId.getValue());
	// return (UserEntity) query.uniqueResult();
	// }

}
