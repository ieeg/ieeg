/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
@Immutable
public final class EpilepsySubtypeMetadata implements Serializable {
	private static final long serialVersionUID = 1L;
	private EpilepsyType epilepsyType;
	private Etiology etiology;
	
	EpilepsySubtypeMetadata() {}

	public EpilepsySubtypeMetadata(
			EpilepsyType epilepsyType,
			Etiology etiology) {
		this.epilepsyType = epilepsyType;
		this.etiology = etiology;
	}

	public EpilepsyType getEpilepsyType() {
		return epilepsyType;
	}

	public Etiology getEtiology() {
		return etiology;
	}

}
