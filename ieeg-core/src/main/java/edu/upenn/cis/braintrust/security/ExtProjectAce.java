/*
 * Copyright (C) 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.IAce;

/**
 * Access Control Entry for a project group.
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class ExtProjectAce implements IAce {

	private static final long serialVersionUID = 1L;
	private ProjectGroup projectGroup;
	private String targetId;
	private Integer aclVersion;
	private Optional<Long> aceId;
	private Optional<Integer> aceVersion;
	private Set<ExtPermission> projectGroupPerms = newHashSet();

	// For GWT
	@SuppressWarnings("unused")
	private ExtProjectAce() {}

	/**
	 * 
	 * @param projectId
	 * @param projectName TODO
	 * @param projectGroupType
	 * @param targetId
	 * @param aclVersion
	 * @param aceId
	 * @param aceVersion
	 */
	public ExtProjectAce(
			String projectId,
			String projectName,
			ProjectGroupType projectGroupType,
			String targetId,
			Integer aclVersion,
			@Nullable Long aceId,
			@Nullable Integer aceVersion) {
		this.projectGroup = new ProjectGroup(
				checkNotNull(projectId),
				checkNotNull(projectName),
				checkNotNull(projectGroupType));
		this.targetId = checkNotNull(targetId);
		this.aclVersion = checkNotNull(aclVersion);
		this.aceId = Optional.fromNullable(aceId);
		this.aceVersion = Optional.fromNullable(aceVersion);
	}

	@Override
	public String getTargetId() {
		return targetId;
	}

	public Integer getAclVersion() {
		return aclVersion;
	}

	public Optional<Integer> getAceVersion() {
		return aceVersion;
	}

	public ProjectGroup getProjectGroup() {
		return projectGroup;
	}

	@Override
	public Set<ExtPermission> getPerms() {
		return projectGroupPerms;
	}

	public Optional<Long> getAceId() {
		return aceId;
	}

	@Override
	public String toString() {
		return "ExtProjectAce [projectGroup=" + projectGroup + ", targetId="
				+ targetId + ", aclVersion=" + aclVersion + ", aceId=" + aceId
				+ ", aceVersion=" + aceVersion + ", projectGroupPerms="
				+ projectGroupPerms + "]";
	}
	
	

}
