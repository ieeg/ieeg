/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.testhelper;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IControlFileDAO;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.annotations.ILayerDAO;
import edu.upenn.cis.braintrust.dao.annotations.IMontagedChAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.DrugDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.SpeciesDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimRegionDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.StimTypeDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.provenance.IProvenanceDAO;
import edu.upenn.cis.braintrust.dao.provenance.hibernate.ProvenanceDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectTaskDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.AnalyzedDsDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DatasetDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ImageDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.IToolDAO;
import edu.upenn.cis.braintrust.dao.tools.ToolDAOHibernate;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.model.EegMontageEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.LayerEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.RecordingObjectTaskEntity;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;

public class TestDbCleaner {

	private final Configuration config;
	private final SessionFactory sessionFactory;
	private final IDAOFactory daoFactory;

	private IPatientDAO patientDAO;
	private IDatasetDAO datasetDAO;
	private IAnalyzedDataSnapshotDAO analyzedDataSnapshotDAO;
	private IImageDAO imageDAO;
	private IToolDAO toolDAO;
	private ISpeciesDAO speciesDAO;
	private IStimRegionDAO stimRegionDAO;
	private IStimTypeDAO stimTypeDAO;
	private IDrugDAO drugDAO;
	private IProvenanceDAO provenanceDAO;
	

	public TestDbCleaner(
			final SessionFactory sessionFactory,
			final Configuration config) {
		this.sessionFactory = sessionFactory;
		this.config = config;
		this.daoFactory = new HibernateDAOFactory();
	}

	private void initDAOs(
			final Session session) {

		this.patientDAO = new PatientDAOHibernate(session);
		this.datasetDAO = new DatasetDAOHibernate(session);
		this.analyzedDataSnapshotDAO = new AnalyzedDsDAOHibernate(session);
		this.imageDAO = new ImageDAOHibernate(session);
		this.toolDAO = new ToolDAOHibernate(session);
		this.speciesDAO = new SpeciesDAOHibernate(session);
		this.stimRegionDAO = new StimRegionDAOHibernate(session);
		this.stimTypeDAO = new StimTypeDAOHibernate(session);
		this.drugDAO = new DrugDAOHibernate(session);
		this.provenanceDAO = new ProvenanceDAOHibernate(session);
	}

	public void deleteEverything() throws Exception {
		Session session = null;
		Transaction trx = null;
		try {
			session = sessionFactory.openSession();
			initDAOs(session);
			trx = session.beginTransaction();
			
			final IControlFileDAO controlFileDAO = daoFactory.getControlFileDAO(session);
			for (final ControlFileEntity controlFile : controlFileDAO.findAll()) {
				controlFileDAO.delete(controlFile);
			}
			
			final ITsAnnotationDAO tsAnnotationDAO = daoFactory
					.getTsAnnotationDAO(
							session,
							config);
			for (final TsAnnotationEntity annotation : tsAnnotationDAO
					.findAll()) {
				tsAnnotationDAO.delete(annotation);
			}

			// This needs to happen before studies and datasets are deleted
			for (final AnalyzedDataSnapshot analyzedDataSnapshot : analyzedDataSnapshotDAO
					.findAll()) {
				analyzedDataSnapshotDAO.delete(analyzedDataSnapshot);
			}

			// This needs to happen before datasets are deleted
			final IJobDAO jobDAO = daoFactory.getJobDAO(session);
			for (final JobEntity job : jobDAO.findAll()) {
				jobDAO.delete(job);
			}

			for (final SnapshotUsage usage : provenanceDAO.findAll()) {
				provenanceDAO.delete(usage);
			}

			final IProjectDAO projectDAO = daoFactory.getProjectDAO(session);

			// Empty all projects before attempting to delete DataSnapshots.
			for (final ProjectEntity project : projectDAO.findAll()) {
				project.getSnapshots().clear();
			}

//			ILayerDAO layerDAO = daoFactory.getLayerDAO(session);
//			IMontagedChAnnotationDAO montagedChAnnDAO =
//					daoFactory.getMontagedChAnnotationDAO(session);
//
//			for (final LayerEntity layer : layerDAO.findAll()) {
//				montagedChAnnDAO.deleteByParent(layer);
//				layerDAO.delete(layer);
//			}

			IRecordingObjectTaskDAO recObjTaskDAO = daoFactory
					.getRecordingObjectTaskDAO(session);
			for (final RecordingObjectTaskEntity recObjTask : recObjTaskDAO
					.findAll()) {
				recObjTaskDAO.delete(recObjTask);
			}
			
			IEegMontageDAO montageDAO = daoFactory.getEegMontageDAO(session);
			for (final EegMontageEntity montage : montageDAO.findAll()) {
				montageDAO.delete(montage);
			}

			// Needs to be deleted before images and time series (which are
			// deleted
			// via patient) and after analyzed studies
			for (final Dataset dataset : datasetDAO.findAll()) {
				datasetDAO.delete(dataset);
			}

			for (final ProjectEntity project : projectDAO.findAll()) {
				projectDAO.delete(project);
			}

			for (final Patient patient : patientDAO.findAll()) {
				patientDAO.delete(patient);
			}

			final IAnimalDAO animalDAO = daoFactory.getAnimalDAO(session);
			for (final AnimalEntity animal : animalDAO.findAll()) {
				animalDAO.delete(animal);
			}

			session.flush();

			for (final SpeciesEntity species : speciesDAO.findAll()) {
				speciesDAO.delete(species);
			}

			// get rid of whatever time series and images are left over
			for (final ImageEntity i : imageDAO.findAll()) {
				imageDAO.delete(i);
			}

			session.flush();

			final ITimeSeriesDAO timeSeriesDAO = daoFactory
					.getTimeSeriesDAO(session);
			for (final TimeSeriesEntity ts : timeSeriesDAO.findAll()) {
				timeSeriesDAO.delete(ts);
			}

			session.flush();

			for (final ToolEntity tool : toolDAO.findAll()) {
				toolDAO.delete(tool);
			}

			session.flush();

			final ISessionDAO sessionDAO = daoFactory.getSessionDAO(session);
			for (final SessionEntity sessionEntity : sessionDAO.findAll()) {
				sessionDAO.delete(sessionEntity);
			}

			session.flush();

			final IUserDAO userDAO = daoFactory.getUserDAO(session);
			for (final UserEntity user : userDAO.findAll()) {
				userDAO.delete(user);
			}

			session.flush();

			for (final StimRegionEntity stimRegion : stimRegionDAO.findAll()) {
				stimRegionDAO.delete(stimRegion);
			}

			session.flush();

			for (final StimTypeEntity stimType : stimTypeDAO.findAll()) {
				stimTypeDAO.delete(stimType);
			}

			session.flush();

			for (final DrugEntity drug : drugDAO.findAll()) {
				drugDAO.delete(drug);
			}
			PersistenceUtil.commit(trx);
		} catch (Exception e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}
}
