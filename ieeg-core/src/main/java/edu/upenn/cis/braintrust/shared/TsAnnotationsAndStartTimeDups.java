/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.List;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableList;

@GwtCompatible(serializable = true)
public final class TsAnnotationsAndStartTimeDups implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TsAnnotationDto> tsAnnotations;

	private long lastStartTimeDups;

	/** For GWT. */
	@SuppressWarnings("unused")
	private TsAnnotationsAndStartTimeDups() {}

	public TsAnnotationsAndStartTimeDups(
			List<TsAnnotationDto> tsAnnotations,
			long lastStartTimeDups) {
		this.tsAnnotations = ImmutableList.copyOf(tsAnnotations);
		this.lastStartTimeDups = lastStartTimeDups;
	}

	public long getLastStartTimeDups() {
		return lastStartTimeDups;
	}

	public List<TsAnnotationDto> getTsAnnotations() {
		return tsAnnotations;
	}

}
