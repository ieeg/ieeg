/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

public class GwtMriCoordinates implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double x;

	private Double y;

	private Double z;

	GwtMriCoordinates() {}

	public GwtMriCoordinates(final Double x, final Double y, final Double z) {
		this.x = checkNotNull(x);
		this.y = checkNotNull(y);
		this.z = checkNotNull(z);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof GwtMriCoordinates)) {
			return false;
		}
		final GwtMriCoordinates other = (GwtMriCoordinates) obj;
		if (x == null) {
			if (other.x != null) {
				return false;
			}
		} else if (!x.equals(other.x)) {
			return false;
		}
		if (y == null) {
			if (other.y != null) {
				return false;
			}
		} else if (!y.equals(other.y)) {
			return false;
		}
		if (z == null) {
			if (other.z != null) {
				return false;
			}
		} else if (!z.equals(other.z)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the x
	 */
	public Double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public Double getY() {
		return y;
	}

	/**
	 * @return the z
	 */
	public Double getZ() {
		return z;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		result = prime * result + ((z == null) ? 0 : z.hashCode());
		return result;
	}

	/**
	 * @param x the x to set
	 */
	@SuppressWarnings("unused")
	private void setX(final Double x) {
		this.x = x;
	}

	/**
	 * @param y the y to set
	 */
	@SuppressWarnings("unused")
	private void setY(final Double y) {
		this.y = y;
	}

	/**
	 * @param z the z to set
	 */
	@SuppressWarnings("unused")
	private void setZ(final Double z) {
		this.z = z;
	}

}
