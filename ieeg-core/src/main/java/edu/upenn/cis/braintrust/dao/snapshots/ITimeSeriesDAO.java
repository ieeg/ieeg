/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots;

import java.util.List;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface ITimeSeriesDAO extends IDAO<TimeSeriesEntity, Long> {

	boolean isInADataset(TimeSeriesEntity timeSeries);

	long countTsAnns(TimeSeriesEntity timeSeries);

	int countByRecording(Recording rec);

	List<TimeSeriesEntity> findByDataSnapshot(DataSnapshotEntity dataSnapshot);

	public TimeSeriesEntity findByTimeSeriesPath(String path);
}
