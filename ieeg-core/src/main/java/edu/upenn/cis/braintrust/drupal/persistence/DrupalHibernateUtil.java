/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.persistence;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;

public class DrupalHibernateUtil {

	public static final String DRUPAL_SESS_FAC_KEY = "drupal";
	public static final String PROPS_FILE =
			DRUPAL_SESS_FAC_KEY + "-hibernate.properties";

	/**
	 * Prevent inheritance and instantiation.
	 * 
	 * @throws AssertionError always
	 */
	private DrupalHibernateUtil() {
		throw new AssertionError("Can't instantiate a DrupalHibernateUtil");
	}

	public static Configuration getConfiguration() {
		return HibernateUtil
				.getConfiguration(DRUPAL_SESS_FAC_KEY);
	}

	public static SessionFactory getSessionFactory() {
		return HibernateUtil
				.getSessionFactory(DRUPAL_SESS_FAC_KEY);
	}

	/**
	 * @param properties
	 */
	public static void putConfigProps(Properties properties) {
		HibernateUtil.putConfigProps(DRUPAL_SESS_FAC_KEY, properties);
	}

}
