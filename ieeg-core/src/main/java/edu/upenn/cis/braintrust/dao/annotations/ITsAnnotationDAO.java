/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.annotations;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.datasnapshot.IAnnotationWriter;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.AnnotationCube;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface ITsAnnotationDAO extends
		IDAO<TsAnnotationEntity, Long> {
	int deleteByParent(DataSnapshotEntity ds);

	int deleteByParentAndId(DataSnapshotEntity ds, String tsAnnRevId);

	int deleteByParentAndIds(DataSnapshotEntity ds, Set<String> tsAnnRevId);

	/**
	 * Delete all annotations in {@code parent} that are only annotating
	 * {@code ts}.
	 * 
	 * @param parent the parent of all annotations to delete
	 * @param ts if the annotations are only annotating this time series
	 * 
	 * @return the number of annotations deleted
	 */
	int removeAnnotatedAndDeleteEmptyAnnotations(
			DataSnapshotEntity parent,
			TimeSeriesEntity ts);

	TsAnnotationEntity findTsAnn(DataSnapshotEntity ds, String tsAnnRevId);

	int insertAll(
			DataSnapshotEntity srcParent,
			DataSnapshotEntity derivedParent);

	int insertWithRevId(
			String srcTsAnnRevId,
			DataSnapshotEntity srcParent,
			Dataset derivedParent);

	List<TsAnnotationDto> findAllByParent(DataSnapshotEntity parent);

	long findContributedAnnotationCount(UserEntity user);

	Map<String, Long> countLayers(DataSnapshotEntity ds);

	List<TsAnnotationEntity> findByParentOrderByStartTimeAndId(
			DataSnapshotEntity parent,
			long startTimeUutc,
			String layer,
			int start,
			int maxCount);

	List<TsAnnotationEntity> findFromIdAndStart(
			DataSnapshotEntity parent,
			long lastTsaId,
			long startTimeUutc,
			String layer,
			Set<String> tsPubIds,
			int maxCount);

	long countByParentAndStartTime(DataSnapshotEntity parent, long startTimeUutc);

	List<TsAnnotationEntity> findLtStart(
			DataSnapshotEntity parent,
			long startTimeUutc,
			String layer,
			int firstResult,
			int maxCount);

	int deleteByLayer(DataSnapshotEntity parent, String layer);

	int setLayerName(
			DataSnapshotEntity parent,
			String originalName,
			String newName);

	List<TsAnnotationEntity> findByParentOrderByStartTimeAndId(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			Set<TimeSeriesEntity> timeSeries,
			int firstResult,
			int maxResults);

	List<TsAnnotationEntity> findLtStart(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			Set<TimeSeriesEntity> timeSeries,
			int firstResult,
			int maxResults);

	/**
	 * Ordered by layer.
	 */
	void streamTsAnnotations(
			DataSnapshotEntity ds,
			IAnnotationWriter annotationWriter);
	
	/**
	 * Info on annotations, for the content indexer.  Does not
	 * obscure info based on permissions.
	 * 
	 * @return
	 */
	public List<AnnotationCube> getAnnotationCubeInfo();
}
