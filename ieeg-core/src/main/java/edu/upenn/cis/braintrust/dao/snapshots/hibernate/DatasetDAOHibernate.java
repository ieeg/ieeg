/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.base.Preconditions.checkNotNull;

import org.hibernate.Session;
import org.hibernate.SimpleNaturalIdLoadAccess;

import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class DatasetDAOHibernate
		extends GenericHibernateDAO<Dataset, Long>
		implements IDatasetDAO {
	public DatasetDAOHibernate() {

	}

	public DatasetDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public Dataset findByPubId(final String datasetPubId) {
		checkNotNull(datasetPubId);
		SimpleNaturalIdLoadAccess la = getSession()
				.bySimpleNaturalId(Dataset.class);
		return (Dataset) la.load(datasetPubId);
	}

	@Override
	public Integer findContributedSnapshotCount(final UserEntity user) {
		return ((Long) getSession()
				.createQuery(
						"select count(*) " +
						"from Dataset ds "
								+ "where ds.creator = :user ")
				.setParameter("user", user)
				.uniqueResult()).intValue();
		
	}
}