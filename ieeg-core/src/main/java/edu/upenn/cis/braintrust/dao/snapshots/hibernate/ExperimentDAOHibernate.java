/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.Search2Hql;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class ExperimentDAOHibernate
		extends GenericHibernateDAO<ExperimentEntity, Long>
		implements IExperimentDAO {

	public ExperimentDAOHibernate() {

	}

	public ExperimentDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public Set<ExperimentEntity> findBySearch(
			ExperimentSearch experimentSearch,
			User user,
			UserEntity userEntity) {

		StringBuilder fromPart = new StringBuilder("select experiment "
				+ "from Experiment experiment "
				+ "join fetch experiment.parent animal "
				+ "join fetch animal.organization organization "
				+ "join fetch experiment.recording recording ");

		// Just so we can start appending and's
		StringBuilder wherePart = new StringBuilder("where 1 = 1 ");

		Map<String, Object> params = newHashMap();

		Search2Hql.experimentSearch2Query(
				experimentSearch,
				fromPart,
				wherePart,
				params);

		Query q;

		if (user.getRoles().contains(Role.ADMIN)) {
			// they should see everything
			q = getSession().createQuery(fromPart.append(wherePart).toString());
		} else {
			fromPart.append(
					"join experiment.extAcl acl "
							+ ExtAclEntity.FROM_PERMISSIONS);

			wherePart.append("and " + ExtAclEntity.WHERE_PERMISSIONS);

			q = getSession()
					.createQuery(fromPart.append(wherePart).toString())
					.setParameter("user", userEntity);
		}

		for (Map.Entry<String, Object> param : params.entrySet()) {
			if (param.getValue() instanceof Collection<?>) {
				q.setParameterList(
						param.getKey(),
						(Collection<?>) param.getValue());
			} else {
				q.setParameter(param.getKey(), param.getValue());
			}
		}

		@SuppressWarnings("unchecked")
		Set<ExperimentEntity> results = newHashSet(q.list());
		return results;
	}

	@Override
	public long countWorldReadableExperiments() {
		final Long count = (Long) getSession()
				.getNamedQuery(
						ExperimentEntity.COUNT_WORLD_READABLE_EXPERIMENTS)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countAllExperiments() {
		final Long count = (Long) getSession()
				.getNamedQuery(ExperimentEntity.COUNT_ALL_STUDIES)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public long countUserReadableExperiments() {
		final Long count = (Long) getSession()
				.getNamedQuery(ExperimentEntity.COUNT_USER_READABLE_EXPERIMENTS)
				.uniqueResult();
		return count.longValue();
	}

	@Override
	public ExperimentEntity findByTimeSeries(TimeSeriesEntity timeSeries) {
		checkNotNull(timeSeries);
		ExperimentEntity experiment = (ExperimentEntity) getSession().
				getNamedQuery(ExperimentEntity.BY_TIME_SERIES).
				setParameter("timeSeries", timeSeries).
				uniqueResult();
		return experiment;
	}

	@Override
	public ExperimentEntity findByRecording(Recording recording) {
		checkNotNull(recording);
		ExperimentEntity experiment = (ExperimentEntity) getSession()
				.getNamedQuery(ExperimentEntity.BY_RECORDING)
				.setParameter("recording", recording)
				.uniqueResult();
		return experiment;
	}

}
