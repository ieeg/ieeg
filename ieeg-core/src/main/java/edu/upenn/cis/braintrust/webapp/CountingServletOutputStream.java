/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;

import javax.servlet.ServletOutputStream;

/**
 * An output stream which counts the number of bytes written to the inner
 * {@code ServletOutputStream}.
 * 
 * @author John Frommeyer
 * 
 */
public class CountingServletOutputStream extends ServletOutputStream {
	private final ServletOutputStream os;
	private long byteCount = 0;

	public CountingServletOutputStream(
			ServletOutputStream os) {
		this.os = checkNotNull(os);
	}

	@Override
	public void flush() throws IOException {
		os.flush();
	}

	@Override
	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		os.write(b, off, len);
		byteCount += len;
	}

	@Override
	public void write(int b) throws IOException {
		os.write(b);
		byteCount++;
	}

	@Override
	public void close() throws IOException {
		os.close();
	}

	public long getByteCount() {
		return byteCount;
	}

}
