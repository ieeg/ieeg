/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

/**
 * Information for an image.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public class Image implements Comparable<Image> {

	private ImageType type;
	private String fileKey;
	private String revId;
	
	public Image() {
		
	}

	public Image(final String fileKey, final ImageType type) {
		this.type = checkNotNull(type);
		this.fileKey = checkNotNull(fileKey);
	}

	/**
	 * Return the type of the image.
	 * 
	 * @return the type
	 */
	@Nonnull
	public ImageType getType() {
		return type;
	}

	/**
	 * Returns the file key for the image.
	 * 
	 * @return the fileKey
	 */
	@Nonnull
	public String getFileKey() {
		return fileKey;
	}

	/**
	 * Returns the revision id of the image.
	 * 
	 * @return the revision Id
	 */
	public String getRevId() {
		return revId;
	}

	public void setRevId(@Nullable final String revId) {
		this.revId = revId;
	}

	@Override
	public int compareTo(Image o) {
		return fileKey.compareTo(o.getFileKey());
	}

}
