/*
 * Copyright (C) 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

@GwtCompatible(serializable = true)
@Immutable
@JsonIgnoreProperties(ignoreUnknown=true)
public final class ContactGroupMetadata implements SerializableMetadata {

	private static final long serialVersionUID = 1L;

	private String channelPrefix;
	private Location location;
	private Side side;
	private double samplingRate;
	private Double lffSetting;
	private Double hffSetting;

	private SerializableMetadata parent;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("channelPrefix", VALUE_TYPE.STRING);
		valueMap.put("location", VALUE_TYPE.STRING);
		valueMap.put("side", VALUE_TYPE.STRING);

		valueMap.put("samplingRate", VALUE_TYPE.DOUBLE);
		valueMap.put("lffSetting", VALUE_TYPE.DOUBLE);
		valueMap.put("hffSetting", VALUE_TYPE.DOUBLE);
	}

	/** For GWT. */
	@SuppressWarnings("unused")
	private ContactGroupMetadata() {}

	public ContactGroupMetadata(
			String channelPrefix,
			Location location,
			Side side,
			double samplingRate,
			@Nullable Double lffSetting,
			@Nullable Double hffSetting) {
		this.channelPrefix = checkNotNull(channelPrefix);
		this.location = checkNotNull(location);
		this.side = checkNotNull(side);
		this.samplingRate = samplingRate;
		this.lffSetting = lffSetting;
		this.hffSetting = hffSetting;
	}

	public String getChannelPrefix() {
		return channelPrefix;
	}

	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		if (key.equals("samplingRate")) {
			return getSamplingRate();
		}
		if (key.equals("lffSetting") && getLffSetting() != null) {
			return getLffSetting();
		}
		if (key.equals("hffSetting") && getHffSetting() != null) {
			return getHffSetting();
		}
		return null;
	}

	public Double getHffSetting() {
		return hffSetting;
	}

	@Override
	public String getId() {
		return getLabel();
	}

	@Override
	public Integer getIntegerValue(String key) {
		return null;
	}

	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@Override
	public String getLabel() {
		return getChannelPrefix() + getLocation().toString();
	}

	public Double getLffSetting() {
		return lffSetting;
	}

	public Location getLocation() {
		return location;
	}

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Configurations.name();
	}

	@JsonIgnore
	@Override
	public List<GeneralMetadata> getMetadataSetValue(String key) {
		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return null;
	}

	@JsonIgnore
	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	public double getSamplingRate() {
		return samplingRate;
	}

	public Side getSide() {
		return side;
	}

	@JsonIgnore
	@Override
	public List<String> getStringSetValue(String key) {
		return null;
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		if (key.equals("channelPrefix")) {
			return getChannelPrefix();
		}
		if (key.equals("location")) {
			return getLocation().toString();
		}
		if (key.equals("side")) {
			return getSide().toString();
		}

		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	public void setId(String str) {}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata) {
			//setParent((SerializableMetadata) p);
			parent = (SerializableMetadata) p;
		} else {
			throw new RuntimeException(
					"Attempted to set non-serializable parent");
		}
	}

//	@Override
//	public void setParent(SerializableMetadata p) {
//		parent = p;
//	}

	public static final ProvidesKey<ContactGroupMetadata> KEY_PROVIDER = new ProvidesKey<ContactGroupMetadata>() {

		public Object getKey(ContactGroupMetadata item) {
			return (item == null) ? null : item.getChannelPrefix();
		}
		
	};

	public static final TextColumn<ContactGroupMetadata> prefixColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return object.getChannelPrefix();
		}
	};

	public static final TextColumn<ContactGroupMetadata> locationColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return object.getLocation().name();
		}
	};

	public static final TextColumn<ContactGroupMetadata> sideColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return object.getSide().name();
		}
	};

	public static final TextColumn<ContactGroupMetadata> lffColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return object.getLffSetting() != null 
					? String.valueOf(object.getLffSetting())
					: "Unkown";
		}
	};
	public static final TextColumn<ContactGroupMetadata> hffColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return object.getHffSetting() != null ? 
					String.valueOf(object.getHffSetting())
					: "Unkown";
		}
	};

	public static final TextColumn<ContactGroupMetadata> rateColumn = new TextColumn<ContactGroupMetadata> () {
		@Override
		public String getValue(ContactGroupMetadata object) {
			return String.valueOf(object.getSamplingRate());
		}
	};

	public void setChannelPrefix(String channelPrefix) {
		this.channelPrefix = channelPrefix;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void setSide(Side side) {
		this.side = side;
	}

	public void setSamplingRate(double samplingRate) {
		this.samplingRate = samplingRate;
	}

	public void setLffSetting(Double lffSetting) {
		this.lffSetting = lffSetting;
	}

	public void setHffSetting(Double hffSetting) {
		this.hffSetting = hffSetting;
	}

}
