package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

@GwtCompatible(serializable=true)
public class WorkerResponse implements Serializable {
	String channel;
	String source;
	String id;
	String status;
	String code;
	String key;
	String user;
	String payload;

	public WorkerResponse() {}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getUser() {
		return user;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public WorkerResponse(String user, String channel, String source, String id,
			String status, String code, String key, String payload) {
		super();
		this.channel = channel;
		this.source = source;
		this.id = id;
		this.status = status;
		this.code = code;
		this.key = key;
		this.user = user;
		this.payload = payload;
	}
	

	@Override
	public String toString() {
		return "id: " + id + ", key: " + key + ", status: " + status + ", code: " + code;
	}
}
