/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

@GwtCompatible(serializable = true)
public final class MontagedChannelDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private TimeSeriesId channelId;
	private Optional<MontagedChId> id = Optional.absent();
	private Set<ReferenceChannelDto> referenceChannels = newHashSet();

	public MontagedChannelDto(
			String name,
			TimeSeriesId channelId,
			Set<ReferenceChannelDto> referenceChannels) {
		this.name = checkNotNull(name);
		this.channelId = checkNotNull(channelId);
		this.referenceChannels = newHashSet(referenceChannels);
	}

	public MontagedChannelDto(
			String name,
			TimeSeriesId channelId,
			Set<ReferenceChannelDto> referenceChannels,
			MontagedChId id) {
		this(name, channelId, referenceChannels);
		this.id = Optional.of(id);
	}

	public TimeSeriesId getChannelId() {
		return channelId;
	}

	public Optional<MontagedChId> getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<ReferenceChannelDto> getReferenceChannels() {
		return referenceChannels;
	}

	public void setId(MontagedChId id) {
		this.id = Optional.of(id);
	}

	public void setName(String name) {
		this.name = checkNotNull(name);
	}

}
