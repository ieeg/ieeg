/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Index;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;

@Entity
public class ContactGroup implements IHasLongId {

	public static final String ID_COLUMN = "contact_group_id";
	public static final Function<ContactGroup, String> getChannelPrefix = new Function<ContactGroup, String>() {
		@Override
		public String apply(ContactGroup input) {
			return input.getChannelPrefix();
		}
	};

	private Long id;
	private Integer version;
	private Recording parent;
	private Double samplingRate;
	private Double lffSetting;
	private Double hffSetting;
	private Boolean notchFilter;
	private Set<Contact> contacts = newHashSet();
	private String electrodeType;
	private String manufacturer;
	private String modelNo;
	private String channelPrefix;
	private Side side;
	private Location location;
	private Double impedance;

	/** For JPA */
	ContactGroup() {}

	public ContactGroup(String channelPrefix,
			String electrodeType,
			Double samplingRate,
			@Nullable Double lffSetting,
			@Nullable Double hffSetting,
			@Nullable Boolean notchFilter,
			Side side,
			Location location,
			@Nullable Double impedance,
			@Nullable String manufacturer,
			@Nullable String modelNo) {
		this.channelPrefix = checkNotNull(channelPrefix);
		this.electrodeType = checkNotNull(electrodeType);
		this.samplingRate = checkNotNull(samplingRate);
		this.lffSetting = lffSetting;
		this.hffSetting = hffSetting;
		this.notchFilter = notchFilter;
		this.side = checkNotNull(side);
		this.location = checkNotNull(location);
		this.impedance = impedance;
		this.manufacturer = manufacturer;
		this.modelNo = modelNo;
	}

	/**
	 * Add {@code contact} to this study and set contact's parent to be this
	 * electrode.
	 * 
	 * @param contact to be added and have its parent set to this electrode
	 * 
	 * @throws IllegalArgumentException if {@code contact} already has a parent
	 * @throws IllegalArgumentException if this electrode already contains
	 *             {@code contact}
	 */
	public void addContact(final Contact contact) {
		checkNotNull(contact);
		checkArgument(contact.getParent() == null,
				"contact already belongs to a contact group");
		checkArgument(!contacts.contains(contact),
				"this contact group's contacts already contains contact");
		contact.setParent(this);
		contacts.add(contact);
	}

	@Column
	@NotNull
	public String getChannelPrefix() {
		return channelPrefix;
	}

	@Column
	@NotNull
	@Size(min = 1, max = 255)
	public String getElectrodeType() {
		return electrodeType;
	}

	public Double getHffSetting() {
		return hffSetting;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	public Double getLffSetting() {
		return lffSetting;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getModelNo() {
		return modelNo;
	}

	@Column(columnDefinition = "BIT", length = 1)
	public Boolean getNotchFilter() {
		return notchFilter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Recording.ID_COLUMN)
	@NotNull
	public Recording getParent() {
		return parent;
	}

	@Column
	@NotNull
	@Index(name = "IDX_SAMPLING_RATE")
	public Double getSamplingRate() {
		return samplingRate;
	}

	/**
	 * @return the contacts
	 */
	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "parent",
			orphanRemoval = true)
	public Set<Contact> getContacts() {
		return contacts;
	}

	@Version
	@Column(name = "obj_version")
	@VisibleForTesting
	public Integer getVersion() {
		return version;
	}

	public Double getImpedance() {
		return impedance;
	}

	@Column(name = "lobe")
	@NotNull
	public Location getLocation() {
		return location;
	}

	@Column
	@NotNull
	public Side getSide() {
		return side;
	}

	/**
	 * @param channelPrefix the channelPrefix to set
	 */
	public void setChannelPrefix(final String channelPrefix) {
		this.channelPrefix = channelPrefix;
	}

	public void setHffSetting(@Nullable Double hffSetting) {
		this.hffSetting = hffSetting;
	}

	@VisibleForTesting
	public void setId(final Long id) {
		this.id = id;
	}

	public void setLffSetting(@Nullable Double lffSetting) {
		this.lffSetting = lffSetting;
	}

	public void setNotchFilter(final Boolean notchFilter) {
		this.notchFilter = notchFilter;
	}


	public void setParent(@Nullable Recording parent) {
		this.parent = parent;
	}

	public void setSamplingRate(final Double samplingRate) {
		this.samplingRate = samplingRate;
	}

	public void setContacts(final Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public void setElectrodeType(String electrodeType) {
		this.electrodeType = checkNotNull(electrodeType);
	}

	@VisibleForTesting
	public void setVersion(final Integer version) {
		this.version = version;
	}

	public void setImpedance(@Nullable final Double impedance) {
		this.impedance = impedance;
	}

	public void setLocation(final Location location) {
		this.location = location;
	}

	public void setManufacturer(@Nullable final String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public void setModelNo(@Nullable final String modelNo) {
		this.modelNo = modelNo;
	}

	public void setSide(final Side side) {
		this.side = side;
	}
}
