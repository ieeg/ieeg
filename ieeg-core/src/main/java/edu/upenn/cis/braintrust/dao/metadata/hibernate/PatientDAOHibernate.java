/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.metadata.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class PatientDAOHibernate extends
		GenericHibernateDAO<Patient, Long>
		implements IPatientDAO {

	public PatientDAOHibernate() {}

	public PatientDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public Patient findByLabelEager(String patientLabel) {
		final Session theSession = getSession();
		final Query query = theSession.getNamedQuery(
				Patient.PATIENT_BY_LABEL_EAGER)
				.setParameter("label", patientLabel);
		return (Patient) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> retrieveEtiologies() {
		final Session theSession = getSession();
		final Query query = theSession.getNamedQuery(
				"Patient-retrieveEtiologies");
		return query.list();
	}

	@Override
	public Patient findByIdEager(final Long id) {
		final Session theSession = getSession();
		final Query query = theSession.getNamedQuery(
				Patient.PATIENT_BY_ID_EAGER)
				.setParameter("id", id);
		return (Patient) query.uniqueResult();
	}

}
