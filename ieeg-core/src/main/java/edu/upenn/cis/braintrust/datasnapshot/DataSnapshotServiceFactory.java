/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.google.common.cache.Cache;

import edu.upenn.cis.braintrust.dao.ISnapshotDiscussionDAO;
import edu.upenn.cis.braintrust.dao.ProjectDAOHibernate;
import edu.upenn.cis.braintrust.dao.SnapshotDiscussionDAOHibernate;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.RegisteredAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.annotations.hibernate.TsAnnotationDAOHibernate;
import edu.upenn.cis.braintrust.dao.identity.hibernate.UserDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.ContactDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.OrganizationDAOHibernate;
import edu.upenn.cis.braintrust.dao.metadata.hibernate.PatientDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.HasAclDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.PermissionDAOHibernate;
import edu.upenn.cis.braintrust.dao.permissions.hibernate.SessionDAOHibernate;
import edu.upenn.cis.braintrust.dao.provenance.EventLogDAOHibernate;
import edu.upenn.cis.braintrust.dao.provenance.hibernate.DataUsageDAOHibernate;
import edu.upenn.cis.braintrust.dao.provenance.hibernate.ProvenanceDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotJsonDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.AnalyzedDsDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DataSnapshotJsonDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.DatasetDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegMontageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ImageDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.TimeSeriesDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.JobDAOHibernate;
import edu.upenn.cis.braintrust.dao.tools.ToolDAOHibernate;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.UserServiceFactory;

/**
 * Creates {@code IDataSnapshotService} instances.
 * 
 * @author John Frommeyer
 */
public class DataSnapshotServiceFactory implements IDataSnapshotServiceFactory {

	@Override
	public IDataSnapshotService newInstance(
			final Session session,
			final Configuration config,
			final int maxTsAnns,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srCacheEntry) {
		checkNotNull(session);
		final IDataSnapshotDAO dataSnapshotDAO =
				new DataSnapshotDAOHibernate(session);

		final ISnapshotDiscussionDAO discussionDAO =
				new SnapshotDiscussionDAOHibernate(session);

		final IEegStudyDAO studyDAO = new EegStudyDAOHibernate(session);
		final IDatasetDAO datasetDAO = new DatasetDAOHibernate(session);
		final ITsAnnotationDAO tsAnnotationDAO = new TsAnnotationDAOHibernate(
				session, config);
		final IDataSnapshotJsonDAO jsonDAO = new DataSnapshotJsonDAOHibernate(session);
		final ITimeSeriesDAO timeSeriesDAO = new TimeSeriesDAOHibernate(
				session);
		final IAnalyzedDataSnapshotDAO analyzedDsDAO = new AnalyzedDsDAOHibernate(
				session);
		final IJobDAO jobDAO = new JobDAOHibernate(session);
		final IDataSnapshotService service =
				new DataSnapshotService(
						dataSnapshotDAO,
						discussionDAO,
						studyDAO,
						datasetDAO,
						jsonDAO,
						timeSeriesDAO,
						tsAnnotationDAO,
						analyzedDsDAO,
						new HasAclDAOHibernate(session),
						new UserDAOHibernate(session),
						new ToolDAOHibernate(session),
						jobDAO,
						new ContactDAOHibernate(session),
						new ImageDAOHibernate(session),
						new ExperimentDAOHibernate(session),
						new SessionDAOHibernate(session),
						new EventLogDAOHibernate(session),
						new ProvenanceDAOHibernate(session),
						new ProjectDAOHibernate(session),
						new DataUsageDAOHibernate(session),
						new RegisteredAnnotationDAOHibernate(session),
						new OrganizationDAOHibernate(session),
						new PermissionDAOHibernate(session),
						new PatientDAOHibernate(session),
						new EegMontageDAOHibernate(session),
						UserServiceFactory.getUserService(),
						config,
						maxTsAnns,
						datasetShortCache,
						srCacheEntry);
		return service;
	}

	@Override
	public IDataSnapshotService newInstance(
			Session session,
			int maxTsAnns,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srResultsCache) {
		return newInstance(
				session,
				HibernateUtil.getConfiguration(),
				maxTsAnns,
				datasetShortCache,
				srResultsCache);
	}

	@Override
	public IDataSnapshotService newInstance(
			Session session,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> srResultsCache) {
		return newInstance(
				session,
				HibernateUtil.getConfiguration(),
				DEFAULT_MAX_TS_ANNS,
				datasetShortCache,
				srResultsCache);
	}

}
