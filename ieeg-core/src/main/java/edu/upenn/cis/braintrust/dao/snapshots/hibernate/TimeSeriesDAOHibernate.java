/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class TimeSeriesDAOHibernate
		extends GenericHibernateDAO<TimeSeriesEntity, Long>
		implements ITimeSeriesDAO {
	
	public TimeSeriesDAOHibernate() {}

	public TimeSeriesDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public boolean isInADataset(TimeSeriesEntity timeSeries) {
		return getSession()
				.getNamedQuery(TimeSeriesEntity.IS_IN_A_DATASET)
				.setParameter("ts", timeSeries)
				.uniqueResult() != null;
	}

	@Override
	public long countTsAnns(TimeSeriesEntity timeSeries) {
		return (Long) getSession()
				.createQuery(
						"select count(*) from TsAnnotation tsa where tsa.annotated = :annotated")
				.setParameter("annotated", timeSeries)
				.uniqueResult();
	}

	@Override
	public int countByRecording(Recording r) {
		return Ints
				.checkedCast((Long) getSession()
						.createQuery(
								"select count(t) from Recording r join r.contactGroups g join g.contacts c join c.trace t where r = :r")
						.setParameter("r", r).uniqueResult());
	}

	@SuppressWarnings("unchecked")
	public List<TimeSeriesEntity> findByDataSnapshot(
			DataSnapshotEntity dataSnapshot) {
		String query = null;
		if (HibernateUtil.isInstanceOf(dataSnapshot, EegStudy.class)) {
			query = TimeSeriesEntity.BY_STUDY;
		} else if (HibernateUtil.isInstanceOf(dataSnapshot,
				ExperimentEntity.class)) {
			query = TimeSeriesEntity.BY_EXPERIMENT;
		} else if (HibernateUtil.isInstanceOf(dataSnapshot, Dataset.class)) {
			query = TimeSeriesEntity.BY_DATASET;
		} else {
			throw new IllegalArgumentException(
					"unknown DatasnapshotEntity type "
							+ Hibernate.getClass(dataSnapshot.getClass()));
		}
		return (List<TimeSeriesEntity>) getSession()
				.getNamedQuery(query)
				.setParameter("dsId", dataSnapshot.getId())
				.list();

	}

	@Override
	public TimeSeriesEntity findByTimeSeriesPath(String path) {
		String query = TimeSeriesEntity.BY_PATH;
		return (TimeSeriesEntity) getSession()
				.getNamedQuery(query)
				.setParameter("path", path)
				.uniqueResult();
				
	}
}
