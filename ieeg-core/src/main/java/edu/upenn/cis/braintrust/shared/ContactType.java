/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public enum ContactType {
	MICRO("Micro"), MACRO("Macro"), OTHER("Other");

	private final String DISPLAY_STRING;

	/**
	 * Maps a {@code EegStudyType}'s toString representation to the
	 * {@code EegStudyType} itself.
	 */
	public static final SortedMap<String, ContactType> FROM_STRING;
	static {
		final SortedMap<String, ContactType> temp = newTreeMap();
		for (final ContactType type : values()) {
			temp.put(type.toString(), type);
		}
		FROM_STRING = Collections.unmodifiableSortedMap(temp);
	}

	private ContactType(final String displayString) {
		this.DISPLAY_STRING = displayString;
	}

	@Override
	public String toString() {
		return DISPLAY_STRING;
	}

}
