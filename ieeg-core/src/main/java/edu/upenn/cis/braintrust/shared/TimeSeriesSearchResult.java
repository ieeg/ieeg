/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public final class TimeSeriesSearchResult implements IHasLabel, Serializable, Comparable<TimeSeriesSearchResult> {

	private static final long serialVersionUID = 1L;
	private String revId;
	private String label;

	/** For GWT. */
	@SuppressWarnings("unused")
	private TimeSeriesSearchResult() {}

	public TimeSeriesSearchResult(final String revId, final String label) {
		this.revId = checkNotNull(revId);
		this.label = checkNotNull(label);
	}

	/**
	 * @return the label
	 */
	@Override
	public String getLabel() {
		return label;
	}

	/**
	 * @return the revId
	 */
	public String getRevId() {
		return revId;
	}

	@Override
	public int compareTo(TimeSeriesSearchResult o) {
		return revId.compareTo(o.revId);
	}

	public boolean equals(Object o) {
		if (!(o instanceof TimeSeriesSearchResult))
			return false;
		else {
			TimeSeriesSearchResult two = (TimeSeriesSearchResult) o;
			
			return ((revId == null && two.revId == null ||
					(revId != null && revId.equals(two.getRevId()))) && 
					label.equals(two.getLabel()));
		}
	}
}
