/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "MontagedChAnnotation")
@Table(name = MontagedChAnnotationEntity.TABLE)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = MontagedChAnnotationEntity.ID_COLUMN)
		)
})
@NamedQueries(
		value = {
				@NamedQuery(
						name = MontagedChAnnotationEntity.BY_PARENT_ORDER_BY_START_TIME_AND_ID,
						query = "select distinct mca "
								+ "from MontagedChAnnotation mca "
								+ "where mca.parent = :parent "
								+ "and mca.startOffsetUsecs >= :startOffsetUsecs "
								+ "order by mca.startOffsetUsecs, mca.id"),
				@NamedQuery(
						name = MontagedChAnnotationEntity.LT_START_TIME,
						query = "select distinct mca "
								+ "from MontagedChAnnotation mca "
								+ "where mca.parent = :parent "
								+ "and mca.startOffsetUsecs < :startOffsetUsecs "
								+ "order by mca.startOffsetUsecs desc, mca.id asc"),
				@NamedQuery(
						name = MontagedChAnnotationEntity.DELETE_BY_PARENT,
						query = "delete MontagedChAnnotation mca where mca.parent = :parent")
		})
public class MontagedChAnnotationEntity extends LongIdAndVersion {

	public static final String TABLE = "montaged_ch_annotation";
	public static final String ID_COLUMN = TABLE + "_id";

	public static final String BY_PARENT_ORDER_BY_START_TIME_AND_ID = "MontagedChAnnotation-byParentOrderByStartTimeAndId";
	public static final String LT_START_TIME = "MontagedChAnnotation-lessThanStart";
	public static final String DELETE_BY_PARENT = "MontagedChAnnotation-deleteByParent";

	private LayerEntity parent;
	private String type;
	private String description;
	private Set<MontagedChannelEntity> montagedChannels = newHashSet();
	private Long startOffsetUsecs;
	private Long endOffsetUsecs;
	private UserEntity creator;
	private Date createTime = new Date();
	private String color;

	/** For JPA. */
	MontagedChAnnotationEntity() {}

	public MontagedChAnnotationEntity(
			LayerEntity parent,
			String type,
			@Nullable String description,
			long startOffsetUsecs,
			long endOffsetUsecs,
			@Nullable String color,
			UserEntity creator) {
		this.parent = checkNotNull(parent);
		this.type = checkNotNull(type);
		this.description = description;
		this.startOffsetUsecs = startOffsetUsecs;
		this.endOffsetUsecs = endOffsetUsecs;
		this.color = color;
		this.creator = checkNotNull(creator);
	}

	@Size(min = 1, max = 255)
	public String getColor() {
		return color;
	}

	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "creator_id")
	public UserEntity getCreator() {
		return creator;
	}

	@Size(min = 1, max = 255)
	public String getDescription() {
		return description;
	}

	@NotNull
	@Min(0)
	public Long getEndOffsetUsecs() {
		return endOffsetUsecs;
	}

	@ManyToMany
	@JoinTable(
			name = TABLE + "_" + MontagedChannelEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns =
			@JoinColumn(name = MontagedChannelEntity.ID_COLUMN))
	public Set<MontagedChannelEntity> getMontagedChannels() {
		return montagedChannels;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public LayerEntity getParent() {
		return parent;
	}

	@NotNull
	@Min(0)
	public Long getStartOffsetUsecs() {
		return startOffsetUsecs;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getType() {
		return type;
	}

	public void setColor(@Nullable String color) {
		this.color = color;
	}

	@SuppressWarnings("unused")
	private void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@SuppressWarnings("unused")
	private void setCreator(UserEntity creator) {
		this.creator = creator;
	}

	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	public void setEndOffsetUsecs(Long endOffsetUsecs) {
		this.endOffsetUsecs = endOffsetUsecs;
	}

	public void setMontagedChannels(
			Set<MontagedChannelEntity> montagedChannels) {
		this.montagedChannels = montagedChannels;
	}

	public void setParent(@Nullable LayerEntity parent) {
		this.parent = parent;
	}

	public void setStartOffsetUsecs(Long startOffsetUsecs) {
		this.startOffsetUsecs = startOffsetUsecs;
	}

	public void setType(String type) {
		this.type = type;
	}
}
