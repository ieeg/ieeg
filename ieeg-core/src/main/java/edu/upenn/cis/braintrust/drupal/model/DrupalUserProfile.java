/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class DrupalUserProfile {

	@Embeddable
	public static class Id implements java.io.Serializable {

		private static final long serialVersionUID = 1L;

		private Long fid;
		private Long uid;

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Id)) {
				return false;
			} else {
				Id two = (Id) o;

				return two.fid.equals(fid)
						&& two.getUid().equals(getUid());
			}
		}

		@Column(columnDefinition = "INT UNSIGNED")
		public Long getFid() {
			return fid;
		}

		@Column(columnDefinition = "INT UNSIGNED")
		public Long getUid() {
			return uid;
		}

		@Override
		public int hashCode() {
			return Long.valueOf(fid + uid).hashCode();
		}

		@SuppressWarnings("unused")
		private void setFid(Long fid) {
			this.fid = fid;
		}

		@SuppressWarnings("unused")
		private void setUid(Long uid) {
			this.uid = uid;
		}

	}

	private DrupalUser user;

	private DrupalProfileValue profileValue;

	public DrupalProfileValue getProfileValue() {
		return profileValue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(
			name = DrupalUser.ID_COLUMN,
			insertable = false,
			updatable = false)
	public DrupalUser getUser() {
		return user;
	}

	@SuppressWarnings("unused")
	private void setProfileValue(DrupalProfileValue profileValue) {
		this.profileValue = profileValue;
	}

	@SuppressWarnings("unused")
	private void setUser(DrupalUser user) {
		this.user = user;
	}
}
