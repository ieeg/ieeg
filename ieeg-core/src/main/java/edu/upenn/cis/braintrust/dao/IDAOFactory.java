/*
 * Copyright (C) 2009 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS of ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import edu.upenn.cis.braintrust.dao.annotations.ILayerDAO;
import edu.upenn.cis.braintrust.dao.annotations.IMontageDAO;
import edu.upenn.cis.braintrust.dao.annotations.IMontagedChAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.metadata.IAnimalDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugAdminDAO;
import edu.upenn.cis.braintrust.dao.metadata.IDrugDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.metadata.ISpeciesDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimRegionDAO;
import edu.upenn.cis.braintrust.dao.metadata.IStimTypeDAO;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.provenance.IDataUsageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectTaskDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.IJobHistoryDAO;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

/**
 * Manufactures {@link IDAO}s.
 * 
 * @author Sam Donnelly
 */
public interface IDAOFactory {

	IAnalyzedDataSnapshotDAO getAnalyzedDataSnapshotDAO();

	IAnimalDAO getAnimalDAO(Session session);

	IDatasetDAO getDatasetDAO();

	IDataSnapshotDAO getDataSnapshotDAO(Session session);

	IDataUsageDAO getDataUsageDAO(Session session);

	IDrugDAO getDrugDAO();

	IDrugAdminDAO getDrugAdminDAO();

	IEegStudyDAO getEegStudyDAO();

	IExperimentDAO getExperimentDAO();

	IImageDAO getImageDAO();

	IJobDAO getJobDAO(Session sess);

	IJobHistoryDAO getJobHistoryDAO(Session sess);

	IOrganizationDAO getOrganizationDAO();

	IPatientDAO getPatientDAO();

	IProjectDAO getProjectDAO(Session session);

	IRecordingDAO getRecordingDAO(Session session);

	ISessionDAO getSessionDAO(Session session);

	ISpeciesDAO getSpeciesDAO();

	IStimRegionDAO getStimRegionDAO();

	IStimTypeDAO getStimTypeDAO();

	ITimeSeriesDAO getTimeSeriesDAO(Session session);

	ITsAnnotationDAO getTsAnnotationDAO(Session session, Configuration config);

	IUserDAO getUserDAO(Session session);

//	IMontageDAO getMontageDAO();

//	ILayerDAO getLayerDAO(Session session);

//	IMontagedChAnnotationDAO getMontagedChAnnotationDAO(Session session);
	
	IRecordingObjectTaskDAO getRecordingObjectTaskDAO(Session session);
	
	IRecordingObjectDAO getRecordingObjectDAO(Session session);

	IControlFileDAO getControlFileDAO(Session session);
	
	IEegMontageDAO getEegMontageDAO(Session session);

}
