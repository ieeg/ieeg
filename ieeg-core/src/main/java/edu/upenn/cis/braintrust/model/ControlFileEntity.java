/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.google.common.annotations.VisibleForTesting;

@NamedQueries({
		@NamedQuery(
				name = ControlFileEntity.BY_BUCKET_FILE_KEY,
				query = "From ControlFile cf "
						+ "where cf.bucket = :bucket "
						+ "and cf.fileKey = :fileKey"),
		@NamedQuery(
				name = ControlFileEntity.ORDERED_BY_CREATE_TIME_ID,
				query = "From ControlFile cf "
						+ "where cf.metadata is null "
						+ "order by cf.createTime asc, cf.id asc"),
		@NamedQuery(
				name = ControlFileEntity.BY_METADATA_ORDERED_BY_CREATE_TIME_ID,
				query = "From ControlFile cf "
						+ "where cf.metadata = :metadata "
						+ "order by cf.createTime asc, cf.id asc")
})
@Entity(name = "ControlFile")
@Table(name = ControlFileEntity.TABLE,
		uniqueConstraints = @UniqueConstraint(
				columnNames = {
						ControlFileEntity.BUCKET_COLUMN,
						ControlFileEntity.FILE_KEY_COLUMN
				}))
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = ControlFileEntity.ID_COLUMN)
		)
})
public class ControlFileEntity extends LongIdAndVersion {

	public final static String TABLE = "control_file";
	public final static String ID_COLUMN = TABLE + "_id";
	public final static String BUCKET_COLUMN = "bucket";
	public final static String FILE_KEY_COLUMN = "file_key";
	public final static String BY_BUCKET_FILE_KEY = "ControlFileEntity-findByBucketFileKey";
	public final static String ORDERED_BY_CREATE_TIME_ID = "ControlFileEntity-findOrderedByCreateTimeAndId";
	public final static String BY_METADATA_ORDERED_BY_CREATE_TIME_ID = "ControlFileEntity-findByQueueOrderedByCreateTimeAndId";

	private String bucket;
	private String fileKey;

	private UserEntity creator;
	private Date createTime = new Date();

	private String status = "NEW";
	private String metadata;

	ControlFileEntity() {}

	public ControlFileEntity(
			String bucket,
			String fileKey,
			UserEntity creator) {
		this.fileKey = checkNotNull(fileKey);
		checkArgument(
				fileKey.length() >= 1,
				"fileKey too short");
		checkArgument(
				fileKey.length() <= 255,
				"fileKey too long");

		this.bucket = checkNotNull(bucket);
		checkArgument(
				bucket.length() >= 1,
				"bucket too short");
		checkArgument(
				bucket.length() <= 255,
				"bucket too long");
		this.creator = checkNotNull(creator);
	}

	@Size(min = 1, max = 255)
	@Column(name = ControlFileEntity.BUCKET_COLUMN)
	@NotNull
	public String getBucket() {
		return bucket;
	}

	@Transient
	public String getPrefix() {
		final int idx = fileKey.lastIndexOf('/');
		return idx == -1
				? ""
				: fileKey.substring(0, idx);
	}

	@Transient
	public String getName() {
		final int idx = fileKey.lastIndexOf('/');
		return fileKey.substring(idx + 1);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id")
	@NotNull
	public UserEntity getCreator() {
		return creator;
	}

	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@Lob
	public String getMetadata() {
		return metadata;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getStatus() {
		return status;
	}

	@NotNull
	@Column(name = ControlFileEntity.FILE_KEY_COLUMN)
	@Size(min = 1, max = 255)
	public String getFileKey() {
		return fileKey;
	}

	@SuppressWarnings("unused")
	private void setBucket(String bucket) {
		this.bucket = bucket;
	}

	@SuppressWarnings("unused")
	private void setFileKey(String fileKey) {
		this.fileKey = fileKey;
	}

	@SuppressWarnings("unused")
	private void setCreator(UserEntity creator) {
		this.creator = creator;
	}

	@VisibleForTesting
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setMetadata(@Nullable String metadata) {
		this.metadata = metadata;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
