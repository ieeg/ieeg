/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;

/**
 * Assembles a {@code TimeSeriesAnnotation} from a
 * {@code TimeSeriesAnnotationEntity}.
 * 
 * @author John Frommeyer
 * 
 */
public class TsAnnotationAssembler {

	public static List<TsAnnotationDto>
			assemble(Iterable<? extends TsAnnotationEntity> tsaEntities) {
		List<TsAnnotationDto> tsAnnDtos = newArrayList();
		for (TsAnnotationEntity tsaEntity : tsaEntities) {
			tsAnnDtos.add(assemble(tsaEntity));
		}
		return tsAnnDtos;
	}

	public static TsAnnotationDto assemble(
			final TsAnnotationEntity aEntity) {
		checkNotNull(aEntity);

		Set<TimeSeriesDto> tsDtos = newHashSet();

		for (TimeSeriesEntity tsEntity : aEntity.getAnnotated()) {
			tsDtos.add(
					TimeSeriesAssembler.assemble(tsEntity));
		}

		final TsAnnotationDto annotation = new TsAnnotationDto(
				tsDtos,
				aEntity.getAnnotator(),
				aEntity.getStartOffsetUsecs(),
				aEntity.getEndOffsetUsecs(),
				aEntity.getType(),
				aEntity.getPubId(),
				aEntity.getDescription(),
				null,
				aEntity.getLayer(),
				aEntity.getColor());
		return annotation;
	}
}
