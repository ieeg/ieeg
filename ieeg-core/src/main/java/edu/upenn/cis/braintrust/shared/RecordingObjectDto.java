/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

/**
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class RecordingObjectDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Optional<RecordingObjectId> id;
	private Long parentId;
	

	@SuppressWarnings("unused")
	private RecordingObjectDto() {}

	public RecordingObjectDto(
			Long parentId) {
		this.parentId = checkNotNull(parentId);
		this.id = Optional.absent();
	}

	public RecordingObjectDto(
			Long parentId,
			RecordingObjectId id) {
		this(parentId);
		this.id = Optional.of(id);
	}

	public Optional<RecordingObjectId> getId() {
		return id;
	}

	public Long getParentId() {
		return this.parentId;
	}

	public void setId(RecordingObjectId id) {
		this.id = Optional.of(id);
	}

	public void setParentId(Long parentId) {
		this.parentId = checkNotNull(parentId);
	}
}
