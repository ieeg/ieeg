/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.annotations.hibernate;

import static com.google.common.collect.Sets.newLinkedHashSet;

import java.util.List;
import java.util.Set;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.annotations.IMontagedChAnnotationDAO;
import edu.upenn.cis.braintrust.model.LayerEntity;
import edu.upenn.cis.braintrust.model.MontagedChAnnotationEntity;

public class MontagedChAnnotationDAOHibernate
		extends GenericHibernateDAOLongId<MontagedChAnnotationEntity>
		implements IMontagedChAnnotationDAO {

	public MontagedChAnnotationDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public Set<MontagedChAnnotationEntity> findByParentOrderByStartTimeAndId(
			LayerEntity parent,
			long startOffsetUsecs,
			int firstResult,
			int maxResults) {
		Query q = getSession()
				.getNamedQuery(
						MontagedChAnnotationEntity.BY_PARENT_ORDER_BY_START_TIME_AND_ID)
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ));

		@SuppressWarnings("unchecked")
		List<MontagedChAnnotationEntity> montagedChAnns = (List<MontagedChAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return newLinkedHashSet(montagedChAnns);
	}

	@Override
	public Set<MontagedChAnnotationEntity> findLtStart(
			LayerEntity parent,
			long startOffsetUsecs,
			int firstResult,
			int maxResults) {
		Query q = getSession()
				.getNamedQuery(
						MontagedChAnnotationEntity.LT_START_TIME)
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ));

		@SuppressWarnings("unchecked")
		List<MontagedChAnnotationEntity> montagedChAnns = (List<MontagedChAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return newLinkedHashSet(montagedChAnns);
	}

	@Override
	public int deleteByParent(LayerEntity parent) {
		Query q = getSession()
				.getNamedQuery(
						MontagedChAnnotationEntity.DELETE_BY_PARENT)
				.setParameter("parent", parent);
		int deleted = q.executeUpdate();
		return deleted;
	}
}
