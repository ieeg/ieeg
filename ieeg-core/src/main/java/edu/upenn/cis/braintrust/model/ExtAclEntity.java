/*
 * Copyright (C) 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ProjectGroupType;

@Entity(name = "ExtAcl")
@Table(name = ExtAclEntity.TABLE)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(name = ExtAclEntity.ID_COLUMN)
		)
})
public class ExtAclEntity extends LongIdAndVersion {

	public static final String TABLE = "ext_acl";
	public static final String ID_COLUMN = TABLE + "_id";

	private Set<ProjectAceEntity> projectAces = newHashSet();
	private Set<ExtUserAceEntity> userAces = newHashSet();
	private Set<PermissionEntity> worldAce = newHashSet();

	public static final String WHERE_USER_READABLE =
			"exists (from ExtUserAce userAce "
					+ "join userAce.perms userPerm "
					+ "join userPerm.mode userPermMode "
					+ "where userAce member of acl.userAces and userPermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or exists (from ProjectAce projectAce "
					+ "join projectAce.perms projectPerm "
					+ "join projectPerm.mode projectPermMode "
					+ "where projectAce member of acl.projectAces and projectPermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or exists (from Permission worldPerm "
					+ "join worldPerm.mode worldPermMode "
					+ "where worldPerm member of acl.worldAce and worldPermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "')";

	public static final String FROM_WORLD_READABLE =
			"join acl.worldAce worldAce join worldAce.mode worldAceMode ";

	public static final String WHERE_WORLD_READABLE =
			"(worldAce.name = '"
					+ CorePermDefs.READ_PERMISSION_NAME
					+ "' and worldAceMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') ";

	public static final String FROM_PERMISSIONS =
			"left join acl.userAces userAce "
					+ "left join userAce.perms userAcePerm "
					+ "left join userAcePerm.mode userAcePermMode "
					+ "left join acl.worldAce worldAce "
					+ "left join worldAce.mode worldAceMode "
					+ "left join acl.projectAces projectAce "
					+ "left join projectAce.perms projectAcePerm "
					+ "left join projectAcePerm.mode projectAcePermMode "
					+ "left join projectAce.project project ";

	public static final String WHERE_PERMISSIONS =
			// user
			"((userAce.user = :user and "
					+ "((userAcePerm.name = '"
					+ CorePermDefs.READ_PERMISSION_NAME
					+ "' and userAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (userAcePerm.name = '"
					+ CorePermDefs.EDIT_PERMISSION_NAME
					+ "' and userAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (userAcePerm.name = '"
					+ CorePermDefs.OWNER_PERMISSION_NAME
					+ "' and userAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "'))) "

					// world
					+ "or (worldAce.name = '"
					+ CorePermDefs.READ_PERMISSION_NAME
					+ "' and worldAceMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (worldAce.name = '"
					+ CorePermDefs.EDIT_PERMISSION_NAME
					+ "' and worldAceMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "

					// project admins
					+ "or ("
					+ "projectAce.projectGroup = '"
					+ ProjectGroupType.ADMINS
					+ "' "
					+ " and :user in elements(project.admins) "
					+ " and ((projectAcePerm.name = '"
					+ CorePermDefs.READ_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (projectAcePerm.name = '"
					+ CorePermDefs.EDIT_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (projectAcePerm.name = '"
					+ CorePermDefs.OWNER_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "'))) "

					// project team
					+ "or ("
					+ "projectAce.projectGroup = '"
					+ ProjectGroupType.TEAM
					+ "' "
					+ " and :user in elements(project.team) "
					+ " and ((projectAcePerm.name = '"
					+ CorePermDefs.READ_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (projectAcePerm.name = '"
					+ CorePermDefs.EDIT_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "') "
					+ "or (projectAcePerm.name = '"
					+ CorePermDefs.OWNER_PERMISSION_NAME
					+ "' and projectAcePermMode.name = '"
					+ CorePermDefs.CORE_MODE_NAME
					+ "')))"
					+ ") ";

	public ExtAclEntity() {}

	public ExtAclEntity(PermissionEntity worldAce) {
		this.worldAce.add(worldAce);
	}

	public ExtAclEntity(Set<PermissionEntity> worldAce) {
		this.worldAce.addAll(worldAce);
	}

	@OneToMany(
			mappedBy = "acl",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.EAGER)
	public Set<ProjectAceEntity> getProjectAces() {
		return projectAces;
	}

	@OneToMany(
			mappedBy = "acl",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.EAGER)
	public Set<ExtUserAceEntity> getUserAces() {
		return userAces;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "world_ace",
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = PermissionEntity.ID_COLUMN))
	@ForeignKey(
			name = "fk_world_ace_ext_acl",
			inverseName = "fk_world_ace_permission")
	public Set<PermissionEntity> getWorldAce() {
		return worldAce;
	}

	@SuppressWarnings("unused")
	private void setProjectAces(Set<ProjectAceEntity> projectAces) {
		this.projectAces = projectAces;
	}

	@SuppressWarnings("unused")
	private void setUserAces(Set<ExtUserAceEntity> userAces) {
		this.userAces = userAces;
	}

	@SuppressWarnings("unused")
	private void setWorldAce(Set<PermissionEntity> worldAce) {
		this.worldAce = worldAce;
	}
}
