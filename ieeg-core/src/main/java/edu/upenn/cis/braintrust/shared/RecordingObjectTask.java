/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

/**
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class RecordingObjectTask implements Serializable {

	private static final long serialVersionUID = 1L;

	private RecordingObjectId recordingObjectId;
	private String status;
	private String metadata;

	@SuppressWarnings("unused")
	private RecordingObjectTask() {}

	public RecordingObjectTask(
			RecordingObjectId recordingObjectId,
			String status,
			String metadata) {
		this.recordingObjectId = checkNotNull(recordingObjectId);
		this.status = checkNotNull(status);
		this.metadata = checkNotNull(metadata);
	}

	public RecordingObjectId getRecordingObjectId() {
		return recordingObjectId;
	}

	public String getStatus() {
		return this.status;
	}

	public String getMetadata() {
		return this.metadata;
	}

	public void setStatus(String status) {
		this.status = checkNotNull(status);
	}

	public void setMetadata(String metadata) {
		this.metadata = checkNotNull(metadata);
	}

}
