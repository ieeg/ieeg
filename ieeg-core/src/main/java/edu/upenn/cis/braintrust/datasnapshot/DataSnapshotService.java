/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Collections2.filter;
import static com.google.common.collect.Iterables.all;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableBiMap.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.ISnapshotDiscussionDAO;
import edu.upenn.cis.braintrust.dao.SecurityUtil;
import edu.upenn.cis.braintrust.dao.annotations.IRegisteredAnnotationDAO;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.metadata.IContactDAO;
import edu.upenn.cis.braintrust.dao.metadata.IOrganizationDAO;
import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.dao.permissions.IHasAclDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.dao.permissions.ISessionDAO;
import edu.upenn.cis.braintrust.dao.provenance.IDataUsageDAO;
import edu.upenn.cis.braintrust.dao.provenance.IEventLogDAO;
import edu.upenn.cis.braintrust.dao.provenance.IProvenanceDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotJsonDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDatasetDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegMontageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.dao.snapshots.ITimeSeriesDAO;
import edu.upenn.cis.braintrust.dao.tools.IJobDAO;
import edu.upenn.cis.braintrust.dao.tools.IToolDAO;
import edu.upenn.cis.braintrust.datasnapshot.assembler.AnnotationDefinitionAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.Assembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.DataSnapshotAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.DsSearchResultAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.ImageAssembler;
import edu.upenn.cis.braintrust.datasnapshot.assembler.TaskAssembler;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.model.AnalysisTask;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.EegMontageEntity;
import edu.upenn.cis.braintrust.model.EegMontagePairEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.EventLogEntity;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.ProjectAceEntity;
import edu.upenn.cis.braintrust.model.ProjectDiscussion;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.RegisteredAnnotation;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.SnapshotDiscussion;
import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesInfo;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtPermissionAssembler;
import edu.upenn.cis.braintrust.security.ExtPermissions;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.IExtAuthzHandler;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.ContactGroupMetadata;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.EpilepsySubtypeMetadata;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.IHasName;
import edu.upenn.cis.braintrust.shared.IHasStringId;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtRecording;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.exception.AclTargetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.JobNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.ProjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TaskNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.braintrust.shared.exception.TooManyTsAnnsException;
import edu.upenn.cis.braintrust.shared.exception.ToolNotFoundForDtoException;
import edu.upenn.cis.db.habitat.persistence.mapping.LoggingMapper;
import edu.upenn.cis.db.habitat.persistence.mapping.ProjectMapper;
import edu.upenn.cis.db.habitat.persistence.mapping.ProvenanceMapper;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.Post;

/**
 * A service to provide access to data snapshots and their contents.
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public class DataSnapshotService implements IDataSnapshotService {

	private static boolean isRelevant(
			PermissionEntity perm,
			String entity,
			String mode) {
		if (perm
				.getMode()
				.getDomain()
				.getName()
				.equals(PermissionDomainEntity.PERM_DOMAIN_WILDCARD)
				|| perm.getMode().getDomain().getName().equals(entity)) {
			if (perm.getMode().getName().equals(mode)) {
				return true;
			}
		}
		return false;
	}

	private final IPatientDAO patientDAO;
	private final IDataSnapshotDAO dataSnapshotDAO;
	private final ISnapshotDiscussionDAO discussionDAO;
	private final IEegStudyDAO studyDAO;
	private final IDatasetDAO datasetDAO;
	private final IDataSnapshotJsonDAO jsonDAO;
	private final IEventLogDAO logDAO;
	private final ITimeSeriesDAO timeSeriesDAO;
	private final ITsAnnotationDAO tsAnnotationDAO;
	private final IAnalyzedDataSnapshotDAO analyzedDsDAO;
	private final IUserDAO userDAO;
	private final IExtAuthzHandler extAuthzHandler = AuthCheckFactory
			.getHandler();
	private final IToolDAO toolDAO;
	private final IJobDAO jobDAO;
	private final IHasAclDAO hasAclDAO;
	private final IContactDAO contactDAO;
	private final IImageDAO imageDAO;
	private final IExperimentDAO experimentDAO;
	private final ISessionDAO sessionDAO;
	private final IProvenanceDAO provenanceDAO;
	private final IProjectDAO projectDAO;
	private final IDataUsageDAO dataUsageDAO;
	private final IPermissionDAO permissionDAO;
	private final IRegisteredAnnotationDAO registeredAnnotationDAO;
	private final IOrganizationDAO organizationDAO;
	private final ExtPermissionAssembler permissionAssembler;
	private final IEegMontageDAO ieegMontageDAO;

	private final IUserService userService;
	private final Cache<String, DataSnapshotEntity> datasetShortCache;

	private final Cache<Long, SearchResultCacheEntry> searchResultCache;

	private final static Logger logger = LoggerFactory
			.getLogger(DataSnapshotService.class);

	// private final static Logger timeLogger =
	// LoggerFactory.getLogger("time."
	// + DataSnapshotService.class.getName());
	private final static Logger loggerDsClob = LoggerFactory
			.getLogger(DataSnapshotService.class.getName()
					+ ".dataSnapshotClob");
	private final int FLUSH_SIZE;

	private final int MAX_TS_ANNS;

	/**
	 * Inject the dao's for testing. So you don't have to hit the database. This
	 * has not proved to be that useful, now injecting the session instead since
	 * we are not afraid of migrating off Hibernate.
	 * 
	 * @param userService TODO
	 * 
	 */
	public DataSnapshotService(
			IDataSnapshotDAO dataSnapshotDAO,
			ISnapshotDiscussionDAO snapshotDAO,
			IEegStudyDAO studyDAO,
			IDatasetDAO datasetDAO,
			IDataSnapshotJsonDAO jsonDAO,
			ITimeSeriesDAO timeSeriesDAO,
			ITsAnnotationDAO tsAnnotationDAO,
			IAnalyzedDataSnapshotDAO analyzedEegStudyDAO,
			IHasAclDAO extAclDAO,
			IUserDAO userDAO,
			IToolDAO toolDAO,
			IJobDAO jobDAO,
			IContactDAO contactDAO,
			IImageDAO imageDAO,
			IExperimentDAO experimentDAO,
			ISessionDAO sessionDAO,
			IEventLogDAO logDAO,
			IProvenanceDAO provenanceDAO,
			IProjectDAO projectDAO,
			IDataUsageDAO dataUsageDAO,
			IRegisteredAnnotationDAO registeredAnnotationDAO,
			IOrganizationDAO organizationDAO,
			IPermissionDAO permissionDAO,
			IPatientDAO patientDAO,
			IEegMontageDAO ieegMontageDAO,
			IUserService userService,
			Configuration hibernateConfig,
			int maxTsAnns,
			Cache<String, DataSnapshotEntity> datasetShortCache,
			Cache<Long, SearchResultCacheEntry> searchResultCache) {
		this.dataSnapshotDAO = checkNotNull(dataSnapshotDAO);
		this.logDAO = checkNotNull(logDAO);
		this.discussionDAO = checkNotNull(snapshotDAO);
		this.studyDAO = checkNotNull(studyDAO);
		this.datasetDAO = checkNotNull(datasetDAO);
		this.timeSeriesDAO = checkNotNull(timeSeriesDAO);
		this.tsAnnotationDAO = checkNotNull(tsAnnotationDAO);
		this.analyzedDsDAO = checkNotNull(analyzedEegStudyDAO);
		this.userDAO = checkNotNull(userDAO);
		this.toolDAO = checkNotNull(toolDAO);
		this.jobDAO = checkNotNull(jobDAO);
		this.hasAclDAO = checkNotNull(extAclDAO);
		this.contactDAO = checkNotNull(contactDAO);
		this.imageDAO = checkNotNull(imageDAO);
		this.experimentDAO = checkNotNull(experimentDAO);
		this.sessionDAO = checkNotNull(sessionDAO);
		this.provenanceDAO = provenanceDAO;
		this.patientDAO = patientDAO;
		this.projectDAO = projectDAO;
		this.dataUsageDAO = dataUsageDAO;
		this.registeredAnnotationDAO = registeredAnnotationDAO;
		this.permissionDAO = permissionDAO;
		this.userService = checkNotNull(userService);
		this.datasetShortCache = checkNotNull(datasetShortCache);
		this.searchResultCache = checkNotNull(searchResultCache);
		this.jsonDAO = jsonDAO;
		this.organizationDAO = organizationDAO;
		this.ieegMontageDAO = ieegMontageDAO;

		// should really be passed in on the constructor
		// 20 seemed to work well.
		FLUSH_SIZE = Integer.valueOf(
				hibernateConfig
						.getProperties()
						.getProperty("braintrust.savetsann.flush.size", "20"));
		this.permissionAssembler =
				new ExtPermissionAssembler(permissionDAO);
		MAX_TS_ANNS = maxTsAnns;
	}

	@Override
	public Long addLogEntry(User user, LogMessage log)
			throws AuthorizationException {
		EventLogEntity entry = getLogFromMessage(log);

		logDAO.saveOrUpdate(entry);

		return entry.getEventId();
	}

	@Override
	public Long addPosting(
			final User user,
			final String dataSnapshotRevId,
			final Post post)
			throws AuthorizationException, DataSnapshotNotFoundException {
		final String m = "addPosting(...)";
		final DataSnapshotEntity ds = dataSnapshotDAO
				.findByNaturalId(dataSnapshotRevId);
		if (ds == null) {
			throw new DataSnapshotNotFoundException(dataSnapshotRevId);
		}
		// User should be able to READ the snapshot if he/she wants to post
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		TsAnnotationEntity ann = null;
		if (post.getAnnotationId() != null) {
			ann = tsAnnotationDAO.findTsAnn(ds, post.getAnnotationId());
		}

		UserEntity u = userDAO.getOrCreateUser(user.getUserId());

		SnapshotDiscussion discussion = new SnapshotDiscussion(
				post.getTitle(),
				post.getPost(),
				ds,
				u,
				getDiscussionFor(post.getRefId()),
				ann
				);
		discussion.setDiscussId(post.getDiscId());

		discussionDAO.saveOrUpdate(discussion);
		logger.debug(
				"{}: Added posting to snapshot {} for user {}.",
				new Object[] {
						m,
						ds.getLabel(), user.getUsername() });
		logger.debug("{}: posting {}", m, post);
		return Long.valueOf(discussion.getDiscussId());
	}

	@Override
	public String addProjectPosting(User user, String projectId,
			Post post) throws AuthorizationException {
		final String m = "addProjectPosting(...)";
		final ProjectEntity p = projectDAO.findByNaturalId(projectId);

		if (p == null) {
			throw new ProjectNotFoundException(projectId);
		}

		UserEntity u = userDAO.getOrCreateUser(user.getUserId());

		if (!p.getTeam().contains(u)) {
			throw new AuthorizationException("User " + user.getUsername()
					+ " is not a member of the project");
		}

		ProjectDiscussion discussion = new ProjectDiscussion(
				post.getTitle(),
				post.getPost(),
				p,
				u,
				getProjectDiscussionFor(post.getRefId())
				);

		discussion.setDiscussId(post.getDiscId());

		projectDAO.saveOrUpdateDiscussion(discussion);
		logger.info(
				"{}: Added posting to snapshot {} for user {}.",
				new Object[] {
						m,
						p.getName(), user.getUsername() });
		logger.debug("{}: posting {}", m, post);
		return discussion.getPubId();
	}

	@Override
	public Long addProvenanceEntry(User user, ProvenanceLogEntry log)
			throws AuthorizationException {
		SnapshotUsage entry = getProvenanceRecord(log);
		provenanceDAO.saveOrUpdate(entry);

		return entry.getUsageId();
	}

	public String addTimeSeriesToSnapshot(final User user,
			final String datasetRevId,
			final List<ChannelInfoDto> channelInfos,
			final List<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		final String m = "addTimeSeriesToSnapshot(...)";
		DataSnapshotEntity dsEntity = dataSnapshotDAO
				.findByNaturalId(datasetRevId);

		if (dsEntity == null)
			throw new DatasetNotFoundException(datasetRevId);
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dsEntity.getPubId(), false),
				CorePermDefs.EDIT,
				dsEntity.getPubId());

		Recording rec = null;
		if (HibernateUtil.isInstanceOf(dsEntity, EegStudy.class)) {
			EegStudy ent = studyDAO.load(dsEntity.getId());

			rec = ent.getRecording();
		} else if (HibernateUtil.isInstanceOf(dsEntity, ExperimentEntity.class)) {
			ExperimentEntity ent = experimentDAO.load(dsEntity.getId());

			rec = ent.getRecording();
		}

		// Take each time series and extract out relevant
		int channel = 0;
		for (final String tsRevId : timeSeriesRevIds) {
			final TimeSeriesEntity ts =
					timeSeriesDAO.findByNaturalId(tsRevId);

			final ChannelInfoDto info = channelInfos.get(channel++);
			info.setId(tsRevId);
			if (ts == null) {
				throw new IllegalArgumentException(
						"There is no time series with rev id " + tsRevId);
			}
			final long mefStartTimeUutc = info.getRecordingStartTime();
			final long mefEndTimeUutc = info.getRecordingEndTime();
			final long recStartTimeUutc = rec.getStartTimeUutc();
			final long recEndTimeUutc = rec.getEndTimeUutc();

			// recStartTimeUutc == recEndTimeUutc == 0 indicates
			// that the Recording is new and no real start or end time has been
			// set.
			if ((recStartTimeUutc == 0
					&& recEndTimeUutc == 0)
					|| mefStartTimeUutc < recStartTimeUutc) {
				rec.setStartTimeUutc(mefStartTimeUutc);
			}
			if (mefEndTimeUutc > recEndTimeUutc) {
				rec.setEndTimeUutc(mefEndTimeUutc);
			}
			// If no objects have been added yet, set Recording.dir based on
			// this
			// time series.
			if (rec.getContactGroups().isEmpty() && rec.getObjects().isEmpty()) {
				final int lastSlashIdx = ts.getFileKey().lastIndexOf("/");
				if (lastSlashIdx > 0) {
					final String recDir = ts
							.getFileKey()
							.substring(0,
									lastSlashIdx);
					final String oldRecDir = rec.getDir();
					rec.setDir(recDir);
					logger.info(
							"{}: Changing Recording directory from {} to {}",
							m,
							oldRecDir,
							recDir);
				}
			}

			final String tsPrefix = guessContactGroupPrefix(ts.getLabel());
			int candidates = 0;
			ContactGroup targetGroup = null;
			for (ContactGroup grp : rec.getContactGroups()) {
				final String grpPrefix = grp.getChannelPrefix();
				if (tsPrefix.equals(grpPrefix)
						|| grpPrefix.matches(Pattern.quote(tsPrefix) + "[0-9]+")) {
					candidates++;
					if (grp.getSamplingRate().equals(
							info.getSamplingFrequency())
							&& grp.getLffSetting().equals(
									info.getLowFrequencyFilterSetting())
							&& grp.getHffSetting().equals(
									info.getHighFrequencyFilterSetting())
							&& grp.getNotchFilter().equals(
									Boolean.valueOf(info
											.getNotchFilterFrequency() != 0))) {
						targetGroup = grp;
						break;
					}
				}
			}

			// No contact group, create a new one based on our channel
			if (targetGroup == null) {
				final String grpPrefix = candidates == 0 ? tsPrefix : tsPrefix
						+ candidates;

				targetGroup = new ContactGroup(grpPrefix,
						"defaultElectrode",
						info.getSamplingFrequency(),
						info.getLowFrequencyFilterSetting(),
						info.getHighFrequencyFilterSetting(),
						info.getNotchFilterFrequency() != 0,
						Side.UNKNOWN,
						Location.UNKOWN,
						Double.MAX_VALUE,
						"unknown",
						"unknown");
				targetGroup.setParent(rec);
				rec.getContactGroups().add(targetGroup);
			}

			boolean contactFound = false;
			for (Contact con : targetGroup.getContacts()) {
				if (con.getTrace().getPubId().equals(tsRevId))
					contactFound = true;
			}

			if (!contactFound) {
				Contact con = new Contact();
				con.setContactType(ContactType.OTHER);
				con.setTrace(ts);
				targetGroup.addContact(con);
				logger.debug(
						"{}: Added time series {} to base data snapshot {}.",
						new Object[] { "addTimeSeriesToSnapshot",
								ts.getFileKey(), dsEntity.getLabel() });
			}
		}

		return datasetRevId;
	}

	@Override
	public String addTimeSeriesToDataset(
			final User user,
			final String datasetRevId,
			final Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		final String m = "addTimeSeriesToDataset(...)";

		final Dataset d = datasetDAO.findByNaturalId(datasetRevId);

		if (d == null) {
			throw new DatasetNotFoundException(datasetRevId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						d.getPubId(), false),
				CorePermDefs.EDIT,
				d.getPubId());

		for (final String tsRevId : timeSeriesRevIds) {
			final TimeSeriesEntity ts =
					timeSeriesDAO.findByNaturalId(tsRevId);
			if (ts == null) {
				throw new IllegalArgumentException(
						"There is no time series with rev id " + tsRevId);
			}
			d.getTimeSeries().add(ts);

			logger.debug(
					"{}: Added time series {} to dataset {}.",
					new Object[] { m, ts.getFileKey(), d.getLabel() });
		}
		logger.info("{}: Added {} time series to dataset {}.", new Object[] {
				m, timeSeriesRevIds.size(), d.getLabel() });
		return datasetRevId;
	}

	@Override
	public int cleanupOldSessions(Date minLastActive) {
		final String m = "cleanupOldSessions(...)";
		final int deleted = sessionDAO.deleteSessionsOlderThan(minLastActive);
		logger.info("{}: Deleted {} old sessions", m, deleted);
		return deleted;
	}

	public List<Post> convertDiscussionToPostings(
			List<SnapshotDiscussion> discussions) {
		List<Post> ret = new ArrayList<Post>();

		// serialize snapshotdiscussion into snapshotposting
		for (SnapshotDiscussion disc : discussions) {
			Post p = new Post(
					disc.getUser().getUsername(),
					disc.getTitle(),
					disc.getPosting(),
					disc.getSnapshot().getPubId(),
					disc.getPubId(),
					(disc.getRefersTo() == null) ? null : disc.getRefersTo()
							.getPubId(),
					(disc.getAnnotation() == null) ? null : disc
							.getAnnotation().getPubId());
			p.setDiscId(disc.getDiscussId());

			ret.add(p);
		}
		return ret;
	}

	@Override
	public void createDataUsage(DataUsageEntity dataUsage) {
		UserEntity user = dataUsage.getUser();
		userDAO.saveOrUpdate(user);
		dataUsageDAO.saveOrUpdate(dataUsage);
	}

	@Override
	public void createJobForUser(UserId userId, String datasetId,
			String toolId,
			JobStatus status, ParallelDto parallelism, Date createTime) {
		final String m = "createJobForUser(...)";
		final JobEntity job = new JobEntity(userId.getValue(),
				datasetId, toolId,
				status, parallelism, createTime, null);
		jobDAO.saveOrUpdate(job);
		logger.info(
				"{}: Created job with tool {} and dataset {} for user {}.",
				new Object[] {
						m,
						toolId,
						datasetId,
						userId });

	}

	@Override
	public String createProject(User user, EegProject project)
			throws AuthorizationException {
		ProjectEntity proj = ProjectMapper.convertToProject(
				userDAO,
				dataSnapshotDAO,
				toolDAO,
				project);
		projectDAO.saveOrUpdate(proj);

		return proj.getPubId();
	}

	@Override
	public SessionToken createSession(User user) {
		final String m = "createSession(...)";
		final UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());
		final SessionEntity sessionEntity = new SessionEntity(userEntity);
		sessionDAO.saveOrUpdate(sessionEntity);
		logger.info("{}: Returning new session token for user {}", m,
				user.getUsername());
		return new SessionToken(sessionEntity.getToken());
	}

	@Override
	public void createUserIfNecessary(User user) {
		userDAO.getOrCreateUser(user.getUserId());
	}

	@Override
	public String deriveDataset(
			final User user,
			final String sourceDataSnapshotId,
			final String derivedDsName,
			final String toolName,
			@Nullable final Set<String> tsIdInclusions,
			@Nullable final Set<String> tsAnnIdInclusions)
			throws DuplicateNameException {
		final String m = "deriveDataset(...)";
		final DataSnapshotEntity sourceDataSnapshot =
				dataSnapshotDAO
						.findByNaturalId(sourceDataSnapshotId);

		if (sourceDataSnapshot == null) {
			throw new IllegalArgumentException(
					"there is no source data snapshot with the rev id ["
							+ sourceDataSnapshotId + "]");
		}

		final UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						sourceDataSnapshot.getPubId(), false),
				CorePermDefs.READ,
				sourceDataSnapshot.getPubId());

		DataSnapshotEntity dupLabel =
				dataSnapshotDAO.findByLabel(derivedDsName);

		if (dupLabel != null) {
			throw new DuplicateNameException(derivedDsName);
		}

		final Dataset derivedDataset = new Dataset(
				derivedDsName,
				userEntity,
				SecurityUtil.createUserOwnedAcl(
						userEntity,
						permissionDAO));

		datasetDAO.saveOrUpdate(derivedDataset);

		final Set<String> actualTsRevIds = newHashSet();
		final Set<String> actualTsAnnRevIds = newHashSet();
		for (final TimeSeriesEntity timeSeries : sourceDataSnapshot
				.getTimeSeries()) {
			actualTsRevIds.add(timeSeries.getPubId());
			if (tsIdInclusions == null
					|| contains(tsIdInclusions, timeSeries.getPubId())) {
				derivedDataset.getTimeSeries().add(timeSeries);
			}
		}

		// have to flush so that the insert methods see what we've already
		// inserted
		datasetDAO.flush();

		if (tsAnnIdInclusions == null) {
			tsAnnotationDAO.insertAll(sourceDataSnapshot, derivedDataset);
		} else {
			for (String srcTsAnnRevId : tsAnnIdInclusions) {
				if (tsAnnotationDAO.insertWithRevId(
						srcTsAnnRevId,
						sourceDataSnapshot,
						derivedDataset) > 0) {
					actualTsAnnRevIds.add(srcTsAnnRevId);
				}
			}
		}

		boolean wrongTsRevIdInclusions = false;
		if (tsIdInclusions != null
				&& !all(tsIdInclusions, in(actualTsRevIds))) {
			wrongTsRevIdInclusions = true;
		}
		boolean wrongTsAnnRevIdInclusions = false;
		if (tsAnnIdInclusions != null
				&& !all(tsAnnIdInclusions, in(actualTsAnnRevIds))) {
			wrongTsAnnRevIdInclusions = true;
		}
		if (wrongTsRevIdInclusions || wrongTsAnnRevIdInclusions) {
			String errMsg = "";
			if (wrongTsRevIdInclusions) {
				errMsg = "tsRevIdInclusions was not a subset of the time series in the source data snapshot, ";
			}
			if (wrongTsAnnRevIdInclusions) {
				errMsg += "tsAnnRevIdInclusions wat not a subset of the annotations of the included time series";
			}
			throw new IllegalArgumentException(errMsg);
		}

		final AnalyzedDataSnapshot analyzedDataSnapshot = new AnalyzedDataSnapshot(
				toolName, derivedDataset, sourceDataSnapshot);
		analyzedDsDAO.saveOrUpdate(analyzedDataSnapshot);
		logger.info("{}: Derived dataset {} for user {} from {}.",
				new Object[] { m, derivedDataset.getLabel(),
						user.getUsername(), sourceDataSnapshot.getLabel() });
		return derivedDataset.getPubId();
	}

	@Override
	public List<EditAclResponse> editAcl(
			User user,
			HasAclType entityType,
			String mode,
			List<IEditAclAction<?>> actions) throws AuthorizationException {
		if (actions.isEmpty()) {
			return Collections.emptyList();
		}
		final Set<String> targetIds = newHashSet();
		for (IEditAclAction<?> action : actions) {
			targetIds.add(action.getAce().getTargetId());
		}
		checkArgument(targetIds.size() == 1,
				"All edit actions should have the same targetId: " + targetIds);
		final String targetId = getOnlyElement(targetIds);
		IHasExtAcl target = hasAclDAO.findByNaturalId(
				entityType,
				targetId);
		if (target == null) {
			throw new AclTargetNotFoundException(
					targetId,
					entityType.getEntityName());
		}
		final ExtAclEntity acl = target.getExtAcl();
		final ExtPermissions perms = getPermissionsHelper(
				user,
				entityType.getEntityName(),
				CorePermDefs.CORE_MODE_NAME,
				target);
		extAuthzHandler.checkPermitted(
				perms,
				CorePermDefs.EDIT_ACL,
				target.getPubId());
		final AclEditor aclEditor = new AclEditor(
				acl,
				mode,
				userDAO,
				projectDAO,
				permissionDAO);
		final List<EditAclResponse> results = aclEditor.editAcl(user, actions);
		return results;
	}

	private void flushBatch(int i, Set<TsAnnotationEntity> toBeEvicteds) {
		if (i % FLUSH_SIZE == 0) {
			flushBatch(toBeEvicteds);
		}
	}

	private void flushBatch(
			final Set<TsAnnotationEntity> toBeEvicteds) {
		tsAnnotationDAO.flush();
		for (final TsAnnotationEntity toBeEvicted : toBeEvicteds) {
			tsAnnotationDAO.evict(toBeEvicted);
		}
		toBeEvicteds.clear();
	}

	@Override
	public GetAcesResponse getAcesResponse(
			User user,
			HasAclType entityType,
			String mode,
			String targetId) {

		IHasExtAcl target = hasAclDAO.findByNaturalId(
				entityType,
				targetId);
		if (target == null) {
			throw new AclTargetNotFoundException(
					targetId,
					entityType.getEntityName());
		}
		final ExtPermissions permsForAuthz = getPermissionsHelper(
				user,
				entityType.getEntityName(),
				mode,
				target);
		extAuthzHandler.checkPermitted(permsForAuthz, CorePermDefs.READ,
				target.getPubId());

		final boolean canEditAcl = extAuthzHandler.isPermitted(
				permsForAuthz,
				CorePermDefs.EDIT_ACL,
				target.getPubId());

		final ExtAclEntity acl = target.getExtAcl();

		// Build User ace list.
		final List<ExtUserAceEntity> userAceEntities =
				newArrayList(acl.getUserAces());

		final List<ExtUserAce> userAces = newArrayList();
		for (final ExtUserAceEntity userAceEntity : userAceEntities) {
			final Collection<PermissionEntity> perms = filter(
					userAceEntity.getPerms(),
					compose(
							equalTo(mode),
							PermissionEntity.getModeName));
			if (!perms.isEmpty()) {
				final UserId aceUserId = userAceEntity.getUser().getId();
				final User aceUser = userService.findUserByUid(aceUserId);
				final ExtUserAce userAce = new ExtUserAce(
						aceUserId,
						aceUser.getUsername(),
						targetId,
						acl.getVersion(),
						userAceEntity.getId(),
						userAceEntity.getVersion());
				userAce.getPerms().addAll(
						permissionAssembler.toDtos(perms));
				userAces.add(userAce);
			}
		}
		Collections.sort(userAces, new Comparator<ExtUserAce>() {

			@Override
			public int compare(ExtUserAce arg0, ExtUserAce arg1) {
				return arg0.getName().compareTo(arg1.getName());
			}
		});

		// Build Project Ace list
		final List<ProjectAceEntity> projectAceEntities =
				newArrayList(acl.getProjectAces());

		final List<ExtProjectAce> projectAces = newArrayList();
		for (final ProjectAceEntity projectAceEntity : projectAceEntities) {
			final Collection<PermissionEntity> perms = filter(
					projectAceEntity.getPerms(),
					compose(
							equalTo(mode),
							PermissionEntity.getModeName));
			if (!perms.isEmpty()) {
				final ExtProjectAce userAce =
						new ExtProjectAce(
								projectAceEntity.getProject().getPubId(),
								projectAceEntity.getProject().getName(),
								projectAceEntity.getProjectGroup(),
								targetId,
								acl.getVersion(),
								projectAceEntity.getId(),
								projectAceEntity.getVersion());
				userAce.getPerms().addAll(
						permissionAssembler.toDtos(perms));
				projectAces.add(userAce);
			}
		}
		Collections.sort(projectAces, new Comparator<ExtProjectAce>() {

			@Override
			public int compare(ExtProjectAce arg0, ExtProjectAce arg1) {
				return ComparisonChain
						.start()
						.compare(
								arg0.getProjectGroup().getProjectName(),
								arg1.getProjectGroup().getProjectName())
						.compare(
								arg0.getProjectGroup().getType().name(),
								arg1.getProjectGroup().getType().name())
						.result();
			}
		});

		final Collection<PermissionEntity> worldPerms = filter(
				acl.getWorldAce(),
				compose(equalTo(mode),
						PermissionEntity.getModeName));
		final ExtWorldAce worldAce = new ExtWorldAce(
				targetId,
				acl.getId(),
				acl.getVersion());
		worldAce.getPerms().addAll(permissionAssembler.toDtos(worldPerms));

		// Use ImmutableBiMap to ensure display order is preserved.
		Builder<String, ExtPermission> userPermMapBuilder = ImmutableBiMap
				.builder();
		Builder<String, ExtPermission> projectGorupPermMapBuilder = ImmutableBiMap
				.builder();
		Builder<String, Optional<ExtPermission>> worldPermMapBuilder = ImmutableBiMap
				.builder();
		worldPermMapBuilder.put("None", Optional.<ExtPermission> absent());

		final List<PermissionEntity> allPermsForDomainAndMode = permissionDAO
				.findByDomainAndMode(
						entityType,
						mode);

		for (final PermissionEntity permEntity : allPermsForDomainAndMode) {
			final ExtPermission permDto = permissionAssembler.toDto(permEntity);
			if (permEntity.isUserPerm()) {
				userPermMapBuilder.put(
						permEntity.getDisplayString(),
						permDto);
			}
			if (permEntity.isProjectGroupPerm()) {
				projectGorupPermMapBuilder.put(
						permEntity.getDisplayString(),
						permDto);
			}
			if (permEntity.isWorldPerm()) {
				worldPermMapBuilder.put(
						permEntity.getDisplayString(),
						Optional.of(permDto));
			}
		}
		return new GetAcesResponse(
				canEditAcl,
				worldAce,
				projectAces,
				userAces,
				worldPermMapBuilder.build(),
				projectGorupPermMapBuilder.build(),
				userPermMapBuilder.build());
	}

	@Override
	public List<AnnotationDefinition> getAnnotationDefinitions(User user,
			String viewerType) {
		final List<RegisteredAnnotation> annotations =
				registeredAnnotationDAO.getRegisteredAnnotations(viewerType);

		final List<AnnotationDefinition> retAnnotations = Lists.newArrayList();
		for (RegisteredAnnotation reg : annotations) {
			retAnnotations.add(AnnotationDefinitionAssembler.assemble(reg));
		}

		logger.info("{}: Returning {} annotation definitions", new Object[] {
				"GetAnnotationDefinitions",
				Integer.valueOf(annotations.size())
		});
		return retAnnotations;
	}

	@Override
	public Integer getAnnotationsByUser(User user)
			throws AuthorizationException {
		final String m = "getAnnotationsByUser(...)";

		logger.info(
				"{}: Returning annotation count by user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		return Ints.checkedCast(
				tsAnnotationDAO.findContributedAnnotationCount(userEntity));
	}

	@Override
	public String getClob(User user, String dataSnapshotId)
			throws AuthorizationException {
		final String m = "getClob(...)";

		final DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dataSnapshotId);

		if (ds == null) {
			throw new DataSnapshotNotFoundException(dataSnapshotId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		logger.info(
				"{}: Returning clob from snapshot {} {}  for user {}.",
				new Object[] { m, ds.getLabel(), ds.getPubId(),
						user.getUsername() });

		return ds.getClob();
	}

	@Override
	public Set<IJsonKeyValue> getJsonKeyValues(User user, String dataSnapshotId)
			throws AuthorizationException, IOException {
		final String m = "getJsonKeyValues(...)";

		final Set<IJsonKeyValue> values = jsonDAO
				.getAllKeyValues(dataSnapshotId);

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dataSnapshotId, false),
				CorePermDefs.READ,
				dataSnapshotId);

		logger.info(
				"{}: Returning JSON from snapshot {}  for user {}.",
				new Object[] { m, dataSnapshotId, user.getUsername() });

		return values;
	}

	@Override
	@Nonnull
	public DataSnapshot getDataSnapshot(
			final User user,
			final String dataSnapshotRevId) throws AuthorizationException {
		final String m = "getDataSnapshot(...)";
		final DataSnapshotEntity dsEntity = dataSnapshotDAO
				.findByNaturalId(dataSnapshotRevId);
		if (dsEntity == null) {
			throw new DataSnapshotNotFoundException(dataSnapshotRevId);
		}
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dsEntity.getPubId(), false),
				CorePermDefs.READ,
				dsEntity.getPubId());
		
		final Optional<Recording> recording = getRecording(dsEntity);
		

		final DataSnapshot ds = DataSnapshotAssembler.assembleWChildren(
				dsEntity,
				recording,
				tsAnnotationDAO);

		logger.info(
				"{}: Returning data snapshot {} to user {}.",
				new Object[] {
						m,
						ds.getLabel(),
						user.getUsername() });
		return ds;
	}

	@SuppressWarnings("all")
	public DataSnapshot getDataSnapshotByLabel(
			final User user,
			final String label) throws AuthorizationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshots(
			final User user,
			final EegStudySearch studySearch) {
		final long inTime = new Date().getTime();
		final String m = "getDataSnapshots(...)";
		checkNotNull(studySearch);
		logger.debug("{}: dsSearch: {}", m, studySearch);

		// todo: this gets called by users before they are logged in, and
		// results in an NPE - how?
		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		final Set<EegStudy> studies =
				dataSnapshotDAO.findBySearch(studySearch, userEntity, user);
		final Set<DataSnapshotSearchResult> searchResults = newHashSet();
		for (EegStudy entry : studies) {
			// authz done in query

			SearchResultCacheEntry searchResultCacheEntry = null;

			searchResultCacheEntry =
					searchResultCache.getIfPresent(entry.getId());

			Set<TimeSeriesEntity> ts = newHashSet();

			if (searchResultCacheEntry == null) {

				int imgCnt = Ints.checkedCast(
						imageDAO.countImagesRecording(entry.getRecording()));

				int tsCount =
						timeSeriesDAO.countByRecording(
								entry.getRecording());
				int tsAnnCnt = Ints.checkedCast(dataSnapshotDAO
						.countTsAnns(entry));
				searchResultCacheEntry =
						new SearchResultCacheEntry(
								imgCnt,
								tsCount,
								tsAnnCnt,
								entry.getOrganization().getName());
				searchResultCache.put(
						entry.getId(),
						searchResultCacheEntry);

			}
			for (long i = 0; i < searchResultCacheEntry.getTsCount(); i++) {
				TimeSeriesEntity tsEntity = new TimeSeriesEntity();
				tsEntity.setPubId("dummy-pubId-" + i);
				tsEntity.setLabel("dummy-label-" + i);
				ts.add(tsEntity);
			}
			DataSnapshotSearchResult dsSearchResult =
					DsSearchResultAssembler.assembleDsSearchResult(
							entry,
							ts,
							searchResultCacheEntry.getImageCount(),
							searchResultCacheEntry.getTsAnnCount(),
							entry.getRecording().getStartTimeUutc(),
							entry.getRecording().getEndTimeUutc(),
							searchResultCacheEntry.getOrganizationName());
			searchResults
					.add(dsSearchResult);

		}
		logger.info(
				"{}: Returning {} data snapshot search results to user {}. {} seconds.",
				new Object[] {
						m,
						searchResults.size(),
						user.getUsername(),
						(new Date().getTime() - inTime) / 1000.0 });
		return searchResults;
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshotsNoCounts(
			final User user,
			final EegStudySearch studySearch) {
		final long inTime = new Date().getTime();
		final String m = "getDataSnapshots(...)";
		checkNotNull(studySearch);
		logger.debug("{}: dsSearch: {}", m, studySearch);

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		final Set<EegStudy> studies =
				dataSnapshotDAO.findBySearch(studySearch, userEntity, user);
		final Set<DataSnapshotSearchResult> searchResults = newHashSet();
		for (EegStudy entry : studies) {
			// authz done in query

			SearchResultCacheEntry searchResultCacheEntry = null;

			searchResultCacheEntry =
					searchResultCache.getIfPresent(entry.getId());

			Set<TimeSeriesEntity> ts = newHashSet();

			if (searchResultCacheEntry == null) {

				int imgCnt = -1;

				int tsCount = -1;
				int tsAnnCnt = -1;

				searchResultCacheEntry =
						new SearchResultCacheEntry(
								imgCnt,
								tsCount,
								tsAnnCnt,
								entry.getOrganization().getName());
				searchResultCache.put(
						entry.getId(),
						searchResultCacheEntry);

			}
			// for (long i = 0; i < searchResultCacheEntry.getTsCount(); i++) {
			// TimeSeriesEntity tsEntity = new TimeSeriesEntity();
			// tsEntity.setPubId("dummy-pubId-" + i);
			// tsEntity.setLabel("dummy-label-" + i);
			// ts.add(tsEntity);
			// }
			DataSnapshotSearchResult dsSearchResult =
					DsSearchResultAssembler.assembleDsSearchResult(
							entry,
							ts,
							searchResultCacheEntry.getImageCount(),
							searchResultCacheEntry.getTsAnnCount(),
							entry.getRecording().getStartTimeUutc(),
							entry.getRecording().getEndTimeUutc(),
							searchResultCacheEntry.getOrganizationName());
			searchResults
					.add(dsSearchResult);

		}
		logger.info(
				"{}: Returning {} data snapshot search results to user {}. {} seconds.",
				new Object[] {
						m,
						searchResults.size(),
						user.getUsername(),
						(new Date().getTime() - inTime) / 1000.0 });
		return searchResults;
	}

	@Override
	public Set<DataSnapshotSearchResult> getDataSnapshotsForUser(
			final User user) {
		final long inTime = new Date().getTime();
		final String m = "getDataSnapshotsForUser(...)";
		logger.debug("{}", m);

		// First get all datasets

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		final Set<Dataset> studies =
				dataSnapshotDAO.findForUser(userEntity, user);

		final Set<DataSnapshotSearchResult> searchResults = newHashSet();

		Set<String> ids = new HashSet<String>();
		for (Dataset ds : studies)
			ids.add(ds.getPubId());

		searchResults.addAll(getLatestSnapshots(user, ids));

		// /////////
		for (DataSnapshotSearchResult ds : this.getDataSnapshots(user,
				new EegStudySearch())) {
			ExtPermissions permissions =
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getId(), false);
			if (permissions.getUserAce().isPresent() && permissions
					.getUserAce()
					.get()
					.getPerms()
					.contains(CorePermDefs.OWNER_PERM)) {
				searchResults.add(ds);
			}
		}

		for (DataSnapshotSearchResult ds : this.getDataSnapshots(user,
				new EegStudySearch())) {
			ExtPermissions permissions =
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							ds.getId(), false);
			if (permissions.getUserAce().isPresent() && permissions
					.getUserAce()
					.get()
					.getPerms()
					.contains(CorePermDefs.OWNER_PERM)) {
				searchResults.add(ds);
			}
		}
		return searchResults;
	}

	private SnapshotDiscussion getDiscussionFor(@Nullable String discussionPubId) {
		if (discussionPubId == null) {
			return null;
		} else {
			return discussionDAO.findByNaturalId(discussionPubId);
		}
	}

	@Override
	public EegStudyMetadata getEegStudyMetadata(User user, String studyRevId)
			throws AuthorizationException {
		// final String M = "getEegStudyMetadata(...)";
		final EegStudy study = studyDAO.findByNaturalId(studyRevId);
		if (study == null) {
			// throw new EegStudyNotFoundException(studyRevId);
			return null;
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						study.getPubId(), false),
				CorePermDefs.READ,
				study.getPubId());

		HospitalAdmission admission = study.getParent();
		Patient patient = admission.getParent();
		Recording recording = study.getRecording();
		Set<ContactGroupMetadata> electrodeMds = Assembler
				.contactGroupEntities2Metadatas(recording.getContactGroups());

		Set<EpilepsySubtypeMetadata> epSubtypeMds = Assembler
				.epilepsySubtypeEntities2Metadatas(study.getEpilepsySubtypes());

		EegStudyMetadata studyMd = new EegStudyMetadata(
				patient.getLabel(),
				patient.getAgeOfOnset(),
				patient.getGender(),
				patient.getHandedness(),
				patient.getEthnicity(),
				patient.getEtiology(),
				study.getRecording().getImages().size(),
				patient.getDevelopmentalDisorders(),
				patient.getTraumaticBrainInjury(),
				patient.getFamilyHistory(),
				patient.getSzTypes(),
				patient.getPrecipitants(),
				admission.getAgeAtAdmission(),
				study.getLabel(),
				study.getRecording().getRefElectrodeDescription(),
				electrodeMds,
				epSubtypeMds);
		return studyMd;
	}

	@Override
	public ExperimentMetadata getExperimentMetadata(
			User user,
			String experimentId) throws AuthorizationException {
		ExperimentEntity experiment = experimentDAO
				.findByNaturalId(experimentId);

		if (experiment == null)
			return null;

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						experiment.getPubId(), false),
				CorePermDefs.READ,
				experiment.getPubId());
		return Assembler.entity2Metadata(experiment);
	}

	@Override
	public Set<Image> getImages(
			User user,
			String dsId)
			throws AuthorizationException {
		// final String M = "getImages(...)";
		DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dsId);
		if (ds == null) {
			throw new DataSnapshotNotFoundException(dsId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		Set<Image> images = newHashSet();

		for (ImageEntity iEntity : ds.getImages()) {
			images.add(ImageAssembler.assemble(iEntity));
		}
		return images;
	}

	@Override
	public List<DataSnapshotSearchResult> getLatestSnapshots(
			final User user,
			final Iterable<String> dsIds) throws AuthorizationException {
		final String m = "getLatestSnapshots(...)";
		List<DataSnapshotSearchResult> results = newArrayList();
		for (String dsId : dsIds) {

			DataSnapshotEntity dataSnapshot =
					dataSnapshotDAO.findByNaturalId(dsId);

			if (dataSnapshot == null) {
				results.add(null);
				continue;
			}

			extAuthzHandler.checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dataSnapshot.getPubId(), false),
					CorePermDefs.READ,
					dataSnapshot.getPubId());

			long imgCnt = dataSnapshot.getImages().size();

			final long tsAnnCnt = dataSnapshotDAO.countTsAnns(dataSnapshot);

			DataSnapshotSearchResult dsSearchResult = DsSearchResultAssembler
					.assembleDsSearchResult(
							dataSnapshot,
							dataSnapshot.getTimeSeries(),
							imgCnt,
							tsAnnCnt);
			results.add(dsSearchResult);

		}
		logger.debug(
				"{}: For ds rev id's {} returning ds search results {} to user {}.",
				new Object[] {
						m,
						dsIds,
						results,
						user.getUsername() });
		logger.info(
				"{}: returned {} ds search results (some possibly null) to user {}.",
				new Object[] {
						m,
						results.size(),
						user.getUsername() });
		return results;
	}

	@Override
	public List<LogMessage> getLogEntries(User user, int startIndex, int count)
			throws AuthorizationException {
		final String m = "getLogEntries(...)";

		logger.info(
				"{}: Returning log entries for user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		List<EventLogEntity> logs =
				logDAO.find(userEntity, startIndex, count);

		List<LogMessage> ret = LoggingMapper.convertLogEntriesToMessages(logs);

		logger.debug("{}: retrieving {} log messages", m, ret.size());
		return ret;
	}

	@Override
	public List<LogMessage> getLogEntriesSince(User user, Timestamp sinceWhen)
			throws AuthorizationException {
		final String m = "getLogEntriesSince(...)";

		// Using debug instead of info to prevent filling of logs.
		logger.debug(
				"{}: Returning log entries for user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		List<EventLogEntity> logs =
				logDAO.findSince(userEntity, sinceWhen);

		List<LogMessage> ret = LoggingMapper.convertLogEntriesToMessages(logs);

		logger.debug("{}: retrieving {} log messages", m, ret.size());
		return ret;
	}

	private EventLogEntity getLogFromMessage(LogMessage log) {
		UserEntity initiator =
				userDAO.getOrCreateUser(log.getSender());
		UserEntity recipient =
				userDAO.getOrCreateUser(log.getRecipient());

		return new EventLogEntity(recipient, initiator, log.getVisibility(),
				log.getMessage(),
				log.getTime());
	}

	@Override
	public List<String> getMEFPaths(
			User user,
			String dataSnapshotId,
			Iterable<String> timeSeriesIds)
			throws AuthorizationException {
		String m = "getMEFPaths(User, String, Iterable)";
		DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dataSnapshotId);

		if (ds == null) {
			throw new DataSnapshotNotFoundException(dataSnapshotId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		Set<TimeSeriesEntity> tss = ds.getTimeSeries();

		List<String> mefPaths = newArrayList();

		logger.debug("{}: mefPaths: ", m, mefPaths);

		for (String timeSeriesId : timeSeriesIds) {
			TimeSeriesEntity ts = timeSeriesDAO.findByNaturalId(timeSeriesId);
			if (ts == null || !tss.contains(ts)) {
				throw new IllegalArgumentException("unknown time series ["
						+ timeSeriesId + "] in snapshot [" + ds.getLabel()
						+ "] ["
						+ ds.getPubId() + "]");
			}
			mefPaths.add(ts.getFileKey());
		}

		return mefPaths;
	}

	@Override
	public ExtPermissions getPermissions(
			User user,
			HasAclType entityType,
			String mode,
			String targetId,
			boolean useDatasetShortCache) {
		final String m = "getPermissions(...)";
		IHasExtAcl target = null;
		if (useDatasetShortCache && entityType == HasAclType.DATA_SNAPSHOT) {
			target = DataSnapshotUtil
					.getFromCache(targetId, dataSnapshotDAO, datasetShortCache);
		} else {
			target =
					hasAclDAO.findByNaturalId(entityType, targetId);
		}
		if (target == null) {
			throw new AclTargetNotFoundException(targetId,
					entityType.getEntityName());
		}
		final String entityTypeName = entityType.getEntityName();
		final ExtPermissions corePerms = getPermissionsHelper(
				user,
				entityTypeName,
				CorePermDefs.CORE_MODE_NAME,
				target);
		if (extAuthzHandler.isPermitted(
				corePerms,
				CorePermDefs.READ,
				target.getPubId())) {

		} else {
			return new ExtPermissions(
					user,
					target.getLabel(),
					targetId,
					null,
					null);
		}

		ExtPermissions requestedPerms = null;
		if (mode.equals(CorePermDefs.CORE_MODE_NAME)) {
			requestedPerms = corePerms;
		} else {
			requestedPerms = getPermissionsHelper(
					user,
					entityTypeName,
					mode,
					target);
		}
		logger.debug("{}: Returning permissions for user {} on {}",
				new Object[] {
						m,
						user.getUsername(),
						target.getLabel()
				});
		return requestedPerms;
	}

	private ExtPermissions getPermissionsHelper(
			User user,
			String entity,
			String mode,
			IHasExtAcl target) {
		String targetName = target.getLabel();
		String targetId = target.getPubId();

		ExtAclEntity acl = target.getExtAcl();

		Optional<ExtUserAceEntity> userAceOpt =
				tryFind(acl.getUserAces(),
						compose(
								equalTo(user.getUserId()),
								ExtUserAceEntity.getUserId));
		Optional<ExtUserAce> userAceDtoOpt = Optional.absent();

		if (userAceOpt.isPresent()) {
			ExtUserAceEntity userAce = userAceOpt.get();
			UserEntity userEnt = userAce.getUser();

			userAceDtoOpt = Optional.of(new ExtUserAce(
					userEnt.getId(),
					user.getUsername(),
					targetId,
					acl.getVersion(),
					userAce.getId(),
					userAce.getVersion()));

			// if it's the right domain or domain is the wild card
			// and it's the mode I asked for then I want it
			for (PermissionEntity perm : userAce.getPerms()) {
				if (isRelevant(perm, entity, mode)) {
					userAceDtoOpt.get().getPerms().add(
							new ExtPermission(
									perm.getMode().getDomain().getName(),
									mode,
									perm.getName()));
				}
			}
		}

		Optional<ExtWorldAce> worldAceOpt = Optional.absent();

		for (PermissionEntity worldPerm : acl.getWorldAce()) {
			if (isRelevant(worldPerm, entity, mode)) {
				if (!worldAceOpt.isPresent()) {
					worldAceOpt = Optional.of(
							new ExtWorldAce(
									targetId,
									acl.getId(),
									acl.getVersion()));
				}
				worldAceOpt.get().getPerms().add(
						new ExtPermission(
								worldPerm.getMode().getDomain().getName(),
								mode,
								worldPerm.getName()));
			}
		}

		ExtPermissions perms = new ExtPermissions(
				user,
				targetName,
				targetId,
				userAceDtoOpt.orNull(),
				worldAceOpt.orNull());

		for (final ProjectAceEntity projAce : acl.getProjectAces()) {
			final ProjectGroupType projGroup = projAce.getProjectGroup();
			Set<UserEntity> projUsers = null;
			switch (projGroup) {
				case TEAM:
					projUsers = projAce.getProject().getTeam();
					break;
				case ADMINS:
					projUsers = projAce.getProject().getAdmins();
					break;
				default:
					throw new IllegalStateException(
							"Unknown project group type: " + projGroup);
			}

			final Optional<UserEntity> userEntity =
					tryFind(projUsers,
							compose(equalTo(user.getUserId()),
									UserEntity.getId));
			if (userEntity.isPresent()) {
				// If a permission has our mode and domain, or our mode and the
				// wildcard domain, then it is relevant.
				final Collection<PermissionEntity> relevantPerms =
						filter(
								projAce.getPerms(),
								and(
										compose(equalTo(mode),
												PermissionEntity.getModeName),
										or(compose(equalTo(entity),
												PermissionEntity.getDomainName),
												compose(equalTo(PermissionDomainEntity.PERM_DOMAIN_WILDCARD),
														PermissionEntity.getDomainName))));
				if (!relevantPerms.isEmpty()) {
					ExtProjectAce projAceDto = new ExtProjectAce(
							projAce.getProject().getPubId(),
							projAce.getProject().getName(),
							projGroup,
							targetId,
							acl.getVersion(),
							projAce.getId(), projAce.getVersion());
					projAceDto.getPerms().addAll(
							permissionAssembler.toDtos(relevantPerms));
					perms.getProjectAces().add(projAceDto);
				}
			}
		}
		return perms;
	}

	@Override
	public List<Post> getPostings(
			User user,
			String dsRevId,
			int postIndex,
			int numPosts) throws AuthorizationException {
		final String m = "getPostings(...)";

		final DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dsRevId);

		if (ds == null) {
			throw new DataSnapshotNotFoundException(dsRevId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		logger.info(
				"{}: Returning postings for snapshot {} {}  for user {}.",
				new Object[] { m, ds.getLabel(), ds.getPubId(),
						user.getUsername() });

		List<SnapshotDiscussion> discussions =
				discussionDAO.findBySnapshotID(ds, postIndex, numPosts);
		// ds.getDiscussions();

		List<Post> ret = convertDiscussionToPostings(discussions);

		logger.debug("{}: retrieving {} posts", m, ret.size());
		return ret;
	}

	@Override
	public EegProject getProject(User user, String id)
			throws AuthorizationException {
		final String m = "getProject(...)";

		logger.info(
				"{}: Returning project owned by user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		ProjectEntity p = projectDAO.findByNaturalId(id);

		if (!p.getTeam().contains(userEntity)) {
			throw new AuthorizationException("User " + user.getUsername()
					+ " is not part of project team");
		}

		return ProjectMapper.convertProjectToEeg(p);
	}

	private ProjectDiscussion getProjectDiscussionFor(String idId) {
		if (idId == null) {
			return null;
		} else {
			return projectDAO.getDiscussionByItsID(idId);
		}
	}

	@Override
	public List<Post> getProjectDiscussions(User user,
			String projectId, int postIndex, int numPosts)
			throws AuthorizationException {
		final String m = "getProjectDiscussions(...)";

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		final ProjectEntity p =
				projectDAO.findByNaturalId(projectId);

		if (p == null) {
			throw new ProjectNotFoundException(projectId);
		}
		if (!p.getTeam().contains(userEntity)
				&& !p.getAdmins().contains(userEntity)) {
			throw new AuthorizationException("User " + user.getUsername()
					+ " is not a member of the project");
		}

		logger.info(
				"{}: Returning postings for project {} {}  for user {}.",
				new Object[] { m, p.getName(), p.getPubId(),
						user.getUsername() });

		List<ProjectDiscussion> discussions =
				projectDAO.findDiscussionByProjectID(p, postIndex, numPosts);

		List<Post> ret = ProjectMapper.convertProjectDiscussionToPostings(
				discussions, userService);

		logger.debug("{}: retrieving {} posts", m, ret.size());
		return ret;
	}

	@Override
	public Set<EegProject> getProjectsForUser(User user)
			throws AuthorizationException {

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		Set<EegProject> projects = projectDAO
				.getEegProjectsForUser(userEntity);

		return projects;
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesByUser(User user)
			throws AuthorizationException {
		final String m = "getProvenanceEntriesByUser(...)";

		logger.info(
				"{}: Returning provenance entries by user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		List<SnapshotUsage> logs =
				provenanceDAO.findCreatedByUser(userEntity);

		List<ProvenanceLogEntry> ret = ProvenanceMapper
				.convertUsageToProvenance(logs, userService);

		logger.debug("{}: retrieving {} provenance messages", m,
				ret.size());
		return ret;
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesForSnapshot(User user,
			String dataSnapshotRevId) throws AuthorizationException {
		final String m = "getProvenanceEntriesForUser(...)";

		logger.info(
				"{}: Returning provenance entries for snapshot {} and user {}.",
				new Object[] { m,
						dataSnapshotRevId, user.getUsername() });

		final DataSnapshotEntity ds = dataSnapshotDAO
				.findByNaturalId(dataSnapshotRevId);
		if (ds == null) {
			throw new DataSnapshotNotFoundException(dataSnapshotRevId);
		}
		// User should be able to READ the snapshot if he/she wants to post
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		List<SnapshotUsage> logs =
				provenanceDAO.findAllProps(ds);

		List<ProvenanceLogEntry> ret = ProvenanceMapper
				.convertUsageToProvenance(logs, userService);

		logger.debug("{}: retrieving {} provenance messages", m,
				ret.size());
		return ret;
	}

	@Override
	public List<ProvenanceLogEntry> getProvenanceEntriesForUser(User user)
			throws AuthorizationException {
		final String m = "getProvenanceEntriesForUser(...)";

		logger.info(
				"{}: Returning provenance entries for user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		List<SnapshotUsage> logs =
				provenanceDAO.findRelevantToUser(userEntity);

		List<ProvenanceLogEntry> ret = ProvenanceMapper
				.convertUsageToProvenance(logs, userService);

		logger.debug("{}: retrieving {} provenance messages", m,
				ret.size());
		return ret;
	}

	// private SnapshotUsage getUsageFromProv(ProvenanceLogEntry log) {
	// final UserEntity user = userDAO.findByNaturalId(log.getUserId());
	//
	// final DataSnapshotEntity snap,
	// final String userId,
	// final USAGE_TYPE usage,
	// final DataSnapshotEntity derived,
	// final TsAnnotationEntity annId
	//
	// if (initiator == null)
	// throw new UserNotFoundException(log.getSender());
	// if (recipient == null)
	// throw new UserNotFoundException(log.getRecipient());
	//
	// return new EventLogEntity(recipient, initiator, log.getVisibility(),
	// log.getMessage(),
	// log.getTime());
	// }

	private SnapshotUsage getProvenanceRecord(ProvenanceLogEntry log) {
		DataSnapshotEntity entity = dataSnapshotDAO.findByNaturalId(
				log.getSnapshot(), false);
		if (entity == null) {
			throw new DataSnapshotNotFoundException(log.getSnapshot());
		}
		DataSnapshotEntity derived = null;
		if (log.getDerivedSnapshot() != null) {
			derived = dataSnapshotDAO.findByNaturalId(log.getDerivedSnapshot(),
					false);
		}
		// SnapshotUsage.USAGE_TYPE utype;
		TsAnnotationEntity annId = null;
		if (log.getAnnotation() != null) {
			annId = dataSnapshotDAO.findTsAnn(entity, log.getAnnotation());
		}

		int utype = log.getUsage().ordinal();
		// switch (log.getUsage()) {
		// case CREATES:
		// utype = SnapshotUsage.USAGE_TYPE.CREATES;
		// break;
		// case ADDSTO:
		// utype = SnapshotUsage.USAGE_TYPE.ADDSTO;
		// break;
		// case VIEWS:
		// utype = SnapshotUsage.USAGE_TYPE.VIEWS;
		// break;
		// case ANNOTATES:
		// utype = SnapshotUsage.USAGE_TYPE.ANNOTATES;
		// break;
		// case DERIVES:
		// utype = SnapshotUsage.USAGE_TYPE.DERIVES;
		// break;
		// case DISCUSSES:
		// utype = SnapshotUsage.USAGE_TYPE.DISCUSSES;
		// break;
		// case FAVORITES:
		// utype = SnapshotUsage.USAGE_TYPE.DERIVES;
		// break;
		// default:
		// throw new RuntimeException("Unable to cast enums");
		// }
		User u = userService.findExistingEnabledUser(log.getUserId());
		return new SnapshotUsage(
				entity,
				userDAO.getOrCreateUser(u.getUserId()),
				utype,
				derived,
				annId);
	}

	@Override
	public Set<DatasetAndTool> getRelatedAnalyses(
			final User user,
			final String inputDataSnapshotRevId) throws AuthorizationException {
		final String m = "getRelatedAnalyses(...)";
		final Set<AnalyzedDataSnapshot> analyzedSnapshots =
				analyzedDsDAO.findByDsPubId(inputDataSnapshotRevId);

		final AnalyzedDataSnapshot firstOne = get(analyzedSnapshots, 0, null);

		if (firstOne != null) {
			extAuthzHandler.checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							firstOne.getDataSnapshot().getPubId(), false),
					CorePermDefs.READ,
					firstOne.getDataSnapshot().getPubId());
		}

		final Set<DatasetAndTool> dsRevIdAndToolNames = newHashSet();
		for (final AnalyzedDataSnapshot analyzedSnapshot : analyzedSnapshots) {
			if (extAuthzHandler.isPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							analyzedSnapshot.getAnalysis().getPubId(), false),
					CorePermDefs.READ,
					analyzedSnapshot.getAnalysis().getPubId())) {
				dsRevIdAndToolNames.add(
						new DatasetAndTool(
								analyzedSnapshot.getAnalysis().getPubId(),
								analyzedSnapshot.getAnalysis().getLabel(),
								analyzedSnapshot.getToolLabel()));
			}
		}
		logger.info(
				"{}: Returning {} related analyses to user {}.",
				new Object[] { m, dsRevIdAndToolNames.size(),
						user.getUsername() });
		return dsRevIdAndToolNames;
	}

	@Override
	public String getSnapshotIDFromName(
			final User user,
			final String ssName)
			throws AuthorizationException {
		// final UserEntity userEntity =
		// userDAO.getOrCreateUser(user.getUserId());

		DataSnapshotEntity dupLabel =
				dataSnapshotDAO.findByLabel(ssName);

		if (dupLabel == null) {
			return null;
		} else {
			extAuthzHandler.checkPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dupLabel.getPubId(), false),
					CorePermDefs.READ,
					dupLabel.getPubId());

			return dupLabel.getPubId();
		}
	}

	@Override
	public Integer getSnapshotsByUser(User user) throws AuthorizationException {
		final String m = "getSnapshotsByUser(...)";

		logger.info(
				"{}: Returning snapshot count by user {}.",
				new Object[] { m,
						user.getUsername() });

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		return datasetDAO.findContributedSnapshotCount(userEntity);
	}

	@Override
	public Map<String, TimeSeriesDto> getTimeSeries(
			User user,
			String dsId) throws AuthorizationException {
		DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dsId);

		if (ds == null) {
			throw new DataSnapshotNotFoundException(dsId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.READ,
				ds.getPubId());

		Set<TimeSeriesEntity> tss = ds.getTimeSeries();

		Map<String, TimeSeriesDto> tsIds2TsDtos = newHashMap();

		for (TimeSeriesEntity ts : tss) {
			tsIds2TsDtos.put(
					ts.getPubId(),
					new TimeSeriesDto(
							ts.getPubId(),
							ts.getLabel(),
							ts.getFileKey()));
		}
		return tsIds2TsDtos;
	}

	@Override
	public Set<ToolDto> getTools(final User user) {
		final String m = "getTools(User)";

		final UserEntity btUser = userDAO.getOrCreateUser(user.getUserId());

		List<ToolEntity> tools = null;

		if (user.getRoles().contains(Role.ADMIN)) {
			tools = toolDAO.findAllForAdmin();
		} else {
			tools = toolDAO.findAll(btUser);
		}
		final Set<ToolDto> toolSet = newHashSet();
		for (final ToolEntity tool : tools) {
			// No need to check for read permission - we only retrieve what can
			// be read
			toolSet.add(
					new ToolDto(
							tool.getLabel(),
							tool.getAuthor().getId(),
							userService.findUserByUid(
									tool.getAuthor().getId()).getUsername(),
							tool.getSourceUrl(),
							tool.getBinUrl(),
							tool.getPubId(),
							tool.getDescription())
					);
		}
		logger.info("{}: Returning {} tools to user {}.", new Object[] { m,
				toolSet.size(), user.getUsername() });
		return toolSet;
	}

	@Override
	public List<ToolDto> getTools(User user, Iterable<String> toolIds) {
		final String m = "getTools(User, Iterable)";
		List<ToolDto> toolDtos = newArrayList();
		for (final String toolPubId : toolIds) {
			final ToolEntity toolEntity = toolDAO
					.findByNaturalId(toolPubId);
			if ((toolEntity == null) ||
					(!extAuthzHandler.isPermitted(
							getPermissions(
									user,
									HasAclType.TOOL,
									CorePermDefs.CORE_MODE_NAME,
									toolEntity.getPubId(),
									false),
							CorePermDefs.READ,
							toolEntity.getPubId()))) {
				toolDtos.add(null);
				continue;
			}

			toolDtos.add(new ToolDto(
					toolEntity.getLabel(),
					// toolEntity.getToolVersion(),
					toolEntity.getAuthor().getId(),
					userService.findUserByUid(toolEntity.getAuthor().getId())
							.getUsername(),
					toolEntity.getSourceUrl(),
					toolEntity.getBinUrl(),
					toolEntity.getPubId(),
					toolEntity.getDescription())
					// toolEntity.getDialogXmlStr())
					);
		}

		logger.info("{}: Returning {} tools to user {}.", new Object[] { m,
				toolDtos.size(), user.getUsername() });
		return toolDtos;
	}

	@Override
	public UserEntity getUser(UserId userId) {
		return userDAO.getOrCreateUser(userId);
	}

	@Override
	public String getUserClob(User user) {
		final String m = "getUserClob(...)";

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		logger.info(
				"{}: Returning clob from user {}.",
				new Object[] { m, user.getUsername() });
		return userEntity.getClob();
	}

	@Override
	public UserIdSessionStatus getUserIdSessionStatus(SessionToken token,
			Date minLastActive) {
		final String m = "getUserIdSessionStatus(...)";

		final SessionEntity sessionEntity = sessionDAO.findByNaturalId(
				token.getId());

		if (sessionEntity == null) {
			throw new SessionNotFoundException("session not found");
		}

		// refresh it: it may have been modified since when this Hibernate
		// session
		// saw it last.
		// We get a upgrade lock here because our
		// multi-threaded client requires us to get a pessimistic write lock.
		// And because we have hql which does blind deletes of expired sessions.
		sessionDAO.refresh(sessionEntity, true);

		boolean timedOut = sessionEntity.getLastAccessTime().before(
				minLastActive);
		if (timedOut && sessionEntity.getStatus() == SessionStatus.ACTIVE) {
			sessionEntity.setStatus(SessionStatus.EXPIRED);
		}
		if (sessionEntity.getStatus() == SessionStatus.ACTIVE) {
			sessionEntity.setLastAccessTime(new Date());
		}
		final UserId userId = sessionEntity.getUser().getId();
		final SessionStatus sessionStatus = sessionEntity.getStatus();
		// This is at debug level because at info level it fills the logs too
		// quickly.
		if (logger.isDebugEnabled()) {
			final User user = userService.findUserByUid(userId);
			logger.debug(
					"{}: Returning session belonging to user {} with status {}",
					new Object[] {
							m,
							user.getUsername(),
							sessionStatus });
		}
		return new UserIdSessionStatus(userId, sessionStatus);

	}

	@Override
	public List<UserId> getUserIdsWithActiveJobs() {
		final String m = "getUserIdsWithActiveJobs(...)";
		final SortedSet<Long> userIdLongs = jobDAO.listJobUserIds();
		final List<UserId> userIdList = newArrayList();
		for (final Long userIdLong : userIdLongs) {
			userIdList.add(new UserId(userIdLong));
		}

		logger.info("{}: Returning {} userIds", m, userIdList.size());
		return userIdList;
	}

	@Override
	public void initializeJob(String datasetId, Set<TaskDto> tasks) {
		final String m = "initializeJob(...)";
		checkNotNull(datasetId);
		checkNotNull(tasks);
		final JobEntity job = jobDAO
				.findByNaturalId(datasetId);
		if (job == null) {
			throw new JobNotFoundException(datasetId);
		}
		if (job.getTasks().isEmpty()) {
			job.getTasks().addAll(TaskAssembler.buildEntitySet(job, tasks));
			logger.info("{}: Added {} tasks to job for dataset {}.",
					new Object[] { m, tasks.size(), datasetId });
		} else {
			throw new IllegalArgumentException("Job for dataset "
					+ datasetId + " already has tasks assigned.");
		}

	}

	@Override
	public boolean isFrozen(User user, String dsId)
			throws AuthorizationException {
		// final String M = "isFrozen(...)";
		final DataSnapshotEntity dsEntity =
				dataSnapshotDAO.findByNaturalId(dsId);
		if (dsEntity == null) {
			throw new DataSnapshotNotFoundException(dsId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dsEntity.getPubId(), false),
				CorePermDefs.READ,
				dsEntity.getPubId());

		return dsEntity.isFrozen();
	}

	@Override
	public boolean isOwnerOfDataset(final User user, final String dsRevId) {
		final DataSnapshotEntity ds = dataSnapshotDAO.findByNaturalId(dsRevId);
		boolean owner = false;
		ExtPermissions permissions =
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false);
		if (permissions
				.getUserAce()
				.get()
				.getPerms()
				.contains(CorePermDefs.OWNER_PERM)) {
			owner = true;
		}
		return owner;
	}

	@Override
	public boolean isRecordingModifiable(DataSnapshotEntity recording) {
		boolean modifiable = !dataSnapshotDAO.isInAProject(recording);
		if (modifiable) {
			for (final TimeSeriesEntity timeSeries : recording.getTimeSeries()) {
				if (timeSeriesDAO.isInADataset(timeSeries)) {
					modifiable = false;
					break;
				}
			}
		}
		return modifiable;
	}

	@Override
	public String removeTimeSeriesFromDataset(
			final User user,
			final String dsId,
			final Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException {
		String m = "removeTimeSeriesFromDataset(...)";
		Dataset dataset = datasetDAO.findByNaturalId(dsId);
		if (dataset == null) {
			throw new DatasetNotFoundException(dsId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dataset.getPubId(), false),
				CorePermDefs.EDIT,
				dataset.getPubId());

		final String dRevId = dataset.getPubId();
		for (final String tsRevId : timeSeriesRevIds) {
			try {
				final TimeSeriesEntity timeSeries = find(
						dataset.getTimeSeries(),
						compose(equalTo(tsRevId), IHasPubId.getPubId));
				dataset.getTimeSeries().remove(timeSeries);
				logger.debug(
						"{}: Removed time series rev id {} from data snapshot [{}] rev id [{}].",
						new Object[] {
								m,
								dataset.getLabel(),
								tsRevId,
								dsId });
				tsAnnotationDAO.removeAnnotatedAndDeleteEmptyAnnotations(
						dataset,
						timeSeries);
				if (!timeSeriesDAO.isInADataset(timeSeries)
						&& contactDAO.findByTrace(timeSeries) == null) {
					timeSeriesDAO.delete(timeSeries);
					logger.info(
							"{}: Deleted time series label {} revId {}; no more references",
							new Object[] {
									m,
									timeSeries.getLabel(),
									tsRevId });
				}
			} catch (final NoSuchElementException e) {
				throw new IllegalArgumentException(
						"The passed in time series rev id "
								+ tsRevId
								+ " does not belong to data snapshot "
								+ dataset);
			}
		}
		logger.info(
				"{}: Removed {} time series from snapshot {} for user {}.",
				new Object[] { m, timeSeriesRevIds.size(), dataset.getLabel(),
						user.getUsername() });
		return dRevId;
	}

	@Override
	public void removeTools(final User user, final Set<String> toolIds)
			throws AuthorizationException {
		final String m = "removeTools(...)";
		for (final String toolPubId : toolIds) {
			final ToolEntity tool = toolDAO.findByNaturalId(toolPubId);
			if (tool == null) {
				throw new IllegalArgumentException(
						"there is no tool with rev id [" + toolPubId + "]");
			}

			extAuthzHandler.checkPermitted(
					getPermissions(
							user,
							HasAclType.TOOL,
							CorePermDefs.CORE_MODE_NAME,
							tool.getPubId(), false),
					CorePermDefs.DELETE,
					null);
			toolDAO.delete(tool);
		}
		logger.info("{}: Removed {} tools for user {}.", new Object[] { m,
				toolIds.size(), user.getUsername() });
	}

	@Override
	@Nonnull
	public String removeTsAnnotations(
			final User user,
			final String dataSnapshotRevId,
			final Set<String> tsAnnotationRevIds)
			throws AuthorizationException {
		final String m = "removeTsAnnotations(...)";

		final DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(dataSnapshotRevId);

		if (ds == null) {
			throw new IllegalArgumentException(
					"there is no data snapshot with rev id ["
							+ dataSnapshotRevId + "]");
		}

		final ExtPermissions perms = getPermissionsHelper(
				user,
				HasAclType.DATA_SNAPSHOT.getEntityName(),
				CorePermDefs.CORE_MODE_NAME,
				ds);
		extAuthzHandler.checkPermitted(perms, CorePermDefs.EDIT, ds.getPubId());

		List<String> revIds = new ArrayList<String>();
		revIds.addAll(tsAnnotationRevIds);

		// Do in batches of 500
		Set<String> subList = new HashSet<String>();
		final int batch = 500;
		for (int i = 0; i < revIds.size(); i += batch) {
			final long startNanos = System.nanoTime();
			subList.clear();
			subList.addAll(revIds.subList(i, i
					+ ((revIds.size() - i) > batch ? batch
							: (revIds.size() - i))));

			if (tsAnnotationDAO.deleteByParentAndIds(ds, subList) == subList
					.size()) {
				logger.debug(
						"{}: Removed annotation rev ids {} from datasnapshot {} for user. {} seconds",
						new Object[] {
								m,
								subList,
								ds,
								BtUtil.diffNowThenSeconds(startNanos) });
			} else {
				logger.debug(
						"{}: Removed only some of annotation rev ids {} from datasnapshot {} for user. {} seconds",
						new Object[] {
								m,
								subList,
								ds,
								BtUtil.diffNowThenSeconds(startNanos) });
			}
		}

		// for (final String tsAnnRevId : tsAnnotationRevIds) {
		// if (tsAnnotationDAO.deleteByParentAndId(ds, tsAnnRevId) == 0) {
		// throw new IllegalArgumentException(
		// "Annotation rev id ["
		// + tsAnnRevId
		// + "] cannot be found in dataset "
		// + ds.toString());
		// }
		// logger.debug(
		// "{}: Removed annotation rev id {} from datasnapshot {} for user.",
		// new Object[] {
		// m,
		// tsAnnRevId,
		// ds });
		// }
		if (logger.isInfoEnabled()) {
			logger.info(
					"{}: Removed {} annotations from snapshot {} for user {}.",
					new Object[] { m, Iterables.size(tsAnnotationRevIds),
							ds.getLabel(),
							user.getUsername() });
		}
		return dataSnapshotRevId;
	}

	@Override
	public void setFrozen(
			User user,
			String dsId,
			boolean frozen)
			throws AuthorizationException {
		// final String M = "setFrozen(...)";
		final DataSnapshotEntity dsEntity =
				dataSnapshotDAO.findByNaturalId(dsId);
		if (dsEntity == null) {
			throw new DataSnapshotNotFoundException(dsId);
		}
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dsEntity.getPubId(), false),
				CorePermDefs.EDIT,
				dsEntity.getPubId());

		dsEntity.setFrozen(frozen);
	}

	@Override
	public void setSessionStatus(SessionToken token, SessionStatus status) {
		final String m = "setSessionStatus(...)";

		final SessionEntity sessionEntity = sessionDAO.findByNaturalId(
				token.getId());

		if (sessionEntity == null) {
			throw new SessionNotFoundException("session not found");
		}

		// refresh it: it may have been modified since when this Hibernate
		// session
		// saw it last.
		// We get a upgrade lock here because our
		// multi-threaded client requires us to get a pessimistic write lock.
		// And because we have hql which does blind deletes of expired sessions.
		sessionDAO.refresh(sessionEntity, true);

		final SessionStatus origStatus = sessionEntity.getStatus();
		UserId userId = sessionEntity.getUser().getId();
		sessionEntity.setStatus(status);
		logger.info("{}: Session for userId {} has changed from {} to {}",
				new Object[] {
						m,
						userId,
						origStatus,
						status });
	}

	@Override
	@Nonnull
	public String storeClob(
			final User user,
			final String dataSnapshotRevId,
			final String clob)
			throws AuthorizationException {
		final String m = "storeClob(...)";
		final DataSnapshotEntity ds = dataSnapshotDAO
				.findByNaturalId(dataSnapshotRevId);
		if (ds == null) {
			throw new IllegalArgumentException(
					"there is no data snapshot with rev id ["
							+ dataSnapshotRevId + "]");
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(),
						false),
				CorePermDefs.EDIT,
				ds.getPubId());

		ds.setClob(clob);
		logger.info(
				"{}: Added clob to snapshot {} for user {}.",
				new Object[] {
						m,
						ds.getLabel(), user.getUsername() });
		loggerDsClob.debug("{}: clob {}", m, clob);
		return ds.getPubId();
	}

	@Override
	@Nonnull
	public String storeJsonKeyValues(
			final User user,
			final String dataSnapshotRevId,
			final Collection<? extends IJsonKeyValue> keyValues)
			throws AuthorizationException, JsonProcessingException {
		final String m = "storeJsonKeyValues(...)";

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						dataSnapshotRevId,
						false),
				CorePermDefs.EDIT,
				dataSnapshotRevId);

		jsonDAO.setKeyValues(dataSnapshotRevId, keyValues);
		logger.info(
				"{}: Set JSON for snapshot {} for user {}.",
				new Object[] {
						m,
						dataSnapshotRevId, user.getUsername() });
		loggerDsClob.debug("{}: clob {}", m, keyValues);
		return dataSnapshotRevId;
	}

	@Override
	public List<String> storeTools(
			final User user,
			final Iterable<? extends ToolDto> tools)
			throws AuthorizationException {
		final String m = "storeTools(...)";
		logger.debug("{}: tools: {}", m, tools);
		UserEntity btUser = userDAO.getOrCreateUser(user.getUserId());
		List<String> toolPubIds = newArrayList();
		for (final ToolDto toolDto : tools) {

			// Get userID, if null --> lookup
			UserId authorId = null;
			if (toolDto.getAuthorId().isPresent()) {
				authorId = toolDto.getAuthorId().get();
			} else {
				authorId = userService.findUserByUsername(
						toolDto.getAuthorName()).getUserId();
			}

			UserEntity author = userDAO.get(authorId);
			ToolEntity toolEntity = null;
			if (toolDto.getId().isPresent()) {
				toolEntity =
						toolDAO.findByNaturalId(toolDto.getId().get());
				if (toolEntity == null) {
					throw new ToolNotFoundForDtoException(
							"could not find tool for " + toolDto, toolDto);
				}

				extAuthzHandler.checkPermitted(
						getPermissions(
								user,
								HasAclType.TOOL,
								CorePermDefs.CORE_MODE_NAME,
								toolEntity.getPubId(), false),
						CorePermDefs.EDIT,
						null);

				toolEntity.setLabel(toolDto.getLabel());
				toolEntity.setAuthor(author);
				toolEntity.setSourceUrl(toolDto.getSourceUrl());
				toolEntity.setBinUrl(toolDto.getBinUrl());
				toolEntity.setDescription(toolDto.getDescription().orNull());
			} else {
				// Create new ToolEntity
				toolEntity = new ToolEntity(
						SecurityUtil.createUserOwnedAcl(
								btUser,
								permissionDAO),
						toolDto.getLabel(),
						author,
						toolDto.getSourceUrl(),
						toolDto.getBinUrl(),
						toolDto.getDescription().orNull());
				toolDAO.saveOrUpdate(toolEntity);
			}
			toolPubIds.add(toolEntity.getPubId());
		}
		logger.info("{}: Stored {} tools for user {}.", new Object[] { m,
				toolPubIds.size(), user.getUsername() });
		return toolPubIds;
	}

	@Override
	public DataSnapshotIds storeTsAnnotations(
			User user,
			String datasetId,
			Iterable<? extends TsAnnotationDto> tsAnnDtos)
			throws AuthorizationException,
			TimeSeriesNotFoundInDatasetException, BadTsAnnotationTimeException {
		String m = "storeTsAnnotations(...)";

		checkNotNull(datasetId, "dataSnapshotRevId is null");

		final DataSnapshotEntity ds =
				dataSnapshotDAO.findByNaturalId(datasetId);

		if (ds == null) {
			throw new DataSnapshotNotFoundException(
					datasetId);
		}

		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						ds.getPubId(), false),
				CorePermDefs.EDIT,
				ds.getPubId());

		Map<String, String> newTsAnnIds = newHashMap();

		long noTsAnns = dataSnapshotDAO.countTsAnns(ds);

		if ((noTsAnns + size(tsAnnDtos) > MAX_TS_ANNS)) {
			throw new TooManyTsAnnsException(ds.getPubId(), ds.getLabel(),
					noTsAnns, size(tsAnnDtos), MAX_TS_ANNS);
		}

		final Set<String> seenTsAnnRevIds = newHashSet();
		TsAnnotationEntity tsAnnEntity;
		int i = 0;
		final Set<TsAnnotationEntity> toBeEvicteds = newHashSet();

		final Map<String, Long> tsRevIdsToIds = newHashMap();

		UserEntity btUser = userDAO.getOrCreateUser(user.getUserId());

		for (final TsAnnotationDto tsAnnDto : tsAnnDtos) {
			// Find the TimeSeriesEntities that this annotation will
			// annotate.
			Set<TimeSeriesEntity> annotatedByTsAnnDtos = newHashSet();
			Set<String> annotatedTsRevIds =
					newHashSet(transform(tsAnnDto.getAnnotated(),
							IHasStringId.getId));
			checkArgument(annotatedTsRevIds.size() > 0,
					"annotation %s has no time series", tsAnnDto);
			if (tsAnnDto.getStartOffsetMicros()
					> tsAnnDto.getEndOffsetMicros()
					|| tsAnnDto.getStartOffsetMicros() < 0
					|| tsAnnDto.getEndOffsetMicros() < 0) {
				throw new BadTsAnnotationTimeException(tsAnnDto);
			}
			for (String annotatedTsRevId : annotatedTsRevIds) {

				if (tsRevIdsToIds.containsKey(annotatedTsRevId)) {
					annotatedByTsAnnDtos
							.add(timeSeriesDAO.findById(
									tsRevIdsToIds.get(annotatedTsRevId), false));
				} else {
					TimeSeriesEntity annotatedByTsAnnDto =
							find(ds.getTimeSeries(),
									compose(equalTo(
											annotatedTsRevId),
											IHasPubId.getPubId),
									null);
					if (annotatedByTsAnnDto == null) {
						throw new TimeSeriesNotFoundInDatasetException(
								"The passed in annotation annotates time series rev id "
										+ annotatedTsRevId
										+ " which does not belong to dataset "
										+ ds);
					}

					// to make FindBugs happy
					checkNotNull(annotatedByTsAnnDto);
					annotatedByTsAnnDtos.add(annotatedByTsAnnDto);
					tsRevIdsToIds.put(
							annotatedByTsAnnDto.getPubId(),
							annotatedByTsAnnDto.getId());
				}
			}
			final String origAnnRevId = tsAnnDto.getId();
			if (origAnnRevId != null) {
				if (seenTsAnnRevIds.add(origAnnRevId)) {

				} else {
					continue;
				}
				//
				// int updates = tsAnnotationDAO.update(
				// tsAnnDto,
				// ds,
				// annotatedByTsAnnDtos);

				tsAnnEntity = tsAnnotationDAO
						.findTsAnn(ds, tsAnnDto.getId());
				checkArgument(tsAnnEntity != null,
						"No Annotation rev id "
								+ tsAnnDto
								+ " in data snapshot "
								+ ds);
				// Make findbugs happy
				checkNotNull(tsAnnEntity);

				tsAnnEntity.setType(tsAnnDto.getType());
				tsAnnEntity.setStartOffsetUsecs(tsAnnDto
						.getStartOffsetMicros());
				tsAnnEntity.setEndOffsetUsecs(tsAnnDto.getEndOffsetMicros());

				Set<TimeSeriesEntity> toBeRemoved = newHashSet();
				for (TimeSeriesEntity ts : tsAnnEntity.getAnnotated()) {
					if (!annotatedByTsAnnDtos.contains(ts)) {
						toBeRemoved.add(ts);
					}
				}

				for (TimeSeriesEntity ts : toBeRemoved) {
					tsAnnEntity.getAnnotated().remove(ts);
				}

				for (TimeSeriesEntity ts : annotatedByTsAnnDtos) {
					tsAnnEntity.getAnnotated().add(ts);
				}

				tsAnnEntity.setAnnotator(tsAnnDto.getAnnotator());
				tsAnnEntity.setCreator(btUser);
				tsAnnEntity.setParent(ds);
				tsAnnEntity
						.setLayer(tsAnnDto.getLayer() == null
								? TsAnnotationEntity.getLayerDefault(ds)
								: tsAnnDto.getLayer());
				tsAnnEntity.setColor(tsAnnDto.getColor());
				tsAnnEntity.setDescription(tsAnnDto.getDescription());

				// A real revision
				logger.trace(
						"{}: Updated annotation rev id {} in data snapshot {}.",
						new Object[] {
								m,
								tsAnnDto,
								ds });
			} else {
				// origAnnRevId null, so must be a new annotation.
				tsAnnEntity = new TsAnnotationEntity(
						tsAnnDto.getType().trim(),
						tsAnnDto.getStartOffsetMicros(),
						tsAnnDto.getEndOffsetMicros(),
						annotatedByTsAnnDtos,
						tsAnnDto.getAnnotator().trim(),
						btUser,
						ds,
						tsAnnDto.getLayer() == null
								? TsAnnotationEntity.getLayerDefault(ds)
								: tsAnnDto.getLayer(),
						BtUtil.nullSafeTrim(tsAnnDto.getDescription()),
						BtUtil.nullSafeTrim(tsAnnDto.getColor()));

				logger.trace(
						"{}: Added new annotation rev id {} to data snapshot rev id{}.",
						new Object[] { m,
								tsAnnEntity.getPubId(),
								datasetId });

			}
			tsAnnotationDAO.saveOrUpdate(tsAnnEntity);

			if (origAnnRevId == null) {
				if (tsAnnDto.getExternalId() == null) {
					logger.debug("{}: both originalAnnRevId and external id are null: "
							+ tsAnnDto);
				} else {
					newTsAnnIds.put(
							tsAnnDto.getExternalId(),
							tsAnnEntity.getPubId());
				}
			}
			tsAnnDto.setId(tsAnnEntity.getPubId());
			toBeEvicteds.add(tsAnnEntity);
			flushBatch(++i, toBeEvicteds);
		}

		if (logger.isInfoEnabled()) {
			logger.info(
					"{}: Stored {} annotations to snapshot {} for user {}.",
					new Object[] { m, Iterables.size(tsAnnDtos),
							ds.getLabel(), user.getUsername() });
		}

		return new DataSnapshotIds(datasetId, newTsAnnIds);
	}

	@Override
	public void storeUserClob(
			final User user,
			final String clob) {
		final String m = "storeUserClob(...)";
		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());
		userEntity.setClob(clob);
		logger.info(
				"{}: Added clob to user {}.",
				new Object[] {
						m,
						user.getUsername() });
	}

	private void updatePE(ProjectEntity project, EegProject newProj) {
		project.getTags().clear();
		project.getTags().addAll(newProj.getTags());
		project.getCredits().clear();
		project.getCredits().addAll(newProj.getCredits());

		project.setName(newProj.getName());
		project.setComplete(newProj.isComplete());
		project.getAdmins().clear();
		for (UserId u : newProj.getAdmins()) {
			UserEntity userEntity = userDAO.getOrCreateUser(u);
			project.getAdmins().add(userEntity);
		}

		project.getTeam().clear();
		for (UserId u : newProj.getTeam()) {
			UserEntity userEntity = userDAO.getOrCreateUser(u);
			project.getTeam().add(userEntity);
		}

		project.getSnapshots().clear();
		for (String snapId : newProj.getSnapshots()) {
			final DataSnapshotEntity dataset = dataSnapshotDAO
					.findByNaturalId(snapId);
			if (dataset != null) {
				project.getSnapshots().add(dataset);
			}
		}

		project.getTools().clear();
		for (String toolId : newProj.getTools()) {
			final ToolEntity tool = toolDAO.findByNaturalId(toolId);
			if (tool != null) {
				project.getTools().add(tool);
			}
		}
	}

	@Override
	public void updateProject(User admin, String projectId,
			IProjectUpdater updater)
			throws AuthorizationException {

		// upgrade lock to avoid deadlocks with other updaters
		ProjectEntity proj = projectDAO.findByNaturalId(projectId, true);

		userDAO.getOrCreateUser(admin.getUserId());

		EegProject eegProj = ProjectMapper.convertProjectToEeg(proj);
		updater.update(eegProj);

		updatePE(proj, eegProj);
	}

	@Override
	public void updateTasks(User user, String datasetId, Set<TaskDto> taskSet) {
		final String m = "updateTasks(...)";
		checkNotNull(user);
		checkNotNull(datasetId);
		checkNotNull(taskSet);
		final JobEntity job = jobDAO
				.findByNaturalId(datasetId);
		if (job == null) {
			throw new JobNotFoundException(datasetId);
		}
		for (final TaskDto taskDto : taskSet) {
			final AnalysisTask task = find(job.getTasks(),
					compose(equalTo(taskDto.getName()), IHasName.getName),
					null);
			if (task == null) {
				throw new TaskNotFoundException(datasetId, taskDto.getName());
			}

			task.setStatus(taskDto.getStatus());
			task.setRunStartTime(new Date(taskDto.getRunStartTimeMillis()));
			task.setWorker(taskDto.getWorker());
		}

		logger.info("{}: Updated {} tasks in job {} for user {}", new Object[] {
				m,
				taskSet.size(), datasetId, user.getUsername() });

	}

	@Override
	public void updateTaskStatus(User user, String datasetId,
			String taskId, TaskStatus newStatus) {
		final String m = "updateTaskStatus(...)";
		checkNotNull(user);
		checkNotNull(datasetId);
		checkNotNull(taskId);
		checkNotNull(newStatus);
		final JobEntity job = jobDAO
				.findByNaturalId(datasetId);
		if (job == null) {
			throw new JobNotFoundException(datasetId);
		}

		final AnalysisTask task = find(job.getTasks(),
				compose(equalTo(taskId), IHasName.getName), null);
		if (task == null) {
			throw new TaskNotFoundException(datasetId, taskId);
		}

		task.setStatus(newStatus);
		logger.info(
				"{}: Setting status of task {} in job {} to {} for user {}",
				new Object[] { m,
						taskId, datasetId, newStatus, user.getUsername() });
	}

	// public List<TimeSeriesEntity> getTimeSeriesEntities(String id) throws
	// DataSnapshotNotFoundException {
	// List<TimeSeriesEntity> channels = new ArrayList<TimeSeriesEntity>();
	// final Dataset ds =
	// datasetDAO.findByNaturalId(id);
	//
	// if (ds != null) {
	// channels = timeSeriesDAO.findByDataset(ds);
	// } else {
	// EegStudy study = studyDAO.findByNaturalId(id);
	// if (study == null) {
	// throw new DataSnapshotNotFoundException(
	// id);
	// }
	//
	// Recording rec = study.getRecording();
	// if (rec == null) {
	// throw new DataSnapshotNotFoundException(
	// id);
	// }
	//
	// channels = timeSeriesDAO.findByRecording(rec);
	// }
	// return channels;
	// }

	@Override
	public List<ChannelInfoDto> writeChannelInfos(
			String dataSnapshotId,
			List<ChannelInfoDto> chInfoDtos) {
		String m = "writeChannelInfo(...)";

		// Set<String> ids = new HashSet<String>();
		// for (ChannelInfoDto channel: info) {
		// if (!ids.contains(channel.getParent().getId())) {
		// ids.add(channel.getParent().getId());
		//
		// checkNotNull(channel.getParent().getId(),
		// "dataSnapshotRevId is null");
		// }
		// }
		// if (ids.size() != 1)
		// throw new
		// RuntimeException("There must be exactly 1 dataset for all channels");
		//
		// String id = ids.iterator().next();

		// final Dataset ds =
		// datasetDAO.findByNaturalId(id);
		//
		// if (ds == null) {
		// throw new DataSnapshotNotFoundException(
		// id);
		// }

		if (chInfoDtos.isEmpty()) {
			return chInfoDtos;
		}

		DataSnapshotEntity dataSnapshot = dataSnapshotDAO
				.findByNaturalId(dataSnapshotId);

		List<TimeSeriesEntity> tsEntities = timeSeriesDAO
				.findByDataSnapshot(dataSnapshot);

		logger.debug(
				"{}: tsEntities.size {} chInfoDtos.size {}",
				m,
				tsEntities.size(),
				chInfoDtos.size());
		int i = -1;
		for (TimeSeriesEntity tsEntity : tsEntities) {
			i++;
			boolean foundIt = false;
			for (ChannelInfoDto chInfoDto : chInfoDtos) {
				logger.debug("{}: tsEntity.pubId [{}] chInfoDto.id [{}]",
						new Object[] {
								m,
								tsEntity.getPubId(),
								chInfoDto.getId() });
				if (tsEntity.getPubId().equals(chInfoDto.getId())) {
					foundIt = true;
					TimeSeriesInfo tsInfo = tsEntity.getInfo();

					if (tsInfo == null)
						tsInfo = new TimeSeriesInfo(
								tsEntity,
								chInfoDto.getInstitution(),
								chInfoDto.getHeaderLength(),
								chInfoDto.getSubjectFirstName(),
								chInfoDto.getSubjectSecondName(),
								chInfoDto.getSubjectThirdName(),
								chInfoDto.getSubjectId(),
								chInfoDto.getNumberOfSamples(),
								chInfoDto.getChannelName(),
								chInfoDto.getRecordingStartTime(),
								chInfoDto.getRecordingEndTime(),
								chInfoDto.getSamplingFrequency(),
								chInfoDto.getLowFrequencyFilterSetting(),
								chInfoDto.getHighFrequencyFilterSetting(),
								chInfoDto.getNotchFilterFrequency(),
								chInfoDto.getAcquisitionSystem(),
								chInfoDto.getChannelComments(),
								chInfoDto.getStudyComments(),
								chInfoDto.getPhysicalChannelNumber(),
								chInfoDto.getMaximumBlockLength(),
								chInfoDto.getBlockInterval(),
								chInfoDto.getBlockHeaderLength(),
								chInfoDto.getMaximumDataValue(),
								chInfoDto.getMinimumDataValue(),
								chInfoDto.getVoltageConversionFactor(),
								i);
					else {
						tsInfo.setInstitution(chInfoDto.getInstitution());
						tsInfo.setHeaderLength(chInfoDto.getHeaderLength());
						tsInfo.setSubjectFirstName(chInfoDto
								.getSubjectFirstName());
						tsInfo.setSubjectSecondName(chInfoDto
								.getSubjectSecondName());
						tsInfo.setSubjectThirdName(chInfoDto
								.getSubjectThirdName());
						tsInfo.setSubjectId(chInfoDto.getSubjectId());
						tsInfo.setNumberOfSamples(chInfoDto
								.getNumberOfSamples());
						tsInfo.setChannelName(chInfoDto.getChannelName());
						tsInfo.setRecordingStartTime(chInfoDto
								.getRecordingStartTime());
						tsInfo.setRecordingEndTime(chInfoDto
								.getRecordingEndTime());
						tsInfo.setSamplingFrequency(chInfoDto
								.getSamplingFrequency());
						tsInfo.setLowFrequencyFilterSetting(chInfoDto
								.getLowFrequencyFilterSetting());
						tsInfo.setHighFrequencyFilterSetting(chInfoDto
								.getHighFrequencyFilterSetting());
						tsInfo.setNotchFilterFrequency(chInfoDto
								.getNotchFilterFrequency());
						tsInfo.setAcquisitionSystem(chInfoDto
								.getAcquisitionSystem());
						tsInfo.setChannelComments(chInfoDto
								.getChannelComments());
						tsInfo.setStudyComments(chInfoDto.getStudyComments());
						tsInfo.setPhysicalChannelNumber(chInfoDto
								.getPhysicalChannelNumber());
						tsInfo.setMaximumBlockLength(chInfoDto
								.getMaximumBlockLength());
						tsInfo.setBlockInterval(chInfoDto.getBlockInterval());
						tsInfo.setBlockHeaderLength(chInfoDto
								.getBlockHeaderLength());
						tsInfo.setMaximumDataValue(chInfoDto
								.getMaximumDataValue());
						tsInfo.setMinimumDataValue(chInfoDto
								.getMinimumDataValue());
						tsInfo.setVoltageConversionFactor(chInfoDto
								.getVoltageConversionFactor());

					}

					tsInfo.setDataModality(chInfoDto.getModality());
					tsInfo.setBaseline(chInfoDto.getBaseline());
					tsInfo.setMultiplier(chInfoDto.getMultiplier());
					tsInfo.setUnits(chInfoDto.getUnits());

					tsEntity.setInfo(tsInfo);
					try {
						timeSeriesDAO.flush(); // to get the internal id
					} catch (ConstraintViolationException cve) {

					}
					// chInfoDto.setInternalId(tsInfo.getId());
					chInfoDto.setInternalId(tsEntity.getInfo().getId());
					break;
				}
			}
			if (!foundIt) {
				logger.debug(
						"Unable to find channel with label ["
								+ tsEntity.getLabel()
								+ "] pubId: ["
								+ tsEntity.getPubId() + "]");
			}
		}

		return chInfoDtos;
	}

	// private Map<TimeSeriesEntity, TimeSeriesInfo>
	// getChannelDetails(List<TimeSeriesEntity> channels) {
	// final Map<TimeSeriesEntity, TimeSeriesInfo> infoMap = new
	// HashMap<TimeSeriesEntity, TimeSeriesInfo>();
	//
	// int i = 0;
	// for (TimeSeriesEntity ch: channels) {
	// infoMap.put(ch, timeSeriesDAO.getDetails(ch));
	// infoMap.get(ch).setChannelNumber(new Integer(i));
	// i++;
	// }
	// Collections.sort(channels, new Comparator<TimeSeriesEntity>() {
	//
	// @Override
	// public int compare(TimeSeriesEntity o1, TimeSeriesEntity o2) {
	// if (!infoMap.containsKey(o1) || !infoMap.containsKey(o2))
	// return 0;
	//
	// return new
	// Integer(infoMap.get(o1).getChannelNumber().compareTo(infoMap.get(o2).getChannelNumber()));
	// }
	//
	// });
	// return infoMap;
	// }
	@Override
	public List<ChannelInfoDto> getChannelInfos(String dataSnapshotId) {
		DataSnapshotEntity dataSnapshot = dataSnapshotDAO
				.findByNaturalId(dataSnapshotId);
		List<TimeSeriesEntity> timeSeriesEntities = timeSeriesDAO
				.findByDataSnapshot(dataSnapshot);
		// final Map<TimeSeriesEntity, TimeSeriesInfo> infoMap =
		// getChannelDetails(timeSeriesEntities);
		List<ChannelInfoDto> ret = new ArrayList<ChannelInfoDto>();
		for (TimeSeriesEntity timeSeriesEntity : timeSeriesEntities) {
			// TimeSeriesInfo timeSeriesInfo = infoMap.get(timeSeriesEntity);
			TimeSeriesInfo timeSeriesInfo = timeSeriesEntity.getInfo();
			if (timeSeriesInfo != null) {
				ChannelInfoDto info =
						new ChannelInfoDto(
								timeSeriesInfo.getTimeSeries().getPubId(),
								timeSeriesInfo.getId(),
								timeSeriesInfo.getInstitution(),
								timeSeriesInfo.getHeaderLength(),
								timeSeriesInfo.getSubjectFirstName(),
								timeSeriesInfo.getSubjectSecondName(),
								timeSeriesInfo.getSubjectThirdName(),
								timeSeriesInfo.getSubjectId(),
								timeSeriesInfo.getNumberOfSamples(),
								// timeSeriesInfo.getChannelName(),
								timeSeriesEntity.getLabel(),
								timeSeriesInfo.getRecordingStartTime(),
								timeSeriesInfo.getRecordingEndTime(),
								timeSeriesInfo.getSamplingFrequency(),
								timeSeriesInfo.getLowFrequencyFilterSetting(),
								timeSeriesInfo.getHighFrequencyFilterSetting(),
								timeSeriesInfo.getNotchFilterFrequency(),
								timeSeriesInfo.getAcquisitionSystem(),
								timeSeriesInfo.getChannelComments(),
								timeSeriesInfo.getStudyComments(),
								timeSeriesInfo.getPhysicalChannelNumber(),
								timeSeriesInfo.getMaximumBlockLength(),
								timeSeriesInfo.getBlockInterval(),
								timeSeriesInfo.getBlockHeaderLength(),
								timeSeriesInfo.getMaximumDataValue(),
								timeSeriesInfo.getMinimumDataValue(),
								timeSeriesInfo.getVoltageConversionFactor(),
								timeSeriesEntity.getFileKey());

				info.setModality(timeSeriesInfo.getDataModality());
				info.setBaseline(timeSeriesInfo.getBaseline());
				info.setMultiplier(timeSeriesInfo.getMultiplier());
				info.setUnits(timeSeriesInfo.getUnits());

				ret.add(info);
			}
		}

		return ret;
	}

	@Override
	public String addMEFPath(User user, String mefPath,
			ChannelInfoDto chInfoDto) {
		String m = "addMEFPath(User, String)";
		TimeSeriesEntity entity = timeSeriesDAO.findByTimeSeriesPath(mefPath);

		// skip if the path already exists!
		if (entity != null)
			return entity.getPubId();

		entity = new TimeSeriesEntity();
		TimeSeriesInfo tsInfo = new TimeSeriesInfo(
				entity,
				chInfoDto.getInstitution(),
				chInfoDto.getHeaderLength(),
				chInfoDto.getSubjectFirstName(),
				chInfoDto.getSubjectSecondName(),
				chInfoDto.getSubjectThirdName(),
				chInfoDto.getSubjectId(),
				chInfoDto.getNumberOfSamples(),
				chInfoDto.getChannelName(),
				chInfoDto.getRecordingStartTime(),
				chInfoDto.getRecordingEndTime(),
				chInfoDto.getSamplingFrequency(),
				chInfoDto.getLowFrequencyFilterSetting(),
				chInfoDto.getHighFrequencyFilterSetting(),
				chInfoDto.getNotchFilterFrequency(),
				chInfoDto.getAcquisitionSystem(),
				chInfoDto.getChannelComments(),
				chInfoDto.getStudyComments(),
				chInfoDto.getPhysicalChannelNumber(),
				chInfoDto.getMaximumBlockLength(),
				chInfoDto.getBlockInterval(),
				chInfoDto.getBlockHeaderLength(),
				chInfoDto.getMaximumDataValue(),
				chInfoDto.getMinimumDataValue(),
				chInfoDto.getVoltageConversionFactor(),
				chInfoDto.getPhysicalChannelNumber());

		tsInfo.setDataModality(chInfoDto.getModality());
		tsInfo.setBaseline(chInfoDto.getBaseline());
		tsInfo.setMultiplier(chInfoDto.getMultiplier());
		tsInfo.setUnits(chInfoDto.getUnits());

		entity.setInfo(tsInfo);
		entity.setFileKey(mefPath);
		entity.setLabel(chInfoDto.getChannelName());
		String pubId = BtUtil.newUuid();
		entity.setPubId(pubId);
		timeSeriesDAO.saveOrUpdate(entity);

		return pubId;
	}

	public static String getSnapshotPath(boolean isHuman, String institution,
			String containerName) {
		return Paths.get(isHuman ?
				"Human_Data" :
				"Animal_Data",
				(institution != null ?
						institution
						: "everywhere"), containerName).toString()
				.replaceAll("\\\\", "/");
	}

	@Override
	public String createSnapshot(User user, String containerName,
			IHasGwtRecording recInfo) {
		final String m = "createSnapshot(...)";

		DataSnapshotEntity study = null;

		UserEntity uent = this.getUser(user.getUserId());

		GwtRecording rec = recInfo.getRecording();

		Recording recording;

		String institution = null;
		if (user.getProfile().getInstitutionName().isPresent()) {
			institution = user.getProfile().getInstitutionName().get();
		}
		String path = getSnapshotPath((recInfo instanceof GwtEegStudy),
				institution, containerName);

		// String classType = (recInfo instanceof GwtEegStudy) ?
		// "Human_Data" :
		// "Animal_Data";

		if (rec == null)
			recording = new Recording(path,
					// classType + "/" +
					// (user.getProfile().getInstitutionName().isPresent() ?
					// user.getProfile().getInstitutionName().get()
					// : "everywhere") + "/" + containerName,
					"mef",
					(long) 0,
					(long) 0,
					null,
					null,
					null,
					null);
		else
			recording = new Recording(rec.getDir(),
					rec.getMefDir(),
					rec.getStartTimeUutc(),
					rec.getEndTimeUutc(),
					rec.getRefElectrodeDescription().get(),
					rec.getImagesFile(),
					rec.getReportFile(),
					rec.getCommentary()
					);

		if (recInfo instanceof GwtEegStudy) {
			GwtEegStudy theStudy = (GwtEegStudy) recInfo;
			EegStudy study2 = new EegStudy(
					SecurityUtil.createUserOwnedAcl(uent, permissionDAO)
					);

			study2.setRecording(recording);
			study2.setLabel(theStudy.getLabel());
			study2.setType(theStudy.getType());
			// if (theStudy.getSzCount().isPresent())
			// study2.setSzCount(theStudy.getSzCount().get());

			study = study2;
		} else if (recInfo instanceof GwtExperiment) {
			GwtExperiment exp = (GwtExperiment) recInfo;

			study = new ExperimentEntity(
					null,
					exp.getLabel(),
					SecurityUtil.createUserOwnedAcl(uent, permissionDAO),
					recording,
					exp.getAge(),
					null,
					null,
					exp.getStimIsiMs(),
					exp.getStimDurationMs(),
					null);
		} else {
			throw new IllegalArgumentException(
					"Unknown IHasGwtRecording type: "
							+ recInfo.getClass().getName());
		}

		study.setPubId(BtUtil.newUuid());

		Set<DataSnapshotEntity> exists = dataSnapshotDAO.findAnyWithLabel(study
				.getLabel());

		boolean reuse = false;
		for (DataSnapshotEntity dse : exists) {
			reuse = extAuthzHandler.isPermitted(
					getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dse.getPubId(), false),
					CorePermDefs.EDIT,
					dse.getPubId());
			if (reuse) {
				study = dse;
				break;
			}
		}

		if (!reuse) {
			// if (exists == null) {
			dataSnapshotDAO.saveOrUpdate(study);
			System.out.println("Created new study " + study.getLabel() + " / "
					+ study.getPubId());
		} else {
			// extAuthzHandler.checkPermitted(
			// getPermissions(
			// user,
			// HasAclType.DATA_SNAPSHOT,
			// CorePermDefs.CORE_MODE_NAME,
			// exists.getPubId(), false),
			// CorePermDefs.EDIT,
			// exists.getPubId());
			System.out.println("Reused study " + study.getLabel() + " / "
					+ study.getPubId());
		}

		logger.info(
				"{}: Added snapshot {} for user {}.",
				new Object[] {
						m,
						study.getPubId(),
						user.getUsername() });

		return study.getPubId();
	}

	@Override
	public OrganizationEntity getOrganization(String orgName, String code) {
		OrganizationEntity org;

		org = organizationDAO.findByOrganization(orgName);

		int count = organizationDAO.findAll().size();

		// Create if it doesn't exist
		if (org == null) {
			org = new OrganizationEntity(
					orgName,
					"I" + count,
					new ExtAclEntity());

			organizationDAO.saveOrUpdate(org);
		}

		return org;
	}

	@Override
	public void setAdmissionInfo(User user, String snapshotId,
			GwtPatient origPatient, int ageAtAdmission)
			throws AuthorizationException, DatasetNotFoundException {
		EegStudy study = studyDAO.findByPubId(snapshotId);

		if (study == null)
			throw new DatasetNotFoundException(snapshotId);
		extAuthzHandler.checkPermitted(
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						study.getPubId(), false),
				CorePermDefs.EDIT,
				study.getPubId());
		// See if patient is new
		Patient patient = patientDAO.findByLabelEager(origPatient.getLabel());

		String orgName = "unknown";
		String code = "";

		if (origPatient.getOrganization() != null &&
				!origPatient.getOrganization().getName().equals("Temporary")) {
			orgName = origPatient.getOrganization().getName();
			code = origPatient.getOrganization().getCode();
		}

		OrganizationEntity org = getOrganization(orgName, code);

		if (patient == null) {

			patient = new Patient(origPatient.getLabel(),
					org,
					origPatient.getHandedness(),
					origPatient.getEthnicity(),
					origPatient.getGender(),
					origPatient.getEtiology(),
					null,
					null,
					null,
					null,
					null);
		}
		if (origPatient.getHandedness() != Handedness.UNKNOWN)
			patient.setHandedness(origPatient.getHandedness());
		if (origPatient.getEthnicity() != Ethnicity.UNKNOWN)
			patient.setEthnicity(origPatient.getEthnicity());

		if (origPatient.getGender() != Gender.UNKOWN)
			patient.setGender(origPatient.getGender());

		if (origPatient.getEtiology() != null
				&& !origPatient.getEtiology().isEmpty())
			patient.setEtiology(origPatient.getEtiology());

		if (patient.getOrganization() != org)
			patient.setOrganization(org);

		if (origPatient.getDevelopmentalDisorders().isPresent())
			patient.setDevelopmentalDisorders(origPatient
					.getDevelopmentalDisorders().get());

		if (origPatient.getTraumaticBrainInjury().isPresent())
			patient.setTraumaticBrainInjury(origPatient
					.getTraumaticBrainInjury().get());

		if (origPatient.getFamilyHistory().isPresent())
			patient.setFamilyHistory(origPatient.getFamilyHistory().get());

		if (origPatient.getAgeOfOnset().isPresent())
			patient.setAgeOfOnset(origPatient.getAgeOfOnset().get());

		if (origPatient.getDevelopmentalDelay().isPresent())
			patient.setDevelopmentalDelay(origPatient.getDevelopmentalDelay()
					.get());

		HospitalAdmission admit = new HospitalAdmission();
		admit.setAgeAtAdmission(ageAtAdmission);

		// Only update
		if (study.getParent() != null) {
			admit = study.getParent();
			patient = admit.getParent();

			if (!admit.getStudies().contains(study)) {
				admit.addStudy(study);
			}
		} else {
			patientDAO.saveOrUpdate(patient);

			patient.addAdmission(admit);
			admit.addStudy(study);
		}
		patientDAO.saveOrUpdate(patient);
		studyDAO.saveOrUpdate(study);
	}

	private String guessContactGroupPrefix(String label) {
		checkNotNull(label);
		final StringBuilder sb = new StringBuilder();
		int i = 0;
		while (i < label.length() && !Character.isDigit(label.charAt(i))) {
			sb.append(label.charAt(i));
			i++;
		}
		final String guess = sb.toString().trim();
		return guess.isEmpty() ? "default" : guess;
	}

	@Override
	public List<EEGMontage> getMontages(User user, String dataSnapshotId) {
		/*
		 * Return montages for User if snapshotId is null. Return montages for
		 * user and snapshot if snapshotId is String.
		 */

		final String m = "getMontages(User)";

		UserEntity btUser;
		if (!(user == null)) {
			btUser = userDAO.getOrCreateUser(user.getUserId());
		} else {
			btUser = null;
		}

		List<EegMontageEntity> montages = null;

		if (dataSnapshotId == null)
		{
			montages = ieegMontageDAO.findForUser(btUser);
		} else {
			DataSnapshotEntity dataSnapshot = dataSnapshotDAO
					.findByNaturalId(dataSnapshotId);
			montages = ieegMontageDAO.findForUserAndSnapshot(btUser,
					dataSnapshot);
		}

		final List<EEGMontage> montageSet = newArrayList();
		for (final EegMontageEntity montage : montages) {
			// No need to check for read permission - we only retrieve what can
			// be read

			EEGMontage newMontage = new EEGMontage(montage.getName());
			List<EEGMontagePair> mPairs = newMontage.getPairs();

			for (EegMontagePairEntity p : montage.getPairs()) {
				mPairs.add(new EEGMontagePair(p.getEl1(), p.getEl2()));
			}

			newMontage.setServerId(montage.getId());

			Boolean setPublic = montage.getIsPublic()
					|| montage.getIsPublic() == null;

			newMontage.setIsPublic(setPublic);

			if (btUser != null && montage.getUser() != null
					&& montage.getUser().equals(btUser))
				newMontage.setOwnedByUser(true);

			montageSet.add(newMontage);
		}
		String userName = "";
		if (!(user == null))
			userName = user.getUsername();

		logger.info("{}: Returning {} montages to user {}.", new Object[] { m,
				montageSet.size(), userName });
		return montageSet;
	}

	@Override
	public String getParentSnapshot(User authenticatedUser, String studyRevId) {
		final String m = "getParentSnapshot(...)";
		final String parent =
				analyzedDsDAO.findBaseDataset(studyRevId);

		if (parent != null && !parent.isEmpty())
			extAuthzHandler.checkPermitted(
					getPermissions(
							authenticatedUser,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							parent, false),
					CorePermDefs.READ,
					parent);

		return parent;
	}

	@Override
	public DataSnapshotSearchResult getDataSnapshotForId(User user,
			String snapshotID) {
		final long inTime = new Date().getTime();
		final String m = "getDataSnapshots(...)";

		UserEntity userEntity = userDAO.getOrCreateUser(user.getUserId());

		final DataSnapshotEntity study =
				dataSnapshotDAO.findByNaturalId(snapshotID);

		// /////////
		boolean ok = false;
		ExtPermissions permissions =
				getPermissions(
						user,
						HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME,
						snapshotID, false);
		if (permissions.getUserAce().isPresent() && permissions
				.getUserAce()
				.get()
				.getPerms()
				.contains(CorePermDefs.OWNER_PERM)) {
			ok = true;
		}

		DataSnapshotSearchResult dsSearchResult =
				DsSearchResultAssembler.assembleDsSearchResult(
						study,
						0,
						0);

		return dsSearchResult;
	}
	
	private Optional<Recording> getRecording(DataSnapshotEntity dataset) {
		Optional<Recording> recordingOpt = Optional.absent();

		if (HibernateUtil.isInstanceOf(
				dataset,
				EegStudy.class)) {
			EegStudy study = studyDAO.load(dataset.getId());
			recordingOpt = Optional.of(study.getRecording());
		} else if (HibernateUtil.isInstanceOf(
				dataset,
				ExperimentEntity.class)) {
			ExperimentEntity experiment = experimentDAO.load(dataset.getId());
			recordingOpt = Optional.of(experiment.getRecording());
		}
		return recordingOpt;
	}

}
