/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtTrace;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class TraceAssembler {

	public GwtTrace buildDto(final Contact contact) {
		checkNotNull(contact);
		final GwtTrace gwtTrace = new GwtTrace(
				contact.getContactType(),
				contact.getTrace().getLabel(),
				contact.getTrace().getFileKey(),
				contact.getId(),
				contact.getVersion(),
				contact.getTrace().getId(),
				contact.getTrace().getVersion());
		return gwtTrace;
	}

	public Contact buildEntity(final GwtTrace gwtTrace) {
		checkNotNull(gwtTrace);
		checkArgument(gwtTrace.getId() == null);
		checkArgument(gwtTrace.getVersion() == null);
		checkArgument(gwtTrace.getContactId() == null);
		checkArgument(gwtTrace.getContactVersion() == null);
		final TimeSeriesEntity trace = new TimeSeriesEntity(
				gwtTrace.getLabel(),
				gwtTrace.getFileKey());
		// Not using full constructor because we don't have coordinates
		final Contact contact = new Contact();
		contact.setContactType(gwtTrace.getContactType());
		contact.setTrace(trace);
		return contact;
	}

	public void modifyEntity(final Contact contact,
			final GwtTrace gwtTrace) throws StaleObjectException {
		final Long gwtContactId = gwtTrace.getContactId();
		final Long dbContactId = contact.getId();
		if (!gwtContactId.equals(dbContactId)) {
			throw new IllegalArgumentException("DTO contact id: ["
					+ gwtContactId
					+ "] does not match entity id: [" + dbContactId + "].");

		}
		final Long gwtTraceId = gwtTrace.getId();
		final Long dbTraceId = contact.getTrace().getId();
		if (!gwtTraceId.equals(dbTraceId)) {
			throw new IllegalArgumentException("DTO trace id: [" + gwtTraceId
					+ "] does not match entity id: [" + dbTraceId + "].");

		}
		final Integer gwtContactVer = gwtTrace.getContactVersion();
		final Integer dbContactVer = contact.getVersion();
		if (!gwtContactVer.equals(dbContactVer)) {
			throw new StaleObjectException(
					"Version mismatch for contact ["
							+ dbContactId + "]. DTO version: [" + gwtContactVer
							+ "], DB version: [" + dbContactVer + "].");
		}
		final Integer gwtTraceVer = gwtTrace.getVersion();
		final Integer dbTraceVer = contact.getTrace().getVersion();
		if (!gwtTraceVer.equals(dbTraceVer)) {
			throw new StaleObjectException(
					"Version mismatch for trace ["
							+ dbTraceId + "]. DTO version: [" + gwtTraceVer
							+ "], DB version: [" + dbTraceVer + "].");
		}
		copyToEntity(contact, gwtTrace);
	}

	private void copyToEntity(final Contact contact,
			final GwtTrace gwtTrace) {
		final TimeSeriesEntity trace = contact.getTrace();
		trace.setFileKey(gwtTrace.getFileKey());
		trace.setLabel(gwtTrace.getLabel());
		contact.setContactType(gwtTrace.getContactType());
	}

}
