/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

/**
 * A task for a tool.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public class TaskDto implements IHasName, Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private TaskStatus status;
	private String worker;
	private long startTimeMicros;
	private long endTimeMicros;
	private Long runStartTimeMillis;
	private Set<String> timeSeriesIds = newHashSet();

	// For GWT
	@SuppressWarnings("unused")
	private TaskDto() {}

	public TaskDto(
			String name,
			TaskStatus status,
			long startTimeMicrosec,
			long endTimeMicrosec,
			@Nullable Long runStartTime,
			@Nullable String worker) {
		this.name = checkNotNull(name);
		this.setStatus(checkNotNull(status));
		this.startTimeMicros = startTimeMicrosec;
		this.endTimeMicros = endTimeMicrosec;
		this.runStartTimeMillis = runStartTime;
		this.worker = worker;
	}

	/**
	 * Returns the end time of the window in the recording of this task.
	 * Measured in microseconds from the start of the recording.
	 * 
	 * @return the end time of the window in the recording of this task.
	 */
	public long getEndTimeMicros() {
		return endTimeMicros;
	}

	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * Returns the time at which this task started running in milliseconds since
	 * the Epoch
	 * 
	 * @return the time at which this task started running in milliseconds since
	 *         the Epoch
	 */
	public Long getRunStartTimeMillis() {
		return runStartTimeMillis;
	}

	/**
	 * Returns the start time of the window in the recording of this task.
	 * Measured in microseconds from the start of the recording.
	 * 
	 * @return the start time of the window in the recording of this task.
	 */
	public long getStartTimeMicros() {
		return startTimeMicros;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public Set<String> getTimeSeriesIds() {
		return timeSeriesIds;
	}

	/**
	 * The host name of the machine that the task ran on.
	 * 
	 * @return the worker
	 */
	public String getWorker() {
		return worker;
	}

	public void setEndTimeMicros(long endTimeMicrosec) {
		this.endTimeMicros = endTimeMicrosec;
	}

	public void setName(String name) {
		this.name = checkNotNull(name);
	}

	public void setRunStartTimeMillis(@Nullable Long runStartTimeMs) {
		this.runStartTimeMillis = runStartTimeMs;
	}

	public void setStartTimeMicros(long taskStartTimeMicrosec) {
		this.startTimeMicros = taskStartTimeMicrosec;
	}

	public void setStatus(TaskStatus status) {
		this.status = checkNotNull(status);
	}

	public void setWorker(@Nullable String worker) {
		this.worker = worker;
	}

}
