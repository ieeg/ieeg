/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.exception;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.IeegException;

@GwtCompatible(serializable = true)
public class ProjectNotFoundException extends IeegException {

	private static final long serialVersionUID = 1L;
	private String projectId;

	public ProjectNotFoundException(String projectId) {
		super("could not find project with id [" + projectId + "]");
		this.projectId = checkNotNull(projectId);
	}

	public ProjectNotFoundException(String projectId, String msg) {
		super(msg);
		this.projectId = checkNotNull(projectId);
	}

	public String getProjectId() {
		return projectId;
	}
}
