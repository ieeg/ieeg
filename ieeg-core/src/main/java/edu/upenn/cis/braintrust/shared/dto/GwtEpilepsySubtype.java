/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.EpilepsyType;
import edu.upenn.cis.braintrust.shared.Etiology;
import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * 
 * 
 * @author John Frommeyer
 * 
 */
@GwtCompatible(serializable = true)
public class GwtEpilepsySubtype implements IHasLongId, IHasVersion,
		Serializable {

	private static final long serialVersionUID = 1L;
	public static final Function<GwtEpilepsySubtype, EpilepsyType> getEpilepsyType = new Function<GwtEpilepsySubtype, EpilepsyType>() {

		@Override
		@Nullable
		public EpilepsyType apply(GwtEpilepsySubtype input) {
			return input.getEpilepsyType();
		}
	};
	public static final Function<GwtEpilepsySubtype, Etiology> getEtiology = new Function<GwtEpilepsySubtype, Etiology>() {

		@Override
		@Nullable
		public Etiology apply(GwtEpilepsySubtype input) {
			return input.getEtiology();
		}
	};
	private EpilepsyType epilepsyType;
	private Etiology etiology;
	private Long id;
	private Integer version;
	private boolean isDeleted = false;

	// For GWT
	@SuppressWarnings("unused")
	private GwtEpilepsySubtype() {}

	public GwtEpilepsySubtype(
			EpilepsyType epilepsyType,
			Etiology etiology,
			@Nullable Long id,
			@Nullable Integer version) {
		this.epilepsyType = checkNotNull(epilepsyType);
		this.etiology = checkNotNull(etiology);
		this.id = id;
		this.version = version;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public Long getId() {
		return id;
	}

	public EpilepsyType getEpilepsyType() {
		return epilepsyType;
	}

	public Etiology getEtiology() {
		return etiology;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
