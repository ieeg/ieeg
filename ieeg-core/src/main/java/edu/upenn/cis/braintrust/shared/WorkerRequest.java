package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.Date;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

@GwtCompatible(serializable=true)
public class WorkerRequest implements Serializable, JsonTyped {
	String channel;
	String source;
	String id;
	String op;
	String type;
	String key;
	String path;
	String token;
	String user;
	Date date;
	boolean isContainer;
	
	public final static String ETL = "etl";
	public final static String SYNC = "sync";
	public final static String PORTAL = "portal";
	
	public final static String LIST = "list"; 
	public final static String SYNCHRONIZE = "sync"; 
	public final static String INDEX = "index"; 
	public final static String DISCOVER = "discover"; 
	
	public WorkerRequest() {}

	public WorkerRequest(String channel, String source, String id, String op,
			String type, String key, String path, String token, String user, boolean isContainer) {
		super();
		this.channel = channel;
		this.source = source;
		this.id = id;
		this.op = op;
		this.type = type;
		this.path = path;
		this.token = token;
		this.key = key;
		this.user = user;
		this.isContainer = isContainer;
		this.date = new Date();
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isContainer() {
		return isContainer;
	}

	public void setContainer(boolean isContainer) {
		this.isContainer = isContainer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "WorkerRequest{" +
				"channel='" + channel + '\'' +
				", source='" + source + '\'' +
				", id='" + id + '\'' +
				", op='" + op + '\'' +
				", type='" + type + '\'' +
				", key='" + key + '\'' +
				", path='" + path + '\'' +
				", token='" + token + '\'' +
				", user='" + user + '\'' +
				", date=" + date +
				", isContainer=" + isContainer +
				'}';
	}
}
