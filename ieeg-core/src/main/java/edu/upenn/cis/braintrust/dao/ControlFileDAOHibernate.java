/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.ControlFileEntity;

/**
 * @author John Frommeyer
 *
 */
public class ControlFileDAOHibernate extends
		GenericHibernateDAOLongId<ControlFileEntity> implements
		IControlFileDAO {
	public ControlFileDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public ControlFileEntity findByBucketFileKey(
			String bucket,
			String fileKey) {
		return (ControlFileEntity) getSession()
				.getNamedQuery(ControlFileEntity.BY_BUCKET_FILE_KEY)
				.setParameter("bucket", bucket)
				.setParameter("fileKey", fileKey)
				.uniqueResult();
	}

	@Override
	public List<ControlFileEntity> findOrderedByCreateTimeAndId(
			int maxResults) {
		checkArgument(maxResults > 0);
		@SuppressWarnings("unchecked")
		final List<ControlFileEntity> result =
				getSession()
						.getNamedQuery(
								ControlFileEntity.ORDERED_BY_CREATE_TIME_ID)
						.setLockOptions(LockOptions.UPGRADE)
						.setMaxResults(maxResults).list();
		return result;
	}

	@Override
	public List<ControlFileEntity> findByMetadataOrderedByCreateTimeAndId(
			String metadata, int maxResults) {
		checkNotNull(metadata);
		checkArgument(maxResults > 0);
		@SuppressWarnings("unchecked")
		final List<ControlFileEntity> result =
				getSession()
						.getNamedQuery(
								ControlFileEntity.BY_METADATA_ORDERED_BY_CREATE_TIME_ID)
						.setParameter("metadata", metadata)
						.setLockOptions(LockOptions.UPGRADE)
						.setMaxResults(maxResults).list();
		return result;

	}
}
