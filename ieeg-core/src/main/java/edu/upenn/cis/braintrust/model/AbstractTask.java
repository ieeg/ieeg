/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.IHasName;
import edu.upenn.cis.braintrust.shared.TaskStatus;

/**
 * A task for a tool.
 * 
 * @author John Frommeyer
 */
@MappedSuperclass
@org.hibernate.annotations.Check(
		constraints = AbstractTask.START_TIME_MICROS_COLUMN
				+ " < " + AbstractTask.END_TIME_MICROS_COLUMN)
public abstract class AbstractTask<P extends AbstractJob>
		implements IHasLongId, IHasName {

	public static final String START_TIME_MICROS_COLUMN = "start_time_micros";
	public static final String END_TIME_MICROS_COLUMN = "end_time_micros";
	private Long id;
	private P parent;
	private String name;
	private String worker;
	private Long startTimeMicros;
	private Long endTimeMicros;
	private Date runStartTime;
	private Set<String> tsPubIds = newHashSet();
	private TaskStatus status;

	protected AbstractTask() {}

	protected AbstractTask(
			P parent,
			String pubId,
			TaskStatus status,
			Long startTimeMicrosec,
			Long endTimeMicrosec,
			@Nullable String worker,
			@Nullable Date runStartTime) {
		this.parent = checkNotNull(parent);
		this.name = checkNotNull(pubId);
		this.status = checkNotNull(status);
		this.startTimeMicros = checkNotNull(startTimeMicrosec);
		this.endTimeMicros = checkNotNull(endTimeMicrosec);
		this.worker = worker;
		this.runStartTime = runStartTime;
	}

	protected AbstractTask(
			P parent,
			String pubId,
			TaskStatus status,
			Long startTimeMicrosec,
			Long endTimeMicrosec,
			Set<String> tsPubIds,
			@Nullable String worker,
			@Nullable Date runStartTime) {
		this.parent = checkNotNull(parent);
		this.name = checkNotNull(pubId);
		this.status = checkNotNull(status);
		this.startTimeMicros = checkNotNull(startTimeMicrosec);
		this.endTimeMicros = checkNotNull(endTimeMicrosec);
		this.tsPubIds.addAll(tsPubIds);
		this.worker = worker;
		this.runStartTime = runStartTime;
	}

	/**
	 * Returns the end time of the window in the recording of this task.
	 * Measured in microseconds from the start of the recording.
	 * 
	 * @return the end time of the window in the recording of this task.
	 */
	@NotNull
	@Column(name = END_TIME_MICROS_COLUMN)
	public Long getEndTimeMicros() {
		return endTimeMicros;
	}

	@Override
	@Transient
	public Long getId() {
		return id;
	}

	/**
	 * Get the name.
	 * <p>
	 * Unique relative to its parent.
	 * 
	 * @return the name.
	 */
	@Override
	@NotNull
	public String getName() {
		return this.name;
	}

	@Transient
	public P getParent() {
		return parent;
	}

	/**
	 * 
	 * Returns the time this task started running
	 * 
	 * @return the time this task started running
	 */
	@Column
	public Date getRunStartTime() {
		return runStartTime;
	}

	/**
	 * Returns the start time of the window in the recording of this task.
	 * Measured in microseconds from the start of the recording.
	 * 
	 * @return the start time of the window in the recording of this task.
	 */
	@NotNull
	@Column(name = START_TIME_MICROS_COLUMN)
	public Long getStartTimeMicros() {
		return startTimeMicros;
	}

	@NotNull
	public TaskStatus getStatus() {
		return status;
	}

	@Transient
	@Nonnull
	public Set<String> getTsPubIds() {
		return tsPubIds;
	}

	/**
	 * The host name of the machine that the task ran on.
	 * 
	 * @return the worker
	 */
	@Column
	public String getWorker() {
		return worker;
	}

	public void setEndTimeMicros(Long endTimeMicrosec) {
		this.endTimeMicros = endTimeMicrosec;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public void setName(String pubId) {
		this.name = pubId;
	}

	public void setParent(P parent) {
		this.parent = parent;
	}

	public void setRunStartTime(@Nullable Date started) {
		this.runStartTime = started;
	}

	public void setStartTimeMicros(Long taskStartTimeMicrosec) {
		this.startTimeMicros = taskStartTimeMicrosec;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	@SuppressWarnings("unused")
	private void setTsPubIds(Set<String> tsPubIds) {
		this.tsPubIds = tsPubIds;
	}

	public void setWorker(@Nullable String worker) {
		this.worker = worker;
	}

}
