/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.model.ProjectDiscussion;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IProjectDAO extends IDAO<ProjectEntity, Long> {

	public List<ProjectEntity> getProjectsForUser(final UserEntity user);

	public List<ProjectDiscussion> findDiscussionByProjectID(
			ProjectEntity entity, int start,
			int count);

	public ProjectDiscussion getDiscussionByItsID(String discussionID);

	public void saveOrUpdateDiscussion(final ProjectDiscussion entity);

	Set<EegProject> getEegProjectsForUser(UserEntity user);

	List<String> findIdsByName(UserEntity user, String projectName);

}
