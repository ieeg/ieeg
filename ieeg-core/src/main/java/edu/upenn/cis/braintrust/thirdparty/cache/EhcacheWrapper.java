/*
 * From http://ehcache.org/documentation/recipes/wrapper
 */
package edu.upenn.cis.braintrust.thirdparty.cache;

import static com.google.common.base.Preconditions.checkNotNull;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

/**
 * @deprecated just pass in the caches through the constructor
 * 
 * @param <K>
 * @param <V>
 */
@Deprecated
public class EhcacheWrapper<K, V> implements CacheWrapper<K, V>
{
	private final String cacheName;
	private final CacheManager cacheManager;

	public EhcacheWrapper(
			String cacheName,
			CacheManager cacheManager) {
		this.cacheName = checkNotNull(cacheName);
		this.cacheManager = checkNotNull(cacheManager);
	}

	public void put(final K key, final V value)
	{
		if (getCache() != null)
			getCache().put(new Element(key, value));
	}

	@SuppressWarnings("unchecked")
	public V get(final K key)
	{
		if (getCache() == null)
			return null;

		Element element = getCache().get(key);
		if (element != null) {
			return (V) element.getObjectValue();
		}
		return null;
	}

	public Ehcache getCache()
	{
		if (cacheManager == null)
			return null;
		else
			return cacheManager.getEhcache(cacheName);
	}
}
