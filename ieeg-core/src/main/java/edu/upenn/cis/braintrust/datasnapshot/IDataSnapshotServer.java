/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.ExtPermissions;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationCube;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.DatasetIdAndVersion;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.StaleDataException;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.exception.AclTargetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetConflictException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundByDatasetAndIdException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.RecordingObject;

public interface IDataSnapshotServer {

	/**
	 * Returns the revision id of the revised dataset.
	 * <p>
	 * Adds the time series with rev ids {@code timeSeriesRevIds} to the data
	 * snapshot with rev id {@code datasetRevId}.
	 * </p>
	 * 
	 * @param user the user performing the modification
	 * @param datasetRevId a non-study revision id
	 * @param timeSeriesRevIds the time series to add
	 * @return
	 * 
	 * @throws AuthorizationException if {@code user} is not authorized to
	 *             modify the given data snapshot
	 * @throws IllegalArgumentException if datasetRevId is a study rev id, if
	 *             there is no dataset with that rev id, or if some time series
	 *             rev id is not found in the database.
	 * 
	 */
	String addTimeSeriesToDataset(
			User user,
			String datasetRevId,
			Set<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException;

	/**
	 * Derived a new dataset into the user's default protection group.
	 * 
	 * @param user user doing the deriving
	 * @param sourceRevId the rev id of the data snapshot being derived
	 * @param derivedDsLabel the label to use in the derived dataset
	 * @param tsRevIdInclusions which time series to include, {@code null} means
	 *            include everything
	 * @param tsAnnoRevIdInclusions which time series annotations to copy,
	 *            {@code null} means copy everything
	 * 
	 * @return the rev id of the derived dataset
	 * 
	 * @throws IllegalArgumentException if there is no source data snapshot with
	 *             the given rev id
	 * @throws IllegalArgumenException if {@code tsRevIdInclusions} has invalid
	 *             rev ids
	 * @throws IllegalArgumentException if {@code tsAnnoRevIdInclusions} has any
	 *             annotations that are not on the included time series
	 * @throws AuthorizationException if the user is not allowed to read the
	 *             source data snapshot
	 * @throws DuplicateNameException
	 */
	String deriveDataset(
			User user,
			String sourceRevId,
			String derivedDsLabel,
			String toolLabel,
			@Nullable Set<String> tsRevIdInclusions,
			@Nullable Set<String> tsAnnoRevIdInclusions)
			throws DuplicateNameException;

	/**
	 * Returns an {@code EditAclResponse} for each action taken. The returned
	 * list may be shorter than {@code actions} if the implementation can obtain
	 * the same results in fewer actions. If the action failed the response will
	 * contain an error message suitable for the front end.
	 * 
	 * @param user
	 * @param entityType the entity type of the target of the acl
	 * @param mode the mode of the permissions to be edited.
	 * @param actions
	 * @return
	 * @throws AuthorizationException if {@code user} does not have permission
	 *             to edit an ACL given in an action.
	 */
	List<EditAclResponse> editAcl(
			User user,
			HasAclType entityType,
			String mode,
			List<IEditAclAction<?>> actions) throws AuthorizationException;

	/**
	 * Returns a {@link GetAcesResponse} with the ACL information for the target
	 * with id {@code targetId}. Only permissions with mode {@code mode} will be
	 * returned.
	 * 
	 * @param user
	 * @param entityType the domain of the target
	 * @param mode
	 * @param targetId
	 * @return a {@link GetAcesResponse} with the ACL information for the target
	 *         with id {@code targetId}
	 * @throws AuthorizationException if {@code user} does not have CORE read
	 *             permission on the target.
	 */
	GetAcesResponse getAcesResponse(
			User user,
			HasAclType entityType,
			String mode,
			String targetId);

	/**
	 * 
	 * @param user
	 * @param dsRevId
	 * 
	 * @return the data snapshot's clob
	 * 
	 * @throws AuthorizationException if the user is not authorized to read the
	 *             data snapshot
	 * @throws DataSnapshotNotFoundException if there is not such data snapshot
	 */
	String getClob(final User user, final String dsRevId)
			throws AuthorizationException;

	/**
	 * 
	 * @param user
	 * @param dsRevId
	 * @param postIndex (0 by default) -- the index position to start with
	 * @param numPosts -- maximum number of posts (-1 = infinite)
	 * 
	 * @return the data snapshot's postings in sequential order, starting at the
	 *         index position
	 * 
	 * @throws AuthorizationException if the user is not authorized to read the
	 *             data snapshot
	 * @throws DataSnapshotNotFoundException if there is not such data snapshot
	 */
	List<Post> getPostings(final User user, final String dsRevId,
			int postIndex, int numPosts) throws AuthorizationException;

	public String getDatasetPortion(String dataSnapshotRevId);

	/**
	 * Returns the {@code DataSnapshot} for the given dataSnapshot revision id.
	 * 
	 * @param user the user requesting the data snapshot
	 * @param dataSnapshotRevId the revision id of the desired data snapshot.
	 * 
	 * @return the {@code DataSnapshot} for the given data snapshot revision id.
	 * 
	 * @throws AuthorizationException if {@code user} does not have read access
	 *             to the data snapshot.
	 * @throws DataSnapshotNotFoundException if no {@code DataSnapshot} is found
	 *             with the given revision id.
	 */
	DataSnapshot getDataSnapshot(
			User user,
			String dataSnapshotRevId) throws AuthorizationException;

	/**
	 * Search over datasets with the criteria given in {@code dsSearch}.
	 * <p>
	 * Only return objects the given user is able to see.
	 * 
	 * @param user user making the search.
	 * @param dataSnapshotSearch
	 * 
	 * @return matching results
	 * @throws AuthorizationException
	 */
	Set<DataSnapshotSearchResult> getDataSnapshots(
			User user,
			EegStudySearch dataSnapshotSearch)
			throws AuthorizationException;

	EegStudyMetadata getEegStudyMetadata(User user, String studyRevId)
			throws AuthorizationException;

	/**
	 * Get the data snapshots search results for the given ids, or null if there
	 * is no such snapshot.
	 * 
	 * @param user the user
	 * @param dsRevIds the ids
	 * 
	 * @return the latest data snapshots search results for the given ids, or
	 *         null if there is no such snapshot.
	 * 
	 * @throws AuthorizationException if the user can't read the snapshot
	 */
	List<DataSnapshotSearchResult> getLatestSnapshots(
			User user,
			Iterable<String> dsRevIds)
			throws AuthorizationException;

	/**
	 * Get the search results for the given project id, only includes datasets
	 * that are readable by the user. If the project contains datasets that
	 * don't exist, they are not returned.
	 * 
	 * @param user user making the request
	 * @param datasetIds id's of the datasets we want
	 * 
	 * @return the search results for the given project id, only includes
	 *         datasets that are readable by the user. If the project contains
	 *         datasets that don't exist, they are not returned.
	 */
	Set<DataSnapshotSearchResult>
			getSearchableDatasetsThatExist(
					User user,
					String projectId);

	/**
	 * Returns the paths of the mef files with the given trace rev ids. Returns
	 * them in the order they are given.
	 * 
	 * @param user the user requesting the mef paths
	 * @param dataSnapshotRevId the revision id of a data snapshot containing
	 *            the time series
	 * @param traceIds the ids of of the desired time series
	 * 
	 * @return the paths of the mef files with the given trace rev ids
	 * 
	 * @throws AuthorizationException if {@code user} does not have read access
	 *             to the data snapshot with revision id
	 *             {@code dataSnapshotRevId}
	 */
	List<String> getMEFPaths(
			User user,
			String dataSnapshotRevId,
			Iterable<String> traceIds)
			throws AuthorizationException;

	public Set<DatasetAndTool> getRelatedAnalyses(
			User user,
			String studyRevId) throws AuthorizationException;

	Set<ToolDto> getTools(User user);

	String getUserClob(User user);

	boolean isFrozen(User user, String datasetId)
			throws AuthorizationException;

	/**
	 * 
	 * Returns the revision id of the revised data snapshot.
	 * <p>
	 * Removes the time series with rev ids {@code timeSeriesRevIds} from the
	 * data snapshot with rev id {@code dataSnapshotRevId}.
	 * </p>
	 * <p>
	 * The removal of time series from studies is not supported.
	 * </p>
	 * 
	 * @param user the user performing the modification
	 * @param dataSnapshotRevId a non-study revision id
	 * @param timeSeriesRevIds the time series to remove
	 * @return the revision id of the revised data snapshot
	 * @throws AuthorizationException if {@code user} is not authorized to
	 *             modify the given data snapshot
	 * @throws DatasetNotFoundException TODO
	 * @throws IllegalArgumentException if dataSnapshotRevId is a study rev id,
	 *             if there is no dataset with that rev id, or if some time
	 *             series rev id is not found in the data snapshot
	 */
	String removeTimeSeriesFromDataset(final User user,
			final String dataSnapshotRevId,
			final Set<String> timeSeriesRevIds) throws AuthorizationException,
			DatasetNotFoundException;

	void removeTools(User user, Set<String> revIds)
			throws AuthorizationException;

	/**
	 * Returns the revision id of the revised data snapshot.
	 * <p>
	 * For each {@code tsAnnotationRevId} in {@code tsAnnotationRevIds}, removes
	 * the annotation with revision id {@code tsAnnotationRevId} from the data
	 * snapshot with rev id {@code dataSnapshotRevId}.
	 * </p>
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @param tsAnnotationRevIds
	 * @return the revision id of the revised data snapshot
	 * @throws AuthorizationException if user does not have modify access to the
	 *             data snapshot with revision id {@code dataSnapshotRevId}
	 * @throws IllegalArgumentException if no data snapshot with the given rev
	 *             id exists
	 * @throws IllegalArgumentException if some {@code tsAnnotationRevId} is not
	 *             found in the database
	 */
	String removeTsAnnotations(
			User user,
			String dataSnapshotRevId,
			Set<String> tsAnnotationRevIds)
			throws AuthorizationException;

	/**
	 * 
	 * @param user
	 * @param dsId
	 * @param frozen
	 * @throws AuthorizationException
	 * @throws DataSnapshotNotFoundException if there's no such data snapshot
	 */
	void setFrozen(User user, String dsId, boolean frozen)
			throws AuthorizationException;

	/**
	 * This will wipe out any previously existing clob associated with the data
	 * snapshot.
	 * 
	 * @param user user making the request
	 * @param dataSnapshotRevId to which we're attaching the clob
	 * @param clob being attached to the data snapshot
	 * 
	 * @return the revision id of the revised data snapshot
	 * 
	 * @throws IllegalArgumentException if there is no data snapshot with the
	 *             given rev id
	 * @throws AuthorizationException if the user is not allowed to write to the
	 *             data snapshot
	 */
	String storeClob(
			User user,
			String dataSnapshotRevId,
			String clob) throws AuthorizationException;

	List<String> storeTools(User user, Iterable<ToolDto> tool)
			throws AuthorizationException;

	DataSnapshotIds storeTsAnnotations(
			User user,
			String dataSnapshotRevId,
			Iterable<? extends TsAnnotationDto> tsAnnDtos)
			throws
			AuthorizationException,
			TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException;

	void storeUserClob(User user, String clob);

	void createJobForUser(UserId userId, String datasetId, String toolId,
			JobStatus status, ParallelDto parallelism, Date createTime);

	void initializeJob(String datasetId, Set<TaskDto> tasks);

	void updateTaskStatus(User user, String datasetId,
			String taskId, TaskStatus newStatus);

	void updateTasks(User user, String datasetId, Set<TaskDto> taskSet);

	List<UserId> getUserIdsWithActiveJobs();

	/**
	 * 
	 * Returns a list of {@code ToolDto}s corresponding to the ids in
	 * {@code toolIds}. The dtos are in the same order as the iterable returns
	 * the ids. If a tool with {@code toolId} is not found, or if {@code user}
	 * is not authorized to read it, then null is returned in corresponding
	 * position.
	 * 
	 * @param user
	 * @param toolId
	 * @return a list of {@code ToolDto}s
	 */
	List<ToolDto> getTools(User user, Iterable<String> toolIds);

	Set<DataSnapshotSearchResult> getExperiments(User user,
			ExperimentSearch experimentSearch);

	ExperimentMetadata getExperimentMetadata(User user, String experimentId)
			throws AuthorizationException;

	ExperimentSearchCriteria getExperimentSearchCriteria();

	Set<Image> getImages(User user, String dsId)
			throws AuthorizationException;

	Map<String, TimeSeriesDto> getTimeSeries(User user, String datasetId)
			throws AuthorizationException;

	Long addPosting(
			final User user,
			final String dataSnapshotRevId,
			final Post post)
			throws AuthorizationException;

	void createUserIfNecessary(User user);

	/**
	 * Logs an activity
	 * 
	 * @param user
	 * @param log
	 * @return
	 * @throws AuthorizationException
	 */
	Long addLogEntry(
			User user,
			LogMessage log)
			throws AuthorizationException;

	List<LogMessage> getLogEntries(
			User user,
			int startIndex,
			int count)
			throws AuthorizationException;

	List<LogMessage> getLogEntriesSince(
			User user,
			Timestamp time)
			throws AuthorizationException;

	/**
	 * Retrieves the provenance entries affecting the user
	 * 
	 * @param user
	 * @return
	 * @throws AuthorizationException
	 */
	List<ProvenanceLogEntry> getProvenanceEntriesForUser(
			User user)
			throws AuthorizationException;

	/**
	 * Retrieves the provenance entries affecting the user
	 * 
	 * @param user
	 * @return
	 * @throws AuthorizationException
	 */
	List<ProvenanceLogEntry> getProvenanceEntriesByUser(
			User user)
			throws AuthorizationException;

	Integer getSnapshotsByUser(
			User user)
			throws AuthorizationException;

	Integer getAnnotationsByUser(
			User user)
			throws AuthorizationException;

	/**
	 * Retrieves the provenance entries corresponding to the snapshot
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @return
	 * @throws AuthorizationException
	 */
	List<ProvenanceLogEntry> getProvenanceEntriesForSnapshot(
			final User user, final String dataSnapshotRevId)
			throws AuthorizationException;

	/**
	 * Adds an entry describing data usage
	 * 
	 * @param user
	 * @param log
	 * @return
	 * @throws AuthorizationException
	 */
	Long addProvenanceEntry(
			final User user,
			final ProvenanceLogEntry log)
			throws AuthorizationException;

	EegProject addProject(
			final User admin,
			final EegProject project)
			throws AuthorizationException;

	void updateProject(
			final User admin,
			final String projectId,
			final IProjectUpdater updater)
			throws AuthorizationException;

	EegProject getProject(
			final User admin,
			final String id)
			throws AuthorizationException;

	Set<EegProject> getProjectsForUser(
			final User user)
			throws AuthorizationException;

	List<Post> getProjectDiscussions(User user,
			String projectId, int postIndex, int numPosts)
			throws AuthorizationException;

	String addProjectPosting(
			final User user,
			final String projectId,
			final Post post)
			throws AuthorizationException;

	/**
	 * Returns a token which identifies a newly created session with
	 * {@code user} as the owner.
	 * 
	 * @param user
	 * @return a token which identifies a newly created session with
	 *         {@code user} as the owner
	 */
	SessionToken createSession(User user);

	/**
	 * Returns a {@code UserIdSessionStatus} with the userId and session status
	 * of the session with the given token. If the session's date of last
	 * activity is after {@code minLastActive} then session's last activity date
	 * is updated. Otherwise, the status is updated to {@code EXPIRED}.
	 * 
	 * @param token
	 * @return the {@code UserId} of the user who owns the active session with
	 *         the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 */
	UserIdSessionStatus getUserIdSessionStatus(SessionToken token,
			Date minLastActive);

	/**
	 * Returns the number of old sessions which were deleted. Deletes sessions
	 * with date of last activity before {@code minLastActive}.
	 * 
	 * @return the number of old sessions which were deleted
	 */
	int cleanupOldSessions(Date minLastActive);

	/**
	 * Sets the {@link SessionStatus} of the given session to {@code status}.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	void setSessionStatus(SessionToken token, SessionStatus status);

	void createDataUsage(
			User user,
			DataUsageEntity dataUsage);

	UserEntity getUser(UserId userId);

	/**
	 * Returns the set of defined annotations and their schemas
	 * 
	 * @param user
	 * @param viewerType the viewer for whom we are looking up annotations
	 * @return
	 */
	public List<AnnotationDefinition> getAnnotationDefinitions(
			User user,
			String viewerType);

	/**
	 * Will be cached for unspecified amount of time. Good use case: logging
	 * with the label where you don't need up to the date information.
	 */
	DataSnapshotEntity getDataSnapshotFromCache(User user, String dsId);

	String getDataSnapshotId(User user, String dsName, boolean willUpdate)
			throws DataSnapshotNotFoundException;

	void doNothing();

	Map<String, Long> countLayers(User user, String dataSnapshotId);

	long countSnapshots();

	/**
	 * Returns the number of EegStudies which some non-admin can view.
	 * 
	 * @return the number of EegStudies which some non-admin can view
	 */
	long countUserReadableStudies();

	/**
	 * Returns the number of EegStudies with a read world ace.
	 * 
	 * @return the number of EegStudies with a read world ace
	 */
	long countWorldReadableStudies();

	/**
	 * Returns the number of Experiments which some non-admin can view.
	 * 
	 * @return the number of Experiments which some non-admin can view
	 */
	long countUserReadableExperiments();

	/**
	 * Returns the number of Experiments with a read world ace.
	 * 
	 * @return the number of Experiments with a read world ace
	 */
	long countWorldReadableExperiments();

	long countAnnotations();

	List<TsAnnotationDto> getTsAnnotations(
			User user,
			String dataSnapshotId,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults) throws TimeSeriesNotFoundByDatasetAndIdException;

	/**
	 * Returns a {@code ExtPermissions} for the given user and instance.
	 * 
	 * @param user the user
	 * @param entityType the entity type of the instance
	 * @param mode the mode the caller is interested in
	 * @param targetId the id of the target of the permissions
	 * @param useDatasetShortCache use a permissions cache, guaranteed to never
	 *            be more than 15 minutes old
	 * 
	 * @return a {@code ExtPermissions} for the given user and instance
	 * 
	 * @throws AclTargetNotFoundException if there is no {@code entity} instance
	 *             with natural id equal to {@code targetId}
	 */
	ExtPermissions getPermissions(
			User user,
			HasAclType entityType,
			String mode,
			String targetId,
			boolean useDatasetShortCache);

	void setDataSnapshotName(
			User user,
			String dataSnapshotId,
			String originalName,
			String newName)
			throws StaleDataException, DuplicateNameException;

	List<TsAnnotationDto> getTsAnnotationsLtStartTime(
			User user,
			String dataSnapshotId,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults);

	List<ProjectGroup> getProjectGroups(Set<ProjectGroup> excludedGroups);

	Set<AllowedAction> getPermittedActionsOnDataset(User user, String dsId);

	int removeTsAnnotationsByLayer(User user, String datasetId, String layer);

	Set<ImageForTrace> getImagesForTimeSeries(
			User user,
			String dataSnapshotId,
			String timeSeriesId);

	int moveTsAnnotations(
			User user,
			String datasetId,
			String name,
			String newName);

	List<ChannelInfoDto> writeChannelInfo(
			String snapshotId,
			List<ChannelInfoDto> info);

	List<ChannelInfoDto> getChannelInfo(String dataSnapshotId);

	// MontageDto createMontage(
	// User user,
	// MontageDto montageDto);
	//
	// Set<MontageInfoDto> getMontageInfos(User user, DataSnapshotId dsId);
	//
	// MontageDto getMontage(User user, MontageId montageId);
	//
	// void deleteMontage(
	// User user,
	// MontageIdAndVersion montageId,
	// boolean checkVersion) throws FailedVersionMatchException;
	//
	// LayerDto createLayer(
	// User user,
	// MontageId montageId,
	// String layerName);
	//
	// Set<LayerDto> getLayers(User user, MontageId montageId);
	//
	// void deleteLayer(
	// User user,
	// LayerId layerId);

	// Set<MontagedChAnnotationDto> getMontagedChAnnotations(
	// User user,
	// LayerId layerId,
	// long startOffsetUsecs,
	// int firstResult,
	// int maxResults);
	//
	// Set<MontagedChAnnotationDto> getMontagedChAnnotationsLtStartTime(
	// User user,
	// LayerId layerId,
	// long startOffsetUsecs,
	// int firstResult,
	// int maxResults);

	// LayerDto patchLayer(User user, LayerPatchDto layerPatch);

	void streamAnnotations(
			User user,
			DataSnapshotId datasetId,
			IAnnotationWriter annotationWriter);

	DataSnapshotId getRecordingIdForTimeSeries(
			User user,
			String timeSeriesId) throws TimeSeriesNotFoundException;

	/**
	 * Stores (replaces) a set of key/value pairs in the JSON store
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @param keyValues
	 * @return
	 * @throws AuthorizationException
	 * @throws JsonProcessingException
	 */
	String storeJsonKeyValues(User user, String dataSnapshotRevId,
			Collection<? extends IJsonKeyValue> keyValues)
			throws AuthorizationException,
			JsonProcessingException;

	/**
	 * Loads the set of key/value pairs from the JSON store
	 * 
	 * @param user
	 * @param dsRevId
	 * @return
	 * @throws AuthorizationException
	 * @throws IOException
	 */
	Set<IJsonKeyValue> getJsonKeyValues(User user, String dsRevId)
			throws AuthorizationException, IOException;

	/**
	 * Creates a recording object and writes corresponding input stream. It
	 * {@code test} is {@code true} then only authz is done. No object is
	 * created or written.
	 * 
	 * @param user
	 * @param recordingObject
	 * @param inputStream
	 * @param test
	 * @return
	 * @throws RecordingNotFoundException
	 * @throws DuplicateNameException
	 * @throws BadRecordingObjectNameException
	 * @throws BadDigestException
	 */
	RecordingObject createRecordingObject(
			User user,
			RecordingObject recordingObject,
			InputStream inputStream,
			boolean test) throws
			RecordingNotFoundException,
			DuplicateNameException,
			BadRecordingObjectNameException,
			BadDigestException;

	void addAsRecordingObject(
			User user,
			String key,
			String description,
			String mimeType,
			int size,
			String snapshotId) throws
			RecordingNotFoundException,
			DuplicateNameException;

	/**
	 * Returns the recording objects for the given dataset.
	 * 
	 * @param user
	 * @param datasetId
	 * @return
	 * @throws RecordingNotFoundException
	 */
	Set<RecordingObject> getRecordingObjects(
			User user,
			DataSnapshotId datasetId);
	
	RecordingObject getRecordingObject(User user, DataSnapshotId datasetId, String name);

	RecordingObjectContent getRecordingObjectContent(
			User user,
			RecordingObjectIdAndVersion idAndVersion,
			boolean checkVersion,
			@Nullable Long startByteOffset,
			@Nullable Long endByteOffset) throws
			FailedVersionMatchException,
			RecordingObjectNotFoundException,
			InvalidRangeException;

	void deleteRecordingObject(
			User user,
			RecordingObjectIdAndVersion idAndVersion,
			boolean checkVersion) throws
			FailedVersionMatchException,
			RecordingObjectNotFoundException;

	String addMEF(User user, String path, ChannelInfoDto channelInfo);

	String createDataSnapshot(User user, String containerName,
			IHasGwtRecording recInfo);

	String addTimeSeriesToDataSnapshot(
			User user,
			String datasetRevId,
			List<ChannelInfoDto> channelInfo,
			List<String> timeSeriesRevIds)
			throws AuthorizationException, DatasetNotFoundException;

	void setAdmissionInfo(
			User user,
			String snapshotId,
			GwtPatient origPatient,
			int ageAtAdmission)
			throws AuthorizationException, DatasetNotFoundException;

	ControlFileRegistration createControlFileRegistration(
			User user,
			ControlFileRegistration registration)
			throws AuthorizationException, DuplicateNameException;

	Set<String> getDataSnapshotNames(User user, User optUser)
			throws AuthorizationException;

	void setAnimal(
			User user,
			DataSnapshotId datasetId,
			GwtAnimal animal)
			throws AuthorizationException,
			DataSnapshotNotFoundException;

	Set<DataSnapshotSearchResult> getRelatedSearchResults(User user,
			String inputDatasetId);

	public abstract Map<String, Set<DataSnapshotSearchResult>> getSearchableDatasetsThatExist(
			User user,
			Set<String> projectIds);

	List<String> getProjectIdsByName(User user, String projectName)
			throws AuthorizationException;

	List<EEGMontage> getMontages(User theUser, String dataSnapshotID);

	EEGMontage createEditEegMontage(User user, EEGMontage montage,
			String datasetId) throws EegMontageNotFoundException;

//	Set<PresentableMetadata> getSearchMatches(User u, Set<String> terms,
//			int start, int max);

	void deleteEegMontage(
			User user,
			Long montageId)
			throws EegMontageNotFoundException;

	Set<DataSnapshotSearchResult> getDataSnapshotsForUser(User user);

	DataSnapshotSearchResult getDataSnapshotForId(User user, String dataSnapshotID);

	void deleteDataset(
			User user, 
			DatasetIdAndVersion datasetId, 
			boolean checkVersion,
			boolean deleteEmptySubject)
			throws FailedVersionMatchException, DatasetConflictException;

	Set<DataSnapshotSearchResult> getDataSnapshotsNoCounts(User user, EegStudySearch studySearch)
			throws AuthorizationException;

	List<AnnotationCube> getAnnotationCube(User user);

	String getParentShapshot(User authenticatedUser, String studyRevId);

	Set<DataSnapshotSearchResult> getExperimentsNoCounts(User user, ExperimentSearch experimentSearch);

	IStoredObjectReference getRecordingObjectHandle(User user, RecordingObject object)
			throws RecordingObjectNotFoundException;

	void deleteDatasetChannels(User user, DatasetIdAndVersion datasetId, Collection<INamedTimeSegment> channels)
			throws DatasetNotFoundException, DatasetConflictException;

	void deleteDatasetObjects(User user, DatasetIdAndVersion datasetId, Collection<RecordingObject> objects)
			throws FailedVersionMatchException, DatasetConflictException;

//	List<DataSnapshotSearchResult> getDataSnapshotsMatching(User user, List<String> searchTerms);
}
