/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;

/**
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
@Immutable
public final class Role implements Serializable {

	public static final Role ADMIN = new Role("admin");

	public static final Role USER = new Role("user");

	public static final Role DATA_ENTERER = new Role("data-enterer");

	public static final Role GUEST = new Role("portal-guest");

	public static final Function<Role, String> getName = new Function<Role, String>() {

		@Override
		public String apply(final Role input) {
			return input.getName();
		}
	};

	private final String name;

	public Role(String name) {
		this.name = checkNotNull(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Role)) {
			return false;
		}
		Role other = (Role) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Role [name=" + name + "]";
	}

}
