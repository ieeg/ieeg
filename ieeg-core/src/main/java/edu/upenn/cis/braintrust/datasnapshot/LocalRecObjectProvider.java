/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.hash.HashCode;
import com.google.common.io.ByteSink;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.model.RecordingObjectEntity;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;

/**
 * @author John Frommeyer
 *
 */
@ThreadSafe
public final class LocalRecObjectProvider implements IRecordingObjectProvider {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final String baseDir;

	public LocalRecObjectProvider(String baseDir) {
		this.baseDir = checkNotNull(baseDir);
	}

	@Override
	public IRecObjectUploadResult writeRecordingObject(
			InputStream inputStream,
			String fileKey,
			String md5Hash,
			@Nullable Long inputLength,
			@Nullable String contentType)
			throws BadDigestException,
			DuplicateNameException {
		final String m = "writeRecordingObject(...)";
		DigestInputStream digestInputStream = null;
		try {
			logger.debug(
					"{}: Uploading file to local file system.",
					m);
			final File objectFile = new File(
					baseDir,
					fileKey);
			if (objectFile.exists()) {
				throw new DuplicateNameException(
						objectFile.getName());
			}
			objectFile.getParentFile().mkdir();
			logger.debug(
					"{}: Object will be stored as [{}]",
					m,
					objectFile.getAbsolutePath());
			final MessageDigest digest = MessageDigest
					.getInstance("MD5");
			digestInputStream = new DigestInputStream(
					inputStream,
					digest);

			final ByteSink sink = Files.asByteSink(objectFile);
			sink.writeFrom(digestInputStream);
			final String md5HashAsRead = HashCode
					.fromBytes(digest.digest())
					.toString();
			logger.debug(
					"{}: Passed in MD5 hash: {}. MD5 hash of read bytes: {}",
					m,
					md5Hash,
					md5HashAsRead);
			if (!md5Hash.equals(md5HashAsRead)) {
				final boolean deleted = objectFile.delete();
				logger.info(
						"{}: MD5 hashes for upload of {} do not match. Result of attempt to delete file: {}",
						m,
						objectFile.getAbsolutePath(),
						deleted);
				throw new BadDigestException(
						"Supplied MD5 hash ["
								+ md5Hash
								+ "] for "
								+ objectFile.getName()
								+ " does not match computed hash ["
								+ md5HashAsRead
								+ "]");
			}
			return new FileObjectUploadResult(objectFile);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (digestInputStream == null) {
				BtUtil.close(inputStream);
			} else {
				BtUtil.close(digestInputStream);
			}
		}
	}

	@Override
	public void deleteRecordingObject(RecordingObjectEntity recordingObject) {
		String fileKey = recordingObject.getFileName();
		File file = new File(baseDir, fileKey);
		file.delete();
	}

	@Override
	public InputStream getRecordingObject(
			String fileKey,
			Long startByteOffset,
			Long endByteOffset) throws
			InvalidRangeException {
		final String m = "getRecordingObject(...)";
		try {
			final File objectFile = new File(
					baseDir,
					fileKey);
			final long length = objectFile.length();
			final FileInputStream fis = new FileInputStream(objectFile);

			if (startByteOffset == null && endByteOffset == null) {
				return fis;
			} else if (startByteOffset != null && endByteOffset != null) {
				if (startByteOffset.longValue() < 0
						|| startByteOffset.longValue() >= length
						|| endByteOffset.longValue() < startByteOffset
								.longValue()) {
					throw new InvalidRangeException(
							startByteOffset,
							endByteOffset);
				}
				ByteStreams.skipFully(fis, startByteOffset.longValue());
				long limit = Math.min(
						endByteOffset.longValue(),
						length - 1)
						- startByteOffset.longValue()
						+ 1;
				return ByteStreams.limit(fis, limit);
			} else {
				throw new IllegalArgumentException(
						"byte range not supported: ["
								+ startByteOffset
								+ ", "
								+ endByteOffset
								+ "]");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

}
