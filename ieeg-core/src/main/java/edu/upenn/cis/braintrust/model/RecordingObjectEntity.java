/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@Entity(name = "RecordingObject")
@Table(name = RecordingObjectEntity.TABLE)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = RecordingObjectEntity.ID_COLUMN)
		)
})
@NamedQueries({
	@NamedQuery(
			name = RecordingObjectEntity.FIND_BY_NAME,
			query = "select ro "
					+ "from RecordingObject ro "
					+ "where ro.fileName = :name ")
})
public class RecordingObjectEntity extends LongIdAndVersion implements IEntity {

	public final static String TABLE = "recording_object";
	public final static String FIND_BY_NAME = "recording_object_by_name";
	public final static String ID_COLUMN = TABLE + "_id";

	private Recording parent;

	private String fileName;

	private String internetMediaType;
	private String md5Hash;
	private UserEntity creator;
	private String description;
	private Date createTime = new Date();
	private Long sizeBytes;
	private Set<RecordingObjectJsonEntity> jsonSet = newHashSet();

	public RecordingObjectEntity() {}

	public RecordingObjectEntity(
			Recording parent,
			String fileName,
			String internetMediaType,
			long sizeBytes,
			String md5,
			UserEntity creator) {
		this.parent = checkNotNull(parent);

		this.fileName = checkNotNull(fileName);
		checkArgument(
				fileName.length() >= 1,
				"fileName too short");
		checkArgument(
				fileName.length() <= 255,
				"fileName too long");

		this.internetMediaType = checkNotNull(internetMediaType);
		checkArgument(
				internetMediaType.length() >= 1,
				"internetMediaType too short");
		checkArgument(
				internetMediaType.length() <= 255,
				"internetMediaType too long");

		this.sizeBytes = sizeBytes;

		this.md5Hash = checkNotNull(md5);
//		checkArgument(
//				md5.length() == 32,
//				"md5 needs to be 32 long, it's [" + md5 + "]");

		this.creator = checkNotNull(creator);
	}

	public RecordingObjectEntity(
			Recording parent,
			String fileName,
			String internetMediaType,
			long sizeBytes,
			String md5,
			UserEntity creator,
			String description) {
		this(
				parent,
				fileName,
				internetMediaType,
				sizeBytes,
				md5,
				creator);
		this.description = checkNotNull(description);
		checkArgument(
				description.length() >= 1,
				"description too short");
		checkArgument(
				description.length() <= 4000,
				"description too long");
	}

	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creator_id")
	@NotNull
	public UserEntity getCreator() {
		return creator;
	}

	@Size(min = 1, max = 4000)
	public String getDescription() {
		return description;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getFileName() {
		return fileName;
	}

	@Size(min = 1, max = 255)
	@NotNull
	public String getInternetMediaType() {
		return internetMediaType;
	}

	@Nullable
	@Transient
	public String getJson() {
		if (jsonSet.isEmpty()) {
			return null;
		}
		return getOnlyElement(jsonSet).getValue();
	}

	/**
	 * Arbitrary json associated with this recording object
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than build time
	 * bytecode instrumentation because that loads all lazy props when you hit
	 * any of them.
	 * 
	 * @return the arbitrary json associated with this object
	 */
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			orphanRemoval = true)
	private Set<RecordingObjectJsonEntity> getJsonSet() {
		return jsonSet;
	}

	@Size(min = 0, max = 32)
	@Column(name = "md5_hash")
	@NotNull
	public String getMd5Hash() {
		return md5Hash;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public Recording getParent() {
		return parent;
	}

	@NotNull
	public Long getSizeBytes() {
		return sizeBytes;
	}

	@SuppressWarnings("unused")
	private void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@SuppressWarnings("unused")
	private void setCreator(UserEntity creator) {
		this.creator = creator;
	}

	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	@SuppressWarnings("unused")
	private void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setInternetMediaType(String internetMediaType) {
		this.internetMediaType = internetMediaType;
	}

	public void setJson(@Nullable String json) {
		if (json == null) {
			if (!jsonSet.isEmpty()) {
				jsonSet.clear();
			}
		} else {
			if (jsonSet.isEmpty()) {
				jsonSet.add(new RecordingObjectJsonEntity(json, this));
			} else {
				getOnlyElement(jsonSet).setValue(json);
			}
		}
	}

	@SuppressWarnings("unused")
	private void setJsonSet(Set<RecordingObjectJsonEntity> jsonSet) {
		this.jsonSet = jsonSet;
	}

	@SuppressWarnings("unused")
	private void setMd5Hash(String md5Hash) {
		this.md5Hash = md5Hash;
	}

	public void setParent(Recording parent) {
		this.parent = parent;
	}

	@SuppressWarnings("unused")
	private void setSizeBytes(long sizeBytes) {
		this.sizeBytes = sizeBytes;
	}

	public static final EntityConstructor<RecordingObjectEntity, String, Recording> CONSTRUCTOR = 
		new EntityConstructor<RecordingObjectEntity,String,Recording>(
				RecordingObjectEntity.class,
				new IPersistentObjectManager.IPersistentKey<RecordingObjectEntity,String>() {

			@Override
			public String getKey(RecordingObjectEntity o) {
				return o.getFileName();
			}

			@Override
			public void setKey(RecordingObjectEntity o, String newKey) {
				o.setFileName(newKey);
			}

		},
		new EntityPersistence.ICreateObject<RecordingObjectEntity,String,Recording>() {

			@Override
			public RecordingObjectEntity create(String label, Recording optParent) {
				// Null media type
				return new RecordingObjectEntity(optParent, label, null, 0, null, null);
			}
		}
				);
	
	@Override
	public EntityConstructor<RecordingObjectEntity, String, Recording> tableConstructor() {
		return CONSTRUCTOR;
	}

}
