/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.shared.IdAndVersionDto;
import edu.upenn.cis.braintrust.shared.RecordingObjectId;

@Immutable
@GwtCompatible(serializable = true)
public final class RecordingObjectIdAndVersion
		extends IdAndVersionDto<Long> {

	private static final long serialVersionUID = 1L;

	/** For GWT. */
	@SuppressWarnings("unused")
	private RecordingObjectIdAndVersion() {}

	public RecordingObjectIdAndVersion(long id, int version) {
		super(id, version);
	}

	public RecordingObjectId asRecordingObjectId() {
		return new RecordingObjectId(getId());
	}

}
