/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.JsonString;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * An annotation of a time series.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public class TsAnnotationDto implements IHasStringId, Serializable, IDetection {

	private static final long serialVersionUID = 1L;

	private Set<TimeSeriesDto> annotated = newHashSet();
	private String type;
	private String annotator;
	private String description;
	private Long startOffsetMicros;
	private Long endOffsetMicros;
	private String id;
	private String layer;
	private String color;
	private String externalId;

	@JsonCreator
	public TsAnnotationDto(
			@JsonProperty("annotated") final Set<TimeSeriesDto> annotated,
			@JsonProperty("annotator") final String annotator,
			@JsonProperty("startOffsetMicros") final Long startOffsetMicros,
			@JsonProperty("endOffsetMicros") final Long endOffsetMicros,
			@JsonProperty("type") final String type,
			@JsonProperty("pubId") @Nullable final String pubId,
			@JsonProperty("description") @Nullable final String description,
			@JsonProperty("externalId") @Nullable final String externalId,
			@JsonProperty("layer") @Nullable final String layer,
			@JsonProperty("color") @Nullable final String color) {
		this.annotated = checkNotNull(annotated);
		this.type = checkNotNull(type);
		this.annotator = checkNotNull(annotator);
		this.startOffsetMicros = checkNotNull(startOffsetMicros);
		this.endOffsetMicros = checkNotNull(endOffsetMicros);
		this.id = pubId;
		this.description = description;
		this.externalId = externalId;
		this.layer = layer;
		this.color = color;
	}

	public Set<TimeSeriesDto> getAnnotated() {
		return annotated;
	}

	public String getAnnotator() {
		return annotator;
	}

	public String getDescription() {
		return description;
	}

	public Long getEndOffsetMicros() {
		return endOffsetMicros;
	}

	public String getExternalId() {
		return externalId;
	}

	public String getLayer() {
		return layer;
	}

	public String getColor() {
		return color;
	}

	@Override
	public String getId() {
		return id;
	}

	public Long getStartOffsetMicros() {
		return startOffsetMicros;
	}

	public String getType() {
		return type;
	}

	public boolean hasValuesEqualTo(
			final TsAnnotationDto tsa) {
		return hasValuesEqualTo(tsa,
				newHashSet(transform(tsa.getAnnotated(), IHasStringId.getId)));
	}

	/**
	 * Compare everything except {@link #getId()}. Only looks at the rev ids of
	 * {@link #getAnnotated()}.
	 * 
	 * @param tsa comparing to
	 * 
	 * @return true if everything except rev stableId is the equals, false
	 *         otherwise
	 */
	public boolean hasValuesEqualTo(
			final TsAnnotationDto tsa,
			final Set<String> otherTsRevIds) {
		if (this == tsa) {
			return true;
		}
		if (tsa == null) {
			return false;
		}
		if (getClass() != tsa.getClass()) {
			return false;
		}
		final TsAnnotationDto other = tsa;
		if (annotated == null) {
			if (other.annotated != null) {
				return false;
			}
		} else {
			if (getAnnotated().size() != otherTsRevIds.size()) {
				return false;
			}
			final Set<String> thisTsRevIds = newHashSet(transform(
					getAnnotated(),
					IHasStringId.getId));
			if (!thisTsRevIds.equals(otherTsRevIds)) {
				return false;
			}
		}
		if (annotator == null) {
			if (other.annotator != null) {
				return false;
			}
		} else if (!annotator.equals(other.annotator)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (endOffsetMicros == null) {
			if (other.endOffsetMicros != null) {
				return false;
			}
		} else if (!endOffsetMicros.equals(other.endOffsetMicros)) {
			return false;
		}
		if (startOffsetMicros == null) {
			if (other.startOffsetMicros != null) {
				return false;
			}
		} else if (!startOffsetMicros.equals(other.startOffsetMicros)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	public void setAnnotated(final Set<TimeSeriesDto> annotated) {
		this.annotated = checkNotNull(annotated);
	}

	/**
	 * @param annotator the annotator to set
	 */
	public void setAnnotator(final String annotator) {
		this.annotator = checkNotNull(annotator);
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(@Nullable final String description) {
		this.description = description;
	}

	/**
	 * @param endOffsetMicros the endTimeUutc to set
	 */
	public void setEndOffsetMicros(Long endOffsetMicros) {
		this.endOffsetMicros = checkNotNull(endOffsetMicros);
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public void setLayer(@Nullable String layer) {
		this.layer = layer;
	}

	public void setId(@Nullable final String id) {
		this.id = id;
	}

	/**
	 * @param startTimeUutc the startTimeUutc to set
	 */
	public void setStartOffsetMicros(final Long startTimeUutc) {
		this.startOffsetMicros = checkNotNull(startTimeUutc);
	}

	/**
	 * @param type the type to set
	 */
	public void setType(final String type) {
		this.type = checkNotNull(type);
	}

	public void setColor(@Nullable final String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "TsAnnotationDto [type=" + type
				+ ", annotator=" + annotator + ", description=" + description
				+ ", startTimeUutc=" + startOffsetMicros + ", endTimeUutc="
				+ endOffsetMicros + ", stableId=" + id + ", layer=" + layer
				+ ", externalId=" + externalId + ", color=" + color + "]";
	}

	@JsonIgnore
	@Override
	public double getStart() {
		return getStartOffsetMicros();
	}

	@JsonIgnore
	@Override
	public double getDuration() {
		return getEndOffsetMicros() - getStartOffsetMicros();
	}

	@JsonIgnore
	@Override
	public int getNumericValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@JsonIgnore
	@Override
	public JsonTyped getNameOrAuxiliaryInfo() {
		return new JsonString(getType());
	}

	@JsonIgnore
	@Override
	public DetectionType getDetectionType() {
		return DetectionType.Annotation;
	}

	@JsonIgnore
	@Override
	public String getStringValue() {
		return getType();
	}
}
