/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import static com.google.common.collect.Maps.newTreeMap;

import java.util.Collections;
import java.util.SortedMap;

/**
 * <b>These must not be reordered - the db depends on their ordinal values.</b>
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 */
public enum Gender {

	MALE("Male"), FEMALE("Female"), UNKOWN("Unknown");

	private final String displayString;

	/**
	 * Maps a {@code Gender}'s toString representation to the {@code Gender}
	 * itself.
	 */
	public static final SortedMap<String, Gender> fromString;
	static {
		final SortedMap<String, Gender> temp = newTreeMap();
		for (final Gender gender : values()) {
			temp.put(gender.toString(), gender);
		}
		fromString = Collections.unmodifiableSortedMap(temp);
	}

	private Gender(final String displayString) {
		this.displayString = displayString;
	}

	@Override
	public String toString() {
		return displayString;
	}
}
