/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.EpilepsySubtypeEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtEpilepsySubtype;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class EpilepsySubtypeAssembler {

	public GwtEpilepsySubtype buildDto(final EpilepsySubtypeEntity subtype) {
		checkNotNull(subtype);
		final GwtEpilepsySubtype gwtSubtype = new GwtEpilepsySubtype(
				subtype.getEpilepsyType(),
				subtype.getEtiology(),
				subtype.getId(),
				subtype.getVersion());
		return gwtSubtype;
	}

	public EpilepsySubtypeEntity buildEntity(EegStudy parent,
			GwtEpilepsySubtype gwtSubtype) {
		checkNotNull(gwtSubtype);
		checkArgument(gwtSubtype.getId() == null);
		checkArgument(gwtSubtype.getVersion() == null);
		final EpilepsySubtypeEntity subtype = new EpilepsySubtypeEntity(
				parent,
				gwtSubtype.getEpilepsyType(),
				gwtSubtype.getEtiology());
		return subtype;
	}

	private void copyToEntity(final EpilepsySubtypeEntity subtype,
			final GwtEpilepsySubtype gwtSubtype) {
		subtype.setEpilepsyType(gwtSubtype.getEpilepsyType());
		subtype.setEtiology(gwtSubtype.getEtiology());
	}

	public void modifyEntity(
			final EpilepsySubtypeEntity subtype,
			final GwtEpilepsySubtype gwtSubtype)
			throws StaleObjectException {
		final Long gwtId = gwtSubtype.getId();
		final Long dbId = subtype.getId();
		if (!gwtId.equals(dbId)) {
			throw new IllegalArgumentException("DTO id: [" + gwtId
					+ "] does not match entity id: [" + dbId + "].");

		}
		final Integer gwtVer = gwtSubtype.getVersion();
		final Integer dbVer = subtype.getVersion();
		if (!gwtVer.equals(dbVer)) {
			throw new StaleObjectException(
					"Version mismatch for epilepsy subtype ["
							+ dbId + "]. DTO version: [" + gwtVer
							+ "], DB version: [" + dbVer + "].");
		}
		copyToEntity(subtype, gwtSubtype);
	}
}
