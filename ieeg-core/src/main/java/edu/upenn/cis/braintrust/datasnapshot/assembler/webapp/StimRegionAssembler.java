/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtStimRegion;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class StimRegionAssembler {

	public GwtStimRegion buildDto(
			final StimRegionEntity stimRegion) {
		checkNotNull(stimRegion);
		GwtStimRegion gwtStimRegion = new GwtStimRegion(
				stimRegion.getLabel(),
				stimRegion.getId(),
				stimRegion.getVersion());
		return gwtStimRegion;
	}

	public StimRegionEntity buildEntity(
			final GwtStimRegion gwtStimRegion) {
		checkNotNull(gwtStimRegion);
		checkArgument(gwtStimRegion.getId() == null);
		checkArgument(gwtStimRegion.getVersion() == null);
		final StimRegionEntity stimRegion = new StimRegionEntity(
				gwtStimRegion.getLabel());
		return stimRegion;
	}

	public void modifyEntity(
			final StimRegionEntity stimRegion,
			final GwtStimRegion gwtStimRegion)
			throws StaleObjectException {
		checkNotNull(gwtStimRegion);
		checkNotNull(stimRegion);
		final Long dtoId = gwtStimRegion.getId();
		final Long entityId = stimRegion.getId();
		if (!dtoId.equals(entityId)) {
			throw new IllegalArgumentException("DTO id: [" + dtoId
					+ "] does not match entity id: [" + entityId + "].");
		}
		final Integer dtoV = gwtStimRegion.getVersion();
		final Integer entityV = stimRegion.getVersion();
		if (!dtoV.equals(entityV)) {
			throw new StaleObjectException(
					"Version mismatch for stimulation region ["
							+ entityId + "]. DTO version: [" + dtoV
							+ "], DB version: [" + entityV + "].");
		}
		copyToEntity(stimRegion, gwtStimRegion);
	}

	private void copyToEntity(
			final StimRegionEntity stimRegion,
			final GwtStimRegion gwtStimRegion) {
		stimRegion.setLabel(gwtStimRegion.getLabel());
	}

}
