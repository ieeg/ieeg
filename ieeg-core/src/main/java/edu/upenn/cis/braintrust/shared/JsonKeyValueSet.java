package edu.upenn.cis.braintrust.shared;

import java.util.HashSet;

import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class JsonKeyValueSet extends HashSet<IJsonKeyValue> implements JsonTyped {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}