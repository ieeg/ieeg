/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;

@Immutable
@GwtCompatible(serializable = true)
public final class ReferenceChannelDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private TimeSeriesId channelId;
	private double coefficient;

	@SuppressWarnings("unused")
	private ReferenceChannelDto() {}

	public ReferenceChannelDto(
			TimeSeriesId channelId,
			double coefficient) {
		this.channelId = checkNotNull(channelId);
		this.coefficient = coefficient;
	}

	public TimeSeriesId getChannelId() {
		return channelId;
	}

	public double getCoefficient() {
		return coefficient;
	}

}
