/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasRecording;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

/**
 * @author John Frommeyer
 */
@NamedQueries({
		@NamedQuery(
				name = ExperimentEntity.COUNT_WORLD_READABLE_EXPERIMENTS,
				query = "select count(*) from Experiment es join es.extAcl acl "
						+ ExtAclEntity.FROM_WORLD_READABLE
						+ "where "
						+ ExtAclEntity.WHERE_WORLD_READABLE),
		@NamedQuery(
				name = ExperimentEntity.COUNT_USER_READABLE_EXPERIMENTS,
				query = "select count(e) from Experiment e "
						+ "join e.extAcl acl "
						+ "where "
						+ ExtAclEntity.WHERE_USER_READABLE),
		@NamedQuery(
				name = ExperimentEntity.COUNT_ALL_STUDIES,
				query = "select count(*) from Experiment es"),
		@NamedQuery(
				name = ExperimentEntity.BY_TIME_SERIES,
				query = "From Experiment e "
						+ "join fetch e.recording r "
						+ "join fetch r.contactGroups g "
						+ "join fetch g.contacts c "
						+ "join fetch c.trace t "
						+ "where t = :timeSeries"),
		@NamedQuery(
				name = ExperimentEntity.BY_RECORDING,
				query = "From Experiment e "
						+ "where e.recording = :recording") })
@Entity(name = "Experiment")
@Table(name = ExperimentEntity.TABLE)
@PrimaryKeyJoinColumn(name = ExperimentEntity.ID_COLUMN)
public class ExperimentEntity
		extends DataSnapshotEntity
		implements IHasRecording {
	public static final String TABLE = "Experiment";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String COUNT_WORLD_READABLE_EXPERIMENTS = "Experiment-countActiveStudies";
	public static final String COUNT_USER_READABLE_EXPERIMENTS = "Experiment-countUserReadableExperiments";
	public static final String COUNT_ALL_STUDIES = "Experiment-countAllStudies";
	public static final String BY_TIME_SERIES = "Experiment-findByTimeSeries";
	public static final String BY_RECORDING = "Experiment-findByRecording";

	private AnimalEntity parent;
	private Recording recording;
	private String age;
	private Set<DrugAdminRelationship> drugAdmins = newHashSet();
	private StimTypeEntity stimType;
	private StimRegionEntity stimRegion;
	private Integer stimIsiMs;
	private Integer stimDurationMs;

	public ExperimentEntity() {}

	/**
	 * This constructor sets both sides of the Animal/Experiment relationship.
	 * 
	 * @param parent
	 * @param label
	 * @param extAcl
	 * @param recording
	 * @param age
	 * @param stimulationType
	 * @param stimulationRegion
	 * @param stimulationIsiMs
	 * @param stimulationDurationMs
	 * @param clob
	 */
	public ExperimentEntity(
			@Nullable AnimalEntity parent,
			String label,
			ExtAclEntity extAcl,
			@Nullable Recording recording,
			@Nullable String age,
			@Nullable StimTypeEntity stimulationType,
			@Nullable StimRegionEntity stimulationRegion,
			@Nullable Integer stimulationIsiMs,
			@Nullable Integer stimulationDurationMs,
			@Nullable String clob) {
		super(label, extAcl, clob);
		this.parent = parent;//checkNotNull(parent);
		this.recording = recording;//checkNotNull(recording);
		this.age = age;
		this.stimType = stimulationType;
		this.stimRegion = stimulationRegion;
		this.stimIsiMs = stimulationIsiMs;
		this.stimDurationMs = stimulationDurationMs;
		if (this.parent != null)
			this.parent.getExperiments().add(this);
	}

	public ExperimentEntity(
			String label,
			ExtAclEntity extAcl,
			@Nullable String age,
			@Nullable StimTypeEntity stimulationType,
			@Nullable StimRegionEntity stimulationRegion,
			@Nullable Integer stimulationIsiMs,
			@Nullable Integer stimulationDurationMs,
			@Nullable String clob) {
		super(label, extAcl, clob);
		this.age = age;
		this.stimType = stimulationType;
		this.stimRegion = stimulationRegion;
		this.stimIsiMs = stimulationIsiMs;
		this.stimDurationMs = stimulationDurationMs;
	}

	public String getAge() {
		return age;
	}

	@Transient
	@Override
	public DataSnapshotType getDataSnapshotType() {
		return DataSnapshotType.EXPERIMENT;
	}

	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<DrugAdminRelationship> getDrugAdmins() {
		return drugAdmins;
	}

	@Override
	@Transient
	public Set<ImageEntity> getImages() {
		return recording.getImages();
	}

	@ManyToOne(fetch = FetchType.EAGER)//LAZY)
	@JoinColumn(name = "parent_id")
//	@NotNull
	public AnimalEntity getParent() {
		return parent;
	}

	@OneToOne(
			cascade = CascadeType.ALL,
			optional = false,
			fetch = FetchType.EAGER,//LAZY,
			orphanRemoval = true)
	@JoinColumn(name = Recording.ID_COLUMN)
	@OnDelete(action = OnDeleteAction.CASCADE)
//	@NotNull
	public Recording getRecording() {
		return recording;
	}

	@Min(0)
	public Integer getStimDurationMs() {
		return stimDurationMs;
	}

	@Min(0)
	public Integer getStimIsiMs() {
		return stimIsiMs;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = StimRegionEntity.ID_COLUMN)
	public StimRegionEntity getStimRegion() {
		return stimRegion;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = StimTypeEntity.ID_COLUMN)
	public StimTypeEntity getStimType() {
		return stimType;
	}

	@Override
	@Transient
	public Set<TimeSeriesEntity> getTimeSeries() {
		final Set<TimeSeriesEntity> tSeries = newHashSet();
		for (final ContactGroup electrode : recording.getContactGroups()) {
			for (final Contact contact : electrode.getContacts()) {
				tSeries.add(contact.getTrace());
			}
		}
		return Collections.unmodifiableSet(tSeries);
	}

	public void setAge(String age) {
		this.age = age;
	}

	@SuppressWarnings("unused")
	private void setDrugAdmins(Set<DrugAdminRelationship> drugAdmins) {
		this.drugAdmins = drugAdmins;
	}

	public void setParent(AnimalEntity parent) {
		this.parent = parent;
		if (this.parent != null)
			this.parent.getExperiments().add(this);
	}

	@SuppressWarnings("unused")
	private void setRecording(Recording recording) {
		this.recording = recording;
	}

	public void setStimDurationMs(Integer stimDurationMs) {
		this.stimDurationMs = stimDurationMs;
	}

	public void setStimIsiMs(Integer isiMs) {
		this.stimIsiMs = isiMs;
	}

	public void setStimRegion(StimRegionEntity region) {
		this.stimRegion = region;
	}

	public void setStimType(StimTypeEntity type) {
		this.stimType = type;
	}

	@Override
	public String toString() {
		return "ExperimentEntity [id="
				+ getId()
				+ ", label=" + getLabel()
				+ "]";

	}

	@Transient
	@Override
	public OrganizationEntity getOrganization() {
		return getParent().getOrganization();
	}

	public static final EntityConstructor<ExperimentEntity, String, AnimalEntity> CONSTRUCTOR =
		new EntityConstructor<ExperimentEntity,String,AnimalEntity>(ExperimentEntity.class,
				new IPersistentObjectManager.IPersistentKey<ExperimentEntity,String>() {

			@Override
			public String getKey(ExperimentEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(ExperimentEntity o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<ExperimentEntity,String,AnimalEntity>() {

			@Override
			public ExperimentEntity create(String label, AnimalEntity optParent) {
				// Null recording, ...
				return new ExperimentEntity(optParent, label, PersistentObjectFactory.getFactory().getEmptyWorldAcl(), null, null, null, null, null, null, null);
			}
		}
				);	

	@Override
	public EntityConstructor<ExperimentEntity, String, AnimalEntity> tableConstructor() {
		return CONSTRUCTOR;
	}

}
