/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.ShortUrl;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class ShortUrlDAOHibernate extends GenericHibernateDAO<ShortUrl, Long>
implements IShortUrlDAO {
	public ShortUrlDAOHibernate(Session session) {
		setSession(session);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ShortUrl getShortUrl(Long urlId) {
		ShortUrl u = (ShortUrl) getSession()
				.createQuery(
						"from ShortUrl "
					  + " left join fetch ShortUrl.eegBookmark "
					  + " left join fetch ShortUrl.imageBookmark "
					  + " where shortId = :id"
						)
				.setParameter("id", urlId)
				.uniqueResult();
		
		return u;
	}
}
