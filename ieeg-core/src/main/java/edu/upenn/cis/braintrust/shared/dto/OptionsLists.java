/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * A wrapper for lists of options for creating studies and experiments
 * 
 * @author John Frommeyer
 * 
 */
public class OptionsLists implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<GwtOrganization> organizations;
	private List<GwtSpecies> species;
	private List<GwtDrug> drugs;
	private List<GwtStimRegion> stimRegions;
	private List<GwtStimType> stimTypes;

	@SuppressWarnings("unused")
	private OptionsLists() {}

	/**
	 * 
	 * 
	 * @param organizations
	 * @param species
	 * @param drugs
	 * @param stimRegions
	 * @param stimTypes
	 */
	public OptionsLists(
			List<GwtOrganization> organizations,
			List<GwtSpecies> species,
			List<GwtDrug> drugs,
			List<GwtStimRegion> stimRegions,
			List<GwtStimType> stimTypes) {
		this.organizations = ImmutableList.copyOf(organizations);
		this.species = ImmutableList.copyOf(species);
		this.drugs = ImmutableList.copyOf(drugs);
		this.stimRegions = ImmutableList.copyOf(stimRegions);
		this.stimTypes = ImmutableList.copyOf(stimTypes);
	}

	/**
	 * @return the organizations
	 */
	public List<GwtOrganization> getOrganizations() {
		return organizations;
	}

	/**
	 * @return the species
	 */
	public List<GwtSpecies> getSpecies() {
		return species;
	}

	/**
	 * @return the drugs
	 */
	public List<GwtDrug> getDrugs() {
		return drugs;
	}

	/**
	 * @return the stimRegions
	 */
	public List<GwtStimRegion> getStimRegions() {
		return stimRegions;
	}

	/**
	 * @return the stimTypes
	 */
	public List<GwtStimType> getStimTypes() {
		return stimTypes;
	}

}
