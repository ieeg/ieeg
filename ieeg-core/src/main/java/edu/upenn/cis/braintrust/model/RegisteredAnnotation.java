/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name=RegisteredAnnotation.TABLE)
@NamedQueries({
	@NamedQuery(
			name = RegisteredAnnotation.FIND_BY_TYPE,
			query = "from RegisteredAnnotation ra "
					+ "where ra.viewer = :viewer")
})
public class RegisteredAnnotation {
	public static final String TABLE = "registered_annotation";
	public static final String FIND_BY_TYPE = "registered_annotation-find-by-type";
	
	private Long shortId;

	private String viewer;
	private String annotationName;
	
	private Integer numberOfPoints;
	
	private String category;
	
	private Boolean global;
	
	public RegisteredAnnotation() {}
	
	
	public RegisteredAnnotation(String viewer,
			String annotationName, int numberOfPoints, String category,
			boolean global) {
		super();
		this.viewer = viewer;
		this.annotationName = annotationName;
		this.numberOfPoints = numberOfPoints;
		this.category = category;
		this.global = global;
	}


	public RegisteredAnnotation(Long shortId, String viewer,
			String annotationName, int numberOfPoints, String category,
			boolean global) {
		this(viewer, annotationName, numberOfPoints, category, global);

		this.shortId = shortId;
	}
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "reg_id", unique = true, nullable = false)
	public Long getShortId() {
		return shortId;
	}

	public void setShortId(Long shortId) {
		this.shortId = shortId;
	}


	@NotNull
	public String getViewer() {
		return viewer;
	}


	public void setViewer(String viewer) {
		this.viewer = viewer;
	}


	@Column(unique = true)
	public String getAnnotationName() {
		return annotationName;
	}


	public void setAnnotationName(String annotationName) {
		this.annotationName = annotationName;
	}


	@NotNull
	@Range(min=1)
	public Integer getNumberOfPoints() {
		return numberOfPoints;
	}


	public void setNumberOfPoints(Integer numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	@NotNull
	@Column(columnDefinition = "BIT", length = 1)
	public Boolean getGlobal() {
		return global;
	}


	public void setGlobal(Boolean global) {
		this.global = global;
	}

	
}
