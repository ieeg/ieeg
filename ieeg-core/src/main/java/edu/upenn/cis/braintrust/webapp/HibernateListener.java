/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import javax.annotation.Nonnull;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.drupal.persistence.DrupalHibernateUtil;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;

public final class HibernateListener
		implements ServletContextListener {

	private Logger logger = LoggerFactory
			.getLogger(HibernateListener.class);

	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		String m = "contextDestroyed(...)";
		logger.info("{}: shutting down hibernate...", m);
		try {
			HibernateUtil.shutdown();
		} catch (Exception t) {
			logger.error("problem shutting down hibernate", t);
		}
	}

	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		String m = "contextInitialized(...)";
		logger.info("starting up hibernate...");
		@Nonnull
		Optional<Properties> hibernateProps = Optional.absent();

		@Nonnull
		Optional<Properties> drupalProps = Optional.absent();
		try {
			if (AwsUtil.isConfigFromS3()) {
				logger.info("reading hibernate properties and drupal hibernate properties from S3");
				{
					String configKey = null;
					configKey = AwsUtil.getConfigS3Dir() + "/" +
							"hibernate.properties";

					logger.info("reading hibernate properties properties from ["
							+ AwsUtil.getConfigS3Bucket() + "] ["
							+ configKey + "]");

					GetObjectRequest getObjectRequest =
							new GetObjectRequest(
									AwsUtil.getConfigS3Bucket(),
									configKey);

					S3Object s3Object = AwsUtil.getS3().getObject(
							getObjectRequest);

					hibernateProps = Optional.of(new Properties());
					hibernateProps.get().load(s3Object.getObjectContent());
				}
				{
					String configKey =
							AwsUtil.getConfigS3Dir() + "/" +
									DrupalHibernateUtil.PROPS_FILE;

					logger.info("reading drupal hibernate properties properties from ["
							+ AwsUtil.getConfigS3Bucket() + "] ["
							+ configKey + "]");

					GetObjectRequest getObjectRequest =
							new GetObjectRequest(
									AwsUtil.getConfigS3Bucket(),
									configKey);

					S3Object s3Object = AwsUtil.getS3().getObject(
							getObjectRequest);

					drupalProps = Optional.of(new Properties());
					drupalProps.get().load(s3Object.getObjectContent());
				}
			} else {
				{
					String configName = BtUtil.getConfigName(
							sce.getServletContext(),
							"hibernate.properties");
					logger.info("looking for hibernate properties at ["
							+ configName
							+ "]");
					URL url = getClass().getClassLoader().getResource(
							configName);
					if (url == null) {
						logger.info("reading hibernate config from hibernate.properties");
						hibernateProps = Optional.absent();
					} else {
						logger.info("reading hibernate config from "
								+ url.toString());
						hibernateProps = Optional.of(BtUtil.loadProps(url));
					}
				}
				{
					String configName = BtUtil.getConfigName(
							sce.getServletContext(),
							DrupalHibernateUtil.PROPS_FILE);
					logger.info("looking for drupal hibernate properties at ["
							+ configName + "]");
					URL url = getClass().getClassLoader().getResource(
							configName);
					if (url == null) {
						logger.info("reading drupal hibernate config from "
								+ DrupalHibernateUtil.PROPS_FILE);
						drupalProps = Optional.absent();
					} else {
						logger.info("reading drupal hibernate config from "
								+ url.toString());
						drupalProps = Optional.of(BtUtil.loadProps(url));
					}
				}
			}
			if (hibernateProps.isPresent()) {
				HibernateUtil.putConfigProps(hibernateProps.get());
			}
			HibernateUtil.getSessionFactory();

			if (drupalProps.isPresent()) {
				DrupalHibernateUtil.putConfigProps(
						drupalProps.get());
			}
			DrupalHibernateUtil.getSessionFactory();

			logger.info("done");

		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
}
