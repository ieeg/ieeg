/*
 *From http://ehcache.org/documentation/recipes/wrapper
 */
package edu.upenn.cis.braintrust.thirdparty.cache;

/**
 * @deprecated Just pass in the caches through the constructor
 *
 * @param <K>
 * @param <V>
 */
@Deprecated
public interface CacheWrapper<K, V> {
	void put(K key, V value);

	V get(K key);
}
