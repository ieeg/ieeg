/*
 * Copyright (C) 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.Set;

import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.model.ModeEntity;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;

public final class ExtPermissionAssembler {
	private IPermissionDAO permissionDao;

	public ExtPermissionAssembler(
			IPermissionDAO permissionDao) {
		this.permissionDao = checkNotNull(permissionDao);
	}

	public Set<ExtPermission> toDtos(Collection<PermissionEntity> entities) {
		final Set<ExtPermission> perms = newHashSet();
		for (final PermissionEntity entity : entities) {
			final ModeEntity mode = entity.getMode();
			final PermissionDomainEntity domain = mode.getDomain();
			perms.add(new ExtPermission(
					domain.getName(),
					mode.getName(),
					entity.getName()));
		}
		return perms;
	}

	public Set<PermissionEntity> toEntities(Set<ExtPermission> perms) {
		final Set<PermissionEntity> entities = permissionDao.findByDtos(perms);
		// Verify that every dto corresponded to an entity.
		if (perms.size() == entities.size()) {
			return entities;
		} else {
			final Set<ExtPermission> missingPerms = newHashSet();
			for (final ExtPermission perm : perms) {
				final PermissionEntity entity = tryFind(
						entities,
						compose(equalTo(perm.getValue()),
								PermissionEntity.getName)).orNull();
				if (entity == null
						|| !perm.getMode().equals(entity.getMode().getName())
						|| !perm.getDomain().equals(
								entity.getMode().getDomain().getName())) {
					missingPerms.add(perm);
				}
			}
			throw new IllegalArgumentException(
					"Uknown permission types: " + missingPerms);
		}
	}

	public PermissionEntity toEntity(ExtPermission perm) {
		final PermissionEntity entity = permissionDao
				.findByDomainModeAndName(perm.getDomain(), perm.getMode(),
						perm.getValue());
		if (entity == null) {
			throw new IllegalArgumentException(
					"Uknown permission type: " + perm);
		}
		return entity;
	}

	public ExtPermission toDto(PermissionEntity permEntity) {
		return new ExtPermission(
				permEntity.getMode().getDomain().getName(),
				permEntity.getMode().getName(),
				permEntity.getName());
	}
}
