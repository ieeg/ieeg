/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

/**
 * A user's job.
 * 
 * @author John Frommeyer
 */
@GwtCompatible(serializable = true)
public class JobDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private UserId userId;
	private String datasetId;
	private String toolId;
	private JobStatus status;
	private Date createTime;
	private ParallelDto parallel;
	private String jobXml;

	// For GWT
	@SuppressWarnings("unused")
	private JobDto() {}

	public JobDto(
			UserId userId,
			String datasetId,
			String toolId,
			JobStatus status,
			Date createTime,
			@Nullable String jobClob) {
		this.userId = checkNotNull(userId);
		this.datasetId = checkNotNull(datasetId);
		this.toolId = checkNotNull(toolId);
		this.status = checkNotNull(status);
		this.createTime = checkNotNull(createTime);
		this.jobXml = jobClob;
	}

	public String getJobXml() {
		return jobXml;
	}

	public String getDatasetId() {
		return datasetId;
	}

	public String getToolId() {
		return toolId;
	}

	public UserId getUserId() {
		return userId;
	}

	public JobStatus getStatus() {
		return status;
	}


	public Date getCreateTime() {
		return createTime;
	}

	public void setJobXml(@Nullable final String jobXml) {
		this.jobXml = jobXml;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = checkNotNull(datasetId);
	}

	public void setToolId(String toolId) {
		this.toolId = checkNotNull(toolId);
	}

	public void setUserId(UserId userId) {
		this.userId = checkNotNull(userId);
	}

	public void setStatus(JobStatus status) {
		this.status = checkNotNull(status);
	}


	public void setCreateTime(Date createTime) {
		this.createTime = checkNotNull(createTime);
	}

}
