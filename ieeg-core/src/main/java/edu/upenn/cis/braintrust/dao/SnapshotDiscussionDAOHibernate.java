/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.SnapshotDiscussion;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class SnapshotDiscussionDAOHibernate
		extends GenericHibernateDAO<SnapshotDiscussion, Long>
		implements ISnapshotDiscussionDAO {

	public SnapshotDiscussionDAOHibernate(
			final Session session) {
		setSession(checkNotNull(session));
	}

	@Override
	public List<SnapshotDiscussion> findBySnapshotID(DataSnapshotEntity entity,
			int start,
			int count) {

		final Session s = getSession();
		Query query = s.getNamedQuery(SnapshotDiscussion.FIND_BY_DATASET)
				.setParameter("snapId", entity)
				.setFirstResult(start)
				.setMaxResults(count);

		@SuppressWarnings("unchecked")
		List<SnapshotDiscussion> discussions = query.list();

		return discussions;
	}

}
