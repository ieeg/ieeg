/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;

/**
 * Assembles a {@code DataSnapshot} from a {@code DataSnapshotEntity} shallowly.
 * 
 * @author John Frommeyer
 */
public class DataSnapshotAssembler {

	/**
	 * Returns a {@code DataSnapshot} version of {@code dsEntity}.
	 * 
	 * @param dsEntity
	 * 
	 * @return a {@code DataSnapshot} version of {@code dsEntity}.
	 */
	public static DataSnapshot assembleWNoChildren(
			final DataSnapshotEntity dsEntity) {
		checkNotNull(dsEntity);
		DataSnapshot ds = new DataSnapshot(dsEntity.getLabel(),
				dsEntity.isFrozen());
		ds.setRevId(dsEntity.getPubId());
		return ds;
	}

	/**
	 * Returns a {@code DataSnapshot} version of {@code dsEntity} which has had
	 * its {@code Image}, {@code TimeSeries}, and {@code TimeSeriesAnnotation}
	 * collections assembled as well.
	 * 
	 * @param dsEntity
	 * @return a {@code DataSnapshot} version of {@code dsEntity} which has had
	 *         its {@code Image}, {@code TimeSeries}, and
	 *         {@code TimeSeriesAnnotation} collections assembled as well.
	 * @throws NullPointerException if {@code dsEntity} is null;
	 * 
	 */
	public static DataSnapshot assembleWChildren(
			final DataSnapshotEntity dsEntity,
			final Optional<Recording> recording,
			ITsAnnotationDAO tsAnnotationDAO) {
		checkNotNull(dsEntity);
		DataSnapshot ds = assembleWNoChildren(dsEntity);
		// Images
		final Set<Image> images = ds.getImages();
		for (ImageEntity iEntity : dsEntity.getImages()) {
			images.add(ImageAssembler.assemble(iEntity));
		}
		
		ds.setHuman(dsEntity instanceof EegStudy);
		ds.setAnimal(dsEntity instanceof ExperimentEntity);
		if (recording.isPresent()) {
			ds.setRecordingStartTimeUutc(recording.get().getStartTimeUutc());
		}


		ds.setTsAnnotations(
				tsAnnotationDAO.findAllByParent(dsEntity));

		// Time Series
		final Set<TimeSeriesDto> timeSeries = ds.getTimeSeries();
		for (TimeSeriesEntity tsEntity : dsEntity.getTimeSeries()) {
			timeSeries.add(TimeSeriesAssembler.assemble(tsEntity));
		}
		return ds;
	}
}
