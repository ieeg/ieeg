/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.removeIf;
import static com.google.common.collect.Iterables.tryFind;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.filter;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.dao.IProjectDAO;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.imodel.IHasPerms;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.ProjectAceEntity;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtPermissionAssembler;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.NewProjectAce;
import edu.upenn.cis.braintrust.shared.NewUserAce;
import edu.upenn.cis.braintrust.shared.RemoveProjectAce;
import edu.upenn.cis.braintrust.shared.RemoveUserAce;
import edu.upenn.cis.braintrust.shared.UpdateProjectAce;
import edu.upenn.cis.braintrust.shared.UpdateUserAce;
import edu.upenn.cis.braintrust.shared.UpdateWorldAce;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.exception.ProjectNotFoundException;

/**
 * Handles the modification of ACLs for permissions with the given mode.
 * 
 * @author John Frommeyer
 */
public class AclEditor {

	private final ExtPermissionAssembler permissionAssembler;
	private final ExtAclEntity acl;
	private final String mode;
	private final IUserDAO userDAO;
	private final IProjectDAO projectDAO;
	private final static Logger logger = LoggerFactory
			.getLogger(AclEditor.class);

	public AclEditor(
			ExtAclEntity acl,
			String mode,
			IUserDAO userDAO,
			IProjectDAO projectDAO,
			IPermissionDAO permissionDAO) {
		this.acl = checkNotNull(acl);
		this.mode = checkNotNull(mode);
		this.userDAO = checkNotNull(userDAO);
		this.projectDAO = checkNotNull(projectDAO);
		permissionAssembler = new ExtPermissionAssembler(permissionDAO);
	}

	public List<EditAclResponse> editAcl(User loggedInUser,
			List<IEditAclAction<?>> actions) throws AuthorizationException {
		final String m = "editAcl(...)";
		final List<EditAclResponse> results = newArrayList();
		final Set<IEditAclAction<?>> shortenedActions = shortenActionList(actions);
		logger.debug(
				"{}: Processing {} actions. Original number of actions: {}",
				new Object[] {
						m,
						shortenedActions.size(),
						actions.size()
				});
		for (IEditAclAction<?> editAction : shortenedActions) {
			if (editAction instanceof RemoveUserAce) {
				final RemoveUserAce action = (RemoveUserAce) editAction;
				results.add(removeUserAce(loggedInUser, action.getAce()));
			} else if (editAction instanceof UpdateUserAce) {
				final UpdateUserAce action = (UpdateUserAce) editAction;
				results.add(updateUserAce(loggedInUser, action.getAce(),
						action.getNewPerms()));
			} else if (editAction instanceof UpdateWorldAce) {
				final UpdateWorldAce action = (UpdateWorldAce) editAction;
				results.add(updateWorldAce(loggedInUser, action.getAce(),
						action.getNewPerms()));
			} else if (editAction instanceof NewUserAce) {
				final NewUserAce action = (NewUserAce) editAction;
				results.add(newUserAce(loggedInUser, action.getAce()));
			} else if (editAction instanceof NewProjectAce) {
				final NewProjectAce action = (NewProjectAce) editAction;
				results.add(newProjectAce(loggedInUser, action.getAce()));
			} else if (editAction instanceof UpdateProjectAce) {
				final UpdateProjectAce action = (UpdateProjectAce) editAction;
				results.add(updateProjectAce(loggedInUser, action.getAce(),
						action.getNewPerms()));
			} else if (editAction instanceof RemoveProjectAce) {
				final RemoveProjectAce action = (RemoveProjectAce) editAction;
				results.add(removeProjectAce(loggedInUser, action.getAce()));
			} else {
				throw new IllegalArgumentException(
						"Unknown IEditAclAction type: ["
								+ editAction.getClass().getName() + "]");
			}
		}

		return results;
	}

	@VisibleForTesting
	Set<IEditAclAction<?>> shortenActionList(
			List<IEditAclAction<?>> actions) {
		final Set<IEditAclAction<?>> actionSet = newHashSet();
		final Map<UserId, IEditAclAction<ExtUserAce>> userIdToAction = newHashMap();
		final Map<ProjectGroup, IEditAclAction<ExtProjectAce>> groupToAction = newHashMap();
		UpdateWorldAce worldAceEdit = null;
		for (IEditAclAction<?> incomingAction : actions) {
			if (incomingAction instanceof RemoveUserAce) {
				handleRemoveUserAce(userIdToAction,
						(RemoveUserAce) incomingAction);
			} else if (incomingAction instanceof UpdateUserAce) {
				handleUpdateUserAce(userIdToAction,
						(UpdateUserAce) incomingAction);
			} else if (incomingAction instanceof NewUserAce) {
				handleNewUserAce(userIdToAction, (NewUserAce) incomingAction);
			} else if (incomingAction instanceof RemoveProjectAce) {
				handleRemoveProjectAce(groupToAction,
						(RemoveProjectAce) incomingAction);
			} else if (incomingAction instanceof UpdateProjectAce) {
				handleUpdateProjectAce(groupToAction,
						(UpdateProjectAce) incomingAction);
			} else if (incomingAction instanceof NewProjectAce) {
				handleNewProjectAce(groupToAction,
						(NewProjectAce) incomingAction);
			} else if (incomingAction instanceof UpdateWorldAce) {
				final UpdateWorldAce incomingUpdateWorldAction = (UpdateWorldAce) incomingAction;
				if (worldAceEdit == null) {
					worldAceEdit = incomingUpdateWorldAction;
				} else {
					worldAceEdit.setNewPerms(incomingUpdateWorldAction
							.getNewPerms().orNull());
				}
			} else {
				throw new IllegalArgumentException(
						"Unknown IEditAclAction type: ["
								+ incomingAction.getClass().getName() + "]");
			}
		}
		actionSet.addAll(userIdToAction.values());
		actionSet.addAll(groupToAction.values());
		if (worldAceEdit != null) {
			actionSet.add(worldAceEdit);
		}
		return actionSet;
	}

	private void handleNewUserAce(
			final Map<UserId, IEditAclAction<ExtUserAce>> userIdToAction,
			final NewUserAce incomingNewUserAction) {
		final UserId userId = incomingNewUserAction.getAce().getUserId();
		IEditAclAction<ExtUserAce> existingAction = userIdToAction
				.get(userId);
		if (existingAction == null) {
			userIdToAction.put(userId, incomingNewUserAction);
		} else if (existingAction instanceof RemoveUserAce) {
			final RemoveUserAce existingRemoveAction = (RemoveUserAce) existingAction;
			final ExtPermission newPerm = getOnlyElement(incomingNewUserAction
					.getAce().getPerms());
			final UpdateUserAce updateAce = new UpdateUserAce(
					existingRemoveAction.getAce(),
					newPerm);
			userIdToAction.put(userId, updateAce);
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingNewUserAction.getClass()
									.getName() + "]");
		}
	}

	private void handleNewProjectAce(
			final Map<ProjectGroup, IEditAclAction<ExtProjectAce>> projectGroupToAction,
			final NewProjectAce incomingNewProjectAction) {
		final ProjectGroup group = incomingNewProjectAction.getAce()
				.getProjectGroup();
		IEditAclAction<ExtProjectAce> existingAction = projectGroupToAction
				.get(group);
		if (existingAction == null) {
			projectGroupToAction.put(group, incomingNewProjectAction);
		} else if (existingAction instanceof RemoveProjectAce) {
			final RemoveProjectAce existingRemoveAction = (RemoveProjectAce) existingAction;
			final ExtPermission newPerm = getOnlyElement(incomingNewProjectAction
					.getAce().getPerms());
			final UpdateProjectAce updateAce = new UpdateProjectAce(
					existingRemoveAction.getAce(),
					newPerm);
			projectGroupToAction.put(group, updateAce);
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingNewProjectAction.getClass()
									.getName() + "]");
		}
	}

	private void handleUpdateUserAce(
			final Map<UserId, IEditAclAction<ExtUserAce>> userIdToAction,
			final UpdateUserAce incomingUpdateUserAction) {
		final UserId userId = incomingUpdateUserAction.getAce().getUserId();
		IEditAclAction<ExtUserAce> existingAction = userIdToAction
				.get(userId);
		if (existingAction == null) {
			userIdToAction.put(userId, incomingUpdateUserAction);
		} else if (existingAction instanceof NewUserAce) {
			final NewUserAce existingNewUserAction = (NewUserAce) existingAction;
			existingNewUserAction.getAce().getPerms().clear();
			existingNewUserAction.getAce().getPerms().add(
					incomingUpdateUserAction.getNewPerms());
		} else if (existingAction instanceof UpdateUserAce) {
			final UpdateUserAce existingUpdateUserAction = (UpdateUserAce) existingAction;
			existingUpdateUserAction
					.setNewPerms(((UpdateUserAce) incomingUpdateUserAction)
							.getNewPerms());
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingUpdateUserAction.getClass().getName()
							+ "]");
		}
	}

	private void handleUpdateProjectAce(
			final Map<ProjectGroup, IEditAclAction<ExtProjectAce>> projectGroupToAction,
			final UpdateProjectAce incomingUpdateAction) {
		final ProjectGroup group = incomingUpdateAction.getAce()
				.getProjectGroup();
		IEditAclAction<ExtProjectAce> existingAction = projectGroupToAction
				.get(group);
		if (existingAction == null) {
			projectGroupToAction.put(group, incomingUpdateAction);
		} else if (existingAction instanceof NewProjectAce) {
			final NewProjectAce existingNewProjectAction = (NewProjectAce) existingAction;
			existingNewProjectAction.getAce().getPerms().clear();
			existingNewProjectAction.getAce().getPerms().add(
					incomingUpdateAction.getNewPerms());
		} else if (existingAction instanceof UpdateProjectAce) {
			final UpdateProjectAce existingUpdateProjectAction = (UpdateProjectAce) existingAction;
			existingUpdateProjectAction
					.setNewPerms(((UpdateProjectAce) incomingUpdateAction)
							.getNewPerms());
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingUpdateAction.getClass().getName()
							+ "]");
		}
	}

	private void handleRemoveUserAce(
			final Map<UserId, IEditAclAction<ExtUserAce>> userIdToAction,
			RemoveUserAce incomingRemoveAction) {
		final UserId userId = incomingRemoveAction.getAce().getUserId();
		IEditAclAction<ExtUserAce> existingAction = userIdToAction
				.get(userId);
		if (existingAction == null) {
			userIdToAction.put(userId, incomingRemoveAction);
		} else if (existingAction instanceof NewUserAce) {
			userIdToAction.remove(userId);
		} else if (existingAction instanceof UpdateUserAce) {
			userIdToAction.put(userId, incomingRemoveAction);
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingRemoveAction.getClass().getName() + "]");
		}
	}

	private void handleRemoveProjectAce(
			final Map<ProjectGroup, IEditAclAction<ExtProjectAce>> projectGroupToAction,
			RemoveProjectAce incomingRemoveAction) {
		final ProjectGroup group = incomingRemoveAction.getAce()
				.getProjectGroup();
		IEditAclAction<ExtProjectAce> existingAction = projectGroupToAction
				.get(group);
		if (existingAction == null) {
			projectGroupToAction.put(group, incomingRemoveAction);
		} else if (existingAction instanceof NewProjectAce) {
			projectGroupToAction.remove(group);
		} else if (existingAction instanceof UpdateProjectAce) {
			projectGroupToAction.put(group, incomingRemoveAction);
		} else {
			throw new IllegalArgumentException(
					"Illegal Action Sequence: ["
							+ existingAction.getClass().getName()
							+ "] followed by ["
							+ incomingRemoveAction.getClass().getName() + "]");
		}
	}

	public EditAclResponse newUserAce(User loggedInUser, ExtUserAce newAce)
			throws AuthorizationException {
		final String m = "newUserAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = newAce.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			final UserEntity aceUser = userDAO.getOrCreateUser(newAce
					.getUserId());

			final Optional<ExtUserAceEntity> existingAce = tryFind(
					acl.getUserAces(),
					compose(equalTo(newAce.getUserId()),
							ExtUserAceEntity.getUserId));
			final Set<PermissionEntity> permEntities = permissionAssembler
					.toEntities(newAce.getPerms());
			if (existingAce.isPresent()) {
				existingAce.get().getPerms()
						.addAll(permEntities);
			} else {
				// Constructor adds ace to acl.
				new ExtUserAceEntity(
						aceUser, acl, permEntities);
			}
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Add {} with {}", new Object[] { m,
					newAce.getName(),
					newAce.getPerms() });
			return response;
		}
	}

	public EditAclResponse newProjectAce(User loggedInUser, ExtProjectAce newAce)
			throws AuthorizationException {
		final String m = "newProjectAce(...)";
		logger.debug("{}: New project ace for {} being added by user {}",
				new Object[] {
						m,
						newAce.getProjectGroup(),
						loggedInUser.getUsername()
				});
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = newAce.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			final Set<PermissionEntity> permEntities = permissionAssembler
					.toEntities(newAce.getPerms());

			ProjectAceEntity existingAce = null;
			for (final ProjectAceEntity pAce : acl.getProjectAces()) {
				final ProjectGroup pAceGroup = new ProjectGroup(
						pAce.getProject().getPubId(),
						pAce.getProject().getName(),
						pAce.getProjectGroup());
				if (pAceGroup.equals(newAce.getProjectGroup())) {
					existingAce = pAce;
					break;
				}
			}
			if (existingAce == null) {
				logger.debug(
						"{}: target {} had no ace for project group {}. Creating new ace.",
						new Object[] {
								m,
								newAce.getTargetId(),
								newAce.getProjectGroup()
						});
				final String projectId = newAce.getProjectGroup()
						.getProjectId();
				final ProjectEntity project = projectDAO.findByNaturalId(
						projectId,
						false);
				if (project == null) {
					throw new ProjectNotFoundException(projectId);
				}
				new ProjectAceEntity(
						project,
						newAce.getProjectGroup().getType(),
						acl,
						permEntities);
			} else {
				logger.debug(
						"{}: target {} has ace for project group {}. Adding perms to existing ace.",
						new Object[] {
								m,
								newAce.getTargetId(),
								newAce.getProjectGroup()
						});
				existingAce.getPerms()
						.addAll(permEntities);
			}
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Add {} with {}", new Object[] {
					m,
					newAce.getProjectGroup(),
					newAce.getPerms() });
			return response;
		}
	}

	public EditAclResponse updateWorldAce(User loggedInUser,
			ExtWorldAce oldAce,
			Optional<ExtPermission> newPermsOpt)
			throws AuthorizationException {
		final String m = "updateWorldAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = oldAce.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			removeIf(
					acl.getWorldAce(),
					compose(equalTo(mode),
							PermissionEntity.getModeName));
			if (newPermsOpt.isPresent()) {
				final ExtPermission newPerms = newPermsOpt.get();
				acl.getWorldAce().add(permissionAssembler.toEntity(newPerms));
			}
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug(
					"{}: WorldAce change: Old value: {}, new value: {}",
					new Object[] {
							m,
							oldAce.getPerms(),
							newPermsOpt });

			return response;

		}

	}

	public EditAclResponse updateUserAce(User loggedInUser, ExtUserAce oldAce,
			ExtPermission newPerms)
			throws AuthorizationException {
		final String m = "updateUserAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = oldAce.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			checkArgument(oldAce.getAceId().isPresent());
			final Long aceId = oldAce.getAceId().get();
			final ExtUserAceEntity aceEntity = find(acl.getUserAces(),
					compose(equalTo(aceId), ExtUserAceEntity.getId), null);
			if (aceEntity == null) {
				return new EditAclResponse(false,
						"Could not modify permissions "
								+ newPerms
								+ " for " + oldAce.getName()
								+ ". Current permissions not found.");
			}
			final Integer currentAceVer = aceEntity.getVersion();
			final Integer dtoAceVer = oldAce.getAceVersion().orNull();
			if (!dtoAceVer.equals(currentAceVer)) {
				return new EditAclResponse(
						false,
						oldAce.getName()
								+ " permissions for this snapshot have changed. Please refresh your view.");
			}
			updatePerms(newPerms, aceEntity);
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Change for {}: Old value: {}, new value: {}",
					new Object[] {
							m,
							oldAce.getName(),
							oldAce.getPerms(),
							newPerms });

			return response;
		}
	}

	/**
	 * If aceEntity does not already contain newPerms, then any existing mode
	 * perms are replaced with newPerms. Otherwise, nothing is done.
	 * 
	 * This method guarantees that the perm set of aceEntity is never empty.
	 * 
	 * @param newPerms the new perm for aceEntity
	 * @param aceEntity the existing ace
	 */
	private void updatePerms(ExtPermission newPerms,
			final IHasPerms aceEntity) {
		// Avoid live view of filter by dumping into a new set.
		final Set<PermissionEntity> existingPerms = newHashSet(filter(
				aceEntity.getPerms(),
				compose(equalTo(mode),
						PermissionEntity.getModeName)));
		final PermissionEntity newPerm = permissionAssembler
				.toEntity(newPerms);
		if (existingPerms.contains(newPerm)) {
			// Do nothing
		} else {
			aceEntity.getPerms().add(newPerm);
			aceEntity.getPerms().removeAll(existingPerms);
		}
	}

	public EditAclResponse updateProjectAce(User loggedInUser,
			ExtProjectAce oldAce,
			ExtPermission newPerms)
			throws AuthorizationException {
		final String m = "updateProjectAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = oldAce.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			checkArgument(oldAce.getAceId().isPresent());
			final Long aceId = oldAce.getAceId().get();
			final ProjectAceEntity aceEntity = find(acl.getProjectAces(),
					compose(equalTo(aceId), ProjectAceEntity.getId), null);
			if (aceEntity == null) {
				return new EditAclResponse(false,
						"Could not modify permissions "
								+ newPerms
								+ " for " + oldAce.getProjectGroup()
								+ ". Current permissions not found.");
			}
			final Integer currentAceVer = aceEntity.getVersion();
			final Integer dtoAceVer = oldAce.getAceVersion().orNull();
			if (!dtoAceVer.equals(currentAceVer)) {
				return new EditAclResponse(
						false,
						oldAce.getProjectGroup()
								+ " permissions for this snapshot have changed. Please refresh your view.");
			}
			updatePerms(newPerms, aceEntity);
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Change for {}: Old value: {}, new value: {}",
					new Object[] {
							m,
							oldAce.getProjectGroup(),
							oldAce.getPerms(),
							newPerms });

			return response;
		}
	}

	public EditAclResponse removeUserAce(User loggedInUser,
			ExtUserAce toBeRemoved)
			throws AuthorizationException {
		final String m = "removeUserAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = toBeRemoved.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			checkArgument(toBeRemoved.getAceId().isPresent());
			final Long aceId = toBeRemoved.getAceId().get();
			final Set<ExtUserAceEntity> userAces = acl.getUserAces();
			final ExtUserAceEntity existingAce = find(userAces,
					compose(equalTo(aceId), ExtUserAceEntity.getId));
			if (existingAce == null) {
				return new EditAclResponse(false, "The permissions for user "
						+ toBeRemoved.getName() + " have already been removed.");
			}

			final Integer currentAceVer = existingAce.getVersion();
			final Integer dtoAceVer = toBeRemoved.getAceVersion().orNull();
			if (!dtoAceVer.equals(currentAceVer)) {
				return new EditAclResponse(
						false,
						toBeRemoved.getName()
								+ " permissions for this snapshot have changed. Please refresh your view.");
			}
			clearModePermsOrRemoveAce(
					userAces,
					existingAce);
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Remove {}", m, toBeRemoved.getName());
			return response;
		}
	}

	public EditAclResponse removeProjectAce(User loggedInUser,
			ExtProjectAce toBeRemoved)
			throws AuthorizationException {
		final String m = "removeProjectAce(...)";
		final Integer currentVer = acl.getVersion();
		final Integer dtoVer = toBeRemoved.getAclVersion();
		if (!dtoVer.equals(currentVer)) {
			return new EditAclResponse(false,
					"This snapshot's permissions have changed. Please refresh your view.");
		} else {
			checkArgument(toBeRemoved.getAceId().isPresent());
			final Long aceId = toBeRemoved.getAceId().get();
			final Set<ProjectAceEntity> projectAces = acl.getProjectAces();
			final ProjectAceEntity existingAce = find(projectAces,
					compose(equalTo(aceId), ProjectAceEntity.getId));
			if (existingAce == null) {
				return new EditAclResponse(false,
						"The permissions for project group "
								+ toBeRemoved.getProjectGroup()
								+ " have already been removed.");
			}

			final Integer currentAceVer = existingAce.getVersion();
			final Integer dtoAceVer = toBeRemoved.getAceVersion().orNull();
			if (!dtoAceVer.equals(currentAceVer)) {
				return new EditAclResponse(
						false,
						toBeRemoved.getProjectGroup()
								+ " permissions for this snapshot have changed. Please refresh your view.");
			}
			clearModePermsOrRemoveAce(projectAces, existingAce);
			final EditAclResponse response = new EditAclResponse(true);
			logger.debug("{}: Remove {}", m, toBeRemoved.getProjectGroup());
			return response;
		}
	}

	/**
	 * If existingAce only has perms with mode {@code mode}, removes it from allAces. Otherwise,
	 * just the {@code mode} perms are removed from existingAce.
	 * 
	 * This avoids leaving an ace with an empty perm set which is a constraint
	 * violation.
	 * 
	 * @param allAces all the aces of a given type from the acl.
	 * @param existingAce the ace from allAces we are examining throws
	 *            IllegalSelectorException if allAces does not contain
	 *            existingAce
	 */
	private void clearModePermsOrRemoveAce(
			final Set<? extends IHasPerms> allAces,
			final IHasPerms existingAce) {
		checkArgument(
				allAces.contains(existingAce),
				"allAces must contain existingAce");
		final Set<PermissionEntity> existingPerms = existingAce.getPerms();
		final Set<PermissionEntity> existingModePerms = filter(
				existingAce.getPerms(),
				compose(equalTo(mode),
						PermissionEntity.getModeName));
		if (existingModePerms.equals(existingPerms)) {
			allAces.remove(existingAce);
		} else {
			existingPerms.removeAll(existingModePerms);
		}
	}
}
