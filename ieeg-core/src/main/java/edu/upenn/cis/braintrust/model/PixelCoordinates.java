/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PixelCoordinates implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String X_COLUMN = "pixel_x";

	public static final String Y_COLUMN = "pixel_y";

	private Integer x;

	private Integer y;

	PixelCoordinates() {}

	public PixelCoordinates(final Integer x, final Integer y) {
		this.x = checkNotNull(x);
		this.y = checkNotNull(y);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PixelCoordinates)) {
			return false;
		}
		final PixelCoordinates other = (PixelCoordinates) obj;
		if (x == null) {
			if (other.x != null) {
				return false;
			}
		} else if (!x.equals(other.x)) {
			return false;
		}
		if (y == null) {
			if (other.y != null) {
				return false;
			}
		} else if (!y.equals(other.y)) {
			return false;
		}
		return true;
	}

	@Column(name = X_COLUMN, nullable = false)
	public Integer getX() {
		return x;
	}

	@Column(name = Y_COLUMN, nullable = false)
	public Integer getY() {
		return y;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((x == null) ? 0 : x.hashCode());
		result = prime * result + ((y == null) ? 0 : y.hashCode());
		return result;
	}

	@SuppressWarnings("unused")
	private void setX(final Integer x) {
		this.x = x;
	}

	@SuppressWarnings("unused")
	private void setY(final Integer y) {
		this.y = y;
	}

}
