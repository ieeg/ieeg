package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;

public class InitMontageListener
		implements ServletContextListener {

	private static Logger logger = LoggerFactory
			.getLogger(InitMontageListener.class);

	@Override
	public void contextInitialized(ServletContextEvent sce) {

		final String m = "contextInitialized(...)";

		logger.info("{} Initializing Static Montages... ", m);
		JsonNode rootNode = null;
		try {
			if (AwsUtil.isConfigFromS3()) {

				String configKey =
						AwsUtil.getConfigS3Dir() + "/"
								+ "Montages.json";

				logger.info("{}: reading Montages.json from s3 [{}] [{}]",
						m,
						AwsUtil.getConfigS3Bucket(),
						configKey);

				GetObjectRequest getObjectRequest =
						new GetObjectRequest(
								AwsUtil.getConfigS3Bucket(),
								configKey);

				S3Object s3Object =
						AwsUtil.getS3().getObject(getObjectRequest);
				if (s3Object == null) {
					logger.info("{}: no Montages.json found on S3", m);
				} else {
					try (InputStream stream = s3Object.getObjectContent()) {
						ObjectMapper objectMapper = new ObjectMapper();
						rootNode = objectMapper.readTree(stream);
					}
				}
			} else {
				String configName = BtUtil.getConfigName(
						sce.getServletContext(),
						"Montages.json");
				logger.info("{}: looking for Montages.json at [{}]",
						m,
						configName);
				URL url = getClass().getClassLoader()
						.getResource(configName);
				if (url == null) {
					logger.info(
							"{}: looking for Montages.json at class path root",
							m);
					url = getClass().getClassLoader()
							.getResource("Montages.json");

				} else {
					logger.info("{}: reading Montages.json from {}",
							m,
							url);

				}
				if (url == null) {
					logger.info("{}: no Montages.json found on file system", m);
				} else {
					try (InputStream stream = url.openStream()) {
						ObjectMapper objectMapper = new ObjectMapper();
						// read JSON like DOM Parser
						rootNode = objectMapper.readTree(stream);
					}
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

		if (rootNode != null) {
			IDataSnapshotServer dsServer = DataSnapshotServerFactory
					.getDataSnapshotServer();
			List<EEGMontage> existingMontages = dsServer.getMontages(
					null,
					null);

			try {

				JsonNode montagesNode = rootNode.path("Montages");
				logger.debug("Montages = " + montagesNode);

				Iterator<JsonNode> elements = montagesNode.elements();

				while (elements.hasNext()) {
					JsonNode montageInstance = elements.next();
					logger.debug("Name = " + montageInstance.path("Name"));

					JsonNode pairsNode = montageInstance.path("Pairs");
					Iterator<JsonNode> pairElements = pairsNode.elements();

					EEGMontage newMontage = new EEGMontage(montageInstance
							.path(
									"Name").textValue());
					List<EEGMontagePair> pairList = newMontage.getPairs();
					newMontage.setIsPublic(true);

					while (pairElements.hasNext()) {
						JsonNode pairInstance = pairElements.next();
						EEGMontagePair newPair = new EEGMontagePair(
								pairInstance
										.path("El1").textValue(), pairInstance
										.path("El2")
										.textValue());
						pairList.add(newPair);
					}

					logger.debug("New EEGMontage: " + newMontage);

					
					if (!existingMontages.contains(newMontage)) {
						logger.debug("{} Adding to DB: {}", m, newMontage);
						dsServer.createEditEegMontage(null, newMontage, null);

					} else {
						logger.debug("{} Montage already exists in DB ", m);
					}

				}
			} catch (EegMontageNotFoundException e) {
				logger.error(
						m
								+ ": Unexpected error while initializing montages. Got a not found exception for a new montage",
						e);
			}
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// NOOP
	}

}
