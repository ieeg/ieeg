package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.NotBlank;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
		@NamedQuery(name = EegMontageEntity.BY_USER,
				query = "from EegMontage p "
						+ "where (:user = p.user or p.user is null)"
		),
		@NamedQuery(
				name = EegMontageEntity.BY_USERSNAPSHOT,
				query = "from EegMontage p "
						+ "where (p.user is null) or ((:user = p.user or p.isPublic = true) and :snapshot member of p.snapshots)"
		),
		@NamedQuery(
				name = EegMontageEntity.BY_SNAPSHOT,
				query = "from EegMontage p "
						+ "where :snapshot member of p.snapshots)"
		)
})
@Entity(name = "EegMontage")
@Table(name = "eeg_montage")
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(
						name = EegMontageEntity.ID_COLUMN)
		)
})
public class EegMontageEntity extends LongIdAndVersion
		implements IHasLongId, IEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public final static String TABLE = "eeg_montage";
	public final static String ID_COLUMN = TABLE + "_id";
	public final static String BY_USER = "eegMontage-findByUser";
	public final static String BY_USERSNAPSHOT = "eegMontage-findByUserAndSnapshot";
	public final static String BY_SNAPSHOT = "eegMontage-findBySnapshot";

	private UserEntity user;
	private String name;
	private List<EegMontagePairEntity> pairs = newArrayList();
	private Set<DataSnapshotEntity> snapshots = new HashSet<>();
	private Boolean isPublic = false;

	public EegMontageEntity() {}

	public EegMontageEntity(String name, @Nullable UserEntity user) {

		this.name = checkNotNull(
				name).trim();
		this.user = user;

		checkArgument(
				name.length() >= 1,
				"Montage name too short.");
		checkArgument(
				name.length() < 256,
				"Montage name too long.");

	}

	@NotBlank
	@Size(min = 1, max = 255)
	public String getName() {
		return name;
	}

	@NotNull
	@Column(name = "is_public", columnDefinition = "BIT", length = 1)
	public Boolean getIsPublic() {
		return isPublic;
	}

	@ManyToOne
	@JoinColumn(name = UserEntity.ID_COLUMN)
	public UserEntity getUser() {
		return user;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "parent",
			orphanRemoval = true)
	@OrderBy("idx")
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<EegMontagePairEntity> getPairs() {
		return pairs;
	}

	@ManyToMany
	@JoinTable(
			name = TABLE + "_" + DataSnapshotEntity.TABLE,
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(
					name = DataSnapshotEntity.ID_COLUMN))
	public Set<DataSnapshotEntity> getSnapshots() {
		return snapshots;
	}

	@SuppressWarnings("unused")
	private void setSnapshots(Set<DataSnapshotEntity> snapshots) {
		this.snapshots = snapshots;
	}

	@SuppressWarnings("unused")
	private void setPairs(List<EegMontagePairEntity> pairs) {
		this.pairs = pairs;
	}

	@SuppressWarnings("unused")
	private void setUser(UserEntity user) {
		this.user = user;
	}

	@SuppressWarnings("unused")
	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public static final EntityConstructor<EegMontageEntity, Long, UserEntity> CONSTRUCTOR =
			new EntityConstructor<EegMontageEntity, Long, UserEntity>(
					EegMontageEntity.class,
					new IPersistentKey<EegMontageEntity, Long>() {

						@Override
						public void setKey(EegMontageEntity o, Long newKey) {
							o.setId(newKey);
						}

						@Override
						public Long getKey(EegMontageEntity o) {
							return o.getId();
						}

					},
					new EntityPersistence.ICreateObject<EegMontageEntity, Long, UserEntity>() {

						@Override
						public EegMontageEntity create(Long id,
								UserEntity optParent) {
							// TODO: no acl?
							// null image type
							return new EegMontageEntity("_" + id, optParent);
						}
					}
			);

	@Override
	public EntityConstructor<EegMontageEntity, Long, UserEntity> tableConstructor() {
		return CONSTRUCTOR;
	}

}
