package edu.upenn.cis.braintrust.imodel;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.upenn.cis.db.habitat.persistence.constructors.RelationshipConstructor;

public interface IRelationship extends IPersistentObject {
	@Transient
	@JsonIgnore
	@Override
	public RelationshipConstructor<? extends IRelationship, ?, ? extends IEntity, ? extends IEntity> tableConstructor();
}
