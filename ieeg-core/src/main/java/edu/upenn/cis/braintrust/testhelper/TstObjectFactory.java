/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.testhelper;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static java.util.Collections.singleton;

import java.util.Date;
import java.util.Random;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;

import edu.upenn.cis.braintrust.model.AnalysisTask;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.model.AnimalEntity;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.DrugAdminRelationship;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.ModeEntity;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.PixelCoordinates;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.SpeciesEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.StrainEntity;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.JobStatus;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.ParallelDto;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.shared.IUser;

/**
 * A factory making objects for tests. This is in {@code src/main} rather than
 * {@code src/test} so that dependent projects can use it without us creating a
 * separate test-jar.
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 * 
 */
public final class TstObjectFactory {

	private static Random random = new Random();

	private static PermissionDomainEntity domain =
			new PermissionDomainEntity(
					PermissionDomainEntity.PERM_DOMAIN_WILDCARD);
	private static ModeEntity mode =
			new ModeEntity(domain, CorePermDefs.CORE_MODE_NAME, null);

	public static <T extends Enum<T>> T randomEnum(final Class<T> enumType) {
		final T[] enumConstants = enumType.getEnumConstants();
		return enumConstants[random.nextInt(enumConstants.length)];
	}

	public static int randomNonnegInt() {
		// These are frequently added together to get a new non-negative int, so
		// divide by three to make sure there is no overflow.
		return random.nextInt(Integer.MAX_VALUE / 3);
	}

	private PermissionEntity readPerm =
			new PermissionEntity(
					mode,
					CorePermDefs.READ_PERMISSION_NAME, "Can read",
					1,
					Boolean.TRUE,
					Boolean.TRUE,
					Boolean.TRUE,
					null);

	private PermissionEntity ownerPerm =
			new PermissionEntity(
					mode,
					CorePermDefs.READ_PERMISSION_NAME,
					"Owner",
					3,
					Boolean.FALSE,
					Boolean.TRUE,
					Boolean.TRUE,
					null);

	private long seqNo = 1;

	private boolean setIds = false;

	public TstObjectFactory() {
		this.setIds = false;
	}

	public TstObjectFactory(boolean setIds) {
		this.setIds = setIds;
	}

	public TstObjectFactory(
			PermissionEntity readPerm,
			PermissionEntity ownerPerm) {
		this.setIds = false;
		this.readPerm = readPerm;
		this.ownerPerm = ownerPerm;
	}

	private Long getNextSeqNo() {
		if (setIds) {
			return seqNo++;
		}
		return null;
	}

	public PermissionEntity getOwnerPerm() {
		return ownerPerm;
	}

	public PermissionEntity getReadPerm() {
		return readPerm;
	}

	private Integer getVersion() {
		if (setIds) {
			return 0;
		}
		return null;
	}

	public AnalyzedDataSnapshot newAnalyzedDataSnapshot(
			DataSnapshotEntity input) {
		checkState(setIds, "must be setting ids to call this method");
		final AnalyzedDataSnapshot analyzedDataSnapshot =
				new AnalyzedDataSnapshot(newUuid(), newDataset(), input);
		return analyzedDataSnapshot;
	}

	public AnimalEntity newAnimal(StrainEntity strain) {
		final AnimalEntity animal = new AnimalEntity(
				newUuid(),
				OrganizationsForTests.JONES,
				strain);
		return animal;
	}

	public Contact newContact(
			ContactType contactType,
			TimeSeriesEntity trace) {
		final Contact contact = new Contact(
				contactType,
				newMriCoordinates(),
				newTalairachCoordinates(),
				trace);
		contact.setId(getNextSeqNo());
		return contact;
	}

	public Contact newContact(TimeSeriesEntity trace) {
		return newContact(randomEnum(ContactType.class), trace);
	}

	public Dataset newDataset() {
		final UserEntity creator = newUserEntity();
		final Dataset dataset = new Dataset(
				newUuid(),
				creator,
				newExtAclEntity());
		dataset.setId(getNextSeqNo());
		return dataset;
	}

	public Dataset newDataset(
			UserEntity creator) {
		return new Dataset(
				newUuid(),
				creator,
				newExtAclEntity());
	}

	public Dataset newDataset(
			UserEntity creator,
			ExtAclEntity acl) {
		return new Dataset(newUuid(), creator, acl);
	}

	public DataSnapshot newDataSnapshotDto() {
		DataSnapshot dsDto = new DataSnapshot(newUuid(), false);
		return dsDto;
	}

	public DrugEntity newDrug() {
		DrugEntity drug = new DrugEntity(newUuid());
		if (setIds) {
			drug.setId(getNextSeqNo());
		}
		return drug;
	}

	public DrugAdminRelationship newDrugAdmin(ExperimentEntity parent, DrugEntity drug) {
		DrugAdminRelationship drugAdmin = new DrugAdminRelationship(
				parent,
				drug,
				null,
				null);
		if (setIds) {
			drugAdmin.setId(getNextSeqNo());
		}
		return drugAdmin;
	}

	public EegStudy newEegStudy() {
		final EegStudy study = new EegStudy(
				newUuid(),
				newExtAclEntity(),
				randomEnum(EegStudyType.class),
				randomNonnegInt(),
				newRecording(),
				newUuid());
		return study;
	}
	
	public EegStudy newEegStudy(ExtAclEntity extAclEntity) {
		final EegStudy study = new EegStudy(
				newUuid(),
				extAclEntity,
				randomEnum(EegStudyType.class),
				randomNonnegInt(),
				newRecording(),
				newUuid());
		return study;
	}

	public ContactGroup newContactGroup() {
		final ContactGroup e = new ContactGroup(
				newUuid(),
				newUuid(),
				random.nextDouble(),
				random.nextDouble(),
				random.nextDouble(),
				random.nextBoolean(),
				randomEnum(Side.class),
				randomEnum(Location.class),
				random.nextDouble(),
				null,
				null);
		e.setId(getNextSeqNo());
		return e;
	}

	public ExperimentEntity newExperiment(
			AnimalEntity parent,
			ExtAclEntity extAcl) {
		final ExperimentEntity experiment = new ExperimentEntity(
				parent,
				newUuid(),
				extAcl,
				newRecording(),
				null,
				null,
				null,
				null,
				null, null);
		return experiment;
	}
	
	public ExperimentEntity newExperimentNoParent(
			ExtAclEntity extAcl) {
		final ExperimentEntity experiment = new ExperimentEntity(
				null,
				newUuid(),
				extAcl,
				newRecording(),
				null,
				null,
				null,
				null,
				null, null);
		return experiment;
	}

	public ExtAclEntity newExtAclEntity() {
		ExtAclEntity acl = new ExtAclEntity();
		acl.setId(getNextSeqNo());
		acl.setVersion(getVersion());
		return acl;
	}

	public ExtAclEntity newExtAclEntity(UserEntity user) {
		ExtAclEntity acl = newExtAclEntity();
		new ExtUserAceEntity(
				user,
				acl,
				ownerPerm);
		acl.setId(getNextSeqNo());
		acl.setVersion(getVersion());
		return acl;
	}

	public ExtAclEntity newExtAclEntityUserOwnedAndWorldReadable(
			final UserEntity user) {
		ExtAclEntity acl = newExtAclEntity();
		new ExtUserAceEntity(
				user,
				acl,
				ownerPerm);
		acl.getWorldAce().add(readPerm);
		acl.setId(getNextSeqNo());
		acl.setVersion(getVersion());
		return acl;
	}

	public ExtAclEntity newExtAclEntityWorldReadable() {
		ExtAclEntity acl = new ExtAclEntity(readPerm);
		acl.setId(getNextSeqNo());
		acl.setVersion(getVersion());
		return acl;
	}

	public ExtProjectAce newExtProjectAce(String targetId) {
		return new ExtProjectAce(
				newUuid(),
				newUuid(),
				randomEnum(ProjectGroupType.class),
				targetId,
				Integer.valueOf(TstObjectFactory.randomNonnegInt()),
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
	}

	public ExtUserAce newExtUserAce(User user, String targetId) {
		return new ExtUserAce(
				user.getUserId(),
				user.getUsername(),
				targetId,
				Integer.valueOf(TstObjectFactory.randomNonnegInt()),
				Long.valueOf(TstObjectFactory.randomNonnegInt()),
				Integer.valueOf(TstObjectFactory.randomNonnegInt()));
	}

	public ExtWorldAce newExtWorldAce(String targetId) {
		return new ExtWorldAce(
				targetId,
				Long.valueOf(randomNonnegInt()),
				Integer.valueOf(randomNonnegInt()));
	}

	public HospitalAdmission newHospitalAdmission() {
		HospitalAdmission admission = new HospitalAdmission(
				random.nextInt());
		return admission;
	}

	public ImageEntity newImage() {
		final ImageType type = randomEnum(ImageType.class);
		final ImageEntity image = new ImageEntity(newUuid(), type);
		return image;
	}

	public ImageEntity newImage(final DataSnapshotEntity ds) {
		final ImageType type = randomEnum(ImageType.class);
		final ImageEntity image = new ImageEntity(newUuid(), type);
		ds.getImages().add(image);
		return image;
	}

	public ImagedContact newImagedContact(ImageEntity image, Contact contact) {
		return new ImagedContact(
				newPixelCoordinates(),
				image,
				contact);
	}

	public JobEntity newJob() {
		final JobEntity job = new JobEntity(Long.valueOf(randomNonnegInt()),
				newUuid(), newUuid(),
				randomEnum(JobStatus.class), randomEnum(ParallelDto.class),
				new Date(randomNonnegInt() * 1000L), newUuid());
		return job;
	}

	public MriCoordinates newMriCoordinates() {
		return new MriCoordinates(random.nextDouble(), random.nextDouble(),
				random.nextDouble());
	}

	public Patient newPatient() {
		return new Patient(
				newUuid(),
				OrganizationsForTests.SPIKE,
				randomEnum(Handedness.class),
				randomEnum(Ethnicity.class),
				randomEnum(Gender.class),
				newUuid(),
				random.nextBoolean(),
				random.nextBoolean(),
				random.nextBoolean(),
				random.nextInt(),
				newUuid());
	}

	public PixelCoordinates newPixelCoordinates() {
		return new PixelCoordinates(
				randomNonnegInt(),
				randomNonnegInt());
	}

	public ProjectEntity newProject(boolean isComplete) {
		return new ProjectEntity(
				newUuid(),
				newUuid(),
				isComplete);
	}

	public Recording newRecording() {
		long startTime = randomNonnegInt();
		long endTime = startTime + randomNonnegInt();
		return new Recording(
				newUuid(),
				newUuid(),
				Long.valueOf(startTime),
				Long.valueOf(endTime),
				null,
				null,
				null,
				null);
	}

	public SessionEntity newSession(UserEntity user) {
		return new SessionEntity(user);
	}

	public SessionToken newSessionToken() {
		return new SessionToken(newUuid());
	}

	public SpeciesEntity newSpecies() {
		return new SpeciesEntity(newUuid());
	}

	public StimRegionEntity newStimRegion() {
		return new StimRegionEntity(newUuid());
	}

	public StimTypeEntity newStimType() {
		return new StimTypeEntity(newUuid());
	}

	public StrainEntity newStrain(SpeciesEntity species) {
		return new StrainEntity(
				species,
				newUuid());
	}

	public TalairachCoordinates newTalairachCoordinates() {
		return new TalairachCoordinates(random.nextDouble(),
				random.nextDouble(), random.nextDouble());
	}

	public AnalysisTask newTask(JobEntity parent) {
		final long startTime = randomNonnegInt();
		final long endTime = startTime + randomNonnegInt();
		final AnalysisTask task = new AnalysisTask(
				parent,
				newUuid(),
				randomEnum(TaskStatus.class),
				Long.valueOf(startTime),
				Long.valueOf(endTime),
				newUuid(),
				new Date(randomNonnegInt() * 1000L));
		final int channelCount = random.nextInt(100) + 1;
		for (int i = 0; i < channelCount; i++) {
			task.getTsPubIds().add(newUuid());
		}

		return task;
	}

	public TaskDto newTaskDto() {
		final long startTime = randomNonnegInt();
		final long endTime = startTime + randomNonnegInt();
		final TaskDto taskDto = new TaskDto(
				newUuid(),
				randomEnum(TaskStatus.class),
				startTime,
				endTime,
				Long.valueOf(randomNonnegInt()),
				newUuid());
		final int channelCount = random.nextInt(100) + 1;
		for (int i = 0; i < channelCount; i++) {
			taskDto.getTimeSeriesIds().add(newUuid());
		}
		return taskDto;
	}

	public TaskDto newTaskDtoUnscheduledNoClob() {
		final TaskDto taskDto = newTaskDto();
		taskDto.setStatus(TaskStatus.UNSCHEDULED);
		taskDto.setRunStartTimeMillis(null);
		taskDto.setWorker(null);
		return taskDto;
	}

	/**
	 * Make a {@link TimeSeriesEntity} filled in with random data.
	 * <p>
	 * At the moment, only fills in non-nullable fields.
	 * 
	 * @return a {@link TimeSeriesEntity} filled in with random data
	 */
	public TimeSeriesEntity newTimeSeries() {
		return new TimeSeriesEntity(newUuid(), newUuid());
	}

	public TimeSeriesEntity newTimeSeries(final Dataset parent) {
		final TimeSeriesEntity ts = new TimeSeriesEntity(newUuid(), newUuid());
		parent.getTimeSeries().add(ts);
		return ts;
	}

	public TimeSeriesDto newTimeSeriesDto() {
		return new TimeSeriesDto(newUuid(), newUuid(), newUuid());
	}

	public TimeSeriesDto newTimeSeriesDto(final String label, final String revId) {
		final TimeSeriesDto timeSeriesDto = new TimeSeriesDto(revId, label,
				newUuid());
		return timeSeriesDto;
	}

	public ToolEntity newTool(final UserEntity user) {
		return new ToolEntity(
				newExtAclEntity(),
				newUuid(),
				// newUuid(),
				user,
				// newUuid(),
				// ParallelDto.PAR_BLOCK,
				newUuid(),
				newUuid(),
				newUuid());
	}

	public ToolDto newToolDto(final UserId authorId) {
		return new ToolDto(
				newUuid(),
				// newUuid(),
				authorId,
				newUuid(),
				// newUuid(),
				// LicenseDto.APACHE,
				// ParallelDto.PAR_BLOCK,
				newUuid(),
				newUuid(),
				null,
				// newUuid(),
				newUuid());
	}

	public TsAnnotationDto newTsAnnDtoNoPubIdNoLayer(
			final TimeSeriesDto annotated) {
		final int startInt = randomNonnegInt();
		final int endInt = startInt + randomNonnegInt();
		final TsAnnotationDto dto = new TsAnnotationDto(
				newHashSet(annotated),
				newUuid(),
				Long.valueOf(startInt),
				Long.valueOf(endInt),
				newUuid(),
				null,
				newUuid(),
				newUuid(),
				null,
				null);
		return dto;
	}

	public TsAnnotationEntity newTsAnnotation(
			final Set<TimeSeriesEntity> annotated,
			final DataSnapshotEntity parent,
			final UserEntity creator) {
		return new TsAnnotationEntity(
				newUuid(),
				0L,
				1L,
				annotated,
				newUuid(),
				creator,
				parent,
				TsAnnotationEntity.getLayerDefault(parent),
				newUuid(),
				"blue");
	}

	public TsAnnotationEntity newTsAnnotation(
			final TimeSeriesEntity annotated,
			final DataSnapshotEntity parent,
			final UserEntity creator) {
		return new TsAnnotationEntity(
				newUuid(),
				0L,
				1L,
				singleton(annotated),
				newUuid(),
				creator,
				parent,
				TsAnnotationEntity.getLayerDefault(parent),
				newUuid(),
				"blue");
	}

	public TsAnnotationDto newTsAnnotationDto(
			Set<TimeSeriesDto> timeSeriesDtos) {
		final int startInt = randomNonnegInt();
		final Long start = Long.valueOf(startInt);
		final Long end = Long.valueOf(startInt + randomNonnegInt());
		final TsAnnotationDto tsAnnotationDto = new TsAnnotationDto(
				timeSeriesDtos,
				newUuid(),
				start,
				end,
				newUuid(),
				Integer.toString(randomNonnegInt()),
				newUuid(),
				newUuid(),
				newUuid(),
				"green");
		return tsAnnotationDto;
	}

	public User newUserAdminEnabled() {
		return new User(
				newUserId(),
				newUuid(),
				newUuid(),
				IUser.Status.ENABLED,
				singleton(Role.ADMIN),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
	}

	public User newUserDataEntererEnabled() {
		return new User(
				newUserId(),
				newUuid(),
				newUuid(),
				IUser.Status.ENABLED,
				singleton(Role.DATA_ENTERER),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));

	}

	public UserEntity newUserEntity() {
		return new UserEntity(new UserId(Long.valueOf(randomNonnegInt())));
	}

	public UserEntity newUserEntity(UserId id) {
		final UserEntity user = new UserEntity(id);
		return user;
	}

	public UserId newUserId() {
		return new UserId(randomNonnegInt());
	}

	public User newUserRegularEnabled() {
		return new User(
				newUserId(),
				newUuid(),
				newUuid(),
				IUser.Status.ENABLED,
				singleton(Role.USER),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
	}

	public User newUserRegularEnabled(String plaintextPassword) {
		final String hashedPassword = DigestUtils.md5Hex(plaintextPassword);
		return new User(
				newUserId(),
				newUuid(),
				hashedPassword,
				IUser.Status.ENABLED,
				singleton(Role.USER),
				null,
				"email",
				new UserProfile("x", "y", "z", "w", "v", null, null, null, null, null, null, null, null, null, null));
	}

	public Double randomDouble() {
		return random.nextDouble();
	}
}
