/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "RecordingObjectTask")
@Table(name = RecordingObjectTaskEntity.TABLE)
@AttributeOverrides({
		@AttributeOverride(
				name = "id",
				column = @Column(name = RecordingObjectTaskEntity.ID_COLUMN)
		)
})
@NamedQueries(@NamedQuery(
		name = RecordingObjectTaskEntity.FIND_TASK,
		query = "from RecordingObjectTask t " +
				"where t.recordingObject = :recordingObject"))
public class RecordingObjectTaskEntity extends LongIdAndVersion {

	public static final String TABLE = "recording_object_task";
	public static final String ID_COLUMN = TABLE + "_id";
	public final static String FIND_TASK = "RecordingObjectTask-findTask";
	
	private RecordingObjectEntity recordingObject;
	private String status;
	private Set<RecordingObjectTaskMetadataEntity> metadataSet = newHashSet();

	/**
	 * For JPA.
	 */
	RecordingObjectTaskEntity() {}

	public RecordingObjectTaskEntity(
			RecordingObjectEntity recordingObject,
			String status) {
		this.recordingObject = checkNotNull(recordingObject);
		this.status = checkNotNull(status);
	}

	@OneToOne(
			fetch = FetchType.LAZY,
			optional = false)
	@JoinColumn(
			name = RecordingObjectEntity.ID_COLUMN,
			unique = true)
	@NotNull
	public RecordingObjectEntity getRecordingObject() {
		return recordingObject;
	}

	@Nullable
	@Transient
	public String getMetadata() {
		if (metadataSet.isEmpty()) {
			return null;
		}
		return getOnlyElement(metadataSet).getValue();
	}

	/**
	 * Arbitrary json metadata associated with this recording object task
	 * <p>
	 * Use a one-to-many to get lazy loading. This seemed better than build time
	 * bytecode instrumentation because that loads all lazy props when you hit
	 * any of them.
	 * 
	 * @return the arbitrary json associated with this object
	 */
	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			fetch = FetchType.LAZY,
			orphanRemoval = true)
	private Set<RecordingObjectTaskMetadataEntity> getMetadataSet() {
		return metadataSet;
	}

	@NotNull
	@Size(min = 1, max = 255)
	public String getStatus() {
		return status;
	}

	@SuppressWarnings("unused")
	private void setRecordingObject(RecordingObjectEntity recordingObject) {
		this.recordingObject = recordingObject;
	}

	public void setMetadata(@Nullable String metadata) {
		if (metadata == null) {
			if (!metadataSet.isEmpty()) {
				metadataSet.clear();
			}
		} else {
			if (metadataSet.isEmpty()) {
				metadataSet.add(new RecordingObjectTaskMetadataEntity(metadata,
						this));
			} else {
				getOnlyElement(metadataSet).setValue(metadata);
			}
		}
	}

	@SuppressWarnings("unused")
	private void setMetadataSet(
			Set<RecordingObjectTaskMetadataEntity> metadataSet) {
		this.metadataSet = metadataSet;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
