/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.drupal.model;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.google.common.base.Objects;
import com.google.common.primitives.Ints;

@Entity
@Table(name = "USERS")
//@Immutable
@NamedQueries({
		@NamedQuery(name = DrupalUser.FIND_BY_NAME,
				query = "from DrupalUser u where u.name=:name"),
		@NamedQuery(name = DrupalUser.FIND_BY_EMAIL,
				query = "from DrupalUser u where u.mail=:mail") })
public class DrupalUser {

	public static final String FIND_BY_NAME = "DrupalUser-findByName";
	public static final String FIND_BY_EMAIL = "DrupalUser-findByEmail";

	public static final Byte DISABLED = 0;
	public static final Byte ENABLED = 1;

	public static final String ID_COLUMN = "uid";

	private Long uid;
	private String name;
	private String pass;
	private String picture;
	private String mail;
	private Byte status;
	private Integer created;
	private Set<DrupalRole> roles = newHashSet();
	private Map<DrupalProfileField, DrupalProfileValue> profile = newHashMap();

	
	public DrupalUser() {
		created = Ints.checkedCast(TimeUnit.SECONDS.convert(new Date().getTime(), TimeUnit.MILLISECONDS));
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DrupalUser)) {
			return false;
		}
		DrupalUser other = (DrupalUser) obj;
		if (!Objects.equal(uid, other.getUid())) {
			return false;
		}
		return true;
	}

	public Integer getCreated() {
		return created;
	}
	
	public String getMail() {
		return mail;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	public String getPicture() {
		return picture;
	}

	@OneToMany(
			mappedBy = "user",
			fetch = FetchType.EAGER)
	@MapKeyJoinColumn(
			name = DrupalProfileField.ID_COLUMN,
			referencedColumnName = DrupalProfileValue.Id.FIELD_ID_COLUMN)
	public Map<DrupalProfileField, DrupalProfileValue> getProfile() {
		return profile;
	}

	/**
	 * @return the roles
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "users_roles",
			joinColumns = @JoinColumn(name = ID_COLUMN),
			inverseJoinColumns = @JoinColumn(name = "rid"))
//	@Immutable
	public Set<DrupalRole> getRoles() {
		return roles;
	}

	/**
	 * @return the status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @return the uid
	 */
	@Id
	@NaturalId
	@GeneratedValue
	@Column(columnDefinition = "INT UNSIGNED", name = ID_COLUMN)
	public Long getUid() {
		return uid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @param pass the pass to set
	 */
	public void setPass(final String pass) {
		this.pass = pass;
	}

	public void setPicture(final String photo) {
		this.picture = photo;
	}

	public void setProfile(Map<DrupalProfileField, DrupalProfileValue> profile) {
		this.profile = profile;
	}

	/**
	 * @param roles the roles to set
	 */
	@SuppressWarnings("unused")
	private void setRoles(final Set<DrupalRole> roles) {
		this.roles = roles;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(final Byte status) {
		this.status = status;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(final Long uid) {
		this.uid = uid;
	}
	
	@SuppressWarnings("unused")
	private void setCreated(Integer created) {
		this.created = created;
	}

}
