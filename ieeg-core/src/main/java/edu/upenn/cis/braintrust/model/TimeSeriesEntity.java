/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.IHasLabel;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
		@NamedQuery(
				name = TimeSeriesEntity.IS_IN_A_DATASET,
				query = "select 1 "
						+ "from Dataset ds "
						+ "join ds.timeSeries ts "
						+ "where ts = :ts"),
		@NamedQuery(
				name = TimeSeriesEntity.TIME_SERIES_BY_RECORDING,
				query = "select t "
						+ "from Recording r "
						+ "join r.contactGroups g "
						+ "join g.contacts c "
						+ "join c.trace t "
						+ "where r = :recording"),
		@NamedQuery(
				name = TimeSeriesEntity.BY_STUDY,
				query =
				"select tr from EegStudy s join s.recording r join r.contactGroups g join g.contacts c join c.trace tr where s.id = :dsId order by tr.id"),
		@NamedQuery(
				name = TimeSeriesEntity.BY_EXPERIMENT,
				query =
				"select tr from Experiment e join e.recording r join r.contactGroups g join g.contacts c join c.trace tr where e.id = :dsId order by tr.id"),
		@NamedQuery(
				name = TimeSeriesEntity.BY_PATH,
				query =
				"select ts from TimeSeries ts where ts.fileKey = :path"),
		@NamedQuery(
				name = TimeSeriesEntity.BY_DATASET,
				query =
				"select ts from Dataset d join d.timeSeries ts where d.id = :dsId order by ts.id")
})
@Entity(name = "TimeSeries")
@Table(name = TimeSeriesEntity.TABLE,
		uniqueConstraints=@UniqueConstraint(columnNames={"fileKey"}))
public class TimeSeriesEntity
		implements IHasLabel, IHasLongId, IHasPubId, IEntity {

	public static final String TABLE = "time_series";
	public static final String ID_COLUMN = TABLE + "_id";

	public static final String IS_IN_A_DATASET = "TimeSeries-isInADataset";
	public static final String TIME_SERIES_BY_RECORDING = "TimeSeries-findTimeSeriesByRec";
	public static final String BY_STUDY = "TimeSeries-byStudy";
	public static final String BY_EXPERIMENT = "TimeSeries-byExperiment";
	public static final String BY_DATASET = "TimeSeries-byDataset";
	public static final String BY_PATH = "TimeSeries-byPath";

	private Long id;
	private Integer version;
	private String pubId = newUuid();
	private Date createTime = new Date();
	private String label;
	private String fileKey;
	private TimeSeriesInfo info;
	
	public TimeSeriesEntity() {}

	public TimeSeriesEntity(
			String label,
			String fileKey) {
		this.label = checkNotNull(label);
		this.fileKey = checkNotNull(fileKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TimeSeriesEntity)) {
			return false;
		}
		TimeSeriesEntity other = (TimeSeriesEntity) obj;
		if (!Objects.equal(pubId, other.getPubId())) {
			return false;
		}
		return true;
	}

	/**
	 * @return the timestamp
	 */
	@Column(name = "create_time")
	@NotNull
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @return the fileKey
	 */
	@Column(unique = true)
	@NotNull
	public String getFileKey() {
		return fileKey;
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@OneToOne(
			mappedBy = "timeSeries",
			fetch = FetchType.EAGER,//LAZY,
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			optional = true)
	public TimeSeriesInfo getInfo() {
		return info;
	}

	@Override
	@Column
	@NotNull
	public String getLabel() {
		return label;
	}

	@Override
	@NaturalId
	@Column(name = "pub_id")
	@Size(min = 36, max = 36)
	@NotNull
	public String getPubId() {
		return pubId;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pubId == null) ? 0 : pubId.hashCode());
		return result;
	}

	/**
	 * @param createTime the timestamp to set
	 */
	@SuppressWarnings("unused")
	private void setCreateTime(final Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @param fileKey the fileKey to set
	 */
	public void setFileKey(final String fileKey) {
		this.fileKey = fileKey;
	}

	@VisibleForTesting
	void setId(Long id) {
		this.id = id;
	}

	public void setInfo(TimeSeriesInfo info) {
		this.info = info;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}

	public static final EntityConstructor<TimeSeriesEntity, String, String> CONSTRUCTOR =
		new EntityConstructor<TimeSeriesEntity,String,String>(TimeSeriesEntity.class,
				new IPersistentObjectManager.IPersistentKey<TimeSeriesEntity,String>() {

			@Override
			public String getKey(TimeSeriesEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(TimeSeriesEntity o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<TimeSeriesEntity,String,String>() {

			@Override
			public TimeSeriesEntity create(String label, String fileKey) {
				return new TimeSeriesEntity(label, fileKey);
			}
		}
				);

	@Override
	public EntityConstructor<TimeSeriesEntity, String, String> tableConstructor() {
		return CONSTRUCTOR;
	}

}
