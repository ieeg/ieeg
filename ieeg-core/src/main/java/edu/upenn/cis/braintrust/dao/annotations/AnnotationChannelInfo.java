/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.annotations;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.base.Objects;

import edu.upenn.cis.braintrust.shared.TimeSeriesId;

@Immutable
public final class AnnotationChannelInfo
		implements Comparable<AnnotationChannelInfo> {

	private final String clientSideFileName;
	private final TimeSeriesId id;
	private final String fileKey;

	public AnnotationChannelInfo(
			String clientSideFileName,
			TimeSeriesId id,
			String fileKey) {
		this.clientSideFileName = checkNotNull(clientSideFileName);
		this.id = checkNotNull(id);
		this.fileKey = checkNotNull(fileKey);
	}

	@Override
	public int compareTo(AnnotationChannelInfo o) {
		return this.getClientSideFileName().compareTo(o.getClientSideFileName());
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AnnotationChannelInfo)) {
			return false;
		}
		AnnotationChannelInfo other = (AnnotationChannelInfo) obj;
		if (!Objects.equal(id, other.id)) {
			return false;
		}
		return true;
	}

	public String getFileKey() {
		return fileKey;
	}

	public TimeSeriesId getId() {
		return id;
	}

	public String getClientSideFileName() {
		return clientSideFileName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
}
