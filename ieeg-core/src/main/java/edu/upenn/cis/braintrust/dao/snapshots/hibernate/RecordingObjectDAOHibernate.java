/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.snapshots.IRecordingObjectDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.RecordingObjectEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;

/**
 * @author John Frommeyer
 *
 */
public class RecordingObjectDAOHibernate extends
		GenericHibernateDAOLongId<RecordingObjectEntity> implements
		IRecordingObjectDAO {
	
	public RecordingObjectDAOHibernate() {}
	
	public RecordingObjectDAOHibernate(Session session) {
		setSession(session);
	}

	@Override
	public RecordingObjectEntity findByName(String name) {
		return (RecordingObjectEntity) getSession()
				.getNamedQuery(RecordingObjectEntity.FIND_BY_NAME)
				.setParameter("name", name)
				.uniqueResult();
	}

}
