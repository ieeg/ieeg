/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.datasnapshot.assembler.webapp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.model.DrugEntity;
import edu.upenn.cis.braintrust.shared.dto.GwtDrug;
import edu.upenn.cis.braintrust.shared.exception.StaleObjectException;

public class DrugAssembler {

	public GwtDrug buildDto(
			final DrugEntity drug) {
		checkNotNull(drug);
		GwtDrug gwtDrug = new GwtDrug(
				drug.getLabel(),
				drug.getId(),
				drug.getVersion());
		return gwtDrug;
	}

	public DrugEntity buildEntity(
			final GwtDrug gwtDrug) {
		checkNotNull(gwtDrug);
		checkArgument(gwtDrug.getId() == null);
		checkArgument(gwtDrug.getVersion() == null);
		final DrugEntity drug = new DrugEntity(
				gwtDrug.getLabel());
		return drug;
	}

	public void modifyEntity(
			final DrugEntity drug,
			final GwtDrug gwtDrug)
			throws StaleObjectException {
		checkNotNull(gwtDrug);
		checkNotNull(drug);
		final Long dtoId = gwtDrug.getId();
		final Long entityId = drug.getId();
		if (!dtoId.equals(entityId)) {
			throw new IllegalArgumentException("DTO id: [" + dtoId
					+ "] does not match entity id: [" + entityId + "].");
		}
		final Integer dtoV = gwtDrug.getVersion();
		final Integer entityV = drug.getVersion();
		if (!dtoV.equals(entityV)) {
			throw new StaleObjectException(
					"Version mismatch for drug ["
							+ entityId + "]. DTO version: [" + dtoV
							+ "], DB version: [" + entityV + "].");
		}
		copyToEntity(drug, gwtDrug);
	}

	private void copyToEntity(
			final DrugEntity drug,
			final GwtDrug gwtDrug) {
		drug.setLabel(gwtDrug.getLabel());
	}

	
}
