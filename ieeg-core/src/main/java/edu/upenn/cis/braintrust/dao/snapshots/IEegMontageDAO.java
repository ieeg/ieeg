package edu.upenn.cis.braintrust.dao.snapshots;

import java.util.List;

import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.EegMontageEntity;
import edu.upenn.cis.braintrust.model.UserEntity;

public interface IEegMontageDAO extends IDAO<EegMontageEntity, Long>{

  List<EegMontageEntity> findForUser(UserEntity btUser);
  
  List<EegMontageEntity> findForUserAndSnapshot(UserEntity btUser, DataSnapshotEntity snapshot);
  
  List<EegMontageEntity> findForSnapshot(DataSnapshotEntity snapshot);

}
