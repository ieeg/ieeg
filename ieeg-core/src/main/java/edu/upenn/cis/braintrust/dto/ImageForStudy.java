/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import edu.upenn.cis.braintrust.shared.ImageType;



@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class ImageForStudy {

	private String imageFileKey;
	private ImageType imageType;
	
	public ImageForStudy() { } 

	/**
	 * @return the imageFileKey
	 */
	@XmlElement
	public String getImageFileKey() {
		return imageFileKey;
	}

	/**
	 * @return the imageType
	 * 
	 * @deprecated instead use {@link #getImageTypeEnum()}
	 */
	@Deprecated
	@XmlElement
	public String getImageType() {
		return imageType.name();
	}

	public ImageType getImageTypeEnum() {
		return imageType;
	}

	/**
	 * @param imageFileKey the imageFileKey to set
	 */
	public void setImageFileKey(final String imageFileKey) {
		this.imageFileKey = imageFileKey;
	}

	@Deprecated
	public void setImageType(final String imageType) {
		this.imageType = ImageType.valueOf(imageType);
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageTypeEnum(final ImageType imageType) {
		this.imageType = imageType;
	}

}
