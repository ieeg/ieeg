/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.validate;

import static com.google.common.collect.Collections2.transform;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import com.google.common.base.Function;
import com.google.common.base.Joiner;

import edu.upenn.cis.braintrust.shared.exception.BtConstraintViolationException;

/**
 * Helps get nice descriptions of constraint violations.
 * 
 * @author John Frommeyer
 * 
 */
public class ConstraintViolationMessageUtil {

	public static Function<ConstraintViolation<?>, String> cVToString = new Function<ConstraintViolation<?>, String>() {

		@Override
		public String apply(ConstraintViolation<?> cv) {
			return "Invalid value[" + cv.getInvalidValue() + "] for property "
					+ cv.getPropertyPath() + " of " + cv.getRootBeanClass()
					+ ": " + cv.getMessage() + ".";
		}
	};

	private ConstraintViolationMessageUtil() {
		throw new AssertionError(
				"Cannot instantiate ConstraintViolationMessageUtil");
	}

	public static void propagateCveAsBtcve(Throwable t) {
		if (t instanceof ConstraintViolationException) {
			ConstraintViolationException cve = (ConstraintViolationException) t;
			throw new BtConstraintViolationException(toString(cve), cve);
		}
	}

	public static String toString(final ConstraintViolationException e) {
		final Set<ConstraintViolation<?>> constraintViolations = e
				.getConstraintViolations();
		Joiner joiner = Joiner.on(", ").skipNulls();
		return joiner.join(transform(constraintViolations, cVToString));
	}
}
