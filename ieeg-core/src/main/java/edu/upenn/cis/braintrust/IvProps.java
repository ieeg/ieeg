/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Throwables.propagate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.google.common.math.LongMath;

/**
 * Utility class for reading ieegview.properties from the classpath.
 * <p>
 * Intended to be thread-safe.
 * 
 * @author Sam Donnelly
 */
@ThreadSafe
public final class IvProps {
	
	private static Logger logger = LoggerFactory.getLogger(IvProps.class);
	
	public static final String PROPERTIES_FILE = "ieegview.properties";
	
	public static final String COORDINATOR = "coordinator";
	public static final String COORDINATOR_DFLT = "localhost:2181";
	public static final String MAILSERVER = "mailserver";
	public static final String MAILSERVER_DFLT = "mail"; 
	public static final String MAILPORT = "mailport";
	public static final int MAILPORT_DFLT = 587;
	public static final String MAILSSL = "mailSsl";
	public static final boolean MAILSSL_DFLT = true;
	public static final String MAILID = "mailid";
	public static final String MAILID_DFLT = "ieeg"; 
	public static final String MAILPASS = "mailpass";
	public static final String MAILPASS_DFLT = "mail"; 

	public static final String SOURCEPATH = "sourcepath";
	public static final String SOURCEPATH_DFLT = "/traces";
	public static final String PREFETCH = "prefetch";
	public static final boolean PREFETCH_DFLT = false;
	public static final String THREADS = "threads";
	public static final int THREADS_DFLT = 10;
	public static final String PAGES = "pages";
	public static final String LAZY_INDEX = "lazyIndexLoading";
	public static final boolean LAZY_INDEX_DFLT = true;
	
	public static final String UNIVERSITY = "university";
	public static final boolean UNIVERSITY_DFLT = false;
	
	public static final String NEEDS_AUTH = "needsAuth";
	public static final boolean NEEDS_AUTH_DFLT = false;
	
	public static final String NEW_AUTH = "newAuth";
	public static final boolean NEW_AUTH_DFLT = false;
	
	public static final String NEW_USER_EMAIL_FROM_ADDRESS = "newUserEmailFromAddress";
	public static final String NEW_USER_EMAIL_TO_ADDRESS = "newUserEmailToAddress";
	public static final String NEW_USER_EMAIL_TO_ADDRESS_DFLT = "";

	public static final String RESEARCH_ONLY = "researchOnly";
	public static final boolean RESEARCH_ONLY_DFLT = false;
	
	public static final String SITE_NAME = "site";
	public static final String SITE_NAME_DFLT = "IEEG.org";

	public static final String SITE_URL = "siteUrl";
	public static final String SITE_URL_DEFAULT = "http://www.habitat.upenn.edu";

	public static final String DATASET_TEST = "datasetTest";
	public static final boolean DATASET_TEST_DFLT = false;

	public static final String ZOOKEEPER = "zookeeper";
	public static final String ZOOKEEPER_DFLT = "localhost:2181"; 

	public static final String QUEUE_SERVER = "queueServer";
	public static final String QUEUE_SERVER_DFLT = "localhost"; 

	public static final String PORTAL = "portalGroup";
	public static final String PORTAL_DFLT = "one"; 

	public static final String QUEUE_USER = "queueUser";
	public static final String QUEUE_USER_DFLT = "guest"; 

	public static final String QUEUE_PASS = "queuePass";
	public static final String QUEUE_PASS_DFLT = "guest"; 

	public static final String DAEMON = "daemonGroup";
	public static final String DAEMON_DFLT = "daemon";
	
	public static final String DYGRAPH_LOG_LEVEL = "dygraphLogLevel";
	public static final int    DYGRAPH_LOG_LEVEL_DFLT = 5;
	
	public static final String TWILIOSID = "twilioSid";
	
	public static final String SALT = "privateSalt";
	public static final String SALT_DEFAULT = "Pr1v@t3P3pp3r!"; 

	public static final String TWILIOTOKEN = "twilioToken";
	
	public static final String TWILIOTEXT = "twilioText";
	public static final String TWILIOSOURCE = "twilioSource";
	
	public static final String ROOTPASSWORD = "rootPassword";
	public static final String HOSTORGANIZATION = "hostOrganization";

	public static final String DROPBOXCREDENTIALS = "dropBoxCredentials";
	public static final String DROPBOXCREDENTIALS_DFLT = "dropBoxCredentials";

	public static final String DROPBOXFOLDER = "dropBoxLocalFolder";
	public static final String DROPBOXFOLDER_DFLT = "/Users/Viewer";
	
	public static final String JEDIS = "jedisServer";
	public static final String JEDIS_DFLT = "localhost";

	public static final String JEDIS_PASSWORD = "jedisPassword";
	public static final String JEDIS_PASSWORD_DFLT = "password";
	
	public static final String LOADER_ID = "loaderUser";
	public static final String LOADER_ID_DFLT = "guest";
	public static final String LOADER_PASSWORD = "loaderPassword";
	public static final String LOADER_PASSWORD_DFLT = "password";

	public static final String MAX_DS_TS_ANNS = "maxDsTsAnns";
	public static final int MAX_DS_TS_ANNS_DFLT = 100000;

	public static final String BLOCK_STORE = "blockStore";

	public static final String DOWNSAMPLED = "downsampled";
	public static final String DOWNSAMPLED_RATE_HZ = "downsampledRate";
	public static final double DOWNSAMPLED_RATE_HZ_DFLT = 100.0;

	public static final String MEF_PAGE_SERVER_S3_THRD_POOL_SZ = "MEFPageServerS3.threadPoolSize";
	public static final int MEF_PAGE_SERVER_S3_THRD_POOL_SZ_DFLT = 10;

	public static final String MEF_PAGE_SERVER_S3_CPU_THRD_POOL_SZ = "MEFPageServerS3.cpuThreadPoolSize";
	public static final int MEF_PAGE_SERVER_S3_CPU_THRD_POOL_SZ_DFLT = 3;
	
	public static final String REQUEST_TIMEOUT = "requestTimeoutMs";
	public static final int REQUEST_TIMEOUT_DEFAULT = 60 * 90 * 1000;

	public static final String MEF_PAGE_SERVER_S3_MAX_REQ_HZ_CHS_SECS = "MEFPageServerS3.maxReqHzChsSecs";
	public static final long MEF_PAGE_SERVER_S3_MAX_REQ_HZ_DFLT = 500;
	public static final long MEF_PAGE_SERVER_S3_MAX_REQ_CHS_DFLT = 130;
	public static final long MEF_PAGE_SERVER_S3_MAX_REQ_SECS_DFLT = 2000;
	public static final long MEF_PAGE_SERVER_S3_MAX_REQ_HZ_CHS_SECS_DFLT =
			LongMath.checkedMultiply(
					MEF_PAGE_SERVER_S3_MAX_REQ_HZ_DFLT,
					LongMath.checkedMultiply(
							MEF_PAGE_SERVER_S3_MAX_REQ_CHS_DFLT,
							MEF_PAGE_SERVER_S3_MAX_REQ_SECS_DFLT));

	public static final String MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_CHS_SECS = "MEFPageServerS3.maxRedReqHzChsSecs";
	public static final long MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_DFLT = 500;
	public static final long MEF_PAGE_SERVER_S3_MAX_RED_REQ_CHS_DFLT = 130;
	public static final long MEF_PAGE_SERVER_S3_MAX_RED_REQ_SECS_DFLT = 2000;
	public static final long MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_CHS_SECS_DFLT =
			LongMath.checkedMultiply(
					MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_DFLT,
					LongMath.checkedMultiply(
							MEF_PAGE_SERVER_S3_MAX_RED_REQ_CHS_DFLT,
							MEF_PAGE_SERVER_S3_MAX_RED_REQ_SECS_DFLT));

	public static final String MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB = "MEFPageServerS3.minDataReqKb";
	public static final int MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB_DFLT = 64;

	public static final String MEF_PAGE_SERVER_S3_MAX_REQUESTS_PER_USER_RAW_RED = "MEFPageServerS3.maxRequestsPerUserRawRed";
	public static final Integer MEF_PAGE_SERVER_S3_MAX_REQUESTS_PER_USER_RAW_RED_DFLT = Integer.MAX_VALUE;

	public static final String MEF_PAGE_SERVER_S3_MAX_REQUESTS_TOTAL = "MEFPageServerS3.maxRequestsTotal";
	public static final Integer MEF_PAGE_SERVER_S3_MAX_REQUESTS_TOTAL_DFLT = 5;

	public static final String MEF_PAGE_SERVER_S3_TIMEOUT_SECS = "MEFPageServerS3.requestTimeoutSecs";
	public static final Integer MEF_PAGE_SERVER_S3_TIMEOUT_SECS_DFLT = 60;

	public static final String MAX_TRANSFERS_ALL_USERS = "server.maxTransfersAllUsers";
	public static final int MAX_TRANSFERS_ALL_USERS_DFLT = 100;

	public static final String MAX_TRANSFERS_PER_USER = "server.maxTransfersPerUser";
	public static final int MAX_TRANSFERS_PER_USER_DFLT = 5;

	public static final String DATA_BUCKET = "dataBucket";
	public static final String DATA_BUCKET_DFLT = "org-ieeg-data";

	public static final String TOOLS_BUCKET = "toolsBucket";
	public static final String TOOLS_BUCKET_DFLT = null;

	public static final String TOOLS_PATH = "toolsPath";
	public static final String TOOLS_PATH_DFLT = "www";

	public static final String IMAGES_BUCKET = "imagesBucket";
	public static final String IMAGES_BUCKET_DFLT = null;

	public static final String IMAGES_PATH = "imagesPath";
	public static final String IMAGES_PATH_DFLT = "www/images";

	public static final String IMAGES_EXP_HRS = "imagesExpirationHrs";
	public static final int IMAGES_EXP_HRS_DFLT = 24;

	public static final String PADDING_SAMPLES = "paddingSamples";
	public static final int PADDING_SAMPLES_DFLT = 150;

	public static final String LEAST_OKAY_MATLAB_CLIENT_VERSION = "leastOkayMatlabClientVersion";
	public static final String LEAST_OKAY_IEEG_CLIENT_VERSION = "leastOkayIeegClientVersion";

	public static final String INBOX_BUCKET = "inbox.bucket";
	public static final String INBOX_BUCKET_DFLT = null;

	public static final String INBOX_ACCESS_KEY_ID = "inbox.accessKeyId";
	public static final String INBOX_ACCESS_KEY_ID_DFTL = null;

	public static final String INBOX_SECRET_KEY = "inbox.secretKey";
	public static final String INBOX_SECRET_KEY_DFLT = null;

	public static final String INBOX_CREDENTIALS_DURATION_SECS = "inbox.credentialDurationSecs";
	public static final int INBOX_CREDENTIALS_DURATION_SECS_DFLT = 36 * 60 * 60;
	
	public static final String STORAGE_FACTORY_STAGING_DEFAULT_PATH = "StorageFactory.staging.defaultPath";
	public static final String STORAGE_FACTORY_STAGING_DEFAULT_BUCKET = "StorageFactory.staging.defaultBucket";
	
	public static final String STORAGE_FACTORY_PERSISTENT_DEFAULT_PATH = "StorageFactory.persistent.defaultPath";
	public static final String STORAGE_FACTORY_PERSISTENT_DEFAULT_BUCKET = "StorageFactory.persistent.defaultBucket";

	/**
	 * If true, then MEF data, tools, zips, and pdfs are stored in S3.
	 */
	public static final String USE_S3 = "useS3";
	public static final boolean USE_S3_DFLT = false;

	public static final String MEF_INDEX_SLICE_NO_ENTRIES = "mefIndexSliceNoEntries";
	public static final int MEF_INDEX_SLICE_NO_ENTRIES_DFLT = (512 * 1024) / 24;

	/**
	 * Active sessions are marked as expired if unused for
	 * {@code SESSION_EXP_MINS} minutes.
	 */
	public static final String SESSION_EXP_MINS = "sessionExpirationMins";
	public static final int SESSION_EXP_MINS_DFLT = 24 * 60;

	/**
	 * One out of every (@code SESSION_CLEANUP_DENOMINATOR} calls to
	 * {@code SessionManager.createSession()} result in old sessions being
	 * deleted from the database.
	 */
	public static final String SESSION_CLEANUP_DENOMINATOR = "sessionCleanupDenominator";
	public static final int SESSION_CLEANUP_DENOMINATOR_DFLT = 100;

	/**
	 * Stripe related settings
	 */
	public static final String STRIPE_LIVE_SECRET_KEY = "stripe.live.secret.key";
	public static final String STRIPE_LIVE_SECRET_KEY_DFLT = "";

	/**
	 * Dev or production environment?
	 */
	public static final String PRODUCTION_ENVIRONMENT = "isProductionEnvironment";
	public static final boolean PRODUCTION_ENVIRONMENT_DFLT = false;

	/**
	 * When session cleanup is run any session older than
	 * {@code SESSION_EXP_MINS + SESSION_CLEANUP_MINS} minutes are deleted.
	 */
	public static final String SESSION_CLEANUP_MINS = "sessionCleanupMins";
	public static final int SESSION_CLEANUP_MINS_DFLT = 24 * 60;

	public static final String ATTR_KEY =
			IvProps.class.getName().toLowerCase();

	private static Map<String, String> ivProps;

	public static final String DFLT_PROPS = "ieegview.properties";

	private static void check() {
		checkState(getPaddingSamples() >= 1, "mefPadding must be at least 1");
	}

	public static String getDataBucket() {
		return BtUtil.get(
				IvProps.getIvProps(),
				IvProps.DATA_BUCKET,
				IvProps.DATA_BUCKET_DFLT);
	}

	/**
	 * Get an immutable ieegview.properties.
	 * 
	 * @return an immutable ieegview.properties
	 * 
	 * @throws IllegalStateException if IvProps.init has not been called
	 */
	public static Map<String, String> getIvProps() {
		checkState(ivProps != null, "IvProps hasn't been initialized");
		return ivProps;
	}

	public static String getMailServer() {
		return BtUtil.get(
				getIvProps(),
				MAILSERVER,
				MAILSERVER_DFLT);
	}
	
	public static String getCoordinator() {
		return BtUtil.get(
				getIvProps(),
				COORDINATOR,
				COORDINATOR_DFLT);
	}

	public static String getMailLogin() {
		return BtUtil.get(
				getIvProps(),
				MAILID,
				MAILID_DFLT);
	}
	
	public static String getMailPassword() {
		return BtUtil.get(
				getIvProps(),
				MAILPASS,
				MAILPASS_DFLT);
	}
	
	public static int getMailPort() {
		return BtUtil.getInt(
				getIvProps(),
				MAILPORT,
				MAILPORT_DFLT);
	}

	public static boolean isMailSSL() {
		return BtUtil.getBoolean(
				getIvProps(),
				MAILSSL,
				MAILSSL_DFLT);
	}

	public static String getInboxBucket() {
		return BtUtil.get(
				getIvProps(),
				INBOX_BUCKET,
				INBOX_BUCKET_DFLT);
	}
	
	public static String getInboxAccessKeyId() {
		return BtUtil.get(
				getIvProps(),
				INBOX_ACCESS_KEY_ID,
				INBOX_ACCESS_KEY_ID_DFTL);
	}
	
	public static String getInboxSecretKey() {
		return BtUtil.get(
				getIvProps(),
				INBOX_SECRET_KEY,
				INBOX_SECRET_KEY_DFLT);
	}
	
	public static int getInboxCredentialsDurationSecs() {
		return BtUtil.getInt(
				getIvProps(),
				INBOX_CREDENTIALS_DURATION_SECS,
				INBOX_CREDENTIALS_DURATION_SECS_DFLT);
	}

	public static int getMaxTsAnnotations() {
		return BtUtil.getInt(
				getIvProps(),
				MAX_DS_TS_ANNS,
				MAX_DS_TS_ANNS_DFLT);
	}

	public static int getMefIndexSliceSize() {
		return BtUtil.getInt(
				getIvProps(),
				MEF_INDEX_SLICE_NO_ENTRIES,
				MEF_INDEX_SLICE_NO_ENTRIES_DFLT);
	}

	public static int getMefPageServerS3CpuThrdPoolSz() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_CPU_THRD_POOL_SZ,
				IvProps.MEF_PAGE_SERVER_S3_CPU_THRD_POOL_SZ_DFLT);
	}

	public static long getMEFPageServerS3MaxReqHzChsSecs() {
		return BtUtil.getLong(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQ_HZ_CHS_SECS,
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQ_HZ_CHS_SECS_DFLT);
	}

	public static long getMEFPageServerS3MaxRedReqHzChsSecs() {
		return BtUtil.getLong(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_CHS_SECS,
				IvProps.MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_CHS_SECS_DFLT);
	}

	public static int getMEFPageServerS3MaxRequestsPerUserRawRed() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQUESTS_PER_USER_RAW_RED,
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQUESTS_PER_USER_RAW_RED_DFLT);
	}

	public static int getMaxTransfersAllUsers() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MAX_TRANSFERS_ALL_USERS,
				IvProps.MAX_TRANSFERS_ALL_USERS_DFLT);
	}

	public static int getMaxTransfersPerUser() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MAX_TRANSFERS_PER_USER,
				IvProps.MAX_TRANSFERS_PER_USER_DFLT);
	}

	public static int getMEFPageServerS3MaxRequestsTotal() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQUESTS_TOTAL,
				IvProps.MEF_PAGE_SERVER_S3_MAX_REQUESTS_TOTAL_DFLT);
	}

	public static int getMEFPageServerS3TimeoutSecs() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.MEF_PAGE_SERVER_S3_TIMEOUT_SECS,
				IvProps.MEF_PAGE_SERVER_S3_TIMEOUT_SECS_DFLT);
	}
	
	public static String getNewUserEmailToAddress() {
		return BtUtil.get(
				getIvProps(),
				IvProps.NEW_USER_EMAIL_TO_ADDRESS,
				IvProps.NEW_USER_EMAIL_TO_ADDRESS_DFLT);
	}
	
	public static String getNewUserEmailFromAddress() {
		return BtUtil.get(
				getIvProps(),
				IvProps.NEW_USER_EMAIL_FROM_ADDRESS,
				getMailLogin());
	}

	public static int getPaddingSamples() {
		return BtUtil.getInt(
				IvProps.getIvProps(),
				IvProps.PADDING_SAMPLES,
				IvProps.PADDING_SAMPLES_DFLT);
	}

	public static int getSessionCleanupDenom() {
		return BtUtil.getInt(
				getIvProps(),
				SESSION_CLEANUP_DENOMINATOR,
				SESSION_CLEANUP_DENOMINATOR_DFLT);
	}

	public static int getSessionCleanupMins() {
		return BtUtil.getInt(
				getIvProps(),
				SESSION_CLEANUP_MINS,
				SESSION_CLEANUP_MINS_DFLT);
	}

	public static int getSessionExpirationMins() {
		return BtUtil.getInt(
				getIvProps(),
				SESSION_EXP_MINS,
				SESSION_EXP_MINS_DFLT);
	}

	public static String getLeastOkayMatlabClientVersion() {
		checkState(getIvProps().containsKey(LEAST_OKAY_MATLAB_CLIENT_VERSION),
				"must set the " + LEAST_OKAY_MATLAB_CLIENT_VERSION
						+ " property");
		String leastOkayMatlabClientVersion = getIvProps().get(
				LEAST_OKAY_MATLAB_CLIENT_VERSION);
		return leastOkayMatlabClientVersion;
	}

	public static String getLeastOkayIeegClientVersion() {
		checkState(getIvProps().containsKey(LEAST_OKAY_IEEG_CLIENT_VERSION),
				"must set the " + LEAST_OKAY_IEEG_CLIENT_VERSION
						+ " property");
		String leastOkayIeegClientVersion = getIvProps().get(
				LEAST_OKAY_IEEG_CLIENT_VERSION);
		return leastOkayIeegClientVersion;
	}

	public static String getToolsBucket() {
		return BtUtil.get(
				IvProps.getIvProps(),
				IvProps.TOOLS_BUCKET,
				IvProps.TOOLS_BUCKET_DFLT);
	}

	public static String getToolsPath() {
		return Files.simplifyPath(
				BtUtil.get(
						IvProps.getIvProps(),
						IvProps.TOOLS_PATH,
						IvProps.TOOLS_PATH_DFLT));
	}

	public static String getSourcePath() {
		return Files.simplifyPath(
				BtUtil.get(
						IvProps.getIvProps(),
						IvProps.SOURCEPATH,
						IvProps.SOURCEPATH_DFLT));
	}
	
	public static synchronized void init(InputStream input) {
		Properties props = new Properties();
		try {
			props.load(input);
			ivProps = Maps.fromProperties(props);
			check();
		} catch (IOException ioe) {
			throw propagate(ioe);
		}
	}

	public static synchronized void init() {
		String path = Thread.currentThread().getContextClassLoader().getResource(PROPERTIES_FILE)
				.getPath();
		IvProps.init(path);
	}
	
	/**
	 * Load the properties with the ieegview.properties file located at
	 * {@code IvProps.class.getClassLoader().getResource(configPath == "" ? "ieegview.properties" : configPath + "/ieegview.properties")}
	 * If no properties file is found there looks in
	 * {@code IvProps.class.getClassLoader().getResource("ieegview.properties")}
	 * , if we haven't already.
	 * <p>
	 * The returned map is immutable.
	 * <p>
	 * Calling this method more than once has no effect.
	 * 
	 * @param configPath directory on the classpath where the
	 *            ieegview.properties file is
	 * 
	 * @return an immutable map that contains the contents of
	 *         ieegview.properties
	 */
	public static synchronized void init(final String configPath) {
		if (ivProps != null) {
			return;
		}
		final String configFileName = configPath.equals("") ? DFLT_PROPS
				: (configPath.endsWith(DFLT_PROPS) ? configPath : configPath
						+ "/" + DFLT_PROPS);
		try {
			
			System.out.println("Configuration initialized to " + configFileName);
			
			logger.info("looking for ieegview properties at ["
					+ configFileName + "]");

			final URL url = IvProps.class.getClassLoader().getResource(
					configFileName);
			if (url == null && configFileName.equals(DFLT_PROPS)) {
				throw new IllegalStateException(
						"could not find ieegview properties @ "
								+ DFLT_PROPS);
			} else if (url == null) {
				InputStream is = null;
				try {
					is = new FileInputStream(configFileName);
					Properties props = new Properties();
					props.load(is);
					ivProps = Maps.fromProperties(props);
				} catch (Exception e) {
					// Deliberate fall-through
					// couldn't find it, fall back to default
					logger.info("couldn't find ieegview properties @ ["
							+ configFileName + "], falling back to default");
					init("");
				}
			} else {
				logger.info("reading ieegview properties from "
						+ url.toString());
				ivProps = Maps.fromProperties(BtUtil.loadProps(url));
			}
			if (ivProps == null) {
				throw new AssertionError(
						"programming error: props is null, but shouldn't be");
			}
			check();
		} catch (IOException e) {
			throw propagate(e);
		}
	}

	public static boolean isUsingS3() {
		return BtUtil.getBoolean(
				ivProps,
				USE_S3,
				USE_S3_DFLT);
	}

	public static String getStripeLiveSecretKey() {
		return BtUtil.get(
				ivProps,
				STRIPE_LIVE_SECRET_KEY,
				STRIPE_LIVE_SECRET_KEY_DFLT);
	}

	public static String getStorageFactoryDefaultStagingPath() {
		return BtUtil.get(
				ivProps, 
				STORAGE_FACTORY_STAGING_DEFAULT_PATH, 
				null);
	}
	
	public static String getStorageFactoryDefaultStagingBucket() {
		return BtUtil.get(
				ivProps, 
				STORAGE_FACTORY_STAGING_DEFAULT_BUCKET, 
				null);
	}
	
	public static String getStorageFactoryDefaultPersistentPath() {
		return BtUtil.get(
				ivProps, 
				STORAGE_FACTORY_PERSISTENT_DEFAULT_PATH, 
				null);
	}
	
	public static String getStorageFactoryDefaultPersistentBucket() {
		return BtUtil.get(
				ivProps, 
				STORAGE_FACTORY_PERSISTENT_DEFAULT_BUCKET, 
				null);
	}
	
	public static int getRequestTimeout() {
		return BtUtil.getInt(ivProps, REQUEST_TIMEOUT, REQUEST_TIMEOUT_DEFAULT);
	}
	
	public static boolean isUniversity() {
		return BtUtil.getBoolean(ivProps, UNIVERSITY, UNIVERSITY_DFLT);
	}
	
	public static boolean doNeedAuthorization() {
		return BtUtil.getBoolean(ivProps, NEEDS_AUTH, NEEDS_AUTH_DFLT);
	}

	public static boolean doNeedResearchAgreement() {
		return BtUtil.getBoolean(ivProps, RESEARCH_ONLY, RESEARCH_ONLY_DFLT);
	}
	
	public static String getSiteName() {
		return BtUtil.get(ivProps, SITE_NAME, SITE_NAME_DFLT);
	}
	
	public static String getSiteUrl() {
		return BtUtil.get(ivProps, SITE_URL, SITE_URL_DEFAULT);
	}

	public static String getQueueServer() {
		return BtUtil.get(ivProps, QUEUE_SERVER, QUEUE_SERVER_DFLT);
	}
	
	public static String getZookeeperServer() {
		return BtUtil.get(ivProps, ZOOKEEPER, ZOOKEEPER_DFLT);
	}
	
	public static String getPortalGroup() {
		return BtUtil.get(ivProps, PORTAL, PORTAL_DFLT);
	}

	public static String getDaemonGroup() {
		return BtUtil.get(ivProps, DAEMON, DAEMON_DFLT);
	}
	
	public static String getTwilioSid() {
		return BtUtil.get(ivProps, TWILIOSID, "");
	}
	
	public static String getTwilioToken() {
		return BtUtil.get(ivProps, TWILIOTOKEN, "");
	}
	
	public static String getTwilioText() {
		return BtUtil.get(ivProps, TWILIOTEXT, "");
	}
	
	public static String getTwilioSource() {
		return BtUtil.get(ivProps, TWILIOSOURCE, "");
	}

	public static String getDropBoxCredentials() {
		return BtUtil.get(ivProps, DROPBOXCREDENTIALS, DROPBOXCREDENTIALS_DFLT);
	}
	
	public static String getDropBoxFolder() {
		return BtUtil.get(ivProps, DROPBOXFOLDER, DROPBOXFOLDER_DFLT);
	}
	
	public static int getDygraphLogLevel() {
		return BtUtil.getInt(
				getIvProps(),
				IvProps.DYGRAPH_LOG_LEVEL,
				IvProps.DYGRAPH_LOG_LEVEL_DFLT);
	}
	
	public static String getJedisServer() {
		return BtUtil.get(ivProps, JEDIS, JEDIS_DFLT);
	}

	public static String getJedisPassword() {
		return BtUtil.get(ivProps, JEDIS_PASSWORD, JEDIS_PASSWORD_DFLT);
	}

	public static String getLoaderAccount() {
		return BtUtil.get(ivProps, LOADER_ID, LOADER_ID_DFLT);
	}

	public static String getLoaderPassword() {
		return BtUtil.get(ivProps, LOADER_PASSWORD, LOADER_PASSWORD_DFLT);
	}
	
	public static String getQueueUser() {
		return BtUtil.get(ivProps, QUEUE_USER, QUEUE_USER_DFLT);
	}
	public static String getQueuePassword() {
		return BtUtil.get(ivProps, QUEUE_PASS, QUEUE_PASS_DFLT);
	}

	public static String getHostOrganization() {
		return BtUtil.get(ivProps, HOSTORGANIZATION, "unknown");
	}

	public static String getRootPassword() {
		return BtUtil.get(ivProps, ROOTPASSWORD, "change_me");
	}

	public static String getPrivateSalt() {
		return BtUtil.get(ivProps, SALT, SALT_DEFAULT);
	}
	
	public static boolean isNewAuth() {
		return BtUtil.getBoolean(
				ivProps,
				NEW_AUTH,
				NEW_AUTH_DFLT);
	}

	public static boolean isProduction() {
		return BtUtil.getBoolean(
				ivProps,
				PRODUCTION_ENVIRONMENT,
				PRODUCTION_ENVIRONMENT_DFLT);
	}

	@VisibleForTesting
	public static synchronized void setIvProps(
			@Nullable Map<String, String> ivProps) {
		IvProps.ivProps = ivProps;
	}

	private IvProps() {
		throw new AssertionError("can't instantiate an "
				+ getClass().getSimpleName());
	}
}
