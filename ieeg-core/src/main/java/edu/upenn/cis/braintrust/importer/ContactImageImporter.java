/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.importer;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.snapshots.IImageDAO;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.ImagedContact;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.PixelCoordinates;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.shared.ImageType;

/**
 * Imports Image and Trace data from a spreadsheet.
 * 
 * @author John Frommeyer
 * 
 */
final class ContactImageImporter {

	private final static Logger logger = LoggerFactory
			.getLogger(ContactImageImporter.class);
	private final static Integer CHANNEL_COL = Integer.valueOf(0);
	@SuppressWarnings("unused")
	private final static Integer ONSET_COL = Integer.valueOf(1);
	@SuppressWarnings("unused")
	private final static Integer INVOLVED_COL = Integer.valueOf(2);
	private final static Integer MRI_X_COL = Integer.valueOf(3);
	private final static Integer MRI_Y_COL = Integer.valueOf(4);
	private final static Integer MRI_Z_COL = Integer.valueOf(5);
	private final static Integer CORONAL_X_COL = Integer.valueOf(7);
	private final static Integer CORONAL_Y_COL = Integer.valueOf(8);
	private final static Integer TAL_X_COL = Integer.valueOf(11);
	private final static Integer TAL_Y_COL = Integer.valueOf(12);
	private final static Integer TAL_Z_COL = Integer.valueOf(13);
	private final static Integer THREE_D_FILE_COL = Integer.valueOf(21);
	private final static Integer THREE_D_IMAGE_PIXEL_X_COL = Integer
			.valueOf(22);
	private final static Integer THREE_D_IMAGE_PIXEL_Y_COL = Integer
			.valueOf(23);

	private final static Integer CT_KEY_COL = Integer.valueOf(25);
	private final static Integer MRI_KEY_COL = Integer.valueOf(26);
	private final static Integer THREE_D_KEY_COL = Integer.valueOf(27);

	private final Map<String, ImageEntity> fileKeyToImage = newHashMap();

	private final EegStudy study;
	private final Sheet sheet;
	private final IImageDAO imageDAO;

	private final FormulaEvaluator evaluator;

	/**
	 * Converts trace/image rows in {@code is} to {@code TraceImagePair}s.
	 * 
	 * @param is
	 */
	ContactImageImporter(final EegStudy study, final IImageDAO imageDAO,
			final int sheetNo, final InputStream is) {
		this.study = checkNotNull(study);
		this.imageDAO = checkNotNull(imageDAO);
		checkNotNull(is);

		Workbook wb = null;
		try {
			wb = WorkbookFactory.create(is);
		} catch (InvalidFormatException e) {
			throw new IllegalStateException("Could not create Workbook.", e);
		} catch (IOException e) {
			throw new IllegalStateException("Could not create Workbook.", e);
		}
		evaluator = wb.getCreationHelper().createFormulaEvaluator();

		this.sheet = checkNotNull(wb.getSheetAt(sheetNo));
	}

	void convertAndImport() {
		int count = 0;

		for (Row row : sheet) {
			count++;

			final Cell channelCell = row.getCell(CHANNEL_COL.intValue());
			if (channelCell != null
					&& channelCell.getCellType() == Cell.CELL_TYPE_STRING) {
				String channel = channelCell.getStringCellValue();
				final Contact contact = ImporterUtil.findContactFromRecording(
						study.getRecording(), channel);
				if (contact == null) {
					logger.info("No contact found for channel " + channel
							+ ". Skipping row.");
					continue;
				} else {
					processRow(contact, row);
				}
				logger.info(
						"Channel: [{}], row: {}",
						new Object[] { channel,
								rowToString(row).toString() });

			} else {
				logger.error(
						"Skipping. Null or unexpected type in channel column: {}",
						rowToString(row).toString());
			}

		}
		logger.info("Found {} rows.", Integer.valueOf(count));

	}

	private void processRow(Contact contact,
			final Row row) {

		final String channelLabel = contact.getTrace().getLabel();
		addCoordsToContact(contact, channelLabel, row);

		final ImageEntity threeDImage = rowTo3DImage(row);
		PixelCoordinates threeDImagePixelCoords = rowTo3DImagePixelCoordinates(row);

		if (threeDImage != null && threeDImagePixelCoords != null) {
			imageDAO.saveOrUpdate(threeDImage);
			study.getImages().add(threeDImage);
			// Constructor takes care of relationships.
			new ImagedContact(threeDImagePixelCoords, threeDImage, contact);
			logger.info("Added {} to study.", threeDImage.getFileKey());
		} else if (threeDImage == null) {
			logger.info("No 3d image found for channel {}.", channelLabel);
		} else {
			logger.info("No coords for 3d image {} found for channel {}.",
					new Object[] { threeDImage.getFileKey(), channelLabel });
		}

		PixelCoordinates mriImageCoordinates = rowToMriPixelCoordinates(row);
		ImageEntity ctImage = rowToCtImage(row);
		if (ctImage != null && mriImageCoordinates != null) {
			imageDAO.saveOrUpdate(ctImage);
			study.getImages().add(ctImage);
			// Constructor takes care of relationships.
			new ImagedContact(mriImageCoordinates, ctImage, contact);
			logger.info("Added {} to study.", ctImage.getFileKey());
		} else if (ctImage == null) {
			logger.info("No CT image found for channel {}.", channelLabel);
		} else {
			logger.info("No coords for CT image {} found for channel {}.",
					new Object[] { ctImage.getFileKey(), channelLabel });
		}

		ImageEntity mriImage = rowToMriImage(row);
		if (mriImage != null && mriImageCoordinates != null) {
			imageDAO.saveOrUpdate(mriImage);
			study.getImages().add(mriImage);
			// Constructor takes care of relationships.
			new ImagedContact(mriImageCoordinates, mriImage, contact);
			logger.info("Added {} to study.", mriImage.getFileKey());
		} else if (mriImage == null) {
			logger.info("No MRI image found for channel {}.", channelLabel);
		} else {
			logger.info("No coords for MRI image {} found for channel {}.",
					new Object[] { mriImage.getFileKey(), channelLabel });
		}

	}

	private static PixelCoordinates rowTo3DImagePixelCoordinates(final Row row) {
		final Cell xPixelCell = row.getCell(THREE_D_IMAGE_PIXEL_X_COL
				.intValue());
		final Cell yPixelCell = row.getCell(THREE_D_IMAGE_PIXEL_Y_COL
				.intValue());
		PixelCoordinates coords = cellsToPixelCoordinates(xPixelCell,
				yPixelCell);
		return coords;
	}

	private static PixelCoordinates rowToMriPixelCoordinates(final Row row) {
		final Cell xPixelCell = row.getCell(CORONAL_X_COL.intValue());
		final Cell yPixelCell = row.getCell(CORONAL_Y_COL.intValue());
		PixelCoordinates coords = cellsToPixelCoordinates(xPixelCell,
				yPixelCell);
		return coords;
	}

	private static PixelCoordinates cellsToPixelCoordinates(
			final Cell xPixelCell, final Cell yPixelCell) {
		if (isNullOrBlank(xPixelCell) || isNullOrBlank(yPixelCell)) {
			return null;
		}
		double xDouble = xPixelCell.getNumericCellValue();
		final int x = round(xDouble);

		double yDouble = yPixelCell.getNumericCellValue();
		final int y = round(yDouble);

		PixelCoordinates coords = new PixelCoordinates(Integer.valueOf(x),
				Integer.valueOf(y));
		return coords;
	}

	private static int round(double doubleValue) {
		BigDecimal bigD = new BigDecimal(doubleValue);
		BigDecimal roundedBigD = bigD.setScale(0, RoundingMode.HALF_UP);
		final int intValue = roundedBigD.intValue();
		return intValue;
	}

	private void addCoordsToContact(final Contact contact,
			final String channelLabel, final Row row) {

		final Cell xMriCell = row.getCell(MRI_X_COL.intValue());
		final Cell yMriCell = row.getCell(MRI_Y_COL.intValue());
		final Cell zMriCell = row.getCell(MRI_Z_COL.intValue());
		final MriCoordinates mriCoords = cellsToMriCoordinates(xMriCell,
				yMriCell, zMriCell);
		if (mriCoords != null) {
			contact.setMriCoordinates(mriCoords);
			logger.info("Set MRI coordinates ({}, {}, {}) for channel {}.",
					new Object[] { mriCoords.getX(), mriCoords.getY(),
							mriCoords.getZ(), channelLabel });
		} else {
			logger.info("No MRI coordinates found for channel {}.",
					channelLabel);
		}

		final Cell xTalCell = row.getCell(TAL_X_COL.intValue());
		final Cell yTalCell = row.getCell(TAL_Y_COL.intValue());
		final Cell zTalCell = row.getCell(TAL_Z_COL.intValue());
		TalairachCoordinates talCoords = cellsToTalaraichCoordinates(xTalCell,
				yTalCell, zTalCell);
		if (talCoords != null) {
			contact.setTalairachCoordinates(talCoords);
			logger.info(
					"Set Talairach coordinates ({}, {}, {}) for channel {}.",
					new Object[] { talCoords.getX(), talCoords.getY(),
							talCoords.getZ(), channelLabel });
		} else {
			logger.info("No Talairach coordinates found for channel {}.",
					channelLabel);
		}

	}

	private ImageEntity rowTo3DImage(final Row row) {
		if (isNullOrBlank(row.getCell(THREE_D_FILE_COL.intValue()))) {
			return null;
		}
		return rowToImage(row, THREE_D_KEY_COL, ImageType.THREE_D_RENDERING);
	}

	private ImageEntity rowToCtImage(final Row row) {
		return rowToImage(row, CT_KEY_COL, ImageType.CT);
	}

	private ImageEntity rowToMriImage(final Row row) {
		return rowToImage(row, MRI_KEY_COL, ImageType.MRI);
	}

	/**
	 * Returns an {@code Image} with the given type and fileKey from the given
	 * column. If there is already an image in {@code fileKeyToImage} with the
	 * given fileKey, then that image is returned rather than creating a new
	 * instance. If {@code fileKeyColumn} is null or blank, then null is
	 * returned.
	 * 
	 * @param row
	 * @param fileKeyColumn
	 * @param imageType
	 * @return an {@code Image} with the given type and fileKey from the given
	 *         column
	 */
	private ImageEntity rowToImage(final Row row, final Integer fileKeyColumn,
			final ImageType imageType) {
		final Cell fileCell = row.getCell(fileKeyColumn.intValue());
		if (isNullOrBlank(fileCell)) {
			return null;
		}

		final CellValue formulaValue = evaluator.evaluate(fileCell);
		final String fileKey = formulaValue.getStringValue();

		ImageEntity imageEntity = fileKeyToImage.get(fileKey);
		if (imageEntity == null) {
			imageEntity = new ImageEntity();
			imageEntity.setFileKey(fileKey);
			imageEntity.setType(imageType);
			fileKeyToImage.put(fileKey, imageEntity);
		}
		return imageEntity;

	}

	private static StringBuffer rowToString(final Row row) {
		StringBuffer sb = new StringBuffer();
		for (Cell cell : row) {
			switch (cell.getCellType()) {
				case Cell.CELL_TYPE_BLANK:
					sb.append(" <blank> ");
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					sb.append(" [" + cell.getBooleanCellValue() + "] ");
					break;
				case Cell.CELL_TYPE_ERROR:
					sb.append(" [ <error> = " + cell.getErrorCellValue() + "]");
					break;
				case Cell.CELL_TYPE_FORMULA:
					sb.append(" [" + cell.getCellFormula() + "] ");
					break;
				case Cell.CELL_TYPE_NUMERIC:
					sb.append(" ["
							+ (DateUtil.isCellDateFormatted(cell) ? cell
									.getDateCellValue() : Double.valueOf(cell
									.getNumericCellValue())) + "] ");
					break;
				case Cell.CELL_TYPE_STRING:
					sb.append(" [" + cell.getStringCellValue() + "] ");
					break;
				default:
					sb.append(" <unknown cell type: " + cell.getCellType()
							+ "> ");
					break;

			}
		}
		return sb;
	}

	private static MriCoordinates cellsToMriCoordinates(
			@Nullable Cell xCell,
			@Nullable Cell yCell,
			@Nullable Cell zCell) {
		if (isNullOrBlank(xCell)
				|| isNullOrBlank(yCell)
				|| isNullOrBlank(zCell)) {
			return null;
		}

		final double x = xCell.getNumericCellValue();
		final double y = yCell.getNumericCellValue();
		final double z = zCell.getNumericCellValue();
		MriCoordinates coords = new MriCoordinates(Double.valueOf(x),
				Double.valueOf(y), Double.valueOf(z));
		return coords;
	}

	private static TalairachCoordinates cellsToTalaraichCoordinates(
			final Cell xCell, final Cell yCell, final Cell zCell) {
		if (isNullOrBlank(xCell) || isNullOrBlank(yCell)
				|| isNullOrBlank(zCell)) {
			return null;
		}

		final double x = xCell.getNumericCellValue();
		final double y = yCell.getNumericCellValue();
		final double z = zCell.getNumericCellValue();
		TalairachCoordinates coords = new TalairachCoordinates(
				Double.valueOf(x), Double.valueOf(y), Double.valueOf(z));
		return coords;
	}

	private static boolean isNullOrBlank(final Cell c) {
		return c == null || c.getCellType() == Cell.CELL_TYPE_BLANK;
	}
}
