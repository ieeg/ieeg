/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.drupal.model.DrupalRole;
import edu.upenn.cis.braintrust.model.OrganizationEntity;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IUser.Status;

public class BrainTrustRealm extends AuthorizingRealm {

	private final IUserService userService;

	/**
	 * A user is allowed to read all organizations
	 */
	@VisibleForTesting
	static final String USER_PERMISSIONS = OrganizationEntity.ENTITY_NAME
			+ ":read:*";

	public BrainTrustRealm() {
		this(UserServiceFactory.getUserService());
	}

	public BrainTrustRealm(final IUserService userService) {
		this.userService = checkNotNull(userService);
		setName(getClass().getSimpleName());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws DisabledAccountException if the account is disabled
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			final AuthenticationToken authcToken)
			throws AuthenticationException {
		final UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		final User user = userService.findUserByUsername(token.getUsername());
		if (user == null) {
			return null;
		} else {
			if (user.getStatus() == IUser.Status.DISABLED) {
				throw new DisabledAccountException("Account ["
						+ user.getUsername()
						+ "] is disabled.");
			}
			return new SimpleAuthenticationInfo(
					user.getUserId(),
					user.getPassword(),
					getName());
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws DisabledAccountException if the account is disabled
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			final PrincipalCollection principals) {
		final UserId userId = (UserId) principals.fromRealm(getName())
				.iterator()
				.next();
		final User user = userService.findUserByUid(userId);
		if (user == null) {
			return null;
		} else {
			if (user.getStatus() == Status.DISABLED) {
				throw new DisabledAccountException("Account ["
						+ user.getUsername()
						+ "] is disabled.");
			}

			final SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

			boolean admin = false;

			for (final Role role : user.getRoles()) {
				if (DrupalRole.ADMIN.equalsIgnoreCase(role.getName())) {
					info.addRole(Role.ADMIN.getName());
					info.addStringPermission("*");
					admin = true;
				}
			}

			if (!admin) {
				info.addRole(Role.USER.getName());
				info.addStringPermission(USER_PERMISSIONS);
			}
			return info;
		}
	}
}
