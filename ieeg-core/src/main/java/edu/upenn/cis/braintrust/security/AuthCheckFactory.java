package edu.upenn.cis.braintrust.security;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.mefview.server.CoralReefServiceFactory;

public class AuthCheckFactory {
	static ExtAuthzHandler handler = null;
	
	static IGraphServer crService;
	
	public static synchronized ExtAuthzHandler getHandler() {
		if (crService == null && IvProps.isNewAuth())
			crService = CoralReefServiceFactory.getGraphServer();
		if (handler == null)
			handler = new ExtAuthzHandler(crService);
		
		return handler;
	}
}
