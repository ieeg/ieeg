/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;

/**
 * @author John Frommeyer
 *
 */
public final class RecordingObjectProviderFactory {

	private static IRecordingObjectProvider writer = null;

	public static IRecordingObjectProvider getRecordingObjectProvider() {
		if (writer != null) {
			return writer;
		} else if (IvProps.isUsingS3()) {
			// return new TransferManagerRecObjectWriter();
			return new S3RecObjectProvider();
		} else {
			return new LocalRecObjectProvider(
					BtUtil.get(
							IvProps.getIvProps(),
							IvProps.SOURCEPATH,
							IvProps.SOURCEPATH_DFLT));
		}
	}

	@VisibleForTesting
	public static void setRecordingObjectProvider(
			@Nullable IRecordingObjectProvider testWriter) {
		writer = testWriter;
	}
}
