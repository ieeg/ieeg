/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "application",
				"key" }))
public class SnapshotApplicationClob {

	private Long id;
	private Integer version;
	private DataSnapshotEntity parent;
	private String key;
	private String application;
	private String value;

	/**
	 * For JPA.
	 */
	SnapshotApplicationClob() {}

	public SnapshotApplicationClob(final String application,
			final String key,
			final String value,
			final DataSnapshotEntity parent) {
		this.value = value;
		this.parent = parent;
		this.key = key;
		this.application = application;
	}

	@Id
	public Long getId() {
		return id;
	}

	@MapsId
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public DataSnapshotEntity getParent() {
		return parent;
	}

	@Lob
	@Column
	@NotNull
	public String getValue() {
		return value;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}
	
	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}
	
	public void setParent(final DataSnapshotEntity parent) {
		this.parent = parent;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	@SuppressWarnings("unused")
	private void setVersion(final Integer version) {
		this.version = version;
	}
	
	@Size(min = 1, max = 255)
	@NotNull
	public String getKey() {
		return key;
	}

	@SuppressWarnings("unused")
	private void setKey(String key) {
		this.key = key;
	}

	@Size(min = 1, max = 255)
	@NotNull
	public String getApplication() {
		return application;
	}

	@SuppressWarnings("unused")
	private void setApplication(String application) {
		this.application = application;
	}

}
