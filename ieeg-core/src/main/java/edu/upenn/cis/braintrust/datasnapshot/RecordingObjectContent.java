/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.InputStream;

import javax.annotation.concurrent.Immutable;

import edu.upenn.cis.braintrust.shared.DataSnapshotId;

/**
 * @author John Frommeyer
 *
 */
@Immutable
public final class RecordingObjectContent {

	private final DataSnapshotId parentId;
	private final InputStream inputStream;
	private final String name;
	private final String internetMediaType;
	private final String eTag;

	public RecordingObjectContent(
			DataSnapshotId parentId,
			InputStream inputStream,
			String name,
			String internetMediaType, String eTag) {
		this.parentId = checkNotNull(parentId);
		this.inputStream = checkNotNull(inputStream);
		this.name = checkNotNull(name);
		this.internetMediaType = checkNotNull(internetMediaType);
		this.eTag = checkNotNull(eTag);
	}

	public DataSnapshotId getParentId() {
		return parentId;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public String getInternetMediaType() {
		return internetMediaType;
	}

	public String getETag() {
		return eTag;
	}

	public String getName() {
		return name;
	}
}
