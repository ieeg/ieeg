/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.IHasName;

/**
 * The name of an entity which has ACLs. * @author John Frommeyer
 * 
 */
@Entity(name = "PermissionDomain")
@Table(name = PermissionDomainEntity.TABLE)
public class PermissionDomainEntity implements IHasLongId, IHasName {
	public static final String TABLE = "permission_domain";
	public static final String ID_COLUMN = TABLE + "_id";

	private Long id;
	private Integer version;
	private String name;
	private Set<ModeEntity> modes = newHashSet();
	public static final String PERM_DOMAIN_WILDCARD = "*";

	// For JPA
	PermissionDomainEntity() {}

	public PermissionDomainEntity(String name) {
		this.name = checkNotNull(name);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return id;
	}

	@Column
	@NaturalId
	@NotNull
	@Override
	public String getName() {
		return name;
	}

	@OneToMany(
			cascade = CascadeType.ALL,
			mappedBy = "domain",
			orphanRemoval = true)
	@Size(min = 1)
	public Set<ModeEntity> getModes() {
		return modes;
	}

	@Version
	@Column(name = "obj_version")
	public Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setId(Long id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("unused")
	private void setModes(Set<ModeEntity> modes) {
		this.modes = modes;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
