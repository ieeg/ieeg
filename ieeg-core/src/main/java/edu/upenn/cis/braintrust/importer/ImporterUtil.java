/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.importer;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;

import edu.upenn.cis.braintrust.dao.metadata.IPatientDAO;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.Recording;

/**
 * Common methods for import.
 * 
 * @author John Frommeyer
 * 
 */
public class ImporterUtil {

	private final static Logger logger = LoggerFactory
			.getLogger(ImporterUtil.class);

	private ImporterUtil() {
		// prevent instantiation
	}

	/**
	 * Returns the {@code EegStudy} with the given {@code studyDir} for the
	 * patient with the given {@code userSuppliedId}.
	 * 
	 * @param patientDAO
	 * @param userSuppliedId
	 * @param studyDir
	 * @return the {@codeEegStudy} with the given {@code studyDir} for the
	 *         patient with the given {@code userSuppliedId}
	 */
	public static EegStudy findStudy(final IPatientDAO patientDAO,
			final String userSuppliedId, final String studyDir) {
		checkNotNull(userSuppliedId);
		checkNotNull(studyDir);
		Patient patient = retrievePatientByLabel(patientDAO,
				userSuppliedId);
		if (patient == null) {
			logger.error("Cannot find patient: {}.", userSuppliedId);
			return null;
		}
		Set<EegStudy> matchingStudies = newHashSet();
		Set<HospitalAdmission> admissions = patient.getAdmissions();
		for (HospitalAdmission admission : admissions) {
			Set<EegStudy> studies = admission.getStudies();
			for (EegStudy study : studies) {
				if (studyDir.equals(study.getDir())) {
					matchingStudies.add(study);
				}
			}
		}
		if (matchingStudies.isEmpty()) {
			throw new IllegalStateException("There are no studies for patient "
					+ userSuppliedId + " with study directory " + studyDir
					+ ".");
		} else if (matchingStudies.size() > 1) {
			throw new IllegalStateException(
					"There is more than one study for patient "
							+ userSuppliedId + " with study directory "
							+ studyDir + ".");
		} else {
			EegStudy study = Iterables.get(matchingStudies, 0);
			logger.info("Found study: {}/{}", study.getDir(), study.getMefDir());
			return study;
		}
	}

	/**
	 * 
	 * Returns the {@code Patient} with the given {@code userSuppliedId}
	 * 
	 * @param patientDAO
	 * @param patientLabel
	 * @return
	 */
	static Patient retrievePatientByLabel(
			final IPatientDAO patientDAO, final String patientLabel) {

		Patient patient = patientDAO
				.findByLabelEager(patientLabel);

		for (HospitalAdmission admission : patient.getAdmissions()) {
			for (EegStudy study : admission.getStudies()) {
				Hibernate.initialize(study.getImages());
				for (ContactGroup electrode : study.getRecording().getContactGroups()) {
					Hibernate.initialize(electrode.getContacts());
				}
			}
		}
		return patient;
	}

	/**
	 * Returns the {@code Contact} from {@code recording} with a time series
	 * having the given label, or null if there is no such Contact.
	 * 
	 * @param recording
	 * @param channelLabel
	 * @return the {@code Contact} from {@code recording} with a time series
	 *         having the given label, or null if there is no such Contact
	 * @throws IllegalArgumentException if the recording has more than one
	 *             matching contact
	 */
	static Contact findContactFromRecording(Recording recording,
			String channelLabel) {
		checkNotNull(channelLabel);
		checkNotNull(recording);
		final Set<Contact> matchingContacts = newHashSet();
		for (final ContactGroup electrode : recording.getContactGroups()) {
			for (final Contact contact : electrode.getContacts()) {
				if (contact.getTrace().getLabel().matches(channelLabel)) {
					matchingContacts.add(contact);
				}
			}
		}
		if (matchingContacts.isEmpty()) {
			return null;
		} else if (matchingContacts.size() == 1) {
			return getOnlyElement(matchingContacts);
		} else {
			throw new IllegalArgumentException("Recording "
					+ recording.getDir() + " contains "
					+ matchingContacts.size()
					+ " contacts with a time series labeled " + channelLabel);
		}
	}

	/**
	 * Returns {@code string} if it is not null and not empty. Otherwise throw
	 * an {@code IllegalStateException}.
	 * 
	 * @param string
	 * @return {@code string} if it is not null and not empty. Otherwise throw
	 *         an {@code IllegalStateException}
	 */
	public static String checkNotNullNotEmpty(String string) {
		if (string != null && !string.isEmpty()) {
			return string;
		}
		throw new IllegalStateException();
	}

	public static String normalizeTsAnnType(String type) {
		checkNotNull(type);
		if ("Artifact".equalsIgnoreCase(type)) {
			return "Artifact";
		} else if ("Seizure".equalsIgnoreCase(type)) {
			return "Seizure";
		} else {
			return type;
		}
	}
}
