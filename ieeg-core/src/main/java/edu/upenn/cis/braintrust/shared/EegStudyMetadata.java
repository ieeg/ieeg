/*
 * Copyright (C) 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableSet;

@GwtCompatible(serializable = true)
@Immutable
@JsonIgnoreProperties(ignoreUnknown=true)
public final class EegStudyMetadata implements CaseMetadata, Serializable {

	private static final long serialVersionUID = 1L;

	private String patientLabel;
	private Integer ageAtOnset;
	private Gender gender;
	private Handedness handedness;
	private Ethnicity ethnicity;
	private String etiology;
	private Boolean developmentalDisorders;
	private Boolean traumaticBrainInjury;
	private Boolean familyHistory;
	private Set<SeizureType> szHistory;
	private Set<Precipitant> preciptants;
	private Integer ageAtAdmission;
	private String label;
	private String refElectrodeDescription;
	private int imageCount;
	private Set<ContactGroupMetadata> contactGroups;
	private Set<EpilepsySubtypeMetadata> epilepsySubtypes;
	
	private String studyRevId;

	/**
	 * For GWT.
	 */
	@SuppressWarnings("unused")
	private EegStudyMetadata() {}

	public EegStudyMetadata(
			String patientLabel,
			@Nullable Integer ageAtOnset,
			Gender gender,
			Handedness handedness,
			Ethnicity ethnicity,
			String etiology,
			int imageCount,
			@Nullable Boolean developmentalDisorders,
			@Nullable Boolean traumaticBrainInjury,
			@Nullable Boolean familyHistory,
			Set<SeizureType> szHistory,
			Set<Precipitant> preciptants,
			@Nullable Integer ageAtAdmission,
			String label,
			@Nullable String refElectrodeDescription,
			Set<ContactGroupMetadata> contactGroups, 
			Set<EpilepsySubtypeMetadata> epilepsySubtypes) {
		this.patientLabel = checkNotNull(patientLabel);
		this.ageAtOnset = ageAtOnset;
		this.gender = checkNotNull(gender);
		this.handedness = checkNotNull(handedness);
		this.ethnicity = checkNotNull(ethnicity);
		this.etiology = checkNotNull(etiology);
		this.imageCount = imageCount;
		this.developmentalDisorders =
				developmentalDisorders;
		this.traumaticBrainInjury = traumaticBrainInjury;
		this.familyHistory = familyHistory;
		this.szHistory = ImmutableSet.copyOf(szHistory);
		this.preciptants = ImmutableSet.copyOf(preciptants);
		this.ageAtAdmission = ageAtAdmission;
		this.label = checkNotNull(label);
		this.refElectrodeDescription = refElectrodeDescription;
		this.contactGroups = ImmutableSet.copyOf(contactGroups);
		this.epilepsySubtypes = ImmutableSet.copyOf(epilepsySubtypes);
	}
	
	@JsonIgnore
	public String getAge() {
		if (getAgeAtAdmission() == null)
			return null;
		else 
			return String.valueOf(getAgeAtAdmission());
	}

	public Integer getAgeAtAdmission() {
		return ageAtAdmission;
	}

	public Integer getAgeAtOnset() {
		return ageAtOnset;
	}

	public Boolean getDevelopmentalDisorders() {
		return developmentalDisorders;
	}

	public Set<ContactGroupMetadata> getContactGroups() {
		return contactGroups;
	}

	public Set<EpilepsySubtypeMetadata> getEpilepsySubtypes() {
		return epilepsySubtypes;
	}

	public Ethnicity getEthnicity() {
		return ethnicity;
	}

	public String getEtiology() {
		return etiology;
	}

	public Boolean getFamilyHistory() {
		return familyHistory;
	}

	public Gender getGender() {
		return gender;
	}

	public Handedness getHandedness() {
		return handedness;
	}
	
	public int getImageCount() {
		return imageCount;
	}

	public String getLabel() {
		return label;
	}

	public String getPatientLabel() {
		return patientLabel;
	}

	public Set<Precipitant> getPreciptants() {
		return preciptants;
	}

	public String getRefElectrodeDescription() {
		return refElectrodeDescription;
	}

	public Set<SeizureType> getSzHistory() {
		return szHistory;
	}

	public Boolean getTraumaticBrainInjury() {
		return traumaticBrainInjury;
	}

	public String getStudyRevId() {
		return studyRevId;
	}
	
	public void setStudyRevId(String r) {
		studyRevId = r;
	}

	@JsonIgnore
	@Override
	public String getId() {
		return getStudyRevId();
	}

	@JsonIgnore
	@Override
	public boolean isHumanPatient() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		return "";
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Metadata";
	}

	public void setPatientLabel(String patientLabel) {
		this.patientLabel = patientLabel;
	}

	public void setAgeAtOnset(Integer ageAtOnset) {
		this.ageAtOnset = ageAtOnset;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public void setHandedness(Handedness handedness) {
		this.handedness = handedness;
	}

	public void setEthnicity(Ethnicity ethnicity) {
		this.ethnicity = ethnicity;
	}

	public void setEtiology(String etiology) {
		this.etiology = etiology;
	}

	public void setDevelopmentalDisorders(Boolean developmentalDisorders) {
		this.developmentalDisorders = developmentalDisorders;
	}

	public void setTraumaticBrainInjury(Boolean traumaticBrainInjury) {
		this.traumaticBrainInjury = traumaticBrainInjury;
	}

	public void setFamilyHistory(Boolean familyHistory) {
		this.familyHistory = familyHistory;
	}

	public void setSzHistory(Set<SeizureType> szHistory) {
		this.szHistory = szHistory;
	}

	public void setPreciptants(Set<Precipitant> preciptants) {
		this.preciptants = preciptants;
	}

	public void setAgeAtAdmission(Integer ageAtAdmission) {
		this.ageAtAdmission = ageAtAdmission;
	}

	public void setRefElectrodeDescription(String refElectrodeDescription) {
		this.refElectrodeDescription = refElectrodeDescription;
	}

	public void setImageCount(int imageCount) {
		this.imageCount = imageCount;
	}

	public void setContactGroups(Set<ContactGroupMetadata> contactGroups) {
		this.contactGroups = contactGroups;
	}

	public void setEpilepsySubtypes(Set<EpilepsySubtypeMetadata> epilepsySubtypes) {
		this.epilepsySubtypes = epilepsySubtypes;
	}
	
	
}
