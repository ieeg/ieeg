/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.tasks.hibernate;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.tasks.IUserTaskDAO;
import edu.upenn.cis.braintrust.model.UserTaskEntity;

/**
 * @author John Frommeyer
 *
 */
public class UserTaskDAOHibernate extends
		GenericHibernateDAOLongId<UserTaskEntity> implements
		IUserTaskDAO {
	
	public UserTaskDAOHibernate() {}
	
	public UserTaskDAOHibernate(Session session) {
		setSession(session);
	}
}
