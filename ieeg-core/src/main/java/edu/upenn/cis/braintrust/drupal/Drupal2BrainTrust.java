/*******************************************************************************
 * Copyright 2011 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.drupal;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import edu.upenn.cis.braintrust.drupal.model.DrupalProfileField;
import edu.upenn.cis.braintrust.drupal.model.DrupalProfileValue;
import edu.upenn.cis.braintrust.drupal.model.DrupalRole;
import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.shared.IUser;

public class Drupal2BrainTrust {
	public interface DrupalFields {
		public DrupalProfileField getField(DrupalUser dUser, String fieldName);
		public DrupalProfileValue createValue(DrupalUser dUser, DrupalProfileField field, String value);
	}


	private final static Logger logger = LoggerFactory
			.getLogger(Drupal2BrainTrust.class);

	public static List<User> drupalUsers2BrainTrustUsers(
			List<DrupalUser> drupalUsers) {
		List<User> users = newArrayList();
		for (DrupalUser drupalUser : drupalUsers) {
			users.add(
					drupalUser2BrainTrustUser(drupalUser));
		}
		return users;
	}
	
//	public static void userRolesUpdate(User newUser, DrupalUser userToUpdate) {
//		UserProfile newProfile = newUser.getProfile();
//
//		// Set roles.  Needs to be tweaked since the roles are immutable!
//		for (final Role role : newUser.getRoles()) {
//			if (role == Role.ADMIN && !userToUpdate.getRoles().contains(DrupalRole.ADMIN))
//				userToUpdate.getRoles().add(DrupalRole.ADMIN);
//			else if (role == Role.DATA_ENTERER && !userToUpdate.getRoles().contains(DrupalRole.DATA_ENTERER))
//				userToUpdate.getRoles().add(DrupalRole.DATA_ENTERER);
//			else if (role == Role.GUEST && !userToUpdate.getRoles().contains(DrupalRole.GUEST))
//				userToUpdate.getRoles().add(DrupalRole.GUEST);
//			else if (!userToUpdate.getRoles().contains(role.getName()))
//				userToUpdate.getRoles().add(role.getName());
//		}
//
//	}

	public static void userInfo2DrupalUserProfile(
			User newUser, DrupalUser userToUpdate,
			DrupalFields fetcher) {
		// New entity!  Set them to disabled by default
		if (userToUpdate.getUid() == null || userToUpdate.getUid() == -1) {
			throw new IllegalStateException("Must have established user!");
		}
		
		// set profile info
		if (newUser.getProfile().getAcademicTitle().isPresent() && 
				!newUser.getProfile().getAcademicTitle().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.ACADEMIC_TITLE_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getAcademicTitle().get());
		}
		if (newUser.getProfile().getStreet().isPresent() && 
				!newUser.getProfile().getStreet().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.STREET_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getStreet().get());
		}
		if (newUser.getProfile().getCity().isPresent() && !newUser.getProfile().getCity().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.CITY_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getCity().get());
		}
		if (newUser.getProfile().getState().isPresent() && !newUser.getProfile().getState().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.STATE_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getState().get());
		}
		if (newUser.getProfile().getZip().isPresent() && !newUser.getProfile().getZip().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.ZIP_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getZip().get());
		}
		if (newUser.getProfile().getCountry().isPresent() && !newUser.getProfile().getCountry().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.COUNTRY_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getCountry().get());
		}
		if (newUser.getProfile().getFirstName().isPresent() && !newUser.getProfile().getFirstName().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.FIRSTNAME_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getFirstName().get());
		}
		if (newUser.getProfile().getLastName().isPresent() && !newUser.getProfile().getLastName().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.LASTNAME_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getLastName().get());
		}
		if (newUser.getProfile().getInstitutionName().isPresent() && !newUser.getProfile().getInstitutionName().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.INSTITUTION_NAME);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getInstitutionName().get());
		}
		if (newUser.getProfile().getInboxAwsAccessKeyId().isPresent() && !newUser.getProfile().getInboxAwsAccessKeyId().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.INBOX_AWS_ACCESS_KEY_ID);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getInboxAwsAccessKeyId().get());
		}
		if (newUser.getProfile().getInboxBucket().isPresent() && !newUser.getProfile().getInboxBucket().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.INBOX_BUCKET);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getInboxBucket().get());
		}
		if (newUser.getProfile().getDropBoxCredential().isPresent() && !newUser.getProfile().getDropBoxCredential().get().isEmpty()) {
			DrupalProfileField f = fetcher.getField(userToUpdate, DrupalProfileField.DROPBOX_KEY);
//			userToUpdate.getProfile().put(f, 
					fetcher.createValue(userToUpdate, f, newUser.getProfile().getDropBoxCredential().get());
		}
//		if (newUser.getProfile().getLatitude().isPresent()) {
//			userToUpdate.getProfile().put(fetcher.getField(DrupalProfileField.LAT), 
//					new DrupalProfileValue(userToUpdate, String.valueOf(newUser.getProfile().getLatitude().get())));
//		}
//		if (newUser.getProfile().getLongitude().isPresent()) {
//			userToUpdate.getProfile().put(fetcher.getField(DrupalProfileField.LASTNAME_NAME), 
//					new DrupalProfileValue(userToUpdate, String.valueOf(newUser.getProfile().getLongitude().get())));
//		}
	}

	public static void userInfo2DrupalUser(
			User newUser, DrupalUser userToUpdate,
			DrupalFields fetcher,
			boolean initToDisabled) {

		// New entity!  Set them to disabled by default
		if (newUser.getUserId() == null || newUser.getUserId().getValue() == -1) {
			if (initToDisabled)
				userToUpdate.setStatus((byte) 0);    // Account disabled
			else
				userToUpdate.setStatus((byte) 1);    // Acccount enabled

			if (newUser.getPassword().isEmpty())
				throw new IllegalStateException("Unable to create user with empty password");

			// TODO: set default roles, if any!
		}

		userToUpdate.setUid(newUser.getUserId().getValue());
		userToUpdate.setName(newUser.getUsername());
		if (newUser.getEmail().isPresent())
			userToUpdate.setMail(newUser.getEmail().get());
		if (!newUser.getPassword().isEmpty())
			userToUpdate.setPass(newUser.getPassword());
		userToUpdate.setPicture(newUser.getPhoto());
	}
	
	public static User drupalUser2BrainTrustUser(
			DrupalUser drupalUser) {
		String m = "drupalUser2BrainTrustUser(...)";

		final Set<Role> roles = newHashSet();
		logger.debug(
				"{}: user: [{}] roles: [{}]",
				new Object[] {
						m,
						drupalUser.getName(),
						drupalUser.getRoles()
				});

		for (final DrupalRole drupalRole : drupalUser.getRoles()) {
			if (drupalRole.getName().equals(DrupalRole.ADMIN)) {
				roles.add(Role.ADMIN);
			} else if (drupalRole.getName().equals(DrupalRole.DATA_ENTERER)) {
				roles.add(Role.DATA_ENTERER);
			} else if (drupalRole.getName().equals(DrupalRole.GUEST)) {
				roles.add(Role.GUEST);
			} else {
				roles.add(new Role(drupalRole.getName()));
			}
		}
		if (roles.contains(Role.GUEST))
			;
		else
			roles.add(Role.USER);

		IUser.Status status = null;

		switch (drupalUser.getStatus()) {
			case 0:
				status = IUser.Status.DISABLED;
				break;
			case 1:
				status = IUser.Status.ENABLED;
				break;
			default:
				throw new IllegalStateException(
						"unknown status in drupal table: "
								+ drupalUser.getStatus());
		}

		String academicTitle = null;
		String job = null;
		String firstName = null;
		String lastName = null;
		String institutionName = null;
		String inboxBucket = null;
		String inboxAwsAccessKeyId = null;
		String dropBoxKey = null;
		Double latitude = null;
		Double longitude = null;
		
		String street = null;
		String city = null;
		String state = null;
		String zip = null;
		String country = null;

		for (DrupalProfileField profileField : drupalUser.getProfile().keySet()) {
			DrupalProfileValue profileValue = drupalUser.getProfile().get(
					profileField);
			if (Strings.isNullOrEmpty(profileValue.getValue())) {} else {
				if (profileField.getName().equals(
						DrupalProfileField.ACADEMIC_TITLE_NAME)) {
					academicTitle = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.JOB_NAME)) {
					job = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.INSTITUTION_NAME)) {
					institutionName = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.FIRSTNAME_NAME)) {
					firstName = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.LASTNAME_NAME)) {
					lastName = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.STREET_NAME)) {
					street = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.CITY_NAME)) {
					city = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.STATE_NAME)) {
					state = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.ZIP_NAME)) {
					zip = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.COUNTRY_NAME)) {
					country = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.INBOX_BUCKET)) {
					inboxBucket = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.INBOX_AWS_ACCESS_KEY_ID)) {
					inboxAwsAccessKeyId = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.DROPBOX_KEY)) {
					dropBoxKey = profileValue.getValue();
				} else if (profileField.getName().equals(
						DrupalProfileField.LAT)) {
					latitude = Double.valueOf(profileValue.getValue());
				} else if (profileField.getName().equals(
						DrupalProfileField.LON)) {
					longitude = Double.valueOf(profileValue.getValue());
				}
			}
		}

		UserProfile userProfile =
				new UserProfile(
						firstName,
						lastName,
						institutionName,
						academicTitle,
						job,
						inboxBucket,
						inboxAwsAccessKeyId,
						dropBoxKey,
						latitude,
						longitude,
						street,
						city,
						state,
						zip,
						country);

		logger.debug("{}: {}", m, userProfile);

		User ret = new User(
				new UserId(drupalUser.getUid()),
				drupalUser.getName(),
				drupalUser.getPass(),
				status,
				roles,
				drupalUser.getPicture(),
				drupalUser.getMail(),
				userProfile);

		return ret;
	}

	private Drupal2BrainTrust() {
		throw new AssertionError();
	}
}
