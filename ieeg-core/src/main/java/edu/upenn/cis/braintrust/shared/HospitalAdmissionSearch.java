/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable = true)
public class HospitalAdmissionSearch implements Serializable {
	private static final long serialVersionUID = 1L;

	// private TreatmentSearch treatmentSearch;
	private PatientSearch patientSearch;

	public PatientSearch getPatientSearch() {
		return patientSearch;
	}

	// public TreatmentSearch getTreatmentSearch() {
	// return treatmentSearch;
	// }

	// public void setTreatmentSearch(final TreatmentSearch treatmentSearch) {
	// this.treatmentSearch = treatmentSearch;
	// }

	public void setPatientSearch(PatientSearch patientSearch) {
		this.patientSearch = patientSearch;
	}

	@Override
	public String toString() {
		return "HospitalAdmissionSearch [patientSearch=" + patientSearch + "]";
	}

}
