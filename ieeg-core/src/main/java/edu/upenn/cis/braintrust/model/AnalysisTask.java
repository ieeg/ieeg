/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.braintrust.model;

import java.util.Date;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.TaskStatus;

/**
 * An active task.
 * 
 * @author John Frommeyer
 */
@Entity
@Table(name = AnalysisTask.TABLE,
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {
						"parent_id",
						"name" }) })
public class AnalysisTask extends AbstractTask<JobEntity> implements IHasLongId {

	public static final String TABLE = "task";
	public static final String ID_COLUMN = TABLE + "_id";

	private Integer version;

	AnalysisTask() {}

	public AnalysisTask(
			JobEntity parent,
			String pubId,
			TaskStatus status,
			Long startTimeMicrosec,
			Long endTimeMicrosec,
			@Nullable String worker,
			@Nullable Date runStartTime) {
		super(
				parent,
				pubId,
				status,
				startTimeMicrosec,
				endTimeMicrosec,
				worker,
				runStartTime);
	}

	@Override
	@Id
	@GeneratedValue
	@Column(name = ID_COLUMN)
	public Long getId() {
		return super.getId();
	}

	@Override
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotNull
	public JobEntity getParent() {
		return super.getParent();
	}

	@Override
	@ElementCollection
	@CollectionTable(
			name = TABLE + "_ts_pub_id",
			joinColumns = @JoinColumn(name = ID_COLUMN))
	@Column(name = "ts_pub_id", nullable = false)
	@Nonnull
	public Set<String> getTsPubIds() {
		return super.getTsPubIds();
	}

	@Version
	@Column(name = "obj_version")
	private Integer getVersion() {
		return version;
	}

	@SuppressWarnings("unused")
	private void setVersion(Integer version) {
		this.version = version;
	}

}
