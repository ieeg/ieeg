/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot.assembler;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.TimeSeriesSearchResult;
import edu.upenn.cis.braintrust.shared.UserId;

public class DsSearchResultAssembler {

	public static DataSnapshotSearchResult assembleDsSearchResult(
			final DataSnapshotEntity dsEntity, final long imgCnt,
			final long tsAnnCnt) {
		UserId creatorId = null;
		if (dsEntity instanceof Dataset)
			creatorId = ((Dataset) dsEntity).getCreator().getId();
		final DataSnapshotSearchResult dsSearchResult =
				new DataSnapshotSearchResult(
						dsEntity.getDataSnapshotType(),
						dsEntity.getPubId(),
						dsEntity.getLabel(),
						dsEntity.getCreateTime(),
						creatorId,
						imgCnt,
						tsAnnCnt,
						dsEntity.isFrozen());
		return dsSearchResult;
	}

	public static Set<DataSnapshotSearchResult> assembleDsSearchResults(
			final Set<? extends DataSnapshotEntity> dsEntities) {
		Set<DataSnapshotSearchResult> dsResults = newHashSet();
		for (DataSnapshotEntity dsEntity : dsEntities) {
			dsResults.add(assembleDsSearchResult(dsEntity, -1, -1));
		}
		return dsResults;
	}

	public static DataSnapshotSearchResult assembleDsSearchResult(
			final DataSnapshotEntity dsEntity,
			@Nullable final Set<? extends TimeSeriesEntity> tsEntities,
			long imgCnt,
			long tsAnnCnt,
			Long startTimeUutc,
			Long endTimeUUtc, 
			String organization) {

		UserId creatorId = null;
		if (dsEntity instanceof Dataset)
			creatorId = ((Dataset) dsEntity).getCreator().getId();
		final DataSnapshotSearchResult dsSearchResult =
				new DataSnapshotSearchResult(
						dsEntity.getDataSnapshotType(),
						dsEntity.getPubId(),
						dsEntity.getLabel(),
						dsEntity.getCreateTime(),
						creatorId,
						imgCnt,
						tsAnnCnt,
						dsEntity.isFrozen(),
						assembleTsSearchResult(tsEntities),
						startTimeUutc,
						endTimeUUtc, 
						organization);
		
		return dsSearchResult;
	}

	public static DataSnapshotSearchResult assembleDsSearchResult(
			final DataSnapshotEntity dsEntity,
			@Nullable final Set<? extends TimeSeriesEntity> tsEntities,
			long imgCnt,
			long tsAnnCnt) {

		UserId creatorId = null;
		if (dsEntity instanceof Dataset)
			creatorId = ((Dataset) dsEntity).getCreator().getId();
		final DataSnapshotSearchResult dsSearchResult =
				new DataSnapshotSearchResult(
						dsEntity.getDataSnapshotType(),
						dsEntity.getPubId(),
						dsEntity.getLabel(),
						dsEntity.getCreateTime(),
						creatorId,
						imgCnt,
						tsAnnCnt,
						dsEntity.isFrozen(),
						assembleTsSearchResult(tsEntities));
		return dsSearchResult;
	}

	/**
	 * Returns null if {@code tsEntities} is null.
	 * 
	 * @param tsEntities
	 * 
	 * @return
	 */
	public static Set<TimeSeriesSearchResult> assembleTsSearchResult(
			@Nullable final Set<? extends TimeSeriesEntity> tsEntities) {
		if (tsEntities == null) {
			return null;
		}
		final Set<TimeSeriesSearchResult> tsSearchResults = newHashSet();
		for (final TimeSeriesEntity tsEntity : tsEntities) {
			tsSearchResults.add(assembleTsSearchResult(tsEntity));
		}
		return tsSearchResults;
	}

	public static TimeSeriesSearchResult assembleTsSearchResult(
			final TimeSeriesEntity timeSeriesEntity) {
		final TimeSeriesSearchResult tsSearchResult =
				new TimeSeriesSearchResult(
						timeSeriesEntity.getPubId(),
						timeSeriesEntity.getLabel());
		
		return tsSearchResult;
	}

	private DsSearchResultAssembler() {
		throw new AssertionError();
	}
}
