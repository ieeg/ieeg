package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasExtAcl;
import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@Entity(name = "UserTask")
@Table(name = UserTaskEntity.TABLE)
public class UserTaskEntity
		implements IHasLongId, IHasPubId, IHasExtAcl, IEntity {
	@Override
	public String toString() {
		return "UserTaskEntity [id=" + id + ", extAcl=" + extAcl
				+ ", creators=" + creators + ", taskName=" + taskName
				+ ", pubId=" + pubId + ", activeDate=" + activeDate
				+ ", dueDate=" + dueDate + ", finalDate=" + finalDate
				+ ", description=" + description + ", url=" + url
				+ ", project=" + project + ", resources=" + resources
				+ ", submissionsAllowed=" + submissionsAllowed
				+ ", patternsAllowed=" + patternsAllowed + ", configuration="
				+ configuration + ", validationOperation="
				+ validationOperation + ", formParameters=" + formParameters
				+ "]";
	}



	public final static String TABLE = "user_task";
	
	private Long id;
	
	private ExtAclEntity extAcl;

	List<UserEntity> creators; 
	String taskName;
	String pubId = newUuid();
	Date activeDate;
	Date dueDate;
	Date finalDate;
	
	String description;
	
	String url;
	String project;
	
	List<DataSnapshotEntity> resources;
	
	List<String> submissionsAllowed;
	
	List<String> patternsAllowed;
	
	String configuration;
	String validationOperation;
	
	String formParameters;
	
	public UserTaskEntity() {}

	
	
	public UserTaskEntity(
			Long id, 
			ExtAclEntity extAcl,
			List<UserEntity> creators, 
			String taskName, 
			String taskId,
			Date activeDate, 
			Date dueDate, 
			Date finalDate,
			String description, 
			String url, 
			String project,
			List<DataSnapshotEntity> resources,
			List<String> submissionsAllowed, 
			List<String> patternsAllowed,
			String configuration, 
			String validationOperation,
			String formParameters) {
		super();
		this.id = id;
		this.extAcl = extAcl;
		this.creators = creators;
		this.taskName = taskName;
		this.pubId = taskId;
		this.activeDate = activeDate;
		this.dueDate = dueDate;
		this.finalDate = finalDate;
		this.description = description;
		this.url = url;
		this.project = project;
		this.resources = resources;
		this.submissionsAllowed = submissionsAllowed;
		this.patternsAllowed = patternsAllowed;
		this.configuration = configuration;
		this.validationOperation = validationOperation;
		this.formParameters = formParameters;
	}



	@Override
	@OneToOne(
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true)
	@JoinColumn(name = ExtAclEntity.ID_COLUMN)
	@NotNull
	public ExtAclEntity getExtAcl() {
		return extAcl;
	}

	@ManyToOne
	// not lazy on purpose
	@JoinColumn(name = "creators_list_id")
	@ForeignKey(name = "FK_TASK_CREATORS")
	@NotNull
	public List<UserEntity> getCreators() {
		return creators;
	}

	@Override
	public String getLabel() {
		return taskName;
	}

	@Override
	public String getPubId() {
		return pubId;
	}

	@Override
	public Long getId() {
		return id;
	}



	public String getTaskName() {
		return taskName;
	}



	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}



	public void setPubId(String taskId) {
		this.pubId = taskId;
	}



	public Date getActiveDate() {
		return activeDate;
	}



	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}



	public Date getDueDate() {
		return dueDate;
	}



	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}



	public Date getFinalDate() {
		return finalDate;
	}



	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public String getProject() {
		return project;
	}



	public void setProject(String project) {
		this.project = project;
	}



	public List<DataSnapshotEntity> getResources() {
		return resources;
	}



	public void setResources(List<DataSnapshotEntity> resources) {
		this.resources = resources;
	}



	public List<String> getSubmissionsAllowed() {
		return submissionsAllowed;
	}



	public void setSubmissionsAllowed(List<String> submissionsAllowed) {
		this.submissionsAllowed = submissionsAllowed;
	}



	public List<String> getPatternsAllowed() {
		return patternsAllowed;
	}



	public void setPatternsAllowed(List<String> patternsAllowed) {
		this.patternsAllowed = patternsAllowed;
	}



	public String getConfiguration() {
		return configuration;
	}



	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}



	public String getValidationOperation() {
		return validationOperation;
	}



	public void setValidationOperation(String validationOperation) {
		this.validationOperation = validationOperation;
	}



	public String getFormParameters() {
		return formParameters;
	}



	public void setFormParameters(String formParameters) {
		this.formParameters = formParameters;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public void setExtAcl(ExtAclEntity extAcl) {
		this.extAcl = extAcl;
	}



	public void setCreators(List<UserEntity> creators) {
		this.creators = creators;
	}


	public static final EntityConstructor<UserTaskEntity,String,UserEntity> CONSTRUCTOR = 
			new EntityConstructor<UserTaskEntity,String,UserEntity>(UserTaskEntity.class,
				new IPersistentObjectManager.IPersistentKey<UserTaskEntity,String>() {

			@Override
			public String getKey(UserTaskEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(UserTaskEntity o, String newKey) {
				o.setPubId(newKey);
				
			}

		},
		new EntityPersistence.ICreateObject<UserTaskEntity,String,UserEntity>() {

			@Override
			public UserTaskEntity create(String label, UserEntity creator) {
				List<UserEntity> creators = new ArrayList<UserEntity>();
				creators.add(creator);
				return new UserTaskEntity(null, PersistentObjectFactory.getFactory().getEmptyWorldAcl(), creators, label, 
						null, null, null, null, label, label, label, null, null, null, label, 
						label, label);
			}
		}
				);


	@Override
	public EntityConstructor<UserTaskEntity, String, UserEntity> tableConstructor() {
		return CONSTRUCTOR;
	}

	
}
