/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.braintrust.model.DataSnapshotJson;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IDataSnapshotJsonDAO extends IDAO<DataSnapshotJson, Long> {
	/**
	 * Retrieve the set of triples for the snapshot
	 * @param snapshotId
	 * @return
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public Set<IJsonKeyValue> getAllKeyValues(String snapshotId) throws JsonProcessingException, IOException;
	
	/**
	 * Add a collection of triples to the snapshot (keeping existing ones)
	 * @param snapshotId
	 * @param triples
	 * @throws JsonProcessingException 
	 * @throws IOException 
	 */
	public void addKeyValues(String snapshotId, Collection<? extends IJsonKeyValue> triples) throws JsonProcessingException, IOException;
	
	/**
	 * Replace the set of triples associated with a snapshot with (or add)
	 * a new set of triples
	 *  
	 * @param snapshotId
	 * @param triples
	 * @throws JsonProcessingException 
	 */
	public void setKeyValues(String snapshotId, Collection<? extends IJsonKeyValue> triples) throws JsonProcessingException;
	
	/**
	 * Add a single triple
	 * 
	 * @param snapshotId
	 * @param triple
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public void addKeyValue(String snapshotId, IJsonKeyValue triple) throws JsonProcessingException, IOException;
	
	/**
	 * Remove a triple by its key (if it exists)
	 * @param snapshotId
	 * @param key
	 * @throws IOException 
	 * @throws JsonProcessingException 
	 */
	public void removeKey(String snapshotId, String key) throws JsonProcessingException, IOException;
}
