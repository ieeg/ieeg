/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.webapp;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;

/**
 * @author Sam Donnelly
 */
public final class HibernateSessionPerRequestFilter implements Filter {

	@Override
	public void destroy() {
		// noop
	}

	@Override
	public void doFilter(
			ServletRequest request,
			ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		try {
			Session sess = sf.openSession();
			sess = ManagedSessionContext.bind(sess);
			chain.doFilter(request, response);
		} finally {
			Session sess =
					ManagedSessionContext.unbind(sf);
			PersistenceUtil.close(sess);
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// no op
	}

}
