/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

@GwtCompatible(serializable = true)
public class DataSnapshot implements IHasStringId {

	private String id;
	private String label;
	private Set<TimeSeriesDto> timeSeries = newHashSet();
	private List<TsAnnotationDto> tsAnnotations = newArrayList();
	private Set<Image> images = newHashSet();
	private boolean frozen;
	private String owner = "";
	
	private boolean isHuman = true;
	private boolean isAnimal = false;
	
	private Optional<Long> recordingStartTimeUutc = Optional.absent();

	public DataSnapshot(final String label, boolean frozen) {
		this.label = checkNotNull(label);
		this.frozen = frozen;
	}

	public Set<Image> getImages() {
		return images;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * Returns the id of the data snapshot.
	 * 
	 * @return the id of the data snapshot
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * Returns the time series of the data snapshot.
	 * 
	 * @return the time series of the data snapshot
	 */
	public Set<TimeSeriesDto> getTimeSeries() {
		return timeSeries;
	}

	/**
	 * @return the tsAnnotations
	 */
	public List<TsAnnotationDto> getTsAnnotations() {
		return tsAnnotations;
	}
	
	public Optional<Long> getRecordingStartTimeUutc() {
		return recordingStartTimeUutc;
	}

	public boolean isFrozen() {
		return frozen;
	}

	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(final Set<Image> images) {
		this.images = checkNotNull(images);
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(final String label) {
		this.label = checkNotNull(label);
	}

	/**
	 * @param revId the revId to set
	 */
	public void setRevId(@Nullable final String revId) {
		this.id = revId;
	}

	public void setTimeSeries(final Set<TimeSeriesDto> timeSeries) {
		this.timeSeries = checkNotNull(timeSeries);
	}

	/**
	 * @param tsAnnotations the tsAnnotations to set
	 */
	public void setTsAnnotations(
			final List<TsAnnotationDto> tsAnnotations) {
		this.tsAnnotations = checkNotNull(tsAnnotations);
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isHuman() {
		return isHuman;
	}

	public void setHuman(boolean isHuman) {
		this.isHuman = isHuman;
	}

	public boolean isAnimal() {
		return isAnimal;
	}

	public void setAnimal(boolean isAnimal) {
		this.isAnimal = isAnimal;
	}

	public void setRecordingStartTimeUutc(long recordingStartTimeUutc) {
		this.recordingStartTimeUutc = Optional.of(Long.valueOf(recordingStartTimeUutc));
	}
	
}
