/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.permissions.hibernate;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import com.google.common.base.Function;
import com.google.common.base.Joiner;

import edu.upenn.cis.braintrust.dao.GenericHibernateDAOLongId;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.model.PermissionDomainEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;

public final class PermissionDAOHibernate
		extends GenericHibernateDAOLongId<PermissionEntity>
		implements IPermissionDAO {
	static private final Function<ExtPermission, String> dtoToTuple = new Function<ExtPermission, String>() {

		@Override
		public String apply(ExtPermission input) {
			return "('" + input.getValue() + "', '" + input.getMode()
					+ "', '"
					+ input.getDomain() + "')";
		}
	};

	static private final Joiner tupleJoiner = Joiner.on(",");

	public PermissionDAOHibernate() {

	}

	public PermissionDAOHibernate(final Session session) {
		setSession(session);
	}

	@Override
	public PermissionEntity findByDomainModeAndName(String permissionTarget,
			String mode, String name) {
		return (PermissionEntity) getSession()
				.getNamedQuery(PermissionEntity.BY_DOMAIN_MODE_AND_NAME)
				.setParameter("domain", permissionTarget)
				.setParameter("mode", mode)
				.setParameter("name", name)
				.uniqueResult();
	}

	@Override
	public Set<PermissionEntity> findByDtos(Set<ExtPermission> permDtos) {
		if (permDtos.size() == 0) {
			return Collections.emptySet();
		}
		final String query = "select p "
				+ "from Permission p "
				+ "left join p.mode mode "
				+ "left join mode.domain domain where (p.name, mode.name, domain.name) in (";
		final StringBuilder sb = new StringBuilder(query);

		sb.append(tupleJoiner.join(transform(permDtos, dtoToTuple)));
		sb.append(")");

		@SuppressWarnings("unchecked")
		List<PermissionEntity> list = getSession().createQuery(sb.toString())
				.list();
		return newHashSet(list);
	}

	@Override
	public PermissionEntity findStarCoreOwnerPerm() {
		return findByDomainModeAndName(
				PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
				CorePermDefs.CORE_MODE_NAME,
				CorePermDefs.OWNER_PERMISSION_NAME);
	}

	@Override
	public PermissionEntity findStarCoreReadPerm() {
		return findByDomainModeAndName(
				PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
				CorePermDefs.CORE_MODE_NAME,
				CorePermDefs.READ_PERMISSION_NAME);
	}

	@Override
	public List<PermissionEntity> findByDomainAndMode(
			HasAclType permissionDomain,
			String mode) {
		@SuppressWarnings("unchecked")
		List<PermissionEntity> result = getSession()
				.getNamedQuery(PermissionEntity.BY_DOMAIN_AND_MODE)
				.setParameter("domain", permissionDomain.getEntityName())
				.setParameter("mode", mode).list();
		return result;
	}

	@Override
	public PermissionEntity findStarCoreEditPerm() {
		return findByDomainModeAndName(
				PermissionDomainEntity.PERM_DOMAIN_WILDCARD,
				CorePermDefs.CORE_MODE_NAME,
				CorePermDefs.EDIT_PERMISSION_NAME);
	}
}