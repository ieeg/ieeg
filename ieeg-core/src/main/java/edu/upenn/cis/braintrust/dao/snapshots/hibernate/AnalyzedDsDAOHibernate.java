/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.snapshots.hibernate;

import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import org.hibernate.Session;

import edu.upenn.cis.braintrust.dao.snapshots.IAnalyzedDataSnapshotDAO;
import edu.upenn.cis.braintrust.model.AnalyzedDataSnapshot;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class AnalyzedDsDAOHibernate
		extends GenericHibernateDAO<AnalyzedDataSnapshot, Long>
		implements IAnalyzedDataSnapshotDAO {
	public AnalyzedDsDAOHibernate() {
	}

	public AnalyzedDsDAOHibernate(final Session session) {
		setSession(session);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<AnalyzedDataSnapshot> findByDsPubId(
			String studyRevId) {
		return newHashSet(getSession()
				.getNamedQuery(AnalyzedDataSnapshot.ANALYZED_DS_BY_DS_ID)
				.setParameter("dataSnapshotPubId", studyRevId).list());
	}

	@Override
	public String findBaseDataset(String dsPubId) {
		Set<AnalyzedDataSnapshot> values = newHashSet(getSession()
				.getNamedQuery(AnalyzedDataSnapshot.ANALYZED_PARENT)
				.setParameter("dataSnapshotPubId", dsPubId).list());
		
		if (values.isEmpty())
			return null;
		else
			return values.iterator().next().getDataSnapshot().getPubId();
	}

}