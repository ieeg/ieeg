/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.IHasLongId;

/**
 * A recording
 * 
 * @author John Frommeyer
 * 
 */
public class GwtRecording implements IHasLongId, IHasVersion, Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Integer version;

	/** The contact groups used in this recording. */
	private Set<GwtContactGroup> contactGroups = newHashSet();
	private Optional<String> refElectrodeDescription;
	private String imagesFile;
	private String reportFile;
	/** The relative path that is home to the study */
	// start at Xyz_IEED_data
	private String dir;
	/** The relative path to dir of the mef directory. */
	private String mefDir;
	private Long startTimeUutc;
	private Long endTimeUutc;
	private String commentary;

	@SuppressWarnings("unused")
	private GwtRecording() {}

	public GwtRecording(
			final String dir,
			final String mefDir,
			Long startTimeUutc,
			Long endTimeUutc,
			@Nullable String refElectrodeDescription,
			@Nullable final String imagesFile,
			@Nullable final String reportFile,
			@Nullable String commentary,
			@Nullable Long id,
			@Nullable Integer version) {
		this.dir = checkNotNull(dir);
		this.mefDir = checkNotNull(mefDir);
		this.startTimeUutc = checkNotNull(startTimeUutc);
		this.endTimeUutc = checkNotNull(endTimeUutc);
		this.refElectrodeDescription = Optional
				.fromNullable(refElectrodeDescription);
		this.imagesFile = imagesFile;
		this.reportFile = reportFile;
		this.id = id;
		this.version = version;
		this.setCommentary(commentary);
	}

	/**
	 * Add {@code contact} to this recording.
	 * 
	 * @param contactGroup to be added
	 * 
	 * @throws IllegalArgumentException if the contact group already has a parent
	 * @throws IllegalArgumentException if this recording already contains
	 *             {@code contactGroup}
	 */
	public void addContactGroup(final GwtContactGroup contactGroup) {
		checkNotNull(contactGroup);
		checkArgument(contactGroup.getParent() == null,
				"contact group already has a parent");
		checkArgument(!contactGroups.contains(contactGroup),
				"this recordings's contact groups already contains contactGroup");
		contactGroups.add(contactGroup);
		contactGroup.setParent(this);
	}

	/**
	 * @return the dirUri
	 */
	public String getDir() {
		return dir;
	}

	public Set<GwtContactGroup> getContactGroups() {
		return contactGroups;
	}

	/**
	 * The end time of EEG monitoring.
	 * 
	 * @return the end time of EEG monitoring
	 */
	public Long getEndTimeUutc() {
		return endTimeUutc;
	}

	public Long getId() {
		return id;
	}

	/** Relative path from dir. */
	public String getImagesFile() {
		return imagesFile;
	}

	/**
	 * The relative path to dir of the mef directory.
	 * 
	 * @return the mefDir
	 */
	public String getMefDir() {
		return mefDir;
	}

	public Optional<String> getRefElectrodeDescription() {
		return refElectrodeDescription;
	}

	/** Relative path from dir. */
	public String getReportFile() {
		return reportFile;
	}

	/**
	 * The start time of EEG monitoring.
	 * 
	 * @return the start time of EEG monitoring
	 */
	public Long getStartTimeUutc() {
		return startTimeUutc;
	}

	public Integer getVersion() {
		return version;
	}

	/**
	 * Remove {@code contactGroup} from this recording.
	 * 
	 * @param contactGroup to be removed
	 * 
	 * @throws IllegalArgumentException if this is not the contactGroup's parent
	 * @throws IllegalArgumentException if this recording does not contain
	 *             {@code contactGroup}
	 */
	public void removeContactGroup(final GwtContactGroup contactGroup) {
		checkNotNull(contactGroup);
		checkArgument(this.equals(contactGroup.getParent()),
				"this recording is not contactGroup's parent");
		checkArgument(contactGroups.contains(contactGroup),
				"this recording does not contain contactGroup");
		contactGroups.remove(contactGroup);
		contactGroup.setParent(null);
	}

	/**
	 * @param dirUri the dirUri to set
	 */
	public void setDir(final String dirUri) {
		this.dir = dirUri;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTimeUutc(final Long endTime) {
		this.endTimeUutc = endTime;
	}

	@VisibleForTesting
	public void setId(@Nullable Long id) {
		this.id = id;
	}

	public void setImagesFile(final String imagesFile) {
		this.imagesFile = imagesFile;
	}

	/**
	 * @param mefDir the mefDir to set
	 */
	public void setMefDir(final String mefDir) {
		this.mefDir = mefDir;
	}

	public void setRefElectrodeDescription(
			@Nullable String refElectrodeDescription) {
		this.refElectrodeDescription = Optional
				.fromNullable(refElectrodeDescription);
	}

	public void setReportFile(final String reportFile) {
		this.reportFile = reportFile;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTimeUutc(final Long startTime) {
		this.startTimeUutc = startTime;
	}

	@VisibleForTesting
	public void setVersion(@Nullable final Integer version) {
		this.version = version;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(@Nullable String commentary) {
		this.commentary = commentary;
	}

}
