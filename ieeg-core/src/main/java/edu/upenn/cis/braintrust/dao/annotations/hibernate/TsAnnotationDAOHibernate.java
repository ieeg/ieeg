/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.annotations.hibernate;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newTreeSet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NamedQuery;

import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.type.StandardBasicTypes;

import com.google.common.base.Throwables;

import edu.upenn.cis.braintrust.dao.annotations.AnnotationChannelInfo;
import edu.upenn.cis.braintrust.dao.annotations.ITsAnnotationDAO;
import edu.upenn.cis.braintrust.datasnapshot.IAnnotationWriter;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TsAnnotationEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.AnnotationCube;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TimeSeriesId;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public final class TsAnnotationDAOHibernate extends
		GenericHibernateDAO<TsAnnotationEntity, Long>
		implements ITsAnnotationDAO {

	private Configuration hibernateConfig;

	public TsAnnotationDAOHibernate() {}

	public TsAnnotationDAOHibernate(
			final Session session,
			final Configuration hibernateConfig) {
		setSession(session);
		this.hibernateConfig = hibernateConfig;
	}

	@Override
	public List<TsAnnotationEntity> findByParentOrderByStartTimeAndId(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults) {
		Query q = getSession()
				.getNamedQuery(
						TsAnnotationEntity.BY_LAYER_AND_START_TIME)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ))
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setString("layer", layer);

		@SuppressWarnings("unchecked")
		List<TsAnnotationEntity> tsAnns = (List<TsAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return tsAnns;
	}

	@Override
	public List<TsAnnotationEntity> findByParentOrderByStartTimeAndId(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			Set<TimeSeriesEntity> timeSeries,
			int firstResult,
			int maxResults) {
		if (timeSeries.size() == 0) {
			return Collections.emptyList();
		}
		Query q = getSession()
				.getNamedQuery(
						TsAnnotationEntity.BY_LAYER_AND_START_TIME_CONTAINS)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ))
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setString("layer", layer)
				.setParameterList("timeSeries", timeSeries);

		@SuppressWarnings("unchecked")
		List<TsAnnotationEntity> tsAnns = (List<TsAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return tsAnns;
	}

	@Override
	public List<TsAnnotationEntity> findLtStart(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			int firstResult,
			int maxResults) {
		Query q = getSession()
				.getNamedQuery(
						TsAnnotationEntity.LT_START_TIME)
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setString("layer", layer)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ));

		@SuppressWarnings("unchecked")
		List<TsAnnotationEntity> tsAnns = (List<TsAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return tsAnns;
	}

	@Override
	public List<TsAnnotationEntity> findLtStart(
			DataSnapshotEntity parent,
			long startOffsetUsecs,
			String layer,
			Set<TimeSeriesEntity> timeSeries,
			int firstResult,
			int maxResults) {
		if (timeSeries.size() == 0) {
			return Collections.emptyList();
		}

		Query q = getSession()
				.getNamedQuery(
						TsAnnotationEntity.LT_START_TIME_CONTAINS)
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setString("layer", layer)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ))
				.setParameterList("timeSeries", timeSeries);

		@SuppressWarnings("unchecked")
		List<TsAnnotationEntity> tsAnns = (List<TsAnnotationEntity>) q
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.list();

		return tsAnns;
	}

	@Override
	public int setLayerName(
			DataSnapshotEntity parent,
			String originalName,
			String newName) {
		Query q = getSession()
				.getNamedQuery(TsAnnotationEntity.SET_LAYER_NAME)
				.setParameter("parent", parent)
				.setString("originalName", originalName)
				.setString("newName", newName);
		int updated = q.executeUpdate();
		return updated;
	}

	@Override
	public List<TsAnnotationEntity> findFromIdAndStart(
			DataSnapshotEntity parent,
			long lastTsaId,
			long startOffsetUsecs,
			String layer,
			Set<String> tsPubIds,
			int maxCount) {
		if (tsPubIds.size() == 0) {
			return Collections.emptyList();
		}

		Query q = getSession()
				.getNamedQuery(
						TsAnnotationEntity.BY_LAYER_FROM_ID_AND_START)
				.setParameter("parent", parent)
				.setLong("lastTsaId", lastTsaId)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.setString("layer", layer)
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ))
				.setParameterList("tsPubIds", tsPubIds);

		@SuppressWarnings("unchecked")
		List<TsAnnotationEntity> tsAnns = (List<TsAnnotationEntity>) q
				.setMaxResults(maxCount)
				.list();

		return tsAnns;
	}

	@Override
	public long countByParentAndStartTime(
			DataSnapshotEntity parent,
			long startOffsetUsecs) {

		long count = (Long) getSession()
				.getNamedQuery(
						TsAnnotationEntity.COUNT_BY_PARENT_START_TIME)
				.setParameter("parent", parent)
				.setLong("startOffsetUsecs", startOffsetUsecs)
				.uniqueResult();

		return count;
	}

	@Override
	public int insertAll(
			final DataSnapshotEntity srcParent,
			final DataSnapshotEntity derivedParent) {
		final String q = "insert into ts_annotation ("
				+ "annotator, "
				+ "creator_id, "
				+ "description, "
				+ "end_time, "
				+ "create_time, "
				+ "start_time, "
				+ "type, "
				+ "obj_version, "
				+ "parent_id, "
				+ "layer, "
				+ "color, "
				+ "src_ann_id) "
				+ "select "
				+ "src_tsa.annotator, "
				+ "src_tsa.creator_id, "
				+ "src_tsa.description, "
				+ "src_tsa.end_time, "
				+ ":derivedRevTimestamp, "
				+ "src_tsa.start_time, "
				+ "src_tsa.type, "
				+ ":version, "
				+ ":derivedParentId, "
				+ "src_tsa.layer, "
				+ "src_tsa.color, "
				+ "src_tsa.ts_annotation_id "
				+ "from ts_annotation src_tsa "
				+ "where src_tsa.parent_id = :srcParentId "
				+ "and src_tsa.ts_annotation_id in "
				+ "(select src_tsa.ts_annotation_id "
				+ "from ts_annotation src_tsa "
				+ "join ts_annotation_time_series src_tsa_ts on src_tsa_ts.ts_annotation_id = src_tsa.ts_annotation_id "
				+ "where src_tsa.parent_id = :srcParentId "
				+ "and src_tsa_ts.time_series_id in "
				+ "(select der_ds_ts.time_series_id "
				+ "from dataset_time_series der_ds_ts "
				+ "where der_ds_ts.dataset_id = :derivedParentId))";
		int noAnnsInserted = getSession().createSQLQuery(q)
				.setDate("derivedRevTimestamp", new Date())
				.setInteger("version", 0)
				.setLong("srcParentId", srcParent.getId())
				.setLong("derivedParentId", derivedParent.getId())
				.executeUpdate();

		if (noAnnsInserted > 0) {

			final String q2 = "insert into ts_annotation_time_series ("
					+ "ts_annotation_id, "
					+ "time_series_id) "
					+ "select "
					+ "der_tsa.ts_annotation_id, "
					+ "src_tsa_ts.time_series_id "
					+ "from ts_annotation_time_series src_tsa_ts "
					+ "join ts_annotation src_tsa on src_tsa.ts_annotation_id = src_tsa_ts.ts_annotation_id, "
					+ "ts_annotation der_tsa "
					+ "where src_tsa.parent_id = :srcParentId "
					+ "and der_tsa.parent_id = :derivedParentId "
					+ "and src_tsa.ts_annotation_id = der_tsa.src_ann_id "
					+ "and src_tsa_ts.time_series_id in "
					+ "(select der_ds_ts.time_series_id "
					+ "from dataset_time_series der_ds_ts "
					+ "where der_ds_ts.dataset_id = :derivedParentId)";

			int noJoinRowsInserted =
					getSession()
							.createSQLQuery(q2)
							.setLong("srcParentId", srcParent.getId())
							.setLong("derivedParentId", derivedParent.getId())
							.executeUpdate();

			if (noJoinRowsInserted == 0) {
				throw new AssertionError();
			}
		}

		return noAnnsInserted;
	}

	@Override
	public int insertWithRevId(
			final String srcTsAnnRevId,
			final DataSnapshotEntity srcParent,
			final Dataset derivedParent) {
		final String q = "insert into ts_annotation ("
				+ "annotator, "
				+ "creator_id, "
				+ "description, "
				+ "end_time, "
				+ "create_time, "
				+ "start_time, "
				+ "type, "
				+ "obj_version, "
				+ "parent_id, "
				+ "layer, color) "
				+ "select "
				+ "src_tsa.annotator, "
				+ "src_tsa.creator_id, "
				+ "src_tsa.description, "
				+ "src_tsa.end_time, "
				+ ":derivedRevTimestamp, "
				+ "src_tsa.start_time, "
				+ "src_tsa.type, "
				+ ":version, "
				+ ":derivedParentId, "
				+ "src_tsa.layer, "
				+ "src_tsa.color "
				+ "from ts_annotation src_tsa "
				+ "where src_tsa.parent_id = :srcParentId "
				+ "and src_tsa.ts_annotation_id = :srcTsAnnRevId "
				+ "and src_tsa.ts_annotation_id in "
				+ "(select src_tsa.ts_annotation_id "
				+ "from ts_annotation src_tsa "
				+ "join ts_annotation_time_series src_tsa_ts on src_tsa_ts.ts_annotation_id = src_tsa.ts_annotation_id "
				+ "where src_tsa.parent_id = :srcParentId "
				+ "and src_tsa_ts.time_series_id in "
				+ "(select der_ds_ts.time_series_id "
				+ "from dataset_time_series der_ds_ts "
				+ "where der_ds_ts.dataset_id = :derivedParentId))";
		int noAnnsInserted = getSession().createSQLQuery(q)
				.setDate("derivedRevTimestamp", new Date())
				.setInteger("version", 0)
				.setLong("derivedParentId", derivedParent.getId())
				.setLong("srcParentId", srcParent.getId())
				.setLong("srcTsAnnRevId", Long.valueOf(srcTsAnnRevId))
				.executeUpdate();

		if (noAnnsInserted > 0) {

			final String q2 = "insert into ts_annotation_time_series ("
					+ "ts_annotation_id, "
					+ "time_series_id) "
					+ "select "
					+ "der_tsa.ts_annotation_id, "
					+ "src_tsa_ts.time_series_id "
					+ "from ts_annotation_time_series src_tsa_ts "
					+ "join ts_annotation src_tsa on src_tsa.ts_annotation_id = src_tsa_ts.ts_annotation_id, "
					+ "ts_annotation der_tsa "
					+ "where src_tsa.parent_id = :srcParentId "
					+ "and der_tsa.parent_id = :derivedParentId "
					+ "and src_tsa.ts_annotation_id = :srcTsAnnRevId "
					+ "and src_tsa_ts.time_series_id in "
					+ "(select der_ds_ts.time_series_id "
					+ "from dataset_time_series der_ds_ts "
					+ "where der_ds_ts.dataset_id = :derivedParentId)";

			int noJoinRowsInserted =
					getSession()
							.createSQLQuery(q2)
							.setLong("srcParentId", srcParent.getId())
							.setLong("derivedParentId", derivedParent.getId())
							.setLong("srcTsAnnRevId",
									Long.valueOf(srcTsAnnRevId))
							.executeUpdate();

			if (noJoinRowsInserted == 0) {
				throw new AssertionError();
			}
		}
		return noAnnsInserted;
	}

	@Override
	public int deleteByParent(DataSnapshotEntity ds) {
		// Using sql here so that the ts_annotation_time_series join table
		// entries can be deleted by an "on delete cascade" in the ddl.
		// Otherwise deleting from the join table was too slow.
		final String q = "delete from ts_annotation "
				+ "where parent_id = :parentId";
		return getSession()
				.createSQLQuery(q)
				.setLong("parentId", ds.getId())
				.executeUpdate();
	}

	@Override
	public int deleteByParentAndId(DataSnapshotEntity ds, String tsaId) {
		// Using sql here so that the ts_annotation_time_series join table
		// entries can be deleted by an "on delete cascade" in the ddl.
		// Otherwise deleting from the join table was too slow.
		return getSession()
				.getNamedQuery(TsAnnotationEntity.DELETE_BY_PARENT_AND_ID)
				.setLong("parentId", ds.getId())
				.setLong("tsaId", Long.parseLong(tsaId))
				.executeUpdate();
	}

	@Override
	public int deleteByParentAndIds(DataSnapshotEntity ds, Set<String> tsaIds) {

		if (tsaIds.isEmpty()) {
			return 0;
		}

		List<Long> ids = new ArrayList<Long>();
		for (String id : tsaIds)
			ids.add(Long.parseLong(id));

		// Using sql here so that the ts_annotation_time_series join table
		// entries can be deleted by an "on delete cascade" in the ddl.
		// Otherwise deleting from the join table was too slow.
		return getSession()
				.getNamedQuery(TsAnnotationEntity.DELETE_BY_PARENT_AND_IDS)
				.setLong("parentId", ds.getId())
				.setParameterList("tsaIds", ids)
				.executeUpdate();


	}

	@Override
	public TsAnnotationEntity findTsAnn(DataSnapshotEntity ds, String tsAnnRevId) {
		return (TsAnnotationEntity) getSession()
				.getNamedQuery(DataSnapshotEntity.FIND_TS_ANNOTATION)
				.setParameter("ds", ds)
				.setLong("tsAnnRevId", Long.valueOf(tsAnnRevId))
				.setLockOptions(new LockOptions(LockMode.PESSIMISTIC_READ))
				.uniqueResult();
	}

	@Override
	public int deleteByLayer(
			DataSnapshotEntity parent,
			String layer) {
		// Using sql here so that the ts_annotation_time_series join table
		// entries can be deleted by an "on delete cascade" in the ddl.
		// Otherwise deleting from the join table was too slow.
		final String q = "delete from ts_annotation "
				+ "where parent_id = :parentId "
				+ "and layer = :layer";

		return getSession()
				.createSQLQuery(q)
				.setLong("parentId", parent.getId())
				.setString("layer", layer)
				.executeUpdate();
	}

	@Override
	public int removeAnnotatedAndDeleteEmptyAnnotations(
			DataSnapshotEntity parent,
			TimeSeriesEntity annotated) {
		String q = "delete from ts_annotation_time_series "
				+ "where time_series_id = :tsId and exists "
				+ "(select 1 from ts_annotation tsa where tsa.ts_annotation_id = ts_annotation_time_series.ts_annotation_id and tsa.parent_id = :parentId "
				+ "and ts_annotation_time_series.time_series_id = :tsId ";
		if (isMysql(hibernateConfig)) {
			q += "lock in share mode)";
		} else {
			q += "for update)";
		}
		getSession().createSQLQuery(q)
				.setLong("parentId", parent.getId())
				.setLong("tsId", annotated.getId())
				.executeUpdate();
		String q2 = "delete from ts_annotation "
				+ "where parent_id = :parentId "
				+ "and not exists(select 1 from ts_annotation_time_series tsa_ts where tsa_ts.ts_annotation_id = ts_annotation.ts_annotation_id)";

		return getSession()
				.createSQLQuery(q2)
				.setParameter("parentId", parent.getId())
				.executeUpdate();
	}

	@Override
	public List<TsAnnotationDto> findAllByParent(
			final DataSnapshotEntity parent) {
		String q = "select "
				+ "tsannotati0_.ts_annotation_id as tsa_ts_annotation_id, "
				+ "tsannotati0_.annotator as tsa_annotator, "
				+ "tsannotati0_.description as tsa_description, "
				+ "tsannotati0_.end_time as tsa_end_time, "
				+ "tsannotati0_.layer as tsa_layer, "
				+ "tsannotati0_.src_ann_id as tsa_src_ann_id, "
				+ "tsannotati0_.start_time as tsa_start_time, "
				+ "tsannotati0_.type as tsa_type, "
				+ "tsannotati0_.color as tsa_color, "
				+ "timeseries2_.file_key as ts_file_key, "
				+ "timeseries2_.label as ts_label, "
				+ "timeseries2_.pub_id as ts_pub_id "
				// + "annotated1_.ts_annotation_id as ts1_23_0__, "
				// + "annotated1_.time_series_id as time2_0__ "
				+ "from "
				+ "ts_annotation tsannotati0_ "
				+ "left outer join "
				+ "ts_annotation_time_series annotated1_ "
				+ "on tsannotati0_.ts_annotation_id = annotated1_.ts_annotation_id "
				+ "left outer join "
				+ "time_series timeseries2_ "
				+ "on annotated1_.time_series_id = timeseries2_.time_series_id "
				+ "where "
				+ "tsannotati0_.parent_id = :parentId "
				+ "order by "
				+ "tsa_ts_annotation_id ";

		Query query = getSession()
				.createSQLQuery(q)
				.addScalar("tsa_ts_annotation_id", StandardBasicTypes.LONG)
				.addScalar("tsa_annotator", StandardBasicTypes.STRING)
				.addScalar("tsa_description", StandardBasicTypes.STRING)
				.addScalar("tsa_end_time", StandardBasicTypes.LONG)
				.addScalar("tsa_layer", StandardBasicTypes.STRING)
				.addScalar("tsa_start_time", StandardBasicTypes.LONG)
				.addScalar("tsa_type", StandardBasicTypes.STRING)
				.addScalar("tsa_color", StandardBasicTypes.STRING)
				.addScalar("ts_file_key", StandardBasicTypes.STRING)
				.addScalar("ts_label", StandardBasicTypes.STRING)
				.addScalar("ts_pub_id", StandardBasicTypes.STRING)
				.setLong("parentId", parent.getId());

		if (isMysql(hibernateConfig)) {
			query.setFetchSize(Integer.MIN_VALUE);
		}

		ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
		List<TsAnnotationDto> tsAnnDtos = newArrayList();
		if (results.next()) {
			boolean done = false;
			while (!done) {
				Long tsAnnId = results.getLong(0);
				String annotator = results.getString(1);
				String description = results.getString(2);
				Long endOffsetUsecs = results.getLong(3);
				String layer = results.getString(4);
				Long startOffsetUsecs = results.getLong(5);
				String type = results.getString(6);
				String color = results.getString(7);

				Set<TimeSeriesDto> tss = newHashSet();

				while (true) {
					if (results.getString(9) != null) {
						TimeSeriesDto ts =
								new TimeSeriesDto(
										results.getString(10),
										results.getString(9),
										results.getString(8));
						tss.add(ts);
					}
					if (results.next() == false) {
						done = true;
						break;
					}
					Long newTsAnnId = results.getLong(0);
					if (!newTsAnnId.equals(tsAnnId)) {
						break;
					}
				}

				TsAnnotationDto tsAnnDto =
						new TsAnnotationDto(
								tss,
								annotator,
								startOffsetUsecs,
								endOffsetUsecs,
								type,
								String.valueOf(tsAnnId),
								description,
								null,
								layer,
								color);

				tsAnnDtos.add(tsAnnDto);
			}
		}
		results.close();

		return tsAnnDtos;
	}

	@Override
	public long findContributedAnnotationCount(UserEntity user) {
		return (Long) getSession()
				.createQuery(
						"select count(*) " +
								"from TsAnnotation a "
								+ "where a.creator = :user ")
				.setParameter("user", user)
				.uniqueResult();
	}

	@Override
	public Map<String, Long> countLayers(DataSnapshotEntity ds) {
		@SuppressWarnings("rawtypes")
		List results =
				getSession().getNamedQuery(
						TsAnnotationEntity.COUNT_LAYERS)
						.setParameter("parent", ds)
						.list();
		Map<String, Long> layers2Counts = newHashMap();
		for (Object result : results) {
			Object[] resultObjArr = (Object[]) result;
			String layer = (String) resultObjArr[0];
			Long count = (Long) resultObjArr[1];
			layers2Counts.put(layer, count);
		}
		return layers2Counts;
	}

	public void setHibernateConfig(Configuration hibernateConfig) {
		this.hibernateConfig = hibernateConfig;
	}

	@Override
	public void streamTsAnnotations(
			DataSnapshotEntity ds,
			IAnnotationWriter annotationWriter) {

		try {

			String q = "select "
					+ "tsannotati0_.ts_annotation_id as tsa_id, "
					+ "tsannotati0_.annotator as tsa_annotator, "
					+ "tsannotati0_.description as tsa_description, "
					+ "tsannotati0_.end_time as tsa_end_time, "
					+ "tsannotati0_.layer as tsa_layer, "
					+ "tsannotati0_.src_ann_id as tsa_src_ann_id, "
					+ "tsannotati0_.start_time as tsa_start_time, "
					+ "tsannotati0_.type as tsa_type, "
					+ "tsannotati0_.color as tsa_color, "
					+ "tsannotati0_.create_time as tsa_create_time, "
					+ "tsannotati0_.creator_id as tsa_creator_id, "
					+ "timeseries2_.file_key as ts_file_key, "
					+ "timeseries2_.label as ts_label, "
					+ "timeseries2_.pub_id as ts_pub_id "

					// + "annotated1_.ts_annotation_id as ts1_23_0__, "
					// + "annotated1_.time_series_id as time2_0__ "
					+ "from "
					+ "ts_annotation tsannotati0_ "
					+ "left outer join "
					+ "ts_annotation_time_series annotated1_ "
					+ "on tsannotati0_.ts_annotation_id = annotated1_.ts_annotation_id "
					+ "left outer join "
					+ "time_series timeseries2_ "
					+ "on annotated1_.time_series_id = timeseries2_.time_series_id "
					+ "where "
					+ "tsannotati0_.parent_id = :parentId "
					+ "order by "
					+ "tsa_layer, "
					+ "tsa_start_time, "
					+ "tsa_id ";

			Query query = getSession()
					.createSQLQuery(q)
					.addScalar("tsa_id", StandardBasicTypes.LONG)
					.addScalar("tsa_annotator", StandardBasicTypes.STRING)
					.addScalar("tsa_description", StandardBasicTypes.STRING)
					.addScalar("tsa_end_time", StandardBasicTypes.LONG)
					.addScalar("tsa_layer", StandardBasicTypes.STRING)
					.addScalar("tsa_start_time", StandardBasicTypes.LONG)
					.addScalar("tsa_type", StandardBasicTypes.STRING)
					.addScalar("tsa_color", StandardBasicTypes.STRING)
					.addScalar("tsa_create_time", StandardBasicTypes.TIMESTAMP)
					.addScalar("tsa_creator_id", StandardBasicTypes.LONG)
					.addScalar("ts_file_key", StandardBasicTypes.STRING)
					.addScalar("ts_label", StandardBasicTypes.STRING)
					.addScalar("ts_pub_id", StandardBasicTypes.STRING)
					.setLong("parentId", ds.getId());

			if (isMysql(hibernateConfig)) {
				query.setFetchSize(Integer.MIN_VALUE);
			}

			annotationWriter.start();

			ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
			if (results.next()) {
				boolean done = false;
				while (!done) {
					Long tsAnnId = results.getLong(0);
					String annotator = results.getString(1);
					String description = results.getString(2);
					Long endOffsetUsecs = results.getLong(3);
					String layer = results.getString(4);
					Long startOffsetUsecs = results.getLong(5);
					String type = results.getString(6);
					String color = results.getString(7);
					Date createTime = results.getDate(8);
					UserId creatorId = new UserId(results.getLong(9));

					Set<AnnotationChannelInfo> channelFileNames = newTreeSet();

					while (true) {
						if (results.getString(12) != null) {
							String label = results.getString(11);
							String clientSideFileName = label + ".mef";
							TimeSeriesId chId =
									new TimeSeriesId(results.getString(12));
							String fileKey = results.getString(10);
							channelFileNames.add(
									new AnnotationChannelInfo(
											clientSideFileName,
											chId,
											fileKey));
						}
						if (results.next() == false) {
							done = true;
							break;
						}
						Long newTsAnnId = results.getLong(0);
						if (!newTsAnnId.equals(tsAnnId)) {
							break;
						}
					}
					annotationWriter.write(
							tsAnnId,
							type,
							description,
							startOffsetUsecs,
							endOffsetUsecs,
							createTime,
							layer,
							annotator,
							creatorId,
							color,
							channelFileNames);
				}
			}
			results.close();
			annotationWriter.finish();

		} catch (IOException ioe) {
			throw Throwables.propagate(ioe);
		}
	}

	@Override
	public List<AnnotationCube> getAnnotationCubeInfo() {
		String q = "SELECT distinct ts.pub_id, tsa.annotator, tsa.layer, tsa.type " +
				    "FROM ts_annotation tsa, ts_annotation_time_series ats, time_series ts " +
				    "WHERE tsa.ts_annotation_id = ats.ts_annotation_id AND ats.time_series_id = ts.time_series_id";
		
		List<AnnotationCube> ret = new ArrayList<AnnotationCube>();
		
		Query query = getSession()
				.createSQLQuery(q)
				.addScalar("pub_id", StandardBasicTypes.STRING)
				.addScalar("annotator", StandardBasicTypes.STRING)
				.addScalar("layer", StandardBasicTypes.STRING)
				.addScalar("type", StandardBasicTypes.STRING);

//		if (isMysql(hibernateConfig)) {
//			query.setFetchSize(10000);
//		}

		ScrollableResults results = query.scroll(ScrollMode.FORWARD_ONLY);
		if (results.first()) {
			do {
				ret.add(new AnnotationCube(
						results.getString(0), 
						results.getString(1), 
						results.getString(2), 
						results.getString(3), 
						1));

			} while (results.next());
		}
		results.close();

		return ret;
	}
}
