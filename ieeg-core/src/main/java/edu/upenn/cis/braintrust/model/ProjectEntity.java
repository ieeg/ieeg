/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import static edu.upenn.cis.braintrust.BtUtil.newUuid;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NaturalId;

import com.google.common.collect.Sets;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
	@NamedQuery(name = ProjectEntity.BY_USER,
			// query = "select distinct p from Project p "
			// + "join p.team t "
			// + "join p.admins a "
			// + "where t = :user "
			// + "or a = :user"
			query = "select distinct p from Project p "
					+ "join fetch p.team "
					+ "join fetch p.admins "
					+ "left join fetch p.snapshots "
					+ "left join fetch p.tools "
					+ "where :user in elements(p.team) "
					+ "or :user in elements(p.admins) "
	),
	@NamedQuery(name = ProjectEntity.TO_EEG_PROJECT,
			query = "select "
					+ "p.projectId, "
					+ "p.pubId, "
					+ "p.name, "
					+ "p.complete, "
					+ "tag, "
					+ "credit, "
					+ "admins.id, "
					+ "team.id, "
					+ "snaps.pubId, "
					+ "tools.pubId "
					+ "from Project p "
					+ "join p.team team "
					+ "join p.admins admins "
					+ "left join p.tags tag "
					+ "left join p.credits credit "
					+ "left join p.snapshots snaps "
					+ "left join p.tools tools "
					+ "where :user in elements(p.team) "
					+ "or :user in elements(p.admins) "
					+ "order by p.projectId"
	),
	@NamedQuery(name = ProjectEntity.IDS_BY_NAME,
			query = "select "
					+ "p.pubId "
					+ "from Project p "
					+ "where :name = p.name and " 
					+ "(:user in elements(p.team) "
					+ "or :user in elements(p.admins)) "
	) })
@Entity(name = "Project")
@Table(name = ProjectEntity.TABLE)
public class ProjectEntity implements IHasPubId, IEntity {
	public static final String TABLE = "project";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String BY_USER = "ProjectEntity-getProjectsForUser";

	public static final String TO_EEG_PROJECT = "ProjectEntity-toEegProject";

	public static final String IDS_BY_NAME = "ProjectEntity-findProjectIdsByName";

	private Long projectId;

	private String pubId = newUuid();

	private String name;

	private Set<UserEntity> team = Sets.newHashSet();

	private Set<UserEntity> admins = Sets.newHashSet();

	private Set<DataSnapshotEntity> snapshots = Sets.newHashSet();

	// private Set<ProjectDiscussion> discussions = Sets.newHashSet();

	private Set<ToolEntity> tools = Sets.newHashSet();

	private Set<String> tags = Sets.newHashSet();

	private Set<String> credits = Sets.newHashSet();

	private boolean complete = false;

	public ProjectEntity() {}

	public ProjectEntity(@Nullable String pubId, String name, boolean complete) {
		super();
		if (pubId != null)
			this.pubId = pubId;
		else
			this.pubId = BtUtil.newUuid();
		this.name = name;
		this.complete = complete;
	}

	public ProjectEntity(Long projectId, String pubId, String name,
			boolean complete) {
		super();
		this.projectId = projectId;
		if (pubId != null)
			this.pubId = pubId;
		else
			this.pubId = BtUtil.newUuid();
		this.name = name;
		this.complete = complete;
	}

	@Override
	@NaturalId
	@NotNull
	public String getPubId() {
		return pubId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = ID_COLUMN)
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany
	@JoinTable(name = "project_team")
	public Set<UserEntity> getTeam() {
		return team;
	}

	public void setTeam(Set<UserEntity> team) {
		this.team = team;
	}

	@ManyToMany
	@JoinTable(name = "project_admins")
	public Set<UserEntity> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<UserEntity> team) {
		this.admins = team;
	}

	@ManyToMany
	@JoinTable(name = "project_snapshot")
	public Set<DataSnapshotEntity> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<DataSnapshotEntity> snapshots) {
		this.snapshots = snapshots;
	}

	@ManyToMany
	@JoinTable(name = "project_tool")
	public Set<ToolEntity> getTools() {
		return tools;
	}

	public void setTools(Set<ToolEntity> tools) {
		this.tools = tools;
	}

	@ElementCollection
	@CollectionTable(name = "project_tags")
	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@ElementCollection
	@CollectionTable(name = "project_credits")
	public Set<String> getCredits() {
		return credits;
	}

	public void setCredits(Set<String> credits) {
		this.credits = credits;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	@Column(columnDefinition = "BIT", nullable = false)
	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	
	public static final EntityConstructor<ProjectEntity, String, String> CONSTRUCTOR =		
		new EntityConstructor<ProjectEntity,String,String>(ProjectEntity.class,
				new IPersistentObjectManager.IPersistentKey<ProjectEntity,String>() {

			@Override
			public String getKey(ProjectEntity o) {
				return o.getPubId();
			}

			@Override
			public void setKey(ProjectEntity o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<ProjectEntity,String,String>() {

			@Override
			public ProjectEntity create(String label, String orgCode) {
				// TODO: why is there no acl??
				return new ProjectEntity(null, label, false);
			}
		}
);
	

	@Override
	public EntityConstructor<ProjectEntity, String, String> tableConstructor() {		
		return CONSTRUCTOR;
	}

}
