/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = ImagedContact.TABLE)
public class ImagedContact {
	/**
	 * Embedded composite identifier class that represents the primary key
	 * columns of the many-to-many join table.
	 */
	@Embeddable
	public static class Id implements Serializable {

		private static final long serialVersionUID = 1L;

		private Long imageId;

		private Long contactId;

		public Id() {}

		public Id(final Long imageId, final Long contactId) {
			this.imageId = imageId;
			this.contactId = contactId;
		}

		@Override
		public boolean equals(final Object o) {
			if (o instanceof Id) {
				final Id that = (Id) o;
				return this.imageId.equals(that.getImageId()) &&
						this.contactId.equals(that.getContactId());
			} else {
				return false;
			}
		}

		/**
		 * @return the electrodeId
		 */
		@Column(name = Contact.ID_COLUMN)
		public Long getContactId() {
			return contactId;
		}

		/**
		 * @return the imageId
		 */
		@Column(name = ImageEntity.ID_COLUMN)
		public Long getImageId() {
			return imageId;
		}

		@Override
		public int hashCode() {
			return imageId.hashCode() + contactId.hashCode();
		}

		/**
		 * @param electrodeId the electrodeId to set
		 */
		@SuppressWarnings("unused")
		private void setContactId(final Long electrodeId) {
			this.contactId = electrodeId;
		}

		/**
		 * @param imageId the imageId to set
		 */
		@SuppressWarnings("unused")
		private void setImageId(final Long imageId) {
			this.imageId = imageId;
		}
	}

	public static final String TABLE = ImageEntity.TABLE + "_"
			+ Contact.TABLE;
	public static final String UPPER_CASE_TABLE = ImageEntity.UPPER_CASE_TABLE
			+ "_"
			+ Contact.UPPER_CASE_TABLE;

	private Id id;

	private Contact contact;

	private ImageEntity imageEntity;

	private PixelCoordinates pxCoordinates;

	/**
	 * No-arg constructor for JavaBean tools
	 */
	public ImagedContact() {}

	/**
	 * Full constructor, the Image and Contact instances have to have an
	 * identifier value, they have to be in detached or persistent state. This
	 * constructor takes care of the bidirectional relationship by adding the
	 * new instance to the collections on either side of the many-to-many
	 * association (added to the collections).
	 */
	public ImagedContact(
			final PixelCoordinates pxCoordinates,
			final ImageEntity imageEntity,
			final Contact contact) {
		this.pxCoordinates = pxCoordinates;

		this.imageEntity = imageEntity;
		this.contact = contact;

		// Set primary key
		this.id = new Id(imageEntity.getId(), contact.getId());

		// Guarantee referential integrity
		imageEntity.getImagedContacts().add(this);
		contact.getImagedContacts().add(this);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !(o instanceof ImagedContact)) {
			return false;
		}
		final ImagedContact that = (ImagedContact) o;
		return id.equals(that.id);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = Contact.ID_COLUMN, insertable = false, updatable = false)
	@ForeignKey(name = "FK_" + UPPER_CASE_TABLE + "_"
			+ Contact.UPPER_CASE_TABLE)
	public Contact getContact() {
		return contact;
	}

	@EmbeddedId
	public Id getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = ImageEntity.ID_COLUMN, insertable = false,
			updatable = false)
	@ForeignKey(name = "FK_" + UPPER_CASE_TABLE + "_"
			+ ImageEntity.UPPER_CASE_TABLE)
	public ImageEntity getImage() {
		return imageEntity;
	}

	/**
	 * @return the pxCoordinates
	 */
	public PixelCoordinates getPxCoordinates() {
		return pxCoordinates;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@SuppressWarnings("unused")
	private void setContact(final Contact trace) {
		this.contact = trace;
	}

	@SuppressWarnings("unused")
	private void setId(final Id id) {
		this.id = id;
	}

	@SuppressWarnings("unused")
	private void setImage(final ImageEntity imageEntity) {
		this.imageEntity = imageEntity;
	}

	@SuppressWarnings("unused")
	private void setPxCoordinates(final PixelCoordinates pxCoordinates) {
		this.pxCoordinates = pxCoordinates;
	}

}
