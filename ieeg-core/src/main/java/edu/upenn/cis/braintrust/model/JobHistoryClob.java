/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = JobHistoryClob.TABLE)
public class JobHistoryClob {

	public static final String TABLE = "job_history_clob";

	private Long id;
	private JobHistory parent;
	private String value;

	/**
	 * For JPA.
	 */
	JobHistoryClob() {}

	public JobHistoryClob(final String value, final JobHistory parent) {
		this.value = value;
		this.parent = parent;
	}

	@Id
	public Long getId() {
		return id;
	}

	@MapsId
	// Needs to be many-to-one because it's one-to-many on the other side
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_job_history_clob_parent")
	@JoinColumn(name = "parent_id")
	@NotNull
	public JobHistory getParent() {
		return parent;
	}

	@Lob
	@Column
	@NotNull
	public String getValue() {
		return value;
	}

	@SuppressWarnings("unused")
	private void setId(final Long id) {
		this.id = id;
	}

	public void setParent(final JobHistory parent) {
		this.parent = parent;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}