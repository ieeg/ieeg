/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.aws;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.webapp.ContentLengthInputStream;

/**
 * 
 * {@code AwsUtil} looks at two pairs of system properties. The credential pair:
 * {@code AWS_ACCESS_KEY_ID} and {@code AWS_SECRET_KEY} and the config pair:
 * {@code PARAM1} and {@code PARAM2}. These property names are determined by the
 * AWS Elastic Beanstalk service.
 * <p>
 * The credential pair is used to create credentials for an {@link AmazonS3}
 * instance. They are optional, but if one of the pair is specified then the
 * other must be as well. If they are not specified the {@link AmazonS3} will
 * still be created with default, dummy credentials, so any calls to the AWS web
 * services will throw exceptions. However, some methods of {@link AmazonS3}
 * which do not call the server may still succeed such as
 * {@link AmazonS3#generatePresignedUrl(String, String, Date)} (Of course, the
 * returned URL will not actually work).
 * <p>
 * The config pair is optional, but if one of the pair is specified then the
 * other must be as well. If they are not specified, then
 * {@link #isConfigFromS3()} returns false and the application's configuration
 * is read from the local file system. If they are specified, then
 * {@link #isConfigFromS3()} returns true. In this case the application's config
 * is read from S3. The value of {@code PARAM1} is the bucket and the value of
 * {@code PARAM2} is the directory inside the bucket where the config files can
 * be found.
 * <p>
 * If the config pair is specified, then the credential pair must also be
 * specified.
 * 
 * @author Sam Donnelly
 * @author John Frommeyer
 * 
 */
public final class AwsUtil {

	private static final AWSCredentials creds;

	private static final boolean isConfigFromS3;

	private static final Optional<String> configS3Bucket;

	private static Optional<String> configS3Dir;

	private static final AmazonS3 s3;

	private static final Logger logger = LoggerFactory.getLogger(AwsUtil.class);

	static {
		try {
			String key = System.getenv("AWS_ACCESS_KEY_ID");
			String val = System.getenv("AWS_SECRET_KEY");
			// We always need an S3 client, if only for the presigned Urls.
			final Optional<String> accessKeyId = Optional
					.fromNullable(emptyToNull(key == null ? System.getProperty(
							"AWS_ACCESS_KEY_ID") : key));
			final Optional<String> secretKey = Optional
					.fromNullable(emptyToNull(val == null ? System.getProperty(
							"AWS_SECRET_KEY") : val));
			boolean realCreds = false;
			if (accessKeyId.isPresent() && secretKey.isPresent()) {
				realCreds = true;
				creds = new BasicAWSCredentials(
						accessKeyId.get(),
						secretKey.get());
			} else if (!accessKeyId.isPresent() && !secretKey.isPresent()) {
				creds = new BasicAWSCredentials(
						"FakeAwsAccessKeyId",
						"FakeAwsSecretKey");
			} else {
				if (accessKeyId.isPresent()) {
					throw new IllegalStateException(
							"specifed AWS_ACCESS_KEY_ID [" + accessKeyId.get()
									+ "] but no AWS_SECRET_KEY given");
				}
				throw new IllegalStateException(
						"specifed AWS_SECRET_KEY but no AWS_ACCESS_KEY_ID given");
			}
			s3 = new AmazonS3Client(creds);
			s3.setRegion(Region.getRegion(Regions.US_EAST_1));

			configS3Bucket = Optional
					.fromNullable(emptyToNull(System.getProperty("PARAM1")));
			configS3Dir =
					Optional.fromNullable
							(emptyToNull(System.getProperty("PARAM2")));
			while (configS3Dir.isPresent() && configS3Dir.get().endsWith("/")) {
				configS3Dir = Optional.of(configS3Dir.get().substring(
						0,
						configS3Dir.get().length() - 1));
			}
			if (realCreds && configS3Bucket.isPresent()
					&& configS3Dir.isPresent()) {
				isConfigFromS3 = true;
			} else if (!realCreds && configS3Bucket.isPresent()
					&& configS3Dir.isPresent()) {
				isConfigFromS3 = false;
				throw new IllegalStateException(
						"specifed S3 config bucket and dir but AWS_ACCESS_KEY_ID and AWS_SECRET_KEY not given");
			} else {
				// At least one of the config bucket and dir is absent, so we no
				// longer care about realCreds.
				isConfigFromS3 = false;
				if (configS3Bucket.isPresent()) {
					throw new IllegalStateException(
							"specified S3 config bucket  ["
									+ configS3Bucket.get() +
									"] but no config S3 dir given");
				}
				if (configS3Dir.isPresent()) {
					throw new IllegalStateException(
							"specified config S3 dir [" + configS3Dir.get() +
									"] but no S3 config bucket given");
				}
			}
		} catch (Throwable t) {
			// We have to catch Throwable, otherwise we will miss
			// NoClassDefFoundError and other subclasses of Error
			// Generally speaking, we only log exceptions at the point of
			// handling, but
			// we sometimes lose this exception otherwise for unknown reasons

			// don't use a logger because this needs to happen before
			// the logging is configured
			System.out.println(
					AwsUtil.class.getName()
							+ ": Building aws failed: "
							+ Throwables.getStackTraceAsString(t));
			throw new ExceptionInInitializerError(t);
		}
	}

	/**
	 * Returns a {@code GetObjectRequest} with the given parameters.
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @return a {@code GetObjectRequest} with the given parameters
	 */
	public static GetObjectRequest createGetObjectRequest(
			String bucketName,
			String objectKey) {
		GetObjectRequest getObjectRequest =
				new GetObjectRequest(bucketName, objectKey);
		return getObjectRequest;
	}

	/**
	 * Returns a {@code GetObjectRequest} with the given parameters.
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @param start
	 * @param end
	 * @return a {@code GetObjectRequest} with the given parameters
	 */
	public static GetObjectRequest createGetObjectRequest(
			String bucketName,
			String objectKey,
			long start,
			long end,
			String eTag) {
		GetObjectRequest getObjectRequest =
				new GetObjectRequest(bucketName, objectKey)
						.withMatchingETagConstraint(eTag)
						.withRange(start, end);
		return getObjectRequest;
	}

	/**
	 * Returns a {@code PutObjectRequest} with the given parameters.
	 * 
	 * @param bucketName
	 * @param key
	 * @param input
	 * @param inputLength
	 * @return a {@code PutObjectRequest} with the given parameters
	 */
	public static PutObjectRequest createPutObjectRequest(
			String bucketName,
			String key,
			InputStream input,
			long inputLength) {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(inputLength);
		PutObjectRequest request = new PutObjectRequest(
				bucketName,
				key,
				input,
				metadata);
		return request;
	}

	public static void deleteObject(
			AmazonS3 s3,
			String bucket,
			String fileKey) {
		s3.deleteObject(
				bucket,
				fileKey);
	}

	/**
	 * Returns the S3 bucket where the application's configuration is located.
	 * This is the value of the System property {@code PARAM1}.
	 * 
	 * @return the S3 bucket where the application's configuration is located
	 * @throws IllegalStateException if {@code isConfigFromS3()} returns false
	 */
	public static String getConfigS3Bucket() {
		checkState(isConfigFromS3(), "we're not reading config from s3");
		return configS3Bucket.get();
	}

	/**
	 * Returns the S3 directory where the application's configuration is located
	 * inside the config S3 bucket. Will not have a trailing slash. This is the
	 * value of the System property {@code PARAM2}.
	 * 
	 * 
	 * @return the S3 directory where the application's configuration is located
	 * @throws IllegalStateException if {@code isConfigFromS3()} returns false
	 * @see {@link #getConfigS3Bucket()}
	 */
	public static String getConfigS3Dir() {
		checkState(isConfigFromS3(), "we're not reading config from s3");
		return configS3Dir.get();
	}

	/**
	 * Returns a {@code AWSCrendtials} instance created from the System
	 * Properties {@code AWS_ACCESS_KEY_ID} and {@code AWS_SECRET_KEY}.
	 * 
	 * @return a {@code AWSCrendtials} instance created from the System
	 *         Properties {@code AWS_ACCESS_KEY_ID} and {@code AWS_SECRET_KEY}
	 */
	public static AWSCredentials getCreds() {
		return creds;
	}

	/**
	 * Returns an {@code AmazonS3} configured using the System Properties
	 * {@code AWS_ACCESS_KEY_ID} and {@code AWS_SECRET_KEY} as the credentials.
	 * 
	 * @return an {@code AmazonS3}
	 */
	public static AmazonS3 getS3() {
		return s3;
	}

	/**
	 * <pre>
	 * From http://stackoverflow.com/questions/9429127/aws-s3-file-search-using-java
	 * </pre>
	 * 
	 * @throws AmazonS3Exception if an error other than 404 is returned from
	 *             Amazon
	 */
	public static boolean isValidFile(
			AmazonS3 s3,
			String bucketName,
			String fileKey) {
		boolean isValidFile = true;
		try {
			s3.getObjectMetadata(bucketName, fileKey);
		} catch (AmazonS3Exception s3e) {
			if (s3e.getStatusCode() == 404) {
				// i.e. 404: NoSuchKey - The specified key does not exist
				isValidFile = false;
			}
			else {
				throw s3e; // rethrow all S3 exceptions other than 404
			}
		}

		return isValidFile;
	}

	/**
	 * There's really not a lot to this method, it's only here for a common
	 * logging point, which we're not even doing yet.
	 * 
	 * @param s3
	 * @param getObjectRequest
	 * @param requestType
	 * @return
	 */
	@Nullable
	public static S3Object requestS3Object(
			AmazonS3 s3,
			GetObjectRequest getObjectRequest,
			@Nullable String requestType) {
		S3Object s3Object = s3.getObject(getObjectRequest);
		return s3Object;
	}

	/**
	 * 
	 * A {@code byte} array with the contents of an S3 object as specified in
	 * the given {@code GetObjectRequest}.
	 * 
	 * @param s3
	 * @param getObjectRequest
	 * @param requestType a string which will help identify the request type in
	 *            the logs.
	 * @return {@code byte} array with the contents of an S3 object as specified
	 *         in the given {@code GetObjectRequest}
	 * @throws IOException in an error occurs while reading the
	 *             {@code S3ObjectInputStream}
	 */
	public static byte[] request(
			AmazonS3 s3,
			GetObjectRequest getObjectRequest,
			@Nullable String requestType)
			throws IOException {
		final String M = "call()";
		long inNanos = -1;
		InputStream s3ObjectInputStream = null;
		try {
			if (logger.isTraceEnabled()) {
				inNanos = System.nanoTime();
				// Ugly, but trying to avoid extra space if requestType is null
				// or empty.
				if (isNullOrEmpty(requestType)) {
					logger.trace(
							"{}: sending request bucket: [{}] key: [{}] start: {} end: {}",
							new Object[] {
									M,
									getObjectRequest.getBucketName(),
									getObjectRequest.getKey(),
									getObjectRequest.getRange() == null ? "-"
											: getObjectRequest.getRange()[0],
									getObjectRequest.getRange() == null ? "-"
											: getObjectRequest.getRange()[1]
							});
				} else {
					logger.trace(
							"{}: sending {} request bucket: [{}] key: [{}] start: {} end: {}",
							new Object[] {
									M,
									requestType,
									getObjectRequest.getBucketName(),
									getObjectRequest.getKey(),
									getObjectRequest.getRange() == null ? "-"
											: getObjectRequest.getRange()[0],
									getObjectRequest.getRange() == null ? "-"
											: getObjectRequest.getRange()[1]
							});
				}

			}
			S3Object s3Object = s3.getObject(getObjectRequest);

			s3ObjectInputStream = s3Object.getObjectContent();

			checkState(
					s3Object.getObjectMetadata().getContentLength() <= Integer.MAX_VALUE,
					"too much content %s", s3Object.getObjectMetadata()
							.getContentLength());

			byte[] buffer =
					new byte[
					Ints.checkedCast(s3Object.getObjectMetadata()
							.getContentLength())];
			ByteStreams.readFully(s3ObjectInputStream, buffer);
			return buffer;
		} finally {
			BtUtil.close(s3ObjectInputStream);
			if (logger.isTraceEnabled()) {
				if (isNullOrEmpty(requestType)) {
					logger.trace(
							"{}: done with request {} {} {} seconds",
							new Object[] {
									M,
									getObjectRequest.getBucketName(),
									getObjectRequest.getKey(),
									BtUtil.diffNowThenSeconds(inNanos)
							});
				} else {
					logger.trace(
							"{}: done with {} request {} {} {} seconds",
							new Object[] {
									M,
									requestType,
									getObjectRequest.getBucketName(),
									getObjectRequest.getKey(),
									BtUtil.diffNowThenSeconds(inNanos)
							});
				}
			}
		}
	}

	/**
	 * Returns {@code true} if the application's configuration is to be read out
	 * of S3 instead of the local file system. This means that both System
	 * properties {@code PARAM1} (the S3 bucket) and {@code PARAM2} (the path
	 * within the bucket) have been specified.
	 * 
	 * @return {@code true} if the application's configuration is to be read out
	 *         of S3
	 */
	public static boolean isConfigFromS3() {
		return isConfigFromS3;
	}

	/**
	 * Executes the given PutObjectRequest.
	 * 
	 * @param s3
	 * @param request
	 * @return 
	 */
	public static PutObjectResult write(
			AmazonS3 s3,
			PutObjectRequest request) {
		final String M = "write(...)";
		// one liner looks goofy - we may add loggging here
		logger.debug("{}: request: {}", M, request);
		return s3.putObject(request);
	}

	/**
	 * Returns a presigned URL for the object in the given bucket with the given
	 * file key which will expire after the given number of milliseconds.
	 * 
	 * @param s3
	 * @param bucketName
	 * @param fileKey
	 * @param expirationPeriodMs
	 * @return a presigned URL for the object in the given bucket with the given
	 *         file key which will expire after the given number of milliseconds
	 */
	public static String generatePresignedUrl(
			AmazonS3 s3,
			String bucketName,
			String fileKey,
			long expirationPeriodMs) {
		final String M = "generatePresignedUrl(...)";
		final long now = System.currentTimeMillis();
		final Date expiration = new Date(now + expirationPeriodMs);
		logger.debug("{}: generating presigned url for {}/{} expiring {}",
				new Object[] {
						M,
						bucketName,
						fileKey,
						expiration });
		return s3.generatePresignedUrl(bucketName, fileKey, expiration)
				.toString();
	}

	public static String getETag(
			AmazonS3 s3,
			String bucketName,
			String fileKey) {
		ObjectMetadata objectMetadata =
				getObjectMetadata(
						s3,
						bucketName,
						fileKey);
		return objectMetadata.getETag();
	}

	public static ObjectMetadata getObjectMetadata(
			AmazonS3 s3,
			String bucketName,
			String fileKey) {
		ObjectMetadata objectMetadata =
				s3.getObjectMetadata(
						bucketName,
						fileKey);
		return objectMetadata;
	}

	public static InputStream getObjectContent(
			AmazonS3 s3,
			String bucket,
			String key,
			@Nullable String eTag,
			@Nullable Long startByteOffset,
			@Nullable Long endByteOffset) {
		final String m = "getObjectContent(String, String, Long, Long)";

		if (startByteOffset != null) {
			if (endByteOffset == null) {
				ObjectMetadata objectMetadata =
						s3.getObjectMetadata(
								bucket,
								key);
				endByteOffset = objectMetadata.getContentLength() - 1;
			}
		} else {
			if (endByteOffset != null) {
				throw new IllegalArgumentException(
						"startByteOffset cannot be null if endByteOffset is not null");
			}
		}

		GetObjectRequest getObjectRequest = new GetObjectRequest(
				bucket,
				key);
		if (startByteOffset != null) {
			getObjectRequest
					.setRange(
							startByteOffset,
							endByteOffset);
		}
		if (eTag != null) {
			List<String> eTags = Collections.singletonList(eTag);
			getObjectRequest
					.setMatchingETagConstraints(eTags);
		}
		S3Object s3Object = s3.getObject(getObjectRequest);
		long contentLength = s3Object.getObjectMetadata().getContentLength();
		InputStream in = new ContentLengthInputStream(
				s3Object.getObjectContent(),
				contentLength);

		return in;
	}

	public static Set<String> listMefFileKeys(
			AmazonS3 s3,
			String bucketName,
			String mefDir) {
		ObjectListing listing = s3.listObjects(bucketName, mefDir);
		final List<S3ObjectSummary> summaries = newArrayList(listing
				.getObjectSummaries());
		while (listing.isTruncated()) {
			listing = s3.listNextBatchOfObjects(listing);
			summaries.addAll(listing.getObjectSummaries());
		}
		final Set<String> mefKeys = newHashSet();
		for (final S3ObjectSummary summary : summaries) {
			final String mefKey = summary.getKey();
			if (mefKey.toLowerCase().endsWith(".mef")) {
				mefKeys.add(mefKey);
			}
		}
		return mefKeys;
	}
}
