/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class JsonKeyValueMapper {
	static JsonKeyValueMapper theMapper = null;
	
	/**
	 * Singleton mapper
	 * 
	 * @return
	 */
	public static synchronized JsonKeyValueMapper getMapper() {
		if (theMapper == null)
			theMapper = new JsonKeyValueMapper();
		
		return theMapper;
	}

	ObjectReader reader;
	ObjectWriter writer;
	ObjectMapper mapper;
	
	public JsonKeyValueMapper() {
		mapper = ObjectMapperFactory.getObjectMapper();
		
		reader = mapper.reader(JsonKeyValueSet.class);
		writer = mapper.writer();
	}
	
	public Set<IJsonKeyValue> getKeyValueSet(String value) throws JsonProcessingException, IOException {
		try {
			return reader.readValue(value);
		} catch (JsonParseException jpe) {
			jpe.printStackTrace();
			return new HashSet<IJsonKeyValue>();
		} catch (JsonProcessingException e) {
			System.err.println("Unable to deserialize " + value);
			throw e;
		}
	}
	
	public JsonKeyValueSet getJsonKeyValueSet(String value) throws JsonProcessingException, IOException {
		try {
			return reader.readValue(value);
		} catch (JsonParseException jpe) {
			jpe.printStackTrace();
			return new JsonKeyValueSet();
		} catch (JsonProcessingException e) {
			System.err.println("Unable to deserialize " + value);
			throw e;
		}
	}
	
	public String createKeyValueSet(Collection<? extends IJsonKeyValue> triples) throws JsonProcessingException, IOException {
	  return writer.writeValueAsString(triples);
	}
	
	public String createKeyValue(IJsonKeyValue triple) throws JsonProcessingException, IOException {
		  return writer.writeValueAsString(triple);
	}

	public String createValue(JsonTyped value) throws JsonProcessingException, IOException {
		  return writer.writeValueAsString(value);
	}
	
	public JsonTyped getValueFromString(String str) {
		try {
			return reader.readValue(str);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public String addKeyValueSet(String value, Collection<? extends IJsonKeyValue> triples) throws JsonProcessingException, IOException {
		Set<IJsonKeyValue> start = getKeyValueSet(value);
		start.addAll(triples);
		
		return writer.writeValueAsString(start);
	}

	public String addKeyValueToSet(String value, IJsonKeyValue triple) throws JsonProcessingException, IOException {
		Set<IJsonKeyValue> start = getKeyValueSet(value);
		start.add(triple);
		
		return writer.writeValueAsString(start);
	}
	
	public String getKeyValueSet(Collection<? extends IJsonKeyValue> tset) throws JsonProcessingException {
		return writer.writeValueAsString(tset);
	}
	
}
