/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;

/**
 * Attributes common to {@code GwtElectrode} and {@code GwtReferenceElectrode}.
 * 
 * @author John Frommeyer
 */
public interface IElectrode {

	public Optional<Double> getImpedance();

	/**
	 * @return the lobe
	 */
	public Location getLocation();

	/**
	 * @return the side
	 */
	public Side getSide();

	public void setImpedance(final Double impedance);

	/**
	 * @param location the lobe to set
	 */
	public void setLocation(final Location location);

	/**
	 * @param side the side to set
	 */
	public void setSide(final Side side);

}
