/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.shared.dto;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Comparator;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.IHasLongId;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Side;

public class GwtContactGroup implements IElectrode, Serializable, IHasLongId,
		IHasVersion {

	public static final Function<GwtContactGroup, String> getChannelPrefix = new Function<GwtContactGroup, String>() {
		@Override
		public String apply(GwtContactGroup input) {
			return input.getChannelPrefix();
		}
	};
	/**
	 * Uses String's Comparator on channel prefixes. Warning: This Comparator is
	 * not consistent with equals.
	 */
	public static final Comparator<GwtContactGroup> CHANNEL_PREFIX_COMPARATOR = new Comparator<GwtContactGroup>() {

		@Override
		public int compare(GwtContactGroup o1, GwtContactGroup o2) {
			return o1.getChannelPrefix().compareTo(o2.getChannelPrefix());
		}
	};
	private static final long serialVersionUID = 1L;
	
	private Long id;

	private Integer version;

	private GwtRecording parent;
	private Double samplingRate;
	private Optional<Double> lffSetting;
	private Optional<Double> hffSetting;
	private Optional<Boolean> notchFilter;
	private String electrodeType;
	private Optional<String> manufacturer;
	private Optional<String> modelNo;
	private String channelPrefix;
	private Side side;
	private Location location;
	private Optional<Double> impedance;
	private Boolean isDeleted = false;

	public GwtContactGroup() {}

	public GwtContactGroup(
			String channelPrefix,
			String electrodeType,
			Side side,
			Location location,
			Double samplingRate,
			Boolean isDeleted,
			@Nullable Double lffSetting,
			@Nullable Double hffSetting,
			@Nullable Boolean notchFilter,
			@Nullable Double impedance,
			@Nullable String manufacturer,
			@Nullable String modelNo,
			@Nullable GwtRecording parent,
			@Nullable Long id,
			@Nullable Integer version) {
		this.id = id;
		this.version = version;
		this.parent = parent;
		this.samplingRate = checkNotNull(samplingRate);
		this.lffSetting = Optional.fromNullable(lffSetting);
		this.hffSetting = Optional.fromNullable(hffSetting);
		this.notchFilter = Optional.fromNullable(notchFilter);
		this.electrodeType = checkNotNull(electrodeType);
		this.channelPrefix = checkNotNull(channelPrefix);
		this.side = checkNotNull(side);
		this.location = checkNotNull(location);
		this.impedance = Optional.fromNullable(impedance);
		this.manufacturer = Optional.fromNullable(manufacturer);
		this.modelNo = Optional.fromNullable(modelNo);
		this.isDeleted = checkNotNull(isDeleted);
	}

	/**
	 * @return the channelPrefix
	 */
	public String getChannelPrefix() {
		return channelPrefix;
	}

	public String getElectrodeType() {
		return electrodeType;
	}
	
	public Optional<Double> getHffSetting() {
		return hffSetting;
	}

	@Nullable
	public Long getId() {
		return id;
	}

	public Optional<Double> getLffSetting() {
		return lffSetting;
	}

	public Optional<String> getManufacturer() {
		return manufacturer;
	}
	
	public Optional<String> getModelNo() {
		return modelNo;
	}
	
	public Optional<Boolean> getNotchFilter() {
		return notchFilter;
	}

	/**
	 * @return the parent
	 */
	@Nullable
	public GwtRecording getParent() {
		return parent;
	}

	public Double getSamplingRate() {
		return samplingRate;
	}

	@Nullable
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param channelPrefix the channelPrefix to set
	 */
	public void setChannelPrefix(final String channelPrefix) {
		this.channelPrefix = checkNotNull(channelPrefix);
	}

	public void setHffSetting(@Nullable final Double hffSetting) {
		this.hffSetting = Optional.fromNullable(hffSetting);
	}

	public void setId(@Nullable final Long id) {
		this.id = id;
	}

	public void setLffSetting(final Double lffSetting) {
		this.lffSetting = Optional.fromNullable(lffSetting);
	}

	public void setNotchFilter(final Boolean notchFilter) {
		this.notchFilter = Optional.fromNullable(notchFilter);
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(@Nullable final GwtRecording parent) {
		this.parent = parent;
	}

	public void setSamplingRate(final Double samplingRate) {
		this.samplingRate = checkNotNull(samplingRate);
	}

	public void setVersion(@Nullable final Integer version) {
		this.version = version;
	}

	public Optional<Double> getImpedance() {
		return impedance;
	}

	public Location getLocation() {
		return location;
	}

	public Side getSide() {
		return side;
	}

	public void setImpedance(@Nullable final Double impedance) {
		this.impedance = Optional.fromNullable(impedance);
	}

	public void setLocation(final Location location) {
		this.location = checkNotNull(location);
	}

	public void setSide(final Side side) {
		this.side = checkNotNull(side);
	}

	public void setDeleted(final Boolean isDeleted) {
		this.isDeleted = checkNotNull(isDeleted);
	}

	public Boolean isDeleted() {
		return this.isDeleted;
	}
	
	public void setManufacturer(@Nullable String manufacturer) {
		this.manufacturer = Optional.fromNullable(manufacturer);
	}
	
	public void setModelNo(@Nullable String modelNo) {
		this.modelNo = Optional.fromNullable(modelNo);
	}
	
	public void setElectrodeType(String electrodeType) {
		this.electrodeType = checkNotNull(electrodeType);
	}
}
