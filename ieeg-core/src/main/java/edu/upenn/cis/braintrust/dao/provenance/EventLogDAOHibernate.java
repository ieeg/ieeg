/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.dao.provenance;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.upenn.cis.braintrust.model.EventLogEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.GenericHibernateDAO;

public class EventLogDAOHibernate extends
		GenericHibernateDAO<EventLogEntity, Long>
		implements IEventLogDAO {

	public EventLogDAOHibernate(
			Session session) {
		setSession(session);
	}

	@Override
	public List<EventLogEntity> find(
			UserEntity user,
			int start,
			int count) {
		Query q = getSession()
				.createQuery(
						"select l from EventLogEntity l "
								+ "where l.notifiedUser = :user "
								+ "order by l.time desc")
				.setParameter("user", user)
				.setFirstResult(start)
				.setMaxResults(count);

		@SuppressWarnings("unchecked")
		List<EventLogEntity> l = q.list();
		return l;
	}

	@Override
	public List<EventLogEntity> findAllEntriesFrom(UserEntity user, int start,
			int count) {

		Query q = getSession()
				.createQuery(
						"select l from EventLogEntity l "
								+ "where l.sourceUser = :user "
								+ "order by l.time desc")
				.setParameter("user", user)
				.setFirstResult(start)
				.setMaxResults(count);

		@SuppressWarnings("unchecked")
		List<EventLogEntity> l = q.list();
		return l;
	}

	@Override
	public List<EventLogEntity> findAllEntriesFromOthers(UserEntity user,
			int start, int count) {

		Query q = getSession()
				.createQuery(
						"select l from EventLogEntity l "
								+ "where l.notifiedUser = :user and l.notifiedUser <> l.sourceUser "
								+ "order by l.time desc")
				.setParameter("user", user)
				.setFirstResult(start)
				.setMaxResults(count);

		@SuppressWarnings("unchecked")
		List<EventLogEntity> l = q.list();
		return l;
	}

	@Override
	public List<EventLogEntity> findSince(UserEntity user,
			Timestamp time) {
		@SuppressWarnings("unchecked")
		List<EventLogEntity> l = (List<EventLogEntity>) getSession()
				.createQuery(
						"select l from EventLogEntity l "
								+ "where l.notifiedUser = :user "
								+ "and l.time > :time "
								+ "order by l.time desc")
				.setParameter("user", user)
				.setParameter("time", time)
				.list();
		return l;
	}
}
