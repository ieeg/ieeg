/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.testhelper;

import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static java.util.Collections.singleton;
import static java.util.UUID.randomUUID;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.HospitalAdmission;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.MriCoordinates;
import edu.upenn.cis.braintrust.model.Patient;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TalairachCoordinates;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.Pathology;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;

public class BuildDb {
	public static void buildDb(
			final List<Patient> patients,
			PermissionEntity readPerm) {
		patients.add(buildPatient0(readPerm));
		patients.add(buildPatient1(readPerm));
		patients.add(buildPatient2());
	}

	public static ContactGroup buildElectrode0000() {
		final ContactGroup electrode0000 = new ContactGroup(
				"RT",
				"Depth",
				499.9070,
				9.890,
				123.23,
				false,
				Side.RIGHT,
				Location.TEMPORAL,
				89.7,
				"Ad-Tech",
				newUuid());

		final TimeSeriesEntity trace0 = new TimeSeriesEntity();
		trace0.setPubId("0000-0------------------------------");
		trace0.setFileKey("jkjk/jkgej0000/jkj1");
		trace0.setLabel("jkj1");

		final Contact contact0 = new Contact(
				ContactType.MACRO,
				new MriCoordinates(
						34.9,
						-987.7,
						-3452.56),
				new TalairachCoordinates(
						4.5,
						-98.3,
						8384.093),
				trace0);
		electrode0000.addContact(contact0);

		final TimeSeriesEntity trace1 = new TimeSeriesEntity();
		trace1.setPubId(("0000-1------------------------------"));
		trace1.setFileKey("jjk/jgej0000/jj2");
		trace1.setLabel("jj2");
		final Contact contact1 = new Contact(
				ContactType.MICRO,
				new MriCoordinates(3.3, 1.3, 2.3),
				new TalairachCoordinates(3.3, 1.3, 2.3),
				trace1);
		electrode0000.addContact(contact1);

		final TimeSeriesEntity trace2 = new TimeSeriesEntity();
		trace2.setPubId(("0000-2------------------------------"));

		trace2.setFileKey("jk/jgej0000/jj3");
		trace2.setLabel("jj3");
		final Contact contact2 = new Contact(
				ContactType.MACRO,
				new MriCoordinates(3.3, 1.3, 2.3),
				new TalairachCoordinates(4.5, -98.4,
						384.093),
				trace2);
		electrode0000.addContact(contact2);

		final TimeSeriesEntity trace3 = new TimeSeriesEntity();
		trace3.setPubId(("0000-3------------------------------"));
		trace3.setFileKey("jk/jgj0000/jj4");
		trace3.setLabel("jj4");
		final Contact contact3 = new Contact(
				ContactType.MACRO,
				new MriCoordinates(3.3, 1.3, 2.3),
				new TalairachCoordinates(4.5, -98.4,
						384.093),
				trace3);
		electrode0000.addContact(contact3);

		return electrode0000;
	}

	public static ContactGroup buildElectrode0001() {
		final ContactGroup electrode0001 = new ContactGroup(
				"LP",
				"Depth",
				499.9070,
				9.890,
				123.23,
				false,
				Side.LEFT,
				Location.PARIETAL,
				89.7,
				"Ad-Tech",
				newUuid());

		{

			final TimeSeriesEntity trace0 = new TimeSeriesEntity();
			trace0.setPubId(("0001-0------------------------------"));
			trace0.setFileKey("jk/jgj0001/j1");
			trace0.setLabel("j1");

			final Contact contact0 = new Contact(
					ContactType.MACRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace0);
			electrode0001.addContact(contact0);
		}
		{

			final TimeSeriesEntity trace1 = new TimeSeriesEntity();
			trace1.setPubId("0001-1------------------------------");
			trace1.setFileKey("jk/jgj0001/j2");
			trace1.setLabel("j2");

			final Contact contact1 = new Contact(
					ContactType.MICRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace1);
			electrode0001.addContact(contact1);
		}
		{

			final TimeSeriesEntity trace2 = new TimeSeriesEntity();
			trace2.setPubId(("0001-2------------------------------"));
			trace2.setFileKey("jk/jgj0001/j3");
			trace2.setLabel("j3");
			final Contact contact2 = new Contact(
					ContactType.MACRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace2);
			electrode0001.addContact(contact2);

		}
		return electrode0001;
	}

	public static ContactGroup buildElectrode0002() {
		final ContactGroup electrode0002 = new ContactGroup(
				"RP",
				"Depth",
				499.9070,
				0.9890,
				1232.3,
				false,
				Side.RIGHT,
				Location.OCCIPITAL,
				89.7,
				"Ad-Tech",
				newUuid());

		{

			final TimeSeriesEntity trace0 = new TimeSeriesEntity();
			trace0.setPubId(("0002-0------------------------------"));
			trace0.setFileKey("jk/jg0002/j1");
			trace0.setLabel("j1");

			final Contact contact0 = new Contact(
					ContactType.MACRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace0);
			electrode0002.addContact(contact0);

		}

		{

			final TimeSeriesEntity trace1 = new TimeSeriesEntity();
			trace1.setPubId(("0002-1------------------------------"));
			trace1.setFileKey("jk/jg0002/j2");
			trace1.setLabel("j2");
			final Contact contact1 = new Contact(
					ContactType.MICRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace1);
			electrode0002.addContact(contact1);

		}
		return electrode0002;
	}

	public static ContactGroup buildElectrode0003() {
		final ContactGroup electrode0003 = new ContactGroup(
				"SP",
				"Depth",
				499.9070,
				0.9890,
				123.23,
				false,
				Side.LEFT,
				Location.PARIETAL,
				89.7,
				"Ad-Tech",
				newUuid());

		{

			final TimeSeriesEntity trace0 = new TimeSeriesEntity();
			trace0.setPubId(("0003-0------------------------------"));

			trace0.setFileKey("j/jg0003/j1");
			trace0.setLabel("j1");

			final Contact contact0 = new Contact(
					ContactType.MACRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace0);
			electrode0003.addContact(contact0);
		}

		{

			final TimeSeriesEntity trace1 = new TimeSeriesEntity();
			trace1.setPubId(("0003-1------------------------------"));

			trace1.setFileKey("j/jg0003/j2");
			trace1.setLabel("j2");

			final Contact contact1 = new Contact(
					ContactType.MICRO,
					new MriCoordinates(34.9, -987.7, 436.3),
					new TalairachCoordinates(4.5,
							-98.34, 8384.093),
					trace1);
			electrode0003.addContact(contact1);
		}

		return electrode0003;
	}

	public static Set<ContactGroup> buildElectrodes000() {
		final Set<ContactGroup> electrodes = newHashSet();
		electrodes.add(buildElectrode0000());
		electrodes.add(buildElectrode0001());
		electrodes.add(buildElectrode0002());
		electrodes.add(buildElectrode0003());
		return electrodes;
	}

	public static HospitalAdmission buildHospitalAdmission00(
			PermissionEntity readPerm) {
		final HospitalAdmission admission00 = new HospitalAdmission();
		admission00.setAgeAtAdmission(Integer.valueOf(55));
		final EegStudy study000 = buildStudy000(readPerm);
		admission00.addStudy(study000);
		return admission00;
	}

	public static ImageEntity buildImage0000() {
		final ImageEntity imageEntity = new ImageEntity();
		imageEntity.setPubId("0000");
		imageEntity.setType(ImageType.THREE_D_RENDERING);
		imageEntity.setFileKey("x/y/a");
		return imageEntity;
	}

	public static ImageEntity buildImage0001() {
		final ImageEntity imageEntity = new ImageEntity();
		imageEntity.setPubId("0001");
		imageEntity.setType(ImageType.THREE_D_RENDERING);
		imageEntity.setFileKey("x/y/b");
		return imageEntity;
	}

	public static ImageEntity buildImage0002() {
		final ImageEntity imageEntity = new ImageEntity();
		imageEntity.setPubId("0002");
		imageEntity.setType(ImageType.THREE_D_RENDERING);
		imageEntity.setFileKey("x/y/c");
		return imageEntity;
	}

	public static Set<ImageEntity> buildImages000() {
		final Set<ImageEntity> imageEntities = newHashSet();
		imageEntities.add(buildImage0000());
		imageEntities.add(buildImage0001());
		imageEntities.add(buildImage0002());
		return imageEntities;
	}

	public static Patient buildPatient0(
			PermissionEntity readPerm) {

		final Patient patient0 = new Patient();
		patient0.setEthnicity(Ethnicity.AMERICAN_INDIAN);
		patient0.setGender(Gender.FEMALE);
		patient0.setHandedness(Handedness.AMBIDEXTROUS);
		patient0.setOrganization(OrganizationsForTests.SPIKE);
		patient0.setLabel("0-A");
		patient0.setAgeOfOnset(0);
		patient0.setEtiology(Pathology.ANOXIA.toString());
		patient0.setDevelopmentalDisorders(Boolean.TRUE);
		patient0.setTraumaticBrainInjury(Boolean.FALSE);
		patient0.setFamilyHistory(Boolean.TRUE);
		patient0.getSzTypes().add(SeizureType.ABSENCE);
		patient0.getSzTypes().add(SeizureType.ATONIC);
		patient0.getPrecipitants().add(Precipitant.ALCOHOL);
		patient0.getPrecipitants().add(Precipitant.DRUGS);
		patient0.getPrecipitants().add(Precipitant.FEBRILE);

		final HospitalAdmission admission00 = buildHospitalAdmission00(
				readPerm);
		patient0.addAdmission(admission00);

		return patient0;
	}

	public static Patient buildPatient1(
			PermissionEntity readPerm) {

		final Patient patient1 = new Patient();
		patient1.setEthnicity(Ethnicity.HISPANIC);
		patient1.setGender(Gender.FEMALE);
		patient1.setHandedness(Handedness.RIGHT);
		patient1.setOrganization(OrganizationsForTests.JONES);
		patient1.setLabel("1-B");
		patient1.setAgeOfOnset(1);
		patient1.setEtiology("anoxia");
		patient1.setDevelopmentalDelay("Some dev delay.");
		patient1.setDevelopmentalDisorders(Boolean.FALSE);
		patient1.setTraumaticBrainInjury(Boolean.TRUE);
		patient1.setFamilyHistory(Boolean.FALSE);
		patient1.getSzTypes().add(SeizureType.ATONIC);
		patient1.getSzTypes().add(SeizureType.LONIC);
		patient1.getSzTypes().add(SeizureType.MYOCLONIC);
		patient1.getPrecipitants().add(Precipitant.ALCOHOL);
		patient1.getPrecipitants().add(Precipitant.DRUGS);
		patient1.getPrecipitants().add(Precipitant.FEBRILE);

		final HospitalAdmission admission0 = new HospitalAdmission();
		patient1.addAdmission(admission0);
		admission0.setAgeAtAdmission(Integer.valueOf(5));

		final EegStudy study00 =
				new EegStudy(
						new ExtAclEntity(
								singleton(readPerm)));

		admission0.addStudy(study00);
		study00.setPubId(newUuid());
		study00.setLabel("some label " + randomUUID().toString());

		study00.setType(EegStudyType.COGNITIVE);
		study00.setSzCount(15);
		// study00.setStartTimeUutc(0L);
		// study00.setEndTimeUutc(50000L);

		final Recording recording = new Recording(
				"Xyz_IEED_002",
				"IC_002",
				0L,
				50000L,
				null,
				null,
				null,
				null);
		study00.setRecording(recording);

		final ContactGroup electrode000 = new ContactGroup(
				"LO",
				"Depth",
				499.9070,
				1.1938,
				8.373,
				false,
				Side.LEFT,
				Location.OCCIPITAL,
				39.7,
				"Ad-Tech",
				newUuid());
		recording.addContactGroup(electrode000);

		final TimeSeriesEntity trace0000 = new TimeSeriesEntity();
		trace0000.setPubId((randomUUID().toString()));

		trace0000.setFileKey("j/j/j");
		trace0000.setLabel("j");

		final Contact contact0000 = new Contact(
				ContactType.MACRO,
				new MriCoordinates(34.9, -987.7, 5235.3),
				new TalairachCoordinates(4.5,
						-98.4, 8383.09),
				trace0000);
		electrode000.addContact(contact0000);

		final TimeSeriesEntity trace0001 = new TimeSeriesEntity();
		trace0001.setPubId((randomUUID().toString()));

		trace0001.setFileKey("kjkgejk/jkjkj/dj");
		trace0001.setLabel("dj");

		final Contact contact0001 = new Contact(
				ContactType.MICRO,
				new MriCoordinates(34.9, -987.7, -97.7),
				new TalairachCoordinates(4.5,
						-98.4, 8384.093),
				trace0001);

		electrode000.addContact(contact0001);

		return patient1;
	}

	public static Patient buildPatient2() {
		final Patient patient2 = new Patient();
		patient2.setEthnicity(Ethnicity.BLACK);
		patient2.setGender(Gender.FEMALE);
		patient2.setHandedness(Handedness.RIGHT);
		patient2.setOrganization(OrganizationsForTests.ANOTHER);
		patient2.setLabel("2-C");
		patient2.setAgeOfOnset(2);
		patient2.setEtiology(Pathology.CRYPTOGENIC.toString());
		patient2.setDevelopmentalDisorders(Boolean.FALSE);
		patient2.setTraumaticBrainInjury(Boolean.FALSE);
		patient2.setFamilyHistory(Boolean.TRUE);
		patient2.getSzTypes().add(SeizureType.MYOCLONIC);
		patient2.getSzTypes().add(
				SeizureType.PARTIAL_WITH_2NDARY_GENERALIZATION);
		patient2.getSzTypes().add(SeizureType.TONIC_CLONIC);
		patient2.getPrecipitants().add(Precipitant.CATAMENIAL);
		patient2.getPrecipitants().add(Precipitant.SLEEP_DEPRIVATION);
		patient2.getPrecipitants().add(Precipitant.STRESS);
		return patient2;
	}

	/**
	 * 
	 * Returns a {@code Patient} with the given {@code userSuppliedId} and which
	 * has no {@code HospitalAdmission}s.
	 * 
	 * @deprecated instead of this method, use one of the
	 *             {@code buildPatientX()} methods and remove and/or replace
	 *             children as desired.
	 * 
	 * @param userSuppliedId
	 * @return a {@code Patient} with the given {@code userSuppliedId} and which
	 *         has no {@code HospitalAdmission}s
	 */
	@Deprecated
	static public Patient buildShallowPatient(final String userSuppliedId) {
		final Patient patient = new Patient();

		patient.setEthnicity(Ethnicity.AMERICAN_INDIAN);
		patient.setGender(Gender.FEMALE);
		patient.setHandedness(Handedness.AMBIDEXTROUS);
		patient.setOrganization(OrganizationsForTests.SPIKE);
		patient.setLabel(userSuppliedId);
		patient.setAgeOfOnset(12);
		patient.setEtiology(Pathology.FOCAL_CORTICAL_DYSPLASIA
				.toString());
		patient.setDevelopmentalDelay("A dev delay.");
		patient.setDevelopmentalDisorders(Boolean.FALSE);
		patient.setTraumaticBrainInjury(Boolean.TRUE);
		patient.setFamilyHistory(Boolean.TRUE);
		patient.getSzTypes().add(SeizureType.ABSENCE);
		patient.getSzTypes().add(SeizureType.ATONIC);
		patient.getPrecipitants().add(Precipitant.ALCOHOL);
		patient.getPrecipitants().add(Precipitant.DRUGS);
		patient.getPrecipitants().add(Precipitant.FEBRILE);

		return patient;
	}

	public static EegStudy buildStudy000(
			PermissionEntity readPerm) {

		final EegStudy study000 =
				new EegStudy(
						new ExtAclEntity(
								singleton(readPerm)));

		study000.setPubId(newUuid());
		study000.setLabel("some label " + randomUUID().toString());
		study000.setSzCount(15);
		study000.setType(EegStudyType.BRAIN_MAPPING);
		// study000.setStartTimeUutc(0L);
		// study000.setEndTimeUutc(50000L);

		final Recording recording = new Recording(
				"Xyz_IEED_001",
				"IC_001",
				0L,
				50000L,
				null,
				null,
				null,
				null);
		study000.setRecording(recording);

		recording.getImages().addAll(buildImages000());

		final Set<ContactGroup> electrodes = buildElectrodes000();
		for (final ContactGroup electrode : electrodes) {
			recording.addContactGroup(electrode);
		}
		return study000;
	}

	/**
	 * 
	 * Returns an {@code EegStudy} with {@code dir} set to {@code studyDir} and
	 * a {@code ReferenceElectrode} with the given
	 * {@code ReferenceElectrodeInfo}.
	 * 
	 * @deprecated instead of this method, use {@code buildStudy000()} and
	 *             remove and/or replace children as desired.
	 * 
	 * @param studyDir
	 * @return a shallowish {@code EegStudy}
	 */
	@Deprecated
	public static EegStudy buildStudyNoElectrodesNoImages(
			final String studyDir,
			PermissionEntity readPerm) {
		final EegStudy study = new EegStudy(
				new ExtAclEntity(
						singleton(readPerm)));

		study.setPubId(newUuid());
		study.setLabel("some label " + randomUUID());
		study.setSzCount(3432);
		study.setType(EegStudyType.INTRAOPERATIVE_MONITORING);
		// study.setStartTimeUutc(0L);
		// study.setEndTimeUutc(50000L);

		final Recording recording = new Recording(
				studyDir,
				"jkfejwkjk",
				0L,
				50000L,
				null,
				null,
				null,
				null);
		study.setRecording(recording);

		return study;
	}

	private BuildDb() {
		throw new AssertionError("can't instantiate a BuildDb");
	}
}
