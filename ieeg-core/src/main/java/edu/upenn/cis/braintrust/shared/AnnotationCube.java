package edu.upenn.cis.braintrust.shared;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.Previewable;

/**
 * Datacube-like info on annotations
 */
@GwtCompatible(serializable=true)
public class AnnotationCube implements Serializable, Previewable {
	String pubId;
	String annotator;
	String layer;
	String type;
	long count;

	public AnnotationCube() { }

	public AnnotationCube(String pubId, String annotator, String layer, String type, long count) {
		super();
		this.pubId = pubId;
		this.annotator = annotator;
		this.layer = layer;
		this.type = type;
		this.count = count;
	}

	public String getPubId() {
		return pubId;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	public String getAnnotator() {
		return annotator;
	}

	public void setAnnotator(String annotator) {
		this.annotator = annotator;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Annotation on timeseries " + pubId + ", by " + annotator +", in layer "
				+ layer + ", of type " + type + ": " + count;
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		if (count > 1)
			return count + " " + type + " annotations by " + annotator;
		else {
			String t = type;
			if (t.length() > 0)
				t = Character.toUpperCase(t.charAt(0)) + t.substring(1);
			else
				t = type;
			return t + " annotation by " + annotator;
		}
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Set of annotations ";
	}
	
}