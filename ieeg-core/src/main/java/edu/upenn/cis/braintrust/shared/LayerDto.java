/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;

@GwtCompatible(serializable = true)
public final class LayerDto implements IHasName, Serializable {

	private static final long serialVersionUID = 1L;

	private Optional<LayerId> id = Optional.absent();
	private String name;
	private Optional<Integer> annotationCount = Optional.absent();

	public LayerDto(String name) {
		this.name = checkNotNull(name);
	}

	public LayerDto(
			String name,
			LayerId layerId) {
		this(name);
		this.id = Optional.of(layerId);
	}

	public LayerDto(
			String name,
			LayerId layerId,
			Integer annotationCount) {
		this(name, layerId);
		this.annotationCount = Optional.of(annotationCount);
	}

	public Optional<Integer> getAnnotationCount() {
		return annotationCount;
	}

	public Optional<LayerId> getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setAnnotationCount(Integer annotationCount) {
		this.annotationCount = Optional.of(annotationCount);
	}

	public void setId(LayerId id) {
		this.id = Optional.of(id);
	}

	public void setName(String name) {
		this.name = checkNotNull(name);
	}
}
