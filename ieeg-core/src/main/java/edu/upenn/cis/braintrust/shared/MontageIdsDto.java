/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableSet;


/**
 * Contains all of the id's in a montage.
 * 
 * @author Sam Donnelly
 */
@GwtCompatible(serializable = true)
@Immutable
public final class MontageIdsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private MontageIdAndVersion montageId;
	private Set<MontagedChId> montagedChannelIds;

	public MontageIdsDto(
			MontageIdAndVersion montageId,
			Set<MontagedChId> montagedChannelIds) {
		this.montageId = checkNotNull(montageId);
		this.montagedChannelIds = ImmutableSet.copyOf(montagedChannelIds);
	}

	public Set<MontagedChId> getMontagedChannelIds() {
		return montagedChannelIds;
	}

	public MontageIdAndVersion getMontageId() {
		return montageId;
	}
}
