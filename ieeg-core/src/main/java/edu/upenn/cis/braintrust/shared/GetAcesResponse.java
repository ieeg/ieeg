/*
 * Copyright (C) 2011 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.List;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Optional;
import com.google.common.collect.BiMap;

import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;

@GwtCompatible(serializable = true)
public class GetAcesResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean canEditAcl;
	private List<ExtUserAce> userAces;
	private List<ExtProjectAce> projectAces;
	private ExtWorldAce worldAce;
	private BiMap<String, ExtPermission> displayStrToUserPerm;
	private BiMap<String, ExtPermission> displayStrToProjectGroupPerm;
	private BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm;

	// For GWT
	@SuppressWarnings("unused")
	private GetAcesResponse() {}

	public GetAcesResponse(
			boolean canEditAcl,
			ExtWorldAce worldAce,
			List<ExtProjectAce> projectAces,
			List<ExtUserAce> userAces,
			BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm,
			BiMap<String, ExtPermission> displayStrToProjectPerm,
			BiMap<String, ExtPermission> displayStrToUserPerm) {
		this.canEditAcl = canEditAcl;
		this.worldAce = checkNotNull(worldAce);
		this.projectAces = checkNotNull(projectAces);
		this.userAces = checkNotNull(userAces);
		this.displayStrToUserPerm = checkNotNull(displayStrToUserPerm);
		this.displayStrToProjectGroupPerm = checkNotNull(displayStrToProjectPerm);
		this.displayStrToWorldPerm = checkNotNull(displayStrToWorldPerm);
	}

	public boolean canEditAcl() {
		return canEditAcl;
	}

	public List<ExtUserAce> getUserAces() {
		return userAces;
	}
	
	public List<ExtProjectAce> getProjectAces() {
		return projectAces;
	}

	public ExtWorldAce getWorldAce() {
		return worldAce;
	}

	/**
	 * Returns a map of display strings to permission dtos. The iteration order
	 * of the map is the correct display order of the permissions.
	 * 
	 * @return the displayStrToUserPerm
	 */
	public BiMap<String, ExtPermission> getDisplayStrToUserPerm() {
		return displayStrToUserPerm;
	}
	
	/**
	 * Returns a map of display strings to permission dtos. The iteration order
	 * of the map is the correct display order of the permissions.
	 * 
	 * @return the displayStrToProjectGroupPerm
	 */
	public BiMap<String, ExtPermission> getDisplayStrToProjectGroupPerm() {
		return displayStrToProjectGroupPerm;
	}

	/**
	 * Returns a map of display strings to permission dtos. The iteration order
	 * of the map is the correct display order of the permissions.
	 * 
	 * @return the displayStrToWorldPerm
	 */
	public BiMap<String, Optional<ExtPermission>> getDisplayStrToWorldPerm() {
		return displayStrToWorldPerm;
	}

}
