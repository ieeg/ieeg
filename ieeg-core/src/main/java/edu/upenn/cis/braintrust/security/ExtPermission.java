/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.security;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import com.google.common.annotations.GwtCompatible;
import com.google.common.base.Function;

/**
 * @author John Frommeyer
 */
@Immutable
@GwtCompatible(serializable = true)
public final class ExtPermission implements Serializable {

	public static Function<ExtPermission, String> getValue = new Function<ExtPermission, String>() {

		@Override
		public String apply(ExtPermission input) {
			return input.getValue();
		}
	};

	private static final long serialVersionUID = 1L;

	private String domain;
	private String mode;
	private String value;

	//For GWT
	@SuppressWarnings("unused")
	private ExtPermission() {}
	
	public ExtPermission(String domain, String mode, String actionSet) {
		this.domain = checkNotNull(domain);
		this.mode = checkNotNull(mode);
		this.value = checkNotNull(actionSet);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ExtPermission)) {
			return false;
		}
		ExtPermission other = (ExtPermission) obj;
		if (mode == null) {
			if (other.mode != null) {
				return false;
			}
		} else if (!mode.equals(other.mode)) {
			return false;
		}
		if (domain == null) {
			if (other.domain != null) {
				return false;
			}
		} else if (!domain.equals(other.domain)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	public String getDomain() {
		return domain;
	}

	public String getMode() {
		return mode;
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mode == null) ? 0 : mode.hashCode());
		result = prime * result
				+ ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "ExtPermission [permDomain=" + domain + ", mode=" + mode
				+ ", value=" + value + "]";
	}

}
