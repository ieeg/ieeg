/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.unmodifiableSet;

import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Index;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IHasRecording;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;

@NamedQueries({
		@NamedQuery(
				name = EegStudy.CONTACT_BY_TRACE_PUB_ID,
				query = "from Contact c where c.trace.pubId = :timeSeriesPubId"),
		@NamedQuery(
				name = EegStudy.COUNT_WORLD_READABLE_STUDIES,
				query = "select count(*) from EegStudy es join es.extAcl acl "
						+ ExtAclEntity.FROM_WORLD_READABLE
						+ "where "
						+ ExtAclEntity.WHERE_WORLD_READABLE),
		@NamedQuery(
				name = EegStudy.COUNT_USER_READABLE_STUDIES,
				query = "select count(s) from EegStudy s "
						+ "join s.extAcl acl "
						+ "where "
						+ ExtAclEntity.WHERE_USER_READABLE),
		@NamedQuery(
				name = EegStudy.COUNT_ALL_STUDIES,
				query = "select count(*) from EegStudy es"),
		@NamedQuery(
				name = EegStudy.BY_TIME_SERIES,
				query = "From EegStudy s "
						+ "join fetch s.recording r "
						+ "join fetch r.contactGroups g "
						+ "join fetch g.contacts c "
						+ "join fetch c.trace t "
						+ "where t = :timeSeries"),
		@NamedQuery(
				name = EegStudy.BY_RECORDING,
				query = "From EegStudy s "
						+ "where s.recording = :recording")
})
@Entity
@Table(name = EegStudy.TABLE)
@PrimaryKeyJoinColumn(name = EegStudy.ID_COLUMN)
public class EegStudy
		extends DataSnapshotEntity
		implements IHasRecording {

	public static final String COUNT_WORLD_READABLE_STUDIES = "EegStudy-countActiveStudies";
	public static final String COUNT_ALL_STUDIES = "EegStudy-countAllStudies";
	public static final String COUNT_USER_READABLE_STUDIES = "EegStudy-countUserReadableStudies";
	public static final String BY_TIME_SERIES = "EegStudy-findByTimeSeries";
	public static final String BY_RECORDING = "EegStudy-findByRecording";

	public static final String TABLE = "eeg_study";

	public static final String UPPER_CASE_TABLE = "EEG_STUDY";
	public static final String ID_COLUMN = TABLE + "_id";
	public static final String CONTACT_BY_TRACE_PUB_ID = "EegStudy-findContactByPubId";

	private HospitalAdmission parent;
	private EegStudyType type;
	private Recording recording;
	private Integer szCount;
	private Set<EpilepsySubtypeEntity> epilepsySubtypes = newHashSet();

	public EegStudy() {}

	/**
	 * Minimal constructor.
	 * 
	 * @param acl
	 * @param extAcl
	 */
	public EegStudy(
			ExtAclEntity extAcl) {
		super(extAcl);
	}

	public EegStudy(
			String label,
			ExtAclEntity extAcl,
			EegStudyType type,
			@Nullable Integer szCount,
			@Nullable Recording recording,
			@Nullable final String clob) {
		super(label, extAcl, clob);
		this.type = checkNotNull(type);
		this.szCount = szCount;
		this.recording = recording;//checkNotNull(recording);
	}

	public EegStudy(
			String label,
			ExtAclEntity extAcl,
			EegStudyType type,
			@Nullable Integer szCount,
			@Nullable final String clob) {
		super(label, extAcl, clob);
		this.type = checkNotNull(type);
		this.szCount = szCount;
		this.recording = null;
	}

	public void removeEpilepsySubtype(EpilepsySubtypeEntity epSubtype) {
		checkNotNull(epSubtype);
		checkArgument(equals(epSubtype.getParent()),
				"this study is not epilepsy subtype's parent");
		checkArgument(epilepsySubtypes.contains(epSubtype),
				"this study does not contain epilepsy subtype");
		epilepsySubtypes.remove(epSubtype);
		if (epSubtype.getParent() != null)
			epSubtype.setParent(null);
	}

	@Transient
	@Override
	public DataSnapshotType getDataSnapshotType() {
		return DataSnapshotType.EEG_STUDY;
	}

	/**
	 * @return the dirUri
	 * @deprecated use getRecording().getDir() instead
	 */
	@Transient
	@Deprecated
	public String getDir() {
		return recording.getDir();
	}

	/**
	 * The end time of EEG monitoring.
	 * 
	 * @return the end time of EEG monitoring
	 * @deprecated use {@code getRecording().getEndtimeUutc()} instead.
	 */
	@Transient
	@Deprecated
	public Long getEndTimeUutc() {
		return recording.getEndTimeUutc();
	}

	@OneToMany(
			mappedBy = "parent",
			cascade = CascadeType.ALL,
			orphanRemoval = true)
	public Set<EpilepsySubtypeEntity> getEpilepsySubtypes() {
		return epilepsySubtypes;
	}

	@Override
	@Transient
	public Set<ImageEntity> getImages() {
		return recording.getImages();
	}

	/**
	 * The relative path to dir of the mef directory.
	 * 
	 * @deprecated use getRecording().getMefDir instead
	 * @return the mefDir
	 */
	@Transient
	@Deprecated
	public String getMefDir() {
		return recording.getMefDir();
	}

	/**
	 * The {@code HospitalAdmission} when this monitoring was performed.
	 * 
	 * @return {@code HospitalAdmission} when this monitoring was performed
	 */
	@ManyToOne(fetch = FetchType.EAGER)//LAZY)
	@JoinColumn(name = HospitalAdmission.ID_COLUMN)
//	@NotNull
	public HospitalAdmission getParent() {
		return parent;
	}

	@Override
	@OneToOne(
			cascade = CascadeType.ALL,
			optional = false,
			fetch = FetchType.EAGER,//LAZY,
			orphanRemoval = true)
	@JoinColumn(name = Recording.ID_COLUMN)
	@OnDelete(action = OnDeleteAction.CASCADE)
//	@NotNull
	public Recording getRecording() {
		return recording;
	}

	/**
	 * The start time of EEG monitoring.
	 * 
	 * @return the start time of EEG monitoring
	 * @deprecated use getRecording().getStarttimeUutc() instead.
	 */
	@Transient
	@Deprecated
	public Long getStartTimeUutc() {
		return recording.getStartTimeUutc();
	}

	/**
	 * The number of seizures observed during the course of this study.
	 * 
	 * @return the number of seizures observed during the course of this study
	 */
	@Column(name = "SEIZURE_COUNT")
	@Index(name = "IDX_SEIZURE_COUNT")
	@Min(0)
	@Override
	public Integer getSzCount() {
		return szCount;
	}

	@Override
	@Transient
	public Set<TimeSeriesEntity> getTimeSeries() {
		final Set<TimeSeriesEntity> tSeries = newHashSet();
		for (final ContactGroup contactGroup : recording.getContactGroups()) {
			for (final Contact contact : contactGroup.getContacts()) {
				tSeries.add(contact.getTrace());
			}
		}
		return unmodifiableSet(tSeries);
	}

//	@NotNull
	public EegStudyType getType() {
		return type;
	}

	@SuppressWarnings("unused")
	private void setEpilepsySubtypes(Set<EpilepsySubtypeEntity> epilepsySubtypes) {
		this.epilepsySubtypes = epilepsySubtypes;
	}

	public void setParent(@Nullable HospitalAdmission hospitalAdmission) {
		this.parent = hospitalAdmission;
	}

	public void setRecording(Recording recording) {
		this.recording = recording;
	}

	public void setSzCount(final Integer szCount) {
		this.szCount = szCount;
	}

	public void setType(final EegStudyType studyType) {
		this.type = studyType;
	}

	@Transient
	@Override
	public OrganizationEntity getOrganization() {
		if (getParent() == null) {
			System.err.println("Will I end up here?");
		}
		return getParent().getParent().getOrganization();
	}

	public static final EntityConstructor<EegStudy, String, UserEntity> CONSTRUCTOR =
		new EntityConstructor<EegStudy,String,UserEntity>(EegStudy.class,
				new IPersistentObjectManager.IPersistentKey<EegStudy,String>() {

			@Override
			public String getKey(EegStudy o) {
				return o.getPubId();
			}

			@Override
			public void setKey(EegStudy o, String newKey) {
				o.setPubId(newKey);
			}

		},
		new EntityPersistence.ICreateObject<EegStudy,String,UserEntity>() {

			@Override
			public EegStudy create(String label, UserEntity optParent) {
				// Null type, seizure count, clob
				return new EegStudy(label, PersistentObjectFactory.getFactory().getEmptyWorldAcl(), null, null, null);
			}
		}
				);

	@Override
	public EntityConstructor<EegStudy, String, UserEntity> tableConstructor() {
		return CONSTRUCTOR;
	}
}
