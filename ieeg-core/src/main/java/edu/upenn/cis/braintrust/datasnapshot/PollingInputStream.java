/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.braintrust.datasnapshot;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author John Frommeyer
 *
 */
public final class PollingInputStream extends FilterInputStream {

	private final InputStream in;
	private boolean poll = true;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Thread pollingThread = new Thread(new Poller());

	private final class Poller implements Runnable {

		@Override
		public void run() {
			final String m = "run()";
			while (poll) {
				try {
					in.skip(0);

					Thread.sleep(2000);
				} catch (IOException e) {
					logger.error(m + ": Ignoring", e);
				} catch (InterruptedException e) {
					logger.debug("{}: Stopping polling", m);
					Thread.currentThread().interrupt();
					poll = false;
					break;
				}
			}
		}

	}

	public PollingInputStream(InputStream in) {
		super(in);
		this.in = in;
		pollingThread.start();
	}

	@Override
	public void close() throws IOException {
		poll = false;
		pollingThread.interrupt();
		in.close();
	}

}
