/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.braintrust.dao.snapshots;

import java.util.Set;

import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.dto.TraceForImage;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.ImageEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;

public interface IImageDAO extends IDAO<ImageEntity, Long> {
	Set<TraceForImage> findByFileKey(final String fileKey);

	Set<ImageForStudy> findByStudyDirAndImageType(final String studyDir,
			final ImageType imageType);

	Set<ImageForStudy> findByStudyDir(final String studyDir);

	long countImagesDataset(Dataset ds);

	long countImagesRecording(Recording recording);
}
