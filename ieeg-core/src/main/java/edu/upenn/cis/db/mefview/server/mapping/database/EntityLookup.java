/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mapping.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.dao.SecurityUtil;
import edu.upenn.cis.braintrust.dao.permissions.IPermissionDAO;
import edu.upenn.cis.braintrust.drupal.model.DrupalUser;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.Dataset;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.ExtUserAceEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.ProjectAceEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.Scope;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceObjectNotFoundException;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class EntityLookup {
	public static EntityLookup main = null;
	
//	IDataSnapshotDAO snapshotDAO;
	IPermissionDAO permissionDAO;
	IUserService userService;
	EntityPersistence<UserEntity,UserId,DrupalUser> userDb;
	EntityPersistence<DataSnapshotEntity,String,Void> snapDb;
	ScopeRetriever currentScope;
	
	public interface ScopeRetriever {
		public Scope getScope();
	}
	
	public EntityLookup(
//			IDataSnapshotDAO snapshot, 
			IPermissionDAO permission,
//			IUserDAO user,
			IUserService uService,
			ScopeRetriever defaultSession) throws PersistenceObjectNotFoundException {
//		snapshotDAO = snapshot;
		permissionDAO = permission;
//		userDAO = user;
		userService = uService;
		userDb = 
				(EntityPersistence<UserEntity,UserId,DrupalUser>)PersistentObjectFactory.getFactory().getPersistenceManager(UserEntity.class);
		snapDb = 
				(EntityPersistence<DataSnapshotEntity,String,Void>)PersistentObjectFactory.getFactory().getPersistenceManager(DataSnapshotEntity.class);
		currentScope = defaultSession;
		main = this;
	}
	
	public static synchronized EntityLookup getSingleton() {
		if (main == null) {
			throw new RuntimeException("Uninitialized EntityLookup!");
		} else
			return main;
	}
	
//	public IPermissionDAO getPermissionDAO() {
//		return permissionDAO;
//	}
//	public void setPermissionDAO(IPermissionDAO permissionDAO) {
//		this.permissionDAO = permissionDAO;
//	}
	
	/**
	 * Given a list of user names, collect the corresponding user entities
	 * 
	 * @param users
	 * @return
	 */
	public List<UserEntity> getUserEntitiesFor(List<String> users) {
		List<UserEntity> ret = new ArrayList<UserEntity>();
		
		for (String user: users) {
			User u = userService.findUserByUsername(user);
			ret.add(userDb.read(u.getUserId(), currentScope.getScope()));
		}

		return ret;
	}
	
	public UserEntity getUserEntityFor(String userName) {
		User u = userService.findUserByUsername(userName);
//		return userDAO.findById(u.getUserId(), false);
		return (userDb.read(u.getUserId(), 
				currentScope.getScope()));
	}
	
	public UserEntity getUserEntityFor(UserId id) {
//		return userDAO.findById(id, false);
		return (userDb.read(id, 
				currentScope.getScope()));
	}
	
	public User getUserFor(String username) {
		return userService.findUserByUsername(username);
	}

	public User getUserFor(UserId id) {
		return userService.findUserByUid(id);
	}

	/**
	 * DataSnapshotEntities by their SearchResults / PresentableMetadata 
	 * @param snapshots
	 * @return
	 */
	public List<DataSnapshotEntity> getDataSnapshotEntities(
			List<PresentableMetadata> snapshots) {
		List<DataSnapshotEntity> ret = new ArrayList<DataSnapshotEntity>();
		
			for (PresentableMetadata pm: snapshots) {
			
			ret.add(snapDb.read(pm.getId(), currentScope.getScope()));
//			ret.add(snapshotDAO.findByNaturalId(pm.getId(), false));
		}
		
		return ret;
	}

	/**
	 * Retrieve DataSnapshotEntities by String IDs
	 * @param snapshots
	 * @return
	 */
	public List<DataSnapshotEntity> getDataSnapshotEntitiesByIds(
			List<String> snapshots) {
		List<DataSnapshotEntity> ret = new ArrayList<DataSnapshotEntity>();
	
		for (String id: snapshots) {
//			ret.add(snapshotDAO.findByNaturalId(id, false));
			ret.add(snapDb.read(id, currentScope.getScope()));
		}
		
		return ret;
	}

	/**
	 * DataSnapshotEntities by their SearchResults / PresentableMetadata 
	 * @param snapshots
	 * @return
	 */
	public DataSnapshotEntity getDataSnapshotEntity(
			PresentableMetadata snapshot) {

		return snapDb.read(snapshot.getId(), currentScope.getScope());
	}

	/**
	 * Retrieve DataSnapshotEntities by String IDs
	 * @param snapshots
	 * @return
	 */
	public DataSnapshotEntity getDataSnapshotEntityById(
			String snapshot) {
		return snapDb.read(snapshot, currentScope.getScope());
	}

	public ExtAclEntity createNewDefaultExtAcl(User user, String pubId) {
//		UserEntity userE = userDAO.findById(user.getUserId(), false);
		UserEntity userE = userDb.read(user.getUserId(), 
				currentScope.getScope());
		
		return SecurityUtil.createUserOwnedAcl(userE, permissionDAO);
	}
	
	public User getUserByEntity(UserEntity entity) {
		return userService.findUserByUid(entity.getId());
	}
	
	public Map<String, Set<String>> getUserPermissions(UserTaskEntity entity) {
		Map<String, Set<String>> permissions = new HashMap<String, Set<String>>();

		if (entity.getExtAcl() != null) {
			Set<ExtUserAceEntity> userAces = entity.getExtAcl().getUserAces();
			
			String name;
			for (ExtUserAceEntity eace : userAces) {
				User user = getUserByEntity(eace.getUser());
				if (user != null)
					name = user.getUsername();
				else
					name = "GUEST";
				permissions.put(name, new HashSet<String>());
				for (PermissionEntity perm: eace.getPerms()) {
					permissions.get(name).add(perm.getName());
				}
			}
		}		
		return permissions;
	}

	/**
	 * TODO: for now just leave this empty
	 * 
	 * @param pubId
	 * @return
	 */
	public List<String> getChannelNamesFor(String pubId) {
		return new ArrayList<String>();
	}
	
	public List<String> getChannelIdsFor(String pubId) {
		return new ArrayList<String>();
	}
	
	public String getCreator(DataSnapshotEntity dse) {
		if (dse instanceof Dataset) {
			return ((Dataset)dse).getCreator().getUsername();
		}

		return null;
	}

	public Set<String> getWorldPermissions(UserTaskEntity entity) {
		Set<String> permissions = new HashSet<String>();

		if (entity.getExtAcl() != null) {
			Set<PermissionEntity> worldAces = entity.getExtAcl().getWorldAce();
	//		Set<ProjectAceEntity> projectAces = entity.getExtAcl().getProjectAces();
			
			String name;
			for (PermissionEntity perm: worldAces) {

				permissions.add(perm.getName());
				
			}
		}		
		return permissions;
	}

	public Map<String, Set<String>> getProjectPermissions(UserTaskEntity entity) {
		Map<String, Set<String>> permissions = new HashMap<String, Set<String>>();

		if (entity.getExtAcl() != null) {
			Set<ProjectAceEntity> projectAces = entity.getExtAcl().getProjectAces();
			
			String name;
			for (ProjectAceEntity eace : projectAces) {
				String proj = eace.getProject().getPubId();

				permissions.put(proj, new HashSet<String>());
				for (PermissionEntity perm: eace.getPerms()) {
					permissions.get(proj).add(perm.getName());
				}
			}
		}		
		return permissions;
	}

}
