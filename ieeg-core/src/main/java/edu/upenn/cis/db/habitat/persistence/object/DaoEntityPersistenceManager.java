/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.object;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javassist.Modifier;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.StringKeyValue;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.Scope;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;


public abstract class DaoEntityPersistenceManager<T extends IEntity, K extends Serializable, P> 
	extends EntityPersistence<T, K, P> {
	IDAO<T,K> dao = null;
	IPersistentObjectManager.IPersistentKey<T, K> keyFetcher;

	public DaoEntityPersistenceManager(IDAO<T,K> dao,
			IPersistentObjectManager.IPersistentKey<T,K> key,
			EntityPersistence.ICreateObject<T, String, P> creator) {
		super(creator);
		this.dao = dao;
		this.keyFetcher = key;
	}
	
	protected abstract T findById(K id, Scope scope);
	
	@Override
	public List<T> read(T example, Scope scope) {
		List<T> ret = new ArrayList<T>();
		scope.createNewScope();
		try {
			dao.setScope(scope);
			
			ret.addAll(dao.findByExample(example));
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		
		return ret;
	}
	
	@Override
	public boolean alreadyExistsKey(K key, Scope scope) {
		scope.createNewScope();
		boolean ret = false;
		try {
			dao.setScope(scope);
			ret = findById(key, scope) != null;
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		return ret;
	}

	@Override
	public boolean alreadyExists(T o, Scope scope) {
		scope.createNewScope();
		boolean ret = false;
		try {
			dao.setScope(scope);
			ret = findById(keyFetcher.getKey(o), scope) != null;
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		return ret;
	}

	@Override
	public void write(T o, Scope scope) {
		scope.createNewScope();
		
		try {
			scope.saveOrUpdate(o);
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
	}

	@Override
	public T read(K key, 
			Scope scope) {
		T ret = null;
		
		scope.createNewScope();
		
		try {
			dao.setScope(scope);
			ret = findById(key, scope);//dao.findByNaturalId(key, false);
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		
		return ret;
	}

	@Override
	public long count(Scope scope) {
		scope.createNewScope();
		
		long count = 0;
		try {
			count = dao.findAll().size();
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		
		return count;
	}

	@Override
	public T readOrCreateIdempotent(
			K key,
			String label,
			P optParent, 
			EntityPersistence.ICreateObject<T, String, P> creator,
			Scope scope) {
		scope.createNewScope();
		
		T ret = null;
		try {
			ret = findById(key, scope);//dao.findByNaturalId(key, false);
			
			// Persist it!
			if (ret == null) {
				ret = creator.create(label, optParent);
				scope.save(ret);
				ret = dao.findByNaturalId(label, false);
			}
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		
		return ret;
	}

	@Override
	public T createTransient(
			String label,
			P optParent, 
			EntityPersistence.ICreateObject<T, String, P> creator) {
		return creator.create(label, optParent);
	}

	@Override
	public T createPersistent(
			String label,
			P optParent, 
			EntityPersistence.ICreateObject<T, String, P> creator,
			Scope scope) {
		scope.createNewScope();
		
		T ret = null;
		try {
			// Persist it!
			ret = creator.create(label, optParent);
			scope.save(ret);
			ret = dao.findByNaturalId(label, false);
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		
		return ret;
	}
	
	@Override
	public boolean delete(K key, Scope scope) {
		try {
			T item = null;
			item = dao.findById(key, true);
			scope.delete(item);
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		return true;
	}
	
	public boolean rename(K oldKey, K newKey, Scope scope) {
		try {
			T item = null;
			item = dao.findById(oldKey, true);
			
			keyFetcher.setKey(item, newKey);
			scope.saveOrUpdate(item);
		} catch (Exception e) {
			scope.rollbackScope();
			throw e;
		}
		scope.commitScope();
		return true;
	}
}
