package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import com.amazonaws.auth.AWSCredentials;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;

@GwtIncompatible("AWSCredentials")
public class AWSContainerCredentials implements ContainerCredentials, Serializable {
	AWSCredentials credentials;
	
	public AWSContainerCredentials() {}
	
	public AWSContainerCredentials(AWSCredentials creds) {
		credentials = creds;
	}

	public AWSCredentials getCredentials() {
		return credentials;
	}

	public void setCredentials(AWSCredentials credentials) {
		this.credentials = credentials;
	}
	
	@Override
	public String toString() {
		return credentials.getAWSAccessKeyId();
	}
}
