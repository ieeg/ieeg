/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import java.util.HashMap;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Sample implements IsSerializable, Comparable<Sample> {
	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public HashMap<String, Integer> getValues() {
		return values;
	}

	public void setValues(HashMap<String, Integer> values) {
		this.values = values;
	}

	public double time;
	public HashMap<String,Integer> values;
	
	public Sample() {
		this.time = 0;
		values = new HashMap<String,Integer>();
	}
	
	public Sample(double time) {
		this.time = time;
		values = new HashMap<String,Integer>();
	}
	
	public int compareTo(Sample s) {
		if (time < s.time)
			return -1;
		else if (time > s.time)
			return 1;
		else
			return 0;
	}
}
