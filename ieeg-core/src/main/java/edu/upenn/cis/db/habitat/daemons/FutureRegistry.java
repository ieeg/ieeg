/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.daemons;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.util.concurrent.Futures;

public class FutureRegistry<K extends IMessage, T> {
	public static interface IFutureResult<K,V> extends Callable<V> {
		public void setView(K v);
		
		public K getRequest();
		
		public void setResult(V object);
		
		public V getResult();
	}
	public static abstract class FutureResult<K,V> implements IFutureResult<K,V> {
		K view;
		V object;
		
		public void setView(K v) {
			this.view = v;
		}
		
		public K getRequest() {
			return view;
		}
		
		public void setResult(V object) {
			this.object = object;
		}
		
		public V getResult() {
			return object;
		}
	}
	
	public static class RegisteredHandler<K,T> {
		IFutureResult<K,T> success;
		IFutureResult<K,String> fail;
		boolean onlyOnce = false;
		boolean hasFired = false;
		
		public RegisteredHandler(IFutureResult<K, T> success,
				IFutureResult<K, String> fail, boolean onlyOnce) {
			super();
			this.success = success;
			this.fail = fail;
			this.onlyOnce = onlyOnce;
		}
		
		public RegisteredHandler(IFutureResult<K, T> success,
				IFutureResult<K, String> fail) {
			this(success, fail, true);
		}

		public IFutureResult<K, T> getSuccess() {
			return success;
		}

		public void setSuccess(IFutureResult<K, T> success) {
			this.success = success;
		}

		public IFutureResult<K, String> getFail() {
			return fail;
		}

		public void setFail(IFutureResult<K, String> fail) {
			this.fail = fail;
		}

		public boolean isOnlyOnce() {
			return onlyOnce;
		}

		public void setOnlyOnce(boolean onlyOnce) {
			this.onlyOnce = onlyOnce;
		}

		public boolean isHasFired() {
			return hasFired;
		}

		public void setHasFired(boolean hasFired) {
			this.hasFired = hasFired;
		}
		
		
	}
	
	/**
	 * For each channel, we can register a set of channel handlers and a set of
	 * (cacheable) "view" handlers.
	 * 
	 * @author zives
	 *
	 * @param <K>
	 * @param <T>
	 */
	public static class ChannelView<K,T> {
		Map<K,Multiset<RegisteredHandler<K,T>>> handlers = 
				new ConcurrentHashMap<K,Multiset<RegisteredHandler<K,T>>>();
		
		Multiset<RegisteredHandler<K,T>> requestHandlers = ConcurrentHashMultiset.create();

		public Multiset<RegisteredHandler<K,T>> getCallbacks(K key) {
			return handlers.get(key);
		}
		
		public boolean isEmpty() {
			return handlers.isEmpty() && requestHandlers.isEmpty(); 
		}
		
		public void addCallbacks(K key, RegisteredHandler<K,T> handler) {
			synchronized (this) {
				if (!handlers.containsKey(key)) {
					handlers.put(key, ConcurrentHashMultiset.<RegisteredHandler<K,T>>create());
				}
			}
			synchronized (handlers.get(key)) {
				handlers.get(key).add(handler);
			}
		}

		public void removeCallback(K key, RegisteredHandler<K,T> handler) {
			synchronized (this) {
				if (!handlers.containsKey(key)) {
					return;
				}
			}
			synchronized (handlers.get(key)) {
				handlers.get(key).remove(handler);
			}
		}

		public void addRequestHandler(RegisteredHandler<K,T> handler) {
			requestHandlers.add(handler);
		}
		
		public void removeRequestHandler(RegisteredHandler<K,T> handler) {
			requestHandlers.remove(handler);
		}

		public Multiset<RegisteredHandler<K,T>> getRequestHandlers() {
			return requestHandlers;
		}
	}
	
	public Map<String,ChannelView<K,T>> channelMaps = 
			new ConcurrentHashMap<String, ChannelView<K,T>>();

	/**
	 * Register a listener for a view, and also an error handler in case the view cannot
	 * be computed.
	 * 
	 * @param view
	 * @param f
	 * @param error
	 */
	public RegisteredHandler<K,T> register(String channel, K view, IFutureResult<K,T> f, 
			IFutureResult<K, String> error, boolean once) {
		
		ChannelView<K,T> channelV = null;
		synchronized (channelMaps) {
			channelV = channelMaps.get(channel);
			if (channelV == null) {
				channelV = new ChannelView<K,T>();
				channelMaps.put(channel, channelV);
			}
		}
		
		RegisteredHandler<K,T> ret = new RegisteredHandler<K,T>(f, error, once);
		channelV.addCallbacks(view, ret);
		return ret;
	}
	
	public void registerChannel(String channelName, RegisteredHandler<K,T> f) {
		
		ChannelView<K,T> channel = null;
		synchronized (channelMaps) {
			channel = channelMaps.get(channelName);
			if (channel == null) {
				channel = new ChannelView<K,T>();
				channelMaps.put(channelName, channel);
			}
		}
		
		channel.addRequestHandler(f);
	}

	public void unregisterChannel(String channelName, RegisteredHandler<K,T> f) {
		
		ChannelView<K,T> channel = null;
		synchronized (channelMaps) {
			channel = channelMaps.get(channelName);
			if (channel == null) {
				return;
			}
		}
		
		channel.removeRequestHandler(f);
	}

	public void unregister(K view, RegisteredHandler<K,T> handler) {
		
		ChannelView<K,T> channel = channelMaps.get(view.getChannel());
		
		if (channel != null) {
			channel.removeCallback(view, handler);
			
			if (channel.isEmpty()) {
				synchronized (channelMaps) {
					channelMaps.remove(view.getChannel());
				}
			}
		}
	}
	
	/**
	 * We succeeded with a given object
	 * 
	 * @param view
	 * @param object
	 */
	public void notifySuccess(String channel, K view, T object) {
		Multiset<RegisteredHandler<K,T>> callbacks = 
				channelMaps.get(channel).getCallbacks(view);
		
		boolean fired = false;

		if (callbacks != null) {
			for (RegisteredHandler<K,T> rh: callbacks)
				try {
					IFutureResult<K,T> fut = rh.getSuccess();
					if (!rh.isHasFired()) {
						fired = true;
						fut.setView(view);
						fut.setResult(object);
						rh.setHasFired(true);
						if (rh.isOnlyOnce())
							unregister(view, rh);
						
						fut.call();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		Multiset<RegisteredHandler<K,T>> handlers = channelMaps.get(channel).getRequestHandlers();
		if (handlers != null) {
			for (RegisteredHandler<K,T> rh: handlers)
				try {
					fired = true;
					rh.getSuccess().setView(view);
					rh.getSuccess().setResult(object);
						
					rh.getSuccess().call();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (!fired) {
			System.err.println("Warning: no handler fired for " + channel);
		}
	}

	public Multiset<IFutureResult<K,T>> getSuccessCallbacks(String channel, K view, T object) {
		Multiset<RegisteredHandler<K,T>> callbacks = 
				channelMaps.get(channel).getCallbacks(view);
		
		Multiset<IFutureResult<K,T>> ret =
				ConcurrentHashMultiset.<IFutureResult<K,T>>create();

		boolean fired = false;

		if (callbacks != null) {
			for (RegisteredHandler<K,T> rh: callbacks)
				try {
					IFutureResult<K,T> fut = rh.getSuccess();
					if (!rh.isHasFired()) {
						ret.add(fut);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		Multiset<RegisteredHandler<K,T>> handlers = channelMaps.get(channel).getRequestHandlers();
		if (handlers != null) {
			for (RegisteredHandler<K,T> rh: handlers)
				try {
					fired = true;
					ret.add(rh.getSuccess());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (!fired) {
			System.err.println("Warning: no handler to fire for " + channel);
		}
		return ret;
	}
	
	/**
	 * We failed and have an error 
	 * @param view
	 * @param message
	 */
	public void notifyError(String channel, K view, String message) {
		Multiset<RegisteredHandler<K,T>> callbacks = 
				channelMaps.get(channel).getCallbacks(view);
//		futureMap.remove(view);
//		failureMap.remove(view);
		
		boolean fired = false;
		
		if (callbacks != null) {
			for (RegisteredHandler<K, T> rh: callbacks)
				try {
					IFutureResult<K,String> fut = rh.getFail();
					if (!rh.isHasFired()) {
						fired = true;
						fut.setView(view);
						fut.setResult(message);
						rh.setHasFired(true);
						if (rh.isOnlyOnce())
							unregister(view, rh);
						
						fut.call();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		Multiset<RegisteredHandler<K,T>> handlers = channelMaps.get(channel).getRequestHandlers();
		if (handlers != null) {
			for (RegisteredHandler<K,T> rh: handlers)
//				if (!rh.hasFired) {
				try {
					fired = true;
					rh.getFail().setView(view);
					rh.getFail().setResult(message);
						
					rh.getFail().call();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (!fired) {
			System.err.println("Warning: no handler fired for " + channel);
		}
	}

	public Multiset<IFutureResult<K,String>> getFailureCallbacks(String channel, K view, String message) {
		Multiset<RegisteredHandler<K,T>> callbacks = 
				channelMaps.get(channel).getCallbacks(view);
		
		Multiset<IFutureResult<K,String>> ret =
				ConcurrentHashMultiset.<IFutureResult<K,String>>create();

		boolean fired = false;

		if (callbacks != null) {
			for (RegisteredHandler<K,T> rh: callbacks)
				try {
					IFutureResult<K,String> fut = rh.getFail();
					if (!rh.isHasFired()) {
						ret.add(fut);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		Multiset<RegisteredHandler<K,T>> handlers = channelMaps.get(channel).getRequestHandlers();
		if (handlers != null) {
			for (RegisteredHandler<K,T> rh: handlers)
				try {
					fired = true;
					ret.add(rh.getFail());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		if (!fired) {
			System.err.println("Warning: no handler to fire for " + channel);
		}
		return ret;
	}
}
