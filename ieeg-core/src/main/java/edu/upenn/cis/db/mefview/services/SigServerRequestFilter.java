/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.annotation.Nullable;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthenticationException;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.db.mefview.server.SessionManager;

@PreMatching
public class SigServerRequestFilter implements ContainerRequestFilter {

	private static Logger logger = LoggerFactory
			.getLogger(SigServerRequestFilter.class);

	private IUserService userService = UserServiceFactory.getUserService();
	private IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private final String signatureAlgorithm = "SHA-256";
	private SignatureGenerator sigGenerator = new SignatureGenerator();
	private SessionManager sessionManager;

	public SigServerRequestFilter() {
		int sessionTimeoutMinutes = IvProps.getSessionExpirationMins();
		int sessionCleanupMins = IvProps.getSessionCleanupMins();
		int sessionCleanupDenom = IvProps.getSessionCleanupDenom();
		sessionManager = new SessionManager(
				dsServer,
				sessionTimeoutMinutes,
				sessionCleanupMins,
				sessionCleanupDenom);
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {
		String m = "filter(...)";
		try {
			String path = requestContext.getUriInfo().getAbsolutePath()
					.getPath();
			DateTime inTime = new DateTime();
			logger.debug("{}: path [{}]", m, path);

			String username = requestContext.getHeaderString("username");
			logger.debug("{}: username [{}]", m, username);

			String sessionId =
					requestContext
							.getUriInfo()
							.getQueryParameters()
							.getFirst("sessionId");

			if (path.contains("/timeseries/versionOkay/")
					|| path.contains("/timeseries/test")) {
				// versionOkay and test are not authenticated
			} else if (sessionId != null
					&& canUseSessionId(requestContext)) {
				SessionToken sessionToken = new SessionToken(sessionId);
				User user = verifyUser(m, sessionToken, false, null);
				requestContext.setProperty("user", user);
			} else {
				if (username == null) {
					throw new AuthenticationException("User name required");
				}
				User user = userService.findExistingEnabledUser(username);
				requestContext.setProperty("user", user);
				String hashedPasswd = user.getPassword();
				String httpMethod = requestContext.getMethod();
				String serverName =
						requestContext
								.getUriInfo()
								.getRequestUri()
								.getHost();
				String queryString =
						requestContext
								.getUriInfo()
								.getRequestUri()
								.getQuery();
				String timestamp = requestContext.getHeaderString("timestamp");
				String signature = requestContext.getHeaderString("signature");

				logger.debug("{}: serverName [{}]", m, serverName);
				logger.debug("{}: timestamp [{}]", m, timestamp);
				logger.debug("{}: method [{}]", m, httpMethod);
				logger.debug("{}: query string [{}]", m, queryString);

				DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
				DateTime requestDateTime = parser.parseDateTime(timestamp);

				boolean requestInFuture = requestDateTime.isAfter(inTime);

				if (requestInFuture
						&& Seconds.secondsBetween(inTime, requestDateTime)
								.getSeconds() <= (15 * 60)) {
					// Allow 15 minutes in the future
					requestDateTime = inTime;
					requestInFuture = false;
				}

				int secondsBetween = Seconds
						.secondsBetween(inTime, requestDateTime)
						.getSeconds();

				if (requestInFuture || secondsBetween > (15 * 60)) {
					IeegWsException wse = new IeegWsException(
							"Access Denied",
							IeegWsError.AUTHENTICATION_FAILURE_TIMESTAMP_IN_RANGE);
					logger.error(
							m
									+ ": User could not be authenticated, timestamp expired or is in the future incoming "
									+ requestDateTime + " server " + inTime
									+ ", errorId = "
									+ wse.getErrorId());
					throw wse;
				}

				if (queryString == null) {
					queryString = "";
				}
				// Not calling requestContext.getLength() since it fails for
				// values larger than Integer.MAX_VALUE.
				String contentLengthStr = requestContext
						.getHeaderString(HttpHeaders.CONTENT_LENGTH);
				long contentLengthLong = contentLengthStr == null
						? -1
						: Long.parseLong(contentLengthStr);
				int contentLength = -1;
				if (contentLengthLong < Integer.MAX_VALUE) {
					contentLength = (int) contentLengthLong;
				}

				String payload = "";
				final boolean isCreateObject = path
						.matches(IDatasetResource.CREATE_OBJECT_URL_REG_EXP_STR)
						&& requestContext.getMethod().equals(HttpMethod.POST);
				if (isCreateObject) {
					payload = requestContext.getHeaderString("Content-MD5");
					logger.debug(
							"{}: Using Content-MD5 value for payload of create recording object payload",
							m);
				} else if (contentLength > 1) {
					byte[] payloadBytes = new byte[contentLength];
					ByteStreams.readFully(
							requestContext.getEntityStream(),
							payloadBytes);
					payload = new String(payloadBytes);
					requestContext.setEntityStream(
							new ByteArrayInputStream(payloadBytes));
				}

				logger.debug("{}: payload [{}]", m, payload);
				logger.debug("{}: dateTimeHere: {}", m, requestDateTime);

				String mySignature = sigGenerator
						.getRequestSignature(
								signatureAlgorithm,
								username,
								hashedPasswd,
								httpMethod,
								serverName,
								path,
								queryString,
								timestamp, payload);
				logger.debug("{}: signature [{}]", m, signature);
				logger.debug("{}: mySignature [{}]", m, mySignature);

				if (!signature.equals(mySignature)) {
					throw new IncorrectCredentialsException(
							"signatures don't match");
				}
			}
		} catch (IeegWsException e) {
			logger.error(m + ": IeegWsExeception, rethrowing", e);
			throw e;
		} catch (AuthenticationException ae) {
			IeegWsException wse =
					new IeegWsException(
							"Access Denied",
							IeegWsError.AUTHENTICATION_FAILURE);
			logger.error(
					m
							+ ": User could not be authenticated, throwing IeegWsException with errorId " + wse.getErrorId(),

					ae);
			throw wse;
		} catch (RuntimeException ae) {
			IeegWsException wse =
					new IeegWsException(
							"An error occured on the server",
							IeegWsError.INTERNAL_ERROR);
			logger.error(
					m
							+ ": Runtime exception, throwing IeegWsException with errorId " + wse.getErrorId() ,

					ae);
			throw wse;
		} catch (Error ae) {
			IeegWsException wse =
					new IeegWsException(
							"An error occured on the server",
							IeegWsError.INTERNAL_ERROR);
			logger.error(
					m
							+ ": Error, throwing IeegWsException with errorId " + wse.getErrorId(),

					ae);
			throw wse;
		}
	}

	/**
	 * Returns the {@code User} who owns the session with the given token. If
	 * {@code allowExpiredSess} is true the user is returned even if the session
	 * has expired.
	 * 
	 * @param token
	 * @return the {@code User} who owns the active session with the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out and {@code allowExpiredSess} is false
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out and {@code allowExpiredSess} is false
	 * @throws DisabledAccountException if the user's account has been disabled
	 * @throws IncorrectCredentialsException if the session owner cannot be
	 *             found
	 */
	public User verifyUser(
			String source,
			SessionToken sessionID,
			boolean allowExpiredSess,
			@Nullable Role role) {
		final UserIdSessionStatus userIdSessStatus = sessionManager
				.getUserIdSessionStatus(sessionID);
		final UserId userId = userIdSessStatus.getUserId();
		User user = userService.findUserByUid(userId);
		if (user == null) {
			sessionManager.disableAcctSession(sessionID);
			throw new IncorrectCredentialsException("User [" + userId
					+ "] not found");
		}
		if (user.getStatus() == User.Status.DISABLED) {
			sessionManager.disableAcctSession(sessionID);
			throw new DisabledAccountException("User [" + user.getUsername()
					+ "] account disabled.");
		}

		if (role != null && !user.getRoles().contains(role))
			throw new AuthorizationException("User [" + userId
					+ "] does not have permission for " + source);

		final SessionStatus status = userIdSessStatus.getSessionStatus();
		switch (status) {
			case ACTIVE:
				return user;
			case EXPIRED:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has expired.");
				}
				// Consider logging out as an expiration
			case LOGGED_OUT:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has logged out.");
				}
			case ACCOUNT_DISABLED:
				throw new DisabledAccountException("User ["
						+ user.getUsername()
						+ "] account disabled.");
			default:
				throw new AssertionError("Unknown enum value [" + status + "]");

		}

	}

	private boolean canUseSessionId(ContainerRequestContext context) {
		boolean canUseSessionId = false;
		String path = context.getUriInfo().getAbsolutePath().getPath();
		String relPath = context.getUriInfo().getPath();
		if (path.contains("/timeseries/getTraceListRawCSV/")
				|| path.contains("/timeseries/getTraceListCSV/")
				|| path
						.contains("/timeseries/getAnnotationsListCSV/")) {
			canUseSessionId = true;
		} else if (relPath.matches("^/objects/\\d+$")) {
			canUseSessionId = context.getMethod().equals(HttpMethod.GET);
		}
		return canUseSessionId;
	}

}
