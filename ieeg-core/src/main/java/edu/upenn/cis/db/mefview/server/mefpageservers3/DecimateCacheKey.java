/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.concurrent.Immutable;

import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.Decimate;
import edu.upenn.cis.db.mefview.services.FilterSpec;

@Immutable
public final class DecimateCacheKey implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String fileKey;
	private final String fileEtag;
	private final long startTimeOffset;
	private final long endTimeOffset;
	private final double targetFrequency;
	private final int filterType;
	private final int numPoles;
	private final double bandpassLowCutoff;
	private final double bandpassHighCutoff;
	private final double bandstopLowCutoff;
	private final double bandstopHighCutoff;
	private final String filterName;

	public DecimateCacheKey(
			String fileKey,
			String fileEtag,
			long startTimeOffset,
			long endTimeOffset,
			Decimate decimate) {
		this.fileKey = fileKey;
		this.fileEtag = fileEtag;
		this.startTimeOffset = startTimeOffset;
		this.endTimeOffset = endTimeOffset;
		this.targetFrequency = checkNotNull(decimate.getSampleRate());
		FilterSpec filterSpec = checkNotNull(decimate.getFilter());
		this.filterType = checkNotNull(filterSpec.getFilterType());
		this.numPoles = checkNotNull(filterSpec.getNumPoles());
		this.bandpassLowCutoff = checkNotNull(filterSpec.getBandpassLowCutoff());
		this.bandpassHighCutoff =
				checkNotNull(filterSpec.getBandpassHighCutoff());
		this.bandstopLowCutoff = checkNotNull(filterSpec.getBandstopLowCutoff());
		this.bandstopHighCutoff =
				checkNotNull(filterSpec.getBandstopHighCutoff());
		this.filterName = checkNotNull(filterSpec.getFilterName());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DecimateCacheKey)) {
			return false;
		}
		DecimateCacheKey other = (DecimateCacheKey) obj;
		if (Double.doubleToLongBits(bandpassHighCutoff) != Double
				.doubleToLongBits(other.bandpassHighCutoff)) {
			return false;
		}
		if (Double.doubleToLongBits(bandpassLowCutoff) != Double
				.doubleToLongBits(other.bandpassLowCutoff)) {
			return false;
		}
		if (Double.doubleToLongBits(bandstopHighCutoff) != Double
				.doubleToLongBits(other.bandstopHighCutoff)) {
			return false;
		}
		if (Double.doubleToLongBits(bandstopLowCutoff) != Double
				.doubleToLongBits(other.bandstopLowCutoff)) {
			return false;
		}
		if (endTimeOffset != other.endTimeOffset) {
			return false;
		}
		if (fileKey == null) {
			if (other.fileKey != null) {
				return false;
			}
		} else if (!fileKey.equals(other.fileKey)) {
			return false;
		}
		if (filterName == null) {
			if (other.filterName != null) {
				return false;
			}
		} else if (!filterName.equals(other.filterName)) {
			return false;
		}
		if (filterType != other.filterType) {
			return false;
		}
		if (numPoles != other.numPoles) {
			return false;
		}
		if (startTimeOffset != other.startTimeOffset) {
			return false;
		}
		if (Double.doubleToLongBits(targetFrequency) != Double
				.doubleToLongBits(other.targetFrequency)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(bandpassHighCutoff);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(bandpassLowCutoff);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(bandstopHighCutoff);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(bandstopLowCutoff);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ (int) (endTimeOffset ^ (endTimeOffset >>> 32));
		result = prime * result + ((fileKey == null) ? 0 : fileKey.hashCode());
		result = prime * result
				+ ((filterName == null) ? 0 : filterName.hashCode());
		result = prime * result + filterType;
		result = prime * result + numPoles;
		result = prime * result
				+ (int) (startTimeOffset ^ (startTimeOffset >>> 32));
		temp = Double.doubleToLongBits(targetFrequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public String getFileEtag() {
		return fileEtag;
	}

	@Override
	public String toString() {
		return "DecimateCacheKey{" +
				"bandpassHighCutoff=" + bandpassHighCutoff +
				", fileKey='" + fileKey + '\'' +
				", fileEtag='" + fileEtag + '\'' +
				", startTimeOffset=" + startTimeOffset +
				", endTimeOffset=" + endTimeOffset +
				", targetFrequency=" + targetFrequency +
				", filterName=" + filterName +
				", filterType=" + filterType +
				", numPoles=" + numPoles +
				", bandpassLowCutoff=" + bandpassLowCutoff +
				", bandstopLowCutoff=" + bandstopLowCutoff +
				", bandstopHighCutoff=" + bandstopHighCutoff +
				'}';
	}
}
