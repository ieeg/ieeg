/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

public class JobInfo implements IsSerializable {
	private String jobId;
	private String description;
	private String toolId;
	private int tasks;
	
	//For GWT
	@SuppressWarnings("unused")
	private JobInfo() {}
	
	public JobInfo(String jobId, String toolId, String description, int tasks) {
		this.jobId = jobId;
		this.toolId = toolId;
		this.description = description;
		this.tasks = tasks;
	}

	public String getJobId() {
		return jobId;
	}

	public String getDescription() {
		return description;
	}

	public int getTasks() {
		return tasks;
	}

	public String getToolId() {
		return toolId;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jobId == null) ? 0 : jobId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof JobInfo)) {
			return false;
		}
		JobInfo other = (JobInfo) obj;
		if (jobId == null) {
			if (other.jobId != null) {
				return false;
			}
		} else if (!jobId.equals(other.jobId)) {
			return false;
		}
		return true;
	}


	public static final ProvidesKey<JobInfo> KEY_PROVIDER = new ProvidesKey<JobInfo>() {

		public Object getKey(JobInfo item) {
			return (item == null) ? null : item.jobId;
		}
		
	};
	public static final TextColumn<JobInfo> jobIdentifier = new TextColumn<JobInfo> () {
		@Override
		public String getValue(JobInfo object) {
			return object.getJobId();
		}
	};
	public static final TextColumn<JobInfo> jobDescription = new TextColumn<JobInfo> () {
		@Override
		public String getValue(JobInfo object) {
			return object.getDescription();
		}
	};
	public static final TextColumn<JobInfo> jobTasks = new TextColumn<JobInfo> () {
		@Override
		public String getValue(JobInfo object) {
			return String.valueOf(object.getTasks());
		}
	};
	public static final TextColumn<JobInfo> jobTool = new TextColumn<JobInfo> () {
		@Override
		public String getValue(JobInfo object) {
			return String.valueOf(object.getToolId());
		}
	};
}
