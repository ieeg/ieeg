/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.daemons;

import edu.upenn.cis.db.mefview.shared.JsonTyped;


public interface IMessageBroker<K, V> {
	public interface IConsumer<K,V> {
		public void handle(K key, V value);
		
		public void handleError(K key, JsonTyped value);
	}
	
	/**
	 * Generically send a message
	 * 
	 * @param key
	 * @param value
	 */
	public void send(String channel, K key, V value);
	
	public void sendError(String channel, K key, JsonTyped value);
	
	public void intercept(String channel, K key, IConsumer<K,V> handler);
	
	public void remove(String channel, K key, IConsumer<K,V> handler);

	public void interceptOnce(String channel, K key, IConsumer<K,V> handler);
	
	public void init();
	
	public void close();
}
