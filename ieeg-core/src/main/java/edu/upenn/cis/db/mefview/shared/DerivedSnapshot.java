/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Sets;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata.VALUE_TYPE;

/**
 * Represents the derivation of a snapshot from a parent snapshot,
 * via a tool.
 * 
 * @author zives
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public final class DerivedSnapshot extends PresentableMetadata {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String revId;
	String analysisId;
	String analysisType;
	String analysisTool;
	String location = "habitat://main";
	
//	static MetadataFormatter presenter = null;
	
	SearchResult analysisMetadata;
	static Map<String, VALUE_TYPE> keys;
	static {
		keys = new HashMap<String, VALUE_TYPE>();
		
		keys.put("id", VALUE_TYPE.STRING);
		keys.put("study", VALUE_TYPE.STRING);
		keys.put("type", VALUE_TYPE.STRING);
		keys.put("tool", VALUE_TYPE.STRING);
		keys.put("parent", VALUE_TYPE.STRING);
	}
	
	
	public DerivedSnapshot() {
		
	}
	
	public DerivedSnapshot(String analysisId, String analysisType, String toolType, String revId,
			SearchResult meta) {
		setAnalysisId(analysisId);
		setSnapshotRevId(revId);
		setAnalysisType(analysisType);
		setAnalysisTool(toolType);
	}
	
	public String getAnalysisTool() {
		return analysisTool;
	}

	public void setAnalysisTool(String analysisTool) {
		this.analysisTool = analysisTool;
	}

	public String getSnapshotRevId() {
		return revId;
	}
	public void setSnapshotRevId(String revId) {
		this.revId = revId;
	}
	public String getAnalysisId() {
		return analysisId;
	}
	public void setAnalysisId(String analysisId) {
		this.analysisId = analysisId;
	}
	public String getAnalysisType() {
		return analysisType;
	}
	public void setAnalysisType(String analysisType) {
		this.analysisType = analysisType;
	}

	public static final ProvidesKey<DerivedSnapshot> KEY_PROVIDER = new ProvidesKey<DerivedSnapshot>() {

		public Object getKey(DerivedSnapshot item) {
			return (item == null) ? null : item.getAnalysisId();//.getFriendlyName();
		}
		
	};

	@Override
	public SerializableMetadata getParent() {
		return analysisMetadata;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SearchResult)
			analysisMetadata = (SearchResult)p;
		else
			throw new RuntimeException("Illegal attempt to set parent on DerivedSnapshot");
	}

	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Annotations.name();
	}

	@Override
	public String getLabel() {
		return analysisType;
	}

	@Override
	public void setId(String id) {
		analysisId = id;
	}

	@Override
	public Set<String> getKeys() {
		return keys.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return analysisId;
		else if (key.equals("tool"))
			return analysisTool;
		else if (key.equals("type"))
			return analysisType;
		else if (key.equals("parent"))
			return revId;
		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return keys.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getId() {
		return analysisId;
	}

	@Override
	public String getFriendlyName() {
		return analysisType;
	}

	@Override
	public List<String> getCreators() {
		return analysisMetadata == null ? null : analysisMetadata.getCreators();
	}

	@Override
	public String getContentDescriptor() {
		return analysisMetadata == null ? "" : analysisMetadata.getContentDescriptor();
	}

	@Override
	public Date getCreationTime() {
		return analysisMetadata == null ? null : analysisMetadata.getCreationTime();
	}

	@Override
	public double getRelevanceScore() {
		return analysisMetadata == null ? 0 : analysisMetadata.getRelevanceScore();
	}

	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		return analysisMetadata == null ? null : analysisMetadata.getUserPermissions();
	}

	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.EEG);
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return presenter;
//	}

//	public static void setPresenter(MetadataFormatter p) {
//		presenter = p;
//	}
//	
//	public static MetadataFormatter getDefaultPresenter() {
//		return presenter;
//	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

//	@Override
//	public MetadataDrillDown getDrillDown() {
//		return null;
//	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Dataset";
	}
}
