/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services.assembler;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.db.mefview.services.DataSnapshotAndTool;

public class DataSnapshotAndToolAssembler {

	final Function<DatasetAndTool, DataSnapshotAndTool> toDataSnapshotAndTool = new Function<DatasetAndTool, DataSnapshotAndTool>() {

		@Override
		public DataSnapshotAndTool apply(DatasetAndTool input) {
			return toDataSnapshotAndTool(input);
		}
	};

	public DataSnapshotAndTool toDataSnapshotAndTool(
			final DatasetAndTool dAndTool) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	public Set<DataSnapshotAndTool> toDataSnapshotAndToolSet(
			final Set<DatasetAndTool> datasetAndToolSet) {
		final Set<DataSnapshotAndTool> dataSnapshotAndToolSet = newHashSet(transform(
				datasetAndToolSet,
				toDataSnapshotAndTool));
		return dataSnapshotAndToolSet;
	}
}
