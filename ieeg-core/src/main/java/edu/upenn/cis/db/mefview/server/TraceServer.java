/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singletonList;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.zip.GZIPOutputStream;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IeegException;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.IHasStringId;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.braintrust.webapp.IeegFilter;
import edu.upenn.cis.db.habitat.persistence.mapping.AnnotationAssembler;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.Decimate;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFDataAndPageNos;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFPageServerS3;
import edu.upenn.cis.db.mefview.server.mefpageservers3.TooMuchDataRequestedException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.Downsample;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.ScaledTimeSeriesSegment;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegment;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.eeg.maf.MAFReader;

/**
 * Core class for handling requests for trace data and header info
 * 
 * TODO: generalize this. We should have a factory for each format, with a map
 * from type descriptor to each server. Perhaps initially there will only be MEF
 * and EDF. We should have a facility for having the metadata in a separate
 * place, with the DB being the first such place.
 * 
 * @author zives
 */
public class TraceServer {
	static ITimeSeriesPageServer server;
	private String sourcePath;

	private final IDataSnapshotServer dsServer = DataSnapshotServerFactory
			.getDataSnapshotServer();
	Integer remaining = -1;

	private final boolean usingS3;
	private final boolean testMode;
	private final static Logger logger = LoggerFactory
			.getLogger(TraceServer.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ TraceServer.class);

	TraceServer() {
		Map<String, String> ivProps = IvProps.getIvProps();
		this.sourcePath = BtUtil.get(
				ivProps,
				IvProps.SOURCEPATH,
				IvProps.SOURCEPATH_DFLT);
		boolean prefetch = BtUtil.getBoolean(
				ivProps,
				IvProps.PREFETCH,
				IvProps.PREFETCH_DFLT);
		int traces = BtUtil.getInt(
				ivProps,
				IvProps.THREADS,
				IvProps.THREADS_DFLT);
		int pages = BtUtil.getInt(
				ivProps,
				IvProps.PAGES,
				MultithreadedPageServer.DEFAULT_CACHE);
		boolean lazyIndex = BtUtil.getBoolean(
				ivProps,
				IvProps.LAZY_INDEX,
				IvProps.LAZY_INDEX_DFLT);
		this.usingS3 = IvProps.isUsingS3();
		this.testMode = BtUtil.getBoolean(
				ivProps,
				IvProps.DATASET_TEST,
				IvProps.DATASET_TEST_DFLT);

		if (this.usingS3) {
			server = new MEFPageServerS3();
		} else {
			server = new MEFPageServer(
					dsServer,
					sourcePath,
					traces,
					prefetch,
					lazyIndex,
					pages);
		}
	}

	public List<String> getPageServerState() {
		ArrayList<String> ret = new ArrayList<String>();

		// ret.add(" *** Paths (" + paths.size() + ") ***");
		// for (String p : paths.keySet())
		// ret.add(p);

		ret.addAll(server.getPageServerState());

		return ret;
	}

	public void closeDown() {
		server.shutdown();
		server = null;
		// paths.clear();
		MEFIndexer.close();
	}

	public ITimeSeriesPageServer getMEFServer() {
		return server;
	}

	private void validateFilterSpec(FilterSpec fs, double freq)
			throws IllegalArgumentException {

		if ((fs.includesFilterType(FilterSpec.LOWPASS_FILTER) || fs
				.includesFilterType(FilterSpec.BANDPASS_FILTER))
				&& fs.getBandpassHighCutoff() > freq / 2) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Low-pass cutoff of "
							+ fs.getBandpassHighCutoff()
							+ " with a sampling frequency of "
							+ freq
							+ ". Low-pass cutoff must be below 1/2 of sampling frequency");
		}
		if ((fs.includesFilterType(FilterSpec.HIGHPASS_FILTER) || fs
				.includesFilterType(FilterSpec.BANDPASS_FILTER))
				&& fs.getBandpassLowCutoff() <= 0) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Cutoff frequency for high-pass or band-pass filter must be above zero");
		}
		if ((fs.includesFilterType(FilterSpec.LOWPASS_FILTER) || fs
				.includesFilterType(FilterSpec.BANDPASS_FILTER))
				&& fs.getBandpassHighCutoff() <= 0) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Low-pass cutoff must be above zero");
		}
		if (fs.includesFilterType(FilterSpec.BANDPASS_FILTER)
				&& fs.getBandpassLowCutoff() >= fs.getBandpassHighCutoff()) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Bandpass lower and upper cutoff frequencies are swapped");
		}

		if (fs.includesFilterType(FilterSpec.BANDSTOP_FILTER)
				&& (fs.getBandstopHighCutoff() > freq / 2 || fs
						.getBandstopLowCutoff() > freq / 2)) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Bandstop frequencies must be below 1/2 of sampling frequency");
		}
		if (fs.includesFilterType(FilterSpec.BANDSTOP_FILTER)
				&& (fs.getBandstopLowCutoff() <= 0 || fs
						.getBandstopHighCutoff() < 0)) {
			throw new IllegalArgumentException(
					"Illegal filter setting: Cutoff frequency for bandstop filter must be above zero");
		}

		if (fs.getNumPoles() < 0 || fs.getNumPoles() > 9) {
			throw new IllegalArgumentException(
					"Number of Butterworth filter poles must be nonnegative and no more than 9");
		}
	}

	public List<ChannelSpecifier> getChannelsFromSegments(User user, String datasetID,
			List<INamedTimeSegment> traceids, double samplingPeriod)
			throws AuthorizationException {
		List<ChannelSpecifier> specifiers = 
				getChannels(user, datasetID, DataMarshalling.getTimeSegmentIds(traceids), samplingPeriod);
		
		for (int i = 0; i < traceids.size(); i++) {
			if (traceids.get(i).isEventBased()) {
				specifiers.set(i, specifiers.get(i).clone());
				specifiers.get(i).setEvents(true);
			}
			specifiers.get(i).setSegment(traceids.get(i));
		}
		
		return specifiers;
	}

	public List<ChannelSpecifier> getChannels(User user, String datasetID,
			List<String> traceids, double samplingPeriod)
			throws AuthorizationException {
		List<ChannelSpecifier> tracefileIds = new ArrayList<ChannelSpecifier>(
				traceids.size());

		SnapshotSpecifier snap = getSnapshotSpecifier(user, datasetID);

		Map<String, ChannelSpecifier> traceIds2ChSpecs = server
				.getChannelSpecifiers(user, snap, newHashSet(traceids),
						samplingPeriod);

		for (String traceId : traceids) {
			tracefileIds.add(traceIds2ChSpecs.get(traceId));
		}

		return tracefileIds;
	}

	/**
	 * Fetch an XREDE source file and read out the sources. Register the
	 * study/trace names and open readers to each.
	 * 
	 * @throws AuthorizationException
	 * @throws ServerTimeoutException TODO
	 */
	public List<TimeSeriesData> getTimeSeries(User user,
			final ChannelSpecifier seriesName, double start, double duration,
			double frequency,
			final FilterSpec filters,
			SignalProcessingStep processing,
			SessionToken sessionToken) throws AuthorizationException,
			IllegalArgumentException, ServerTimeoutException {
		// String tracePath = server.getTracePath(user, dataset, seriesName);

		double freq = getSampleRate(seriesName);// sourcePath + tracePath);
		validateFilterSpec(filters, freq);

		FilterManager.FilterBuffer buf = new FilterManager.FilterBuffer(
				(int) (duration * frequency));
		return getTimeSeries(user, seriesName, start, duration,
				frequency, filters, buf, processing, sessionToken);
	}

	public List<TimeSeriesData> getTimeSeries(
			User user,
			ChannelSpecifier tracePath,
			double start,
			double duration,
			double frequency,
			final FilterSpec filters,
			FilterManager.FilterBuffer buf,
			SignalProcessingStep processing,
			SessionToken sessionToken) throws ServerTimeoutException {
		final String M = "getTimeSeries()";
		if (start == 0)
			start = getZero(tracePath);// sourcePath + tracePath);
		logger.trace(M + ": Handling request for " + tracePath + " index "
				+ (new Date((long) (start / 1000))).toString() + "." +
				(int) (start % 1000000) + "(" + start + ") for " + duration
				+ " usec");
		try {
			Decimate ds = new Decimate(frequency, filters, buf, false);

			List<TimeSeriesData> rv = server.requestRead(
					tracePath,
					(long) start, (long) (start + duration), ds, user,
					sessionToken);

			// TODO: add signal processing step

			return rv;// ret;
		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (IOException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}
	}

	public List<List<TimeSeriesData>> getTimeSeriesSet(
			User user,
			IEventServer eventServer,
			IDataset dataset,
			final List<ChannelSpecifier> tracePaths,
			double start,
			double duration,
			double frequency,
			final List<FilterSpec> filters2,
			final SessionCacheEntry se,
			SignalProcessingStep processing,
			SessionToken sessionToken) throws ServerTimeoutException, IOException {
		final long in = System.nanoTime();
		if (duration <= 0)
			return new ArrayList<List<TimeSeriesData>>();

		final String M = "getTimeSeriesSet()";
		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Handling request for " + tracePaths + " index "
					+ (new Date((long) (start / 1000))).toString() + "." +
					(int) (start % 1000000) + "(" + start + ") for " + duration
					+ " usec");
		}
		
		List<ChannelSpecifier> sampleChannels = new ArrayList<ChannelSpecifier>();
		List<INamedTimeSegment> events = new ArrayList<INamedTimeSegment>();
		List<FilterSpec> filters = new ArrayList<FilterSpec>();
		List<Integer> channelMap = new ArrayList<Integer>();
		List<Integer> eventMap = new ArrayList<Integer>();
		for (int i = 0; i < tracePaths.size(); i++) {
			ChannelSpecifier ch = tracePaths.get(i);
			if (ch.isEvents()) {
				events.add(ch.getSegment());
				events.get(events.size()-1).setTimeShift(ch.getTimeOffset());
				eventMap.add(i);
//				System.out.println("Multichannel / should trigger event for " + ch.getLabel());
			} else {
				sampleChannels.add(ch);
				channelMap.add(i);
				
				if (filters2 != null)
					filters.add(filters2.get(i));
			}
		}

		PostProcessor[] proc = new PostProcessor[se == null || sampleChannels.size() < se.channelIDs.length ? sampleChannels.size() 
				: se.channelIDs.length];

		if (start == 0)
			start = getZero(sampleChannels.get(0));
		double createProcSec = -1;
		double multiChannelReadSec = -1;
		try {
			final long createProcStart = System.nanoTime();
			final List<Double> sampleRates = server.getSampleRates(sampleChannels);
			int i = 0;

			boolean isMinMax = false;
			if (se != null) {
				for (i = 0; i < se.channelInfo.length && i < sampleChannels.size(); i++) {
					FilterSpec fs = se.channelInfo[i].theFilter;
					if (se.channelInfo[i].isValidated) {
						proc[i] = se.channelInfo[i].processor;
						
						if (proc[i] instanceof Decimate)
							isMinMax |= ((Decimate)proc[i]).minMax;
					} else {
						validateFilterSpec(fs, sampleRates.get(i));
						se.channelInfo[i].isValidated = true;

						se.channelInfo[i].processor = new Decimate(frequency,
								se.channelInfo[i].theFilter, true);// filters.get(i));
						proc[i] = se.channelInfo[i].processor;
						isMinMax = true;
					}
				}
			} else
				for (FilterSpec fs : filters) {
					
					validateFilterSpec(fs, sampleRates.get(i).doubleValue());// getSampleRate(tracePaths.get(i)));
					proc[i] = new Decimate(frequency, filters.get(i), false);
					
					i++;
				}

			createProcSec = BtUtil.diffNowThenSeconds(createProcStart);

			final long multiChannelReadStart = System.nanoTime();
			List<List<TimeSeriesData>> rv;

			if (testMode) {
				rv = new ArrayList<List<TimeSeriesData>>();
				for (int j = 0; j < tracePaths.size() / 2; j++) {
					try {
						if (tracePaths.get(i).isEvents())
							System.out.println("Should trigger event for " + tracePaths.get(i).getLabel());
						
						rv.add(server.requestRead(tracePaths.get(j),
								(long) (start),
								(long) (start + duration),
								proc[j], user,
								sessionToken));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int j = 0; j < tracePaths.size() - (tracePaths.size() / 2); j++) {
					try {
						if (tracePaths.get(i).isEvents())
							System.out.println("Trace read / should trigger event for " + tracePaths.get(i).getLabel());

						rv.add(server.requestRead(tracePaths.get(j),
								(long) (start + 2000000.),
								(long) (start + 2000000. + duration),
								proc[j], user,
								sessionToken));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				List<List<TimeSeriesData>> rv1 = server
						.requestMultiChannelRead(
								sampleChannels,
								(long) start, (long) (start + duration),
								proc, user,
								sessionToken);
				
				double actualPeriod = 0;
				long actualStart = 0;
				for (List<TimeSeriesData> lsts : rv1) {
					if (!lsts.isEmpty()) {
						actualStart = lsts.get(0).getStartTime();
						actualPeriod = lsts.get(0).getPeriod();
					}
				}
				
				// TODO: cache this!!!
				
				String filename = dataset.getFriendlyName() + ".events";
				Set<RecordingObject> objects = dsServer
						.getRecordingObjects(user, new DataSnapshotId(dataset.getId()));

				IStoredObjectReference handle = null;
				for (RecordingObject o: objects) {
					if (o.getName().equals(filename)) {
						try {
							handle = dsServer.getRecordingObjectHandle(user, o);
						} catch (RecordingObjectNotFoundException e) {
							logger.error(M + ": Expected handle for " + filename + ".events RecordingObject not found.", e);
						}
					}
				}
				
				// Events -- note we may need to double to match minMax??
				List<List<TimeSeriesData>> rv2 = new ArrayList<List<TimeSeriesData>>();
				if (handle != null && !events.isEmpty()) {
					long startTime = events.get(0).getStartTime() - events.get(0).getTimeShift();
//					for (ChannelSpecifier ch: sampleChannels) {
//						if (ch.getSegment().getStartTime() < startTime)
//							startTime = ch.getSegment().getStartTime();
//					}

					System.out.println("Start events at " + startTime + ", offset " + actualStart + " for " + duration);
					Map<INamedTimeSegment, int[]> segs = eventServer.getEventVector(
							dataset, 
							handle, 
							events, 
							startTime,
							actualStart, 
							(actualStart + duration), actualPeriod, 
							!IvProps.getIvProps().containsKey("minMaxOff") && 
								isMinMax);
					
					for (INamedTimeSegment seg: segs.keySet()) {
						TimeSeriesData data = new TimeSeriesData((long)start, actualPeriod, 1, segs.get(seg));
						
						rv2.add(Lists.newArrayList(data));
					}
				}
				
				rv = new ArrayList<List<TimeSeriesData>>();
				for (int x = 0; x < rv1.size() + rv2.size(); x++)
					rv.add(new ArrayList<TimeSeriesData>());
				
				for (int x = 0; x < rv1.size(); x++)
					rv.set(channelMap.get(x), rv1.get(x));

				for (int x = 0; x < rv2.size(); x++)
					rv.set(eventMap.get(x), rv2.get(x));
			}
			multiChannelReadSec = BtUtil
					.diffNowThenSeconds(multiChannelReadStart);

			// TODO: processing step

			return rv;

		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (Exception e) {
			propagateIfInstanceOf(e, ServerTimeoutException.class);
			propagateIfInstanceOf(e, TooMuchDataRequestedException.class);
			logger.error(M + ": Caught, returning null", e);
			return null;
		} finally {
			timeLogger
					.trace("{}: createProc {} seconds, multiChannelRead {} seconds, {} seconds",
							new Object[] { M, createProcSec,
									multiChannelReadSec,
									BtUtil.diffNowThenSeconds(in) });
		}
	}

	public List<ScaledTimeSeriesSegment> getScaledTimeSeries(
			User user,
			final ChannelSpecifier tracePath,
			double start, double duration, double frequency,
			final FilterSpec filters,
			SignalProcessingStep processing,
			SessionToken sessionToken) throws ServerTimeoutException {

		FilterManager.FilterBuffer buf = new FilterManager.FilterBuffer(
				(int) (duration * frequency));
		List<TimeSeriesData> intSeries =
				// Series intSeries =
				getTimeSeries(user, tracePath, start, duration, frequency,
						filters,
						buf, processing, sessionToken);
		List<ScaledTimeSeriesSegment> ret = new ArrayList<ScaledTimeSeriesSegment>();

		// Rescale each segment by the voltage factor
		for (TimeSeriesData segment : intSeries) {// intSeries.getSegments()) {
			ScaledTimeSeriesSegment value = new ScaledTimeSeriesSegment(
					segment.getSegment());

			ret.add(value);
		}

		return ret;
	}

	public List<List<ScaledTimeSeriesSegment>> getScaledTimeSeriesSet(
			final List<ChannelSpecifier> tracePaths,
			// final List<String> revIds,
			double start, double duration,
			double frequency, final List<FilterSpec> filters, User user,
			SignalProcessingStep processing,
			SessionToken sessionToken) throws ServerTimeoutException, IOException {
		final String M = "getScaledTimeSeriesSet()";
		logger.trace(M + ": Handling request for " + tracePaths + " index "
				+ (new Date((long) (start / 1000))).toString() + "." +
				(int) (start % 1000000) + "(" + start + ") for " + duration
				+ " usec");
		try {
			PostProcessor[] proc = new PostProcessor[filters.size()];

			int i = 0;
			for (FilterSpec fs : filters) {
				validateFilterSpec(fs,
						getSampleRate(/* sourcePath + */tracePaths.get(i)));
				proc[i] = new Decimate(frequency, filters.get(i), false);
				i++;
			}
			List<List<TimeSeriesData>> intSeriesList =
					server.requestMultiChannelRead(
							tracePaths,
							(long) start,
							(long) (start + duration),
							proc,
							user,
							sessionToken);

			List<List<ScaledTimeSeriesSegment>> ret = new ArrayList<List<ScaledTimeSeriesSegment>>();

			for (List<TimeSeriesData> intSeries : intSeriesList) {
				List<ScaledTimeSeriesSegment> series = new ArrayList<ScaledTimeSeriesSegment>();
				// Rescale each segment by the voltage factor
				for (TimeSeriesData segment : intSeries) {// intSeries.getSegments())
															// {
					ScaledTimeSeriesSegment value = new ScaledTimeSeriesSegment(
							segment.getSegment());

					series.add(value);
				}
				ret.add(series);
			}

			// TODO: processing step

			return ret;
		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}
	}

	// //////////////////// EXACT

	public List<TimeSeriesData> getTimeSeriesRaw(
			ChannelSpecifier tracePath,
			double start,
			double duration,
			int scaleFactor,
			SessionToken sessionToken) throws ServerTimeoutException {
		final String M = "getTimeSeries()";

		// String sp = sourcePath + tracePath;
		ChannelSpecifier sp = tracePath;
		if (start == 0)
			start = getZero(sp);

		Downsample proc = new Downsample(getSampleRate(sp), scaleFactor);
		logger.trace(M + ": Handling request for " + tracePath + " index "
				+ (new Date((long) (start / 1000))).toString() + "." +
				(int) (start % 1000000) + "(" + start + ") for " + duration
				+ " usec");
		try {
			// ArrayList<UnscaledTimeSeriesSegment> ret = new
			// ArrayList<UnscaledTimeSeriesSegment>();
			List<TimeSeriesData> rv = server.requestRead(
					// sourcePath,
					tracePath,
					(long) start, (long) (start + duration), proc,
					null,
					sessionToken);

			// for (TimeSeriesData trace: rv)
			// ret.add(trace.getSegment());

			return rv;// ret;
		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (IOException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}
	}

	public List<MEFDataAndPageNos> getTimeSeriesSetRed(
			User user,
			List<? extends ChannelSpecifier>
			tracePaths,
			double start,
			double duration)
			throws StaleMEFException, ServerTimeoutException {
		return server.requestMultiChannelReadRed(
				tracePaths,
				(long) start,
				(long) (start + duration),
				user);
	}

	public InputStream getData(
			String key,
			@Nullable String eTag,
			@Nullable Long startByteOffset,
			@Nullable Long endByteOffset) {
		return server.getData(
				key,
				eTag,
				startByteOffset,
				endByteOffset);
	}

	public List<List<TimeSeriesData>> getTimeSeriesSetRaw(
			User user,
			final List<ChannelSpecifier> tracePaths,
			double start,
			double duration,
			int scaleFactor,
			SessionToken sessionToken) throws ServerTimeoutException, IOException {
		if (duration <= 0)
			return new ArrayList<List<TimeSeriesData>>();

		final String M = "getTimeSeriesSet()";
		logger.trace(M + ": Handling request for " + tracePaths + " index "
				+ (new Date((long) (start / 1000))).toString() + "." +
				(int) (start % 1000000) + "(" + start + ") for " + duration
				+ " usec");
		if (start == 0)
			start = getZero(/* sourcePath + */tracePaths.get(0));

		Downsample ds[] = new Downsample[tracePaths.size()];

		if (scaleFactor < 1)
			scaleFactor = 1;
		for (int i = 0; i < ds.length; i++)
			ds[i] = new Downsample(
					getSampleRate(/* sourcePath + */tracePaths.get(i)),
					scaleFactor);

		// double frequency = this.getSampleRate(/*sourcePath +*/
		// tracePaths.get(0)) / scaleFactor;
		try {
			List<List<TimeSeriesData>> rv = server
					.requestMultiChannelRead(// sourcePath,
							tracePaths,
							(long) start,
							(long) (start + duration),
							ds, user,
							sessionToken);

			return rv;

		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}
	}

	public List<ScaledTimeSeriesSegment> getScaledTimeSeriesRaw(
			final User user, // final String dataset,
			final ChannelSpecifier tracePath, double start,
			double duration,
			int scaleFactor,
			boolean minMax,
			SessionToken sessionToken) throws AuthorizationException,
			ServerTimeoutException {

		// String tracePath = server.getTracePath(user, dataset, seriesName);

		return getScaledTimeSeriesRaw(tracePath, start, duration, scaleFactor,
				minMax, sessionToken);
	}

	public List<ScaledTimeSeriesSegment> getScaledTimeSeriesRaw(
			final ChannelSpecifier tracePath,
			double start, double duration, int scaleFactor,
			boolean minMax,
			SessionToken sessionToken) throws ServerTimeoutException {

		List<TimeSeriesData> intSeries =
				getTimeSeriesRaw(tracePath, start, duration, scaleFactor,
						sessionToken);
		List<ScaledTimeSeriesSegment> ret = new ArrayList<ScaledTimeSeriesSegment>();

		// Rescale each segment by the voltage factor
		for (TimeSeriesData segment : intSeries) {// intSeries.getSegments()) {
			ScaledTimeSeriesSegment value = new ScaledTimeSeriesSegment(
					segment.getSegment());

			ret.add(value);
		}

		return ret;
	}

	public List<List<ScaledTimeSeriesSegment>> getScaledTimeSeriesSetRaw(
			final List<ChannelSpecifier> tracePaths,
			// final List<String> revIds,
			double start,
			double duration,
			int scaleFactor,
			boolean minMax,
			SessionToken sessionToken) throws ServerTimeoutException, IOException {
		final String M = "getScaledTimeSeriesSet()";
		logger.trace(M + ": Handling request for " + tracePaths + " index "
				+ (new Date((long) (start / 1000))).toString() + "." +
				(int) (start % 1000000) + "(" + start + ") for " + duration
				+ " usec");
		try {
			Downsample[] ds = new Downsample[tracePaths.size()];
			for (int i = 0; i < ds.length; i++)
				ds[i] = new Downsample(
						getSampleRate(/* sourcePath + */tracePaths.get(i)),
						scaleFactor);
			List<List<TimeSeriesData>> intSeriesList =
					server.requestMultiChannelRead(// sourcePath,
							tracePaths,
							// revIds,
							(long) start, (long) (start + duration),
							ds, null,
							sessionToken);

			List<List<ScaledTimeSeriesSegment>> ret = new ArrayList<List<ScaledTimeSeriesSegment>>();

			for (List<TimeSeriesData> intSeries : intSeriesList) {
				List<ScaledTimeSeriesSegment> series = new ArrayList<ScaledTimeSeriesSegment>();
				// Rescale each segment by the voltage factor
				for (TimeSeriesData segment : intSeries) {// intSeries.getSegments())
															// {
					ScaledTimeSeriesSegment value = new ScaledTimeSeriesSegment(
							segment.getSegment());

					series.add(value);
				}
				ret.add(series);
			}

			return ret;
		} catch (InterruptedException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		} catch (ExecutionException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}
	}

	// public ArrayList<ArrayList<ScaledTimeSeriesSegment>>
	// getScaledTimeSeriesSetRaw(
	// final User user, final String dataset,
	// final List<String> seriesNames, double start, double duration,
	// int scaleFactor) throws AuthorizationException {
	//
	// List<String> tracePaths = new ArrayList<String>();
	// for (String seriesName : seriesNames)
	// tracePaths.add(server.getTracePath(user, dataset, seriesName));
	//
	// return getScaledTimeSeriesSetRaw(tracePaths, seriesNames, start,
	// duration,
	// scaleFactor);
	// }

	// //////////////////// END EXACT

	/**
	 * Returns the total duration of a file (in usec)
	 * 
	 * @throws AuthorizationException
	 * @throws IOException
	 */
	public Double getDuration(User user, String dataset, String trace)
			throws IllegalArgumentException, AuthorizationException,
			IOException {
		// String tracePath = server.getTracePath(user, dataset, trace);
		ChannelSpecifier tracePath = getChannelSpecifier(user,
				getSnapshotSpecifier(user, dataset), trace, 0);

		return getDuration(tracePath);
	}

	public SnapshotSpecifier getSnapshotSpecifier(User user, String revID)
			throws AuthorizationException {
		return server.getSnapshotSpecifier(user, revID);
	}

	public ChannelSpecifier getChannelSpecifier(User user,
			SnapshotSpecifier snapshot, String revID,
			double samplingPeriod) throws AuthorizationException {
		return getChannelSpecifiers(user, snapshot, Collections.singleton(revID), samplingPeriod).get(revID);
	}
	
	public Map<String, ChannelSpecifier> getChannelSpecifiers(User user,
			SnapshotSpecifier snapshot, Set<String> revIds,
			double samplingPeriod) throws AuthorizationException {
		if (snapshot instanceof MEFSnapshotSpecifier) {
			return server.getChannelSpecifiers(user, snapshot, revIds,
					samplingPeriod);
		} else {
			throw new RuntimeException("Unsupported snapshot type!");
		}
	}

	public Double getDuration(ChannelSpecifier tracePath)
			throws IllegalArgumentException, IOException {
		return Double.valueOf(server.getEndUutc(tracePath)
				- server.getStartUutc(tracePath));
	}

	public int getMinSampleValue(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getMinSampleValue(tracePath);
	}

	public int getMaxSampleValue(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getMaxSampleValue(tracePath);
	}

	public long getNumberOfSamples(ChannelSpecifier tracePath) {
		return server.getNumSamples(tracePath);
	}

	public String getInstitution(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getInstitution(tracePath);
	}

	public String getAcquisitionSystem(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getAcquisitionSystem(tracePath);
	}

	public String getChannelName(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getChannelName(tracePath);
	}

	public String getDataSignature(ChannelSpecifier chSpec) {
		return server.getDataVersion(chSpec);
	}

	public String getChannelComments(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getChannelComments(tracePath);
	}

	public String getSubjectId(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getSubjectId(tracePath);
	}

	/**
	 * Returns start UUTC time for trace
	 * 
	 * @throws AuthorizationException
	 */
	// public long getStartDate(User user, String dataset, String trace)
	// throws IllegalArgumentException, AuthorizationException {
	// String tracePath = server.getTracePath(user, dataset, trace);
	// return getStartDate(tracePath);
	// }

	public long getZero(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return 0;// server.getStartTime(tracePath);
	}

	public long getStartUutc(ChannelSpecifier chSpec) {
		return server.getStartUutc(chSpec);
	}

	public long getEndUutc(ChannelSpecifier chSpec) {
		return server.getEndUutc(chSpec);
	}

	/**
	 * Returns sampling rate (in usec) for trace
	 * 
	 * @throws AuthorizationException
	 */
	// public double getSampleRate(User user, ChannelSpecifier tracePath)
	// throws IllegalArgumentException, AuthorizationException {
	// String tracePath = server.getTracePath(user, dataset, trace);
	// return getSampleRate(tracePath);
	// }

	public double getSampleRate(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return server.getSampleRate(tracePath);
	}

	public List<Double> getSampleRates(List<ChannelSpecifier> chSpecs) {
		return server.getSampleRates(chSpecs);
	}

	/**
	 * Returns the voltage conversion factor
	 * 
	 * @param trace
	 * @return
	 * @throws IllegalArgumentException
	 * @throws AuthorizationException
	 */
	public double getVoltageConversionFactor(User user,
			SnapshotSpecifier dataset,
			String trace)
			throws IllegalArgumentException, AuthorizationException {
		ChannelSpecifier tracePath = server.getChannelSpecifier(user, dataset,
				trace, 0);
		// server.getTracePath(user, dataset, trace);
		return getVoltageConversionFactor(tracePath);
	}

	public double getVoltageConversionFactor(ChannelSpecifier tracePath)
			throws IllegalArgumentException {
		return (double) server.getVoltageConversionFactor(tracePath);
	}

	// //////////////////// Annotations

	/**
	 * Returns a map of events: time --> (Type, Channel)
	 */
	public SortedMap<Double, HashMap<String, Set<String>>> getEvents(
			String annotationFile,
			String type) throws IllegalArgumentException {
		return MAFReader.getEvents(annotationFile, type);
	}

	public DataSnapshotIds saveAnnotations(User uid, String datasetID,
			Collection<Annotation> annotations)
			throws
			AuthorizationException,
			TimeSeriesNotFoundInDatasetException, BadTsAnnotationTimeException {

		logger.trace("Saving " + annotations.size()
				+ " annotations on dataset " + datasetID);
		Set<TsAnnotationDto> tsAnnotations = newHashSet();

		for (Annotation a : annotations) {
			TsAnnotationDto tsa = AnnotationAssembler.toTsAnnotationDto(a);
			logger.trace("Saving " + a.getType() + " from " + a.getStart()
					+ "-" + a.getEnd());
			// System.out.println("Saving " + tsa);
			tsAnnotations.add(tsa);
		}

		DataSnapshotIds dsAndTsAnnIds;

		dsAndTsAnnIds = dsServer
				.storeTsAnnotations(uid, datasetID, tsAnnotations);

		return dsAndTsAnnIds;
	}

	public String removeAnnotations(User user, String datasetID,
			Set<Annotation> annotations) {

		final Set<String> annotationRevIds = newHashSet();
		for (Annotation a : annotations) {
			for (int i = 0; i < a.getChannels().size(); i++) {
				logger.trace("Removing " + a.getType() + " from "
						+ a.getStart() + "-" + a.getEnd());
				annotationRevIds.add(a.getRevId());
			}
		}
		datasetID = dsServer.removeTsAnnotations(user, datasetID,
				annotationRevIds);

		return datasetID;
	}

	public int removeAnnotationsByLayer(User user, String datasetID,
	    Set<String> annotationLayersToRemove){
	  
	  int removedItems = 0;
	  int aux;
	  for (String l : annotationLayersToRemove) { 
	    aux = dsServer.removeTsAnnotationsByLayer(user, datasetID, l);
	    removedItems+=aux;
	  }
	  return removedItems;
	  
	}
	
	
	public String saveTimeSeriesAnnotationDtos(User user, String datasetID,
			Set<TsAnnotationDto> annotations)
			throws
			AuthorizationException, TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException {
		datasetID = dsServer.storeTsAnnotations(user, datasetID, annotations)
				.getDsRevId();

		return datasetID;
	}

	public String removeTimeSeriesAnnotationDtos(User user, String datasetID,
			Set<TsAnnotationDto> annotations)
			throws IllegalArgumentException,
			AuthorizationException {
		final Set<String> annotationRevIds =
				newHashSet(
				transform(annotations, IHasStringId.getId));
		datasetID = dsServer.removeTsAnnotations(user, datasetID,
				annotationRevIds);
		return datasetID;
	}

	public InputStream getAnnotationsCsv(final User user,
			final String dataSnapshotID) throws IOException,
			FileNotFoundException, AuthorizationException {
		DataSnapshot dss = dsServer.getDataSnapshot(user,
				dataSnapshotID);

		if (dss == null) {
			throw new IllegalArgumentException("DataSnapshot "
					+ dataSnapshotID + " does not exist");
		}

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final PrintWriter csv = new PrintWriter(new OutputStreamWriter(out));

		csv.println("Type,Start,End,Description,Channel,ChannelID,AnnotationID,Creator");
		for (TsAnnotationDto ann : dss.getTsAnnotations()) {
			final String annRevId = ann.getId();
			final String type = ann.getType();
			final Long startTime = ann.getStartOffsetMicros();
			final Long endTime = ann.getEndOffsetMicros();
			final String description = ann.getDescription() != null ? ann
					.getDescription() : "";
			final String creator = ann.getAnnotator();
			for (final TimeSeriesDto tsDto : ann.getAnnotated()) {
				csv.println(type
						+ ","
						+ startTime
						+ ","
						+ endTime
						+ ","
						+ description
						+ ","
						+ tsDto.getLabel()
						+ ","
						+ tsDto.getId()
						+ ","
						+ annRevId
						+ ","
						+ creator);
			}
		}
		csv.flush();
		csv.close();

		final byte[] csvData = out.toByteArray();

		return new ByteArrayInputStream(csvData);
	}

	public InputStream getAnnotationsJson(final User user,
			final String dataSnapshotID) throws IOException,
			FileNotFoundException, AuthorizationException {
		DataSnapshot dss = dsServer.getDataSnapshot(user,
				dataSnapshotID);

		if (dss == null) {
			throw new IllegalArgumentException("DataSnapshot "
					+ dataSnapshotID + " does not exist");
		}

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final PrintWriter json = new PrintWriter(new OutputStreamWriter(out));

		// csv.println("Type,Start,End,Description,Channels");
		json.print('[');
		boolean first = true;
		for (TsAnnotationDto ann : dss.getTsAnnotations()) {
			if (first)
				first = false;
			else
				json.print(',');
			final String type = ann.getType();
			final Long startTimeUutc = ann.getStartOffsetMicros();
			final Long endTimeUutc = ann.getEndOffsetMicros();
			final String description = ann.getDescription() != null ? ann
					.getDescription() : "";
			final String annRevId = ann.getId();
			final String creator = ann.getAnnotator();
			for (final TimeSeriesDto tsDto : ann.getAnnotated()) {
				json.print("{'type': '"
						+ type
						+ "', 'start': "
						+ startTimeUutc
						+ ", 'end':"
						+ endTimeUutc
						+ ", 'description:' '"
						+ description
						+ "' , 'label': '"
						+ tsDto.getLabel()
						+ "', 'revID': '"
						+ tsDto.getId()
						+ "', 'annotationRevID': '"
						+ annRevId
						+ "', 'creator': '"
						+ creator
						+ "'}");
			}
		}
		json.flush();
		json.close();

		final byte[] csvData = out.toByteArray();

		return new ByteArrayInputStream(csvData);
	}

	private ListeningExecutorService downloadThreadPool =
			MoreExecutors.listeningDecorator(
					Executors.newFixedThreadPool(10));

	public InputStream getTracesCSV(final User user, String dataSnapshot,
			IEventServer eventServer,
			final List<String> revIds, final long start, final long duration,
			final double frequency,
			DisplayConfiguration fp,
			Date timeOfRequest,
			long timeOfRequestNanos,
			HttpServletRequest httpServletRequest)
			throws AuthorizationException {
		return getTracesCSV(user, dataSnapshot, eventServer, revIds, start, duration,
				frequency,
				fp, timeOfRequest, timeOfRequestNanos, httpServletRequest, null);
	}

	public InputStream getTracesCSV(final User user, 
			String dataSnapshot,
			final IEventServer eventServer,
			final List<String> revIds, final long start, final long duration,
			final double frequency,
			DisplayConfiguration fp,
			Date timeOfRequest,
			long timeOfRequestNanos,
			final HttpServletRequest httpServletRequest,
			final SignalProcessingStep processing)
			throws AuthorizationException {
		final String m = "getTracesCSV(...)";
		try {
			DataSnapshot snap = dsServer.getDataSnapshot(user, dataSnapshot);
			final DataSnapshotSearchResult dataset = dsServer.getDataSnapshotForId(user, dataSnapshot);
			Set<TimeSeriesDto> tss = snap.getTimeSeries();

			SnapshotSpecifier ss = this
					.getSnapshotSpecifier(user, dataSnapshot);

			final List<String> channelNames = new ArrayList<String>(
					revIds.size());
			final List<ChannelSpecifier> channelSpecs = new ArrayList<ChannelSpecifier>(
					revIds.size());
			for (String revId : revIds) {
				for (TimeSeriesDto ts : tss)
					if (ts.getId().equals(revId)) {
						channelNames.add(ts.getLabel());
						channelSpecs.add(this.getChannelSpecifier(user, ss,
								revId,
								1.E6 / frequency));
					}
			}

			final List<FilterSpec> filters = new ArrayList<FilterSpec>();

			FilterSpec filt = getFilterFrom(fp);
			for (@SuppressWarnings("unused")
			String str : revIds) {
				filters.add(filt);
			}

			final PipedOutputStream out = new PipedOutputStream();

			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.CSV_DOWNLOAD,
							dataSnapshot,
							start,
							duration);
			IeegFilter.setUserAndDataUsage(httpServletRequest, user,
					dataUsage);

			// Don't close this - client is responsible for closing it
			InputStream in = new PipedInputStream(out);

			final SessionToken downloadSessionToken = new SessionToken(
					httpServletRequest.getSession().getId());

			downloadThreadPool.execute(new Runnable() {
				public void run() {
					PrintWriter csv = null;
					try {
						GZIPOutputStream gos = new GZIPOutputStream(out);
						csv = new PrintWriter(gos);
						String value = "Channel Label,Channel ID,Start,Duration,Data...";
						csv.println(value);
						int ind = 0;
						for (ChannelSpecifier chSpec : channelSpecs) {
							final List<List<TimeSeriesData>> mefMatrix = getTimeSeriesSet(
									user, eventServer, dataset, singletonList(chSpec),
									start, duration, frequency,
									singletonList(filters.get(ind)), null,
									processing,
									// non-perfect: should be a real session
									// token
									// passed in from the web services - maybe
									downloadSessionToken);

							List<TimeSeriesData> mefList = getOnlyElement(mefMatrix);

							final List<UnscaledTimeSeriesSegment> segList = new ArrayList<UnscaledTimeSeriesSegment>();
							for (final TimeSeriesData mef : mefList) {
								segList.add(mef.getSegment());
							}

							value = channelNames.get(ind) + ","
									+ revIds.get(ind++)
									+ ","
									+ start + "," + duration;
							csv.print(value);
							for (UnscaledTimeSeriesSegment seg : segList) {
								int[] var = seg.getSeries();
								for (int i = 0; i < var.length; i++) {
									csv.print(",");
									if (seg.isInGap(i)) {
										csv.print("-");
									} else {
										value = String.valueOf(var[i]);
										csv.print(value);
									}
								}
							}
							csv.println();
						}
					} catch (Exception e) {
						logger.error(m + ": caught exception", e);
					} finally {
						BtUtil.close(csv);
					}
				}
			});

			return in;
		} catch (Exception e) {
			Throwables.propagateIfPossible(e, AuthorizationException.class);
			throw Throwables.propagate(e);
		}
	}

	public InputStream getTracesJSON(User user, String dataSnapshot,
			List<String> channels, long start, long duration, double frequency,
			DisplayConfiguration fp) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getDataSnapshotId(final User user, final String dsName) {
		return dsServer.getDataSnapshotId(user, dsName, false);
	}

	public InputStream getTracesRawCSV(final User user, String dataSnapshot,
			final List<String> revIds, final long start, final long duration,
			final int sampleFactor,
			Date timeOfRequest,
			long timeOfRequestNanos,
			final HttpServletRequest httpServletRequest)
			throws AuthorizationException {
		final String m = "getTracesRawCSV(...)";
		try {
			DataSnapshot snap = dsServer.getDataSnapshot(user, dataSnapshot);
			Set<TimeSeriesDto> tss = snap.getTimeSeries();
			SnapshotSpecifier ss = this
					.getSnapshotSpecifier(user, dataSnapshot);

			final List<String> channelNames = new ArrayList<String>(
					revIds.size());
			final List<ChannelSpecifier> channelSpecs = new ArrayList<ChannelSpecifier>(
					revIds.size());
			for (String revId : revIds) {
				for (TimeSeriesDto ts : tss)
					if (ts.getId().equals(revId)) {
						channelNames.add(ts.getLabel());
						channelSpecs.add(this.getChannelSpecifier(user, ss,
								revId,
								-sampleFactor));
					}
			}

			final PipedOutputStream out = new PipedOutputStream();

			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.CSV_DOWNLOAD_RAW,
							dataSnapshot,
							start,
							duration);
			IeegFilter.setUserAndDataUsage(httpServletRequest, user,
					dataUsage);

			// Don't close this - client is responsible for closing it
			InputStream in = new PipedInputStream(out);
			final SessionToken downloadSessionToken = new SessionToken(
					httpServletRequest.getSession().getId());

			downloadThreadPool.execute(new Runnable() {
				public void run() {
					PrintWriter csv = null;
					try {
						GZIPOutputStream gos = new GZIPOutputStream(out);
						csv = new PrintWriter(gos);

						csv.println("Channel Label,Channel ID,Start,Duration,Data...");

						int ind = 0;
						for (ChannelSpecifier chSpec : channelSpecs) {

							List<List<TimeSeriesData>> mefMatrix =
									getTimeSeriesSetRaw(
											user,
											singletonList(chSpec),
											start, duration, sampleFactor,
											downloadSessionToken);
							List<TimeSeriesData> mefList =
									getOnlyElement(mefMatrix);
							final ArrayList<UnscaledTimeSeriesSegment> segList = new ArrayList<UnscaledTimeSeriesSegment>();
							for (final TimeSeriesData mef : mefList) {
								segList.add(mef.getSegment());
							}

							csv.print(channelNames.get(ind) + ","
									+ revIds.get(ind++) + ","
									+ start + "," + duration);

							for (UnscaledTimeSeriesSegment seg : segList) {
								int[] var = seg.getSeries();
								for (int i = 0; i < var.length; i++) {
									csv.print(",");
									if (seg.isInGap(i))
										csv.print("-");
									else
										csv.print(var[i]);
								}
							}
							csv.println();
						}
					} catch (Exception e) {
						logger.error(m + ": caught exception", e);
					} finally {
						BtUtil.close(csv);
					}
				}
			});
			return in;
		} catch (Exception e) {
			Throwables.propagateIfPossible(e, AuthorizationException.class);
			throw Throwables.propagate(e);
		}
	}

	public InputStream getTracesRawJSON(User user, String dataSnapshot,
			List<String> channels, long start, long duration, int sampleFactor) {
		// TODO Auto-generated method stub
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final PrintWriter json = new PrintWriter(new OutputStreamWriter(out));
		return null;
	}

	public InputStream getZip(String zipPath) throws IOException,
			FileNotFoundException {
		InputStream bigInputStream;
		if (usingS3) {
			S3Object s3Object = AwsUtil.requestS3Object(
					AwsUtil.getS3(),
					AwsUtil.createGetObjectRequest(
							IvProps.getDataBucket(),
							zipPath),
					null);
			bigInputStream = s3Object.getObjectContent();
		} else {
			bigInputStream =
					new BufferedInputStream(new FileInputStream(sourcePath
							+ zipPath));
		}
		return bigInputStream;
	}

	public void indexDataset(final User user, final String dataSnapshotID,
			double sampleRate,
			double pageLength) {
		throw new UnsupportedOperationException("not implemneted");
		// try {
		//
		// if (user.getRoles().contains(Role.ADMIN)) {
		//
		// MEFIndexer mei = new MEFIndexer(sourcePath, server);
		//
		// DataSnapshot dss = ds.getDataSnapshot(user,
		// dataSnapshotID);
		//
		// List<String> tracePaths = new ArrayList<String>();
		//
		// for (TimeSeriesDto series : dss.getTimeSeries()) {
		// tracePaths.add(server.getTracePath(user, dataSnapshotID,
		// series.getId()));
		// }
		//
		// mei.run(dataSnapshotID, tracePaths, sampleRate, pageLength);
		// } else
		// throw new UnauthorizedException("Only admins may create indices!");
		// } catch (FileNotFoundException e) {
		// throw new UnauthorizedException(e.getMessage());
		// } catch (AuthorizationException e) {
		// throw new UnauthorizedException(e.getMessage());
		// } catch (IOException e) {
		// throw new UnauthorizedException(e.getMessage());
		// } catch (InterruptedException e) {
		// throw new UnauthorizedException(e.getMessage());
		// } catch (ExecutionException e) {
		// throw new UnauthorizedException(e.getMessage());
		// }
	}

	public static List<FilterSpec> getFiltersFrom(List<DisplayConfiguration> fp) {
		List<FilterSpec> ret = new ArrayList<FilterSpec>();
		for (DisplayConfiguration f : fp) {
			ret.add(getFilterFrom(f));
		}
		return ret;
	}

	public static List<DisplayConfiguration> getFiltersFromSpec(List<FilterSpec> fs) {
		List<DisplayConfiguration> ret = new ArrayList<DisplayConfiguration>();
		for (FilterSpec f : fs) {
			ret.add(getFilterFrom(f));
		}
		return ret;
	}

	public static FilterSpec getFilterFrom(DisplayConfiguration fp) {
		FilterSpec ret = new FilterSpec();

		ret.setBandpassHighCutoff(fp.getBandpassHighCutoff());
		ret.setBandpassLowCutoff(fp.getBandpassLowCutoff());
		ret.setBandstopHighCutoff(fp.getBandstopHighCutoff());
		ret.setBandstopLowCutoff(fp.getBandstopLowCutoff());
		ret.setFilterType(fp.getFilterType());
		ret.setNumPoles(fp.getNumPoles());
		ret.setFilterName(fp.getFilterName());

		return ret;
	}

	public static DisplayConfiguration getFilterFrom(FilterSpec fp) {
		DisplayConfiguration ret = new DisplayConfiguration();

		ret.setBandpassHighCutoff(fp.getBandpassHighCutoff());
		ret.setBandpassLowCutoff(fp.getBandpassLowCutoff());
		ret.setBandstopHighCutoff(fp.getBandstopHighCutoff());
		ret.setBandstopLowCutoff(fp.getBandstopLowCutoff());
		ret.setFilterType(fp.getFilterType());
		ret.setNumPoles(fp.getNumPoles());

		return ret;
	}

	public List<ChannelInfoDto> getTimeSeriesChannelInfo(
			List<TimeSeriesDto> series,
			List<ChannelSpecifier> paths) throws IllegalArgumentException,
			IOException {
		List<ChannelInfoDto> channelList = new ArrayList<ChannelInfoDto>();
		int i = 0;
		for (TimeSeriesDto ts : series) {
			ChannelSpecifier chSpec = paths.get(i++);

			logger.trace("Scanning trace " + chSpec);
			// String p = sourcePath + tracePath;
			channelList.add(new ChannelInfoDto(ts.getId(), (Long) null,
					server.getInstitution(chSpec),
					null,// server.getHeaderLength(tracePath),
					null,// server.getSubjectFirstName(tracePath),
					null,// server.getSubjectSecondName(tracePath),
					null,// server.getSubjectThirdName(tracePath),
					null,// server.getSubjectId(tracePath),
					null,// server.getNumberOfSamples(tracePath),
					server.getChannelName(chSpec),
					new Long(getZero(chSpec)),
					new Long(getDuration(chSpec).longValue()),
					new Double(server.getSampleRate(chSpec)),
					null,// server.getLowFrequencyFilterSetting(tracePath),
					null,// server.getHighFrequencyFilterSetting(tracePath),
					null,// server.getNotchFilterFrequency(tracePath),
					server.getAcquisitionSystem(chSpec),
					server.getChannelComments(chSpec),
					null,// server.getStudyComments(tracePath),
					new Integer(i - 1),// server.getPhysicalChannelNumber(tracePath),
					null,// server.getMaximumBlockLength(tracePath),
					null,// server.getBlockInterval(tracePath),
					null,// server.getBlockHeaderLength(tracePath),
					new Integer(server.getMaxSampleValue(chSpec)),
					new Integer(server.getMinSampleValue(chSpec)),
					new Double(server.getVoltageConversionFactor(chSpec)),
					null));
		}
		return channelList;
	}

	/**
	 * Returns the size of the channel file in bytes if available.
	 * 
	 * @param channelSpecifier
	 * @return the size of the channel file in bytes if available.
	 * 
	 */
	public long getSizeBytes(ChannelSpecifier channelSpecifier) {
		return server.getSizeBytes(channelSpecifier);
	}

	public ControlFileRegistration createRegistration(User user, ControlFileRegistration registration) 
			throws AuthorizationException, DuplicateNameException {
		return dsServer.createControlFileRegistration(user, registration);
	}
}
