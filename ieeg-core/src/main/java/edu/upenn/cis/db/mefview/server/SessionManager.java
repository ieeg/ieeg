
/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;

/**
 * Manages {@code SessionToken}s
 * 
 * @author John Frommeyer
 * 
 */
public class SessionManager implements ISessionManager {

	private final IDataSnapshotServer dsServer;
	private final int sessionExpirationMins;
	private final int sessionCleanupMins;
	private final int sessionCleanupDenominator;
	private final AtomicInteger cleanupCount = new AtomicInteger(0);

	private final static Logger logger = LoggerFactory
			.getLogger(ISessionManager.class);

	/**
	 * @see {@link #SessionManager(int, int, int)}
	 * 
	 * @param dsServer
	 * @param sessionExpirationMins
	 * @param sessionCleanupMins
	 * @param sessionCleanupDenominator
	 */
	public SessionManager(
			IDataSnapshotServer dsServer,
			int sessionExpirationMins,
			int sessionCleanupMins,
			int sessionCleanupDenominator) {
		checkArgument(sessionExpirationMins > 0,
				"sessionTimoutMins must be >= 0");
		checkArgument(sessionCleanupMins > 0,
				"sessionCleanupMins must be >= 0");
		checkArgument(sessionCleanupDenominator > 0,
				"sessionCleanupDenominator must be > 0");
		this.dsServer = checkNotNull(dsServer);
		this.sessionExpirationMins = sessionExpirationMins;
		this.sessionCleanupMins = sessionCleanupMins;
		this.sessionCleanupDenominator = sessionCleanupDenominator;

	}

	/**
	 * Creates a {@code SessionManager}. Sessions will expire if their time of
	 * last activity is more than {@code sessionExpirationMins} minutes ago. One
	 * out of every {@code sessionCleanupDenominator} calls to
	 * {@link #createSession()} will result in sessions older than
	 * {@code sessionExpirationMins + sessionCleanupMins} minutes being deleted.
	 * 
	 * @param sessionExpirationMins
	 * @param sessionCleanupMins
	 * @param sessionCleanupDemoninator
	 * 
	 * @throws IllegalArgumentException if {@code sessionExpirationMins} is
	 *             negative.
	 * @throws IllegalArgumentException if {@code sessionCleanupMins} is
	 *             negative.
	 * @throws IllegalArgumentException if {@code sessionCleanupDenominator} is
	 *             less than or equal to zero.
	 */
	public SessionManager(
			int sessionExpirationMins,
			int sessionCleanupMins,
			int sessionCleanupDemoninator) {
		this(
				DataSnapshotServerFactory.getDataSnapshotServer(),
				sessionExpirationMins,
				sessionCleanupMins,
				sessionCleanupDemoninator);
	}

	/**
	 * Returns a token which identifies a newly created session with
	 * {@code user} as the owner.
	 * 
	 * @param user
	 * @return a token which identifies a newly created session with
	 *         {@code user} as the owner
	 */
	public SessionToken createSession(User user) {
		cleanupOldSessions();
		return dsServer.createSession(user);
	}

	/**
	 * Sets the given session's status to logged out.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	public void logoutSession(SessionToken token) {
		dsServer.setSessionStatus(token, SessionStatus.LOGGED_OUT);
	}

	/**
	 * Sets the given session's status to indicate that the user's account has
	 * been deleted or disabled.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	public void disableAcctSession(SessionToken token) {
		dsServer.setSessionStatus(token, SessionStatus.ACCOUNT_DISABLED);
	}

	/**
	 * Returns the number of old sessions which were deleted or -1 if cleanup
	 * was not run. On one out of every {@code sessionCleanupDemoninator} calls
	 * to this method deletes sessions with date of last activity more than
	 * {@code sessionTimeoutMins} minutes old and returns the number of deleted
	 * sessions. If cleanup is not run, then -1 is returned.
	 * 
	 * @return the number of old sessions which were deleted or -1
	 */
	@VisibleForTesting
	int cleanupOldSessions() {
		final String M = "cleanupOldSessions(...)";
		final int cleanup = cleanupCount.getAndIncrement();
		if ((cleanup % sessionCleanupDenominator) == 0) {
			logger.info(
					"{}: Running cleanup. Deleting sessions which have been expired for more than {} minutes",
					M, sessionExpirationMins);
			final Date minCleanup = getMinCleanupDate();
			return dsServer.cleanupOldSessions(minCleanup);
		}
		return -1;

	}

	/**
	 * Returns the {@code UserId} of the user who owns the active session with
	 * the given token.
	 * 
	 * @param token
	 * @return the {@code UserId} of the user who owns the active session with
	 *         the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out
	 * @throws DisabledAccountException if the user's account has been disabled
	 */
	public UserId authenticateSession(SessionToken token) {
		final UserIdSessionStatus userStatus = getUserIdSessionStatus(token);
		switch (userStatus.getSessionStatus()) {
			case ACTIVE:
				return userStatus.getUserId();
			case ACCOUNT_DISABLED:
				throw new DisabledAccountException("UserId ["
						+ userStatus.getUserId()
						+ "] account disabled.");
			case EXPIRED:
				throw new SessionExpirationException("UserId ["
						+ userStatus.getUserId()
						+ "] session has expired.");
			case LOGGED_OUT:
				throw new SessionExpirationException("UserId ["
						+ userStatus.getUserId()
						+ "] session has expired.");
			default:
				throw new AssertionError("Unknown enum value ["
						+ userStatus.getSessionStatus() + "]");
		}
	}

	/**
	 * Returns the {@link UserIdSessionStatus} for the given token. If the
	 * returned {@link SessionStatus} is {@link SessionStatus.ACTIVE} then the
	 * session's last access date was updated.
	 * 
	 * @param token
	 * @return the {@code UserIdSessionStatus} for the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 */
	public UserIdSessionStatus getUserIdSessionStatus(SessionToken token) {
		final Date minLastActive = getMinLastActiveDate();
		final UserIdSessionStatus userStatus = dsServer
				.getUserIdSessionStatus(token, minLastActive);
		return userStatus;
	}

	private Date getMinLastActiveDate() {
		return getMinsAgo(sessionExpirationMins);
	}

	private Date getMinCleanupDate() {
		return getMinsAgo(sessionExpirationMins + sessionCleanupMins);
	}

	/**
	 * Returns the {@code Date} of {@code minsAgo} minutes before the call.
	 * 
	 * @param minsAgo
	 * @return the {@code Date} of {@code minsAgo} minutes before the call.
	 */
	private Date getMinsAgo(int minsAgo) {
		final Calendar minsAgoCal = Calendar.getInstance();
		minsAgoCal.add(Calendar.MINUTE, -minsAgo);
		return minsAgoCal.getTime();
	}
	
	public IDataSnapshotServer getDataSnapshotServer() {
		return this.dsServer;
	}
}
