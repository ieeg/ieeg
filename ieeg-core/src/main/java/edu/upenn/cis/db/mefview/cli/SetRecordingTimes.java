/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.cli;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Maps.fromProperties;
import static com.google.common.collect.Sets.newHashSet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import net.sf.ehcache.CacheManager;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.dao.snapshots.IEegStudyDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IExperimentDAO;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.EegStudyDAOHibernate;
import edu.upenn.cis.braintrust.dao.snapshots.hibernate.ExperimentDAOHibernate;
import edu.upenn.cis.braintrust.model.Contact;
import edu.upenn.cis.braintrust.model.ContactGroup;
import edu.upenn.cis.braintrust.model.EegStudy;
import edu.upenn.cis.braintrust.model.ExperimentEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.TraceServer;
import edu.upenn.cis.db.mefview.server.TraceServerFactory;

/**
 * Sets the recording times of Studies and Experiments to that of one of their
 * time series.
 * 
 * @author John Frommeyer
 * 
 */
public class SetRecordingTimes {

	private static final AmazonS3 s3 = AwsUtil.getS3();

	/**
	 * 
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("starting "
				+ SetRecordingTimes.class.getSimpleName() + "...");

		checkArgument(args.length == 3);
		Properties hibernateProps = getProps(args[0]);

		if (hibernateProps.containsKey("hibernate.hbm2ddl.auto")) {
			System.err
					.println("you have an hibernate.hbm2ddl.auto!!! stopping...");
			System.exit(1);
		}

		HibernateUtil.putConfigProps(hibernateProps);

		Properties ivProps = getProps(args[1]);
		IvProps.setIvProps(fromProperties(ivProps));

		final FileInputStream ieegEhcacheConfig = new FileInputStream(args[2]);
		CacheManager.create(ieegEhcacheConfig);

		Session sess = null;
		Transaction trx = null;
		final Set<String> failedDatasets = newHashSet();
		try {
			sess = HibernateUtil.getSessionFactory().openSession();

			final TraceServer traceServer = TraceServerFactory.getTraceServer();
			final IEegStudyDAO studyDAO = new EegStudyDAOHibernate(sess);
			final List<EegStudy> allStudies = studyDAO.findAll();
			for (final EegStudy study : allStudies) {
				final Recording recording = study.getRecording();
				try {
					trx = sess.beginTransaction();
					updateRecording(study.getLabel(), recording, traceServer);
					trx.commit();
				} catch (RuntimeException e) {
					failedDatasets.add(study.getLabel());
					PersistenceUtil.rollback(trx);
					System.err.println("ERROR: Rolling back changes to "
							+ study.getLabel());
					e.printStackTrace();
				}
			}

			final IExperimentDAO experimentDAO = new ExperimentDAOHibernate(
					sess);
			final List<ExperimentEntity> allExperiments = experimentDAO
					.findAll();
			for (final ExperimentEntity experiment : allExperiments) {
				final Recording recording = experiment.getRecording();
				try {
					trx = sess.beginTransaction();
					updateRecording(experiment.getLabel(), recording,
							traceServer);
					trx.commit();
				} catch (RuntimeException e) {
					failedDatasets.add(experiment.getLabel());
					PersistenceUtil.rollback(trx);
					System.err.println("ERROR: Rolling back changes to "
							+ experiment.getLabel());
					e.printStackTrace();
				}
			}
		} catch (RuntimeException t) {
			t.printStackTrace();
			System.exit(1);
		} finally {
			PersistenceUtil.close(sess);
			if (!failedDatasets.isEmpty()) {
				System.err
						.println("ERROR: The recording times for "
								+ failedDatasets
								+ " could not be set. Search output for specific errors.");
			}
		}
		System.out.println("Finished");
		System.exit(0);

	}

	private static void updateRecording(
			String parentLabel,
			Recording recording,
			TraceServer traceServer) {
		System.out.println("Looking at dataset "
				+ parentLabel + "...");
		final long startUutc = recording.getStartTimeUutc();
		final long endUutc = recording.getEndTimeUutc();
		boolean foundTimeSeries = false;
		Optional<Long> headerStartUutc = Optional.absent();
		Long minHeaderStartUutc = Long.MAX_VALUE;
		Optional<Long> headerEndUutc = Optional.absent();
		Long maxHeaderEndUutc = Long.MIN_VALUE;
		for (final ContactGroup contactGroup : recording.getContactGroups()) {
			for (final Contact contact : contactGroup.getContacts()) {
				foundTimeSeries = true;
				final TimeSeriesEntity timeSeries = contact
						.getTrace();
				final String fileKey = timeSeries.getFileKey();
				final String dataBucket = IvProps.getDataBucket();
				try {
					final String dataVersion = AwsUtil.getETag(
							s3,
							dataBucket,
							fileKey);
					final MEFChannelSpecifier channelSpecifier = new MEFChannelSpecifier(
							new MEFSnapshotSpecifier(
									"",
									""),
							fileKey,
							"",
							0,
							dataVersion);
					final Long currentHeaderStartUutc = Long
							.valueOf(traceServer
									.getStartUutc(channelSpecifier));

					if (!headerStartUutc.isPresent()) {
						headerStartUutc = Optional.of(currentHeaderStartUutc);
						System.out.println("First start time found: "
								+ currentHeaderStartUutc);
					} else {
						if (!currentHeaderStartUutc.equals(headerStartUutc
								.get())) {
							System.out.println("WARNING: file " + fileKey
									+ " start time " + currentHeaderStartUutc
									+ " does not match first start time: "
									+ headerStartUutc.get());
						}

					}
					minHeaderStartUutc = Math.min(
							minHeaderStartUutc,
							currentHeaderStartUutc);

					final Long currentHeaderEndUutc = Long.valueOf(traceServer
							.getEndUutc(channelSpecifier));
					if (!headerEndUutc.isPresent()) {
						headerEndUutc = Optional.of(currentHeaderEndUutc);
						System.out.println("First end time found: "
								+ currentHeaderEndUutc);
					} else {
						if (!currentHeaderEndUutc.equals(headerEndUutc.get())) {
							System.out.println("WARNING: file " + fileKey
									+ " end time " + currentHeaderEndUutc
									+ " does not match first end time: "
									+ headerEndUutc.get());
						}
					}
					maxHeaderEndUutc = Math.max(
							maxHeaderEndUutc,
							currentHeaderEndUutc);
				} catch (RuntimeException e) {
					System.err.println("Error getting times for "
							+ fileKey);
					e.printStackTrace();
				}
			}
		}
		if (foundTimeSeries == false) {
			System.out.println("WARNING: No time series found in "
					+ parentLabel
					+ ". No action taken.");
		} else {
			if (minHeaderStartUutc.equals(Long.MAX_VALUE)
					|| maxHeaderEndUutc.equals(Long.MIN_VALUE)) {
				System.out
						.println("WARNING: No good values for " + parentLabel);
			} else {
				System.out.println("Changing recording times for "
						+ parentLabel + " from [" + startUutc + ","
						+ endUutc + "] to ["
						+ minHeaderStartUutc + "," + maxHeaderEndUutc
						+ "].");
				recording.setStartTimeUutc(minHeaderStartUutc);
				recording.setEndTimeUutc(maxHeaderEndUutc);
			}
		}

	}

	private static Properties getProps(String file) throws IOException {
		FileInputStream fis = null;
		Properties props = null;
		try {
			fis = new FileInputStream(file);
			props = new Properties();
			props.load(fis);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		return props;
	}
}
