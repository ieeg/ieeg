/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mapping.database;

import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.model.UserTaskEntity;

public class TaskMappingFactory {
	
	/**
	 * Get user permissions for UserTaskEntity
	 * 
	 * @param entity
	 * @return
	 */
	public Map<String, Set<String>> getUserPermissions(UserTaskEntity entity) {
		return EntityLookup.getSingleton().getUserPermissions(entity);
	}
	
	public Set<String> getWorldPermissions(UserTaskEntity entity) {
		return EntityLookup.getSingleton().getWorldPermissions(entity);
	}

	public Map<String, Set<String>> getProjectPermissions(UserTaskEntity entity) {
		return EntityLookup.getSingleton().getProjectPermissions(entity);
	}
}
