/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.sql.Timestamp;

import com.google.gwt.user.client.rpc.IsSerializable;

public class LogEntry implements IsSerializable, Comparable<LogEntry> {
	private String recipient;
	private String sender;
	
	private String message;
	private Timestamp time;
	private int visibility;
	
	private Long eventId;
	
	public LogEntry() {}
	

	public LogEntry(String recipient, String sender, String message,
			Timestamp time, int visibility, Long eventId) {
		super();
		this.recipient = recipient;
		this.sender = sender;
		this.message = message;
		this.time = time;
		this.visibility = visibility;
		this.eventId = eventId;
	}



	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}


	@Override
	public int compareTo(LogEntry o) {
		return time.compareTo(o.getTime());
	}

	@Override
	public String toString() {
		return "LogEntry{" +
				"eventId=" + eventId +
				", recipient='" + recipient + '\'' +
				", sender='" + sender + '\'' +
				", message='" + message + '\'' +
				", time=" + time +
				", visibility=" + visibility +
				'}';
	}
}
