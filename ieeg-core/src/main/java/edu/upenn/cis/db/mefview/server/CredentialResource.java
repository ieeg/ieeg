/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.Statement.Effect;
import com.amazonaws.auth.policy.actions.S3Actions;
import com.amazonaws.auth.policy.conditions.StringCondition;
import com.amazonaws.auth.policy.conditions.StringCondition.StringComparisonType;
import com.amazonaws.auth.policy.resources.S3BucketResource;
import com.amazonaws.auth.policy.resources.S3ObjectResource;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.Credentials;
import com.amazonaws.services.securitytoken.model.GetFederationTokenRequest;
import com.amazonaws.services.securitytoken.model.GetFederationTokenResult;
import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.SecurityTokenServiceClientFactory;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.exceptions.InboxCredentialsConflictException;
import edu.upenn.cis.db.mefview.services.ICredentialResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.UploadCredentials;

public final class CredentialResource implements ICredentialResource {

	private final AWSSecurityTokenServiceClient securityTokenClient = SecurityTokenServiceClientFactory
			.getSecurityTokenServiceClient();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final HttpServletRequest httpServletRequest;
	private final User user;

	public CredentialResource(
			@Context HttpServletRequest httpServletRequest) {
		String m = "CredentialResource(...)";
		this.httpServletRequest = httpServletRequest;
		this.user = (User) this.httpServletRequest.getAttribute("user");
		logger.debug("{}: user: ", m, user.getUsername());
	}

	@Override
	public UploadCredentials getCredentials(
			String bucket,
			String collectionName) {
		final long startNanos = System.nanoTime();
		final String m = "getCredentials(...)";
		try {

			final Optional<String> profileBucket = user
					.getProfile()
					.getInboxBucket();
			final String defaultBucket = IvProps.getInboxBucket();

			String authzBucket = defaultBucket;
			if (bucket != null) {
				authzBucket = bucket;
			} else if (profileBucket.isPresent()) {
				authzBucket = profileBucket.get();
			}

			final Optional<String> profileAccessKeyId = user
					.getProfile()
					.getInboxAwsAccessKeyId();

			final String username = user.getUsername();
			String authzPrefix = user.getUserId().getValue().toString();

			if (collectionName != null) {
				authzPrefix += "/" + collectionName;
			}

			if (profileAccessKeyId.isPresent()) {
				if (!authzBucket.equals(defaultBucket)) {
					// User supplied credentials and user supplied bucket.
					// Okay.
					return new UploadCredentials(
							profileAccessKeyId.get(),
							authzBucket,
							authzPrefix);
				} else {
					// User supplied credentials and default bucket. No good.
					throw new InboxCredentialsConflictException(
							"User specified credentidals cannot access default inbox bucket. "
									+ "Either include a bucket in your request to which your credentials will allow you to write "
									+ " or remove your credentials from your user profile and use the default bucket.");
				}
			} else {
				if (!authzBucket.equals(defaultBucket)) {
					// Temporary credentials and user supplied bucket. No good.
					throw new InboxCredentialsConflictException(
							"Non-default inbox specified: ["
									+ authzBucket
									+ "] but no user supplied AWS credentials found. "
									+ "Either use default bucket or add AWS credentials for this bucket to your user profile.");
				} else {
					// Temporary credentials and default bucket. Okay.
					final Statement bucketStatement = new Statement(
							Effect.Allow)
							.withActions(S3Actions.ListObjects)
							.withResources(
									new S3BucketResource(authzBucket))
							.withConditions(
									new StringCondition(
											StringComparisonType.StringLike,
											"s3:prefix",
											authzPrefix + "/*"));
					final Statement objectStatement = new Statement(
							Effect.Allow)
							.withActions(S3Actions.PutObject,
									S3Actions.DeleteObject)
							.withResources(
									new S3ObjectResource(
											authzBucket,
											authzPrefix
													+ "/*"));
					final Policy policy = new Policy()
							.withStatements(bucketStatement, objectStatement);

					final String cleanedUsername = username
							.replaceAll(
									"[^\\w+=,.@-]",
									"-");

					final GetFederationTokenRequest request = new GetFederationTokenRequest()
							.withDurationSeconds(IvProps
									.getInboxCredentialsDurationSecs())
							.withName(cleanedUsername)
							.withPolicy(policy.toJson());

					final GetFederationTokenResult result = securityTokenClient
							.getFederationToken(request);
					final Credentials credentials = result.getCredentials();
					return new UploadCredentials(
							credentials
									.getAccessKeyId(),
							authzBucket,
							authzPrefix,
							credentials
									.getSecretAccessKey(),
							credentials
									.getSessionToken());
				}
			}
		} catch (InboxCredentialsConflictException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToInboxCredentialsConflictFailure(
							e,
							m,
							logger);
		} catch (RuntimeException e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);
		} catch (Error e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);
		} finally {
			logger.info(
					"{}: For user {} {} seconds",
					m,
					user.getUsername(),
					BtUtil.diffNowThenSeconds(startNanos));
		}
	}
}
