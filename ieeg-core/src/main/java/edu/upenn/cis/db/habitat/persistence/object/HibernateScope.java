/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.object;

import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.db.habitat.persistence.Scope;

public class HibernateScope extends Scope {

	Session session = null;
	Transaction trans = null;
	
	public HibernateScope() {
		gcSession = true;
		gcTrans = true;
	}
	
	public HibernateScope(Session sess) {
		session = sess;
		gcTrans = true;
	}
	
	public HibernateScope(SessionAndTrx str) {
		this.session = str.session;
		if (str.trx.isPresent())
			this.trans = str.trx.get();
		
		this.gcSession = false;
		this.gcTrans = trans != null;
	}
	
	public HibernateScope(Session sess, Transaction trans) {
		session = sess;
		this.trans = trans;
		gcTrans = false;
		gcSession = false;
	}
	
	public Session getSession() {
		return session;
	}
	
	public Transaction getTransaction() {
		return trans;
	}

	public void createNewScope() {
		if (gcSession) {
			SessionAndTrx t = PersistenceUtil.getSessAndTrx(HibernateUtil.getSessionFactory());
			this.session = t.session;
			this.trans = t.trx.get();
			return;
		}
		if (gcTrans) {
//			System.out.println("New transaction");
			trans = session.beginTransaction();
		}
	}

	public void commitScope() {
		if (gcTrans) {
//			System.out.println("Commit transaction");
			trans.commit();
			trans = null;
		}

		if (gcSession) {
//			System.out.println("Killing session");
			session.close();
			session = null;
		}
//		createNewScope();
	}
	
	public void rollbackScope() {
		if (gcTrans) {
//			System.out.println("Abort transaction");
			trans.rollback();
			trans = null;
		}
		
		if (gcSession) {
//			System.out.println("Killing session");
			session.close();
			session = null;
		}
//		createNewScope();
	}

	@Override
	public boolean isUnitializedScope() {
		return session == null && trans == null;
	}

	@Override
	public void save(Object o) {
		session.save(o);
	}

	@Override
	public void saveOrUpdate(Object o) {
		session.saveOrUpdate(o);
	}
	
	@Override
	public void delete(Object o) {
		session.delete(o);
	}
	
}
