/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PagingPresenter<T> {
	public void refreshList(List<? extends T> newContent);
	
	public void nextPage(T collectionItem);
	
	public void prevPage(T collectionItem);
	
	public void toggleStar(T collectionItem);
	
	public void toggleRating(T collectionItem);

	public boolean isStarred(T collectionItem);
	
	public boolean isFirstPage(T collectionItem);
	
	public boolean isLastPage(T collectionItem);
	
	public void onItemRemoved(T collectionItem);
	
	public void refresh(T rootItem);

	public Set<T> getSelected();
	
	public void restoreSelected(Collection<T> sel);
}
