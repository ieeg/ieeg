/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.user.client.rpc.IsSerializable;

public class TimeSeriesBookmark implements IsSerializable {
	private String snapshotId;
	
	private long timeOffset;
	
	private Set<String> channelsSelected = Sets.newHashSet();
	
	private long viewWidth;
	
	private String annotationID;
	
	private int filterType;
	private double passLow;
	private double passHigh;
	private double stopLow;
	private double stopHigh;
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public long getTimeOffset() {
		return timeOffset;
	}
	public void setTimeOffset(long timeOffset) {
		this.timeOffset = timeOffset;
	}
	public Set<String> getChannelsSelected() {
		return channelsSelected;
	}
	public void setChannelsSelected(Set<String> channelsSelected) {
		this.channelsSelected = channelsSelected;
	}
	public long getViewWidth() {
		return viewWidth;
	}
	public void setViewWidth(long viewWidth) {
		this.viewWidth = viewWidth;
	}
	public String getAnnotationID() {
		return annotationID;
	}
	public void setAnnotationID(String annotationID) {
		this.annotationID = annotationID;
	}
	public int getFilterType() {
		return filterType;
	}
	public void setFilterType(int filterType) {
		this.filterType = filterType;
	}
	public double getPassLow() {
		return passLow;
	}
	public void setPassLow(double passLow) {
		this.passLow = passLow;
	}
	public double getPassHigh() {
		return passHigh;
	}
	public void setPassHigh(double passHigh) {
		this.passHigh = passHigh;
	}
	public double getStopLow() {
		return stopLow;
	}
	public void setStopLow(double stopLow) {
		this.stopLow = stopLow;
	}
	public double getStopHigh() {
		return stopHigh;
	}
	public void setStopHigh(double stopHigh) {
		this.stopHigh = stopHigh;
	}
	
}
