/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.mapping;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class SimpleTypeMapper {
	private ObjectMapper om = ObjectMapperFactory.getObjectMapper();
	
	public String asStringFromSet(Set<IJsonKeyValue> set) {
		try {
			return JsonKeyValueMapper.getMapper().createKeyValueSet(set);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String asStringFromSet(JsonKeyValueSet set) {
		try {
			return JsonKeyValueMapper.getMapper().createKeyValueSet(set);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Set<IJsonKeyValue> asKeyValueSet(String value) {
		try {
			return JsonKeyValueMapper.getMapper().getKeyValueSet(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new HashSet<IJsonKeyValue>();
	}
	
	public JsonKeyValueSet asJsonKeyValueSet(String value) {
		try {
			return JsonKeyValueMapper.getMapper().getJsonKeyValueSet(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new JsonKeyValueSet();
	}

	public IJsonKeyValue asKeyValue(String value) {
		try {
			return (IJsonKeyValue)om.reader(IJsonKeyValue.class).readValue(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
//	@JsonTypedTranslator
	public JsonTyped asJson(String value) {
		try {
			return (JsonTyped)om.reader(JsonTyped.class).readValue(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

//	@WebLinkTranslator
	public WebLink asWebLink(String value) {
		try {
//			System.err.println("Deserializing " + value);
			return (WebLink)om.reader(WebLink.class).readValue(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String asStringFromJson(JsonTyped value) {
		try {
			return om.writer().writeValueAsString(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String asStringFromKV(IJsonKeyValue value) {
		try {
			return om.writer().writeValueAsString(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
