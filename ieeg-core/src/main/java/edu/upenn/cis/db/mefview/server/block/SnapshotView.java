/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import java.io.Serializable;
import java.util.List;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredMap;

import edu.upenn.cis.db.mefview.server.block.SnapshotCatalogView.SnapshotConfig;

public class SnapshotView {
	SnapshotHeader header;
	SnapshotConfig config;
	SnapshotDb db;
	
	StoredMap<ChannelPos,Block> blockMap;
	StoredMap<String,SnapshotHeader> headerMap;
	
	public static class ChannelPos implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public ChannelPos(short channel, long index) {
			channelId = channel;
			start = index;
		}
		public long start;
		public short channelId;
	}
	
	public static class Block implements Serializable {
/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//		public int end;
		public byte[] array;
	}
	
	public SnapshotView(SnapshotConfig config, SnapshotCatalogDb catalog, String dbName, String basePath) {
		this.config = config;
		db = new SnapshotDb(catalog, dbName, basePath);
		
		EntryBinding<String> headerKeyBinding = new SerialBinding<String>(catalog.getClassCatalog(), String.class);
		EntryBinding<SnapshotHeader> headerBinding = new SerialBinding<SnapshotHeader>(catalog.getClassCatalog(), SnapshotHeader.class);
		
		headerMap = new StoredMap<String,SnapshotHeader>(db.getDatabase(), headerKeyBinding, headerBinding, true);

		EntryBinding<ChannelPos> blockKeyBinding = new SerialBinding<ChannelPos>(catalog.getClassCatalog(), ChannelPos.class);
		EntryBinding<Block> blockBinding = new SerialBinding<Block>(catalog.getClassCatalog(), Block.class);
		
		blockMap = new StoredMap<ChannelPos,Block>(db.getDatabase(), blockKeyBinding, blockBinding, true);
	}
	
	public boolean isCompressed() {
		return header.isCompressed();
	}
	
	public List<SnapshotCatalogView.Interval> getGaps() {
		return config.getGaps();
	}

	public StoredMap<ChannelPos, Block> getBlockMap() {
		return blockMap;
	}

	public StoredMap<String, SnapshotHeader> getHeaderMap() {
		return headerMap;
	}
	
	@SuppressWarnings("unchecked")
	public final StoredEntrySet<String,SnapshotHeader> getHeaderEntrySet() {
		return (StoredEntrySet<String,SnapshotHeader>) headerMap.entrySet();
	}

	@SuppressWarnings("unchecked")
	public final StoredEntrySet<ChannelPos,Block> getBlockEntrySet() {
		return (StoredEntrySet<ChannelPos,Block>) blockMap.entrySet();
	}
	
	public void close() {
		db.close();
	}
}
