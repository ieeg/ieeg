package edu.upenn.cis.db.mefview.server;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;

import edu.upenn.cis.db.mefview.shared.FileReference;

public interface IJsonExtractor {
	public void initialize(FileReference ref) throws FileNotFoundException, IOException;
	public JsonNode createJson() throws IOException;
	public JsonNode createJson(int max, boolean addNote) throws IOException;
	public void close();
}
