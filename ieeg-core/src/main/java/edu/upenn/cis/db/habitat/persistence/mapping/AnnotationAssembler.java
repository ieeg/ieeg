/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.persistence.mapping;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class AnnotationAssembler {
	/**
	 * 
	 * Returns a {@code TsAnnotationDto} with annotated {@code TimeSeriesDto}s
	 * assembled from the values found in {@code a}.
	 * 
	 * @param a
	 * @return a {@code TsAnnotationDto} with annotated {@code TimeSeriesDto}s
	 */
	public static TsAnnotationDto toTsAnnotationDto(Annotation a) {
		final Set<TimeSeriesDto> annotated = newHashSet();
		for (int i = 0; i < a.getChannelInfo().size(); i++) {
			String revid = a.getChannelInfo().get(i).getId();
			annotated.add(new TimeSeriesDto(revid, a.getChannels().get(i),
					null));

		}
		TsAnnotationDto tsa = new TsAnnotationDto(annotated,
				a.getCreator(),
				Long.valueOf((long) a.getStart()),
				Long.valueOf((long) a.getEnd()),
				a.getType(),
				a.getRevId(),
				a.getDescription(),
				a.getInternalId(),
				a.getLayer(),
				a.getColor());
		return tsa;
	}

	public static Annotation toAnnotation(TsAnnotationDto tsaDto,
			List<TraceInfo> traceInfos) {
		final Set<String> annotatedChannelLabels = newHashSet();
		final List<TraceInfo> annotatedTraceInfos = newArrayList();
		for (final TimeSeriesDto tsDto : tsaDto.getAnnotated()) {
			final String annotatedChannelLabel = tsDto.getLabel();
			annotatedChannelLabels.add(annotatedChannelLabel);
			TraceInfo annotatedTraceInfo = find(
					traceInfos,
					compose(equalTo(annotatedChannelLabel),
							TraceInfo.getLabel));
			annotatedTraceInfos.add(annotatedTraceInfo);
		}
		final Annotation a = new Annotation(tsaDto.getAnnotator(),
				tsaDto.getType(),
				tsaDto.getStartOffsetMicros(),
				tsaDto.getEndOffsetMicros(),
				annotatedChannelLabels,
				false,
				annotatedTraceInfos);
		a.setLayer(tsaDto.getLayer());
		a.setDescription(tsaDto.getDescription());
		a.setRevAndInternalId(tsaDto.getId());
		a.setColor(tsaDto.getColor());
		return a;
	}

	public static Annotation toAnnotation(TsAnnotationDto tsaDto) {
		final Set<String> channelLabels = toChannelLabelSet(tsaDto
				.getAnnotated());
		final Annotation ann = new Annotation(tsaDto.getAnnotator(),
				tsaDto.getType(),
				tsaDto.getStartOffsetMicros().doubleValue(),
				tsaDto.getEndOffsetMicros().doubleValue(),
				channelLabels);
		ann.setDescription(tsaDto.getDescription());
		ann.setRevAndInternalId(tsaDto.getId());
		ann.setLayer(tsaDto.getLayer());
		ann.setColor(tsaDto.getColor());
		return ann;
	}

	public static Set<String> toChannelLabelSet(Set<TimeSeriesDto> tsDtoSet) {
		return newHashSet(transform(
				tsDtoSet, TimeSeriesDto.getLabel));
	}

}
