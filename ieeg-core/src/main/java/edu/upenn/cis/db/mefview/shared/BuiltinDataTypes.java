/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

public class BuiltinDataTypes {
	// Really, timeseries
	public static final String EEG = "eeg";
	
	public static final String DICOM = "dicom";

	public static final String PDF = "pdf";
	
	public static final String MRI = "mri";

	public static final String CT = "ct";

	public static final String THREED = "3d";
	
	public static final String URL = "url";
	
	public static final String DCN = "dcn";
	
	public static final String MATLAB = "matlab";
	
	public static final String JAVA = "java";
	
	public static final String JS = "js";
	
	public static final String TEXT = "txt";
	
	public static final String BIN = "bin";
	
	public static final String USER = "user";
	
	public static final String ACL = "ACL";
	
	public static final String PROJECT = "project";
}
