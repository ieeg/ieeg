/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;

/**
 * Specifier for a filter
 * 
 * @author zives
 * 
 */
@GwtCompatible(serializable=true)
public class DisplayConfiguration implements Serializable {
	public static final int NO_FILTER = 0;//(1 << 0);
	public static final int LOWPASS_FILTER = (1 << 1);
	public static final int HIGHPASS_FILTER = (1 << 2);
	public static final int BANDPASS_FILTER = (1 << 3);
	public static final int BANDSTOP_FILTER = (1 << 12);

	public static final int DEFAULT_FILTER_POLES = 4;
	public static final int MAX_FILTER_POLES = 128;
	public static final double DEFAULT_LOW_FC = 0.0;
	public static final double DEFAULT_HIGH_FC = 500.0;
	
	int filterType;
	int numPoles;
	double bandpassLowCutoff;
	double bandpassHighCutoff;
	double bandstopLowCutoff;
	double bandstopHighCutoff;
	String filterName;
	
	boolean raster;
	
	long timeOffset;
	
	public DisplayConfiguration() {
		filterType = 0;//NO_FILTER;

		numPoles = DEFAULT_FILTER_POLES;

		bandpassLowCutoff = DEFAULT_LOW_FC;

		bandpassHighCutoff = DEFAULT_HIGH_FC;

		bandstopLowCutoff = 0;

		bandstopHighCutoff = 0;
		
		filterName = "";
	}

	public DisplayConfiguration(String filterName, int filterType, int numPoles, double bandpassLowCutoff,
			double bandpassHighCutoff, double bandstopLowCutoff,
			double bandstopHighCutoff) {
		this.filterType = filterType;
		this.numPoles = numPoles;
		this.bandpassLowCutoff = bandpassLowCutoff;
		this.bandpassHighCutoff = bandpassHighCutoff;
		this.bandstopLowCutoff = bandstopLowCutoff;
		this.bandstopHighCutoff = bandstopHighCutoff;
		this.filterName = filterName;
	}
	
	public DisplayConfiguration(DisplayConfiguration source) {
		this (source.filterName, source.filterType, source.numPoles, source.bandpassLowCutoff,
				source.bandpassHighCutoff, source.bandstopLowCutoff, 
				source.bandstopHighCutoff);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DisplayConfiguration)) {
			return false;
		}
		DisplayConfiguration fp = (DisplayConfiguration)o;
		
		return (fp.filterType == filterType && fp.getFilterName().equals(getFilterName()) && fp.numPoles == numPoles &&
				fp.bandpassLowCutoff == bandpassLowCutoff &&
				fp.bandpassHighCutoff == bandpassHighCutoff &&
				(((filterType & BANDSTOP_FILTER) == 0) || (fp.bandstopLowCutoff == bandstopLowCutoff &&
				fp.bandstopHighCutoff == bandstopHighCutoff)));
	}

	public void addFilterType(int typ) {
		filterType = filterType | typ;
	}

	public void setFilterType(int typ) {
		filterType = typ;
	}

	public void removeFilterType(int typ) {
		filterType = filterType & (~typ);
	}

	public boolean includesFilterType(int typ) {
		return (filterType & typ) != 0;
	}

	@XmlElement(name = "filterType")
	public int getFilterType() {
		return filterType;
	}

	@XmlElement(name = "poles")
	public int getNumPoles() {
		return numPoles;
	}

	public void setNumPoles(int numPoles) {
		if (numPoles < MAX_FILTER_POLES)
			this.numPoles = numPoles;
	}

	@XmlElement(name = "bandpassLow")
	public double getBandpassLowCutoff() {
		return bandpassLowCutoff;
	}

	public void setBandpassLowCutoff(double bandpassLowCutoff) {
		this.bandpassLowCutoff = bandpassLowCutoff;
	}

	@XmlElement(name = "bandpassHigh")
	public double getBandpassHighCutoff() {
		return bandpassHighCutoff;
	}

	public void setBandpassHighCutoff(double bandpassHighCutoff) {
		this.bandpassHighCutoff = bandpassHighCutoff;
	}

	@XmlElement(name = "bandstopLow")
	public double getBandstopLowCutoff() {
		return bandstopLowCutoff;
	}

	public void setBandstopLowCutoff(double bandstopLowCutoff) {
		this.bandstopLowCutoff = bandstopLowCutoff;
	}

	@XmlElement(name = "bandstopHigh")
	public double getBandstopHighCutoff() {
		return bandstopHighCutoff;
	}

	public void setBandstopHighCutoff(double bandstopHighCutoff) {
		this.bandstopHighCutoff = bandstopHighCutoff;
	}

	@Override
	public int hashCode() {
		return getFilterType() ^ Double.valueOf(bandpassLowCutoff).hashCode() ^
				Double.valueOf(bandpassHighCutoff).hashCode()
				^ Double.valueOf(bandstopLowCutoff).hashCode()
				^ Double.valueOf(bandstopHighCutoff).hashCode() ^ getNumPoles() ^
				getFilterName().hashCode();
	}

	public String getPath() {
		return "filterSpec";
	}

	public String getFilterName() {
		if (filterName != null)
			return filterName;
		else
			return "";
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	private String paramsToString() {
		return "filterName=" + filterName + 
				";filterType=" + filterType
				+ ";numPoles=" + numPoles
				+ ";bandpassLowCutoff="
				+ bandpassLowCutoff
				+ ";bandpassHighCutoff="
				+ bandpassHighCutoff
				+ ";bandstopLowCutoff="
				+ bandstopLowCutoff
				+ ";bandstopHighCutoff="
				+ bandstopHighCutoff;
	}
	
	@Override
	public String toString() {
		return getPath() + ";" + paramsToString();
	}
	
	public String paramsToURL() {
		return "&type="+filterName +
				"&filter=" + filterType
				+ "&poles=" + numPoles
				+ "&bpLo="
				+ (int)bandpassLowCutoff
				+ "&bpHi="
				+ (int)bandpassHighCutoff
				+ "&bsLo="
				+ (int)bandstopLowCutoff
				+ "&bsHi="
				+ (int)bandstopHighCutoff;
	}

	@JsonIgnore
	@Transient
	public boolean isFiltered() {
		return filterType != NO_FILTER && filterType != 0;
	}

	public boolean isRaster() {
		return raster;
	}

	public void setRaster(boolean raster) {
		this.raster = raster;
	}
	
	public void setTimeOffset(long offset) {
		this.timeOffset = offset;
	}
	
	public long getTimeOffset() {
		return timeOffset;
	}
}
