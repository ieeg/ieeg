package edu.upenn.cis.db.habitat.persistence.mapping;

import java.util.List;

import com.google.common.collect.Lists;

import edu.upenn.cis.braintrust.model.EventLogEntity;
import edu.upenn.cis.braintrust.shared.LogMessage;

//@Mapper
public class LoggingMapper {
//	LoggingMapper INSTANCE = Mappers.getMapper ( LoggingMapper.class );
//
//	@Mappings({
//		@Mapping(source="notifiedUser", target = "recipient")
//	})
//	LogMessage logEntryToMessage(EventLogEntity log);
//	
//	@InheritInverseConfiguration
//	EventLogEntity logMessageToEntity(LogMessage msg);
	
	public static LogMessage convertEntryToMessage(EventLogEntity event) {
		return new LogMessage(
				event.getNotifiedUser().getId(),
				event.getSourceUser().getId(),
				event.getEvent(),
				event.getTime(),
				event.getVisibility(),
				event.getEventId(),
				event.getMessageType(),
				event.getMessageIdentifier());
	}

	public static List<LogMessage> convertLogEntriesToMessages(
			List<EventLogEntity> logs) {
		List<LogMessage> ret = Lists.newArrayList();

		for (EventLogEntity event : logs) {
			ret.add(convertEntryToMessage(event));
		}

		return ret;
	}

}
