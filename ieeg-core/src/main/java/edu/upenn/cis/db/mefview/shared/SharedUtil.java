/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

public final class SharedUtil {

	public static String getDate(long date){
		  int days = (int)(date / (1E6 * 3600 * 24));
		  
		  long hms = (long)(date - days * (1E6 * 3600 * 24));
		  
		  int hours = (int)(hms / 1E6 / 3600);
		  
		  hms = (long)(hms - hours * (1E6 * 3600));
		  
		  int minutes = (int)(hms / (1E6 * 60));
		  
		  hms = (long)(hms - minutes * (1E6 * 60));
		  
		  int seconds = (int)(hms / (1E6));
		  
		  String ret = Integer.toString(seconds);
		  
		  if (ret.length() < 2)
			  ret = "0" + ret;
		  
		  String min = Integer.toString(minutes);
		  if (min.length() < 2)
			  min = "0" + min;
	
		  ret = " " + hours + ":" + min + ":" + ret;
	
		  return "Day " + Integer.toString(days + 1) + ret;
	}

}
