/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.slf4j.Logger;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService.UsersAndTotalCount;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;

public interface IIeegUserService extends IUserAuth {
	public void createSessionManager(IDataSnapshotServer dsServer,
			int sessionTimeoutMinutes, 
			int sessionCleanupMins, 
			int sessionCleanupDenom);
	
	public void setSessionManager(ISessionManager mgr);
	
	public ISessionManager getSessionManager();
	

	/**
	 * Returns the {@code User} who owns the session with the given token. If
	 * {@code allowExpiredSess} is true the user is returned even if the session
	 * has expired.
	 * 
	 * @param token
	 * @return the {@code User} who owns the active session with the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out and {@code allowExpiredSess} is false
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out and {@code allowExpiredSess} is false
	 * @throws DisabledAccountException if the user's account has been disabled
	 * @throws IncorrectCredentialsException if the session owner cannot be
	 *             found
	 */
	public User verifyUser(
			String sourceMethod, 
			SessionToken sessionID,
			boolean allowExpiredSess, 
			@Nullable Role role);
	/**
	 * Returns the {@code User} who owns the active session with the given
	 * token.
	 * 
	 * @param token
	 * @return the {@code User} who owns the active session with the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out
	 * @throws DisabledAccountException if the user's account has been disabled
	 */
	public User verifyUser(String source, SessionToken sessionID,
			@Nullable Role role);

	public User verifyUserError(final String caller, 
			final SessionToken sessionID, @Nullable Role role, final Logger logger);
	
	public UsersAndTotalCount getUsersAndCount();
	
	public long getUserCount();

	public User getUserByName(final String username);
	
	public IDataSnapshotServer getDataSnapshotAccessSession();
	
	public void setDataSnapshotAccessSession(IDataSnapshotServer ds);

//	public List<String> getUserAddresses();
	
	public List<LatLon> getLatLon();

	UserAccountInfo getUserAccountInfoHelper(String requestedUserName);

	User updateUserFields(User user, UserProfile newUser);

	UsersAndTotalCount getUsersAndCount(int start, int length,
			Set<UserId> excludes);
}
