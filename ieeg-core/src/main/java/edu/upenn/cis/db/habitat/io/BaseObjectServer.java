package edu.upenn.cis.db.habitat.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

public abstract class BaseObjectServer implements IObjectServer {

	@Override
	public List<IStoredObjectReference> getFilesMatching(IStoredObjectContainer dir, String pattern) {
		List<IStoredObjectReference> ret = new ArrayList<IStoredObjectReference>();
		
		try {
			for (IStoredObjectReference file: getDirectoryContents(dir)) {
				if (!file.isContainer() && file.getFilePath().matches(pattern))
					ret.add(file);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}

	@Override
	public List<IStoredObjectReference> getSubdirectories(IStoredObjectContainer dir) {
		List<IStoredObjectReference> ret = new ArrayList<IStoredObjectReference>();
		
		try {
			for (IStoredObjectReference file: getDirectoryContents(dir)) {
				if (file.isContainer()) {
					System.out.println("Found directory: " + file.getFilePath());
			    	ret.add(file);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return ret;
	}
	
	@Override
	public List<IStoredObjectReference> getFilesRecursivelyMatching(IStoredObjectContainer root, 
			String pattern) {
		List<IStoredObjectReference> returnResults = new ArrayList<IStoredObjectReference>();
		
		getAllFiles(root, returnResults, pattern);
		
		return returnResults;
	}
	
	void getAllFiles(IStoredObjectContainer root, 
			List<IStoredObjectReference> returnResults, String pattern) {
		List<IStoredObjectReference> children = getSubdirectories(root);
		
		System.out.println("Traversing directory " + root.getFileDirectory());
		for (IStoredObjectReference p: children) {
			getAllFiles(getContainerFrom(root, p), returnResults, pattern);
		}
		
		returnResults.addAll(getFilesMatching(root, pattern));
	}


	@Override
	public IStoredObjectContainer getContainerFrom(IStoredObjectContainer cont, IStoredObjectReference ref) {
		if (!ref.isContainer())
			throw new RuntimeException("Not a container!");
		else {
			String extra = "";
			if (!ref.getDirBucketContainer().getFileDirectory().endsWith("/") &&
					!ref.getFilePath().startsWith("/"))
				extra = "/";
			
			return new DirectoryBucketContainer(ref.getDirBucketContainer().getBucket(),
					ref.getDirBucketContainer().getFileDirectory() + extra + ref.getFilePath());
		}
	}


	@Override
	public void init() {
		// TODO Auto-generated method stub

	}
}
