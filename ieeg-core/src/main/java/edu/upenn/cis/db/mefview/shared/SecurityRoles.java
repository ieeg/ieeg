package edu.upenn.cis.db.mefview.shared;

public class SecurityRoles {
	public static Integer READER = new Integer(3);
	public static Integer UPDATER = new Integer(4);
	public static Integer ORG_ADMIN = new Integer(2);
	public static Integer DB_ADMIN = new Integer(1);
	public static Integer GRANTER = new Integer(5);
	public static Integer DOWNLOADER = new Integer(6);
	public static Integer ANNOTATOR = new Integer(7);
	public static Integer SEARCHER = new Integer(8);
}
