/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;

/**
 * Basic page server class, supporting background threads for fetching
 * data.
 * 
 * @author zives
 *
 * @param <PageData>
 */
public abstract class MultithreadedPageServer<PageData> extends TimeSeriesPageServer<PageData>{
	
//	PageReader<PageData> reader;
	private ExecutorService exec;
//	ScheduledExecutorService mon;
	public int CACHE_SIZE = 512;//0;
	public int PREFETCHES = 10;//30;
	
	public MultithreadedPageServer(int threads, boolean poolFetches) {
//		pageMap = //new HashMap<Long,PageData>();
//			Collections.synchronizedMap(new LruCache<Long,PageData>(CACHE_SIZE));

//		fullPageMap = Collections.synchronizedMap(new LruCache<PageID,PageData>(CACHE_SIZE));
		
		exec = Executors.newFixedThreadPool(threads);
//		mon = Executors.newScheduledThreadPool(1);
//		exec = Executors.newSingleThreadExecutor();//threads);
		
		if (!poolFetches)
			PREFETCHES = 0;
		/*
		mon.scheduleAtFixedRate(new Runnable() {

			public void run() {
//				System.out.println("Checking available memory: " + Runtime.getRuntime().freeMemory());
				int count = fullPageMap.size();
//				System.out.println("Entries in cache: " + count);
				if (Runtime.getRuntime().freeMemory() < 10000000L)
					clearBuffer();
				
				doWork();
			}
			
		}, 360, 360, TimeUnit.SECONDS);*/
	}
	
	abstract void doWork();
	
	public MultithreadedPageServer(ChannelSpecifier doc, PageReader<PageData> reader) {
		exec = Executors.newFixedThreadPool(10);
		addReader(doc, reader);
	}
	
	public void shutdown() {
//		mon.shutdownNow();
		exec.shutdownNow();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Enqueue a request for a page
	 * 
	 * @param pageId
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public PageData queueUpPage(ChannelSpecifier doc, long pageId, int resolution) throws InterruptedException, ExecutionException {
		Future<PageData> f = exec.submit(new PageLoader(doc, pageId, resolution, 
				fullPageMap, getReaderFor(doc)));
		
		PageData pag = f.get();
		System.out.println("Cached " + ((TimeSeriesPage)pag).values.length + " bytes");
		addToCache(doc, pageId, pag);
		return pag;
	}

	/**
	 * Request a page
	 * @param pageId
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
//	@Override
//	public PageData requestPage(String doc, long pageId, int resolution) throws InterruptedException, ExecutionException {
//		String uid = doc + "/" + String.valueOf(pageId);
////		System.out.println("> Requesting " + uid);
//		PageData result = fullPageMap.get(uid);
//		if (result == null) {
//			result = queueUpPage(doc, pageId, resolution);
//			misses++;
//			return result;
//		} else {
//			hits++;
//		}
//		return result;
//	}

//	public abstract ArrayList<TimeSeriesData> requestReadAtSampleRate(String source, String path, long startTime, 
//			long endTime, double samplingPeriod,
//			final FilterSpec filter, FilterManager.FilterBuffer buf) 
//	throws InterruptedException, ExecutionException;
//
//	public abstract ArrayList<TimeSeriesData> requestReadExactMultiple(
//			final String source, final String path, long startTimeOffset,
//			long endTimeOffset, int factor)
//			throws InterruptedException, ExecutionException;
	
	public class PageLoader implements Callable<PageData> {
		long pid;
		int resolution;
		ChannelSpecifier doc;
		PageReader<PageData> reader;
		Map<PageID<ChannelSpecifier>,PageData> thisCache;
		
		public PageLoader(ChannelSpecifier doc, long pageId, int resolution, 
				Map<PageID<ChannelSpecifier>,PageData> cacheMap, PageReader<PageData> reader) {
			pid = pageId;
			this.resolution = resolution;
			this.reader = reader;
			this.doc = doc;
			thisCache = cacheMap;
		}

		public PageData call() throws Exception {
//			String uid = doc + "/" + String.valueOf(pid);
			PageID<ChannelSpecifier> uid = new PageID<ChannelSpecifier>(doc, pid);
			PageData pg = thisCache.get(uid); 
			if (pg == null) {
				
				System.out.println("Reading " + pid + " for " + PREFETCHES + " pages");
				PageData[] pgs = reader.readPages(pid, PREFETCHES, true);
				
				int i = 0;
				for (PageData pd : pgs) {
					if (i == 0)
						pg = pd;
					uid = new PageID<ChannelSpecifier>(doc, pid+i);
					thisCache.put(uid, pd);
					i++;
				}
				
			}
			return pg;
		}

	}
	
}
