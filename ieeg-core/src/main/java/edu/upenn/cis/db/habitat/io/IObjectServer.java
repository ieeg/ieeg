/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

/**
 * Generic base interface for file servers that could be local or cloud
 * @author zives
 *
 */
public interface IObjectServer {
	
	public interface IStream {
		public void close() throws IOException;
		
		public ICursor getStart();
		
		public ICursor getOffset(long offset);
		
		public IStoredObjectReference getHandle();
	}
	
	/**
	 * Input stream for reading content
	 * 
	 * @author zives
	 *
	 */
	public interface IInputStream extends IStream {
		
		public ICursor getBytes(WritableByteChannel target, long bytes) throws IOException;
		
		public ICursor getBytes(ICursor position, WritableByteChannel target, long bytes) throws IOException;

		public ICursor getBytes(ByteBuffer target, long bytes) throws IOException;
		
		public ICursor getBytes(ICursor position, ByteBuffer target, long bytes) throws IOException;

		public boolean isAtEof(ICursor cursor) throws IOException;
		
		public long getLength() throws IOException;

		public void copyTo(OutputStream stream) throws IOException;

		public InputStream getInputStream() throws IOException;
	}
	
	/**
	 * Output stream for writing content
	 * 
	 * @author zives
	 *
	 */
	public interface IOutputStream extends IStream {
		public ICursor putBytes(ReadableByteChannel source, long numBytes) throws IOException;
		
		public ICursor putBytes(ICursor position, ReadableByteChannel source, long numBytes) throws IOException;

		public ICursor putBytes(ByteBuffer source) throws IOException;
		
		public ICursor putBytes(ICursor position, ByteBuffer source) throws IOException;

		public void copyFrom(IInputStream stream) throws IOException;
		
		public void copyFrom(InputStream stream, long length) throws IOException;
		
		public OutputStream getOutputStream() throws IOException;
	}
	
	public interface ICursor {
		public long getPosition();
		
		public void setPosition(long offset);
	}
	
	public void init();
	
	public IInputStream openForInput(IStoredObjectReference handle) throws IOException;

	public IOutputStream openForOutput(IStoredObjectReference handle) throws IOException;
	
	/**
	 * Read into a byte channel, from start of file
	 * 
	 * @param stream
	 * @param target
	 * @param numBytesToFetch
	 * @return
	 * @throws IOException
	 */
	public ICursor getBytes(IInputStream stream, WritableByteChannel target, long numBytesToFetch) throws IOException;
	
	public ICursor getBytes(IInputStream stream, ICursor currentPosition, WritableByteChannel target, long numBytesToFetch) throws IOException;
	
	/**
	 * Read into a byte buffer, from start of file
	 * 
	 * @param stream
	 * @param target
	 * @param numBytesToFetch
	 * @return
	 * @throws IOException
	 */
	public ICursor getBytes(IInputStream stream, ByteBuffer target, long numBytesToFetch) throws IOException;
	
	public ICursor getBytes(IInputStream stream, ICursor currentPosition, ByteBuffer target, long numBytesToFetch) throws IOException;

	/**
	 * Read into a byte channel, from start of file
	 * 
	 * @param stream
	 * @param target
	 * @param numBytesToFetch
	 * @return
	 * @throws IOException
	 */
	public ICursor putBytes(IOutputStream stream, ReadableByteChannel source, long numBytesTowrite) throws IOException;
	
	public ICursor putBytes(IOutputStream stream, ICursor currentPosition, ReadableByteChannel source, long numBytesToWrite) throws IOException;
	
	/**
	 * Read into a byte buffer, from start of file
	 * 
	 * @param stream
	 * @param target
	 * @param numBytesToFetch
	 * @return
	 * @throws IOException
	 */
	public ICursor putBytes(IOutputStream stream, ByteBuffer source) throws IOException;
	
	public ICursor putBytes(IOutputStream stream, ICursor currentPosition, ByteBuffer source) throws IOException;
	
	public void close(IStream handle) throws IOException;
	
	public void shutdown();
	
	public void deleteItem(IStoredObjectReference handle) throws IOException;
	
	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource) throws IOException;

	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource, 
			String folder) throws IOException;
	
	public IStoredObjectContainer getDefaultContainer();

	public IStoredObjectContainer getContainerFrom(IStoredObjectContainer root, IStoredObjectReference ref);

	public List<IStoredObjectReference> getFilesRecursivelyMatching(IStoredObjectContainer root, 
			String pattern);

	public List<IStoredObjectReference> getSubdirectories(IStoredObjectContainer dir);

	public List<IStoredObjectReference> getFilesMatching(IStoredObjectContainer dir, String pattern);

	public boolean createDirectory(IStoredObjectContainer db, String dirName);
	
	public boolean emptyDirectoryMeansMissing();

	public void moveObject(IStoredObjectReference cont, IStoredObjectReference ref) throws IOException;

	public void removeDirectory(IStoredObjectContainer db, String dirName);
}
