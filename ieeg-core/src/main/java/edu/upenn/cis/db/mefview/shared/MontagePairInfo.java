package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

public class MontagePairInfo {

	private EEGMontagePair pair;
	
	public MontagePairInfo(){}
	public MontagePairInfo(EEGMontagePair p){
		this.pair = p;
	}
	
	public EEGMontagePair getPair() {
		return pair;
	}
	
	public static final ProvidesKey<MontagePairInfo> KEY_PROVIDER = new ProvidesKey<MontagePairInfo>() {

		public Object getKey(MontagePairInfo item) {
			return (item == null) ? null : item.pair.toString();
		}
	};
	public static final TextColumn<MontagePairInfo> traceName = new TextColumn<MontagePairInfo> () {
		@Override
		public String getValue(MontagePairInfo object) {
			return object.pair.toString();
		}
	};
	
	
	
}
