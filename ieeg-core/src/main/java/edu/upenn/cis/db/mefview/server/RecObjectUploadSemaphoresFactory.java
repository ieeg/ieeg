/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.concurrent.Semaphore;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.util.concurrent.Striped;

public class RecObjectUploadSemaphoresFactory {

	public static class InProgressKey {
		private final String datasetId;
		private final String name;

		public InProgressKey(
				String datasetId,
				String name) {
			this.datasetId = checkNotNull(datasetId);
			this.name = checkNotNull(name);
		}

		public String getDatasetId() {
			return datasetId;
		}

		public String getName() {
			return name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((datasetId == null) ? 0 : datasetId.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof InProgressKey)) {
				return false;
			}
			InProgressKey other = (InProgressKey) obj;
			if (datasetId == null) {
				if (other.datasetId != null) {
					return false;
				}
			} else if (!datasetId.equals(other.datasetId)) {
				return false;
			}
			if (name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!name.equals(other.name)) {
				return false;
			}
			return true;
		}

	}

	private static class RecObjectUploadSemaphoresInProgressSupplier
			implements Supplier<Striped<Semaphore>> {
		@Override
		public Striped<Semaphore> get() {
			return Striped.semaphore(1000, 1);
		}
	}

	private static Supplier<Striped<Semaphore>> recObjectUploadSemaphoresInProgressSupplier =
			Suppliers
					.memoize(new RecObjectUploadSemaphoresInProgressSupplier());

	public static Striped<Semaphore> getRecObjectUploadSemaphoresInProgress() {
		return recObjectUploadSemaphoresInProgressSupplier.get();
	}

	private RecObjectUploadSemaphoresFactory() {}
}
