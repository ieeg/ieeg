/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.io;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Get the appropriate storage server container.
 * 
 * @author zives
 *
 */
public class StorageFactory {
	static FileSystemObjectServer source;
	static S3ObjectServer inbox;
	static IObjectServer target;
	private static final Logger logger = LoggerFactory
			.getLogger(StorageFactory.class);

	static Map<String, IObjectServer> auxServers = new HashMap<String, IObjectServer>();

	/**
	 * The local staging server (typically a temp directory on the local
	 * machine)
	 * 
	 * @return
	 */
	public static synchronized IObjectServer getStagingServer() {
		final String m = "getStagingServer()";
		if (source == null) {
			final String basePath = IvProps.getSourcePath();
			logger.trace(
					"{}: Creating file system object server with base path [{}]",
					m, basePath);
			source = new FileSystemObjectServer(basePath);
			source.init();
		}
		return source;
	}

	public static synchronized IObjectServer getUploadServer() {
		final String m = "getUploadServer()";
		// if (IvProps.isUsingS3())
		// return getStagingServer();

		if (inbox == null) {
			// final String basePath = IvProps.getSourcePath();
			// logger.trace("{}: Creating file system object server with base path [{}]",
			// m, basePath);
			inbox = new S3ObjectServer(IvProps.getIvProps().get("inboxBucket"));
			inbox.init();
		}
		return inbox;
	}

	public static synchronized IObjectServer getStagingServer(String basePath) {
		final String m = "getStagingServer()";
		if (source == null) {
			logger.trace(
					"{}: Creating file system object server with base path [{}]",
					m, basePath);
			source = new FileSystemObjectServer(basePath);
			source.init();
		}
		return source;
	}

	public static void register(String source, IObjectServer server) {
		auxServers.put(source, server);
	}

	/**
	 * The persistent (cloud/S3, usually) server
	 * 
	 * @return
	 */
	public static synchronized IObjectServer getPersistentServer() {
		final String m = "getPersistentServer()";

		if (target == null) {
			final String defaultPersistentPath = IvProps
					.getStorageFactoryDefaultPersistentPath();
			final String defaultPersistentBucket = IvProps
					.getStorageFactoryDefaultPersistentBucket();
			if (defaultPersistentPath != null
					&& defaultPersistentBucket != null) {
				throw new IllegalStateException(
						"Invalid configuration: "
								+ IvProps.STORAGE_FACTORY_PERSISTENT_DEFAULT_PATH
								+ " and "
								+ IvProps.STORAGE_FACTORY_PERSISTENT_DEFAULT_BUCKET
								+ " cannot both be non-null. Use "
								+ IvProps.STORAGE_FACTORY_PERSISTENT_DEFAULT_PATH
								+ " to specify a local file persistent object server or "
								+ IvProps.STORAGE_FACTORY_PERSISTENT_DEFAULT_BUCKET
								+ " to specify a S3 persistent object server.");
				// } else if (!IvProps.isUsingS3()) {
				// return getStagingServer();
			} else if (defaultPersistentPath != null) {
				target = new FileSystemObjectServer(defaultPersistentPath);
				logger.debug(
						"{}: Persistent IObjectServer is filesystem and is using default path [{}]",
						m,
						defaultPersistentPath);
			} else if (defaultPersistentBucket != null) {
				target = new S3ObjectServer(defaultPersistentBucket);
				logger.debug(
						"{}: Persistent IObjectServer is S3 and is using default bucket [{}]",
						m,
						defaultPersistentBucket);
			} else {
				// Both StorageFactory specific props are null, so fall back to
				// data bucket
				final String dataBucket = IvProps.getDataBucket();
				target = new S3ObjectServer(dataBucket);
				logger.debug(
						"{}: Properties for StorageFactory persistent IObjectServer not set. Using S3 and default bucket [{}]",
						m,
						dataBucket);
			}
			target.init();

		}
		return target;
	}

	/**
	 * Determines which server is most appropriate for the object
	 * 
	 * @param container
	 * @return
	 */
	public static synchronized IObjectServer getBestServerFor(
			IStoredObjectContainer container) {
		if (container != null
				&& (
				container.getBucket() == null && container.getFileDirectory() != null)) {
			return getStagingServer();
		}
		if (container != null
				&&
				(container.getBucket() != null && container.getFileDirectory() == null)) {
			IObjectServer persistentServer = getPersistentS3ServerOrThrowException(
					"Container "
							+ container
							+ " only specifies S3 bucket, but there is no S3ObjectServer configured");
			return persistentServer;
		}

		String dir = ".";

		if (container != null && container.getFileDirectory() != null)
			dir = container.getFileDirectory();

		File f = new File(dir);

		if (f.exists())
			return getStagingServer();
		else {
			IObjectServer persistentServer = getPersistentS3ServerOrThrowException(
					"Container "
							+ container
							+ " specifies a directory not located in the local filesystem, but there is no S3ObjectServer configured");
			return persistentServer;
		}
	}

	public static File getLocalFileFor(IStoredObjectReference handle) {
		IStoredObjectContainer container = handle.getDirBucketContainer();

		String dir = source.getBasePath();

		// See if we have an absolute path
		if (container != null
				&& (container.getFileDirectory().startsWith("/") ||
				(container.getFileDirectory().length() > 1 && container
						.getFileDirectory().charAt(1) == ':')))
			dir = container.getFileDirectory();
		else if (container != null && container.getFileDirectory() != null
				&& !container.getFileDirectory().startsWith(dir))
			dir = dir + container.getFileDirectory();

		File f = new File(dir, handle.getFilePath());

		return f;
	}

	/**
	 * Determines which server is most appropriate for the object
	 * 
	 * @param container
	 * @return
	 */
	public static synchronized IObjectServer getBestServerFor(
			IStoredObjectReference handle) {
		final String m = "getBestServerFor(IObjectHandle)";
		IStoredObjectContainer container = handle.getDirBucketContainer();

		if (container != null
				&& ((DirectoryBucketContainer) container).getSource() != null &&
				!((DirectoryBucketContainer) container).getSource().isEmpty()) {
			final IObjectServer auxServer = auxServers
					.get(((DirectoryBucketContainer) container).getSource());
			if (auxServer != null) {
				return auxServer;
			}
		}
		if (container != null
				&& (
				container.getBucket() == null && container.getFileDirectory() != null)) {
			logger.trace(
					"{}: Container has file directory [{}] but no S3 bucket. Returning staging server",
					m,
					container.getFileDirectory());
			return getStagingServer();
		}
		if (container != null
				&&
				(container.getBucket() != null && container.getFileDirectory() == null)) {
			IObjectServer persistentServer = getPersistentS3ServerOrThrowException(
					"Container for "
							+ handle
							+ " only specifies S3 bucket, but there is no S3ObjectServer configured");
			logger.trace(
					"{}: Container has S3 bucket [{}] but no file directory. Returning persistent server",
					m,
					container.getBucket());
			// nasty side effect
			handle.setDirBucketContainer(persistentServer.getDefaultContainer());

			return persistentServer;
		}

		// Null instead of "." in case handle.getFilePath() is absolute
		getStagingServer();
		// String dir = source.getBasePath();
		//
		// if (container != null &&
		// (container.getFileDirectory().startsWith("/") ||
		// (container.getFileDirectory().length() > 1 &&
		// container.getFileDirectory().charAt(1) == ':')))
		// dir = container.getFileDirectory();
		// else if (container != null && container.getFileDirectory() != null &&
		// !container.getFileDirectory().startsWith(dir))
		// dir = dir + container.getFileDirectory();
		//
		// File f = new File(dir, handle.getFilePath());
		File f = getLocalFileFor(handle);
		// File f2 = new File(handle.getFilePath());

		if (f.exists()) {// || f2.exists())
			logger.trace("{}: File [{}] exists, returning staging server",
					m,
					f);
			return getStagingServer();
		} else {
			IObjectServer persistentServer = getPersistentS3ServerOrThrowException(
					"Handle "
							+ handle
							+ " specifies a file not located in the local filesystem, but there is no S3ObjectServer configured");
			logger.trace(
					"{}: File [{}] does not exist, returning persistent server",
					m,
					f);
			handle.setDirBucketContainer(persistentServer.getDefaultContainer());
			return persistentServer;
		}
	}

	/**
	 * Returns the persistent server if it is an instance of
	 * {@link S3ObjectServer} or throw {@link IllegalArgumentException}
	 * 
	 * @param illegalArgMessage the message to use for IllegalArgumentException
	 * @return the persistent server if it is an instance of
	 *         {@link S3ObjectServer}
	 * 
	 * @throws IllegalArgumentException if the persistent server is not an
	 *             instance of {@link S3ObjectServer}
	 */
	private static IObjectServer getPersistentS3ServerOrThrowException(
			String illegalArgMessage) {
		final IObjectServer persistentServer = getPersistentServer();
		if (persistentServer instanceof S3ObjectServer) {
			return persistentServer;
		}
		throw new IllegalArgumentException(illegalArgMessage);
	}

	/**
	 * Returns an IObjectServer for the Inbox S3 bucket
	 * 
	 * @return
	 */
	public static IObjectServer getS3Inbox() {
		return new S3ObjectServer(IvProps.getInboxBucket());
	}

	/**
	 * Set a persistent server for tests. Remember to call
	 * setPersistentServer(null) to clear it out after test.
	 * 
	 * @param testServer
	 */
	@VisibleForTesting
	public static void setPersistentServer(@Nullable IObjectServer testServer) {
		if (target != null) {
			target.shutdown();
		}
		target = testServer;
	}
}
