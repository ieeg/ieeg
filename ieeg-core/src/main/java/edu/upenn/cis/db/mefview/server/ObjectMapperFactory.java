/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * Configures an ObjectMapper with the required Mix-ins. Also provides a writer
 * which will filter out password properties so that json can be logged without
 * passwords.
 * 
 * @author John Frommeyer
 *
 */
public final class ObjectMapperFactory {

	private ObjectMapperFactory() {}

	private static final ObjectMapper instance = newObjectMapper();

	// We can add additional filters to this provider if we have more password
	// properties
	private static final FilterProvider passwordFilters = new SimpleFilterProvider()
			.addFilter(
					AwsCredentialsMixIn.AWS_SECRET_KEY_FILTER,
					SimpleBeanPropertyFilter
							.serializeAllExcept(AwsCredentialsMixIn.AWS_SECRET_KEY_PROPERTY));

	public static ObjectMapper getObjectMapper() {
		return instance;
	}

	/**
	 * Returns a new ObjectWriter that does not serialize any passwords filtered
	 * by this factory.
	 * 
	 * @return
	 */
	public static ObjectWriter newPasswordFilteredWriter() {
		return instance.writer(passwordFilters);
	}

	private static ObjectMapper newObjectMapper() {
		final ObjectMapper mapper = new ObjectMapper();
		mapper.addMixInAnnotations(AWSCredentials.class,
				AwsCredentialsMixIn.class);
		mapper.addMixInAnnotations(BasicAWSCredentials.class,
				BasicAwsCredentialsMixIn.class);
		// By default we want no filtering, but need to set a filter provider
		// otherwise we get an error since a JsonFilter is defined for
		// AwsCredentialsMixIn.
		// The provider below will just ignore the fact that it does not have a
		// filter with id AWS_SECRET_KEY_FILTER
		mapper.setFilters(new SimpleFilterProvider().setFailOnUnknownId(false));
		return mapper;
	}

}
