/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.timeseries.meta;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.db.habitat.daemons.CachingRequestor;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.IMessage;
import edu.upenn.cis.db.habitat.daemons.IMessageBroker;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Request metadata (header) info for a time series channel, using the asynchronous
 * communications infrastructure.
 * 
 * @author zives
 *
 */
public class TimeSeriesMetadataRequestor extends 
	CachingRequestor<TimeSeriesMetadataRequestor.Request, ChannelSpecifier, ChannelInfoDto> {
	
	public static class Request implements IMessage<ChannelSpecifier> {
		ChannelSpecifier specifier;

		public Request(ChannelSpecifier specifier) {
			this.specifier = specifier;
		}

		@Override
		public String getChannel() {
			return TimeSeriesMetadataResponder.CHANNEL;
		}

		@Override
		public String getKey() {
			return specifier.getId();
		}

		@Override
		public String getSerializedParameters() {
			try {
				return JsonKeyValueMapper.getMapper().createValue(specifier);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		@Override
		public ChannelSpecifier getParameters() {
			return specifier;
		}

	}
	
	public static final String CHANNEL = TimeSeriesMetadataRequestor.class.getCanonicalName();//"timeSeriesMeta"; 
	
	public TimeSeriesMetadataRequestor(
//			FutureRegistry<TimeSeriesMetadataRequestor.Request, ChannelInfoDto> registry,
			IMessageBroker<TimeSeriesMetadataRequestor.Request, ChannelSpecifier> requestSender,
			IMessageBroker<TimeSeriesMetadataRequestor.Request, ChannelInfoDto> requestReceiver,
			int cacheElements) {
		super(requestSender, requestReceiver, cacheElements);
		
		register(CHANNEL, null, new FutureRegistry.FutureResult<TimeSeriesMetadataRequestor.Request, ChannelInfoDto>() {

			@Override
			public ChannelInfoDto call() throws Exception {
				System.out.println("Received " + super.getResult());
				return super.getResult();
			}

			
		}, new FutureRegistry.FutureResult<TimeSeriesMetadataRequestor.Request, String>() {

			@Override
			public String call() throws Exception {
				System.out.println("Error " + super.getResult());
				// TODO Auto-generated method stub
				return super.getResult();
			}
			
		}, true);
	}

	/**
	 * Requests the channel info synchronously
	 * 
	 * @param channel
	 * @return
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public ChannelInfoDto getChannelInfo(ChannelSpecifier channel) 
			throws TimeoutException, InterruptedException, ExecutionException {
		return requestSync(new Request(channel), -1);
	}

}
