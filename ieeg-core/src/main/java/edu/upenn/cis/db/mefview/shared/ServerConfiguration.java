package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;

@GwtCompatible(serializable=true)
public class ServerConfiguration implements Serializable {
	boolean isUniversity;
	boolean needsResearchAgreement;
	boolean needsAuthorizedParty;
	String serverName;
	String serverUrl;
	Map<String,String> otherParameters;
	boolean keywordSearch = false;
	private int dygraphLogLevel;
	
	List<AnnotationDefinition> annotationDefinitions;
	
	public ServerConfiguration() {}
	
	public ServerConfiguration(boolean isUniversity,
			boolean needsResearchAgreement, boolean needsAuthorizedParty,
			String serverName, String serverUrl, int dygraphLogLevel, Map<String,String> otherParameters) {
		super();
		this.isUniversity = isUniversity;
		this.needsResearchAgreement = needsResearchAgreement;
		this.needsAuthorizedParty = needsAuthorizedParty;
		this.serverName = serverName;
		this.serverUrl = serverUrl;
		this.dygraphLogLevel = dygraphLogLevel;
		this.otherParameters = otherParameters;
	}
	
	public int getDygraphLogLevel() {
		return dygraphLogLevel;
	}
	public void setDygraphLogLevel(int logLevel) {
		this.dygraphLogLevel = logLevel;
	}
	public boolean isUniversity() {
		return isUniversity;
	}
	public void setUniversity(boolean isUniversity) {
		this.isUniversity = isUniversity;
	}
	public boolean needsResearchAgreement() {
		return needsResearchAgreement;
	}
	public void setNeedsResearchAgreement(boolean needsResearchAgreement) {
		this.needsResearchAgreement = needsResearchAgreement;
	}
	public boolean isNeedsAuthorizedParty() {
		return needsAuthorizedParty;
	}
	public void setNeedsAuthorizedParty(boolean needsAuthorizedParty) {
		this.needsAuthorizedParty = needsAuthorizedParty;
	}
	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Map<String, String> getOtherParameters() {
		return otherParameters;
	}

	public void setOtherParameters(Map<String, String> otherParameters) {
		this.otherParameters = otherParameters;
	}

	public boolean isNeedsResearchAgreement() {
		return needsResearchAgreement;
	}

	public boolean isKeywordSearch() {
		return keywordSearch;
	}

	public void setKeywordSearch(boolean keywordSearch) {
		this.keywordSearch = keywordSearch;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public List<AnnotationDefinition> getAnnotationDefinitions() {
		return annotationDefinitions;
	}

	public void setAnnotationDefinitions(List<AnnotationDefinition> annotationDefinitions) {
		this.annotationDefinitions = annotationDefinitions;
	}

	
}
