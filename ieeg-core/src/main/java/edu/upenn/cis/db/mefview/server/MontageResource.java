/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Sets.newHashSet;

import java.net.URI;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.LayerDto;
import edu.upenn.cis.braintrust.shared.MontageDto;
import edu.upenn.cis.braintrust.shared.MontageId;
import edu.upenn.cis.braintrust.shared.MontageIdAndVersion;
import edu.upenn.cis.braintrust.shared.MontagedChannelDto;
import edu.upenn.cis.braintrust.shared.ReferenceChannelDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.db.mefview.services.IMontageResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.Layer;
import edu.upenn.cis.db.mefview.services.LayerInfos;
import edu.upenn.cis.db.mefview.services.Montage;
import edu.upenn.cis.db.mefview.services.MontagedChannel;
import edu.upenn.cis.db.mefview.services.ReferenceChannel;

public final class MontageResource implements IMontageResource {

	private final Logger logger =
			LoggerFactory.getLogger(getClass());
	private final IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private final User user;

	public MontageResource(@Context HttpServletRequest req) {
		String m = "MontageResource(...)";
		this.user = (User) req.getAttribute("user");
		logger.debug("{}: user: ", m, user.getUsername());
	}

//	@Override
//	public Response createLayer(
//			long montageIdLong,
//			Layer layer) {
//		String m = "createLayer(...)";
//		try {
//			MontageId montageId = new MontageId(montageIdLong);
//			LayerDto layerDto =
//					dsServer.createLayer(user, montageId, layer.getName());
//			Layer layerRet = new Layer(
//					layerDto.getName(),
//					layerDto.getId().get().getId(),
//					layerDto.getAnnotationCount().get());
//			Response response =
//					Response.created(
//							URI.create("/layers/" + layerRet.getId()))
//							.entity(layerRet)
//							.build();
//			return response;
//		} catch (AuthorizationException e) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//		} catch (RuntimeException t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		} catch (Error t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//
//		}
//	}

//	@Override
//	public void deleteMontage(
//			String eTag,
//			long montageId) {
//		String m = "deleteMontage(...)";
//		try {
//			Integer eTagInt = null;
//			boolean checkVersion = false;
//			try {
//				if (eTag != null) {
//					eTagInt = Integer.valueOf(eTag);
//					checkVersion = true;
//				} else {
//					eTagInt = -1;
//					checkVersion = false;
//				}
//			} catch (NumberFormatException nbe) {
//				eTagInt = -1;
//				checkVersion = true;
//			}
//
//			dsServer.deleteMontage(
//					user,
//					new MontageIdAndVersion(montageId, eTagInt),
//					checkVersion);
//		} catch (AuthorizationException e) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//
//		} catch (FailedVersionMatchException e) {
//			throw IeegWsExceptionUtil
//					.logAndConvertToPreconditionFailed(
//							e,
//							m,
//							logger);
//		} catch (RuntimeException t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		} catch (Error t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//
//		}
//	}

//	@Override
//	public LayerInfos getLayerInfos(
//			long montageIdLong) {
//		String m = "getLayers(...)";
//		try {
//			MontageId montageId = new MontageId(montageIdLong);
//			Set<LayerDto> layerDtos = dsServer.getLayers(user, montageId);
//			Set<Layer> layerSet = newHashSet();
//			for (LayerDto layerDto : layerDtos) {
//				Layer layer = new Layer(
//						layerDto.getName(),
//						layerDto.getId().get().getId(),
//						layerDto.getAnnotationCount().get());
//				layerSet.add(layer);
//			}
//			LayerInfos layers = new LayerInfos(layerSet);
//			return layers;
//		} catch (AuthorizationException e) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//		} catch (RuntimeException t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		} catch (Error t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//
//		}
//	}

//	@Override
//	public Response getMontage(long montageId) {
//		String m = "getMontage(...)";
//		try {
//			MontageDto montageDto = dsServer.getMontage(
//					user,
//					new MontageId(montageId));
//			Set<MontagedChannel> montageChs = newHashSet();
//			for (MontagedChannelDto montagedChDto : montageDto
//					.getMontagedChannels()) {
//				Set<ReferenceChannel> refChs = newHashSet();
//				for (ReferenceChannelDto refChDto : montagedChDto
//						.getReferenceChannels()) {
//					ReferenceChannel refCh = new ReferenceChannel(
//							refChDto.getChannelId().getId(),
//							refChDto.getCoefficient());
//					refChs.add(refCh);
//				}
//				MontagedChannel montagedCh = new MontagedChannel(
//						montagedChDto.getName(),
//						montagedChDto.getChannelId().getId(),
//						refChs,
//						montagedChDto.getId().get().getId());
//				montageChs.add(montagedCh);
//			}
//			Montage montage = new Montage(
//					montageDto.getName(),
//					montageChs,
//					montageDto.getId().get().getId(),
//					String.valueOf(montageDto.getId().get().getVersion()),
//					montageDto.getParentId().getId());
//			return Response
//					.ok(montage)
//					.tag(String.valueOf(montageDto.getId().get().getVersion()))
//					.build();
//		} catch (AuthorizationException e) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//		} catch (RuntimeException t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		} catch (Error t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		}
//	}

//	@Override
//	public Montage updateMontage(
//			@PathParam("montageId") Long montageId,
//			@HeaderParam("ETag") String eTag,
//			Montage montage) {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
