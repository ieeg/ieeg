/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import edu.upenn.cis.db.mefview.services.IeegWsExceptionMapper;
import edu.upenn.cis.db.mefview.services.SigServerRequestFilter;
import edu.upenn.cis.thirdparty.ClosingInputStreamProvider;

/**
 * REST web services
 * 
 * @author zives
 */
public class IEEGServices extends Application {
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public IEEGServices() {
		classes.add(TimeSeriesResource.class);
		classes.add(MontageResource.class);
		classes.add(LayerResource.class);
		classes.add(DatasetResource.class);
		classes.add(ObjectResource.class);
		classes.add(ConfigResource.class);
		classes.add(CredentialResource.class);
		classes.add(RegistrationResource.class);
		classes.add(IeegWsExceptionMapper.class);
		classes.add(ClosingInputStreamProvider.class);
		classes.add(SigServerRequestFilter.class);
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}
