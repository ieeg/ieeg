/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Function;
import com.google.common.collect.Sets;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

@JsonIgnoreProperties(ignoreUnknown=true)
public final class TraceInfo extends PresentableMetadata implements INamedTimeSegment {
	
	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();
			
	IDataset parent;
//	static MetadataFormatter presenter = null;
	String location = "habitat://main";
	
	double baseline = 0;
	String modality = BuiltinDataTypes.EEG;
	double multiplier = 0.001;
	String units = "V";
	long timeOffset = 0;

	static {
		valueMap.put("startTime", VALUE_TYPE.DOUBLE);
		valueMap.put("duration", VALUE_TYPE.DOUBLE);
		valueMap.put("voltageConversionFactor", VALUE_TYPE.DOUBLE);
		valueMap.put("sampleRate", VALUE_TYPE.DOUBLE);
		valueMap.put("label", VALUE_TYPE.STRING);
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("subject", VALUE_TYPE.STRING);
		valueMap.put("comments", VALUE_TYPE.STRING);
		valueMap.put("institution", VALUE_TYPE.STRING);
		valueMap.put("acquisitionSystem", VALUE_TYPE.STRING);
		valueMap.put("minSampleValue", VALUE_TYPE.INT);
		valueMap.put("maxSampleValue", VALUE_TYPE.INT);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Function<TraceInfo, String> getLabel = new Function<TraceInfo, String>() {
		@Override
		public String apply(TraceInfo input) {
			return input.getLabel();
		}
	};
	
	public TraceInfo() {}
	
	public TraceInfo(IDataset snapshot,String channelRevId) {
		this.revId = channelRevId;
		parent = snapshot;
	}
	
	public TraceInfo(IDataset snapshot,
			long startTime, double duration, double sampleRate, double voltageConversion, 
			int minSampleValue, int maxSampleValue, String institution,	String acquisitionSystem,
			String channelComments, String subjectId, String label, String revId) {
		this.startTime = startTime;
		this.duration = duration;
		this.startTime = startTime;
		this.voltageConversion = voltageConversion;
		this.label = label;
		this.revId = revId;
		this.minSampleValue = minSampleValue;
		this.maxSampleValue = maxSampleValue;
		this.institution = institution;
		this.acquisitionSystem = acquisitionSystem;
		this.channelComments = channelComments;
		this.subjectId = subjectId;
		this.sampleRate = sampleRate;

		parent = snapshot;
	}
	
	@JsonIgnore
	public String getId() {
		return revId;
	}
	
	public void setId(String revId) {
		this.revId = revId;
	}
	
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public double getSampleRate() {
		return sampleRate;
	}
	public void setSampleRate(double sampleRate) {
		this.sampleRate = sampleRate;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public double getVoltageConversion() {
		return voltageConversion;
	}
	public void setVoltageConversion(double voltageConversion) {
		this.voltageConversion = voltageConversion;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getRevId() {
		return revId;
	}
	public void setRevId(String revId) {
		this.revId = revId;
	}
	
	
	public int getMinSampleValue() {
		return minSampleValue;
	}

	public void setMinSampleValue(int minSampleValue) {
		this.minSampleValue = minSampleValue;
	}

	public int getMaxSampleValue() {
		return maxSampleValue;
	}

	public void setMaxSampleValue(int maxSampleValue) {
		this.maxSampleValue = maxSampleValue;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getAcquisitionSystem() {
		return acquisitionSystem;
	}

	public void setAcquisitionSystem(String acquisitionSystem) {
		this.acquisitionSystem = acquisitionSystem;
	}

	public String getChannelComments() {
		return channelComments;
	}

	public void setChannelComments(String channelComments) {
		this.channelComments = channelComments;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}


	double duration;
	double sampleRate;
	long startTime;
	double voltageConversion;
	String label;
	String revId;

	int minSampleValue;
	int maxSampleValue;
	String institution;
	String acquisitionSystem;
	String channelComments;
	String subjectId;
	
	/**
	 * Sort order on labels
	 */
	public int compareTo(TraceInfo o) {
		if (!(o instanceof TraceInfo))
			throw new RuntimeException("Incomparable!");
		
		return label.compareTo(((TraceInfo)o).label);
	}

	public static final ProvidesKey<INamedTimeSegment> KEY_PROVIDER = new ProvidesKey<INamedTimeSegment>() {

		public Object getKey(INamedTimeSegment item) {
			return (item == null) ? null : item.getId();//.getFriendlyName();
		}
		
	};
	public static final TextColumn<INamedTimeSegment> traceName = new TextColumn<INamedTimeSegment> () {
		@Override
		public String getValue(INamedTimeSegment object) {
			return object.getLabel();
		}
	};
	public static final TextColumn<INamedTimeSegment> traceComments = new TextColumn<INamedTimeSegment> () {
		@Override
		public String getValue(INamedTimeSegment object) {
			String ret = object.getChannelComments();
			
			if (ret.equals("not entered"))
				return "";
			else
				return ret;
		}
	};
	public static final TextColumn<TraceInfo> traceRevID = new TextColumn<TraceInfo> () {
		@Override
		public String getValue(TraceInfo object) {
			return object.getRevId();
		}
	};

	@JsonIgnore
	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		if (key.equals("startTime"))
			return ((double)getStartTime());
		if (key.equals("duration"))
			return (getDuration());
		if (key.equals("voltageConversionFactor"))
			return (getVoltageConversion());
		if (key.equals("sampleRate"))
			return (getSampleRate());

		return null;
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		if (key.equals("label"))
			return getLabel();
		if (key.equals("id"))
			return getRevId();
		if (key.equals("subject"))
			return getSubjectId();
		if (key.equals("comments"))
			return getChannelComments();
		if (key.equals("institution"))
			return getInstitution();
		if (key.equals("acquisitionSystem"))
			return getAcquisitionSystem();

		return null;
	}

	@JsonIgnore
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@JsonIgnore
	@Override
	public Integer getIntegerValue(String key) {
		if (key.equals("minSampleValue"))
			return (getMinSampleValue());
		if (key.equals("maxSampleValue"))
			return (getMaxSampleValue());
		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public List<GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public IDataset getParent() {
		return parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof IDataset)
			parent = (IDataset)p;
	}

	@JsonIgnore
	@Override
	public String getFriendlyName() {
		return getLabel();
	}

	@JsonIgnore
	@Override
	public List<String> getCreators() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreators();
		return null;
	}

	@JsonIgnore
	@Override
	public String getContentDescriptor() {
		return getLabel();
	}

	@JsonIgnore
	@Override
	public Date getCreationTime() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreationTime();
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@JsonIgnore
	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getUserPermissions();
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.EEG);
	}

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Timeseries.name();
	}

	public double getBaseline() {
		return baseline;
	}

	public String getDataModality() {
		return modality;
	}

	public double getMultiplier() {
		return this.multiplier;
	}

	public String getUnits() {
		return this.units;
	}

	public void setBaseline(double baseline) {
		this.baseline = baseline;
	}

	public void setDataModality(String modality) {
		this.modality = modality;
	}

	public void setMultiplier(double m) {
		this.multiplier = m;
	}

	public void setUnits(String units) {
		this.units = units;
	}
	
//	@Override
//	public MetadataFormatter getFormatter() {
//		return presenter;
//	}
//	
//	public static void setPresenter(MetadataFormatter presenter) {
//		TraceInfo.presenter = presenter;
//	}
//
//	public static MetadataFormatter getDefaultPresenter() {
//		return presenter;
//	}

	@JsonIgnore
	@Override
	public boolean isLeaf() {
		return true;
	}

	@JsonIgnore
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public MetadataDrillDown getDrillDown() {
//		return null;
//	}
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Channel";
	}

	@Override
	public long getTimeShift() {
		return timeOffset;
	}

	public void setTimeShift(long timeOffset) {
		this.timeOffset = timeOffset;
	}

	@JsonIgnore
	@Override
	public double getScale() {
		return getVoltageConversion();
	}

	@Override
	public boolean isEventBased() {
		return false;
	}
}
