package edu.upenn.cis.db.habitat.persistence.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import edu.upenn.cis.braintrust.model.UserSubmissionRelationship;
import edu.upenn.cis.db.mefview.shared.TaskSubmission;

@Mapper(
		uses={
				SimpleTypeMapper.class, 
//				UserMappingFactory.class
		}
		)
public interface UserSubmissionMapper {
	UserSubmissionMapper INSTANCE = Mappers.getMapper ( UserSubmissionMapper.class );

//	@Mappings({
//		@Mapping(target = "creators", 
//				expression = "java(userMappingFactory.getUserEntityFor(task.getUser()))" )
//	})
//	UserSubmissionRelationship taskSubmissionToUserSubmission(TaskSubmission task);

//	@InheritInverseConfiguration
//	TaskSubmission userSubmissionRelationshipToTaskSubmission(UserSubmissionRelationship taskEntity);
}
