/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToAuthzFailure;
import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToInternalError;

import java.util.Set;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.LayerDto;
import edu.upenn.cis.braintrust.shared.LayerId;
import edu.upenn.cis.braintrust.shared.LayerPatchDto;
import edu.upenn.cis.braintrust.shared.MontagedChAnnotationDto;
import edu.upenn.cis.braintrust.shared.MontagedChId;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.services.ILayerResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.Layer;
import edu.upenn.cis.db.mefview.services.LayerPatch;
import edu.upenn.cis.db.mefview.services.MontagedChAnnotation;
import edu.upenn.cis.db.mefview.services.MontagedChAnnotations;

public final class LayerResource implements ILayerResource {

//	private final IDataSnapshotServer dsServer =
//			DataSnapshotServerFactory.getDataSnapshotServer();
//	private final Logger logger = LoggerFactory.getLogger(getClass());
//	private final User user;
//
//	public LayerResource(@Context HttpServletRequest req) {
//		String m = "LayerResource(...)";
//		this.user = (User) req.getAttribute("user");
//		logger.debug("{}: user: ", m, user);
//	}
//
//	@Override
//	public void deleteLayer(long layerId) {
//		String m = "deleteLayer(...)";
//		try {
//			LayerId layerIdObj =
//					new LayerId(layerId);
//			dsServer.deleteLayer(user, layerIdObj);
//		} catch (AuthorizationException ae) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					ae,
//					m,
//					logger);
//		} catch (RuntimeException rte) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					rte,
//					m,
//					logger);
//		} catch (Error e) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					e,
//					m,
//					logger);
//
//		}
//	}
//
//	@Override
//	public Layer patchLayer(
//			long layerId,
//			LayerPatch layerPatch) {
//		String m = "patchLayer(...)";
//		try {
//			LayerId layerIdDto = new LayerId(layerId);
//
//			Set<MontagedChAnnotationDto> createAnnDtos = newHashSet();
//			for (MontagedChAnnotation ann : layerPatch.getCreateAnnotations()) {
//				Set<MontagedChId> montageIds =
//						MontagedChId.of(ann.getMontagedChannelIds());
//				MontagedChAnnotationDto annDto = new MontagedChAnnotationDto(
//						layerIdDto,
//						ann.getType(),
//						ann.getDescription(),
//						ann.getStartOffsetUsecs(),
//						ann.getEndOffsetUsecs(),
//						ann.getColor(),
//						user.getUserId(),
//						montageIds);
//				createAnnDtos.add(annDto);
//			}
//
//			LayerPatchDto layerPatchDto =
//					new LayerPatchDto(
//							layerIdDto,
//							layerPatch.getName(),
//							createAnnDtos);
//
//			LayerDto layerDtoRet = dsServer.patchLayer(user, layerPatchDto);
//			Layer layerRet = new Layer(
//					layerDtoRet.getName(),
//					layerDtoRet.getId().get().getId(),
//					layerDtoRet.getAnnotationCount().get());
//			return layerRet;
//		} catch (AuthorizationException e) {
//			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//		} catch (RuntimeException t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		} catch (Error t) {
//			throw IeegWsExceptionUtil.logAndConvertToInternalError(
//					t,
//					m,
//					logger);
//		}
//	}
//
//	@Override
//	public MontagedChAnnotations getAnnotations(
//			long layerId,
//			long startOffsetUsecs,
//			@Nullable Boolean ltStartOffsetUsecs,
//			int firstResult,
//			int maxResults) {
//		final String m = "getAnnotations(...)";
//		try {
//			checkArgument(
//					maxResults <= 1000,
//					"MaxCount [%s] exceeds limit of 1000",
//					maxResults);
//
//			Set<MontagedChAnnotationDto> montagedChAnnDtos = null;
//			LayerId layerIdDto = new LayerId(layerId);
//			if (ltStartOffsetUsecs == null || !ltStartOffsetUsecs) {
//				montagedChAnnDtos = dsServer.getMontagedChAnnotations(
//						user,
//						layerIdDto,
//						startOffsetUsecs,
//						firstResult,
//						maxResults);
//			} else {
//				montagedChAnnDtos =
//						dsServer.getMontagedChAnnotationsLtStartTime(
//								user,
//								layerIdDto,
//								startOffsetUsecs,
//								firstResult,
//								maxResults);
//			}
//			Set<MontagedChAnnotation> montagedChAnns = newLinkedHashSet();
//
//			for (MontagedChAnnotationDto montagedChAnnDto : montagedChAnnDtos) {
//				Set<Long> montagedChIds =
//						MontagedChAnnotationDto
//								.toLongIds(montagedChAnnDto.getMontagedChIds());
//				MontagedChAnnotation montagedChAnn = new MontagedChAnnotation(
//						layerId,
//						montagedChAnnDto.getType(),
//						montagedChAnnDto.getDescription().orNull(),
//						montagedChAnnDto.getStartOffsetUsecs(),
//						montagedChAnnDto.getEndOffsetUsecs(),
//						montagedChAnnDto.getColor().orNull(),
//						montagedChIds);
//				montagedChAnns.add(montagedChAnn);
//			}
//
//			return new MontagedChAnnotations(montagedChAnns);
//		} catch (AuthorizationException e) {
//			throw logAndConvertToAuthzFailure(
//					e,
//					m,
//					logger);
//		} catch (RuntimeException e) {
//			throw logAndConvertToInternalError(
//					e,
//					m,
//					logger);
//		} catch (Error e) {
//			throw logAndConvertToInternalError(
//					e,
//					m,
//					logger);
//		}
//
//	}
}
