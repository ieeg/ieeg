package edu.upenn.cis.db.mefview.server;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.TsEventType;

public interface IEventServer {
	public Map<TsEventType, List<INamedTimeSegment>> getSegmentMap(
			IDataset dataset, 
			IStoredObjectReference reference,
			List<INamedTimeSegment> segments,
			double seriesStart,
			double start, double end, double samplingPeriod) throws IOException;

	public Map<INamedTimeSegment, List<IDetection>> getEvents(
			IDataset dataset, 
			IStoredObjectReference reference, 
			List<INamedTimeSegment> segments,
			TsEventType partitionOn)
			throws IOException;

	SortedSet<INamedTimeSegment> getSegments(IDataset dataset, IStoredObjectReference reference,
			List<INamedTimeSegment> channels,
			double seriesStart,
			double start, double end, double samplingPeriod) throws IOException;

	public List<INamedTimeSegment> getEventChannels(IDataset dataset, List<INamedTimeSegment> channels, 
			IStoredObjectReference handle) throws IOException;

	Map<INamedTimeSegment, int[]> getEventVector(IDataset dataset, IStoredObjectReference reference,
			Collection<INamedTimeSegment> channels, double seriesStart, 
			double start, double end, double samplingPeriod, boolean minMax)
					throws IOException;
}
