/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.exceptions;

import java.util.concurrent.ExecutionException;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

@GwtCompatible(serializable=true)
public class RequestFailedException extends ExecutionException implements JsonTyped {

	public RequestFailedException() {
		// TODO Auto-generated constructor stub
	}

	public RequestFailedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RequestFailedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RequestFailedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
