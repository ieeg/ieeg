/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.views;

public class TextView implements IDataView {

	int startLine;
	int endLine;
	
	public TextView() {}
	
	public TextView(int startLine, int endLine) {
		this.startLine = startLine;
		this.endLine = endLine;
	}

	@Override
	public boolean isCompatibleWith(IDataView two) {
		return (two instanceof TextView);
	}

	@Override
	public boolean overlapsWith(IDataView two) {
		if (isCompatibleWith(two)) {
			TextView t = (TextView)two;
			
			return startLine <= t.endLine && endLine >= t.startLine;  
		}
		return false;
	}

	@Override
	public boolean contains(IDataView two) {
		if (isCompatibleWith(two)) {
			TextView t = (TextView)two;
			
			return startLine <= t.startLine && endLine >= t.endLine;  
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Integer.valueOf(startLine).hashCode() ^
				Integer.valueOf(endLine).hashCode();
	}

	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	
}
