/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.mapping;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.db.mefview.server.mapping.database.SnapshotMappingFactory;
import edu.upenn.cis.db.mefview.server.mapping.database.TaskMappingFactory;
import edu.upenn.cis.db.mefview.server.mapping.database.UserMappingFactory;
import edu.upenn.cis.db.mefview.shared.Task;

@Mapper(
		uses={
				SimpleTypeMapper.class, 
				SnapshotMappingFactory.class, 
				TaskMappingFactory.class, 
				UserMappingFactory.class})
public interface UserTaskMapper {
	UserTaskMapper INSTANCE = Mappers.getMapper ( UserTaskMapper.class );
	
	@Mappings({
		@Mapping(source = "dbId", target = "id"),
		@Mapping(source = "taskId", target = "pubId"),
		@Mapping(target = "extAcl", expression = "java(userMappingFactory.getAclsIdempotent(task.getUser(), task.getTaskId()))")
	})
	UserTaskEntity taskToUserTaskEntity(Task task);

	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "configuration", expression = "java(simpleTypeMapper.asJsonKeyValueSet(taskEntity.getConfiguration()))"  ),
		@Mapping(target = "formParameters", expression = "java(simpleTypeMapper.asJsonKeyValueSet(taskEntity.getFormParameters()))"  ),
		@Mapping(target = "validationOperation", expression = "java(simpleTypeMapper.asJsonKeyValueSet(taskEntity.getValidationOperation()))" ),
		@Mapping(target = "parent", expression = "java(null)" ),
		@Mapping(target = "children", expression = "java(new ArrayList<edu.upenn.cis.db.mefview.shared.PresentableMetadata>())" ),
		@Mapping(target = "userPermissions", expression = "java(taskMappingFactory.getUserPermissions(taskEntity))" ),
		@Mapping(target = "worldPermissions", expression = "java(taskMappingFactory.getWorldPermissions(taskEntity))" ),
		@Mapping(target = "projectPermissions", expression = "java(taskMappingFactory.getProjectPermissions(taskEntity))" ),
		
		// This needs to be set to the current logged-in user if we want to map back!
		@Mapping(target = "user", expression = "java(null)"),
		@Mapping(target="allMetadata", ignore=true),
		@Mapping(target="childMetadata", ignore=true),
//		@Mapping(target="formatter", ignore=true),
		@Mapping(target="keys", ignore=true),
		@Mapping(target="associatedDataTypes", ignore=true),
	})
	Task userTaskEntityToTask(UserTaskEntity taskEntity);
}
