/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Sets;

import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.braintrust.shared.UserId;

@GwtCompatible(serializable=true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Task extends PresentableMetadata implements Serializable, JsonTyped {
	Long dbId = null; 
	List<String> creators = new ArrayList<String>(); 
	String taskName;
	String taskId;
	Date activeDate;
	Date dueDate;
	Date finalDate;
	
//	static MetadataFormatter formatter = null;
	
	String description;
	
	WebLink url;
	
	String project;
	
	List<PresentableMetadata> resources;
	
	List<String> submissionsAllowed;
	
	List<String> patternsAllowed;
	String location = "habitat://main";
	
	JsonKeyValueSet configuration;
	JsonKeyValueSet validationOperation;
	
	JsonKeyValueSet formParameters;
	
	UserId theUser = null;
	Map<String, Set<String>> userPermissions;
	Set<String> worldPermissions;
	Map<String, Set<String>> projectPermissions;
	
	public Task() {
		dueDate = null;//Optional.absent();
		finalDate = null;//Optional.absent();
		
		url = null;//Optional.absent();
		
		project = null;//Optional.absent();
		
		configuration = null;//Optional.absent();
		validationOperation = null;//Optional.absent();
		
		formParameters = null;//Optional.absent();

		resources = new ArrayList<PresentableMetadata>();
		submissionsAllowed = new ArrayList<String>();
		patternsAllowed = new ArrayList<String>();
	}
	
	
	
	public Task(
			@NotNull String taskId,
			@NotNull String taskName,
			@NotNull Date activeDate, 
			@Nullable Date dueDate, 
			@Nullable Date finalDate,
			@NotNull String description, 
			@Nullable WebLink url, 
			@Nullable String project, 
			@NotNull List<PresentableMetadata> resources,
			@NotNull List<String> submissionsAllowed, 
			@Nullable List<String> patternsAllowed,
			@Nullable JsonKeyValueSet configuration, 
			@Nullable JsonKeyValueSet validationOperation,
			@Nullable JsonKeyValueSet formParameters) {
		super();
		this.taskId = taskId;
		this.activeDate = activeDate;
		this.dueDate = (dueDate);
		this.finalDate = (finalDate);
		this.description = description;
		this.url = (url);
		this.project = (project);
		this.taskName = taskName;
		this.resources = resources;
		this.submissionsAllowed = submissionsAllowed;
		if (patternsAllowed != null)
			this.patternsAllowed = patternsAllowed;
		else
			this.patternsAllowed = new ArrayList<String>();
		this.configuration = (configuration);
		this.validationOperation = (validationOperation);
		this.formParameters = (formParameters);
	}

	/**
	 * Data submission becomes open
	 * 
	 * @return
	 */
	public Date getActiveDate() {
		return activeDate;
	}
	
	/**
	 * Configuration info for the task
	 * (execution environment etc)
	 * 
	 * @return
	 */
	public Set<IJsonKeyValue> getConfiguration() {
		return configuration;
	}
	
	/**
	 * The description of the task
	 * 
	 * @return
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Due date for submission
	 * @return
	 */
	public Date getDueDate() {
		return dueDate;
	}
	
	/**
	 * Last date to accept (overdue) submissions
	 * 
	 * @return
	 */
	public Date getFinalDate() {
		return finalDate;
	}
	
	/**
	 * Parameters for task submission form
	 * 
	 * @return
	 */
	public Set<IJsonKeyValue> getFormParameters() {
		return formParameters;
	}
	
	public List<String> getPatternsAllowed() {
		return patternsAllowed;
	}
	/**
	 * Name of associated project (null if none)
	 * 
	 * @return
	 */
	public String getProject() {
		return project;
	}
	
	/**
	 * List of resources related to the task
	 * 
	 * @return
	 */
	public List<PresentableMetadata> getResources() {
		return resources;
	}
	
	/**
	 * Names for allowed submissions
	 * @return
	 */
	public List<String> getSubmissionsAllowed() {
		return submissionsAllowed;
	}
	
	/**
	 * UUID of the task
	 * 
	 * @return
	 */
	public String getTaskId() {
		return taskId;
	}
	/**
	 * Name for the task
	 * @return
	 */
	public String getTaskName() {
		return taskName;
	}
	
	/**
	 * URL for more info
	 * 
	 * @return
	 */
	public WebLink getUrl() {
		return url;
	}
	
	/**
	 * Specifier for any data validation
	 * 
	 * @return
	 */
	public Set<IJsonKeyValue> getValidationOperation() {
		return validationOperation;
	}
	
	
	/**
	 * Data submission becomes open
	 * 
	 * @param activeDate
	 */
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}
	
	/**
	 * Configuration info for the task
	 * (execution environment etc)
	 * 
	 * @param configuration
	 */
	public void setConfiguration(JsonKeyValueSet configuration) {
		this.configuration = (configuration);
	}
	
	/**
	 * Task description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Official deadline
	 * 
	 * @param dueDate
	 */
	public void setDueDate(Date dueDate) {
		this.dueDate = (dueDate);
	}
	
	/**
	 * Deadline after which late submissions
	 * aren't accepted
	 * 
	 * @param finalDate
	 */
	public void setFinalDate(Date finalDate) {
		this.finalDate = (finalDate);
	}
	
	/**
	 * Parameters for a submission form
	 * 
	 * @param formParameters
	 */
	public void setFormParameters(JsonKeyValueSet formParameters) {
		this.formParameters = (formParameters);
	}
	
	/**
	 * Restrictions on the submissions (e.g., files)
	 * 
	 * @param patternsAllowed
	 */
	public void setPatternsAllowed(List<String> patternsAllowed) {
		this.patternsAllowed = patternsAllowed;
	}
	
	/**
	 * Associated project (null if none)
	 * 
	 * @param project
	 */
	public void setProject(String project) {
		this.project = (project);
	}
	public void setResources(List<PresentableMetadata> resources) {
		this.resources = resources;
	}
	public void setSubmissionsAllowed(List<String> submissionsAllowed) {
		this.submissionsAllowed = submissionsAllowed;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setUrl(WebLink url) {
		this.url = (url);
	}


	public void setValidationOperation(JsonKeyValueSet validationOperation) {
		this.validationOperation = (validationOperation);
	}



	@Override
	public SerializableMetadata getParent() {
		return null;
	}



	@Override
	public void setParent(GeneralMetadata p) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Projects.name();
	}



	@Override
	public String getLabel() {
		return taskName;
	}



	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String getStringValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public VALUE_TYPE getValueType(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		return new ArrayList<GeneralMetadata>();
	}



	@Override
	public String getId() {
		return taskId;
	}



	@Override
	public String getFriendlyName() {
		return taskName;
	}



	@Override
	public List<String> getCreators() {
		return creators;
	}

	public void addCreator(String creator) {
		creators.add(creator);
	}

	@JsonIgnore
	@Transient
	@Override
	public String getContentDescriptor() {
		return "application/x-habitat-task";
	}



	@Override
	public Date getCreationTime() {
		// TODO Auto-generated method stub
		return null;
	}


	@JsonIgnore
	@Transient
	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}


	@JsonIgnore
	@Transient
	@Override
	public Set<String> getAssociatedDataTypes() {
		Set<String> ret = Sets.newHashSet();
		ret.add("application/x-habitat-task");
		return ret;
	}



//	@JsonIgnore
//	@Transient
//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}


	@JsonIgnore
	@Transient
	@Override
	public boolean isLeaf() {
		return false;
	}

	@JsonIgnore
	@Transient
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getDbId() {
		return dbId;
	}
	
	public void setDbId(Long id) {
		this.dbId = id;
	}

	@JsonIgnore
	@Transient
	public UserId getUser() {
		return theUser;
	}

	public void setUser(UserId theUser) {
		this.theUser = theUser;
	}

	public void setCreators(List<String> creators) {
		this.creators = creators;
	}

//	public static void setFormatter(MetadataFormatter formatter) {
//		Task.formatter = formatter;
//	}



	public Map<String, Set<String>> getUserPermissions() {
		return userPermissions;
	}



	public void setUserPermissions(Map<String, Set<String>> userPermissions) {
		this.userPermissions = userPermissions;
	}



	public Set<String> getWorldPermissions() {
		return worldPermissions;
	}



	public void setWorldPermissions(Set<String> worldPermissions) {
		this.worldPermissions = worldPermissions;
	}



	public Map<String, Set<String>> getProjectPermissions() {
		return projectPermissions;
	}



	public void setProjectPermissions(Map<String, Set<String>> projectPermissions) {
		this.projectPermissions = projectPermissions;
	}



	public UserId getTheUser() {
		return theUser;
	}



	public void setTheUser(UserId theUser) {
		this.theUser = theUser;
	}



	@Override
	public String toString() {
		return "Task [dbId=" + dbId + ", creators=" + creators + ", taskName="
				+ taskName + ", taskId=" + taskId + ", activeDate="
				+ activeDate + ", dueDate=" + dueDate + ", finalDate="
				+ finalDate + ", description=" + description + ", url=" + url
				+ ", project=" + project + ", resources=" + resources
				+ ", submissionsAllowed=" + submissionsAllowed
				+ ", patternsAllowed=" + patternsAllowed + ", configuration="
				+ configuration + ", validationOperation="
				+ validationOperation + ", formParameters=" + formParameters
				+ ", theUser=" + theUser + ", userPermissions="
				+ userPermissions + ", worldPermissions=" + worldPermissions
				+ ", projectPermissions=" + projectPermissions + "]"
				;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Task";
	}
}
