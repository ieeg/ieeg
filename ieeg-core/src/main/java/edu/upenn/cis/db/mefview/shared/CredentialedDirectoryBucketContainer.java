/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;

/**
 * A file container with associated AWS credentials we're supposed
 * to use in accessing the container
 * 
 * @author zives
 *
 */
@GwtCompatible(serializable=true)
public class CredentialedDirectoryBucketContainer extends DirectoryBucketContainer implements Serializable {
	ContainerCredentials credentials;
	
	public CredentialedDirectoryBucketContainer() {}
	
	public CredentialedDirectoryBucketContainer(String bucket, String path) {
		super(bucket, path);
	}

	public CredentialedDirectoryBucketContainer(String bucket, String path, ContainerCredentials creds) {
		super(bucket, path);
		this.credentials = creds;
	}

	public CredentialedDirectoryBucketContainer(DirectoryBucketContainer cont) {
		super(cont.getBucket(), cont.getFileDirectory());
	}

	public void setCredentials(ContainerCredentials credentials) {
		this.credentials = credentials;
	}
	
	public ContainerCredentials getCredentials() {
		return credentials;
	}
	
	@Override
	public String toString() {
		return getSource() + ": {" + getFileDirectory() + "/" + getBucket() + "," + 
				((credentials == null) ? "" : credentials.toString()) + "}";
	}
}
