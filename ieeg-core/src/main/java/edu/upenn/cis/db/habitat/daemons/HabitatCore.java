/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.ehcache.CacheManager;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil.SessionAndTrx;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.server.SessionManager;
import edu.upenn.cis.db.mefview.shared.AWSContainerCredentials;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;

/**
 * Initialize the basic HABITAT core:  properties, cache, AWS connection
 * 
 * @author zives
 *
 */
public class HabitatCore {
	public static HabitatCore theFactory = null;
	static IObjectServer fss;
	static IObjectServer sss;
	
	private final static Logger logger = LoggerFactory
			.getLogger(HabitatCore.class);

	private static DocumentBuilderFactory dbf;

	/**
	 * I/O container for source path
	 */
	static CredentialedDirectoryBucketContainer container;
	
	SessionFactory sessFac;
	Configuration config;

	public static synchronized HabitatCore getSingleton() {
		if (theFactory == null)
			theFactory = new HabitatCore();
		return theFactory;
	}
	
	public HabitatCore() {
		this(Thread.currentThread().getContextClassLoader().getResource("ieegview.properties").getPath());
	}
	
	public HabitatCore(String path) {
		System.out.println("* Loading HABITAT core engine, portions Copr. 2010-2015 University of Pennsylvania and Copr. 2015 Blackfynn Inc.");
		System.out.println();
		
		if (AwsUtil.getCreds().getAWSAccessKeyId() == null) {
			System.err.println("No AWS access key specified");
			System.exit(1);
		}
		
		logger.info("Initializing properties based on " + path);
		IvProps.init(path);//".");

		String dataBucket = IvProps.getDataBucket();
		String sourcePath = IvProps.getIvProps().get(IvProps.SOURCEPATH); 
		logger.info("Initializing connection to AWS container " + dataBucket + "//" + sourcePath);

		container = new CredentialedDirectoryBucketContainer(
				dataBucket,
				sourcePath);
		
		container.setCredentials(new AWSContainerCredentials(AwsUtil.getCreds()));
		
		fss = StorageFactory.getStagingServer();
		sss = StorageFactory.getPersistentServer();
		
		String cache = Thread.currentThread().getContextClassLoader().getResource("ieeg-ehcache.xml").getPath();
		logger.info("Initializing object cache based on " + cache);
		initEhcache();

		// Initialize persistence
		
		logger.info("Initializing OR mapping to database");
		
		sessFac = HibernateUtil.getSessionFactory();
		config = HibernateUtil.getConfiguration();
		
		// Run and abort a transaction to bind our session
		rollback(getDefaultTransaction());
	}
	
	public synchronized SessionAndTrx getDefaultTransaction() {
		SessionAndTrx sessAndTrx = sessAndTrx =
					PersistenceUtil.getSessAndTrx(sessFac);

		return sessAndTrx;
	}
	
	public void rollback(SessionAndTrx trans) {
		synchronized (trans) {
			PersistenceUtil.rollback(trans);
		}
	}
	
	public void commit(SessionAndTrx trans) {
		synchronized (trans) {
			PersistenceUtil.commit(trans);
		}
	}

	/**
	 * Get the cache file
	 */
	private void initEhcache() {
		InputStream is = 
				Thread.currentThread().getContextClassLoader().getResourceAsStream("ieeg-ehcache.xml");
		if (is == null) {
			throw new IllegalStateException("An ieeg-ehcache.xml configuration file must be on the classpath");
		}
		CacheManager.newInstance(is);
	}

	/**
	 * Initialize the session manager
	 * 
	 * @param datasets
	 * @return
	 */
	public SessionManager initializeSessionManager(IDataSnapshotServer datasets) {
		final int sessionTimeoutMinutes = IvProps
				.getSessionExpirationMins();
		final int sessionCleanupMins = IvProps.getSessionCleanupMins();
		final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();

		datasets = DataSnapshotServerFactory.getDataSnapshotServer();
		return new SessionManager(datasets,
				sessionTimeoutMinutes, sessionCleanupMins,
				sessionCleanupDenom);
	}
}