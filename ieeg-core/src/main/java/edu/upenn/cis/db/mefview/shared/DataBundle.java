package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DataBundle implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String bundleName;
	
	Map<String, DataBundle> nestedBundles;
	
	Map<String, String> objectMimeTypes;
	Map<String, PresentableMetadata> objectMetadata;
	
	public DataBundle() {
		nestedBundles = new HashMap<String, DataBundle>();
		objectMimeTypes = new HashMap<String, String>();
		objectMetadata = new HashMap<String, PresentableMetadata>();
	}
	
	public DataBundle(String bundleName) {
		this();
		
		this.bundleName = bundleName;
	}

	public String getBundleName() {
		return bundleName;
	}

	public void setBundleName(String bundleName) {
		this.bundleName = bundleName;
	}

	public Map<String, DataBundle> getNestedBundles() {
		return nestedBundles;
	}

	public void setNestedBundles(Map<String, DataBundle> nestedBundles) {
		this.nestedBundles = nestedBundles;
	}

	public Map<String, String> getObjectMimeTypes() {
		return objectMimeTypes;
	}

	public void setObjectMimeTypes(Map<String, String> objectsAndMimeTypes) {
		this.objectMimeTypes = objectsAndMimeTypes;
	}
	
	public void addObjectMimeType(String object, String mimeType) {
		objectMimeTypes.put(object, mimeType);
	}

	public Map<String, PresentableMetadata> getObjectMetadata() {
		return objectMetadata;
	}

	public void setObjectMetadata(Map<String, PresentableMetadata> objectMetadata) {
		this.objectMetadata = objectMetadata;
	}
	
	public void addObjectMetadata(String object, PresentableMetadata metadata) {
		this.objectMetadata.put(object, metadata);
	}

	@Override
	public String toString() {
		return bundleName + ": " + objectMetadata.keySet().toString() + "[" + nestedBundles.values().toString() + "]";
	}
}
