/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.ImmutableSet;

import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.ContactGroupMetadata;

@GwtCompatible(serializable = true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class StudyMetadata implements SerializableMetadata, CaseMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String patientLabel;

	private Integer ageAtOnset;

	private String gender;

	private String handedness;

	private String ethnicity;

	private String etiology;
	
	private int imageCount;

	private Boolean developmentalDisorders;

	private Boolean traumaticBrainInjury;

	private Boolean familyHistory;

	private Set<String> szHistory;

	private Set<String> precipitants;

	private Set<String> szSubtypes;

	private Integer ageAtAdmission;

	private String refElectrodeDescription;

	private Set<ContactGroupMetadata> electrodes;

	private String dcnImageUrl;
	
	SerializableMetadata parent = null;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("gender", VALUE_TYPE.STRING);
		valueMap.put("ethnicity", VALUE_TYPE.STRING);
		valueMap.put("etiology", VALUE_TYPE.STRING);
		valueMap.put("dcnImageUrl", VALUE_TYPE.STRING);
		valueMap.put("seizureHistory", VALUE_TYPE.STRING_SET);
		valueMap.put("precipitants", VALUE_TYPE.STRING_SET);
		valueMap.put("seizureSubtypes", VALUE_TYPE.STRING_SET);
		valueMap.put("refElectrodeDescription", VALUE_TYPE.STRING_SET);

		valueMap.put("electrodes", VALUE_TYPE.META_SET);

		valueMap.put("ageAtOnset", VALUE_TYPE.INT);
		valueMap.put("devDisorderFlag", VALUE_TYPE.INT);
		valueMap.put("traumaticInjuryFlag", VALUE_TYPE.INT);
		valueMap.put("familyHistoryFlag", VALUE_TYPE.INT);
	}

	public StudyMetadata() {
		patientLabel = "";
		ageAtOnset = null;
		gender = "";
		handedness = "";
		ethnicity = "";
		etiology = "";
		imageCount = 0;
		developmentalDisorders = null;
		traumaticBrainInjury = null;
		familyHistory = null;
		szHistory = new HashSet<String>();
		precipitants = new HashSet<String>();
		szSubtypes = new HashSet<String>();
		ageAtAdmission = null;
		refElectrodeDescription = null;
		electrodes = new HashSet<ContactGroupMetadata>();
		dcnImageUrl = "";
	}

	public StudyMetadata(
			final String patientLabel,
			@Nullable final Integer ageAtOnset,
			final String gender,
			final String handedness,
			final String ethnicity,
			final String etiology,
			int imageCount,
			@Nullable final Boolean developmentalDisorders,
			@Nullable final Boolean traumaticBrainInjury,
			@Nullable final Boolean familyHistory,
			final Set<String> szHistory,
			final Set<String> precipitants,
			@Nullable final Integer ageAtAdmission,
			@Nullable final String refElectrodeDescription,
			final Set<ContactGroupMetadata> electrodes,
			final Set<String> szSubtypes, final String dcnImageUrl) {
		super();
		this.patientLabel = checkNotNull(patientLabel);
		this.ageAtOnset = ageAtOnset;
		this.gender = checkNotNull(gender);
		this.handedness = checkNotNull(handedness);
		this.ethnicity = checkNotNull(ethnicity);
		this.etiology = checkNotNull(etiology);
		this.imageCount = imageCount;
		this.developmentalDisorders = developmentalDisorders;
		this.traumaticBrainInjury = traumaticBrainInjury;
		this.familyHistory = familyHistory;
		this.szHistory = ImmutableSet.copyOf(szHistory);
		this.precipitants = ImmutableSet.copyOf(precipitants);
		this.szSubtypes = ImmutableSet.copyOf(szSubtypes);
		this.ageAtAdmission = ageAtAdmission;
		this.refElectrodeDescription = refElectrodeDescription;
		this.electrodes = ImmutableSet.copyOf(electrodes);
		this.dcnImageUrl = checkNotNull(dcnImageUrl);
	}

	public Integer getAgeAtAdmission() {
		return ageAtAdmission;
	}

	public Integer getAgeAtOnset() {
		return ageAtOnset;
	}

	public Set<ContactGroupMetadata> getElectrodes() {
		return electrodes;
	}
	
	@JsonIgnore
	@Override
	public Set<ContactGroupMetadata> getContactGroups() {
		return getElectrodes();
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public String getEtiology() {
		return etiology;
	}

	public String getGender() {
		return gender;
	}

	public String getHandedness() {
		return handedness;
	}
	
	public int getImageCount() {
		return imageCount;
	}

	public String getPatientLabel() {
		return patientLabel;
	}

	public Set<String> getPreciptants() {
		return precipitants;
	}

	public String getRefElectrodeDescription() {
		return refElectrodeDescription;
	}

	public Set<String> getSzHistory() {
		return szHistory;
	}

	public Set<String> getSzSubtypes() {
		return szSubtypes;
	}

	public Boolean getDevelopmentalDisorders() {
		return developmentalDisorders;
	}

	public Boolean getFamilyHistory() {
		return familyHistory;
	}

	public Boolean getTraumaticBrainInjury() {
		return traumaticBrainInjury;
	}

	public String getDcnImageUrl() {
		return dcnImageUrl;
	}

	@Override
	public String getLabel() {
		return getPatientLabel();
	}

	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return getId();
		if (key.equals("gender"))
			return getGender();
		if (key.equals("ethnicity"))
			return getEthnicity();
		if (key.equals("etiology"))
			return getEtiology();
		if (key.equals("dcnImageUrl"))
			return getDcnImageUrl();
		if (key.equals("refElectrodeDescription")) {
			if (getRefElectrodeDescription() != null)
				return getRefElectrodeDescription();
			else
				return null;
		}

		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		valueMap.put("traumaticInjuryFlag", VALUE_TYPE.INT);
		valueMap.put("familyHistoryFlag", VALUE_TYPE.INT);

		if (key.equals("ageAtOnset") && getAgeAtOnset() != null) {
			return getAgeAtOnset();
		}
		if (key.equals("devDisorderFlag")
				&& getDevelopmentalDisorders() != null)
			return this.getDevelopmentalDisorders() ? 1 : 0;
		if (key.equals("traumaticInjuryFlag")
				&& getTraumaticBrainInjury() != null)
			return this.getTraumaticBrainInjury() ? 1 : 0;
		if (key.equals("familyHistoryFlag") && getFamilyHistory() != null)
			return this.getFamilyHistory() ? 1 : 0;

		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		List<String> ret = new ArrayList<String>();

		if (key.equals("seizureHistory")) {
			ret.addAll(this.getSzHistory());
			return ret;
		}
		if (key.equals("precipitants")) {
			ret.addAll(this.getPreciptants());
			return ret;
		}
		if (key.equals("seizureSubtypes")) {
			ret.addAll(this.getSzSubtypes());
			return ret;
		}

		return null;
	}

	@Override
	public List<GeneralMetadata> getMetadataSetValue(String key) {
		List<GeneralMetadata> ret = new ArrayList<GeneralMetadata>();
		if (key.equals("electrodes")) {
			ret.addAll(getElectrodes());
			return ret;
		}
		return null;
	}

	@Override
	public String getId() {
		return getLabel();
	}

	@Override
	public void setId(String id) {
		
	}

	@JsonIgnore
	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			parent = (SerializableMetadata)p;
		else
			throw new RuntimeException("Attempted to set non-serializable parent");
	}

	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Metadata.name();
	}

	@JsonIgnore
	public String getAge() {
		if (getAgeAtAdmission() != null)
			return String.valueOf(getAgeAtAdmission());
		else
			return null;
	}

	@JsonIgnore
	@Override
	public boolean isHumanPatient() {
		return true;
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		return "";
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Metadata";
	}
}
