/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;

import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer;
import edu.upenn.cis.db.habitat.daemons.exceptions.RequestFailedException;
import edu.upenn.cis.db.mefview.shared.JsonString;

/**
 * Message callback handler for IConsumer
 * 
 * @author zives
 *
 * @param <K> Key for request
 * @param <V> Value type for response message
 */
public class ConsumerHandler<K,V> {
	K key;
	V value = null;
	String errCode = null;
	IFutureResult<K,V> successHandler;
	IFutureResult<K,String> errorHandler;
	boolean done = false;
	private final Queue<Thread> waiters = new ConcurrentLinkedQueue<Thread>();
	
	public ConsumerHandler(K theKey, final IConsumer<K,V> handler) {
		this.key = theKey;
		successHandler = new IFutureResult<K,V>() {

			@Override
			public V call() throws Exception {
				handler.handle(getRequest(), getResult());
				return value;
			}

			@Override
			public void setView(K v) {
				key = v;
			}

			@Override
			public K getRequest() {
				return key;
			}

			@Override
			public void setResult(V object) {
				value = object;
			}

			@Override
			public V getResult() {
				return value;
			}
			
		};
		errorHandler = new IFutureResult<K,String>() {

			@Override
			public String call() throws Exception {
				handler.handleError(getRequest(), new JsonString(errCode));
				return errCode;
			}

			@Override
			public void setView(K v) {
				key = v;
			}

			@Override
			public K getRequest() {
				return key;
			}

			@Override
			public void setResult(String error) {
				errCode = error;
			}

			@Override
			public String getResult() {
				return errCode;
			}
			
		};
		
	}
	
	public IFutureResult<K,V> getSuccessHandler() {
		return successHandler;
	}
	
	public IFutureResult<K,String> getErrorHandler() {
		return errorHandler;
	}
}