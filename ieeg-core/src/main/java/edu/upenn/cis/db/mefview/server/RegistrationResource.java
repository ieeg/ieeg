/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.db.mefview.services.IRegistrationResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;

public final class RegistrationResource implements IRegistrationResource {

	private final IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final HttpServletRequest httpServletRequest;
	private final User user;

	public RegistrationResource(
			@Context HttpServletRequest httpServletRequest) {
		String m = "RegistrationResource(...)";
		this.httpServletRequest = httpServletRequest;
		this.user = (User) this.httpServletRequest.getAttribute("user");
		logger.debug("{}: user: ", m, user.getUsername());
	}

	@Override
	public Response createControlFileRegistration(
			ControlFileRegistration registration) {
		final String m = "createControlFileRegistration(...)";
		try {
			final ControlFileRegistration createdObject = dsServer
					.createControlFileRegistration(
							user,
							registration);
			final Response response =
					Response.created(
							URI.create(IRegistrationResource.PATH
									+ "/"
									+ createdObject.getId()))
							.tag(createdObject.getETag())
							.entity(createdObject)
							.build();

			return response;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

}
