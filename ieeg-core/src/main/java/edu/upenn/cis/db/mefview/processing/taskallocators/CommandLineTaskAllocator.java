/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.processing.taskallocators;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

@Deprecated
public class CommandLineTaskAllocator implements ITaskAllocator {
	
	List<String> parameters;
	
	public String userid = "btuser";
	public String pwd = "password";
	public String server = "http://localhost:9997";
	int threshold = 400;
	List<String> datasets = new ArrayList<String>();

	public CommandLineTaskAllocator(String[] parms) {
		parameters = new ArrayList<String>(parms.length);
		
		for (String p : parms)
			parameters.add(p);
	}

	@Override
	public void init() {
		if (parameters.size() > 0) {
			userid = parameters.get(0);
			pwd = parameters.get(1);
			server = parameters.get(2);
			threshold = Integer.valueOf(parameters.get(3));
			for (int i = 4; i < parameters.size(); i++)
				datasets.add(parameters.get(i));
		}
	}

	@Override
	public JsonTyped getNextTask() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTaskStatus(String task, JsonTyped newParameters) {
		// TODO Auto-generated method stub

	}

}
