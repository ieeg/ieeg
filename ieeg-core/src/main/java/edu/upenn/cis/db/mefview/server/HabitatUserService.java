/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.slf4j.Logger;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IUserService.UsersAndTotalCount;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import org.slf4j.LoggerFactory;

public class HabitatUserService implements IIeegUserService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static Logger logger = LoggerFactory.getLogger(HabitatUserService.class);

	protected IUserService dus;
	protected transient IDataSnapshotServer datasets;

	private transient ISessionManager sessionManager;
	
	private final transient MessageServer messageServer = SmtpServerFactory.getSmtpServer();

	public HabitatUserService(IUserService userService) {
		this(userService, null);
	}

	public HabitatUserService(IUserService userService,
			IDataSnapshotServer dsServer) {
		dus = userService;
		this.datasets = dsServer;

		if (dsServer != null)
			createSessionManager(dsServer,
					IvProps.SESSION_EXP_MINS_DFLT,
					IvProps.SESSION_CLEANUP_MINS_DFLT,
					IvProps.SESSION_CLEANUP_DENOMINATOR_DFLT);
	}

	public void createSessionManager(IDataSnapshotServer dsServer,
			int sessionTimeoutMinutes,
			int sessionCleanupMins,
			int sessionCleanupDenom) {

		datasets = dsServer;
		sessionManager = new SessionManager(dsServer,
				sessionTimeoutMinutes, sessionCleanupMins,
				sessionCleanupDenom);
	}

	public void setSessionManager(ISessionManager mgr) {
		sessionManager = mgr;
	}

	public ISessionManager getSessionManager() {
		return sessionManager;
	}

	@Override
	public User getEnabledUser(String username) throws AuthorizationException {
		User user = dus.findUserByUsername(username);
		if (user != null && user.getStatus() != User.Status.DISABLED) {
			return user;
		}
		throw new AuthorizationException("User [" + username + "] not found!");
	}

	@Override
	public User getEnabledUser(UserId userid) throws AuthorizationException {
		User user = dus.findUserByUid(userid);
		if (user != null && user.getStatus() != User.Status.DISABLED) {
			return user;
		}
		throw new AuthorizationException("User [" + userid + "] not found!");

	}

	@Override
	public UserInfo getUserInfoHelper(String requestedUserName) {
		User user2 = dus.findUserByUsername(requestedUserName);

		if (user2 == null)
			throw new IllegalArgumentException("User [" + requestedUserName
					+ "] not found");

		UserInfo ret = new UserInfo(user2.getUsername(), user2.getPhoto(), user2.getUserId());
		logger.info("User info for "+user2.getUsername() +" "+ user2.getPhoto() +" "+ user2.getUserId());

		ret.setProfile(user2.getProfile());
		return ret;
	}

	@Override
	public UserAccountInfo getUserAccountInfoHelper(String requestedUserName) {
		User user2 = dus.findUserByUsername(requestedUserName);

		if (user2 == null)
			throw new IllegalArgumentException("User [" + requestedUserName
					+ "] not found");

		UserAccountInfo ret = new UserAccountInfo(
				user2.getUsername(), // login
				user2.getEmail().isPresent() ? user2.getEmail().get() : "", // email
				user2.getProfile().getFirstName().isPresent() ? user2
						.getProfile().getFirstName().get() : "",
				user2.getProfile().getLastName().isPresent() ? user2
						.getProfile().getLastName().get() : "",
				user2.getProfile().getInstitutionName().isPresent() ? user2
						.getProfile().getInstitutionName().get() : "",
				user2.getProfile().getStreet().isPresent() ? user2.getProfile()
						.getStreet().get() : "", // street
				user2.getProfile().getCity().isPresent() ? user2.getProfile()
						.getCity().get() : "", // city
				user2.getProfile().getState().isPresent() ? user2.getProfile()
						.getState().get() : "", // state
				user2.getProfile().getZip().isPresent() ? user2.getProfile()
						.getZip().get() : "", // zip
				user2.getProfile().getCountry().isPresent() ? user2
						.getProfile().getCountry().get() : "", // country
				user2.getProfile().getAcademicTitle().isPresent() ? user2
						.getProfile().getAcademicTitle().get() : "",
				user2.getProfile().getJob().isPresent() ? user2.getProfile()
						.getJob().get() : "",
				user2.getPhoto(),
				null, // AWS
				null,
				null // DropBox
		);

		return ret;
	}

	@Override
	public List<ToolDto> getToolsHelper(User requestingUser,
			Set<String> toolRevIds) {
		List<ToolDto> tools = datasets.getTools(requestingUser, toolRevIds);

		List<ToolDto> toolsList = new ArrayList<ToolDto>();

		for (ToolDto toolDto : tools) {
			if (toolDto != null) {
				toolsList.add(toolDto);
			}
		}
		return toolsList;
	}

	/**
	 * Returns the {@code User} who owns the session with the given token. If
	 * {@code allowExpiredSess} is true the user is returned even if the session
	 * has expired.
	 * 
	 * @param token
	 * @return the {@code User} who owns the active session with the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out and {@code allowExpiredSess} is false
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out and {@code allowExpiredSess} is false
	 * @throws DisabledAccountException if the user's account has been disabled
	 * @throws IncorrectCredentialsException if the session owner cannot be
	 *             found
	 */
	public User verifyUser(
			String sourceMethod,
			SessionToken sessionID,
			boolean allowExpiredSess,
			@Nullable Role role) {
		UserIdSessionStatus userIdSessStatus = sessionManager
				.getUserIdSessionStatus(sessionID);

		final UserId userId = userIdSessStatus.getUserId();
		User user = dus.findUserByUid(userId);
		if (user == null) {
			sessionManager.disableAcctSession(sessionID);
			throw new IncorrectCredentialsException("User [" + userId
					+ "] not found");
		}
		if (user.getStatus() == User.Status.DISABLED) {
			sessionManager.disableAcctSession(sessionID);
			throw new DisabledAccountException("User [" + user.getUsername()
					+ "] account disabled.");
		}

		if (role != null && !user.getRoles().contains(role))
			throw new AuthorizationException("User [" + user.getUsername()
					+ "] does not have permission for " + sourceMethod);

		final SessionStatus status = userIdSessStatus.getSessionStatus();
		switch (status) {
			case ACTIVE:
				return user;
			case EXPIRED:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has expired.");
				}
				// Consider logging out as an expiration
			case LOGGED_OUT:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has logged out.");
				}
			case ACCOUNT_DISABLED:
				throw new DisabledAccountException("User ["
						+ user.getUsername()
						+ "] account disabled.");
			default:
				throw new AssertionError("Unknown enum value [" + status + "]");

		}

	}

	/**
	 * Returns the {@code User} who owns the active session with the given
	 * token.
	 * 
	 * @param token
	 * @return the {@code User} who owns the active session with the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out
	 * @throws DisabledAccountException if the user's account has been disabled
	 */
	public User verifyUser(String source, SessionToken sessionID,
			@Nullable Role role) {
		return verifyUser(source, sessionID, false, role);
	}

	public User verifyUserError(final String caller,
			final SessionToken sessionID, @Nullable Role role,
			final Logger logger) {
		try {
			return verifyUser(caller, sessionID, Role.USER);
		} catch (Exception e) {
			logger.error(caller + ": Caught Exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	public UsersAndTotalCount getUsersAndCount() {
		UsersAndTotalCount uac = dus.findEnabledUsers(0, Integer.MAX_VALUE,
				new HashSet<UserId>());

		return uac;
	}
	
	@Override
	public UsersAndTotalCount getUsersAndCount(int start, int length, Set<UserId> excludes) {
		UsersAndTotalCount uac = dus.findEnabledUsers(start, length,
				excludes);

		return uac;
	}

	public long getUserCount() {
		return dus.countEnabledUsers();
	}

	public User getUserByName(final String username) {
		return dus.findUserByUsername(username);
	}

	public IDataSnapshotServer getDataSnapshotAccessSession() {
		return datasets;
	}

	public void setDataSnapshotAccessSession(IDataSnapshotServer ds) {
		datasets = ds;
	}

	public List<LatLon> getLatLon() {
		return dus.findLatLon();
	}

	public User createUser(UserAccountInfo newUser, String encryptedPwd) throws DuplicateNameException {
		User user = getUserByName(newUser.getLogin());

		if (user != null) {
			throw new DuplicateNameException(newUser.getLogin());
		}

		UserProfile userProfile = newUser.getProfile();
		user = new User(
				new UserId(-1),
				newUser.getLogin(),
				encryptedPwd,
				IUser.Status.ENABLED,
				new HashSet<Role>(),
				newUser.getPhoto(),
				newUser.getEmail(),
				userProfile);

		User theUser = dus.createOrUpdateUser(user);

		if (!IvProps.getNewUserEmailToAddress().isEmpty() && !IvProps.getNewUserEmailFromAddress().isEmpty()) {
			messageServer.sendMessage(IvProps.getNewUserEmailToAddress(), "New user created", IvProps.getNewUserEmailFromAddress(), "User " +
					theUser.getUsername() + " has been created and needs to be enabled.\n");
		}
		return theUser;
	}
	
	public User updateUser(User user, UserAccountInfo newUser, String hashedPassword) {
		checkNotNull(user);
		checkNotNull(newUser);
		checkNotNull(hashedPassword);
		user.setUsername(newUser.getLogin());
		user.setEmail(Optional.of(newUser.getEmail()));
		user.setPhoto(newUser.getPhoto());
		if (!hashedPassword.isEmpty())
			user.setPassword(hashedPassword);

		user.setProfile(newUser.getProfile());
		return dus.createOrUpdateUser(user);
	}

	public List<EEGMontage> getMontagesHelper(User theUser,
			String dataSnapshotId) {
		List<EEGMontage> montages = datasets.getMontages(theUser,
				dataSnapshotId);
		return montages;
	}

	@Override
	public User updateUserFields(User user, UserProfile newUser) {
		if (user != null) {
			user.setProfile(newUser);
			User theUser = dus.createOrUpdateUser(user);

			return theUser;
		} else
			return null;
	}

	public List<UserAccountInfo> getUserAccountInfoHelperForAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object setUserAccountInfoHelperForAll(List<UserAccountInfo> info) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(UserId userId) {
		return dus.findUserByUid(userId);
	}

}
