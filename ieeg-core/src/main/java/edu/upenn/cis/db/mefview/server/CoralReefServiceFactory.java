package edu.upenn.cis.db.mefview.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.persistence.IGraphServer;

public class CoralReefServiceFactory {
	  private final static Logger logger = LoggerFactory
		      .getLogger(CoralReefServiceFactory.class);

	  private static IGraphServer crService;

	public static IGraphServer getGraphServer() {
		if (crService != null)
			return crService;
		
		try {
            @SuppressWarnings("unchecked")
			Class<IGraphServer> exampleClass = 
            		(Class<IGraphServer>) Class.forName("edu.upenn.cis.db.habitat.persistence.CoralReefServer");
            
            crService = exampleClass.newInstance();
        } catch (ClassNotFoundException e) {
            logger.debug("Launch CoralReef: " + e);
        } catch (InstantiationException e) {
            logger.debug("Launch CoralReef: " + e);
		} catch (IllegalAccessException e) {
            logger.debug("Launch CoralReef: " + e);
		}

		return crService;
	}
}
