/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Iterables.getOnlyElement;
import static edu.upenn.cis.braintrust.BtUtil.getBoolean;
import static java.util.Collections.singleton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.block.BlockStoreServer.BlockChannelSpecifier;
import edu.upenn.cis.db.mefview.server.block.BlockStoreServer.BlockSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFDataAndPageNos;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.eeg.mef.MEFObjectReader;
import edu.upenn.cis.eeg.mef.MEFReader;

public class MEFPageServer extends MultithreadedPageServer<TimeSeriesPage> {
	public Map<String, String> paths;
	public Map<String, MEFChannelSpecifier> pathSpec;
	
	public final static int CACHE_SIZE = 5120;
	
	public final boolean downsampled;
	public final double downsampledRate;

	// public static final int PREFETCH = 30;
	Map<String, Integer> busyMap;
	// Integer started = 1;
	// boolean startFlag = false;
	ExecutorService exec2;
//	static Map<FilterSpec, TimeSeriesFilter> filterCache = new ConcurrentHashMap<FilterSpec, TimeSeriesFilter>();
	IDataSnapshotServer ds;
	String sourcePath;
	
	boolean lazy = true;

	// static PrintWriter tempLog;

	private final static Logger logger = LoggerFactory
			.getLogger(MEFPageServer.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time." + MEFPageServer.class.getName());
	
	/**
	 * Initialize this, pass into child threads, wait on it.
	 * 
	 * @author zives
	 * 
	 */
	public static class Counter {
		int count = 0;

		public Counter(int start) {
			count = start;
		}

		public synchronized void decrement() {
			count--;
			if (count == 0)
				notify();
		}
	}

	public static class MEFChannelSpecifier extends ChannelSpecifier implements GeneralMetadata {

//		public static class BlockChannelSpecifier extends ChannelSpecifier implements GeneralMetadata {
//			private static final long serialVersionUID = 1L;
//			private String fullPath;
//			private BlockSnapshotSpecifier snapshot;
//			
//			static Map<String,VALUE_TYPE> keys;
//			static {
//				keys = new HashMap<String,VALUE_TYPE>();
//				
//				keys.put("id", VALUE_TYPE.STRING);
//			}
//			
//			
//			public BlockChannelSpecifier(BlockSnapshotSpecifier sourcePath, String subPath, String revID) {
//				super(revID);
//				snapshot = sourcePath;
//				fullPath = sourcePath.getPath() + subPath;
//			}
//		
//		
//			@Override
//			public String getHandle() { return fullPath; }
//		
//			@Override
//			public edu.upenn.cis.db.mefview.services.SnapshotSpecifier getSnapshot() {
//				return snapshot;
//			}
//		
//			@Override
//			public int hashCode() {
//				final int prime = 31;
//				int result = 1;
//				result = prime * result
//						+ ((getId() == null) ? 0 : getId().hashCode());
//				return result;
//			}
//		
//			@Override
//			public boolean equals(Object obj) {
//				if (this == obj)
//					return true;
//				if (obj == null)
//					return false;
//				if (!(obj instanceof BlockChannelSpecifier))
//					return false;
//				BlockChannelSpecifier other = (BlockChannelSpecifier) obj;
//				if (getId() == null) {
//					if (other.getId() != null)
//						return false;
//				} else if (!getId().equals(other.getId()))
//					return false;
//				return true;
//			}
//		
//			@Override
//			public Set<String> getKeys() {
//				return keys.keySet();
//			}
//		
//			@Override
//			public VALUE_TYPE getValueType(String key) {
//				return keys.get(key);
//			}
//		
//			@Override
//			public GeneralMetadata getMetadataValue(String key) {
//				return null;
//			}
//		
//			@Override
//			public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
//				return null;
//			}
//		
//		
//			public GeneralMetadata getParent() {
//				return snapshot;
//			}
//		
//			@Override
//			public void setParent(GeneralMetadata p) {
//				if (p instanceof BlockSnapshotSpecifier)
//					snapshot = (BlockSnapshotSpecifier)p;
//			}
//		
//		
//			@Override
//			public String getMetadataCategory() {
//				return BASIC_CATEGORIES.Timeseries.name();
//			}
//		
//		}

		private static final long serialVersionUID = 1L;

		private String fullPath;
		private MEFSnapshotSpecifier snapshot;
		private int rate = 0;
		
		private GeneralMetadata parent;
		
		static Map<String,VALUE_TYPE> keys;
		static {
			keys = new HashMap<String,VALUE_TYPE>();
			
			keys.put("id", VALUE_TYPE.STRING);
		}

		public MEFChannelSpecifier(
				MEFSnapshotSpecifier sourcePath,
				String subPath, 
				String revID, 
				int rate, 
				String dataVersion) {
			super(revID, dataVersion);
			snapshot = sourcePath;
			File test = new File(sourcePath.getPath() + subPath); 
			if (test.exists())
				fullPath = sourcePath.getPath() + subPath;
			else
				fullPath = subPath;
			this.rate = rate;
		}

		@Override
		public String getHandle() {
			return fullPath;
		}

		@Override
		public String getKey() {
			if (rate == 0)
				return getId();
			else
				return getId() + "-" + rate;
		}

		@Override
		public MEFSnapshotSpecifier getSnapshot() {
			return snapshot;
		}

		@Override
		public String toString() {
			return "MEFChannelSpecifier [fullPath=" + fullPath + ", snapshot="
					+ snapshot + ", rate=" + rate + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((getId() == null) ? 0 : getId().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof MEFChannelSpecifier))
				return false;
			MEFChannelSpecifier other = (MEFChannelSpecifier) obj;
			if (!equal(getId(), other.getId())) {
				return false;
			}
			if (!equal(getCheckStr(), other.getCheckStr())) { 
				return false;
			}
			return true;
		}

		@Override
		public void setParent(GeneralMetadata p) {
			parent = p;
		}

		@Override
		public Set<String> getKeys() {
			return keys.keySet();
		}

		@Override
		public VALUE_TYPE getValueType(String key) {
			return keys.get(key);
		}

		@Override
		public GeneralMetadata getMetadataValue(String key) {
			return null;
		}

		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			return null;
		}

		@Override
		public GeneralMetadata getParent() {
			return parent;
		}

		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Timeseries.name();
		}

		@Override
		public ChannelSpecifier clone() {
			MEFChannelSpecifier ret = new MEFChannelSpecifier(
					snapshot,
					fullPath, 
					getId(), 
					rate, 
					super.getCheckStr());
			
			ret.fullPath = fullPath;
			
			return ret;
			
		}

	}
	
	public static class MEFSnapshotSpecifier extends SnapshotSpecifier implements GeneralMetadata {
		private static final long serialVersionUID = 1L;

		private String revID;
		private String fullPath;
		
		GeneralMetadata parent;
		static Map<String, VALUE_TYPE> keys;
		
		static {
			keys = new HashMap<String, VALUE_TYPE>();
			keys.put("id", VALUE_TYPE.STRING);
		}
		
		public MEFSnapshotSpecifier(String sourcePath, String rid) {
			fullPath = sourcePath;
			revID = rid;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fullPath == null) ? 0 : fullPath.hashCode());
			result = prime * result + ((revID == null) ? 0 : revID.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof MEFSnapshotSpecifier)) {
				return false;
			}
			MEFSnapshotSpecifier other = (MEFSnapshotSpecifier) obj;
			if (fullPath == null) {
				if (other.fullPath != null) {
					return false;
				}
			} else if (!fullPath.equals(other.fullPath)) {
				return false;
			}
			if (revID == null) {
				if (other.revID != null) {
					return false;
				}
			} else if (!revID.equals(other.revID)) {
				return false;
			}
			return true;
		}

		public String getPath() {
			return fullPath;
		}

		public String getRevID() {
			return revID;
		}

		@Override
		public String toString() {
			return "MEFSnapshotSpecifier [revID=" + revID + ", fullPath="
					+ fullPath + "]";
		}

		@Override
		public Set<String> getKeys() {
			return keys.keySet();
		}

		@Override
		public VALUE_TYPE getValueType(String key) {
			return keys.get(key);
		}

		@Override
		public GeneralMetadata getMetadataValue(String key) {
			return null;
		}

		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			return null;
		}

		@Override
		public GeneralMetadata getParent() {
			return parent;
		}

		@Override
		public void setParent(GeneralMetadata p) {
			parent = p;
		}

		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Snapshot.name();
		}
	};
	

	public class TraceLoader implements Callable<List<TimeSeriesData>> {
		ChannelSpecifier path;
		long startTime;
		long endTime;
		PostProcessor processor;

		public TraceLoader( 
				ChannelSpecifier path,
				long startTime, long endTime,
				PostProcessor postProc) {
			this.path = path;
			this.startTime = startTime;
			this.endTime = endTime;
			processor = postProc;
		}

		public List<TimeSeriesData> call() throws Exception {
			final String M = "MEFTraceLoader.call()";
			try {
				ArrayList<TimeSeriesData> ret;

				ret = requestRead(
					path, startTime, endTime, processor, null, null);

				for (TimeSeriesData ts : ret) {
					ts.setRevId(path.getId());//revId);
				}

				return ret;
			} catch (Throwable t) {
				logger.error(M + ": Caught, returning null", t);
				return null;
			}
		}

	}

	public MEFPageServer(IDataSnapshotServer ds, String sourcePath, int threads, boolean prefetch, boolean lazy, int pages) {
		super(threads, prefetch);
		this.lazy = lazy;
		final String M = "MEFPageServer(...)";
		this.ds = ds;
		this.sourcePath = sourcePath;
		paths = new ConcurrentHashMap<String, String>(new LruCache<String, String>(CACHE_SIZE));
		pathSpec = new ConcurrentHashMap<String, MEFChannelSpecifier>(new LruCache<String, MEFChannelSpecifier>(CACHE_SIZE));
		exec2 = Executors.newFixedThreadPool(threads);
		busyMap = new ConcurrentHashMap<String, Integer>();
		downsampled = getBoolean(IvProps.getIvProps(), IvProps.DOWNSAMPLED, false);
		downsampledRate = BtUtil.getDouble(IvProps.getIvProps(), IvProps.DOWNSAMPLED_RATE_HZ, 100.0);
		logger.debug("{}: Using downsampled: {}, downsampledRate: {}", new Object[] {M, downsampled, downsampledRate});
	}

	@Override
	public void shutdown() {
		final String M = "shutdown()";
		exec2.shutdownNow();
		paths.clear();
		pathSpec.clear();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error(M + ": Caught, ignoring", e);
		}
		super.shutdown();
	}

	public List<String> getPageServerState() {
		ArrayList<String> ret = new ArrayList<String>();

		// ret.add
		logger.trace("  ** Busy (" + busyMap.size() + ") **");
		for (String s : busyMap.keySet())
			// ret.add(s);
			logger.trace(s);

		// ret.add
		logger.trace("  ** Readers (" + readerMap.size() + ") **");

		for (String s : this.readerMap.keySet())
			// ret.add(s);
			logger.trace(s);

		// ret.add
		logger.trace("  ** Pages (" + fullPageMap.size() + ") **");

		return ret;
	}

	MEFObjectReader getReader(ChannelSpecifier path) throws IOException {

		if (path == null) {
			logger.error("Null path");
			return null;
		}

		MEFObjectReader ret = (MEFObjectReader) getReaderFor(path);//(readerMap.get(path));
		if (ret == null || !ret.isOpen()) {
			ret = reload(path);//.getString());
//		} else {
//			try {
				ret.getHeader();
//				ret.getFile().getFilePointer();
//				ret.get
//			} catch (IOException e) {
//				ret = reload(path);//.getString());
//			}
		}
		return ret;
	}

	private MEFObjectReader reload(final ChannelSpecifier path) throws IOException {
		final String M = "reload()";
		MEFObjectReader ret;
		waitForBusy(path);

		logger.debug(M + ": Creating new reader for " + path.getHandle());

		try {
			String name = path.getHandle();
			if (name.startsWith(IvProps.getIvProps().get(IvProps.SOURCEPATH)))
				name = name.substring(IvProps.getIvProps().get(IvProps.SOURCEPATH).length());
				
			ret = new MEFObjectReader(new FileReference(
					new DirectoryBucketContainer(IvProps.getDataBucket(), IvProps.getIvProps().get(IvProps.SOURCEPATH)),
					name, name), lazy);
		} catch (IOException e) {
			throw e;
		}
		readerMap.put(path.getKey(), ret);
		Integer b = busyMap.get(path.getKey());
		synchronized (b) {
			busyMap.put(path.getKey(), new Integer(0));
			b.notifyAll();
		}

		return ret;
	}

	@Override
	public void clearBuffer() {
		super.clearBuffer();
	}

	private void waitForBusy(ChannelSpecifier path) {
		final String M = "waitForBusy()";
		Integer b = busyMap.get(path.getKey());

		if (b == null) {
			registerReader(path);
			return;
		}

		logger.debug(M + ":  * " + path.getHandle() + " waiting");
		while (b == 1) {
			synchronized (b) {
				try {
					b.wait(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.error(M + ": Caught, ignoring", e);
				}
			}
			b = busyMap.get(path.getKey());
		}
	}

	public synchronized void registerReader(final ChannelSpecifier pathToSource) {
		Integer b = busyMap.get(pathToSource.getKey());
		if (b == null) {
			b = new Integer(1);
			busyMap.put(pathToSource.getKey(), b);
		}
	}

	public synchronized void addReader(final ChannelSpecifier pathToSource) {
		exec2.execute(new Runnable() {

			public void run() {
				final String M = "addReader().Runnable.run()";
				try {
					Integer b = busyMap.get(pathToSource.getKey());
					synchronized (b) {
						addReader(pathToSource, new MEFReader(pathToSource.getHandle()));
						
						busyMap.put(pathToSource.getKey(), new Integer(0));
						logger.trace(M + ": Server for trace " + (pathToSource.getHandle())
								+ " started.");
						b.notifyAll();
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					logger.error(M + ": Caught, ignoring", e);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error(M + ": Caught, ignoring", e);
				}
			}

		});
		// }
	}

	public TimeSeriesPage[] requestPages(//String doc, 
			ChannelSpecifier doc,
			long startPage, long endPage,
			int resolution) throws InterruptedException, ExecutionException,
			IOException {
		String M = "requestPages(...)";
		int max = (int)(endPage - startPage + 1);
		TimeSeriesPage[] ret = new TimeSeriesPage[max];
//		new ArrayList<MEFPage>(
//				(int) (endPage - startPage + 1));

		PageID<ChannelSpecifier> uid = new PageID<ChannelSpecifier>(doc, startPage);
		for (int pageId = 0; pageId < max; pageId++) {
//			String uid = doc + "/" + String.valueOf(pageId + startPage);
			
			uid.setPageId(pageId + startPage);
			TimeSeriesPage result = fullPageMap.get(uid);
			if (result == null) {
				TimeSeriesPage[] resultList = fetchPages(readerMap.get(doc.getKey()), doc,
						pageId + startPage,
						max - pageId, resolution);
				misses++;
				
				if (resultList != null && resultList.length > 0) {
//					ret.addAll(resultList);
					for (int i = 0; i < resultList.length && i + pageId < ret.length; i++) {
						ret[i + pageId] = resultList[i];
					}
				} else {
					logger.warn("Null result for request of " + doc
							+ " at position " + startPage);

//					System.err.println("NULL RESULT");
//					ret.add(new MEFPage());
					for (int i = pageId; i <= (int) (endPage - startPage) && i < ret.length; i++) {
						ret[i] = new TimeSeriesPage();
					}
				}
				break;
			} else {
				hits++;
//				ret.add(result);
				ret[pageId] = result;
			}
		}
		logger.debug("{}: fullPageMap hits [{}] misses [{}]", new Object[] {M, hits, misses});
		return ret;
	}

	public TimeSeriesPage[] fetchPages(PageReader<TimeSeriesPage> reader, ChannelSpecifier doc,
			long pageId, int count, int resolution)
			throws InterruptedException, ExecutionException, IOException {
		final String M = "fetchPages(...)";
		logger.debug("{}: Reading channel {} page {} for {} pages", new Object[] {M, doc, pageId, count});
		TimeSeriesPage[] pgs = reader.readPages(pageId, count, true);

		int i = 0;
		for (TimeSeriesPage pd : pgs) {
			PageID<ChannelSpecifier> uid = new PageID<ChannelSpecifier>(doc, pageId + i);
			if (!fullPageMap.containsKey(uid)) {
//				logger.debug("{}: Adding channel {} page {} to fullPageMap", new Object[] {M, doc, uid.pageID});
				fullPageMap.put(uid, pd);
			}
			i++;
		}

		return pgs;
	}

	@Override
	public long getNumSamples(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getNumSamples();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public long getEndUutc(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getEndTime();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public int getMaxSampleValue(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getMaxSampleValue();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public int getMinSampleValue(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getMinSampleValue();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public long getNumIndexEntries(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getNumIndexEntries();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public double getSampleRate(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getSampleRate();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public long getStartUutc(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getStartTime();
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public String getInstitution(ChannelSpecifier path) throws IllegalArgumentException {
		try {
			return getReader(path) == null ? null : getReader(path).getInstitution();
		} catch (IOException e)  {
			return null;
		}
	}

	@Override
	public String getAcquisitionSystem(ChannelSpecifier path)
			throws IllegalArgumentException {
		try {
			return getReader(path) == null ? null : getReader(path).getAcquisitionSystem();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public String getChannelName(ChannelSpecifier path) throws IllegalArgumentException {
		try {
			return getReader(path) == null ? null : getReader(path).getChannelName();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public String getChannelComments(ChannelSpecifier path)
			throws IllegalArgumentException {
		try {
			return getReader(path) == null ? null : getReader(path).getChannelComments();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public String getSubjectId(ChannelSpecifier path) throws IllegalArgumentException {
		try {
			return getReader(path) == null ? null : getReader(path).getSubjectId();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public double getVoltageConversionFactor(ChannelSpecifier path) {
		try {
			return getReader(path) == null ? 0 : getReader(path).getVoltageConversionFactor();
		} catch (IOException e) {
			return 0;
		}
	}

	static final int[] nul = new int[0];

	void doWork() {
	}
	
	@Override
	public ArrayList<TimeSeriesData> requestRead(//SnapshotSpecifier source, 
			ChannelSpecifier path,
			long startTimeOffset, long endTimeOffset,
			edu.upenn.cis.db.mefview.services.PostProcessor postProc, User user, 
			SessionToken sessionToken)
			throws InterruptedException, ExecutionException, IOException, ServerTimeoutException {
		final String M = "MEFPageServer.requestRead()";

		ChannelSpecifier key = path;
		MEFObjectReader reader = getReader(key);
		
		double freq = ((MEFObjectReader) reader).getSampleRate();
		long startHandler = System.nanoTime();
		
		double targetFrequency = postProc.getSampleRate();
		double targetPeriod = 1.E6 / targetFrequency;
		
		if (logger.isTraceEnabled()) {
			logger.trace(M + ":  * " + path + " requesting read from offset "
					+ startTimeOffset + " to " + endTimeOffset + " @"
					+ targetFrequency);
		}

		final long startTime = reader.getStartTime() + startTimeOffset - path.getTimeOffset();
		final long endTime = startTime + (endTimeOffset - startTimeOffset);

		if (logger.isTraceEnabled()) {
			// +
			logger.trace(M + ":    " + path + " converted to read from "
					+ startTime
					+ " to " + endTime + "(" + (endTime - startTime) + ") @"
					+ targetFrequency);
		}

		double scale = ((MEFObjectReader) reader).getVoltageConversionFactor();
		double startT = ((MEFObjectReader) reader).getStartTime();
		double endT = ((MEFObjectReader) reader).getEndTime();
		
		long paddingMicros = (long) Math.ceil(targetPeriod
				* getPaddingSamples());
		
		double startReq = startTime;
		double endReq = endTime;
		
		double startPad = 0;
		double endPad = 0;
		
		if (startReq < startT) {
			startPad += startT - startReq;
			startReq = startT;
		}
		if (startReq > endT) {
			startPad += startReq - endT;
			startReq = endT;
		}
		if (endReq > endT) {
			endReq = endT;
		}

		startReq -= paddingMicros;
		endReq += paddingMicros;
		
		System.out.println(path.getLabel() + " read blocks from " + startTime + " (" + (long)startReq + ") - " + endTime + " with start @" + reader.getStartTime());

		PageReader.PageList pages = null;
		try {
			pages = reader.getPages((long) (startReq), (long) (endReq), 0);
		} catch (IOException e) {
			logger.error(M + ": Caught, returning null", e);
			return null;
		}

		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Reading "
					+ (pages.endPage - pages.startPage + 1)
					+ " pages");
		}
		long nextHandler = System.nanoTime();
		long readIndex = nextHandler - startHandler;

		// List<MEFPage> pageList = null;
		TimeSeriesPage[] pageList = null;

		try {
			pageList = requestPages(/* source + */path, pages.startPage,
					pages.endPage, 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(M + ": Caught, ignoring", e);
		}

		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Retrieved " + (pageList.length) + " pages");
		}
		
		long readHandler = System.nanoTime(); 
		long readPages = readHandler - nextHandler;

		ArrayList<TimeSeriesData> results = new ArrayList<TimeSeriesData>();
		
		results = postProc.process(startTime, endTime, freq, startTimeOffset, 
				scale, pageList, path, false, paddingMicros, paddingMicros);
		

		long spanHandler = System.nanoTime();
		long createSpan = spanHandler - readHandler;
		
//		nextHandler = System.nanoTime();

		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Stats: readIndex " + readIndex + "; readPages "
					+ readPages + "; createSpan "
					+ createSpan);
		}

		return results;
	}

	@Override
	protected List<List<TimeSeriesData>>
	requestMultiChannelReadBlock(
			final List<? extends ChannelSpecifier> paths,
			long startTime, long endTime, PostProcessor[] postProc)
			throws InterruptedException, ExecutionException, ServerTimeoutException, IOException {
		final String M = "requestMultiChannelReadBlock(...)";
		List<List<TimeSeriesData>> ret = new ArrayList<List<TimeSeriesData>>(
				paths.size());
		List<Future<List<TimeSeriesData>>> pending = new ArrayList<Future<List<TimeSeriesData>>>(
				paths.size());
		
		double freq = Double.MIN_VALUE;
		for (ChannelSpecifier path: paths) {
			MEFObjectReader reader = getReader(path);//key);
			if (reader.getSampleRate() > freq)
				freq = reader.getSampleRate();
		}
		
		int size = (int)((endTime - startTime) / 
				freq + 1);
		
		if (size <= 0) {
			throw new RuntimeException("Illegal size!");
		} else if (size > 5E6) {
			logger.trace("Large request");
		}
		
		if (logger.isTraceEnabled()) {
			logger.trace("MEFPageServer requestMultiChannelReadBlock creating downsampling buffer with "
					+ paths.size() + " rows and " + size + " elements");
		}

		int[][] buf = null;

		boolean reset = false;
		for (PostProcessor proc: postProc) {
			if (proc.needsWorkBuffer(size)) {
				reset = true;
			}
		}
		if (reset)
			buf = new int[paths.size()][size];
		
		logger.trace("MEFPageServer requestMultiChannelReadBlock is " + (reset ? "": "not ") +
				"creating a buffer");
		
		long readIn = System.nanoTime();
		
		checkArgument(paths.size() == postProc.length, "paths and postProc must have the same size");
		int i = 0;
		for (ChannelSpecifier path : paths) {
			if (reset)
				postProc[i].setWorkBuffer(buf[i]);
			pending.add(exec2.submit(new TraceLoader( 
					path,
					startTime, endTime, postProc[i])));
			i++;
		}

		boolean wasError = false;
		String msg = "";

		for (Future<List<TimeSeriesData>> p : pending) {
			try {
				ret.add(p.get());
			} catch (Exception e) {
				e.printStackTrace();
				wasError = true;
				msg = msg + e.getMessage() + e.getStackTrace();
			}
		}
		
		timeLogger.trace("{}: reading and processing time series {} seconds", M, BtUtil.diffNowThenSeconds(readIn));
		
		if (wasError)
			throw new RuntimeException(msg);

		return ret;
	}

	/**
	 * Find the actual MEF file corresponding to dataset/series
	 * 
	 * @param datasetID
	 * @param series
	 * @return
	 * @throws AuthorizationException
	 */
	public String getTracePath(final User user,
			final String datasetID, final String series)
			throws AuthorizationException {
		final String M = "getTracePath(...)";
		String fullName = /* datasetID + "/" + */series;
		if (paths.containsKey(fullName))
			return paths.get(fullName);
		else {
			String path = getOnlyElement(ds.getMEFPaths(user, datasetID,
					singleton(series)));
			if (logger.isDebugEnabled()) {
				logger.debug("{}: Adding {} -> {} to paths cache", new Object[] {M, fullName, path});
			}
			paths.put(fullName, path);
			return path;
		}
	}

	@Override
	public MEFSnapshotSpecifier getSnapshotSpecifier(
			User user, String revId) throws AuthorizationException {
		return new MEFSnapshotSpecifier(sourcePath, revId);
	}
	
	/**
	 * Returns native rate.
	 * 
	 * @param user
	 * @param snapshot
	 * @param revId
	 * @return
	 * @throws AuthorizationException is user is not authorized to read snapshot.
	 */
	private double getNativeRate(final User user, 
			final edu.upenn.cis.db.mefview.services.SnapshotSpecifier snapshot,
			final String revId) {
		final String M = "getNativeRate(...)";
		if (snapshot instanceof MEFSnapshotSpecifier) {
			String fullName = revId;
			
			MEFChannelSpecifier fil;
			
			if (paths.containsKey(fullName))
				fil = new MEFChannelSpecifier((MEFSnapshotSpecifier)snapshot, paths.get(fullName), revId, 0, "1");
			else {
				String path = getTracePath(user, snapshot.getRevID(), revId);//getOnlyElement(ds.getMEFPaths(user, snapshot.getRevID(), singleton(revId)));
				paths.put(fullName, path);
				fil = new MEFChannelSpecifier((MEFSnapshotSpecifier)snapshot, path, revId, 0, "1");
				if (logger.isDebugEnabled()) {
					logger.debug("{}: Adding {} -> {} to paths cache", new Object[] {M, fullName, path});
				}
			}
			
			return this.getSampleRate(fil);
		} else
			throw new IllegalArgumentException("Illegal to use MEF reader with non-MEF snapshot token!");
	}

	/**
	 * @throws AuthorizationException if user is not authorized to read snapshot.
	 */
	@Override
	public MEFChannelSpecifier getChannelSpecifier(final User user, 
			final edu.upenn.cis.db.mefview.services.SnapshotSpecifier snapshot,
			String revId, final double samplingPeriod) {
		final String M = "getChannelSpecifier(...)";
		
		if (snapshot instanceof MEFSnapshotSpecifier) {
			String fullName = /* datasetID + "/" + */revId;
			double rate = 0;
			
			if (samplingPeriod > 0)
				rate = 1.E6 / samplingPeriod;
			
			if (!downsampled)
				rate = 0;

			// If rate == 0 then we are good.
			// If rate < 0 then we need to do a scale factor (which means we need.
			//  to get the native rate)
			// If rate > 0 then we need to find the closest match
			
			if (rate == -1)	// -1 = full sample rate, which is the same as 0
				rate = 0;
			if (rate != 0) {
				if (rate < 0) {
					double realRate = getNativeRate(user, snapshot, revId);
					
					rate = realRate / (-rate);
				} else
					rate = 1.E6 / samplingPeriod;
				
				
				//// Find nearest upwards match...?
				if (rate < downsampledRate)
					rate = downsampledRate;
				
				fullName = fullName + "-" + String.valueOf((int)rate);
			}
			
			if (pathSpec.containsKey(fullName)) {
//				logger.debug("{}: Returning ID {}", new Object[] {M, revId});
//				return new MEFChannelSpecifier((MEFSnapshotSpecifier)snapshot, paths.get(fullName), revId);
				return pathSpec.get(fullName);
			} else {
				String path = getTracePath(user, snapshot.getRevID(), revId);//getOnlyElement(ds.getMEFPaths(user, snapshot.getRevID(), singleton(revId)));
				if (rate != 0) {
					String oldPath = path;
					if (path.endsWith(".mef")) {
						path = path.substring(0, path.length() - 4) +  "-" + String.valueOf((int)rate) + ".mef";
					} else
						path = path + "-" + String.valueOf((int)rate);
					
					if (!(new File(((MEFSnapshotSpecifier)snapshot).getPath() + path)).exists()) {
						path = oldPath;
						rate = 0;
					}
				}
				paths.put(fullName, path);

				if (logger.isDebugEnabled()) {
					logger.debug("{}: Adding ID {} path {}", new Object[] {M, revId, path});
				}
				MEFChannelSpecifier spec = new MEFChannelSpecifier((MEFSnapshotSpecifier)snapshot, path, revId, (int)rate, "1");
				pathSpec.put(fullName, spec);
				return spec;
			}
		} else
			throw new IllegalArgumentException("Illegal to use MEF reader with non-MEF snapshot token!");
	}

	@Override
	public List<MEFDataAndPageNos> requestMultiChannelReadRed(
			List<? extends ChannelSpecifier> chSpecs,
			long startTimeOffset, long endTimeOffset, User user) throws StaleMEFException, ServerTimeoutException {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getDataVersion(ChannelSpecifier chSpec) {
		//throw new UnsupportedOperationException();
		return chSpec.getId();
	}

	@Override
	public GeneralMetadata getMetadata(ChannelSpecifier channelSpec) {
		return new TraceInfo(
				null,//((GeneralMetadata)channelSpec).getParent(),
				getStartUutc(channelSpec),//0,//getStartTime(channelSpec), 
				getEndUutc(channelSpec) - getStartUutc(channelSpec), 
				getSampleRate(channelSpec),
				getVoltageConversionFactor(channelSpec), 
				getMinSampleValue(channelSpec), 
				getMaxSampleValue(channelSpec),
				getInstitution(channelSpec), 
				getAcquisitionSystem(channelSpec), 
				getChannelComments(channelSpec),
				getSubjectId(channelSpec), 
				getChannelName(channelSpec), 
				channelSpec.getId());
	}

	public void setParent(GeneralMetadata parent) {}

	@Override
	public InputStream getData(String key, String eTag, Long startByteOffset,
			Long endByteOffset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getSizeBytes(ChannelSpecifier channelSpecifier) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, ChannelSpecifier> getChannelSpecifiersFromSegments(User user, SnapshotSpecifier dsSpec,
			Set<INamedTimeSegment> tsIds, double samplingPeriod) throws AuthorizationException {
		return getChannelSpecifiers(user, dsSpec, DataMarshalling.getTimeSegmentIds(tsIds), samplingPeriod);
	}
}
