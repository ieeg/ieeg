/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.views;

public class ImageView implements IDataView {
	String filename = "";
	int top;
	int left;
	int right;
	int bottom;
	
	public ImageView() {}
	
	public ImageView(String filename, int top, int left, int right, int bottom) {
		super();
		this.filename = filename;
		this.top = top;
		this.left = left;
		this.right = right;
		this.bottom = bottom;
	}
	
	@Override
	public boolean isCompatibleWith(IDataView two) {
		return two instanceof ImageView;
	}
	
	@Override
	public boolean overlapsWith(IDataView two) {
		if (isCompatibleWith(two)) {
			ImageView iv = (ImageView)two;
			
			return filename.equals(iv.filename) && top <= iv.bottom && bottom >= iv.top &&
					left <= iv.right && right >= iv.left;
		}
		return false;
	}
	@Override
	public boolean contains(IDataView two) {
		if (isCompatibleWith(two)) {
			ImageView iv = (ImageView)two;
			
			return filename.equals(iv.filename) && top <= iv.top && bottom >= iv.bottom &&
					left <= iv.left && right >= iv.right;
		}
		return false;
	}
	

	@Override
	public int hashCode() {
		return filename.hashCode();
	}
}
