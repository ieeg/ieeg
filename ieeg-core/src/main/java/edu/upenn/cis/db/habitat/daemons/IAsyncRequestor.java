/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.daemons;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.exceptions.RequestFailedException;

/**
 * Basic pub/sub daemon requestor
 * 
 * @author zives
 *
 * @param <K> Request message with key + parameters
 * @param <V> Returned response (if applicable)
 */
public interface IAsyncRequestor<K,V> {
	/**
	 * Stream interface: send a message
	 * 
	 * @param keyValue
	 */
	public void stream(K keyValue);
	
	/**
	 * Stream interface: register success + failure handlers
	 * @param onSuccess
	 * @param onFailure
	 */
	public void register(String channel, K key, 
			IFutureResult<K,V> onSuccess, 
			IFutureResult<K,String> onFailure,
			boolean once);
	
	/**
	 * Asynchronous request with registered callbacks
	 * 
	 * @param key
	 * @param onSuccess
	 * @param onFailure
	 */
	public void request(K key, 
			IFutureResult<K,V> onSuccess, 
			IFutureResult<K,String> onFailure);
	
	/**
	 * Synchronous request with return result (with timeout)
	 * 
	 * @param key
	 * @param timeout, -1 if no timeout
	 * @return
	 * @throws RequestFailedException
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public V requestSync(K key, long timeout) throws RequestFailedException, TimeoutException, InterruptedException, ExecutionException;
}
