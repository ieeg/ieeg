/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.common.annotations.GwtCompatible;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;


@GwtCompatible(serializable=true)
public class AnnotationScheme implements JsonTyped, Serializable {
	/**
   * 
   */
  private static final long serialVersionUID = 1L;

  public static AnnotationScheme defaultScheme = new AnnotationScheme();
	
	public static final Map<String, AnnotationScheme> defaultSchemes;
    static {
      Map<String, AnnotationScheme> tmpScheme = new HashMap<String, AnnotationScheme>();
      tmpScheme.put("Scheme 1", new AnnotationScheme("Scheme 1", "blue"));
      tmpScheme.put("Scheme 2", new AnnotationScheme("Scheme 2", "red"));
      tmpScheme.put("Scheme 3", new AnnotationScheme("Scheme 3", "green"));
      tmpScheme.put("Scheme 4", new AnnotationScheme("Scheme 4", "darkorchid"));
      tmpScheme.put("Scheme 5", new AnnotationScheme("Scheme 4", "brown"));
      tmpScheme.put("Scheme 6", new AnnotationScheme("Scheme 4", "indigo"));
      tmpScheme.put("Scheme 7", new AnnotationScheme("Scheme 4", "darkgreen"));
      tmpScheme.put("Scheme 8", new AnnotationScheme("Scheme 4", "black"));
      defaultSchemes = tmpScheme;
    }
	
	
	private String color;
	private String name;
	
	public AnnotationScheme() {
	  name = "Default";
	  color = "grey";
	}
	
	public AnnotationScheme(String str, String clr){
	  name = str;
      color = clr;
	}
	
	public String getShortName(String typ) {
		return "";
	}
	
	public String getCssStyle(String typ) {
		return "";
	}
	
	public String getCssStyle(String typ, String styleGroup) {
		return "";
	}
	
		
	public static String getDefaultShortName(String typ) {
		String typ2 = typ.toLowerCase();
		if (typ.indexOf(':') != -1)
			typ2 = typ.substring(typ.lastIndexOf(':') + 1);
		
		if (typ2.equals("seizure"))
			return "Z";
		else if (typ2.equals("scrub event"))
			return "E";
		else if (typ2.equals("line length event"))
			return "L";
		else if (typ2.equals("dcn event"))
			return "D";
		else if (typ2.equals("artifact"))
			return "A";
		else if (typ2.equals("microseizure"))
			return "M";
		else if (typ2.equals("fast ripple"))
			return "F";
		else if (typ2.equals("hfo"))
			return "H";
		else if (typ2.equals("spike"))
			return "S";
		else
			return String.valueOf(typ2.charAt(0));
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String col) {
		color = col;
	}
	
	/*
	 * Returns copy of one of the default schemes.
	 */
	public static AnnotationScheme getDefaultScheme(int index){
    
	  int length = defaultSchemes.size();
	  int retIndex = index%length + 1;
	  
	  AnnotationScheme retScheme = new AnnotationScheme("Scheme "+ index,
	      defaultSchemes.get("Scheme " + retIndex).getColor());
	  
	  return retScheme;
	  
	}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  public String toString(){
    return this.name + " : " + this.color;
  }
  
  public static final ProvidesKey<AnnotationScheme> KEY_PROVIDER = new ProvidesKey<AnnotationScheme>() {
    public Object getKey(AnnotationScheme item) {
        return (item == null) ? null : item.name;
    }    
  };
  public static final TextColumn<AnnotationScheme> schemeColumn = new TextColumn<AnnotationScheme> () {
    @Override
    public String getValue(AnnotationScheme object) {
      return object.name;
    }
  };
 
}
