/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services.assembler;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;

public class TsAnnotationAssembler {

	Function<TsAnnotationDto, TimeSeriesAnnotation> toTimeSeriesAnnotation = new Function<TsAnnotationDto, TimeSeriesAnnotation>() {

		public TimeSeriesAnnotation apply(TsAnnotationDto input) {
			return toTimeSeriesAnnotation(input);
		}

	};

	Function<TimeSeriesAnnotation, TsAnnotationDto> toTsAnnotationDto = new Function<TimeSeriesAnnotation, TsAnnotationDto>() {

		public TsAnnotationDto apply(TimeSeriesAnnotation input) {
			return toTsAnnotationDto(input);
		}

	};

	private final TimeSeriesAssembler tsAssembler;

	public TsAnnotationAssembler() {
		this.tsAssembler = new TimeSeriesAssembler();
	}

	public TsAnnotationAssembler(final TimeSeriesAssembler tsAssembler) {
		this.tsAssembler = tsAssembler;
	}

	/**
	 * Returns a new {@link TsAnnotationDto} with same info as
	 * {@code timeSeriesAnnotation}.
	 * 
	 * @param timeSeriesAnnotation
	 * @return a new {@link TimeSeriesDto} with same info as
	 *         {@code timeSeriesAnnotation}
	 */
	public TsAnnotationDto toTsAnnotationDto(
			final TimeSeriesAnnotation timeSeriesAnnotation) {
		final SortedSet<TimeSeries> annotated = timeSeriesAnnotation
				.getAnnotatedSeries();
		final TsAnnotationDto gtsa = new TsAnnotationDto(
				tsAssembler.toTimeSeriesDtoSet(annotated),
				timeSeriesAnnotation.getAnnotator(),
				timeSeriesAnnotation.getStartTimeUutc(),
				timeSeriesAnnotation.getEndTimeUutc(),
				timeSeriesAnnotation.getType(),
				timeSeriesAnnotation.getRevId(),
				timeSeriesAnnotation.getDescription(),
				null,
				timeSeriesAnnotation.getLayer(),
				timeSeriesAnnotation.getColor());
		return gtsa;

	}

	/**
	 * Returns a new {@link TimeSeriesAnnotation} with same info as
	 * {@code gwtTimeSeriesAnnotation}.
	 * 
	 * @param tsAnnotationDto
	 * @return a new {@link TimeSeriesAnnotation} with same info as
	 *         {@code gwtTimeSeriesAnnotation}
	 */

	public TimeSeriesAnnotation toTimeSeriesAnnotation(
			final TsAnnotationDto tsAnnotationDto) {
		final Set<TimeSeriesDto> dtoAnnotated = tsAnnotationDto.getAnnotated();
		final SortedSet<TimeSeries> annotated = tsAssembler
				.toTimeSeriesSortedSet(dtoAnnotated);
		final TimeSeriesAnnotation tsa = new TimeSeriesAnnotation(
				annotated,
				tsAnnotationDto.getAnnotator(),
				tsAnnotationDto.getStartOffsetMicros(),
				tsAnnotationDto.getEndOffsetMicros(),
				tsAnnotationDto.getType(),
				tsAnnotationDto.getDescription(),
				tsAnnotationDto.getLayer());
		tsa.setRevId(tsAnnotationDto.getId());
		return tsa;

	}

	public List<TimeSeriesAnnotation> toTimeSeriesAnnotationList(
			final Collection<TsAnnotationDto> tsAnnotationDtoSet) {
		final List<TimeSeriesAnnotation> timeSeriesAnnotationList = newArrayList(transform(
				tsAnnotationDtoSet,
				toTimeSeriesAnnotation));
		return timeSeriesAnnotationList;
	}

	public Set<TsAnnotationDto> toTsAnnotationDtoSet(
			final Collection<TimeSeriesAnnotation> timeSeriesAnnotationSet) {
		final Set<TsAnnotationDto> tsAnnotationDtoSet = newHashSet(transform(
				timeSeriesAnnotationSet,
				toTsAnnotationDto));
		return tsAnnotationDtoSet;
	}
}
