/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import com.google.common.annotations.GwtCompatible;

/**
 * A simple hash table-based sparse representation of a vector
 * 
 * @author Zack Ives
 *
 */
@GwtCompatible(serializable=true)
public class BasicFeatureVector implements IFeatureVector, Serializable {
	Map<String,Boolean> features = new HashMap<String, Boolean>();
	List<String> featureNames;
	
	static AtomicLong vectorIdGenerator = new AtomicLong(0);
	
	public static long getUniqueId() {
//		System.out.println("* id *");
		return vectorIdGenerator.getAndAdd(1);
	}
	
	boolean isMemod = false;
	String memodSig = "";
	long id;
	
	public BasicFeatureVector() { }
	
	public BasicFeatureVector(List<String> featureNames) {
		this.featureNames = featureNames;
		this.id = getUniqueId();
	}
	
	public BasicFeatureVector(List<String> featureNames, boolean setAll) {
		this.featureNames = new ArrayList<String>();
		this.featureNames.addAll(featureNames);
		this.id = getUniqueId();
		if (setAll)
			for (String f: featureNames)
				setFeatureNamePresent(f);
	}
	
	public BasicFeatureVector(String featureName, boolean set) {
		this.featureNames = new ArrayList<String>();
		featureNames.add(featureName);
		this.id = getUniqueId();
		if (set)
			setFeatureNamePresent(featureName);
	}

	public BasicFeatureVector(IFeatureVector two) {
		featureNames = new ArrayList<String>();
		featureNames.addAll(two.getFeatureNames());
		this.id = getUniqueId();
		
		for (String f: two.getFeatures().keySet())
			features.put(f, Boolean.TRUE);
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof BasicFeatureVector))
			return false;
		else return ((BasicFeatureVector)o).id == id;
	}

	@Override
	public int getNumFeatures() {
		return featureNames.size();
	}

	@Override
	public List<String> getFeatureNames() {
		return featureNames;
	}
	
	@Override
	public void addFeature(String feature) {
		if (!featureNames.contains(feature))
			featureNames.add(feature);
	}

	@Override
	public void addFeature(String feature, boolean set) {
		if (!featureNames.contains(feature))
			featureNames.add(feature);
		if (set)
			features.put(feature, Boolean.TRUE);
	}

	@Override
	public boolean getFeaturePresent(int inx) {
		Boolean b = features.get(featureNames.get(inx));
		return b != null && b.equals(Boolean.TRUE);
	}

	@Override
	public String getSignature() {
		if (isMemod)
			return memodSig;
		
		StringBuilder sb = new StringBuilder();
		
		for (String key: getFeatureNames())
			if (getFeatureNamePresent(key)) {
				sb.append('\0');
				sb.append(key);
			}
		
		String ret = sb.toString();
		memodSig = ret;
		isMemod = true;
		return ret;
	}

	@Override
	public IFeatureVector concat(IFeatureVector second) {
		for (String fn : featureNames)
			if (second.getFeatureNames().contains(fn)) {
				if (features.containsKey(fn) && !second.getFeatureNames().contains(fn) ||
						second.getFeatureNames().contains(fn) && !features.containsKey(fn) ||
						!features.get(fn).equals(second.getFeatures().get(fn)))
					throw new RuntimeException("Two vectors with same feature name but different values");
			}

		BasicFeatureVector n = new BasicFeatureVector(featureNames);
		n.featureNames.addAll(second.getFeatureNames());
		
		for (Entry<String,Boolean> ent: features.entrySet()) {
			if (ent.getValue())
				n.setFeatureNamePresent(ent.getKey());
		}
		for (Entry<String,Boolean> ent: second.getFeatures().entrySet()) {
			if (ent.getValue())
				n.setFeatureNamePresent(ent.getKey());
		}

		return n;
	}

	@Override
	public void setFeaturePresent(int inx) {
		features.put(featureNames.get(inx), Boolean.TRUE);
	}

	@Override
	public void clearFeaturePresent(int inx) {
		features.remove(featureNames.get(inx));
	}

	@Override
	public Map<String, Boolean> getFeatures() {
		return features;
	}

	@Override
	public boolean getFeatureNamePresent(String name) {
		Boolean b = features.get(name);
		return b != null && b.equals(Boolean.TRUE);
	}

	@Override
	public void setFeatureNamePresent(String name) {
		features.put(name, Boolean.TRUE);
	}

	@Override
	public void clearFeatureNamePresent(String name) {
		features.remove(name);
	}

	@Override
	public void addAll(IFeatureVector features2) {
		for (String f: features2.getFeatures().keySet()) {
				if (!featureNames.contains(f))
					featureNames.add(f);
				
				setFeatureNamePresent(f);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		
		ret.append('[');
		boolean isFirst = true;
		for (String f: featureNames) {
			if (isFirst)
				isFirst = false;
			else
				ret.append(',');
			if (f != null) {
				if (getFeatureNamePresent(f)) {
					ret.append(f.toUpperCase());
				} else
					ret.append(f.toLowerCase());
			}
		}
		
		ret.append(']');
		return ret.toString();
	}

	@Override
	public double getCost(IWeightVector vector) {
		double weight = 0;
		for (String f: features.keySet()) {
			weight += vector.getWeight(f);
		}
		return weight;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public Set<String> getFeaturesPresent() {
		return features.keySet();
	}
	
	@Override
	public int hashCode() {
		return features.keySet().hashCode();
	}
}
