/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserPrefs implements Serializable, IsSerializable, JsonTyped {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  Map<String, AnnotationScheme> annSchemes = new HashMap<String, AnnotationScheme>();


  public void setAnnSchemes(Map<String, AnnotationScheme> annSchemes) {
    this.annSchemes = annSchemes;
  }

  /*
   * Gets or creates new annotationScheme
   */
  public AnnotationScheme getAnnotationScheme(String name){

    AnnotationScheme scheme = annSchemes.get(name);
    if (scheme == null){
      int length = annSchemes.size();
      scheme = AnnotationScheme.getDefaultScheme(length + 1);
      scheme.setName(name);
      annSchemes.put(name, scheme);
    }

    return scheme;

  }
  
  public void renameAnnotationScheme(String oldName, String newName){
    
    AnnotationScheme scheme = annSchemes.get(oldName);
    if (scheme !=null){
      annSchemes.remove(scheme.getName());
      scheme.setName(newName);
      annSchemes.put(newName, scheme);
      
      
    }

  }
  
  public void addAnnotationScheme(AnnotationScheme scheme) {
    
    if (!annSchemes.containsKey(scheme.getName())){
      annSchemes.put(scheme.getName(), scheme);
    }
    
    
  }

  public Map<String, AnnotationScheme> getAnnSchemes() {
    return annSchemes;
  }
  

}
