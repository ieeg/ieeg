/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Sets;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UserInfo extends PresentableMetadata implements Serializable {
	private static final long serialVersionUID = 1L;
	private SessionToken sessionToken;
	private String photo;
	private String login;
	private String name;
	private boolean isGuest = false;
	private int impact;
	private int studies;
	private int analyses;
	private int tools;
	private long annotations;
	private UserProfile profile;
	String location = "habitat://main";

//	static MetadataFormatter formatter = null;

	public UserProfile getProfile() {
		return profile;
	}

	public void setProfile(UserProfile profile) {
		this.profile = profile;
	}

	private ISecurityCredentials uid;

	public UserInfo() {

	}

	public UserInfo(SessionToken sessionToken, String photo, ISecurityCredentials uid) {
		this.sessionToken = sessionToken;
		this.photo = photo;
		this.uid = uid;
	}

	public UserInfo(String name, String photo, ISecurityCredentials uid) {
		this.name = name;
		this.login = name;
		this.photo = photo;
		this.uid = uid;
	}

	public SessionToken getSessionToken() {
		return sessionToken;
	}

	public void setSessionToken(SessionToken sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public ISecurityCredentials getUid() {
		return uid;
	}

	public void setUid(UserId uid) {
		this.uid = uid;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getImpact() {
		return impact;
	}

	public void setImpact(int impact) {
		this.impact = impact;
	}

	public int getStudies() {
		return studies;
	}

	public void setStudies(int studies) {
		this.studies = studies;
	}

	public int getAnalyses() {
		return analyses;
	}

	public void setAnalyses(int analyses) {
		this.analyses = analyses;
	}

	public int getTools() {
		return tools;
	}

	public void setTools(int tools) {
		this.tools = tools;
	}

	public long getAnnotations() {
		return annotations;
	}

	public void setAnnotations(long annotations) {
		this.annotations = annotations;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof UserInfo))
			return false;
		else {
			return this.name.equals(((UserInfo) o).name);
		}
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	public void setGuest(boolean guest) {
		isGuest = guest;
	}

	public boolean isGuest() {
		return isGuest;
	}

	@Override
	public String toString() {
		return "UserInfo: { name=" + name + ", uid=" + uid + "}";
	}

	@Override
	public SerializableMetadata getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMetadataCategory() {
		return GeneralMetadata.BASIC_CATEGORIES.Users.name();
	}

	@Override
	public String getLabel() {
		return getName();
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return new HashSet<String>();
	}

	@Override
	public String getStringValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getId() {
		return getName();
	}

	@Override
	public String getFriendlyName() {
		return getName();
	}

	@Override
	public List<String> getCreators() {
		return new ArrayList<String>();
	}

	@Override
	public String getContentDescriptor() {
		return "User " + getName();
	}

	@Override
	public Date getCreationTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Set<String>> getUserPermissions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.USER);
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}
//
//	public static void setFormatter(MetadataFormatter presenter) {
//		UserInfo.formatter = presenter;
//	}
//
//	public static MetadataFormatter getDefaultFormatter() {
//		return formatter;
//	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@JsonIgnore
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "User";
	}
}
