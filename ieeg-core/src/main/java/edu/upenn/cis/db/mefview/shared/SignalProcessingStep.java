/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Represents a signal processing step at the server-side
 * 
 * @author zives
 *
 */
public class SignalProcessingStep implements IsSerializable, JsonTyped {
	String stepName;
	JsonTyped parametersJson;
	
	SignalProcessingStep nextStep;
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public JsonTyped getParametersJson() {
		return parametersJson;
	}
	public void setParametersJson(JsonTyped parametersJson) {
		this.parametersJson = parametersJson;
	}
	public SignalProcessingStep getNextStep() {
		return nextStep;
	}
	public void setNextStep(SignalProcessingStep nextStep) {
		this.nextStep = nextStep;
	}
	
	public SignalProcessingStep(String stepName, JsonTyped parametersJson,
			SignalProcessingStep nextStep) {
		super();
		this.stepName = stepName;
		this.parametersJson = parametersJson;
		this.nextStep = nextStep;
	}
	
	public SignalProcessingStep(String stepName, JsonTyped parametersJson) {
		super();
		this.stepName = stepName;
		this.parametersJson = parametersJson;
		this.nextStep = null;
	}
	
	public SignalProcessingStep() {
		super();
		this.stepName = null;
		this.parametersJson = null;
		this.nextStep = null;
	}
}
