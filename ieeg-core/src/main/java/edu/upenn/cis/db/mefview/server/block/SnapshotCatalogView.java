/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredMap;

public class SnapshotCatalogView {
	public static class Interval implements Serializable {
		private long start;
		private long stop;
		
		public Interval(long start, long stop) {
			this.start = start;
			this.stop = stop;
		}
		
		
		
		public long getStart() {
			return start;
		}



		public void setStart(long start) {
			this.start = start;
		}



		public long getStop() {
			return stop;
		}



		public void setStop(long stop) {
			this.stop = stop;
		}



		public String toString() {
			return "[" + start + "," + stop + "]";
		}
	}
	
	public static class SnapshotConfig implements Serializable {
		private Map<Double,String> options;
		private List<Interval> gaps;
		
		private List<String> channels;
		
		public SnapshotConfig() {
			options = new HashMap<Double,String>();
			gaps = new ArrayList<Interval>();
			channels = new ArrayList<String>();
		}
		
		public Set<Double> getFrequencies() {
			return options.keySet();
		}
		
		public Collection<String> getDatabases() {
			return options.values();
		}
		
		public String getDatabaseFor(double freq) {
			return options.get(freq);
		}
		
		public void addDatabase(double freq, String databaseName) {
			options.put(freq, databaseName);
		}

		public List<Interval> getGaps() {
			return gaps;
		}

		public void setGaps(List<Interval> gaps) {
			this.gaps = gaps;
		}

		public List<String> getChannels() {
			return channels;
		}

		public void setChannels(List<String> channels) {
			this.channels = channels;
		}
		

	};
	
	public static class SnapshotKey implements Serializable {
		private String snapshotId;

		public SnapshotKey(final String sid) {
			snapshotId = sid;
		}
		
		public final String getString() {
			return snapshotId;
		}
		
		public String toString() {
			return snapshotId;
		}
	};
	
	
	private StoredMap<SnapshotKey,SnapshotConfig> snapshotMap;
	
	public SnapshotCatalogView(SnapshotCatalogDb db, boolean readWrite) {
		ClassCatalog catalog = db.getClassCatalog();
		
		EntryBinding<SnapshotKey> snapshotKeyBinding = new SerialBinding<SnapshotKey>(catalog, SnapshotKey.class);
		EntryBinding<SnapshotConfig> snapshotConfigBinding = new SerialBinding<SnapshotConfig>(catalog, SnapshotConfig.class);
		
		snapshotMap = new StoredMap<SnapshotKey,SnapshotConfig>(db.getSnapshotDatabase(), snapshotKeyBinding, snapshotConfigBinding, readWrite);
	}
	
	public final StoredMap<SnapshotKey,SnapshotConfig> getSnapshotMap() {
		return snapshotMap;
	}
	
	@SuppressWarnings("unchecked")
	public final StoredEntrySet<SnapshotKey,SnapshotConfig> getSnapshotEntrySet() {
		return (StoredEntrySet<SnapshotKey,SnapshotConfig>) snapshotMap.entrySet();
	}
}
