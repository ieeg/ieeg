/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.IvProps;

/**
 * Skeleton server for sending SMTP messages
 * 
 * @author zives
 *
 */
public class SmtpServer extends MessageServer {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Session session;

	/**
	 * Default constructor: get settings from IvProps
	 */
	public SmtpServer() {
		this(IvProps.getMailServer(),
				IvProps.getMailPort(),
				IvProps.getMailLogin(),
				IvProps.getMailPassword(),
				IvProps.isMailSSL());
	}

	/**
	 * Custom constructor: initialize server etc
	 * 
	 * @param server
	 * @param port
	 * @param login
	 * @param pass
	 * @param ssl
	 */
	public SmtpServer(
			final String server,
			final int port,
			final String login,
			final String pass,
			final boolean ssl) {
		final Properties props = new Properties();
		//props.put("mail.debug", "true");
		props.put("mail.smtp.host", server);
		props.put("mail.smtp.port", port);
		if (ssl) {
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.auth", "true"); // If you need to authenticate
		}

		session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(login, pass);
					}
				});
	}

	@Override
	public void sendMessage(
			final String recipients,
			final String title,
			final String sender,
			final String body) {
		final String m = "sendMessage(...)";
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(sender));
			msg.addRecipients(Message.RecipientType.TO,
					recipients);
			msg.setSubject(title);
			msg.setSentDate(new Date());
			msg.setText(body);
			Transport.send(msg);
		} catch (MessagingException mex) {
			logger.error(m
					+ ": Error sending message ["
					+ body
					+ "] with title ["
					+ title
					+ "] to ["
					+ recipients
					+ "] from ["
					+ sender
					+ "]",
					mex);
		}
	}
}
