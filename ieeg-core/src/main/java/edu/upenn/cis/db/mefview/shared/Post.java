/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Lists;

@GwtCompatible(serializable = true)
public class Post implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String postId;
	private String snapId;
	private Long discId;

	private String refId;
	private String authorId;
	private String title;
	private String post;
	private Timestamp time;
	
	private String annId;
	
	private List<Post> replies = Lists.newArrayList();
	
	public Post() {
		postId = "";
		title = "";
		snapId = "";
		refId = "";
		authorId = "";
		post = "";
		time = new Timestamp(new java.util.Date().getTime());
		annId = "";
	}
	
	public Post(
			final String author,
			final String title,
			final String post,
			final String snapshotId,
			
			final String postId,
			final String refId,
			final String annId
			) {
		this.authorId = checkNotNull(author);
		this.post = checkNotNull(post);
		this.title = checkNotNull(title);
		this.snapId = checkNotNull(snapshotId);
		this.postId = postId;
		this.refId = refId;
		this.annId = annId;
		java.util.Date now = new java.util.Date();
		this.time = new Timestamp(now.getTime());
	}
	
	public Post(
			final String author,
			final String title,
			final String post,
			final String snapshotId,
			final Timestamp time,
			
			final String postId,
			final String refId,
			final String annId
			) {
		this.authorId = checkNotNull(author);
		this.title = checkNotNull(title);
		this.post = checkNotNull(post);
		this.snapId = checkNotNull(snapshotId);
		this.postId = postId;
		this.refId = refId;
		this.annId = annId;
		this.time = time;
	}
	
	@Override
	public String toString() {
		return post;
	}
	
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	
	public String getSnapshotId() {
		return snapId;
	}
	public void setSnapshotId(String snapId) {
		this.snapId = snapId;
	}
	
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	
	public String getAuthorId() {
		return authorId;
	}
	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}
	
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}

	public String getAnnotationId() {
		return annId;
	}

	public void setAnnotationId(String annId) {
		this.annId = annId;
	}

	
	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonIgnore
	public List<Post> getReplies() {
		return replies;
	}

	public void setReplies(List<Post> replies) {
		this.replies = replies;
		for (Post reply: replies)
			reply.setRefId(this.postId);
	}
	
	public void addReply(Post reply) {
		replies.add(0, reply);
		reply.setRefId(this.postId);
	}

	public Long getDiscId() {
		return discId;
	}

	public void setDiscId(Long discId) {
		this.discId = discId;
	}
}
