/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;

public class HabitatUserServiceFactory {
	static HabitatUserService uService = null;
	
	public static synchronized void setUserService(HabitatUserService svc) {
		uService = svc;
	}
	
	public static synchronized HabitatUserService getUserService(IUserService userService, IDataSnapshotServer dsServer) {
		if (uService == null)
			uService = new HabitatUserService(userService, dsServer);
		return uService;
	}
	
	public static synchronized HabitatUserService getUserService() {
		if (uService == null)
			uService = new HabitatUserService(UserServiceFactory.getUserService(), 
					DataSnapshotServerFactory.getDataSnapshotServer());
		
		return uService;
	}
}
