/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.shared;

import java.util.List;

/**
 * Abstraction for a weight vector associated with an edge or a node
 * 
 * @author Zack Ives
 *
 */
public interface IWeightVector {
	/**
	 * How many weights in the vector
	 * 
	 * @return
	 */
	public int getNumWeights();
	
	public double getWeight(int inx);
	
	public double getWeight(String feature);
	
	public void addWeight(String name, Double weight);

	public List<String> getFeatureNames();

	public IWeightVector concat(IWeightVector second);

	public long getId();
}
