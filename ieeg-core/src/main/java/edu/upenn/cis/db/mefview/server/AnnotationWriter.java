/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.dao.annotations.AnnotationChannelInfo;
import edu.upenn.cis.braintrust.datasnapshot.IAnnotationWriter;
import edu.upenn.cis.braintrust.shared.UserId;

public final class AnnotationWriter implements IAnnotationWriter {

	private final OutputStream outputStream;
	private boolean started = false;
	private boolean finished = false;

	public AnnotationWriter(OutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public void start() throws IOException {
		if (!started) {
			outputStream
					.write(
					"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
							.getBytes());
			outputStream.write("\n<annotations>".getBytes());
			started = true;
		}
	}

	@Override
	public void write(
			long id,
			String type,
			@Nullable String description,
			long startOffsetUsecs,
			long endOffsetUsecs,
			Date createTime,
			String layer,
			String annotator,
			UserId creatorId,
			@Nullable String color,
			Set<AnnotationChannelInfo> channelNameAndIds) throws IOException {
		start();
		StringBuilder sb = new StringBuilder();
		sb.append("\n  <annotation");
		sb.append("\n    id=\"").append(id).append("\"");
		sb.append("\n    type=\"").append(type).append("\"");
		if (description != null) {
			sb
					.append("\n    description=\"")
					.append(description)
					.append("\"");
		}
		sb
				.append("\n    startOffsetUsecs=\"")
				.append(startOffsetUsecs)
				.append("\"");
		sb
				.append("\n    endOffsetUsecs=\"")
				.append(endOffsetUsecs)
				.append("\"");
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);
		String createTimeAsISO = df.format(createTime);
		sb
				.append("\n    createTime=\"")
				.append(createTimeAsISO)
				.append("\"");
		sb
				.append("\n    layer=\"")
				.append(layer)
				.append("\"");
		sb
				.append("\n    annotator=\"")
				.append(annotator)
				.append("\"");
		sb
				.append("\n    creatorId=\"")
				.append(creatorId.getValue())
				.append("\"");
		if (color != null) {
			sb
					.append("\n    color=\"")
					.append(color)
					.append("\"");
		}
		sb.append(">");
		sb.append("\n    <channels>");
		for (AnnotationChannelInfo chNameAndId : channelNameAndIds) {
			sb
					.append("\n      <channel")
					.append("\n        fileName=\"")
					.append(chNameAndId.getClientSideFileName())
					.append("\"")
					.append("\n        id=\"")
					.append(chNameAndId.getId().getId())
					.append("\"")
					.append("\n        serverFileKey=\"")
					.append(chNameAndId.getFileKey())
					.append("\"/>");
		}
		sb.append("\n    </channels>");
		sb.append("\n  </annotation>");
		outputStream.write(sb.toString().getBytes());
	}

	@Override
	public void finish() throws IOException {
		if (started && !finished) {
			outputStream.write("\n</annotations>\n".getBytes());
		}
		if (!finished) {
			outputStream.close();
			finished = true;
		}
	}
}
