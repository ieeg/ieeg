/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

public class SeriesData implements Comparable<SeriesData>, IsSerializable {
	public String label;
	public String revId;
	public String studyId;
	public String friendly;
	
	public int compareTo(SeriesData o) {
		if (!(o instanceof SeriesData))
			throw new RuntimeException("Type mismatch");
		else
			return label.compareTo(((SeriesData)o).label);
	}

	public static final ProvidesKey<SeriesData> KEY_PROVIDER = new ProvidesKey<SeriesData>() {

		public Object getKey(SeriesData item) {
			return (item == null) ? null : item.revId;//.getFriendlyName();
		}
		
	};

	public static final TextColumn<SeriesData> studyColumn = new TextColumn<SeriesData> () {
		@Override
		public String getValue(SeriesData object) {
			return object.friendly;
		}
	};
	
	public static final TextColumn<SeriesData> seriesColumn = new TextColumn<SeriesData> () {
		@Override
		public String getValue(SeriesData object) {
			return object.label;
		}
	};
	
	public static final TextColumn<SeriesData> seriesID = new TextColumn<SeriesData> () {
		@Override
		public String getValue(SeriesData object) {
			return object.revId;
		}
	};
}
