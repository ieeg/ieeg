/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javassist.Modifier;

import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.datasnapshot.IHasPubId;
import edu.upenn.cis.braintrust.datasnapshot.IRecordingObjectProvider;
import edu.upenn.cis.braintrust.datasnapshot.RecordingObjectProviderFactory;
import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IPersistentObject;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.JobEntity;
import edu.upenn.cis.braintrust.model.JobHistory;
import edu.upenn.cis.braintrust.model.LayerEntity;
import edu.upenn.cis.braintrust.model.MontageEntity;
import edu.upenn.cis.braintrust.model.MontagedChAnnotationEntity;
import edu.upenn.cis.braintrust.model.PermissionEntity;
import edu.upenn.cis.braintrust.model.Recording;
import edu.upenn.cis.braintrust.model.RecordingObjectTaskEntity;
import edu.upenn.cis.braintrust.model.SessionEntity;
import edu.upenn.cis.braintrust.model.StimRegionEntity;
import edu.upenn.cis.braintrust.model.StimTypeEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.thirdparty.dao.IDAO;
import edu.upenn.cis.db.habitat.persistence.constructors.EntityConstructor;
import edu.upenn.cis.db.habitat.persistence.constructors.PersistentObjectConstructor;
import edu.upenn.cis.db.habitat.persistence.constructors.RelationshipConstructor;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceObjectNotFoundException;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceUnexpectedTypeException;
import edu.upenn.cis.db.habitat.persistence.object.DaoLongEntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.DaoStringEntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.DaoStringRelationshipPersistence;
import edu.upenn.cis.db.habitat.persistence.object.DaoUserEntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence;

/**
 * Central registry for new entity types. The file entity.properties should
 * include entity-type:persistence-manager-type pairs (qualified java classnames
 * for both).
 * 
 * TODO: extend to also consider mapstruct-mapped DTOs?
 * 
 * @author zives
 *
 */
public class PersistentObjectFactory {
	static PersistentObjectFactory theFactory = null;

	private final IRecordingObjectProvider recObjProvider = RecordingObjectProviderFactory
			.getRecordingObjectProvider();

	public static synchronized PersistentObjectFactory getFactory() {
		String path = Thread.currentThread().getContextClassLoader()
				.getResource("ieegview.properties").getPath();

		IvProps.init(path);

		if (theFactory == null)
			theFactory = new PersistentObjectFactory();

		return theFactory;
	}

	// Global registry for classes by their canonical names
	Map<String, Class<?>> registry = new HashMap<String, Class<?>>();
	
	// Global registry of persistence managers for classes
	Map<Class<?>, IPersistentObjectManager<?, ?>> persistence = new HashMap<Class<?>, IPersistentObjectManager<?, ?>>();
	
	// Global registry of constructors for classes
	Map<Class<?>, PersistentObjectConstructor<?, ?>> constructor = new HashMap<Class<?>, PersistentObjectConstructor<?, ?>>();
	
	IGraphServer graphStore = null;

	Properties props = new Properties();

	@VisibleForTesting
	PersistentObjectFactory() {
		load();
	}
	
	@SuppressWarnings("unchecked")
	private void load() {
		if (PersistentObjectFactory.class.getResourceAsStream("entities.properties") != null) {
			try {
				props.load(PersistentObjectFactory.class
						.getResourceAsStream("entities.properties"));

				// Read key-class, manager-class pairs
				for (Entry<Object, Object> keyValue : props.entrySet()) {
					try {
						Object persister = Class
								.forName((String) keyValue.getValue())
								.newInstance();

						// DAO objects need a special registration depending on their type
						// so we can wrap them
						if (persister instanceof IDAO) {
							Class<? extends IPersistentObject> tableType = 
									(Class<? extends IPersistentObject>)Class.forName((String) keyValue.getKey());
							
							if (IEntity.class.isAssignableFrom(tableType)) {
								registerEntityDao((String) keyValue.getKey(),
										(IDAO<? extends IEntity,?>) persister);
							} else if (IRelationship.class.isAssignableFrom(tableType)) {
								registerRelationshipDao(
										(String) keyValue.getKey(), (IDAO) persister);
							} else
								throw new PersistenceUnexpectedTypeException(
										"Unexpected key type in entities.properties!");
							
						// Else we just use a custom persistence manager
						} else {
							IPersistentObjectManager<?, ?> manager = (IPersistentObjectManager<?, ?>) (persister);
							Class<? extends IPersistentObject> tableType = 
									(Class<? extends IPersistentObject>)Class.forName((String) keyValue.getKey());
							
							if (IEntity.class.isAssignableFrom(tableType)) {
								EntityPersistence<? extends IEntity,? extends Serializable,?> ep = 
										(EntityPersistence)manager;
								
								if (!Modifier.isAbstract(tableType.getModifiers()))
									ep.setObjectEntity((IEntity)tableType.newInstance());
							} else if (IRelationship.class.isAssignableFrom(tableType)) {
								RelationshipPersistence<? extends IRelationship,?,? extends IEntity,? extends IEntity> 
								rp = (RelationshipPersistence)manager;
								
								if (!Modifier.isAbstract(tableType.getModifiers()))
									rp.setObjectRelationship((IRelationship)tableType.newInstance()); 

							} else
								throw new PersistenceUnexpectedTypeException(
										"Unexpected key type in entities.properties!");

							register((String) keyValue.getKey(), manager);
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						// Go on
					} catch (IllegalAccessException e) {
						e.printStackTrace();
						// Go on
					} catch (InstantiationException e) {
						System.err.println("Unable to instantiate " + keyValue.getValue());
						e.printStackTrace();
						// Go on
					} catch (PersistenceUnexpectedTypeException e) {
						System.err.println("Unknown type in entities.properties");
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Register a DAO associated with an IEntity class.  Creates
	 * a DaoEntityPersistence manager for it.
	 * 
	 * @param className IEntity class
	 * @param dao IDAO associated with that class
	 */
	@SuppressWarnings("unchecked")
	public <T extends IEntity, K extends Serializable> void registerEntityDao(String className,
			IDAO<T, K> dao) {
		Class<T> table;
		try {
			table = (Class<T>)Class.forName(className);

			if (registry.containsKey(className)
					|| persistence.containsKey(table))
				throw new RuntimeException(
						"Attempting to register a non-unique class "
								+ className);

			registry.put(className, table);
			
			EntityConstructor<T, String, ?> construct = null;
			EntityPersistence<T, K, ?> persist;

			// If it's an abstract class, we don't have a constructor
			// and in fact we also won't have a key fetcher
			if (Modifier.isAbstract(table.getModifiers())) {
				if (IHasPubId.class.isAssignableFrom(table)) {
					persist = new DaoStringEntityPersistence(
							dao, null, null);
				} else if (UserEntity.class.isAssignableFrom(table)) {
					persist = new DaoUserEntityPersistence(
							(IUserDAO)dao, null, null);
				} else {
					persist = new DaoLongEntityPersistence(
							dao, null, null);
	
				}
			
			// If it's a concrete class, we want to create a dummy
			// object to get its constructor and key fetcher
			} else {
				T obj = table.newInstance();
				
				construct = (EntityConstructor<T, String, ?>) (obj).tableConstructor();

				// PubId classes have a string label / ID
				if (IHasPubId.class.isAssignableFrom(table)) {
					persist = new DaoStringEntityPersistence(
							dao, construct.getKeyFetcher(),
							construct.getObjectCreator());
					
				// UserEntity is weird and has a UserId
				} else if (UserEntity.class.isAssignableFrom(table)) {
					persist = new DaoUserEntityPersistence(
							(IUserDAO)dao, construct.getKeyFetcher(),
							construct.getObjectCreator());
					
				// Many things just have a numeric (long) key
				} else {
					persist = new DaoLongEntityPersistence(
							dao, construct.getKeyFetcher(),
							construct.getObjectCreator());
	
				}
				constructor.put(table, (obj).tableConstructor());
			}
			
			persistence.put(table, persist);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Register a DAO associated with an IRelationship class. Creates
	 * a DaoRelationshipPersistence manager for it.
	 * 
	 * @param className IRelationship class
	 * @param dao IDAO associated with that class
	 */
	@SuppressWarnings("unchecked")
	public <T extends IRelationship, K extends Serializable> 
		void registerRelationshipDao(
				String className,
				IDAO<T, K> dao) {
		Class<T> table;
		try {
			table = (Class<T>)Class.forName(className);

			if (registry.containsKey(className)
					|| persistence.containsKey(table))
				throw new RuntimeException(
						"Attempting to register a non-unique class "
								+ className);

			registry.put(className, table);
			
			RelationshipPersistence<T, K, ? extends IEntity, ? extends IEntity> persist;
			
			// If it's an abstract class, we don't have a constructor
			// and in fact we also won't have a key fetcher
			if (!Modifier.isAbstract(table.getModifiers())) {
				RelationshipConstructor<T, K, ? extends IEntity, ? extends IEntity> 
					construct;
				
				construct = (RelationshipConstructor<T, K, ? extends IEntity, ? extends IEntity>) 
						table.newInstance().tableConstructor(); 
						
				constructor.put(table,
						((IPersistentObject) table.newInstance()).tableConstructor());
				
				persist = new DaoStringRelationshipPersistence(
						dao, construct.getKeyFetcher(), 
						construct.getCreateObject());
				
			// If it's a concrete class, we want to create a dummy
			// object to get its constructor and key fetcher
			} else
				persist = new DaoStringRelationshipPersistence(
						dao, null, null);

			persistence.put(table, persist);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Register a class with its own persistence manager
	 * 
	 * @param className
	 * @param manager
	 */
	public void register(String className, IPersistentObjectManager<?, ?> manager) {
		Class<?> table;
		try {
			table = Class.forName(className);

			if (registry.containsKey(className)
					|| persistence.containsKey(table))
				throw new RuntimeException(
						"Attempting to register a non-unique class "
								+ className);

			registry.put(className, table);
			persistence.put(table, manager);
			constructor.put(table,
					((IPersistentObject) table.newInstance()).tableConstructor());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Set<String> getRegisteredEntities() {
		return registry.keySet();
	}

	public IPersistentObjectManager<?, ?> getPersistenceManager(String name) {
		if (registry.containsKey(name))
			return persistence.get(registry.get(name));
		else
			return null;
	}

	public IPersistentObjectManager<?, ?> getPersistenceManager(Class<?> table) throws PersistenceObjectNotFoundException {
		if (!persistence.containsKey(table)) {
			throw new PersistenceObjectNotFoundException("Class " + table.getCanonicalName() + " wasn't registered");
		}
		return persistence.get(table);
	}

	public PersistentObjectConstructor<?, ?> getPersistentConstructor(
			String name) throws PersistenceObjectNotFoundException {
		if (registry.containsKey(name))
			return constructor.get(registry.get(name));
		else
			throw new PersistenceObjectNotFoundException("Class " + name + " wasn't registered");
	}

	public PersistentObjectConstructor<?, ?> getPersistentConstructor(
			Class<?> table) throws PersistenceObjectNotFoundException {
		if (!constructor.containsKey(table)) {
			throw new PersistenceObjectNotFoundException("Class " + table.getCanonicalName() + " wasn't registered");
		}
		return constructor.get(table);
	}

	public Object getBlankEntity(String type) throws InstantiationException,
			IllegalAccessException {
		if (registry.get(type) != null) {
			return registry.get(type).newInstance();
		}
		return null;
	}

	public ExtAclEntity getEmptyWorldAcl() {
		return new ExtAclEntity(new HashSet<PermissionEntity>());
	}
	
	public void registerBuiltIn() {
		// ImageEntity -- no ACL!!
		// ProjectEntity -- no ACL!!

		// 

		Class<?>[] list = { 
				JobEntity.class, 
				JobHistory.class, 
				Recording.class, 
				SessionEntity.class, 
				StimRegionEntity.class, 
				StimTypeEntity.class,
				MontageEntity.class, 
				LayerEntity.class,
				MontagedChAnnotationEntity.class,
				RecordingObjectTaskEntity.class,
				ControlFileEntity.class };
		// IAnalyzedDataSnapshotDAO getAnalyzedDataSnapshotDAO();
		// IAnimalDAO getAnimalDAO(Session session);
		// IDatasetDAO getDatasetDAO();
		// IDataSnapshotDAO getDataSnapshotDAO(Session session);
		// IDataUsageDAO getDataUsageDAO(Session session);
		// IDrugDAO getDrugDAO();
		// IDrugAdminDAO getDrugAdminDAO();
		// IEegStudyDAO getEegStudyDAO();
		// IExperimentDAO getExperimentDAO();
		// IImageDAO getImageDAO();
		// IJobDAO getJobDAO(Session sess);
		// IJobHistoryDAO getJobHistoryDAO(Session sess);
		// IOrganizationDAO getOrganizationDAO();
		// IPatientDAO getPatientDAO();
		// IProjectDAO getProjectDAO(Session session);
		// IRecordingDAO getRecordingDAO(Session session);
		// ISessionDAO getSessionDAO(Session session);
		// ISpeciesDAO getSpeciesDAO();
		// IStimRegionDAO getStimRegionDAO();
		// IStimTypeDAO getStimTypeDAO();
		// ITimeSeriesDAO getTimeSeriesDAO(Session session);
		// ITsAnnotationDAO getTsAnnotationDAO(Session session, Configuration
		// config);
		// IUserDAO getUserDAO(Session session);
		// IMontageDAO getMontageDAO();
		// ILayerDAO getLayerDAO(Session session);
		// IMontagedChAnnotationDAO getMontagedChAnnotationDAO(Session session);
		// IRecordingObjectTaskDAO getRecordingObjectTaskDAO(Session session);
		// IRecordingObjectDAO getRecordingObjectDAO(Session session);
		// IControlFileDAO getControlFileDAO(Session session);
	}
	
	public IGraphServer getGraphStore() {
		return graphStore;
	}

	public void setGraphStore(IGraphServer graphStore) {
		this.graphStore = graphStore;
	}
}
