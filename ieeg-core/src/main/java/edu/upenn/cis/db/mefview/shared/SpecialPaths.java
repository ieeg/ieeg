package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.core.client.GWT;

public class SpecialPaths {
	public final static String FolderType = "Folder";
	public final static String UserFolderType = "Users";
			
	public final static String SearchResults = "Search Results";
	public final static String PSearchResults = "/" + SearchResults;
	
	public final static String Groups = "groups";
	public final static String PGroups = "/" + Groups;

	public final static String Shared = "shared";
	public final static String PShared = "/" + Shared;
	
	public final static String Home = "My Data";
	public final static String VirtualLab = "(Back to My Data)";
	public final static String PVirtualLab = "/";

	public final static String Projects = "Team Projects";
	public final static String PProjects = "/" + Projects;
	
	public final static String Data = "Accessible Data";
	public final static String PData = "/" + Data;

	public final static String Public = "Public Data";
	public final static String PPublic = "/" + Public;

	public final static String Recs = "Candidate colleagues";
	public final static String PRecs = "/" + Recs;
	
	public final static String Colleagues = "Colleagues";
	public final static String PColleagues = "/" + Colleagues;

	public final static String Incoming = "Shared with Me";
	public final static String PIncoming = "/" + Incoming;

	public final static String Dropbox = "Dropbox";
	public final static String PDropbox = "/" + Dropbox;
	
	public final static String Inbox = "Inbox for import";
	public final static String PInbox = "/" + Inbox;

	public static boolean isSearchResult(String path, String userHome) {
		return path.equals(PSearchResults) ||
				path.equals(userHome + PSearchResults);
	}
	
	public static String getSearchResultPath(String userHome) {
		return userHome + PSearchResults;
	}
	
	public static String getDropboxPath(String userHome) {
		return userHome + PDropbox;
	}

	public static boolean isHome(String path, String userHome) {
		return path.equals("/") || path.equals(userHome);
	}
	
	public static String getHomePath(String userHome) {
		return userHome;
	}
	
	public static boolean isProjects(String path, String userHome) {
		return //path.startsWith(PProjects) || 
				path.startsWith(userHome + PProjects);
	}

	public static boolean isGroups(String path, String userHome) {
		return //path.equals(PGroups) || 
				path.equals(userHome + PGroups);
	}

	public static boolean isAGroup(String path, String userHome) {
		return path.startsWith(userHome + "/groups/");
	}
	
	public static boolean isShared(String path, String userHome) {
		return //path.equals(PShared) || 
				path.equals(userHome + PShared);
	}

	public static boolean isASharedItem(String path, String userHome) {
		return path.startsWith(userHome + "/shared/");
	}
	
	public static boolean isData(String path, String userHome) {
		return //path.equals(PData) || 
				path.equals(userHome + PData);
	}
	public static boolean isRecs(String path, String userHome) {
		return //path.equals(PRecs) || 
				path.equals(userHome + PRecs);
	}
	public static boolean isColleagues(String path, String userHome) {
		return //path.equals(PColleagues) || 
				path.equals(userHome + PColleagues);
	}

	public static boolean isIncoming(String path, String userHome) {
		return //path.equals(PIncoming) ||
				path.equals(userHome + PIncoming);
	}
	
	public static boolean isDropbox(String path, String userHome) {
		return path.equals(PDropbox) ||
				path.equals(userHome + PDropbox);
	}
	
	public static String getIncoming(String userHome) {
		return userHome + PIncoming;
	}
	
	public static boolean isSpecial(String path, String userHome) {
		boolean ret = (isSearchResult(path, userHome)) ||
				(isHome(path, userHome)) ||
				(isProjects(path, userHome)) ||
				(isData(path, userHome)) ||
				(isIncoming(path, userHome)) ||
				(isRecs(path, userHome)) ||
				(isColleagues(path, userHome) ||
				(isDropbox(path, userHome)));
		
//		GWT.log("isSpecial " + path + " vs " + userHome + ": " + ret);
		
		return ret;
	}

	public static boolean isPublic(String path, String userHome) {
		return //path.equals(PPublic) ||
				path.equals(userHome + PPublic);
	}
}
