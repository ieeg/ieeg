/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SnapshotAnnotationInfo implements IsSerializable {
	public static final int MASK_SIZE = 3172;
	
	String snapshotId;
	double snapshotStartIndex;
	double snapshotEndIndex;
	
	byte[] annotatedMask;
	
	DisplayConfiguration defaultSnapshotFilters;
	boolean filtersAreModifiable = true;
	
	double defaultPeriod;
	boolean periodIsModifiable = true;
	
	double windowStart = 0;
	double windowEnd = 0;

	List<Annotation> knownAnnotations = new ArrayList<Annotation>();
	
	public static class IllegalSnapshotAnnotationMerge extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public IllegalSnapshotAnnotationMerge(String str) {
			super(str);
		}
	}

	public SnapshotAnnotationInfo() {
		snapshotId = "";
		snapshotStartIndex = 0;
		snapshotEndIndex = 0;
		annotatedMask = new byte[MASK_SIZE];
	}
	
	public SnapshotAnnotationInfo(String snapshotId, double snapshotStartIndex,
			double snapshotEndIndex, byte[] annotatedMask,
			Collection<Annotation> knownAnnotations) {
		super();
		this.snapshotId = snapshotId;
		this.snapshotStartIndex = snapshotStartIndex;
		this.snapshotEndIndex = snapshotEndIndex;
		this.annotatedMask = annotatedMask;
		this.knownAnnotations.addAll(knownAnnotations);
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public double getSnapshotStartIndex() {
		return snapshotStartIndex;
	}

	public void setSnapshotStartIndex(double snapshotStartIndex) {
		this.snapshotStartIndex = snapshotStartIndex;
	}

	public double getSnapshotEndIndex() {
		return snapshotEndIndex;
	}

	public void setSnapshotEndIndex(double snapshotEndIndex) {
		this.snapshotEndIndex = snapshotEndIndex;
	}

	public byte[] getAnnotatedMask() {
		return annotatedMask;
	}

	public void setAnnotatedMask(byte[] annotatedMask) {
		this.annotatedMask = annotatedMask;
	}

	public List<Annotation> getKnownAnnotations() {
		return knownAnnotations;
	}

	public void setKnownAnnotations(Collection<Annotation> knownAnnotations) {
		this.knownAnnotations.clear();
		this.knownAnnotations.addAll(knownAnnotations);
	}
	
	public void addAnnotations(Collection<Annotation> ann) {
		knownAnnotations.addAll(ann);
	}

	
	public DisplayConfiguration getDefaultSnapshotFilters() {
		return defaultSnapshotFilters;
	}

	public void setDefaultSnapshotFilters(DisplayConfiguration defaultSnapshotFilters) {
		this.defaultSnapshotFilters = defaultSnapshotFilters;
	}

	public boolean isFiltersAreModifiable() {
		return filtersAreModifiable;
	}

	public void setFiltersAreModifiable(boolean filtersAreModifiable) {
		this.filtersAreModifiable = filtersAreModifiable;
	}

	public double getDefaultPeriod() {
		return defaultPeriod;
	}

	public void setDefaultPeriod(double defaultPeriod) {
		this.defaultPeriod = defaultPeriod;
	}

	public boolean isPeriodIsModifiable() {
		return periodIsModifiable;
	}

	public void setPeriodIsModifiable(boolean periodIsModifiable) {
		this.periodIsModifiable = periodIsModifiable;
	}


	public void mergeWith(SnapshotAnnotationInfo two) throws IllegalSnapshotAnnotationMerge {
		if (!snapshotId.equals(two.snapshotId))
			throw new IllegalSnapshotAnnotationMerge("Cannot merge annotations from two snapshots");
		
		for (int i = 0; i < annotatedMask.length; i++)
			annotatedMask[i] |= two.annotatedMask[i];
	}
	
	public void markAnnotation(Annotation ann) {
		for (int inx = getIndexFor(ann.getStart()); inx < getIndexFor(ann.getEnd()); inx++)
			annotatedMask[inx / 8] |= (1 << (inx & 7));
	}
	
	private int getIndexFor(double time) {
		return (int)((snapshotEndIndex - snapshotStartIndex) / MASK_SIZE * time);
	}
	
	public void addAnnotation(Annotation ann) {
		knownAnnotations.add(ann);
		markAnnotation(ann);
	}

	public double getWindowStart() {
		return windowStart;
	}

	public void setWindowStart(double windowStart) {
		this.windowStart = windowStart;
	}

	public double getWindowEnd() {
		return windowEnd;
	}

	public void setWindowEnd(double windowEnd) {
		this.windowEnd = windowEnd;
	}

}
