/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.shared;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IFeatureVector {
	public int getNumFeatures();
	
	public List<String> getFeatureNames();
	
	public Map<String, Boolean> getFeatures();
	
	public boolean getFeaturePresent(int inx);
	
	public void setFeaturePresent(int inx);

	public void clearFeaturePresent(int inx);

	public boolean getFeatureNamePresent(String name);
	
	public void setFeatureNamePresent(String name);

	public void clearFeatureNamePresent(String name);

	public String getSignature();
	
	public Set<String> getFeaturesPresent();
	
	public IFeatureVector concat(IFeatureVector second);

	void addFeature(String feature);
	
	void addFeature(String feature, boolean set);

	public double getCost(IWeightVector weights);
	
	public long getId();
	
	public void addAll(IFeatureVector features2);
}
