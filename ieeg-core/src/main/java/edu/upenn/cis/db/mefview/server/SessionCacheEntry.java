/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.util.List;

import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;

public class SessionCacheEntry {

	// private Logger logger = LoggerFactory.getLogger(SessionCacheEntry.class);

	public int getBufferSizesBytes() {
		int sizeBytes = 0;
		if (channelInfo != null) {
			for (PerChannel chInfo : channelInfo) {
				if (chInfo.filterBuffer != null) {
					sizeBytes += chInfo.filterBuffer.length * 4;
				}
				if (chInfo.processor != null) {
					sizeBytes += chInfo.processor.getWorkBufferSize() * 4;
				}
			}
		}
		return sizeBytes;
	}

	public static class PerChannel {
		DisplayConfiguration lastFilter;
		FilterSpec theFilter;
		boolean isValidated = false;
		PostProcessor processor;
		int[] filterBuffer;

		public PerChannel(DisplayConfiguration filter, double samplingRate,
				double duration) {
			lastFilter = filter;
			isValidated = false;
			ensureCapacity((int) (duration / (1.E6 / samplingRate)) + 1);
		}

		public boolean isMatch(DisplayConfiguration filter) {
			return lastFilter.equals(filter);
		}

		public boolean update(DisplayConfiguration filter, double samplingRate,
				double duration) {
			if (!isMatch(filter)) {
				lastFilter = filter;
				theFilter = null;
				isValidated = false;
				return testCapacity((int) (duration / (1.E6 / samplingRate)) + 1);
				// return true;
			} else {
				return testCapacity((int) (duration / (1.E6 / samplingRate)) + 1);
			}
		}

		public boolean testCapacity(int cap) {
			if (filterBuffer == null || filterBuffer.length < cap) {
				// filterBuffer = new int[cap];
				return true;
			} else
				return false;
		}

		public boolean ensureCapacity(int cap) {
			if (filterBuffer == null || filterBuffer.length < cap) {
				// filterBuffer = new int[cap];
				return true;
			} else
				return false;
		}
	}

	// long startTime;
	double duration;
	double samplingRate;

	String[] channelIDs;
	PerChannel[] channelInfo;
	List<ChannelSpecifier> tracefileIds;

	public SessionCacheEntry(List<String> channelIDs,
			List<ChannelSpecifier> channelSpecifiers,
			List<DisplayConfiguration> filters, double samplingRate, double duration) {
		set(channelIDs, channelSpecifiers, filters, samplingRate, duration);
	}

	public SessionCacheEntry(String[] channelIDs,
			List<ChannelSpecifier> channelSpecifiers,
			List<DisplayConfiguration> filters, double samplingRate, double duration) {
		set(channelIDs, channelSpecifiers, filters, samplingRate, duration);
	}

	public void set(List<String> channelIDs,
			List<ChannelSpecifier> channelSpecifiers,
			List<DisplayConfiguration> filters, double samplingRate, double duration) {
		this.channelIDs = new String[channelIDs.size()];
		channelInfo = new PerChannel[channelIDs.size()];
		tracefileIds = channelSpecifiers;

		for (int i = 0; i < channelIDs.size(); i++) {
			this.channelIDs[i] = channelIDs.get(i);
			channelInfo[i] = new PerChannel(filters.get(i), samplingRate,
					duration);
		}
		List<FilterSpec> fList = TraceServer.getFiltersFrom(filters);
		for (int i = 0; i < filters.size(); i++) {
			channelInfo[i].theFilter = fList.get(i);
		}

		this.samplingRate = samplingRate;
		this.duration = duration;
	}

	public void set(String[] channelIDs,
			List<ChannelSpecifier> channelSpecifiers,
			List<DisplayConfiguration> filters, double samplingRate, double duration) {
		this.channelIDs = new String[channelIDs.length];
		channelInfo = new PerChannel[channelIDs.length];
		tracefileIds = channelSpecifiers;

		for (int i = 0; i < channelIDs.length; i++) {
			this.channelIDs[i] = channelIDs[i];
			channelInfo[i] = new PerChannel(filters.get(i), samplingRate,
					duration);
		}
		List<FilterSpec> fList = TraceServer.getFiltersFrom(filters);
		for (int i = 0; i < filters.size(); i++) {
			channelInfo[i].theFilter = fList.get(i);
		}

		this.samplingRate = samplingRate;
		this.duration = duration;
	}

	public SessionCacheEntry update(List<DisplayConfiguration> filters,
			double samplingRate, double duration) {
		boolean updated = false;
		for (int i = 0; i < filters.size(); i++) {
			updated |= channelInfo[i].update(filters.get(i), samplingRate,
					duration);
		}
		if (updated) {
			return new SessionCacheEntry(channelIDs, tracefileIds, filters,
					samplingRate, duration);
		} else
			return this;
	}
}
