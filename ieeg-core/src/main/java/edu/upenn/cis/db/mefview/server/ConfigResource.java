/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToInternalError;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.services.Config;
import edu.upenn.cis.db.mefview.services.IConfigResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;

public final class ConfigResource implements IConfigResource {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final User user;

	public ConfigResource(
			@Context HttpServletRequest req) {
		this.user = (User) req.getAttribute("user");
	}

	@Override
	public Config getConfig() {
		final String m = "getConfig(...)";
		long in = System.nanoTime();
		try {
			String leastOkayIeegClientVersion = IvProps
					.getLeastOkayIeegClientVersion();
			Config config = new Config(leastOkayIeegClientVersion);
			return config;
		} catch (RuntimeException e) {
			throw logAndConvertToInternalError(
					e,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} finally {
			logger.info(
					"{}: user {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							BtUtil.diffNowThenSeconds(in) });

		}

	}

}
