/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Sets.newHashSet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.hash.HashCode;
import com.google.common.io.BaseEncoding;
import com.google.common.util.concurrent.Striped;

import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IAnnotationWriter;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetIdAndVersion;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetConflictException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.webapp.IeegFilter;
import edu.upenn.cis.db.mefview.server.exceptions.ServerBusyException;
import edu.upenn.cis.db.mefview.server.exceptions.TooManyRequestsException;
import edu.upenn.cis.db.mefview.server.exceptions.UploadInProgressException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.DatasetFileInfos;
import edu.upenn.cis.db.mefview.services.FileInfo;
import edu.upenn.cis.db.mefview.services.IDatasetResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontages;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.RecordingObjects;

public final class DatasetResource implements IDatasetResource {

	private final IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private final TraceServer traceServer = TraceServerFactory.getTraceServer();
	private final Semaphore transferSemaphoresAllUsers =
			TransferSemaphoresFactory.getTransferSemaphoreAllUsers();
	private final Striped<Semaphore> transferSemaphoresPerUser =
			TransferSemaphoresFactory.getTransferSemaphoresPerUser();
	private final Striped<Semaphore> recObjUploadSemaphoresInProgress =
			RecObjectUploadSemaphoresFactory
					.getRecObjectUploadSemaphoresInProgress();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final HttpServletRequest httpServletRequest;
	private final User user;

	public DatasetResource(@Context HttpServletRequest httpServletRequest) {
		String m = "DatasetResource(...)";
		this.httpServletRequest = httpServletRequest;
		this.user = (User) httpServletRequest.getAttribute("user");
		logger.debug("{}: user: {}", m, user.getUsername());
	}

	@Override
	public Response createEditMontage(
			String datasetId,
			EEGMontage montage) {
		String m = "createEditMontage(...)";
		try {
			final boolean isNew = montage.getServerId() == null;
			dsServer.createEditEegMontage(
					user,
					montage,
					datasetId);

			// Probably a non-standard hack for the location. But we're not
			// implementing GETs for individual montages yet.
			final URI location = URI.create("datasets/montages");
			final ResponseBuilder responseBuilder = isNew
					? Response.created(location)
					: Response.noContent().location(location);
			Response response =
					responseBuilder
							.build();

			return response;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (EegMontageNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchMontageFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public DatasetFileInfos getDatasetFileInfos(
			String datasetId) {
		final String m = "getDatasetFileInfos(...)";
		try {
			Map<String, TimeSeriesDto> tsIdToTs =
					dsServer.getTimeSeries(
							user,
							datasetId);
			Set<FileInfo> fileInfos = newHashSet();
			for (TimeSeriesDto timeSeries : tsIdToTs.values()) {
				String id = timeSeries.getId();

				String key = timeSeries.getFileKey();
				String fileName = timeSeries.getLabel() + ".mef";
				SnapshotSpecifier snapshot = traceServer.getSnapshotSpecifier(
						user,
						datasetId);
				ChannelSpecifier channelSpecifier = traceServer
						.getChannelSpecifier(
								user,
								snapshot,
								id,
								0);
				String eTag = channelSpecifier.getCheckStr();
				long sizeBytes = traceServer.getSizeBytes(channelSpecifier);
				FileInfo fileInfo =
						new FileInfo(
								id,
								eTag,
								key,
								fileName,
								sizeBytes);
				fileInfos.add(fileInfo);
			}
			Set<String> datasetIdAsSet = Collections.singleton(datasetId);
			List<DataSnapshotSearchResult> searchResults =
					dsServer
							.getLatestSnapshots(
									user,
									datasetIdAsSet);
			DataSnapshotSearchResult searchResult = getOnlyElement(searchResults);
			if (searchResult == null) {
				throw new DataSnapshotNotFoundException(datasetId);
			}
			String datasetName = searchResult.getLabel();
			DatasetFileInfos datasetFileInfos =
					new DatasetFileInfos(
							datasetId,
							datasetName,
							fileInfos);
			return datasetFileInfos;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}

	}

	@Override
	public EEGMontages getMontages(
			String datasetId) {
		String m = "getMontages(...)";
		try {
			final List<EEGMontage> montageInfoDtos =
					dsServer.getMontages(
							user,
							datasetId);
			final EEGMontages montageInfosObject =
					new EEGMontages(montageInfoDtos);
			return montageInfosObject;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public RecordingObjects getRecordingObjects(
			String datasetId) {
		String m = "getRecordingObjects(...)";
		try {
			Set<RecordingObject> recordingObjects =
					dsServer.getRecordingObjects(
							user,
							new DataSnapshotId(datasetId));
			RecordingObjects recordingObjestsObject =
					new RecordingObjects(recordingObjects);
			return recordingObjestsObject;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public Response getTsAnnotations(
			String datasetIdStr) {
		String m = "getTsAnnotations(String)";
		try {
			getTransferLocks();

			final DataSnapshotId datasetId = new DataSnapshotId(datasetIdStr);
			StreamingOutput streamingOutput =
					new StreamingOutput() {

						@Override
						public void write(OutputStream output)
								throws IOException, WebApplicationException {
							final String m = "write(...)";
							IAnnotationWriter annotationWriter =
									new AnnotationWriter(output);
							try {
								dsServer.streamAnnotations(
										user,
										datasetId,
										annotationWriter);
							} catch (AuthorizationException e) {
								throw IeegWsExceptionUtil
										.logAndConvertToAuthzFailure(
												e,
												m,
												logger);
							} catch (DataSnapshotNotFoundException e) {
								throw IeegWsExceptionUtil
										.logAndConvertToNoSuchDsFailure(
												e,
												m,
												logger);
							} catch (RuntimeException t) {
								throw IeegWsExceptionUtil
										.logAndConvertToInternalError(
												t,
												m,
												logger);
							} catch (Error t) {
								throw IeegWsExceptionUtil
										.logAndConvertToInternalError(
												t,
												m,
												logger);
							}

						}
					};
			Response response = Response.ok(streamingOutput).build();
			return response;
		} catch (TooManyRequestsException e) {
			throw IeegWsExceptionUtil.logAndConvertToTooManyRequests(
					e,
					m,
					logger);
		} catch (ServerBusyException sbe) {
			throw IeegWsExceptionUtil.logAndConvertToServerBusyException(
					sbe,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	private void getTransferLocks()
			throws ServerBusyException,
			TooManyRequestsException {
		Semaphore userSemaphore = transferSemaphoresPerUser.get(user);

		try {

			if (!transferSemaphoresAllUsers.tryAcquire(
					15,
					TimeUnit.SECONDS)) {
				throw new ServerBusyException(
						"max total simultaneous tranfers exceeded, user "
								+ user.getUsername());
			}
			IeegFilter.setTransferAllUsersSemaphore(
					httpServletRequest,
					transferSemaphoresAllUsers);

			if (!userSemaphore.tryAcquire(
					15,
					TimeUnit.SECONDS)) {
				throw new TooManyRequestsException(
						"no more transfers avaliable for user "
								+ user.getUsername());
			}
			IeegFilter.setTransferPerUserSemaphore(
					httpServletRequest,
					userSemaphore);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new TooManyRequestsException(
					"could not aquire transfer lock for user "
							+ user.getUsername(), e);
		}
	}

	@Override
	public Response createRecordingObject(
			String datasetId,
			InputStream input,
			String filename,
			String md5Base64,
			boolean test) {
		final String m = "createObject(...)";
		try {
			final RecObjectUploadSemaphoresFactory.InProgressKey upload = new RecObjectUploadSemaphoresFactory.InProgressKey(
					datasetId,
					filename);
			final Semaphore inProgressSemaphore = recObjUploadSemaphoresInProgress
					.get(upload);
			if (!inProgressSemaphore.tryAcquire()) {
				throw new UploadInProgressException(upload.getName(),
						upload.getDatasetId());
			}
			IeegFilter.seUploadInProgressSemaphore(
					httpServletRequest,
					inProgressSemaphore);
			final String contentType = httpServletRequest
					.getHeader(HttpHeaders.CONTENT_TYPE);
			final String md5Hash = HashCode.fromBytes(
					BaseEncoding.base64().decode(md5Base64)).toString();
			final String contentLengthStr = httpServletRequest
					.getHeader(HttpHeaders.CONTENT_LENGTH);
			long contentLength = -1;
			if (!Strings.isNullOrEmpty(contentLengthStr)) {
				contentLength = Long.parseLong(contentLengthStr);
			}
			final RecordingObject recordingObject = new RecordingObject(
					filename,
					md5Hash,
					contentLength,
					user.getUsername(),
					contentType,
					datasetId);
			final RecordingObject createdObject = dsServer
					.createRecordingObject(
							user,
							recordingObject,
							input,
							test);
			// If this was a test just return no content since nothing was
			// created.
			final Response response = test ?
					Response.noContent().build() :
					Response.created(
							URI.create("objects/" + createdObject.getId()))
							.tag(createdObject.getETag())
							.entity(createdObject)
							.build();

			return response;
		} catch (UploadInProgressException e) {
			throw IeegWsExceptionUtil.logAndConvertToUploadInProgress(
					e,
					m,
					logger);
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (BadDigestException e) {
			throw IeegWsExceptionUtil.logAndConvertToBadDigest(
					e,
					m,
					logger);
		} catch (BadRecordingObjectNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToBadRecordingObjectName(
					e,
					m,
					logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(
					e,
					m,
					logger);
		} catch (RecordingNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchRecordingFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public void deleteDataset(
			String datasetId,
			@Nullable String eTag,
			@Nullable Boolean deleteEmptySubject) {
		final String m = "deleteDataset(...)";
		try {
			Integer eTagInt = null;
			boolean checkVersion = false;
			try {
				if (eTag != null) {
					eTagInt = Integer.valueOf(eTag);
					checkVersion = true;
				} else {
					eTagInt = -1;
					checkVersion = false;
				}
			} catch (NumberFormatException nbe) {
				eTagInt = -1;
				checkVersion = true;
			}
			final boolean deleteEmptySubjectPrim = deleteEmptySubject == null
					? false
					: deleteEmptySubject;
			dsServer.deleteDataset(
					user,
					new DatasetIdAndVersion(datasetId, eTagInt),
					checkVersion,
					deleteEmptySubjectPrim);
		} catch (AuthorizationException ae) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					ae,
					m,
					logger);
		} catch (FailedVersionMatchException e) {
			throw IeegWsExceptionUtil.logAndConvertToPreconditionFailed(
					e,
					m,
					logger);
		} catch (DatasetConflictException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToDatasetDeletionConflictFailure(
							e,
							m,
							logger);
		} catch (RuntimeException rte) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					rte,
					m,
					logger);
		} catch (Error e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);

		}

	}
}
