/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.parameters;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * The parameters to the line length detector
 */
@GwtCompatible(serializable = true)
public class LineLengthEchauzParameters implements JsonTyped, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int threshold;
	private double samplingFrequency;
	private double windowSize;
	private double stepSize;
	
	public int getThreshold() { return threshold; }
	public void setThreshold(int th) { threshold = th; }

	
	public double getSamplingFrequency() {
		return samplingFrequency;
	}
	public void setSamplingFrequency(double samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}
	public double getWindowSize() {
		return windowSize;
	}
	public void setWindowSize(double windowSize) {
		this.windowSize = windowSize;
	}
	public double getStepSize() {
		return stepSize;
	}
	public void setStepSize(double stepSize) {
		this.stepSize = stepSize;
	}
	public LineLengthEchauzParameters() {
		
	}
	
	public LineLengthEchauzParameters(int threshold, double samplingFrequency, double windowSize, double stepSize) {
		setThreshold(threshold);
		setSamplingFrequency(samplingFrequency);
		setWindowSize(windowSize);
		setStepSize(stepSize);
	}
}

