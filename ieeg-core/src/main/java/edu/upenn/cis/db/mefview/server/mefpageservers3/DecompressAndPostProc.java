/*
 * Copyright 2013 Sam Donnelly
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.server.mefpageservers3.DecompressAndPostProc.ChSpecAndTsDatas;
import edu.upenn.cis.db.mefview.server.mefpageservers3.DecompressAndPostProc.ForDecompressAndPostProc;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.thirdparty.RED;
import edu.upenn.cis.thirdparty.RED.PageAndBytesRead;

public final class DecompressAndPostProc
		implements
		Function<ForDecompressAndPostProc, ChSpecAndTsDatas> {

	public static final class ChSpecAndTsDatas {
		public final ChannelSpecifier chSpec;
		public final ArrayList<TimeSeriesData> timeSeriesDatas;
		public final PostProcessor postProc;

		public ChSpecAndTsDatas(
				ChannelSpecifier chSpec,
				ArrayList<TimeSeriesData> timeSeriesDatas,
				PostProcessor postProc) {
			this.chSpec = chSpec;
			this.timeSeriesDatas = timeSeriesDatas;
			this.postProc = postProc;
		}
	}

	public static final class ForDecompressAndPostProc {
		public final Map<MEFDataCacheKey, byte[]> mefData;
		public final MefHeader2 header;
		public final ChannelSpecifier chSpec;
		public final PostProcessor postProc;
		public final long startPage;
		public final long endPage;
		public final long chapterSize;
		public final long paddingMicros;
		public final long startTimeOffsetUutc;
		public final long endTimeOffsetUutc;
		public final long startPageByteOffset;

		public ForDecompressAndPostProc(
				Map<MEFDataCacheKey, byte[]> mefData,
				MefHeader2 header,
				ChannelSpecifier chSpec,
				PostProcessor postProc,
				long startPage,
				long endPage,
				long chapterSize,
				long paddingMicros,
				long startTimeOffsetUutc,
				long endTimeOffsetUutc,
				long startPageByteOffset) {
			this.mefData = mefData;
			this.header = header;
			this.chSpec = chSpec;
			this.postProc = postProc;
			this.startPage = startPage;
			this.endPage = endPage;
			this.chapterSize = chapterSize;
			this.paddingMicros = paddingMicros;
			this.startTimeOffsetUutc = startTimeOffsetUutc;
			this.endTimeOffsetUutc = endTimeOffsetUutc;
			this.startPageByteOffset = startPageByteOffset;
		}
	}

	private static Logger logger =
			LoggerFactory.getLogger(DecompressAndPostProc.class);
	private static Logger timeLogger =
			LoggerFactory.getLogger("time." + DecompressAndPostProc.class);

	private final Ehcache decompressedDataCache;

	public DecompressAndPostProc(Ehcache decompressedDataCache) {
		this.decompressedDataCache = decompressedDataCache;
	}

	@Override
	public ChSpecAndTsDatas apply(
			ForDecompressAndPostProc arguments) {
		try {
			String m = "apply(...)";
			logger.debug("{}: entering...", m);
			long in = System.nanoTime();

			RED red = new RED();
			double samplePeriod = 
					1E6 / arguments.header.getSamplingFrequency();

			List<TimeSeriesPage> pages = newArrayList();

			int pos = Ints.checkedCast(arguments.startPageByteOffset);
			long startChapter = MEFPageServerS3Util.getChapter(
					arguments.startPage,
					arguments.chapterSize);
			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			long decodeStart = System.nanoTime();
			for (long chapter = startChapter; chapter <= arguments.endPage; chapter += arguments.chapterSize) {
				MEFDataCacheKey fileKeyPageNo =
						new MEFDataCacheKey(
								arguments.chSpec.getHandle(),
								chapter,
								arguments.chSpec.getCheckStr());
				byte[] pagesData = arguments.mefData.get(fileKeyPageNo);

				if (chapter > startChapter) {
					pos = 0;
				}

				for (long pageNo = Math.max(arguments.startPage, chapter); pageNo < chapter
						+ arguments.chapterSize
						&& pageNo <= arguments.endPage; pageNo++) {

					PageAndBytesRead pageAndBytesRead = null;
					MEFDataCacheKey decompressedFileKeyPageNo =
							new MEFDataCacheKey(
									fileKeyPageNo.getFileKey(),
									pageNo,
									arguments.chSpec.getCheckStr());
					Element elem = decompressedDataCache
							.get(decompressedFileKeyPageNo);
					if (elem != null) {
						pageAndBytesRead =
								(PageAndBytesRead) elem.getObjectValue();
					}

					if (pageAndBytesRead == null) {
						pageAndBytesRead =
								red.decodePage(pagesData, pos, samplePeriod);
						decompressedDataCache
								.put(
								new Element(
										decompressedFileKeyPageNo,
										pageAndBytesRead));
					}
					TimeSeriesPage page = pageAndBytesRead.page;
					pos += pageAndBytesRead.noBytesRead;

					pages.add(page);
				}
				// free it up
				arguments.mefData.put(fileKeyPageNo, null);
			}

			double decompandSecs = BtUtil.diffNowThenSeconds(decodeStart);

			long startTimeRealUutc = arguments.header.getRecordingStartTime()
					+ arguments.startTimeOffsetUutc;
			long endTimeRealUutc = arguments.header.getRecordingStartTime()
					+ arguments.endTimeOffsetUutc;

			long postProcStart = System.nanoTime();

			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			ArrayList<TimeSeriesData> ret = arguments.postProc.process(
					startTimeRealUutc,
					endTimeRealUutc,
					arguments.header.getSamplingFrequency(),
					arguments.startTimeOffsetUutc,
					arguments.header.getVoltageConversionFactor(),
					pages.toArray(new TimeSeriesPage[pages.size()]),
					arguments.chSpec,
					false,
					arguments.paddingMicros,
					arguments.paddingMicros);
			double postProcSecs = BtUtil.diffNowThenSeconds(postProcStart);
			timeLogger.trace("{}: decomp {} postProc {} total {} seconds",
					new Object[] {
							m,
							decompandSecs,
							postProcSecs,
							BtUtil.diffNowThenSeconds(in) });
			return new ChSpecAndTsDatas(arguments.chSpec, ret,
					arguments.postProc);
		} catch (Exception e) {
			throw propagate(e);
		}
	}
}
