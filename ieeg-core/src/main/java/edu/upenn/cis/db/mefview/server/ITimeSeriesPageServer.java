/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.ChSpecTimeSeriesDto;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFDataAndPageNos;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public interface ITimeSeriesPageServer {

	ChannelSpecifier getChannelSpecifier(
			User user,
			SnapshotSpecifier dsSpec,
			String tsId,
			double samplingPeriod);

	GeneralMetadata getMetadata(ChannelSpecifier channelSpec);
	
	String getAcquisitionSystem(ChannelSpecifier channelSpec);

	String getChannelComments(ChannelSpecifier channelSpec);

	String getChannelName(ChannelSpecifier channelSpec);
	
	String getDataVersion(ChannelSpecifier chSpec);

	long getEndUutc(ChannelSpecifier channelSpec);

	String getInstitution(ChannelSpecifier channelSpec);

	int getMaxSampleValue(ChannelSpecifier channelSpec);

	int getMinSampleValue(ChannelSpecifier channelSpec);

	long getNumSamples(ChannelSpecifier channelSpec);

	double getSampleRate(ChannelSpecifier channelSpec);

	long getStartUutc(ChannelSpecifier channelSpec);

	String getSubjectId(ChannelSpecifier channelSpec);

	double getVoltageConversionFactor(ChannelSpecifier channelSpec);

	List<List<TimeSeriesData>>
			requestMultiChannelRead(
					List<? extends ChannelSpecifier> paths,
					long startTime,
					long endTime,
					PostProcessor[] postProc, User user, 
					@Nullable SessionToken sessionToken)
					throws InterruptedException, ExecutionException, ServerTimeoutException, IOException;

	void shutdown();

	SnapshotSpecifier getSnapshotSpecifier(User user, String revId)
			throws AuthorizationException;

	List<TimeSeriesData> requestRead(ChannelSpecifier path,
			long startTime, long endTime, PostProcessor postProc, User user, 
			SessionToken sessionToken)
			throws InterruptedException, ExecutionException, IOException, ServerTimeoutException;

	List<String> getPageServerState();

	void clearAll();

	List<TraceInfo> getTraceInfos(
			List<ChSpecTimeSeriesDto> cstsList) throws IOException,
			InterruptedException, ExecutionException;

	List<Double> getSampleRates(List<? extends ChannelSpecifier> chSpecs);

	Map<String, ChannelSpecifier> getChannelSpecifiers(
			User user,
			SnapshotSpecifier dsSpec,
			Set<String> tsIds,
			double samplingPeriod) throws AuthorizationException;

	Map<String, ChannelSpecifier> getChannelSpecifiersFromSegments(
			User user,
			SnapshotSpecifier dsSpec,
			Set<INamedTimeSegment> tsIds,
			double samplingPeriod) throws AuthorizationException;

	List<MEFDataAndPageNos> requestMultiChannelReadRed(
			List<? extends ChannelSpecifier> chSpecs, 
			long startTimeOffset,
			long endTimeOffset, 
			User user) throws StaleMEFException, ServerTimeoutException;

	InputStream getData(
			String key, 
			@Nullable String eTag, 
			@Nullable Long startByteOffset,
			@Nullable Long endByteOffset);

	long getSizeBytes(ChannelSpecifier channelSpecifier);


}
