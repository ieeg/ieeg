/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible
public class ImageInfo implements Serializable, Comparable<ImageInfo> {

	private static final long serialVersionUID = 1L;

	private String url;
	private String name;
	private String id;

	private String type;

	private int posX;

	private int posY;

	//For GWT
	@SuppressWarnings("unused")
	private ImageInfo() {}

	public ImageInfo(String url,
			String name,
			String type,
			int posX,
			int posY,
			@Nullable String imageId) {
		this.url = checkNotNull(url);
		this.name = checkNotNull(name);
		this.type = checkNotNull(type);
		this.posX = posX;
		this.posY = posY;
		this.id = imageId;
	}

	public String getUrl() {
		return url;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int compareTo(ImageInfo o) {
		if (name == null && o.name != null)
			return -1;
		else if (name == null && o.name == null)
			return 0;
		else if (o.name == null)
			return 1;
		else
			return name.compareTo(o.name);
	}

}
