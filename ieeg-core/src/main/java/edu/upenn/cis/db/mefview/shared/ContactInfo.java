package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

/**
 * The contact person
 * 
 * @author zives
 *
 */
@GwtCompatible(serializable = true)
public class ContactInfo implements Serializable {
	String firstName;
	String lastName;
	String title;
	String department;
	String institution;
	PostalAddress address;
	
	public ContactInfo(
			String firstName, 
			String lastName, 
			String department,
			String institution, 
			String street,
			String city,
			String state,
			String zip,
			String country) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.department = department;
		address = new PostalAddress(street, city, state, zip, country);
		
	}

}
