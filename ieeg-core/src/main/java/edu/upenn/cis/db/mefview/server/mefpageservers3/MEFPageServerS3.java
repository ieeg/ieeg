/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.braintrust.BtUtil.getBoolean;
import static edu.upenn.cis.braintrust.BtUtil.getDouble;
import static edu.upenn.cis.braintrust.BtUtil.getInt;
import static java.util.Collections.emptyList;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Range;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Striped;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFSnapshotSpecifier;
import edu.upenn.cis.db.mefview.server.DataMarshalling;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.DecompressAndPostProc.ChSpecAndTsDatas;
import edu.upenn.cis.db.mefview.server.mefpageservers3.DecompressAndPostProc.ForDecompressAndPostProc;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.eeg.mef.MefHeader2;

@ThreadSafe
public final class MEFPageServerS3
		extends TimeSeriesPageServer<TimeSeriesPage> {

	private static final class ChSpecAndData {
		public final ChannelSpecifier chSpec;

		public final MEFDataAndPageNos data;

		public ChSpecAndData(ChannelSpecifier chSpec, MEFDataAndPageNos data) {
			this.chSpec = chSpec;
			this.data = data;
		}
	}

	private final class MEFHeaderLoaderS3 implements
			Callable<Object[]> {
		private final ChannelSpecifier chSpec;

		public MEFHeaderLoaderS3(ChannelSpecifier chSpec) {
			this.chSpec = chSpec;
		}

		@Override
		public Object[] call() throws Exception {
			return new Object[] { chSpec, getHeader(chSpec) };
		}
	}

	private final class PageLoaderS3 implements
			Callable<ForDecompressAndPostProc> {

		private final ChannelSpecifier chSpec;
		private final long startTimeOffset;
		private final long endTimeOffset;
		private final PostProcessor postProc;

		public PageLoaderS3(
				ChannelSpecifier chSpec,
				long startTimeOffset,
				long endTimeOffset,
				PostProcessor postProc) {
			checkArgument(startTimeOffset <= endTimeOffset);
			this.chSpec = chSpec;
			this.startTimeOffset = startTimeOffset;
			this.endTimeOffset = endTimeOffset;
			this.postProc = postProc;
		}

		@Override
		public ForDecompressAndPostProc call() {
			return requestReadHelper(
					chSpec,
					startTimeOffset,
					endTimeOffset,
					postProc);
		}
	}

	private static Logger logger =
			LoggerFactory.getLogger(MEFPageServerS3.class);

	private static Logger timeLogger =
			LoggerFactory.getLogger("time." + MEFPageServerS3.class.getName());

	private static long getChapter(long pageNo, long chapterPageCount) {
		return (pageNo / chapterPageCount) * chapterPageCount;
	}

	private final AmazonS3 s3;
	private final Ehcache headerCache;
	private final Ehcache dataCache;
	private final Ehcache decompressedDataCache;
	private final Ehcache postProcDataCache;
	private final IDataSnapshotServer dsServer;
	private final String dataBucket;
	private final ListeningExecutorService remoteFileThreadPool;
	private final ListeningExecutorService cpuThreadPool;
	private final boolean downsampled;
	private final double downsampledRate;
	private final int minDataReqBytes;
	private final Cache<String, MEFChannelSpecifier> tsIdsPlusRatesToChSpecs =
			CacheBuilder
					.newBuilder()
					.maximumSize(4000)
					.expireAfterWrite(1, TimeUnit.DAYS)
					.build();
	private final Cache<SnapshotSpecifier, Boolean> dssToIsDownsampled =
			CacheBuilder
					.newBuilder()
					.maximumSize(4000)
					.build();

	private final Striped<Lock> sessionLocks;
	private final Striped<Semaphore> userSemaphoresRawRed;
	private final Semaphore allUsersSemaphore;
	private final int timeoutSecs;
	private final MEFIndexService idxService;
	private final long maxReqHzChsSecs;
	private final long maxRedReqHzChsSecs;

	public MEFPageServerS3() {
		this(
				AwsUtil.getS3(),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.header"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.data"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.decompressedDataCache"),
				CacheManager.getCacheManager("ieeg")
						.getEhcache("MEFPageServerS3.postProcDataCache"),
				DataSnapshotServerFactory.getDataSnapshotServer(),
				IvProps.getDataBucket(),
				getInt(
						IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ,
						IvProps.MEF_PAGE_SERVER_S3_THRD_POOL_SZ_DFLT),
				IvProps.getMefPageServerS3CpuThrdPoolSz(),
				getBoolean(IvProps.getIvProps(), IvProps.DOWNSAMPLED, false),
				getDouble(IvProps.getIvProps(), IvProps.DOWNSAMPLED_RATE_HZ,
						100.0),
				1024 * getInt(IvProps.getIvProps(),
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB,
						IvProps.MEF_PAGE_SERVER_S3_MIN_DATA_REQ_KB_DFLT),
				IvProps.getPaddingSamples(),
				new MEFIndexService(),
				IvProps.getMEFPageServerS3MaxRequestsPerUserRawRed(),
				IvProps.getMEFPageServerS3MaxRequestsTotal(),
				IvProps.getMEFPageServerS3TimeoutSecs(),
				IvProps.getMEFPageServerS3MaxReqHzChsSecs(),
				IvProps.getMEFPageServerS3MaxRedReqHzChsSecs());
	}

	public MEFPageServerS3(
			AmazonS3 s3,
			Ehcache headerCache,
			Ehcache dataCache,
			Ehcache decompressedDataCache,
			Ehcache postProcDataCache,
			IDataSnapshotServer dsServer,
			String dataBucket,
			int filePoolSize,
			int cpuPoolSize,
			boolean downsampled,
			double downsampledRate,
			int minDataReqBytes,
			int mefPadding,
			MEFIndexService indexService,
			int maxUserRequestsRawRed,
			int maxUsers,
			int timeoutSecs,
			long maxReqHzChsSecs,
			long maxRedReqHzChsSecs) {
		super(mefPadding);
		final String M = "MEFPageServerS3(...)";
		this.s3 = s3;
		this.headerCache = headerCache;
		this.dataCache = dataCache;
		this.decompressedDataCache = decompressedDataCache;
		this.postProcDataCache = postProcDataCache;
		this.dsServer = dsServer;
		this.dataBucket = dataBucket;
		remoteFileThreadPool =
				MoreExecutors.listeningDecorator(
						Executors.newFixedThreadPool(filePoolSize));
		cpuThreadPool =
				MoreExecutors.listeningDecorator(
						Executors.newFixedThreadPool(cpuPoolSize));

		this.downsampled = downsampled;
		this.downsampledRate = downsampledRate;
		// this.reds = new RED[redDecoderPoolSize];
		// for (int i = 0; i < reds.length; i++) {
		// reds[i] = new RED();
		// }
		this.minDataReqBytes = minDataReqBytes;
		if (logger.isDebugEnabled()) {
			logger.debug(
					"{}: dataBucketName = {}, threadPoolSize = {}, downsampled = {}, downsampledRate = {}, minDataReqBytes = {}",
					new Object[] { M,
							dataBucket,
							filePoolSize,
							this.downsampled,
							this.downsampledRate,
							this.minDataReqBytes });
		}
		this.idxService = indexService;
		this.userSemaphoresRawRed =
				Striped.semaphore(1000, maxUserRequestsRawRed);
		this.sessionLocks = Striped.lock(1000);
		this.allUsersSemaphore = new Semaphore(maxUsers);
		this.timeoutSecs = timeoutSecs;
		this.maxReqHzChsSecs = maxReqHzChsSecs;
		this.maxRedReqHzChsSecs = maxRedReqHzChsSecs;
	}

	@Override
	public String getAcquisitionSystem(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getAcquisitionSystem();
	}

	private double getBiggestSampleRate(
			Iterable<? extends ChannelSpecifier> chSpecs) {
		double biggestSampleRate = -1.0;
		for (ChannelSpecifier chSpec : chSpecs) {
			double thisSampleRate = getSampleRate(chSpec);
			if (thisSampleRate > biggestSampleRate) {
				biggestSampleRate = thisSampleRate;
			}
		}
		return biggestSampleRate;
	}

	@Override
	public String getChannelComments(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getChannelComments();
	}

	@Override
	public String getChannelName(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getChannelName();
	}

	@Override
	public String getDataVersion(ChannelSpecifier chSpec) {
		return AwsUtil.getETag(s3, dataBucket, chSpec.getHandle());
	}

	@Override
	public ChannelSpecifier getChannelSpecifier(
			User user,
			SnapshotSpecifier dsSpec,
			final String tsId,
			double samplingPeriod) throws AuthorizationException {
		final String M = "getChannelSpecifier(...)";
		logger.debug("{}: user: {} dsSpec: {} tsId: {} samplingPeriod: {}",
				new Object[] {
						M,
						user.getUsername(),
						dsSpec,
						tsId,
						samplingPeriod });
		checkArgument(dsSpec instanceof MEFSnapshotSpecifier,
				"Illegal to use MEF reader with non-MEF snapshot token!");

		return getChannelSpecifiers(
				user,
				dsSpec,
				newHashSet(tsId),
				samplingPeriod).get(tsId);
	}

	@Override
	public Map<String, ChannelSpecifier> getChannelSpecifiers(
			User user,
			SnapshotSpecifier dsSpec,
			Set<String> tsIds,
			double samplingPeriod) throws AuthorizationException {
		final String M = "getChannelSpecifiers(...)";
		logger.debug("{}: user: {} dsSpec: {} tsId: {} samplingPeriod: {}",
				new Object[] {
						M,
						user.getUsername(),
						dsSpec,
						tsIds,
						samplingPeriod });
		checkArgument(dsSpec instanceof MEFSnapshotSpecifier,
				"Illegal to use MEF reader with non-MEF snapshot token!");

		Map<MEFChannelSpecifier, Double> chSpecs2SampleRates = newHashMap();
		List<MEFChannelSpecifier> tsIdChSpecs = newArrayList();

		Map<String, MEFChannelSpecifier> tsIdsPlusRatesToChSpecsLocal = tsIdsPlusRatesToChSpecs
				.asMap();

		if (user.isAdmin()) {
			tsIdsPlusRatesToChSpecsLocal = newHashMap();
		}

		for (String tsId : tsIds) {
			MEFChannelSpecifier chSpec =
					tsIdsPlusRatesToChSpecsLocal.get(tsId);
			if (chSpec != null) {
				tsIdChSpecs.add(chSpec);
			} else {
				// load up all channels specs for this datasnapshot
				Map<String, TimeSeriesDto> timeSeries = dsServer
						.getTimeSeries(
								user,
								dsSpec.getRevID());
				for (String tsId2 : timeSeries.keySet()) {
					// Don't hit s3 if it's an admin because this method
					// may be called repeatedly asking for single channels
					// and so this will hit s3 channels * channels times.
					if (user.isAdmin() && !tsIds.contains(tsId2)) {

					} else {
						MEFChannelSpecifier tsIdChSpec = new MEFChannelSpecifier(
								(MEFSnapshotSpecifier) dsSpec,
								timeSeries.get(tsId2).getFileKey(),
								tsId2,
								0,
								AwsUtil.getETag(
										s3,
										dataBucket,
										timeSeries.get(tsId2).getFileKey()));
						tsIdsPlusRatesToChSpecsLocal.put(tsId2, tsIdChSpec);
					}
				}
			}
		}

		List<Double> sampleRates = getSampleRates(tsIdChSpecs);
		for (int i = 0; i < tsIdChSpecs.size(); i++) {
			chSpecs2SampleRates.put(tsIdChSpecs.get(i),
					sampleRates.get(i));
		}

		Map<String, ChannelSpecifier> tsIds2ChSpecs = newHashMap();

		for (String tsId : tsIds) {

			MEFChannelSpecifier tsIdChSpec = tsIdsPlusRatesToChSpecsLocal
					.get(tsId);

			final Boolean dssIsDownsampled =
					dssToIsDownsampled.getIfPresent(dsSpec);
			if (!downsampled
					||
					(dssIsDownsampled != null && dssIsDownsampled == false)) {
				tsIds2ChSpecs.put(tsId, tsIdChSpec);
			} else {
				double rate = 0;

				if (samplingPeriod > 0) {
					rate = 1.E6 / samplingPeriod;
				}

				// If rate == 0 then we are good.
				// If rate < 0 then we need to do a scale factor (which means we
				// need.
				// to get the native rate)
				// If rate > 0 then we need to find the closest match

				String tsIdPlusRate = tsId;

				if (rate == -1) { // -1 = full sample rate, which is the same
									// as
									// 0
					rate = 0;
				}
				if (rate != 0) {
					if (rate < 0) {
						double realRate = getSampleRate(tsIdChSpec);
						rate = realRate / (-rate);
					} else {
						rate = 1.E6 / samplingPeriod;
					}

					// // Find nearest upwards match...?
					if (rate <= downsampledRate) {
						rate = downsampledRate;
						tsIdPlusRate = tsId + "-" + String.valueOf((int) rate);
					}
				}
				MEFChannelSpecifier tsIdPlusRateChSpec = null;

				if (tsIdsPlusRatesToChSpecsLocal.containsKey(tsIdPlusRate)) {
					tsIdPlusRateChSpec = tsIdsPlusRatesToChSpecsLocal
							.get(tsIdPlusRate);
				} else {
					// so it must be a "CH-RATE.mef" variety because otherwise
					// we
					// would
					// have already added it to tsIdsPlusRatesToChSpecs
					String nativePath = tsIdsPlusRatesToChSpecsLocal.get(tsId)
							.getHandle();
					String pathPlusRate =
							nativePath.substring(0, nativePath.length() - 4)
									+ "-"
									+ String.valueOf((int) rate)
									+ ".mef";

					Boolean isDownsampled =
							dssToIsDownsampled.getIfPresent(dsSpec);
					if (isDownsampled == null) {
						final long validityStart = System.nanoTime();
						isDownsampled = Boolean.valueOf(AwsUtil.isValidFile(s3,
								dataBucket, pathPlusRate));
						timeLogger.trace("{}: validity check {} seconds", M,
								BtUtil.diffNowThenSeconds(validityStart));
						dssToIsDownsampled.put(dsSpec, isDownsampled);
						logger.debug(
								"{}: setting {} is downsampled to {}",
								new Object[] { M,
										dsSpec, isDownsampled });

					}
					if (isDownsampled) {
						tsIdPlusRateChSpec = new MEFChannelSpecifier(
								(MEFSnapshotSpecifier) dsSpec,
								pathPlusRate,
								tsIdPlusRate,
								(int) rate,
								AwsUtil.getETag(s3, dataBucket, pathPlusRate));
						tsIdsPlusRatesToChSpecsLocal.put(
								tsIdPlusRate,
								tsIdPlusRateChSpec);
					} else {
						logger.debug("{}: returning {}", M, tsIdChSpec);
						tsIdPlusRateChSpec = tsIdChSpec;
					}
				}
				tsIds2ChSpecs.put(tsId, tsIdPlusRateChSpec);
			}
		}
		logger.debug("{}: returning {}", M, tsIds2ChSpecs);
		return tsIds2ChSpecs;
	}

	private Optional<long[]> getChaptersWeNeed(
			ChannelSpecifier chSpec,
			long startChapter,
			long endChapter,
			Map<MEFDataCacheKey, byte[]> pageDataWeHave,
			long chapterSize,
			MefHeader2 header,
			boolean readFromS3) throws StaleMEFException {
		final String M = "getPagesWeNeed(...)";
		Long startChapterWeNeed = null;
		Long endChapterWeNeed = null;

		for (long chapter = startChapter; chapter <= endChapter; chapter += chapterSize) {
			MEFDataCacheKey fileKeyPageNo =
					new MEFDataCacheKey(
							chSpec.getHandle(),
							chapter,
							chSpec.getCheckStr());

			byte[] data = pageDataWeHave.get(fileKeyPageNo);

			if (data == null) {
				Element elem = dataCache.get(fileKeyPageNo);
				if (elem != null) {
					data = (byte[]) elem.getObjectValue();
					pageDataWeHave.put(fileKeyPageNo, data);
				}
			}
			if (data == null) {
				if (startChapterWeNeed == null) {
					startChapterWeNeed = fileKeyPageNo
							.getPageNo();
				}
				endChapterWeNeed = fileKeyPageNo.getPageNo()
						+ chapterSize - 1;
				if (endChapterWeNeed > header.getNumberOfIndexEntries() - 1) {
					endChapterWeNeed = header.getNumberOfIndexEntries() - 1;
				}
			}
		}

		if (startChapterWeNeed == null) {
			return Optional.of(
					new long[] {
							-1,
							-1,
							-1,
							-1 });
		} else {
			Optional<Long> startOffset =
					idxService.getStartByteOffset(
							header,
							chSpec,
							startChapterWeNeed,
							readFromS3);
			Optional<Long> endOffset =
					idxService.getEndByteOffset(
							header,
							chSpec,
							endChapterWeNeed,
							readFromS3);
			if (!startOffset.isPresent() || !endOffset.isPresent()) {
				return Optional.absent();
			}

			logger.debug(
					"{}: pageList.startPage: {} pageList.endPage: {} startPageWeNeed: {} endPageWeNeed: {} startOffset: {} endOffset: {} reqPageCount: {}",
					new Object[] {
							M, startChapter,
							endChapter, startChapterWeNeed, endChapterWeNeed,
							startOffset, endOffset, chapterSize });
			return Optional.of(
					new long[] {
							startChapterWeNeed,
							endChapterWeNeed,
							startOffset.get(),
							endOffset.get()
					});
		}
	}

	private Optional<Map<MEFDataCacheKey, byte[]>> getData(
			ChannelSpecifier chSpec,
			long startChapter,
			long endChapter,
			long chapterSize,
			MefHeader2 header,
			boolean readFromS3) throws StaleMEFException {
		String m = "getData(...)";
		checkArgument(chSpec instanceof MEFChannelSpecifier);
		Map<MEFDataCacheKey, byte[]> pageDataWeHave = newHashMap();
		Optional<long[]> pagesWeNeedOpt = getChaptersWeNeed(
				chSpec,
				startChapter,
				endChapter,
				pageDataWeHave,
				chapterSize,
				header,
				readFromS3);

		if (!pagesWeNeedOpt.isPresent()) {
			return Optional.absent();
		}

		long[] pagesWeNeed = pagesWeNeedOpt.get();

		if (pagesWeNeed[0] != -1 && !readFromS3) {
			return Optional.absent();
		}

		try {

			if (pagesWeNeed[0] != -1) {

				long startChapterWeNeed = pagesWeNeed[0];
				long endChapterWeNeed = pagesWeNeed[1];

				long startOffset = pagesWeNeed[2];
				long endOffset = pagesWeNeed[3];

				logger.debug("{}: requesting {} {} {}", new Object[] {
						m, chSpec.getHandle(), startChapterWeNeed,
						endChapterWeNeed });
				InputStream s3ObjectInputStream = null;
				Map<MEFDataCacheKey, byte[]> pageDataWeJustRead = newHashMap();
				long awsNanos = System.nanoTime();
				try {
					S3Object s3Object = AwsUtil.requestS3Object(
							s3,
							AwsUtil.createGetObjectRequest(
									dataBucket,
									chSpec.getHandle(),
									startOffset,
									endOffset,
									chSpec.getCheckStr()),
							"data");
					if (s3Object == null) {
						throw new StaleMEFException(chSpec.getId());
					}
					s3ObjectInputStream = s3Object.getObjectContent();
					for (long chapter = startChapterWeNeed; chapter <= endChapterWeNeed; chapter += chapterSize) {
						logger.debug(
								"pageNo {} startPageWeNeed {} endPageWeNeed {} reqPageCount {}",
								new Object[] { chapter, startChapterWeNeed,
										endChapterWeNeed, chapterSize });
						long lastPageInGroup =
								chapter + chapterSize - 1;
						if (lastPageInGroup > header
								.getNumberOfIndexEntries() - 1) {
							lastPageInGroup =
									header.getNumberOfIndexEntries() - 1;
						}

						Optional<Long> startByteOffsetOpt = idxService
								.getStartByteOffset(
										header,
										chSpec,
										chapter,
										readFromS3);

						Optional<Long> endByteOffsetOpt = idxService
								.getEndByteOffset(
										header,
										chSpec,
										lastPageInGroup,
										readFromS3);

						if (!startByteOffsetOpt.isPresent()
								|| !endByteOffsetOpt.isPresent()) {
							return Optional.absent();
						}

						long startByteOffset = startByteOffsetOpt.get();
						long endByteOffset = endByteOffsetOpt.get();

						int len =
								Ints.checkedCast(
										endByteOffset - startByteOffset)
												+ 1;

						byte[] pageData = new byte[len];
						ByteStreams.readFully(s3ObjectInputStream, pageData);
						MEFDataCacheKey fileKeyPageNo =
								new MEFDataCacheKey(
										chSpec.getHandle(),
										chapter,
										chSpec.getCheckStr());
						pageDataWeJustRead.put(fileKeyPageNo, pageData);
					}
				} finally {
					BtUtil.close(s3ObjectInputStream);
					timeLogger.trace("{}: from s3 {}",
							m,
							BtUtil.diffNowThenSeconds(awsNanos));
				}
				for (long chapter = startChapterWeNeed; chapter <= endChapterWeNeed; chapter += chapterSize) {

					MEFDataCacheKey fileKeyPageNo =
							new MEFDataCacheKey(
									chSpec.getHandle(),
									chapter,
									chSpec.getCheckStr());

					dataCache.put(new Element(fileKeyPageNo,
							pageDataWeJustRead.get(fileKeyPageNo)));
					// Lock pageLock = pageLocks.get(fileKeyPageNo);
					// pageLock.unlock();
					// myPageLocks.remove(pageLock);

					// only hang onto it if we need it right now
					if (startChapter <= chapter
							&& chapter <= endChapter) {
						pageDataWeHave.put(
								fileKeyPageNo,
								pageDataWeJustRead.get(fileKeyPageNo));
					}
				}
			}

			return Optional.of(pageDataWeHave);
		} catch (Exception e) {
			throw propagate(e);
		}
	}

	@Override
	public long getEndUutc(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getRecordingEndTime();
	}

	private MefHeader2 getHeader(ChannelSpecifier chSpec) {
		return getHeader(chSpec, true).get();
	}

	private Optional<MefHeader2> getHeader(
			ChannelSpecifier chSpec,
			boolean goToS3) {
		try {

			MEFHeaderCacheKey mefHeaderCacheKey = new MEFHeaderCacheKey(
					chSpec.getHandle(),
					chSpec.getCheckStr());

			Element elem = headerCache.get(
					mefHeaderCacheKey);
			if (elem != null) {
				return Optional.of((MefHeader2) elem.getObjectValue());
			}

			if (goToS3) {

			} else {
				return Optional.absent();
			}

			byte[] result = MEFPageServerS3Util.getHeader(
					s3,
					dataBucket,
					chSpec);

			MefHeader2 header = new MefHeader2(result);
			headerCache.put(
					new Element(
							mefHeaderCacheKey,
							header));

			return Optional.of(header);
		} catch (Exception e) {
			throw propagate(e);
		}
	}

	private Map<ChannelSpecifier, MefHeader2> getHeaders(
			Set<? extends ChannelSpecifier> chSpecs) {
		try {
			List<ListenableFuture<Object[]>> requests = newArrayList();
			Map<ChannelSpecifier, MefHeader2> headers = newHashMap();
			for (ChannelSpecifier chSpec : chSpecs) {
				Optional<MefHeader2> headerOpt = getHeader(chSpec, false);
				if (headerOpt.isPresent()) {
					headers.put(chSpec, headerOpt.get());
				} else {
					MEFHeaderLoaderS3 loader =
							new MEFHeaderLoaderS3(chSpec);
					ListenableFuture<Object[]> future =
							remoteFileThreadPool.submit(loader);
					requests.add(future);
				}
			}
			ListenableFuture<List<Object[]>> wrappedRequests =
					Futures.allAsList(requests);
			for (Object[] request : wrappedRequests.get()) {
				headers.put(
						(ChannelSpecifier) request[0],
						(MefHeader2) request[1]);
			}
			return headers;
		} catch (ExecutionException e) {
			throw propagate(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw propagate(e);
		}
	}

	@Override
	public String getInstitution(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getInstitution();
	}

	@Override
	public int getMaxSampleValue(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getMaximumDataValue();
	}

	@Override
	public int getMinSampleValue(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getMinimumDataValue();
	}

	@Override
	public long getNumIndexEntries(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getNumberOfIndexEntries();
	}

	@Override
	public long getNumSamples(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getNumberOfSamples();
	}

	@Override
	public List<String> getPageServerState() {
		return emptyList();
	}

	@Override
	public double getSampleRate(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getSamplingFrequency();
	}

	@Override
	public List<Double> getSampleRates(List<? extends ChannelSpecifier> chSpecs) {
		List<Double> sampleRates = newArrayList();
		Map<ChannelSpecifier, MefHeader2> headers = getHeaders(newHashSet(chSpecs));
		for (ChannelSpecifier chSpec : chSpecs) {
			MefHeader2 header = headers.get(chSpec);
			sampleRates.add(Double.valueOf(header.getSamplingFrequency()));
		}
		return sampleRates;
	}

	@Override
	public MEFSnapshotSpecifier getSnapshotSpecifier(
			User user, String revId) throws AuthorizationException {
		return new MEFSnapshotSpecifier("", revId);
	}

	@Override
	public long getStartUutc(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getRecordingStartTime();
	}

	@Override
	public String getSubjectId(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getSubjectId();
	}

	@Override
	public List<TraceInfo> getTraceInfos(
			List<ChSpecTimeSeriesDto> cstsList)
			throws InterruptedException, ExecutionException {
		String m = "getTraceInfos(...)";
		long in = System.nanoTime();
		try {
			List<TraceInfo> traceInfos = newArrayList();
			List<ChannelSpecifier> chSpecs = newArrayList();
			for (ChSpecTimeSeriesDto chSpecTs : cstsList) {
				chSpecs.add(chSpecTs.channelSpecifier);
			}

			Map<ChannelSpecifier, MefHeader2> chSpecs2Headers = getHeaders(newHashSet(chSpecs));

			List<MefHeader2> headers = newArrayList();

			for (ChannelSpecifier chSpec : chSpecs) {
				headers.add(chSpecs2Headers.get(chSpec));
			}

			if (cstsList.size() != headers.size()) {
				throw new AssertionError();
			}
			if (cstsList.size() != headers.size()) {
				throw new AssertionError();
			}
			Iterator<ChSpecTimeSeriesDto> cstsIter = cstsList
					.iterator();

//			int i = 0;
			for (final MefHeader2 header : headers) {
				final double duration = header.getRecordingEndTime()
						- header.getRecordingStartTime();
				final TimeSeriesDto tsDto = cstsIter.next().timeSeriesDto;
				final TraceInfo ti = new TraceInfo(
						null,// chSpecs.get(i).getSnapshot(),
						0,
						duration,
						header.getSamplingFrequency(),
						header.getVoltageConversionFactor(),
						header.getMinimumDataValue(),
						header.getMaximumDataValue(),
						header.getInstitution(),
						header.getAcquisitionSystem(),
						header.getChannelComments(),
						header.getSubjectId(),
						tsDto.getLabel(),
						tsDto.getId());
				traceInfos.add(ti);
//				i++;
			}

			return traceInfos;
		} finally {
			timeLogger.trace("{}: {}", m, BtUtil.diffNowThenSeconds(in));
		}
	}

	@Override
	public double getVoltageConversionFactor(ChannelSpecifier chSpec) {
		return getHeader(chSpec).getVoltageConversionFactor();
	}

	@Override
	public List<List<TimeSeriesData>>
			requestMultiChannelRead(
					List<? extends ChannelSpecifier> chSpecs,
					long startTime,
					long endTime,
					PostProcessor[] postProcs,
					User user,
					@Nullable SessionToken sessionToken)
					throws InterruptedException, ExecutionException,
					ServerTimeoutException {
		checkNotNull(user);
		checkArgument(endTime >= startTime);

		long in = System.nanoTime();

		String m = "requestMultiChannelRead(...)";

		double biggestSampleRate = getBiggestSampleRate(chSpecs);

		long reqSizeMicros = endTime - startTime;

		if (biggestSampleRate * (reqSizeMicros / 1E6) * chSpecs.size() > maxReqHzChsSecs) {
			throw new TooMuchDataRequestedException(
					chSpecs.size(),
					reqSizeMicros / 1E6,
					biggestSampleRate,
					IvProps.MEF_PAGE_SERVER_S3_MAX_REQ_HZ_DFLT,
					IvProps.MEF_PAGE_SERVER_S3_MAX_REQ_CHS_DFLT,
					IvProps.MEF_PAGE_SERVER_S3_MAX_REQ_SECS_DFLT);
		}

		Lock mySessionLock = null;
		boolean acquiredMySessionLock = false;
		try {
			Object sessionLockKey = null;
			if (sessionToken == null) {
				// until we figure out how best to calls in from
				// web services, we'll lock on the user
				sessionLockKey = user;
			} else {
				sessionLockKey = sessionToken;
			}

			mySessionLock = sessionLocks.get(sessionLockKey);

			logger.debug("{}: about to get per-session lock {}",
					m,
					user.getUsername());

			if (((ReentrantLock) mySessionLock).isLocked()) {
				logger.info(
						"{}: no per-session lock, waiting... user {}",
						m,
						user.getUsername());
			}

			if (mySessionLock.tryLock(timeoutSecs, TimeUnit.SECONDS)) {
				acquiredMySessionLock = true;
				logger.debug("{}: got my permit {} seconds", m,
						BtUtil.diffNowThenSeconds(in));
			} else {
				throw new ServerTimeoutException("server busy");
			}
			boolean acquiredAllUsersSemaphore = false;

			try {

				long timeSoFarNanos = System.nanoTime() - in;

				// is max necessary? - I don't know.
				long timeoutNanos = Math.max(
						0,
						BtUtil.secs2Nanos(timeoutSecs) - timeSoFarNanos);

				if (allUsersSemaphore.availablePermits() == 0) {
					logger.info(
							"{}: no all-user permits , waiting... user {}",
							m,
							user.getUsername());
				}
				if (allUsersSemaphore.tryAcquire(
						timeoutNanos,
						TimeUnit.NANOSECONDS)) {
					acquiredAllUsersSemaphore = true;
				} else {
					throw new ServerTimeoutException("server busy");
				}

				// samd changed from
				// long blockSize = 30 * (long) 1E6;
				// to for s3,

				long blockSize = 3000 * (long) 1E6;

				if (biggestSampleRate > 200.0) {
					blockSize = 600 * (long) 1E6;
				} else if (biggestSampleRate > 600.0) {
					blockSize = 60 * (long) 1E6;
				}

				long blockEnd = (endTime > startTime + blockSize) ? startTime
						+ blockSize : endTime;
				long blockStart = startTime;

				if (blockEnd < blockStart) {
					blockStart = blockEnd;
				}

				List<List<TimeSeriesData>> ret = requestMultiChannelReadBlock(
						chSpecs,
						blockStart, blockEnd, postProcs, in);

				blockStart += blockSize;
				while (blockStart < endTime) {
					blockEnd = (endTime > blockStart + blockSize) ? blockStart
							+ blockSize : endTime;

					List<List<TimeSeriesData>> newSegment = requestMultiChannelReadBlock(
							chSpecs,
							blockStart,
							blockEnd,
							postProcs,
							in);

					merge(ret, newSegment);
					blockStart += blockSize;
				}

				return ret;
			} finally {
				if (acquiredAllUsersSemaphore) {
					allUsersSemaphore.release();
				}
			}
		} finally {
			if (acquiredMySessionLock) {
				mySessionLock.unlock();
			}
		}
	}

	@Override
	protected List<List<TimeSeriesData>> requestMultiChannelReadBlock(
			List<? extends ChannelSpecifier> paths, long startTime,
			long endTime, PostProcessor[] postProc)
			throws InterruptedException, ExecutionException,
			ServerTimeoutException {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	private List<List<TimeSeriesData>> requestMultiChannelReadBlock(
			List<? extends ChannelSpecifier> chSpecs,
			long startTimeOffset,
			long endTimeOffset,
			PostProcessor[] postProcs,
			final long timeoutStartNanos)
			throws InterruptedException, ExecutionException,
			ServerTimeoutException {
		String m = "requestMultiChannelReadBlock(...)";

		final long in = System.nanoTime();
		double initialReads = 0.0;
		double restOfIt = -1;

		try {
			checkArgument(startTimeOffset <= endTimeOffset);
			checkArgument(chSpecs.size() <= postProcs.length,
					"chSpecs.size %s is > postProc.length %s",
					chSpecs.size(),
					postProcs.length);

			List<MEFChannelSpecifier> mefChSpecs =
					newArrayList(filter(chSpecs, MEFChannelSpecifier.class));
			checkArgument(mefChSpecs.size() == chSpecs.size(),
					"some chSpecs were not of type MEFChannelSpecifier");
			List<ListenableFuture<ChSpecAndTsDatas>> requests = newArrayList();
			Map<ChannelSpecifier, List<TimeSeriesData>> chsToData = newHashMap();

			List<List<TimeSeriesData>> ret = newArrayList();

			int chSpecIdx = -1;
			for (final ChannelSpecifier chSpec : chSpecs) {
				long startTime = startTimeOffset - chSpec.getTimeOffset();
				long endTime = endTimeOffset - chSpec.getTimeOffset();
				
				chSpecIdx++;
				ArrayList<TimeSeriesData> postProcData = null;

				if (postProcs[chSpecIdx] instanceof Decimate) {
					DecimateCacheKey decimateCacheKey =
							new DecimateCacheKey(
									chSpec.getHandle(),
									chSpec.getCheckStr(),
									startTime,
									endTime,
									(Decimate) postProcs[chSpecIdx]);
					Element elem = postProcDataCache.get(decimateCacheKey);
					if (elem != null) {
						postProcData =
								(ArrayList<TimeSeriesData>) elem
										.getObjectValue();
					}
				}

				if (postProcData != null) {
					chsToData.put(chSpec, postProcData);
				} else {

					long thisInitialRead = System.nanoTime();

					final Optional<ForDecompressAndPostProc> data = requestReadHelper(
							chSpec,
							startTime,
							endTime,
							postProcs[chSpecIdx],
							false);
					initialReads += BtUtil
							.diffNowThenSeconds(thisInitialRead);

					if (data.isPresent()) {
						logger.debug(
								"{}: found everything we need locally", m);

						Callable<ChSpecAndTsDatas> decompAndPostProcCallable = new Callable<ChSpecAndTsDatas>() {
							@Override
							public ChSpecAndTsDatas call()
									throws Exception {
								DecompressAndPostProc decompAndPostProc =
										new DecompressAndPostProc(
												decompressedDataCache
										);
								return decompAndPostProc.apply(data.get());
							}
						};
						requests.add(
								cpuThreadPool.submit(decompAndPostProcCallable));
					} else {
						logger.debug("{}: going to s3");
						PageLoaderS3 pageLoader =
								new PageLoaderS3(
										chSpec,
										startTime,
										endTime,
										postProcs[chSpecIdx]);
						ListenableFuture<ForDecompressAndPostProc> pageLoaderLf =
								remoteFileThreadPool.submit(pageLoader);
						ListenableFuture<ChSpecAndTsDatas> result = Futures
								.transform(
										pageLoaderLf,
										new DecompressAndPostProc(
												decompressedDataCache
										),
										cpuThreadPool);
						requests.add(result);
					}
				}
			}
			long timeSoFarNanos = System.nanoTime() - timeoutStartNanos;

			// is this necessary? - I don't know.
			long timeoutNanos = Math.max(
					0,
					BtUtil.secs2Nanos(timeoutSecs) - timeSoFarNanos);

			ListenableFuture<List<ChSpecAndTsDatas>> wrappedRequests =
					Futures.allAsList(requests);

			try {
				List<ChSpecAndTsDatas> results = wrappedRequests.get(
						timeoutNanos,
						TimeUnit.NANOSECONDS);
				for (ChSpecAndTsDatas request : results) {
					chsToData.put(request.chSpec, request.timeSeriesDatas);
					if (request.postProc instanceof Decimate) {
						DecimateCacheKey decimateCacheKey =
								new DecimateCacheKey(
										request.chSpec.getHandle(),
										request.chSpec.getCheckStr(),
										startTimeOffset,
										endTimeOffset,
										(Decimate) request.postProc);
						postProcDataCache
								.put(new Element(decimateCacheKey,
										request.timeSeriesDatas));
					}
				}
			} catch (TimeoutException toe) {
				logger.debug("{}: cancelling task caught ", m, toe);
				cancel(requests, startTimeOffset, endTimeOffset);
				throw new ServerTimeoutException("server busy", toe);
			} catch (RuntimeException e) {
				logger.debug("{}: cancelling task caught ", m, e);
				cancel(requests, startTimeOffset, endTimeOffset);
				throw e;
			}
			for (ChannelSpecifier chSpec : chSpecs) {

				// in case we already added it - it's confusing to the
				// caller
				// if we return the same list more than once
				ArrayList<TimeSeriesData> tsDatasCopy =
						newArrayList(chsToData.get(chSpec));
				ret.add(tsDatasCopy);
			}

			return ret;
		} finally {
			timeLogger
					.trace("{}: initialReads {} restOfIt {} total {} seconds",
							new Object[] {
									m,
									initialReads,
									restOfIt,
									BtUtil.diffNowThenSeconds(in)
							});
		}
	}

	private void cancel(
			List<ListenableFuture<ChSpecAndTsDatas>> futures,
			long startTimeOffset,
			long endTimeOffset) {
		String m = "cancel(...)";
		for (Future<ChSpecAndTsDatas> futureCancel : futures) {
			if (!futureCancel.isDone()) {
				futureCancel.cancel(true);
			} else {
				try {
					ChSpecAndTsDatas request = futureCancel.get();
					if (request.postProc instanceof Decimate) {
						DecimateCacheKey decimateCacheKey =
								new DecimateCacheKey(
										request.chSpec.getHandle(),
										request.chSpec
												.getCheckStr(),
										startTimeOffset,
										endTimeOffset,
										(Decimate) request.postProc);
						postProcDataCache
								.put(new Element(decimateCacheKey,
										request.timeSeriesDatas));
					}
				} catch (Exception e1) {
					logger.error(
							m + ": caught from future, continuing", e1);
				}
			}
		}
	}

	@Override
	public List<MEFDataAndPageNos> requestMultiChannelReadRed(
			List<? extends ChannelSpecifier> chSpecs,
			final long startTimeOffset,
			final long endTimeOffset,
			User user) throws StaleMEFException, ServerTimeoutException {
		final long in = System.nanoTime();
		checkNotNull(user);
		String m = "requestMultiChannelReadRed(...)";
		logger.debug(
				"{}: startTimeOffSet: {} endTimeOffset: {}",
				new Object[] {
						m, startTimeOffset, endTimeOffset });

		List<MEFChannelSpecifier> mefChSpecs =
				newArrayList(filter(chSpecs, MEFChannelSpecifier.class));
		checkArgument(mefChSpecs.size() == chSpecs.size(),
				"some chSpecs were not of type MEFChannelSpecifier");

		double biggestSampleRate = getBiggestSampleRate(chSpecs);

		checkArgument(startTimeOffset <= endTimeOffset);

		long reqSizeMicros = endTimeOffset - startTimeOffset;

		if (biggestSampleRate * (reqSizeMicros / 1E6) * chSpecs.size() > maxRedReqHzChsSecs) {
			throw new TooMuchDataRequestedException(
					chSpecs.size(),
					reqSizeMicros / 1E6,
					biggestSampleRate,
					IvProps.MEF_PAGE_SERVER_S3_MAX_RED_REQ_HZ_DFLT,
					IvProps.MEF_PAGE_SERVER_S3_MAX_RED_REQ_CHS_DFLT,
					IvProps.MEF_PAGE_SERVER_S3_MAX_RED_REQ_SECS_DFLT);
		}

		Semaphore myUserSemaphore = null;
		boolean acquiredMyUserSemaphore = false;
		try {
			myUserSemaphore = userSemaphoresRawRed.get(user);
			if (myUserSemaphore.availablePermits() == 0) {
				logger.info(
						"{}: no per-user permits , waiting... user {}",
						m,
						user.getUsername());
			}
			if (myUserSemaphore.tryAcquire(timeoutSecs, TimeUnit.SECONDS)) {
				acquiredMyUserSemaphore = true;
			} else {
				throw new ServerTimeoutException("server busy");
			}
			boolean acquiredAllUsersSemaphore = false;

			try {
				long timeSoFarNanos = System.nanoTime() - in;

				// is max necessary? - I don't know.
				long timeoutNanos = Math.max(
						0,
						BtUtil.secs2Nanos(timeoutSecs) - timeSoFarNanos);
				if (allUsersSemaphore.availablePermits() == 0) {
					logger.info(
							"{}: no all-user permits , waiting... user {}",
							m,
							user.getUsername());
				}
				if (allUsersSemaphore.tryAcquire(
						timeoutNanos,
						TimeUnit.NANOSECONDS)) {
					acquiredAllUsersSemaphore = true;
				} else {
					throw new ServerTimeoutException("server busy");
				}

				List<ListenableFuture<ChSpecAndData>> requests = newArrayList();

				Map<ChannelSpecifier, MEFDataAndPageNos> chSpecs2Data = newHashMap();

				for (final MEFChannelSpecifier chSpec : mefChSpecs) {

					Optional<MEFDataAndPageNos> dataOpt =
							requestReadHelperRed(
									chSpec,
									startTimeOffset,
									endTimeOffset,
									false,
									in);
					if (dataOpt.isPresent()) {
						chSpecs2Data.put(chSpec, dataOpt.get());
					} else {

						Callable<ChSpecAndData> loader = new Callable<ChSpecAndData>() {
							@Override
							public ChSpecAndData call() throws Exception {
								return new ChSpecAndData(
										chSpec,
										requestReadHelperRed(
												chSpec,
												startTimeOffset,
												endTimeOffset,
												true,
												in).get());
							}
						};
						requests.add(remoteFileThreadPool.submit(loader));
					}
				}
				ListenableFuture<List<ChSpecAndData>> wrappedRequests =
						Futures.allAsList(requests);

				timeSoFarNanos = System.nanoTime() - in;

				// is max necessary? - I don't know.
				timeoutNanos = Math.max(
						0,
						BtUtil.secs2Nanos(timeoutSecs) - timeSoFarNanos);
				boolean caughtException = false;
				try {
					List<ChSpecAndData> results = wrappedRequests.get(
							timeoutNanos,
							TimeUnit.NANOSECONDS);
					for (ChSpecAndData request : results) {
						chSpecs2Data.put(request.chSpec, request.data);
					}
				} catch (TimeoutException e) {
					caughtException = true;
					throw new ServerTimeoutException("server busy", e);
				} catch (ExecutionException e) {
					caughtException = true;
					throw propagate(e);
				} catch (RuntimeException e) {
					caughtException = true;
					throw e;
				} finally {
					if (caughtException) {
						for (Future<ChSpecAndData> futureCancel : requests) {
							futureCancel.cancel(true);
						}
					}
				}

				List<MEFDataAndPageNos> ret = newArrayList();

				for (ChannelSpecifier chSpec : chSpecs) {
					ret.add(chSpecs2Data.get(chSpec));
				}

				return ret;
			} finally {
				if (acquiredAllUsersSemaphore) {
					allUsersSemaphore.release();
				}
			}
		} catch (StaleMEFException e) {
			throw e;
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw propagate(e);
		} finally {
			if (acquiredMyUserSemaphore) {
				myUserSemaphore.release();
			}
			timeLogger
					.trace("{}: {} seconds", m, BtUtil.diffNowThenSeconds(in));
		}
	}

	@Override
	public List<TimeSeriesData> requestRead(
			ChannelSpecifier chSpec,
			long startTimeOffset,
			long endTimeOffset,
			PostProcessor postProc,
			User user,
			@Nullable SessionToken sessionToken)
			throws InterruptedException,
			ExecutionException, ServerTimeoutException {
		return requestMultiChannelRead(
				newArrayList(chSpec),
				startTimeOffset,
				endTimeOffset,
				new PostProcessor[] { postProc }, user, sessionToken).get(0);
	}

	private Optional<ForDecompressAndPostProc> requestReadHelper(
			ChannelSpecifier chSpec,
			long startTimeOffset,
			long endTimeOffset,
			PostProcessor postProc,
			boolean readFromS3) {

		String m = "requestReadHelper(...)";
		long in = System.nanoTime();

		logger.debug("{}: chSpec: {} {} {}", new Object[] { m, chSpec,
				startTimeOffset, endTimeOffset });
		double headerSec = -1;
		double blkIndicesSec = -1;
		double initalPagesWeNeedSec = -1;
		double subsequentPagesWeNeedLoopSecs = 0;
		double subsequentPagesWeNeedSec = 0;
		double awsSecs = -1;
		double putSecs = 0;
		double postProcSecs = -1;
		double acquiringLocksSecs = 0;
		double copyingArraysSecs = 0;
		double decodingSecs = 0;

		long chapterSize = Math.max(
				1,
				minDataReqBytes
						/ getHeader(chSpec).getMaximumCompressedBlockSize());
		logger.debug("{}: {} sample count {}",
				new Object[] {
						m,
						chSpec.getHandle(),
						getHeader(chSpec).getNumberOfSamples() });

		logger.debug("{}: minReqPageCount {}", m, chapterSize);

		try {
			MEFChannelSpecifier mefChSpec = (MEFChannelSpecifier) chSpec;

			final long startHeader = System.nanoTime();
			Optional<MefHeader2> headerOpt = getHeader(chSpec, readFromS3);

			if (!headerOpt.isPresent()) {
				return Optional.absent();
			}

			MefHeader2 header = headerOpt.get();
			headerSec = BtUtil.diffNowThenSeconds(startHeader);

			long startTimeAbs =
					header.getRecordingStartTime() + startTimeOffset;
			long endTimeAbs =
					header.getRecordingStartTime() + endTimeOffset;

			double samplingPeriodMicros = 1E6 / postProc.getSampleRate();

			long paddingMicros = (long) Math.ceil(
					samplingPeriodMicros
							* getPaddingSamples());

			long startTimeAbsPadded = startTimeAbs - paddingMicros;
			long endTimeAbsPadded = endTimeAbs + paddingMicros;

			long startIndices = System.nanoTime();

			blkIndicesSec = BtUtil.diffNowThenSeconds(startIndices);

			// We need to do this to preserve old behavior of MEFPageServer: its
			// version of blkIndices returns a half open interval, but that
			// seems
			// like a mistake.
			// if (pageList.endPage < blkIndices.getIndexLength() - 1) {
			// pageList.endPage++;
			// }

			Optional<long[]> startPageAndEndPageOpt =
					idxService.getStartAndEndPageNos(
							header,
							mefChSpec,
							startTimeAbsPadded,
							endTimeAbsPadded,
							readFromS3);
			if (!startPageAndEndPageOpt.isPresent()) {
				return Optional.absent();
			}

			long[] startPageAndEndPage = startPageAndEndPageOpt.get();

			long startPage = startPageAndEndPage[0];
			long endPage = startPageAndEndPage[1];

			long startChapter = getChapter(startPage, chapterSize);

			long endChapter = getChapter(endPage, chapterSize);

			logger.debug("{}: pageList.startPage {} pageList.endPage{}",
					new Object[] { m, startPage, endPage });

			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			Optional<Map<MEFDataCacheKey, byte[]>> pageDataWeHave =
					getData(chSpec,
							startChapter,
							endChapter,
							chapterSize,
							header,
							readFromS3);

			if (!pageDataWeHave.isPresent()) {
				return Optional.absent();
			}

			Optional<Long> startChapterByteOffset =
					idxService.getStartByteOffset(
							header,
							mefChSpec,
							startChapter,
							readFromS3);

			Optional<Long> startPageByteOffset =
					idxService.getStartByteOffset(
							header,
							chSpec,
							startPage,
							readFromS3);

			if (!startChapterByteOffset.isPresent()
					|| !startPageByteOffset.isPresent()) {
				return Optional.absent();
			}

			long startPageRelativeByteOffset =
					startPageByteOffset.get()
							- startChapterByteOffset.get();

			ForDecompressAndPostProc forDecompAndPostProc =
					new ForDecompressAndPostProc(
							pageDataWeHave.get(),
							header,
							chSpec,
							postProc,
							startPage,
							endPage,
							chapterSize,
							paddingMicros,
							startTimeOffset,
							endTimeOffset,
							startPageRelativeByteOffset);

			return Optional.of(forDecompAndPostProc);
		} catch (Exception e) {
			throw propagate(e);
		} finally {
			timeLogger
					.trace("{}: header {} secs, blockIndices {} secs, inital pagesWeNeed {} secs, subsequent pagesWeNeed loop {} secs, subsequent pagesWeNeed {} secs, AWS {} secs, data put {} secs, postProc {} secs, locks {} secs, array copies {} secs, decoding {} secs, {} total secs",
							new Object[] {
									m,
									headerSec,
									blkIndicesSec,
									initalPagesWeNeedSec,
									subsequentPagesWeNeedLoopSecs,
									subsequentPagesWeNeedSec,
									awsSecs,
									putSecs,
									postProcSecs,
									acquiringLocksSecs,
									copyingArraysSecs,
									decodingSecs,
									BtUtil.diffNowThenSeconds(in)
							});
			// if (0 == (reqCnt++ % 100)) {
			// logger.info(
			// "{}: header cache stats {}",
			// m, headerCache.getStatistics());
			// logger.info(
			// "{}: data cache stats {}",
			// m, dataCache.getStatistics());
			// logger.info(
			// "{}: index cache stats {}",
			// m, idxService.getCache().getStatistics());
			// logger.info(
			// "{}: decompressed data cache stats {}",
			// m, decompressedDataCache.getStatistics());
			// logger.info(
			// "{}: postproc data cache stats {}",
			// m, postProcDataCache.getStatistics());
			// headerCache.clearStatistics();
			// dataCache.clearStatistics();
			// idxService.getCache().clearStatistics();
			// decompressedDataCache.clearStatistics();
			// postProcDataCache.clearStatistics();
			// }

		}
	}

	private ForDecompressAndPostProc requestReadHelper(
			ChannelSpecifier chSpec,
			long startTimeOffset,
			long endTimeOffset,
			PostProcessor postProc) {
		return requestReadHelper(
				chSpec,
				startTimeOffset,
				endTimeOffset,
				postProc,
				true).get();
	}

	private Optional<MEFDataAndPageNos> requestReadHelperRed(
			ChannelSpecifier chSpec,
			long startTimeOffset,
			long endTimeOffset,
			boolean readFromS3,
			long timeoutStartNanos) throws StaleMEFException {

		String m = "requestReadHelperRed(...)";
		long in = System.nanoTime();

		logger.debug("{}: chSpec: {} {} {}", new Object[] { m, chSpec,
				startTimeOffset, endTimeOffset });
		double headerSec = -1;
		double blkIndicesSec = -1;
		double initalPagesWeNeedSec = -1;
		double subsequentPagesWeNeedLoopSecs = 0;
		double subsequentPagesWeNeedSec = 0;
		double awsSecs = -1;
		double putSecs = 0;
		double postProcSecs = -1;
		double acquiringLocksSecs = 0;
		double copyingArraysSecs = 0;
		double decodingSecs = 0;

		long chapterSize = Math.max(
				1,
				minDataReqBytes
						/ getHeader(chSpec).getMaximumCompressedBlockSize());
		logger.debug("{}: minReqPageCount {}", m, chapterSize);

		try {

			final long startHeader = System.nanoTime();
			Optional<MefHeader2> headerOpt = getHeader(chSpec, readFromS3);

			if (!headerOpt.isPresent()) {
				return Optional.absent();
			}

			MefHeader2 header = headerOpt.get();

			headerSec = BtUtil.diffNowThenSeconds(startHeader);

			long startTimeAbs =
					header.getRecordingStartTime() + startTimeOffset;
			long endTimeAbs =
					header.getRecordingStartTime() + endTimeOffset;

			long startIndices = System.nanoTime();

			blkIndicesSec = BtUtil.diffNowThenSeconds(startIndices);

			// We need to do this to preserve old behavior of MEFPageServer: its
			// version of blkIndices returns a half open interval, but that
			// seems
			// like a mistake.
			// if (pageList.endPage < blkIndices.getIndexLength() - 1) {
			// pageList.endPage++;
			// }

			Optional<long[]> startPageAndEndPageOpt =
					idxService.getStartAndEndPageNos(
							header,
							chSpec,
							startTimeAbs,
							endTimeAbs,
							readFromS3);

			if (!startPageAndEndPageOpt.isPresent()) {
				return Optional.absent();
			}

			long[] startPageAndEndPage = startPageAndEndPageOpt.get();

			long startPage = startPageAndEndPage[0];
			long endPage = startPageAndEndPage[1];

			long startChapter = getChapter(startPage, chapterSize);
			long endChapter = getChapter(endPage, chapterSize);

			logger.debug("{}: pageList.startPage {} pageList.endPage{}",
					new Object[] { m, startPage, endPage });

			if (Thread.interrupted()) {
				throw new InterruptedException();
			}

			Optional<Map<MEFDataCacheKey, byte[]>> pageDataWeHaveOpt =
					getData(chSpec,
							startChapter,
							endChapter,
							chapterSize,
							header,
							readFromS3);

			if (!pageDataWeHaveOpt.isPresent()) {
				return Optional.absent();
			}

			Map<MEFDataCacheKey, byte[]> pageDataWeHave =
					pageDataWeHaveOpt.get();

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			for (long pageNo = startChapter; pageNo <= endChapter; pageNo += chapterSize) {
				MEFDataCacheKey fileKeyPageNo =
						new MEFDataCacheKey(
								chSpec.getHandle(),
								pageNo,
								chSpec.getCheckStr());
				byte[] pagesData = pageDataWeHave.get(fileKeyPageNo);

				logger.debug("pageData {} fileKeyPageNo {}",
						pagesData == null ? null : pagesData.length,
						fileKeyPageNo);

				Optional<Long> pageNoStartOffsetOpt =
						idxService.getStartByteOffset(
								header,
								chSpec,
								pageNo,
								readFromS3);
				if (!pageNoStartOffsetOpt.isPresent()) {
					return Optional.absent();
				}

				long pageNoStartOffset = pageNoStartOffsetOpt.get();

				for (long pageNo2 = Math.max(startPage, pageNo); pageNo2 < pageNo
						+ chapterSize
						&& pageNo2 <= endPage; pageNo2++) {

					Optional<Long> pageNo2StartOffsetOpt =
							idxService.getStartByteOffset(
									header,
									chSpec,
									pageNo2,
									readFromS3);
					Optional<Long> pageNo2EndOffsetOpt =
							idxService.getEndByteOffset(
									header,
									chSpec,
									pageNo2,
									readFromS3);

					if (!pageNo2StartOffsetOpt.isPresent()
							|| !pageNo2EndOffsetOpt.isPresent()) {
						return Optional.absent();
					}

					long pageNo2StartOffset = pageNo2StartOffsetOpt.get();
					long pageNo2EndOffset = pageNo2EndOffsetOpt.get();

					bos.write(Arrays.copyOfRange(
							pagesData,
							Ints.checkedCast(pageNo2StartOffset
									- pageNoStartOffset),
							Ints.checkedCast(pageNo2EndOffset
									- pageNoStartOffset + 1)));
				}
			}
			MEFDataAndPageNos mefPages =
					new MEFDataAndPageNos(
							bos.toByteArray(),
							Range.closed(startPage, endPage));
			return Optional.of(mefPages);
		} catch (StaleMEFException e) {
			throw e;
		} catch (IOException e) {
			throw propagate(e);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw propagate(e);
		} finally {
			timeLogger
					.trace("{}: header {} secs, blockIndices {} secs, inital pagesWeNeed {} secs, subsequent pagesWeNeed loop {} secs, subsequent pagesWeNeed {} secs, AWS {} secs, data put {} secs, postProc {} secs, locks {} secs, array copies {} secs, decoding {} secs, {} total secs",
							new Object[] {
									m,
									headerSec,
									blkIndicesSec,
									initalPagesWeNeedSec,
									subsequentPagesWeNeedLoopSecs,
									subsequentPagesWeNeedSec,
									awsSecs,
									putSecs,
									postProcSecs,
									acquiringLocksSecs,
									copyingArraysSecs,
									decodingSecs,
									BtUtil.diffNowThenSeconds(in) });
			// if (0 == (reqCnt++ % 100)) {
			// logger.info(
			// "{}: header cache stats {}",
			// m, headerCache.getStatistics());
			// logger.info(
			// "{}: data cache stats {}",
			// m, dataCache.getStatistics());
			// logger.info(
			// "{}: index cache stats{}",
			// m, idxService.getCache().getStatistics());
			// headerCache.clearStatistics();
			// dataCache.clearStatistics();
			// idxService.getCache().clearStatistics();
			// }

		}
	}

	@Override
	public void shutdown() {
		final String M = "shutdown()";
		logger.info("{}: shutting down executorService...", M);
		remoteFileThreadPool.shutdownNow();
		logger.info("{}: done", M);
	}

	@Override
	public GeneralMetadata getMetadata(ChannelSpecifier channelSpec) {
		return new TraceInfo(
				null,// ((GeneralMetadata)channelSpec).getParent(),
				0,// getStartTime(channelSpec),
				getEndUutc(channelSpec) - getStartUutc(channelSpec),
				getSampleRate(channelSpec),
				getVoltageConversionFactor(channelSpec),
				getMinSampleValue(channelSpec),
				getMaxSampleValue(channelSpec),
				getInstitution(channelSpec),
				getAcquisitionSystem(channelSpec),
				getChannelComments(channelSpec),
				getSubjectId(channelSpec),
				getChannelName(channelSpec),
				channelSpec.getId());
	}

	@Override
	public InputStream getData(
			String key,
			@Nullable String eTag,
			@Nullable Long startByteOffset,
			@Nullable Long endByteOffset) {
		return AwsUtil.getObjectContent(
				s3,
				dataBucket,
				key,
				eTag,
				startByteOffset,
				endByteOffset);
	}

	@Override
	public long getSizeBytes(ChannelSpecifier channelSpecifier) {
		ObjectMetadata objMetadata = AwsUtil.getObjectMetadata(
				s3,
				dataBucket,
				channelSpecifier.getHandle());
		return objMetadata.getContentLength();
	}

	@Override
	public Map<String, ChannelSpecifier> getChannelSpecifiersFromSegments(User user, SnapshotSpecifier dsSpec,
			Set<INamedTimeSegment> tsIds, double samplingPeriod) throws AuthorizationException {
		return getChannelSpecifiers(user, dsSpec, DataMarshalling.getTimeSegmentIds(tsIds), samplingPeriod);
	}
}
