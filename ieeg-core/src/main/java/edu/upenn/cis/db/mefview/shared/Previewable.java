package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface Previewable extends Serializable {
	@JsonIgnore
	public String getObjectName();
	
	@JsonIgnore
	public String getPreviewFor(Set<String> keywords);
}
