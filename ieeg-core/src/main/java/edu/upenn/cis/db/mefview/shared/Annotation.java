/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Objects.equal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;

@GwtCompatible(serializable=true)
public class Annotation implements Serializable, Comparable<Annotation>, JsonTyped, IDetection, Previewable {
	private String type;
	private String color = null;
//	private String shortType;
	private String creator;
	private double start;
	private double end;
	private String revId;
	private String description;
	private String internalId = null;
	private ArrayList<String> channels;
	private ArrayList<INamedTimeSegment> channelInfo;
	private String layer;

	private boolean modifiedItem;
	private boolean newItem;

	public Annotation() {
		channels = new ArrayList<String>();
		channelInfo = new ArrayList<INamedTimeSegment>();
		modifiedItem = false;
		newItem = false;
	}

	public Annotation(String creator, String type, double start, double end,
			Collection<String> channels) {
		this.channels = new ArrayList<String>();
		this.creator = creator;
		this.type = type;
		this.start = start;
		this.end = end;
		this.channels.addAll(channels);
		channelInfo = new ArrayList<INamedTimeSegment>();
//		this.shortType = getShortName(type);
		modifiedItem = false;
		newItem = false;
	}

	public Annotation(String creator, String type, double start, double end,
			Collection<String> channels, boolean userCreated,
			final List<? extends INamedTimeSegment> traces) {
//		this(creator, type, //getShortName(type), 
//				start, end, channels,
//				userCreated, traces);
		this.channels = new ArrayList<String>();
		this.creator = creator;
		this.type = type;
		this.start = start;
		this.end = end;
		channelInfo = new ArrayList<INamedTimeSegment>();
		this.channels.addAll(channels);
		for (String chToAdd : channels)
			for (INamedTimeSegment t : traces)
				if (t.getLabel().equals(chToAdd))
					this.channelInfo.add(t);
//		this.shortType = shortType;
		modifiedItem = false;
		newItem = userCreated;
	}
//
//	public Annotation(String creator, String type, //String shortType,
//			double start, double end, Collection<String> channels,
//			boolean userCreated, final List<TraceInfo> traces) {
//		this.channels = new ArrayList<String>();
//		this.creator = creator;
//		this.type = type;
//		this.start = start;
//		this.end = end;
//		channelInfo = new ArrayList<TraceInfo>();
//		this.channels.addAll(channels);
//		for (String chToAdd : channels)
//			for (TraceInfo t : traces)
//				if (t.getLabel().equals(chToAdd))
//					this.channelInfo.add(t);
////		this.shortType = shortType;
//		modifiedItem = false;
//		newItem = userCreated;
//	}

	public Annotation(Annotation a2, List<String> from, List<String> to,
			final List<? extends INamedTimeSegment> traces) {
		this.channels = new ArrayList<String>();
		this.creator = a2.creator;
		this.type = a2.type;
		this.start = a2.start;
		this.end = a2.end;
		this.layer = a2.layer;
		this.color = a2.color;
		this.description = a2.description;
		channelInfo = new ArrayList<INamedTimeSegment>();
		for (String ch : a2.channels) {
			int pos = from.indexOf(ch);
			if (pos >= 0) {
				String chToAdd = to.get(pos);
				this.channels.add(chToAdd);
				for (INamedTimeSegment t : traces)
					if (t.getLabel().equals(chToAdd))
						this.channelInfo.add(t);
			}
		}
//		this.shortType = a2.shortType;
		modifiedItem = false;
		newItem = true;
	}

	public Annotation createCopy() {
		Annotation ret = new Annotation(this, getChannels(), getChannels(),
				getChannelInfo());
		
		return ret;
	}
	
	public Annotation createFullCopy() {
		Annotation ret = new Annotation(this, getChannels(), getChannels(),
				getChannelInfo());
		
		ret.setRevId(revId);
		ret.internalId = internalId;
		
		return ret;
	}

	public String toString() {
		return toJSON(false, "", null, new ArrayList<String>());
	}

	public String toJSON2(boolean atTop, String col, 
			List<AnnotationDefinition> defs, List<String> allChannels) {
		for (AnnotationDefinition def : defs) {
			if (def.getAnnotationName().equals(this.getType()))
				return toJSON(atTop, col, def, allChannels);
		}
		return toJSON(atTop, col, null, allChannels);
	}
	
	public String toJSON(boolean atTop, String col, 
			AnnotationDefinition def, List<String> allChannels) {
		String ret = "";
		String attachTop = "";
		if (atTop)
			attachTop = ",\"attachAtTop\": true";

		boolean isFirst = true;
		String name = channels.toString();
		String series = "";
		if (def == null || !def.getGlobal()) {
			for (String n: channels) {
				if (!series.isEmpty())
					series = series + ",";
				series = series + "\"" + n + "\"";
			}
		} else if (def.getGlobal()) {
			for (String n: allChannels) {
				if (!series.isEmpty())
					series = series + ",";
				series = series + "\"" + n + "\"";
			}
		}
		
		String vet = "";
		if (getLayer() != null && (getLayer().contains("(unvetted)") || getLayer().equals("Linelength") || true)) {
			vet = ",\"vet\": true";
		}
		
		if (channels.size() > 1 || (def != null	 && def.getGlobal() && allChannels.size() > 1) || series.isEmpty())
			series = "[" + series + "]";
		

		if (col == null || col.isEmpty())
			col = getColor();
		
			if (col == null)
				col = "";

			if (isFirst)
				isFirst = false;
			else
				ret = ret + ',';

			String st;
			String en;
			
//			if (com.google.gwt.core.client.GWT.isScript()){
//				st = NumberFormat.getFormat("################0").format(
//						start);
//				en = NumberFormat.getFormat("################0").format(end);
//			} else {
				st = String.valueOf((long)start);
				en = String.valueOf((long)end);
//			}
				

			if (def != null)
				GWT.log("Annotation has " + def.getNumberOfPoints() + " points and is " + (def.getGlobal() ? "global" : "local"));
//			else
//				GWT.log("Annotation is unknown");
			if (type.equals("Bookmark") || type.equals("Label") || type.equals("Time Span of Interest")) {
				final String escapedDesc = escapeQuotes(description)
;				ret = ret + "{\"series\": " + series + ",\"x\":\"" + st
						+ ((def == null || def.getNumberOfPoints() > 1) ? "\",\"x2\":\"" + en : "")
//						+ "\",\"shortText\": \"" + shortType
						+ "\",\"text\": \"" +
						escapedDesc + ":" + name + "\",\"description\": \"" + escapedDesc
						+ "\",\"cssClass\": \"annotation\"" + attachTop
						+ ",\"revId\": \"" + getRevId() +
						"\",\"internalId\": \"" + getInternalId() +
						((col.isEmpty()) ? "" : "\", \"color\": \"" + col) + 
						"\"" + vet + "}";
			} else {
				String desc = description;
				if (desc == null || desc.isEmpty() || desc.equals("null"))
					desc = type;
				
				final String escapedDesc = escapeQuotes(desc);
				final String escapedType = escapeQuotes(type);
				
				//desc = desc.replaceAll("\\\\", "\\\\\\\\");
				//desc = desc.replaceAll("\\\'", "\\\\\\\'");
				//desc = desc.replaceAll("\\\"", "\\\\\\\"");
				
				ret = ret + "{\"series\": " + series + ",\"x\":\"" + st
					+ ((def == null || def.getNumberOfPoints() > 1)	&& (long) start != (long) end ? "\",\"x2\":\"" + en : "")
//						+ "\",\"shortText\": \"" + shortType
						+ "\",\"text\": \"" +
						escapedType + ":" + name + "\",\"description\": \""
						+ escapedDesc + "\",\"cssClass\": \"annotation\""
						+ attachTop + ",\"revId\": \"" + getRevId() +
						"\",\"internalId\": \"" + getInternalId() +
						((col.isEmpty()) ? "" : "\", \"color\": \"" + col) + 
						"\"" + vet + "}";
			}
//		}
			
//			GWT.log(ret);

		return ret;
	}
	
	private static String escapeQuotes(String desc) {
		String escaped = desc.replaceAll("\\\\", "\\\\\\\\");
		escaped = escaped.replaceAll("\\\'", "\\\\\\\'");
		escaped = escaped.replaceAll("\\\"", "\\\\\\\"");
		return escaped;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public double getStart() {
		return start;
	}

	public void setStart(double start) {
		this.start = start;
	}

	public double getEnd() {
		return end;
	}

	public void setEnd(double end) {
		this.end = end;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(final String id) {
		internalId = id;
	}

	public String getRevId() {
		return revId;
	}

	public void setRevId(String revId) { 
		this.revId = revId;
	}
	
	public void setRevAndInternalId(String id) {
		this.revId = id;
		this.internalId = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getChannels() {
		return channels;
	}

	public List<INamedTimeSegment> getChannelInfo() {
		return channelInfo;
	}

	public void setChannelInfo(List<INamedTimeSegment> revIDs) {
		channelInfo.clear();
		channelInfo.addAll(revIDs);
	}

	public void setChannels(Collection<String> channels) {
		this.channels.clear();
		this.channels.addAll(channels);
	}

	public void setModified(boolean item) {
		modifiedItem = true;
	}

	public boolean isModified() {
		return modifiedItem;
	}

	public void setNew(boolean newItem) {
		this.newItem = newItem;
	}
	
	public boolean isNew() {
		return newItem;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String col) {
		color = col;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Annotation))
			return false;
		
		Annotation a = (Annotation)o;
		if (revId != null && a.revId != null)
			return revId.equals(a.revId);
		else if (revId == null && a.revId == null)
			if (internalId != null && a.internalId != null)
				return internalId.equals(a.internalId);
			else
				return false;
		else
			return (start == a.start && end == a.end);
	}
	
	public int hashCode() {
		if (revId != null)
			return revId.hashCode();
		else
			return Double.valueOf(getStart()).hashCode();
	}

	@Override
	public int compareTo(Annotation o) {
		final boolean haveRevIds = revId != null && o.revId != null;
		if (haveRevIds && revId.equals(o.revId)) {
			return 0;
		} else if (start < o.start || (start == o.start && end < o.end))
			return -1;
		else if (start == o.start && end == o.end) {
			if (haveRevIds) {
				return revId.compareTo(o.revId);
			} else if (internalId != null && o.internalId != null) {
				return internalId.compareTo(o.internalId);
			} else if (!equal(description, o.description)) {
				if (description != null && o.description != null)
					return description.compareTo(o.description);
				else
					return (description == null) ? -1 : 1;
			} else {
				return creator.compareTo(o.creator);
			}
		}

		return 1;
	}

	public static final ProvidesKey<Annotation> KEY_PROVIDER = new ProvidesKey<Annotation>() {

		public Object getKey(Annotation item) {
			return (item == null) ? null : item.getInternalId();// .getFriendlyName();
		}

	};

	public static final TextColumn<Annotation> annChannel = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			return object.getChannels().toString();
		}
	};
	public static final TextColumn<Annotation> annType = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			return object.getType();
		}
	};
	public static final TextColumn<Annotation> annStart = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			// return Double.toString(object.getStart());
			return SharedUtil.getDate((long) object.getStart());
		}
	};
	public static final TextColumn<Annotation> annDesc = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			return object.getDescription();
		}
	};
	public static final TextColumn<Annotation> annEnd = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			return SharedUtil.getDate((long) object.getEnd());
			// return Double.toString(object.getEnd());
		}
	};
	public static final TextColumn<Annotation> annCreator = new TextColumn<Annotation>() {
		@Override
		public String getValue(Annotation object) {
			return object.getCreator();
		}
	};

	  public void setSaved() {
	    this.newItem = false;
	    this.modifiedItem = false;
	    
	  }

		@JsonIgnore
	@Override
	public double getDuration() {
		return getEnd() - getStart();
	}

		@JsonIgnore
	@Override
	public int getNumericValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@JsonIgnore
	@Override
	public JsonTyped getNameOrAuxiliaryInfo() {
		return new JsonString(getType());
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		String t = type;
		if (t.length() > 0)
			t = Character.toUpperCase(t.charAt(0)) + t.substring(1);
		else
			t = type;
		return t + " annotation " + this.getDescription() + " by " + this.getCreator();
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Annotation ";
	}

	@JsonIgnore
	@Override
	public DetectionType getDetectionType() {
		return DetectionType.Annotation;
	}

	@JsonIgnore
	@Override
	public String getStringValue() {
		return getType();
	}
}
