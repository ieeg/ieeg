/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import static java.util.Collections.emptyList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.DataMarshalling;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer;
import edu.upenn.cis.db.mefview.server.block.SnapshotCatalogView.SnapshotConfig;
import edu.upenn.cis.db.mefview.server.block.SnapshotView.Block;
import edu.upenn.cis.db.mefview.server.block.SnapshotView.ChannelPos;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFDataAndPageNos;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.eeg.TimeSeriesChannel;
import edu.upenn.cis.thirdparty.RED;

public class BlockStoreServer extends TimeSeriesPageServer<TimeSeriesPage> {
	static Map<String, SnapshotView> dbCache = new ConcurrentHashMap<String, SnapshotView>();
	private final static Logger logger = LoggerFactory
	.getLogger(BlockStoreServer.class);
	static RED redDecoder = new RED();
//	Map<String,MEFPage> fullPageMap;
	
//	public int CACHE_SIZE = 512;//0;
	SnapshotCatalogView catalogView;
	SnapshotCatalogDb db;
	String base;
	
	public static class BlockChannelSpecifier extends ChannelSpecifier implements GeneralMetadata {
		private static final long serialVersionUID = 1L;
		private String fullPath;
		private BlockSnapshotSpecifier snapshot;
		
		static Map<String,VALUE_TYPE> keys;
		static {
			keys = new HashMap<String,VALUE_TYPE>();
			
			keys.put("id", VALUE_TYPE.STRING);
		}
		
		
		public BlockChannelSpecifier(BlockSnapshotSpecifier sourcePath, String subPath, String revID) {
			super(revID);
			snapshot = sourcePath;
			fullPath = sourcePath.getPath() + subPath;
		}
		
		public BlockChannelSpecifier clone() {
			BlockChannelSpecifier spec = new BlockChannelSpecifier(snapshot, fullPath, getId());
			
			spec.fullPath = fullPath;
			
			return spec;
		}


		@Override
		public String getHandle() { return fullPath; }

		@Override
		public edu.upenn.cis.db.mefview.services.SnapshotSpecifier getSnapshot() {
			return snapshot;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((getId() == null) ? 0 : getId().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof BlockChannelSpecifier))
				return false;
			BlockChannelSpecifier other = (BlockChannelSpecifier) obj;
			if (getId() == null) {
				if (other.getId() != null)
					return false;
			} else if (!getId().equals(other.getId()))
				return false;
			return true;
		}

		@Override
		public Set<String> getKeys() {
			return keys.keySet();
		}

		@Override
		public VALUE_TYPE getValueType(String key) {
			return keys.get(key);
		}

		@Override
		public GeneralMetadata getMetadataValue(String key) {
			return null;
		}

		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			return null;
		}


		public GeneralMetadata getParent() {
			return snapshot;
		}

		@Override
		public void setParent(GeneralMetadata p) {
			if (p instanceof BlockSnapshotSpecifier)
				snapshot = (BlockSnapshotSpecifier)p;
		}


		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Timeseries.name();
		}

	}
	
	public static class BlockSnapshotSpecifier extends SnapshotSpecifier implements GeneralMetadata {
		private String revID;
		private String fullPath;
		
		GeneralMetadata parent;
		static Map<String,VALUE_TYPE> keys;
		static {
			keys = new HashMap<String,VALUE_TYPE>();
			
			keys.put("id", VALUE_TYPE.STRING);
		}

		public BlockSnapshotSpecifier(String sourcePath, String rid) {
			fullPath = sourcePath;
			revID = rid;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fullPath == null) ? 0 : fullPath.hashCode());
			result = prime * result + ((revID == null) ? 0 : revID.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof BlockSnapshotSpecifier)) {
				return false;
			}
			BlockSnapshotSpecifier other = (BlockSnapshotSpecifier) obj;
			if (fullPath == null) {
				if (other.fullPath != null) {
					return false;
				}
			} else if (!fullPath.equals(other.fullPath)) {
				return false;
			}
			if (revID == null) {
				if (other.revID != null) {
					return false;
				}
			} else if (!revID.equals(other.revID)) {
				return false;
			}
			return true;
		}

		public String getPath() {
			return fullPath;
		}

		public String getRevID() {
			return revID;
		}

		@Override
		public Set<String> getKeys() {
			return keys.keySet();
		}

		@Override
		public VALUE_TYPE getValueType(String key) {
			return keys.get(key);
		}

		@Override
		public GeneralMetadata getMetadataValue(String key) {
			return null;
		}

		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			return null;
		}

		@Override
		public GeneralMetadata getParent() {
			return parent;
		}

		@Override
		public void setParent(GeneralMetadata p) {
			parent = p;
		}

		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Snapshot.name();
		}
	};
	
	
	public BlockStoreServer(SnapshotCatalogDb db, SnapshotCatalogView cv, String basePath) {
		this.db = db;
		base = basePath;
		this.catalogView = cv;
		
//		fullPageMap = Collections.synchronizedMap(new LruCache<String,MEFPage>(CACHE_SIZE));
	}

	public SnapshotView getSnapshotInfo(SnapshotConfig config, String snapshotName) {
		
		SnapshotView ret = dbCache.get(snapshotName);
		if (ret == null)
			ret = new SnapshotView(config, db, base, snapshotName);
		
		return ret;
	}
	
	public void close() {
		try {
			
		} catch (Exception e) {
			
		}
	}

//	private ArrayList<ArrayList<TimeSeriesData>>
//	requestMultiChannelReadBlock(final SnapshotView view,
//			final List<String> paths, final List<String> revIds,
//			long startTime, long endTime,
//			double samplingPeriod, final List<FilterSpec> filters)
//			throws InterruptedException, ExecutionException {
//		ArrayList<ArrayList<TimeSeriesData>> ret = new ArrayList<ArrayList<TimeSeriesData>>(
//				paths.size());
//
//		int i = 0;
//		try {
//			SnapshotHeader header = view.getHeaderMap().get("all");
//			
//			List<MefHeader2> mef = header.getHeaders();
//			
//			// Define a work buffer for all of the filter ops
//			FilterManager.FilterBuffer buf = new FilterManager.FilterBuffer((int)((endTime - startTime) / samplingPeriod + 1));
//
//			for (String path : paths) {
//				ArrayList<TimeSeriesData> ret2 = requestReadAtSampleRate(view, header, mef,
//						path, startTime, endTime, samplingPeriod,
//						filters.get(i), buf);
//
//				for (TimeSeriesData ts : ret2) {
//					ts.setRevId(revIds.get(i));
//				}
//
//				ret.add(ret2);
//				i++;
//			}
//		} catch (IndexOutOfBoundsException e) {
//			System.err.println(paths.size());
//			throw(e);
//		}
//
//		return ret;
//	}
//	
//	private ArrayList<ArrayList<TimeSeriesData>>
//	requestMultiChannelReadBlockExactMultiple(final SnapshotView view,
//			final List<String> paths, final List<String> revIds,
//			long startTime, long endTime,
//			int scaleFactor)
//			throws InterruptedException, ExecutionException {
//		ArrayList<ArrayList<TimeSeriesData>> ret = new ArrayList<ArrayList<TimeSeriesData>>(
//				paths.size());
//
//		int i = 0;
//		try {
//			SnapshotHeader header = view.getHeaderMap().get("all");
//			
//			List<MefHeader2> mef = header.getHeaders();
//			
//			for (String path : paths) {
//				ArrayList<TimeSeriesData> ret2 = requestReadExactMultiple(view, header, mef,
//						path, startTime, endTime, scaleFactor);
//
//				for (TimeSeriesData ts : ret2) {
//					ts.setRevId(revIds.get(i));
//				}
//
//				ret.add(ret2);
//				i++;
//			}
//		} catch (IndexOutOfBoundsException e) {
//			System.err.println(paths.size());
//			throw(e);
//		}
//
//		return ret;
//	}
//	
//	public ArrayList<ArrayList<TimeSeriesData>>
//	requestMultiChannelRead(final SnapshotView view,
//			final List<String> paths, final List<String> revIds,
//			long startTime, long endTime,
//			double samplingPeriod, final List<FilterSpec> filters)
//			throws InterruptedException, ExecutionException {
//		ArrayList<ArrayList<TimeSeriesData>> ret = new ArrayList<ArrayList<TimeSeriesData>>();
//		
//		// Scan and downsample the page
//		ret = requestMultiChannelReadBlock(
//				view, paths, revIds, startTime, endTime,
//				samplingPeriod, filters);
//		
//		return ret;
//	}
//	
//	public ArrayList<ArrayList<TimeSeriesData>>
//	requestMultiChannelReadExactMultiple(final SnapshotView view,
//			final List<String> paths, final List<String> revIds,
//			long startTime, long endTime,
//			int scaleFactor)
//			throws InterruptedException, ExecutionException {
//		ArrayList<ArrayList<TimeSeriesData>> ret = new ArrayList<ArrayList<TimeSeriesData>>();
//		
//		// Scan and downsample the page
//		ret = requestMultiChannelReadBlockExactMultiple(
//				view, paths, revIds, startTime, endTime,
//				scaleFactor);
//		
//		return ret;
//	}
//	
//	/**
//	 * Request a read while linearly downsampling. Applies a low-pass filter
//	 * first.
//	 * 
//	 * @param path
//	 * @param startTime
//	 * @param endTime
//	 * @param samplingPeriod
//	 * @return
//	 * @throws InterruptedException
//	 * @throws ExecutionException
//	 */
//	public ArrayList<TimeSeriesData> requestReadAtSampleRate(
//			final SnapshotView view,
//			final SnapshotHeader header, List<MefHeader2> mef,
//			final String path, long startTimeOffset,
//			long endTimeOffset,
//			double samplingPeriod, final FilterSpec filter,
//			final FilterManager.FilterBuffer buf)
//			throws InterruptedException, ExecutionException {
//		return requestReadInternal(view, header, mef,
//				path, startTimeOffset, endTimeOffset, samplingPeriod, 0,
//				filter, buf, false);
//	}
//
//	public ArrayList<TimeSeriesData> requestReadExactMultiple(
//			final SnapshotView view,
//			final SnapshotHeader header, List<MefHeader2> mef,
//			final String path, long startTimeOffset,
//			long endTimeOffset,
//			int scaleFactor)
//			throws InterruptedException, ExecutionException {
//		return requestReadInternal(view, header, mef,
//				path, startTimeOffset, endTimeOffset, 0, scaleFactor,
//				null, null, true);
//	}
//	
//	private ArrayList<TimeSeriesData> requestReadInternal(
//			final SnapshotView view,
//			final SnapshotHeader header, List<MefHeader2> mef,
//			final String path, long startTimeOffset,
//			long endTimeOffset,
//			double samplingPeriod, int factor, final FilterSpec filter,
//			final FilterManager.FilterBuffer buf, boolean isExact)
//			throws InterruptedException, ExecutionException {
//		
//	
//		int inx = header.getChannels().indexOf(path);
//		
//		if (inx < 0) {
//			throw new RuntimeException("Unable to find channel " + path + " in " + header.getChannels());
//		}
//		
//		MefHeader2 meh = mef.get(inx);
//		
//		final String M = "requestReadAtSampleRate()";
//		long startHandler = System.nanoTime();
//		long contention = System.nanoTime() - startHandler;
//		long nextHandler = System.nanoTime();
//
//		logger.trace(M + ":  * " + path + " requesting read from offset "
//				+ startTimeOffset + " to " + endTimeOffset + " @"
//				+ samplingPeriod);
//
//		long startTime = meh.getStartTime() + startTimeOffset;
//		long endTime = startTime + (endTimeOffset - startTimeOffset);// endTimeOffset
//																		// +
//		logger.trace(M + ":    " + path + " converted to read from " + startTime
//				+ " to " + endTime + "(" + (endTime - startTime) + ") @"
//				+ samplingPeriod);
//
//		double freq = meh.getSampleRate();
//		double scale = meh.getVoltageConversionFactor();
//		double startT = meh.getStartTime();
//		double endT = meh.getEndTime();
//		double startReq = startTime - samplingPeriod;
//		double endReq = endTime - samplingPeriod;
//
//		if (startReq < startT)
//			startReq = startT;
//		if (startReq > endT)
//			startReq = endT;
//		if (endReq > endT)
//			endReq = endT;
//		
//		MEFPage[] pageList = null;
//
//		try {
//			pageList = getPages(view, header, (short)inx, header.getTimeIndex(inx), 
//					(long) (startReq), (long) (endReq));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			logger.error(M + ": Caught, ignoring", e);
//		}
//
//		logger.trace(M + ": Retrieved " + (pageList.length) + " pages");
//
//		long readPages = System.nanoTime() - nextHandler;
//		nextHandler = System.nanoTime();
//		
//		ArrayList<TimeSeriesData> results = new ArrayList<TimeSeriesData>();
//		if (!isExact)
//			results = FilterManager.downsampleAndFilter(startTime, endTime, freq, 
//					startTimeOffset, samplingPeriod, scale, pageList,
//					filter, path, false, buf);
//		else
//			results = FilterManager.integerDownsample(startTime, endTime, freq, 
//					startTimeOffset, factor, scale, pageList,
//					path, false);
//
//		long createSpan = System.nanoTime() - nextHandler;
//		nextHandler = System.nanoTime();
//
//		logger.trace(M + ": Stats: contention = " + contention + 
//				"; readPages " + readPages + "; createSpan "
//				+ createSpan);
//
//		return results;
//	}
	

//	public PageReader.PageList getPages(MefHeader2 header, long[] timeIndex, 
	public static TimeSeriesPage[] getPages(SnapshotView view, SnapshotHeader hdr, short inx,
			long[] timeIndex,
			long startTime, long endTime) throws IOException {
		
		TimeSeriesChannel header = hdr.getHeaders().get(inx);

		long st = startTime;//(long)(startTime * 1000);
		long et = endTime;//(long)(endTime * 1000);
		
		double frequency = hdr.getFrequency();
		
		if (st > header.getRecordingEndTime())
			throw new IOException("Request for time past end");
		if (st < header.getRecordingStartTime()) {
			st = header.getRecordingEndTime();
			et = st + endTime - startTime;
		}

		if (et > header.getRecordingEndTime())
			et = header.getRecordingEndTime();
		
		// Find start index
		int i = Arrays.binarySearch(timeIndex, st);
		if (i < 0)
			i = -i - 2;
		for (; i < timeIndex.length - 1; i++)
			if (timeIndex[i+1] > st)
				break;
		
		// Period in usec
		double period = 1000000. / frequency;
		
		// Find end index
		int j = Arrays.binarySearch(timeIndex, et); // i
		if (j < 0)
			j = -j - 1;//2;

		for (; j < timeIndex.length - 1; j++)
			if (timeIndex[j+1] > et)
				break;
		
		if (j > timeIndex.length - 1)
			j = timeIndex.length - 1;
		
		Entry<ChannelPos,Block> test = view.getBlockEntrySet().iterator().next();
		
		Block t1 = test.getValue();
		
		TimeSeriesPage[] ret = new TimeSeriesPage[j-i+1];//ArrayList<MEFPage>();
		for (int x = i; x <= j; x++) {
			ChannelPos pos = new ChannelPos(inx, timeIndex[x]);
			
			Block block = view.getBlockMap().get(pos);
			
			TimeSeriesPage p = new TimeSeriesPage();
//			ret.add(p);
			ret[x-i] = p;
	
			p.timeStart = timeIndex[x];
			
			try {
				if (hdr.isCompressed())
					synchronized (redDecoder) {
						p.values = redDecoder.decode(block.array, 
								0, block.array.length);//buffer.length);
					}
				else {
					int len = block.array.length / 4;
					p.values = new int[len];
					int bp = 0;
					for (int v = 0; v < len; v++) {
						p.values[v] = (block.array[bp] & 0xff) | 
							((block.array[bp+1] << 8) & 0xff00) |
							((block.array[bp+2] << 16) & 0xff0000) | 
							((block.array[bp+3] << 24) & 0xff000000);
						bp += 4;
					}
				}
				
			} catch (RuntimeException rt) {
				logger.error("Error in reading time " + timeIndex[x] +")");
				rt.printStackTrace();
				throw rt;
			}
	
			// Use # samples to determine end time
			p.timeEnd = (long)(p.timeStart + period * p.values.length);
		}
		
		return ret;
	}

	@Override
	public long getNumSamples(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getNumberOfSamples();
	}

	@Override
	public long getEndUutc(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getRecordingEndTime();
	}

	@Override
	public int getMaxSampleValue(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getMaximumDataValue();
	}

	@Override
	public int getMinSampleValue(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getMinimumDataValue();
	}

	@Override
	public long getNumIndexEntries(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getTimeIndices().length;
	}

	@Override
	public double getSampleRate(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getSamplingFrequency();
	}

	@Override
	public long getStartUutc(ChannelSpecifier path) {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getRecordingStartTime();
	}

	@Override
	public String getInstitution(ChannelSpecifier path) throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getInstitution();
	}

	@Override
	public String getAcquisitionSystem(ChannelSpecifier path)
			throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getAcquisitionSystem();
	}

	@Override
	public String getChannelName(ChannelSpecifier path) throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getChannelName();
	}

	@Override
	public String getChannelComments(ChannelSpecifier path)
			throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getChannelComments();
	}

	@Override
	public String getSubjectId(ChannelSpecifier path) throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getSubjectId();
	}

	@Override
	public double getVoltageConversionFactor(ChannelSpecifier path)
			throws IllegalArgumentException {
		BSHeader header = getHeader(path);
		return header.header.getHeaders().get(0).getVoltageConversionFactor();
	}
	
	public static class BSHeader {
		public SnapshotView view;
		public SnapshotHeader header;
		
		public BSHeader(SnapshotView view, SnapshotHeader header) {
			this.view = view;
			this.header = header;
		}
	}
	
	protected BSHeader getBestHeader(ChannelSpecifier path, PostProcessor postProc) {
		SnapshotConfig config = catalogView.getSnapshotMap().get(new 
				SnapshotCatalogView.SnapshotKey(path.getHandle()));

		final String M = "BlockStoreServer.getBestHeader()"; 
		double frequency = postProc.getSampleRate();

		if (config != null && !config.getFrequencies().isEmpty()) {
			//		String snapshotName = config.getDatabases().iterator().next();

			logger.trace(M + ": Found a B+ Tree for " + path.getHandle() + " at " + config.getFrequencies().iterator().next());

			double min = Double.MAX_VALUE;
			for (double d: config.getFrequencies()) {
				if (min > d && min >= frequency)
					min = d;
			}

			if (min != Double.MAX_VALUE) {
				String snapshotName = config.getDatabaseFor(min);

				SnapshotView view = getSnapshotInfo(config, snapshotName);

				SnapshotHeader header = view.getHeaderMap().get("all");
				
				return new BSHeader(view, header);
			}
		}
		return null;
	}

	protected BSHeader getHeader(ChannelSpecifier path) {
		SnapshotConfig config = catalogView.getSnapshotMap().get(new 
				SnapshotCatalogView.SnapshotKey(path.getHandle()));

		final String M = "BlockStoreServer.getBestHeader()"; 

		if (config != null && !config.getFrequencies().isEmpty()) {
			//		String snapshotName = config.getDatabases().iterator().next();

			logger.trace(M + ": Found a B+ Tree for " + path + " at " + config.getFrequencies().iterator().next());

			double max = Double.MIN_VALUE;
			for (double d: config.getFrequencies()) {
				if (max < d)
					max = d;
			}

			if (max != Double.MIN_VALUE) {
				String snapshotName = config.getDatabaseFor(max);

				SnapshotView view = getSnapshotInfo(config, snapshotName);

				SnapshotHeader header = view.getHeaderMap().get("all");
				
				return new BSHeader(view, header);
			}
		}
		return null;
	}

	@Override
	public ArrayList<TimeSeriesData> requestRead(
//			SnapshotSpecifier source,
			ChannelSpecifier path,
			long startTimeOffset,
			long endTimeOffset,
			edu.upenn.cis.db.mefview.services.PostProcessor postProc, 
			User user, 
			@Nullable SessionToken sessionToken)
			throws InterruptedException, ExecutionException, ServerTimeoutException {
		
		BSHeader res = getBestHeader(path, postProc);

		int inx = -1;//res.header.getChannels().indexOf(path);
		
		if (inx < 0) {
			throw new RuntimeException("Unable to find channel " + path + " in " + res.header.getChannels());
		}
		
		final String M = "BlockStoreServer.requestRead()";
		List<TimeSeriesChannel> mef = res.header.getHeaders();
		TimeSeriesChannel meh = mef.get(inx);
		
		double samplingPeriod = 1.E6 / postProc.getSampleRate();
		
		long startHandler = System.nanoTime();
		long contention = System.nanoTime() - startHandler;
		long nextHandler = System.nanoTime();

		logger.trace(M + ":  * " + path + " requesting read from offset "
				+ startTimeOffset + " to " + endTimeOffset + " @"
				+ samplingPeriod);

		long startTime = meh.getRecordingStartTime() + startTimeOffset;
		long endTime = startTime + (endTimeOffset - startTimeOffset);// endTimeOffset
																		// +
		logger.trace(M + ":    " + path + " converted to read from " + startTime
				+ " to " + endTime + "(" + (endTime - startTime) + ") @"
				+ samplingPeriod);

		double freq = meh.getSamplingFrequency();
		double scale = meh.getVoltageConversionFactor();
		double startT = meh.getRecordingStartTime();
		double endT = meh.getRecordingEndTime();
		double startReq = startTime - samplingPeriod;
		double endReq = endTime - samplingPeriod;

		if (startReq < startT)
			startReq = startT;
		if (startReq > endT)
			startReq = endT;
		if (endReq > endT)
			endReq = endT;
		
		TimeSeriesPage[] pageList = null;

		try {
			pageList = getPages(res.view, res.header, (short)inx, res.header.getTimeIndex(inx), 
					(long) (startReq), (long) (endReq));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(M + ": Caught, ignoring", e);
		}

		logger.trace(M + ": Retrieved " + (pageList.length) + " pages");

		long readPages = System.nanoTime() - nextHandler;
		nextHandler = System.nanoTime();
		
		ArrayList<TimeSeriesData> results = new ArrayList<TimeSeriesData>();
		results = postProc.process(startTime, endTime, freq, startTimeOffset, 
				scale, pageList, path, false, 0, 0);

		long createSpan = System.nanoTime() - nextHandler;
		nextHandler = System.nanoTime();

		logger.trace(M + ": Stats: contention = " + contention + 
				"; readPages " + readPages + "; createSpan "
				+ createSpan);

		return results;
	}

	@Override
	protected List<List<TimeSeriesData>> requestMultiChannelReadBlock(
//			SnapshotSpecifier source,
			List<? extends ChannelSpecifier> paths,
//			List<String> revIds,
			long startTime,
			long endTime,
			edu.upenn.cis.db.mefview.services.PostProcessor[] postProc)
			throws InterruptedException, ExecutionException, ServerTimeoutException {
		List<List<TimeSeriesData>> ret = new ArrayList<List<TimeSeriesData>>(
				paths.size());

		try {
			int i = 0;

			for (ChannelSpecifier path : paths) {
				List<TimeSeriesData> ret2 = requestRead(//source, 
						paths.get(i), startTime, endTime, postProc[i], null, null);

				for (TimeSeriesData ts : ret2) {
					ts.setRevId(path.getId());
				}

				ret.add(ret2);
				i++;
			}
		} catch (IndexOutOfBoundsException e) {
			System.err.println(paths.size());
			throw(e);
		}

		return ret;
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public edu.upenn.cis.db.mefview.services.SnapshotSpecifier getSnapshotSpecifier(
			User user, String revId) throws AuthorizationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public edu.upenn.cis.db.mefview.services.ChannelSpecifier getChannelSpecifier(
			User user,
			edu.upenn.cis.db.mefview.services.SnapshotSpecifier snapshot,
			String revId,
			double samplingPeriod) throws AuthorizationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getPageServerState() {
		return emptyList();
	}

	@Override
	public List<MEFDataAndPageNos> requestMultiChannelReadRed(
			List<? extends ChannelSpecifier> chSpecs,
			long startTimeOffset, long endTimeOffset, User user) throws StaleMEFException, ServerTimeoutException {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getDataVersion(ChannelSpecifier chSpec) {
		throw new UnsupportedOperationException();
	}

	@Override
	public GeneralMetadata getMetadata(ChannelSpecifier channelSpec) {
		return new TraceInfo(
				//((GeneralMetadata)channelSpec).getParent(),
				null,
				0,//getStartTime(channelSpec), 
				getEndUutc(channelSpec) - getStartUutc(channelSpec), 
				getSampleRate(channelSpec),
				getVoltageConversionFactor(channelSpec), 
				getMinSampleValue(channelSpec), 
				getMaxSampleValue(channelSpec),
				getInstitution(channelSpec), 
				getAcquisitionSystem(channelSpec), 
				getChannelComments(channelSpec),
				getSubjectId(channelSpec), 
				getChannelName(channelSpec), 
				channelSpec.getId());
	}

	@Override
	public InputStream getData(String key, String eTag, Long startByteOffset,
			Long endByteOffset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getSizeBytes(ChannelSpecifier channelSpecifier) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, ChannelSpecifier> getChannelSpecifiersFromSegments(User user, SnapshotSpecifier dsSpec,
			Set<INamedTimeSegment> tsIds, double samplingPeriod) throws AuthorizationException {
		return getChannelSpecifiers(user, dsSpec, DataMarshalling.getTimeSegmentIds(tsIds), samplingPeriod);
	}

//	@Override
//	public MEFPage requestPage(String doc, long pageId, int resolution)
//			throws InterruptedException, ExecutionException {
//		
//		SnapshotConfig config = catalogView.getSnapshotMap().get(new 
//				SnapshotCatalogView.SnapshotKey(doc));
//		
//		final String M = "BlockStoreServer.requestPage()";
//		
//		double frequency;
//		
//		if (config != null && !config.getFrequencies().isEmpty()) {
////				String snapshotName = config.getDatabases().iterator().next();
//			
//			logger.trace(M + ": Found a B+ Tree for " + doc + " at " + config.getFrequencies().iterator().next());
//
//			double min = Double.MAX_VALUE;
//			for (double d: config.getFrequencies()) {
//				if (min > d && min >= frequency)
//					min = d;
//			}
//			
//			if (min != Double.MAX_VALUE) {
//				String snapshotName = config.getDatabaseFor(min);
//				
//				SnapshotView view = getSnapshotInfo(config, snapshotName);
//
//				SnapshotHeader header = view.getHeaderMap().get("all");
//		
//				List<MefHeader2> mef = header.getHeaders();
//			}
//		}
//		// TODO Auto-generated method stub
//		return null;
//	}
}
