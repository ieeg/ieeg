/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.common.base.Throwables;
import com.sleepycat.je.DatabaseException;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.Decimate;
import edu.upenn.cis.db.mefview.server.block.BlockStoreServer;
import edu.upenn.cis.db.mefview.server.block.SnapshotCatalogDb;
import edu.upenn.cis.db.mefview.server.block.SnapshotCatalogView;
import edu.upenn.cis.db.mefview.server.block.SnapshotCatalogView.SnapshotConfig;
import edu.upenn.cis.db.mefview.server.block.SnapshotHeader;
import edu.upenn.cis.db.mefview.server.block.SnapshotView;
import edu.upenn.cis.db.mefview.server.block.SnapshotView.ChannelPos;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.filters.TimeSeriesFilterButter2;
import edu.upenn.cis.eeg.mef.MEFObjectReader;
import edu.upenn.cis.eeg.mef.MEFReader;

public class MEFIndexer {
	static MEFPageServer mps;
	
	static SnapshotCatalogDb catalogDb = null;
	static SnapshotCatalogView catalogView = null;
	
	String base;
	String path = "G:\\viewer\\";
	String prefix = "Mayo_IEED_data/Mayo_IEED_005/IC_005/";

	public MEFIndexer(String basePath) throws DatabaseException, FileNotFoundException {
		base = basePath;
		if (catalogDb == null) {
			catalogDb = new SnapshotCatalogDb(base);
			
			catalogView = new SnapshotCatalogView(catalogDb, true);
			
			mps = new MEFPageServer(null, path + prefix, 1, false, false, 100);
		}
	}

	public MEFIndexer(String basePath, MEFPageServer pages) throws DatabaseException, FileNotFoundException {
		base = basePath;
		if (catalogDb == null) {
			catalogDb = new SnapshotCatalogDb(base);
			
			catalogView = new SnapshotCatalogView(catalogDb, true);
			
			mps = pages;
		}
	}


	public static void main(String[] args) throws DatabaseException, IOException, InterruptedException, ExecutionException, AuthorizationException {
		MEFIndexer index = new MEFIndexer("index");
	
		index.run();
		
		MEFIndexer.close();
	}
	
	public void dump(String snapshot) {
		SnapshotConfig newConfig;
		
		newConfig = catalogView.getSnapshotMap().get(new SnapshotCatalogView.SnapshotKey(snapshot));
		
		if (newConfig != null) {
			System.out.println("Databases: ");
			for (String str: newConfig.getDatabases())
				System.out.println(str);
			
			System.out.println("Frequencies: ");
			for (Double fr: newConfig.getFrequencies())
				System.out.println(fr);
		} else
			System.out.println("Error: null configuration");
	}
	
	public void run() throws IOException, InterruptedException, ExecutionException, AuthorizationException {
		String[] names = {"LTD1", "LTD2", "LTD3", "LTD4", 
				"LTD5", "LTD6", "LTD7", "LTD8",
				"RTD1", "RTD2", "RTD3", "RTD4", 
				"RTD5", "RTD6", "RTD7", "RTD8"};
		
		List<MEFReader> reader = new ArrayList<MEFReader>();
		
		List<String> channels = new ArrayList<String>();
		
//		String path = "G:\\viewer\\";
//		String prefix = "Mayo_IEED_data/Mayo_IEED_005/IC_005/";

		for (String str: names) {
			channels.add(prefix + str + ".mef");
			reader.add(new MEFReader(path + prefix + str + ".mef"));
		}
		
//		double baseFreq = 499.907;
		double freq = 100;
		
//		FilterSpec filter = new FilterSpec(FilterSpec.LOWPASS_FILTER, 3, 0,
//				Math.min(baseFreq / 2, freq / 2), 0, 0);
		
		String snapshot = "3cf5e56c-8320-11e0-8800-0015c5e207d6"; 
		crawlDownsampled(snapshot, path, channels, freq, (long)60E6);
//		crawl(snapshot, channels, reader, 499.907);

		SnapshotConfig newConfig;
		
		newConfig = catalogView.getSnapshotMap().get(new SnapshotCatalogView.SnapshotKey(snapshot));
		
		if (newConfig == null) {
			System.out.println("Error: null configuration");
		
//			if (newConfig == null) {
//				newConfig = new SnapshotConfig();
//			}
//			
//			newConfig.setChannels(channels);
//			newConfig.addDatabase(freq, snapshot + "-" + freq);
//			catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
		} else {
			newConfig.setChannels(channels);
			catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
			dump(snapshot);
		}
	}
	
	public void run(String snapshot, List<String> channels, double targetFreq, double length) 
	throws IOException, InterruptedException, ExecutionException, AuthorizationException {
		List<MEFReader> reader = new ArrayList<MEFReader>();
		
		for (String str: channels) {
			reader.add(new MEFReader(base + str));
		}
		
		double freq = 100;
		
		crawlDownsampled(snapshot, base, channels, freq, (long)60E6);
//		crawl(snapshot, channels, reader, 499.907);

		SnapshotConfig newConfig;
		
		newConfig = catalogView.getSnapshotMap().get(new SnapshotCatalogView.SnapshotKey(snapshot));
		
		if (newConfig == null) {
			System.out.println("Error: null configuration");
		
//			if (newConfig == null) {
//				newConfig = new SnapshotConfig();
//			}
//			
//			newConfig.setChannels(channels);
//			newConfig.addDatabase(freq, snapshot + "-" + freq);
//			catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
		} else {
			newConfig.setChannels(channels);
			catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
			dump(snapshot);
		}
	}
	
	public static void close() {
		if (catalogDb != null)
			catalogDb.close();
	}
	
	public void crawl(String snapshot, List<String> channels, List<MEFReader> readers, double frequency) throws IOException {
		SnapshotConfig newConfig;
		
		newConfig = catalogView.getSnapshotMap().get(new SnapshotCatalogView.SnapshotKey(snapshot));
		
		if (newConfig == null) {
			newConfig = new SnapshotConfig();
		
			newConfig.setChannels(channels);
		}
		
		String nam = snapshot + "-" + frequency;
		newConfig.addDatabase(frequency, nam);
		
		SnapshotView snv = new SnapshotView(newConfig, catalogDb, base, nam);
		
		SnapshotHeader snh = new SnapshotHeader(false, channels, frequency);
		
		int i = 0;
		for (MEFReader r: readers) {
			int count = 0;
			System.out.println("Indexing: " + r.getChannelName() + " for " + r.getTimeIndex().length);
			
//			int len = r.getTimeIndex().length;
			for (long key: r.getTimeIndex()) {
				//ArrayList<MEFReader.MEFPage> 
				TimeSeriesPage[] pl = r.readPages(i, 1, true);
				
				SnapshotView.Block b = new SnapshotView.Block();
//				b.end = (int)(pl.get(0).timeEnd - pl.get(0).timeStart);
				
				// Compact these back.  They were expanded from bytes!!!

				int[] ar = pl[0].values;
				b.array = new byte[ar.length];//(ar.length + 3) / 4];
				for (int x = 0; x < ar.length; x++)
					b.array[x] = (byte)ar[x];
//				int x = 0;
//				for (int p = 0; p < ar.length; p+= 4) {
//					int first = ar[p];
//					int second = 0;
//					int third = 0;
//					int fourth = 0;
//					
//					if (p+1 < ar.length)
//						second = (ar[p+1] << 8) & 0xff00;
//					if (p+2 < ar.length)
//						third = (ar[p+2] << 16) & 0xff0000;
//					if (p+3 < ar.length)
//						fourth = (ar[p+3] << 24) & 0xff000000;
//					
//					b.array[x++] = first | second | third | fourth; 
//				}
				
//				b.array = pl.get(0).values;
				
				ChannelPos cp = new ChannelPos((short)i, (key - r.getStartTime()));
				snv.getBlockMap().put(cp, b);
				count++;
				if (count % 10000 == 0)
					System.out.print(".");
			}
			System.out.println("\nIndexed: " + r.getChannelName() + " for " + count + " pages out of " + r.getTimeIndex().length);
			
			snh.setTimeIndex(i, r.getTimeIndex());
			snh.getHeaders().add(r.getHeader());
			
			i++;
		}
		snh.setIsCompressed(true);
		snv.getHeaderMap().put("all", snh);
		snv.close();
		catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
	}
	
	public void crawlDownsampled(String snapshot, String path, List<String> channels,  
			double frequency, /*FilterSpec filter,*/ long interval) throws IOException, 
			InterruptedException, ExecutionException, AuthorizationException {
		SnapshotConfig newConfig;
		
		newConfig = catalogView.getSnapshotMap().get(new SnapshotCatalogView.SnapshotKey(snapshot));
		
		if (newConfig == null) {
			newConfig = new SnapshotConfig();
		
			newConfig.setChannels(channels);
		}
		
		newConfig.setChannels(channels);
		
		String nam = snapshot + "-" + frequency;
		newConfig.addDatabase(frequency, nam);
		
		SnapshotView snv = new SnapshotView(newConfig, catalogDb, base, nam);
		
		int i = 0;
		double samplingPeriod = 1.E6 / frequency;
		SnapshotHeader snh = new SnapshotHeader(false, channels, frequency);
		
		User user = null;
		
		SnapshotSpecifier ss = mps.getSnapshotSpecifier(user, snapshot);
		
		for (String channel: channels) {
			int count = 0;
//			String p = path + channel;
			ChannelSpecifier p = mps.getChannelSpecifier(user, ss, channel, 0);
			
			MEFObjectReader r = mps.getReader(p);
			
			double baseFreq = r.getSampleRate();
			
			FilterSpec filter = new FilterSpec(TimeSeriesFilterButter2.NAME, FilterSpec.LOWPASS_FILTER, 3, 0,
					Math.min(baseFreq / 2, frequency / 2), 0, 0);

			long duration = r.getEndTime() - r.getStartTime();
			
			System.out.println("Indexing: " + r.getChannelName() + " for " + duration);
			snh.getHeaders().add(r.getHeader());
			
			FilterManager.FilterBuffer buf = new FilterManager.FilterBuffer((int)(interval / samplingPeriod + 1));
			
			PostProcessor ds = new Decimate(1.E6 / samplingPeriod, filter, buf, false);

			List<Long> indices = new ArrayList<Long>();
			for (long pos = 0; pos < duration; pos += interval) {
				ArrayList<TimeSeriesData> result;
				try {
					result = mps.requestRead(//path, channel, 
							p, pos, pos + interval, 
							ds, null, null);
				} catch (ServerTimeoutException e) {
					throw Throwables.propagate(e);
				}
			
				SnapshotView.Block b = new SnapshotView.Block();
				if (result.size() != 1)
					throw new RuntimeException("Unexpected: non-singular result");
				
				int[] ar = result.get(0).getSeries();
				b.array = new byte[ar.length * 4];
//				b.array = result.get(0).getSeries();
				
				// Set null to be MIN_VALUE.  If there's already a MIN_VALUE incr by 1
				boolean nonGap = false;
				int bp = 0;
				for (int x = 0; x < ar.length; x++) {
					if (result.get(0).isInGap(x))
//						b.array[x] = Integer.MIN_VALUE;
						ar[x] = Integer.MIN_VALUE;
					else if (ar[x] == Integer.MIN_VALUE) {
						ar[x] = Integer.MIN_VALUE + 1;
						nonGap = true;
					} else
						nonGap = true;

					b.array[bp++] = (byte)(ar[x] & 0xff);
					b.array[bp++] = (byte)((ar[x] >> 8) & 0xff);
					b.array[bp++] = (byte)((ar[x] >> 16) & 0xff);
					b.array[bp++] = (byte)((ar[x] >> 24) & 0xff);
					
					bp -= 4;
					int temp = (b.array[bp] & 0xff) | 
					((b.array[bp+1] << 8) & 0xff00) |
					((b.array[bp+2] << 16) & 0xff0000) | 
					((b.array[bp+3] << 24) & 0xff000000);
					
					if (temp != ar[x])
						throw new RuntimeException("Oops!");
					bp += 4;
					
				}
				
				// Skip storage if it's only gaps
				if (!nonGap)
					continue;
				
				ChannelPos cp = new ChannelPos((short)i, (pos + r.getStartTime()));
				snv.getBlockMap().put(cp, b);
				indices.add(pos + r.getStartTime());
				
				// Test
				long[] inx = new long[indices.size()];
				for (int l = 0; l < inx.length; l++)
					inx[l] = indices.get(l);
				
				TimeSeriesPage[] stored = BlockStoreServer.getPages(snv, snh, (short)i, inx,
						(long)pos + r.getStartTime(), (long)(pos + r.getStartTime() +
						samplingPeriod * ar.length));
				
				if (ar.length != stored[0].values.length)
					throw new RuntimeException("Oops");
				for (int l = 0; l < stored[0].values.length; l++)
					if (stored[0].values[l] != ar[l])
						throw new RuntimeException("Oops");
				
				count++;
				if (count % 10000 == 0)
					System.out.print(".");
			}
			System.out.println("\nIndexed: " + r.getChannelName() + " for " + count + " pages");
			
			long[] inx = new long[indices.size()];
			for (int l = 0; l < inx.length; l++)
				inx[l] = indices.get(l);
			
			snh.setTimeIndex(i, inx);
			
			i++;
		}
		snh.setIsCompressed(false);
		snv.getHeaderMap().put("all", snh);
		snv.close();
		catalogView.getSnapshotMap().put(new SnapshotCatalogView.SnapshotKey(snapshot), newConfig);
	}
	
}
