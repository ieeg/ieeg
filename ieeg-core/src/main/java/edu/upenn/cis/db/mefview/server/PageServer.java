/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

public abstract class PageServer<Source,PageData> {
	static int misses = 0;
	static int hits = 0;
	public static int DEFAULT_CACHE = 512;//0;
	
//	Map<Long,PageData> pageMap;
	
	public static class PageID<Source> implements Serializable {
		private static final long serialVersionUID = 1L;
		public Source document;
		public long pageID;
		int docHash;
		
		public PageID(final Source pageNum, final long pageId) {
			document = pageNum;
			docHash = document.hashCode();
			pageID = pageId;
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof PageID) || o == null)
				return false;
			else {
				PageID<?> page2 = (PageID<?>)o;
				
				return (page2.pageID == pageID &&
						page2.docHash == docHash &&
						document.equals(page2.document));
			}
		}
		
		@Override
		public int hashCode() {
			return docHash ^ (int)pageID;
		}
		
		public void setPageId(long id) {
			pageID = id;
		}
	}
	
	Map<PageID<Source>,PageData> fullPageMap;

	public PageServer() {
		fullPageMap = Collections
				.synchronizedMap(new LruCache<PageID<Source>, PageData>(
						DEFAULT_CACHE));
	}

	public void clearAll() {
		clearBuffer();
	}
	
//	protected void finalize() throws Throwable {
//		super.finalize();
//	}
	
	public void clearBuffer() {
		System.gc();
		System.gc();
	}
	
	abstract public void shutdown();

	public double hitRate() {
		if (hits + misses > 0)
			return ((double)hits) / (hits + misses);
		else
			return 0;
	}
	
	public void addToCache(Source doc, long pageId, PageData page) {
		PageID<Source> uid = new PageID<Source>(doc, pageId);
		fullPageMap.put(uid, page);
	}
	
	public abstract PageID<Source> getSourceFrom(Source doc, long pageId);
	
	public PageData getCachedPage(Source doc, long pageId) {
		PageID<Source> uid = getSourceFrom(doc, pageId);//doc + "/" + String.valueOf(pageId);
		return fullPageMap.get(uid);
	}
	
	public void evict(Source doc, long pageId) {
		PageID<Source> uid = getSourceFrom(doc, pageId);//doc + "/" + String.valueOf(pageId);
		fullPageMap.remove(uid);
	}

//	public abstract PageData requestPage(Source doc, long pageId, int resolution) throws InterruptedException, ExecutionException;

}
