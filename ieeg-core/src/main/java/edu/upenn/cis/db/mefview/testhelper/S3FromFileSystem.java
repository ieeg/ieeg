/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.testhelper;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Throwables.propagate;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.AmazonWebServiceRequest;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.S3ResponseMetadata;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.BucketAccelerateConfiguration;
import com.amazonaws.services.s3.model.BucketCrossOriginConfiguration;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.s3.model.BucketLoggingConfiguration;
import com.amazonaws.services.s3.model.BucketNotificationConfiguration;
import com.amazonaws.services.s3.model.BucketPolicy;
import com.amazonaws.services.s3.model.BucketReplicationConfiguration;
import com.amazonaws.services.s3.model.BucketTaggingConfiguration;
import com.amazonaws.services.s3.model.BucketVersioningConfiguration;
import com.amazonaws.services.s3.model.BucketWebsiteConfiguration;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CompleteMultipartUploadResult;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.DeleteBucketCrossOriginConfigurationRequest;
import com.amazonaws.services.s3.model.DeleteBucketLifecycleConfigurationRequest;
import com.amazonaws.services.s3.model.DeleteBucketPolicyRequest;
import com.amazonaws.services.s3.model.DeleteBucketReplicationConfigurationRequest;
import com.amazonaws.services.s3.model.DeleteBucketRequest;
import com.amazonaws.services.s3.model.DeleteBucketTaggingConfigurationRequest;
import com.amazonaws.services.s3.model.DeleteBucketWebsiteConfigurationRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.DeleteVersionRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetBucketAccelerateConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketAclRequest;
import com.amazonaws.services.s3.model.GetBucketCrossOriginConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketLifecycleConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketLocationRequest;
import com.amazonaws.services.s3.model.GetBucketLoggingConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketNotificationConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketPolicyRequest;
import com.amazonaws.services.s3.model.GetBucketReplicationConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketTaggingConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketVersioningConfigurationRequest;
import com.amazonaws.services.s3.model.GetBucketWebsiteConfigurationRequest;
import com.amazonaws.services.s3.model.GetObjectAclRequest;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.GetS3AccountOwnerRequest;
import com.amazonaws.services.s3.model.HeadBucketRequest;
import com.amazonaws.services.s3.model.HeadBucketResult;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ListBucketsRequest;
import com.amazonaws.services.s3.model.ListMultipartUploadsRequest;
import com.amazonaws.services.s3.model.ListNextBatchOfObjectsRequest;
import com.amazonaws.services.s3.model.ListNextBatchOfVersionsRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ListPartsRequest;
import com.amazonaws.services.s3.model.ListVersionsRequest;
import com.amazonaws.services.s3.model.MultipartUploadListing;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.Owner;
import com.amazonaws.services.s3.model.PartListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.Region;
import com.amazonaws.services.s3.model.RestoreObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.SetBucketAccelerateConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketAclRequest;
import com.amazonaws.services.s3.model.SetBucketCrossOriginConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketLifecycleConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketLoggingConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketNotificationConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketPolicyRequest;
import com.amazonaws.services.s3.model.SetBucketReplicationConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketTaggingConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketVersioningConfigurationRequest;
import com.amazonaws.services.s3.model.SetBucketWebsiteConfigurationRequest;
import com.amazonaws.services.s3.model.SetObjectAclRequest;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.amazonaws.services.s3.model.UploadPartResult;
import com.amazonaws.services.s3.model.VersionListing;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;

/**
 * @author Sam Donnelly
 * 
 */
public class S3FromFileSystem implements AmazonS3 {

	private final static Logger logger = LoggerFactory
			.getLogger(S3FromFileSystem.class);

	@Override
	public void abortMultipartUpload(AbortMultipartUploadRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeObjectStorageClass(String arg0, String arg1,
			StorageClass arg2) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CompleteMultipartUploadResult completeMultipartUpload(
			CompleteMultipartUploadRequest arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CopyObjectResult copyObject(CopyObjectRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CopyObjectResult copyObject(String arg0, String arg1, String arg2,
			String arg3) throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public CopyPartResult copyPart(CopyPartRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public Bucket createBucket(CreateBucketRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public Bucket createBucket(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public Bucket createBucket(String arg0, Region arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public Bucket createBucket(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucket(DeleteBucketRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucket(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketLifecycleConfiguration(String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketPolicy(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketPolicy(DeleteBucketPolicyRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketWebsiteConfiguration(String arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketWebsiteConfiguration(
			DeleteBucketWebsiteConfigurationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteObject(DeleteObjectRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteObject(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public DeleteObjectsResult deleteObjects(DeleteObjectsRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteVersion(DeleteVersionRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteVersion(String arg0, String arg1, String arg2)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean doesBucketExist(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public URL generatePresignedUrl(GeneratePresignedUrlRequest arg0)
			throws AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public URL generatePresignedUrl(String arg0, String arg1, Date arg2)
			throws AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public URL generatePresignedUrl(String arg0, String arg1, Date arg2,
			HttpMethod arg3) throws AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public AccessControlList getBucketAcl(String arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public AccessControlList getBucketAcl(GetBucketAclRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketLifecycleConfiguration getBucketLifecycleConfiguration(
			String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public String getBucketLocation(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public String getBucketLocation(GetBucketLocationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketLoggingConfiguration getBucketLoggingConfiguration(String arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketNotificationConfiguration getBucketNotificationConfiguration(
			String arg0) throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketPolicy getBucketPolicy(String arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketPolicy getBucketPolicy(GetBucketPolicyRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketVersioningConfiguration getBucketVersioningConfiguration(
			String arg0) throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketWebsiteConfiguration getBucketWebsiteConfiguration(String arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketWebsiteConfiguration getBucketWebsiteConfiguration(
			GetBucketWebsiteConfigurationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public S3ResponseMetadata getCachedResponseMetadata(
			AmazonWebServiceRequest arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("resource")
	@Override
	public S3Object getObject(GetObjectRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		final String M = "getObject(...)";
		FileInputStream is = null;
		try {
			final String bucket = arg0.getBucketName();
			String key = arg0.getKey();
			if (key.matches("/.?:/.*"))
				key = key.substring(1);
			
			String filePath = key;
			if (!Paths.get(key).isAbsolute()) {
				filePath = bucket + (bucket.endsWith(File.separator) || key.startsWith(File.separator) ? "" : 
					File.separator) + key;
			}
			logger.debug("{}: file name [{}]", M, filePath);
			final File file = new File(URLDecoder.decode(filePath, "UTF-8"));
			is = new FileInputStream(file);

			S3Object s3Object = new S3Object();

			ObjectMetadata om = new ObjectMetadata();

			int startRange = Ints.checkedCast(arg0.getRange()[0]);

			om.setContentLength(arg0.getRange()[1] - startRange + 1);
			s3Object.setObjectMetadata(om);

			int skipped = Ints.checkedCast(is.skip(startRange));

			checkState(
					skipped == startRange,
					"%i %i",
					skipped,
					startRange);

			byte[] buffer =
					new byte[
					Ints.checkedCast(s3Object.getObjectMetadata()
							.getContentLength())];
			int totalBytesRead = 0;
			while (totalBytesRead < buffer.length) {
				int bytesRead = is.read(
						buffer,
						totalBytesRead,
						buffer.length - totalBytesRead);
				if (bytesRead == -1) {
					throw new AssertionError();
				} else {
					totalBytesRead += bytesRead;
				}
			}

			s3Object.setObjectContent(new ByteArrayInputStream(buffer));

			return s3Object;
		} catch (IOException ioe) {
			throw propagate(ioe);
		} finally {
			BtUtil.close(is);
		}
	}

	@Override
	public S3Object getObject(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectMetadata getObject(GetObjectRequest arg0, File arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public AccessControlList getObjectAcl(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public AccessControlList getObjectAcl(String arg0, String arg1, String arg2)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectMetadata getObjectMetadata(GetObjectMetadataRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		return new ObjectMetadata();
	}

	@Override
	public ObjectMetadata getObjectMetadata(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		final ObjectMetadata om = new ObjectMetadata();
		om.setHeader(Headers.ETAG, BtUtil.newUuid());
		return om;
	}

	@Override
	public Owner getS3AccountOwner() throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public InitiateMultipartUploadResult initiateMultipartUpload(
			InitiateMultipartUploadRequest arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Bucket> listBuckets() throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Bucket> listBuckets(ListBucketsRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public MultipartUploadListing listMultipartUploads(
			ListMultipartUploadsRequest arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectListing listNextBatchOfObjects(ObjectListing arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public VersionListing listNextBatchOfVersions(VersionListing arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectListing listObjects(String arg0) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectListing listObjects(ListObjectsRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectListing listObjects(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public PartListing listParts(ListPartsRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public VersionListing listVersions(ListVersionsRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public VersionListing listVersions(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public VersionListing listVersions(String arg0, String arg1, String arg2,
			String arg3, String arg4, Integer arg5)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public PutObjectResult putObject(PutObjectRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public PutObjectResult putObject(String arg0, String arg1, File arg2)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public PutObjectResult putObject(String arg0, String arg1,
			InputStream arg2, ObjectMetadata arg3)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketAcl(SetBucketAclRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketAcl(String arg0, AccessControlList arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketAcl(String arg0, CannedAccessControlList arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketLifecycleConfiguration(String arg0,
			BucketLifecycleConfiguration arg1) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketLoggingConfiguration(
			SetBucketLoggingConfigurationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketNotificationConfiguration(String arg0,
			BucketNotificationConfiguration arg1) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketPolicy(SetBucketPolicyRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketPolicy(String arg0, String arg1)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketVersioningConfiguration(
			SetBucketVersioningConfigurationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketWebsiteConfiguration(
			SetBucketWebsiteConfigurationRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketWebsiteConfiguration(String arg0,
			BucketWebsiteConfiguration arg1) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setEndpoint(String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObjectAcl(String arg0, String arg1, AccessControlList arg2)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObjectAcl(String arg0, String arg1,
			CannedAccessControlList arg2) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObjectAcl(String arg0, String arg1, String arg2,
			AccessControlList arg3) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObjectAcl(String arg0, String arg1, String arg2,
			CannedAccessControlList arg3) throws AmazonClientException,
			AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public UploadPartResult uploadPart(UploadPartRequest arg0)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketCrossOriginConfiguration(String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteBucketTaggingConfiguration(String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketCrossOriginConfiguration getBucketCrossOriginConfiguration(
			String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public BucketTaggingConfiguration getBucketTaggingConfiguration(String arg0) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketCrossOriginConfiguration(String arg0,
			BucketCrossOriginConfiguration arg1) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setBucketTaggingConfiguration(String arg0,
			BucketTaggingConfiguration arg1) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setObjectRedirectLocation(String arg0, String arg1, String arg2)
			throws AmazonClientException, AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void restoreObject(RestoreObjectRequest arg0)
			throws AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void restoreObject(String arg0, String arg1, int arg2)
			throws AmazonServiceException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRegion(com.amazonaws.regions.Region region)
			throws IllegalArgumentException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setS3ClientOptions(S3ClientOptions clientOptions) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setObjectAcl(SetObjectAclRequest setObjectAclRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "setObjectAcl(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketLifecycleConfiguration(
			SetBucketLifecycleConfigurationRequest setBucketLifecycleConfigurationRequest) {
		final String m = "setBucketLifecycleConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void deleteBucketLifecycleConfiguration(
			DeleteBucketLifecycleConfigurationRequest deleteBucketLifecycleConfigurationRequest) {
		final String m = "deleteBucketLifecycleConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketCrossOriginConfiguration(
			SetBucketCrossOriginConfigurationRequest setBucketCrossOriginConfigurationRequest) {
		final String m = "setBucketCrossOriginConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void deleteBucketCrossOriginConfiguration(
			DeleteBucketCrossOriginConfigurationRequest deleteBucketCrossOriginConfigurationRequest) {
		final String m = "deleteBucketCrossOriginConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketTaggingConfiguration(
			SetBucketTaggingConfigurationRequest setBucketTaggingConfigurationRequest) {
		final String m = "setBucketTaggingConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void deleteBucketTaggingConfiguration(
			DeleteBucketTaggingConfigurationRequest deleteBucketTaggingConfigurationRequest) {
		final String m = "deleteBucketTaggingConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketNotificationConfiguration(
			SetBucketNotificationConfigurationRequest setBucketNotificationConfigurationRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "setBucketNotificationConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void enableRequesterPays(String bucketName)
			throws AmazonServiceException, AmazonClientException {
		final String m = "enableRequesterPays(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void disableRequesterPays(String bucketName)
			throws AmazonServiceException, AmazonClientException {
		final String m = "disableRequesterPays(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public boolean isRequesterPaysEnabled(String bucketName)
			throws AmazonServiceException, AmazonClientException {
		final String m = "isRequesterPaysEnabled(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void deleteBucketReplicationConfiguration(String arg0) throws AmazonServiceException, AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public BucketReplicationConfiguration getBucketReplicationConfiguration(String arg0)
			throws AmazonServiceException, AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void setBucketReplicationConfiguration(SetBucketReplicationConfigurationRequest arg0)
			throws AmazonServiceException, AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void setBucketReplicationConfiguration(String arg0, BucketReplicationConfiguration arg1)
			throws AmazonServiceException, AmazonClientException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public ObjectListing listNextBatchOfObjects(
			ListNextBatchOfObjectsRequest listNextBatchOfObjectsRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "listNextBatchOfObjects(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public VersionListing listNextBatchOfVersions(
			ListNextBatchOfVersionsRequest listNextBatchOfVersionsRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "listNextBatchOfVersions(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public Owner getS3AccountOwner(
			GetS3AccountOwnerRequest getS3AccountOwnerRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "getS3AccountOwner(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public HeadBucketResult headBucket(HeadBucketRequest headBucketRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "headBucket(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketLoggingConfiguration getBucketLoggingConfiguration(
			GetBucketLoggingConfigurationRequest getBucketLoggingConfigurationRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "getBucketLoggingConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketVersioningConfiguration getBucketVersioningConfiguration(
			GetBucketVersioningConfigurationRequest getBucketVersioningConfigurationRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "getBucketVersioningConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketLifecycleConfiguration getBucketLifecycleConfiguration(
			GetBucketLifecycleConfigurationRequest getBucketLifecycleConfigurationRequest) {
		final String m = "getBucketLifecycleConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketCrossOriginConfiguration getBucketCrossOriginConfiguration(
			GetBucketCrossOriginConfigurationRequest getBucketCrossOriginConfigurationRequest) {
		final String m = "getBucketCrossOriginConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketTaggingConfiguration getBucketTaggingConfiguration(
			GetBucketTaggingConfigurationRequest getBucketTaggingConfigurationRequest) {
		final String m = "getBucketTaggingConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketNotificationConfiguration getBucketNotificationConfiguration(
			GetBucketNotificationConfigurationRequest getBucketNotificationConfigurationRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "getBucketNotificationConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketReplicationConfiguration getBucketReplicationConfiguration(
			GetBucketReplicationConfigurationRequest getBucketReplicationConfigurationRequest)
			throws AmazonServiceException, AmazonClientException {
		final String m = "getBucketReplicationConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public ListObjectsV2Result listObjectsV2(String bucketName)
			throws AmazonClientException, AmazonServiceException {
		final String m = "listObjectsV2(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public ListObjectsV2Result listObjectsV2(String bucketName, String prefix)
			throws AmazonClientException, AmazonServiceException {
		final String m = "listObjectsV2(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public ListObjectsV2Result listObjectsV2(
			ListObjectsV2Request listObjectsV2Request)
			throws AmazonClientException, AmazonServiceException {
		final String m = "listObjectsV2(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public AccessControlList getObjectAcl(
			GetObjectAclRequest getObjectAclRequest)
			throws AmazonClientException, AmazonServiceException {
		final String m = "getObjectAcl(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void deleteBucketReplicationConfiguration(
			DeleteBucketReplicationConfigurationRequest request)
			throws AmazonServiceException, AmazonClientException {
		final String m = "deleteBucketReplicationConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public boolean doesObjectExist(String bucketName, String objectName)
			throws AmazonServiceException, AmazonClientException {
		final String m = "doesObjectExist(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketAccelerateConfiguration getBucketAccelerateConfiguration(
			String bucket) throws AmazonServiceException, AmazonClientException {
		final String m = "getBucketAccelerateConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public BucketAccelerateConfiguration getBucketAccelerateConfiguration(
			GetBucketAccelerateConfigurationRequest getBucketAccelerateConfigurationRequest)
			throws AmazonServiceException, AmazonClientException {
		final String m = "getBucketAccelerateConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketAccelerateConfiguration(String bucketName,
			BucketAccelerateConfiguration accelerateConfiguration)
			throws AmazonServiceException, AmazonClientException {
		final String m = "setBucketAccelerateConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

	@Override
	public void setBucketAccelerateConfiguration(
			SetBucketAccelerateConfigurationRequest setBucketAccelerateConfigurationRequest)
			throws AmazonServiceException, AmazonClientException {
		final String m = "setBucketAccelerateConfiguration(...)";
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Operation not supported.");
		
	}

}
