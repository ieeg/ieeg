/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkArgument;
import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToAuthzFailure;

import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.Striped;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.RecordingObjectContent;
import edu.upenn.cis.braintrust.datasnapshot.RecordingObjectIdAndVersion;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.webapp.IeegFilter;
import edu.upenn.cis.db.mefview.server.exceptions.ServerBusyException;
import edu.upenn.cis.db.mefview.server.exceptions.TooManyRequestsException;
import edu.upenn.cis.db.mefview.services.IObjectResource;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.shared.RecordingObject;

public final class ObjectResource implements IObjectResource {

	private final IDataSnapshotServer dsServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Semaphore transferSemaphoresAllUsers =
			TransferSemaphoresFactory.getTransferSemaphoreAllUsers();
	private final Striped<Semaphore> transferSemaphoresPerUser =
			TransferSemaphoresFactory.getTransferSemaphoresPerUser();
	private final HttpServletRequest httpServletRequest;
	private final User user;

	public ObjectResource(
			@Context HttpServletRequest httpServletRequest) {
		String m = "ObjectResource(...)";
		this.httpServletRequest = httpServletRequest;
		this.user = (User) this.httpServletRequest.getAttribute("user");
		logger.debug("{}: user: ", m, user.getUsername());
	}

	@Override
	public Response getObject(
			String objectIdStr,
			String eTag,
			String range,
			String disposition) {
		long in = System.nanoTime();
		if (objectIdStr.contains(".")) {
			objectIdStr = objectIdStr.substring(0, objectIdStr.indexOf('.'));
		}
		long objectId = Long.valueOf(objectIdStr);
		
		final String m = "getObject(...)";
		try {
			checkArgument(disposition == null
					|| disposition.equals("inline")
					|| disposition.equals("attachment"));
			Semaphore userSemaphore = transferSemaphoresPerUser.get(user);

			try {
				if (!transferSemaphoresAllUsers.tryAcquire(
						15,
						TimeUnit.SECONDS)) {
					throw new ServerBusyException(
							"max total simultaneous downloads exceeded, user "
									+ user.getUsername());
				}
				IeegFilter.setTransferAllUsersSemaphore(
						httpServletRequest,
						transferSemaphoresAllUsers);

				if (!userSemaphore.tryAcquire(
						15,
						TimeUnit.SECONDS)) {
					throw new TooManyRequestsException(
							"no more downloads avaliable for user "
									+ user.getUsername());
				}
				IeegFilter.setTransferPerUserSemaphore(
						httpServletRequest,
						userSemaphore);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				logger.error(m
						+ ": Interrupted while waiting for userSemaphore", e);
				throw new TooManyRequestsException(
						"could not aquire download lock for user "
								+ user.getUsername());
			}

			Long startByteOffset = null;
			Long endByteOffset = null;

			if (range != null) {
				ByteRange byteRange = ByteRange.parse(range);
				startByteOffset = byteRange.getStart();
				if (byteRange.hasEnd()) {
					endByteOffset = byteRange.getEnd();
				}
			}

			int eTagInt = -1;
			boolean checkVersion = false;
			if (eTag != null) {
				checkVersion = true;
				try {
					eTagInt = Integer.parseInt(eTag);
				} catch (NumberFormatException e) {
					logger.error(
							"Got non-integer If-Match value",
							e);
					// This woudn't match, so may as well return now.
					return Response.status(
							Status.PRECONDITION_FAILED).build();
				}
			}
			RecordingObjectIdAndVersion idAndVersion = new RecordingObjectIdAndVersion(
					objectId,
					eTagInt);
			final RecordingObjectContent content = dsServer
					.getRecordingObjectContent(
							user,
							idAndVersion,
							checkVersion,
							startByteOffset,
							endByteOffset);
			Response resp = null;
			if (range == null) {
				resp = Response
						.ok(content.getInputStream())
						.build();
			} else {
				resp = Response
						.status(Status.PARTIAL_CONTENT)
						.entity(content.getInputStream())
						.build();
			}
			resp.getHeaders().putSingle(
					HttpHeaders.CONTENT_TYPE,
					content.getInternetMediaType());
			resp.getHeaders().putSingle(
					HttpHeaders.ETAG,
					content.getETag());
			if (disposition != null) {
				resp.getHeaders().putSingle(
						HttpHeaders.CONTENT_DISPOSITION,
						disposition
								+ "; filename="
								+ content.getName());
			}
			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.WS_RECORDING_OBJECT,
							content.getParentId().getId());
			IeegFilter.setUserAndDataUsage(
					httpServletRequest,
					user,
					dataUsage);
			return resp;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RecordingObjectNotFoundException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToNoSuchRecordingObjectFailure(
							e,
							m,
							logger);
		} catch (FailedVersionMatchException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToPreconditionFailed(
							e,
							m,
							logger);
		} catch (ServerBusyException sbe) {
			throw IeegWsExceptionUtil.logAndConvertToServerBusyException(
					sbe,
					m,
					logger);
		} catch (TooManyRequestsException e) {
			throw IeegWsExceptionUtil.logAndConvertToTooManyRequests(
					e,
					m,
					logger);
		} catch (InvalidRangeException e) {
			throw IeegWsExceptionUtil.logAndConvertToInvalidRangeFailure(
					e,
					m,
					logger);
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} finally {
			logger.info(
					"{}: user {} objectId {} range {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							objectId,
							range,
							BtUtil.diffNowThenSeconds(in) });

		}

	}

	@Override
	public void deleteObject(
			String eTag,
			long objectId) {
		String m = "deleteObject(...)";
		try {
			Integer eTagInt = null;
			boolean checkVersion = false;
			try {
				if (eTag != null) {
					eTagInt = Integer.valueOf(eTag);
					checkVersion = true;
				} else {
					eTagInt = -1;
					checkVersion = false;
				}
			} catch (NumberFormatException nbe) {
				eTagInt = -1;
				checkVersion = true;
			}

			dsServer.deleteRecordingObject(
					user,
					new RecordingObjectIdAndVersion(
							objectId,
							eTagInt),
					checkVersion);
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (RecordingObjectNotFoundException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToNoSuchRecordingObjectFailure(
							e,
							m,
							logger);
		} catch (FailedVersionMatchException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToPreconditionFailed(
							e,
							m,
							logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public Response getFile(String datasetId, String objectId, String eTag, String range, String disposition) {
		Set<RecordingObject> ros = dsServer.getRecordingObjects(user, new DataSnapshotId(datasetId));
		
		for (RecordingObject ro: ros) {
			if (ro.getName().equals(objectId)) {
				return getObject(String.valueOf(ro.getId()), eTag, range, disposition);
			}
		}

		return Response.status(
				Status.NOT_FOUND).build();
	}

}
