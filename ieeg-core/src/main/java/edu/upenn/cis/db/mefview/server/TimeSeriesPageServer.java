/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.server.FilterManager.FilterBuffer;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.PostProcessor;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public abstract class TimeSeriesPageServer<PageData>
		extends PageServer<ChannelSpecifier, PageData>
		implements ITimeSeriesPageServer {
	public static final int MAX_CHANNELS = 16;
	public static final int CHANNEL_GROUP = 8;

	public static final class ChSpecTimeSeriesDto {
		public final ChannelSpecifier channelSpecifier;
		public final TimeSeriesDto timeSeriesDto;

		public ChSpecTimeSeriesDto(ChannelSpecifier chSpec,
				TimeSeriesDto ts) {
			this.channelSpecifier = chSpec;
			this.timeSeriesDto = ts;
		}
	}

	public interface TimeSeriesVersion {
		double getBaseFrequency();
	}

	public static class Decimate implements PostProcessor {
		final FilterSpec filter;
		final double targetFrequency;
		FilterManager.FilterBuffer filterBuffer;
		final boolean minMax;

		public Decimate(double targetFrequency, FilterSpec filter,
				boolean minMax) {
			this.targetFrequency = targetFrequency;
			this.filter = filter;
			this.filterBuffer = null;
			this.minMax = minMax;
		}

		public Decimate(double targetFrequency, FilterSpec filter,
				FilterBuffer fb, boolean minMax) {
			this.targetFrequency = targetFrequency;
			this.filter = filter;
			this.filterBuffer = fb;
			this.minMax = minMax;
		}

		public Decimate(Decimate n, FilterSpec filter, int[] buffer,
				boolean minMax) {
			targetFrequency = n.targetFrequency;
			this.filter = filter;
			setWorkBuffer(buffer);
			this.minMax = minMax;
		}

		public Decimate(double targetFrequency, FilterSpec filter,
				int sampleCount, boolean minMax) {
			this.targetFrequency = targetFrequency;
			this.filter = filter;
			this.minMax = minMax;

			createWorkBuffer(sampleCount);
		}

		public Decimate(double targetFrequency, FilterSpec filter, int[] buffer,
				boolean minMax) {
			this.targetFrequency = targetFrequency;
			this.filter = filter;
			this.minMax = minMax;

			setWorkBuffer(buffer);
		}

		@Override
		public boolean needsWorkBuffer(int sampleCount) {
			return (filterBuffer == null || filterBuffer.buf.length < sampleCount);
		}

		@Override
		public void createWorkBuffer(int sampleCount) {
			filterBuffer = new FilterManager.FilterBuffer(sampleCount);
		}

		@Override
		public void setWorkBuffer(int[] buffer) {
			filterBuffer = new FilterManager.FilterBuffer(buffer);
		}

		@Override
		public boolean isFiltered() {
			return (this.filter.getFilterType() != FilterSpec.DECIMATE_FILTER);
		}

		@Override
		public double getSampleRate() {
			return targetFrequency;
		}

		@Override
		public ArrayList<TimeSeriesData> process(double startTime,
				double endTime, double trueFreq,
				long startTimeOffset, double voltageScale,
				TimeSeriesPage[] pageList, ChannelSpecifier path,
				boolean minValIsNull, double padBefore, double padAfter) {

			if (filterBuffer == null)
				this.createWorkBuffer((int)((endTime - startTime + padBefore + padAfter) / trueFreq) + 1);
			
			boolean minMax = this.minMax && !IvProps.getIvProps().containsKey("minMaxOff");
			
			if (minMax && 2 * getSampleRate() <= trueFreq)
				return FilterManager.decimateMinMax(startTime, endTime, trueFreq, 
						startTimeOffset, 1.E6 / getSampleRate(), voltageScale, pageList,
						filter, path, minValIsNull, filterBuffer, padBefore, padAfter);
			else
				return FilterManager.decimateLinearInterpolate(startTime, endTime, trueFreq, 
						startTimeOffset, 1.E6 / getSampleRate(), voltageScale, pageList,
						filter, path, minValIsNull, filterBuffer, padBefore, padAfter);
				
		}

		public FilterSpec getFilter() {
			return filter;
		}

		@Override
		public int getWorkBufferSize() {
			if (filterBuffer != null
					&& filterBuffer.buf != null) {
				return filterBuffer.buf.length;
			} else {
				return 0;
			}
		}
	}

	public abstract SnapshotSpecifier getSnapshotSpecifier(final User user,
			final String revId) throws AuthorizationException;

	public abstract ChannelSpecifier getChannelSpecifier(final User user,
			final SnapshotSpecifier snapshot, final String revId,
			double samplingPeriod);

	// public abstract ChannelSpecifier getChannelSpecifier(final User user,
	// final String snapshotId, final String channelId) throws
	// AuthorizationException ;

	Map<String, PageReader<PageData>> readerMap = new LruCache<String, PageReader<PageData>>(
			500);
	private final int mefPadding;

	public TimeSeriesPageServer() {
		super();
		mefPadding = IvProps.getPaddingSamples();
	}

	public TimeSeriesPageServer(
			int mefPadding) {
		this.mefPadding = mefPadding;
	}

	protected int getPaddingSamples() {
		return mefPadding;
	}

	@Override
	public PageID<ChannelSpecifier> getSourceFrom(ChannelSpecifier doc,
			long pageId) {
		return new PageID<ChannelSpecifier>(doc, pageId);
	}

	@Override
	public void clearAll() {
		super.clearAll();
		readerMap.clear();
	}

	/**
	 * Add a new reader, unless it already exists
	 * 
	 * @param doc
	 * @param reader
	 */
	public void addReader(ChannelSpecifier doc, PageReader<PageData> reader) {
		if (readerMap.get(doc.getKey()) == null) {
			readerMap.put(doc.getKey(), reader);
		}
	}

	public PageReader<PageData> getReaderFor(ChannelSpecifier doc) {
		return readerMap.get(doc.getKey());
	}

	public abstract long getNumSamples(ChannelSpecifier spec);// //String path);

	public abstract long getEndUutc(ChannelSpecifier spec);// //String path);

	public abstract int getMaxSampleValue(ChannelSpecifier spec);// //String
																	// path);

	public abstract int getMinSampleValue(ChannelSpecifier spec);// //String
																	// path);

	public abstract long getNumIndexEntries(ChannelSpecifier spec);// //String
																	// path);

	public abstract double getSampleRate(ChannelSpecifier spec);// //String
																// path);

	public abstract long getStartUutc(ChannelSpecifier spec);

	public abstract String getInstitution(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract String getAcquisitionSystem(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract String getChannelName(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract String getChannelComments(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract String getSubjectId(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract double getVoltageConversionFactor(ChannelSpecifier spec)
			throws IllegalArgumentException;

	public abstract List<TimeSeriesData> requestRead(
			ChannelSpecifier path, long startTime,
			long endTime, PostProcessor postProc, User user, 
			@Nullable SessionToken session)
			throws InterruptedException, ExecutionException, IOException, ServerTimeoutException;

	protected abstract List<List<TimeSeriesData>>
			requestMultiChannelReadBlock(
					final List<? extends ChannelSpecifier> paths, 
																	
																	
					long startTime, long endTime, PostProcessor[] postProc)
					throws InterruptedException, ExecutionException, ServerTimeoutException, IOException;

	@Override
	public List<List<TimeSeriesData>>
			requestMultiChannelRead(
					final List<? extends ChannelSpecifier> paths, 
					long startTime, long endTime,					
					PostProcessor[] postProc, User user, @Nullable SessionToken sessionToken)
					throws InterruptedException, ExecutionException, ServerTimeoutException, IOException {

		long blockSize = 30 * (long) 1E6;

		long blockEnd = (endTime > startTime + blockSize) ? startTime
				+ blockSize : endTime;
		long blockStart = startTime;

		if (blockEnd < blockStart) {
			blockStart = blockEnd;
		}

		if (paths.size() > MAX_CHANNELS)
			return requestMultiChannelReadLarge(paths, startTime, endTime,
					postProc);

		List<List<TimeSeriesData>> ret = requestMultiChannelReadBlock(
				// source,
				paths, // revIds,
				blockStart, blockEnd, postProc);

		blockStart += blockSize;// (blockEnd - blockStart);
		// while (blockEnd < endTime) {
		while (blockStart < endTime) {
			// blockEnd = (endTime > blockStart + blockSize) ? blockStart
			// + blockSize : endTime;
			blockEnd = (endTime > blockStart + blockSize) ? blockStart
					+ blockSize : endTime;

			// TODO: request 100 sample overlap

			List<List<TimeSeriesData>> newSegment = requestMultiChannelReadBlock(
					// source,
					paths, // revIds,
					blockStart, blockEnd,
					postProc);

			merge(ret, newSegment);
			blockStart += blockSize;// (blockEnd - blockStart);
		}

		return ret;
	}

	/**
	 * For large numbers of channels, break into 16-channel requests to conserve
	 * resources
	 * 
	 * @param paths
	 * @param startTime
	 * @param endTime
	 * @param postProc
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws ServerTimeoutException TODO
	 * @throws IOException 
	 */
	private List<List<TimeSeriesData>>
			requestMultiChannelReadLarge(// final SnapshotSpecifier source,
					final List<? extends ChannelSpecifier> paths, // final
																	// List<String>
																	// revIds,
					long startTime, long endTime,
					PostProcessor[] postProc)
					throws InterruptedException, ExecutionException, ServerTimeoutException, IOException {

		long blockSize = 30 * (long) 1E6;

		List<List<TimeSeriesData>> ret2 = new ArrayList<>();
		List<ChannelSpecifier> pathList = new ArrayList<>();
		List<PostProcessor> postProcList = new ArrayList<>();
		for (int i = 0; i < paths.size(); i += CHANNEL_GROUP) {// 16) {

			long blockEnd = (endTime > startTime + blockSize) ? startTime
					+ blockSize : endTime;
			long blockStart = startTime;

			if (blockEnd < blockStart) {
				blockStart = blockEnd;
			}

			pathList.clear();
			postProcList.clear();
			for (int p = i; p < paths.size() && p < i + CHANNEL_GROUP; p++) {
				pathList.add(paths.get(p));
				postProcList.add(postProc[p]);
			}

			final PostProcessor[] postProcArray = postProcList.toArray(new PostProcessor[0]);
			List<List<TimeSeriesData>> ret = requestMultiChannelReadBlock(
					// source,
					pathList, // revIds,
					blockStart, blockEnd, postProcArray);

			blockStart += blockSize;// (blockEnd - blockStart);
			// while (blockEnd < endTime) {
			while (blockStart < endTime) {
				// blockEnd = (endTime > blockStart + blockSize) ? blockStart
				// + blockSize : endTime;
				blockEnd = (endTime > blockStart + blockSize) ? blockStart
						+ blockSize : endTime;

				// TODO: request 100 sample overlap

				List<List<TimeSeriesData>> newSegment = requestMultiChannelReadBlock(
						// source,
						pathList, // revIds,
						blockStart, blockEnd,
						postProcArray);

				merge(ret, newSegment);
				blockStart += blockSize;// (blockEnd - blockStart);
			}

			ret2.addAll(ret);
		}

		return ret2;
	}

	public static void merge(
			List<? extends List<TimeSeriesData>> existing,
			List<? extends List<TimeSeriesData>> newSegment) {
		for (int i = 0; i < existing.size(); i++) {
			existing.get(i).addAll(newSegment.get(i));
		}
	}

	/**
	 * Returns a list of {@code TraceInfo} for the passed in list.
	 * 
	 * This implementation repeatedly calls {@code get*()} methods for header
	 * info. Subclasses should override this method if they have a more
	 * efficient method of accomplishing this.
	 * 
	 * @param cstsList
	 * @return the trace infos
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@Override
	public List<TraceInfo> getTraceInfos(
			List<ChSpecTimeSeriesDto> cstsList) throws IOException,
			InterruptedException, ExecutionException {
		final List<TraceInfo> traceInfos = newArrayList();
		for (final ChSpecTimeSeriesDto csts : cstsList) {
			final ChannelSpecifier p = csts.channelSpecifier;
			final TimeSeriesDto tsDto = csts.timeSeriesDto;
//			final double duration = getEndUutc(p) - getStartUutc(p);
			TraceInfo ti = new TraceInfo(
					//(GeneralMetadata)p,
					null,
					getStartUutc(p),//0,
					getEndUutc(p) - getStartUutc(p),//duration,
					getSampleRate(p),
					getVoltageConversionFactor(p),
					getMinSampleValue(p),
					getMaxSampleValue(p),
					getInstitution(p),
					getAcquisitionSystem(p),
					getChannelComments(p),
					getSubjectId(p),
					tsDto.getLabel(),
					tsDto.getId());
			traceInfos.add(ti);
		}
		return traceInfos;
	}

	/**
	 * Returns the list of sample rates for {@code chSpecs}. This implementation
	 * repeatedly calls {@code getSampleRate()}. Subclasses should override this
	 * method if they have a more efficient method of accomplishing this.
	 * 
	 * @param chSpecs
	 * @return the sample rates
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@Override
	public List<Double> getSampleRates(List<? extends ChannelSpecifier> chSpecs) {
		try {
			final List<Double> rates = newArrayList();
			for (final ChannelSpecifier chSpec : chSpecs) {
				rates.add(Double.valueOf(getSampleRate(chSpec)));
			}
			return rates;
		} catch (Exception e) {
			throw propagate(e);
		}
	}

	@Override
	public Map<String, ChannelSpecifier> getChannelSpecifiers(
			User user,
			edu.upenn.cis.db.mefview.services.SnapshotSpecifier dsSpec,
			Set<String> tsIds, double samplingPeriod)
			throws AuthorizationException {
		Map<String, ChannelSpecifier> tsIds2ChSpecs = newHashMap();
		for (String tsId : tsIds) {
			tsIds2ChSpecs.put(tsId,
					getChannelSpecifier(user, dsSpec, tsId, samplingPeriod));
		}
		return tsIds2ChSpecs;
	}
}
