/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class BasicBroker<K extends IMessage<?>, V> implements IMessageBroker<K, V> {
	Map<String,FutureRegistry<K,V>> futures;

	public BasicBroker() {
		futures = new HashMap<String,FutureRegistry<K, V>>();
	}

	public void setHandler(String channel, FutureRegistry<K,V> futures) {
		this.futures.put(channel, futures);
	}

	@Override
	public void intercept(
			String channel, 
			K key,
			edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer<K, V> handler) {

		synchronized (futures) {
			if (!futures.containsKey(channel))
				futures.put(channel,  new FutureRegistry<K,V>());
			
			synchronized (futures.get(channel)) {
				System.out.println("Intercepting channel " + channel);
				ConsumerHandler<K,V> fh = new ConsumerHandler<K,V>(key, handler);
				futures.get(channel).registerChannel(
						channel, 
						new FutureRegistry.RegisteredHandler<K,V>(fh.getSuccessHandler(), fh.getErrorHandler()));
			}
		}
	}

	@Override
	public void remove(
			String channel, 
			K key,
			edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer<K, V> handler) {
		synchronized (futures) {
			if (futures.containsKey(channel))
				futures.remove(channel);
		}
	}

	@Override
	public void interceptOnce(
			final String channel, 
			final K key,
			final edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer<K, V> handler) {
		
		// Nest the handler inside another handler, which removes, then calls, the handler
		IConsumer<K,V> wrapper = new IConsumer<K,V>() {
			private boolean isDone = false;

			@Override
			public synchronized void handle(K key, V value) {
				remove(channel, key, this);
				if (!isDone) {
					handler.handle(key, value);
					isDone = true;
				}
			}

			@Override
			public void handleError(K key, JsonTyped value) {
				remove(channel, key, this);
				if (!isDone) {
					handler.handleError(key, value);
					isDone = true;
				}
			}
			
		};

		// Register our wrapper
		intercept(channel, key, wrapper);
	}

}
