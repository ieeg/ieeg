/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesPage;
import edu.upenn.cis.eeg.filters.TimeSeriesFilter;
import edu.upenn.cis.eeg.filters.TimeSeriesFilterFactory;

public class FilterManager {
	private final static Logger logger = LoggerFactory
	.getLogger(FilterManager.class);

	public static class FilterBuffer {
		public int[] buf = null;
		
		public FilterBuffer(int size) {
			buf = new int[0];
		}
		
		public FilterBuffer(int[] arr) {
			buf = arr;
		}
	}
	

	public static FilterSpec getFilterSpecFor(double targetFrequency, FilterSpec filter, double sampleRate) {
		return modifyFilterSpec(targetFrequency, sampleRate, filter);
	}

	public static FilterSpec modifyFilterSpec(double targetFrequency,
			double sampleRate, FilterSpec ret) {

		// filtering based on Nyquist settings
		if ((ret.getFilterType() & FilterSpec.DECIMATE_FILTER) == 0) {
			double nyquist = 0;
			if (targetFrequency < sampleRate)
				nyquist = /* 0.99 */(targetFrequency / 5.0);//2.0);
			else
				nyquist = /* 0.99 * */(sampleRate / 5.0);//2.0);

			if (ret.getBandpassHighCutoff() > nyquist) {
				// fprintf(stderr,
				logger.info("Output low-pass frequency setting of "
						+ ret.getBandpassHighCutoff()
						+ " violates Nyquist => setting to Nyquist " + nyquist);// %lf
																				// %lf\n",
																				// chan->filter.bandpass_high_fc,
																				// nyquist);
//				ret.setBandpassHighCutoff(nyquist);
			} else if (ret.getBandpassHighCutoff() < 0.0) {
//				ret.setBandpassHighCutoff(targetFrequency / 5.0);
				ret.setBandpassHighCutoff(0.0);
			}
			if (ret.getBandpassLowCutoff() < 0.0)
				ret.setBandpassLowCutoff(0.0);
			if (ret.getNumPoles() <= 0) {
				ret.setNumPoles(FilterSpec.DEFAULT_FILTER_POLES);
			} else if (ret.getNumPoles() > FilterSpec.MAX_FILTER_POLES) {
				ret.setNumPoles(FilterSpec.MAX_FILTER_POLES);
			}

			// Set a filter at the nyquist frequency
		} else if ((ret.getFilterType() & FilterSpec.DECIMATE_FILTER) != 0) {
			double nyquist = 0;
			if (targetFrequency < sampleRate)
				nyquist = 0.99 * (targetFrequency / 2.0);//2.0);
			else
				nyquist = 0.99 * (sampleRate / 5.0);//2.0);
			ret.setBandpassHighCutoff(nyquist);
			ret.setFilterType(FilterSpec.LOWPASS_FILTER);
		}

		return ret;
	}

	private static final int[] nul = new int[0];

	
	/**
	 * "Decimate" the source data -- run a low-pass filter, then interpolate.  Should result in downsampling
	 * that removes aliasing.
	 * 
	 * @param startTime Start time for returned samples
	 * @param endTime End time for returned samples
	 * @param trueFreq Native frequency of the data
	 * @param startTimeOffset The offset to the MEF start time (otherwise startTime is ref'd to 0)
	 * @param samplingPeriod Requested sampling period
	 * @param scale The scale factor of the data (from the MEF header, copied into the <code>TimeSeriesData</code> struct)
	 * @param pageList List of pages from the MEF file
	 * @param filter Specification of filter to apply
	 * @param path Specifier for the file -- only used for logging
	 * @param minValIsNull Treat Integer.MIN_VALUE as a null (gap); otherwise it's treated as a value
	 * @param workSpace Working buffer for decompression
	 * @param filterPadBefore Adjustment to start time -- apply filter to data starting with <code>startTime - filterPadBefore</code>
	 * @param filterPadAfter Adjustment to start time -- apply filter to data ending at <code>startTime + filterPadAfter</code>
	 * @return
	 */
	/*
	public static ArrayList<TimeSeriesData> decimateNew(double startTime, double endTime, double trueFreq, 
			long startTimeOffset, double samplingPeriod, double scale, TimeSeriesPage[] pageList,
			FilterSpec filter, ChannelSpecifier path, boolean minValIsNull, FilterBuffer workSpace,
			double filterPadBefore, double filterPadAfter, boolean minMax) {
		final String M = "decimate()";

		ArrayList<TimeSeriesData> results = new ArrayList<TimeSeriesData>();
		double actualPeriod = 1.E6 / trueFreq;

//		long trueStartTime = pageList[0].timeStart;
		
		double outputTime = startTime;
		double spanStart = getSpanStart(pageList, startTime, actualPeriod);
		double spanEnd = getSpanEnd(pageList, spanStart, actualPeriod);
		
		int outputIndex = 0;
		outputTime = startTime + outputIndex * actualPeriod;
		while (outputTime < endTime) {
			TimeSeriesData newData;// = new TimeSeriesData(outputTime, actualPeriod, 1, samples);
			
			
			int[] samples = getSpanSamples(spanStart, spanEnd);
			applyFilter(samples);
			
			// TODO: concatenate span to output
			
			
			spanStart = getSpanStart(pageList, spanEnd);
			
			// TODO: pad with null samples from spanEnd to new spanStart
			
			spanEnd = getSpanEnd(pageList, spanStart);
			
			while (outputTime < spanStart) {
				// TODO: pad with null
				span.add(null);
				outputTime = startTime + (outputIndex++) * actualPeriod;
			}
		}
	}
	
	double getSpanStart(TimeSeriesPage[] pageList, double desiredStart, double outputSamplePeriod) {
		// TODO: find the page for (desiredStart - outputSamplePeriod)
	}
	
	double getSpanEnd(TimeSeriesPage[] pageList, double spanStart, double outputSamplePeriod) {
		
	}*/
	
	static class RetInfo {
		public int page;
		public int pos;
		public int bufferIndex;
//		public int bufferLength;
		public int nonEmptyPages;
		public int totalPages;
	}
	
	private static RetInfo decimateAntialias(
			double startTime, 
			double endTime, 
			double trueFreq, 
			long startTimeOffset, 
			double samplingPeriod, 
			double scale, 
			TimeSeriesPage[] pageList,
			FilterSpec filter, 
			ChannelSpecifier path, 
			boolean minValIsNull, 
			FilterBuffer workSpace,
			double filterPadBefore, 
			double filterPadAfter) {
		final String M = "decimate()";

		double actualPeriod = 1.E6 / trueFreq;

		long trueStartTime = pageList[0].timeStart;// lastSampleTime;

		int currentPage = 0;
		int pos = 0;

		// Read the first sample
		while (currentPage < pageList.length &&
				pos >= pageList[currentPage].values.length) {
			currentPage++;
		}

		// * Collapse the pages into spans (can we just have a common buffer???)
		// - Or make the filter into an incremental operation with exposed state
		// * Filter the spans
		// * Downsample the spans
		// * Construct the returned result, possibly null-padding
		// (or perhaps that can be added in decompression?)

		int siz = pageList.length;

		int[][] collection = new int[siz][];
		for (int i = 0; i < siz; i++) {
			if (pageList[i] != null && pageList[i].values != null)
				collection[i] = pageList[i].values;
			else
				collection[i] = nul;
		}
		IntArrayWrapper iw = new IntArrayWrapper(collection);
		TimeSeriesFilter tf = null;
		try {
			double target = (1.E6 / samplingPeriod);
			FilterSpec f2 = filter.setAt(target);
			if (f2.getFilterType() == 0 || target >= trueFreq) {
				logger.trace(M + ": Clearing all filters");
				tf = null;
			} else if (f2.getFilterType() == FilterSpec.DECIMATE_FILTER) {
				tf = TimeSeriesFilterFactory.getFilterFor(target, f2, trueFreq);
				logger.trace(M + ": Applying default low-pass filter to frequency "
						+ trueFreq + " to get " + target + ": " + tf.toString());
			} else {
				tf = TimeSeriesFilterFactory.getFilterFor(target, f2, trueFreq);
				if (tf != null)
					logger.trace(M + ": Applying explicit filter to frequency " + trueFreq
							+ ": " + tf.toString());
				else
					logger.trace(M + ": Error applying explicit filter to frequency " + trueFreq);
			}
		} catch (Exception e) {
			logger.error(M + ": Caught, setting TimeSeriesFilterButterworth to null", e);
			tf = null;
		}

		int pages = pageList.length;
		while (pages > 0 && pageList[pages-1] == null)
			pages--;

		int startInx = 0;

		if (startInx > iw.length())
			startInx = iw.length();

		int length = iw.length() - startInx;

		if (length < 0)
			length = 0;

		pos = startInx;

		// Apply the filter, segmenting into different regions as necessitated
		// by gaps
		synchronized (workSpace) {

			if (workSpace.buf == null || workSpace.buf.length < length)
				workSpace.buf = new int[length];
			if (tf != null) {
				try {
					logger.trace("{}: Filtering buffer of {} samples...", M, length);
					logger.trace("{}: From {} pages", M, pages);
					logger.trace("{}: Page region start index is {}", M, Long.valueOf(pageList[0].timeStart));

					// Add padding
					double time = (startTime - filterPadBefore - trueStartTime);
					if (time < 0)
						time = 0;
					if (time > (pageList[currentPage].timeEnd - pageList[currentPage].timeStart))
						time = pageList[currentPage].timeEnd - pageList[currentPage].timeStart;
					//				logger.trace("{}: Expected number of samples is {}", M, (int)((endTime - startTime) / actualPeriod));

					int addEarlier = (int)(time / actualPeriod);
					int useIndex = startInx + addEarlier;
					length += addEarlier + (int)(filterPadAfter / actualPeriod);

					if (length > iw.length())// - useIndex)
						length = iw.length();// - useIndex;

					// find max contiguous page.
					int pageStart = currentPage;
					int pageEnd = pageStart;

					int startIndex = 0;
					for (int p = 0; p < pageStart; p++)
						startIndex += pageList[p].values.length;
					int endIndex;

					for (int i = 0; i < length; i++)
						workSpace.buf[i] = -123;

					do {
						endIndex = startIndex + pageList[pageEnd].values.length;
						while (pageEnd+1 < pages && pageList[pageEnd+1].timeStart -
								pageList[pageEnd].timeEnd <= actualPeriod) {
							endIndex += pageList[++pageEnd].values.length;
						}

						// readjust useIndex based on pageStart
						// readjust length based on pageEnd

						int from = (useIndex < startIndex) ? startIndex : useIndex;
						if (from > endIndex)
							from = endIndex;

						int len = length - ((useIndex < startIndex) ? 0 : (useIndex - startIndex));

						if (from + len > endIndex)
							len = endIndex - from;

						if (len < 0)
							len = 0;

						// TODO: ensure we aren't out of the page's contiguous bounds
						// due to a gap

						logger.trace(M + ": Filter region is Page " + pageStart + "/"  + from + " thru " +
								"Page " + pageEnd + "/" + (from + len));

						if (len > 0 && from < iw.length() && from + len <= iw.length()) {
							try {
								tf.apply(iw, from, from, 
										len, workSpace.buf);
							} catch (ArrayIndexOutOfBoundsException i) {
								logger.error(M + ": caught exception, ignoring", i);
							}
						} else if (len != 0) {
							logger.debug("{}: Current length: {}", M, iw.length());
							logger.debug("{}: ILLEGAL BOUNDS: {} - {}", new Object[] {M, from, (from + len)});
						}

						length -= (endIndex - startIndex);
						startIndex = endIndex;
						useIndex = startIndex;
						pageStart = ++pageEnd;
					} while (pageStart < pages);
				} catch (Exception ie) {
					logger.error(M + ": caught exception, not applying filter", ie);

					for (int i = 0; i < length; i++)
						workSpace.buf[i] = iw.get(startInx + i);
				}
			} else {
				for (int i = 0; i < length; i++)
					workSpace.buf[i] = iw.get(startInx + i);
			}
		}
	
		RetInfo ret = new RetInfo();
		ret.page = currentPage;
		ret.pos = pos;
		ret.bufferIndex = startInx;
//		ret.bufferLength = iw.length();
		ret.nonEmptyPages = pages;
		ret.totalPages = siz;
		
		return ret;
	}

	/**
	 * "Decimate" the source data -- run a low-pass filter, then interpolate.  Should result in downsampling
	 * that removes aliasing.
	 * 
	 * @param startTime Start time for returned samples
	 * @param endTime End time for returned samples
	 * @param trueFreq Native frequency of the data
	 * @param startTimeOffset The offset to the MEF start time (otherwise startTime is ref'd to 0)
	 * @param samplingPeriod Requested sampling period
	 * @param scale The scale factor of the data (from the MEF header, copied into the <code>TimeSeriesData</code> struct)
	 * @param pageList List of pages from the MEF file
	 * @param filter Specification of filter to apply
	 * @param path Specifier for the file -- only used for logging
	 * @param minValIsNull Treat Integer.MIN_VALUE as a null (gap); otherwise it's treated as a value
	 * @param workSpace Working buffer for decompression
	 * @param filterPadBefore Adjustment to start time -- apply filter to data starting with <code>startTime - filterPadBefore</code>
	 * @param filterPadAfter Adjustment to start time -- apply filter to data ending at <code>startTime + filterPadAfter</code>
	 * @return
	 */
	public static ArrayList<TimeSeriesData> decimateLinearInterpolate(
			double startTime, 
			double endTime, 
			double trueFreq, 
			long startTimeOffset, 
			double samplingPeriod, 
			double scale, 
			TimeSeriesPage[] pageList,
			FilterSpec filter, 
			ChannelSpecifier path, 
			boolean minValIsNull, 
			FilterBuffer workSpace,
			double filterPadBefore, 
			double filterPadAfter) {
		final String M = "decimateLinearInterpolate()";

		ArrayList<TimeSeriesData> results = new ArrayList<TimeSeriesData>();
		double actualPeriod = 1.E6 / trueFreq;

		double outputTime = startTime;
		double lastSampleTime = -1;
		double nextSampleTime = pageList[0].timeStart;
		long trueStartTime = pageList[0].timeStart;// lastSampleTime;

		int currentPage = 0;
		int pos = 0;
		int lastSampleValue = 0;
		int nextSampleValue = 0;

		int valuesAdded = 0;
		int nullsAdded = 0;
		int size = (int) Math.ceil((endTime - startTime) / samplingPeriod);
		
		if (size < 0) {
			size = 0;
		}
		
		int count = 0;
		int inSpanCount = 0;

		if (filterPadBefore < samplingPeriod)
			filterPadBefore = samplingPeriod;
		if (filterPadAfter < samplingPeriod)
			filterPadAfter = samplingPeriod;

		synchronized (workSpace) {
			RetInfo resultsOfAntiAlias =
					decimateAntialias(startTime, endTime,
							trueFreq, startTimeOffset, samplingPeriod,
							scale, pageList, filter, path,
							minValIsNull, workSpace, filterPadBefore, filterPadAfter);

			currentPage = resultsOfAntiAlias.page;
			pos = resultsOfAntiAlias.pos;
			int startInx = resultsOfAntiAlias.bufferIndex;

			logger.trace(M + ": Trace should return " + size
					+ " samples and has " + size + " elements");
			TimeSeriesData span = new TimeSeriesData(startTimeOffset, actualPeriod,
					scale, size);
			results.add(span);
			span.setPeriod(samplingPeriod);

			while (outputTime < trueStartTime && outputTime < endTime) {
				span.addSample(null);
				outputTime = startTime + samplingPeriod * (++nullsAdded);
			}

			// Now do the downsampling, over the workSpace.buf contents
			// I don't think nextSampleTime is what it's listed as here...

			int index = startInx;
			nextSampleTime = pageList[0].timeStart + pos++ * actualPeriod;//(1.E6 / trueFreq);
			nextSampleValue = workSpace.buf[index++];//iw.get(startInx);


			double lastOutput = -1;


			// TODO: reconcile workSpace.buf and the decimation

			// outputTime = sample time we are looking for in the output
			for (int iteration = nullsAdded; iteration < size; iteration++) {
				outputTime = startTime + samplingPeriod * iteration;

				// Read the next sample
				while (nextSampleTime < outputTime && currentPage < 
						resultsOfAntiAlias.nonEmptyPages) {
					lastSampleTime = nextSampleTime;
					lastSampleValue = nextSampleValue;

					// If we are outside the page limits then
					// skip to the next page, repeatedly as long as the
					// page is empty
					while (currentPage < resultsOfAntiAlias.totalPages
							&& (pageList[currentPage] == null || pageList
							[currentPage].values.length <= pos)) {
						pos = 0;
						currentPage++;
					}
					if (currentPage < resultsOfAntiAlias.totalPages) {
						nextSampleTime = pageList[currentPage].timeStart + pos
								* actualPeriod;

						nextSampleValue = workSpace.buf[index++];
						pos++;

						count++;
					}				
				}

				if (currentPage >= resultsOfAntiAlias.totalPages) {
					if (lastOutput <= lastSampleTime) {
						nullsAdded++;
						span.addSample(null);
					} else {
						span.addSample(lastSampleValue);
						lastOutput = lastSampleTime;
					}

				} else if ((lastSampleTime != -1 && nextSampleTime - lastSampleTime > actualPeriod * 2)
						|| (nextSampleValue == Integer.MIN_VALUE && minValIsNull)) {
					while (outputTime < nextSampleTime && iteration < size) {
						nullsAdded++;
						iteration++;
						span.addSample(null);
						outputTime = startTime + samplingPeriod * iteration;
					}
					if (iteration < size) {
						span.addSample(nextSampleValue);
					}
				} else {
					valuesAdded++;

					int val = nextSampleValue;
					if (lastSampleTime != -1 && nextSampleTime > lastSampleTime) {
						val = (int) (lastSampleValue + ((outputTime - lastSampleTime)
								* (nextSampleValue - lastSampleValue))
								/ (nextSampleTime - lastSampleTime));
						lastOutput = outputTime;
					} else {
						lastOutput = nextSampleTime;
					}

					if (Math.abs(lastSampleTime - outputTime) < Math.abs(nextSampleTime - outputTime))
						val = lastSampleValue;
					else
						val = nextSampleValue;

					span.addSample(val);
				}
				inSpanCount++;
			}
		}

		logger.trace(M + ": Output time: " + (long) (outputTime - (valuesAdded + nullsAdded) * 
				samplingPeriod) +
				" -" + (long) outputTime + //" and offset: "
				//				+ (long) offset + 
				", versus " + (long)startTime + "-" + (long)endTime + " with " + 
				size + " iterations");
		logger.trace(M + ": Downsampling resulted in " + valuesAdded
				+ " samples, and " + nullsAdded + " gap entries, for total of "
				+ (valuesAdded + nullsAdded));

		logger.trace(M + ": " + path + " request is complete with " + results.size()
				+ " spans at " /*and " + pages + " pages at "*/ + actualPeriod
				+ " with scale " + scale + ". " + count + " periods and "
				+ inSpanCount + " samples.  Expected "
				+ (int)Math.ceil((endTime - startTime) / samplingPeriod));

		return results;
	}

	public static ArrayList<TimeSeriesData> decimateMinMax(double startTime, double endTime, double trueFreq, 
			long startTimeOffset, double samplingPeriod, double scale, TimeSeriesPage[] pageList,
			FilterSpec filter, ChannelSpecifier path, boolean minValIsNull, FilterBuffer workSpace,
			double filterPadBefore, double filterPadAfter) {

		final String M = "decimateMinMax()";

		ArrayList<TimeSeriesData> results = new ArrayList<>();
		double actualPeriod = 1.E6 / trueFreq;

		double outputTime = startTime;
		double lastSampleTime = -1;
		final long pagesStartTimeUutc = pageList[0].timeStart;

		int valuesAdded = 0;
		int nullsAdded = 0;
		int size = (int) Math.ceil((endTime - startTime) / samplingPeriod);
		if (size < 0) {
			size = 0;
		}
		logger.trace("{}: Trace should return {} samples", M, size);
		
		int count = 0;
		int inSpanCount = 0;

		if (filterPadBefore < samplingPeriod)
			filterPadBefore = samplingPeriod;
		if (filterPadAfter < samplingPeriod)
			filterPadAfter = samplingPeriod;

		synchronized (workSpace) {
			RetInfo resultsOfAntiAlias =
					decimateAntialias(startTime, endTime,
							trueFreq, startTimeOffset, samplingPeriod,
							scale, pageList, filter, path,
							minValIsNull, workSpace, filterPadBefore, filterPadAfter);

			int currentPage = resultsOfAntiAlias.page;
			int pos = resultsOfAntiAlias.pos;
			int startInx = resultsOfAntiAlias.bufferIndex;
			
			TimeSeriesData span = new TimeSeriesData(startTimeOffset, actualPeriod,
					scale, size*2);
			results.add(span);
			span.setPeriod(samplingPeriod);
			span.setMinMax(true);

			while (outputTime < pagesStartTimeUutc && outputTime < endTime) {
				span.addSample(null);
				span.addSample(null);
				outputTime = startTime + samplingPeriod * (++nullsAdded);
			}

			
			int index = pos;
			double lastOutput = -1;
			
			double nextSampleTime = pageList[currentPage].timeStart + pos++ * actualPeriod;
			int nextSampleValue = workSpace.buf[index++];


			int lastMinValue = Integer.MAX_VALUE;
			int lastMaxValue = Integer.MIN_VALUE;

			// outputTime = sample time we are looking for in the output
			for (int iteration = nullsAdded; iteration < size; iteration++) {
				outputTime = startTime + samplingPeriod * iteration;
				final double outputTimeEnd = startTime + samplingPeriod * (iteration + 1);

				// Read the next sample
				while (nextSampleTime < outputTimeEnd && currentPage < resultsOfAntiAlias.nonEmptyPages) {
					lastSampleTime = nextSampleTime;

					// If we are outside the page limits then
					// skip to the next page, repeatedly as long as the
					// page is empty
					while (currentPage < resultsOfAntiAlias.totalPages
							&& (pageList[currentPage] == null || pageList
							[currentPage].values.length <= pos)) {
						pos = 0;
						currentPage++;
					}
					if (currentPage < resultsOfAntiAlias.totalPages) {
						
						if (nextSampleTime >= outputTime) {
							if (nextSampleValue < lastMinValue)
								lastMinValue = nextSampleValue;
							if (nextSampleValue > lastMaxValue)
								lastMaxValue = nextSampleValue;
							count++;
						}
						nextSampleTime = pageList[currentPage].timeStart + pos
								* actualPeriod;

						nextSampleValue = workSpace.buf[index++];
						
						pos++;

						
					}				
				}

				if (currentPage >= resultsOfAntiAlias.totalPages) {
					if (lastOutput <= lastSampleTime) {
						nullsAdded++;
						span.addSample(null);
						span.addSample(null);
						lastMinValue = Integer.MAX_VALUE;
						lastMaxValue = Integer.MIN_VALUE;
					} else {
						span.addSample(lastMinValue);
						span.addSample(lastMaxValue);
						lastOutput = lastSampleTime;
						lastMinValue = Integer.MAX_VALUE;
						lastMaxValue = Integer.MIN_VALUE;
					}

				} else if ((lastSampleTime != -1 && nextSampleTime - lastSampleTime > actualPeriod * 2)
						|| (nextSampleValue == Integer.MIN_VALUE && minValIsNull)) {
					while (outputTime < nextSampleTime && iteration < size) {
						nullsAdded++;
						iteration++;
						span.addSample(null);
						span.addSample(null);

						outputTime = startTime + samplingPeriod * iteration;
					}
					if (iteration < size) {
						span.addSample(lastMinValue);
						span.addSample(lastMaxValue);
					}
					lastMinValue = Integer.MAX_VALUE;
					lastMaxValue = Integer.MIN_VALUE;
				} else {
					valuesAdded++;

					if (lastSampleTime != -1 && nextSampleTime > lastSampleTime) {
						lastOutput = outputTime;
					} else {
						index++;
						lastOutput = nextSampleTime;
					}

					span.addSample(lastMinValue);
					span.addSample(lastMaxValue);
					lastMinValue = Integer.MAX_VALUE;
					lastMaxValue = Integer.MIN_VALUE;
				}
				inSpanCount++;
			}
		}

		logger.trace(M + ": Output time: " + (long) (outputTime - (valuesAdded + nullsAdded) * 
				samplingPeriod) +
				" -" + (long) outputTime + //" and offset: "
				//				+ (long) offset + 
				", versus " + (long)startTime + "-" + (long)endTime + " with " + 
				size + " iterations");
		logger.trace(M + ": Downsampling resulted in " + valuesAdded
				+ " samples, and " + nullsAdded + " gap entries, for total of "
				+ (valuesAdded + nullsAdded));

		logger.trace(M + ": " + path + " request is complete with " + results.size()
				+ " spans at " /*and " + pages + " pages at "*/ + actualPeriod
				+ " with scale " + scale + ". " + count + " periods and "
				+ inSpanCount + " samples.  Expected "
				+ (int)Math.ceil((endTime - startTime) / samplingPeriod));

		return results;
	}	

//	private static TimeSeriesFilterButterworth getFilterFor(double targetFrequency,
//			FilterSpec fs) throws IllegalFilterException {
//		final String M = "getFilterFor()";
//		fs.setAt((int) targetFrequency);
//		TimeSeriesFilterButterworth ret = filterCache.get(fs);
//
//		try {
//			if (ret == null) {
//				ret = new TimeSeriesFilterButterworth(targetFrequency, fs);
//				filterCache.put(fs, ret);
//			} // else
//				// System.out.println("Found filter");
//		} catch (org.apache.commons.math.linear.SingularMatrixException e){
//			logger.error(M + ": Caught, ignoring", e);
//		}
//		return ret;
//	}

}
