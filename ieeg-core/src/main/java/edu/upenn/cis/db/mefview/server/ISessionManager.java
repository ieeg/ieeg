/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;

/**
 * Manages {@code SessionToken}s
 * 
 * @author John Frommeyer
 * 
 */
public interface ISessionManager {

	/**
	 * Returns a token which identifies a newly created session with
	 * {@code user} as the owner.
	 * 
	 * @param user
	 * @return a token which identifies a newly created session with
	 *         {@code user} as the owner
	 */
	public SessionToken createSession(User user);

	/**
	 * Sets the given session's status to logged out.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	public void logoutSession(SessionToken token);

	/**
	 * Sets the given session's status to indicate that the user's account has
	 * been deleted or disabled.
	 * 
	 * @param token
	 * @throws SessionNotFoundException if no session with the given token is
	 *             found
	 */
	public void disableAcctSession(SessionToken token);

	/**
	 * Returns the {@code UserId} of the user who owns the active session with
	 * the given token.
	 * 
	 * @param token
	 * @return the {@code UserId} of the user who owns the active session with
	 *         the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 * @throws SessionExpirationException if the session with the given token
	 *             has timed out
	 * @throws SessionExpirationException if the session with the given token
	 *             has been logged out
	 * @throws DisabledAccountException if the user's account has been disabled
	 */
	public UserId authenticateSession(SessionToken token);

	/**
	 * Returns the {@link UserIdSessionStatus} for the given token. If the
	 * returned {@link SessionStatus} is {@link SessionStatus.ACTIVE} then the
	 * session's last access date was updated.
	 * 
	 * @param token
	 * @return the {@code UserIdSessionStatus} for the given token
	 * @throws SessionNotFoundException if no session with the given token
	 *             exists
	 */
	public UserIdSessionStatus getUserIdSessionStatus(SessionToken token);

}
