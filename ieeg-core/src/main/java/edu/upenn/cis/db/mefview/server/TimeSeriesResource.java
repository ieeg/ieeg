/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToAuthzFailure;
import static edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil.logAndConvertToInternalError;
import static java.util.Collections.singleton;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.Striped;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.StaleDataException;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundByDatasetAndIdException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.braintrust.webapp.IeegFilter;
import edu.upenn.cis.db.mefview.server.exceptions.ServerBusyException;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.exceptions.TooManyRequestsException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.MEFDataAndPageNos;
import edu.upenn.cis.db.mefview.server.mefpageservers3.TooMuchDataRequestedException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.CountsByLayer;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.ITimeSeriesResource;
import edu.upenn.cis.db.mefview.services.IeegWsException;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.RevisionIdList;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotationList;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetailList;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetails;
import edu.upenn.cis.db.mefview.services.TimeSeriesIdAndDCheck;
import edu.upenn.cis.db.mefview.services.TimeSeriesIdAndDChecks;
import edu.upenn.cis.db.mefview.services.TimeSeriesList;
import edu.upenn.cis.db.mefview.services.TimeSeriesRequestList;
import edu.upenn.cis.db.mefview.services.TsAnnotationsDeleted;
import edu.upenn.cis.db.mefview.services.TsAnnotationsMoved;
import edu.upenn.cis.db.mefview.services.VersionOkay;
import edu.upenn.cis.db.mefview.services.VersionString;
import edu.upenn.cis.db.mefview.services.assembler.TimeSeriesAssembler;
import edu.upenn.cis.db.mefview.services.assembler.TsAnnotationAssembler;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;

/**
 * Top level service methods should only throw instances of
 * {@link IeegWsException} so that the error can be mapped to a proper response
 * and handled by the client.
 */
public class TimeSeriesResource implements ITimeSeriesResource {

	private TraceServer traces = TraceServerFactory.getTraceServer();
	private IDataSnapshotServer dataSnapshotServer =
			DataSnapshotServerFactory.getDataSnapshotServer();
	private IUserService userService = UserServiceFactory.getUserService();
	private final Semaphore downloadSemaphoresAllUsers =
			TransferSemaphoresFactory.getTransferSemaphoreAllUsers();
	private final Striped<Semaphore> downloadSemaphoresPerUser =
			TransferSemaphoresFactory.getTransferSemaphoresPerUser();
	private HttpServletRequest httpServletRequest;
	
	private static IEventServer eventServer = null;

	private static Cache<String, TimeSeriesDetails> traceDetails = CacheBuilder.newBuilder()
			.maximumSize(500)
			.expireAfterWrite(1, TimeUnit.DAYS)
			.build();

	private final TimeSeriesAssembler tsAssembler;
	private final TsAnnotationAssembler annotationAssembler;

	private static final Logger logger =
			LoggerFactory.getLogger(TimeSeriesResource.class);

	private final User user;

	/**
	 * Constructor: initialize parameters and MEF page server, based on servlet
	 * context parameters
	 */
	public TimeSeriesResource(@Context HttpServletRequest req) {
		String m = "TimeSeriesResource(...)";
		this.httpServletRequest = req;
		this.tsAssembler = new TimeSeriesAssembler();
		this.annotationAssembler = new TsAnnotationAssembler(tsAssembler);
		this.user = (User) req.getAttribute("user");
		logger.debug("{}: user: ", m, (user == null ? "No user set" : user.getUsername()));
		if (eventServer == null)
			try {
				Class<IEventServer> c = 
						(Class<IEventServer>) Class.forName("com.blackfynn.dsp.server.timeseries.EventTimeSeriesServer");
				eventServer = c.newInstance();
			} catch (Exception e) {
//				e.printStackTrace();
			}
	}

	@Override
	public VersionOkay isVersionOkay(
			String matlabClientVersion) {
		String m = "isVersionOkay(...)";
		try {
			VersionString clientVersion = new VersionString(matlabClientVersion);
			String leastOkayVersionString = IvProps
					.getLeastOkayMatlabClientVersion();
			VersionString leastOkayVersion = new VersionString(
					leastOkayVersionString);
			if (clientVersion.compareTo(leastOkayVersion) >= 0) {
				return new VersionOkay(true);
			}
			return new VersionOkay(false);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	// /////////////////////// Lookup for dataset contents

	@Override
	public TimeSeriesList getDataSnapshotTimeSeries(
			String datasetId) {
		final String m = "getDataSnapshotTimeSeries(...)";
		try {
			DataSnapshot dss = dataSnapshotServer.getDataSnapshot(
					user,
					datasetId);
			Set<TimeSeries> rl =
					tsAssembler
							.toTimeSeriesSortedSet(dss.getTimeSeries());
			return new TimeSeriesList(rl);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public TimeSeriesDetailList getDataSnapshotTimeSeriesDetails(
			final String dataSnapshot) {
		final String m = "getDataSnapshotTimeSeriesDetails()";
		try {
			return getDataSnapshotTimeSeriesDetailsHelper(
					user.getUsername(),
					dataSnapshot);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(e, m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	private TimeSeriesDetailList getDataSnapshotTimeSeriesDetailsHelper(
			String username,
			String dataSnapshot) {
		final String m = "getDataSnapshotTimeSeriesDetailsHelper(...)";
		final long startMethod = System.nanoTime();
		DataSnapshot dss = null;
		try {
			Set<TimeSeriesDetails> ret = new HashSet<TimeSeriesDetails>();
			User user = userService.findUserByUsername(username);
			dss = dataSnapshotServer.getDataSnapshot(user, dataSnapshot);

			if (dss == null)
				throw new DataSnapshotNotFoundException(dataSnapshot);

			Set<TimeSeries> tss =
					tsAssembler
							.toTimeSeriesSortedSet(dss.getTimeSeries());

			SnapshotSpecifier snap =
					traces.getSnapshotSpecifier(user, dataSnapshot);
			final Set<String> tsIds = new HashSet<>();
			for (TimeSeries ts : tss) {
				tsIds.add(ts.getRevId());
			}
			final ImmutableMap<String, TimeSeriesDetails> cached = traceDetails.getAllPresent(tsIds);
			ret.addAll(cached.values());
			if (ret.size() != tsIds.size()) {
				final Map<String, TimeSeries> idToTimeSeries = new HashMap<>();
				for (TimeSeries ts : tss) {
					if (!cached.keySet().contains(ts.getRevId())) {
						idToTimeSeries.put(ts.getRevId(), ts);
					}
				}
				final Map<String, ChannelSpecifier> loaded = traces
						.getChannelSpecifiers(user, snap,
								idToTimeSeries.keySet(), 0);
				for (Entry<String, ChannelSpecifier> loadedEntry : loaded
						.entrySet()) {
					final ChannelSpecifier tracePath = loadedEntry.getValue();
					final TimeSeries ts = idToTimeSeries.get(loadedEntry
							.getKey());
					logger.trace("Scanning trace " + tracePath);
					final TimeSeriesDetails det = new TimeSeriesDetails(
							traces.getStartUutc(tracePath),
							traces.getEndUutc(tracePath),
							traces.getDuration(tracePath),
							traces.getSampleRate(tracePath), traces
									.getVoltageConversionFactor(tracePath),
							traces.getMinSampleValue(tracePath), traces
									.getMaxSampleValue(tracePath),
							traces
									.getAcquisitionSystem(tracePath),
							traces.getChannelName(tracePath),
							ts.getLabel(),
							ts.getRevId(),
							traces.getDataSignature(tracePath),
							traces.getNumberOfSamples(tracePath));
					ret.add(det);
					traceDetails.put(ts.getRevId(), det);
				}
			}

			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.WS_TIME_SERIES_DETAILS,
							dataSnapshot);
			IeegFilter.setUserAndDataUsage(httpServletRequest, user,
					dataUsage);
			return new TimeSeriesDetailList(ret);
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		} finally {
			logger.info("{}: {} {} {} seconds",
					new Object[] {
							m,
							username,
							dss != null ? dss.getLabel() : null,
							BtUtil.diffNowThenSeconds(startMethod) });
		}
	}

	@Override
	public void setDataSnapshotName(
			String dataSnapshotId,
			String originalName,
			String newName) {
		String m = "setDataSnapshotName(...)";
		try {
			dataSnapshotServer.setDataSnapshotName(
					user,
					dataSnapshotId, originalName, newName);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (StaleDataException e) {
			throw IeegWsExceptionUtil.logAndConvertToStaleDataException(
					e,
					m,
					logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(e, m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public TimeSeriesAnnotationList getTsAnnotations(
			String dataSnapshotId,
			long startOffsetUsecs,
			String layer,
			@Nullable Boolean ltStartOffsetUsecs,
			int firstResult,
			int maxResults) {
		String m = "getTsAnnotations(...)";
		try {
			checkArgument(
					maxResults <= 1000,
					"MaxCount [%s] exceeds limit of 1000",
					maxResults);

			List<TsAnnotationDto> tsAnns = null;

			if (ltStartOffsetUsecs == null || !ltStartOffsetUsecs) {
				tsAnns = dataSnapshotServer
						.getTsAnnotations(
								user,
								dataSnapshotId,
								startOffsetUsecs,
								layer,
								firstResult,
								maxResults);
			} else {
				tsAnns =
						dataSnapshotServer
								.getTsAnnotationsLtStartTime(
										user,
										dataSnapshotId,
										startOffsetUsecs,
										layer,
										firstResult,
										maxResults);
			}
			List<TimeSeriesAnnotation> rl = new ArrayList<TimeSeriesAnnotation>();

			rl.addAll(annotationAssembler
					.toTimeSeriesAnnotationList(tsAnns));

			return new TimeSeriesAnnotationList(rl);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e, m, logger);
		} catch (TimeSeriesNotFoundByDatasetAndIdException tse) {
			// TODO: specialized error message
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					tse,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	// /////////////////////// Retrieve time series
	@Override
	public Response getUnscaledTimeSeriesSetBinary(
			String dataset,
			TimeSeriesRequestList timeSeriesRequestList,
			double start,
			double duration,
			@Nullable Double frequency,
			@Nullable String processingString) {
		long in = System.nanoTime();
		String m = "getUnscaledTimeSeriesSetBinary(...)";
		int bytesWriten = -1;
		try {
			
			double samplingPeriod = -1;
			if(null!=frequency){
				samplingPeriod = 1.E6 / frequency;
			}
			
//			double samplingPeriod = 1.E6 / frequency;
			final List<String> revIds = timeSeriesRequestList
					.getRevisionIds()
					.getRevIds();
			final List<ChannelSpecifier> channels = traces.getChannels(
					user, dataset, revIds, samplingPeriod);

			// TODO: parse JSON string to get out the processing step
			final List<List<TimeSeriesData>> mefMatrix;
			if (frequency==null){
								
				mefMatrix = traces.getTimeSeriesSetRaw(
						user, 
						channels, 
						start, 
						duration, 
						1, 
						null);

			}else{
				
				final List<FilterSpec> filters = timeSeriesRequestList
						.getFilterSpecs();
				
				final DataSnapshotSearchResult data = dataSnapshotServer.getDataSnapshotForId(user, dataset);
				mefMatrix = traces
						.getTimeSeriesSet(
								user,
								eventServer,
								data,
								channels,
								start, duration, frequency, filters, null,
								null,// processing,
								null);
			}
			
			
			
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			DataOutputStream dos = new DataOutputStream(bos);
			List<Double> voltConvFactorsMv = newArrayList();
			List<Integer> samplesPerRow = newArrayList();
			List<Double> sampleRates = traces.getSampleRates(channels);
			
			for (List<TimeSeriesData> tsDatas : mefMatrix) {
				TimeSeriesData tsData = getOnlyElement(tsDatas);
				voltConvFactorsMv.add(tsData.getScale());
				samplesPerRow.add(tsData.getSeriesLength());
				for (int j = 0; j < tsData.getSeriesLength(); j++) {
					if (tsData.isInGap(j)) {
						dos.writeInt(Integer.MIN_VALUE);
					} else {
						dos.writeInt(tsData.getAt(j));
					}
				}
			}

			
			Joiner joiner = Joiner.on(',');
			String sampleRatesStr = joiner.join(sampleRates);

			byte[] respData = bos.toByteArray();
			bytesWriten = respData.length;
			Response resp = Response
					.ok(new ByteArrayInputStream(respData))
					.header(SAMPLE_RATES, sampleRatesStr)
					.header(VOLTAGE_CONVERSION_FACTORS_MV,
							joiner.join(voltConvFactorsMv))
					.header(SAMPLES_PER_ROW, joiner.join(samplesPerRow))
					.build();
			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.WS_UNSCALED_RAW_BINARY,
							dataset,
							(long) start,
							(long) duration);
			IeegFilter.setUserAndDataUsage(httpServletRequest, user,
					dataUsage);

			return resp;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (TooMuchDataRequestedException e) {
			throw IeegWsExceptionUtil.logAndConvertToTooMuchDataRequested(
					e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(e, m,
					logger);
		} catch (ServerTimeoutException e) {
			throw IeegWsExceptionUtil.logAndConvertToServerTimeout(
					e,
					m,
					logger);
		} catch (IOException ioe) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					ioe,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		} finally {
			logger.info(
					"{}: user {} start {} end {} response bytes {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							start,
							start + duration,
							bytesWriten,
							BtUtil.diffNowThenSeconds(in) });

		}
	}

	@Override
	public TsAnnotationsMoved moveTsAnnotations(
			String datasetId,
			String fromLayerName,
			String toLayerName) {
		String m = "setLayerName(...)";
		try {
			int moved = dataSnapshotServer.moveTsAnnotations(
					user,
					datasetId,
					fromLayerName,
					toLayerName);
			return new TsAnnotationsMoved(moved);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public String getDataSnapshotIdByName(
			String dsName) {
		String m = "getDataSnapshotIdByName(...)";
		try {
			return dataSnapshotServer.getDataSnapshotId(user, dsName, false);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public String getOwnedDataSnapshotNames(String creator) {
		String m = "getDataSnapshotNames(...)";
		try {

			User optUser = null;

			if (!creator.equals("any"))
				optUser = userService.findUserByUsername(creator);

			StringBuilder sb = new StringBuilder();
			Set<String> names = dataSnapshotServer.getDataSnapshotNames(user,
					optUser);
			boolean first = true;
			for (String nam : names) {
				if (!first)
					sb.append(",");
				else
					first = false;
				sb.append(nam);
			}
			return sb.toString();
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			System.err.println("Error");
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public CountsByLayer getCountsByLayer(
			String datasetId) {
		long in = System.nanoTime();
		String m = "getCountsByLayer(...)";
		Optional<CountsByLayer> countsByLayer = Optional.absent();
		try {
			countsByLayer = Optional.of(
					new CountsByLayer(dataSnapshotServer.countLayers(user,
							datasetId)));
			logger.debug("{}: countsByLayer: {}", m, countsByLayer.get()
					.getCountsByLayer());
			return countsByLayer.get();
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} finally {
			logger.info("{}: returning {} results {} seconds",
					new Object[] {
							m,
							countsByLayer.isPresent()
									? countsByLayer.get().getCountsByLayer()
											.size()
									: null,
							BtUtil.diffNowThenSeconds(in)
					});
		}
	}

	@Override
	public TsAnnotationsDeleted removeTsAnnotationsByLayer(
			String datasetId,
			String layer) {
		String m = "removeTsAnnotationsByLayer(...)";
		try {
			return new TsAnnotationsDeleted(
					dataSnapshotServer
							.removeTsAnnotationsByLayer(
									user,
									datasetId,
									layer));
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public String test(final String parm) {
		return "Test succeeded: " + parm;
	}

	@Override
	public String addAnnotationsToDataSnapshot(
			String dataSnapshot,
			TimeSeriesAnnotationList annotations) {
		String m = "addAnnotationsToDataSnapshot(...)";
		try {
			final Set<TsAnnotationDto> tsAnnotationDtoSet = annotationAssembler
					.toTsAnnotationDtoSet(annotations.getList());
			return traces.saveTimeSeriesAnnotationDtos(
					user,
					dataSnapshot,
					tsAnnotationDtoSet);
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (TimeSeriesNotFoundInDatasetException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchTimeSeriesFailure(
					e,
					m,
					logger);
		} catch (BadTsAnnotationTimeException e) {
			throw IeegWsExceptionUtil.logAndConvertToBadTsAnnTime(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw logAndConvertToInternalError(t, m, logger);
		} catch (Error e) {
			throw logAndConvertToInternalError(e, m, logger);
		}
	}

	@Override
	public String deriveEmptyDataSnapshot(
			final String friendlyName,
			final String previousRevId, final String toolName) {
		final String m = "deriveEmptyDataSnapshot()";
		try {
			final Set<String> emptySet = Collections.emptySet();
			final String resultsDataSnapshotRevId = dataSnapshotServer
					.deriveDataset(
							user,
							previousRevId,
							friendlyName,
							toolName,
							emptySet, emptySet);
			return resultsDataSnapshotRevId;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public String deriveDataSnapshot(
			String friendlyName,
			RevisionIdList revisionIdList,
			String previousRevId,
			String toolName) {
		String m = "deriveDataSnapshot()";
		try {
			final String resultsDataSnapshotRevId = dataSnapshotServer
					.deriveDataset(
							user,
							previousRevId,
							friendlyName,
							toolName,
							newHashSet(
							revisionIdList.getRevIds()),
							null);
			return resultsDataSnapshotRevId;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(e, m,
					logger);
		} catch (RuntimeException t) {
			throw logAndConvertToInternalError(t, m, logger);
		} catch (Error t) {
			throw logAndConvertToInternalError(t, m, logger);
		}
	}

	@Override
	public String deriveDataSnapshot(
			final String friendlyName,
			final String previousRevId, final String toolName) {
		String m = "deriveDataSnapshot(...)";
		try {
			String resultsDataSnapshotRevId = dataSnapshotServer.deriveDataset(
					user, previousRevId,
					friendlyName,
					toolName, null, null);
			return resultsDataSnapshotRevId;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(e, m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);

		}
	}

	@Override
	public Response getAnnotationsListCSV(
			final long timestamp,
			final String signature,
			final String dataSnapshot) {
		final String m = "getAnnotationsListCSV(...)";
		try {
			return Response
					.ok(
							traces.getAnnotationsCsv(
									user,
									dataSnapshot))
					.header("Content-Disposition",
							"attachment; filename=\"" + dataSnapshot
									+ ".csv\"")
					.build();

		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (IOException e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		}
	}

	@Override
	public Response getTraceListCSV(
			String dataSnapshotId,
			String channelList,
			long start,
			long duration,
			double frequency,
			String filterType,
			int filter,
			int numPoles,
			int bandpassLowCutoff,
			int bandpassHighCutoff,
			int bandstopLowCutoff,
			int bandstopHighCutoff
			) {
		long inNanos = System.nanoTime();
		Date timeOfRequest = new Date();
		final String M = "getTraceListCSV(...)";
		try {
			List<String> channels = new ArrayList<String>();

			String str = channelList;

			while (str.contains(",")) {
				String ch = str.substring(0, str.indexOf(','));
				str = str.substring(str.indexOf(',') + 1);
				channels.add(ch);
			}
			channels.add(str);

			DisplayConfiguration fp = new DisplayConfiguration(filterType, filter,
					numPoles,
					bandpassLowCutoff,
					bandpassHighCutoff, bandstopLowCutoff,
					bandstopHighCutoff);

			DataSnapshotSearchResult dsSearchResult =
					getOnlyElement(dataSnapshotServer.getLatestSnapshots(user,
							singleton(dataSnapshotId)));
			String dsLabel = dsSearchResult.getLabel();

			return Response
					.ok(
							traces.getTracesCSV(
									user,
									dataSnapshotId,
									eventServer,
									channels, start, duration, frequency,
									fp, timeOfRequest, inNanos,
									httpServletRequest
									))
					.header("Content-Disposition",
							"attachment; filename=\"" + dsLabel
									+ ".csv.gz\"")
					.build();
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, M, logger);
		} catch (RuntimeException rte) {
			throw logAndConvertToInternalError(rte, M, logger);
		} catch (Error e) {
			throw logAndConvertToInternalError(e, M, logger);
		}
	}

	@Override
	public Response getUnscaledTimeSeriesSetRawRed(
			String dataset,
			TimeSeriesIdAndDChecks timeSeriesIdAndDChecks,
			double start,
			double duration) {
		long in = System.nanoTime();
		String m = "getUnscaledTimeSeriesSetRawRed(...)";
		int bytesWritten = -1;
		try {

			AuthCheckFactory.getHandler().checkPermitted(
					dataSnapshotServer.getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							dataset,
							true),
					CorePermDefs.READ,
					dataset);

			final List<String> timeSeriesIds = newArrayList();

			for (TimeSeriesIdAndDCheck timeSeriesIdAndVer : timeSeriesIdAndDChecks) {
				timeSeriesIds.add(timeSeriesIdAndVer.getId());
			}

			final List<ChannelSpecifier> channels = traces.getChannels(
					user,
					dataset,
					timeSeriesIds,
					-1);

			List<MEFDataAndPageNos> redDatas = traces.getTimeSeriesSetRed(
					user,
					channels,
					start,
					duration);

			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			List<Integer> lengths = newArrayList();
			List<Long> mefBlockNos = newArrayList();

			for (MEFDataAndPageNos redData : redDatas) {
				bos.write(redData.getData());
				lengths.add(redData.getData().length);
				mefBlockNos.add(redData.getPageNoRange().lowerEndpoint());
				mefBlockNos.add(redData.getPageNoRange().upperEndpoint());
			}

			byte[] respData = bos.toByteArray();
			bytesWritten = respData.length;
			Joiner joiner = Joiner.on(',');
			String lengthsStr = joiner.join(lengths);

			List<Double> sampleRates = traces.getSampleRates(channels);
			List<Double> voltageConversionFactorsMv = newArrayList();

			for (ChannelSpecifier channel : channels) {
				voltageConversionFactorsMv
						.add(traces.getVoltageConversionFactor(channel));
			}

			List<Long> startsUutc = newArrayList();
			for (ChannelSpecifier channel : channels) {
				startsUutc.add(traces.getStartUutc(channel));
			}

			String sampleRatesStr = joiner.join(sampleRates);
			String voltageConversionFactorsMvStr =
					joiner.join(voltageConversionFactorsMv);
			String startTimesUutcStr = joiner.join(startsUutc);
			String mefBlockNosStr = joiner.join(mefBlockNos);
			logger.debug("{}: block nos: {}", m, mefBlockNosStr);

			Response resp = Response
					.ok(new ByteArrayInputStream(respData))
					.header(SAMPLE_RATES, sampleRatesStr)
					.header(ROW_LENGTHS, lengthsStr)
					.header(VOLTAGE_CONVERSION_FACTORS_MV,
							voltageConversionFactorsMvStr)
					.header(START_TIMES_UUTC, startTimesUutcStr)
					.header(MEF_BLOCK_NOS, mefBlockNosStr)
					.build();
			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.WS_UNSCALED_RAW_RED,
							dataset,
							(long) start,
							(long) duration);
			IeegFilter.setUserAndDataUsage(httpServletRequest, user,
					dataUsage);
			return resp;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (TooMuchDataRequestedException e) {
			throw IeegWsExceptionUtil
					.logAndConvertToTooMuchDataRequested(
							e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(e, m,
					logger);
		} catch (StaleMEFException e) {
			throw IeegWsExceptionUtil.logAndConvertToStaleMef(
					e,
					m,
					logger);
		} catch (ServerTimeoutException e) {
			throw IeegWsExceptionUtil.logAndConvertToServerTimeout(
					e,
					m,
					logger);
		} catch (IOException ioe) {
			throw logAndConvertToInternalError(ioe, m, logger);
		} catch (RuntimeException rte) {
			throw logAndConvertToInternalError(rte, m, logger);
		} catch (Error e) {
			throw logAndConvertToInternalError(e, m, logger);
		} finally {
			logger.info(
					"{}: user {} start {} end {} response bytes {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							start,
							start + duration,
							bytesWritten,
							BtUtil.diffNowThenSeconds(in) });
		}
	}

	@Override
	public Response getTraceListRawCSV(
			final String dataSnapshotId,
			final String channelList,
			final long start,
			final long duration,
			final int sampleFactor
			) {
		long timeOfRequestNanos = System.nanoTime();
		Date timeOfRequest = new Date();
		String m = "getTraceListRawCSV(...)";
		try {
			List<String> channels = new ArrayList<String>();

			String str = channelList;

			while (str.contains(",")) {
				String ch = str.substring(0, str.indexOf(','));
				str = str.substring(str.indexOf(',') + 1);
				channels.add(ch);
			}
			channels.add(str);

			DataSnapshotSearchResult dsSearchResult =
					getOnlyElement(dataSnapshotServer.getLatestSnapshots(user,
							singleton(dataSnapshotId)));
			String dsLabel = dsSearchResult.getLabel();

			return Response
					.ok(
							traces.getTracesRawCSV(
									user,
									dataSnapshotId,
									channels, start, duration,
									sampleFactor,
									timeOfRequest,
									timeOfRequestNanos,
									httpServletRequest))
					.header("Content-Disposition",
							"attachment; filename=\"" + dsLabel
									+ ".csv.gz\"").
					build();
		} catch (AuthorizationException ae) {
			throw logAndConvertToAuthzFailure(ae, m, logger);
		} catch (RuntimeException rte) {
			throw logAndConvertToInternalError(rte, m, logger);
		} catch (Error e) {
			throw logAndConvertToInternalError(e, m, logger);
		}
	}

	@Override
	public Response getUnscaledTimeSeriesSetBinaryRaw(
			String dataSnapshot,
			TimeSeriesIdAndDChecks timeSeriesIdAndDChecks,
			//TimeSeriesRequestList seriesNames,
			double start,
			double duration) {
		
		List<FilterSpec> filters = new ArrayList<FilterSpec>();
		List<String> revIds = new ArrayList<String>();
		Iterator<TimeSeriesIdAndDCheck> idIterator = timeSeriesIdAndDChecks.iterator();
		while(idIterator.hasNext()) {
	         filters.add(new FilterSpec());
	         revIds.add(idIterator.next().getId());
	      }
	      System.out.println();
		
		
	      
//		TimeSeriesRequestList(final List<String> revIds,
//				final List<FilterSpec> filters) 
//		
		TimeSeriesRequestList seriesNames = new TimeSeriesRequestList(revIds,filters);
		return getUnscaledTimeSeriesSetBinary(
				dataSnapshot,
				seriesNames,
				start,
				duration,
				null,
				null);
	}
	
	@Override
	public Response getUnscaledTimeSeriesSetBinary(
			String dataSnapshot,
			TimeSeriesRequestList seriesNames,
			double start,
			double duration,
			Double frequency) {
		return getUnscaledTimeSeriesSetBinary(
				dataSnapshot,
				seriesNames,
				start,
				duration,
				frequency,
				null);
	}

	@Override
	public Response getTimeSeriesContent(
			String timeSeriesId,
			String eTag,
			String range) {
		final String m = "getTimeSeriesContent(...)";
		long in = System.nanoTime();
		String fileKey = null;
		try {
			Semaphore userSemaphore = downloadSemaphoresPerUser.get(user);

			try {
				if (!downloadSemaphoresAllUsers.tryAcquire(
						15,
						TimeUnit.SECONDS)) {
					throw new ServerBusyException(
							"max total simultaneous downloads exceeded, user "
									+ user.getUsername());
				}
				IeegFilter.setTransferAllUsersSemaphore(
						httpServletRequest,
						downloadSemaphoresAllUsers);

				if (!userSemaphore.tryAcquire(
						15,
						TimeUnit.SECONDS)) {
					throw new TooManyRequestsException(
							"no more downloads avaliable for user "
									+ user.getUsername());
				}
				IeegFilter.setTransferPerUserSemaphore(
						httpServletRequest,
						userSemaphore);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				logger.error(m
						+ ": Interrupted while waiting for userSemaphore", e);
				throw new TooManyRequestsException(
						"could not aquire download lock for user "
								+ user.getUsername());
			}

			DataSnapshotId recordingId = dataSnapshotServer
					.getRecordingIdForTimeSeries(
							user,
							timeSeriesId);
			Set<String> timeSeriesIdAsSet = Collections.singleton(timeSeriesId);
			List<String> keys = dataSnapshotServer.getMEFPaths(
					user,
					recordingId.getId(),
					timeSeriesIdAsSet);
			fileKey = getOnlyElement(keys);
			Long startByteOffset = null;
			Long endByteOffset = null;

			if (range != null) {
				ByteRange byteRange = ByteRange.parse(range);
				startByteOffset = byteRange.getStart();
				if (byteRange.hasEnd()) {
					endByteOffset = byteRange.getEnd();
				}
			}

			final InputStream is =
					traces.getData(
							fileKey,
							eTag,
							startByteOffset,
							endByteOffset);
			Response resp = null;
			if (range == null) {
				resp = Response
						.ok(is)
						.build();
			} else {
				resp = Response
						.status(Status.PARTIAL_CONTENT)
						.entity(is)
						.build();
			}
			DataUsageEntity dataUsage =
					new DataUsageEntity(
							DataRequestType.WS_DIRECT_DATA,
							recordingId.getId());
			IeegFilter.setUserAndDataUsage(
					httpServletRequest,
					user,
					dataUsage);
			return resp;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(
					e,
					m,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					m,
					logger);
		} catch (TimeSeriesNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchTimeSeriesFailure(
					e,
					m,
					logger);
		} catch (ServerBusyException sbe) {
			throw IeegWsExceptionUtil.logAndConvertToServerBusyException(
					sbe,
					m,
					logger);
		} catch (TooManyRequestsException e) {
			throw IeegWsExceptionUtil.logAndConvertToTooManyRequests(
					e,
					m,
					logger);
		} catch (RuntimeException e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					m,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					m,
					logger);
		} finally {
			logger.info(
					"{}: user {} file {} range {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							fileKey,
							range,
							BtUtil.diffNowThenSeconds(in) });

		}

	}

	@Override
	public Response getEdfHeaderRaw(String datasetId) {
		final String m = "getEdfHeaderRaw(...)";
		long in = System.nanoTime();
		long sizeGuessBytes = -1;
		try {

			AuthCheckFactory.getHandler().checkPermitted(
					dataSnapshotServer.getPermissions(
							user,
							HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME,
							datasetId,
							true),
					CorePermDefs.READ,
					datasetId);
			final DataSnapshot dataset = dataSnapshotServer
					.getDataSnapshot(user, datasetId);
			final String datasetName = dataset.getLabel();
			final AmazonS3 s3 = AwsUtil.getS3();
			final String bucket = "org-ieeg-inbox";
			final String keyPrefix =
					"960/Animal_Data/Neuropace/"
							+ datasetName
							+ "/";
			final ObjectListing objectListing = s3.listObjects(bucket,
					keyPrefix);
			final List<S3ObjectSummary> objectSummaries = objectListing
					.getObjectSummaries();
			if (objectSummaries.isEmpty()) {
				throw new IllegalArgumentException("No files found in "
						+ keyPrefix);
			}
			if (objectSummaries.size() > 1) {
				logger.info("{}: Found {} files in {}. Ignoring all but first",
						m, objectSummaries.size(), keyPrefix);
			}
			final S3ObjectSummary summary = getFirst(objectSummaries, null);
			final String eTag = summary.getETag();
			final String key = summary.getKey();

			// Add a channel in case of EDF annotation channel
			sizeGuessBytes = 256
					+ 256 * (dataset.getTimeSeries()
					.size() + 1);
			final InputStream edfContent = AwsUtil.getObjectContent(
					s3,
					bucket,
					key,
					eTag,
					0L,
					sizeGuessBytes);
				Response resp = Response
						.ok(edfContent)
						.build();
				return resp;
		} catch (AuthorizationException e) {
			throw logAndConvertToAuthzFailure(e, m, logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(e, m,
					logger);
		} catch (RuntimeException rte) {
			throw logAndConvertToInternalError(rte, m, logger);
		} catch (Error e) {
			throw logAndConvertToInternalError(e, m, logger);
		} finally {
			logger.info(
					"{}: user {} response bytes {} total {} seconds",
					new Object[] {
							m,
							user.getUsername(),
							sizeGuessBytes,
							BtUtil.diffNowThenSeconds(in) });
		}

	}
}
