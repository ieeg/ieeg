/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import org.slf4j.Logger;

import edu.upenn.cis.braintrust.security.AuthenticationException;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.StaleDataException;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetConflictException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.FailedVersionMatchException;
import edu.upenn.cis.braintrust.shared.exception.InvalidRangeException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundException;
import edu.upenn.cis.db.mefview.server.exceptions.InboxCredentialsConflictException;
import edu.upenn.cis.db.mefview.server.exceptions.ServerBusyException;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.server.exceptions.TooManyRequestsException;
import edu.upenn.cis.db.mefview.server.exceptions.UploadInProgressException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.TooMuchDataRequestedException;

/**
 * Convert and log exceptions to {@code IeegWsException}s.
 * 
 * @author John Frommeyer
 * 
 */
public class IeegWsExceptionUtil {
	private IeegWsExceptionUtil() {
		throw new AssertionError("This class cannot be instantiated.");
	}

	public static IeegWsException logAndConvertToAuthzFailure(
			AuthorizationException ae,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException("Access Denied",
				IeegWsError.AUTHORIZATION_FAILURE);
		logger.error(
				callingMethodName
						+ ": User not authorized to perform this operation, errorId = "
						+ wse.getErrorId(),
				ae);

		return wse;
	}

	public static IeegWsException logAndConvertToServerTimeout(
			ServerTimeoutException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				"server was unable to answer the request before the time out, please try again",
				IeegWsError.SERVER_TIMEOUT);
		logger.error(
				callingMethodName
						+ ": server busy, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToServerBusyException(
			ServerBusyException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				"the server was too busy to process your request",
				IeegWsError.SERVER_BUSY);
		logger.error(
				callingMethodName
						+ ": server busy, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToTooManyRequests(
			TooManyRequestsException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				"too many simultaneous requests",
				IeegWsError.TOO_MANY_REQUESTS);
		logger.error(
				callingMethodName
						+ ": server busy, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToNoSuchDsFailure(
			DataSnapshotNotFoundException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.NO_SUCH_DATA_SNAPSHOT);
		logger.error(
				callingMethodName
						+ ": Could not find data snapshot, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToNoSuchMontageFailure(
			EegMontageNotFoundException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.NO_SUCH_MONTAGE);
		logger.error(
				callingMethodName
						+ ": Could not find montage, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToNoSuchRecordingFailure(
			RecordingNotFoundException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.NO_SUCH_RECORDING);
		logger.error(
				callingMethodName
						+ ": Could not find recording, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToTooMuchDataRequested(
			TooMuchDataRequestedException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.TOO_MUCH_DATA_REQUESTED);
		logger.error(
				callingMethodName
						+ ": Too much data requested, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToStaleMef(
			StaleMEFException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.STALE_MEF);
		logger.error(
				callingMethodName
						+ ": Statle mef requested, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToNoSuchTimeSeriesFailure(
			TimeSeriesNotFoundException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.NO_SUCH_TIME_SERIES);
		logger.error(
				callingMethodName
						+ ": Could not find time series, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToStaleDataException(
			StaleDataException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.STALE_DATA_EXCEPTION);
		logger.error(
				callingMethodName
						+ ": Stale data, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToDuplicateName(
			DuplicateNameException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.DUPLICATE_NAME);
		logger.error(
				callingMethodName
						+ ": Duplicate name, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToBadTsAnnTime(
			BadTsAnnotationTimeException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				"a start or end time was incorrect " + e.getTsAnn().getType()
						+ " id "
						+ e.getTsAnn().getId()
						+ " start "
						+ e.getTsAnn().getStartOffsetMicros()
						+ " end "
						+ e.getTsAnn().getEndOffsetMicros(),
				IeegWsError.BAD_TS_ANN_TIME);
		logger.error(
				callingMethodName
						+ ": Bad Ts Ann time, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToBadRecordingObjectName(
			BadRecordingObjectNameException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.BAD_OBJECT_NAME);
		logger.error(
				callingMethodName
						+ ": Bad object name, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToAuthcFailure(
			AuthenticationException ae,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException("Access Denied",
				IeegWsError.AUTHENTICATION_FAILURE);
		logger.error(
				callingMethodName
						+ ": User could not be authenticated, errorId = "
						+ wse.getErrorId(),
				ae);

		return wse;
	}

	public static IeegWsException logAndConvertToInternalError(
			Throwable t,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				"An error occured on the server",
				IeegWsError.INTERNAL_ERROR);
		logger.error(
				callingMethodName + ": Caught exception, errorId = "
						+ wse.getErrorId(), t);
		return wse;
	}

	public static IeegWsException logAndConvertToPreconditionFailed(
			FailedVersionMatchException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.PRECONDITION_FAILED);
		logger.error(
				callingMethodName
						+ ": precondition failed, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToBadDigest(
			BadDigestException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.BAD_DIGEST);
		logger.error(
				callingMethodName
						+ ": bad digest, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToUploadInProgress(
			UploadInProgressException e,
			String m,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.UPLOAD_IN_PROGRESS);
		logger.error(
				m
						+ ": upload in progress, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;
	}

	public static IeegWsException logAndConvertToNoSuchRecordingObjectFailure(
			RecordingObjectNotFoundException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.NO_SUCH_RECORDING_OBJECT);
		logger.error(
				callingMethodName
						+ ": Could not find recording object, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;

	}

	public static IeegWsException logAndConvertToInvalidRangeFailure(
			InvalidRangeException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.INVALID_RANGE);
		logger.error(
				callingMethodName
						+ ": Invalid byte range, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;

	}

	public static IeegWsException logAndConvertToInboxCredentialsConflictFailure(
			InboxCredentialsConflictException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.INBOX_CREDENTIALS_CONFLICT);
		logger.error(
				callingMethodName
						+ ": Inbox credentials conflict, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;

	}

	public static IeegWsException logAndConvertToDatasetDeletionConflictFailure(
			DatasetConflictException e,
			String callingMethodName,
			Logger logger) {
		final IeegWsException wse = new IeegWsException(
				e.getMessage(),
				IeegWsError.DATASET_DELETION_CONFLICT);
		logger.error(
				callingMethodName
						+ ": dataset deletion conflict, errorId = "
						+ wse.getErrorId(),
				e);
		return wse;

	}
}
