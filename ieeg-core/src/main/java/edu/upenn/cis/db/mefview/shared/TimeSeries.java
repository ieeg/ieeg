/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.db.mefview.eeg.ITimeSeries;

public class TimeSeries implements IsSerializable, ITimeSeries {
	private double scale;
	private double period;
	private long startTime;
	private int[] series;
	private int[] gapsStart;
	private int[] gapsEnd;

	public int[] getGapsStart() {
		return gapsStart;
	}

	public void setGapsStart(int[] gapStart) {
		this.gapsStart = gapStart;
	}

	public int[] getGapsEnd() {
		return gapsEnd;
	}

	public void setGapsEnd(int[] gapEnd) {
		this.gapsEnd = gapEnd;
	}

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public double getPeriod() {
		return period;
	}

	public void setPeriod(double period) {
		this.period = period;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String toString() {
		return getStartTime() + "@" + getPeriod() + " x" + scale + ": " + Arrays.toString(series);
	}
	
	public int[] getSeries() {
		return series;
	}

	public void setSeries(int[] series) {
		this.series = series;
	}
	
	public TimeSeries(int samples, int gaps) {
		super();
		series = new int[samples];
		gapsStart = new int[gaps];
		gapsEnd = new int[gaps];
	}

	public TimeSeries(long startTime, double period, double scale, int[] samples, int[] gapStart, int[] gapEnd) {
		this.scale = scale;
		this.startTime = startTime;
		this.period = period;
		this.series = samples;
		this.gapsStart = gapStart;
		this.gapsEnd = gapEnd;
	}
	
	public TimeSeries() {
		
	}

	/**
	 * Returns true if the particular index is within a gap
	 * 
	 * @param i
	 * @return
	 */
	public boolean isInGap(int i) {

		// the jth gap is
		// [gapsStart[j], gapsEnd[j])
		for (int j = 0; j < gapsStart.length; j++) {
			if (gapsStart[j] <= i && i < gapsEnd[j]) {
				return true;
			}
		}
		return false;

	}

	@Override
	public List<Integer> getGapStart() {
		List<Integer> ret = new ArrayList<Integer>(gapsStart.length);
		for (int i = 0; i < gapsStart.length; i++)
			ret.add(gapsStart[i]);
		return ret;
	}

	@Override
	public List<Integer> getGapEnd() {
		List<Integer> ret = new ArrayList<Integer>(gapsEnd.length);
		for (int i = 0; i < gapsEnd.length; i++)
			ret.add(gapsEnd[i]);
		return ret;
	}

	@Override
	public Integer getValue(int i) {
		if (!isInGap(i))
			return getAt(i);
		else
			return null;
	}

	@Override
	public int getAt(int i) {
		return series[i];
	}

	@Override
	public int getSeriesLength() {
		return series.length;
	}

	@JsonIgnore
	public long[] getTimes() {
		long[] ret = new long[getSeriesLength()];
		
		for (int i = 0; i < series.length; i++)
			ret[i] = (long)(getStartTime() + i * getPeriod());
		return ret;
	}
}
