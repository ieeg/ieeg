/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class PresentableMetadata implements SerializableMetadata, Previewable, Comparable<PresentableMetadata> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public abstract String getId();
	
	public void updateRevId(String oldId, String newId) {
		if (oldId.equals(getId()))
			setId(newId);
		for (GeneralMetadata md: getAllMetadata()) {
			if (md instanceof PresentableMetadata)
				((PresentableMetadata)md).updateRevId(oldId, newId);
		}
	}
	
	public abstract String getFriendlyName();
	
	public abstract List<String> getCreators();
	
	/**
	 * Description of MIME type etc.
	 * @return
	 */
	public abstract String getContentDescriptor();
	
	@JsonIgnore
	@Transient
	public Date getUpdateTime() {
		return getCreationTime();
	}
	
	public abstract Date getCreationTime();
	
	@JsonIgnore
	@Transient
	public abstract double getRelevanceScore();
	
	public abstract Map<String, Set<String>> getUserPermissions();

	/**
	 * What datatypes are associated with this item?
	 * @return
	 */
	
	public abstract Set<String> getAssociatedDataTypes();

	public int compareTo(PresentableMetadata o) {
		return (getFriendlyName().compareTo((o).getFriendlyName()));
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PresentableMetadata))
			return false;
		
		PresentableMetadata r = (PresentableMetadata)o;
		if (!getId().equals(r.getId()))
			return false;
		else {
			return true;
		}
	}
	
	@Override
	public int hashCode() {
		if (getId() == null)
			return 0;
		else
			return getId().hashCode();
	}
	
	@JsonIgnore
	@Transient
	public boolean isNested() {
		for (String s: getKeys()) {
			if (getValueType(s) == VALUE_TYPE.META_SET)
				return true;
		}
		return false;
	}

//	public static final ProvidesKey<PresentableMetadata> KEY_PROVIDER = new ProvidesKey<PresentableMetadata>() {
//
//		public Object getKey(PresentableMetadata item) {
//			return (item == null) ? null : item.getId();//.getFriendlyName();
//		}
//	};
//
//	public static final ProvidesKey<GeneralMetadata> BASIC_KEY_PROVIDER = new ProvidesKey<GeneralMetadata>() {
//
//		public Object getKey(GeneralMetadata item) {
//			return (item == null) ? null : item.getLabel();//.getFriendlyName();
//		}
//	};
//
//	public static final ProvidesKey<PresentableMetadata> key = new ProvidesKey<PresentableMetadata>() {
//
//		public Object getKey(PresentableMetadata item) {
//			return (item == null) ? null : item.getId();//.getFriendlyName();
//		}
//	};
//	
//	public static final TextColumn<PresentableMetadata> contents = new TextColumn<PresentableMetadata> () {
//		@Override
//		public String getValue(PresentableMetadata object) {
//			return (object == null) ? null : (object.getContentDescriptor());
//		}
//	};
//	
//	public static final TextColumn<PresentableMetadata> creators = new TextColumn<PresentableMetadata> () {
//		@Override
//		public String getValue(PresentableMetadata object) {
//			return (object == null) ? null : (object.getCreators().toString());
//		}
//	};

	@Transient
	@JsonIgnore
	public List<? extends GeneralMetadata> getAllMetadata() {
		List<GeneralMetadata> ret = new ArrayList<GeneralMetadata>();
		
		return getAllMetadata(ret, this);
	}
	
	public List<? extends GeneralMetadata> getChildMetadata() {
		List<GeneralMetadata> ret = new ArrayList<GeneralMetadata>();
		for (String key: getKeys()) {
			if (getValueType(key) == VALUE_TYPE.META)
				ret.add(getMetadataValue(key));
			else if (getValueType(key) == VALUE_TYPE.META_SET && getMetadataSetValue(key) != null) {
				for (GeneralMetadata item2: getMetadataSetValue(key)) {
					ret.add(item2);
				}
			}
		}
		return ret;
	}
	
	@Transient
	@JsonIgnore
	public static List<GeneralMetadata> getAllMetadata(List<GeneralMetadata> meta, GeneralMetadata item) {
		meta.add(item);
		
		for (String key: item.getKeys()) {
			if (item.getValueType(key) == VALUE_TYPE.META)
				getAllMetadata(meta, item.getMetadataValue(key));
			else if (item.getValueType(key) == VALUE_TYPE.META_SET && item.getMetadataSetValue(key) != null) {
				for (GeneralMetadata item2: item.getMetadataSetValue(key)) {
					getAllMetadata(meta, item2);
				}
			}
		}
		
		return meta;
	}
	
	List<PresentableMetadata> children = new ArrayList<PresentableMetadata>();
	
	@JsonIgnore
	public List<PresentableMetadata> getChildren() {
		return children;
	}
	
	public void addKnownChild(PresentableMetadata md) {
		if (!children.contains(md))
			children.add(md);
	}
	
	public void clearKnownChildren() {
		children.clear();
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata) {
			setParent((SerializableMetadata)p);
		} else
			throw new RuntimeException("Attempted to set non-serializable parent");
	}
	
//	/**
//	 * The Presenter determines what gets rendered, column-wise, for this metadata
//	 * @return
//	 */
//	@Transient
//	@JsonIgnore
//	public abstract MetadataFormatter getFormatter();
	
	/**
	 * Test for whether there are any children (i.e., whether DrillDown does anything)
	 * 
	 * @return
	 */
	@Transient
	@JsonIgnore
	public abstract boolean isLeaf();
	
	@Transient
	@JsonIgnore
	public abstract MetadataPresenter getPresenter();
	
	@JsonIgnore
	public abstract String getLocation();
	
	/**
	 * The DrillDown fetches and expands child nodes
	 * @return
	 */
//	public abstract MetadataDrillDown getDrillDown();
	
	public String toString() { return getLabel(); }
	
	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		String preview = getObjectName() + ' ' + getFriendlyName();
		
		StringBuilder content = new StringBuilder(" (");
		
		if (this.getContentDescriptor() != null) {
			for (String word: keywords)
				if (this.getContentDescriptor().contains(word)) {
					content.append("Description: " + getContentDescriptor() + "\n");
					break;
				}
		}
		
		if (this.getCreators() != null)
			for (String creator: getCreators())
				for (String word: keywords)
					if (creator.contains(word)) {
						content.append("Creator: " + creator + "\n");
						break;
					}
		
		content.append(')');
		if (content.length() == 3)
			return preview;
		else 
			return preview + content.toString();
	}
	
}
