/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.testhelper;

import static com.google.common.collect.Lists.transform;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static edu.upenn.cis.braintrust.testhelper.TstObjectFactory.randomNonnegInt;

import java.util.List;
import java.util.Random;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

/**
 * Create object for tests. Under main instead of test so that dependent modules
 * can use it easily.
 * 
 * @author John Frommeyer
 * 
 */
public class CoreTstObjectFactory {
	private final TstObjectFactory btTstFac = new TstObjectFactory(false);
	private final Random random = new Random();

	public ToolDto newToolInfoWRevId(UserId author) {
		final ToolDto toolInfo = new ToolDto(
				"TestLabel",
				// "v1",
				author,
				newUuid(),
				// newUuid(),
				// LicenseDto.GPL,
				// ParallelDto.SERIAL,
				newUuid(),
				newUuid(),
				null,
				null);

		return toolInfo;
	}

	public DerivedSnapshot newToolResult() {
		return new DerivedSnapshot(newUuid(), newUuid(), newUuid(), newUuid(), new SearchResult());
	}

	public TraceInfo newTraceInfo() {
		final int minSampleValue = randomNonnegInt();
		final int maxSampleValue = minSampleValue + randomNonnegInt();
		final String channelNameLabel = newUuid();
		return new TraceInfo(null, (long) randomNonnegInt(),
				(double) randomNonnegInt(),
				(double) randomNonnegInt(),
				(double) randomNonnegInt(),
				minSampleValue,
				maxSampleValue,
				newUuid(),
				newUuid(),
				newUuid(),
				newUuid(),
				channelNameLabel,
				newUuid());
	}

	public Annotation newAnnotation(List<TraceInfo> traceInfos) {
		final double start = randomNonnegInt();
		final double end = start + randomNonnegInt();
		final List<String> channels = transform(traceInfos,
				new Function<TraceInfo, String>() {
					@Override
					public String apply(TraceInfo input) {
						return input.getLabel();
					}
				});
		final Annotation annotation = new Annotation(newUuid(),
				newUuid(),
				start,
				end,
				channels,
				random.nextBoolean(),
				traceInfos);
		annotation.setLayer(newUuid());
		return annotation;
	}

}
