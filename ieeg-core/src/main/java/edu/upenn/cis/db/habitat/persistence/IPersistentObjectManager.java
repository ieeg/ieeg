/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.List;

import edu.upenn.cis.braintrust.imodel.IPersistentObject;
import edu.upenn.cis.braintrust.shared.StringKeyValue;

/**
 * Access manager to persistent objects in a storage
 * system
 * 
 * @author zives
 *
 * @param <T> Type of the entity / relationship
 * @param <K>
 */
public interface IPersistentObjectManager<T extends IPersistentObject,K extends Serializable> 
extends IPersistenceManager<K,T> {
	
	public interface IPersistentKey<T,K> {
		public K getKey(T o);
		public void setKey(T o, K newKey);
	}
	
	public boolean alreadyExists(T o, Scope scope);
	public boolean alreadyExistsKey(K key, Scope scope);
	
	public void write(T o, Scope scope);
	
	public T read(K key, Scope scope);
	public List<T> read(T example, Scope scope);
	
	public long count(Scope scope);
	
	public Scope getDefaultScope();
}
