/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.server.search;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.apache.pdfbox.searchengine.lucene.LucenePDFDocument;
import org.apache.tika.mime.MimeTypeException;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.thirdparty.ContentParser;
import edu.upenn.cis.thirdparty.ContentParserException;
import edu.upenn.cis.thirdparty.TikaDocumentParser;

/**
 * Index/text search routines
 * 
 * @author zives
 *
 */
public class SearchServer {
	static SearchServer theServer = null;
	
	public static SearchServer getServer() { 
		if (theServer == null)
			theServer = new SearchServer();
		
		return theServer; 
	}
	
	StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
	Directory index = new RAMDirectory();
	
	IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer);
	IndexWriter writer;
	
	private SearchServer() {
		try {
			writer = new IndexWriter(index, config);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Index a document (in filename form) that must be parsable using Tika
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void indexFileContents(String filename) throws IOException {
			
		ContentParser cp = new TikaDocumentParser();
		try {
			writer.addDocument(cp.getDocument(new File(filename)));
		} catch (MimeTypeException | ContentParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		writer.commit();
	}

	/**
	 * Index a document (in stream form) that must be parsable using Tika
	 * 
	 * @param filename
	 * @throws IOException
	 */
	public void indexFileContents(InputStream stream, String filename) throws IOException {
			
		ContentParser cp = new TikaDocumentParser();
		try {
			writer.addDocument(cp.getDocument(stream, filename));
		} catch (MimeTypeException | ContentParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		writer.commit();
	}

	public void reIndexDatabase()
			throws IOException {
			
		addDoc(writer, "String", "id");
			
		writer.commit();
	}
	
	public List<PresentableMetadata> queryDatabase(SessionToken sessionID, String query) 
	throws IOException, ParseException {
		int hitsPerPage = 10;
		IndexReader reader = IndexReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
		
		Query q = new QueryParser(Version.LUCENE_CURRENT, "title", analyzer).parse(query);
		searcher.search(q, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;

		for (ScoreDoc sd: hits) {
			int docId = sd.doc;
			Document d = searcher.doc(docId);
		}
			
		return null;
	}
	
	private static void addDoc(IndexWriter w, String title, String id) throws IOException {
		Document doc = new Document();
		
		// Gets tokenized
		doc.add(new TextField("title", title, Field.Store.YES));
		
		// Doesn't get tokenized
		doc.add(new StringField("id", id, Field.Store.YES));
		w.addDocument(doc);
	}

	private static void addPDFDoc(IndexWriter w, File file) throws IOException {
		w.addDocument(LucenePDFDocument.getDocument(file));
	}

	private static void addPDFDoc(IndexWriter w, URL url) throws IOException {
		w.addDocument(LucenePDFDocument.getDocument(url));
	}

}
