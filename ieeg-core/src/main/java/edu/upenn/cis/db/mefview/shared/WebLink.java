/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Sets;

/**
 * A named URL
 * 
 * @author zives
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class WebLink extends PresentableMetadata implements Serializable, JsonTyped {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SerializableMetadata parent;
	
//	static MetadataFormatter formatter;
	
	public String getStudy() {
		return study;
	}

	public void setStudy(String study) {
		this.study = study;
	}

	@JsonIgnore
	@Transient
	public String getDetails() {
		return url;
	}

	public void setDetails(String details) {
		this.url = details;
	}

//	public String getFriendlyName() {
//		return friendly;
//	}

	public void setFriendlyName(String friendly) {
		this.friendly = friendly;
	}

	String study;
	String url;
	String friendly;
	
	static Map<String, VALUE_TYPE> keys;
	static {
		keys = new HashMap<String, VALUE_TYPE>();
		
		keys.put("study", VALUE_TYPE.STRING);
		keys.put("friendly", VALUE_TYPE.STRING);
		keys.put("url", VALUE_TYPE.STRING);
	}
	
	public WebLink() {}
	
	/**
	 * Construct a WebLink associated with a given study,
	 * with a friendly name and URL
	 * 
	 * @param studyId
	 * @param friendly
	 * @param url
	 */
	public WebLink(String studyId, String friendly, String url) {
		study = studyId;
		this.url = url;
		this.friendly = friendly;
	}

	@JsonIgnore
	@Transient
	@Override
	public String getLabel() {
		return friendly;
	}
	
	@Override
	public String getId() {
		return url;
	}

	@Override
	public void setId(String id) {
		url = id;
	}

	@Override
	public Set<String> getKeys() {
		return keys.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("study"))
			return getStudy();
		else if (key.equals("friendly"))
			return getFriendlyName();
		else if (key.equals("url"))
			return getDetails();

		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return keys.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		return null;
	}

	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			parent = (SerializableMetadata)p;
	}

	@Override
	public String getFriendlyName() {
		return getLabel();
	}

	@JsonIgnore
	@Transient
	@Override
	public List<String> getCreators() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreators();
		return null;
	}

	@JsonIgnore
	@Transient
	@Override
	public String getContentDescriptor() {
		return getDetails();
	}

	@JsonIgnore
	@Transient
	@Override
	public Date getCreationTime() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreationTime();
		return null;
	}
	
	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getUserPermissions();
		return null;
	}

	@JsonIgnore
	@Transient
	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.PDF);
	}

	@JsonIgnore
	@Transient
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Links.name();
	}

//	@JsonIgnore
//	@Transient
//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}
//
//	public static void setPresenter(MetadataFormatter presenter) {
//		WebLink.formatter = presenter;
//	}
//
//	@JsonIgnore
//	@Transient
//	public static MetadataFormatter getDefaultPresenter() {
//		return formatter;
//	}
	
	@JsonIgnore
	@Transient
	@Override
	public boolean isLeaf() {
		return true;
	}

	@JsonIgnore
	@Transient
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@JsonIgnore
	@Transient
	public List<PresentableMetadata> getChildren() {
		return super.getChildren();
	}
	
	@JsonIgnore
	@Transient
	@Override
	public List<? extends GeneralMetadata> getChildMetadata() {
		return super.getChildMetadata();
	}

	@JsonIgnore
	@Transient
	public String getLocation() {
		return url;
	}
	
	public void setLocation(String location) {
		url = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "URL";
	}
}