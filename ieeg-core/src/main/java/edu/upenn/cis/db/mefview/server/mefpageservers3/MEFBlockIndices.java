/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.primitives.Ints;

import edu.upenn.cis.db.mefview.server.PageReader;
import edu.upenn.cis.db.mefview.server.PageReader.PageList;

/**
 * Represents the block indices section of a MEF.
 */
public final class MEFBlockIndices implements Serializable {

	private static final long serialVersionUID = 1L;

	private long[] timeIndex;
	private long[] offsetIndex;
	private long recStartMicros;
	private long recEndMicros;
	private double samplingFreqHz;
	private long eegDataEndOffset;

	private static Logger logger = LoggerFactory
			.getLogger(MEFBlockIndices.class);

	/**
	 * Read the index structures into memory.
	 * <p>
	 * Assumes little endian.
	 * 
	 * @throws IOException
	 */
	public MEFBlockIndices(
			InputStream data,
			long recStartMicros,
			long recEndMicros,
			double samplingFreqHz,
			long dataEndFileOffset,
			int noIndexEntries) throws IOException {
		this.recStartMicros = recStartMicros;
		this.recEndMicros = recEndMicros;
		this.samplingFreqHz = samplingFreqHz;
		this.eegDataEndOffset = dataEndFileOffset;
		@SuppressWarnings("resource")
		DataInput di = new LittleEndianDataInputStream(data);

		timeIndex = new long[noIndexEntries];
		offsetIndex = new long[noIndexEntries];

		logger.debug("There are " + timeIndex.length
				+ " entries in the index...");

		for (int i = 0; i < timeIndex.length; i++) {
			timeIndex[i] = di.readLong();
			offsetIndex[i] = di.readLong();

			// Need to read past the sample number index entry
			di.readLong();
		}
	}

	public long getStartTime(int i) {
		return timeIndex[i];
	}

	public long getStartTime(long i) {
		return getStartTime(Ints.checkedCast(i));
	}

	public long getStartOffset(int i) {
		if (i >= offsetIndex.length) {
			i = offsetIndex.length - 1;
		}
		if (i < 0) {
			i = 0;
		}
		return offsetIndex[i];
	}

	public long getStartOffset(long i) {
		return getStartOffset(Ints.checkedCast(i));
	}

	public long getEndOffset(int i) {
		if (i + 1 < offsetIndex.length) {
			return offsetIndex[i + 1] - 1;
		}
		return eegDataEndOffset;
	}

	public long getEndOffset(long i) {
		return getEndOffset(Ints.checkedCast(i));
	}

	public int getIndexLength() {
		return timeIndex.length;
	}

	public long getStartTime() {
		return timeIndex[0];
	}

	public PageList getPages(
			long st,
			long et) {
		checkArgument(st <= et);
		PageReader.PageList ret = new PageReader.PageList();
		if (st > recEndMicros)
			st = recEndMicros;

		if (st < recStartMicros)
			st = recStartMicros;

		if (et > recEndMicros)
			et = recEndMicros;

		// Find start index
		int i = Arrays.binarySearch(timeIndex, st);
		if (i < 0)
			i = (-i - 1) - 1;

		// Period in usec
		double period = 1000000. / samplingFreqHz;

		// Find end index
		int j = Arrays.binarySearch(timeIndex, et); // i
		if (j < 0)
			j = (-j - 1) - 1; // we used to advance one more here

		ret.startPage = i;
		ret.startOffset = (long) ((st - timeIndex[i]) / period);
		ret.endPage = j;
		if (j < timeIndex.length) {
			ret.endOffset = (long) ((et - timeIndex[j]) / period + 1);
		} else {
			// can't get here because we added in changed code above (-j - 1) to
			// (-j - 2)to prevent j from
			// falling off the end - it okay to
			// not
			// get here?
			ret.endOffset = 0;
		}

		ret.startTime = st;
		ret.endTime = et;

		// This is the time index for the *next* page, in case
		// this one doesn't have any relevant samples. (We can't
		// tell without reading it.)

		if (i < timeIndex.length - 1)
			ret.nextStart = timeIndex[i + 1];
		else
			ret.nextStart = 0;

		return ret;
	}

	public int getPage(long desiredOffset) {
		if (desiredOffset < 0) {
			desiredOffset = 0;
		}
		final long offset = desiredOffset > eegDataEndOffset ? eegDataEndOffset
				: desiredOffset;

		int page = Arrays.binarySearch(offsetIndex, offset);
		if (page < 0) {
			page = (-page - 1) - 1;
		}

		return page;
	}
}
