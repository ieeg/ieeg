/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable=true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class BasicCollectionNode extends PresentableMetadata implements Serializable, JsonTyped {
		  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public static final String[] PLACEHOLDER = {"Favorites", "Recents"};

		public static Map<String, VALUE_TYPE> valueMap =
				new HashMap<String, VALUE_TYPE>();

		static {
			valueMap.put("id", VALUE_TYPE.STRING);
			valueMap.put("category", VALUE_TYPE.STRING);
			valueMap.put("count", VALUE_TYPE.INT);
		}
		
		int page = 0;
		String id;
		String label;
		String category;
		String location = "habitat://main";
		Date creationTime = null;
		  
		@SuppressWarnings("unused")
		public BasicCollectionNode() {

		}
		  
		  public BasicCollectionNode(
				  String id,
				  String label,
				  String category) {
			  this.id = id;
			  this.label = label;
			  this.category = category;
		  }
		  

		  @JsonIgnore
		  @Override
		  public boolean isNested() {
			  return true;
		  }
		  
		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		@JsonIgnore
		public int getCount() {
			return 0;
		}
		
//		public void setCount(int count) {
//			this.count = count;
//		}
		
		public void addAllMetadata(Collection<? extends PresentableMetadata> meta) {
			for (PresentableMetadata pm: meta)
				addMetadata(pm);
//			metadata.addAll(meta);
//			count += meta.size();
		}
		
		public void addMetadata(PresentableMetadata meta) {
//			if (!metadata.contains(meta)) {
//				metadata.add(meta);
////				count++;
//			}
		}
		
		public void addMetadata(int inx, PresentableMetadata meta) {
//			if (!metadata.contains(meta)) {
//				metadata.add(inx, meta);
//				
//				if (meta instanceof SearchResult) {
//					SearchResult sr = (SearchResult)meta;
//					
//					terms.addAll(sr.getTermsMatched());
//				}
////				count++;
//			}
		}

		public void removeMetadata(PresentableMetadata meta) {
//			if (metadata.contains(meta)) {
//				metadata.remove(meta);
////				count--;
//			}
		}

		public void removeMetadata(int pos) {
//			if (metadata.size() > pos) {
//				metadata.remove(pos);
////				count--;
//			}
		}
		
		@JsonIgnore
		@Override
		public List<PresentableMetadata> getChildMetadata() {
			return null;//metadata;
		}
		  
		@JsonIgnore
		public PresentableMetadata getMetadata() {
//			return metadata.get(0);
			return null;
		}
		
		@JsonIgnore
		@Override
		public SerializableMetadata getParent() {
			return null;
		}

		@JsonIgnore
		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Collections.name();
		}
		
		public void setMetadataCategory() {}

		@Override
		public String getLabel() {
			return label;
		}
		
		public void setLabel(String label) {
			this.label = label;
		}

		@Override
		public void setId(String id) {
			// TODO Auto-generated method stub
			this.id = id;
		}

		@JsonIgnore
		@Override
		public Set<String> getKeys() {
			return valueMap.keySet();
		}
		
		@JsonIgnore
		@Override
		public String getStringValue(String key) {
			if (key.equals("id"))
				return getId();
			else if (key.equals("category"))
				return getMetadataCategory();

			return null;
		}
		
		@JsonIgnore
		@Override
		public VALUE_TYPE getValueType(String key) {
			return valueMap.get(key);
		}

		@JsonIgnore
		@Override
		public Double getDoubleValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@JsonIgnore
		@Override
		public Integer getIntegerValue(String key) {
			if (key.equals("count"))
				return getCount();
			return null;
		}

		@JsonIgnore
		@Override
		public GeneralMetadata getMetadataValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@JsonIgnore
		@Override
		public List<String> getStringSetValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@JsonIgnore
		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getId() {
			return id;
		}

		@JsonIgnore
		@Override
		public String getFriendlyName() {
			return getLabel();
		}

		@JsonIgnore
		@Override
		public List<String> getCreators() {
			return new ArrayList<String>();
		}

		@JsonIgnore
		@Override
		public String getContentDescriptor() {
			return getLabel();
		}

		@Override
		public Date getCreationTime() {
			return creationTime;
		}
		
		public void setCreationTime(Date time) {
			creationTime = time;
		}

		@JsonIgnore
		@Override
		public double getRelevanceScore() {
			return 0;
		}
		
		@JsonIgnore
		@Override
		public Map<String, Set<String>>  getUserPermissions() {
			return new HashMap<String,Set<String>>();
		}
		
		@JsonIgnore
		@Override
		public Set<String> getAssociatedDataTypes() {
			return null;
		}

//		@Override
//		public MetadataFormatter getFormatter() {
//			return formatter;
//		}
//		
//		public static void setFormatter(MetadataFormatter presenter) {
//			CollectionNode.formatter = presenter;
//		}
//
//		public static MetadataFormatter getDefaultFormatter() {
//			return formatter;
//		}
		
		

		public int getPage() {
			return page;
		}
		
		public void setPage(int p) {
			page = p;
		}

		@JsonIgnore
		@Override
		public boolean isLeaf() {
			
			for (String h: PLACEHOLDER) {
				if (h.equals(getLabel()))
					return true;
			}
			return false;
		}

		public void clear() {
//			metadata.clear();
		}

		@JsonIgnore
		@Override
		public List<PresentableMetadata> getChildren() {
//			return metadata;
			return null;
		}
		
		public void setChildren(List<PresentableMetadata> children) {}

		@JsonIgnore
		@Override
		public MetadataPresenter getPresenter() {
			return null;
		}
		
		public String getLocation() {
			return location;
		}
		
		public void setLocation(String location) {
			this.location = location;
		}
		
		@JsonIgnore
		public String getKey() {
			if (label == null)
				return getId();
			else
				return getId() + "/" + getLabel();
		}
		
		@Override
		public int hashCode() {
			return getKey().hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof BasicCollectionNode) {
				return getKey().equals(((BasicCollectionNode)o).getKey());
			} else
				return false;
		}

		@JsonIgnore
		@Override
		public String getObjectName() {
//			if (label.equals("self") && category.equals("self"))
//				return "Dataset ";
//			else
				return getCategory();
		}
}