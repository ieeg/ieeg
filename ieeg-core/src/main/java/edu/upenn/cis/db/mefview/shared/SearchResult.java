/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.braintrust.shared.TimeSeriesSearchResult;

@JsonIgnoreProperties(ignoreUnknown=true)
public final class SearchResult extends PresentableMetadata
		implements IsSerializable, IDataset {
	private static final long serialVersionUID = 1L;

	public static final TextColumn<SearchResult> baseColumn = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			// if (object.getBaseRevId() != null)
			// return
			// MainSnapshotsWindow.this.parent.getStudyFriendlyName(object.getBaseRevId());
			// else
			// return object.getFriendlyName();
			if (object.getBaseFriendly() != null)
				return object.getBaseFriendly();
			else
				return ("-");
		}
	};
	public static final ProvidesKey<SearchResult> KEY_PROVIDER = new ProvidesKey<SearchResult>() {

		public Object getKey(SearchResult item) {
			return (item == null) ? null : item.getDatasetRevId();// .getFriendlyName();
		}

	};
	
	public static final TextColumn<SearchResult> studyAnnotations = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			if (object.getBaseRevId() == null)
				return String.valueOf(object.getAnnotationCount());
			else
				return "-";
		}
	};
	public static final TextColumn<SearchResult> studyChannels = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			if (object.getBaseRevId() == null)
				return String.valueOf(object.getTimeSeriesRevIds().size());
			else
				return "-";
		}
	};
	public static final TextColumn<SearchResult> studyColumn = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			if (object.getHighlighted())
				return "*" + object.getFriendlyName() + "*";
			else
				return object.getFriendlyName();
		}
	};
	
	public static final TextColumn<SearchResult> studyImages = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			return String.valueOf(object.getImageCount());
		}
	};
	public static final ProvidesKey<SearchResult> studyKey = new ProvidesKey<SearchResult>() {

		public Object getKey(SearchResult item) {
			return (item == null) ? null : item.getDatasetRevId();// .getFriendlyName();
		}

	};
	
	public static final TextColumn<SearchResult> studySeries = new TextColumn<SearchResult>() {
		@Override
		public String getValue(SearchResult object) {
			return String.valueOf(object.getDatasetRevId());
		}
	};

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("baseId", VALUE_TYPE.STRING);
		valueMap.put("baseLabel", VALUE_TYPE.STRING);
		// valueMap.put("base", VALUE_TYPE.META);
		valueMap.put("label", VALUE_TYPE.STRING);
		// valueMap.put("series", VALUE_TYPE.META_SET);
		valueMap.put("seriesIds", VALUE_TYPE.STRING_SET);
		valueMap.put("seriesLabels", VALUE_TYPE.STRING_SET);
		valueMap.put("contained", VALUE_TYPE.STRING_SET);
		valueMap.put("type", VALUE_TYPE.STRING);
		valueMap.put("creators", VALUE_TYPE.STRING_SET);

		valueMap.put("annotationCount", VALUE_TYPE.INT);
		valueMap.put("imageCount", VALUE_TYPE.INT);
		valueMap.put("startUutc", VALUE_TYPE.INT);
		valueMap.put("endUutc", VALUE_TYPE.INT);
		valueMap.put("organization", VALUE_TYPE.STRING);
		valueMap.put("derived", VALUE_TYPE.META_SET);
	}
	
	long annotationCount;
	String baseFriendly;

	String baseRevId;
	List<PresentableMetadata> collections = new ArrayList<PresentableMetadata>();
	String creator;
	private Date creationTime;
	String dsRevId;
	private Long endUutc;
	
	IFeatureVector features = null;
	String friendlyName;

	boolean highlighted = false;
	long imageCount;
	private boolean isLeaf = false;
	Boolean isValid = null;
	private boolean knowIfLeaf = false;
	
	String location = "habitat://main";
	
	private String organization;
	
	boolean isEditable = true;

	SerializableMetadata parent = null;

	double score = 0.5;

	private Long startUutc;

	double termRate = 0.1;

//	Set<String> termsMatched = new HashSet<String>();
	Multimap<String, Previewable> matches = HashMultimap.<String, Previewable>create();
	
	CaseMetadata detailedMetadata = null;

	// These should be transient
//	List<String> tsRevIds;
//	List<String> tsLabels;
	List<TimeSeriesSearchResult> timeSeries;

	DataSnapshotType type;

	public SearchResult() {
		dsRevId = "";
		creator = "";
		friendlyName = "";
//		tsRevIds = new ArrayList<String>();
//		tsLabels = new ArrayList<String>();
		timeSeries = new ArrayList<TimeSeriesSearchResult>();
	}

	public SearchResult(SearchResult two) {
		this.dsRevId = two.dsRevId;
		this.creator = two.creator;
		this.friendlyName = two.friendlyName;
//		this.tsRevIds = two.tsRevIds;
//		this.tsLabels = two.tsLabels;
		timeSeries = new ArrayList<TimeSeriesSearchResult>();
		timeSeries.addAll(two.getTimeSeries());
	}

	public SearchResult(String dsRevId, 
			String creator, 
			String friendlyName, 
			List<String> tsRevIds, 
			List<String> tsLabels) {
		this.dsRevId = dsRevId;
		this.creator = creator;
		this.friendlyName = friendlyName;
//		this.tsRevIds = tsRevIds;
//		this.tsLabels = tsLabels;
		timeSeries = new ArrayList<TimeSeriesSearchResult>();
		for (int i = 0; i < tsRevIds.size() && i < tsLabels.size(); i++) {
			timeSeries.add(new TimeSeriesSearchResult(tsRevIds.get(i), tsLabels.get(i)));
		}
	}

	public void addDerived(PresentableMetadata dr) {
		if (dr != this && !dr.getId().equals(getId())) {
			collections.add(dr);
			if (dr instanceof TraceInfo) {
//				tsRevIds.add(dr.getId());
//				tsLabels.add(dr.getLabel());
				timeSeries.add(new TimeSeriesSearchResult(dr.getId(), dr.getLabel()));
			}
		}
	}

//	public void setTimeSeriesRevIds(List<String> tsRevIds) {
//		this.tsRevIds = tsRevIds;
//	}
//
//	public void addTimeSeriesRevId(String tsRevId) {
//		tsRevIds.add(tsRevId);
//	}

	public void addTermMatched(String term, Previewable md) {
		matches.put(term, md);
	}

//	public void setTimeSeriesLabels(List<String> tsLabels) {
//		this.tsLabels = tsLabels;
//	}
//
//	public void addTimeSeriesLabel(String tsLabel) {
//		tsLabels.add(tsLabel);
//	}

	public void addTimeSeries(String revId, String label) {
		timeSeries.add(new TimeSeriesSearchResult(revId, label));
	}

	public void addTimeSeries(TimeSeriesSearchResult ts) {
		timeSeries.add(ts);
	}

//	@Override
//	public int compareTo(PresentableMetadata o) {
//		if (!(o instanceof SearchResult))
//			return super.compareTo(o);
//		else
//			return (friendlyName.compareTo(((SearchResult) o).friendlyName));
//	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof SearchResult))
			return false;

		SearchResult r = (SearchResult) o;
		if (!dsRevId.equals(r.dsRevId) || timeSeries.size() != r.timeSeries.size())//tsRevIds.size() != r.tsRevIds.size())
			return false;
		else {
			// Can't really include the time series since one may have dummy labels and ids.
//			for (int i = 0; i < timeSeries.size(); i++)//tsRevIds.size(); i++)
//				if (!r.timeSeries.contains(timeSeries.get(i)))//.tsRevIds.contains(tsRevIds.get(i)))
//					return false;

			return true;
		}
	}

	public long getAnnotationCount() {
		return annotationCount;
	}

	@JsonIgnore
	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.EEG, BuiltinDataTypes.MRI, BuiltinDataTypes.CT,
				BuiltinDataTypes.DCN, BuiltinDataTypes.PDF);
	}

	public String getBaseFriendly() {
		return baseFriendly;
	}

	public String getBaseRevId() {
		return baseRevId;
	}

	@JsonIgnore
	@Override
	public String getContentDescriptor() {
		return annotationCount + " annotations";
	}

	@Override
	public Date getCreationTime() {
		return creationTime;
	}

	public String getCreator() {
		return creator;
	}

	@JsonIgnore
	@Override
	public List<String> getCreators() {
		List<String> ret = Lists.newArrayList();

		ret.add(creator);

		return ret;
	}

	public String getDatasetRevId() {
		return dsRevId;
	}
	@JsonIgnore
	public List<PresentableMetadata> getDerived() {
		return collections;
	}
	@JsonIgnore
	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	public Long getEndUutc() {
		return endUutc;
	}
	@JsonIgnore
	public IFeatureVector getFeatures() {
		return features;
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public boolean getHighlighted() {
		return highlighted;
	}

	@JsonIgnore
	@Override
	public String getId() {
		return getDatasetRevId();
	}

	public long getImageCount() {
		return imageCount;
	}

	@JsonIgnore
	@Override
	public Integer getIntegerValue(String key) {
		if (key.equals("annotationCount"))
			return (int) getAnnotationCount();
		if (key.equals("imageCount"))
			return (int) getImageCount();
		return null;
	}

	public Boolean getIsValid() {
		return isValid;
	}

	@JsonIgnore
	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return valueMap.keySet();
	}

	@JsonIgnore
	@Override
	public String getLabel() {
		return getFriendlyName();
	}

	public String getLocation() {
		return location;
	}

	// /

	@JsonIgnore
	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Snapshot.name();
	}

	@JsonIgnore
	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		if (key.equals("derived"))
			return this.collections;

		return null;
	}

	@JsonIgnore
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOrganization() {
		return organization;
	}
	
	@JsonIgnore
	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

//	@Override
//	public List<RelatedMetadata> getSubItems() {
//		List<RelatedMetadata> related = Lists.newArrayList();
//
//		// TODO: add studies
//
//		return related;
//	}
//
//	@Override
//	public List<RelatedMetadata> getSuperItems() {
//		List<RelatedMetadata> ret = Lists.newArrayList();
//
//		RelatedMetadata related = new RelatedMetadata(baseRevId, baseFriendly,
//				Sets.newHashSet(BuiltinDataTypes.EEG));
//
//		ret.add(related);
//
//		return ret;
//	}

	@JsonIgnore
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Override
	public double getRelevanceScore() {
		return 1;
	}

	@JsonIgnore
	@Transient
	public double getScore() {
		return score + termRate * getTermsMatched().values().size();
	}

	public Long getStartUutc() {
		return startUutc;
	}

	@JsonIgnore
	@Override
	public List<String> getStringSetValue(String key) {
		if (key.equals("creators")) {
			return getCreators();
		}
		if (key.equals("seriesIds"))
			return this.getTimeSeriesRevIds();
		if (key.equals("seriesLabels"))
			return this.getTimeSeriesLabels();

		return null;
	}

	@JsonIgnore
	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return this.getDatasetRevId();
		if (key.equals("baseId"))
			return this.getBaseRevId();
		if (key.equals("baseLabel"))
			return this.getBaseFriendly();
		if (key.equals("label"))
			return this.getFriendlyName();
		if (key.equals("type"))
			return this.getType().toString();
		if (key.equals("organization"))
			return getOrganization();

		return null;
	}

	/**
	 * How much to weight term matches?
	 * 
	 * @return
	 */
	@JsonIgnore
	@Transient
	public double getTermRate() {
		return termRate;
	}

	@JsonIgnore
	@Transient
	public Multimap<String, Previewable> getTermsMatched() {
		return matches;
	}

	public List<TimeSeriesSearchResult> getTimeSeries() {
		return timeSeries;
	}

	@JsonIgnore
	@Transient
	public List<String> getTimeSeriesLabels() {
//		return tsLabels;
		List<String> ret = new ArrayList<String>(timeSeries.size());
		for (TimeSeriesSearchResult tsr: timeSeries) {
			ret.add(tsr.getLabel());
		}
		return ret;
	}

	@JsonIgnore
	@Transient
	public List<String> getTimeSeriesRevIds() {
//		return tsRevIds;
		List<String> ret = new ArrayList<String>(timeSeries.size());
		for (TimeSeriesSearchResult tsr: timeSeries) {
			ret.add(tsr.getRevId());
		}
		return ret;
	}

	public DataSnapshotType getType() {
		return type;
	}

	@JsonIgnore
	@Override
	public Date getUpdateTime() {
		return getCreationTime();
	}

	@JsonIgnore
	@Override
	public Map<String, Set<String>> getUserPermissions() {
		// TODO Auto-generated method stub
		return new HashMap<String,Set<String>>();
	}

	@JsonIgnore
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	public int hashCode() {
		if (dsRevId == null)
			return 0;
		else
			return dsRevId.hashCode();
	}

	public void highlight() {
		highlighted = true;
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}
//	
//	public static void setFormatter(MetadataFormatter presenter) {
//		SearchResult.formatter = presenter;
//	}
//
//	public static MetadataFormatter getDefaultFormatter() {
//		return formatter;
//	}
	
	@JsonIgnore
	@Override
	public boolean isLeaf() {
		return false;//knowIfLeaf && isLeaf;
	}
	
	public void setAnnotationCount(long annotationCount) {
		this.annotationCount = annotationCount;
	}

	public void setBaseFriendly(String bf) {
		this.baseFriendly = bf;
	}

	public void setBaseRevId(String dsRevId) {
		this.baseRevId = dsRevId;
	}

	public void setCreationTime(Date date) {
		this.creationTime = date;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public void setDatasetRevId(String dsRevId) {
		this.dsRevId = dsRevId;
	}

	public void setEndUutc(@Nullable Long endUutc) {
		this.endUutc = endUutc;
	}
	
	public void setFeatures(IFeatureVector features) {
		this.features = features;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	@Override
	public void setId(String id) {
		this.dsRevId = id;
	}

	public void setImageCount(long l) {
		this.imageCount = l;
	}

	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}

	public void setLeafStatus(boolean isLeaf) {
		this.isLeaf = isLeaf;
		knowIfLeaf = true;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setOrganization(@Nullable String organization) {
		this.organization = organization;
	}
	
	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			parent = (SerializableMetadata)p;
	}
	
	public void setScore(double score) {
		this.score = score;
	}
	
	public void setStartUutc(@Nullable Long startUutc) {
		this.startUutc = startUutc;
	}
	
	public void setTermRate(double termRate) {
		this.termRate = termRate;
	}
	
	public void setTermsMatched(Multimap<String, Previewable> termsMatched) {
		this.matches = termsMatched;
	}
	
	public void setTimeSeries(List<TimeSeriesSearchResult> timeSeries2) {
		timeSeries.addAll(timeSeries2);
	}
	
	public void setType(DataSnapshotType type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return super.toString() + " (" + getTermsMatched().keys() + ")" + 
				((getFeatures() == null) ? "" : getFeatures().toString());
	}

	public void unhighlight() {
		highlighted = false;
	}

	public CaseMetadata getDetailedMetadata() {
		return detailedMetadata;
	}

	public void setDetailedMetadata(CaseMetadata detailedMetadata) {
		this.detailedMetadata = detailedMetadata;
	}
	
	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		String current = super.getPreviewFor(keywords);
		
		if (detailedMetadata != null)
			return current + "\n" + detailedMetadata.getPreviewFor(keywords);
		else
			return current;
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Dataset";
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}
	
	
}
