/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import edu.upenn.cis.db.mefview.shared.AnnotationScheme;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

@XmlRootElement(name="WorkPlace")
public class WorkPlaceSerializable implements Serializable, JsonTyped {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(WorkPlaceSerializable.class);
	List<String> favoriteStudies = new ArrayList<String>();
	List<String> favoriteTools = new ArrayList<String>();
	List<String> recentStudies = new ArrayList<String>();
	List<String> recentTools = new ArrayList<String>();
	
	Map<String, AnnotationScheme> annSchemes = new HashMap<String, AnnotationScheme>();

//	List<String> windows = new ArrayList<String>();

	List<PlaceSerializable> contexts = Lists.newArrayList();
	
	String project;
	
	public WorkPlaceSerializable() {
		
	}

	public static WorkPlaceSerializable getResult(String str){
		JAXBContext context;
		try {
			context = JAXBContext
					.newInstance(WorkPlaceSerializable.class, PlaceSerializable.class, EEGPlaceSerializable.class, StringParamPlaceSerializable.class);
//			context = JAXBContext.newInstance(WorkPlaceSerializable.class);
//			System.out.println("jaxbContext is=" + context.toString());
			
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			StringReader sr = new StringReader(str);
			
			return (WorkPlaceSerializable)unmarshaller.unmarshal(sr);
		} catch (JAXBException e) {
			logger.error("Ignoring exception", e);
		}
		return new WorkPlaceSerializable();
	}

	public List<String> getFavoriteStudies() {
		return favoriteStudies;
	}

	public void setFavoriteStudies(List<String> favoriteStudies) {
		this.favoriteStudies = favoriteStudies;
	}

	public List<String> getFavoriteTools() {
		return favoriteTools;
	}

	public void setFavoriteTools(List<String> favoriteTools) {
		this.favoriteTools = favoriteTools;
	}

	public List<String> getRecentStudies() {
		return recentStudies;
	}

	public void setRecentStudies(List<String> recentStudies) {
		this.recentStudies = recentStudies;
	}

	public List<String> getRecentTools() {
		return recentTools;
	}

	public void setRecentTools(List<String> recentTools) {
		this.recentTools = recentTools;
	}

//	public List<String> getWindows() {
//		return windows;
//	}
//
//	public void setWindows(List<String> windows) {
//		this.windows = windows;
//	}

	@XmlElement(name="EEGPlace")
	public List<PlaceSerializable> getContexts() {
		return contexts;
	}

	public void setContexts(List<PlaceSerializable> contexts) {
		this.contexts = contexts;
	}

	public void setProject(String proj) {
		project = proj;
	}
	
	public String getProject() {
		return project;
	}
}
