/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.daemons;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry.FutureResult;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer;
import edu.upenn.cis.db.mefview.server.LruCache;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * A message-based requestor with caching.  If the key is in the cache, we return the response
 * right away, else we register the responders and we send the message.
 * 
 * @author zives
 *
 * @param <K> Key -- the request info and parameter
 * @param <V> Value type for send requests
 * @param <V2> Value type for response
 */
public abstract class CachingRequestor<K extends IMessage<V>,V extends JsonTyped, V2 extends JsonTyped> 
	implements IAsyncRequestor<K,V2> {
	
	/**
	 * Basic LRU cache.  Note that there is no expiration here.
	 */
	LruCache<K,V2> map;
	
	/**
	 * Registry of handlers for async responses
	 */
//	FutureRegistry<K,V2> futures;
	
	/**
	 * Target for sending requests
	 */
	IMessageBroker<K,V> messageSender;
	IMessageBroker<K,V2> messageReceiver;

	public CachingRequestor(
//			FutureRegistry<K,V2> registry, 
			IMessageBroker<K,V> requestSender,
			IMessageBroker<K,V2> responseReceiver,
			int cacheElements) {
		map = new LruCache<K,V2>(cacheElements);
//		futures = registry;
		messageSender = requestSender;
		messageReceiver = responseReceiver;
	}

	/**
	 * Enqueue a request identified by the key.  If the item is in the cache,
	 * we return it immediately, else we register a FutureResult handler.
	 * We also register a handler to update our cache.
	 * 
	 * @param key
	 * @param onSuccess
	 * @param onFailure
	 */
	public void request(K key,
			IFutureResult<K,V2> onSuccess, 
			IFutureResult<K,String> onFailure) {
		V2 value = map.get(key);
		
		if (value == null) {
			register(key.getChannel(), key, onSuccess, onFailure, true);
			stream(key);

		} else {
			onSuccess.setView(key);
			onSuccess.setResult(value);
			try {
				onSuccess.call();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Synchronous request:  makes a request and waits for a response (or an exception) 
	 */
	@Override
	public V2 requestSync(K key, long timeout) throws 
		TimeoutException, 
		InterruptedException, 
		ExecutionException {
		
		FutureHandler<K,V2> future = new FutureHandler<K,V2>(key);
		
		V2 result = future.get(timeout, TimeUnit.MILLISECONDS);
		
		return result;
	}

	@Override
	public void stream(K keyValue) {
		messageSender.send(keyValue.getChannel(), keyValue, keyValue.getParameters());
	}

	@Override
	public void register(String channel, K key, final IFutureResult<K, V2> onSuccess,
			final IFutureResult<K, String> onFailure, boolean once) {

		messageReceiver.intercept(channel, key, 
				new IMessageBroker.IConsumer<K, V2>() {

			@Override
			public void handle(K key, V2 value) {
				onSuccess.setView(key);
				onSuccess.setResult(value);
				System.out.println("Received response");
				try {
					map.put(key, value);
					onSuccess.call();
				} catch (Exception e) {
					onFailure.setView(key);
					onFailure.setResult(e.getMessage());
					try {
						onFailure.call();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			@Override
			public void handleError(K key, JsonTyped value) {
				onFailure.setView(key);
				try {
					onFailure.setResult(JsonKeyValueMapper.getMapper().createValue(value));
					onFailure.call();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});

//			//  Need to register the message response handler.
//			//  Once we get the message back, we look up the registry entries.
//
//			// Register the callbacks that were requested
//			futures.register(key.getChannel(), key, onSuccess, onFailure, once);
//			
//			messageReceiver.intercept(key.getChannel(), key, new IConsumer<K,V2>() {
//
//				@Override
//				public void handle(K key, V2 value) {
//					futures.notifySuccess(key.getChannel(), key, value);
//				}
//
//				@Override
//				public void handleError(K key, JsonTyped value) {
//					futures.notifyError(key.getChannel(), key, ((Exception)value).getMessage());
//				}
//				
//			});

	}
}
