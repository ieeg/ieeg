package edu.upenn.cis.db.habitat.persistence.mapping;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.dao.snapshots.IDataSnapshotDAO;
import edu.upenn.cis.braintrust.dao.tools.IToolDAO;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ProjectDiscussion;
import edu.upenn.cis.braintrust.model.ProjectEntity;
import edu.upenn.cis.braintrust.model.ToolEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.shared.Post;

public class ProjectMapper {

	public static List<Post> convertProjectDiscussionToPostings(
			List<ProjectDiscussion> discussions,
			IUserService userService) {
		List<Post> ret = new ArrayList<Post>();

		// serialize snapshotdiscussion into snapshotposting
		for (ProjectDiscussion disc : discussions) {
			final User postUser = userService.findUserByUid(disc.getUser().getId());
			Post p = new Post(
					postUser.getUsername(),
					disc.getTitle(),
					disc.getPosting(),
					disc.getProject().getPubId(),
					disc.getPubId(),
					(disc.getRefersTo() == null) ? null : disc.getRefersTo()
							.getPubId(),
							null);

			p.setDiscId(disc.getDiscussId());
			ret.add(p);
		}
		return ret;
	}

	public static EegProject convertProjectToEeg(ProjectEntity p) {
		final String m = "convertProjectToEeg(...)";
		EegProject ret = new EegProject(p.getProjectId(),
				p.getPubId(),
				p.getName(),
				p.getTags(),
				p.getCredits(),
				p.isComplete());

		//final long adminsStartNanos = System.nanoTime();
		for (UserEntity u : p.getAdmins()) {
			ret.getAdmins().add(u.getId());
		}
//		logger.info("{}: admins {} seconds", m,
//				BtUtil.diffNowThenSeconds(adminsStartNanos));

		//final long teamStartNanos = System.nanoTime();
		for (UserEntity u : p.getTeam()) {
			ret.getTeam().add(u.getId());
		}
//		logger.info("{}: team {} seconds", m,
//				BtUtil.diffNowThenSeconds(teamStartNanos));

		//final long datasetsStartNanos = System.nanoTime();
		for (DataSnapshotEntity snap : p.getSnapshots()) {
			ret.getSnapshots().add(snap.getPubId());
		}
//		logger.info("{}: datasets {} seconds", m,
//				BtUtil.diffNowThenSeconds(datasetsStartNanos));

		//final long toolsStartNanos = System.nanoTime();
		for (ToolEntity tool : p.getTools()) {
			ret.getTools().add(tool.getPubId());
		}
//		logger.info("{}: tools {} seconds", m,
//				BtUtil.diffNowThenSeconds(toolsStartNanos));

		return ret;
	}

	public static ProjectEntity convertToProject(IUserDAO userDAO, 
			IDataSnapshotDAO dataSnapshotDAO, IToolDAO toolDAO, EegProject p) {
		ProjectEntity ret = new ProjectEntity(
				p.getProjectId(),
				p.getPubId(),
				p.getName(),
				p.isComplete());

		ret.getCredits().addAll(p.getCredits());
		ret.getTags().addAll(p.getTags());

		for (UserId u : p.getAdmins()) {
			UserEntity userEntity = userDAO.getOrCreateUser(u);
			ret.getAdmins().add(userEntity);
		}

		for (UserId u : p.getTeam()) {
			UserEntity userEntity = userDAO.getOrCreateUser(u);
			ret.getTeam().add(userEntity);
		}

		for (String snapId : p.getSnapshots()) {
			ret.getSnapshots().add(dataSnapshotDAO.findByNaturalId(snapId));
		}

		for (String toolId : p.getTools()) {
			ret.getTools().add(toolDAO.findByNaturalId(toolId));
		}

		return ret;
	}



}
