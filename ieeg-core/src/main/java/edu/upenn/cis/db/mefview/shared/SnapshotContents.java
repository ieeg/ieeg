/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValue;

public class SnapshotContents implements SerializableMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	SearchResult metadata;
	Set<AllowedAction> permissions;
	List<INamedTimeSegment> traces;
	Set<DerivedSnapshot> results;
	List<SearchResult> derivedMetadata;
	
	List<FileInfo> fileContent;
	
	List<Long> traceOffsets;
	
	private List<RecordingObject> recordingObjects;


	Set<IJsonKeyValue> otherContent;



	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();
			
	static {
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("permissions", VALUE_TYPE.STRING_SET);
		valueMap.put("traces", VALUE_TYPE.META_SET);
		valueMap.put("derived", VALUE_TYPE.META_SET);

		valueMap.put("files", VALUE_TYPE.STRING_SET);
		valueMap.put("images", VALUE_TYPE.STRING_SET);
	}
	

	public SnapshotContents() {}

	public SnapshotContents(SearchResult metadata, Set<AllowedAction> permissions,
			List<INamedTimeSegment> traces, Set<DerivedSnapshot> tools, List<SearchResult> derived,
			List<RecordingObject> recordingObjects) {
		super();
		this.metadata = metadata;
		this.permissions = permissions;
		this.traces = traces;
		this.results = tools;
		this.derivedMetadata = derived;
		this.recordingObjects = recordingObjects;
		
		fileContent = new ArrayList<FileInfo>();
		
		for (RecordingObject obj: getRecordingObjects()) {
			FileInfo si = new FileInfo(
					obj.getName(), 
					metadata.getId(), 
					metadata.getFriendlyName(), 
					obj.getId().toString(),
					obj.getInternetMediaType());
			si.setParent(metadata);
			metadata.addDerived(si);
			metadata.addKnownChild(si);
			fileContent.add(si);
		}
	}
	
	public Set<DerivedSnapshot> getDerivedResults() {
		return results;
	}

	public void setDerivedResults(Set<DerivedSnapshot> results) {
		this.results = results;
	}

	/**
	 * The metadata on the snapshot itself
	 * 
	 * @return
	 */
	public SearchResult getMetadata() {
		return metadata;
	}

	public void setMetadata(SearchResult metadata) {
		this.metadata = metadata;
	}

	/**
	 * Allowed actions / permissions
	 * 
	 * @return
	 */
	public Set<AllowedAction> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<AllowedAction> permissions) {
		this.permissions = permissions;
	}
	
	/**
	 * Set of pending uploads for the snapshot
	 * 
	 * @return
	 */
	public List<RecordingObject> getRecordingObjects() {
		return recordingObjects;
	}
	
	public void setRecordingObjects(List<RecordingObject> recordingObjects) {
		this.recordingObjects = recordingObjects;
		fileContent = new ArrayList<FileInfo>();
		
		for (RecordingObject obj: getRecordingObjects()) {
			FileInfo si = new FileInfo(
					obj.getName(), 
					metadata.getId(), 
					metadata.getFriendlyName(), 
					obj.getId().toString(),
					obj.getInternetMediaType());
			si.setParent(metadata);
			metadata.addDerived(si);
			metadata.addKnownChild(si);
			fileContent.add(si);
		}
	}
	
	public void setTraceOffsets() {
		traceOffsets = new ArrayList<Long>();
		long max = 0;//Long.MAX_VALUE;
		for (INamedTimeSegment seg: traces) {
			if (max < seg.getStartTime())
				max = seg.getStartTime();
		}
		for (INamedTimeSegment seg: traces) {
			traceOffsets.add(seg.getStartTime() - max);
		}
	}
	
	/**
	 * Time offsets (usec) for each channel from
	 * the earliest channel start time
	 *  
	 * @return
	 */
	public List<Long> getTraceOffsets() {
		return traceOffsets;
	}

	/**
	 * Time series traces
	 * 
	 * @return
	 */
	public List<INamedTimeSegment> getTraces() {
		return traces;
	}

	public void setTraces(List<INamedTimeSegment> traces) {
		this.traces = traces;
	}

	/**
	 * Any derived snapshots' metadata
	 * 
	 * @return
	 */
	public List<SearchResult> getDerived() {
		return derivedMetadata;
	}

	public void setDerived(List<SearchResult> derived) {
		this.derivedMetadata = derived;
	}

	/**
	 * Revision ID
	 */
	public String getId() {
		return metadata.getDatasetRevId();
	}
	
	public void setId(String id) {
		metadata.setDatasetRevId(id);
	}
	
	@Override
	public String getLabel() {
		return metadata.getFriendlyName();
	}
	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}
	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return metadata.getDatasetRevId();
		return null;
	}
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}
	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return null;
	}
	@Override
	public List<String> getStringSetValue(String key) {
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		if (key.equals("permissions"))
			return null;
		if (key.equals("traces"))
			return traces;
		if (key.equals("derived"))
			return derivedMetadata;

		return null;
	}

	@Override
	public SerializableMetadata getParent() {
		return metadata.getParent();
	}
	
	public void setParent(SerializableMetadata par) {
		metadata.setParent(par);
	}
	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			setParent((SerializableMetadata)p);
		else
			throw new RuntimeException("Attempted to set non-serializable parent");
	}

	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Metadata.name();
	}

	/**
	 * Any other content, described as JSON key/values
	 * 
	 * @return
	 */
	public Set<IJsonKeyValue> getOtherContent() {
		return otherContent;
	}

	public void setOtherContent(Set<IJsonKeyValue> otherContent) {
		this.otherContent = otherContent;
	}

	public List<FileInfo> getFileContent() {
		return fileContent;
	}

}
