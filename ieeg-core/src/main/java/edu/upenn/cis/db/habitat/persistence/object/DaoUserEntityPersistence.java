/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.object;

import edu.upenn.cis.braintrust.dao.identity.IUserDAO;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.Scope;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;



public class DaoUserEntityPersistence<P> extends DaoEntityPersistenceManager<UserEntity,UserId,P> {

	public DaoUserEntityPersistence(
			IUserDAO dao,
			edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey<UserEntity, UserId> key,
			edu.upenn.cis.db.habitat.persistence.object.EntityPersistence.ICreateObject<UserEntity, String, P> creator) {
		super(dao, key, creator);
	}

	@Override
	public UserEntity findById(UserId id, Scope scope) {
		return ((IUserDAO)dao).getOrCreateUser(id);
	}

	@Override
	public Scope getDefaultScope() {
		return dao.getDefaultScope();
	}
}
