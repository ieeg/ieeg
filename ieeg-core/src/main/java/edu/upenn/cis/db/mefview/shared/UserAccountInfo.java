package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.shared.UserProfile;

@GwtCompatible(serializable=true)
public class UserAccountInfo implements Serializable, JsonTyped {
	String login;
	String email;
	String photo;

	UserProfile profile;
	
	public UserAccountInfo() {}
	
	public UserAccountInfo(
			String login,
			String email,
			String firstName, 
			String lastName, 
			String institution, 
			String street,
			String city,
			String state,
			String zip,
			String country,
			String academicTitle,
			String job,
			String photo,
			@Nullable String inboxBucket,
			@Nullable String inboxAwsKey,
			@Nullable String dropBoxKey) {
		profile = new UserProfile(firstName, lastName, institution, academicTitle, job,
				inboxBucket, inboxAwsKey, dropBoxKey, null, null, street, city, state, zip, country);
		
		this.login = login;
		this.email = email;
		this.photo = photo;
	}

	public UserProfile getProfile() {
		return profile;
	}

	public void setProfile(UserProfile info) {
		this.profile = info;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}
