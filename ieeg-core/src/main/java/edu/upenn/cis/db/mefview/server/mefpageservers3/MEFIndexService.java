/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Set;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.db.mefview.server.MEFPageServer.MEFChannelSpecifier;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.eeg.mef.MefHeader2;

/**
 * @author Sam Donnelly
 */
@ThreadSafe
public final class MEFIndexService {

	/**
	 * Start and end times are inclusive of the index entries in a slice.
	 * 
	 * @author Sam Donnelly
	 */
	@Immutable
	final static class IndexCacheKey implements Serializable {
		private static final long serialVersionUID = 1L;

		private final String fileKey;
		private final String fileEtag;
		private final int sliceNo;
		private final long startTimeMicros;
		private final long endTimeMicros;

		public IndexCacheKey(
				String fileKey,
				String fileEtag,
				int sliceNo) {
			this(fileKey, fileEtag, sliceNo, -1, -1);
		}

		public IndexCacheKey(
				String fileKey,
				String fileEtag,
				int sliceNo,
				long startTimeMicros,
				long endTimeMicros) {
			this.fileKey = checkNotNull(fileKey);
			this.fileEtag = checkNotNull(fileEtag);
			this.sliceNo = sliceNo;
			this.startTimeMicros = startTimeMicros;
			this.endTimeMicros = endTimeMicros;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			IndexCacheKey other = (IndexCacheKey) obj;
			if (!Objects.equal(fileEtag, other.fileEtag)) {
				return false;
			}
			if (!Objects.equal(fileKey, other.fileKey)) {
				return false;
			}
			if (sliceNo != other.sliceNo) {
				return false;
			}
			return true;
		}

		public long getEndTimeMicros() {
			return endTimeMicros;
		}

		public String getFileKey() {
			return fileKey;
		}

		public int getSliceNo() {
			return sliceNo;
		}

		public long getStartTimeMicros() {
			return startTimeMicros;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((fileEtag == null) ? 0 : fileEtag.hashCode());
			result = prime * result
					+ ((fileKey == null) ? 0 : fileKey.hashCode());
			result = prime * result + sliceNo;
			return result;
		}
	}

	private static final Logger logger = LoggerFactory
			.getLogger(MEFIndexService.class);

	private static final Logger timeLogger = LoggerFactory.getLogger("time."
			+ MEFIndexService.class.getName());

	public static final String CACHE_NAME =
			MEFIndexService.class.getSimpleName() + ".index";

	private final int noEntriesInASlice;
	private final AmazonS3 s3;
	private final Ehcache cache;

	private final String dataBucket;

	public MEFIndexService() {
		this(
				AwsUtil.getS3(),
				CacheManager
						.getCacheManager("ieeg")
						.getEhcache("MEFIndexService.index"),
				IvProps.getMefIndexSliceSize(),
				IvProps.getDataBucket());
	}

	public MEFIndexService(
			final AmazonS3 s3,
			final Ehcache cache,
			final int noEntriesInASlice,
			final String dataBucket) {
		this.s3 = s3;
		this.cache = cache;
		this.noEntriesInASlice = noEntriesInASlice;
		this.dataBucket = dataBucket;
	}

	Ehcache getCache() {
		return cache;
	}

	public Optional<Long> getEndByteOffset(
			MefHeader2 header,
			ChannelSpecifier chSpec,
			long pageNo,
			boolean readFromS3) throws StaleMEFException {
		checkArgument(pageNo >= 0);
		checkArgument(pageNo < header.getNumberOfIndexEntries());
		final Optional<MEFIndexSlice> slice =
				getOrLoadAndCacheSlice(
						chSpec,
						indexEntry2SliceNo(pageNo, header),
						header,
						readFromS3);
		if (slice.isPresent()) {
			return Optional.of(slice.get().getEndByteOffset(pageNo));
		}
		return Optional.absent();
	}

	private long getNoEntriesBetweenFileOffsets(long start, long end) {
		checkArgument(((end - start + 1) % 24) == 0);
		return (end - start + 1) / 24;
	}

	private int getNoSlices(MefHeader2 header) {
		long noIndexEntries = header.getNumberOfIndexEntries();

		int noSlices = (int) Math.ceil(
				(double) noIndexEntries
						/ noEntriesInASlice);
		return noSlices;
	}

	private Optional<MEFIndexSlice> getOrLoadAndCacheSlice(
			ChannelSpecifier chSpec,
			int sliceNo,
			MefHeader2 header,
			boolean readFromS3) throws StaleMEFException {
		// final String M = "getOrLoadCacheSlice(...)";
		checkArgument(sliceNo >= 0);
		int noSlices = getNoSlices(header);
		if (sliceNo >= noSlices) {
			throw new IllegalArgumentException();
		}
		IndexCacheKey key =
				new IndexCacheKey(
						chSpec.getHandle(),
						chSpec.getCheckStr(),
						sliceNo);
		Element elem = cache.get(key);
		Optional<MEFIndexSlice> slice;
		if (elem == null) {
			slice = Optional.absent();
		} else {
			slice = Optional.of((MEFIndexSlice) elem.getObjectValue());
		}
		if (slice.isPresent() || !readFromS3) {
			return slice;
		}
		return Optional.of(loadAndCacheSlice(chSpec, sliceNo, header));
	}

	private Optional<MEFIndexSlice> getSlice(
			long timeMicros,
			@Nullable IndexCacheKey lowKey,
			@Nullable IndexCacheKey highKey,
			MefHeader2 header,
			ChannelSpecifier chSpec,
			boolean readFromS3) throws StaleMEFException {
		checkArgument(!(lowKey != null && highKey != null)
				|| (lowKey.getSliceNo() <= highKey.getSliceNo())); // A->B
		final String M = "getSlice(...)";
		long inNanos = System.nanoTime();
		double allSecs = -1, needHighSliceSecs = -1, needLowSliceSecs = -1, makingHashSetSecs = -1, searchSecs = -1, getOrLoadSecs = -1;
		try {
			Optional<MEFIndexSlice> sliceWeAreLookingFor = Optional.absent();

			long needLowSliceNanos = System.nanoTime();

			if (lowKey != null
					&& lowKey.getStartTimeMicros() <= timeMicros
					&& lowKey.getEndTimeMicros() >= timeMicros) {
				Element elem = cache.get(lowKey);
				if (elem != null) {
					sliceWeAreLookingFor = Optional.of(
							((MEFIndexSlice) elem.getObjectValue()));
				}
			}

			needLowSliceSecs = BtUtil.diffNowThenSeconds(needLowSliceNanos);

			long needHighSliceNanos = System.nanoTime();
			if (!sliceWeAreLookingFor.isPresent()) {
				if (highKey != null
						&& highKey.getStartTimeMicros() <= timeMicros
						&& highKey.getEndTimeMicros() >= timeMicros) {
					Element elem = cache.get(highKey);
					if (elem != null) {
						sliceWeAreLookingFor = Optional.of(
								(MEFIndexSlice) elem.getObjectValue());
					}
				}
			}

			needHighSliceSecs = BtUtil.diffNowThenSeconds(needHighSliceNanos);

			if (!sliceWeAreLookingFor.isPresent() && !readFromS3) {
				return Optional.absent();
			}

			long makingHashSetStartNanos = System.nanoTime();

			Set<Integer> seenSliceNos = newHashSet();

			makingHashSetSecs = BtUtil
					.diffNowThenSeconds(makingHashSetStartNanos);

			int searches = 0;

			long searchNanos = System.nanoTime();

			while (!sliceWeAreLookingFor.isPresent()) {
				searches++;

				int sliceGuess = guessSliceNo(
						timeMicros,
						lowKey,
						highKey,
						header);

				if (seenSliceNos.contains(sliceGuess)) {
					throw new AssertionError();
				}

				seenSliceNos.add(sliceGuess);

				long getOrLoadStartNanos = System.nanoTime();

				Optional<MEFIndexSlice> sliceWeFoundOpt =
						getOrLoadAndCacheSlice(
								chSpec,
								sliceGuess,
								header,
								readFromS3);

				if (!sliceWeFoundOpt.isPresent()) {
					return Optional.absent();
				}

				MEFIndexSlice sliceWeFound = sliceWeFoundOpt.get();

				getOrLoadSecs = BtUtil.diffNowThenSeconds(getOrLoadStartNanos);

				if (sliceWeFound.getStartMicros() <= timeMicros
						&& sliceWeFound.getEndMicros() >= timeMicros) {
					sliceWeAreLookingFor = sliceWeFoundOpt;
				}

				if (timeMicros > sliceWeFound.getEndMicros()
						&&
						(lowKey == null
						||
						sliceWeFound.getEndMicros() >
						lowKey.getEndTimeMicros())) {
					lowKey = slice2Key(sliceWeFound, header, chSpec);
				}

				if (timeMicros < sliceWeFound.getStartMicros()
						&&
						(highKey == null
						||
						sliceWeFound.getStartMicros() < highKey
								.getStartTimeMicros())) {
					highKey = slice2Key(sliceWeFound, header, chSpec);
				}
			}
			searchSecs = BtUtil.diffNowThenSeconds(searchNanos);
			logger.debug("{}: searches: {}", M, searches);
			return sliceWeAreLookingFor;
		} finally {
			allSecs = BtUtil.diffNowThenSeconds(inNanos);
			timeLogger
					.trace("{}: needLowSlice {} needHighSlice {} makingHashSet {} getOrLoad {} searchSecs {} total {} seconds",
							new Object[] {
									M,
									needLowSliceSecs,
									needHighSliceSecs,
									makingHashSetSecs,
									getOrLoadSecs,
									searchSecs,
									allSecs });
		}
	}

	/**
	 * Needs to read an additional entry so that the end time in a slice can be
	 * known. So this will include 24 more bytes than one would think.
	 */
	private long[] getSliceOffsetsPlusOne(
			int sliceNo,
			MefHeader2 header) {
		checkArgument(sliceNo >= 0);
		checkArgument(sliceNo < getNoSlices(header));
		long indexDataOffset = header.getIndexDataOffset();
		long indexDataOffsetEnd = header.getIndexDataOffsetEnd();
		long start = (sliceNo * noEntriesInASlice * 24) + indexDataOffset;
		if (start <= 0) {
			throw new AssertionError();
		}
		if (start > indexDataOffsetEnd) {
			throw new AssertionError();
		}
		long end = start
				+ (noEntriesInASlice * 24)
				- 1
				+ 24;

		if (end > header.getIndexDataOffsetEnd()) {
			end = header.getIndexDataOffsetEnd();
		}
		if (end <= 0) {
			throw new AssertionError();
		}
		if (end < start) {
			throw new AssertionError();
		}
		return new long[] { start, end };
	}

	/**
	 * Returns the page numbers on which the samples @ {@code startTimeMicros}
	 * and @ {@code endTimeMicros} would be if the pages were perfect (were at
	 * 1000khz).
	 * 
	 * @param header the header for the channel we're interested
	 * @param chSpec the channel spec of the channel we're interested in
	 * @param startTimeMicros the time of the start page we want
	 * @param endTimeMicros the time of the end page we want
	 * 
	 * @return {@code [0]} is the start time's page number, {@code [1]} is the
	 *         end time's page number
	 * @throws StaleMEFException
	 */
	public Optional<long[]> getStartAndEndPageNos(
			MefHeader2 header,
			ChannelSpecifier chSpec,
			long startTimeMicros,
			long endTimeMicros,
			boolean readFromS3) throws StaleMEFException {
		long in = System.nanoTime();
		final String M = "getStartAndEndPageNos(...)";
		double getLowSliceSecs = -1.0, getHighSliceSecs = -1.0;
		try {
			checkArgument(
					startTimeMicros <= endTimeMicros,
					"startTimeMicros must be <= to endTimeMicros");
			long endRecordingTime = header.getRecordingEndTime();

			if (startTimeMicros < header.getRecordingStartTime()) {
				startTimeMicros = header.getRecordingStartTime();
			}

			if (startTimeMicros > endRecordingTime) {
				startTimeMicros = endRecordingTime;
			}

			if (endTimeMicros < header.getRecordingStartTime()) {
				endTimeMicros = header.getRecordingStartTime();
			}

			if (endTimeMicros > endRecordingTime) {
				endTimeMicros = endRecordingTime;
			}

			IndexCacheKey lowKey = null;
			IndexCacheKey highKey = null;

			boolean needLowKey = true, needHighKey = true;

			// having an index would be faster
			for (final Object keyObj : cache.getKeys()) {
				if (!needLowKey && !needHighKey) {
					break;
				}
				final IndexCacheKey key = (IndexCacheKey) keyObj;
				if (key.getFileKey().equals(chSpec.getHandle())) {
					if (needLowKey
							&& key.getStartTimeMicros() <= startTimeMicros
							&& (lowKey == null
							|| lowKey.getStartTimeMicros() <
							key.getStartTimeMicros())) {
						lowKey = key;
						if (lowKey.getStartTimeMicros() <= startTimeMicros
								&& startTimeMicros <= lowKey.getEndTimeMicros()) {
							needLowKey = false;
						}
					}

					if (needHighKey
							&& key.getEndTimeMicros() >= endTimeMicros
							&& (highKey == null
							|| highKey.getEndTimeMicros() > key
									.getEndTimeMicros())) {
						highKey = key;
						if (highKey.getStartTimeMicros() <= endTimeMicros
								&& endTimeMicros <= highKey.getEndTimeMicros()) {
							needHighKey = false;
						}
					}
				}
			}

			long getLowSliceStartNanos = System.nanoTime();

			Optional<MEFIndexSlice> lowSliceOpt =
					getSlice(
							startTimeMicros,
							lowKey,
							highKey,
							header,
							chSpec,
							readFromS3);
			if (!lowSliceOpt.isPresent()) {
				return Optional.absent();
			}

			MEFIndexSlice lowSlice = lowSliceOpt.get();

			getLowSliceSecs = BtUtil.diffNowThenSeconds(getLowSliceStartNanos);

			long getHighSliceStartNanos = System.nanoTime();

			Optional<MEFIndexSlice> highSliceOpt =
					getSlice(
							endTimeMicros,
							lowKey,
							highKey,
							header,
							chSpec,
							readFromS3);

			if (!highSliceOpt.isPresent()) {
				return Optional.absent();
			}
			MEFIndexSlice highSlice = highSliceOpt.get();

			getHighSliceSecs =
					BtUtil.diffNowThenSeconds(getHighSliceStartNanos);

			long startPageNo = lowSlice.getPageNo(startTimeMicros);
			long endPageNo = highSlice.getPageNo(endTimeMicros);

			return Optional.of(new long[] { startPageNo, endPageNo });
		} finally {
			timeLogger.trace(
					"{}: getLowSlice {} getHighSlice {} total {} seconds",
					new Object[] { M,
							getLowSliceSecs,
							getHighSliceSecs,
							BtUtil.diffNowThenSeconds(in) });
		}
	}

	public Optional<Long> getStartByteOffset(
			MefHeader2 header,
			ChannelSpecifier chSpec,
			long pageNo,
			boolean readFromS3) throws StaleMEFException {
		checkArgument(pageNo >= 0);
		Optional<MEFIndexSlice> slice =
				getOrLoadAndCacheSlice(
						chSpec,
						indexEntry2SliceNo(pageNo, header),
						header,
						readFromS3);
		if (slice.isPresent()) {
			return Optional.of(slice.get().getStartByteOffset(pageNo));
		}
		return Optional.absent();
	}

	public Optional<Long> getStartTimeMicros(
			MefHeader2 header,
			ChannelSpecifier chSpec,
			long pageNo,
			boolean readFromS3) throws StaleMEFException {
		checkArgument(pageNo >= 0);
		Optional<MEFIndexSlice> slice =
				getOrLoadAndCacheSlice(
						chSpec,
						indexEntry2SliceNo(pageNo, header),
						header,
						readFromS3);
		if (slice.isPresent()) {
			return Optional.of(slice.get().getStartTimeMicros(pageNo));
		}
		return Optional.absent();
	}

	@VisibleForTesting
	int guessSliceNo(
			long timeMicros,
			@Nullable IndexCacheKey lowKey,
			@Nullable IndexCacheKey highKey,
			MefHeader2 header) {

		// the sliceno should be stricly less than - otherwise we already have
		// our slice
		checkArgument(!(lowKey != null && highKey != null)
				|| (lowKey.getSliceNo() < highKey.getSliceNo())); // A->B
		long guessLowerBound =
				lowKey == null
						? header.getRecordingStartTime()
						: lowKey.getEndTimeMicros() + 1;
		long guessHigherBound =
				highKey == null
						? header.getRecordingEndTime()
						: highKey.getStartTimeMicros() - 1;

		if (guessHigherBound < guessLowerBound) {
			throw new AssertionError();
		}
		float timePercentageIn =
				(float) (timeMicros - guessLowerBound)
						/ (float) (guessHigherBound - guessLowerBound);

		if (timePercentageIn < 0.0) {
			throw new AssertionError();
		}

		long lowIndexNo = -1;
		if (lowKey == null) {
			lowIndexNo = 0;
		} else {
			lowIndexNo =
					sliceNoToFirstEntryNo(lowKey.getSliceNo())
							+ noEntriesInASlice;
		}

		long highIndexNo = -1;
		if (highKey == null) {
			highIndexNo = header.getNumberOfIndexEntries() - 1;
		} else {
			highIndexNo =
					sliceNoToFirstEntryNo(highKey.getSliceNo()) - 1;
		}

		int entriesIn =
				Math.round(timePercentageIn
						* (highIndexNo - lowIndexNo + 1));
		if (entriesIn == 0) {
			entriesIn = 1;
		}

		long idxGuess = lowIndexNo + entriesIn - 1;

		int sliceGuess =
				indexEntry2SliceNo(
						idxGuess,
						header);
		return sliceGuess;
	}

	private int indexEntry2SliceNo(long indexEntry, MefHeader2 header) {
		checkArgument(indexEntry >= 0);
		checkArgument(
				indexEntry < header.getNumberOfIndexEntries(),
				"indexEntry %s noOfEntries %s",
				indexEntry,
				header.getNumberOfIndexEntries());
		int sliceNo =
				Ints.checkedCast(
						indexEntry / noEntriesInASlice);
		return sliceNo;
	}

	private MEFIndexSlice loadAndCacheSlice(
			ChannelSpecifier chSpec,
			int sliceNo,
			MefHeader2 header) throws StaleMEFException {
		final String M = "loadAndCacheSlice(...)";
		logger.debug("{}: going to S3 for slice", M);
		checkArgument(sliceNo >= 0);
		checkArgument(sliceNo < getNoSlices(header));
		checkArgument(chSpec instanceof MEFChannelSpecifier);
		MEFChannelSpecifier mefChSpec = (MEFChannelSpecifier) chSpec;
		final long[] sliceOffsets =
				getSliceOffsetsPlusOne(sliceNo, header);
		InputStream dataInputStream = null;
		try {
			S3Object s3Object =
					AwsUtil.requestS3Object(
							s3,
							AwsUtil.createGetObjectRequest(
									dataBucket,
									chSpec.getHandle(),
									sliceOffsets[0],
									sliceOffsets[1],
									mefChSpec.getCheckStr()),
							"index");
			if (s3Object == null) {
				throw new StaleMEFException(mefChSpec.getId());
			}
			dataInputStream = s3Object.getObjectContent();
			Long dataEndMicros = null;
			Long dataEndByteOffset = null;
			if (sliceOffsets[1] == header.getIndexDataOffsetEnd()) {
				dataEndMicros = header.getRecordingEndTime();
				dataEndByteOffset = header.getIndexDataOffset() - 1;
			}

			MEFIndexSlice slice = new MEFIndexSlice(
					dataInputStream,
					Ints.checkedCast(
							getNoEntriesBetweenFileOffsets(
									sliceOffsets[0],
									sliceOffsets[1])),
					sliceNoToFirstEntryNo(sliceNo),
					dataEndMicros,
					dataEndByteOffset);
			IndexCacheKey key = new IndexCacheKey(
					chSpec.getHandle(),
					chSpec.getCheckStr(),
					sliceNo,
					slice.getStartMicros(),
					slice.getEndMicros());
			cache.put(new Element(key, slice));
			return slice;
		} finally {
			BtUtil.close(dataInputStream);
		}
	}

	@VisibleForTesting
	IndexCacheKey slice2Key(
			MEFIndexSlice slice,
			MefHeader2 header,
			ChannelSpecifier chSpec) {
		return new IndexCacheKey(
				chSpec.getHandle(),
				chSpec.getCheckStr(),
				indexEntry2SliceNo(slice.getFirstPageNo(), header),
				slice.getStartMicros(),
				slice.getEndMicros());
	}

	private int sliceNoToFirstEntryNo(int sliceNo) {
		return sliceNo * noEntriesInASlice;
	}

}
