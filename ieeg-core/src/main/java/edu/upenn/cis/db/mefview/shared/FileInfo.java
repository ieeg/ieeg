/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;
import com.google.common.collect.Sets;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

@GwtCompatible(serializable=true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class FileInfo extends PresentableMetadata implements Serializable,
JsonTyped {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SerializableMetadata parent;
	
//	static MetadataFormatter formatter;
	
	String study;
	String title;
	String fileName;
	String friendly;
	private String mediaType;
	String location = "habitat://main";
	
	static Map<String, VALUE_TYPE> keys;
	static {
		keys = new HashMap<String, VALUE_TYPE>();
		
		keys.put("id", VALUE_TYPE.STRING);
		keys.put("study", VALUE_TYPE.STRING);
		keys.put("details", VALUE_TYPE.STRING);
		keys.put("mediaType", VALUE_TYPE.STRING);
	}
	
	@SuppressWarnings("unused")
    private FileInfo(){
	  // For GWT
	}
	
	public FileInfo(String title, String studyId, String friendly, String details, String mediaType) {
		this.title = title;
		study = studyId;
		this.fileName = details;
		this.friendly = friendly;
		this.mediaType = mediaType;
	}
	
	public String getStudy() {
		return study;
	}

	public String getFileName() {return fileName;}

	public void setStudy(String study) {
		this.study = study;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetails() {
		return fileName;
	}

	public void setDetails(String details) {
		this.fileName = details;
	}

	public String getFriendly() {
		return friendly;
	}

	public void setFriendly(String friendly) {
		this.friendly = friendly;
	}
	
	public String getMediaType() {
		return mediaType;
	}
	
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public static final ProvidesKey<FileInfo> KEY_PROVIDER = new ProvidesKey<FileInfo>() {

		public Object getKey(FileInfo item) {
			return (item == null) ? null : item.study;
		}
		
	};

	public static final TextColumn<FileInfo> detailsColumn = new TextColumn<FileInfo> () {
		@Override
		public String getValue(FileInfo object) {
			return object.title;
		}
	};

	public static final TextColumn<FileInfo> linkColumn = new TextColumn<FileInfo> () {
		@Override
		public String getValue(FileInfo object) {
			return object.fileName;
		}
	};
	
	public static final TextColumn<FileInfo> nameColumn = new TextColumn<FileInfo> () {
		@Override
		public String getValue(FileInfo object) {
			return object.friendly;
		}
	};

	@Override
	public String getLabel() {
		return title;
	}

	@Override
	public String getId() {
		return study + getTitle();
	}

	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<String> getKeys() {
		return keys.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return getId();
		else if (key.equals("study"))
			return getStudy();
		else if (key.equals("details"))
			return getDetails();
		else if (key.equals("mediaType"))
			return getMediaType();

		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return keys.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		return null;
	}

	@Override
	public SerializableMetadata getParent() {
		return parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			parent = (SerializableMetadata)p;
	}

	@Override
	public String getFriendlyName() {
		return getLabel();
	}

	@Override
	public List<String> getCreators() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreators();
		return null;
	}

	@Override
	public String getContentDescriptor() {
		return mediaType;//getDetails();
	}

	@Override
	public Date getCreationTime() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getCreationTime();
		return null;
	}

	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		if (getParent() instanceof PresentableMetadata)
			return ((PresentableMetadata)getParent()).getUserPermissions();
		return null;
	}

	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.PDF);
	}

	@Override
	public String getMetadataCategory() {
		if (getLabel().endsWith(".txt") && getLabel().contains("_chr_"))
			return BASIC_CATEGORIES.Genes.name();
		else if ("application/pdf".equals(mediaType))
			return BASIC_CATEGORIES.Documents.name();
		else if ("text/plain".equals(mediaType)) 
			return BASIC_CATEGORIES.Documents.name();
		else if (mediaType != null && mediaType.startsWith("image/"))
			return BASIC_CATEGORIES.Images.name();
		else if (mediaType != null && 
				(mediaType.equals("application/dicom")
				|| mediaType.equals("application/x-nifti")
				|| mediaType.equals("application/x-nifti-gz")
				|| getLabel().endsWith(".nii.gz")))
			return BASIC_CATEGORIES.ImageStack.name();
		else
			return BASIC_CATEGORIES.Downloads.name();
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}
//
//	public static void setPresenter(MetadataFormatter presenter) {
//		FileInfo.formatter = presenter;
//	}
//
//	public static MetadataFormatter getDefaultPresenter() {
//		return formatter;
//	}
	
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "File";
	}

	@JsonIgnore
	@Override
	public String getPreviewFor(Set<String> keywords) {
		String ret = getFriendlyName();
		
		// Trim path spec from filename
		if (ret.lastIndexOf('/') >= 0)
			ret = ret.substring(ret.lastIndexOf('/') + 1);
		
		return ret;
	}

	@Override
	public String toString() {
		return "FileInfo{" +
				"fileName='" + fileName + '\'' +
				", parent=" + parent +
				", study='" + study + '\'' +
				", title='" + title + '\'' +
				", friendly='" + friendly + '\'' +
				", mediaType='" + mediaType + '\'' +
				", location='" + location + '\'' +
				'}';
	}
}
