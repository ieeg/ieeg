/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.constructors;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence;
import edu.upenn.cis.db.habitat.persistence.object.EntityPersistence.ICreateObject;

public class EntityConstructor<T extends IEntity,K,P> extends PersistentObjectConstructor<T,K> {
	EntityPersistence.ICreateObject<T, K, P> createObject;

	public EntityConstructor(Class<T> theClass, IPersistentKey<T, K> getKey, 
			EntityPersistence.ICreateObject<T, K, P> createObject) {
		super(theClass, getKey);
		this.createObject = createObject;
	}
	public EntityPersistence.ICreateObject<T, K, P> getObjectCreator() {
		return createObject;
	}
	public void setObjectCreator(EntityPersistence.ICreateObject<T, K, P> createObject) {
		this.createObject = createObject;
	}
}