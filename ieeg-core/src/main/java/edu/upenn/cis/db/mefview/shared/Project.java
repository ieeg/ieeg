/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Sets;

import edu.upenn.cis.braintrust.shared.ToolDto;


@JsonIgnoreProperties(ignoreUnknown=true)
public class Project extends PresentableMetadata {//IsSerializable {
	
	public static enum STATE {UNLOADED, LOADING, LOADED}
	
	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();

	static {
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("label", VALUE_TYPE.STRING);

		valueMap.put("tags", VALUE_TYPE.STRING_SET);
		valueMap.put("credits", VALUE_TYPE.STRING_SET);
		valueMap.put("admins", VALUE_TYPE.STRING_SET);
		valueMap.put("team", VALUE_TYPE.STRING_SET);
		valueMap.put("snapshots", VALUE_TYPE.META_SET);
		valueMap.put("tools", VALUE_TYPE.META_SET);
	}

	String location = "habitat://main";

	private Long projectId = null;
	
	private String pubId;
	
	private String name;
	
	private Set<String> tags = Sets.newHashSet();
	
	private Set<String> credits = Sets.newHashSet();
	
	private Set<UserInfo> admins = Sets.newHashSet();
	
	private Set<UserInfo> team = Sets.newHashSet();
	
	private Set<PresentableMetadata> snapshots = Sets.newHashSet();
	
	private Set<ToolDto> tools = Sets.newHashSet();

	private boolean complete = false;
	
	private STATE state = STATE.LOADED;
	
//	static MetadataFormatter formatter = null;
	
	public Project() {
		
	}
	
	public Project(
			final String pubId, 
			final String name, 
			final Set<String> tags, 
			final Set<String> credits, 
			boolean isComplete) {
		this.projectId = null;
		this.pubId = pubId;
		this.name = name;
		this.tags = tags;
		this.credits = credits;
	}

	public Project(
			final Long id,
			final String pubId, 
			final String name, 
			final Set<String> tags, 
			final Set<String> credits, 
			boolean isComplete) {
		this.projectId = id;
		this.pubId = pubId;
		this.name = name;
		this.tags = tags;
		this.credits = credits;
	}
	
	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getPubId() {
		return pubId;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public Set<String> getCredits() {
		return credits;
	}

	public void setCredits(Set<String> credits) {
		this.credits = credits;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public Set<UserInfo> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<UserInfo> admins) {
		this.admins = admins;
	}

	public Set<UserInfo> getTeam() {
		return team;
	}

	public void setTeam(Set<UserInfo> team) {
		this.team = team;
	}

	public Set<PresentableMetadata> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<? extends PresentableMetadata> snapshots) {
		this.snapshots.addAll(snapshots);
	}
	
	public STATE getState() {
		return this.state;
	}
	
	public void setState(STATE state) {
		this.state = checkNotNull(state);
	}

	public Set<ToolDto> getTools() {
		return tools;
	}

	public void setTools(Set<ToolDto> tools) {
		this.tools = tools;
	}

	@Override
	public SerializableMetadata getParent() {
		return null;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		
	}

	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Projects.name();
	}

	@Override
	public String getLabel() {
		return getName();
	}

	@Override
	public void setId(String id) {
		setPubId(id);
	}

	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}

	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return getId();
		else if (key.equals("label"))
			return getLabel();

		return null;
	}

	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		return null;
	}

	@Override
	public Integer getIntegerValue(String key) {
		return null;
	}

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStringSetValue(String key) {
		List<String> ret = new ArrayList<String>();
		if (key.equals("tags")) {
			ret.addAll(tags);
			return ret;
		} else if (key.equals("credits")) {
			ret.addAll(credits);
			return ret;
		} else if (key.equals("admins")) {
			for (UserInfo u: getAdmins())
				ret.add(u.getName());
			return ret;
		} else if (key.equals("team")) {
			for (UserInfo u: getTeam())
				ret.add(u.getName());
			return ret;
		}
		return null;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		if (key.equals("snapshots")) {
			List<PresentableMetadata> snaps = new ArrayList<PresentableMetadata>();
			snaps.addAll(snapshots);
			return snaps;
		} else if (key.equals("tools")) {
			// TODO: ToolDto isn't PresentableMetadata
		}

		return null;
	}
	
	@Override
	public List<? extends GeneralMetadata> getAllMetadata() {
		return getMetadataSetValue("snapshots");
	}

	@Override
	public String getId() {
		return getPubId();
	}

	@Override
	public String getFriendlyName() {
		return getName();
	}

	@Override
	public List<String> getCreators() {
		List<String> ret = new ArrayList<String>();
		for (UserInfo u: admins)
			ret.add(u.getName());
		return ret;
	}

	@Override
	public String getContentDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date getCreationTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		// TODO Auto-generated method stub
		return new HashMap<String, Set<String>>();
	}

	@Override
	public Set<String> getAssociatedDataTypes() {
		return Sets.newHashSet(BuiltinDataTypes.EEG, BuiltinDataTypes.MRI, BuiltinDataTypes.CT,
				BuiltinDataTypes.DCN, BuiltinDataTypes.PDF);
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return formatter;
//	}
//	
//	public static void setFormatter(MetadataFormatter presenter) {
//		Project.formatter = presenter;
//	}
//	
//	public static MetadataFormatter getDefaultFormatter() {
//		return formatter;
//	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Project";
	}
}
