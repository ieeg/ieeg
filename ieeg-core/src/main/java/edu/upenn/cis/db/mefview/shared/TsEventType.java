package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable=true)
public class TsEventType implements Serializable {
	INamedTimeSegment channel;
	Integer eventId;
	Integer unitId;
	Integer value;
	
	String name;
	
	public TsEventType() {}
	
	/**
	 * Partition by channel -> event and optional unit
	 * 
	 * @param channel
	 * @param eventId
	 * @param unitId
	 */
	public TsEventType(String name, INamedTimeSegment channel, Integer eventId, @Nullable Integer unitId) {
		this.name = name;
		this.channel = channel;
		this.eventId = eventId;
		this.unitId = unitId;
		this.value = null;
	}

	/**
	 * Partition by channel -> event optional unit with a specific value
	 * 
	 * @param channel
	 * @param eventId
	 * @param unitId
	 */
	public TsEventType(String name, INamedTimeSegment channel, Integer eventId, Integer unitId, Integer value) {
		this.name = name;
		this.channel = channel;
		this.eventId = eventId;
		this.unitId = unitId;
		this.value = value;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof TsEventType))
			return false;
		
		TsEventType t = (TsEventType)o;
		
		return channel.equals(t.channel) && eventId.equals(t.eventId) && (unitId == null || unitId.equals(t.unitId)) && 
				(value == null || value.equals(t.value));
	}

	public INamedTimeSegment getChannel() {
		return channel;
	}

	public void setChannel(INamedTimeSegment channel) {
		this.channel = channel;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
