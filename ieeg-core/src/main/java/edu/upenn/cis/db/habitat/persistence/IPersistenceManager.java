/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence;

import java.io.Serializable;

import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceCannotSerializeException;

/**
 * Basic encapsulation of persistence in Habitat platform.  Captures transaction
 * scopes (should not be nested!) and key/value combinations.  General enough
 * to be used for Hibernate DAOs as well as key/value stores.
 * 
 * @author Zack Ives
 *
 * @param <K> Key-type
 * @param <V> Value- or object-type
 */
public interface IPersistenceManager<K,V> {
	
	/**
	 * Is the key in the storage system?
	 * 
	 * @param key
	 * @param scope
	 * @return
	 */
	public boolean alreadyExistsKey(K key, Scope scope);
	
	/**
	 * Write the key/value pair
	 * 
	 * @param k
	 * @param value
	 * @param scope
	 * @throws PersistenceCannotSerializeException 
	 */
	public void write(K k, V value, Scope scope) throws PersistenceCannotSerializeException;
	
	/**
	 * Reach the value associated with a key
	 * 
	 * @param key
	 * @param scope
	 * @return
	 * @throws PersistenceCannotSerializeException 
	 */
	public V read(K key, Scope scope) throws PersistenceCannotSerializeException;
	
	/**
	 * Delete the key/value pair, if the key exists
	 * 
	 * @param key
	 * @param scope
	 * @return
	 */
	public boolean delete(K key, Scope scope);
	
	/**
	 * Rename a key/value pair to a newkey/value pair
	 * @param oldKey
	 * @param newKey
	 * @param scope
	 * @return
	 */
	public boolean rename(K oldKey, K newKey, Scope scope);
	
	/**
	 * Returns the number of records
	 * 
	 * @param scope
	 * @return
	 */
	public long count(Scope scope);
	
	/**
	 * Gets the current / active transaction scope
	 * 
	 * @return
	 */
	public Scope getDefaultScope();

}
