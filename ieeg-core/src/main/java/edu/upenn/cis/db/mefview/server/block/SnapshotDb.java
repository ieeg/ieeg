/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;

public class SnapshotDb {
	SnapshotCatalogDb database;
	ClassCatalog catalog;
	Environment env;
	Database snapshot;
	public boolean isOpen = false;
	
	public SnapshotDb(SnapshotCatalogDb mainDb, String basePath, String dbName) {
		database = mainDb;
		catalog = database.getClassCatalog();
		
		env = mainDb.getEnvironment();

		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setTransactional(false);
		dbConfig.setAllowCreate(true);
		dbConfig.setSortedDuplicates(false);
		
		snapshot = env.openDatabase(null, dbName, dbConfig);
		isOpen = true;
	}
	
	public Database getDatabase() {
		return snapshot;
	}
	
	/**
	 * Ensure that we close on deallocation
	 */
	protected void finalize() throws Throwable {
		try {
			close();
		} finally {
			super.finalize();
		}
	}
	
	public void close() {
		if (isOpen)
			snapshot.close();
		isOpen = false;
	}
	
	public ClassCatalog getClassCatalog() {
		return catalog;
	}
}
