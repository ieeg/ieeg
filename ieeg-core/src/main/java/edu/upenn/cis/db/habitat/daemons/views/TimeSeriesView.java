/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.views;

public class TimeSeriesView implements IDataView {
	long startTime;
	long endTime;
	String channel = "";
	double minFrequency;
//	SignalProcessingStep appliedSteps;
	
	public TimeSeriesView() {}
	
	public TimeSeriesView(long startTime, long endTime, String channel,
		double minFrequency) {
	super();
	this.startTime = startTime;
	this.endTime = endTime;
	this.channel = channel;
	this.minFrequency = minFrequency;
}



	@Override
	public boolean isCompatibleWith(IDataView two) {
		return (two instanceof TimeSeriesView);
	}
	@Override
	public boolean overlapsWith(IDataView two) {
		if (isCompatibleWith(two)) {
			TimeSeriesView tsv = (TimeSeriesView)two;
			
			return channel.equals(tsv.channel) &&
					startTime <= tsv.endTime && endTime >= tsv.startTime;
		}
		return false;
	}
	@Override
	public boolean contains(IDataView two) {
		if (isCompatibleWith(two)) {
			TimeSeriesView tsv = (TimeSeriesView)two;
			
			return channel.equals(tsv.channel) &&
					startTime <= tsv.startTime && endTime >= tsv.endTime &&
					minFrequency <= tsv.minFrequency;
		}
		return false;
	}



	public long getStartTime() {
		return startTime;
	}



	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}



	public long getEndTime() {
		return endTime;
	}



	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}



	public String getChannel() {
		return channel;
	}



	public void setChannel(String channel) {
		this.channel = channel;
	}



	public double getMinFrequency() {
		return minFrequency;
	}



	public void setMinFrequency(double minFrequency) {
		this.minFrequency = minFrequency;
	}


	@Override
	public int hashCode() {
		return channel.hashCode() ^ Long.valueOf(startTime).hashCode();
	}
}
