package edu.upenn.cis.db.habitat.persistence.mapping;

import java.util.List;

import com.google.common.collect.Lists;

import edu.upenn.cis.braintrust.model.SnapshotUsage;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.User;

public class ProvenanceMapper {

	public static List<ProvenanceLogEntry> convertUsageToProvenance(
			List<SnapshotUsage> prov,
			IUserService userService) {
		List<ProvenanceLogEntry> ret = Lists.newArrayList();

		ProvenanceLogEntry.UsageType utype;

		for (SnapshotUsage u : prov) {
			utype = ProvenanceLogEntry.UsageType.values()[u.getUsageMode()];
			// switch (u.getUsage()) {
			// case CREATES:
			// utype = ProvenanceLogEntry.USAGE_TYPE.CREATES;
			// break;
			// case ADDSTO:
			// utype = ProvenanceLogEntry.USAGE_TYPE.ADDSTO;
			// break;
			// case VIEWS:
			// utype = ProvenanceLogEntry.USAGE_TYPE.VIEWS;
			// break;
			// case ANNOTATES:
			// utype = ProvenanceLogEntry.USAGE_TYPE.ANNOTATES;
			// break;
			// case DERIVES:
			// utype = ProvenanceLogEntry.USAGE_TYPE.DERIVES;
			// Pbreak;
			// case DISCUSSES:
			// utype = ProvenanceLogEntry.USAGE_TYPE.DISCUSSES;
			// break;
			// case FAVORITES:
			// utype = ProvenanceLogEntry.USAGE_TYPE.DERIVES;
			// break;
			// default:
			// throw new RuntimeException("Unable to cast enums");
			// }
			String derived = null;
			String ann = null;
			if (u.getDerivedSnapshot() != null) {
				derived = u.getDerivedSnapshot().getPubId();
			}

			if (u.getAnnotation() != null) {
				ann = u.getAnnotation().getPubId();
			}
			final User usageUser = userService.findUserByUid(u.getUser().getId());
			ret.add(new ProvenanceLogEntry(
					usageUser.getUsername(),
					utype,
					u.getSnapshot().getPubId(),
					u.getTime(),
					derived,
					ann));
		}

		return ret;
	}


}
