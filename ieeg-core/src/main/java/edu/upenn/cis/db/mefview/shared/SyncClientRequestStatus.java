package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Date;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable=true)
public class SyncClientRequestStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Date started;
	String requestName;
	String path;
	String statusMessage;
	boolean complete;
	boolean failed;
	
	public SyncClientRequestStatus() {
		started = new Date();
		complete = false;
		failed = false;
		statusMessage = "Requested";
	}
	
	
	public SyncClientRequestStatus(String requestName, String path) {
		this(new Date(), requestName, path, "Requested", false, false);
	}

	
	public SyncClientRequestStatus(Date started, String requestName, String path, String statusMessage,
			boolean complete, boolean failed) {
		super();
		this.started = started;
		this.requestName = requestName;
		this.path = path;
		this.statusMessage = statusMessage;
		this.complete = false;
		this.failed = false;
	}



	public Date getStarted() {
		return started;
	}
	public void setStarted(Date started) {
		this.started = started;
	}
	public String getRequestName() {
		return requestName;
	}
	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}
	public boolean isFailed() {
		return failed;
	}
	public void setFailed(boolean failed) {
		this.failed = failed;
	}
	
	
}
