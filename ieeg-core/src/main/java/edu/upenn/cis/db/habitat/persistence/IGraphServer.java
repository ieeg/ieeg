package edu.upenn.cis.db.habitat.persistence;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceCannotSerializeException;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceObjectNotFoundException;
import edu.upenn.cis.db.mefview.server.IIeegUserService;
import edu.upenn.cis.db.mefview.server.IUserAuth;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationModification;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.IFeatureVector;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.ISecurityCredentials;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWeightVector;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.UnauthorizedException;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface IGraphServer {
	public interface SearchComponent {
		enum Match {Equal, Approx, Less, LessEq, Greater, GreaterEq, NotEq, Contains};
		
		public String getMatchTerm();
		
		public boolean hasValue();
		
		public String getMatchValue();
		
		public Match getMatchType();
	};
	
	public static class StringSearchComponent implements SearchComponent {
		String keyword;
		
		public StringSearchComponent(String keyword) {
			this.keyword = keyword;
		}
		
		@Override
		public String getMatchTerm() {
			return keyword;
		}
		
		@Override
		public boolean hasValue() {
			return false;
		}

		@Override
		public String getMatchValue() {
			return null;
		}

		@Override
		public Match getMatchType() {
			return null;
		}
		
		@Override
		public String toString() {
			return getMatchTerm();
		}
	};
	
	public static class ValueSearchComponent implements SearchComponent {
		String keyword;
		String value;
		Match match;
		
		public ValueSearchComponent(String keyword, String value, Match match) {
			this.keyword = keyword;
			this.value = value;
			this.match = match;
		}
		
		@Override
		public String getMatchTerm() {
			return keyword;
		}
		
		public String getMatchValue() {
			return value;
		}
		
		public Match getMatchType() {
			return match;
		}

		@Override
		public boolean hasValue() {
			return true;
		}

		@Override
		public String toString() {
			return getMatchTerm() + " " + getMatchType().name() + " " + getMatchValue();
		}
};

	/**
	 * Loads information about every data snapshot and builds a map between
	 * channel filenames and UUIDs.
	 * 
	 * @param me
	 */
	public void loadSnapshotMetadata(IDataSnapshotServer datasets, User me, boolean store);
	
	/**
	 * Traverses the Hibernate database for time series annotations, and tries to link these
	 * to known snapshots.  If so, adds a node and a link.
	 * 
	 * @param me
	 */
	public void loadTimeSeriesAnnotations(IDataSnapshotServer datasets, User me);
	
	/**
	 * Recursively traverse basePath for MEF files, try to find their associated
	 * studies (by matching against known filePaths in the snapshotMap).
	 * Import the headers and add an edge from study node to MEF header file / channel.
	 * 
	 * Allows for the possibility that there are multiple snapshots with the same time
	 * series.
	 * 
	 * @param uid
	 * @param basePath
	 */
	public void loadMEFFileHeaders(ISecurityCredentials uid, String basePath);	

	public Map<String, Double> getFeatureWeights(Long userID);
	
	public void setFeatureWeights(Long userID, Map<String, Double> weights);
	
	public IWeightVector getVectorFromWeights(Map<String, Double> weights);
	
	public void shutdown();

	public List<PresentableMetadata> getSnapshotsMatching(
			IUserAuth uService, User u, Set<SearchComponent> terms) throws IOException, UnauthorizedException;

	void setFeedbackOnFeatures(Long user, List<IFeatureVector> positive, List<IFeatureVector> negative);

	public Set<PresentableMetadata> getMyData(IIeegUserService uService, User u, IUserAuth authService) throws UnauthorizedException, IOException;

	Set<UserInfo> getKnownUsers(User u, IUserAuth auth) throws UnauthorizedException, IOException;

	public Set<PresentableMetadata> getRecommendedUsers(User u) throws UnauthorizedException, IOException;

	
	
	public void addFriend(User u, User u2) throws UnauthorizedException, IOException;
	
	public void removeFriend(User u, User u2) throws UnauthorizedException, IOException;

	public void addToGroup(User userToAdd, String groupName, User onBehalfOf) throws UnauthorizedException, IOException;

	void removeFromGroup(User userToRemove, String groupName, User onBehalfOf)
			throws UnauthorizedException, IOException;
	
	public enum ShareLevel {ShareRead, ShareWrite, ShareGrant};

	void shareWith(User granter, PresentableMetadata object, ShareLevel level, User sharee) throws UnauthorizedException, IOException;
	
	public void shareWithGroup(User u, PresentableMetadata object, ShareLevel level, String group)  throws UnauthorizedException, IOException;

	void unshareWith(User granter, PresentableMetadata object, User sharee) throws UnauthorizedException, IOException;

	/**
	 * Doesn't exactly change the owner, instead adds GRANTER rights
	 * 
	 * @param oldOwner
	 * @param object
	 * @param newOwner
	 * @throws UnauthorizedException
	 * @throws IOException
	 */
	void changeOwner(User oldOwner, PresentableMetadata object, User newOwner) throws UnauthorizedException, IOException;

	void createGroup(User creator, String groupName) throws UnauthorizedException, IOException;
	
	void removeGroup(User remover, String groupName) throws UnauthorizedException, IOException;

	BasicCollectionNode createFolder(User owner, String label, String folderPathName) throws UnauthorizedException, IOException;
	
	void removeFolder(User owner, String folderPathName) throws UnauthorizedException, IOException;

	void addToFolder(Object toAdd, BasicCollectionNode folder, User onBehalfOf) throws UnauthorizedException, PersistenceCannotSerializeException, IOException;
	
	void removeFromFolder(Object toRemove, BasicCollectionNode folder, User onBehalfOf) throws UnauthorizedException, Exception, IOException;

	void removeDataset(String dsId, User onBehalfOf) throws UnauthorizedException, Exception, IOException;

	/**
	 * Create new user account from the User object.
	 * Will replace the internal UID with one of its own.
	 * 
	 * @param user
	 * @param cleartextPassword
	 * @throws UnauthorizedException
	 * @throws IOException 
	 */
	void createUser(User user, String cleartextPassword) throws UnauthorizedException, IOException;
	
	/**
	 * Remove the user account.  Will not garbage-collect anything the user owned.
	 * 
	 * @param user
	 * @param initiator
	 * @throws UnauthorizedException
	 * @throws IOException 
	 */
	void removeUser(User user, User initiator) throws UnauthorizedException, IOException;

	Set<BasicCollectionNode> getGroups(User user)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

	Set<PresentableMetadata> getFolderContents(User user, String path)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

	BasicCollectionNode getHomeFolder(User owner) throws IOException, UnauthorizedException;

	String getUrlBase();

	public void storeMetadata(User u, BasicCollectionNode path, PresentableMetadata object) 
			throws IOException, UnauthorizedException;

	public void removeMetadata(User u, BasicCollectionNode path, PresentableMetadata object) 
			throws IOException, UnauthorizedException;
	
	public void storeMetadata(User u, SearchResult parent, PresentableMetadata object) 
			throws IOException, UnauthorizedException;

	public void storeMetadata(User u, SearchResult parent, RecordingObject object) 
			throws IOException, UnauthorizedException;

	public void removeMetadata(User u, SearchResult parent, PresentableMetadata object) 
			throws IOException, UnauthorizedException;
	
	public void storeMetadataObject(User u, SearchResult parent, FileInfo metadata, IObjectServer os, IStoredObjectReference ref) throws IOException, UnauthorizedException;

	void removeMetadataObject(User u, SearchResult parent, FileInfo metadata) throws UnauthorizedException, IOException;

	void storeTimeSeriesObject(User u, SearchResult parent, TraceInfo metadata, IObjectServer os, IStoredObjectReference ref) throws IOException, UnauthorizedException;

	void storeTimeSeriesObject(User u, String snapshotID, ChannelInfoDto metadata, String traceId, IObjectServer os, IStoredObjectReference ref) 
			throws IOException, UnauthorizedException, PersistenceObjectNotFoundException;

	public void attachTimeSeries(User user, String snapshotID, ChannelInfoDto channel, String traceId) 
			throws IOException, UnauthorizedException;

	void removeTimeSeriesObject(User u, SearchResult parent, TraceInfo metadata) throws IOException, UnauthorizedException;

	public Set<PresentableMetadata> getFolderContents(User u, BasicCollectionNode folder)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

	public Set<PresentableMetadata> getIncomingFolderContents(User u, BasicCollectionNode folder)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

	void saveSnapshot(DataSnapshotSearchResult dsa, User user) throws UnauthorizedException, IOException;

	Set<PresentableMetadata> getIncoming(User user)
			throws JsonParseException, JsonMappingException, IOException, UnauthorizedException;
	
	Map<String, Set<Integer>> getUserPermissions(User user, PresentableMetadata object)
			throws JsonParseException, JsonMappingException, IOException, UnauthorizedException;

	public Set<AnnotationModification> getAnnotationModifications(User user, String datasetID)
			throws UnauthorizedException, IOException;
	
	public void saveAnnotations(User user, String datasetID, List<Annotation> newAnnotations)
			throws UnauthorizedException, IOException;

	public void saveAnnotationModifications(User user, String datasetID, Collection<AnnotationModification> newAnnotations)
			throws UnauthorizedException, IOException;

	void savePatientSnapshotMetadata(User user, String studyRevId, EegStudyMetadata meta)
			throws IOException, UnauthorizedException;

	void saveAnimalSnapshotMetadata(User user, String studyRevId, ExperimentMetadata meta2)
			throws IOException, UnauthorizedException;

	void addDerivation(User user, SearchResult parent, String provenance, SearchResult child)
			throws IOException, UnauthorizedException;

	void indexDocument(User user, SearchResult parent, FileInfo file, IObjectServer os, IStoredObjectReference ref)
			throws UnauthorizedException, IOException;

	void indexDocument(User user, SearchResult parent, RecordingObject ro, IObjectServer os, IStoredObjectReference ref)
			throws UnauthorizedException, PersistenceCannotSerializeException, PersistenceObjectNotFoundException;

	long getOrReuseRecordingObjectId(User user, String name)
			throws PersistenceCannotSerializeException, IOException, UnauthorizedException;

//	SearchResult getInbox(User owner) throws UnauthorizedException, IOException;

	public Set<PresentableMetadata> getUsersInGroup(String groupName, User u) throws UnauthorizedException, IOException;

	public Set<PresentableMetadata> getGroupAccessible(String groupName, User u, int level) 
			throws UnauthorizedException, IOException;

	boolean isReader(User u, String datasetID) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isUpdater(User u, String datasetID) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isAnnotator(User u, String datasetID) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isAdmin(User u, String datasetID) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isObjectReader(User u, Object o) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isObjectAnnotator(User u, Object o) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isObjectUpdater(User u, Object o) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isObjectAdmin(User u, Object o) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	boolean isObjectGranter(User u, Object o) throws PersistenceCannotSerializeException, UnauthorizedException, IOException;

	BasicCollectionNode getIncomingFolder(String user) throws UnauthorizedException, IOException;

	void addToIncoming(Object toAdd, String target, User onBehalfOf, Set<Integer> roles) throws UnauthorizedException, IOException;
	
	void storeEventChannels(User u, String datasetID, List<INamedTimeSegment> channels, boolean main) throws UnauthorizedException, IOException;

	List<INamedTimeSegment> getEventChannels(User u, String datasetID, boolean main) throws UnauthorizedException, IOException;

	void removeMetadata(User u, SearchResult parent, RecordingObject object) throws IOException, UnauthorizedException;

	public void acceptShare(PresentableMetadata object, BasicCollectionNode target, User user) throws IOException, UnauthorizedException;

	public void rejectShare(PresentableMetadata object, User user) throws IOException, UnauthorizedException;

	Set<PresentableMetadata> getPublicData(User user)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

	void setPublicRoles(User user, PresentableMetadata object, Set<Integer> roles)
			throws UnauthorizedException, JsonParseException, JsonMappingException, IOException;

//	void removeFromFolder(Object toRemove, BasicCollectionNode folder, User onBehalfOf) throws UnauthorizedException, PersistenceCannotSerializeException; 
}
