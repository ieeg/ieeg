/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.constructors;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence;
import edu.upenn.cis.db.habitat.persistence.object.RelationshipPersistence.ICreateRelationship;

/**
 * Constructor for relationship objects (binary relationships)
 * 
 * @author zives
 *
 * @param <T>
 * @param <K>
 * @param <E1>
 * @param <E2>
 */
public class RelationshipConstructor<T extends IRelationship,K,E1 extends IEntity,E2 extends IEntity> 
extends PersistentObjectConstructor<T,K>{
	RelationshipPersistence.ICreateRelationship<T, K, E1, E2> createObject;

	public RelationshipConstructor(Class<T> theClass, 
			IPersistentKey<T, K> getKey,
			ICreateRelationship<T, K, E1, E2> createObject) {
		super(theClass, getKey);
		this.createObject = createObject;
	}

	public RelationshipPersistence.ICreateRelationship<T, K, E1, E2> getCreateObject() {
		return createObject;
	}
	public void setCreateObject(
			RelationshipPersistence.ICreateRelationship<T, K, E1, E2> createObject) {
		this.createObject = createObject;
	}
}