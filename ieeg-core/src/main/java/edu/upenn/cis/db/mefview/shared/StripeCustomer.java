package edu.upenn.cis.db.mefview.shared;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Stripe customer data
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class StripeCustomer implements Serializable {
    public String stripe_id;
}
