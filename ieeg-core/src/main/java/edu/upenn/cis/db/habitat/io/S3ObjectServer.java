/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.io;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ListPartsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.StorageClass;
import com.amazonaws.services.s3.model.UploadPartRequest;
import com.amazonaws.services.s3.model.UploadPartResult;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.db.mefview.shared.AWSContainerCredentials;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.thirdparty.ByteBufferBackedInputStream;

/**
 * Amazon S3 file server
 * 
 * @author zives
 *
 */
public class S3ObjectServer extends BaseObjectServer {
	
	public static class S3Cursor implements ICursor {
		GetObjectRequest req;
		long position = 0;

		public S3Cursor(GetObjectRequest req, long pos) {
			this(req);
			
			setPosition(pos);
		}
		
		public S3Cursor(GetObjectRequest req) {
			this.req = req;
			
			if (req.getRange() != null)
				position = req.getRange()[0];
		}
		
		public GetObjectRequest getOR() {
			return req;
		}
		
		public void setOR(GetObjectRequest or) {
			req = or;
		}
		
		@Override
		public long getPosition() {
			return position;
		}
		
		@Override
		public void setPosition(long pos) {
			position = pos;
		}
		
		public void addLength(long length) {
			position += length;
		}
	}
	
	public static class InputS3Stream implements IInputStream {
		
		AmazonS3 instance;
		FileReference object;
		S3ObjectServer server;
		private final Logger logger = LoggerFactory.getLogger(getClass().getName().replace('$', '.'));
		//srcFile and srcBuffer should never both be non-null
		private File srcFile = null;
		private ByteBuffer srcBuffer = null;
		
		public InputS3Stream(S3ObjectServer server, AmazonS3 s3Instance, FileReference object, DirectoryBucketContainer defaultContainer) throws FileNotFoundException {
			this.server = server;
			instance = s3Instance;
			this.object = new FileReference(object.getDirBucketContainer(), object.getObjectKey(), object.getFilePath());
			
			// Set default bucket
			if (this.object.getDirBucketContainer() == null)
				this.object.setDirBucketContainer(
                                        new DirectoryBucketContainer(
                                                defaultContainer.getBucket(),
                                                defaultContainer.getFileDirectory()));

			if (!exists(this.object))
				throw new FileNotFoundException("Cannot find " + object + " in S3!");
			}
		
		public boolean exists(FileReference object) {
			try {
				instance.getObjectMetadata(object.getDirBucketContainer().getBucket(), 
					object.getObjectKey());
				return true;
			} catch (AmazonServiceException e) {
				return false;
			}
		}

		@Override
		public void close() throws IOException {
			if (srcFile != null) {
				srcFile.delete();
				srcFile = null;
			}
			if (srcBuffer != null) {
				srcBuffer = null;
			}
			
		}

		@Override
		public S3Cursor getStart() {
			return new S3Cursor(AwsUtil.createGetObjectRequest(object.getDirBucketContainer().getBucket(), 
					object.getObjectKey()));
		}

		@Override
		public S3Cursor getOffset(long pos) {
			return new S3Cursor(AwsUtil.createGetObjectRequest(object.getDirBucketContainer().getBucket(), 
					object.getObjectKey()), pos);
		}

		@Override
		public boolean isAtEof(ICursor cursor) throws IOException {
			return cursor.getPosition() == getLength();
		}

		@Override
		public long getLength() throws IOException {
			ObjectMetadata md = server.getS3(getHandle()).getObjectMetadata(
					new GetObjectMetadataRequest(getHandle().getDirBucketContainer().getBucket(),
												getHandle().getObjectKey()));

			return md.getContentLength();
		}
		
		public FileReference getHandle() {
			return object;
		}

		@Override
		public ICursor getBytes(WritableByteChannel target, long bytes)
				throws IOException {
			return server.getBytes(this, target, bytes);
		}

		@Override
		public ICursor getBytes(ICursor position, WritableByteChannel target,
				long bytes) throws IOException {
			return server.getBytes(this, position, target, bytes);
		}

		@Override
		public ICursor getBytes(ByteBuffer target, long bytes)
				throws IOException {
			return server.getBytes(this, target, bytes);
		}

		@Override
		public ICursor getBytes(ICursor position, ByteBuffer target, long bytes)
				throws IOException {
			return server.getBytes(this, position, target, bytes);
		}

		@Override
		public void copyTo(OutputStream stream) throws IOException {
			try (S3Object objectSegment = server.getS3(getHandle()).getObject(
					getStart().getOR())) {

				InputStream is = objectSegment.getObjectContent();

				org.apache.commons.io.IOUtils.copy(is, stream);
			}
		}
		
		@Override
		public InputStream getInputStream() throws IOException {
			final String m = "getInputStream()";
			if (srcBuffer != null) {
				return new ByteArrayInputStream(srcBuffer.array());
			}
			if (srcFile != null) {
				return new FileInputStream(srcFile);
			}
			if (getLength() < 100000) {
				srcBuffer = ByteBuffer.allocate((int)getLength());
				
				getBytes(srcBuffer, srcBuffer.limit());
	
				return new ByteArrayInputStream(srcBuffer.array());
			} else {
				srcFile = File.createTempFile("InputS3Stream-", "tmp");
				logger.trace("{}: Using srcFile {}", m, srcFile);
				try (FileOutputStream temp = new FileOutputStream(srcFile)) {
					copyTo(temp);
					return new FileInputStream(srcFile);
				} catch (IOException e) {
					srcFile.delete();
					srcFile = null;
					throw e;
				}
			}
		}
	}
	
	public static class OutputS3Stream implements IOutputStream {
		AmazonS3 instance;
		FileReference object;
		S3ObjectServer server;
		File tempFile = null;
		RandomAccessFile raFile = null;
		boolean hasStream = false;
		private final long multiPartCopyPartBytes = 5 * 1048576; //5MB
		private final Logger logger = LoggerFactory.getLogger(getClass().getName().replace('$', '.'));
		
		public OutputS3Stream(S3ObjectServer server, AmazonS3 s3Instance, FileReference object, DirectoryBucketContainer defaultContainer) {
			this.server = server;
			instance = s3Instance;
			this.object = new FileReference(object.getDirBucketContainer(), object.getObjectKey(), object.getFilePath());
			
			// Set default bucket
			if (this.object.getDirBucketContainer() == null)
				this.object.setDirBucketContainer(
						new DirectoryBucketContainer(
								defaultContainer.getBucket(), 
								defaultContainer.getFileDirectory()));
		}
		
		private RandomAccessFile getTempFile() throws IOException {
			tempFile = File.createTempFile("ieeg-temp-", ".s3");
			tempFile.deleteOnExit();
			
			raFile = new RandomAccessFile(tempFile, "rw");
			return raFile;
		}

		@Override
		public void close() throws IOException {
			final String m = "close()";
			// Read file, Write object!

			if (raFile != null) {
				try {
					raFile.close();
				} catch (IOException e) {
					raFile = new RandomAccessFile(tempFile, "rw");
					raFile.close();
				}

				final long contentLength = tempFile.length();
				if (contentLength >= 20 * multiPartCopyPartBytes) {
					multiPartUpload(tempFile);
				} else {
					final String destinationBucket = getHandle()
							.getDirBucketContainer().getBucket();
					final String destinationKey = getHandle().getObjectKey();
					logger.debug(
							"{}: Using single put to upload source {} to {}:{}",
							m,
							tempFile.getCanonicalPath(),
							destinationBucket,
							destinationKey);

					PutObjectRequest pr = new PutObjectRequest(
							destinationBucket,
							destinationKey,
							tempFile)
							.withStorageClass(StorageClass.Standard);

					if (getHandle().getDirBucketContainer() instanceof CredentialedDirectoryBucketContainer
							&&
							((CredentialedDirectoryBucketContainer) getHandle()
									.getDirBucketContainer()).getCredentials() != null) {
						CredentialedDirectoryBucketContainer container =
								((CredentialedDirectoryBucketContainer) getHandle()
										.getDirBucketContainer());
						AWSCredentials creds = ((AWSContainerCredentials) container
								.getCredentials()).getCredentials();
						pr.setRequestCredentials(creds);
					} else {
						pr.setRequestCredentials(AwsUtil.getCreds());
					}

					instance.putObject(pr);
				}
				tempFile.delete();
			}
		}

		@Override
		public S3Cursor getStart() {
			return new S3Cursor(AwsUtil.createGetObjectRequest(object.getDirBucketContainer().getBucket(), 
					object.getObjectKey()));
		}
		
		@Override
		public S3Cursor getOffset(long pos) {
			return new S3Cursor(AwsUtil.createGetObjectRequest(object.getDirBucketContainer().getBucket(), 
					object.getObjectKey()), pos);
		}

		public FileReference getHandle() {
			return object;
		}
		
		public RandomAccessFile getFile() {
			try {
				return getTempFile();
			} catch (IOException e) {
				return null;
			}
		}

		@Override
		public ICursor putBytes(ReadableByteChannel source, long numBytes)
				throws IOException {
			return server.putBytes(this, source, numBytes);
		}

		@Override
		public ICursor putBytes(ICursor position, ReadableByteChannel source,
				long numBytes) throws IOException {
			return server.putBytes(this, position, source, numBytes);
		}

		@Override
		public ICursor putBytes(ByteBuffer source) throws IOException {
			return server.putBytes(this, source);
		}

		@Override
		public ICursor putBytes(ICursor position, ByteBuffer source)
				throws IOException {
			return server.putBytes(this, position, source);
		}

		@Override
		public void copyFrom(IInputStream stream) throws IOException {
			final String m = "copyFrom(IInputStream)";
			// S3 -> S3 copy directly
			if (stream.getStart() instanceof S3Cursor) {
				S3Cursor orig = (S3Cursor)stream.getStart();
				final String sourceBucket = orig.getOR().getBucketName();
				final String sourceKey = orig.getOR().getKey();
				final String destinationBucket = getHandle().getDirBucketContainer().getBucket();
				final String destinationKey = getHandle().getObjectKey();
				
				// Self-copy, skip it!
				if (sourceBucket.equals(destinationBucket) 
						&& sourceKey.equals(destinationKey)) {
					logger.debug("{}: Source equals destination: {}:{}", m, sourceBucket, sourceKey);
					return;
				}
				logger.debug(
						"{}: Will copy {}:{} to {}:{}", 
						m, 
						sourceBucket, 
						sourceKey, 
						destinationBucket, 
						destinationKey);
				final long sourceLengthBytes = stream.getLength();
				if (sourceLengthBytes < 20 * multiPartCopyPartBytes) {
					logger.debug(
							"{}: Using single copy to copy source {}:{} to {}:{}", 
							m, 
							sourceBucket, 
							sourceKey, 
							destinationBucket, 
							destinationKey);
					CopyObjectRequest cr = new CopyObjectRequest(
							sourceBucket,
							sourceKey, 
							destinationBucket,
							destinationKey)
						.withStorageClass(StorageClass.Standard);
				
					instance.copyObject(cr);
				} else {
					List<CopyPartResult> copyResponses =
							newArrayList();

					InitiateMultipartUploadRequest initiateRequest =
							new InitiateMultipartUploadRequest(
									destinationBucket, destinationKey)
									.withStorageClass(StorageClass.Standard);

					InitiateMultipartUploadResult initResult =
							instance.initiateMultipartUpload(initiateRequest);

					String uploadId = initResult.getUploadId();
					logger.info(
							"{}: Using multipart copy to copy source {}:{} to {}:{}. Upload id: {}",
							m,
							sourceBucket,
							sourceKey,
							destinationBucket,
							destinationKey,
							uploadId);
					Throwable failedPartThrowable = null;
					long bytePosition = 0;
					for (int i = 1; bytePosition < sourceLengthBytes; i++) {
						final long lastByte = 
								bytePosition + multiPartCopyPartBytes - 1 >= sourceLengthBytes
								? sourceLengthBytes - 1
								: bytePosition + multiPartCopyPartBytes - 1;
						CopyPartRequest copyRequest = new CopyPartRequest()
								.withDestinationBucketName(destinationBucket)
								.withDestinationKey(destinationKey)
								.withSourceBucketName(sourceBucket)
								.withSourceKey(sourceKey)
								.withUploadId(uploadId)
								.withFirstByte(bytePosition)
								.withLastByte(lastByte)
								.withPartNumber(i);
						logger.trace(
								"{}: Copying part {} of {}:{} to {}:{}",
								m,
								i,
								sourceBucket,
								sourceKey,
								destinationBucket,
								destinationKey);
						try {
							copyResponses.add(instance.copyPart(copyRequest));
						} catch (Throwable t) {
							failedPartThrowable = t;
							break;
						}
						bytePosition += multiPartCopyPartBytes;
					}
					
					if (failedPartThrowable != null) {
						throw abortMultipartUpload(
								destinationBucket, 
								destinationKey, 
								uploadId,
								failedPartThrowable);
					}
					List<PartETag> partETags = newArrayList();
					for (final CopyPartResult copyResponse : copyResponses) {
						partETags.add(
								new PartETag(
										copyResponse.getPartNumber(),
										copyResponse.getETag()));
					}
					final CompleteMultipartUploadRequest completeUploadRequest = new CompleteMultipartUploadRequest(
							destinationBucket,
							destinationKey,
							uploadId,
							partETags);
					instance.completeMultipartUpload(completeUploadRequest);
					
				}
				
			// Local disk to S3 copy
			} else {
				long length = stream.getLength();
	
				ByteBuffer buf = ByteBuffer.allocate((int)length);
				ICursor cursor = stream.getBytes(buf, length);
				
				buf.flip();
				
				copyFrom(new ByteBufferBackedInputStream(buf), length);
			}
		}
		
		private void multiPartUpload(File file) throws IOException {
			final String m = "multiPartUpload(...)";
			final String bucketName = getHandle().getDirBucketContainer()
					.getBucket();
			final String keyName = getHandle().getObjectKey();

			final List<PartETag> partETags = new ArrayList<>();

			// Initiate the multipart upload.
			final InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(
					bucketName, keyName);
			final InitiateMultipartUploadResult initResponse = instance
					.initiateMultipartUpload(initRequest);
			final String uploadId = initResponse.getUploadId();
			
			logger.info(
					"{}: Using multipart upload to upload source {} to {}:{}. Upload id: {}",
					m,
					file.getCanonicalPath(),
					bucketName,
					keyName,
					uploadId);
			
			try {
				// Upload the file parts.
				final long contentLength = file.length();
				long filePosition = 0;
				for (int i = 1; filePosition < contentLength; i++) {
					// Because the last part could be less than 5 MB, adjust the
					// part size as needed.
					final long partSize = Math.min(multiPartCopyPartBytes,
							(contentLength - filePosition));

					// Create the request to upload a part.
					final UploadPartRequest uploadRequest = new UploadPartRequest()
							.withBucketName(bucketName)
							.withKey(keyName)
							.withUploadId(uploadId)
							.withPartNumber(i)
							.withFileOffset(filePosition)
							.withFile(file)
							.withPartSize(partSize);

					// Upload the part and add the response's ETag to our list.
					final UploadPartResult uploadResult = instance
							.uploadPart(uploadRequest);
					partETags.add(uploadResult.getPartETag());

					filePosition += partSize;
				}

				// Complete the multipart upload.
				CompleteMultipartUploadRequest compRequest = new CompleteMultipartUploadRequest(
						bucketName,
						keyName,
						uploadId, 
						partETags);
				instance.completeMultipartUpload(compRequest);
			} catch (Throwable e) {
				throw abortMultipartUpload(bucketName, 
						keyName, 
						uploadId, 
						e);
			}

		}

		/**
		 * Attempts to abort the multipart upload until ListParts or AbortMultipartUpload returns a NoSuchUpload error. 
		 * Returns {@code originalThrowable} as is if it is and IOException or wrapped in an IOException otherwise. 
		 * 
		 * @param destinationBucket
		 * @param destinationKey
		 * @param uploadId
		 * @param originalThrowable
		 */
		private IOException abortMultipartUpload(
				String destinationBucket,
				String destinationKey,
				String uploadId,
				Throwable originalThrowable) {
			final String m = "abortMultipartUpload(...)";
			boolean aborted = false;
			final int maxAbortAttempts = 10;
			int abortAttempts = 0;
			while (!aborted && abortAttempts < maxAbortAttempts) {
				try {
					abortAttempts++;
					final AbortMultipartUploadRequest abortRequest = new AbortMultipartUploadRequest(
							destinationBucket,
							destinationKey,
							uploadId);
					instance.abortMultipartUpload(abortRequest);
					//AWS docs suggest that multiple abort requests might be necessary if list parts still returns parts.
					final ListPartsRequest listPartsRequest = new ListPartsRequest(
							destinationBucket,
							destinationKey,
							uploadId);
					instance.listParts(listPartsRequest);

				} catch (AmazonServiceException e) {
					if ("NoSuchUpload".equals(e.getErrorCode())) {
						aborted = true;
						logger.info("{}: Mulipart upload {} of {} to {} sucessfully aborted after {} tries",
								m,
								uploadId,
								destinationKey,
								destinationBucket,
								abortAttempts);
					} else if (abortAttempts < maxAbortAttempts){
						logger.info("{}: Unexpected error while attempting to abort mulipart upload " + uploadId + ". Will retry", e);
					} else {
						logger.error("{}: Failed to abort mulipart upload " + uploadId, e);
					}
				}
			}
			
			if (originalThrowable instanceof IOException) {
				return (IOException) originalThrowable;
			}
			return new IOException(originalThrowable);
		}
		
		@Override
		public void copyFrom(InputStream stream, long length) throws IOException {
			PutObjectRequest pr = AwsUtil.createPutObjectRequest(
				getHandle().getDirBucketContainer().getBucket(), 
				getHandle().getObjectKey(), 
				stream, length).withStorageClass(StorageClass.Standard);
			
//			pr.setRequestCredentials(getHandle().getContainer().getCredentials());
			if (getHandle().getDirBucketContainer() instanceof CredentialedDirectoryBucketContainer && 
					((CredentialedDirectoryBucketContainer)getHandle().getDirBucketContainer()).getCredentials() != null) {
				CredentialedDirectoryBucketContainer container = 
						((CredentialedDirectoryBucketContainer)getHandle().getDirBucketContainer());
				AWSCredentials creds = ((AWSContainerCredentials)container.getCredentials()).getCredentials();
				pr.setRequestCredentials(creds);//AwsUtil.getCreds());
			} else {
				pr.setRequestCredentials(AwsUtil.getCreds());
			}
			
			server.getS3(getHandle()).putObject(pr);
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			hasStream = true;
			return new FileOutputStream(getFile().getFD());
		}

	}

	Map<String,AmazonS3> instancesByKeys = new HashMap<String,AmazonS3>();
//	AmazonS3 s3Instance;
	String defaultBucket;
	String defaultPath;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * Initialize with the bucket being the default one in IvProps
	 */
	public S3ObjectServer() {
		defaultBucket = IvProps.getDataBucket();
		defaultPath = IvProps.getIvProps().get(IvProps.SOURCEPATH);
	}

	/**
	 * Initialize with the specific bucket mentioned
	 * 
	 * @param bucket
	 */
	public S3ObjectServer(String bucket) {
		this.defaultBucket = bucket;
	}

	
	@Override
	public void init() {
		instancesByKeys.put("default", AwsUtil.getS3());
//		s3Instance = AwsUtil.getS3();
	}
	
	/**
	 * Looks up the credentials associated with this bucket, and finds an AmazonS3
	 * instance that is appropriate
	 * 
	 * @param credentials
	 * @return
	 */
	public AmazonS3 getInstance(AWSCredentials credentials) {
		String key = credentials == null ? "default" : credentials.getAWSAccessKeyId();
		if (!instancesByKeys.containsKey(key)) {
			System.out.println("Creating S3 instance for " + key);
			if (key.equals("default"))
				instancesByKeys.put(key, AwsUtil.getS3());
			else
				instancesByKeys.put(key, new AmazonS3Client(credentials));
		}

		return instancesByKeys.get(key);
	}

	@Override
	public IInputStream openForInput(IStoredObjectReference handle) throws IOException {
		AWSCredentials creds = null;
		
		if (((FileReference)handle).getDirBucketContainer() instanceof CredentialedDirectoryBucketContainer) {
			CredentialedDirectoryBucketContainer container = 
					((CredentialedDirectoryBucketContainer)((FileReference)handle).getDirBucketContainer());
			creds = ((AWSContainerCredentials)container.getCredentials()).getCredentials();
		}
		if (creds == null)
			creds = (AwsUtil.getCreds());
		
		
		return new InputS3Stream(this, 
				getInstance(creds),
				(FileReference)handle, 
				new CredentialedDirectoryBucketContainer(handle.getDirBucketContainer().getBucket(), handle.getFilePath()));
	}

	@Override
	public ICursor getBytes(IInputStream stream, WritableByteChannel target,
			long numBytesToFetch) throws IOException {
		InputS3Stream theStream = (InputS3Stream)stream;
		
		S3Cursor cursor = theStream.getStart();

		return getBytes(stream, cursor, target, numBytesToFetch);
	}

	@Override
	public ICursor getBytes(IInputStream stream, ICursor currentPosition,
			WritableByteChannel target, long numBytesToFetch)
			throws IOException {

		ByteBuffer buf = ByteBuffer.allocate((int)numBytesToFetch);
		
		ICursor ret = getBytes(stream, currentPosition, buf, numBytesToFetch);
		
		target.write(buf);
		
		return ret;
	}

	@Override
	public ICursor getBytes(IInputStream stream, ByteBuffer target,
			long numBytesToFetch) throws IOException {
		InputS3Stream theStream = (InputS3Stream)stream;
		
		S3Cursor cursor = theStream.getStart();

		return getBytes(stream, cursor, target, numBytesToFetch);
	}

	@Override
	public ICursor getBytes(IInputStream stream, ICursor currentPosition,
			ByteBuffer buf, long numBytesToFetch) throws IOException {
		final String m = "getBytes(...)";
		S3Cursor cursor = (S3Cursor)currentPosition;
		final int numBytesToFetchInt = Ints.checkedCast(numBytesToFetch);
		cursor.getOR().setRange(cursor.getPosition(), cursor.getPosition() + numBytesToFetchInt - 1);
		
		InputS3Stream str = (InputS3Stream)stream;
		
		try (S3Object objectSegment = getS3(str.getHandle()).getObject(
				cursor.getOR())) {

			InputStream is = objectSegment.getObjectContent();

			ByteStreams.readFully(is, buf.array(), 0, numBytesToFetchInt);

			buf.limit(numBytesToFetchInt);
			buf.position(numBytesToFetchInt);

			logger.debug("{}: Read {} bytes from S3", m, numBytesToFetchInt);

			if (numBytesToFetchInt > 0)
				cursor.addLength(numBytesToFetchInt);

			return cursor;
		}
	}

	@Override
	public void close(IStream handle) throws IOException {
		handle.close();
	}

	@Override
	public void shutdown() {
		
	}
	
	/**
	 * Get the S3 bucket+credentials associated with the container
	 *  
	 * @param handle
	 * @return
	 */
	private AmazonS3 getS3(FileReference handle) {
		AWSCredentials cred = null;
		if (handle.getDirBucketContainer() instanceof CredentialedDirectoryBucketContainer) {
			return getS3((CredentialedDirectoryBucketContainer)handle.getDirBucketContainer());
		}

		if (cred == null) {
			cred = (AwsUtil.getCreds());
		}

		return getInstance(cred);
	}

	private AmazonS3 getS3(CredentialedDirectoryBucketContainer container) {
		AWSContainerCredentials containerCred = ((AWSContainerCredentials)container.getCredentials());
		if (containerCred == null) {
			containerCred = new AWSContainerCredentials(AwsUtil.getCreds());
			container.setCredentials(containerCred);
		} else if (containerCred.getCredentials() == null) {
			containerCred.setCredentials(AwsUtil.getCreds());
		}
		final AWSCredentials cred = containerCred.getCredentials();
		return getInstance(cred);
	}

	@Override
	public void deleteItem(IStoredObjectReference handle) throws IOException {
		getS3((FileReference)handle).deleteObject(handle.getDirBucketContainer().getBucket(), handle.getObjectKey());
	}

	@Override
	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource)
			throws IOException {
		
		ObjectListing listing = getS3((CredentialedDirectoryBucketContainer)resource).listObjects(resource.getBucket(), resource.getFileDirectory());
		
		final List<S3ObjectSummary> summaries = newArrayList(listing.getObjectSummaries());
		while(listing.isTruncated()) {
			listing = getS3((CredentialedDirectoryBucketContainer)resource).listNextBatchOfObjects(listing);
			summaries.addAll(listing.getObjectSummaries());
		}
		final Set<IStoredObjectReference> objectKeys = newHashSet();
		for (final S3ObjectSummary summary : summaries) {
			FileReference ref =  new FileReference((CredentialedDirectoryBucketContainer)resource, summary.getKey(), summary.getKey());
			if (summary.getKey().endsWith("/"))
				ref.setContainer();
			objectKeys.add(ref);
		}
		return objectKeys;
	}

	@Override
	public IOutputStream openForOutput(IStoredObjectReference handle) throws IOException {
		return new OutputS3Stream(this,
				getS3((FileReference)handle), 
				(FileReference)handle,
				new CredentialedDirectoryBucketContainer(handle.getDirBucketContainer().getBucket(), handle.getFilePath()));
	}

	@Override
	public ICursor putBytes(IOutputStream stream, ReadableByteChannel source,
			long numBytesTowrite) throws IOException {
		OutputS3Stream theStream = (OutputS3Stream)stream;
		
		S3Cursor cursor = theStream.getStart();

		return putBytes(stream, cursor, source, numBytesTowrite);
	}

	@Override
	public ICursor putBytes(IOutputStream stream, ICursor currentPosition,
			ReadableByteChannel source, long numBytesToWrite)
			throws IOException {
		ByteBuffer buf = ByteBuffer.allocate((int)numBytesToWrite);
		
		int len = source.read(buf);
		
		ICursor ret = putBytes(stream, currentPosition, buf);
		
		return ret;
	}

	@Override
	public ICursor putBytes(IOutputStream stream, ByteBuffer source)
			throws IOException {
		S3Cursor cursor = ((OutputS3Stream)stream).getStart();

		return putBytes(stream, cursor, source);
	}

	@Override
	public ICursor putBytes(IOutputStream stream, ICursor currentPosition,
			ByteBuffer source) throws IOException {
		S3Cursor cursor = (S3Cursor)currentPosition;
		OutputS3Stream theStream = (OutputS3Stream)stream;
		
		FileChannel fc = theStream.getFile().getChannel();
		
//		int remaining = source.remaining();
		
		int length = fc.write(source, cursor.getPosition());
		
		System.out.println("Spooled " + length + " bytes to tempfile, starting at " + cursor.getPosition());
		
		S3Cursor cursor2 = new S3Cursor(cursor.getOR()); 
		cursor2.setPosition(cursor.getPosition() + length);
		
		System.out.println("New cursor = " + cursor2.getPosition());
		
		return cursor2;
	}


	@Override
	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource, String folder) throws IOException {
		try {
			final String m = "getDirectoryContents(...)";
			final Set<IStoredObjectReference> objectKeys = newHashSet();
			logger.info("Bucket: "+resource.getBucket() +" prefix:"+ folder );

			// return an empty set if the bucket or folder is null
			// TODO: real fix is finding and fixing the callers, this
			// is just preserving existing semantics
			if (resource.getBucket() != null && folder != null) {
				ListObjectsRequest list = new ListObjectsRequest();
				list.setBucketName(resource.getBucket());
				list.setPrefix(folder + "/");
				list.setDelimiter("/");
				ObjectListing listing =
						getS3((CredentialedDirectoryBucketContainer)resource).listObjects(list);

				final List<S3ObjectSummary> summaries = newArrayList(listing.getObjectSummaries());
				logger.debug("{}: Got {} summaries for bucket [{}] and prefix [{}]",
						m, summaries.size(), list.getBucketName(), list.getPrefix());
				while(listing.isTruncated()) {
					listing = getS3((CredentialedDirectoryBucketContainer)resource).listNextBatchOfObjects(listing);
					List<S3ObjectSummary> nextBatch = listing.getObjectSummaries();
					logger.debug("{}: Added {} additional summaries for bucket [{}] and prefix [{}]",
							m, summaries.size(), list.getBucketName(), list.getPrefix());
					summaries.addAll(nextBatch);
				}

				for (final S3ObjectSummary summary : summaries) {
					objectKeys.add(new FileReference((CredentialedDirectoryBucketContainer)resource, summary.getKey(), summary.getKey()));
				}
			}

			return objectKeys;
		} catch (AmazonClientException e) {
			logger.error("Error in S3ObjectServer", e);
			logger.error("resource = [" + resource + "], folder = [" + folder + "]");
			logger.error("resource.getBucket() = " + resource.getBucket());

			throw e;
		}
	}

	@Override
	public IStoredObjectContainer getDefaultContainer() {
		CredentialedDirectoryBucketContainer ret = new CredentialedDirectoryBucketContainer(defaultBucket, null);
		
		ret.setCredentials(new AWSContainerCredentials(AwsUtil.getCreds()));
		
		return ret;
	}

	@Override
	public IStoredObjectContainer getContainerFrom(IStoredObjectContainer cont, IStoredObjectReference ref) {
		if (!ref.isContainer())
			throw new RuntimeException("Not a container!");
		else {
			String extra = "";
			if (!ref.getDirBucketContainer().getFileDirectory().endsWith("/") &&
					!ref.getFilePath().startsWith("/"))
				extra = "/";
			
			return new DirectoryBucketContainer(ref.getDirBucketContainer().getBucket(),
					ref.getDirBucketContainer().getFileDirectory() + extra + ref.getFilePath());
		}
	}

	@Override
	public boolean createDirectory(IStoredObjectContainer db, String dirName) {
		// todo: fix callers passing null paths
		if (db.getBucket() != null && dirName != null) {
			Path dir = Paths.get(db.getFileDirectory(), dirName);

			AmazonS3 handle = this.getS3((CredentialedDirectoryBucketContainer)db);

			PutObjectRequest pr = new PutObjectRequest(db.getBucket(),
					dirName + "/", new ByteArrayInputStream(new byte[0]), null)
					.withStorageClass(StorageClass.Standard);

			handle.putObject(pr);
		}

		return true;
	}

	@Override
	public void removeDirectory(IStoredObjectContainer db, String dirName) {
		Path dir = Paths.get(db.getFileDirectory(), dirName);

		AmazonS3 handle = this.getS3((CredentialedDirectoryBucketContainer)db);
		
		DeleteObjectRequest dr = new DeleteObjectRequest(
				db.getBucket(), 
				dirName + "/");
		if (db.getBucket() != null && dirName != null) {
			//todo: handle invalid callers
			handle.deleteObject(dr);
		}

	}

	@Override
	public boolean emptyDirectoryMeansMissing() {
		return true;
	}

	@Override
	public void moveObject(IStoredObjectReference cont, IStoredObjectReference ref) throws IOException {
		AmazonS3 handle = (cont instanceof DirectoryBucketContainer) ? this.getS3((CredentialedDirectoryBucketContainer)cont)
				: this.getS3((CredentialedDirectoryBucketContainer)cont.getDirBucketContainer());

		CopyObjectRequest cr = new CopyObjectRequest(cont.getDirBucketContainer().getBucket(), 
				cont.getDirBucketContainer().getFileDirectory() == null ? cont.getFilePath() :
					cont.getDirBucketContainer().getFileDirectory() + "/" + cont.getFilePath(), 
				ref.getDirBucketContainer().getBucket(), ref.getDirBucketContainer().getFileDirectory() + "/" + ref.getFilePath());
		
		handle.copyObject(cr);
		
		deleteItem(cont);
		
		// TODO: remove source (for now we leave)
//		throw new UnsupportedOperationException("Cannot create directory in chunked object server");
	}
}
