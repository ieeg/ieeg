/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mapping.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class SnapshotMappingFactory {
	
	/**
	 * Return the ACLs associated with a pubId, else
	 * create a default ACL
	 * 
	 * @param pubId
	 * @return
	 */
	public ExtAclEntity getAcls(User user, String pubId) {
		DataSnapshotEntity entity = EntityLookup.getSingleton().getDataSnapshotEntityById(pubId);
		if (entity == null) {
			return EntityLookup.getSingleton().createNewDefaultExtAcl(user, pubId);
		} else
			return entity.getExtAcl();
	}
	
	/**
	 * List of snapshot entities --> list of snapshot IDs
	 * 
	 * @param snapshots
	 * @return
	 */
	public List<PresentableMetadata> asStringsFromSnapshots(List<DataSnapshotEntity> snapshots) {
		List<PresentableMetadata> ret = new ArrayList<PresentableMetadata>();
		for (DataSnapshotEntity ds: snapshots)
			ret.add(getSearchResult(ds));
		
		return ret;
	}
	
	/**
	 * List of PresentableMetadata (SearchResult) --> list of DataSnapshotEntity
	 *  
	 * @param snapshots
	 * @return
	 */
	public List<DataSnapshotEntity> asDataSnapshotEntityList(List<PresentableMetadata> snapshots) {
		return EntityLookup.getSingleton().getDataSnapshotEntities(snapshots);
	}
	
	/**
	 * SearchResult from DataSnapshotEntity.
	 * 
	 * @param dsE
	 * @return
	 */
	public SearchResult getSearchResult(DataSnapshotEntity dsE) {
		return new SearchResult(dsE.getPubId(),
				getCreator(dsE),
				dsE.getLabel(),
				getChannelRevIds(dsE.getPubId()),
				getChannelNames(dsE.getPubId()));
	}
	
	public List<String> getChannelNames(String pubId) {
		return EntityLookup.getSingleton().getChannelNamesFor(pubId);
	}
	public List<String> getChannelNames(DataSnapshotEntity dse) {
		return getChannelNames(dse.getPubId());
	}
	
	public List<String> getChannelRevIds(String pubId) {
		return EntityLookup.getSingleton().getChannelIdsFor(pubId);
	}
	
	public List<String> getChannelRevIds(DataSnapshotEntity dse) {
		return getChannelRevIds(dse.getPubId());
	}
	
	public String getCreator(DataSnapshotEntity dse) {
		return EntityLookup.getSingleton().getCreator(dse);
	}
	
}
