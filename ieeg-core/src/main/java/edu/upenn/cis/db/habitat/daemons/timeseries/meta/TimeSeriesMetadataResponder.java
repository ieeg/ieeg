/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.timeseries.meta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.model.TimeSeriesEntity;
import edu.upenn.cis.braintrust.model.TimeSeriesInfo;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.db.habitat.daemons.BasicResponder;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry;
import edu.upenn.cis.db.habitat.daemons.IMessageBroker;
import edu.upenn.cis.db.habitat.daemons.timeseries.meta.TimeSeriesMetadataRequestor.Request;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.PersistentObjectFactory;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceObjectNotFoundException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class TimeSeriesMetadataResponder 
	extends BasicResponder<TimeSeriesMetadataRequestor.Request, ChannelInfoDto, ChannelSpecifier> {
	private final static Logger logger = LoggerFactory
			.getLogger(TimeSeriesMetadataResponder.class);

	IPersistentObjectManager<TimeSeriesEntity, String> persistence;
//	FutureRegistry<TimeSeriesMetadataRequestor.Request, ChannelSpecifier> receivers;
	
	public static final String CHANNEL = TimeSeriesMetadataResponder.class.getCanonicalName(); 
	
	public TimeSeriesMetadataResponder(
//			FutureRegistry<TimeSeriesMetadataRequestor.Request, ChannelSpecifier> receivers,
			IMessageBroker<TimeSeriesMetadataRequestor.Request, ChannelSpecifier> requestReceiver,
			IMessageBroker<TimeSeriesMetadataRequestor.Request, ChannelInfoDto> requestSender) 
					throws PersistenceObjectNotFoundException {
		super(CHANNEL, requestReceiver, requestSender);

		persistence = (IPersistentObjectManager<TimeSeriesEntity, String>) 
				PersistentObjectFactory.getFactory().getPersistenceManager(TimeSeriesEntity.class);

		intercept(CHANNEL);
	}
	
	@Override
	public void call(TimeSeriesMetadataRequestor.Request request,
			ChannelSpecifier parameter) throws Exception {
		
		ChannelInfoDto value = null;

		TimeSeriesEntity entity = persistence.read(request.specifier.getId(), 
				persistence.getDefaultScope());
		
		System.out.println("Got entity: " + ((entity == null) ? "no" : "yes"));
		
		// No match -- TODO: trigger an error
		if (entity == null) {
			Exception e = new PersistenceObjectNotFoundException("Could not find " + request.specifier.getId());
			
			respondException(TimeSeriesMetadataRequestor.CHANNEL, request, e);
		} else {
			TimeSeriesInfo timeSeriesInfo = entity.getInfo();
			if (timeSeriesInfo != null) {
				value = new ChannelInfoDto(
						timeSeriesInfo.getTimeSeries().getPubId(),
						timeSeriesInfo.getId(),
						timeSeriesInfo.getInstitution(),
						timeSeriesInfo.getHeaderLength(),
						timeSeriesInfo.getSubjectFirstName(),
						timeSeriesInfo.getSubjectSecondName(),
						timeSeriesInfo.getSubjectThirdName(),
						timeSeriesInfo.getSubjectId(),
						timeSeriesInfo.getNumberOfSamples(),
						entity.getLabel(),
						timeSeriesInfo.getRecordingStartTime(),
						timeSeriesInfo.getRecordingEndTime(),
						timeSeriesInfo.getSamplingFrequency(),
						timeSeriesInfo.getLowFrequencyFilterSetting(),
						timeSeriesInfo.getHighFrequencyFilterSetting(),
						timeSeriesInfo.getNotchFilterFrequency(),
						timeSeriesInfo.getAcquisitionSystem(),
						timeSeriesInfo.getChannelComments(),
						timeSeriesInfo.getStudyComments(),
						timeSeriesInfo.getPhysicalChannelNumber(),
						timeSeriesInfo.getMaximumBlockLength(),
						timeSeriesInfo.getBlockInterval(),
						timeSeriesInfo.getBlockHeaderLength(),
						timeSeriesInfo.getMaximumDataValue(),
						timeSeriesInfo.getMinimumDataValue(),
						timeSeriesInfo.getVoltageConversionFactor(),
						entity.getFileKey());
				value.setModality(timeSeriesInfo.getDataModality());
				value.setBaseline(timeSeriesInfo.getBaseline());
				value.setMultiplier(timeSeriesInfo.getMultiplier());
				value.setUnits(timeSeriesInfo.getUnits());
			}
			
			respond(TimeSeriesMetadataRequestor.CHANNEL, request, value);
		}
	}

/*
	@Override
	public String addMEFPath(User user, String mefPath,
			ChannelInfoDto chInfoDto) {
		String m = "addMEFPath(User, String)";
		TimeSeriesEntity entity = timeSeriesDAO.findByTimeSeriesPath(mefPath);

		// skip if the path already exists!
		if (entity != null)
			return entity.getPubId();

		entity = new TimeSeriesEntity();
		TimeSeriesInfo tsInfo = new TimeSeriesInfo(
				entity,
				chInfoDto.getInstitution(),
				chInfoDto.getHeaderLength(),
				chInfoDto.getSubjectFirstName(),
				chInfoDto.getSubjectSecondName(),
				chInfoDto.getSubjectThirdName(),
				chInfoDto.getSubjectId(),
				chInfoDto.getNumberOfSamples(),
				chInfoDto.getChannelName(),
				chInfoDto.getRecordingStartTime(),
				chInfoDto.getRecordingEndTime(),
				chInfoDto.getSamplingFrequency(),
				chInfoDto.getLowFrequencyFilterSetting(),
				chInfoDto.getHighFrequencyFilterSetting(),
				chInfoDto.getNotchFilterFrequency(),
				chInfoDto.getAcquisitionSystem(),
				chInfoDto.getChannelComments(),
				chInfoDto.getStudyComments(),
				chInfoDto.getPhysicalChannelNumber(),
				chInfoDto.getMaximumBlockLength(),
				chInfoDto.getBlockInterval(),
				chInfoDto.getBlockHeaderLength(),
				chInfoDto.getMaximumDataValue(),
				chInfoDto.getMinimumDataValue(),
				chInfoDto.getVoltageConversionFactor(),
				chInfoDto.getPhysicalChannelNumber());

		entity.setInfo(tsInfo);
		entity.setFileKey(mefPath);
		entity.setLabel(chInfoDto.getChannelName());
		String pubId = BtUtil.newUuid();
		entity.setPubId(pubId);
		timeSeriesDAO.saveOrUpdate(entity);

		return pubId;
	}
 */
}
