/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.object;

import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.shared.StringKeyValue;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.Scope;


public abstract class EntityPersistence<T extends IEntity,K extends Serializable,P> 
implements IPersistentObjectManager<T,K> {
	ICreateObject<T, String, P> objectCreator;
	
	public EntityPersistence() {
		
	}
	
	public EntityPersistence(ICreateObject<T, String, P> creator) {
		objectCreator = creator;
	}
	
	public ICreateObject<T, String, P> getObjectCreator() {
		return objectCreator;
	}
	
	public void setObjectCreator(ICreateObject<T, String, P> creator) {
		objectCreator = creator;
	}
	
	public void setObjectEntity(IEntity entity) {
		objectCreator = (ICreateObject<T, String, P>) (entity.tableConstructor().getObjectCreator());
	}
	
	public interface ICreateObject<T,K,P> {
		public T create(K key, P optParent);
	}
	
	public T readOrCreateIdempotent(K key, String label, P optParent, Scope scope) {
		return readOrCreateIdempotent(key, label, optParent, getObjectCreator(), scope);
	}
	public T createTransient(String label, P optParent) {
		return createTransient(label, optParent, getObjectCreator());
	}
	public T createPersistent(String label, P optParent, Scope scope) {
		return createPersistent(label, optParent, getObjectCreator(), scope);
	}
	
	public abstract T readOrCreateIdempotent(K key, String label, P optParent, ICreateObject<T,String,P> creator, Scope scope);	
	public abstract T createTransient(String label, P optParent, ICreateObject<T,String,P> creator);
	public abstract T createPersistent(String label, P optParent, ICreateObject<T,String,P> creator, Scope scope);

	public void write(K k, T value, Scope scope) {
		write(value, scope);
	}
}
