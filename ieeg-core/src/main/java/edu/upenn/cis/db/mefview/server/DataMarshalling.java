/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TimeSeriesSearchResult;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.LogEntry;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.eeg.mef.MefHeader2;

public class DataMarshalling {
	
	private final static Logger logger = LoggerFactory
			.getLogger(DataMarshalling.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ DataMarshalling.class);
	
	static StringBuilder concatenateJson(List<String> json) {
		StringBuilder ret = new StringBuilder("[");

		boolean isFirst = true;
		for (String r : json) {
			if (isFirst)
				isFirst = false;
			else
				ret.append(",");
			ret.append("[");
			ret.append(r);
			ret.append("]");
		}

		ret.append("]");
		return ret;
	}

	public static Project convertEegProject(
			User user, 
			EegProject project,
			IUserAuth authCheck, 
			IDataSnapshotServer datasets) {
		Set<String> tags = Sets.newHashSet();
		Set<String> credits = Sets.newHashSet();
		tags.addAll(project.getTags());
		credits.addAll(project.getCredits());
		Project ret = new Project(project.getProjectId(),
				project.getPubId(),
				project.getName(),
				tags,
				credits,
				project.isComplete());
		
		for (UserId u: project.getAdmins()) {
			final User projAdmin = authCheck.getUser(u);
			if (projAdmin != null) {
				UserInfo ui = authCheck.getUserInfoHelper(projAdmin.getUsername());
				if (ui != null)
					ret.getAdmins().add(ui);
			}
		}
		
		for (UserId u: project.getTeam()) {
			final User projTeamMember = authCheck.getUser(u);
			if (projTeamMember != null) {
				UserInfo ui = authCheck.getUserInfoHelper(projTeamMember.getUsername());
				if (ui != null)
					ret.getTeam().add(ui);
			}
		}

		Set<DataSnapshotSearchResult> dsSearchResults =
				datasets.getSearchableDatasetsThatExist(
						user,
						project.getPubId());
		
		Set<SearchResult> searchResults = convertSearchResults(
				dsSearchResults,
				user, authCheck);

		ret.getSnapshots().addAll(searchResults);

		ret.getTools().addAll(
				authCheck.getToolsHelper(user, project.getTools()));
		
		return ret;
	}
	
	public static Project convertEegProject(
			User user, 
			EegProject project,
			IUserAuth authCheck, 
			Map<String, Set<DataSnapshotSearchResult>> pubIdToSearchResults) {
		Set<String> tags = Sets.newHashSet();
		Set<String> credits = Sets.newHashSet();
		tags.addAll(project.getTags());
		credits.addAll(project.getCredits());
		Project ret = new Project(project.getProjectId(),
				project.getPubId(),
				project.getName(),
				tags,
				credits,
				project.isComplete());
		
		for (UserId u: project.getAdmins()) {
			final User projAdmin = authCheck.getUser(u);
			if (projAdmin != null) {
				UserInfo ui = authCheck.getUserInfoHelper(projAdmin.getUsername());
				if (ui != null)
					ret.getAdmins().add(ui);
			}
		}
		
		for (UserId u: project.getTeam()) {
			final User projTeamMember = authCheck.getUser(u);
			if (projTeamMember != null) {
				UserInfo ui = authCheck.getUserInfoHelper(projTeamMember.getUsername());
				if (ui != null)
					ret.getTeam().add(ui);
			}
		}

		if (!pubIdToSearchResults.isEmpty()) {
			Set<DataSnapshotSearchResult> dsSearchResults = pubIdToSearchResults.get(project.getPubId());
		
			Set<SearchResult> searchResults = convertSearchResults(
					dsSearchResults,
					user, authCheck);

			ret.getSnapshots().addAll(searchResults);
		}
		ret.getTools().addAll(
				authCheck.getToolsHelper(user, project.getTools()));
		
		return ret;
	}
	
	public static List<FilterSpec> convertFilters(List<DisplayConfiguration> fpl) {
		List<FilterSpec> fs = Lists.newArrayList();
		for (DisplayConfiguration fp : fpl) {
			fs.add(new FilterSpec(fp.getFilterName(),
					fp.getFilterType(),
					fp.getNumPoles(),
					fp.getBandpassLowCutoff(),
					fp.getBandpassHighCutoff(),
					fp.getBandstopLowCutoff(),
					fp.getBandstopHighCutoff()));
		}
		return fs;
	}
	
	public static DerivedSnapshot convertFromDatasetAndTool(String studyRevId, SearchResult res,
			DatasetAndTool t) {
		return new DerivedSnapshot(t.getDsRevId(), t.getDsLabel(), t.getToolLabel(),
				studyRevId, res);
	}
	
	public static LogMessage convertLogEntryToMessage(final LogEntry entry,
			IUserAuth authCheck) {
		User user1 = null;
		if (entry.getRecipient() != null)
			user1 = authCheck.getEnabledUser(entry.getRecipient());
		User user2 = null;
		if (entry.getSender() != null)
			user2 = authCheck.getEnabledUser(entry.getSender());
		
		return new LogMessage(user1.getUserId(), user2.getUserId(),
				entry.getMessage(), entry.getTime(), entry.getVisibility(),
				entry.getEventId(), 0, null);
	}

	public static List<LogEntry> convertLogMessagesToEntries(List<LogMessage> messages,
			IUserAuth authCheck) {
		List<LogEntry> ret = Lists.newArrayList();
		
		for (LogMessage m: messages) {
			if (authCheck != null) {
				try {
				ret.add(new LogEntry(
						authCheck.getUser(m.getRecipient()).getUsername(),
						authCheck.getUser(m.getSender()).getUsername(),
						m.getMessage(),
						m.getTime(),
						m.getVisibility(),
						m.getEventId()));
				} catch (Throwable t) {
					ret.add(new LogEntry(
							(m.getRecipient() == null) ? "" : m.getRecipient().getName(),
							(m.getSender() == null) ? "" : m.getSender().getName(),
							m.getMessage(),
							m.getTime(),
							m.getVisibility(),
							m.getEventId()));
				}
			} else {
				ret.add(new LogEntry(
						(m.getRecipient() == null) ? "" : m.getRecipient().getName(),
						(m.getSender() == null) ? "" : m.getSender().getName(),
						m.getMessage(),
						m.getTime(),
						m.getVisibility(),
						m.getEventId()));
			}
		}
		
		return ret;
	}

//	static List<ProvenanceOperation> convertProvenanceEntriesToOperations(
//			List<ProvenanceLogEntry> entries,
//			IUserAuth authCheck) {
//		List<ProvenanceOperation> ret = Lists.newArrayList();
//		
//		for (ProvenanceLogEntry prov: entries) {
//			ProvenanceOperation.USAGE_TYPE utype;
//			switch (prov.getUsage()) {
//			case CREATES:
//				utype = ProvenanceOperation.USAGE_TYPE.CREATES;
//				break;
//			case ADDSTO:
//				utype = ProvenanceOperation.USAGE_TYPE.ADDSTO;
//				break;
//			case VIEWS:
//				utype = ProvenanceOperation.USAGE_TYPE.VIEWS;
//				break;
//			case ANNOTATES:
//				utype = ProvenanceOperation.USAGE_TYPE.ANNOTATES;
//				break;
//			case DERIVES: 
//				utype = ProvenanceOperation.USAGE_TYPE.DERIVES;
//				break;
//			case DISCUSSES:
//				utype = ProvenanceOperation.USAGE_TYPE.DISCUSSES;
//				break;
//			case FAVORITES:
//				utype = ProvenanceOperation.USAGE_TYPE.DERIVES;
//				break;
//			default:
//				throw new RuntimeException("Unable to cast enums");
//			}
//			User u = authCheck.getEnabledUser(prov.getUserId());
//			ret.add(new ProvenanceOperation(u.getUsername(),
//					utype, 
//					prov.getSnapshot(), 
//					prov.getTime(),
//					prov.getDerivedSnapshot(), 
//					prov.getAnnotation()));
//		}
//		
//		return ret;
//	}
	
//	static ProvenanceLogEntry convertProvenanceOpToEntry(
//			ProvenanceOperation entry,
//			IUserAuth authCheck) {
//		ProvenanceLogEntry.UsageType utype;
//		switch (entry.getUsage()) {
//		case CREATES:
//			utype = ProvenanceLogEntry.UsageType.CREATES;
//			break;
//		case ADDSTO:
//			utype = ProvenanceLogEntry.UsageType.ADDSTO;
//			break;
//		case VIEWS:
//			utype = ProvenanceLogEntry.UsageType.VIEWS;
//			break;
//		case ANNOTATES:
//			utype = ProvenanceLogEntry.UsageType.ANNOTATES;
//			break;
//		case DERIVES: 
//			utype = ProvenanceLogEntry.UsageType.DERIVES;
//			break;
//		case DISCUSSES:
//			utype = ProvenanceLogEntry.UsageType.DISCUSSES;
//			break;
//		case FAVORITES:
//			utype = ProvenanceLogEntry.UsageType.DERIVES;
//			break;
//		default:
//			throw new RuntimeException("Unable to cast enums");
//		}
//		return new ProvenanceLogEntry(
//				authCheck.getEnabledUser(entry.getUserId()).getUserId(),
//				utype,
//				entry.getSnapshot(),
//				entry.getTime(),
//				entry.getDerivedSnapshot(),
//				entry.getAnnotation());
//	}

	/**
	 * Converts to a GWT-marshallable search result for data snapshots
	 * 
	 * @param dsr
	 * @return
	 */
	public static SearchResult convertSearchResult(
			DataSnapshotSearchResult dsr, 
			User user,
			IUserAuth authCheck) {
		SearchResult ret = new SearchResult();
		
		User creator = null;
		
		if (authCheck != null && dsr.getOwner() != null)
			try {
				creator = authCheck.getEnabledUser(dsr.getOwner());
			} catch (AuthorizationException e) {
				creator = null;
			}

		ret.setDatasetRevId(dsr.getId());
		ret.setImageCount(dsr.getImageCount());
		ret.setAnnotationCount(dsr.getTsAnnCount());
		ret.setType(dsr.getType());
		if (creator != null)
			ret.setCreator(creator.getUsername());

		if (dsr.getTimeSeries() != null && !dsr.getTimeSeries().isEmpty()) {
			for (TimeSeriesSearchResult ts : dsr.getTimeSeries())
				ret.addTimeSeries(ts);
//				if (ts != null) {
//					ret.addTimeSeriesRevId(ts.getRevId());
//					ret.addTimeSeriesLabel(ts.getLabel());
//				}
		}
		String label = dsr.getLabel();
		ret.setFriendlyName(label);
		ret.setStartUutc(dsr.getStartTimeUutc());
		ret.setEndUutc(dsr.getEndTimeUutc());
		ret.setOrganization(dsr.getOrganization());
		ret.setCreationTime(dsr.getCreationTime());
		
		return ret;
	}

	/**
	 * Converts a set of data snapshot results to a set of GWT-marshallable ones
	 * 
	 * @param dsrs
	 * @return
	 */
	public static Set<SearchResult> convertSearchResults(
			Collection<DataSnapshotSearchResult> dsrs, 
			User user,
			IUserAuth authCheck) {
		Set<SearchResult> ret = new HashSet<SearchResult>();

		for (DataSnapshotSearchResult dsr : dsrs) {
			if (dsr != null)
				ret.add(convertSearchResult(dsr, user, authCheck));
		}

		return ret;
	}

	public static TimeSeries[] convertTimeSeriesToGwtSegments(List<List<TimeSeriesData>> ts) {
		TimeSeries[] ret = new TimeSeries[ts.size()];
		for (int i = 0; i < ts.size(); i++)
			ret[i] = toGwtSegment(ts.get(i).get(0));
		
		return ret;
	}

	public static List<String> convertTimeSeriesToJson(List<List<TimeSeriesData>> series,
			double requestedStart, double requestedSamplingPeriod, double requestedDuration,
			double thresholdedPeriod) {
		final String M = "converTimeSeriesToJson";
		long resultCreationIn = System.nanoTime();
		
		final List<String> results = Lists.newArrayList();

		// See if we got ANY results
		if (series != null && !series.isEmpty() && series.get(0) != null) {
			int r = series.size();
			for (int z = 0; z < series.get(0).size(); z++) {
				int count = -1;
				boolean isMinMaxOverall = false;
				// Max number of traces
				for (int j = 0; j < r; j++) {
					try {
						if (count < series.get(j).get(z).getSeriesLength())
							count = series.get(j).get(z).getSeriesLength();
					} catch (Exception e) {
						logger.error(M + ": Error getting series length.", e);
					}
					if (series.size() > j && series.get(j).size() > z)
						isMinMaxOverall = isMinMaxOverall || series.get(j).get(z).isMinMax();
				}

				// Put a header:
				// actual start time
				// actual period (capped by sample rate in the data)
				// number of samples
				// requested start
				// requested duration
				double actualStart = series.get(0).get(z).getStartTime();
				
				results.add(String.valueOf((long) (actualStart)) + ","
						+ String.valueOf(thresholdedPeriod) + "," +
						String.valueOf(count) + ","
						+ String.valueOf((long) requestedStart) + ","
						+ String.valueOf(requestedDuration) + ","
						+ isMinMaxOverall);

				logger.trace(
						"{}: Adding series beginning at {} and going for {} samples",
						new Object[] { M, actualStart, count });
				if (isMinMaxOverall) {
					ts2JsonMinMaxHelper(series, z, count, results);
				} else {
					ts2JsonHelper(series, z, count, results);
				}
			}
		} else {
			// Put a header:
			// actual start time
			// actual period
			// number of samples
			// requested start
			// requested duration
			double actualStart = 0;

			results.add(String.valueOf((long) (actualStart)) + ","
					+ String.valueOf(thresholdedPeriod) + "," +
					String.valueOf(0) + ","
					+ String.valueOf((long) requestedStart) + ","
					+ String.valueOf(requestedDuration));
		}
		timeLogger.trace("{} results creation {} seconds", M,
				BtUtil.diffNowThenSeconds(resultCreationIn));

		return results;
	}

	private static void ts2JsonHelper(
			List<List<TimeSeriesData>> series,
			int seriesColumn, 
			int numSamples, 
			final List<String> results) {
		final String m = "ts2JsonHelper(...)";
		final int numChannels = series.size();
		int missing = 0;

		// Iterate through each timestamp
		for (int i = 0; i < numSamples; i++) {
			StringBuilder buf = new StringBuilder();
			boolean first = true;
			for (int ch = 0; ch < numChannels; ch++) {
				if (!first)
					buf.append(",");
				else
					first = false;
				final TimeSeriesData timeSeriesData = series.get(ch).get(seriesColumn);
				if ((timeSeriesData == null) ||
						timeSeriesData.getSeries() == null) {
					logger.warn("{}: Series {} is totally missing column {}", m, ch, seriesColumn);
				} else if (timeSeriesData.getSeriesLength() > i) {
					// Initial value
					if (timeSeriesData.isInGap(i)) {
						buf.append("null");
						missing++;
					} else {
						buf.append(timeSeriesData.getAt(i));
					}
				} else {
					buf.append("null");
					missing++;
				}
			}
			results.add(buf.toString());
		}
		logger.trace("{}: Results: {} by {} minus {}",
				new Object[] { m, numChannels, numSamples, missing });
	}
	
	private static void ts2JsonMinMaxHelper(
			List<List<TimeSeriesData>> series,
			int seriesColumn, 
			int numSamplesPerChannel, 
			final List<String> results) {
		final String m = "ts2JsonMinMaxHelper(...)";
		if (numSamplesPerChannel % 2 != 0) {
			throw new IllegalArgumentException("TimeSeriesData with isMinMax true should have even number of samples. Has " + numSamplesPerChannel);
		}
		final int numChannels = series.size();
		int missing = 0;

		// Iterate through each timestamp
		int samplesCreated = 0;
		int minMaxIdx = 0;
		int nonMinMaxIdx = 0;
		while (samplesCreated < numSamplesPerChannel) {
			final StringBuilder minBuf = new StringBuilder();
			final StringBuilder maxBuf = new StringBuilder();
			boolean first = true;
			for (int ch = 0; ch < numChannels; ch++) {
				if (!first) {
					minBuf.append(",");
					maxBuf.append(",");
				} else {
					first = false;
				}
				final TimeSeriesData timeSeriesData = series.get(ch).get(seriesColumn);
				if ((timeSeriesData == null) ||
						timeSeriesData.getSeries() == null) {
					logger.warn("{}: Series {} is totally missing column {}", m, ch, seriesColumn);
					continue;
				}
				
				//Overall we are doing min/max, but this particular channel might not be min/max if recording sample rate is small enough.
				final boolean chIsMinMax = timeSeriesData.isMinMax();
				if (chIsMinMax && minMaxIdx + 1 < timeSeriesData.getSeriesLength()) {
					if (timeSeriesData.isInGap(minMaxIdx)) {
						if (!timeSeriesData.isInGap(minMaxIdx + 1)) {
							throw new IllegalArgumentException("Found min/max pair which do not agree on isInGap()");
						}
						minBuf.append("null");
						maxBuf.append("null");
						missing++;
						
					} else {
						minBuf.append(timeSeriesData.getAt(minMaxIdx));
						maxBuf.append(timeSeriesData.getAt(minMaxIdx + 1));
					}
				} else if (!chIsMinMax && nonMinMaxIdx < timeSeriesData.getSeriesLength()){
					if (timeSeriesData.isInGap(nonMinMaxIdx)) {
						minBuf.append("null");
						maxBuf.append("null");
						missing++;
					} else {
						minBuf.append(timeSeriesData.getAt(nonMinMaxIdx));
						maxBuf.append(timeSeriesData.getAt(nonMinMaxIdx));
					}
				} else {
					minBuf.append("null");
					maxBuf.append("null");
					missing++;
				}
				
			}
			results.add(minBuf.toString());
			results.add(maxBuf.toString());
			samplesCreated +=2;
			minMaxIdx +=2;
			nonMinMaxIdx++;
		}
		logger.trace("{}: Results: {} by {} minus {}",
				new Object[] { m, numChannels, numSamplesPerChannel, missing });
	}
	
	public static List<TraceInfo> convertChannelInfoToTraceInfo(
			IDataset parent, List<ChannelInfoDto> channels) {
		List<TraceInfo> ret = new ArrayList<TraceInfo>(channels.size());

		for (ChannelInfoDto ch : channels) {
			ret.add(new TraceInfo(
					parent,
					ch.getRecordingStartTime(),
					ch.getRecordingEndTime() - ch.getRecordingStartTime(),
					ch.getSamplingFrequency(),
					ch.getVoltageConversionFactor(),
					ch.getMinimumDataValue(),
					ch.getMaximumDataValue(),
					ch.getInstitution(),
					ch.getAcquisitionSystem(),
					ch.getChannelComments(),
					ch.getSubjectId(),
					ch.getChannelName(),
					ch.getId()));
		}
		return ret;
	}

	public static TraceInfo convertChannelInfoToTraceInfo(
			IDataset parent, ChannelInfoDto ch) {
		
		return new TraceInfo(
					parent,
					ch.getRecordingStartTime(),
					ch.getRecordingEndTime() - ch.getRecordingStartTime(),
					ch.getSamplingFrequency(),
					ch.getVoltageConversionFactor(),
					ch.getMinimumDataValue(),
					ch.getMaximumDataValue(),
					ch.getInstitution(),
					ch.getAcquisitionSystem(),
					ch.getChannelComments(),
					ch.getSubjectId(),
					ch.getChannelName(),
					ch.getId());
	}

	public static TraceInfo convertMefHeaderTraceInfo(
			IDataset parent, MefHeader2 ch, String revId) {
		
		TraceInfo ti = new TraceInfo(
					parent,
					ch.getRecordingStartTime(),
					ch.getRecordingEndTime() - ch.getRecordingStartTime(),
					ch.getSamplingFrequency(),
					ch.getVoltageConversionFactor(),
					ch.getMinimumDataValue(),
					ch.getMaximumDataValue(),
					ch.getInstitution(),
					ch.getAcquisitionSystem(),
					ch.getChannelComments(),
					ch.getSubjectId(),
					ch.getChannelName(),
					revId);
		
		ti.setMultiplier(ch.getVoltageConversionFactor());
		
		return ti;
	}

	public static List<TraceInfo> convertTimeSeriesToTraceInfo(
			List<TimeSeriesDto> series,
			List<ChannelSpecifier> paths,
			TraceServer traces) throws IllegalArgumentException, IOException {
		List<TraceInfo> channelList = new ArrayList<TraceInfo>();
		int i = 0;
		for (TimeSeriesDto ts : series) {
			// String tracePath;
			ChannelSpecifier tracePath = paths.get(i++);
//			tracePath = traces.getChannelSpecifier(user, snap, ts.getId(),
//					0);

			logger.trace("Scanning trace " + tracePath);
			// String p = sourcePath + tracePath;
			TraceInfo ti = new TraceInfo((IDataset)((SerializableMetadata)tracePath).getParent(), 
					traces.getZero(tracePath),
					traces.getDuration(tracePath), traces
							.getSampleRate(tracePath), traces
							.getVoltageConversionFactor(tracePath), traces
							.getMinSampleValue(tracePath), traces
							.getMaxSampleValue(tracePath), traces
							.getInstitution(tracePath), traces
							.getAcquisitionSystem(tracePath), traces
							.getChannelComments(tracePath), traces
							.getSubjectId(tracePath), ts.getLabel(), ts
							.getId());
			
			ti.setMultiplier(traces.getVoltageConversionFactor(tracePath));

			channelList.add(ti);
		}
		return channelList;
	}

	public static TraceInfo convertTimeSeriesToTraceInfo(
			IDataset parent,
			TimeSeriesDto ts,
			ChannelSpecifier tracePath,
			TraceServer traces) throws IllegalArgumentException, IOException {

		TraceInfo ti = new TraceInfo(
				parent, 
				traces.getZero(tracePath),
				traces.getDuration(tracePath), traces
						.getSampleRate(tracePath), traces
						.getVoltageConversionFactor(tracePath), traces
						.getMinSampleValue(tracePath), traces
						.getMaxSampleValue(tracePath), traces
						.getInstitution(tracePath), traces
						.getAcquisitionSystem(tracePath), traces
						.getChannelComments(tracePath), traces
						.getSubjectId(tracePath), 
						 ts.getLabel(), ts
						.getId());
		
		ti.setMultiplier(traces.getVoltageConversionFactor(tracePath));
		
		return ti;
	}

	public static TraceInfo convertTimeSeriesToTraceInfo(
			IDataset parent,
			TimeSeriesDto ts) throws IllegalArgumentException, IOException {

		return new TraceInfo(
				parent, 
				0,
				0, 
				0, 
				1, 
				0, 
				0, 
				"", 
				"", 
				"", 
				"", 
						 ts.getLabel(), ts
						.getId());
	}

	public static EegProject convertToEegProject(Project project) {
		EegProject ret = new EegProject(project.getProjectId(),
				project.getPubId(),
				project.getName(),
				project.getTags(),
				project.getCredits(),
				project.isComplete());
		
		for (UserInfo u: project.getAdmins())
			ret.getAdmins().add(new UserId(u.getUid().getIdentifier()));
		for (UserInfo u: project.getTeam())
			ret.getTeam().add(new UserId(u.getUid().getIdentifier()));
		for (PresentableMetadata sr: project.getSnapshots())
			ret.getSnapshots().add(sr.getId());
		for (ToolDto ti: project.getTools())
			ret.getTools().add(ti.getId().get());

		return ret;
	}

	// private static String extractMEFName(String str) {
	// int firstSlash = str.indexOf('/');
	// return str.substring(firstSlash + 1, str.indexOf('/', firstSlash + 1));
	// }

//	static List<Post> convertToPosts(List<PostingOnItem> posts,
//			IUserAuth authCheck) throws AuthorizationException {
//		List<Post> ret = Lists.newArrayList();
//		
//		for (PostingOnItem post: posts) {
//			User u = authCheck.getEnabledUser(post.getAuthorId());
//			ret.add(new Post(u.getUsername(), 
//					post.getTitle(),
//					post.getPost(),
//					post.getSnapshotId(),
//					post.getTime(),
//					post.getPostId(),
//					post.getRefId(),
//					post.getAnnotationId()));
//		}
//		return ret;
//	}
//
//	static PostingOnItem convertToSnapshotPosting(Post p,
//			IUserAuth authCheck) throws AuthorizationException {
//		User u = authCheck.getEnabledUser(p.getAuthorId());
//		return new PostingOnItem(u.getUserId(), 
//				p.getTitle(),
//				p.getPost(),
//				p.getSnapshotId(),
//				p.getTime(),
//				p.getPostId(),
//				p.getRefId(),
//				p.getAnnotationId());
//		/*
//			final String author,
//			final String post,
//			final String snapshotId,
//			final Timestamp time,
//			
//			@Nullable final String postId,
//			@Nullable final String refId,
//			@Nullable final Long annId
//		 */
//	}

	public static TimeSeries toGwtSegment(TimeSeriesData tsd) {
		int values[] = tsd.getSeries();
		
		int[] gStart = Ints.toArray(tsd.getGapStart());
		int[] gEnd = Ints.toArray(tsd.getGapEnd());
		
		if (tsd.getSeriesLength() != values.length) {
			values = new int[tsd.getSeriesLength()];
			for (int i = 0; i < tsd.getSeriesLength(); i++)
				values[i] = tsd.getAt(i);
		}
		TimeSeries s2 = new TimeSeries(tsd.getStartTime(), tsd.getPeriod(), tsd.getScale(), values, gStart, gEnd);
		return s2;
	}

	public static List<String> getTimeSegmentIds(List<INamedTimeSegment> segments) {
		List<String> ret = new ArrayList<String>();
		for (INamedTimeSegment seg: segments) {
			ret.add(seg.getId());
		}
		return ret;
	}

	public static Set<String> getTimeSegmentIds(Set<INamedTimeSegment> segments) {
		Set<String> ret = new HashSet<String>();
		for (INamedTimeSegment seg: segments) {
			ret.add(seg.getId());
		}
		return ret;
	}
}
