/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.mefview.services.FilterSpec;

@XmlRootElement(name="EEGPlace")
public class EEGPlaceSerializable extends PlaceSerializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(EEGPlaceSerializable.class);
	
	String snapshotID;
	
	List<Boolean> channelsEnabled = new ArrayList<Boolean>();
	List<FilterSpec> filters = new ArrayList<FilterSpec>();
	long position = -1;
	long width = -1;
	boolean autoScale = false;
	double scaleFactor = 1.0;
	
	public EEGPlaceSerializable() {
		super("eeg");
		snapshotID = "";
	}
	
	public EEGPlaceSerializable(String str) {
		super("eeg");
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(EEGPlaceSerializable.class);
			
			Unmarshaller unmarshaller = context.createUnmarshaller();
			
			StringReader sr = new StringReader(str);
			
			unmarshaller.unmarshal(sr);
		} catch (JAXBException e) {
			logger.error("ignoring exception", e);
		}
	}
	
	public String getSnapshotID() {
		return snapshotID;
	}

	public void setSnapshotID(String snapshotID) {
		this.snapshotID = snapshotID;
	}

	public List<Boolean> getChannelsEnabled() {
		return channelsEnabled;
	}

	public void setChannelsEnabled(List<Boolean> channelsEnabled) {
		this.channelsEnabled = channelsEnabled;
	}

	public List<FilterSpec> getFilters() {
		return filters;
	}

	public void setFilters(List<FilterSpec> filters) {
		this.filters = filters;
	}

	public long getPosition() {
		return position;
	}

	public void setPosition(long position) {
		this.position = position;
	}

	public long getWidth() {
		return width;
	}

	public void setWidth(long width) {
		this.width = width;
	}

	public boolean getAutoScale() {
		return autoScale;
	}

	public void setAutoScale(boolean autoScale) {
		this.autoScale = autoScale;
	}

	public double getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
	
}
