/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import java.io.File;
import java.io.FileNotFoundException;

import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

public class SnapshotCatalogDb {
	private Environment env;
	private static final String CLASS_CATALOG = "ieeg_class_catalog";
	private static final String SNAPSHOTS_CATALOG = "snapshots_catalog";
	
	private StoredClassCatalog javaCatalog;
	private Database snapshotsDb;
	
	public SnapshotCatalogDb(final String basePath) throws DatabaseException, FileNotFoundException {
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setTransactional(true);
		envConfig.setAllowCreate(true);
		
		env = new Environment(new File(basePath), envConfig);
		
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setTransactional(true);
		dbConfig.setAllowCreate(true);
		Database catalogDb = env.openDatabase(null, CLASS_CATALOG,
				dbConfig);
		javaCatalog = new StoredClassCatalog(catalogDb);
		
		snapshotsDb = env.openDatabase(null, SNAPSHOTS_CATALOG, dbConfig);
	}
	
	public final StoredClassCatalog getClassCatalog() {
		return javaCatalog;
	}
	
	public Database getSnapshotDatabase() {
		return snapshotsDb;
	}
	
	public Environment getEnvironment() {
		return env;
	}
	
	public void close() throws DatabaseException {
		snapshotsDb.close();
		javaCatalog.close();
		env.close();
	}
}
