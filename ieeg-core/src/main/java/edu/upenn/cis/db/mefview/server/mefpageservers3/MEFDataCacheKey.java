/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;
import java.util.Arrays;

import javax.annotation.concurrent.Immutable;

/**
 * Deliberately package-private
 * 
 * @author Sam Donnelly
 */
@Immutable
public final class MEFDataCacheKey implements Serializable {
	private static final long serialVersionUID = 1L;
	private final byte[] fileKey;
	private final long pageNo;
	private final byte[] fileEtag;

	public MEFDataCacheKey(
			String fileKey,
			long pageNo,
			String fileEtag) {
		this.fileKey = fileKey.getBytes();

		checkArgument(pageNo >= 0);
		this.pageNo = pageNo;

		this.fileEtag = fileEtag.getBytes();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof MEFDataCacheKey)) {
			return false;
		}
		MEFDataCacheKey other = (MEFDataCacheKey) obj;
		if (!Arrays.equals(fileEtag, other.fileEtag)) {
			return false;
		}
		if (!Arrays.equals(fileKey, other.fileKey)) {
			return false;
		}
		if (pageNo != other.pageNo) {
			return false;
		}
		return true;
	}

	public String getFileEtag() {
		return new String(fileEtag);
	}

	public String getFileKey() {
		return new String(fileKey);
	}

	public long getPageNo() {
		return pageNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(fileEtag);
		result = prime * result + Arrays.hashCode(fileKey);
		result = prime * result + (int) (pageNo ^ (pageNo >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "FileKeyPageNo [fileKey=" + new String(fileKey)
				+ ", pageNo=" + pageNo + "]";
	}
}