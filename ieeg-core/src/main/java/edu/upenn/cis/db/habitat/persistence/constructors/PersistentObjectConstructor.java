/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.constructors;

import edu.upenn.cis.braintrust.imodel.IPersistentObject;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager.IPersistentKey;

/**
 * Constructor for persistent objects: both entities and relationships
 * 
 * @author zives
 *
 * @param <T>
 * @param <K>
 */
public abstract class PersistentObjectConstructor<T extends IPersistentObject,K> {
	Class<T> theClass;
	String name;
	
	IPersistentObjectManager.IPersistentKey<T, K> getKey;
	
	public PersistentObjectConstructor(Class<T> theClass, IPersistentKey<T, K> getKey) {
		super();
		this.theClass = theClass;
		this.name = theClass.getCanonicalName();
		this.getKey = getKey;
	}
	
	/**
	 * Class object
	 * @return
	 */
	public Class<T> getTheClass() {
		return theClass;
	}
	public void setTheClass(Class<T> theClass) {
		this.theClass = theClass;
	}
	
	/**
	 * Canonical name of class object
	 * @return
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Key-fetch object
	 * @return
	 */
	public IPersistentObjectManager.IPersistentKey<T, K> getKeyFetcher() {
		return getKey;
	}
	public void setKeyFetcher(IPersistentObjectManager.IPersistentKey<T, K> getKey) {
		this.getKey = getKey;
	}
}