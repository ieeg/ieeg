/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.jobs;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Set;

import edu.upenn.cis.braintrust.shared.TaskDto;
import edu.upenn.cis.braintrust.shared.TaskStatus;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask;
import edu.upenn.cis.db.mefview.services.TimeSeriesTask.STATUS;

public class TaskAssembler {

	public static TaskDto toTask(TimeSeriesTask tsTask) {
		final TaskDto task = new TaskDto(tsTask.getName(),
				toTaskStatus(tsTask.getStatus()), tsTask.getStartTime(),
				tsTask.getEndTime(), tsTask.getStartedRunning(),
				tsTask.getAssignedWorker());
		for (String timeSeriesId : tsTask.getChannels()) {
			task.getTimeSeriesIds().add(timeSeriesId);
		}
		return task;
	}

	public static TaskStatus toTaskStatus(TimeSeriesTask.STATUS tstStatus) {
		checkNotNull(tstStatus);
		switch (tstStatus) {
			case UNSCHEDULED:
				return TaskStatus.UNSCHEDULED;
			case RUNNING:
				return TaskStatus.RUNNING;
			case COMPLETE:
				return TaskStatus.COMPLETE;
			case KILLED:
				return TaskStatus.KILLED;
			default:
				throw new IllegalArgumentException(
						"Unkown TimeSeriesTask.STATUS: " + tstStatus);
		}
	}

	public static STATUS of(TaskStatus taskStatus) {
		switch (taskStatus) {
			case COMPLETE:
				return STATUS.COMPLETE;
			case KILLED:
				return STATUS.KILLED;
			case RUNNING:
				return STATUS.RUNNING;
			case UNSCHEDULED:
				return STATUS.UNSCHEDULED;
			default:
				throw new IllegalArgumentException(
						"unknown TaskStatus: " + taskStatus);
		}
	}

	public static Set<TaskDto> toTaskSet(Iterable<TimeSeriesTask> tstTasks) {
		final Set<TaskDto> tasks = newHashSet();
		for (final TimeSeriesTask tstTask : tstTasks) {
			tasks.add(toTask(tstTask));
		}
		return tasks;
	}
}
