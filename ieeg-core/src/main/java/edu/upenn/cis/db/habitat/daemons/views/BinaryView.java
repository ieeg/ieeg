/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons.views;

import java.io.Serializable;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class BinaryView implements IDataView, JsonTyped, Serializable {

	String filename;
	int startIndex;
	int endIndex;
	
	public BinaryView() {}
	
	public BinaryView(String filename, int startIndex, int endIndex) {
		this.filename = filename;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	@Override
	public boolean isCompatibleWith(IDataView two) {
		return (two instanceof BinaryView);
	}

	@Override
	public boolean overlapsWith(IDataView two) {
		if (isCompatibleWith(two)) {
			BinaryView t = (BinaryView)two;
			
			return filename.equals(t.filename) && 
					startIndex <= t.endIndex && endIndex >= t.startIndex;  
		}
		return false;
	}

	@Override
	public boolean contains(IDataView two) {
		if (isCompatibleWith(two)) {
			BinaryView t = (BinaryView)two;
			
			return filename.equals(t.filename) && 
					startIndex <= t.startIndex && endIndex >= t.endIndex;  
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Integer.valueOf(startIndex).hashCode() ^
				Integer.valueOf(endIndex).hashCode();
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}
}
