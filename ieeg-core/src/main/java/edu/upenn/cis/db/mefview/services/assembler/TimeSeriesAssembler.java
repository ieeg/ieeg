/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services.assembler;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newTreeSet;

import java.util.Set;
import java.util.SortedSet;

import com.google.common.base.Function;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.db.mefview.services.TimeSeries;

public class TimeSeriesAssembler {

	Function<TimeSeriesDto, TimeSeries> toTimeSeries = new Function<TimeSeriesDto, TimeSeries>() {

		public TimeSeries apply(TimeSeriesDto input) {
			return toTimeSeries(input);
		}

	};

	Function<TimeSeries, TimeSeriesDto> toTimeSeriesDto = new Function<TimeSeries, TimeSeriesDto>() {

		public TimeSeriesDto apply(TimeSeries input) {
			return toTimeSeriesDto(input);
		}

	};

	/**
	 * Returns a new {@link TimeSeriesDto} with same info as {@code timeSeries}.
	 * 
	 * @param timeSeries
	 * @return a new {@link TimeSeriesDto} with same info as {@code timeSeries}
	 */
	public TimeSeriesDto toTimeSeriesDto(final TimeSeries timeSeries) {
		final TimeSeriesDto gts = new TimeSeriesDto(timeSeries.getRevId(),
				timeSeries.getLabel(), null);
		return gts;

	}

	/**
	 * Returns a new {@link TimeSeries} with same info as {@code timeSeriesDto}.
	 * 
	 * @param timeSeriesDto
	 * @return a new {@link TimeSeries} with same info as {@code timeSeriesDto}
	 */

	public TimeSeries toTimeSeries(final TimeSeriesDto timeSeriesDto) {
		final TimeSeries ts = new TimeSeries(timeSeriesDto.getId(),
				timeSeriesDto.getLabel());
		return ts;

	}

	public SortedSet<TimeSeries> toTimeSeriesSortedSet(
			final Set<TimeSeriesDto> timeSeriesDtoSet) {
		final SortedSet<TimeSeries> timeSeries = newTreeSet(transform(
				timeSeriesDtoSet,
				toTimeSeries));
		return timeSeries;
	}

	public Set<TimeSeriesDto> toTimeSeriesDtoSet(
			final Set<TimeSeries> timeSeriesSet) {
		final Set<TimeSeriesDto> timeSeriesDto = newHashSet(transform(
				timeSeriesSet,
				toTimeSeriesDto));
		return timeSeriesDto;
	}
}
