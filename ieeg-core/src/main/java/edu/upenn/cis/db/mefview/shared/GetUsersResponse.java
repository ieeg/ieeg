/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.shared.UserId;

@GwtCompatible(serializable = true)
public class GetUsersResponse implements Serializable {

	@GwtCompatible(serializable = true)
	public static class UserIdAndName implements Serializable {

		private static final long serialVersionUID = 1L;
		private UserId userId;
		private String username;

		// For Gwt
		@SuppressWarnings("unused")
		private UserIdAndName() {}

		public UserIdAndName(UserId userId, String username) {
			this.userId = checkNotNull(userId);
			this.username = checkNotNull(username);
		}

		@Nonnull
		public UserId getUserId() {
			return userId;
		}

		@Nonnull
		public String getUsername() {
			return username;
		}
	}

	private static final long serialVersionUID = 1L;
	private List<UserIdAndName> users;
	private int totalCount;

	// For GWT
	@SuppressWarnings("unused")
	private GetUsersResponse() {}

	public GetUsersResponse(final List<UserIdAndName> users,
			final int totalCount) {
		this.users = checkNotNull(users);
		this.totalCount = totalCount;
	}

	@Nonnull
	public List<UserIdAndName> getUsers() {
		return users;
	}

	public int getTotalCount() {
		return totalCount;
	}

}
