/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.IeegException;

@GwtCompatible(serializable = true)
public class TooMuchDataRequestedException extends IeegException {

	private static final long serialVersionUID = 1L;

	private int requestedChannels;
	private double requestedSeconds;
	private double requestedHz;
	private long maxHz;
	private long maxChannels;
	private long maxSeconds;

	//For GWT
	@SuppressWarnings("unused")
	private TooMuchDataRequestedException() {
		this(
				-1,
				-1,
				-1,
				-1,
				-1,
				-1);
	};

	public TooMuchDataRequestedException(
			int requestedChannels,
			double requestedLengthSeconds,
			double requestedHz,
			long maxHz,
			long maxChannels,
			long maxSeconds) {
		super(
				"too much data was requested. The current (hz * channels * seconds) max is ("
						+ maxHz
						+ " * "
						+ maxChannels
						+ " * "
						+ maxSeconds
						+ "). requested "
						+ requestedHz
						+ " hz, "
						+ requestedChannels
						+ ", "
						+ requestedLengthSeconds
						+ " seconds");
		this.requestedChannels = requestedChannels;
		this.requestedSeconds = requestedLengthSeconds;
		this.requestedChannels = requestedChannels;
		this.maxHz = maxHz;
		this.maxChannels = maxChannels;
		this.maxSeconds = maxSeconds;
	}

	public long getMaxChannels() {
		return maxChannels;
	}

	public long getMaxHz() {
		return maxHz;
	}

	public long getMaxSeconds() {
		return maxSeconds;
	}

	public int getRequestedChannels() {
		return requestedChannels;
	}

	public double getRequestedHz() {
		return requestedHz;
	}

	public double getRequestedSeconds() {
		return requestedSeconds;
	}

}
