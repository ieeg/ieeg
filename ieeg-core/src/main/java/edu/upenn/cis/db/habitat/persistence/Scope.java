/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence;


public abstract class Scope {
	protected boolean gcSession = false;
	protected boolean gcTrans = false;
	
	public abstract boolean isUnitializedScope();
	
	public abstract void save(Object o);
	
	public abstract void saveOrUpdate(Object o);
	
	public abstract void createNewScope();
	
	public abstract void commitScope();

	public abstract void rollbackScope();

	public abstract void delete(Object o);
}
