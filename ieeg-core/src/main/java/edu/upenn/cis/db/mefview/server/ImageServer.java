/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Iterables.getOnlyElement;
import static java.util.Collections.singleton;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.dto.ImageForStudy;
import edu.upenn.cis.braintrust.dto.ImageForTrace;
import edu.upenn.cis.braintrust.dto.TraceForImage;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.services.IImagesForStudyResource;
import edu.upenn.cis.braintrust.services.ITracesForImageResource;
import edu.upenn.cis.braintrust.services.ImagesForStudyResource;
import edu.upenn.cis.braintrust.services.TracesForImageResource;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;

public class ImageServer {
	static IDAOFactory daoFactory = new HibernateDAOFactory();
	private final static Logger logger = LoggerFactory
			.getLogger(ImageServer.class);
	static {
		// this initialization only needs to be done once per VM
		RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
	}
	
	
	/**
	 * Utility class -- temp file placeholder.  When file
	 * is garbage collected, it will also be removed.
	 * 
	 * @author zives
	 *
	 */
	public static class TempFile {
		String fileName;
		File file;
		
		public TempFile(final String fileName) {
			this.fileName = fileName;			
		}
		
		public TempFile(final String fileName, final File file) {
			this.fileName = fileName;			
			this.file = file;
		}
		
		public void setFile(final File file) {
			this.file = file;
		}
		
		public File getFile() {
			return file;
		}
		
		protected void finalize() throws Throwable {
			if (file != null) {
				file.delete();
			}
		}
	}

	public static final int CACHE_SIZE = 1024;
	public static int token = 100;
	private static Map<String,TempFile> dicomMap = Collections.synchronizedMap(new LruCache<String,TempFile>(CACHE_SIZE));

	/**
	 * Gets a new png temp file for the corresponding DICOM image
	 * 
	 * @param name
	 * @return
	 */
	public static File getTempFile(final String name) {
		TempFile ret;
		if ((ret = dicomMap.get(name)) == null) {
			String newName = name + (token++) + ".png";
			File fil = new File(newName);
			fil.deleteOnExit();
			
			dicomMap.put(name, new TempFile(newName, fil));
			return ret.getFile();
		}
		return null;
	}

	public static File getTempFileIfExists(final String name) {
		TempFile ret;
		if ((ret = dicomMap.get(name)) != null) {
			return ret.getFile();
		} else
			return null;
	}
	
	private final IDataSnapshotServer ds;
	private final IUrlFactory imageUrlFactory;

	public ImageServer(IDataSnapshotServer dSservice, IUrlFactory imageUrlFactory) {
		this.ds = dSservice;
		this.imageUrlFactory = imageUrlFactory;
	}
	
	

	public Set<ImageInfo> getImageInfo(User user, String datasetId, String traceId, String trace, String typ)
	throws IllegalArgumentException, AuthorizationException {
		final String M = "getImageInfo(...)";

		logger.debug("{}: Info for {} / {}", new Object[] {M, datasetId, trace});
		String path = getOnlyElement(ds.getMEFPaths(user, datasetId, singleton(traceId)));
		path = path.substring(0, path.lastIndexOf('/'));
		path = path.substring(0, path.lastIndexOf('/'));

		logger.debug("{}: {}", M, path);
		
		final Set<ImageForTrace> imagesForTrace = ds.getImagesForTimeSeries(user, datasetId, traceId);

		Set<ImageInfo> imageInfo = new HashSet<ImageInfo>(imagesForTrace.size());

		logger.debug("{}: Info for {} / {} with {} entries...",
				new Object[] {M, datasetId, trace, imagesForTrace.size()});
		for (ImageForTrace i: imagesForTrace) {
			final String imageFileKey = i.getImageFileKey();
			final String imageUrl = imageUrlFactory.getUrl(imageFileKey);
			ImageInfo ii = new ImageInfo(
					imageUrl,
					imageFileKey,
					i.getImageTypeEnum().name(),
					i.getPixelCoordX(),
					i.getPixelCoordY(),
					i.getImagePubId());
			logger.debug("{}:  - {}", M, ii.getName());
			imageInfo.add(ii);
		}
		return imageInfo;
	}
	

	public Set<TraceImageInfo> getTracesForImage(User user, String imageWithPath)
	throws IllegalArgumentException, AuthorizationException {
		final String M = "getTracesForImage(...)";
		ITracesForImageResource tracesForImageResource = new TracesForImageResource(daoFactory);
		
		final Set<TraceForImage> tracesForImage = tracesForImageResource.getTracesForImage(imageWithPath);

		Set<TraceImageInfo> traceImageInfo = new HashSet<TraceImageInfo>(tracesForImage.size());

		logger.debug("{}: Looking up {} with {} entries...",
				new Object[] {M, imageWithPath, tracesForImage.size()});
		for (TraceForImage t: tracesForImage) {
			TraceImageInfo ii = new TraceImageInfo(t.getChannel(), t.getPixelCoordX(), t.getPixelCoordY());
			logger.debug("{}: - {}", M, ii.getTraceName());
			traceImageInfo.add(ii);
		}
		return traceImageInfo;
	}


	public Set<ImageInfo> getImagesForStudy(User user, String dataset)
	throws IllegalArgumentException, AuthorizationException {
		final String M = "getImagesForStudy(...)";
		IImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
				daoFactory);

		final Set<ImageForStudy> allImagesForStudy = imagesForStudyResource
				.getAllImagesForStudy(ds.getDatasetPortion(dataset) + "/");

		Set<ImageInfo> imageInfo = new HashSet<ImageInfo>(
				allImagesForStudy.size());

		logger.debug("{}: Looking up {} with {} entries...",
				new Object[] {M, dataset, allImagesForStudy.size()});
		for (ImageForStudy i : allImagesForStudy) {
			final String imageFileKey = i.getImageFileKey();
			final String imageUrl = imageUrlFactory.getUrl(imageFileKey);
			ImageInfo ii = new ImageInfo(
					imageUrl,
					imageFileKey,
					i.getImageTypeEnum().name(),
					0,
					0,
					null);
			logger.debug("{}:  - {}", M, ii.getName());
			imageInfo.add(ii);
		}
		return imageInfo;
	}
	
	
	public Set<ImageInfo> getImagesForStudy(User user, String dataset, String typ)
	throws IllegalArgumentException, AuthorizationException {
		final String M = "getImagesForStudy(...)";
		IImagesForStudyResource imagesForStudyResource = new ImagesForStudyResource(
				daoFactory);

		final Set<ImageForStudy> allImagesForStudy = imagesForStudyResource
				.getImagesForStudy(ds.getDatasetPortion(dataset) + "/", typ);

		Set<ImageInfo> imageInfo = new HashSet<ImageInfo>(
				allImagesForStudy.size());

		logger.debug("{}: Looking up {} with {} entries...",
				new Object[] {M, dataset, allImagesForStudy.size()});
		for (ImageForStudy i : allImagesForStudy) {
			final String imageFileKey = i.getImageFileKey();
			final String imageUrl = imageUrlFactory.getUrl(imageFileKey);
			ImageInfo ii = new ImageInfo(
					imageUrl,
					imageFileKey,
					i.getImageTypeEnum().name(),
					0,
					0,
					null);
			logger.debug("{}: - {}", M, ii.getName());
			imageInfo.add(ii);
		}
		return imageInfo;
	}

	public InputStream getPng(String imageName,
			int width, int height) throws IOException, FileNotFoundException {
		
		BufferedImage image = ImageIO.read(new File(imageName));
		
	    if (image != null) {
	    	BufferedImage resizedImage = image;
	    	
	    	if (width > -1 && height > -1) {
		        resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		        Graphics2D g = resizedImage.createGraphics();
		        g.drawImage(image, 0, 0, width, height, null);
		        g.dispose();
		        g.setComposite(AlphaComposite.Src);
		        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		        g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    	}
	        
	        final ByteArrayOutputStream out = new ByteArrayOutputStream();
	            ImageIO.write(resizedImage, "png", out);
	 
            final byte[] imgData = out.toByteArray();
 
            return new ByteArrayInputStream(imgData);
 	    }
	    return null;
	}
	
	public InputStream getJpeg(String imageName,
			int width, int height) throws IOException, FileNotFoundException {
		
		BufferedImage image = ImageIO.read(new File(imageName));
		
	    if (image != null) {
	    	BufferedImage resizedImage = image;
	    	
	    	if (width > -1 && height > -1) {
		        resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		        Graphics2D g = resizedImage.createGraphics();
		        g.drawImage(image, 0, 0, width, height, null);
		        g.dispose();
		        g.setComposite(AlphaComposite.Src);
		        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		        g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
		        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    	}
	        
	        final ByteArrayOutputStream out = new ByteArrayOutputStream();
	            ImageIO.write(resizedImage, "jpg", out);
	 
            final byte[] imgData = out.toByteArray();
 
            return new ByteArrayInputStream(imgData);
 	    }
	    return null;
	}
	
//	public InputStream getDicom(String imageName,
//			int index) throws IOException, FileNotFoundException {
//		
//		File cached = getTempFileIfExists(imageName);
//		if (cached != null) {
//            InputStream bigInputStream = new BufferedInputStream(new FileInputStream(cached.getName()));
//
//	        return bigInputStream;
// 		}
//		
//    	// load image
//    	ImagePlus ip = DicomOpener.loadImage(new File(imageName), null, null);
//    	
//	    if (ip != null) {
//        	File f = getTempFile(imageName);
//        	IJ.saveAs(ip, "png", f.getName());
//    		
//            final InputStream bigInputStream =
//            	new BufferedInputStream(new FileInputStream(f.getName()));
// 
//            return bigInputStream;
//	    }
//	    return null;
//	}
}
