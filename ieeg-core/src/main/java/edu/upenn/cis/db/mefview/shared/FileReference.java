/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;


/**
 * A handle (ref) to a file that could either be on the filesystem or in S3
 * 
 * @author zives
 *
 */
@GwtCompatible(serializable = true)
public class FileReference implements IStoredObjectReference, Serializable, Comparable<IStoredObjectReference> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String s3Key;
	DirectoryBucketContainer dirBucketContainer;
	String filePath;
	boolean isContainer;
	
	public FileReference() {
		
	}
	
	public FileReference(DirectoryBucketContainer container, String s3key, String file) {
		this.dirBucketContainer = container;
		this.s3Key = s3key;
		this.filePath = file;
		this.isContainer = false;
	}

	public FileReference(String file) {
		this.dirBucketContainer = null;
		this.s3Key = file;
		this.filePath = file;
		this.isContainer = false;
	}
	
	public void setDirBucketContainer(IStoredObjectContainer container) {
		this.dirBucketContainer = (DirectoryBucketContainer)container;
	}
	
	@Override
	public DirectoryBucketContainer getDirBucketContainer() {
		return dirBucketContainer;
	}

	@Override
	public String getObjectKey() {
		if (s3Key == null)
			return getFilePath();
		else
			return s3Key;
	}

	@Override
	public String getFilePath() {
		return filePath;
	}

	public void setObjectKey(String s3Key) {
		this.s3Key = s3Key;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		String cont = (getDirBucketContainer() == null) ? "" : getDirBucketContainer().toString(); 
		return "[(" + cont + ")" + filePath + "; key: " + s3Key + "]";
	}

	@Override
	public int compareTo(IStoredObjectReference o) {
		return filePath.compareTo(o.getFilePath());
	}

	@Override
	public boolean isContainer() {
		return isContainer;
	}
	
	public void setContainer() {
		isContainer = true;
	}

	public void setContainer(boolean value) {
		isContainer = value;
	}
}
