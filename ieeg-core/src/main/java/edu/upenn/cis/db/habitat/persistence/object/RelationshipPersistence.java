/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.object;

import java.io.Serializable;

import edu.upenn.cis.braintrust.imodel.IEntity;
import edu.upenn.cis.braintrust.imodel.IRelationship;
import edu.upenn.cis.db.habitat.persistence.IPersistentObjectManager;
import edu.upenn.cis.db.habitat.persistence.Scope;


public abstract class RelationshipPersistence<T extends IRelationship, K extends Serializable, 
E1 extends IEntity, E2 extends IEntity> 
implements IPersistentObjectManager<T, K> {
	public interface ICreateRelationship<T,K,E1, E2> {
		public T create(K key, E1 entity1, E2 entity2);
	}

	protected ICreateRelationship<T, String, E1, E2> objectCreator;
	
	public RelationshipPersistence(ICreateRelationship<T, String, E1, E2> creator) {
		objectCreator = creator;
	}

	public void setObjectCreator(ICreateRelationship<T, String, E1, E2> creator) {
		objectCreator = creator;
	}
	
	public void setObjectRelationship(IRelationship rel) {
		objectCreator = (ICreateRelationship<T, String, E1, E2>) (rel.tableConstructor().getCreateObject());
	}
	
//	public abstract boolean alreadyRelated(E1 entity1, E2 entity2);
//	public abstract List<T> readRelationship(E1 entity1, E2 entity2);
	
	public T readOrCreateIdempotent(K key, String label, E1 entity1, E2 entity2, Scope scope) {
		return readOrCreateIdempotent(key, label, entity1, entity2, objectCreator, scope);
	}
	public T createTransient(String label, E1 entity1, E2 entity2) {
		return createTransient(label, entity1, entity2, objectCreator);
	}
	public T createPersistent(String label, E1 entity1, E2 entity2, Scope scope) {
		return createPersistent(label, entity1, entity2, objectCreator, scope);
	}
	
	public abstract T readOrCreateIdempotent(K key, String label, E1 entity1, E2 entity2, ICreateRelationship<T, String, E1, E2> creator, Scope scope);	
	public abstract T createTransient(String label, E1 entity1, E2 entity2, ICreateRelationship<T, String, E1, E2> creator);
	public abstract T createPersistent(String label, E1 entity1, E2 entity2, ICreateRelationship<T, String, E1, E2> creator, Scope scope);

	public void write(K k, T value, Scope scope) {
		write(value, scope);
	}
}
