/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The top level web service methods should convert any exceptions into this
 * type so that they can be mapped by {@link IeegWsExceptionMapper} for the
 * client.
 * 
 * @author John Frommeyer
 * 
 */
public class IeegWsException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final Logger logger =
			LoggerFactory.getLogger(IeegWsException.class);
	private final int httpStatus;
	private final String errorCode;
	private final String subMessage;
	private final String errorId = UUID.randomUUID().toString();
	private String hostname;

	
	public IeegWsException(String subMessage, IeegWsError error) {
		final String m = "constructor";
		this.httpStatus = error.getHttpStatus();
		this.errorCode = error.getCode();
		this.subMessage = checkNotNull(subMessage);
		try {
			this.hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.error(m + ": Could not get local hostname for errorId ["
					+ errorId + "], setting hostname to 'unknown'", e);
			this.hostname = "unkown";
		}
	}

	public String getMessage() {
		String msg;
		if (errorCode.equals(IeegWsError.AUTHORIZATION_FAILURE
				.getCode())) {
			msg = "Authorization failure. Error code: ["
					+ this.errorCode
					+ "], Error message: ["
					+ this.subMessage
					+ "], host: ["
					+ this.hostname
					+ "], error id: ["
					+ this.errorId + "].";
		} else if (errorCode.equals(IeegWsError.AUTHENTICATION_FAILURE
				.getCode())) {
			msg = "Authentication failure. Please check your credentials. Error code: ["
					+ this.errorCode
					+ "], Error message: ["
					+ this.subMessage
					+ "], host: ["
					+ this.hostname
					+ "], error id: ["
					+ this.errorId + "].";
		} else {
			final String statusDescr = Response.Status.fromStatusCode(
					this.httpStatus).getReasonPhrase();
			msg = "The server responded with "
					+ this.httpStatus
					+ " "
					+ statusDescr
					+ ". If reporting the error please include the following: Error code: ["
					+ this.errorCode
					+ "], Error message: ["
					+ subMessage
					+ "], host: ["
					+ this.hostname
					+ "], error id: ["
					+ this.errorId + "].";
		}
		return msg;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorId() {
		return errorId;
	}

	public String getHostname() {
		return hostname;
	}
}
