/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceObjectNotFoundException;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class BasicResponder<R extends IMessage<V2>, V extends JsonTyped, V2 extends JsonTyped> {
//	FutureRegistry<R, V> registry;
	IMessageBroker<R, V2> requestReceiver;
	IMessageBroker<R, V> requestSender;
	String CHANNEL;
	
	public BasicResponder(final String channel,
//			FutureRegistry<R, V> registry,
			IMessageBroker<R, V2> requestReceiver,
			IMessageBroker<R, V> requestSender) 
					throws PersistenceObjectNotFoundException {
//		this.registry = registry;
		this.requestSender = requestSender;
		this.requestReceiver = requestReceiver;
		this.CHANNEL = channel;
	}
	
	protected void intercept() {
		intercept(CHANNEL);
	}
	
	/**
	 * Intercept all requests to the channel
	 * 
	 * @param channel
	 */
	protected void intercept(final String channel) {
		requestReceiver.intercept(channel, null, 
				new IMessageBroker.IConsumer<R, V2>() {

			@Override
			public void handle(R key, V2 value) {
				System.out.println("Received metadata request");
				try {
					call(key, value);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void handleError(R key, JsonTyped value) {
				System.out.println("Error during metadata request");
				BasicResponder.this.requestSender.sendError(channel, key, value);
			}
			
		});

	}
	
	protected void respond(R request, V value) {
		respond(CHANNEL, request, value);
	}
	
	protected void respond(final String channel, R request, V value) {
		requestSender.send(channel, request, value);
	}
	
	protected void respondException(R request, Exception e) {
		respondException(CHANNEL, request, e);
	}
	
	protected void respondException(final String channel, R request, Exception e) {
		requestSender.sendError(channel, request, (JsonTyped)e);
	}
	
	public abstract void call(R request, V2 value) throws Exception;
}
