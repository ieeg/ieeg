/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.persistence.mapping;

import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.db.mefview.shared.Task;

public class EntityMappingFactory {
	//new EntityLookup(snapshot, permission, user, uService);
	
	public static UserTaskEntity mapFromIdempotent(Task task) {
		// TODO: query for the task first!
		
		return UserTaskMapper.INSTANCE.taskToUserTaskEntity(task);
	}
	
	public static Task mapFrom(UserTaskEntity taskEntity) {
		return UserTaskMapper.INSTANCE.userTaskEntityToTask(taskEntity);
	}
}
