/*
 * Copyright 2015 Zachary G. Ives
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mapping.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.ExtAclEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.model.UserTaskEntity;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

/**
 * Map between different representations of a User.
 * 
 * @author zives
 *
 */
public class UserMappingFactory {
	
	/**
	 * List of UserEntities --> list of user names
	 * 
	 * @param users
	 * @return
	 */
	public List<String> asStringsFromEntities(List<UserEntity> users) {
		List<String> ret = new ArrayList<String>();
		for (UserEntity u: users)
			ret.add(u.getUsername());
		
		return ret;
	}
	
	/**
	 * Return the ACLs associated with a pubId, else
	 * create a default ACL
	 * 
	 * @param pubId
	 * @return
	 */
	public ExtAclEntity getAclsIdempotent(User user, String pubId) {
		DataSnapshotEntity entity = EntityLookup.getSingleton().getDataSnapshotEntityById(pubId);
		if (entity == null) {
			return EntityLookup.getSingleton().createNewDefaultExtAcl(user, pubId);
		} else
			return entity.getExtAcl();
	}
	
	/**
	 * Return the ACLs associated with a pubId, else
	 * create a default ACL
	 * 
	 * @param pubId
	 * @return
	 */
	public ExtAclEntity getAclsIdempotent(UserId userId, String pubId) {
		User user = EntityLookup.getSingleton().getUserFor(userId);
		return getAclsIdempotent(user, pubId);
	}

	/**
	 * User names --> look up UserEntities
	 * 
	 * @param users
	 * @return
	 */
	public List<UserEntity> asUserEntityList(List<String> users) {
		return EntityLookup.getSingleton().getUserEntitiesFor(users);
	}

	public UserEntity getUserEntityFor(User user) {
		return EntityLookup.getSingleton().getUserEntityFor(user.getUsername());
	}
	
	public User getUserEntityFor(UserEntity user) {
		return EntityLookup.getSingleton().getUserByEntity(user);
	}
	
	public UserId getUserIdFor(UserEntity user) {
		return user.getId();
	}
	
	public UserEntity getUserEntityFor(UserId id) {
		return EntityLookup.getSingleton().getUserEntityFor(id);
	}

	public User getUserFor(UserId id) {
		return EntityLookup.getSingleton().getUserFor(id);
	}
}
