/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.block;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.eeg.TimeSeriesChannel;

public class SnapshotHeader implements Serializable {
	boolean isRed;
	double frequency;
	List<String> channels;
	List<TimeSeriesChannel> headers;
	long[][] timeIndex;
	
	public SnapshotHeader(boolean isCompressed, List<String> channels, double frequency) {
		isRed = isCompressed;
		timeIndex = new long[channels.size()][];
		headers = new ArrayList<TimeSeriesChannel>();
		this.channels = channels;
		this.frequency = frequency;
	}

	public SnapshotHeader(boolean isCompressed, long[][] times) {
		isRed = isCompressed;
		timeIndex = times;
	}

	public boolean isCompressed() {
		return isRed;
	}

	public void setIsCompressed(boolean isRed) {
		this.isRed = isRed;
	}
	
	public long[] getTimeIndex(int channelId){
		return timeIndex[channelId];
	}
	
	public void setTimeIndex(int channelId, long[] index) {
		timeIndex[channelId] = index;
	}
	
	public long[] getTimeIndex(String channelId) {
		int i = channels.indexOf(channelId);
		
		if (i == -1)
			return null;
		else return getTimeIndex(i);
	}

	public long[][] getTimeIndices() {
		return timeIndex;
	}

	public List<String> getChannels() {
		return channels;
	}

	public void setChannels(List<String> channels) {
		this.channels = channels;
	}

	public List<TimeSeriesChannel> getHeaders() {
		return headers;
	}

	public void setHeaders(List<TimeSeriesChannel> headers) {
		this.headers = headers;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

}
