/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;

@GwtCompatible(serializable=true)
@JsonIgnoreProperties(ignoreUnknown=true)
public class TaskSubmission extends PresentableMetadata implements Serializable, JsonTyped {
	Long dbId = null;
	
	Date submissionDate;
	String submissionId;
	
	String submissionComments;
	
	JsonKeyValueSet submissionValues;
	
	JsonKeyValueSet submissionParameters;

	String taskId;
	UserId theUser;
	String location = "habitat://main";
	
//	Task task = null;
//	User theUser = null;
	
	public TaskSubmission() {
		submissionDate = new Date();
		submissionComments = null;//Optional.absent();
		submissionParameters =  null;//Optional.absent();
		submissionValues = new JsonKeyValueSet();
	}
	

	public TaskSubmission(
//			@NotNull Task task,
			@NotNull Date submissionDate, 
			@Nullable String submissionComments,
			JsonKeyValueSet submissionValues,
			JsonKeyValueSet submissionParameters) {
		super();
//		this.task = task;
		this.submissionDate = submissionDate;
		this.submissionComments = submissionComments;
		if (submissionValues == null)
			this.submissionValues = new JsonKeyValueSet();
		else
			this.submissionValues = submissionValues;
		this.submissionParameters = (submissionParameters);
	}

//	public Task getTask() {
//		return task;
//	}
//
//
//
//	public void setTask(Task task) {
//		this.task = task;
//	}



	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getSubmissionComments() {
		return submissionComments;
	}

	public void setSubmissionComments(String submissionComments) {
		this.submissionComments = (submissionComments);
	}

	public JsonKeyValueSet getSubmissionValues() {
		return submissionValues;
	}

	public void setSubmissionValues(JsonKeyValueSet submissionValues) {
		this.submissionValues = submissionValues;
	}

//	public void addSubmission(IJsonKeyValue value) {
//		getSubmissionValues().add(value);
//	}

	public JsonKeyValueSet getSubmissionParameters() {
		return submissionParameters;
	}

	public void setSubmissionParameters(JsonKeyValueSet submissionParameters) {
		this.submissionParameters = (submissionParameters);
	}



	public UserId getTheUser() {
		return theUser;
	}



	public void setTheUser(UserId theUser) {
		this.theUser = theUser;
	}



	public String getSubmissionId() {
		return submissionId;
	}



	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}


	@Override
	public SerializableMetadata getParent() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setParent(GeneralMetadata p) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getMetadataCategory() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void setId(String id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Set<String> getKeys() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getStringValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public VALUE_TYPE getValueType(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Double getDoubleValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Integer getIntegerValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public GeneralMetadata getMetadataValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<String> getStringSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getFriendlyName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<String> getCreators() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	public Long getDbId() {
		return dbId;
	}


	public void setDbId(Long dbId) {
		this.dbId = dbId;
	}


	@JsonIgnore
	@Transient
	@Override
	public String getContentDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Date getCreationTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Transient
	@Override
	public double getRelevanceScore() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public Map<String, Set<String>> getUserPermissions() {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonIgnore
	@Transient
	@Override
	public Set<String> getAssociatedDataTypes() {
		return new HashSet<String>();
	}

//	@JsonIgnore
//	@Transient
//	@Override
//	public MetadataFormatter getFormatter() {
//		// TODO Auto-generated method stub
//		return null;
//	}


	@JsonIgnore
	@Transient
	@Override
	public boolean isLeaf() {
		// TODO Auto-generated method stub
		return false;
	}

	@JsonIgnore
	@Transient
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	@JsonIgnore
	@Override
	public String getObjectName() {
		return "Submission";
	}
}
