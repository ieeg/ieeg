/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.db.mefview.server.exceptions.StaleMEFException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.eeg.mef.MEFDefs;
import edu.upenn.cis.eeg.mef.MefHeader2;

public class MEFPageServerS3Util {

	public static long getChapter(
			long pageNo,
			long chapterLength) {
		return (pageNo / chapterLength) * chapterLength;
	}

	public static long getChapterSize(
			int minDataReqBytes,
			MefHeader2 header) {
		long chapterSize = Math.max(
				1,
				minDataReqBytes
						/ header.getMaximumCompressedBlockSize());
		return chapterSize;
	}

	public static byte[] getHeader(
			AmazonS3 s3,
			String bucket,
			ChannelSpecifier chSpec)
			throws
			IOException,
			StaleMEFException {
		InputStream s3ObjectInputStream = null;
		try {
			S3Object s3Object =
					AwsUtil.requestS3Object(
							s3,
							AwsUtil.createGetObjectRequest(
									bucket,
									chSpec.getHandle(),
									0,
									MEFDefs.MEF_HEADER_LENGTH - 1,
									chSpec.getCheckStr()),
							"header");
			if (s3Object == null) {
				throw new StaleMEFException(chSpec.getId());
			}
			byte[] buffer =
					new byte[
					Ints.checkedCast(s3Object.getObjectMetadata()
							.getContentLength())];
			s3ObjectInputStream = s3Object.getObjectContent();
			ByteStreams.readFully(s3ObjectInputStream, buffer);
			return buffer;
		} finally {
			BtUtil.close(s3ObjectInputStream);
		}
	}

	private MEFPageServerS3Util() {}
}
