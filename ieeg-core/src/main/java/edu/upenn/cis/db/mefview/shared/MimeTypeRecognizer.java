package edu.upenn.cis.db.mefview.shared;

public class MimeTypeRecognizer {
	
	
	public final static String IEEG_JSON_ANNOTATION_MIME_TYPE_NAME = "application/x-ieeg-json-annotations"; 
	public final static String IEEG_JSON_MONTAGE_MIME_TYPE_NAME = "application/x-ieeg-json-montages";
	public final static String GRASS_TWIN_ANNOTATION_MIME_TYPE_NAME = "application/x-grass-twin-annotations";
	private final static String medtronicRegex = "/?[^/]*/medtronic/.*";
	private final static String neuropaceRegex = "/?[^/]*/neuropace/.*";
	private final static String kahanaRegex = "/?[^/]*/kahana/.*";

	/**
	 * Attempts to classify a file's mime type by looking at its name and extension.
	 * In some cases it needs a full upload/organization path to differentiate.
	 * 
	 * @param file
	 * @return
	 */
	public static String getMimeTypeForFile(String file) {
		if (file.toLowerCase().endsWith(".png"))
			return "image/png";
		if (file.toLowerCase().endsWith(".jpg"))
			return "image/jpeg";
		if (file.toLowerCase().endsWith(".xls") || file.toLowerCase().endsWith(".xlsx"))
			return "application/excel";
		if (file.toLowerCase().endsWith(".doc") || file.toLowerCase().endsWith(".docx"))
			return "application/msword";
		if (file.toLowerCase().endsWith(".mef"))
			return "application/mef";
		else if (file.toLowerCase().endsWith(".mat"))
			return "application/mat";
		else if (file.toLowerCase().matches(kahanaRegex))
			return "application/kahana";
		else if (file.toLowerCase().endsWith(".pdf"))
			return "application/pdf";
		else if (file.toLowerCase().endsWith(".edf"))
			return "application/edf";
		else if (file.toLowerCase().endsWith(".bni"))
			return "application/bni";
		else if (file.toLowerCase().matches(medtronicRegex) &&
				file.toLowerCase().endsWith(".xml"))
			return "application/medtronic";
		else if (file.toLowerCase().endsWith(".ent.txt")
				|| file.toLowerCase().endsWith("/natus-annotations.txt")) 
			return "application/x-natus-annotations";
		else if (file.toLowerCase().endsWith(".tfn.csv")) 
			return GRASS_TWIN_ANNOTATION_MIME_TYPE_NAME;
		else if (file.toLowerCase().matches(medtronicRegex) &&
				(file.toLowerCase().endsWith(".text")
				|| file.toLowerCase().endsWith(".txt")))
			return "application/mdt-csv";
		else if (file.toLowerCase().matches(neuropaceRegex) &&
				(file.toLowerCase().endsWith(".lay")))
			return "application/neuropace";
		else if (file.toLowerCase().endsWith(".lay"))
			return "application/persyst";
		else if (file.toLowerCase().endsWith(".dcm"))
			return "application/dicom";
		else if (file.toLowerCase().endsWith(".nii"))
			return "application/x-nifti";
		else if (file.toLowerCase().endsWith(".nii.gz"))
			return "application/x-nifti-gz";
		else if (file.toLowerCase().endsWith(".mgz"))
			return "application/x-mgh-nmr-gz";
		else if (file.toLowerCase().endsWith(".mgh"))
			return "application/x-mgh-nmr";
		else if (file.toLowerCase().endsWith(".xml"))
			return "application/xml";
		else if (file.toLowerCase().endsWith(".txt")
				|| file.toLowerCase().endsWith(".text"))
			return "text/plain";
		else if (file.toLowerCase().endsWith(".html"))
			return "text/html";
		else if (file.toLowerCase().endsWith(".iann.json"))
			return IEEG_JSON_ANNOTATION_MIME_TYPE_NAME;
		else if (file.toLowerCase().endsWith(".imtg.json"))
			return IEEG_JSON_MONTAGE_MIME_TYPE_NAME;
		else if (file.toLowerCase().endsWith(".json"))
			return "application/json";
		else if (file.toLowerCase().endsWith(".control"))
			return "application/control";
		else if (file.toLowerCase().endsWith(".zip"))
			return "application/zip";
		else if (file.toLowerCase().endsWith(".gz"))
			return "application/x-gzip";
		else if (file.toLowerCase().endsWith(".mnc"))
			return "application/x-minc";
		else if (file.toLowerCase().endsWith(".ima"))
			return "application/x-siemens-ima";
		else if (file.toLowerCase().endsWith(".plx"))
			return "application/x-plexon";
		else if (file.toLowerCase().endsWith(".events"))
			return "application/x-blackfynn-events";
		else if (file.toLowerCase().endsWith(".c") || 
				file.toLowerCase().endsWith(".h") || 
				file.toLowerCase().endsWith(".hxx") || 
				file.toLowerCase().endsWith(".hpp") || 
				file.toLowerCase().endsWith(".cxx") || 
				file.toLowerCase().endsWith(".cpp"))
			return "application/cpp";
		else if (file.toLowerCase().endsWith(".java"))
			return "application/java";
		else if (file.toLowerCase().endsWith(".py"))
			return "application/python";
		else if (file.toLowerCase().endsWith(".pl"))
			return "application/perl";
		else if (file.toLowerCase().endsWith(".sh"))
			return "application/x-shell";
		else if (file.toLowerCase().endsWith(".m"))
			return "application/x-matlab-source";
		else if (isDirectory(file))
			return "application/directory";
		else if (file.toLowerCase().endsWith("/ieeg-dataset.ini"))
			return "application/x-ieeg-import-config";
		
		
		return "application/octet-stream";
	}

	public static String getIconForMimeType(String mimeType){
		switch (mimeType){
			case "image/png":case "image/jpeg":
				return "image:image";
			case "application/dicom":
			case "application/x-nifti":
				return "blackfynn:image";
			case "application/pdf":
				return "blackfynn:pdf";
			case "application/msword":
				return "blackfynn:word";
			case "application/excel":
				return "blackfynn:xls";
			case "application/mat":
				return "blackfynn:matlab";
			case "application/x-plexon":
				return "blackfynn:unit-data";
			case "application/python":
				return "blackfynn:python";
			case "application/R":
				return "blackfynn:R";
			case "application/mef":
				return "blackfynn:time-series";

		}

		return "description";
	}
	
	public static boolean isImage(String mimeType) {
		return mimeType.startsWith("image/")
			|| mimeType.equals("application/dicom")
			|| mimeType.equals("application/x-nifti")
			|| mimeType.equals("application/x-siemens-ima")
			|| mimeType.equals("application/x-nifti-gz");		
	}

	public static boolean isArchive(String mimeType) {
		return mimeType.equals("application/zip")
			|| mimeType.equals("application/x-gzip")
			|| mimeType.equals("application/x-nifti-gz");		
	}

	public static boolean isDocument(String mimeType) {
		return mimeType.startsWith("text/")
			|| mimeType.equals("application/xml")
			|| mimeType.equals("application/json")
			|| mimeType.equals("application/pdf");		
	}

	public static boolean isText(String mimeType) {
		return mimeType.startsWith("text/")
			|| mimeType.equals("application/xml")
			|| mimeType.equals("text/plain")
			|| mimeType.equals("text/html")
			|| mimeType.equals("application/cpp")
			|| mimeType.equals("application/python")
			|| mimeType.equals("application/perl")
			|| mimeType.equals("application/x-shell")
			|| mimeType.equals("application/x-matlab-source")
			|| mimeType.equals("application/json");		
	}

	public static boolean isTimeseries(String mimeType) {
		return mimeType.equals("application/mef")
			|| mimeType.equals("application/x-blackfynn-events")
			|| mimeType.equals("application/edf")
			|| mimeType.equals("application/bni")
			|| mimeType.equals("application/persyst")
			|| mimeType.equals("application/neuropace")
			|| mimeType.equals("application/kahana")
			;		
	}
	
	public static boolean isGeneData(String mimeType, String fileName) {
		return (mimeType.equals("text/plain") && fileName.contains("_chr_")) ||
				mimeType.equals("application/x-ncbi-gene");
	}

	/**
	 * Friendly English name for the MIME type
	 * 
	 * @param mimeType
	 * @return
	 */
	public static String getNameforMimeType(String mimeType) {
		if (mimeType.equals("image/png"))
			return "PNG image";
		else if (mimeType.equals("image/jpg"))
			return "JPEG image";
		else if (mimeType.equals("application/excel"))
			return "Excel worksheet";
		else if (mimeType.equals("application/msword"))
			return "Word document";
		else if (mimeType.equals("application/mef"))
			return "Orca (MEF) timeseries";
		else if (mimeType.equals("application/mat"))
			return "Matlab data";
		else if (mimeType.equals("application/kahana"))
			return "Penn data";
		else if (mimeType.equals("application/pdf"))
			return "PDF document";
		else if (mimeType.equals("application/edf"))
			return "EDF timeseries";
		else if (mimeType.equals("application/bni"))
			return "BNI timeseries";
		else if (mimeType.equals("application/medtronic"))
			return "Medtronic timeseries";
		else if (mimeType.equals("application/edf"))
			return "EDF timeseries";
		else if (mimeType.equals("application/mdt-csv"))
			return "Medtronic raw timeseries";
		else if (mimeType.equals("application/neuropace"))
			return "Neuropace timeseries";
		else if (mimeType.equals("application/persyst"))
			return "Persyst timeseries";
		else if (mimeType.equals("application/dicom"))
			return "DICOM image";
		else if (mimeType.equals("application/x-nifti"))
			return "NIFTI image";
		else if (mimeType.equals("application/x-nifti-gz"))
			return "NIFTI archive";
		else if (mimeType.equals("application/x-mgh-nmr-gz"))
			return "MGH image archive";
		else if (mimeType.equals("application/x-mgh-nmr"))
			return "MGH image";
		else if (mimeType.equals("application/xml"))
			return "XML data";
		else if (mimeType.equals("text/plain"))
			return "Text data";
		else if (mimeType.equals("text/html"))
			return "HTML document";
		else if (mimeType.equals("application/json"))
			return "JSON data";
		else if (mimeType.equals("application/control"))
			return "Orca import control data";
		else if (mimeType.equals("application/zip"))
			return "ZIP archive";
		else if (mimeType.equals("application/x-gzip"))
			return "GZIP archive";
		else if (mimeType.equals("application/x-minc"))
			return "MINC image";
		else if (mimeType.equals("application/x-siemens-ima"))
			return "Siemens image";
		else if (mimeType.equals("application/x-plexon"))
			return "Plexon timeseries";
		else if (mimeType.equals("application/x-blackfynn-events"))
			return "Orca event data";
		else if (mimeType.equals("application/directory"))
			return "Directory";
		else if (mimeType.equals("application/cpp"))
			return "C/C++ source";
		else if (mimeType.equals("application/java"))
			return "Java source";
		else if (mimeType.equals("application/python"))
			return "Python source";
		else if (mimeType.equals("application/perl"))
			return "Perlsource";
		else if (mimeType.equals("application/x-shell"))
			return "Shell script";
		else if (mimeType.equals("application/x-matlab-source"))
			return "Matlab source";
		else if (mimeType.equals("application/x-ieeg-import-config"))
			return "IEEG directory import configuration";
		else if (mimeType.equals("application/x-natus-annotations"))
			return "Natus annotation file";
		else if (mimeType.equals(GRASS_TWIN_ANNOTATION_MIME_TYPE_NAME))
			return "Grass TWin annotation file";
		else if (mimeType.equals(IEEG_JSON_ANNOTATION_MIME_TYPE_NAME))
			return "IEEG JSON annotation file";
		else if (mimeType.equals(IEEG_JSON_MONTAGE_MIME_TYPE_NAME))
			return "IEEG JSON montage file";
		
		return "application/octet-stream";
	}
	
	private static boolean isDirectory(String path) {
		return path != null 
				&& (path.endsWith("/") 
						|| path.endsWith("/.") 
						|| path.endsWith("/*"));
	}

	/**
	 * This type supplies JSON metadata
	 * 
	 * @param mediaType
	 * @return
	 */
	public static boolean isJsonSupplier(String mimeType) {
		return mimeType.equals("application/mat")
				|| mimeType.equals("application/json")
//				|| mimeType.equals("application/xml")
				;		
	}
}
