/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Multiset;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Message broker with a fixed number of workers, which are used to execute the interceptors
 * corresponding to each message receipt.
 * 
 * @author zives
 *
 * @param <K>
 * @param <V>
 */
public class ThreadPoolBroker<K extends IMessage<?>,V> extends BasicBroker<K, V> {
	ExecutorService pool;
	
	/**
	 * A task for the work queue
	 * 
	 * @author zives
	 *
	 * @param <K>
	 * @param <V>
	 */
	public static class ThreadPoolTask<K,V> implements Callable<V> {
		K key;
		V value;
		JsonTyped error;
		
		IConsumer<K,V> interceptor;
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}

		public JsonTyped getError() {
			return error;
		}
		public void setError(JsonTyped value) {
			this.error = value;
		}
		public IConsumer<K, V> getInterceptor() {
			return interceptor;
		}
		public void setInterceptor(IConsumer<K, V> interceptor) {
			this.interceptor = interceptor;
		}
		
		
		
		public ThreadPoolTask(
				K key,
				V value,
				JsonTyped error,
				edu.upenn.cis.db.habitat.daemons.IMessageBroker.IConsumer<K, V> interceptor) {
			super();
			this.key = key;
			this.interceptor = interceptor;
			this.error = error;
		}
		
		@Override
		public V call() throws Exception {

			if (error == null)
				interceptor.handle(key, value);
			else
				interceptor.handleError(key, error);
			
			return null;
		}
	}
	
	public static class ThreadPoolFutureTask<K,V> implements Callable<V> {
		K key;
		V value;
		
		IFutureResult<K,V> interceptor;
		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}

		public IFutureResult<K, V> getFutureResult() {
			return interceptor;
		}
		public void setFutureResult(IFutureResult<K, V> interceptor) {
			this.interceptor = interceptor;
		}
		
		
		
		public ThreadPoolFutureTask(
				K key,
				V value,
				IFutureResult<K, V> interceptor) {
			super();
			this.key = key;
			this.interceptor = interceptor;
		}
		
		@Override
		public V call() throws Exception {
			interceptor.setView(key);
			interceptor.setResult(value);

			return interceptor.call();
		}
	}

	public ThreadPoolBroker(int poolSize) {
		pool = Executors.newFixedThreadPool(poolSize);
	}

	@Override
	public void send(String channel, K key, V value) {

		Multiset<IFutureResult<K, V>> callbacks = null;
		synchronized (futures) {
			callbacks = futures.get(channel).getSuccessCallbacks(channel, key, value);
		}
		
		for (IFutureResult<K, V> cb: callbacks) {
			pool.submit(new ThreadPoolFutureTask<K, V>(key, value, cb));
		}
	}

	@Override
	public void sendError(String channel, K key, JsonTyped value) {
		Multiset<IFutureResult<K, String>> callbacks = null;
		String valueString = "";
		try {
			valueString = JsonKeyValueMapper.getMapper().createValue(value);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		synchronized (futures) {
			callbacks = futures.get(channel).getFailureCallbacks(channel, key, valueString);
		}
		
		for (IFutureResult<K, String> cb: callbacks) {
			pool.submit(new ThreadPoolFutureTask<K, String>(key, valueString, cb));
		}
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

}
