/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.util.concurrent.Semaphore;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.util.concurrent.Striped;

import edu.upenn.cis.braintrust.IvProps;

public class TransferSemaphoresFactory {

	private static class TransferSemaphoresAllUsersSupplier
			implements Supplier<Semaphore> {
		@Override
		public Semaphore get() {
			int maxTransfersAllUsers = IvProps.getMaxTransfersAllUsers();
			return new Semaphore(maxTransfersAllUsers);
		}
	}

	private static class TransferSemaphoresPerUserSupplier
			implements Supplier<Striped<Semaphore>> {
		@Override
		public Striped<Semaphore> get() {
			int maxTransfersPerUser = IvProps.getMaxTransfersPerUser();
			return Striped.semaphore(1000, maxTransfersPerUser);
		}
	}

	private static Supplier<Semaphore> semaphoreAllUsersSupplier =
			Suppliers.memoize(new TransferSemaphoresAllUsersSupplier());

	private static Supplier<Striped<Semaphore>> semaphoresPerUserSupplier =
			Suppliers.memoize(new TransferSemaphoresPerUserSupplier());

	public static Semaphore getTransferSemaphoreAllUsers() {
		return semaphoreAllUsersSupplier.get();
	}

	public static Striped<Semaphore> getTransferSemaphoresPerUser() {
		return semaphoresPerUserSupplier.get();
	}

	private TransferSemaphoresFactory() {}
}
