/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.util;


/**
 * Methods and constants for dealing with the tool upload authc cookie.
 * 
 * @author John Frommeyer
 * 
 */
public final class ToolUploadCookies {
	private ToolUploadCookies() {
		throw new AssertionError("This class is not instantiable");
	}

	// Is there a better way to get this?
	public final static String SERVLET_PATH = "/servlet.gupld";
	public final static String COOKIE_NAME = "ieeg-sid";
}
