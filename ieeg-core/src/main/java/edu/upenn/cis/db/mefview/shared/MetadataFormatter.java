/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.resources.client.ImageResource;

/**
 * A metadata formatter converts the metadata item into displayable columns,
 * an icon image, a selectable star, and some amount of (optional) pagination
 * for collection-valued items
 * 
 * @author zives
 *
 */
public interface MetadataFormatter {
	public int getNumColumns();
	
	public ImageResource getImage(PresentableMetadata meta);
	
	public String getColumnContent(PresentableMetadata meta, int index);
	
	public String getColumnContent(PresentableMetadata meta, String attrib);
	
	public boolean isStarrable();
	
	public boolean isStarred(PresentableMetadata meta, MetadataPresenter presenter);
	
	public boolean showPrev(PresentableMetadata meta, MetadataPresenter presenter);
	
	public boolean showNext(PresentableMetadata meta, MetadataPresenter presenter);
	
	public void toggleStar(PresentableMetadata meta, MetadataPresenter presenter);
	
	public void prevPage(PresentableMetadata meta, MetadataPresenter presenter);
	
	public void nextPage(PresentableMetadata meta, MetadataPresenter presenter);

	public boolean isRatable();

	public Boolean isRated(PresentableMetadata object, MetadataPresenter presenter);

	void toggleRating(PresentableMetadata meta, MetadataPresenter presenter);
}

