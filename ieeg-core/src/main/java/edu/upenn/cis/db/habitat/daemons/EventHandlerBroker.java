/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Event handler broker uses the existing (sender) thread to execute each callback
 * handler.
 * 
 * @author zives
 *
 * @param <K>
 * @param <V>
 */
public class EventHandlerBroker<K extends IMessage<?>,V> extends BasicBroker<K, V> {

	public EventHandlerBroker() {
		super();
	}

	@Override
	public void send(String channel, K key, V value) {

		FutureRegistry<K,V> callbacks = null;
		synchronized (futures) {
			if (futures.containsKey(channel))
				callbacks = futures.get(channel);//.addAll(interceptors.get(channel));
		}

		if (callbacks != null)
			callbacks.notifySuccess(channel, key, value);
	}

	@Override
	public void sendError(String channel, K key, JsonTyped value) {
		synchronized (futures) {
			if (futures.containsKey(channel)) {
				try {
					futures.get(channel).notifyError(channel, key, JsonKeyValueMapper.getMapper().createValue(value));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

}
