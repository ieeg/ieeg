/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Deprecated
public class Datablock {
	public int numPoints;
	public double minXValue;
	public double maxXValue;
	public ArrayList<Sample> values;
	public Datablock next;
	public ArrayList<Datablock> children;
	public Datablock parent;
	
	public Datablock(ArrayList<Sample> value) {
		minXValue = value.get(0).time;
		maxXValue = value.get(value.size() - 1).time;
		numPoints = value.size();
		values = value;
	}

	/**
	 * Return the "closest" block to the desired value -- null if the value belongs at the head
	 * of the list
	 * 
	 * @param minXValue
	 * @param scaleFactor
	 * @return
	 */
	public Datablock findFirst(double minXValue, double scaleFactor) {
		// First block
		if (minXValue < this.minXValue)
			return null;
		
		Datablock n = this;
		while (n.next != null && n.maxXValue < minXValue)
			n = n.next;
		
		return n;
	}
	
	public List<Datablock> findAll(double minXValue, double maxValue, double scaleFactor) {
		List<Datablock> ret = new ArrayList<Datablock>();
		Datablock n = findFirst(minXValue, scaleFactor);
		
		if (n != null && n.maxXValue >= minXValue) {
			while (n != null && n.minXValue <= maxValue) {
				ret.add(n);
				n = n.next;
			}
		}
		
		return ret;
	}
	
	public Datablock append(Datablock d) {
		Datablock pred = findFirst(d.minXValue, (d.maxXValue - d.minXValue) / d.numPoints);
		
		if (pred == null) {
			d.next = this;
			return d;
		} else {
			if (pred.maxXValue > d.minXValue)
				Datablock.merge(pred, d);
			if (!d.values.isEmpty()) {
				d.next = pred.next;
				pred.next = d;
			}
			return this;
		}
	}
	
//	public static int binSearch(ArrayList<Sample> s, double time, int min, int max) {
//		if (max < min)
//			return -1;
//		
//		int mid = min + ((min + max) / 2);
//		
//		if (s.get(mid).time > time) {
//			return binSearch(s, time, min, mid - 1);
//		} else if (s.get(mid).time < time) {
//			return binSearch(s, time, mid + 1, max);
//		} else {
//			return mid;
//		}
//	}
	
	public static void merge(Datablock one, Datablock two) {
		// One is to the left or equal to two
		if (one.minXValue > two.minXValue) {
			Datablock temp = one;
			one = two;
			two = temp;
		}

		boolean doSort = false;
		int inx;
		for (inx = 0; inx < two.values.size(); inx++) {
			Sample v = two.values.get(inx);
			
			if (v.getTime() <= one.maxXValue) {
				int match = Collections.binarySearch(one.values, v);
				
				if (match >= 0) {
					for (String k: v.values.keySet())
						one.values.get(match).values.put(k, v.values.get(k));
				} else {
					one.values.add(v);
					doSort = true;
				}
			} else
				break;
		}
		// Trim data block two to only consider the remaining items
		if (inx < two.values.size()) {
			// Any remaining nodes
			two.minXValue = two.values.get(inx).time;
			for (int x = inx - 1; x >= 0; x++)
				two.values.remove(x);
		} else
			two.values.clear();
		if (doSort) {
			Collections.sort(one.values);
		}
			
	}
	
}


