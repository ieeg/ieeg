/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.server.mefpageservers3;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Throwables.propagate;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.primitives.Ints;

/**
 * @author Sam Donnelly
 */
@Immutable
public final class MEFIndexSlice implements Serializable {

	private static final long serialVersionUID = 1L;

	private final long[] timeIndex;
	private final long[] offsetIndex;
	private final long firstPageNo;
	private final long endMicros;
	private final long endOffset;

	/**
	 * {@code indexEndMicros != null && indexEndOffset != null} iff this is the
	 * last slice in the index
	 * 
	 * @param is
	 * @param noEntriesOnStream unless it's the last slice of the index, this
	 *            includes one extra entry
	 * @param firstPageNo zero based
	 * @param indexEndMicros
	 */
	public MEFIndexSlice(
			InputStream is,
			int noEntriesOnStream,
			long firstPageNo,
			@Nullable Long indexEndMicros,
			@Nullable Long indexEndOffset) {
		checkArgument(indexEndMicros != null || indexEndOffset == null); // A->B
		checkArgument(indexEndOffset != null || indexEndMicros == null); // B->A
		this.firstPageNo = firstPageNo;
		Boolean isLastEntry = false;
		if (indexEndMicros != null) {
			isLastEntry = true;
		}
		try {
			@SuppressWarnings("resource")
			DataInput di = new LittleEndianDataInputStream(is);

			int myEntriesOnStream = noEntriesOnStream - 1;
			if (isLastEntry) {
				myEntriesOnStream++;
			}

			timeIndex = new long[myEntriesOnStream];
			offsetIndex = new long[myEntriesOnStream];

			for (int i = 0; i < myEntriesOnStream; i++) {
				timeIndex[i] = di.readLong();
				offsetIndex[i] = di.readLong();

				// Need to read past the sample number index entry
				Long sampleNo = di.readLong();

				if (firstPageNo == 0 && i == 0) {
					String errMsg = "";
					if (offsetIndex[0] != 1024) {
						errMsg += "first file offset is "
								+ offsetIndex[0]
								+ " expected 1024 ";
					}
					if (sampleNo != 0) {
						errMsg += "first sample no is "
								+ sampleNo
								+ " expected 0";

					}
					checkArgument(errMsg.isEmpty(), errMsg);
				}
			}

			if (isLastEntry) {
				endMicros = indexEndMicros;
				endOffset = indexEndOffset;
			} else {
				endMicros = di.readLong() - 1;
				endOffset = di.readLong() - 1;

				// Just read off the end so we read the whole thing
				di.readLong();
			}

		} catch (IOException ioe) {
			throw propagate(ioe);
		}
		if (timeIndex.length != offsetIndex.length) {
			throw new AssertionError();
		}
	}

	private long entryNo2PageNo(int entryNo) {
		return entryNo + firstPageNo;
	}

	public long getEndByteOffset(long pageNo) {
		int entryNo = pageNo2IndexEntryNo(pageNo);
		return getEndByteOffsetForEntryNo(entryNo);
	}

	private long getEndByteOffsetForEntryNo(int entryNo) {
		checkArgument(entryNo >= 0);
		checkArgument(entryNo < offsetIndex.length);
		Long pageInfosEndOffset = null;
		if (entryNo == offsetIndex.length - 1) {
			pageInfosEndOffset = endOffset;
		} else {
			pageInfosEndOffset =
					offsetIndex[entryNo + 1] - 1;
		}
		return pageInfosEndOffset;
	}

	public long getEndMicros() {
		return endMicros;
	}

	public long getFirstPageNo() {
		return firstPageNo;
	}

	private long getLastPageNo() {
		return firstPageNo + timeIndex.length - 1;
	}

	public long getPageNo(long timeMicros) {
		int entryNo = Arrays.binarySearch(timeIndex, timeMicros);
		if (entryNo < 0) {
			entryNo = (-entryNo - 1) - 1;
		}
		long pageNo = entryNo2PageNo(entryNo);
		return pageNo;
	}

	public long getStartByteOffset(long pageNo) {
		checkArgument(pageNo >= firstPageNo);
		checkArgument(
				pageNo <= getLastPageNo(),
				"pageNo: %s firstPageNo: %s lastPageNo: %s",
				pageNo,
				firstPageNo,
				getLastPageNo());
		int entryNo = pageNo2IndexEntryNo(pageNo);
		long offset = offsetIndex[entryNo];
		return offset;
	}

	public long getStartMicros() {
		return timeIndex[0];
	}

	public long getStartTimeMicros(long pageNo) {
		int entryNo = pageNo2IndexEntryNo(pageNo);
		long startTime = timeIndex[entryNo];
		return startTime;
	}

	private int pageNo2IndexEntryNo(long pageNo) {
		checkArgument(pageNo >= firstPageNo);
		int entryNo = Ints.checkedCast(pageNo - firstPageNo);
		checkArgument(
				entryNo < timeIndex.length,
				"entryNo: %s sampleTimes2ByteOffsets.size(): %s",
				entryNo, timeIndex.length);
		return entryNo;
	}

	@Override
	public String toString() {
		return "MEFIndexSlice [firstPageNo=" + firstPageNo + ", endMicros="
				+ endMicros + ", endOffset=" + endOffset + "]";
	}
}
