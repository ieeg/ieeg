/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.io;

import java.io.Closeable;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.eeg.ITimeSeriesFileWriter;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.mef.MefHeader2;

/**
 * @author John Frommeyer
 *
 */
public class HabitatMefWriter implements Closeable {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private boolean closed = false;
	private final edu.upenn.cis.eeg.mef.MefWriter mefWriter;
	private final IOutputStream mefOutputStream;

	public HabitatMefWriter(
			IObjectServer serv,
			FileReference handle,
			SimpleChannel chInfo) throws IOException {
		final String m = "MefWriter(...)";

		logger.debug("{}: Trying to create channel {}", m, handle);

		IOutputStream output = serv.openForOutput(handle);

		FileOutputStream tempStream = (FileOutputStream) output
				.getOutputStream();

		MefHeader2 meh = new MefHeader2();

		meh.setChannelName(chInfo.getChannelName());
		meh.setSamplingFrequency(chInfo.getSamplingFrequency());
		meh.setHeaderLength((short) 1024);

		// Set voltage conversion factor
		meh.setVoltageConversionFactor(chInfo.getVoltageConversionFactor());

		double secPerMEFBlock = Math
				.floor(6000 / chInfo.getSamplingFrequency());
		long discontinuity_threshold = (long) Math.ceil(1.5 * (1e6/chInfo.getSamplingFrequency()));

		mefWriter = new edu.upenn.cis.eeg.mef.MefWriter(
				tempStream,
				chInfo.getChannelName(),
				meh,
				secPerMEFBlock,
				chInfo.getSamplingFrequency(),
				discontinuity_threshold);
		mefOutputStream = output;
	}

	public void writeData(int[] samps, long[] timestamps) {
		mefWriter.writeData(samps, timestamps);
	}

	@Override
	public void close() throws IOException {
		if (!closed) {
			mefWriter.close();
			mefOutputStream.close();
			closed = true;
		}
	}
}
