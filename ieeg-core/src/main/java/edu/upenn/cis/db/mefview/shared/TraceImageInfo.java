/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible
public class TraceImageInfo implements Serializable, Comparable<TraceImageInfo> {

	private static final long serialVersionUID = 1L;

	private String traceName;
	private int posX;
	private int posY;

	@SuppressWarnings("unused")
	// For GWT
	private TraceImageInfo() {}

	public TraceImageInfo(
			String traceName,
			int posX,
			int posY) {
		this.traceName = checkNotNull(traceName);
		this.posX = posX;
		this.posY = posY;
	}

	public String getTraceName() {
		return traceName;
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public int compareTo(TraceImageInfo o) {
		if (traceName == null && o.traceName != null)
			return -1;
		else if (traceName == null && o.traceName == null)
			return 0;
		else if (o.traceName == null)
			return 1;
		else
			return traceName.compareTo(o.traceName);
	}

}
