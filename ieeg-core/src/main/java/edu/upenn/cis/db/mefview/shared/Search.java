/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Search implements IsSerializable {
	
	private Boolean excludeStudies;

	
	private Boolean excludeDatasets = true;

	private Set<String> imageTypes = new HashSet<String>();
	
	private Integer minRecordedSzCnt;
	
	private Integer maxRecordedSzCnt;

	public Integer getMinRecordedSzCnt() {
		return minRecordedSzCnt;
	}

	public void setMinRecordedSzCnt(Integer minRecordedSzCnt) {
		this.minRecordedSzCnt = minRecordedSzCnt;
	}

	public Integer getMaxRecordedSzCnt() {
		return maxRecordedSzCnt;
	}

	public void setMaxRecordedSzCnt(Integer maxRecordedSzCnt) {
		this.maxRecordedSzCnt = maxRecordedSzCnt;
	}


	/// Time Series Search
	
	private Integer minInterannMicrosecs;

	
	private Integer maxInterannMicrosecs;

	public Boolean getExcludeStudies() {
		return excludeStudies;
	}

	public void setExcludeStudies(@Nullable Boolean excludeStudies) {
		this.excludeStudies = excludeStudies;
	}

	public Boolean getExcludeDatasets() {
		return excludeDatasets;
	}

	public void setExcludeDatasets(@Nullable Boolean excludeDatasets) {
		this.excludeDatasets = excludeDatasets;
	}

	public Set<String> getImageTypes() {
		return imageTypes;
	}

	public void setImageTypes(Set<String> imageTypes) {
		this.imageTypes = imageTypes;
	}

	public Integer getMinInterannMicrosecs() {
		return minInterannMicrosecs;
	}

	public void setMinInterannMicrosecs(@Nullable Integer minInterannMicrosecs) {
		this.minInterannMicrosecs = minInterannMicrosecs;
	}

	public Integer getMaxInterannMicrosecs() {
		return maxInterannMicrosecs;
	}

	public void setMaxInterannMicrosecs(@Nullable Integer maxInterannMicrosecs) {
		this.maxInterannMicrosecs = maxInterannMicrosecs;
	}

	public String getAnnotationType() {
		return annotationType;
	}

	public void setAnnotationType(@Nullable String annotationType) {
		this.annotationType = annotationType;
	}

	public Integer getMinAnnotationCount() {
		return minAnnotationCount;
	}

	public void setMinAnnotationCount(@Nullable Integer minAnnotationCount) {
		this.minAnnotationCount = minAnnotationCount;
	}

	public Integer getMaxAnnotationCount() {
		return maxAnnotationCount;
	}

	public void setMaxAnnotationCount(@Nullable Integer maxAnnotationCount) {
		this.maxAnnotationCount = maxAnnotationCount;
	}

	public Set<String> getSzTypes() {
		return szTypes;
	}

	public void setSzTypes(Set<String> szTypes) {
		this.szTypes = szTypes;
	}

	public Set<String> getGenders() {
		return genders;
	}

	public void setGenders(Set<String> genders) {
		this.genders = genders;
	}

	public Integer getAgeOfOnsetMin() {
		return ageOfOnsetMin;
	}

	public void setAgeOfOnsetMin(@Nullable Integer ageOfOnsetMin) {
		this.ageOfOnsetMin = ageOfOnsetMin;
	}

	public Integer getAgeOfOnsetMax() {
		return ageOfOnsetMax;
	}

	public void setAgeOfOnsetMax(@Nullable Integer ageOfOnsetMax) {
		this.ageOfOnsetMax = ageOfOnsetMax;
	}

	public Double getSamplingRateMin() {
		return samplingRateMin;
	}

	public void setSamplingRateMin(Double samplingRateMin) {
		this.samplingRateMin = samplingRateMin;
	}

	public Double getSamplingRateMax() {
		return samplingRateMax;
	}

	public void setSamplingRateMax(Double samplingRateMax) {
		this.samplingRateMax = samplingRateMax;
	}

	public Set<String> getSides() {
		return sides;
	}

	public void setSides(Set<String> sides) {
		this.sides = sides;
	}

	public Set<String> getLocations() {
		return locations;
	}

	public void setLocations(Set<String> locations) {
		this.locations = locations;
	}

	public String getElectrodeType() {
		return electrodeType;
	}

	public void setElectrodeType(String electrodeType) {
		this.electrodeType = electrodeType;
	}

	
	private String annotationType;

	
	private Integer minAnnotationCount;


	private Integer maxAnnotationCount;
	
	public Boolean getGenSzTypes() {
		return genSzTypes;
	}

	public void setGenSzTypes(Boolean genSzTypes) {
		this.genSzTypes = genSzTypes;
	}

	public Boolean getPartialSzTypes() {
		return partialSzTypes;
	}

	public void setPartialSzTypes(Boolean partialSzTypes) {
		this.partialSzTypes = partialSzTypes;
	}

	private Boolean genSzTypes = false;
	private Boolean partialSzTypes = false;

	/// Patient Search
	private Set<String> szTypes = new HashSet<String>();

	private Set<String> genders = new HashSet<String>();

	
	private Integer ageOfOnsetMin;

	private Integer ageOfOnsetMax;
	
	// Electrode info

	private Double samplingRateMin;
	private Double samplingRateMax;
	private Set<String> sides = new HashSet<String>();
	private Set<String> locations = new HashSet<String>();
	private String electrodeType;
	private Set<String> surgeries = new HashSet<String>();
	private Set<String> contactTypes = new HashSet<String>();
	
	public Set<String> getSurgeryTypes() {
		return surgeries;
	}
	
	public void setSurgeryTypes(Set<String> typ) {
		surgeries = typ;
	}
	
	private String postop;
	
	public void setPostopRating(String rating) {
		postop = rating;
	}
	
	public String getPostopRating() {
		return postop;
	}
	
	public Set<String> getContactTypes() { 
		return contactTypes;
	}
	
	public void setContactTypes(Set<String> contactTypes) { 
		this.contactTypes = contactTypes;
	}
	
}
