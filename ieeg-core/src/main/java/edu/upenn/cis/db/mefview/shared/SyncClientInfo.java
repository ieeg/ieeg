package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable=true)
public class SyncClientInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String url;
	String operatingSystem;
	String descriptor;
	
	public SyncClientInfo() {}
	
	
	
	public SyncClientInfo(String url, String operatingSystem, String descriptor) {
		super();
		this.url = url;
		this.operatingSystem = operatingSystem;
		this.descriptor = descriptor;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
