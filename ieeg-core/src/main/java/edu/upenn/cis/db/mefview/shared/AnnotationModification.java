package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.braintrust.BtUtil;

@GwtCompatible(serializable=true)
public class AnnotationModification implements Serializable {
	public static final int DELETED = 0;
	public static final int INSERTED = 1;
	public static final int CHANGED = 2;
	public static final int THREADED = 3;
	
	String id;
	String uid;
	long when;
	int op;
	Annotation prior;
	Annotation next;
	
	public AnnotationModification() {}
	
	public AnnotationModification(String id, int op, Annotation prior, Annotation next, String user) {
		super();
		this.id = id;
		this.op = op;
		this.uid = user;
		this.prior = prior;
		this.next = next;
		this.when = new Date().getTime();
	}

	public int getOp() {
		return op;
	}

	public void setOp(int op) {
		this.op = op;
	}

	public Annotation getPrior() {
		return prior;
	}

	public void setPrior(Annotation prior) {
		this.prior = prior;
	}

	public Annotation getNext() {
		return next;
	}

	public void setNext(Annotation next) {
		this.next = next;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser() {
		return uid;
	}

	public void setUser(String uid) {
		this.uid = uid;
	}

	public long getWhen() {
		return when;
	}

	public void setWhen(long when) {
		this.when = when;
	}

	@JsonIgnore
	public String getLayer() {
		if (prior != null && prior.getLayer() != null)
			return prior.getLayer();
		else if (next != null && next.getLayer() != null)
			return next.getLayer();
		else
			return "";
	}

	public static AnnotationModification getDelete(String uuid, String user, Annotation deleted) {
		return new AnnotationModification(uuid, DELETED, deleted, null, user);
	}

	public static AnnotationModification getInsert(String uuid, String user, Annotation inserted) {
		return new AnnotationModification(uuid, INSERTED, null, inserted, user);
	}

	public static AnnotationModification getUpdate(String uuid, String user, Annotation oldVersion, Annotation newVersion) {
		return new AnnotationModification(uuid, CHANGED, oldVersion, newVersion, user);
	}

	public static AnnotationModification getReference(String uuid, String user, Annotation cause, Annotation reaction) {
		return new AnnotationModification(uuid, THREADED, cause, reaction, user);
	}

	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("[AnnMod " + getId() + ":");
		
		if (getPrior() != null)
			ret.append(" From (" + getPrior().toString() + ")");
		
		if (getNext() != null)
			ret.append(" To (" + getNext().toString() + ")");

		ret.append("]");
		return ret.toString();
	}
}
