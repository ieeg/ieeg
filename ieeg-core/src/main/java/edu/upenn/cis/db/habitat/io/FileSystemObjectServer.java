/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.io;

//import static org.junit.Assert.assertNotNull;

import static com.google.common.base.Strings.nullToEmpty;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.thirdparty.ByteBufferBackedInputStream;

/**
 * A fileserver using the local filesystem
 * 
 * @author zives
 *
 */
public class FileSystemObjectServer extends BaseObjectServer {
	
	public static class InputFileStream implements IInputStream {
		String fileName;
		RandomAccessFile file;
		FileSystemObjectServer server;
		IStoredObjectReference handle;
		
		long prefetchPosition = 0;
		ByteBuffer prefetchBuffer = null;
		long prefetchLimit = 0;
		
		public InputFileStream(FileSystemObjectServer server, 
				String f, 
				IStoredObjectReference handle) throws FileNotFoundException {
			this(server, f, handle, 65536);
		}
		
		public InputFileStream(FileSystemObjectServer server, 
				String f, 
				IStoredObjectReference handle, int prefetchSize) throws FileNotFoundException {
			fileName = f;
			file = new RandomAccessFile(f, "r");
			this.server = server;
			this.handle = handle;
			prefetchBuffer = ByteBuffer.allocate(prefetchSize);
		}
		
		public RandomAccessFile getFile() {
			return file;
		}
		
		public void setFile(RandomAccessFile f2) {
			file = f2;
		}

		@Override
		public void close() throws IOException {
			file.close();
		}
		
		public FileCursor getStart() {
			return new FileCursor(0);
		}

		@Override
		public FileCursor getOffset(long pos) {
			return new FileCursor(pos);
		}

		@Override
		public boolean isAtEof(ICursor cursor) throws IOException {
			return ((FileCursor)cursor).getPosition() == file.getChannel().size();
		}

		@Override
		public long getLength() throws IOException {
			return file.length();
		}

		@Override
		public IStoredObjectReference getHandle() {
			return handle;
		}
		
		@Override
		public FileCursor getBytes(WritableByteChannel target, long bytes) throws IOException {
			return getBytes(getStart(), target, bytes);
		}

		@Override
		public FileCursor getBytes(ByteBuffer target, long bytes) throws IOException {
			return getBytes(getStart(), target, bytes);
		}

		public void flush() {
			prefetchBuffer.clear();
			prefetchLimit = 0;
		}
		
		boolean isInCache(long position, long bytes) {
			return (position >= prefetchPosition && position + 
					bytes <= prefetchBuffer.limit() - prefetchPosition);
		}

		@Override
		public FileCursor getBytes(ICursor position, WritableByteChannel target,
				long bytes) throws IOException {
//			if (bytes > Integer.MAX_VALUE)
//				throw new RuntimeException("Can only do MAX_INT reads at a time");
//			
//			if (!isInCache(position.getPosition(), bytes) && bytes <= prefetchLimit) {
//				prefetchPosition = position.getPosition();
//				prefetchBuffer.rewind();
//				ICursor cursor = server.getBytes(this, position, prefetchBuffer, bytes);
//				prefetchLimit = cursor.getPosition() - position.getPosition();
//				prefetchBuffer.flip();
//			}
//			if (isInCache(position.getPosition(), bytes)) {
//				prefetchBuffer.position((int)(position.getPosition() - prefetchPosition));
//				prefetchBuffer.limit((int)bytes);
//				int actual = target.write(prefetchBuffer);
//				return new FileCursor(position.getPosition() + actual);
//			} else
				return server.getBytes(this, position, target, bytes);
		}

		@Override
		public FileCursor getBytes(ICursor position, ByteBuffer target, long bytes) throws IOException {
//			if (bytes > Integer.MAX_VALUE)
//				throw new RuntimeException("Can only do MAX_INT reads at a time");
//			
//			if (!isInCache(position.getPosition(), bytes) && bytes <= prefetchLimit) {
//				prefetchPosition = position.getPosition();
//				prefetchBuffer.rewind();
//				ICursor cursor = server.getBytes(this, position, prefetchBuffer, bytes);
//				prefetchLimit = cursor.getPosition() - position.getPosition();
//				prefetchBuffer.flip();
//			}
//			if (isInCache(position.getPosition(), bytes)) {
//				prefetchBuffer.position((int)(position.getPosition() - prefetchPosition));
//				prefetchBuffer.limit((int)bytes);
//
//				int i;
//				for (i = 0; target.hasRemaining() && prefetchBuffer.hasRemaining(); i++)
//					target.put(prefetchBuffer.get());
//				
//				return new FileCursor(position.getPosition() + i);
//			} else
				return server.getBytes(this, position, target, bytes);
		}

		@Override
		public void copyTo(OutputStream stream) throws IOException {
			FileInputStream is = new FileInputStream(fileName);
			
			org.apache.commons.io.IOUtils.copy(is, stream);
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return new FileInputStream(fileName);
		}
		
		public Path getFilePath() {
			return Paths.get(fileName);
		}
		
		public String getFileName() {
			return fileName;
		}
	}
	
	public static class OutputFileStream implements IOutputStream {
		RandomAccessFile file;
		FileSystemObjectServer server;
		final Logger logger = LoggerFactory.getLogger(getClass());
		IStoredObjectReference handle;

		public OutputFileStream(FileSystemObjectServer server, String f, IStoredObjectReference handle) throws FileNotFoundException {
			this.server = server;
			this.handle = handle;
			final File path = new File(f);
			try {
				path.getParentFile().mkdirs();
				file = new RandomAccessFile(path, "rw");
			} catch (FileNotFoundException fnf) {
				throw new IllegalArgumentException(": Cannot open file " 
						+ path.getAbsoluteFile() 
						+ " 'rw'"
						, fnf);
			}		
		}
		
		public RandomAccessFile getFile() {
			return file;
		}
		
		public void setFile(RandomAccessFile f2) {
			file = f2;
		}

		@Override
		public void close() throws IOException {
			file.close();
		}

		@Override
		public FileCursor getStart() {
			return new FileCursor(0);
		}

		@Override
		public FileCursor getOffset(long pos) {
			return new FileCursor(pos);
		}

		@Override
		public IStoredObjectReference getHandle() {
			return handle;
		}

		@Override
		public FileCursor putBytes(ReadableByteChannel source, long numBytes) throws IOException {
			return server.putBytes(this, source, numBytes);
		}

		@Override
		public FileCursor putBytes(ICursor position, ReadableByteChannel source, long numBytes) throws IOException {
			return server.putBytes(this, position, source, numBytes);
		}

		@Override
		public FileCursor putBytes(ByteBuffer source) throws IOException {
			return server.putBytes(this, source);
		}

		@Override
		public FileCursor putBytes(ICursor position, ByteBuffer source) throws IOException {
			return server.putBytes(this, position, source);
		}

		@Override
		public void copyFrom(IInputStream stream) throws IOException {
			long length = stream.getLength();
			
			ByteBuffer buf = ByteBuffer.allocate((int)length);
			stream.getBytes(buf, length);
			buf.rewind();
			ByteBufferBackedInputStream inStream = new ByteBufferBackedInputStream(buf);
			
			copyFrom(inStream, length);
		}

		@Override
		public void copyFrom(InputStream stream, long length)
				throws IOException {
			ReadableByteChannel inChannel = Channels.newChannel(stream);
			   			
			long offset = 0;
			long quantum = 1 << 20; // Unit of transfer
			long count;
			while ((count = file.getChannel().transferFrom(inChannel, offset, quantum)) > 0)
			    offset += count;
		}

		@Override
		public OutputStream getOutputStream() throws IOException {
			return new FileOutputStream(getFile().getFD());
		}
	}

	public static class FileCursor implements ICursor {
		long position = 0;
		
		public FileCursor() {
			
		}
		
		public FileCursor(long pos) {
			position = pos;
		}
		
		@Override
		public long getPosition() {
			return position;
		}
		
		@Override
		public void setPosition(long pos) {
			position = pos;
		}
		
		@Override
		public String toString() {
			return "Pos(" + position + ")";
		}
	}
	
	String basePath;
	boolean lock = false;
	
	public FileSystemObjectServer(String basePath) {
		this.basePath = basePath;
	}

	public FileSystemObjectServer(String basePath, boolean lock) {
		this.basePath = basePath;
		this.lock = lock;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public InputFileStream openForInput(IStoredObjectReference handle) throws IOException {
		final String absolutePath = getAbsolutePath(handle);
		return new InputFileStream(this, absolutePath, handle);
	}
	
	public String getBasePath() {
		String extra = "";
		
		if (basePath.endsWith("/"))
			extra = "";
		else
			extra = "/";
		return basePath + extra;
	}
	
	private String getAbsolutePath(IStoredObjectReference handle) {
		String containerFileDirectory = (handle.getDirBucketContainer() != null && handle.getDirBucketContainer().getFileDirectory() != null) ?
				handle.getDirBucketContainer().getFileDirectory()
				: "";
		if (!containerFileDirectory.isEmpty() && !containerFileDirectory.endsWith("/")) {
			containerFileDirectory += "/";
		}

		// container is an absolute path
		if (!containerFileDirectory.isEmpty() && (containerFileDirectory.startsWith("/") || containerFileDirectory.charAt(1) == ':')) {
			return containerFileDirectory + handle.getFilePath();
		} else {
			final String basePathWithSlash = getBasePath();
			return basePathWithSlash + containerFileDirectory + handle.getFilePath();
		}
	}
	
	@Override
	public IOutputStream openForOutput(IStoredObjectReference handle) throws IOException {
		final String absolutePath = getAbsolutePath(handle);
		return new OutputFileStream(this, absolutePath, handle);
	}


	@Override
	public FileCursor getBytes(IInputStream stream, WritableByteChannel buf, long numBytesToFetch) throws IOException {
		return getBytes(stream, stream.getStart(), buf, numBytesToFetch);
	}

	@Override
	public FileCursor getBytes(
			IInputStream stream, 
			ICursor currentPosition, 
			WritableByteChannel buf, 
			long numBytesToFetch) throws IOException {
		InputFileStream fil = (InputFileStream)stream;
		FileCursor curs = (FileCursor)currentPosition;
		
		FileChannel fc = fil.getFile().getChannel();
		
		if (numBytesToFetch == -1)
			numBytesToFetch = fc.size();
		
		FileLock fLock = (lock) ? null : fc.lock(curs.getPosition(), numBytesToFetch, true);
		
		fc.transferTo(curs.getPosition(), numBytesToFetch, buf);
		
		if (fLock != null) {
			fLock.close();
		}
		
		return new FileCursor(fc.position());
	}

	@Override
	public FileCursor getBytes(IInputStream stream, ByteBuffer buf, long numBytesToFetch) throws IOException {
		return getBytes(stream, stream.getStart(), buf, numBytesToFetch);
	}

	@Override
	public FileCursor getBytes(
			IInputStream stream, 
			ICursor currentPosition, 
			ByteBuffer buf, 
			long numBytesToFetch) throws IOException {
		InputFileStream fil = (InputFileStream)stream;
		FileCursor curs = (FileCursor)currentPosition;
		
		FileChannel fc = fil.getFile().getChannel();
		
		if (numBytesToFetch == -1)
			numBytesToFetch = fc.size();

		FileLock fLock = (lock) ? null : fc.lock(curs.getPosition(), numBytesToFetch, true);

		long rd = fc.read(buf, curs.getPosition());
	
		if (fLock != null) {
			fLock.close();
		}
		
//		System.out.println("Read " + rd + " bytes from " + curs.getPosition());
		
		return new FileCursor(curs.getPosition() + rd);//fc.position());
	}

	@Override
	public void close(IStream stream) throws IOException {
//		assertNotNull(stream);
		stream.close();
	}

	@Override
	public void shutdown() {
	}

	@Override
	public void deleteItem(IStoredObjectReference handle) throws IOException {
		final Path path = FileSystems.getDefault().getPath(getAbsolutePath(handle));
		Files.deleteIfExists(path);
	}

	@Override
	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource) throws IOException {
		Set<IStoredObjectReference> ret = new HashSet<IStoredObjectReference>();
		
		Path dir  = Paths.get(resource.getFileDirectory());
		
	    DirectoryStream<Path> directoryStream =  
	    		Files.newDirectoryStream(dir);
	    
	    
        for (Path path : directoryStream) {
        	Path relPath = dir.relativize(path);
        	
        	if (path.toFile().isDirectory()) {
        		FileReference ref = new FileReference((DirectoryBucketContainer)resource, relPath.toString(), relPath.toString()); 
        		ret.add(ref);
        		ref.setContainer();
        	} else
        		ret.add(new FileReference((DirectoryBucketContainer)resource, relPath.toString(), relPath.toString()));
        }
	    return ret;
	}

	@Override
	public FileCursor putBytes(IOutputStream stream, ReadableByteChannel source,
			long numBytesToWrite) throws IOException {
		return putBytes(stream, ((OutputFileStream)stream).getStart(), source, numBytesToWrite);
	}

	@Override
	public FileCursor putBytes(IOutputStream stream, ICursor currentPosition,
			ReadableByteChannel source, long numBytesToWrite)
			throws IOException {
		OutputFileStream fStream = (OutputFileStream)stream;
		FileCursor cursor = (FileCursor)currentPosition;
		
		FileChannel fc = fStream.getFile().getChannel();
		
		ByteBuffer buf = ByteBuffer.allocate((int)numBytesToWrite);
		
		source.read(buf);
		buf.flip();
		int remaining = buf.remaining();
		
		FileLock fLock = (lock) ? fc.lock(cursor.getPosition(), remaining, false) : null;
		
		int length = fc.write(buf, cursor.getPosition());
		
		cursor.setPosition(cursor.getPosition() + length);
		
		if (fLock != null)
			fLock.close();
		return cursor;
	}

	@Override
	public FileCursor putBytes(IOutputStream stream, ByteBuffer source) throws IOException {
		return putBytes(stream, stream.getStart(), source);
	}

	@Override
	public FileCursor putBytes(IOutputStream stream, ICursor currentPosition,
			ByteBuffer source) throws IOException {

		OutputFileStream fStream = (OutputFileStream)stream;
		FileCursor cursor = (FileCursor)currentPosition;
		
		FileChannel fc = fStream.getFile().getChannel();
		
		int remaining = source.remaining();
		
//		System.out.println("Writing " + remaining + " bytes at position " + cursor.getPosition());
		
		FileLock fLock = (lock) ? fc.lock(cursor.getPosition(), remaining, false) : null;
		
		int length = fc.write(source, cursor.getPosition());
		
//		System.out.println("Wrote " + length + " bytes");
		
		FileCursor cursor2 = new FileCursor(cursor.getPosition() + length);

//		cursor.setPosition(cursor.getPosition() + length);
		
		if (fLock != null)
			fLock.close();
		return cursor2;
	}

	@Override
	public Set<IStoredObjectReference> getDirectoryContents(IStoredObjectContainer resource,
			String folder) throws IOException {
		Set<IStoredObjectReference> ret = new HashSet<IStoredObjectReference>();
		
		Path dir = (resource.getFileDirectory() != null &&
				resource.getFileDirectory().startsWith(basePath)) ?
				Paths.get(resource.getFileDirectory()) :
				Paths.get(basePath, nullToEmpty(resource.getFileDirectory()));
		
	    DirectoryStream<Path> directoryStream =  
	    		Files.newDirectoryStream(dir.resolve(folder));
        for (Path path : directoryStream) {
          Path relPath = dir.relativize(path); 
          FileReference ref = new FileReference((DirectoryBucketContainer)resource, relPath.toString(), relPath.toString());
          if (Files.isDirectory(path))
        	  ref.setContainer();
            ret.add(ref);
        }
	    return ret;
	}

	@Override
	public IStoredObjectContainer getDefaultContainer() {
		return new DirectoryBucketContainer(null, basePath);
	}

	@Override
	public boolean createDirectory(IStoredObjectContainer db, String dirName) {
		
		Path dir =
				db.getFileDirectory().startsWith(basePath) ?
				Paths.get(db.getFileDirectory(), dirName)
						:
				Paths.get(basePath, db.getFileDirectory(), dirName);
		
		File file = new File(dir.toString());
		
		if (file.exists())
			return true;
		
		if (file.mkdirs()) {
			return true;
		} else
			return false;
	}

	@Override
	public void removeDirectory(IStoredObjectContainer db, String dirName) {
		
		Path dir =
				db.getFileDirectory().startsWith(basePath) ?
				Paths.get(db.getFileDirectory(), dirName)
						:
				Paths.get(basePath, db.getFileDirectory(), dirName);
		
		File file = new File(dir.toString());
		
		if (!file.exists())
			return;
		
		for (File f: file.listFiles())
			f.delete();
		
		file.delete();
	}

	@Override
	public boolean emptyDirectoryMeansMissing() {
		return false;
	}

	@Override
	public void moveObject(IStoredObjectReference object, IStoredObjectReference ref) throws IOException {
		IStoredObjectContainer resource = ref.getDirBucketContainer();

		String fPath = object.getFilePath().replaceAll("\\\\", "/");
		if (fPath.indexOf('/') != 0)
			fPath = fPath.substring(fPath.indexOf('/') + 1);
		
		Path dir = (resource.getFileDirectory() != null &&
				resource.getFileDirectory().startsWith(basePath)) ?
				Paths.get(resource.getFileDirectory(), ref.getFilePath(), fPath) :
				Paths.get(basePath, nullToEmpty(resource.getFileDirectory()), ref.getFilePath(), fPath);
				
		resource = object.getDirBucketContainer();
		Path file = (resource.getFileDirectory() != null &&
				resource.getFileDirectory().startsWith(basePath)) ?
				Paths.get(resource.getFileDirectory(), object.getFilePath()) :
				Paths.get(basePath, nullToEmpty(resource.getFileDirectory()), object.getFilePath());
				
		// Overwrite target
		File test = new File(file.toString());
		File test1 = new File(dir.toString());
		if (test.exists() && test1.exists())
			Files.delete(dir);
				
		Files.move(file, dir);
		
	}
}
