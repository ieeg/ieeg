/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkNotNull;

import com.amazonaws.services.s3.AmazonS3;

import edu.upenn.cis.braintrust.aws.AwsUtil;

public class S3PresignedUrlFactory implements IUrlFactory {

	private final AmazonS3 s3 = AwsUtil.getS3();
	private final String bucket;
	//The part of the file key which is not included in the database.
	private final String fileKeyPrefix;
	private final long expirationPeriodMs;

	/**
	 * Create an {@code IImageUrlFactory} which creates presigned urls for AWS.
	 * 
	 * @param bucket the bucket holding the items
	 * @param fileKeyPrefix the part of the file key which is not included in the database
	 * @param expirationPeriodHours the number of hours for which the url
	 *            will be valid
	 */
	public S3PresignedUrlFactory(String bucket, String fileKeyPrefix,
			int expirationPeriodHours) {
		this.bucket = checkNotNull(bucket);
		this.fileKeyPrefix = checkNotNull(fileKeyPrefix);
		this.expirationPeriodMs = expirationPeriodHours * 60 * 60 * 1000;
	}

	@Override
	public String getUrl(String fileKey) {
		final String fullFileKey = fileKeyPrefix + fileKey;
		return AwsUtil.generatePresignedUrl(
				s3,
				bucket,
				fullFileKey,
				expirationPeriodMs);
	}

}
