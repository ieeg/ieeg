/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

import edu.upenn.cis.braintrust.IvProps;

/**
 * @author John Frommeyer
 *
 */
public final class SmtpServerFactory {
	private static class SmtpServerSupplier
			implements Supplier<SmtpServer> {

		@Override
		public SmtpServer get() {
			return new SmtpServer(
					IvProps.getMailServer(),
					IvProps.getMailPort(),
					IvProps.getMailLogin(),
					IvProps.getMailPassword(),
					IvProps.isMailSSL());
		}
	}

	private static Supplier<SmtpServer> smtpServerSupplier =
			Suppliers.memoize(new SmtpServerSupplier());

	public static SmtpServer getSmtpServer() {
		return smtpServerSupplier.get();
	}

	@VisibleForTesting
	public static void setSmtpServer(
			final @Nullable SmtpServer smtpServer) {
		smtpServerSupplier =
				new Supplier<SmtpServer>() {

					@Override
					public SmtpServer get() {
						return smtpServer;
					}
				};
	}

	private SmtpServerFactory() {}
}
