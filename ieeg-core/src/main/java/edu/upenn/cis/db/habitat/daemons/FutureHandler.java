/*
 * Copyright 2015 Blackfynn Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.daemons;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.LockSupport;

import edu.upenn.cis.db.habitat.daemons.FutureRegistry.IFutureResult;
import edu.upenn.cis.db.habitat.daemons.exceptions.RequestFailedException;

/**
 * Message callback handler implementing the Future interface
 * 
 * @author zives
 *
 * @param <K> Key for request
 * @param <V> Value type for response message
 */
public class FutureHandler<K,V> implements Future<V> {
	K key;
	V value = null;
	String errCode = null;
	IFutureResult<K,V> successHandler;
	IFutureResult<K,String> errorHandler;
	boolean done = false;
	private final Queue<Thread> waiters = new ConcurrentLinkedQueue<Thread>();
	
	public FutureHandler(K theKey) {
		this.key = theKey;
		successHandler = new IFutureResult<K,V>() {

			@Override
			public V call() throws Exception {
				synchronized (waiters) {
					done = true;
					while (!waiters.isEmpty())
						LockSupport.unpark(waiters.poll());
				}
				return value;
			}

			@Override
			public void setView(K v) {
				key = v;
			}

			@Override
			public K getRequest() {
				return key;
			}

			@Override
			public void setResult(V object) {
				value = object;
			}

			@Override
			public V getResult() {
				return value;
			}
			
		};
		errorHandler = new IFutureResult<K,String>() {

			@Override
			public String call() throws Exception {
				done = true;
				return errCode;
			}

			@Override
			public void setView(K v) {
				key = v;
			}

			@Override
			public K getRequest() {
				return key;
			}

			@Override
			public void setResult(String error) {
				errCode = error;
			}

			@Override
			public String getResult() {
				return errCode;
			}
			
		};
		
	}
	
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCancelled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDone() {
		return done;
	}

	/**
	 * Instantaneous attempt to get -- returns null if request hasn't yet
	 * returned, else returns value.  Triggers exception if there was
	 * an error code.
	 * 
	 */
	@Override
	public V get() throws InterruptedException, ExecutionException {
		synchronized (waiters) {
			if (!done)
				return null;
			else if (errCode != null)
				throw new RequestFailedException(errCode);
			else
				return value;
		}
	}

	@Override
	public V get(long timeout, TimeUnit unit)
			throws InterruptedException, ExecutionException,
			TimeoutException {
		long time = System.nanoTime();
		long timeout2 = timeout;
		if (timeout2 != -1) {
			if (unit == TimeUnit.MICROSECONDS)
				timeout2 *= 1000;
			if (unit == TimeUnit.MILLISECONDS)
				timeout2 *= 1E6;
			if (unit == TimeUnit.SECONDS)
				timeout2 *= 1E9;
		}
		while (true || timeout == -1 || System.nanoTime() - time < timeout2) {
			V ret = get();
			if (ret == null) {
				waiters.add(Thread.currentThread());
				if (timeout != -1)
					LockSupport.parkNanos(this, timeout);
				else
					LockSupport.park(this);
//				Thread.sleep(timeout == -1 ? 10 : timeout / 2);
			} else
				return ret;
			
		}
		throw new TimeoutException();
	}
	
	public IFutureResult<K,V> getSuccessHandler() {
		return successHandler;
	}
	
	public IFutureResult<K,String> getErrorHandler() {
		return errorHandler;
	}
}