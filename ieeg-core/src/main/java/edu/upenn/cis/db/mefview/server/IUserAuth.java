/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface IUserAuth {
	/**
	 * Returns the {@link User} with the given user name if one exists and is enabled.
	 * 
	 * @param username
	 * @return the {@link User} with the given user name if one exists and is enabled
	 * @throws AuthorizationException if no such user exists or if user is disabled
	 */
	public User getEnabledUser(String username)  throws AuthorizationException;

	/**
	 * Returns the {@link User} with the given {@code UserId} if one exists and is enabled.
	 * 
	 * @param username
	 * @return the {@link User} with the given {@code UserId} if one exists and is enabled
	 * @throws AuthorizationException if no such user exists or if user is disabled
	 */
	public User getEnabledUser(UserId userId)  throws AuthorizationException;

	/**
	 * Returns the {@link User} with the given {@code UserId} or null if no such
	 * user exists. It is caller's responsibility to check the status of the
	 * user if necessary.
	 * 
	 * @param userId
	 * @return the {@link User} with the given {@code UserId} or null if no such
	 *         user exists
	 */
	public User getUser(UserId userId);
	
	UserInfo getUserInfoHelper(String requestedUserName);

	List<ToolDto> getToolsHelper(User requestingUser, Set<String> toolRevIds);
}
