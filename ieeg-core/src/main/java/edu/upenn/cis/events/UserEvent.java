package edu.upenn.cis.events;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * User event logging
 */
public class UserEvent {
    private final static Logger logger = LoggerFactory.getLogger(UserEvent.class);

    //
    // Note: fields lengths
    // table is `event_type` varchar(20) NOT NULL, `message` varchar(120) NOT NULL,
    //

    public enum EventType {
        REGISTRATION, LOGIN, FAILED_LOGIN, DATASET_UPLOAD, SHARE, SEARCH, SUBSCRIBE, FORGOT_PASSWORD, FORGOT_PWD_SUCCESS, FORGOT_PWD_FAIL
    }

    public static final void logEvent(EventType eventType, String message) {
        if (IvProps.isProduction()) {
            final Session session = HibernateUtil.getSessionFactory().openSession();
            session.doWork(new CreateEvent(eventType, message));
            session.close();
        }
    }

    /**
     * Helper class to create an event
     */
    private static class CreateEvent implements Work {
        private final EventType eventType;
        private final String message;

        public CreateEvent(EventType eventType, String message) {
            this.eventType = eventType;
            this.message = message;
        }

        public void execute(Connection connection) {
            try {
                PreparedStatement ps = connection.prepareStatement("insert into user_event (event_type, message, event_time) values (?,?, NOW())");
                ps.setString(1, eventType.name());
                String trimMessage = message;
                if (message != null && message.length() > 120) {
                    trimMessage = message.substring(0,120);
                }
                ps.setString(2, trimMessage);
                ps.executeUpdate();
                connection.commit();

                // use the fancy Fluent API to post to a webhook
                String jsonMsg = "{\"text\": \""+eventType+"\n"+message+"\"}";
                Request.Post("https://hooks.slack.com/services/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")
                        .bodyForm(Form.form().add("payload", jsonMsg).build()).execute();
            } catch (IOException e) {
                logger.error("Error creating UserEvent",e);
            } catch (SQLException e) {
                logger.error("Error creating UserEvent",e);
            }

            logger.debug("UserEvent "+eventType.name()+" "+message);
        }
    }

}
