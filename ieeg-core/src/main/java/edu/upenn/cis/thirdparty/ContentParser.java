package edu.upenn.cis.thirdparty;

import java.io.File;
import java.io.InputStream;

import org.apache.lucene.document.Document;
import org.apache.tika.mime.MimeTypeException;

public interface ContentParser {
	public Document getDocument(File file) throws ContentParserException, MimeTypeException;
	
	public Document getDocument(InputStream input, String fileName)
			throws ContentParserException, MimeTypeException;
}
