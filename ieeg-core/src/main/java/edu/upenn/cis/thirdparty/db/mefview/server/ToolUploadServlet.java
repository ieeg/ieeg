/*
 * Copyright 2010 Manuel Carrasco Moñino. (manolo at apache/org) 
 * http://code.google.com/p/gwtupload
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.thirdparty.db.mefview.server;

import static com.google.common.collect.Maps.newConcurrentMap;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.server.SessionManager;
import edu.upenn.cis.db.mefview.shared.util.ToolUploadCookies;
import gwtupload.server.UploadAction;
import gwtupload.server.exceptions.UploadActionException;
import gwtupload.shared.UConsts;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Derived from SampleUploadServlet, Manolo Carrasco Mo�ino,
 * http://code.google.com/p/gwtupload/wiki/GwtUpload_GettingStarted
 * 
 * @author zives
 * 
 */
public class ToolUploadServlet extends UploadAction {
	public static final String INVALID_CHARACTER_REGEX = "[\\\\/><\\|\\s\"'{}()\\[\\]]+";

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(ToolUploadServlet.class);
	private boolean usingS3;
	private SessionManager sessionManager;

	private Map<String, String> receivedContentTypes = newConcurrentMap();
	/**
	 * Maintain a list with received files and their content types.
	 */
	private Map<String, File> receivedFiles = newConcurrentMap();

	public void init() {
		usingS3 = IvProps.isUsingS3();
		final int sessionTimeoutMins = IvProps.getSessionExpirationMins();
		final int sessionCleanupMins = IvProps.getSessionCleanupMins();
		final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();
		sessionManager = new SessionManager(
				sessionTimeoutMins,
				sessionCleanupMins,
				sessionCleanupDenom);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		final String sessionId = getSessionId(request);
		if (sessionId == null) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		} else {
			final SessionToken token = new SessionToken(sessionId);
			sessionManager.authenticateSession(token);
			super.doGet(request, response);
		}

	}

	/**
	 * Override executeAction to save the received files in a custom place and
	 * delete this items from session.
	 */
	@Override
	public String executeAction(HttpServletRequest request,
			List<FileItem> sessionFiles) throws UploadActionException {
		String response = "";
		int cont = 0;
		final String user = request.getParameter("user");
		final String sessionId = getSessionId(request);
		if (sessionId == null) {
			throw new UploadActionException(
					"You are not authorized to upload code.");
		}
		try {
			sessionManager.authenticateSession(new SessionToken(sessionId));
		} catch (SessionNotFoundException e) {
			throw new UploadActionException(
					"You are not authorized to upload code.");
		} catch (SessionExpirationException e) {
			throw new UploadActionException(
					"You are not authorized to upload code.");
		}
		
		for (FileItem item : sessionFiles) {
			if (false == item.isFormField()) {
				cont++;
				try {
					// / Create a new file based on the remote file name in the
					// client
					String saveName = item.getName().replaceAll(
							INVALID_CHARACTER_REGEX, "_");
					// File file =new File("/tmp/" + saveName);

					int firstIndex = saveName.indexOf('.');

					String extension = "";

					if (firstIndex != -1) {
						extension = saveName.substring(firstIndex,
								saveName.length());
					}

					// / Create a temporary file placed in /tmp (only works in
					// unix)
					// File file = File.createTempFile("upload-", ".bin", new
					// File("/tmp"));

					// / Create a temporary file placed in the default system
					// temp folder

					File file = null;

					String fpath = null;

					if (usingS3) {
						file = null;
						fpath = "tools/" + user + "-upload-" + newUuid()
								+ extension;
						// we don't add to receivedFiles here because we don't
						// have a file
						// and it looks like receivedFiles never gets used
						writeToS3(item, fpath);
					} else {
						file = File.createTempFile(user + "-upload-",
								extension, new File("./tools"));
						write(item, file);
						// / Save a list with the received files
						receivedFiles.put(item.getFieldName(), file);
						fpath = file.getPath();
					}
					receivedContentTypes.put(item.getFieldName(),
							item.getContentType());

					// int inx = fpath.indexOf(".\\html\\tools");
					// if (inx >= 0) {
					// fpath = fpath.substring(inx);
					// } else {
					// inx = fpath.indexOf("./html/tools");
					// if (inx >= 0) {
					// fpath = fpath.substring(inx);
					// }
					// }

					// / Send a customized message to the client.
					response += fpath;

				} catch (Exception e) {
					throw new UploadActionException(e);
				}
			}
		}

		// / Remove files from session because we have a copy of them
		removeSessionFileItems(request);

		// / Send your customized message to the client.
		return response;
	}

	/**
	 * Get the content of an uploaded file.
	 */
	@Override
	public void getUploadedFile(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String fieldName = request.getParameter(UConsts.PARAM_SHOW);
		File f = receivedFiles.get(fieldName);
		if (f != null) {
			response.setContentType(receivedContentTypes.get(fieldName));
			FileInputStream is = new FileInputStream(f);
			copyFromInputStreamToOutputStream(is, response.getOutputStream());
		} else {
			renderXmlResponse(request, response, XML_ERROR_ITEM_NOT_FOUND);
		}
	}

	/**
	 * Remove a file when the user sends a delete request.
	 */
	@Override
	public void removeItem(HttpServletRequest request, String fieldName)
			throws UploadActionException {
		File file = receivedFiles.get(fieldName);
		receivedFiles.remove(fieldName);
		receivedContentTypes.remove(fieldName);
		if (file != null) {
			file.delete();
		}
	}

	private void writeToS3(FileItem item, String fileName) throws Exception {
		InputStream is = null;
		try {
			if (item.isInMemory()) {
				is = new ByteArrayInputStream(item.get());
			} else {
				InputStream uploadedFile = item.getInputStream();
				if (uploadedFile != null) {
					is = new BufferedInputStream(uploadedFile);
				} else {
					/*
					 * For whatever reason we cannot write the file to disk.
					 */
					throw new FileUploadException(
							"Cannot write uploaded file to S3!");
				}
			}
			AwsUtil.write(
					AwsUtil.getS3(),
					AwsUtil.createPutObjectRequest(
							IvProps.getToolsBucket(),
							IvProps.getToolsPath() + "/" + fileName,
							is,
							item.getSize()));
		} finally {
			BtUtil.close(is);
		}
	}

	private void write(FileItem item, File file) throws Exception {
		if (item.isInMemory()) {
			FileOutputStream fout = null;
			try {
				fout = new FileOutputStream(file);
				fout.write(item.get());
			} finally {
				if (fout != null) {
					fout.close();
				}
			}
		} else {
			InputStream uploadedFile = item.getInputStream();
			if (uploadedFile != null) {
				BufferedInputStream in = null;
				BufferedOutputStream out = null;
				try {
					in = new BufferedInputStream(uploadedFile);
					out = new BufferedOutputStream(
							new FileOutputStream(file));
					IOUtils.copy(in, out);
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							// ignore
						}
					}
					if (out != null) {
						try {
							out.close();
						} catch (IOException e) {
							// ignore
						}
					}
				}
			} else {
				/*
				 * For whatever reason we cannot write the file to disk.
				 */
				throw new FileUploadException(
						"Cannot write uploaded file to disk!");
			}
		}
	}

	private String getSessionId(HttpServletRequest request) {
		String sessionId = null;
		final Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (final Cookie cookie : cookies) {
				if (ToolUploadCookies.COOKIE_NAME.equals(cookie.getName())) {
					sessionId = cookie.getValue();
					break;
				}
			}
		}
		return sessionId;
	}
}
