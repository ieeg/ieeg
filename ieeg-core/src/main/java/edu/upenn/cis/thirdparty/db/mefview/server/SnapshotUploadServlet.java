/*
 * Copyright 2010 Manuel Carrasco Moñino. (manolo at apache/org) 
 * http://code.google.com/p/gwtupload
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package edu.upenn.cis.thirdparty.db.mefview.server;

import static com.google.common.collect.Maps.newConcurrentMap;
import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.ExtAuthzHandler;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.braintrust.security.SessionNotFoundException;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.mefview.server.IIeegUserService;
import edu.upenn.cis.db.mefview.server.SessionManager;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import gwtupload.server.UploadAction;
import gwtupload.server.exceptions.UploadActionException;
import gwtupload.shared.UConsts;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Derived from SampleUploadServlet, Manolo Carrasco Mo�ino,
 * http://code.google.com/p/gwtupload/wiki/GwtUpload_GettingStarted
 * 
 * @author zives
 * 
 */
public class SnapshotUploadServlet extends UploadAction {
	public static final String INVALID_CHARACTER_REGEX = "[\\\\/><\\|\\s\"'{}()\\[\\]]+";

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(SnapshotUploadServlet.class);
	private boolean usingS3;
	private SessionManager sessionManager;
	private IUserService dus;
	private IIeegUserService auth;
	IDataSnapshotServer ds;
	IObjectServer targetServer;

	private Map<String, String> receivedContentTypes = newConcurrentMap();
	/**
	 * Maintain a list with received files and their content types.
	 */
	private Map<String, String> receivedFiles = newConcurrentMap();

	public void init() {
		usingS3 = IvProps.isUsingS3();
		final int sessionTimeoutMins = IvProps.getSessionExpirationMins();
		final int sessionCleanupMins = IvProps.getSessionCleanupMins();
		final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();
		sessionManager = new SessionManager(
				sessionTimeoutMins,
				sessionCleanupMins,
				sessionCleanupDenom);
		
		ds = sessionManager.getDataSnapshotServer();
		dus = UserServiceFactory.getUserService();
		targetServer = StorageFactory.getPersistentServer();
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		final String sessionId = getSessionId(request);
		if (sessionId == null) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		} else {
			final SessionToken token = new SessionToken(sessionId);
			sessionManager.authenticateSession(token);
			super.doGet(request, response);
		}

	}

	public User verifyUser(
			String sourceMethod, 
			SessionToken sessionID,
			boolean allowExpiredSess, 
			@Nullable Role role) {
		UserIdSessionStatus userIdSessStatus = sessionManager
					.getUserIdSessionStatus(sessionID);

		final UserId userId = userIdSessStatus.getUserId();
		User user = dus.findUserByUid(userId);
		if (user == null) {
			sessionManager.disableAcctSession(sessionID);
			throw new IncorrectCredentialsException("User [" + userId
					+ "] not found");
		}
		if (user.getStatus() == User.Status.DISABLED) {
			sessionManager.disableAcctSession(sessionID);
			throw new DisabledAccountException("User [" + user.getUsername()
					+ "] account disabled.");
		}

		if (role != null && !user.getRoles().contains(role))
			throw new AuthorizationException("User [" + user.getUsername()
					+ "] does not have permission for " + sourceMethod);

		final SessionStatus status = userIdSessStatus.getSessionStatus();
		switch (status) {
			case ACTIVE:
				return user;
			case EXPIRED:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has expired.");
				}
				// Consider logging out as an expiration
			case LOGGED_OUT:
				if (allowExpiredSess) {
					return user;
				} else {
					throw new SessionExpirationException("User ["
							+ user.getUsername() + "] session has logged out.");
				}
			case ACCOUNT_DISABLED:
				throw new DisabledAccountException("User ["
						+ user.getUsername()
						+ "] account disabled.");
			default:
				throw new AssertionError("Unknown enum value [" + status + "]");

		}

	}
	
	public static String getCanonicalName(String user, String snap, String file) {
		return "snapshots/" + user.replaceAll(INVALID_CHARACTER_REGEX, "_") + 
				"/" + snap.replaceAll(INVALID_CHARACTER_REGEX, "_") + "/" + 
				file.replaceAll(INVALID_CHARACTER_REGEX, "_");//newUuid()
	}

	/**
	 * Override executeAction to save the received files in a custom place and
	 * delete this items from session.
	 */
	@Override
	public String executeAction(HttpServletRequest request,
			List<FileItem> sessionFiles) throws UploadActionException {
		String response = "";
		int cont = 0;
//		final String user = request.getParameter("user");
		final String snapshotId = request.getParameter("snapshot");
		final String sessionId = getSessionId(request);
		if (sessionId == null) {
			throw new UploadActionException(
					"You are not authorized to upload snapshots.");
		}
		SessionToken token = new SessionToken(sessionId);
		UserId user;
		try {
			user = sessionManager.authenticateSession(token);
			
			
		} catch (SessionNotFoundException e) {
			throw new UploadActionException(
					"You are not authorized to upload snapshots.");
		} catch (SessionExpirationException e) {
			throw new UploadActionException(
					"You are not authorized to upload snapshots.");
		}
		
		User theUser = verifyUser("SnapshotUploader.executeAction", token, 
				false, Role.USER);//.DATA_ENTERER);
		
		String revId = null;
		try { 
			revId = ds.getDataSnapshotId(theUser, snapshotId, true);
			
		} catch (DataSnapshotNotFoundException e) {
			
		}
		
		if (revId != null) {
//			throw new UploadActionException("You cannot currently upload to an existing snapshot");
			
			// Test that we have permissions
			AuthCheckFactory.getHandler().checkPermitted(auth.getDataSnapshotAccessSession().getPermissions(theUser,
					HasAclType.DATA_SNAPSHOT, CorePermDefs.CORE_MODE_NAME,
					revId, true), CorePermDefs.EDIT, revId);

		}

		for (FileItem item : sessionFiles) {
			if (false == item.isFormField()) {
				cont++;
				try {
					// / Create a new file based on the remote file name in the
					// client
					String saveName = item.getName().replaceAll(
							INVALID_CHARACTER_REGEX, "_");
					// File file =new File("/tmp/" + saveName);

					int firstIndex = saveName.indexOf('.');

					String extension = "";

					if (firstIndex != -1) {
						extension = saveName.substring(firstIndex,
								saveName.length());
					}

					// / Create a temporary file placed in /tmp (only works in
					// unix)
					// File file = File.createTempFile("upload-", ".bin", new
					// File("/tmp"));

					// / Create a temporary file placed in the default system
					// temp folder
					
//					String s3Key = "snapshots/" + theUser.getUsername().replaceAll(INVALID_CHARACTER_REGEX, "_") + 
//							"/" + snapshotId + "/" + saveName;//newUuid()

					String s3Key = getCanonicalName(theUser.getUsername(), snapshotId, saveName);
					
					DirectoryBucketContainer container = (DirectoryBucketContainer)targetServer.getDefaultContainer();
//					FileS3Container container = new FileS3Container(IvProps.getDataBucket(), 
//							IvProps.getSourcePath() + theUser.getUsername() + snapshotId);
					IStoredObjectReference outputHandle = new FileReference(container, s3Key, s3Key);
					
					IOutputStream outStream = targetServer.openForOutput(outputHandle);
					
					System.out.println("Writing to " + s3Key);
					
					writeToStream(item, outStream);

//					File file = null;
//
//					String fpath = null;
//
//					if (usingS3) {
//						file = null;
//						fpath = "snapshots/" + theUser.getUsername() + snapshotId + newUuid() + "/" + saveName;//newUuid()
////								+ extension;
//
//						// / Save a list with the received files
//						receivedFiles.put(item.getFieldName(), fpath);
//						writeToS3(item, fpath);
//					} else {
//						String targetDir = IvProps.getSourcePath();//System.getProperty("java.io.tmpdir");
//						
//						String newDir = targetDir + theUser.getUsername() + snapshotId;
//
//						System.out.println("Dir: " + newDir);
//						System.out.println("File: " + saveName);
//						
//						if (new File(newDir).exists()) {
//							
//						} else {
//							new File(newDir).mkdir();
//						}
//						
////						file = File.createTempFile(user + snapshotId + "-upload/" + newUuid(),
////								extension, new File(newDir));
//						file = new File(newDir + "/" + saveName);
//						write(item, file);
//						// / Save a list with the received files
//						receivedFiles.put(item.getFieldName(), file.getPath());
//						fpath = file.getPath();
//					}
					receivedFiles.put(item.getFieldName(), s3Key);
					receivedContentTypes.put(item.getFieldName(),
							item.getContentType());

					// / Send a customized message to the client.
					response += s3Key;

				} catch (Exception e) {
					throw new UploadActionException(e);
				}
			}
		}

		// / Remove files from session because we have a copy of them
		removeSessionFileItems(request);

		// / Send your customized message to the client.
		return response;
	}

	/**
	 * Get the content of an uploaded file.
	 */
	@Override
	public void getUploadedFile(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String fieldName = request.getParameter(UConsts.PARAM_SHOW);
		String f = receivedFiles.get(fieldName);
		if (f != null) {
			response.setContentType(receivedContentTypes.get(fieldName));
			if (!usingS3) {
				FileInputStream is = new FileInputStream(f);
				copyFromInputStreamToOutputStream(is, response.getOutputStream());
			} else {
				AwsUtil.getObjectContent(
						AwsUtil.getS3(), 
						IvProps.getDataBucket(), 
						f, 
						null, 
						null, 
						null);
			}
		} else {
			renderXmlResponse(request, response, XML_ERROR_ITEM_NOT_FOUND);
		}
	}

	/**
	 * Remove a file when the user sends a delete request.
	 */
	@Override
	public void removeItem(HttpServletRequest request, String fieldName)
			throws UploadActionException {
		String file = receivedFiles.get(fieldName);
		receivedFiles.remove(fieldName);
		receivedContentTypes.remove(fieldName);
		if (file != null) {
			if (!usingS3)
				new File(file).delete();
			else
				AwsUtil.deleteObject(AwsUtil.getS3(), IvProps.getDataBucket(), file);
		}
	}
	
	private void writeToStream(FileItem item, IOutputStream outstream) throws Exception {
		InputStream is = null;
		try {
			if (item.isInMemory()) {
				is = new ByteArrayInputStream(item.get());
			} else {
				InputStream uploadedFile = item.getInputStream();
				if (uploadedFile != null) {
					is = new BufferedInputStream(uploadedFile);
				} else {
					/*
					 * For whatever reason we cannot write the file to disk.
					 */
					throw new FileUploadException(
							"Cannot write uploaded file to S3!");
				}
			}
			IOUtils.copy(is, outstream.getOutputStream());
			outstream.close();
		} finally {
			BtUtil.close(is);
		}
	}

	private void writeToS3(FileItem item, String fileName) throws Exception {
		InputStream is = null;
		try {
			if (item.isInMemory()) {
				is = new ByteArrayInputStream(item.get());
			} else {
				InputStream uploadedFile = item.getInputStream();
				if (uploadedFile != null) {
					is = new BufferedInputStream(uploadedFile);
				} else {
					/*
					 * For whatever reason we cannot write the file to disk.
					 */
					throw new FileUploadException(
							"Cannot write uploaded file to S3!");
				}
			}
			AwsUtil.write(
					AwsUtil.getS3(),
					AwsUtil.createPutObjectRequest(
							IvProps.getDataBucket(),
							IvProps.getSourcePath() + "/" + fileName,
							is,
							item.getSize()));
		} finally {
			BtUtil.close(is);
		}
	}

	private void write(FileItem item, File file) throws Exception {
		if (item.isInMemory()) {
			FileOutputStream fout = null;
			try {
				fout = new FileOutputStream(file);
				fout.write(item.get());
			} finally {
				if (fout != null) {
					System.out.println("Wrote " + file.getPath());
					fout.close();
				}
			}
		} else {
			InputStream uploadedFile = item.getInputStream();
			if (uploadedFile != null) {
				BufferedInputStream in = null;
				BufferedOutputStream out = null;
				try {
					in = new BufferedInputStream(uploadedFile);
					out = new BufferedOutputStream(
							new FileOutputStream(file));
					IOUtils.copy(in, out);
				} finally {
					if (in != null) {
						try {
							in.close();
						} catch (IOException e) {
							// ignore
						}
					}
					if (out != null) {
						try {
							System.out.println("Wrote " + file.getPath());
							out.close();
						} catch (IOException e) {
							// ignore
						}
					}
				}
			} else {
				/*
				 * For whatever reason we cannot write the file to disk.
				 */
				throw new FileUploadException(
						"Cannot write uploaded file to disk!");
			}
		}
	}

	private String getSessionId(HttpServletRequest request) {
		String sessionId = request.getParameter("session");
//		final Cookie[] cookies = request.getCookies();
//		if (cookies != null) {
//			for (final Cookie cookie : cookies) {
//				if (ToolUploadCookies.COOKIE_NAME.equals(cookie.getName())) {
//					sessionId = cookie.getValue();
//					break;
//				}
//			}
//		}
		System.err.println("Session ID: " + sessionId);
		return sessionId;
	}
}
