package edu.upenn.cis.thirdparty;

public class ContentParserException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContentParserException(Exception e) {
		super(e.getMessage());
	}
}
