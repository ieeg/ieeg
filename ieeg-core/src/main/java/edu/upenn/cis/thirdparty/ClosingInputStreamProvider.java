/*
 * From Resteasy which is Apache 2.0 licensed. (There was no header in the original file.)
 */
package edu.upenn.cis.thirdparty;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.util.HttpHeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.webapp.ContentLengthInputStream;


/**
 * Fixes a bug: the input stream was not being closed when this was done with
 * it.
 * 
 * @author <a href="mailto:bill@burkecentral.com">BillBurke</a>
 * @version $Revision: 1 $
 */
@Provider
@Produces("*/*")
@Consumes("*/*")
public class ClosingInputStreamProvider implements
		MessageBodyReader<InputStream>,
		MessageBodyWriter<InputStream>
{

	private static Logger logger = LoggerFactory
			.getLogger(ClosingInputStreamProvider.class);

	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType)
	{
		return type.equals(InputStream.class);
	}

	public InputStream readFrom(Class<InputStream> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException
	{
		return entityStream;
	}

	public boolean isWriteable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType)
	{
		return InputStream.class.isAssignableFrom(type);
	}

	public long getSize(InputStream inputStream, Class<?> type,
			Type genericType, Annotation[] annotations, MediaType mediaType)
	{
		return -1;
	}

	public void writeTo(InputStream inputStream, Class<?> type,
			Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders,
			OutputStream entityStream) throws IOException
	{
		long in = System.nanoTime();
		final String m = "writeTo(...)";
		logger.debug("{}: using fixed writeTo", m);
		try {
			int c = inputStream.read();
			if (c == -1)
			{
				httpHeaders.putSingle(HttpHeaderNames.CONTENT_LENGTH,
						Integer.toString(0));
				entityStream.write(new byte[0]); // fix RESTEASY-204
				return;
			}
			if (inputStream instanceof ContentLengthInputStream) {
				Long contentLength = Long
						.valueOf(((ContentLengthInputStream) inputStream)
								.getSizeBytes());
				logger.debug(
						"{}: setting content length to {}",
						m,
						contentLength);
				httpHeaders.putSingle(HttpHeaderNames.CONTENT_LENGTH,
						contentLength);
			}
			entityStream.write(c);
			writeTo(inputStream, entityStream);
		} finally {
			BtUtil.close(inputStream);
			logger.info("{}: {} seconds", m, BtUtil.diffNowThenSeconds(in));
		}
	}

	public static void writeTo(final InputStream in, final OutputStream out)
			throws IOException
	{
		int read;
		final byte[] buf = new byte[4096];
		while ((read = in.read(buf)) != -1)
		{
			out.write(buf, 0, read);
		}
	}
}
