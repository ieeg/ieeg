ALTER TABLE `electrode` MODIFY `sampling_rate` DOUBLE NOT NULL;
ALTER TABLE `trace` MODIFY `mri_coordinate_x` DOUBLE NULL;
ALTER TABLE `trace` MODIFY `mri_coordinate_y` DOUBLE NULL;
ALTER TABLE `trace` MODIFY `mri_coordinate_z` DOUBLE NULL;
ALTER TABLE `trace` MODIFY `talairach_x` DOUBLE NULL;
ALTER TABLE `trace` MODIFY `talairach_y` DOUBLE NULL;
ALTER TABLE `trace` MODIFY `talairach_z` DOUBLE NULL;
ALTER TABLE `eeg_study` ADD CONSTRAINT `dir` UNIQUE (`dir`);
