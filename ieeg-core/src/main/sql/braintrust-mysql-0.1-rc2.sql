-- MySQL dump 10.13  Distrib 5.1.40, for apple-darwin9.5.0 (i386)
--
-- Host: localhost    Database: braintrust
-- ------------------------------------------------------
-- Server version	5.1.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `antiepileptic_drug`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `antiepileptic_drug` (
  `antiepileptic_drug_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `failed` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `taking_at_time_of_admission` bit(1) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `treatment_id` bigint(20) NOT NULL,
  PRIMARY KEY (`antiepileptic_drug_id`),
  KEY `FK554153CC98C9333B` (`treatment_id`),
  CONSTRAINT `FK554153CC98C9333B` FOREIGN KEY (`treatment_id`) REFERENCES `treatment` (`treatment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_info` (
  `contact_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_number` int(11) NOT NULL,
  `contact_type` int(11) NOT NULL,
  `material` varchar(255) NOT NULL,
  `relative_coordinate_x` double NOT NULL,
  `relative_coordinate_y` double NOT NULL,
  `relative_coordinate_z` double NOT NULL,
  `surface_area_mm_sqr` double NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `electrode_info_id` bigint(20) NOT NULL,
  PRIMARY KEY (`contact_info_id`),
  KEY `FK4C268D6D4FF7D176` (`electrode_info_id`),
  CONSTRAINT `FK4C268D6D4FF7D176` FOREIGN KEY (`electrode_info_id`) REFERENCES `electrode_info` (`electrode_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eeg_study`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eeg_study` (
  `eeg_study_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir` varchar(255) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `maf_file` varchar(255) NOT NULL,
  `mef_dir` varchar(255) NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `seizure_count` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `hospital_admission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`eeg_study_id`),
  KEY `IDX_SEIZURE_COUNT` (`seizure_count`),
  KEY `FK6C1FC751D94C0966` (`hospital_admission_id`),
  CONSTRAINT `FK6C1FC751D94C0966` FOREIGN KEY (`hospital_admission_id`) REFERENCES `hospital_admission` (`hospital_admission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eeg_study_image`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eeg_study_image` (
  `eeg_study_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  PRIMARY KEY (`eeg_study_id`,`image_id`),
  UNIQUE KEY `image_id` (`image_id`),
  KEY `FKF5DE52ED1CEACCDB` (`image_id`),
  KEY `FKF5DE52EDD3072E6A` (`eeg_study_id`),
  CONSTRAINT `FKF5DE52EDD3072E6A` FOREIGN KEY (`eeg_study_id`) REFERENCES `eeg_study` (`eeg_study_id`),
  CONSTRAINT `FKF5DE52ED1CEACCDB` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `eeg_study_image_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eeg_study_image_type` (
  `eeg_study_id` bigint(20) NOT NULL,
  `image_type` int(11) NOT NULL,
  PRIMARY KEY (`eeg_study_id`,`image_type`),
  KEY `FK8C22C14CD3072E6A` (`eeg_study_id`),
  CONSTRAINT `FK8C22C14CD3072E6A` FOREIGN KEY (`eeg_study_id`) REFERENCES `eeg_study` (`eeg_study_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrode`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrode` (
  `electrode_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `impedance` double DEFAULT NULL,
  `lobe` int(11) NOT NULL,
  `side` int(11) NOT NULL,
  `channel_prefix` varchar(255) NOT NULL,
  `hff_setting` int(11) NOT NULL,
  `lff_setting` int(11) NOT NULL,
  `notch_filter` bit(1) NOT NULL,
  `sampling_rate` float DEFAULT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `electrode_info_id` bigint(20) NOT NULL,
  `eeg_study_id` bigint(20) NOT NULL,
  PRIMARY KEY (`electrode_id`),
  KEY `IDX_CHANNEL_PREFIX` (`channel_prefix`),
  KEY `IDX_SAMPLING_RATE` (`sampling_rate`),
  KEY `FKE05C058D4FF7D176` (`electrode_info_id`),
  KEY `FKE05C058DD3072E6A` (`eeg_study_id`),
  CONSTRAINT `FKE05C058DD3072E6A` FOREIGN KEY (`eeg_study_id`) REFERENCES `eeg_study` (`eeg_study_id`),
  CONSTRAINT `FKE05C058D4FF7D176` FOREIGN KEY (`electrode_info_id`) REFERENCES `electrode_info` (`electrode_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrode_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrode_info` (
  `electrode_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `electrode_type` varchar(255) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `model_no` varchar(255) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`electrode_info_id`),
  UNIQUE KEY `manufacturer` (`manufacturer`,`model_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_admission`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_admission` (
  `hospital_admission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `age_at_admission` int(11) DEFAULT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `patient_id` bigint(20) NOT NULL,
  PRIMARY KEY (`hospital_admission_id`),
  KEY `FK94751044C771385B` (`patient_id`),
  CONSTRAINT `FK94751044C771385B` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hospital_admission_seizure_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital_admission_seizure_type` (
  `hospital_admission_id` bigint(20) NOT NULL,
  `seizure_type` int(11) DEFAULT NULL,
  KEY `FK37CCA7CFD94C0966` (`hospital_admission_id`),
  CONSTRAINT `FK37CCA7CFD94C0966` FOREIGN KEY (`hospital_admission_id`) REFERENCES `hospital_admission` (`hospital_admission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `image`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_key` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `image_trace`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image_trace` (
  `image_id` bigint(20) NOT NULL,
  `trace_id` bigint(20) NOT NULL,
  `pixel_x` int(11) DEFAULT NULL,
  `pixel_y` int(11) DEFAULT NULL,
  PRIMARY KEY (`image_id`,`trace_id`),
  KEY `FK96234D611CEACCDB` (`image_id`),
  KEY `FK96234D619C7BCC9B` (`trace_id`),
  CONSTRAINT `FK96234D619C7BCC9B` FOREIGN KEY (`trace_id`) REFERENCES `trace` (`trace_id`),
  CONSTRAINT `FK96234D611CEACCDB` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patient`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `patient_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `age_of_onset` int(11) NOT NULL,
  `developmental_delay` varchar(255) DEFAULT NULL,
  `developmental_disorders` bit(1) NOT NULL,
  `ethnicity` int(11) NOT NULL,
  `etiology` varchar(255) NOT NULL,
  `family_history` bit(1) NOT NULL,
  `gender` int(11) NOT NULL,
  `handedness` int(11) NOT NULL,
  `institution` varchar(255) NOT NULL,
  `traumatic_brain_injury` bit(1) NOT NULL,
  `user_supplied_id` varchar(255) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`patient_id`),
  UNIQUE KEY `user_supplied_id` (`user_supplied_id`),
  KEY `IDX_ETIOLOGY` (`etiology`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patient_image`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_image` (
  `patient_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  PRIMARY KEY (`patient_id`,`image_id`),
  UNIQUE KEY `image_id` (`image_id`),
  KEY `FKAF8DDFA11CEACCDB` (`image_id`),
  KEY `FKAF8DDFA1C771385B` (`patient_id`),
  CONSTRAINT `FKAF8DDFA1C771385B` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`),
  CONSTRAINT `FKAF8DDFA11CEACCDB` FOREIGN KEY (`image_id`) REFERENCES `image` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patient_precipitant`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_precipitant` (
  `patient_id` bigint(20) NOT NULL,
  `precipitant` int(11) NOT NULL,
  PRIMARY KEY (`patient_id`,`precipitant`),
  KEY `FK6288FBBC771385B` (`patient_id`),
  CONSTRAINT `FK6288FBBC771385B` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `patient_seizure_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_seizure_type` (
  `patient_id` bigint(20) NOT NULL,
  `seizure_type` int(11) NOT NULL,
  PRIMARY KEY (`patient_id`,`seizure_type`),
  KEY `FKAA150EAEC771385B` (`patient_id`),
  CONSTRAINT `FKAA150EAEC771385B` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reference_electrode`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reference_electrode` (
  `reference_electrode_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `impedance` double DEFAULT NULL,
  `lobe` int(11) DEFAULT NULL,
  `side` int(11) DEFAULT NULL,
  `mri_coordinate_x` double DEFAULT NULL,
  `mri_coordinate_y` double DEFAULT NULL,
  `mri_coordinate_z` double DEFAULT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `eeg_study_id` bigint(20) NOT NULL,
  `reference_electrode_info_id` bigint(20) NOT NULL,
  PRIMARY KEY (`reference_electrode_id`),
  UNIQUE KEY `eeg_study_id` (`eeg_study_id`),
  KEY `FK21B5DD99D3072E6A` (`eeg_study_id`),
  KEY `FK21B5DD99A17A1915` (`reference_electrode_info_id`),
  CONSTRAINT `FK21B5DD99A17A1915` FOREIGN KEY (`reference_electrode_info_id`) REFERENCES `reference_electrode_info` (`reference_electrode_info_id`),
  CONSTRAINT `FK21B5DD99D3072E6A` FOREIGN KEY (`eeg_study_id`) REFERENCES `eeg_study` (`eeg_study_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reference_electrode_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reference_electrode_info` (
  `reference_electrode_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `material` varchar(255) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`reference_electrode_info_id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `source_document`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_document` (
  `source_document_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_uri` varchar(255) NOT NULL,
  `impression` varchar(255) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `patient_id` bigint(20) NOT NULL,
  PRIMARY KEY (`source_document_id`),
  KEY `FKE9E588BFC771385B` (`patient_id`),
  CONSTRAINT `FKE9E588BFC771385B` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surgical_intervention`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surgical_intervention` (
  `surgical_intervention_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lobe` int(11) NOT NULL,
  `postop_clinical_desc` varchar(255) NOT NULL,
  `postop_rating_scale` int(11) NOT NULL,
  `side` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `treatment_id` bigint(20) NOT NULL,
  PRIMARY KEY (`surgical_intervention_id`),
  KEY `FK5133ADA98C9333B` (`treatment_id`),
  CONSTRAINT `FK5133ADA98C9333B` FOREIGN KEY (`treatment_id`) REFERENCES `treatment` (`treatment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trace`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trace` (
  `trace_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_no` int(11) NOT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `ictal_onset_region_contact` bit(1) DEFAULT NULL,
  `involved_inasz` bit(1) DEFAULT NULL,
  `mri_coordinate_x` double NOT NULL,
  `mri_coordinate_y` double NOT NULL,
  `mri_coordinate_z` double NOT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `talairach_x` double NOT NULL,
  `talairach_y` double NOT NULL,
  `talairach_z` double NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `electrode_id` bigint(20) NOT NULL,
  PRIMARY KEY (`trace_id`),
  KEY `IDX_CONTACT_NO` (`contact_no`),
  KEY `FK697F145CDAB9F1B` (`electrode_id`),
  CONSTRAINT `FK697F145CDAB9F1B` FOREIGN KEY (`electrode_id`) REFERENCES `electrode` (`electrode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `treatment`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `treatment` (
  `treatment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brain_stimulator_implant` bit(1) NOT NULL,
  `ketogenic_diet` bit(1) NOT NULL,
  `vagus_nerve_stimulator` bit(1) NOT NULL,
  `obj_version` int(11) DEFAULT NULL,
  `hospital_admission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`treatment_id`),
  KEY `FKFC397878D94C0966` (`hospital_admission_id`),
  CONSTRAINT `FKFC397878D94C0966` FOREIGN KEY (`hospital_admission_id`) REFERENCES `hospital_admission` (`hospital_admission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-11-03 10:50:38
