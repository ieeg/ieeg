/**
 * 
 */
IndexOfTest = function() {
	this.minMaxArrayZeroModFour = [ [ 0, 1, 5 ], [ 0, 3, 15 ], [ 50.4, 7, 19 ],
			[ 50.4, 23, 56 ], [ 100, 5, 0 ], [ 100, 10, 3 ], [ 150.39, 2 ],
			[ 150.39, 20 ] ];

	this.minMaxArrayOneModFour = [ [ 0, 1, 5 ], [ 0, 3, 15 ], [ 50.4, 7, 19 ],
			[ 50.4, 23, 56 ], [ 100, 10, 3 ], [ 150.39, 2 ], [ 150.39, 20 ],
			[ 200.41, 4 ], [ 200.41, 7 ] ];

	this.minMaxArrayTwoModFour = [ [ 0, 1, 5 ], [ 0, 3, 15 ], [ 50.4, 7, 19 ],
			[ 50.4, 23, 56 ], [ 100, 5, 0 ], [ 100, 10, 3 ], [ 150.39, 2 ],
			[ 150.39, 20 ], [ 200.41, 4 ], [ 200.41, 7 ] ];

	this.minMaxArrayThreeModFour = [ [ 0, 1, 5 ], [ 0, 3, 15 ],
			[ 50.4, 7, 19 ], [ 50.4, 23, 56 ], [ 100, 5, 0 ], [ 150.39, 2 ],
			[ 150.39, 20 ] ];

	this.singlePointsArray = [ [ 0, 1, 5 ], [ 50.4, 7, 19 ], [ 100, 10, 3 ],
			[ 150.39, 2 ], [ 200.41, 4 ], ];
}

IndexOfTest.prototype.testMinMaxZeroModFour = function testMinMaxZeroModFour() {
	var dataSet = new DDataSet(this.minMaxArrayZeroModFour);

	var indexOf = dataSet.indexOf(-1.4);
	this.assertEquals(-1, indexOf);

	indexOf = dataSet.lastIndexOf(-1.4);
	this.assertEquals(-1, indexOf);

	//

	indexOf = dataSet.indexOf(0);
	this.assertEquals(0, indexOf);

	indexOf = dataSet.lastIndexOf(0);
	this.assertEquals(1, indexOf);

	//

	indexOf = dataSet.indexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(50.4);
	this.assertEquals(2, indexOf);

	indexOf = dataSet.lastIndexOf(50.4);
	this.assertEquals(3, indexOf);

	//

	indexOf = dataSet.indexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(100);
	this.assertEquals(4, indexOf);

	indexOf = dataSet.lastIndexOf(100);
	this.assertEquals(5, indexOf);

	//

	indexOf = dataSet.indexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(150.39);
	this.assertEquals(6, indexOf);

	indexOf = dataSet.lastIndexOf(150.39);
	this.assertEquals(7, indexOf);

	//

	indexOf = dataSet.indexOf(170.5);
	this.assertEquals(6, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(170.5);
	this.assertEquals(6, -indexOf - 2);
};

IndexOfTest.prototype.testMinMaxOneModFour = function testMinMaxOneModFour() {
	var dataSet = new DDataSet(this.minMaxArrayOneModFour);

	var indexOf = dataSet.indexOf(-1.4);
	this.assertEquals(-1, indexOf);

	indexOf = dataSet.lastIndexOf(-1.4);
	this.assertEquals(-1, indexOf);

	//

	indexOf = dataSet.indexOf(0);
	this.assertEquals(0, indexOf);

	indexOf = dataSet.lastIndexOf(0);
	this.assertEquals(1, indexOf);

	//

	indexOf = dataSet.indexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(50.4);
	this.assertEquals(2, indexOf);

	indexOf = dataSet.lastIndexOf(50.4);
	this.assertEquals(3, indexOf);

	//

	indexOf = dataSet.indexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(100);
	this.assertEquals(4, indexOf);

	indexOf = dataSet.lastIndexOf(100);
	this.assertEquals(4, indexOf);

	//

	indexOf = dataSet.indexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(150.39);
	this.assertEquals(5, indexOf);

	indexOf = dataSet.lastIndexOf(150.39);
	this.assertEquals(6, indexOf);

	//

	indexOf = dataSet.indexOf(170.5);
	this.assertEquals(5, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(170.5);
	this.assertEquals(5, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(200.41);
	this.assertEquals(7, indexOf);

	indexOf = dataSet.lastIndexOf(200.41);
	this.assertEquals(8, indexOf);

	//

	indexOf = dataSet.indexOf(270.5);
	this.assertEquals(7, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(270.5);
	this.assertEquals(7, -indexOf - 2);
};

IndexOfTest.prototype.testMinMaxThreeModFour = function testMinMaxThreeModFour() {
	var dataSet = new DDataSet(this.minMaxArrayThreeModFour);

	var indexOf = dataSet.indexOf(-1.4);
	this.assertEquals(-1, indexOf);

	indexOf = dataSet.lastIndexOf(-1.4);
	this.assertEquals(-1, indexOf);

	//

	indexOf = dataSet.indexOf(0);
	this.assertEquals(0, indexOf);

	indexOf = dataSet.lastIndexOf(0);
	this.assertEquals(1, indexOf);

	//

	indexOf = dataSet.indexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(50.4);
	this.assertEquals(2, indexOf);

	indexOf = dataSet.lastIndexOf(50.4);
	this.assertEquals(3, indexOf);

	//

	indexOf = dataSet.indexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	// Only one entry with value 100

	indexOf = dataSet.indexOf(100);
	this.assertEquals(4, indexOf);

	indexOf = dataSet.lastIndexOf(100);
	this.assertEquals(4, indexOf);

	//

	indexOf = dataSet.indexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(150.39);
	this.assertEquals(5, indexOf);

	indexOf = dataSet.lastIndexOf(150.39);
	this.assertEquals(6, indexOf);

	//

	indexOf = dataSet.indexOf(170.5);
	this.assertEquals(5, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(170.5);
	this.assertEquals(5, -indexOf - 2);
};

IndexOfTest.prototype.testMinMaxTwoModFour = function testMinMaxTwoModFour() {
	var dataSet = new DDataSet(this.minMaxArrayTwoModFour);

	var indexOf = dataSet.indexOf(-1.4);
	this.assertEquals(-1, indexOf);

	indexOf = dataSet.lastIndexOf(-1.4);
	this.assertEquals(-1, indexOf);

	//

	indexOf = dataSet.indexOf(0);
	this.assertEquals(0, indexOf);

	indexOf = dataSet.lastIndexOf(0);
	this.assertEquals(1, indexOf);

	//

	indexOf = dataSet.indexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(50.4);
	this.assertEquals(2, indexOf);

	indexOf = dataSet.lastIndexOf(50.4);
	this.assertEquals(3, indexOf);

	//

	indexOf = dataSet.indexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(70.5);
	this.assertEquals(2, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(100);
	this.assertEquals(4, indexOf);

	indexOf = dataSet.lastIndexOf(100);
	this.assertEquals(5, indexOf);

	//

	indexOf = dataSet.indexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(130.5);
	this.assertEquals(4, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(150.39);
	this.assertEquals(6, indexOf);

	indexOf = dataSet.lastIndexOf(150.39);
	this.assertEquals(7, indexOf);

	//

	indexOf = dataSet.indexOf(170.5);
	this.assertEquals(6, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(170.5);
	this.assertEquals(6, -indexOf - 2);

	//

	indexOf = dataSet.indexOf(200.41);
	this.assertEquals(8, indexOf);

	indexOf = dataSet.lastIndexOf(200.41);
	this.assertEquals(9, indexOf);

	//

	indexOf = dataSet.indexOf(270.5);
	this.assertEquals(8, -indexOf - 2);

	indexOf = dataSet.lastIndexOf(270.5);
	this.assertEquals(8, -indexOf - 2);
};

IndexOfTest.prototype.testSinglePoints = function testSinglePoints() {
	var singlePointsArray = this.singlePointsArray;
	var singlePointsSet = new DDataSet(singlePointsArray);

	var indexOf = singlePointsSet.indexOf(-1.4);
	this.assertEquals(-1, indexOf);

	indexOf = singlePointsSet.lastIndexOf(-1.4);
	this.assertEquals(-1, indexOf);

	//

	indexOf = singlePointsSet.indexOf(0);
	this.assertEquals(0, indexOf);

	indexOf = singlePointsSet.lastIndexOf(0);
	this.assertEquals(0, indexOf);

	//

	indexOf = singlePointsSet.indexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	indexOf = singlePointsSet.lastIndexOf(10.5);
	this.assertEquals(0, -indexOf - 2);

	//

	indexOf = singlePointsSet.indexOf(50.4);
	this.assertEquals(1, indexOf);

	indexOf = singlePointsSet.lastIndexOf(50.4);
	this.assertEquals(1, indexOf);

	//

	indexOf = singlePointsSet.indexOf(70.5);
	this.assertEquals(1, -indexOf - 2);

	indexOf = singlePointsSet.lastIndexOf(70.5);
	this.assertEquals(1, -indexOf - 2);

	//

	indexOf = singlePointsSet.indexOf(100);
	this.assertEquals(2, indexOf);

	indexOf = singlePointsSet.lastIndexOf(100);
	this.assertEquals(2, indexOf);

	//

	indexOf = singlePointsSet.indexOf(130.5);
	this.assertEquals(2, -indexOf - 2);

	indexOf = singlePointsSet.lastIndexOf(130.5);
	this.assertEquals(2, -indexOf - 2);

	//

	indexOf = singlePointsSet.indexOf(150.39);
	this.assertEquals(3, indexOf);

	indexOf = singlePointsSet.lastIndexOf(150.39);
	this.assertEquals(3, indexOf);

	//

	indexOf = singlePointsSet.indexOf(170.5);
	this.assertEquals(3, -indexOf - 2);

	indexOf = singlePointsSet.lastIndexOf(170.5);
	this.assertEquals(3, -indexOf - 2);

	//

	indexOf = singlePointsSet.indexOf(200.41);
	this.assertEquals(4, indexOf);

	indexOf = singlePointsSet.lastIndexOf(200.41);
	this.assertEquals(4, indexOf);

	//

	indexOf = singlePointsSet.indexOf(270.5);
	this.assertEquals(4, -indexOf - 2);

	indexOf = singlePointsSet.lastIndexOf(270.5);
	this.assertEquals(4, -indexOf - 2);

};

IndexOfTest.prototype.assertEquals = function(expected, actual) {
	if (expected != actual) {
		throw new Error("Expected: " + expected + ", Actual: " + actual);
	}
};

IndexOfTest.prototype.passed = function(testName) {
	console.log("Passed: " + testName);
};

IndexOfTest.prototype.failed = function(testName, error) {
	console.error("Failed: ", testName, error);
};

IndexOfTest.prototype.runTest = function(test) {
	var context = this;
	return function() {
		var args = arguments;
		try {
			// Use of test.name is why the test functions are double named when
			// defined.
			console.log("Running: " + test.name);
			test.apply(context, args);
			context.passed(test.name);
		} catch (e) {
			context.failed(test.name, e);
		}
	};
};

IndexOfTest.prototype.runTests = function() {
	this.runTest(this.testMinMaxZeroModFour)();
	this.runTest(this.testMinMaxOneModFour)();
	this.runTest(this.testMinMaxTwoModFour)();
	this.runTest(this.testMinMaxThreeModFour)();
	this.runTest(this.testSinglePoints)();
}

document.addEventListener("DOMContentLoaded", function(event) {
	var test = new IndexOfTest();
	test.runTests();
});
