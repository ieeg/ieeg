/**
 * $Id: CsvDygraph.java 14 2010-08-31 21:46:16Z steven.jardine $ 
 * Copyright (c) 2010 Steven Jardine, MJN Services, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */
package org.danvk;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayBoolean;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Timer;

/**
 * A GWT wrapper for the TsDygraph implementation. This wrapper handles GWT JsArrays
 * for dygraph.
 * 
 * @version $Rev: 14 $
 * @author Original author Steven Jardine
 * @author New code copyright 2011-3 by Zachary G. Ives and the Trustees of the University of Pennsylvania
 */
public class JsArrayDygraph extends AbstractDygraph {

    private static int idCount = 0;

    /**
     * Draw the dygraph.
     * 
     * @param elementId
     *            the elementId of the div to draw the graph.
     * @param dataArray
     *            the <b>ENCODED</b> url for the dygraph data.
     * @param options
     *            the dygraph options to set.
     * @return the JavaScript object.
     */
    private static native JavaScriptObject drawDygraphParsed(
	    final Element element, final JsArray<?> points,
	    final JsArrayString labels, 
	    final double minDurationMicros,
	    final double maxDurationMicros, final JavaScriptObject options) /*-{
		var status = "";

		options.labels = labels;
		try {
			var g = new $wnd.TsDygraph(element,//$wnd.document.getElementById(elementId),
					points, options);
				g.setMinDurationMicros(minDurationMicros);
				g.setMaxDurationMicros(maxDurationMicros);

			return g;
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
		return null;
    }-*/;

    /**
     * @return the next id for the csv dygraph.
     */
    private static String getNextId() {
	return "json-dygraph-" + idCount++;
    }

    public native double getPosition() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		var range = graph.dataView_.getXView();
		return parseFloat(range[0]);
    }-*/;

    public native double getWindowWidth() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		var range = graph.dataView_.getXView();
		return parseFloat(range[1] - range[0]);// * 4;
    }-*/;
    
    public native double getPeriod() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.dataView_.getPeriod();
    }-*/;

    public void setPosition(double minX, double maxX, double period,
	    boolean dontRedraw) {
	try {
	    setPosition(dygraph, minX, maxX, period, dontRedraw);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
    public native void setPosition(final JavaScriptObject graph, double minX,
	    double maxX, double period, boolean dontRedraw) /*-{
		try {
			//			if (console && console.info)
			//				console.info("Setting position to [" + minX + "," + maxX + "]");
			graph.setPosition(minX, maxX, period);
			graph.dataView_.setXView(minX, maxX);
			graph.requestDataFromServer();
			if (!dontRedraw) {
				//			console.debug("Changing position -- redraw");
				graph.drawGraph_(graph.rawData_, true);
			}
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
    }-*/;

    public void initPosition(double minX, double maxX) {
    	try {
    	    initPosition(dygraph, minX, maxX);
    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
    }
    
    public void invertSignal(boolean inv) {
    	invertSignal(dygraph, inv);
    }
    
    public native void invertSignal(final JavaScriptObject graph, boolean inv) /*-{
    	graph.dataView_.setInverted(inv);
    }-*/;
    
    /**
     * The EEG signal is plotted inverted?
     * 
     * @return
     */
    public boolean isInvertedSignal() {
    	return (isInvertedSignal(dygraph));
    }

    public native boolean isInvertedSignal(final JavaScriptObject graph) /*-{
		if (graph.dataView_.isInverted())
			return true;
		else
			return false;
	}-*/;
    
    public native void initPosition(final JavaScriptObject graph, double minX,
    	    double maxX) /*-{
    		try {
    			graph.dataView_.setXView(minX, maxX);
    		} catch (err) {
    			if (console && console.error)
    				console.error("Error " + err);
    		}
        }-*/;
    
    public native void addNewData(// final JavaScriptObject graph,
	    final JsArray<?> arr, boolean nodraw, boolean isMinMax) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;

		//		console.debug("** ADDING DATA **");
		if (graph == null || arr == null || arr.length == 0)
			return;
		try {
			graph.addNewData(arr, false, nodraw, isMinMax);
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
    }-*/;

    public native void addAnnotations(// final JavaScriptObject graph,
	    final String json) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		var ann = graph.annotations();
		var newAnn = JSON.parse(json);
		for ( var i = 0; i < newAnn.length; i++)
			ann.push(newAnn[i]);
		try {
			if (newAnn.length > 0)
				graph.setAnnotations(ann);
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
    }-*/;
    
    public native void setVoltConvFactor(double voltConvF) /*-{
    	var graph = this.@org.danvk.JsArrayDygraph::dygraph;
    	graph.dataView_.setVoltConvFactor(voltConvF);
    }-*/;

    public native void setAnnotations(final String json) /*-{
		try {
			var graph = this.@org.danvk.JsArrayDygraph::dygraph;
			graph.setAnnotations(JSON.parse(json));
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
    }-*/;

    public native boolean isHidden(// final JavaScriptObject graph,
	    final String trace) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		for ( var x = graph.dataView_.labels.length - 1; x >= 1; x--) {
			if (graph.dataView_.labels[x] == trace) {
				return !graph.isVisible(x - 1);
			}
		}
		return true;
    }-*/;

    public native JsArrayBoolean getVisibility() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.getVisibility();
    }-*/;

    public native JsArrayString getLabels() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.dataView_.labels;
    }-*/;
    
    public double getMaxDurationMicros() {
    	return maxDurationMicros;
    }
    
    public double getMinDurationMicros() {
    	return minDurationMicros;
    }

	public void showThese(final boolean[] v) {
		setVisibility(v);
		refresh();
	}

//    private native void setFullScale(boolean full) /*-{
//		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
//		graph.setFullScale(full);
//    }-*/;

//    private native void rescale(final double scale) /*-{
//		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
//		graph.rescale(scale);
//    }-*/;

    public native void showItem(final int x, final boolean visib) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.dataView_.setRowVisibility(x, visib);
    }-*/;
    
    public void setMaxDurationMicros(final double maxDurationMicros) {
    	this.maxDurationMicros = maxDurationMicros;
  	}
    
    public void setMinDurationMicros(final double minDurationMicros) {
    	this.minDurationMicros = minDurationMicros;
  	}
    
    public native void setVisibility(final boolean[] v) /*-{
      var graph = this.@org.danvk.JsArrayDygraph::dygraph;
      graph.dataView_.setVisibility(v);
  
    }-*/;
    
    public native void setSourceVec(final int[] s) /*-{
    var graph = this.@org.danvk.JsArrayDygraph::dygraph;
    graph.dataView_.setSourceVec(s);

  }-*/;
    
    public native void setRank(final int[] r) /*-{
      var graph = this.@org.danvk.JsArrayDygraph::dygraph;
      graph.dataView_.setRank(r);
    
    }-*/;

    public native void showThese() /*-{
		try {
			var graph = this.@org.danvk.JsArrayDygraph::dygraph;
			graph.drawGraph_(graph.rawData_, false);
			if (graph.attr_("visibilityCallback")) {
				graph.attr_('visibilityCallback')("multi", true);
			}
		} catch (e) {
			if (console && console.error)
				console.error(e.message);
		}
    }-*/;

    private Boolean canAutoReload = false;

    private JsArray<?> reloadData = null;

    private JsArray<?> dataArray = null;

    JavaScriptObject dygraph = null;

    private boolean hasDrawn = false;

    private Integer reloadInterval = null;

    private Timer reloadTimer = null;

    private JsArrayString labels;
    
    private double maxDurationMicros;
    
    private double minDurationMicros;

    /**
     * Default constructor.
     * 
     * @param dataArray
     *            the url of the csv data.
     * @param options
     *            the graph options.
     */
    public JsArrayDygraph(final JsArray<?> dataPoints, final JsArrayString labels,
        final DygraphOptions options) {
      super(getNextId(), options);
      this.dataArray = dataPoints;
      this.labels = labels;
            
      if (options.getHeight() != null) {
        setHeight(options.getHeight() + "px");
      }
      if (options.getWidth() != null) {
        setWidth(options.getWidth() + "px");
      }
      draw();
    }

    public final native void setZoom(final double min, final double max,
	    final boolean notify) /*-{
		//if (graph.zoomIn)
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.zoomIn(min, max, !notify);
    }-*/;

    /**
     * @return the canAutoReload
     */
    public final Boolean getCanAutoReload() {
	return canAutoReload;
    }

    /**
     * @return the csvReloadUrl
     */
    public final JsArray<?> getReloadData() {
	return reloadData;
    }

    /**
     * @return the reloadInterval
     */
    public final Integer getReloadInterval() {
	return reloadInterval;
    }

    /** {@inheritDoc} */
    @Override
    public final void onLoad() {
		if (getAutoDraw()) {
		    draw();
		}
    }

    /**
     * Native reload function.
     * 
     * @param graph
     *            the graph javascript object.
     * @param url
     *            the url of the data csv file.
     */
    public native void reload2(final JsArray<?> data) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.updateOptions({
			'file' : data
		});
    }-*/;

    /**
     * Reload the function.
     * 
     * @param url
     *            the url of the data csv file.
     */
    private void reload(final JsArray<?> data) {
	dataArray = data;
	reload2(data);
    }

    public void update(final JsArray<?> data, final double start,
	    final double end, DygraphOptions opt, boolean isMinMax) {

	updateOptions(opt, true);
	dataArray = data;
	setBounds(dygraph);
	
	redraw(dataArray, start, end, isMinMax);
    }

    private static native void setBounds(final JavaScriptObject graph) /*-{
		graph.setDataBounds();
    }-*/;

    public void updateOptions(DygraphOptions opt, boolean dontRedraw) {
	JavaScriptObject jsOptions = opt.getJsObject();
	if (dontRedraw)
	    updateOptionsNoRedraw(dygraph, jsOptions);
	else
	    updateOptions(dygraph, jsOptions);
	// draw();
    }

    private native void updateOptions(final JavaScriptObject graph,
	    final JavaScriptObject opts) /*-{
		graph.updateOptions(opts);
    }-*/;

    private native void updateOptionsNoRedraw(final JavaScriptObject graph,
	    final JavaScriptObject opts) /*-{
		graph.updateOptions(opts, true);
    }-*/;

    public JavaScriptObject getGraph() {
	return dygraph;
    }

    public native void redraw(JsArray<?> dataArray, double start, double end, boolean isMinMax) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.redraw(dataArray, start, end, isMinMax);
    }-*/;

    /** {@inheritDoc} */
    @Override
    public final void draw() {
		JavaScriptObject jsOptions = null;
		if (options != null) {
		    jsOptions = options.getJsObject();
		}
		// setLabels(jsOptions, label);
		dygraph = drawDygraphParsed(getRootDiv(), dataArray, labels, minDurationMicros, maxDurationMicros, jsOptions);
		hasDrawn = true;
		//createReloadTimer();
    }

    public native double getTraceHeight() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.getTraceHeight();
    }-*/;

    public native int getYDisplayMin() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.yAxisRange()[0];
    }-*/;

    public native int getYDisplayMax() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.yAxisRange()[1];
    }-*/;

    public native double getVisScale() /*-{
	var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.dataView_.getVisScale();
	}-*/;
    
    public native double getYScale() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.dataView_.getScaleFactor();
    }-*/;

    public void setYScale(double d) {
    	setYScale(dygraph, d);
    }

    private native final void setYScale(final JavaScriptObject graph, double d) /*-{
		try {
			graph.dataView_.setVisScale(d);
			graph.drawGraph_(graph.rawData_);
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}

    }-*/;
    
    public native void resetYScale() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.dataView_.resetScale();
    }-*/; 

    public native void setAutosizeY(boolean set) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.resizeY = set;
    }-*/;

    public native boolean getAutosizeY() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.resizeY;
    }-*/;

    public native void resize(int width, int height) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.resize(width, height);
    }-*/;

    public native void setShading(boolean setIt) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.plotter_.options.drawShadow = setIt;
    }-*/;

    public native void clearHighlightedY() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.plotter_.updateLabel("");
    }-*/;

    public native void setPlotAgainst(int[] a) /*-{
    	var graph = this.@org.danvk.JsArrayDygraph::dygraph;    
    	graph.dataView_.setPlotAgainst(a);
    }-*/;
    
    
    
    
//    public void clearRemontage(int row) {
//      setRemontage(row, -1);
//    }

//    public native void setRemontage(int row, int rowAgainst) /*-{
//		try {
//			var graph = this.@org.danvk.JsArrayDygraph::dygraph;
//			graph.dataView_.remontage(row, rowAgainst);
//			
//		} catch (err) {
//			if (console && console.error)
//				console.error("Error " + err);
//		}
//    }-*/;

    public native JsArrayInteger getRemontage() /*-{
      try {
			var graph = this.@org.danvk.JsArrayDygraph::dygraph;
			
			return graph.dataView_.plotAgainst;
      } catch (err) {
			if (console && console.error)
				console.error("Error " + err);
			var ret = [];
			return ret;
		}
    }-*/;
    
    public void setMontageAverage() {
	  setMontageAverage(true);
    }

    public void clearMontageAverage() {
      setMontageAverage(false);
    }

    public native void setMontageAverage(boolean set) /*-{
		try {
		console.debug("Setting remontage/average to " + set);
			var graph = this.@org.danvk.JsArrayDygraph::dygraph;
			graph.dataView_.montageAverage(set);
			graph.dataView_.setAverage = set;
			graph.dataView_.recenter();
			graph.drawGraph_(graph.rawData_, true);
		} catch (err) {
			if (console && console.error)
				console.error("Error " + err);
		}
    }-*/;

    public native boolean getAverageRemontage() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		console.debug("Returning remontage/average as " + graph.dataView_.setAverage);
		return graph.dataView_.setAverage;
    }-*/;
    
    public native void refresh() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.refresh();
    }-*/;

    public native int getPlotWidth() /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.getPlotWidth();
    }-*/;
    
    public native String getSelectedLabel() /*-{
    	var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		return graph.selectedLabel;
    }-*/;
    
    /**
     * Resize the graphs div.
     * 
     * @param divId
     *            the div containing the graph.
     * @param height
     *            the height to resize to.
     * @param width
     *            the width to resize to.
     */
    public static native void resizeDiv(final Element div,//String divId,
	    final int width, final int height) /*-{
//		var div = $wnd.document.getElementById(divId);
		if (div) {
			div.style.height = height + "px";
			div.style.width = width + "px";
		}
    }-*/;

	public native void resetRowScale() /*-{
		// TODO Auto-generated method stub
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.dataView_.resetScale();
	}-*/;

	public native void setDPI(double dotsPerInch) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.dataView_.setDPI(dotsPerInch);
		
	}-*/;
	
	public static JsArrayString toJsArray(List<String> input) {
		JsArrayString jsArrayString = JsArrayString.createArray().cast();
	    for (String s : input) {
	        jsArrayString.push(s);
	    }
	    return jsArrayString; 
	};

	public native void flushData() /*-{
        var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.flushData();
	}-*/;

	/**
	 * For raster plots -- indexed position and css color
	 * @param colors
	 */
	public native void setRaster(String channelName) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.setRaster(channelName);
	}-*/;

	/**
	 * For raster plots -- indexed position and css color
	 * @param colors
	 */
	public native void clearRaster(String channelName) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.clearRaster(channelName);
	}-*/;

	/**
	 * For raster plots -- indexed position and css color
	 * @param colors
	 */
	public native void setColorTable(JsArrayString colors) /*-{
		var graph = this.@org.danvk.JsArrayDygraph::dygraph;
		graph.setColorTable(colors);
	}-*/;
}
