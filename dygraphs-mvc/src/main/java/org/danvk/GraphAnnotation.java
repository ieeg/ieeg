package org.danvk;

import com.google.gwt.core.client.JavaScriptObject;

public class GraphAnnotation extends JavaScriptObject {

    /**
     * Default constructor.
     */
    protected GraphAnnotation() {
	super();
    }

    public final native String getSeries() /*-{
		var series = this["series"];
		return series;
    }-*/;

    public final native String getShortText() /*-{
		var shortText = this["shortText"];
		return shortText;
    }-*/;

    public final native String getText() /*-{
		var text = this["text"];
		return text;
    }-*/;

    /**
     * @return the x coordinate.
     */
    public final native double getStart()/*-{
		var x = parseFloat(this["xval"]);
		//		console.debug(JSON.stringify(x));
		return x && null != x ? x : -1
    }-*/;

    /**
     * @return the x coordinate.
     */
    public final native double getEnd()/*-{
		var x = parseFloat(this["x2"]);
		//		console.debug(JSON.stringify(x));
		return x && null != x ? x : -1
    }-*/;

    /**
     * @return the x coordinate.
     */
    public final native double getXVal()/*-{
		var x = parseFloat(this["xval"]);
		//		console.debug(JSON.stringify(x));
		return x && null != x ? x : -1
    }-*/;

    public final native String getRevId()/*-{
		var revId = this["revId"];
		return revId;
    }-*/;

    public final native String getInternalId()/*-{
		var intId = this["internalId"];
		return intId;
    }-*/;

}
