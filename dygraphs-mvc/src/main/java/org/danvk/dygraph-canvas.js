//Copyright 2006 Dan Vanderkam (danvdk@gmail.com)
//All Rights Reserved.

/**
 * Creates a new TsDygraphLayout object.
 * @return {Object} The TsDygraphLayout object
 */
TsDygraphLayout = function(dygraph) {
	this.dygraph_ = dygraph;
	this.annotations = new Array()
};

TsDygraphLayout.prototype.attr_ = function(name) {
	return this.dygraph_.attr_(name);
};

TsDygraphLayout.prototype.setDataView = function(dv) {
	this.dataviewer = dv;
};

TsDygraphLayout.prototype.setAnnotations = function(ann) {
	// The TsDygraph object's annotations aren't parsed. We parse them here and
	// save a copy.

	try {	
		TsDygraph.log(TsDygraph.DEBUG,"In setAnnotations");
		this.annotations = [];
		var parse = this.attr_('xValueParser');
		for (var i = 0; i < ann.length; i++) {
			var a = {};
			if (!ann[i].xval && !ann[i].x) {
				this.dygraph_.error("Annotations must have an 'x' property");
				return;
			}

			TsDygraph.update(a, ann[i]);
			if (!a.xval) a.xval = parse(a.x);
			this.annotations.push(a);
		}
	} catch (e) {
		this.dygraph_.error("Annotation parse failure: " + e.message);
	}
};

TsDygraphLayout.prototype.evaluate = function(plotter) {
	if (!this.dataviewer || !this.dataviewer.dataset)
		return;

	var predraw = (new Date()).getTime();
	this._updateScales();
	var limits = (new Date()).getTime();
	this._evaluateLineCharts(plotter);
	var linecharts = (new Date()).getTime();
	this._evaluateLineTicks();
	var lineticks = (new Date()).getTime();
	this._evaluateAnnotations(plotter);
	var annotations = (new Date()).getTime();

	TsDygraph.log(TsDygraph.TIMING,"  + EVAL Limits " + (limits - predraw) + "msec");
	TsDygraph.log(TsDygraph.TIMING,"  + EVAL Line charts " + (linecharts - limits) + "msec");
	TsDygraph.log(TsDygraph.TIMING,"  + EVAL Line ticks " + (lineticks - linecharts) + "msec");
	TsDygraph.log(TsDygraph.TIMING,"  + EVAL Annotations " + (annotations - lineticks) + "msec");
};

TsDygraphLayout.prototype._updateScales = function() {
	
	var xMinMax = this.dataviewer.getXView();
	var xrange = xMinMax[1] - xMinMax[0];
	var xscale = (xrange != 0 ? 1/xrange : 1.0);
	
	// scale Y should scale from uV/mm --> uV/pix
	
	var yrange = this.dygraph_.displayedYRange_[1] - this.dygraph_.displayedYRange_[0];
	var yscale = (yrange != 0 ? 1/(yrange): 1.0);
	this.dataviewer.setScales(xscale, yscale);
};

TsDygraphLayout.prototype._evaluateLineCharts = function(plotter) {
	
	this.points = this.dataviewer.computeWindow(
			plotter.area.x,
			plotter.area.w,
			plotter.area.h);

};

// this.xticks and this.yticks are on [0 1] scale
TsDygraphLayout.prototype._evaluateLineTicks = function() {
	this.xticks = new Array(); // ticks in canvas percentage.
	var scales = this.dataviewer.getScales();
	var xMinMax = this.dataviewer.getXView();
	
	for (var i = 0; i < this.dygraph_.attr_("xTicks").length; i++) {
		var tick = this.dygraph_.attr_("xTicks")[i];
		var label = tick.label;
		
		var pos = scales[0] * (tick.v - xMinMax[0]);
		if ((pos >= 0.0) && (pos <= 1.0)) {
			this.xticks.push([pos, label]);
		}
	}

	this.yticks = new Array();
	for (var i = 0; i < this.dygraph_.attr_("yTicks").length; i++) {
		var tick = this.dygraph_.attr_("yTicks")[i];
		var label = tick.label;
			
		var pos = scales[1] * (tick.v - this.dygraph_.displayedYRange_[0]);
		if ((pos >= 0.0) && (pos <= 1.0)) {
			this.yticks.push([pos, label]);
		}
	}
};

//Place annotations based on where the screen is
TsDygraphLayout.prototype._evaluateAnnotations = function(plotter) {
	// Add the annotations to the point to which they belong.
	// Make a map from (setName, xval) to annotation for quick lookups.

	this.annotated_points = [];

	var points = this.points;
	if (points.length == 0) {
		return;
	}
	var traceCount = this.dygraph_.getVisibleTraceCount();

	var cols = points.length / traceCount;
	var left = points[0].xval;
	var rightIdx = cols - 1;
	var right = points[rightIdx].xval;

	// Iterate over the annotations
	for (var i = 0; i < this.annotations.length; i++) {
		var a = this.annotations[i];

		// Make sure that startTime <= endTime
		if(a.x2 < a.xval){
			var tmp = a.xval;
			a.xval = a.x2;
			a.x2 = tmp;
		}

		// If any part of annotation is visible in plotted window
		if (!(a.x2 < left || a.xval > right)) {
			var xv 	= a.xval; 	// Start point of annotation
			var xv2 = a.x2; 	// End point of annotation	
			a.startEarly = false;  // Set if startTime < left of screen
			a.endLate = false;	// Set is endTime > right of screen


			if (xv < left - 1) {
				xv = left - 1;
				j = 0;
				a.startEarly = true;
			} else {
				// Find first x that is bigger than x-annotation
				var j = null;
				for (j = 0; j < cols; j++) {
					if (points[j].xval && (points[j].xval > xv))
						break;
				}
				// Round to nearests x-value
				if (j > 1 && (xv - points[j-1].xval < points[j].xval - xv))
					j--;	
			}
			
			
			

			// If also end-time, find first x that is bigger than x2-annotation
			var j2 = null;
			if (xv2){
				if (xv2 > right - 1) {
					xv2 = right - 1;
					j2 = cols -1;
					a.endLate = true; // prevent anchors from rendering
				} else {
					// Find first larger x
					for (j2 = j; j2 < cols; j2++) {
						if (points[j2].xval && (points[j2].xval > xv2))
							break;
					}
					// Round to nearests x-value
					if (j2 > 1 && (xv2 - points[j2-1].xval < points[j2].xval - xv2))
						j2--;
				}
			}

			// Init canvas coordinate arrays
			a.xStart = [];
			a.yStart = [];
			a.xEnd = [];
			a.yEnd = [];
			a.needsWedge = [];

			// Iterate over traces and create annotation start point for included traces
			for (var k = 0; k < traceCount; k++) {
				var loc = parseInt(j+k*cols);
				if (a.series.indexOf(points[loc].name) >= 0) {
					a.needsWedge.push(true);
				} else {
					a.needsWedge.push(false);
				}
				a.xStart.push(points[loc].canvasx);
				a.yStart.push(points[loc].canvasy);

				if(a.x2) {
					var loc2 = parseInt(j2+k*cols);
					a.xEnd.push(points[loc2].canvasx);
					a.yEnd.push(points[loc2].canvasy);
				} 

			}


			
			// Add annotation to to-be-rendered array if included traces are rendered.
			// This is false when traces with annotation are not visible.
			//if (a.xStart.length)
				this.annotated_points.push(a);

		}
	}
};

/**
 * Sets some PlotKit.CanvasRenderer options
 * @param {Object} element The canvas to attach to
 * @param {Layout} layout The TsDygraphLayout object for this graph.
 * @param {Object} options Options to pass on to CanvasRenderer
 */
TsDygraphCanvasRenderer = function(dygraph, element, layout, viewer) {
	this.dygraph_ = dygraph;
	this.element = element;
	this.layout = layout;
	this.dataviewer = viewer;
	this.allLabels = new Array();
	this.labelsHaveTag = false;
	
	this.colorTable = ['red', 'blue', 'darkgreen', 'brown', 'darkorchid', 'darkolivegreen', 'indigo'];
	
	this.container = this.element.parentNode;
	this.height = this.element.height;
	this.width = this.element.width;

	// --- check whether everything is ok before we return
	if (!this.isIE && !(TsDygraphCanvasRenderer.isSupported(this.element)))
		throw "Canvas is not supported.";

	// internal state
	this.xlabels = new Array();
	this.ylabels = new Array();
	this.beingSelected = new Array();
	this.annotations = new Array();

	this.area = {
			x: this.dygraph_.attr_("yAxisLabelWidth") + 2 * this.dygraph_.attr_("axisTickSize"),
			y: 0
	};
	this.area.w = this.width - this.area.x - this.dygraph_.attr_("rightGap");
	this.area.h = this.height - this.dygraph_.attr_("axisLabelFontSize") -
	2 * this.dygraph_.attr_("axisTickSize");

	this.container.style.position = "relative";
	this.container.style.width = this.width + "px";
	
	TsDygraph.log(TsDygraph.INFO,"Size setting: " + this.area.w + " : " +this.area.x)
	
};

TsDygraphCanvasRenderer.prototype.updateSize = function(width, height) {
	this.height = height;
	this.width = width;
	
	var style = "{font-size: " + this.dygraph_.attr_("axisLabelFontSize") + "px }";
	if (this.layout && this.layout.ticks)
		for (var i = 0; i < this.layout.ticks.length; i++) {
			var wide = this.layout.ticks[i].width(style);
			if (wide > this.dygraph_.attr_("yAxisLabelWidth"))
				this.dygraph_.attrs_["yAxisLabelWidth"] = wide;
		}
	this.area = {
			x: this.dygraph_.attr_("yAxisLabelWidth") + 2 * this.dygraph_.attr_("axisTickSize"),
			y: 0
	};
	this.area.w = this.width - this.area.x - this.dygraph_.attr_("rightGap");
	this.area.h = this.height - this.dygraph_.attr_("axisLabelFontSize") -
	2 * this.dygraph_.attr_("axisTickSize");
	
	this.container.style.position = "relative";
	this.container.style.width = this.width + "px";
}

TsDygraphCanvasRenderer.prototype.clear = function() {
	if (this.isIE) {
		// VML takes a while to start up, so we just poll every this.IEDelay
		try {
			if (this.clearDelay) {
				this.clearDelay.cancel();
				this.clearDelay = null;
			}
			var context = this.element.getContext("2d");
		}
		catch (e) {
			// TODO(danvk): this is broken, since MochiKit.Async is gone.
			this.clearDelay = MochiKit.Async.wait(this.IEDelay);
			this.clearDelay.addCallback(bind(this.clear, this));
			return;
		}
	}

	var context = this.element.getContext("2d");
	context.clearRect(0, 0, this.width, this.height);

	for (var i = 0; i < this.xlabels.length; i++) {
		var el = this.xlabels[i];
		el.parentNode.removeChild(el);
	}
	for (var i = 0; i < this.ylabels.length; i++) {
		var el = this.ylabels[i];
		el.parentNode.removeChild(el);
	}
	for (var i = 0; i < this.annotations.length; i++) {
		TsDygraph.log(TsDygraph.DEBUG,"Annotation  " + this.annotations[i]);
		var el = this.annotations[i];
		el.parentNode.removeChild(el);
	}
	this.xlabels = new Array();
	this.ylabels = new Array();
	this.annotations = new Array();
};


TsDygraphCanvasRenderer.isSupported = function(canvasName) {
	var canvas = null;
	try {
		if (typeof(canvasName) == 'undefined' || canvasName == null)
			canvas = document.createElement("canvas");
		else
			canvas = canvasName;
		var context = canvas.getContext("2d");
	}
	catch (e) {
		var ie = navigator.appVersion.match(/MSIE (\d\.\d)/);
		var opera = (navigator.userAgent.toLowerCase().indexOf("opera") != -1);
		if ((!ie) || (ie[1] < 6) || (opera))
			return false;
		return true;
	}
	return true;
};

TsDygraphCanvasRenderer.prototype.render = function(callback) {
	
	this.visLabels = this.dataviewer.getVisLabels();
	
	var minAxisY = this.dygraph_.displayedYRange_[0];
	var maxAxisY = this.dygraph_.displayedYRange_[1];
	
	// Draw the new X/Y grid
	var ctx = this.element.getContext("2d");
	
	if (this.dygraph_.attr_("drawXGrid")) {
		var ticks = this.layout.xticks;
		ctx.save();
		ctx.strokeStyle = this.dygraph_.attr_("gridLineColor");
		for (var i=0; i<ticks.length; i++) {
			var x = this.area.x + ticks[i][0] * this.area.w;
			var y = this.area.y + this.area.h;

			if (ticks[i].label == "")
				ctx.lineWidth = this.dygraph_.attr_("axisLineWidth") / 2;
			else
				ctx.lineWidth = this.dygraph_.attr_("axisLineWidth");
			ctx.beginPath();
			ctx.moveTo(x, y);
			ctx.lineTo(x, this.area.y);
			ctx.closePath();
			ctx.stroke();
		}
	}


	var annotated = (new Date()).getTime();
	try {
		this._renderLineChart(minAxisY);
	} catch (ann) {
		console.log(ann.message);
	}
	
	var linechart = (new Date()).getTime();
	try {
		this._renderAxis(callback);
	} catch (ann) {
		console.log(ann.message);
	}
	
	var axis = (new Date()).getTime();
	try {
		this._renderAnnotations();
	} catch (ann) {
		console.log(ann.message);
	}
	
	// Timing
	var annotations = (new Date()).getTime();
	TsDygraph.log(TsDygraph.TIMING,"  + RENDER Line chart " + (linechart - annotated) + "msec");
	TsDygraph.log(TsDygraph.TIMING,"  + RENDER Axis " + (axis - linechart) + "msec");
	TsDygraph.log(TsDygraph.TIMING,"  + RENDER Annotations " + (annotations - axis) + "msec");
};

TsDygraphCanvasRenderer.prototype.updateLabelHighlights = function(annotated) {
	for (var i = 0; i < this.layout.yticks.length; i++) {
		var name = this.layout.yticks[i][1];
		var label = this.ylabels[i];

	}
	this.beingSelected = annotated;
};

TsDygraphCanvasRenderer.prototype.updateLabel = function(n) {
	for (var i = 0; i < this.layout.yticks.length; i++) {
		var name = this.layout.yticks[i][1];
		var label = this.ylabels[i];

		if (!label)
			continue;

	}
};

TsDygraphCanvasRenderer.prototype._renderAxis = function(callback) {
	if (!this.dygraph_.attr_("drawXAxis") && !this.dygraph_.attr_("drawYAxis"))
		return;

	var context = this.element.getContext("2d");

	var labelStyle = {
			"position": "absolute",
			"fontSize": this.dygraph_.attr_("axisLabelFontSize") + "px",
			"buttonSize": "9px",
			//"zIndex": 10,
			"color": this.dygraph_.attr_("axisLabelColor"),
			"width": this.dygraph_.attr_("axisLabelWidth") + "px",
			"whiteSpace": "nowrap",
			"overflow": "hidden"
	};
	var plotter = this;
	
	var makeDiv2 = function(txt,tag){
		var tbl, r1,r2,r3;
		var div = document.createElement("div");
		for (var name in labelStyle) {
			if (labelStyle.hasOwnProperty(name)) {
				div.style[name] = labelStyle[name];
			}
		}
		
		innerDiv = document.createElement("span");
		innerDiv.style.position = "relative";
		
		innerDiv2 = document.createElement("span");
		
		var div1 = document.createElement("div");
		div1.style.position = "absolute";
		div1.style.left = 0;
		div1.style.top = 0;
		div1.style.width = "50%"
		div1.style.height = "100%"
		div1.style.cursor = "zoom-out";
		div1.style.zIndex = 10;
		var div2 = document.createElement("div");
		div2.style.position = "absolute";
		div2.style.left = "50%";
		div2.style.top = 0;
		div2.style.cursor = "zoom-in";
		div2.style.width="50%";
		div2.style.height="100%";
		div2.style.zIndex = 10;
		var div3 = document.createElement("span");
		div3.innerHTML = txt;
		div3.style.zIndex = 1;

		innerDiv.appendChild(div3);
		
		innerDiv.appendChild(div1);
		innerDiv.appendChild(div2);	
		
		div.appendChild(innerDiv);
		
		if (tag) {
			TsDygraph.addEvent(div,'dblclick',function(event) {
				callback.dClick(event, txt);
			});

			var link = document.createElement('span');
			link.setAttribute('id','remove');
			link.className = "eegviewer_channel_button";

			var img2 = document.createElement('img');
			img2.setAttribute('src','close-unsel.png');
			img2.setAttribute('title','Remove ' + txt);
			img2.setAttribute('height',labelStyle.buttonSize);
			img2.setAttribute('width',labelStyle.buttonSize);
			link.appendChild(img2);
			innerDiv2.appendChild(link);
			TsDygraph.addEvent(link,'click',function(event) {
				callback.hideTraceHandler(event, txt);
			});
			TsDygraph.addEvent(link,'mouseover',function(event) {
				img2.setAttribute('src','close-sel.png');
			});
			TsDygraph.addEvent(link,'mouseout',function(event) {
				img2.setAttribute('src','close-unsel.png');
			});
		}
		div.appendChild(innerDiv2);
			
		TsDygraph.addEvent(div1,'click',function(event) {
			callback.zoomTraceHandler(event, txt, true);
		});
		
		TsDygraph.addEvent(div2,'click',function(event) {
			callback.zoomTraceHandler(event, txt, false);
		});
		
		return div;

	}
	
	// MakeDiv is used to create X-axis labels;
	var makeDiv = function(txt, tag) {
		var div = document.createElement("div");
		for (var name in labelStyle) {
			if (labelStyle.hasOwnProperty(name)) {
				div.style[name] = labelStyle[name];
			}
		}

		var lbl = document.createTextNode(txt + " ");
		div.appendChild(lbl);
		
		return div;
	};

	
	var atualFontSize = this.dygraph_.attr_("axisLabelFontSize");
	if (this.dygraph_.attr_("axisLabelFontSize") > this.area.h / this.visLabels.length) {
		this.yaxisLargeFont = this.dygraph_.attr_("axisLabelFontSize") - 1 + "px";
		atualFontSize = (~~ (this.area.h / this.visLabels.length + 0.5 ))
		labelStyle.fontSize = atualFontSize + "px";
		labelStyle.buttonSize = (~~ (this.area.h / this.visLabels.length - 1.5 )) + "px";
	} else
		this.yaxisLargeFont = this.dygraph_.attr_("axisLabelFontSize") + 3 + "px";

	this.yaxisFontSize = labelStyle.fontSize;

	// axis lines
	context.save();
	context.strokeStyle = this.dygraph_.attr_("axisLineColor");
	context.lineWidth = this.dygraph_.attr_("axisLineWidth");
	
	if (this.dygraph_.attr_("drawYAxis")) {
		if (this.layout.yticks && this.layout.yticks.length > 0) {
			
			
			var addTag = (parseInt(this.area.h / this.visLabels.length) >= 10);
			
			var setHaveTag = false;
			for (var i = 0; i < this.layout.yticks.length; i++) {
				var tick = this.layout.yticks[i];
				if (typeof(tick) == "function") return;
				var x = this.area.x;
				var y = this.area.y + tick[0] * this.area.h;
				context.beginPath();
				context.moveTo(x, y);
				context.lineTo(x - this.dygraph_.attr_("axisTickSize"), y);
				context.closePath();
				context.stroke();

				

				var label;
				
				
				if (!this.allLabels || !this.allLabels[i] || 
						this.dataviewer.updateLabels || this.labelsHaveTag!=addTag){
					 label = makeDiv2(tick[1], addTag);
					 this.allLabels[i] = label; 
					 setHaveTag = true;
				} else {
					label = this.allLabels[i];
					label.style.fontSize = labelStyle.fontSize;
				}
				
				var top = (y - atualFontSize / 2);

				if (top + atualFontSize + 3 > this.height) {
					label.style.bottom = "0px";
				} else {
					label.style.top = top + "px";
				}
				label.style.left = "0px";
				label.style.textAlign = "right";

				label.style.color = this.assignColor();
				this.container.appendChild(label);
				this.ylabels.push(label);
			}
			
			if (setHaveTag)
				this.labelsHaveTag = addTag;


		}

		this.dataviewer.updateLabels = false;
		
		context.beginPath();
		context.moveTo(this.area.x, this.area.y);
		context.lineTo(this.area.x, this.area.y + this.area.h);
		context.closePath();
		context.stroke();
	}

	if (this.dygraph_.attr_("drawXAxis")) {
		if (this.layout.xticks) {
			for (var i = 0; i < this.layout.xticks.length; i++) {
				var tick = this.layout.xticks[i];
				if (typeof(dataset) == "function") return;

				var x = this.area.x + tick[0] * this.area.w;
				var y = this.area.y + this.area.h;
				context.beginPath();
				context.moveTo(x, y);
				context.lineTo(x, y + this.dygraph_.attr_("axisTickSize"));
				context.closePath();
				context.stroke();

				var label = makeDiv(tick[1]);
				label.style.textAlign = "left";//"center";
				label.style.fontSize = "8pt";
				label.style.bottom = "0px";

				var left = (x - this.dygraph_.attr_("axisLabelWidth")/2);
				if (left + this.dygraph_.attr_("axisLabelWidth") > this.width) {
					left = this.width - this.dygraph_.attr_("xAxisLabelWidth");
					label.style.textAlign = "right";
				}
				if (left < 0) {
					left = 0;
					label.style.textAlign = "left";
				}

				label.style.left = left + "px";
				label.style.width = this.dygraph_.attr_("xAxisLabelWidth") + "px";
				this.container.appendChild(label);
				this.xlabels.push(label);
			}
		}

		context.beginPath();
		context.moveTo(this.area.x, this.area.y + this.area.h);
		context.lineTo(this.area.x + this.area.w, this.area.y + this.area.h);
		context.closePath();
		context.stroke();
	}

	context.restore();
};

TsDygraphCanvasRenderer.prototype._createAnnDiv = function(a, annColor) {
	var bindEvt = function(eventName, classEventName, a, self) {
		return function(e) {
			if (a.hasOwnProperty(eventName)) {
				a[eventName](a,  self.dygraph_, e);
			} else if (self.dygraph_.attr_(classEventName)) {
				self.dygraph_.attr_(classEventName)(a, a, self.dygraph_,e );
			}
		};
	}
	var annotationStyle = {
			"position": "absolute",
			"fontSize": this.dygraph_.attr_("axisLabelFontSize") + "px",
			"overflow": "hidden",
			"filter"  : "alpha(opacity=50)"
	};

	var div = document.createElement("div");
	div.className = "dygraphDefaultAnnotation";
	
	for (var name in annotationStyle) {
		if (annotationStyle.hasOwnProperty(name)) {
			div.style[name] = annotationStyle[name];
		}
	}

	div.title = a.text;

	div.style.color = 'white';
	div.style.backgroundColor = annColor;
	div.style.borderColor = annColor;
	
	TsDygraph.addEvent(div, 'click',
			bindEvt('clickHandler', 'annotationClickHandler', a, this));
	TsDygraph.addEvent(div, 'dblclick',
			bindEvt('dblClickHandler', 'annotationDblClickHandler', a, this));
	TsDygraph.addEvent(div, 'mouseover',
			bindEvt('mouseOverHandler', 'annotationMouseOverHandler', a, this));
	TsDygraph.addEvent(div, 'mouseout',
			bindEvt('mouseOutHandler', 'annotationMouseOutHandler', a, this));
	

	return div;
}

TsDygraphCanvasRenderer.prototype._renderAnnotations = function() {

	var labelStep = 6; //Step in px to offset y-values of annotation labels.
	var labelStepMod = 3; //Number of Annotations after which labelStep resets.

	// Get a list of points with annotations.
	var anns = this.layout.annotated_points;
	for (var i = 0; i < anns.length; i++) {
		var a = anns[i];		
		
		// Create Label
		var div = this._createAnnDiv(a, a.color);
		var node = 	document.createTextNode(a.description);
		div.appendChild(node);
		
		// Set width and height of annotation
		var width = a.hasOwnProperty('width') ? a.width : 60;
		var height = a.hasOwnProperty('height') ? a.height : 16;
		var descriptionWidth = a.description.width();
		if (width < descriptionWidth) 
			width = descriptionWidth;
		if (a.x2 && a.x2 != a.x)
			width = a.xEnd[0] - a.xStart[0] - 1;
		else if (width > this.width)
			// If annotation has no duration but a description longer than graph width
			width = 60;
		if (width < 0)
			width = -width;

		this.container.appendChild(div);
		
		// Add div to annotations array (for deleting on update)
		this.annotations.push(div); 
		
		
		// Draw Timetrace anchors.
		for (var iCh = 0; iCh < a.xStart.length; iCh++){
			var pleft = (a.xStart[iCh]);
			div.style.left = (a.xStart[iCh]) + "px";
			var ptop = (i % labelStepMod) * labelStep;
			div.style.top = ptop + "px";
			div.style.width = width + "px";
			div.style.height = height + "px";			

			// DRAW WEDGES
			var ctx = this.element.getContext("2d");
			ctx.strokeStyle = a.color;;
			ctx.fillStyle = a.color;;
			
			// Draw Start Wedge
			if (a.needsWedge[iCh] && !a.startEarly){
				var xOffset = - 10;		
				var ptop1 = (a.yStart[iCh] - (height/2));
				
				ctx.beginPath();
				ctx.moveTo(a.xStart[iCh], ptop1);
				ctx.lineTo(a.xStart[iCh], ptop1 + height);
				ctx.lineTo(a.xStart[iCh] + xOffset, ptop1 + height / 2);
				ctx.lineTo(a.xStart[iCh], ptop1);
				ctx.closePath();
				ctx.fill();
			}
	
			// Draw End Wedge
			if (a.needsWedge[iCh] && a.x2 && !a.endLate){
				var xOffset2 = 10;
				var ptop2 = (a.yEnd[iCh] - (height/2));
				
				ctx.beginPath();
				ctx.moveTo(a.xEnd[iCh], ptop2);
				ctx.lineTo(a.xEnd[iCh], ptop2 + height);
				ctx.lineTo(a.xEnd[iCh] + xOffset2, ptop2 + height / 2);
				ctx.lineTo(a.xEnd[iCh], ptop2);
				ctx.closePath();
				ctx.fill();
			}
		}
						
		//Draw Start line
		ctx.lineWidth =1;
		if (!a.startEarly){
			var maxY = Math.max.apply(null,a.yStart);
			var index = a.yStart.indexOf(maxY);
			ctx.moveTo(a.xStart[0], a.yStart[index]);
			ctx.lineTo(a.xStart[0], labelStep *  (i % labelStepMod));
		}
		if (a.x2 && !a.endLate){ // Draw End line
			var maxY = Math.max.apply(null,a.yEnd);
			var index = a.yEnd.indexOf(maxY);
			ctx.moveTo(a.xEnd[0],a.yEnd[index]);
			ctx.lineTo(a.xEnd[0], labelStep *  (i % labelStepMod));
		}
		ctx.stroke();
	}
};

// Assign color based on option, option can be type of TimeTrace
TsDygraphCanvasRenderer.prototype.assignColor = function(option) {
	return "rgb(0,0,0)";
};

TsDygraphCanvasRenderer.prototype.assignColors = function(setNames) {
	this.colors = {}
	var setCount = setNames.length;
	for (var i = 1; i < setCount; i++) {
		this.colors[setNames[i]] = "rgb(0,0,0)";
	}
	this.setNames = setNames;
};

//TsDygraphCanvasRenderer.prototype.setVisibility = function(visibility) {
//	this.visibility = visibility;
//}

//TsDygraphCanvasRenderer.prototype.setRank = function(rank) {
//	TsDygraph.log(TsDygraph.INFO,"Setting RANK: " + rank);
//	this.rank = rank;
//}


/**
 * Overrides the CanvasRenderer method 
 */
TsDygraphCanvasRenderer.prototype._renderLineChart = function(minAxisY) {
	var ctx = this.element.getContext("2d");

	var vis = this.dataviewer.getVisibility();
		
	var minX = 1000;
	var maxX = 0;
	var minY = 1000;
	var maxY = 0;

	// create paths
	var isOK = function(x) { return (x == 0) || (x && !isNaN(x)); };

	ctx.lineWidth = this.dygraph_.attr_("strokeWidth");

	var setCount = vis.length; 
	var points = this.layout.points;

	var realIndex = [];
	var traceCount = setCount ;
	for (var i = 0; i < setCount; i++) {
		if (!vis[i]) {
			traceCount--;
		} else {
			realIndex.push(i+1);
		}
	}

	var cols = points.length / traceCount;
	var startPoint = 0;
	
	// setup graphics context
	ctx.save();

	//Adjust for for even/odd lineWidth
	//var translate = (ctx.lineWidth % 2) / 2;
    //ctx.translate(translate, 0);
    
	for (var i = 0; i < traceCount; i++) {
		TsDygraph.log(TsDygraph.INFO,"Rendering Traces -- count: "+ traceCount + "  columns:" + cols + " points: " + points.length);

//		var setName = this.setNames[realIndex[i]];
		var color = this.assignColor();

		ctx.beginPath();
		ctx.strokeStyle = color;
		
		var first = true;
		var open = false;
		
		var lastx = 0;
		for (var j = 0; j < cols; j++) {
			
			if (j + startPoint >= points.length)
				break;

			var point = points[j + startPoint];

			var cy = point.canvasy;
			var cx = point.canvasx; 
			
			if (point.raster) {
				if (point.yval != 0) {
					ctx.closePath();
					ctx.beginPath();
					ctx.strokeStyle = this.colorTable[(point.yval|0) % this.colorTable.length];
					ctx.moveTo(cx, cy - 5);
					ctx.lineTo(cx, cy + 5);
					ctx.stroke();
				}
				lastx = cx;
				continue;
			}
			
			// Make sure that we don't try to render outside canvas on top.
			// Trimmed at cy=2 so cy=1 is not visible.
			if (cy < 1)
				cy=1;
			
			if (cy < minY)
				minY = cy;
			if (cy > maxY)
				maxY = cy;
			if (cx < minX)
				minX = cx;
			if (cx > maxX)
				maxX = cx;

			if (!cy || !isOK(cy) ) {
				if (open) {
					ctx.stroke();
					open = false;
					first = true;
				} 
				
				// Draw circles if missing data
				if (cx > lastx + 15) {
					TsDygraph.log(TsDygraph.DEBUG,"Render missing data :" + j + " : " + point +" " + cy + " : " + cx + " - " + lastx);
					var possibleY = 
						this.dataviewer.getYPositionFor(realIndex[i] - 1,minAxisY,
								this.area.h,this.area.y);// + this.area.y;
					ctx.beginPath();
					ctx.arc(cx, possibleY, 2, 0, 2 * Math.PI, false);
					ctx.stroke();
					lastx = cx;
				}

			} else if (first) {
				ctx.moveTo(cx, cy);
				open = true;
				lastx = cx;
				first = false;
				
			} else {
				ctx.lineTo(cx, cy);
				lastx = cx;
				open = true;
			}

		}
		if (open) {
			ctx.stroke();
		}
		//If min/max handle the fill
		if (this.dataviewer.isMinMax()) {
			ctx.beginPath();
			ctx.fillStyle = color;
			first = true;
			for (var k = 0; k < cols; k++) {
				var point = points[startPoint + k];
				var canvasx = point.canvasx;
				var canvasyMin = point.canvasyMin;
				if (first) {
					ctx.moveTo(canvasx, canvasyMin);
					first = false;
				} else {
					ctx.lineTo(canvasx, canvasyMin);
				}
			}
			for (k = cols - 1; k >= 0; k--) {
				var point = points[startPoint + k];
				var canvasx = point.canvasx;
				var canvasyMax = point.canvasyMax;
				ctx.lineTo(canvasx, canvasyMax);
			}
			ctx.closePath();
			ctx.fill();
		}
		
		startPoint += cols;
	}

	TsDygraph.log(TsDygraph.TIMING, "  + Rendered " + (traceCount * cols) + " pixels, (" + minX + ","+ minY + ")-(" + 
			maxX + "," + maxY + ")");

	ctx.restore();
};
// Leave comment below at end so that this file show up in 
// Chrome Dev tools once it is loaded: https://developer.chrome.com/devtools/docs/javascript-debugging#breakpoints-dynamic-javascript
//# sourceURL=dygraph-canvas.js
