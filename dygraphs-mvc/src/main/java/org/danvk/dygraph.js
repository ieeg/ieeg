
//Derived from dygraphs

//Copyright 2006 Dan Vanderkam (danvdk@gmail.com)
//All Rights Reserved.

//Portions Copyright 2011-3 by Zachary G. Ives and the Trustees of the University
//of Pennsylvania.  All rights reserved.

TsDygraph = function(div, data, opts) {
	
	//From http://www.dscripts.net/2010/06/06/disable-text-selection-in-webpage-using-javascript/
	//this function disables text select on a perticular dom object
	if (typeof div.onselectstart!="undefined") //IE route
		div.onselectstart=function(){return false}
	else if (typeof div.style.MozUserSelect!="undefined") //Firefox route
		div.style.MozUserSelect="none"
			else //All other route (ie: Opera)
				div.onmousedown=function(){return false}
	div.style.cursor = "default"
		
	this.__init__(div, data, opts);
};

TsDygraph.NAME = "TsDygraph";
TsDygraph.VERSION = "1.2";
TsDygraph.__repr__ = function() {
	return "[" + this.NAME + " " + this.VERSION + "]";
};
TsDygraph.toString = function() {
	return this.__repr__();
};

//Various default values
TsDygraph.DEFAULT_ROLL_PERIOD = 1;
TsDygraph.CHANNEL_OFFSET = 1000; // space between ticks on Y-axis
TsDygraph.DEFAULT_WIDTH = 480;
TsDygraph.DEFAULT_HEIGHT = 320;
TsDygraph.AXIS_LINE_WIDTH = 0.3;

//Default attribute values.
TsDygraph.DEFAULT_ATTRS = {
		highlightCircleSize: 3,
		pixelsPerXLabel: 60,
		pixelsPerYLabel: 30,

		labelsDivWidth: 200,
		labelsDivStyles: {},
		showLabelsOnHighlight: true,

		yValueFormatter: function(x) { return TsDygraph.round_(x, 2); },

		strokeWidth: 1.0,

		drawXAxis: true,
		drawYAxis: true,
		drawYGrid: false,
		drawXGrid: true,
		axisTickSize: 3,
		axisLineColor: "black",
		axisLineWidth: 0.5,
		axisLabelFontSize: 9,
		axisLabelColor: "black",
		axisLabelFont: "Arial",
		xAxisLabelWidth: 50,
		yAxisLabelWidth: 50,
		xAxisLabelFormatter: TsDygraph.dateAxisFormatter,
		rightGap: 0,
		gridLineColor: "rgb(160,160,160)",

		xValueFormatter: TsDygraph.dateString_,
		xValueParser: TsDygraph.dateParser,
		xTicker: TsDygraph.dateTicker,
		valueScale: 1,
		isAnnotating: false,
		colorScheme: [],

};

//Set the logging level
//Dont't change this for development. Instead use the dygraphLogLevel property in ieegview.properties.
TsDygraph.LEVEL = 1;

//Various logging levels.
TsDygraph.DEBUG = 1;
TsDygraph.INFO = 2;
TsDygraph.TIMING = 3;
TsDygraph.WARNING = 4;
TsDygraph.ERROR = 5;

//A log level for dev only. Will check full data array for corruption on every modification.
TsDygraph.VALIDATE = 0;

//Used for initializing annotation CSS rules only once.
//TsDygraph.addedAnnotationCSS = false;

/**
 * Initializes the TsDygraph. This creates a new DIV and constructs the PlotKit
 * and interaction &lt;canvas&gt; inside of it. See the constructor for details
 * on the parameters.
 * @param {Element} div the Element to render the graph into.
 * @param {String | Function} file Source data
 * @param {Object} attrs Miscellaneous other options
 * @private
 */
TsDygraph.prototype.__init__ = function(div, file, attrs) {
	// Support two-argument constructor
	if (attrs == null) { attrs = {}; }

	// Copy the important bits into the object
	this.maindiv_ = div;
	this.file_ = file;
	this.previousVerticalX_ = -1;
	this.annotations_ = [];
	this.selectedLabel = '';
	this.mouseDown = false;

	// Clear the div. This ensure that, if multiple dygraphs are passed the same
	// div, then only one will be drawn.
	div.innerHTML = "";

	// If the div isn't already sized then inherit from our attrs or
	// give it a default size.
	if (div.style.width == '') {
		div.style.width = attrs.width || TsDygraph.DEFAULT_WIDTH + "px";
	}
	if (div.style.height == '') {
		div.style.height = attrs.height || TsDygraph.DEFAULT_HEIGHT + "px";
	}
	this.width_ = parseInt(div.style.width, 10);
	this.height_ = parseInt(div.style.height, 10);
	// The div might have been specified as percent of the current window size,
	// convert that to an appropriate number of pixels.
	if (div.style.width.indexOf("%") == div.style.width.length - 1) {
		this.width_ = div.offsetWidth;
	}
	if (div.style.height.indexOf("%") == div.style.height.length - 1) {
		this.height_ = div.offsetHeight;
	}

	if (this.width_ == 0) {
		this.error("dygraph has zero width. Please specify a width in pixels.");
	}
	if (this.height_ == 0) {
		this.error("dygraph has zero height. Please specify a height in pixels.");
	}

	this.rawData_ = new DDataSet([[0,0]]);
	this.dataView_ = new DDataView(this.rawData_);
	
	// Dygraphs has many options, some of which interact with one another.
	// To keep track of everything, we maintain two sets of options:
	//
	//  this.user_attrs_   only options explicitly set by the user.
	//  this.attrs_        defaults, options derived from user_attrs_, data.
	//
	// Options are then accessed this.attr_('attr'), which first looks at
	// user_attrs_ and then computed attrs_. This way Dygraphs can set intelligent
	// defaults without overriding behavior that the user specifically asks for.
	this.user_attrs_ = {};
	TsDygraph.update(this.user_attrs_, attrs);

	this.attrs_ = {};
	TsDygraph.update(this.attrs_, TsDygraph.DEFAULT_ATTRS);

	// Create the containing DIV and other interactive elements
	this.createInterface_();
	this.dataView_.setLabels(this.attr_("labels"));
	this.processData(this.parseArray_(this.file_));

	this.touch_init();
};

TsDygraph.prototype.attr_ = function(name, seriesName) {
	if (seriesName &&
			typeof(this.user_attrs_[seriesName]) != 'undefined' &&
			this.user_attrs_[seriesName] != null &&
			typeof(this.user_attrs_[seriesName][name]) != 'undefined') {
		return this.user_attrs_[seriesName][name];
	} else if (typeof(this.user_attrs_[name]) != 'undefined') {
		return this.user_attrs_[name];
	} else if (typeof(this.attrs_[name]) != 'undefined') {
		return this.attrs_[name];
	} else {
		return null;
	}
};

//TODO(danvk): any way I can get the line numbers to be this.warn call?
TsDygraph.prototype.log = function(severity, message) {
	if (console && (typeof(console) != 'undefined')) {
		if (severity < TsDygraph.LEVEL)
			return;
		switch (severity) {
		case TsDygraph.DEBUG:
			if (console.log)
				console.log('dyg-dbg: ' + message);
			else if (console.debug)
				console.debug('dyg-dbg: ' + message);
			break;
		case TsDygraph.TIMING:
			if (console.log)
				console.log('dyg-tim: ' + message);
			else if (console.debug)
				console.debug('dyg-tim: ' + message);
			break;
		case TsDygraph.INFO:
			console.info('dyg-inf: ' + message);
			break;
		case TsDygraph.WARNING:
			console.warn('dyg-wrn: ' + message);
			break;
		case TsDygraph.ERROR:
			console.error('dygraphs: ' + message);
			break;
		}
	}
};
TsDygraph.prototype.info = function(message) {
	this.log(TsDygraph.INFO, message);
};
TsDygraph.prototype.warn = function(message) {
	this.log(TsDygraph.WARNING, message);
};
TsDygraph.prototype.error = function(message) {
	this.log(TsDygraph.ERROR, message);
};
TsDygraph.log = function(severity, message) {
	if (console && (typeof(console) != 'undefined')) {
		if (severity < TsDygraph.LEVEL)
			return;
		switch (severity) {
		case TsDygraph.DEBUG:
//			return;
			if (console.log)
				console.log('dy-dbg: ' + message);
			else if (console.debug)
				console.debug('dyg-dbg: ' + message);
			break;
		case TsDygraph.INFO:
			console.info('dyg-inf: ' + message);
			break;
		case TsDygraph.TIMING:
			if (console.log)
				console.log('dyg-tim: ' + message);
			else if (console.debug)
				console.debug('dyg-tim: ' + message);
			break;
		case TsDygraph.WARNING:
			console.warn('dyg-wrn: ' + message);
			break;
		case TsDygraph.ERROR:
			console.error('dygraphs: ' + message);
			break;
		}
	}
};


/**
 * Returns the currently-visible y-range. This can be affected by zooming,
 * panning or a call to updateOptions.
 * Returns a two-element array: [bottom, top].
 */
TsDygraph.prototype.yAxisRange = function() {
	return this.displayedYRange_;
};

TsDygraph.prototype.setDataBounds = function() {
//	this.minMax = 
	return this.dataView_.getMinMaxY();
};

///**
// * Convert from data coordinates to canvas/div X/Y coordinates.
// * Returns a two-element array: [X, Y]
// */
//TsDygraph.prototype.toDomCoords = function(x, y) {
//	var ret = [null, null];
//	var area = this.plotter_.area;
//	if (x !== null) {
//		var xRange = this.dataView_.getXView();
//		ret[0] = area.x + (x - xRange[0]) / (xRange[1] - xRange[0]) * area.w;
//	}
//
//	if (y !== null) {
//		var yRange = this.yAxisRange();
//		ret[1] = area.y + (yRange[1] - y) / (yRange[1] - yRange[0]) * area.h;
//	}
//
//	return ret;
//};

TsDygraph.prototype.getPlotWidth = function() {
	return this.plotter_.area.w;
}

TsDygraph.prototype.getTraceHeight = function() {
	var count = 0;
	for (var v = this.dataView_.labels.length - 1; v >= 0; v--)
		if (this.dataView_.labels[v])
			count++;

	return this.plotter_.area.h / count;
};

//TODO(danvk): use these functions throughout dygraphs.
/**
 * Convert from canvas/div coords to data coordinates.
 * Returns a two-element array: [X, Y]
 */
TsDygraph.prototype.toDataCoords = function(x, y) {
	var ret = [null, null];
	var area = this.plotter_.area;
	if (x !== null) {
		var xRange = this.dataView_.getXView();
		ret[0] = xRange[0] + (x - area.x) / area.w * (xRange[1] - xRange[0]);
	}

	if (y !== null) {
		var yRange = this.yAxisRange();
		ret[1] = yRange[0] + (area.h - y) / area.h * (yRange[1] - yRange[0]);
	}

	return ret;
};

/**
 * Returns the number of columns (including the independent variable).
 */
TsDygraph.prototype.numColumns = function() {
	return this.rawData_.getNumXValues();//[0].length;
};

/**
 * Returns the number of rows (excluding any header/label row).
 */
TsDygraph.prototype.numRows = function() {
	TsDygraph.log(TsDygraph.DEBUG,"GETNUMROWS:" + this.dataView_.getNrTraces());
	return this.dataView_.getNrTraces();//.length;
};

///**
// * Returns the value in the given row and column. If the row and column exceed
// * the bounds on the data, returns null. Also returns null if the value is
// * missing.
// */
//TsDygraph.prototype.getValue = function(row, col) {
//	if (row < 0 || row > this.numRows()) return null;
//	if (col < 0 || col > this.rawData_.getNumXValues()) return null;
//	
//	var dataIndex = this.sourceVec[row] + 1;
//
//	return this.rawData_.data[dataIndex][col]  * (this.dataView_.invertSignal ? -1 : 1);
//};

TsDygraph.addEvent = function(el, evt, fn) {
	var normed_fn = function(e) {
		if (!e) var e = window.event;
		fn(e);
	};
	if (window.addEventListener) {  // Mozilla, Netscape, Firefox
		if (evt == 'mousewheel')
			el.addEventListener('DOMMouseScroll', normed_fn, false);
		el.addEventListener(evt, normed_fn, false);
	} else {  // IE
		el.attachEvent('on' + evt, normed_fn);
	}
};

TsDygraph.clipCanvas_ = function(cnv, clip) {
	var ctx = cnv.getContext("2d");
	ctx.beginPath();
	ctx.rect(clip.left, clip.top + 2, clip.width, clip.height-2);
	ctx.clip();
};

/**
 * Generates interface elements for the TsDygraph: a containing div, a div to
 * display the current point, and a textbox to adjust the rolling average
 * period. Also creates the Renderer/Layout elements.
 * @private
 */
TsDygraph.prototype.createInterface_ = function() {
	// Create the all-enclosing graph div

	if (this.width_ < 0)
		this.width_ = -this.width_;

	var enclosing = this.maindiv_;

	this.graphDiv = document.createElement("div");
	this.graphDiv.style.width = this.width_ + "px";
	this.graphDiv.style.height = this.height_ + "px";
	enclosing.appendChild(this.graphDiv);

	var clip = {
			top: 0,
			left: this.attr_("yAxisLabelWidth") + 2 * this.attr_("axisTickSize")
	};
	clip.width = this.width_ - clip.left - this.attr_("rightGap");
	clip.height = this.height_ - this.attr_("axisLabelFontSize") - 2 * this.attr_("axisTickSize");
	this.clippingArea_ = clip;

	// Create the canvas for interactive parts of the chart.
	this.canvas_ = TsDygraph.createCanvas();
	this.canvas_.style.position = "absolute";
	this.canvas_.width = this.width_;
	this.canvas_.height = this.height_;
	this.canvas_.style.left = "0px";				  // For FF
	this.canvas_.style.width = this.width_ + "px";    // for IE
	this.canvas_.style.height = this.height_ + "px";  // for IE
	this.canvas_.style.position = "absolute";

	// ... and for static parts of the chart.
	this.hidden_ = this.createPlotKitCanvas_(this.canvas_);

	// The interactive parts of the graph are drawn on top of the chart.
	this.graphDiv.appendChild(this.hidden_);
	this.graphDiv.appendChild(this.canvas_);
	this.mouseEventElement_ = this.canvas_;

	// Make sure we don't overdraw.
	TsDygraph.clipCanvas_(this.hidden_, this.clippingArea_);
	TsDygraph.clipCanvas_(this.canvas_, this.clippingArea_);

	var dygraph = this;
	TsDygraph.addEvent(this.mouseEventElement_, 'mousemove', function(e) {
		dygraph.mouseMove_(e);
	});
	TsDygraph.addEvent(this.mouseEventElement_, 'mouseout', function(e) {
		dygraph.mouseOut_(e);
	});
	TsDygraph.addEvent(this.mouseEventElement_, 'mousewheel',  _.throttle(function(e) {
		dygraph.mouseWheelZoom_(e);
	}, 100));
	//The unthrottled mouseWheelGain_ cancels the default browser mousewheel response for all events. Which is good.
	TsDygraph.addEvent(this.mouseEventElement_, 'mousewheel',  function(e) {
		dygraph.mouseWheelGain_(e);
	});

	this.layout_ = new TsDygraphLayout(this);
	this.layout_.setDataView(this.dataView_);

	
	this.plotter_ = new TsDygraphCanvasRenderer(this,
			this.hidden_, this.layout_,
			this.dataView_);

	this.plotter_.updateSize(this.width_, this.height_);
	this.createStatusMessage_();
	this.createDragInterface_();
};

///**
// * Detach DOM elements in the dygraph and null out all data references.
// * Calling this when you're done with a dygraph can dramatically reduce memory
// * usage. See, e.g., the tests/perf.html example.
// */
//TsDygraph.prototype.destroy = function() {
//	var removeRecursive = function(node) {
//		while (node.hasChildNodes()) {
//			removeRecursive(node.firstChild);
//			node.removeChild(node.firstChild);
//		}
//	};
//	removeRecursive(this.maindiv_);
//
//	var nullOut = function(obj) {
//		for (var n in obj) {
//			if (typeof(obj[n]) === 'object') {
//				obj[n] = null;
//			}
//		}
//	};
//
//	// These may not all be necessary, but it can't hurt...
//	nullOut(this.layout_);
//	nullOut(this.plotter_);
//	nullOut(this);
//};

/**
 * Creates the canvas containing the PlotKit graph. Only plotkit ever draws on
 * this particular canvas. All TsDygraph work is done on this.canvas_.
 * @param {Object} canvas The TsDygraph canvas over which to overlay the plot
 * @return {Object} The newly-created canvas
 * @private
 */
TsDygraph.prototype.createPlotKitCanvas_ = function(canvas) {
	var h = TsDygraph.createCanvas();
	h.style.position = "absolute";
	h.style.top = canvas.style.top;
	h.style.left = canvas.style.left;
	h.width = this.width_;
	h.height = this.height_;
	h.style.width = this.width_ + "px";    // for IE
	h.style.height = this.height_ + "px";  // for IE
	return h;
};

//Taken from MochiKit.Color
TsDygraph.hsvToRGB = function (hue, saturation, value) {
	var red;
	var green;
	var blue;
	if (saturation === 0) {
		red = value;
		green = value;
		blue = value;
	} else {
		var i = Math.floor(hue * 6);
		var f = (hue * 6) - i;
		var p = value * (1 - saturation);
		var q = value * (1 - (saturation * f));
		var t = value * (1 - (saturation * (1 - f)));
		switch (i) {
		case 1: red = q; green = value; blue = p; break;
		case 2: red = p; green = value; blue = t; break;
		case 3: red = p; green = q; blue = value; break;
		case 4: red = t; green = p; blue = value; break;
		case 5: red = value; green = p; blue = q; break;
		case 6: // fall through
		case 0: red = value; green = t; blue = p; break;
		}
	}
	red = Math.floor(255 * red + 0.5);
	green = Math.floor(255 * green + 0.5);
	blue = Math.floor(255 * blue + 0.5);
	return 'rgb(' + red + ',' + green + ',' + blue + ')';
};


/**
 * Generate a set of distinct colors for the data series. This is done with a
 * color wheel. Saturation/Value are customizable, and the hue is
 * equally-spaced around the color wheel. If a custom set of colors is
 * specified, that is used instead.
 * @private
 */
TsDygraph.prototype.initColors_ = function() {
	var num = this.dataView_.getLabels().length - 1;
	this.colors_ = [];
	var colors = this.attr_('colors');
	if (!colors) {
		var sat = this.attr_('colorSaturation') || 1.0;
		var val = this.attr_('colorValue') || 0.5;
		var half = Math.ceil(num / 2);
		for (var i = 1; i <= num; i++) {
			// alternate colors for high contrast.
			var idx = i % 2 ? Math.ceil(i / 2) : (half + i / 2);
			var hue = (1.0 * idx/ (1 + num));
			this.attr_("colorScheme").push(TsDygraph.hsvToRGB(hue, sat, val));
		}
	} else {
		for (var i = 0; i < num; i++) {
			var colorStr = colors[i % colors.length];
			this.attr_("colorScheme").push(colorStr);
		}
	}
}

TsDygraph.prototype.setColors_ = function(colors) {
	this.colors_ = colors;
	this.plotter_.assignColors(this.dataView_.getLabels());
}

TsDygraph.prototype.setMinDurationMicros = function(minDurationMicros) {
	this.minDurationMicros_ = minDurationMicros;
}

TsDygraph.prototype.setMaxDurationMicros = function(maxDurationMicros) {
	this.maxDurationMicros_ = maxDurationMicros;
}

/**
 * Return the list of colors. This is either the list of colors passed in the
 * attributes, or the autogenerated list of rgb(r,g,b) strings.
 * @return {Array<string>} The list of colors.
 */
TsDygraph.prototype.getColors = function() {
	return this.colors_;
};

//The following functions are from quirksmode.org with a modification for Safari from
//http://blog.firetree.net/2005/07/04/javascript-find-position/
//http://www.quirksmode.org/js/findpos.html
TsDygraph.findPosX = function(obj) {
	var curleft = 0;
	if(obj.offsetParent)
		while(1)
		{
			curleft += obj.offsetLeft;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	else if(obj.x)
		curleft += obj.x;
	return curleft;
};

TsDygraph.findPosY = function(obj) {
	var curtop = 0;
	if(obj.offsetParent)
		while(1)
		{
			curtop += obj.offsetTop;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	else if(obj.y)
		curtop += obj.y;
	return curtop;
};



/**
 * Create the div that contains information on the selected point(s)
 * This goes in the top right of the canvas, unless an external div has already
 * been specified.
 * @private
 */
TsDygraph.prototype.createStatusMessage_ = function() {
	var userLabelsDiv = this.user_attrs_["labelsDiv"];
	if (userLabelsDiv && null != userLabelsDiv
			&& (typeof(userLabelsDiv) == "string" || userLabelsDiv instanceof String)) {
		this.user_attrs_["labelsDiv"] = document.getElementById(userLabelsDiv);
	}
	if (!this.attr_("labelsDiv")) {
		var divWidth = this.attr_('labelsDivWidth');
		var messagestyle = {
				"position": "absolute",
				"fontSize": "8pt",//14px",
				"zIndex": 10,
				"width": divWidth + "px",
				"top": "0px",
				"left": (this.width_ - divWidth - 2) + "px",
				"background": "white",
				"textAlign": "left",
				"overflow": "hidden",
				"opacity":0.4,
				"text-align": "right"};
		TsDygraph.update(messagestyle, this.attr_('labelsDivStyles'));
		var div = document.createElement("div");
		for (var name in messagestyle) {
			if (messagestyle.hasOwnProperty(name)) {
				div.style[name] = messagestyle[name];
			}
		}
		this.graphDiv.appendChild(div);
		this.attrs_.labelsDiv = div;
	}
};

//These functions are taken from MochiKit.Signal
TsDygraph.pageX = function(e) {
	if (e.pageX) {
		return (!e.pageX || e.pageX < 0) ? 0 : e.pageX;
	} else {
		var de = document;
		var b = document.body;
		return e.clientX +
		(de.scrollLeft || b.scrollLeft) -
		(de.clientLeft || 0);
	}
};

TsDygraph.pageY = function(e) {
	if (e.pageY) {
		return (!e.pageY || e.pageY < 0) ? 0 : e.pageY;
	} else {
		var de = document;
		var b = document.body;
		return e.clientY +
		(de.scrollTop || b.scrollTop) -
		(de.clientTop || 0);
	}
};

TsDygraph.prototype.zoomIn = function(minDate, maxDate, nocallback) {
//	var oldZoomWidth = 0;
//	var range = this.dataView_.getXView(); 
//
//	oldZoomWidth = range[1] - range[0];

	TsDygraph.log(TsDygraph.TIMING,">> Zoom in -- redraw");
	if (this.maxDurationMicros_ && (maxDate - minDate) > this.maxDurationMicros_) {
		var change = ((maxDate - minDate) - this.maxDurationMicros_)/2;
		minDate += change;
		maxDate -= change;
	}
	if (this.minDurationMicros_ && (maxDate - minDate) < this.minDurationMicros_) {
		var change = (this.minDurationMicros_ - (maxDate - minDate))/2;
		minDate -= change;
		maxDate += change;
		if (minDate < 0) {
			minDate = 0;
			maxDate = this.minDurationMicros_;
		}
	}
	if ((maxDate-minDate)<=0){
		return;
	}
	
	this.dataView_.setXView(minDate, maxDate);
	this.drawGraph_(this.rawData_, true);
	this.checkDetailAgainstZoom(minDate, maxDate);
	if (!nocallback) {

		if (this.attr_("zoomCallback")) {
			this.attr_("zoomCallback")(minDate, maxDate, -1);
		} 
	}
};

TsDygraph.prototype.dClick = function(event,lbl) {
	var pts = [];
	var point = {
			canvasx: 0,
			canvasy: 0,
			xval: 0,
			yval: 0,
			name: lbl
	};
	pts.push(point);

	this.attr_('doubleClickCallback')(event, 0, pts);
};

/**
 * Set up all the mouse handlers needed to capture dragging behavior for zoom
 * events.
 * @private
 */
TsDygraph.prototype.createDragInterface_ = function() {
	var self = this;

	// Tracks whether the mouse is down right now
	var isZooming = false;
	var isPanning = false;
	var dragStartX = null;
	var dragStartY = null;
	var dragEndX = null;
	var dragEndY = null;
	var prevEndX = null;
	var prevEndY = null;
	var annEndX = null;
	var annEndY = null;
	var draggingDate = null;
	var dateRange = null;

	// Utility function to convert page-wide coordinates to canvas coords
	var px = 0;
	var py = 0;
	var getX = function(e) { return TsDygraph.pageX(e) - px };
	var getY = function(e) { return TsDygraph.pageY(e) - py };

	var getXDate = function(e, range) { return (getX(e) - self.clippingArea_.left) / self.clippingArea_.width * (range[1] - range[0]) + range[0]};

	// Draw zoom rectangles when the mouse is down and the user moves around
	TsDygraph.addEvent(this.mouseEventElement_, 'mousemove', function(event) {
		if (isZooming) {
			TsDygraph.log(TsDygraph.DEBUG,"<zoom>");
			dragEndX = getX(event);
			dragEndY = getY(event);

			self.drawZoomRect_(dragStartX, dragStartY, dragEndX, dragEndY, prevEndX, prevEndY);
			prevEndX = dragEndX;
			prevEndY = dragEndY;
		} else if (self.isAnnotating) {
			TsDygraph.log(TsDygraph.DEBUG,"<ann>");
			var range = self.dataView_.getXView();
			annEndX = getXDate(event, range);//getX(event) / self.width_ * (range[1] - range[0]) + range[0];
			annEndY = getY(event);

			self.drawAnnRect_(self.annStartX, annEndX, self.annStartY, annEndY, self.prevAnnEndX, self.prevAnnEndY);
			self.prevAnnEndX = annEndX;
			self.prevAnnEndY = annEndY;
		} else if (isPanning) {
			if (self.isAnnotating) {
				TsDygraph.log(TsDygraph.DEBUG,"<pan but should be ann>");
				isPanning = false;
				return;
			}
			TsDygraph.log(TsDygraph.DEBUG,"<pan>");

			dragEndX = getX(event);
			dragEndY = getY(event);

			TsDygraph.log(TsDygraph.DEBUG,"Pan -- redraw");

			// Want to have it so that:
			// 1. draggingDate appears at dragEndX
			// 2. daterange = (dateWindow_[1] - dateWindow_[0]) is unaltered.

			// TODO(zives): adjust this
			var left = draggingDate - ((dragEndX - self.clippingArea_.left) / self.clippingArea_.width) * dateRange;
			var period = self.dataView_.getPeriod();
			self.setPosition(left,
					left + dateRange, period);

			self.drawGraph_(self.rawData_, true);
		}
	});

	// Track the beginning of drag events
	TsDygraph.addEvent(this.mouseEventElement_, 'mousedown', function(event) {
		if (self.isAnnotating) {
			return;
		}
		TsDygraph.log(TsDygraph.DEBUG,"<down>");
		
		self.mouseDown = true;

		px = TsDygraph.findPosX(self.canvas_);
		py = TsDygraph.findPosY(self.canvas_);
		dragStartX = getX(event);
		dragStartY = getY(event);

		if (!(event.altKey || event.shiftKey)) {

			isPanning = true;
			var range = self.dataView_.getXView();
			dateRange = range[1] - range[0];
			draggingDate = ((dragStartX - self.clippingArea_.left) / self.clippingArea_.width) * dateRange +
			range[0];
		} else {
			isZooming = true;
		}
	});

	// If the user releases the mouse button during a drag, but not over the
	// canvas, then it doesn't count as a zooming action. But we do complete any dragging action.
	TsDygraph.addEvent(document, 'mouseup', function(event) {
		
		TsDygraph.log(TsDygraph.DEBUG,"<up>");
		self.mouseDown = false;
		
		if (isZooming || isPanning) {
			isZooming = false;
			dragStartX = null;
			dragStartY = null;
		}

		if (isPanning) {
			TsDygraph.log(TsDygraph.DEBUG,"<up-pan>");

			isPanning = false;
			draggingDate = null;
			dateRange = null;

			dragEndX = getX(event);
			dragEndY = getY(event);

			if (dragEndX != dragStartX) {
				TsDygraph.log(TsDygraph.DEBUG,"Requesting...");
				self.requestDataFromServer();
			}
			if (self.attr_('clickCallback') != null) {
				self.attr_('clickCallback')(event, self.lastx_, self.selPoints_);
			}
		}
	});

	// Temporarily cancel the dragging event when the mouse leaves the graph
	TsDygraph.addEvent(this.mouseEventElement_, 'mouseout', function(event) {
		TsDygraph.log(TsDygraph.DEBUG,"<out-of-bounds>");
		if (isZooming) {
			dragEndX = null;
			dragEndY = null;
		}
	});

	// If the mouse is released on the canvas during a drag event, then it's a
	// zoom. Only do the zoom if it's over a large enough area (>= 10 pixels)
	TsDygraph.addEvent(this.mouseEventElement_, 'mouseup', function(event) {
		if (isZooming) {
			TsDygraph.log(TsDygraph.DEBUG,"<up-zoom>");
			isZooming = false;
			dragEndX = getX(event);
			dragEndY = getY(event);
			var regionWidth = Math.abs(dragEndX - dragStartX);
			var regionHeight = Math.abs(dragEndY - dragStartY);

			if (regionWidth < 2 && regionHeight < 2 &&
					self.lastx_ != undefined && self.lastx_ != -1) {
				// TODO(danvk): pass along more info about the points, e.g. 'x'
				if (self.attr_('clickCallback') != null) {
					self.attr_('clickCallback')(event, self.lastx_, self.selPoints_);
				}
				if (self.attr_('pointClickCallback')) {
					// check if the click was on a particular point.
					var closestIdx = -1;
					var closestDistance = 0;
					for (var i = 0; i < self.selPoints_.length; i++) {
						var p = self.selPoints_[i];
						var distance = Math.pow(p.canvasx - dragEndX, 2) +
						Math.pow(p.canvasy - dragEndY, 2);
						if (closestIdx == -1 || distance < closestDistance) {
							closestDistance = distance;
							closestIdx = i;
						}
					}

					// Allow any click within two pixels of the dot.
					var radius = self.attr_('highlightCircleSize') + 2;
					if (closestDistance <= 5 * 5) {
						self.attr_('pointClickCallback')(event, self.selPoints_[closestIdx]);
					}
				}
			}

			if (regionWidth >= 10) {
				self.doZoom_(Math.min(dragStartX, dragEndX),
						Math.max(dragStartX, dragEndX), Math.min(dragStartY, dragEndY),
						Math.max(dragStartY, dragEndY));
			} else {
				self.canvas_.getContext("2d").clearRect(0, 0,
						self.canvas_.width,
						self.canvas_.height);
			}

			dragStartX = null;
			dragStartY = null;
		}

	});

	// Double-clicking zooms back out
	TsDygraph.addEvent(this.mouseEventElement_, 'dblclick', function(event) {


		TsDygraph.log(TsDygraph.DEBUG,"<dclick>");
		var range = self.dataView_.getXView();
		if (!self.isAnnotating) {
			try {
				TsDygraph.log(TsDygraph.DEBUG,"Range " + JSON.stringify(range) + " and width " + self.clippingArea_.width);
			} catch (e) {

			}
			self.annStartX = getXDate(event, range);
			self.annStartY = getY(event);
			self.isAnnotating = true;
			TsDygraph.log(TsDygraph.DEBUG,"Starting annotation at " + self.annStartX + " / " + self.getCoordFromDate(self.annStartX));
		} else {
			self.isAnnotating = false;
			if (self.prevAnnEndX) {
				var ctx = self.canvas_.getContext("2d");
				ctx.clearRect(Math.min(self.getCoordFromDate(self.annStartX), self.getCoordFromDate(self.prevAnnEndX)), Math.min(self.annStartY, self.prevAnnEndY),//0,
						Math.abs(self.getCoordFromDate(self.annStartX) - self.getCoordFromDate(self.prevAnnEndX)), Math.abs(self.annStartY - self.prevAnnEndY));//this.height_);
			}
			annEndX = getXDate(event, range);
			annEndY = getY(event);
			TsDygraph.log(TsDygraph.DEBUG,"Annotation complete at " + annEndX + " / " + self.getCoordFromDate(annEndX));

			if (self.attr_("doubleClickCallback")) {
				var pts;

				if (self.attr_("selectAll")) {
					pts = self.selPoints_;
				} else {
					// check if the click was on a particular point.
					var closestIdx = -1;
					var closestDistance = 0;
					var endX = getX(event);

					pts = [];
					for (var i = 0; i < self.selPoints_.length; i++) {
						var p = self.selPoints_[i];

						if (p.canvasy >= Math.min(self.annStartY,annEndY) && p.canvasy <= Math.max(self.annStartY, annEndY))
							pts.push(self.selPoints_[i]);

					}
				}

				self.plotter_.beingSelected = [];
				self.attr_('doubleClickCallback')(event, parseFloat(self.annStartX), parseFloat(annEndX)//self.lastx_
						, pts);
			}
		}
	});
};

TsDygraph.prototype.requestDataFromServer = function() {
	var range = this.dataView_.getXView();
	var minX = parseFloat(range[0]);
	var maxX = parseFloat(range[1]);

	TsDygraph.log(TsDygraph.DEBUG,"<fetch>");
	// Old value range
	var oldMin = parseFloat(this.rawData_.getMinX());
	var oldMax = parseFloat(this.rawData_.getMaxX());

	if ((minX < oldMin || maxX > oldMax) && this.attr_("outOfDataCallback")) {
		this.attr_('outOfDataCallback')(minX, maxX, oldMin, oldMax);

	}
};

TsDygraph.prototype.setPosition = function(minX,maxX,period) {
	var gap = maxX - minX;
	//var sd = parseFloat(this.attr_("startDate"));
	var sd = parseFloat(this.attr_("studyStartTime"));

	TsDygraph.log(TsDygraph.DEBUG,"<move>");
//	TsDygraph.log(TsDygraph.DEBUG,"** Moving to position " + minX + " - " + maxX);
	if (!sd)
		sd = 0;
	if (minX < sd) {
//		TsDygraph.log(TsDygraph.DEBUG,"Exceeded start date (" + minX + "," + sd + ")");
		minX = sd;
		maxX = sd + gap;
//		TsDygraph.log(TsDygraph.DEBUG,"Recalibrating position (" + minX + "," + maxX + ")");
	}
	//var ed = parseFloat(this.attr_("endDate"));
	var ed = parseFloat(this.attr_("studyEndTime"));
	if ((ed && maxX > ed)) {
//		TsDygraph.log(TsDygraph.DEBUG,"Exceeded end date (" + maxX + "," + ed + ")");
		maxX = ed;
		minX = maxX - gap;
	}
//	TsDygraph.log(TsDygraph.DEBUG,"Computing axis range");
	var range = this.dataView_.getXView();
	if (minX != range[0] || maxX != range[1]) {

		this.posPeriod = period;

//		TsDygraph.log(TsDygraph.DEBUG,"Requesting view change");
//		TsDygraph.log(TsDygraph.INFO,"Set position");
		this.dataView_.setXView(minX, maxX);
//		TsDygraph.log(TsDygraph.DEBUG,"Requesting move callback " + minX + " / " + maxX + " / " + period);
//		TsDygraph.log(TsDygraph.DEBUG,"Setting position (" + minX + "," + maxX + ") -- " + (maxX - minX));
		if (this.attr_("moveCallback")) {
			this.attr_('moveCallback')(minX, maxX, period);
		}
	}
};

TsDygraph.prototype.addNewData = function(data, incremental, nodraw, isMinMax) {

	TsDygraph.log(TsDygraph.DEBUG,"IN ADDNEWDATA: incremental:" + incremental + " : nodraw:" + nodraw);
	var lb = data[0][0];
	var ub = data[data.length-1][0];

	var min = this.rawData_.getMinX();
	var max = this.rawData_.getMaxX();

	TsDygraph.log(TsDygraph.DEBUG,"<newdata>");
	var numNewXValues = isMinMax ? data.length / 2 : data.length;
	var newPeriod = (ub - lb) / numNewXValues;
	var period = this.dataView_.getPeriod();
	if ((lb >= min - period && lb <= max + period) || (ub >= min - period && ub <= max + period)) {
		incremental = true;
		if (newPeriod <  0.9 * period || newPeriod > 1.1 * period)
			incremental = false;
		TsDygraph.log(TsDygraph.DEBUG,"Incremental input: (" + lb + "," + ub + ")");
	} else {
		TsDygraph.log(TsDygraph.DEBUG,"Input: (" + lb + "," + ub + ")");
	}

	this.dataWasLoaded = true;

	// Replace the dataset if it isn't a region that subsumes or somehow overlaps
	if (!incremental) {
		this.rawData_.setValues(data, isMinMax);
		this.dataView_.reprocessData();
	} else {
		this.dataView_.addNewDataColumns(data, isMinMax);
	}
	if (!nodraw)
		TsDygraph.log(TsDygraph.TIMING,"<-- New data -- redraw -->");

	var xMinMax = this.dataView_.getXView();
	var width = xMinMax[1] - xMinMax[0];
	var min = this.rawData_.getMinX();
	if (xMinMax[0] < min) {
		TsDygraph.log(TsDygraph.DEBUG, "Out of range: jump to " + min);
		this.setPosition(min, min+width, period);
	} else if (incremental) {
		if (!nodraw) {
			this.drawGraph_(this.rawData_, true);
		}
	} else if (!nodraw) {
		this.drawGraph_(this.rawData_, false);
	}

};

/**
 * Draw a gray zoom rectangle over the desired area of the canvas. Also clears
 * up any previous zoom rectangles that were drawn. This could be optimized to
 * avoid extra redrawing, but it's tricky to avoid interactions with the status
 * dots.
 * @param {Number} startX The X position where the drag started, in canvas
 * coordinates.
 * @param {Number} endX The current X position of the drag, in canvas coords.
 * @param {Number} prevEndX The value of endX on the previous call to this
 * function. Used to avoid excess redrawing
 * @private
 */
TsDygraph.prototype.drawZoomRect_ = function(startX, startY, endX, endY, prevEndX, prevEndY) {
	var ctx = this.canvas_.getContext("2d");

	// Clean up from the previous rect if necessary
	if (prevEndX) {
		ctx.clearRect(Math.min(startX, prevEndX), Math.min(startY, prevEndY),
				Math.abs(startX - prevEndX), Math.abs(prevEndY - startY));//this.height_);
	}

	// Draw a light-grey rectangle to show the new viewing area
	if (endX && startX) {
		ctx.fillStyle = "rgba(128,128,128,0.33)";
		ctx.fillRect(Math.min(startX, endX), Math.min(startY, endY),
				Math.abs(endX - startX), Math.abs(startY - endY));//this.height_);
	}
};

/**
 * Utility function: convert x-coordinate on the screen (from getX) to
 * an actual date
 * 
 * @param xval Coordinate relative to canvas, from getX
 */
TsDygraph.prototype.getCoordFromDate = function(xval) {
	var xRange = this.dataView_.getXView();

	if (xval < xRange[0])
		return 0;
	else if (xval > xRange[1])
		return this.width_;
	else
		return (xval - xRange[0]) / (xRange[1] - xRange[0]) * this.clippingArea_.width + this.clippingArea_.left;
};

/**
 * Draws a cyan rectangle representing the annotation window
 */
TsDygraph.prototype.drawAnnRect_ = function(startX, endX, startY, endY, 
		prevEndX, prevEndY) {
	var ctx = this.canvas_.getContext("2d");

	var startPt = this.getCoordFromDate(startX);

	var endPt = this.getCoordFromDate(endX);

	// Clean up from the previous rect if necessary
	if (prevEndX) {
		var prevPt = this.getCoordFromDate(prevEndX);

		ctx.clearRect(Math.min(startPt, prevPt), Math.min(startY, prevEndY),
				Math.abs(startPt - prevPt), Math.abs(startY - prevEndY));//this.height_);
	}

	// Draw a light-grey rectangle to show the new viewing area
	if (endX && startX) {
		ctx.globalAlpha = 0.73;
		ctx.fillStyle = "rgba(128,128,128,0.25)";
		ctx.fillRect(Math.min(startPt, endPt), Math.min(startY, endY),//0,
				Math.abs(endPt - startPt), Math.abs(startY - endY));//this.height_);
	}

	var pts = [];
	for (var i = 0; i < this.selPoints_.length; i++) {
		var p = this.selPoints_[i];

		if (p.canvasy >= Math.min(startY,endY) && p.canvasy <= Math.max(startY, endY)) {
			pts[p.name] = true;
		}
	}
	this.plotter_.updateLabelHighlights(pts);

};


/**
 * Zoom to something containing [lowX, highX]. These are pixel coordinates
 * in the canvas. The exact zoom window may be slightly larger if there are no
 * data points near lowX or highX. This function redraws the graph.
 * @param {Number} lowX The leftmost pixel value that should be visible.
 * @param {Number} highX The rightmost pixel value that should be visible.
 * @private
 */
TsDygraph.prototype.doZoom_ = function(lowX, highX, lowY, highY) {
	// Find the earliest and latest dates contained in this canvasx range.
	var range = this.dataView_.getXView();
	var r = this.toDataCoords(lowX, null);
	var minDate = r[0];
	r = this.toDataCoords(highX, null);
	var maxDate = r[0];

	var oldMin = range[0];
	var oldMax = range[1];
	var oldPer = this.dataView_.getPeriod();

	this.dataView_.setXView(minDate, maxDate);
	
	TsDygraph.log(TsDygraph.TIMING,">> Zoom in -- redraw");
	this.drawGraph_(this.rawData_, false);

	this.checkDetailAgainstZoom(minDate, maxDate);

	var start = 100000;
	var startI = 1;
	var end = -1;
	var endI = -1;
	var found = [];

	// TODO: compare rectangle versus the positions of the captions
	for (var i = 0; i < this.layout_.yticks.length; i++) {
		var h = this.layout_.yticks[i][0] * this.plotter_.area.h; 
		if (h >= lowY && h < start) {
			start = h;
			startI = i;
		}
		if (h <= highY && h > end) {
			end = h;
			endI = i;
		}
	}

	if (this.attr_("zoomSelCallback")) {
		this.attr_("zoomSelCallback")(minDate, maxDate, startI, endI, oldMin, oldMax, oldPer);
	} else  if (this.attr_("zoomCallback")) {
		this.attr_("zoomCallback")(minDate, maxDate);
	}
};

TsDygraph.prototype.checkDetailAgainstZoom = function(minDate, maxDate) {

	TsDygraph.log(TsDygraph.DEBUG,"<checkbounds>");

	var samples = (maxDate - minDate) / this.approxPeriodOfRange(minDate, maxDate);
	if (samples > this.plotter_.area.w * 1.1 ||
			samples < this.plotter_.area.w * 0.9) {
		this.attr_('outOfDataCallback')(parseFloat(minDate), parseFloat(maxDate), 0.0, 0.0);
	}
};

//Approximately the period in microseconds of the data on hand for the given range
TsDygraph.prototype.approxPeriodOfRange = function(minDate, maxDate) {
	var rightIdx = this.rawData_.indexOf(maxDate);
	if (rightIdx == -1) {
		return 0;
	} else if (rightIdx < 0) {
		rightIdx = -rightIdx - 2;
	}
	
	var leftIdx = this.rawData_.indexOf(minDate);
	if (leftIdx == -1) {
		leftIdx = 0;
	} else if (leftIdx < 0) {
		leftIdx = -leftIdx - 2;
	}
	var samplesOnHand = this.rawData_.isMinMax ?  (rightIdx - leftIdx + 1) / 2 : (rightIdx - leftIdx + 1);
	return (this.rawData_.data[rightIdx][0] - this.rawData_.data[leftIdx][0]) / samplesOnHand;
};

///**
// * Updates the scale factor on the data
// */
//TsDygraph.prototype.rescale = function(scale) {
//	TsDygraph.log(TsDygraph.DEBUG,"<rescale>");
//	var labels = this.dataView_.getLabels();
//	for (var v = 1; v < labels.length; v++) {
//		this.dataView_.setRowScale(v-1, scale);
//	}
//	TsDygraph.log(TsDygraph.TIMING,">> Rescale -- redraw");
//	this.drawGraph_(this.rawData_, false);
//}
TsDygraph.prototype.zoomTraceHandler = function(event, name, zoomIn) {
	TsDygraph.log(TsDygraph.DEBUG,"ZOOM EVENT : "+zoomIn);
	
	var zoomRow = this.findIndexFromLabel(name);
	var oldScale = this.dataView_.getRowScale(zoomRow);
	var newScale;
	if(zoomIn){
		newScale = oldScale - oldScale*0.1;
	} else {
		newScale = oldScale + oldScale*0.1;
	}
	
	this.dataView_.setRowScale(zoomRow, newScale); 
	this.drawGraph_(this.rawData_, false);
}

TsDygraph.prototype.findIndexFromLabel = function(name) {
	// Parse Label name
	
	var labels = this.dataView_.getLabels();
	var sepName = name.split(" vs ");
	var sourceName = sepName[0];
	var refName = "";
	if (sepName.length > 1){
		refName = sepName[1];
	}
		
	TsDygraph.log(TsDygraph.DEBUG,"HIDETRACEHANDLER: " + name + " from: " + labels);
	
	// Find SourceName and RefName index in Labels (will always be there)
	var sIdx;
	var rIdx = refName=="" ? -1:null;
	var i = 1;
	while (i<labels.length && (null == sIdx || null == rIdx)){
		if(labels[i] == sourceName){
			sIdx = i-1;
		}
		if(labels[i] == refName){
			rIdx = i-1;
		}
		i++;
	}	
	
	// Find combination in SourceVec and PlotAgainst that matches.
	var index;
	var i = 0;
	while (i<this.dataView_.sourceVec.length && null == index){
		if (this.dataView_.sourceVec[i] == sIdx && (this.dataView_.plotAgainst[i] == rIdx || this.dataView_.plotAgainst[i] == this.dataView_.sourceVec[i])){
			index = i;
		}
		i++;
	}
	
	return index;
}


TsDygraph.prototype.hideTraceHandler = function(event, name) {
	
	// Set RowBaselineUpdate refresh
	this.dataView_.doBaselineUpdate = true;
	this.dataView_.updateLabels = true;
	
	var removeRow = this.findIndexFromLabel(name);
	
//	// Parse Label name
//	var labels = this.dataView_.getLabels();
//	var sepName = name.split(" ");
//	var sourceName = sepName[0];
//	var refName = "";
//	if (sepName.length > 1){
//		refName = sepName[2];
//	}
//		
//	TsDygraph.log(TsDygraph.DEBUG,"HIDETRACEHANDLER: " + name + " from: " + labels);
//	
//	// Find SourceName and RefName index in Labels (will always be there)
//	var sIdx;
//	var rIdx = refName=="" ? -1:null;
//	var i = 1;
//	while (i<labels.length && (null == sIdx || null == rIdx)){
//		if(labels[i] == sourceName){
//			sIdx = i-1;
//		}
//		if(labels[i] == refName){
//			rIdx = i-1;
//		}
//		i++;
//	}	
//	
//	// Find combination in SourceVec and PlotAgainst that matches.
//	var removeRow;
//	var i = 0;
//	while (i<this.dataView_.sourceVec.length && null == removeRow){
//		if (this.dataView_.sourceVec[i] == sIdx && (this.dataView_.plotAgainst[i] == rIdx || this.dataView_.plotAgainst[i] == this.dataView_.sourceVec[i])){
//			removeRow = i;
//		}
//		i++;
//	}
	
	// Set Trace to invisible and redraw
	if (null!=removeRow){
		this.dataView_.setRowVisibility(removeRow, false);
		this.drawGraph_(this.rawData_, false);
	} else {
		TsDygraph.log(TsDygraph.ERROR,"HIDETRACEHANDLER: Can't find trace to delete: " + name + " from: " + labels);
	}

	this.dataView_.updateLabels = true;
	
}

//TsDygraph.prototype.vetHandler = function(event,name,flag) {
//	if (this.attr_("vetCallback")) {
//		this.attr_('vetCallback')(name, flag);
//	}
//}

//TsDygraph.prototype.showTraceHandler = function(event,name) {
//	var v = 1;
//	var labels = this.dataView_.getLabels();
//	var sepName = name.split(" ");
//	sepName = sepName[0];
//	
//	while (labels[v] != sepName && v < labels.length)
//		v++;
//	if (v < labels.length) {
////		TsDygraph.log(TsDygraph.DEBUG,"Show " + name + ": " + v);
//		this.dataView_.setRowVisibility(v-1, true);
//		TsDygraph.log(TsDygraph.DEBUG,"Show trace -- redraw");
//		this.drawGraph_(this.rawData_, false);
//		if (this.attr_("visibilityCallback")) {
//			this.attr_('visibilityCallback')(name, true);
//		}
//	}
//}

/**
 * When the mouse moves in the canvas, display information about a nearby data
 * point and draw dots over those points in the data series. This function
 * takes care of cleanup of previously-drawn dots.
 * @param {Object} event The mousemove event from the browser.
 * @private
 */
TsDygraph.prototype.mouseMove_ = function(event) {
	var canvasx = TsDygraph.pageX(event) - TsDygraph.findPosX(this.mouseEventElement_);
	var points = this.layout_.points;
	var px = 0;
	var py = 0;
	var getX = function(e) { return TsDygraph.pageX(e) - px };
	var getY = function(e) { return TsDygraph.pageY(e) - py };

	if (!points || points.length == 0)
		return;

	var lastx = -1;
	var lasty = -1;

	// Loop through all the points and find the date nearest to our current
	// location.
	var minDist = 1e+100;
	var minDistIdx = -1;
	for (var i = 0; i < points.length; i++) {
		var dist = Math.abs(points[i].canvasx - canvasx);
		if (dist > minDist) continue;
		minDist = dist;
		minDistIdx = i;
	}
	if (minDistIdx >= 0) lastx = points[minDistIdx].xval;

	// Check that you can really highlight the last day's data
	var traceCount = this.getVisibleTraceCount();
	
	if (traceCount < 1)
		return;
	
	var cols = points.length / traceCount;
	var maxCanvasxIdx = cols - 1;
	if (canvasx > points[maxCanvasxIdx].canvasx)
		lastx = points[maxCanvasxIdx].xval;

	// Extract the points we've selected
	this.selPoints_ = [];
	var l = points.length;
	
	// Need to 'unstack' points starting from the bottom
	for (var i = l - 1; i >= 0; i--) {
		if (points[i].xval == lastx) {
			var p = {};  // Clone the point since we modify it
			for (var k in points[i]) {
				p[k] = points[i][k];
			}
			this.selPoints_.push(p);
		}
	}
	this.selPoints_.reverse();
	
	// Save last x position for callbacks.
	this.lastx_ = lastx;

	px = TsDygraph.findPosX(this.canvas_);
	py = TsDygraph.findPosY(this.canvas_);

	var endX = getX(event);
	var endY = getY(event);

	// check for the nearest point.
	var closestIdx = -1;
	var closestDistance = 0;

	for (var i = 0; i < this.selPoints_.length; i++) {
		var p = this.selPoints_[i];
		var distance = Math.pow(p.canvasx - endX, 2) +
		Math.pow(p.canvasy - endY, 2);
		if (closestIdx == -1 || distance < closestDistance) {
			closestDistance = distance;
			closestIdx = i;
		}
	}

	// Allow any click within two pixels of the dot.
	var radius = this.attr_('highlightCircleSize') + 2;
	var highlight = "";
	if (closestDistance <= 5 * 5) {
		var pointName = this.selPoints_[closestIdx].name;
//		TsDygraph.log(TsDygraph.DEBUG,"POINT: " + pointName + " - " + this.selPoints_[closestIdx].name);
		
		var c = new RGBColor("black");
		var value = (this.dataView_.isMinMax() ? "avg: " : "") + (this.selPoints_[closestIdx].yval).toFixed(1) +" uV  "; 
		highlight = " <font color='" + c.toHex() + "'>"  + value + "</font>";

		this.selectedLabel = pointName;
		this.plotter_.updateLabel(pointName);
	} else
		this.selectedLabel = '';

	this.updateSelection_(highlight);
};

TsDygraph.prototype.scaleUp = function(newScale) {
	
	this.dataView_.setVisScale(newScale );
	this.drawGraph_(this.rawData_, true);
	
	// Call Java side callback
	if (this.attr_("zoomCallback")) {
		TsDygraph.log(TsDygraph.DEBUG,"WHEELEVENT: " + newScale);
		this.attr_("zoomCallback")(null,null,newScale);
	} 
	
};

TsDygraph.prototype.mouseWheelGain_ = function(event) {
	event = event ? event : window.event;

	if (event.shiftKey == false && event.ctrlKey == false){
		var wheelData = -(event.detail? event.detail * -1 : event.wheelDelta / 120);
		if (wheelData === 0) {
			return this.cancelEvent(event);
		}
		wheelData = wheelData > 0 ? 1 : -1;
		TsDygraph.log(TsDygraph.DEBUG,"WHEELEVENT(Gain): " + wheelData);
		
		var scale = this.dataView_.getVisScale();
		var newScale = scale + scale * 0.25 * wheelData;
		
		if (newScale < 0){
			return;
		}
		
		this.scaleUp(newScale);
	}
	return this.cancelEvent(event);
};

TsDygraph.prototype.mouseWheelZoom_ = function(event) {
	event = event ? event : window.event;
	
	if (event.shiftKey == true || event.ctrlKey == true){
		var wheelData = -(event.detail? event.detail * -1 : event.wheelDelta / 120);
		if (wheelData === 0) {
			return this.cancelEvent(event);
		}
		wheelData = wheelData > 0 ? 1 : -1;
		TsDygraph.log(TsDygraph.DEBUG,"WHEELEVENT(Zoom): " + wheelData);
		var oldXMinMax = this.dataView_.getXView();
		var oldRange = oldXMinMax[1] - oldXMinMax[0];
		var newRange = oldRange * (1 + 0.2 * wheelData);
		var change = (newRange - oldRange)/2;
		var newXMax = oldXMinMax[1] + change;
		var newXMin = oldXMinMax[0] - change;
		if (newXMin < 0) {
			newXMin = 0;
			newXMax = newRange;
		}
		TsDygraph.log(TsDygraph.DEBUG, "Zooming to [" + newXMin + ", " + newXMax + "]: " + (newXMax - newXMin)/1e6 + " seconds");
		this.zoomIn(newXMin, newXMax, false);
		
	}
	return this.cancelEvent(event);
};

//Based on http://www.switchonthecode.com/tutorials/javascript-tutorial-the-scroll-wheel
TsDygraph.prototype.cancelEvent = function(e) {
	e = e ? e : window.event;
	if(e.stopPropagation)
		e.stopPropagation();
	if(e.preventDefault)
		e.preventDefault();
	e.cancelBubble = true;
	e.cancel = true;
	e.returnValue = false;

	return false;
};

/**
 * Draw dots over the selected points in the data series. This function
 * takes care of cleanup of previously-drawn dots.
 * @private
 */
TsDygraph.prototype.updateSelection_ = function(highlightLabel) {
	// Clear the previously drawn vertical, if there is one
	var ctx = this.canvas_.getContext("2d");
	if (this.previousVerticalX_ >= 0) {
		// Determine the maximum highlight circle size.
		var maxCircleSize = 0;
		var labels = this.attr_('labels');
		for (var i = 1; i < labels.length; i++) {
			var r = this.attr_('highlightCircleSize', labels[i]);
			if (r > maxCircleSize) maxCircleSize = r;
		}
		var px = this.previousVerticalX_;
		ctx.clearRect(px - maxCircleSize - 1, 0,
				2 * maxCircleSize + 2, this.height_);
	}

	var isOK = function(x) { return x && !isNaN(x); };

	if (this.selPoints_.length > 0) {
		var canvasx = this.selPoints_[0].canvasx;

		// Set the status message to indicate the selected point(s)
		var replace = this.attr_('xValueFormatter')(this.lastx_, this);// + ":";
		var clen = this.colors_.length;

		if (this.attr_('showLabelsOnHighlight')) {
			this.attr_("labelsDiv").innerHTML = replace + "&nbsp;&nbsp;&nbsp;"+  highlightLabel + "&nbsp;";
		}
		
		var divWidth = this.attr_('labelsDivWidth');
		this.attr_("labelsDiv").style.left = (this.width_ - divWidth - 1) + "px",

		// Draw colored circles over the center of each selected point
		ctx.save();
		for (var i = 0; i < this.selPoints_.length; i++) {
			if (isOK(this.selPoints_[i].canvasy)) {
				var circleSize =
					this.attr_('highlightCircleSize', this.selPoints_[i].name);
				ctx.beginPath();
				ctx.fillStyle = this.plotter_.colors[this.selPoints_[i].name];
				ctx.arc(canvasx, this.selPoints_[i].canvasy, circleSize,
						0, 2 * Math.PI, false);
				ctx.fill();
			}
		}

		ctx.restore();

		this.previousVerticalX_ = canvasx;
	}
};

/**
 * The mouse has left the canvas. Clear out whatever artifacts remain
 * @param {Object} event the mouseout event from the browser.
 * @private
 */
TsDygraph.prototype.mouseOut_ = function(event) {
	if (this.attr_("unhighlightCallback")) {
		this.attr_("unhighlightCallback")(event);
	}

	this.clearSelection();

};

/**
 * Remove all selection from the canvas
 * @public
 */
TsDygraph.prototype.clearSelection = function() {
	// Get rid of the overlay data
	var ctx = this.canvas_.getContext("2d");
	ctx.clearRect(0, 0, this.width_, this.height_);
	this.attr_("labelsDiv").innerHTML = "";
	this.selPoints_ = [];
	this.lastx_ = -1;
}

TsDygraph.zeropad = function(x) {
	if (x < 10) return "0" + x; else return "" + x;
}

/**
 * Return a string version of the hours, minutes and seconds portion of a date.
 * @param {Number} date The JavaScript date (ms since epoch)
 * @return {String} A time of the form "HH:MM:SS"
 * @private
 */
TsDygraph.hmsString_ = function(date) {
	var zeropad = TsDygraph.zeropad;
	var d = new Date(date);
	if (d.getSeconds()) {
		return zeropad(d.getHours()) + ":" +
		zeropad(d.getMinutes()) + ":" +
		zeropad(d.getSeconds());
	} else {
		return zeropad(d.getHours()) + ":" + zeropad(d.getMinutes());
	}
}

/**
 * Convert a JS date to a string appropriate to display on an axis that
 * is displaying values at the stated granularity.
 * @param {Date} date The date to format
 * @param {Number} granularity One of the TsDygraph granularity constants
 * @return {String} The formatted date
 * @private
 */
TsDygraph.dateAxisFormatter = function(date, granularity) {
	if (granularity >= TsDygraph.MONTHLY) {
		return date.strftime('%b %y');
	} else {
		var frac = date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds() + date.getMilliseconds();
		if (frac == 0 || granularity >= TsDygraph.DAILY) {
			return new Date(date.getTime() + 3600*1000).strftime('%d%b');
		} else {
			return TsDygraph.hmsString_(date.getTime());
		}
	}
}

/**
 * Convert a JS date (millis since epoch) to YYYY/MM/DD
 * @param {Number} date The JavaScript date (ms since epoch)
 * @return {String} A date of the form "YYYY/MM/DD"
 * @private
 */
TsDygraph.dateString_ = function(date, self) {
	var zeropad = TsDygraph.zeropad;

	var days = parseInt(date / (1E6 * 3600 * 24));

	var hms = date - days * (1E6 * 3600 * 24);

	var hours = zeropad(parseInt(hms / 1E6 / 3600));

	hms = hms - hours * (1E6 * 3600);

	var minutes = zeropad(parseInt(hms / (1E6 * 60)));

	hms = hms - minutes * (1E6 * 60);

	var seconds = zeropad(parseInt(hms / (1E6)));//.toFixed(0));

	var ret = " " + hours + ":" + minutes + ":" + seconds;

	var dayNo = days + 1;
	if (dayNo > 1)
		return "Day " + dayNo + ret;
	else
		return ret;
};

/**
 * Round a number to the specified number of digits past the decimal point.
 * @param {Number} num The number to round
 * @param {Number} places The number of decimals to which to round
 * @return {Number} The rounded number
 * @private
 */
TsDygraph.round_ = function(num, places) {
	var shift = Math.pow(10, places);
	return Math.round(num * shift)/shift;
};

///**
// * Fires when there's data available to be graphed.
// * @param {String} data Raw CSV data to be plotted
// * @private
// */
//TsDygraph.prototype.loadedEvent_ = function(data) {
//	this.processData(this.parseCSV_(data));
//	TsDygraph.log(TsDygraph.TIMING,">> Data loaded event -- redraw");
//	this.drawGraph_(this.rawData_);
//};

TsDygraph.prototype.processData = function(data,isMinMax) {
	this.rawData_.setValues(data,isMinMax);
	this.dataView_.reprocessData();
	if (!this.colors_)
		this.initColors_();
	this.setColors_(this.colors_);
	this.attrs_['pointSize'] = 0.5 * this.attr_('highlightCircleSize');
}

TsDygraph.prototype.months =  ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                             "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
TsDygraph.prototype.quarters = ["Jan", "Apr", "Jul", "Oct"];

/**
 * Add ticks on the x-axis representing years, months, quarters, weeks, or days
 * @private
 */
TsDygraph.prototype.addXTicks_ = function() {
	// Determine the correct ticks scale on the x-axis: quarterly, monthly, ...
	var range = this.dataView_.getXView();
	startDate = range[0];
	endDate = range[1];

	var xTicks = this.attr_('xTicker')(startDate, endDate, this, this.attr_('xAxisLabelFormatter'));
	this.attrs_['xTicks'] = xTicks;
};

//Time granularity enumeration
TsDygraph.SECONDLY = 0;
TsDygraph.TWO_SECONDLY = 1;
TsDygraph.FIVE_SECONDLY = 2;
TsDygraph.TEN_SECONDLY = 3;
TsDygraph.THIRTY_SECONDLY  = 4;
TsDygraph.MINUTELY = 5;
TsDygraph.TWO_MINUTELY = 6;
TsDygraph.FIVE_MINUTELY = 7;
TsDygraph.TEN_MINUTELY = 8;
TsDygraph.THIRTY_MINUTELY = 9;
TsDygraph.HOURLY = 10;
TsDygraph.TWO_HOURLY = 11;
TsDygraph.SIX_HOURLY = 12;
TsDygraph.DAILY = 13;
TsDygraph.WEEKLY = 14;
TsDygraph.MONTHLY = 15;
TsDygraph.QUARTERLY = 16;
TsDygraph.BIANNUAL = 17;
TsDygraph.ANNUAL = 18;
TsDygraph.DECADAL = 19;
TsDygraph.NUM_GRANULARITIES = 20;

TsDygraph.SHORT_SPACINGS = [];
TsDygraph.SHORT_SPACINGS[TsDygraph.SECONDLY]        = 1000 * 1;
TsDygraph.SHORT_SPACINGS[TsDygraph.TWO_SECONDLY]    = 1000 * 2;
TsDygraph.SHORT_SPACINGS[TsDygraph.FIVE_SECONDLY]   = 1000 * 5;
TsDygraph.SHORT_SPACINGS[TsDygraph.TEN_SECONDLY]    = 1000 * 10;
TsDygraph.SHORT_SPACINGS[TsDygraph.THIRTY_SECONDLY] = 1000 * 30;
TsDygraph.SHORT_SPACINGS[TsDygraph.MINUTELY]        = 1000 * 60;
TsDygraph.SHORT_SPACINGS[TsDygraph.TWO_MINUTELY]    = 1000 * 60 * 2;
TsDygraph.SHORT_SPACINGS[TsDygraph.FIVE_MINUTELY]   = 1000 * 60 * 5;
TsDygraph.SHORT_SPACINGS[TsDygraph.TEN_MINUTELY]    = 1000 * 60 * 10;
TsDygraph.SHORT_SPACINGS[TsDygraph.THIRTY_MINUTELY] = 1000 * 60 * 30;
TsDygraph.SHORT_SPACINGS[TsDygraph.HOURLY]          = 1000 * 3600;
TsDygraph.SHORT_SPACINGS[TsDygraph.TWO_HOURLY]      = 1000 * 3600 * 2;
TsDygraph.SHORT_SPACINGS[TsDygraph.SIX_HOURLY]      = 1000 * 3600 * 6;
TsDygraph.SHORT_SPACINGS[TsDygraph.DAILY]           = 1000 * 86400;
TsDygraph.SHORT_SPACINGS[TsDygraph.WEEKLY]          = 1000 * 604800;


//If we used this time granularity, how many ticks would there be?
//This is only an approximation, but it's generally good enough.
TsDygraph.prototype.NumXTicks = function(start_time, end_time, granularity) {
	if (granularity < TsDygraph.MONTHLY) {
		// Generate one tick mark for every fixed interval of time.
		var spacing = TsDygraph.SHORT_SPACINGS[granularity];
		return Math.floor(0.5 + 1.0 * (end_time - start_time) / spacing);
	} else {
		var year_mod = 1;  // e.g. to only print one point every 10 years.
		var num_months = 12;
		if (granularity == TsDygraph.QUARTERLY) num_months = 3;
		if (granularity == TsDygraph.BIANNUAL) num_months = 2;
		if (granularity == TsDygraph.ANNUAL) num_months = 1;
		if (granularity == TsDygraph.DECADAL) { num_months = 1; year_mod = 10; }

		var msInYear = 365.2524 * 24 * 3600 * 1000;
		var num_years = 1.0 * (end_time - start_time) / msInYear;
		return Math.floor(0.5 + 1.0 * num_years * num_months / year_mod);
	}
};

//GetXAxis()

//Construct an x-axis of nicely-formatted times on meaningful boundaries
//(e.g. 'Jan 09' rather than 'Jan 22, 2009').

//Returns an array containing {v: millis, label: label} dictionaries.

TsDygraph.prototype.GetXAxis = function(start_time, end_time, granularity) {
	var formatter = this.attr_("xAxisLabelFormatter");
	var ticks = [];
	if (granularity < TsDygraph.MONTHLY) {
		// Generate one tick mark for every fixed interval of time.
		// TODO(zives): generalize this again
		var spacing = TsDygraph.SHORT_SPACINGS[granularity];
		var format = '%d%b';  // e.g. "1Jan"

		// Find a time less than start_time which occurs on a "nice" time boundary
		// for this granularity.
		var g = spacing / 1000;
		var d = new Date(start_time);
		if (g <= 60) {  // seconds
			var x = d.getSeconds(); d.setSeconds(x - x % g);
		} else {
			d.setSeconds(0);
			g /= 60;
			if (g <= 60) {  // minutes
				var x = d.getMinutes(); d.setMinutes(x - x % g);
			} else {
				d.setMinutes(0);
				g /= 60;

				if (g <= 24) {  // days
					var x = d.getHours(); d.setHours(x - x % g);
				} else {
					d.setHours(0);
					g /= 24;

					if (g == 7) {  // one week
						d.setDate(d.getDate() - d.getDay());
					}
				}
			}
		}
		start_time = d.getTime();

		for (var t = start_time; t <= end_time; t += spacing) {
			ticks.push({ v:t, label: formatter(new Date(t), granularity) });
		}
	} else {
		// Display a tick mark on the first of a set of months of each year.
		// Years get a tick mark iff y % year_mod == 0. This is useful for
		// displaying a tick mark once every 10 years, say, on long time scales.
		var months;
		var year_mod = 1;  // e.g. to only print one point every 10 years.

		if (granularity == TsDygraph.MONTHLY) {
			months = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ];
		} else if (granularity == TsDygraph.QUARTERLY) {
			months = [ 0, 3, 6, 9 ];
		} else if (granularity == TsDygraph.BIANNUAL) {
			months = [ 0, 6 ];
		} else if (granularity == TsDygraph.ANNUAL) {
			months = [ 0 ];
		} else if (granularity == TsDygraph.DECADAL) {
			months = [ 0 ];
			year_mod = 10;
		}

		var start_year = new Date(start_time).getFullYear();
		var end_year   = new Date(end_time).getFullYear();
		var zeropad = TsDygraph.zeropad;
		for (var i = start_year; i <= end_year; i++) {
			if (i % year_mod != 0) continue;
			for (var j = 0; j < months.length; j++) {
				var date_str = i + "/" + zeropad(1 + months[j]) + "/01";
				var t = Date.parse(date_str);
				if (t < start_time || t > end_time) continue;
				ticks.push({ v:t, label: formatter(new Date(t), granularity) });
			}
		}
	}

	return ticks;
};


/**
 * Add ticks to the x-axis based on a date range.
 * @param {Number} startDate Start of the date window (millis since epoch)
 * @param {Number} endDate End of the date window (millis since epoch)
 * @return {Array.<Object>} Array of {label, value} tuples.
 * @public
 */
TsDygraph.dateTicker = function(startDate, endDate, self, ignored) {
	var chosen = -1;
	for (var i = 0; i < TsDygraph.NUM_GRANULARITIES; i++) {
		var num_ticks = self.NumXTicks(startDate, endDate, i);
		if (self.width_ / num_ticks >= self.attr_('pixelsPerXLabel')) {
			chosen = i;
			break;
		}
	}

	if (chosen >= 0) {
		return self.GetXAxis(startDate, endDate, chosen);
	} else {
		// TODO(danvk): signal error.
	}
};

/**
 * Add ticks when the x axis has numbers on it (instead of dates)
 * @param {Number} startDate Start of the date window (millis since epoch)
 * @param {Number} endDate End of the date window (millis since epoch)
 * @param self
 * @param {function} formatter: Optional formatter to use for each tick value
 * @return {Array.<Object>} Array of {label, value} tuples.
 * @public
 */
TsDygraph.numericTicks = function(minV, maxV, self, formatter) {
	var ticks = [];

	var tickV = parseInt((minV / 1E6)) * 1E6;

	var durationSeconds = (maxV - minV) / 1E6;
	var numTicks = Math.ceil(durationSeconds);
	var plotWidthPx = self.getPlotWidth();
	
	var labelInterval = Math.round(durationSeconds / 8);
	var increment = 1E6;
	if (numTicks > plotWidthPx) {
		increment = Math.round((maxV - minV)/plotWidthPx);
		labelInterval = Math.round(plotWidthPx/8);
	}
	
	
	
	if (labelInterval < 1)
		labelInterval = 1;
	
	
	var count = 0;
	for (; tickV < maxV; tickV += increment) {
		var label = TsDygraph.dateString_(tickV);// + "." + dec;

		count++;

		if (count % labelInterval == 0)
			ticks.push( {label: label, v: tickV} );
		else
			ticks.push( {label: "", v: tickV} );
	}
	TsDygraph.log(TsDygraph.DEBUG,"Created " + ticks.length + " X ticks");
	return ticks;
};

/**
 * Adds appropriate ticks on the y-axis
 * @param {Number} minY The minimum Y value in the data set
 * @param {Number} maxY The maximum Y value in the data set
 * @private
 */
TsDygraph.prototype.addYTicks_ = function(minY, maxY) {
	// Set the number of ticks so that the labels are human-friendly.
//	var formatter = this.attr_('yAxisLabelFormatter') ? this.attr_('yAxisLabelFormatter') : this.attr_('yValueFormatter');

	var ticks;
	ticks = this.dataView_.getRowTicks();
	this.attrs_['yTicks'] = ticks;

};

/**
 * Update the graph with new data. Data is in the format
 * [ [date1, val1, val2, ...], [date2, val1, val2, ...] 
 * @param {Array.<Object>} data The data (see above)
 * @private
 */
TsDygraph.prototype.drawGraph_ = function(data, dontAdjust) {

	TsDygraph.log(TsDygraph.DEBUG,"Drawing..." + this.mouseDown);
	
	// WorkAround, (ugly) don't draw if you bounds are not set
	if (!this.dataView_.getMaxBound()){
		TsDygraph.log(TsDygraph.WARN, "TsDygraph.drawGraph called without bounds ---> not drawing");
		return;
	}
	
	var start = (new Date()).getTime();

	try {
		if (this.dataView_.doRecenter && this.mouseDown==false){
			this.dataView_.recenter();  // resetting means of all traces
		}
		
		if (this.dataView_.doBaselineUpdate){
			this.dataView_.updateRowBaseline(); // resetting order of channels
		}
		
		var recenterEnd = (new Date()).getTime();

		var maxAxisY = this.dataView_.getMaxBound();
		var minAxisY = this.dataView_.getMinBound();

		this.addYTicks_(minAxisY, maxAxisY);
		this.displayedYRange_ = [minAxisY, maxAxisY];

		TsDygraph.log(TsDygraph.INFO, "DisplayedRange: " + this.displayedYRange_);

		var xBounds = this.dataView_.getXView();
		TsDygraph.log(TsDygraph.INFO, "Setting new bounds" + xBounds);
	
		// Draw X-axis
		this.addXTicks_();
		
		this.layout_.evaluate(this.plotter_);

		var evaluateEnd = (new Date()).getTime(); //timing

		this.plotter_.clear();
		this.plotter_.render(this);
		
		var renderEnd = (new Date()).getTime(); //timing

		//draw Rectangle during annotating
		if (this.isAnnotating) {
			var px = TsDygraph.findPosX(this.canvas_);
			var py = TsDygraph.findPosY(this.canvas_);
			var getX = function(e) { return TsDygraph.pageX(e) - px };
			var getY = function(e) { return TsDygraph.pageY(e) - py };

			var range = this.dataView_.getXView();
			var annEndX = getX(event) / self.width_ * (range[1] - range[0]) + range[0];
			var annEndY = getY(event);

			this.drawAnnRect_(this.annStartX, annEndX, this.annStartY, annEndY, this.prevAnnEndX, this.prevAnnEndY);
			this.prevAnnEndX = annEndX;
			this.prevAnnEndY = annEndY;
		}
		var end = (new Date()).getTime(); //timing
	} catch (e) {
		TsDygraph.log(TsDygraph.ERROR,e.message);
	}
	TsDygraph.log(TsDygraph.TIMING,"drawGraph took " + (end - start) + "msec (" + (end - evaluateEnd)+ " in render loop");
	TsDygraph.log(TsDygraph.TIMING," -  took " + (recenterEnd - start) + "msec in recenter");
	TsDygraph.log(TsDygraph.TIMING," -  took " + (evaluateEnd - recenterEnd) + "msec in evaluate");
	TsDygraph.log(TsDygraph.TIMING," -  took " + (renderEnd - evaluateEnd) + "msec in render");
};

/**
 * Parses a date, returning the number of milliseconds since epoch. This can be
 * passed in as an xValueParser in the TsDygraph constructor.
 * TODO(danvk): enumerate formats that this understands.
 * @param {String} A date in YYYYMMDD format.
 * @return {Number} Milliseconds since epoch.
 * @public
 */
TsDygraph.dateParser = function(dateStr, self) {
	var dateStrSlashed;
	var d;
	if (dateStr.search("-") != -1) {  // e.g. '2009-7-12' or '2009-07-12'
		dateStrSlashed = dateStr.replace("-", "/", "g");
		while (dateStrSlashed.search("-") != -1) {
			dateStrSlashed = dateStrSlashed.replace("-", "/");
		}
		d = Date.parse(dateStrSlashed);
	} else if (dateStr.length == 8) {  // e.g. '20090712'
		// TODO(danvk): remove support for this format. It's confusing.
		dateStrSlashed = dateStr.substr(0,4) + "/" + dateStr.substr(4,2)
		+ "/" + dateStr.substr(6,2);
		d = Date.parse(dateStrSlashed);
	} else {
		// Any format that Date.parse will accept, e.g. "2009/07/12" or
		// "2009/07/12 12:34:56"
		d = Date.parse(dateStr);
	}

	if (!d || isNaN(d)) {
		self.error("Couldn't parse " + dateStr + " as a date");
	}
	return d;
};

/**
 * Detects the type of the str (date or numeric) and sets the various
 * formatting attributes in this.attrs_ based on this type.
 * @param {String} str An x value.
 * @private
 */
TsDygraph.prototype.detectTypeFromString_ = function(str) {
	var isDate = false;
	if (str.indexOf('-') >= 0 ||
			str.indexOf('/') >= 0 ||
			isNaN(parseFloat(str))) {
		isDate = true;
	} else if (str.length == 8 && str > '19700101' && str < '20371231') {
		// TODO(danvk): remove support for this format.
		isDate = true;
	}

	if (isDate) {
		this.attrs_.xValueFormatter = TsDygraph.dateString_;
		this.attrs_.xValueParser = TsDygraph.dateParser;
		this.attrs_.xTicker = TsDygraph.dateTicker;
		this.attrs_.xAxisLabelFormatter = TsDygraph.dateAxisFormatter;
	} else {
		this.attrs_.xValueFormatter = function(x) { return x; };
		this.attrs_.xValueParser = function(x) { return parseFloat(x); };
		this.attrs_.xTicker = TsDygraph.numericTicks;
		this.attrs_.xAxisLabelFormatter = this.attrs_.xValueFormatter;
	}
};

/**
 * The user has provided their data as a pre-packaged JS array. If the x values
 * are numeric, this is the same as dygraphs' internal format. If the x values
 * are dates, we need to convert them from Date objects to ms since epoch.
 * @param {Array.<Object>} data
 * @return {Array.<Object>} data with numeric x values.
 */
TsDygraph.prototype.parseArray_ = function(data) {
	// Peek at the first x value to see if it's numeric.
	if (data.length == 0) {
		this.error("Can't plot empty data set");
		return null;
	}
	if (data[0].length == 0) {
		this.error("Data set cannot contain an empty row");
		return null;
	}

	if (this.dataView_.getLabels() == null) {
		this.warn("Using default labels. Set labels explicitly via 'labels' " +
		"in the options parameter");
		this.attrs_.labels = [ "X" ];
		for (var i = 1; i < data[0].length; i++) {
			this.attrs_.labels.push("Y" + i);
		}
	}

	if (TsDygraph.isDateLike(data[0][0])) {
		// Some intelligent defaults for a date x-axis.
		this.attrs_.xValueFormatter = TsDygraph.dateString_;
		this.attrs_.xValueParser = TsDygraph.dateParser;
		this.attrs_.xAxisLabelFormatter = TsDygraph.dateAxisFormatter;
		this.attrs_.xTicker = TsDygraph.dateTicker;

		// Assume they're all dates.
		var parsedData = TsDygraph.clone(data);
		for (var i = 0; i < data.length; i++) {
			if (parsedData[i].length == 0) {
				this.error("Row " + (1 + i) + " of data is empty");
				return null;
			}
			if (parsedData[i][0] == null
					|| typeof(parsedData[i][0].getTime) != 'function'
						|| isNaN(parsedData[i][0].getTime())) {
				this.error("x value in row " + (1 + i) + " is not a Date");
				return null;
			}
			parsedData[i][0] = parsedData[i][0].getTime();
		}
		return parsedData;
	} else {
		// Some intelligent defaults for a numeric x-axis.
		this.attrs_.xValueParser = function(x) { return parseFloat(x); };
		if (!this.attr_('xuUTC')) {
			this.attrs_.xValueFormatter = function(x) { return x; };
			this.attrs_.xAxisLabelFormatter = this.attrs_.xValueFormatter;
		} else {
			this.attrs_.xAxisLabelFormatter = function(d, gran) {
				var dec = '' + parseInt(d % 1E6);
				return dec;
				while (dec.length < 6)
					dec = "0" + dec;
				//return TsDygraph.dateAxisFormatter(new Date(d / 1000), TsDygraph.SECONDLY) + "." + dec;
				//return TsDygraph.dateAxisFormatter(new Date(d / 1000), TsDygraph.SECONDLY);
			};
			this.attrs_.xValueFormatter = function(d) {
				var dec = '' + parseInt(d % 1E6);
				while (dec.length < 6)
					dec = "0" + dec;
//				return TsDygraph.dateString_(new Date(d / 1000)) + "." + dec;
				return TsDygraph.dateString_(d) + "." + dec;
			};
		}
		this.attrs_.xTicker = TsDygraph.numericTicks;
		return data;
	}
};

///**
// * Parses a DataTable object from gviz.
// * The data is expected to have a first column that is either a date or a
// * number. All subsequent columns must be numbers. If there is a clear mismatch
// * between this.xValueParser_ and the type of the first column, it will be
// * fixed. Fills out rawData_.
// * @param {Array.<Object>} data See above.
// * @private
// */
//TsDygraph.prototype.parseDataTable_ = function(data) {
//	var cols = data.getNumberOfColumns();
//	var rows = data.getNumberOfRows();
//
//	var indepType = data.getColumnType(0);
//	if (indepType == 'date' || indepType == 'datetime') {
//		this.attrs_.xValueFormatter = TsDygraph.dateString_;
//		this.attrs_.xValueParser = TsDygraph.dateParser;
//		this.attrs_.xTicker = TsDygraph.dateTicker;
//		this.attrs_.xAxisLabelFormatter = TsDygraph.dateAxisFormatter;
//	} else if (indepType == 'number') {
//		this.attrs_.xValueFormatter = function(x) { return x; };
//		this.attrs_.xValueParser = function(x) { return parseFloat(x); };
//		this.attrs_.xTicker = TsDygraph.numericTicks;
//		this.attrs_.xAxisLabelFormatter = this.attrs_.xValueFormatter;
//	} else {
//		this.error("only 'date', 'datetime' and 'number' types are supported for " +
//				"column 1 of DataTable input (Got '" + indepType + "')");
//		return null;
//	}
//
//	// Array of the column indices which contain data (and not annotations).
//	var colIdx = [];
//	var annotationCols = {};  // data index -> [annotation cols]
//	var hasAnnotations = false;
//	for (var i = 1; i < cols; i++) {
//		var type = data.getColumnType(i);
//		if (type == 'number') {
//			colIdx.push(i);
//		} else if (type == 'string' && this.attr_('displayAnnotations')) {
//			// This is OK -- it's an annotation column.
//			var dataIdx = colIdx[colIdx.length - 1];
//			if (!annotationCols.hasOwnProperty(dataIdx)) {
//				annotationCols[dataIdx] = [i];
//			} else {
//				annotationCols[dataIdx].push(i);
//			}
//			hasAnnotations = true;
//		} else {
//			this.error("Only 'number' is supported as a dependent type with Gviz." +
//			" 'string' is only supported if displayAnnotations is true");
//		}
//	}
//
//	// Read column labels
//	var labels = [data.getColumnLabel(0)];
//	for (var i = 0; i < colIdx.length; i++) {
//		labels.push(data.getColumnLabel(colIdx[i]));
//	}
//	this.attrs_.labels = labels;
//	cols = labels.length;
//
//	var ret = [];
//	var outOfOrder = false;
//	var annotations = [];
//	for (var i = 0; i < rows; i++) {
//		var row = [];
//		if (typeof(data.getValue(i, 0)) === 'undefined' ||
//				data.getValue(i, 0) === null) {
//			this.warn("Ignoring row " + i +
//			" of DataTable because of undefined or null first column.");
//			continue;
//		}
//
//		if (indepType == 'date' || indepType == 'datetime') {
//			row.push(data.getValue(i, 0).getTime());
//		} else {
//			row.push(data.getValue(i, 0));
//		}
//
//		for (var j = 0; j < cols - 1; j++) {
//			row.push([ data.getValue(i, 1 + 2 * j), data.getValue(i, 2 + 2 * j) ]);
//		}
//		
//		if (ret.length > 0 && row[0] < ret[ret.length - 1][0]) {
//			outOfOrder = true;
//		}
//		ret.push(row);
//	}
//
//	if (outOfOrder) {
//		this.warn("DataTable is out of order; order it correctly to speed loading.");
//		ret.sort(function(a,b) { return a[0] - b[0] });
//	}
//	this.processData(ret);
//
//	if (annotations.length > 0) {
//		TsDygraph.log(TsDygraph.INFO,"Setting Annotations in TsDygraph");
//
//		this.setAnnotations(annotations, true);
//	}
//}

//These functions are all based on MochiKit.
TsDygraph.update = function (self, o) {
	if (typeof(o) != 'undefined' && o !== null) {
		for (var k in o) {
			if (o.hasOwnProperty(k)) {
				self[k] = o[k];
			}
		}
	}
	return self;
};

TsDygraph.isArrayLike = function (o) {
	var typ = typeof(o);
	if (
			(typ != 'object' && !(typ == 'function' &&
					typeof(o.item) == 'function')) ||
					o === null ||
					typeof(o.length) != 'number' ||
					o.nodeType === 3
	) {
		return false;
	}
	return true;
};

TsDygraph.isDateLike = function (o) {
	if (typeof(o) != "object" || o === null ||
			typeof(o.getTime) != 'function') {
		return false;
	}
	return true;
};

TsDygraph.clone = function(o) {
	// TODO(danvk): figure out how MochiKit's version works
	var r = [];
	for (var i = 0; i < o.length; i++) {
		if (TsDygraph.isArrayLike(o[i])) {
			r.push(TsDygraph.clone(o[i]));
		} else {
			r.push(o[i]);
		}
	}
	return r;
};


TsDygraph.prototype.redraw = function(arr,minX,maxX,isMinMax) {
	this.processData(arr,isMinMax);
	TsDygraph.log(TsDygraph.TIMING,"Call to redraw and optionally set trace gaps -- redraw");

	if (maxX) {
		this.dataView_.setXView(minX, maxX);
	}
	

	this.drawGraph_(this.rawData_);

};

TsDygraph.prototype.refresh = function() {
	TsDygraph.log(TsDygraph.TIMING,">>> Call to refresh -- redraw");
	
//	this.dataView_.setTraceGaps();
	this.drawGraph_(this.rawData_);
};

/**
 * Get the CSV data. If it's in a function, call that function. If it's in a
 * file, do an XMLHttpRequest to get it.
 * @private
 */
TsDygraph.prototype.start_ = function() {
	
	TsDygraph.log(TsDygraph.DEBUG,"In start_ with file:" + this.file_);

	this.processData(this.parseArray_(this.file_));
//	this.drawGraph_(this.rawData_);

};

/**
 * Changes various properties of the graph. These can include:
 * <ul>
 * <li>file: changes the source data for the graph</li>
 * </ul>
 * @param {Object} attrs The new properties and values
 */
TsDygraph.prototype.updateOptions = function(attrs,dontredraw) {
	this.dataView_.minPeriod = Number(attrs.minPeriod);
	if (attrs['file']) {
		this.file_ = attrs['file'];
		this.start_();
	} else if (!dontredraw) {
		TsDygraph.log(TsDygraph.TIMING,">> Change options -- redraw");
		this.drawGraph_(this.rawData_);
	}
};

/**
 * Resizes the dygraph. If no parameters are specified, resizes to fill the
 * containing div (which has presumably changed size since the dygraph was
 * instantiated. If the width/height are specified, the div will be resized.
 *
 * This is far more efficient than destroying and re-instantiating a
 * TsDygraph, since it doesn't have to reparse the underlying data.
 *
 * @param {Number} width Width (in pixels)
 * @param {Number} height Height (in pixels)
 */
TsDygraph.prototype.resize = function(width, height) {
	
	TsDygraph.log(TsDygraph.INFO,">> Resize:width: " + width);
	
	if (this.resize_lock) {
		return;
	}
	this.resize_lock = true;

	if ((width === null) != (height === null)) {
		this.warn("TsDygraph.resize() should be called with zero parameters or " +
		"two non-NULL parameters. Pretending it was zero.");
		width = height = null;
	}

	if (width) {
		this.maindiv_.style.width = width + "px";
		this.maindiv_.style.height = height + "px";
		this.width_ = width;
		this.height_ = height;
	} else {
		this.width_ = this.maindiv_.offsetWidth;
		this.height_ = this.maindiv_.offsetHeight;
	}
	
	// //
	if (this.width_ < 0)
		this.width_ = -this.width_;

	this.graphDiv.style.width = this.width_ + "px";
	this.graphDiv.style.height = this.height_ + "px";

	var clip = {
			top: 0,
			left: this.attr_("yAxisLabelWidth") + 2 * this.attr_("axisTickSize")
	};
	clip.width = this.width_ - clip.left - this.attr_("rightGap");
	clip.height = this.height_ - this.attr_("axisLabelFontSize")
	- 2 * this.attr_("axisTickSize");
	this.clippingArea_ = clip;

	// Resize the canvas for interactive parts of the chart.
	this.canvas_.width = this.width_;
	this.canvas_.height = this.height_;
	this.canvas_.style.left = "0px";				  // For FF
	this.canvas_.style.width = this.width_ + "px";    // for IE
	this.canvas_.style.height = this.height_ + "px";  // for IE

	// ... and for static parts of the chart.
	this.hidden_.width = this.width_;
	this.hidden_.height = this.height_;
	this.hidden_.style.left = "0px";				  // For FF
	this.hidden_.style.width = this.width_ + "px";    // for IE
	this.hidden_.style.height = this.height_ + "px";  // for IE
	
	// Make sure we don't overdraw.
	TsDygraph.clipCanvas_(this.hidden_, this.clippingArea_);
	TsDygraph.clipCanvas_(this.canvas_, this.clippingArea_);

	this.plotter_.updateSize(this.width_, this.height_);
	
	TsDygraph.log(TsDygraph.TIMING,">> Resize -- redraw");
	
	
	this.drawGraph_(this.rawData_, false);
	this.resize_lock = false;
};



///**
// * Returns a boolean array of visibility statuses.
// */
//TsDygraph.prototype.visibility = function() {
//	// Do lazy-initialization, so that this happens after we know the number of
//	// data series.
//	if (!this.attr_("visibility")) {
//		this.attrs_["visibility"] = [];
//	}
//	while (this.attr_("visibility").length < this.rawData_.getNumYValues() - 1) {//[0].length - 1) {
//		this.attr_("visibility").push(true);
//	}
////	console.log("Visibility set to " + this.attr_("visibility").length + " entries");
//	return this.attr_("visibility");
//};

TsDygraph.prototype.getVisibility = function() {
	return this.dataView_.visibility;
};

TsDygraph.prototype.isVisible = function(row) {
	return this.dataView_.getRowVisibility(row);
};

TsDygraph.prototype.getVisibleTraceCount = function() {
	var visibility = this.getVisibility();
	var visibleTraceCount = 0;
	for (var i = 0; i < visibility.length; i++) {
		if (visibility[i]) {
			visibleTraceCount++;
		}
	}
	return visibleTraceCount;
};

/**
 * Public method:  Changes the visiblity of a single series.
 */
TsDygraph.prototype.setVisibility = function(num, value) {

	this.dataView_.setRowVisibility(num, value);
	TsDygraph.log(TsDygraph.DEBUG,"Set visibility -- redraw");
	this.drawGraph_(this.rawData_);
};

/**
 * Specify the set of visible traces using a boolean vector.
 */
TsDygraph.prototype.setVisibleSet = function(vis) {
	var max = this.dataView_.visibility.length;

	var i;
	for (i = max; i >= 0; i--) {
		this.dataView_.setRowVisibile(i, vis[i]);
	}
	TsDygraph.log(TsDygraph.DEBUG,"Set visible set -- redraw");
	this.drawGraph_(this.rawData_);
}

TsDygraph.prototype.getData = function() {
	return this.rawData_;
}

/**
 * Update the list of annotations and redraw the chart.
 */
TsDygraph.prototype.setAnnotations = function(ann, suppressDraw) {
	this.annotations_ = ann;
	this.layout_.setAnnotations(this.annotations_);
	if (!suppressDraw) {
		TsDygraph.log(TsDygraph.TIMING,">> Change annotations -- redraw");
		this.drawGraph_(this.rawData_);
	}
};

/**
 * Return the list of annotations.
 */
TsDygraph.prototype.annotations = function() {
	return this.annotations_;
};


/**
 * Create a new canvas element. This is more complex than a simple
 * document.createElement("canvas") because of IE and excanvas.
 */
TsDygraph.createCanvas = function() {
	var canvas = document.createElement("canvas");

	isIE = (/MSIE/.test(navigator.userAgent) && !window.opera);
	if (isIE && (typeof(G_vmlCanvasManager) != 'undefined')) {
		canvas = G_vmlCanvasManager.initElement(canvas);
	}

	return canvas;
};

//////////From http://stackoverflow.com/questions/1517924/javascript-mapping-touch-events-to-mouse-events

TsDygraph.prototype.touchHandler = function(event)
{
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch(event.type)
    {
        case "touchstart": type = "mousedown"; break;
        case "touchmove":  type = "mousemove"; break;        
        case "touchend":   type = "mouseup";   break;
        default:           return;
    }

    // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
    //                screenX, screenY, clientX, clientY, ctrlKey, 
    //                altKey, shiftKey, metaKey, button, relatedTarget);

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1, 
                                  first.screenX, first.screenY, 
                                  first.clientX, first.clientY, false, 
                                  false, false, false, 0/*left*/, null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

TsDygraph.prototype.touch_init = function() 
{
    this.graphDiv.addEventListener("touchstart", this.touchHandler, true);
    this.graphDiv.addEventListener("touchmove", this.touchHandler, true);
    this.graphDiv.addEventListener("touchend", this.touchHandler, true);
    this.graphDiv.addEventListener("touchcancel", this.touchHandler, true);    
    this.graphDiv.addEventListener('gestureend', function(e) {
    	var scale = this.dataView_.getVisScale();
    	var newScale = scale + e.scale;
    	
    	if (newScale < 0){
    		return;
    	}
    	
    	this.dataView_.setVisScale(newScale );
    	this.drawGraph_(this.rawData_, true);
    	
    	// Call Java side callback
    	if (this.attr_("zoomCallback")) {
    		TsDygraph.log(TsDygraph.DEBUG,"WHEELEVENT: " + newScale);
    		this.attr_("zoomCallback")(null,null,newScale);
    	} 
    }, true);
}

TsDygraph.prototype.flushData = function(){
	this.rawData_.flush();
}

TsDygraph.prototype.setRaster = function(label) {
	this.dataView_.setRaster(label);
}

TsDygraph.prototype.clearRaster = function(label) {
	this.dataView_.clearRaster(label);
}

TsDygraph.prototype.setColorTable = function(table) {
	this.plotter_.colorTable = table;
}

//Helper function: returns true if parameter is an array
function isAnArray(v) {
	return v.constructor.toString().indexOf("Array") != -1;
}



DDataSet = function(data) {
	///////////////////////////////////////	
	//Data set constructor requires data
	//to be in an array of arrays, of the
	//form [[x1,y1,y2,y3],[x2,y1,y2,y3]...]
	//where the x's are in ascending order
	
	this.setValues(data);

	
	
};

DDataSet.prototype.flush = function(){

	var dl = this.data.length;
	for (var i = 0; i < dl; i++) {
		this.data[i].length=0;
		this.data[i][0] = 0;
	}


};

DDataSet.prototype.setValues = function(data,isMinMax) {
	this.data = [];
	this.isMinMax = isMinMax;
//	this.minY = 2E9;
//	this.maxY = -1;
	var dl = data.length;

	if (data.length == 0)
		return;
	for (var i = 0; i < dl; i++) {
		var dil = data[i].length;

		this.data.push(new Array(dil));
		this.data[i][0] = data[i][0];

		for (var j = 1; j < dil; j++) {
//			if (data[i][j]) {
//				if (this.minY > data[i][j])
//					this.minY = data[i][j];
//				if (this.maxY < data[i][j])
//					this.maxY = data[i][j];
//			}
			this.data[i][j] = data[i][j];
		}
	}
	var valid = DDataSet.validate(this.data, this.isMinMax);
	if (!valid) {
		TsDygraph.log(TsDygraph.ERROR, "DDataSet corrupted");
	}
};

DDataSet.prototype.addNewDataColumns = function(newDataset, isMinMax) {
	var valid = DDataSet.validate(newDataset, isMinMax);
	if (!valid) {
		TsDygraph.log(TsDygraph.ERROR, "Incoming dataset corrupted");
	}
	if (isMinMax != this.isMinMax) {
		this.data = newDataset;
		this.isMinMax = isMinMax;
		return;
	}
	var count = newDataset.length;

	// Find bounds
	var lb = newDataset[0][0];
	var ub = newDataset[newDataset.length - 1][0];

	// Find where these bounds are relative to the existing dataset
	var lInx = this.indexOf(lb);
	var rInx = this.lastIndexOf(ub);


	// If rInx == -1, i.e., it all goes before the existing data, then just insert at the front
	if (rInx == -1) {
//		TsDygraph.log(TsDygraph.DEBUG,"Inserting in front");
		// Bulk insert in the front
		this.data = newDataset.concat(this.data);
		var valid = DDataSet.validate(this.data, this.isMinMax);
		if (!valid) {
			TsDygraph.log(TsDygraph.ERROR, "DDataSet corrupted");
		}
		return;
	}
	
//	TsDygraph.log(TsDygraph.DEBUG,"Inserting items (" + lb + "," + ub +")...");
	// If it's negative then it's the predecessor to the actual value,
	// which wasn't found.
	if (lInx == -1) {
		lInx = 0;
	} else if (this.isMinMax && lInx < 0) {
		lInx = -lInx - 2 + 2;	// Advance *past* the start position
	} else if (!this.isMinMax && lInx < 0) {
		lInx= -lInx - 2 + 1;	// Advance *past* the start position
	}
	
	if (rInx >= 0) {
		//Move past matching timestamp so that we use the new sample value
		rInx++;
	} else if (this.isMinMax && rInx < 0) {
		rInx = -rInx - 2 + 2;
	} else if (!this.isMinMax && rInx < 0) {
		rInx = -rInx - 2 + 1;
	}

//	TsDygraph.log(TsDygraph.DEBUG,"Replacing positions " + lInx + " - " + rInx);
//	TsDygraph.log(TsDygraph.DEBUG,"Replacing (" + this.dataset.data[lInx][0] + "," + this.dataset.data[rInx][0] +")...");
	var leftSegment = [];
	if (lInx > 0)
		leftSegment = this.data.slice(0, lInx);

	var rightSegment = [];
	if (rInx < this.data.length)
		rightSegment = this.data.slice(rInx);

//	TsDygraph.log(TsDygraph.DEBUG,"Including " + leftSegment.length + " on left, " + rightSegment.length + " on right");

	this.data = leftSegment.concat(newDataset,rightSegment);

	this.recomputeMaxima();
	var valid = DDataSet.validate(this.data, this.isMinMax);
	if (!valid) {
		TsDygraph.log(TsDygraph.ERROR, "DDataSet corrupted");
	}
};

DDataSet.NAME = "DygraphData";
DDataSet.VERSION = "1.0";
DDataSet.__repr__ = function() {
	return "[" + this.NAME + " " + this.VERSION + "]" + JSON.stringify(this.data);
};

DDataSet.prototype.getNumXValues = function() {
	return this.data.length;
};

DDataSet.prototype.getNumYValues = function() {
	if (this.data.length == 0)
		return 0;
	return this.data[0].length;
};

DDataSet.prototype.getMinX = function() {
	return this.data[0][0];
};

DDataSet.prototype.getMaxX = function() {
	return this.data[this.data.length - 1][0];
};


//DDataSet.prototype.getMinY = function() {
//	return null; //this.minY;
//};

//DDataSet.prototype.getMaxY = function() {
//	return null; //this.maxY;
//};

DDataSet.prototype.recomputeMaxima = function () {
//	this.minY = 2E9;
//	this.maxY = -1;
	var dl = this.data.length;

	for (var i = 0; i < dl; i++) {
		var dil = this.data[i].length;

//		for (var j = 1; j < dil; j++) {
//			if (this.minY > this.data[i][j])
//				this.minY = this.data[i][j];
//			if (this.maxY < this.data[i][j])
//				this.maxY = this.data[i][j];
//		}
	}
}

DDataSet.toString = function() {
	return this.__repr__();
};

//Binary search for an x value within the data set, returning
//the index of the value, or its predecessor if the value
//is missing.  Uses negative numbers for predecessors!

//Return code status:
//Positive number:  first index of the value
//Negative 1 (-1):  belongs before the start of the dataset
//Other negative value:  belongs after (-value - 2)
DDataSet.prototype.indexOf = function(val) {
	return this.indexOf_(val, 0, this.data.length - 1, true);
};

//Return code status:
//Positive number:  last index of the value
//Negative 1 (-1):  belongs before the start of the dataset
//Other negative value:  belongs after (-value - 2)
DDataSet.prototype.lastIndexOf = function(val) {
	return this.indexOf_(val, 0, this.data.length - 1, false);
};

//Binary search for an x value within the data set, returning
//the index of the value, or its predecessor if the value
//is missing.  Uses negative numbers for predecessors!

//Return code status:
//Positive number:  index of the value
//Negative 1 (-1):  belongs before the start of the dataset
//Other negative value:  belongs after (-value - 2)
//If the result is < -1 then (-value -2) points to the first entry with the predecessor value.
DDataSet.prototype.indexOf_ = function(val,min,max,firstIndex) {
	if (max < min) {
		var pred;
		if (max >= 0) {
			//return -max - 2;
			pred = max;
		} else {
			//return max;
			pred = -max - 2;
		}
		if (pred == -1) {
			return pred;
		}
		var predVal = this.data[pred][0];
		while (pred >= 0 && this.data[pred][0] == predVal) {
			pred--;
		}
		pred++;
		return -pred - 2;
	}

	var mid = parseInt((min + max) / 2);
	//TsDygraph.log(TsDygraph.DEBUG,"Searching for " + val + " between " + min + "," + max + " with midpoint at " + mid);

	if (this.data[mid][0] > val) {
		return this.indexOf_(val, min, mid - 1,firstIndex);
	} else if (this.data[mid][0] < val) {
		return this.indexOf_(val, mid + 1, max,firstIndex);
	} else {
		var index = mid;
		if (firstIndex) {
			while (index >= 0 && this.data[index][0] == val) {
				index--;
			}
			index++;
		} else {
			while (index < this.data.length && this.data[index][0] == val) {
				index++;
			}
			index--;
		}
		return index;
	}
};

DDataSet.validate = function(data, isMinMax) {
	var valid = true;
	if (TsDygraph.LEVEL > TsDygraph.VALIDATE) {
		return valid;
	}
	
	if (!isMinMax) {
		var lastTs = NaN;
		for (var i = 0; i < data.length; i++ ) {
			var ts = data[i][0];
			if (!isNaN(lastTs) && ts < lastTs) {
				valid = false;
				break;
			}
			lastTs = ts;
		}
	} else {
		valid = data.length % 2 == 0;
		if (valid) {
			var lastTs = NaN;
			for (var i = 0; i < data.length; i += 2 ) {
				var minTs = data[i][0];
				var maxTs = data[i+1][0];
				if (minTs != maxTs) {
					valid = false;
					break;
				}
				if (!isNaN(lastTs) && minTs < lastTs) {
					valid = false;
					break;
				}
				lastTs = minTs;
			}
		}
	}
	return valid;
};

//Test driver
DDataSet.prototype.test = function() {
	var test = new DDataSet([[1,2,3,4,5,6,7],
	                         [4,1,2,3,4,5,6],
	                         [8,9,10,11,12,13,14],
	                         [20,21,22,23,24,25,26]]);//,
	//['a','b','c','d','e','f']);

	// Search tests
	TsDygraph.log(TsDygraph.DEBUG,"Initial array: " + JSON.stringify(test.data));
	var result = test.indexOf(1);
	if (test.data[result][0] != 1)
		console.error("ERROR: Search for 1: " + result + "/" + test.data[result]);

	result = test.indexOf(4);
	if (test.data[result][0] != 4)
		console.error("ERROR: Search for 4: " + result + "/" + test.data[result]);

	result = test.indexOf(8);
	if (test.data[result][0] != 8)
		console.error("ERROR: Search for 8: " + result + "/" + test.data[result]);

	result = test.indexOf(20);
	if (test.data[result][0] != 20)
		console.error("ERROR: Search for 20: " + result + "/" + test.data[result]);

	result = test.indexOf(18);
	if (result > 0 || test.data[-result - 2][0] != 8)
		console.error("ERROR: Search for missing value 8 does not show predecessor: " + result + "/" + test.data[result]);

	result = test.indexOf(21);
	if (test.data[-result - 2][0] != 20)
		console.error("ERROR: Search for missing value 21 does not show predecessor: " + result + "/" + test.data[result]);

	result = test.indexOf(0);
	if (result != -1)
		console.error("ERROR: Search for missing value 0 does not before-list value: " + result + "/" + test.data[result]);

	// Col insert tests
	test.insertCol([5,6,7,8,9,10,11]);
	if (test.data[test.indexOf(5)][0] != 5)
		console.error("ERROR: Insert of col position 5 failed: " + JSON.stringify(test.data));
	test.insertCol([0,7,8,9,10,11,12]);
	if (test.data[test.indexOf(0)][0] != 0)
		console.error("ERROR: Insert of col position 0 failed: " + JSON.stringify(test.data));
	test.insertCol([19,26,27,28,29,30,31]);
	if (test.data[test.indexOf(19)][0] != 19)
		console.error("ERROR: Insert of col position 19 failed: " + JSON.stringify(test.data));
	test.insertCol([50,56,57,58,59,50,51]);
	if (test.data[test.indexOf(50)][0] != 50)
		console.error("ERROR: Insert of col position 50 failed: " + JSON.stringify(test.data));

	// Row insert tests
	var height = test.data[0].length;
	test.appendRow([75,76,77,78,79,80,81,82]);
	if (height +1 != test.data[0].length || test.data[0][test.data[0].length - 1] != 75)
		console.error("ERROR: Append of row failed: " + JSON.stringify(test.data));
	test.insertRow([45,46,47,48,49,40,41,42],3);
	if (height +2 != test.data[0].length || test.data[0][3] != 45)
		console.error("ERROR: Insert of row position 5 failed: " + JSON.stringify(test.data));

	test.deleteRow(5);
	// Row delete tests
	if (test.data[0].length != height + 1)
		console.error("ERROR: Delete of row position 5 failed: " + JSON.stringify(test.data));

	// Col delete tests
	test.deleteCol(5);
	if (test.indexOf(5) >= 0)
		console.error("ERROR: Delete of col position 5 failed: " + JSON.stringify(test.data));
	var len = test.data.length;
	test.deleteCol(5);
	if (test.data.length != len)
		console.error("ERROR: Delete of col position 5, now missing, affected array: " + JSON.stringify(test.data));
}

DDataView = function(dataset) {
	this.setNew(dataset);
	this.setAverage = false;
	this.visScale = 1/70; // VisScale is scale in mm/uv the inverse of what is set in dropBox.
	this.voltConvFactor = 1; // Voltage Conversion Factor.	
	this.doBaselineUpdate = true;
	this.doRecenter = true;
	this.updateLabels = true;
	this.dotsPerInch = 1;
};

DDataView.prototype.setNew = function(dataset) {
	this.dataset = dataset;
	this.plotAgainst = [];
	this.points = [];
	this.rowScale = [];
	this.visibility = [];
	this.rowAdjust = [];
	this.rowBaseline = [];
	this.rank = [];
	this.sourceVec = [];

	var rows = this.getNrTraces();
	for (var r = 1; r <= rows; r++) {
		this.plotAgainst.push(-1);
		this.rowScale.push(1);
		this.visibility.push(true);
		this.rowAdjust.push(0);
		this.rowBaseline.push(0);
		this.rank.push(1);
		this.raster.push(false);
		this.sourceVec.push(r);
	}
	
	TsDygraph.log(TsDygraph.INFO," DYGRAPH SETNEW: " + rows + " rows");
	
};

DDataView.prototype.getNrTraces = function() {
	// Return nr of traces in the view. This is based on the number of
	// channels is the montage, or dataset if no montage is selected.
	return this.sourceVec.length;
}

DDataView.prototype.reprocessData = function(scaleX, scaleY) {
	TsDygraph.log(TsDygraph.INFO,"Reprocess (uses dataset bounds)");
	this.setXView(this.dataset.getMinX(), this.dataset.getMaxX());

	if (!scaleX) {
		if (!this.scaleX)
			this.scaleX = 1.0;
	} else{
		this.scaleX = scaleX;
	}
	
	if (!scaleY) {
		if (!this.scaleY)
			this.scaleY = 1.0;
	} else {
		this.scaleY = scaleY;
	}

};

//Re-montage against a different row's value
DDataView.prototype.remontage = function(row, rowAgainst) {
	// if -1, plot native montage
	// TODO: if -2, plot against global reference
	
	if (rowAgainst)
		this.plotAgainst[row] = rowAgainst;
	else
		this.plotAgainst[row] = -1;
	TsDygraph.log(TsDygraph.INFO,"Plotting " + row + " against " + rowAgainst);
};

//Montage against average value
DDataView.prototype.montageAverage = function(doIt) {
	this.setAverage = doIt;
};

DDataView.prototype.getMinMaxY = function() {
	var ret = [];
	var rows = this.getNrTraces();
	var cols = this.dataset.getNumXValues();
	var min = 1E10;
	var max = -1E10;
	for (var r = 0; r < rows; r++) {
		if (this.visibility[r]) {
			for (var c = 0; c < cols; c++) {
				var v = this.getCoord(r,c);
				if (max < v)
					max = v;
				if (min > v)
					min = v;
			}
		}
	}
	ret.push(min);
	ret.push(max);
	return ret;
};

/**
 * Returns the overall scale factor of a row (without rowScale by default)
 */
DDataView.prototype.getScaleFactor = function(row) {
	if (!row) {
		return this.visScale;
//		if (!this.rowScale || !this.rowScale[0])
//			return this.scaleY;
//		else
//			return this.rowScale[0] * this.scaleY;
	} else
		if (!this.rowScale || !this.rowScale[row])
			return this.visScale;
//			return this.scaleY;
		else
			return this.rowScale[row] * this.visScale;
//			return this.rowScale[row-1] * this.scaleY;
};

/**
 * Adjust the scale factor for a given row, or all data if row
 * is omitted.  Leaves the scaleY alone.
 */
//DDataView.prototype.adjustScale = function(target, row) {
//	if (!this.rowScale) {
//		this.rowScale = [];
//	}
//	if (!row) {
//		var r;
//		for (r = 0; r < this.rowBaseline.length; r++)
//			this.rowScale[r] = target / this.scaleY;
//	} else
//		this.rowScale[row] = target / this.scaleY;
//};

DDataView.prototype.resetScale = function() {
	this.rowScale = [];
	for (var r = 0; r < this.sourceVec.length; r++)
		this.rowScale[r] = 1.0;
};

DDataView.prototype.getValue = function(row, col){
	var data = this.dataset.data;
	var val = data[col][row + 1];
	if (this.plotAgainst && this.plotAgainst[row] != -1 && data[col][this.plotAgainst[row] +1])
		val -= data[col][this.plotAgainst[row]+1];
	return val * this.voltConvFactor * (this.invertSignal ? -1 : 1);
};

/**
 * GETCOORD calculated y-coordinate for each row and column in data. It
 * automatically offsets for montaged channels.
 */
DDataView.prototype.getCoord = function(row, col) {

	var data = this.dataset.data;
	var dataIndex = this.sourceVec[row] + 1;

	try {

		// ** Compute offset when rendering montage ** 
		var refValue = 0 ; 
		
		// If montaged, adjust refValue
		if (this.plotAgainst && this.plotAgainst[row] != -1 && this.plotAgainst[row] != this.sourceVec[row] && data[col][this.plotAgainst[row] +1]) 
			refValue = data[col][this.plotAgainst[row]+1];

		// Compute value in uV * scale. (and invert because canvas coord system is inversed)
		var val = ((data[col][dataIndex] -refValue + this.rowAdjust[row]) * (this.invertSignal ? 1 : -1));
		
		// Compute value in pixels
		val = (val * this.voltConvFactor * this.visScale * this.rowScale[row]) * (this.dotsPerInch/ 25.4);// + this.rowBaseline[row];
		
		return val;

	} catch (e) {
		TsDygraph.log(TsDygraph.DEBUG,"Could not access " + col + " / " + row);
		throw e;
	}
};

DDataView.prototype.computeWindow = function(offsetX,xwidth,yheight) {

	var newPoints = this.computeScaledWindow(this.dataset,
			this.scaleX,
			offsetX,
			xwidth, yheight,
			this.visibility,this.points);
	if (newPoints.length > 0) {
		this.points = newPoints;
	}
	return this.points;
};

DDataView.prototype.computeScaledWindow = function(dataset,scaleX,
		offsetX,xwidth,yheight,visibilityVector,points) {

	
	var nrTraces = this.sourceVec.length;
	while (this.rowAdjust.length < nrTraces)
		this.rowAdjust.push(0);
	
	if(this.plotAgainst.length!=nrTraces || this.visibility.length!=nrTraces ||
			this.rowScale.length!=nrTraces || this.rowBaseline.length!=nrTraces || 
			this.rowAdjust.length!=nrTraces){
		TsDygraph.log(TsDygraph.ERROR, "Lengths of vis-vectors should be equal: nrTraces: " +
				nrTraces + " plotAgainst: " + this.plotAgainst.length + " visibility: " +
				this.visibility.length + " rowScale: " + this.rowScale.length + 
				" rowBaseline: " + this.rowBaseline.length + " rowAdjust: " + this.rowAdjust.length);
	}
	
	
	var max = dataset.getNumXValues();
	var left = dataset.indexOf(this.minX);
	//Deliberatly not using lastIndexOf. Since we move to the right one more sample this makes the arithmetic easier
	//when we actually find a match
	var right = dataset.indexOf(this.maxX);

	if (max == 0)
		return;

	// If it's heavily negative, this means that an exact match wasn't found but we
	// found the predecessor
	if (right < -1)
		right = -right - 2;

	if (left < -1)
		left = -left - 2;
	
	var data = dataset.data;
	var isMinMax = dataset.isMinMax;

	// Left and right datapoints
	if (!isMinMax && left > 0)
		left--;
	else if (isMinMax && left > 1)
		left -= 2;
	else if (left < 0)
		left = 0;
	
	if (right < max - 1) {
		//For non min/max this moves us up one sample. For min/max this just moves us to the second sample in pair
		right++;
	    if (isMinMax && right < max - 2)
	    	//now we've moved up one sample for min/max too.
	    	right += 2;
 	} else if (right < 0)
		right = max - 1;

	if (xwidth < 0)
		xwidth = -xwidth;

	var xscale = scaleX * xwidth;
	
	var requestedNumCols = right - left + 1;
	if (isMinMax) {
		requestedNumCols = requestedNumCols / 2;
	}
	
	//Ignore request to make large number of points unless we need to correct for too few points for some reason.
	if (requestedNumCols > 1.1 * xwidth && points.length > 0.9 * xwidth) {
		TsDygraph.log(TsDygraph.DEBUG, "Ignoring request to create " + requestedNumCols + " points per channel for plot " + xwidth + " pixels wide");
		return [];
	}
	
	if (isMinMax) {
		this.computeScaledMinMaxPoints(data, left, right, xscale, offsetX, yheight, visibilityVector, points);
	} else {
		this.computeScaledPoints(data, left, right, xscale, offsetX, yheight, visibilityVector, points);
	}

	return points;
};


DDataView.prototype.computeScaledPoints = function(data,left,right,xscale, offsetX, yheight, visibilityVector,points) {
	var inx = 0;
	var labels = this.getAllMontagedLabels(); 
	
	for (j = 0; j < labels.length; j++) {
		if (!visibilityVector[j]) {
			continue;
		}
		if (this.raster[j] == true) {
			for (var i = left; i <= right; i++) {	
				var itemY = this.getValue(j,i);  // in scaled units around offset
				var itemX = data[i][0];
				var point = {
						canvasx: ((((itemX - this.minX) * xscale)) + offsetX)|0,
						canvasy: this.rowBaseline[j] * yheight * this.scaleY,
						xval: itemX,
						yval: this.getValue(j,i),
						raster: true,
						name: labels[j]
				};
				
				points[inx++ ] = point;
			}
		} else {
			for (var i = left; i <= right; i++) {	 
				var itemY = this.getCoord(j,i);  // in scaled units around offset
				var itemX = data[i][0];
				var point = {
						canvasx: ((((itemX - this.minX) * xscale)) + offsetX)|0,
						canvasy: (this.rowBaseline[j] * yheight * this.scaleY) + itemY,
						xval: itemX,
						yval: this.getValue(j,i),
						name: labels[j]
				};
				
				points[inx++ ] = point;
			}
		}
	
	}
	points.length = inx;
	//TsDygraph.log(TsDygraph.DEBUG, "Not min/max. Not counting canvasx gaps larger than 1");

}

DDataView.prototype.computeScaledMinMaxPoints = function(data,left,right,xscale,offsetX, yheight, visibilityVector,points) {
	if (left % 2 != 0) {
		TsDygraph.log(TsDygraph.ERROR, "left must be even for MinMax: " + left);
	}
	if (right % 2 != 1) {
		TsDygraph.log(TsDygraph.ERROR, "right must be odd for MinMax: " + right);
	}
	
	var inx = 0;
	var labels = this.getAllMontagedLabels(); 
	
	//noSamples is even because of above checks
	var noSamples = right - left + 1;
	
	for (j = 0; j < labels.length; j++) {
		if (!visibilityVector[j]) {
			continue;
		}
		var lastCanvasx = NaN;
		var noBigGaps = 0;
		var raster = this.raster[j];
		for (var s = 0; s < noSamples; s += 2) {	
			
			//Point for Min
			var itemXMin = data[s + left][0];
			var canvasxMin = Math.round(((((itemXMin - this.minX) * xscale)) + offsetX));
			var yvalMin = this.getValue(j, s + left);
			var itemYMin, canvasyMin;
			if (raster) {
				itemYMin = this.getValue(j, s + left);
				canvasyMin = (this.rowBaseline[j] * yheight * this.scaleY);
			} else {
				itemYMin = this.getCoord(j, s + left);  // in scaled units around offset
				canvasyMin = (this.rowBaseline[j] * yheight * this.scaleY) + itemYMin;
			}
			
			//Point for Max
			var itemXMax = data[s + 1 + left][0];
			var itemYMax,canvasyMax, yvalMax;
			if (itemXMin != itemXMax) {
				TsDygraph.log(TsDygraph.ERROR, "timestamps must come in pairs for MinMax: " + itemXMin + " != " + itemXMax + ". Using " + itemXMin);
				itemXMax = itemXMin;
				itemYMax = itemYMin;
				canvasyMax = canvasyMin;
				yvalMax = yvalMin;
			} else {
				yvalMax = this.getValue(j, s + 1 + left);
				if (raster) {
					itemYMax = this.getValue(j, s + 1 + left);
					canvasyMax = (this.rowBaseline[j] * yheight * this.scaleY);
				} else {
					itemYMax = this.getCoord(j, s + 1 + left);  // in scaled units around offset
					canvasyMax = (this.rowBaseline[j] * yheight * this.scaleY) + itemYMax;
				}
			}
			
			var point = {
					canvasx: canvasxMin,
					canvasy: (canvasyMin + canvasyMax)/2,
					xval: itemXMin,
					yval: (yvalMin + yvalMax)/2,
					name: labels[j],
					canvasyMin: canvasyMin,
					canvasyMax: canvasyMax,
					yvalMin: yvalMin,
					yvalMax: yvalMax
			};
			
			if (raster)
				point.raster = true;
			
			points[inx++] = point;
			

			if (!isNaN(lastCanvasx) && point.canvasx - lastCanvasx > 2) {
				noBigGaps++;
			}
			lastCanvasx = point.canvasx;
			
		}
		if (TsDygraph.LEVEL <= TsDygraph.DEBUG) {
			if ((noBigGaps/(noSamples/2 - 1)) > 0.1) {
				TsDygraph.log(TsDygraph.ERROR, noBigGaps + " out of " + (noSamples/2 - 1) + " canvasx gaps were larger than 2");
			} else {
				TsDygraph.log(TsDygraph.DEBUG, noBigGaps + " out of " + (noSamples/2 - 1) + " canvasx gaps were larger than 2");
			}
		}
	}
	points.length = inx;
}

DDataView.prototype.setLabels = function(labels) {
	TsDygraph.log(TsDygraph.INFO,"DDATAVIEW: Setting labels");
	
	this.labels = labels;
	this.sourceVec = [];
	this.visibility = [];
	this.rank = [];
	this.plotAgainst = [];
	this.rowScale = [];
	
	this.raster = [];
	
	// Labels include TIME so only update sourceVec for real channels.
	for (var i=1; i<labels.length; i++){
		this.sourceVec.push(i-1);
		this.visibility.push(true);
		this.rank.push(i-1);
		this.plotAgainst.push(-1);
		this.rowScale.push(1);
		this.raster.push(false);
	}
	this.doBaselineUpdate = true;
	this.updateLabels = true;
	this.doRecenter = true;

};

DDataView.prototype.getLabels = function() {
	return this.labels;
}

DDataView.prototype.setRaster = function(label) {
	for (var i = 1; i < this.labels.length; i++)
		if (this.labels[i] == label) {
			this.raster[i-1] = true;
		}
}

DDataView.prototype.clearRaster = function(label) {
	for (var i = 1; i < this.labels.length; i++)
		if (this.labels[i] == label) {
			this.raster[i-1] = true;
		}
}

// VisibleScale is the scale in uV/mm that is set in the dropbox
DDataView.prototype.setVisScale = function(scale) {
	TsDygraph.log(TsDygraph.DEBUG,"Updating VISSCALE to: " + scale);
	if(!isNaN(scale) && scale > 0){
		this.visScale = scale;
		this.rowScale = [];
		for (var i=0; i<this.sourceVec.length; i++){
			this.rowScale[i] = 1;
		}
		
	}
}

DDataView.prototype.setVoltConvFactor = function(convF) {
	TsDygraph.log(TsDygraph.DEBUG,"Updating VOLTCONVFACTOR to: " + convF);
	if(!isNaN(convF)){
		this.voltConvFactor = convF;
	}
}

DDataView.prototype.getVisScale = function() {
	return this.visScale;
}


//These scales are the overall x, and Y scale of the Axis (not traces)
DDataView.prototype.setScales = function(xscale,yscale) {

	TsDygraph.log(TsDygraph.DEBUG,"Updating XScale to: " + xscale + " and YScale to: " + yscale);
	if (!isNaN(xscale)) {
		this.scaleX = xscale;
	}

	if (!isNaN(yscale))
		this.scaleY = yscale;
	
	
};

DDataView.prototype.getScales = function() {
	return [this.scaleX, this.scaleY];
}


DDataView.prototype.setXView = function(minX,maxX) {
	TsDygraph.log(TsDygraph.DEBUG,"*** Setting x view [" + minX + "," + maxX + "]");
	this.minX = minX;
	this.maxX = maxX;
	this.xrange = this.maxX - this.minX;
	this.scaleX = (this.xrange != 0 ? 1/this.xrange : 1.0);
	this.doRecenter = true;
//	console.debug("setXView set scale to " + this.scaleX);
};

DDataView.prototype.getXView = function() {
	return [this.minX, this.maxX];
};

DDataView.prototype.setRowView = function(minYRow,maxYRow) {
	var max = this.getNrTraces();
	for (var i = 1; i < max; i++) {
		if (i < minYRow || i > maxYRow)
			this.visibility[i] = false;
		else
			this.visibility[i] = true;
	}
};

DDataView.prototype.setRowVisibility = function(row, status) {
	if (row < 0 || row >= this.visibility.length)
		return;

	this.doBaselineUpdate = true;
//	var prev = this.visibility[row];
	this.visibility[row] = status;
//	if (status == prev)
//		return;	

};

DDataView.prototype.getVisibility = function() {
	return this.visibility;
}

DDataView.prototype.getRowVisibility = function(row) {
	return this.visibility[row];
}

DDataView.prototype.setSourceVec = function(s) {
	TsDygraph.log(TsDygraph.INFO,"** Setting sourceVec in DDataView** " + s);
	this.sourceVec = s;
	this.doBaselineUpdate = true;
	this.updateLabels = true;
}

DDataView.prototype.setVisibility = function(v) {
	TsDygraph.log(TsDygraph.INFO,"** Setting visibility in DDataView** " + v);
	this.visibility = v;
	this.doBaselineUpdate = true;
	this.updateLabels = true;
}


DDataView.prototype.setRank = function(r) {
	TsDygraph.log(TsDygraph.INFO,"** Setting rank in DDataView** " + r);
	this.rank = r;
	this.doBaselineUpdate = true;
	this.updateLabels = true;
}

DDataView.prototype.setPlotAgainst = function(a) {
	TsDygraph.log(TsDygraph.INFO,"** Setting PlotAgainst in DDataView** " + a);
	this.plotAgainst = a;
	this.doRecenter = true;
	this.updateLabels = true;
}

//DDataView.prototype.setRowRank = function(row, rank) {
//	// Set a rank for channel, order of channels is determined by rank.
//	// High rank is displayed higher in graph
//	TsDygraph.log(TsDygraph.INFO,"** Setting rank in DDataView.setRowRank**" + rank + "**"+row);
//
//	if (row < 0 || row >= this.rank.length)
//		return;
//	
//	var prev = this.rank[row];
//	this.rank[row] = rank;
//	if (rank == prev)
//		return;
//
//	if (this.rowBaseline) {
//		var offset = 0;
//		// Adjust the rows' baselines
//		for (var r = this.getNrTraces() - 1; r >= 0; r--) {
//			if (this.visibility[r]) {
//				this.rowBaseline[r] = offset;
//				offset += TsDygraph.CHANNEL_OFFSET;//this.channelOffset;
//			}
//		}
//	}
//	
//}

DDataView.prototype.getRowRank = function(row){
	return this.rank[row];
}



//DDataView.prototype.addTrace = function(rowData,label,yoffset,scale) { // Also optional ref trace?
//	this.dataset.appendRow(rowData);
//
//	this.rowAdjust.push(yoffset);
//	this.rowScale.push(scale);
//	this.labels.push(label);
//};

DDataView.prototype.addNewDataColumns = function(newDataset, isMinMax) {
	this.dataset.addNewDataColumns(newDataset, isMinMax);
};

DDataView.prototype.getRowScale = function(row){	
	return this.rowScale[row];
}

DDataView.prototype.setRowScale = function(row, scaleFactor) {
	if (!this.rowScale) {
		this.rowScale = [];
		for (var i = 0; i < this.getNrTraces()-1; i++)
			if (i == row)
				this.rowScale.push(scaleFactor);
			else
				this.rowScale.push(1.0);
	} else
		this.rowScale[row] = scaleFactor;
};

//DDataView.prototype.setRowAdjustment = function(row,refValue) {
//	if (!this.rowAdjust) {
//		this.rowAdjust = [];
//		for (var i = 1; i < this.dataset.getNumYValues(); i++)
//			if (i == row - 1)
//				this.rowAdjust.push(refValue);
//			else
//				this.rowScale.push(0);
//	} else
//		this.rowAdjust[row] = refValue;
//};

DDataView.prototype.isScaledData = function() {
	return this.rowAdjust != null;
};

DDataView.prototype.isInverted = function() {
	return this.invertSignal == true;
}

DDataView.prototype.isMinMax = function() {
	return this.dataset.isMinMax;
}

DDataView.prototype.setDPI = function(dotsPerInch) {
	this.dotsPerInch = dotsPerInch;
}

DDataView.prototype.setInverted = function(inv) {
	this.invertSignal = inv;
}

// Recenter sets the rowAdjust for all channels based on the mean of the plotted data.
DDataView.prototype.recenter = function() {
	TsDygraph.log(TsDygraph.DEBUG, "RECENTERING");
	var rows = this.getNrTraces();
	var cols = this.dataset.getNumXValues();
	
	this.rowAdjust = [];
	for (var r = 0; r < rows; r++) {

		this.rowAdjust[r] = 0;

		for (var c = 0; c < cols; c++) {
			
			this.rowAdjust[r] += this.dataset.data[c][this.sourceVec[r] + 1];
			if (this.plotAgainst && this.plotAgainst[r] > -1 && this.plotAgainst[r] != this.sourceVec[r]){
				this.rowAdjust[r] -= this.dataset.data[c][this.plotAgainst[r] + 1];
			}
		}
		
		// RowAdjust is mean of the columns per row
		this.rowAdjust[r] = -(this.rowAdjust[r] / cols);

	}
	this.doRecenter = false;
	TsDygraph.log(TsDygraph.DEBUG,"In recenter: rowAdjust = " + this.rowAdjust);
};

DDataView.prototype.getMinBound = function() {
	// Return minimum baseline - 1 x distance between baselines.
	
	var minB = Number.MAX_VALUE;
	for (var i = 0; i < this.rowBaseline.length; i++){
		if (this.visibility[i] && this.rowBaseline[i]<minB)
			minB = this.rowBaseline[i];
	}
	if (minB == Number.MAX_VALUE) {
		minB = 0;
	}
	return minB - TsDygraph.CHANNEL_OFFSET;

};

DDataView.prototype.getMaxBound = function() {
	// Return maximum baseline + 1xdistance between baselines.
	
	var maxB = 0;
	for (var i = 0; i < this.rowBaseline.length; i++){
		if (this.visibility[i] && this.rowBaseline[i]>maxB)
			maxB = this.rowBaseline[i];
	}
	
	return maxB + TsDygraph.CHANNEL_OFFSET;

};

DDataView.prototype.getPeriod = function() {
	//var numXValues = this.dataset.isMinMax ? this.dataset.getNumXValues() / 2 : this.dataset.getNumXValues();
	//return (this.dataset.getMaxX() - this.dataset.getMinX()) / numXValues;
	
	if (this.dataset.data.length > 1) {
		if (this.isMinMax()) {
			return this.dataset.data[2][0] - this.dataset.data[0][0];
		} else {
			return this.dataset.data[1][0] - this.dataset.data[0][0];
		}
	} else {
		return 0;
	}
};


///**
//* Helper function to sort channels based on Rank.
////*/
//DDataView.prototype.sortWithIndeces = function(toSort) {
//	var toSort2 = toSort.slice();
//	for (var i = 0; i < toSort2.length; i++) {
//		toSort2[i] = [toSort2[i], i];
//	}
//	toSort2.sort(function(left, right) {
//		return left[0] - right[0];
//	});
//	toSort2.sortIndices = [];
//	for (var j = 0; j < toSort2.length; j++) {
//		toSort2.sortIndices.push(toSort2[j][1]);
//		toSort2[j] = toSort2[j][0];
//	}
//	return toSort2;
//};

// Set this.rowBaseline for each channel --> offset to mean in graph
DDataView.prototype.updateRowBaseline = function(){
	var offset = TsDygraph.CHANNEL_OFFSET;
	this.rowBaseline = [];
	
	TsDygraph.log(TsDygraph.DEBUG,"UPDATEROWBASELINE: Current Rank:" + this.rank);
	
	for (var r = 0; r < this.getNrTraces(); r++) {
		if (this.visibility[r]) {
			this.rowBaseline[r] = offset;
			offset += TsDygraph.CHANNEL_OFFSET;
		} else {
			this.rowBaseline[r] =0;
		}
	}
	this.doBaselineUpdate = false;
	TsDygraph.log(TsDygraph.DEBUG,"UpdateRowBaseline rowBaseline: " + this.rowBaseline);	
};

DDataView.prototype.getAllMontagedLabels = function(){
	var allLabels = [];
	var rows = this.getNrTraces();
	
	for (var r=0; r < rows; r++){
		allLabels.push(this.getMontagedLabel(r));
	}
	return allLabels;
		
};

DDataView.prototype.getVisLabels = function(){
	var allLabels = [];
	var rows = this.getNrTraces();
	
	for (var r=0; r < rows; r++){
		if (this.visibility[r]){
			allLabels.push(this.getMontagedLabel(r));
		}
	}
	return allLabels;
		
};

DDataView.prototype.getMontagedLabel = function(r){
	var curLabel ="";
	if (this.plotAgainst[r] > -1 && this.plotAgainst[r] != this.sourceVec[r]){
		curLabel = this.labels[this.sourceVec[r] + 1] + " vs " + this.labels[this.plotAgainst[r] + 1];
	} else {
		curLabel = this.labels[this.sourceVec[r] + 1];
	}
	return curLabel;
};

DDataView.prototype.getRowTicks = function(){
	var ticks = [];
	var rows = this.getNrTraces();
	
	TsDygraph.log(TsDygraph.DEBUG, "GETROWTICKS: " + this.labels +" " + this.plotAgainst.length);
	
	var curIdx = 0;
	for (var r = 0; r < rows; r++) {
		if (this.visibility[r]) {	
			ticks.push( {label: this.getMontagedLabel(r), v: this.rowBaseline[r]} );
			TsDygraph.log(TsDygraph.DEBUG, "GETROWTICKS: r: " + r +" montagedLabel: " + this.getMontagedLabel(r) +" baseline: " + this.rowBaseline[r]);
		}
	}
	return ticks;
};

// Returns canvas coordinate for particular row.
DDataView.prototype.getYPositionFor = function(row, minY, yheight, offsetY) {
	return ((1.0 - ((this.rowBaseline[row] - minY) * this.scaleY)) * yheight) + offsetY;
};

////From http://www.dscripts.net/2010/06/06/disable-text-selection-in-webpage-using-javascript/
////this function disables text select on a perticular dom object
//function disableSelection(target)
//{
//	if (typeof target.onselectstart!="undefined") //IE route
//		target.onselectstart=function(){return false}
//	else if (typeof target.style.MozUserSelect!="undefined") //Firefox route
//		target.style.MozUserSelect="none"
//			else //All other route (ie: Opera)
//				target.onmousedown=function(){return false}
//	target.style.cursor = "default"
//}

//DDataController.prototype.setPageNext = function() {

//};

//DDataController.prototype.setPagePrev = function() {

//};

//DDataController.prototype.setPageUp = function() {

//};

//DDataController.prototype.setPageDown = function() {

//};

//DDataController.prototype.setZoomLevel = function(xMin,xMax,yMin,yMax,pixels) {

//};


//Still TODO(zives):   * support for baseline correct, negate, etc.
//* incremental DDataView update for pan, zoom, hideTrace, showTrace
//* support for move-to-top
//* support for scrolling among a set of traces [done.]
//* support for show all traces
//* notify on scale change due to show/hide (in hideTraceHandler) [visibilityCallback]
//* notify on move (non-zoom) [moveCallback]

String.prototype.width = function(style) {
	try {
		var s = style || '{font: 12px Arial}';
		var  o = document.createElement('div');
		var t = document.createTextNode(this);
		o.appendChild(t);
		document.getElementsByTagName('body')[0].appendChild(o);

		o.style.position = 'absolute';
		o.style.float = 'left';
		o.style.whiteSpace = 'nowrap';
		o.style.visibility = 'hidden';
		o.style.style = s;
		var w = (o.clientWidth + 1);

		document.getElementsByTagName('body')[0].removeChild(o);

//		TsDygraph.log(TsDygraph.DEBUG,"DYGRAPH.JS returning width: " + w);
		
		return w;
	} catch (e) {
		TsDygraph.log(TsDygraph.ERROR, "String width exception = " + e.message);
	}
}

String.prototype.height = function(style) {
	try {
		var s = style || '{font: 12px Arial}';
		var  o = document.createElement('div');
		var t = document.createTextNode(this);
		o.appendChild(t);
		document.getElementsByTagName('body')[0].appendChild(o);

		o.style.position = 'absolute';
		o.style.float = 'left';
		o.style.whiteSpace = 'nowrap';
		o.style.visibility = 'hidden';
		o.style.style = s;
		var w = (o.clientHeight + 1);// + "px";

		document.getElementsByTagName('body')[0].removeChild(o);

		return w;
	} catch (e) {
		TsDygraph.log(TsDygraph.ERROR, "String width exception = " + e.message);
	}
}
//Leave comment below at end so that this file show up in 
//Chrome Dev tools once it is loaded: https://developer.chrome.com/devtools/docs/javascript-debugging#breakpoints-dynamic-javascript
//# sourceURL=dygraph.js
