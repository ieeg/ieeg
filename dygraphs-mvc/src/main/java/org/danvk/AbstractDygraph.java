/**
 * $Id: AbstractDygraph.java 28 2010-09-10 01:38:03Z steven.jardine $ 
 * Copyright (c) 2010 Steven Jardine, MJN Services, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 */
package org.danvk;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;

/**
 * Event handler implementation for the dygraph. Should be the basis for all
 * dygraph wrapper implementations.
 * 
 * @version $Rev: 28 $
 * @author Steven Jardine
 */
public abstract class AbstractDygraph extends Widget {

    private boolean attached = false;

    private boolean autoDraw = true;

    private Boolean canFormatYValue = false;

    private Boolean canHandleClickEvents = false;

    private Boolean canHandleAnnotationClickEvents = false;

    private Boolean canHandleDoubleClickEvents = false;

    private Boolean canHandleOutOfDataEvents = false;

    private Boolean canHandleDrawEvents = false;

    private Boolean canHandleHighlightEvents = false;

    private Boolean canHandleZoomEvents = false;

    private Boolean canHandleZoomSelEvents = false;

    private Boolean canHandleMoveEvents = false;

    private Boolean canHandleVetEvents = false;

    private Boolean canHandleVisibilityEvents = false;

    private final List<TsDygraphClickHandler> clickHandlers = new ArrayList<TsDygraphClickHandler>();

    private final List<TsDygraphAnnotationClickHandler> annotationClickHandlers = new ArrayList<TsDygraphAnnotationClickHandler>();

    private final List<TsDygraphDoubleClickHandler> doubleClickHandlers = new ArrayList<TsDygraphDoubleClickHandler>();

    private final List<TsDygraphOutOfDataHandler> outOfDataHandlers = new ArrayList<TsDygraphOutOfDataHandler>();

    private final List<TsDygraphDrawHandler> drawHandlers = new ArrayList<TsDygraphDrawHandler>();

    private final List<TsDygraphHighlightHandler> highlightHandlers = new ArrayList<TsDygraphHighlightHandler>();

    private final List<TsDygraphMoveHandler> moveHandlers = new ArrayList<TsDygraphMoveHandler>();

    private final List<TsDygraphVisibilityChangeHandler> visibilityHandlers = new ArrayList<TsDygraphVisibilityChangeHandler>();

    private final List<TsDygraphVetHandler> vetHandlers = new ArrayList<TsDygraphVetHandler>();

    protected String id = null;
    protected Element rootDiv;

    protected DygraphOptions options = null;

    private YValueFormatter yValueFormatter = null;

    private final List<TsDygraphZoomHandler> zoomHandlers = new ArrayList<TsDygraphZoomHandler>();

    private final List<TsDygraphZoomSelHandler> zoomSelHandlers = new ArrayList<TsDygraphZoomSelHandler>();

    private boolean useDouble = false;

    /**
     * Default constructor.
     * 
     * @param id
     *            the id of the div element to hold the graph.
     * @param options
     *            the dygraph options.
     */
    public AbstractDygraph(final String id, final DygraphOptions options) {
		super();
		this.id = id;
		this.options = options;
	
		Element element = Document.get().createDivElement();
		rootDiv = element;
		if (id != null) {
		    element.setId(id);
		}
		setElement(element);
    }

    public void setDouble(boolean useDouble) {
    	this.useDouble = useDouble;
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addClickHandler(final TsDygraphClickHandler handler) {
	if (handler != null) {
	    clickHandlers.add(handler);
	}
	if (!attached && clickHandlers.size() > 0) {
	    setCanHandleClickEvents(true);
	}
    }

    public final void addAnnotationClickHandler(
	    final TsDygraphAnnotationClickHandler handler) {
	if (handler != null) {
	    annotationClickHandlers.add(handler);
	}
	if (!attached && annotationClickHandlers.size() > 0) {
	    setCanHandleAnnotationClickEvents(true);
	}
    }

    public final void addMoveHandler(final TsDygraphMoveHandler handler) {
	if (handler != null) {
	    moveHandlers.add(handler);
	}
	if (!attached && moveHandlers.size() > 0) {
	    setCanHandleMoveEvents(true);
	}
    }

    public final void addVetHandler(final TsDygraphVetHandler handler) {
		if (handler != null) {
		    vetHandlers.add(handler);
		}
		if (!attached && vetHandlers.size() > 0) {
		    setCanHandleVetEvents(true);
		}
    }

    public final void addVisibilityChangeHandler(
	    final TsDygraphVisibilityChangeHandler handler) {
	if (handler != null) {
	    visibilityHandlers.add(handler);
	}
	if (!attached && moveHandlers.size() > 0) {
	    setCanHandleVisibilityChangeEvents(true);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addDoubleClickHandler(
	    final TsDygraphDoubleClickHandler handler) {
	if (handler != null) {
	    doubleClickHandlers.add(handler);
	}
	if (!attached && doubleClickHandlers.size() > 0) {
	    setCanHandleDoubleClickEvents(true);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addOutOfDataHandler(final TsDygraphOutOfDataHandler handler) {
	if (handler != null) {
	    outOfDataHandlers.add(handler);
	}
	if (!attached && outOfDataHandlers.size() > 0) {
	    setCanHandleOutOfDataEvents(true);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addDrawHandler(final TsDygraphDrawHandler handler) {
	if (handler != null) {
	    drawHandlers.add(handler);
	}
	if (!attached && drawHandlers.size() > 0) {
	    setCanHandleDrawEvents(true);
	}
    }

    /**
     * Adds handlers to the graph.
     */
    protected final void addHandlers() {
	JavaScriptObject jsOptions = options.getJsObject();
	if (canFormatYValue) {
	    setYValueFormatter(this, jsOptions);
	}
	if (canHandleClickEvents) {
	    setClickCallback(this, jsOptions);
	}
	if (canHandleAnnotationClickEvents) {
	    setAnnotationClickCallback(this, jsOptions);
	}
	if (canHandleDoubleClickEvents) {
	    setDoubleClickCallback(this, jsOptions);
	}
	if (canHandleOutOfDataEvents) {
	    setOutOfDataCallback(this, jsOptions);
	}
	if (canHandleDrawEvents) {
	    setDrawCallback(this, jsOptions);
	}
	if (canHandleZoomEvents) {
	    setZoomCallback(this, jsOptions);
	}
	if (canHandleZoomSelEvents) {
	    setZoomSelCallback(this, jsOptions);
	}
	if (canHandleHighlightEvents) {
	    setHighlightCallback(this, jsOptions);
	}
	if (canHandleMoveEvents) {
	    setMoveCallback(this, jsOptions);
	}
	if (canHandleVetEvents) {
	    setVetCallback(this, jsOptions);
	}
	if (canHandleVisibilityEvents) {
	    setVisibilityChangeCallback(this, jsOptions);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addHighlightHandler(final TsDygraphHighlightHandler handler) {
	if (handler != null) {
	    highlightHandlers.add(handler);
	}
	if (!attached && highlightHandlers.size() > 0) {
	    setCanHandleHighlightEvents(true);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addZoomHandler(final TsDygraphZoomHandler handler) {
	if (handler != null) {
	    zoomHandlers.add(handler);
	}
	if (!attached && zoomHandlers.size() > 0) {
	    setCanHandleZoomEvents(true);
	}
    }

    /**
     * Adds a click handler to the dygraph. If the dygraph hasn't been attached
     * we will assume the user wants to auto inject the handler code for this
     * event.
     * 
     * @param handler
     *            the handler to add.
     */
    public final void addZoomSelHandler(final TsDygraphZoomSelHandler handler) {
	if (handler != null) {
	    zoomSelHandlers.add(handler);
	}
	if (!attached && zoomSelHandlers.size() > 0) {
	    setCanHandleZoomSelEvents(true);
	}
    }

    /**
     * Responsible for actually drawing the dygraph. If autoDraw is false then
     * this method must be called to render the dygraph.
     */
    public abstract void draw();

    /**
     * Format the y value.
     * 
     * @param yValue
     *            the value to format
     * @return the formatter value.
     */
    protected final String formatYValue(final double yValue) {
	String result = null;
	if (yValueFormatter != null) {
	    result = yValueFormatter.format(yValue);
	} else {
	    // CHECKSTYLE:OFF
	    result = String.valueOf((double) Math.round(yValue * 100) / 100);
	    // CHECKSTYLE:ON
	}
	return result;
    }

    /**
     * @return the autoDraw
     */
    public final boolean getAutoDraw() {
	return autoDraw;
    }

    /**
     * @return the canFormatYValue
     */
    public final Boolean getCanFormatYValue() {
	return canFormatYValue;
    }

    /**
     * @return the canHandleClickEvents
     */
    public final Boolean getCanHandleClickEvents() {
	return canHandleClickEvents;
    }

    /**
     * @return the canHandleDrawEvents
     */
    public final Boolean getCanHandleDrawEvents() {
	return canHandleDrawEvents;
    }

    /**
     * @return the canHandleHighlightEvents
     */
    public final Boolean getCanHandleHighlightEvents() {
	return canHandleHighlightEvents;
    }

    /**
     * @return the canHandleZoomEvents
     */
    public final Boolean getCanHandleZoomEvents() {
	return canHandleZoomEvents;
    }

    /**
     * @return the canHandleZoomEvents
     */
    public final Boolean getCanHandleZoomSelEvents() {
	return canHandleZoomSelEvents;
    }

    public final Boolean getCanHandleMoveEvents() {
    	return canHandleMoveEvents;
    }

    public final Boolean getCanHandleVetEvents() {
    	return canHandleVetEvents;
    }

    public final Boolean getCanHandleVisibilityChangeEvents() {
	return canHandleVisibilityEvents;
    }

    /**
     * @return the id to get.
     */
    public final String getId() {
	return id;
    }
    
    public final Element getRootDiv() {
    	return rootDiv;
    }

    /** {@inheritDoc} */
    @Override
    protected final void onAttach() {
		attached = true;
		addHandlers();
		super.onAttach();
    }
    
    public void attach() {
    	onAttach();
    }

    /**
     * Handle the onclick event.
     * 
     * @param event
     *            the event.
     * @param dateDbl
     *            the date.
     * @param points
     *            the points on the y axis.
     */
    protected final void onClick(final MouseEvent event, final double dateDbl,
	    final Points points) {
		if (useDouble) {
		    for (TsDygraphClickHandler handler : clickHandlers)
			handler.onClick(event, dateDbl, points);
		} else {
		    Date date = new Date(Math.round(dateDbl));
		    for (TsDygraphClickHandler handler : clickHandlers)
			handler.onClick(event, date, points);
		}
    }

    protected final void onAnnotationClick(final GraphAnnotation annotation,
	    final Point point, final MouseEvent event) {
		for (TsDygraphAnnotationClickHandler handler : annotationClickHandlers)
		    handler.onClick(annotation, point, event);
    }

    /**
     * Handle the onclick event.
     * 
     * @param event
     *            the event.
     * @param dateDbl
     *            the date.
     * @param points
     *            the points on the y axis.
     */
    protected final void onDoubleClick(final MouseEvent event,
	    final double startDbl, final double dateDbl, final Points points) {

	if (useDouble) {
	    for (TsDygraphDoubleClickHandler handler : doubleClickHandlers)
		handler.onDoubleClick(event, startDbl, dateDbl, points);
	} else {
	    Date start = new Date(Math.round(startDbl));
	    Date date = new Date(Math.round(dateDbl));
	    for (TsDygraphDoubleClickHandler handler : doubleClickHandlers)
		handler.onDoubleClick(event, start, date, points);
	}
    }

    /**
     * Handle the onclick event.
     * 
     * @param event
     *            the event.
     * @param dateDbl
     *            the date.
     * @param points
     *            the points on the y axis.
     */
    protected final void onOutOfData(final double newMinX,
	    final double newMaxX, final double oldMinX, final double oldMaxX) {
		for (TsDygraphOutOfDataHandler handler : outOfDataHandlers)
		    handler.onOutOfData(newMinX, newMaxX, oldMinX, oldMaxX);
    }

    /**
     * Handle the onDraw event.
     * 
     * @param startDateDbl
     *            the start date.
     * @param endDateDbl
     *            the end date.
     * @param isInitial
     *            is this the initial draw?
     */
    protected final void onDraw(final double startDateDbl,
	    final double endDateDbl, final boolean isInitial) {
		if (useDouble) {
		    for (TsDygraphDrawHandler handler : drawHandlers)
			handler.onDraw(startDateDbl, endDateDbl);
		} else {
		    Date startDate = new Date(Math.round(startDateDbl));
		    Date endDate = new Date(Math.round(endDateDbl));
		    for (TsDygraphDrawHandler handler : drawHandlers) {
			handler.onDraw(startDate, endDate);
		    }
		}
    }

    /**
     * Handle the onHighlight event.
     * 
     * @param event
     *            the event.
     * @param dateDbl
     *            the date.
     * @param points
     *            the points on the y axis.
     */
    protected final void onHighlight(final MouseEvent event,
	    final double dateDbl, final Points points) {
		if (useDouble) {
		    for (TsDygraphHighlightHandler handler : highlightHandlers)
			handler.onHighlight(event, dateDbl, points);
		} else {
		    Date date = new Date(Math.round(dateDbl));
		    for (TsDygraphHighlightHandler handler : highlightHandlers) {
			handler.onHighlight(event, date, points);
		    }
		}
    }

    /**
     * Handle the onHighlight event.
     * 
     * @param event
     *            the event.
     */
    protected final void onUnhighlight(final MouseEvent event) {
		for (TsDygraphHighlightHandler handler : highlightHandlers) {
		    handler.onUnhighlight(event);
		}
    }

    /**
     * Handle the onZoom event.
     * 
     * @param startDateDbl
     *            the start date.
     * @param endDateDbl
     *            the end date.
     * @param yScale
     *            yZoom.
     */
    protected final void onZoom(final double startDateDbl,
	    final double endDateDbl, final double yScale) {
	if (useDouble) {
	    for (TsDygraphZoomHandler handler : zoomHandlers)
		handler.onZoom(startDateDbl, endDateDbl, yScale);
	} else {
	    Date startDate = new Date(Math.round(startDateDbl));
	    Date endDate = new Date(Math.round(endDateDbl));
	    for (TsDygraphZoomHandler handler : zoomHandlers) {
		handler.onZoom(startDate, endDate, yScale);
	    }
	}
    }

//    /**
//     * Handle the onZoom event.
//     * 
//     * @param startDateDbl
//     *            the start date.
//     * @param endDateDbl
//     *            the end date.
//     */
    protected final void onZoomSel(final double startDateDbl,
	    final double endDateDbl, int startY, int endY,
	    double oldStart, double oldEnd, double oldPeriod) {
//    	System.err.println("Initial callback");
		for (TsDygraphZoomSelHandler handler : zoomSelHandlers)
		    handler.onZoomSel(startDateDbl, endDateDbl, startY, endY,
		    		oldStart, oldEnd, oldPeriod);
    }

    protected final void onMove(final double newMinX, final double newMaxX,
	    final double period) {
		// System.err.println("Moving " + newMinX + " / " + newMaxX + " / "
		// + period);
		for (TsDygraphMoveHandler handler : moveHandlers)
		    handler.onMove(newMinX, newMaxX, period);
		// System.err.println("Exiting");
    }

    protected final void onVet(final String annot, boolean flag) {
    	for (TsDygraphVetHandler handler : vetHandlers)
    	    handler.onVet(annot, flag);
        }

    protected final void onVisibilityChange(final String name,
	    final boolean visible) {
	for (TsDygraphVisibilityChangeHandler handler : visibilityHandlers)
	    handler.onVisibilityChange(name, visible);
    }

    /**
     * @param autoDraw
     *            the autoDraw to set
     */
    public final void setAutoDraw(final boolean autoDraw) {
	this.autoDraw = autoDraw;
    }

    /**
     * @param canFormatYValue
     *            the canFormatYValue to set
     */
    public final void setCanFormatYValue(final Boolean canFormatYValue) {
	this.canFormatYValue = canFormatYValue;
    }

    /**
     * @param canHandleClickEvents
     *            the canHandleClickEvents to set
     */
    public final void setCanHandleClickEvents(final Boolean canHandleClickEvents) {
	this.canHandleClickEvents = canHandleClickEvents;
    }

    /**
     * @param canHandleClickEvents
     *            the canHandleClickEvents to set
     */
    public final void setCanHandleAnnotationClickEvents(
	    final Boolean canHandleAnnotationClickEvents) {
	this.canHandleAnnotationClickEvents = canHandleAnnotationClickEvents;
    }

    /**
     * @param canHandleClickEvents
     *            the canHandleClickEvents to set
     */
    public final void setCanHandleDoubleClickEvents(
	    final Boolean canHandleDoubleClickEvents) {
	this.canHandleDoubleClickEvents = canHandleDoubleClickEvents;
    }

    public final void setCanHandleOutOfDataEvents(
	    final Boolean canHandleOutOfDataEvents) {
	this.canHandleOutOfDataEvents = canHandleOutOfDataEvents;
    }

    public final void setCanHandleMoveEvents(final Boolean canHandleMoveEvents) {
    	this.canHandleMoveEvents = canHandleMoveEvents;
    }

    public final void setCanHandleVetEvents(final Boolean canHandleVetEvents) {
    	this.canHandleVetEvents = canHandleVetEvents;
    }

    public final void setCanHandleVisibilityChangeEvents(
	    final Boolean canHandleVisibilityChangeEvents) {
	this.canHandleVisibilityEvents = canHandleVisibilityChangeEvents;
    }

    /**
     * @param canHandleDrawEvents
     *            the canHandleDrawEvents to set
     */
    public final void setCanHandleDrawEvents(final Boolean canHandleDrawEvents) {
	this.canHandleDrawEvents = canHandleDrawEvents;
    }

    /**
     * @param canHandleHighlightEvents
     *            the canHandleHighlightEvents to set
     */
    public final void setCanHandleHighlightEvents(
	    final Boolean canHandleHighlightEvents) {
	this.canHandleHighlightEvents = canHandleHighlightEvents;
    }

    /**
     * @param canHandleZoomEvents
     *            the canHandleZoomEvents to set
     */
    public final void setCanHandleZoomEvents(final Boolean canHandleZoomEvents) {
	this.canHandleZoomEvents = canHandleZoomEvents;
    }

    /**
     * @param canHandleZoomEvents
     *            the canHandleZoomEvents to set
     */
    public final void setCanHandleZoomSelEvents(
	    final Boolean canHandleZoomSelEvents) {
	this.canHandleZoomSelEvents = canHandleZoomSelEvents;
    }

    /**
     * Sets the click callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setClickCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["clickCallback"] = function(e, x, pts) {
			graph.@org.danvk.AbstractDygraph::onClick(Lorg/danvk/MouseEvent;DLorg/danvk/Points;)(e,x,pts);
		}
    }-*/;

    private native void setAnnotationClickCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["annotationClickHandler"] = function(a, pt, g, e) {
			//			console.debug("Annotation clicked: " + a.text + "@" + a.xval);
			//			console.debug("Annotation point: " + JSON.stringify(pt));
			//			console.debug("Annotation event: " + JSON.stringify(e));
			// Ignore dygraph g
			graph.@org.danvk.AbstractDygraph::onAnnotationClick(Lorg/danvk/GraphAnnotation;Lorg/danvk/Point;Lorg/danvk/MouseEvent;)(a,pt,e);
		}
    }-*/;

    /**
     * Sets the click callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setDoubleClickCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["doubleClickCallback"] = function(e, s, x, pts) {
			graph.@org.danvk.AbstractDygraph::onDoubleClick(Lorg/danvk/MouseEvent;DDLorg/danvk/Points;)(e,s,x,pts);
		}
    }-*/;

    private native void setOutOfDataCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
        opts["outOfDataCallback"]=function(newXMin, newXMax, oldXMin, oldXMax){
        graph.@org.danvk.AbstractDygraph::onOutOfData(DDDD)(parseFloat(newXMin),
        parseFloat(newXMax),parseFloat(oldXMin),parseFloat(oldXMax));
        }
    }-*/;

    private native void setMoveCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["moveCallback"] = function(newXMin, newXMax, period) {
			graph.@org.danvk.AbstractDygraph::onMove(DDD)(newXMin,newXMax,period);
		}
    }-*/;

    private native void setVetCallback(final AbstractDygraph graph,
    	    final JavaScriptObject opts)/*-{
    		opts["vetCallback"] = function(annot, isVetted) {
    			graph.@org.danvk.AbstractDygraph::onVet(Ljava/lang/String;Z)(annot,isVetted);
    		}
        }-*/;

    private native void setVisibilityChangeCallback(
	    final AbstractDygraph graph, final JavaScriptObject opts)/*-{
		opts["visibilityCallback"] = function(name, visible) {
			graph.@org.danvk.AbstractDygraph::onVisibilityChange(Ljava/lang/String;Z)(name,visible);
		}
    }-*/;

    /**
     * Sets the draw callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setDrawCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["drawCallback"] = function(dygraph, isInitial) {
			var min = dygraph.xAxisRange()[0];
			var max = dygraph.xAxisRange()[1];
			graph.@org.danvk.AbstractDygraph::onDraw(DDZ)(min,max,isInitial);
		}
    }-*/;

    /**
     * Sets the zoom callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setHighlightCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["highlightCallback"] = function(e, x, pts) {
			graph.@org.danvk.AbstractDygraph::onHighlight(Lorg/danvk/MouseEvent;DLorg/danvk/Points;)(e,x,pts);
		}
		opts["unhighlightCallback"] = function(e) {
			graph.@org.danvk.AbstractDygraph::onUnhighlight(Lorg/danvk/MouseEvent;)(e);
		}
    }-*/;

    /**
     * Sets the yValueFormatter.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to set.
     */
    private native void setYValueFormatter(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["yValueFormatter"] = function(x) {
			return graph.@org.danvk.AbstractDygraph::formatYValue(D)(x);
		}
    }-*/;

    /**
     * Sets the Y value formatter. If the dygraph hasn't been attached we will
     * assume the user wants to auto inject the formatter code for this event.
     * 
     * @param formatter
     *            the y value formatter to set.
     */
    public final void setYValueFormatter(final YValueFormatter formatter) {
	if (formatter != null) {
	    yValueFormatter = formatter;
	    if (!attached) {
		setCanFormatYValue(true);
	    }
	}
    }

    /**
     * Sets the zoom callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setZoomCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["zoomCallback"] = function(min, max,scale) {
			graph.@org.danvk.AbstractDygraph::onZoom(DDD)(min,max,scale);
		}
    }-*/;

    /**
     * Sets the zoom callback.
     * 
     * @param graph
     *            the graph to set.
     * @param opts
     *            the options to use.
     */
    private native void setZoomSelCallback(final AbstractDygraph graph,
	    final JavaScriptObject opts)/*-{
		opts["zoomSelCallback"] = function(min, max, start, end, oldMin, oldMax, oldPer) {
//  	console.debug("Callback with " + oldMin + "-" + oldMax + " and period " + oldPer);
			graph.@org.danvk.AbstractDygraph::onZoomSel(DDIIDDD)(parseFloat(min),
			parseFloat(max),parseInt(start),parseInt(end),
			parseFloat(oldMin), parseFloat(oldMax), parseFloat(oldPer));
		}
    }-*/;
}
