package org.danvk;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.core.client.JsArrayString;

public class ColorMap {
	Map<String, String> channelColors = new HashMap<String, String>();
	Map<String, String> layerColors = new HashMap<String, String>();
	
	public ColorMap() {
		
	}
	
	public ColorMap(JsArrayString initialColors, JsArrayString labels) {
		setInitial(initialColors, labels);
	}
	
	public void setInitial(JsArrayString initialColors, JsArrayString labels) {
		if (initialColors.length() != labels.length())
			throw new RuntimeException("Mismatched colors + labels");
		
		for (int i = 0; i < labels.length(); i++) {
			channelColors.put(labels.get(i), initialColors.get(i));
		}
	}

	public void setAnnotationColor(final String annLayer, final String color) {
		layerColors.put(annLayer, color);
	}
	
	public String getAnnotationColor(final String annLayer) {
		return layerColors.get(annLayer);
	}
	
	public void setChannelColor(final String annLayer, final String color) {
		channelColors.put(annLayer, color);
	}
	
	public String getChannelColor(final String label) {
		return channelColors.get(label);
	}

	public native JsArrayString getChannelColorMap(JsArrayString labels) /*-{
		var ret = [];
		var cc = this.@org.danvk.ColorMap::channelColors;
		for (var i = 0; i < labels.length; i++) {
			ret.push(cc.get(labels[i]));
		}
		return ret;
	}-*/;
}
