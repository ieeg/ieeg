package org.danvk;

import com.google.gwt.core.client.JsArrayInteger;

public interface TsDygraphPointMapper {

	public int getYCoordinate(int channelIndex, int channelBaselineY,
			JsArrayInteger otherChannels, boolean invertChannel, double scaleFactor);
}
