/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import java.net.UnknownHostException;

public class IEEGJobScheduler {
	public static String url = "http://somehost";
	public static String user = "xyz";
	public static String password = "123";
	public static String jobTrackerUrl;
	public static void usage() {
		System.out
				.println("Usage: wsexample [url] [jobTrackerUrl] [userid] [password] [studyRevId]");

	}

	private static String studyRevId = "123-456-789";


	public static void main(final String[] args) throws UnknownHostException, InterruptedException {
		if (args.length > 0
				&& (args[0].equals("-h") || args[0].equals("/h")
						|| args[0].equals("--h") ||
				args[0].equals("--help"))) {
			usage();
		}

		if (args.length >= 1)
			url = args[0];

		if (args.length >= 2)
			jobTrackerUrl = args[1];

		if (args.length >= 3)
			user = args[2];
		if (args.length >= 4)
			password = args[3];

		if (password.length() == 0) {
			usage();
			throw new RuntimeException("Password not specified");
		}
		if (args.length >= 5) {
			studyRevId = args[4];
		}


		System.out.println("Connecting to Web Service, user = " + user);
		final TimeSeriesInterface tsInterface = new TimeSeriesInterface("123-456-789", url
				+ "/services",
				user, password, jobTrackerUrl);
		
//		final JobInterface ji = new JobInterface(url + "/services", user, password);
		
		do {
			studyRevId = tsInterface.getNewJob();
			System.out.println("Request: " + studyRevId);
			if (studyRevId.isEmpty())
				Thread.sleep(1000);
		} while (studyRevId.isEmpty());
			
//			boolean scheduled = ji.scheduleJob(studyRevId);
			
	//		if (scheduled) {
//				String job = ji.getNewJob();
		
//				System.out.println("Got job " + job);
//				
//				String xml = ji.getTaskXML(job);
//				
//				System.out.println(xml);
				
				TimeSeriesTask task = tsInterface.getFirstTimeSeriesTask(studyRevId);
				
//				xml = ji.getTaskXML(job);
				while (task != null && !task.isEmpty()) {
					System.out.println("Executing task " + task.getName() + " with channels ");
					for (String s : task.getChannels())
						System.out.println(s);
					
					task = tsInterface.getNextTimeSeriesTask(studyRevId, task.getName());
//					xml = ji.getTaskXML(job);
//					System.out.println(xml);
				}
		
//		}
	}

}
