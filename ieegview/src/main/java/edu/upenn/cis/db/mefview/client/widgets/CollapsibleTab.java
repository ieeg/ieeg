/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;

public class CollapsibleTab extends FlowPanel implements Attachable {
	Image img;
	Label lab;

	public CollapsibleTab(String name, String icon, boolean collapses, boolean closes,
			ClickHandler closeHandler) {
	
		if (icon != null && !icon.isEmpty()) {
			img = new Image(icon);
//			img.setHeight("20px");
//			img.setWidth("20px");
			img.setPixelSize(20, 20);
			img.setAltText(name);
			img.getElement().getStyle().setFloat(Float.LEFT);
			add(img);
		}

		lab = new Label(name);
        lab.setStyleName("tinyLabel");
        lab.getElement().getStyle().setFloat(Float.LEFT);
        add(lab);

        init(collapses, closes, closeHandler);
	}

	public CollapsibleTab(String name, ImageResource icon, boolean collapses, boolean closes,
			ClickHandler closeHandler) {
//	  this.getElement().getStyle().setFloat(Float.LEFT);
		
		
		
		if (icon != null) {
			img = new Image(icon.getSafeUri());
//			img.setHeight("20px");
//			img.setWidth("20px");
			img.setPixelSize(20, 20);
			img.setAltText(name);
			img.getElement().getStyle().setFloat(Float.LEFT);
			add(img);
		}
		
		lab = new Label(name);
        lab.setStyleName("tinyLabel");
        lab.getElement().getStyle().setFloat(Float.LEFT);
        add(lab);

        init(collapses, closes, closeHandler);
	}
	
	private void init(boolean collapses, boolean closes, ClickHandler closeHandler) {
		if (collapses) {
			lab.setVisible(false);
			img.addMouseOverHandler(new MouseOverHandler() {
	
				@Override
				public void onMouseOver(MouseOverEvent event) {
					lab.setVisible(true);
				}
				
			});
			
			img.addMouseOutHandler(new MouseOutHandler() {
	
				@Override
				public void onMouseOut(MouseOutEvent event) {
					lab.setVisible(false);
				}
			});
			
			lab.addMouseOverHandler(new MouseOverHandler() {
	
				@Override
				public void onMouseOver(MouseOverEvent event) {
					lab.setVisible(true);
				}
				
			});
			
			lab.addMouseOutHandler(new MouseOutHandler() {
	
				@Override
				public void onMouseOut(MouseOutEvent event) {
					lab.setVisible(false);
				}
			});
		}
		if (closes) {
//		    Image closeIm = new Image("images/closeTab.png");
//		    closeIm.setPixelSize(20, 20);
			final PushButton closeImage = new PushButton();
			closeImage.setStyleName("tabCloseButton");
			closeImage.getElement().getStyle().setFloat(Float.LEFT);
			
			closeImage.setPixelSize(15, 15);
//			closeImage.getElement().getStyle().setPaddingTop(0, Unit.PX);
//			closeImage.getElement().getStyle().setPaddingBottom(4, Unit.PX);

			closeImage.addClickHandler(closeHandler);
			add(closeImage);
		}
	}

	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
