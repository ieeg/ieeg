/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.List;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.combo.ComboBoxChangeEvent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IEventBus;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.AnnotationCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationGroupCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationGroupRemovedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationLayerEditedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRemovedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationSchemeEditedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationViewRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.ChangedViewEvent;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.AnnotationSet;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.SnapshotAnnotationModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.CreateAnnotationLayer;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.EditAnnotationLayer;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.ShowAnnotations;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.widgets.SimpleDataGrid;
import edu.upenn.cis.db.mefview.client.widgets.StringComboBox;
import edu.upenn.cis.db.mefview.client.widgets.StringEnterHandler;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class AnnotationGroupPane extends LayoutPanel 
implements ChangedViewEvent.Handler, 
AnnotationGroupCreatedEvent.Handler,AnnotationGroupRemovedEvent.Handler,
AnnotationCreatedEvent.Handler, AnnotationRemovedEvent.Handler,
AnnotationRefreshEvent.Handler, AnnotationViewRefreshEvent.Handler,
AnnotationLayerEditedEvent.Handler, AnnotationSchemeEditedEvent.Handler{

  static AnnotationSet<Annotation> annSet = new AnnotationSet<Annotation>();

  ClientFactory clientFactory;
  SessionModel sessionModel;
  IEEGPane display;
  boolean haveModel = false;

  Button annotations = new Button("Layer Details");
  Button remLayerButton = new Button("Remove selected Layer");
  Button createNew = new Button("New Layer"); 
  
   

  ComboBoxDataModel annGroups;
  
  // Active Layer Selector
  StringComboBox<ComboBoxDataModel> theAnnGroup = new StringComboBox<ComboBoxDataModel>(false);
  
  // All Layer List
  SimpleDataGrid<AnnotationGroup<Annotation>> annList;
  
  // Bottom Button Bar
//  HorizontalPanel buttonBar = new HorizontalPanel();

  DataSnapshotModel currentModel;
  List<AnnotationGroup<Annotation>> currentGroup = new ArrayList<AnnotationGroup<Annotation>>();

  public final TextColumn<AnnotationGroup<Annotation>> annStudy = new TextColumn<AnnotationGroup<Annotation>> () {
    @Override
    public String getValue(AnnotationGroup<Annotation> object) {
      return currentModel.getFriendlyName();
    }
  };

  public AnnotationGroupPane(IEEGPane pane, SessionModel theSessionModel, DataSnapshotModel theModel,
		  ClientFactory factory) {
//    this.factory = theMessageBus;
	  this.clientFactory = factory;
	  this.setStylePrimaryName("gwt-StackLayoutPanel gwt-StackLayoutPanelContent-managePanel");
	  this.sessionModel = theSessionModel;

	  currentModel = theModel;
	  annGroups = new ComboBoxDataModel();
	  theAnnGroup.setModel(annGroups);
	  display = pane;

    
    
    theAnnGroup.addChangeHandler(new ChangeHandler() {

      @Override
      public void onChange(ChangeEvent event) {
        if (currentModel == null)
          return;

        if (event instanceof ComboBoxChangeEvent) {
          int row = ((ComboBoxChangeEvent)event).getRow();
          String name = (String)theAnnGroup.getModel().get(row);
          theAnnGroup.setValue(name);

          AnnotationGroup<Annotation> grp = currentModel.getAnnotationModel().getRootGroup();
          if (grp == null) {
            System.err.println("Unable to find root group");
            return;
          }
          grp = grp.getGroup(name);
          if (grp != null) {
            currentModel.getAnnotationModel().selectGroup(grp);
          } else {
            System.err.println("Unable to find root group name");
            return;
          }
        }
      }

    });

    theAnnGroup.setEnterHandler(new StringEnterHandler() {

      @Override
      public void handleEnterRequest(String name) {
        AnnotationGroup<Annotation> grp = currentModel.getAnnotationModel().getRootGroup();
        if (grp == null) {
          System.err.println("Unable to find root group");
          return;
        }
        grp = grp.getGroup(name);
        if (grp != null) {
          currentModel.getAnnotationModel().selectGroup(grp);
        } else {
          AnnotationGroup<Annotation> newGrp = new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, name, false);
          currentModel.getAnnotationModel().addOrExtendGroup(newGrp);
          currentModel.getAnnotationModel().selectGroup(newGrp);
        }
      }

    });

    annSet.annColumn.setSortable(true);
    annSet.annStudy.setSortable(true);
    List<String> annColNames = new ArrayList<String>();
    List<TextColumn<AnnotationGroup<Annotation>>> annColumns = 
        new ArrayList<TextColumn<AnnotationGroup<Annotation>>>();

    annColNames.add("Name");
    annColumns.add(AnnotationGroup.annColumn);
    annColNames.add("Count");
    annColumns.add(AnnotationGroup.annCountColumn);
    annColNames.add("Style");
    annColumns.add(AnnotationGroup.annScheme);

    annList = new SimpleDataGrid<AnnotationGroup<Annotation>>(10, true, true, true,
        currentGroup, AnnotationGroup.KEY_PROVIDER, annColNames, 
        annColumns);
    annList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

    annList.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
      @Override
      public void onSelectionChange(SelectionChangeEvent event) {
        for (AnnotationGroup<Annotation> grp: annList.getList()) {
          if (annList.getSelectionModel().isSelected(grp)) {
             //GWT.log("Toggling annotation group " + grp.getName() + " to ON");
            grp.setEnabled(true);
          } else {
            // GWT.log("Toggling annotation group " + grp.getName() + " to OFF");
            grp.setEnabled(false);
          }
        }

        if (currentModel != null)
        	IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRefreshEvent(currentModel.getSnapshotID()));
      }
    });

    HorizontalPanel gp = new HorizontalPanel();
    gp.add(new Label("Active Layer:"));
    gp.add(theAnnGroup);
    add(gp);
    setWidgetTopHeight(gp, 5, Unit.PX, 50, Unit.PX);
    setWidgetLeftRight(gp, 5, Unit.PX, 5, Unit.PX);

    add(annList);
    setWidgetTopBottom(annList, 50, Unit.PX, 50, Unit.PX);
    setWidgetLeftRight(annList, 1, Unit.PX, 1, Unit.PX);

    // Annotations  Properties  Copy  Merge  Export
    Button createNew = new Button("New Layer");
    Button editButton = new Button("Edit Layer");
    
    
    HorizontalPanel hp = new HorizontalPanel();

    hp.add(createNew);
    hp.add(editButton);
    hp.add(remLayerButton);

    add(hp);
    setWidgetBottomHeight(hp, 1, Unit.PX, 44, Unit.PX);
    setWidgetLeftRight(hp, 1, Unit.PX, 1, Unit.PX);
    
    IEventBus bus = IeegEventBusFactory.getGlobalEventBus();

    bus.registerHandler(ChangedViewEvent.getType(), this);

    bus.registerHandler(AnnotationGroupCreatedEvent.getType(), this);
    bus.registerHandler(AnnotationGroupRemovedEvent.getType(), this);

    bus.registerHandler(AnnotationCreatedEvent.getType(), this);
    bus.registerHandler(AnnotationLayerEditedEvent.getType(), this);
    bus.registerHandler(AnnotationRemovedEvent.getType(), this);

    bus.registerHandler(AnnotationRefreshEvent.getType(), this);
    bus.registerHandler(AnnotationViewRefreshEvent.getType(), this);
    bus.registerHandler(AnnotationSchemeEditedEvent.getType(), this);

    editButton.addClickHandler(new ClickHandler() {
      
      @Override
      public void onClick(ClickEvent arg0) {
        
        int selIdx = annList.getKeyboardSelectedRow();
        if (selIdx >= 0){
          AnnotationGroup<Annotation> selRow = annList.get(selIdx);
        
        //SnapshotAnnotationModel annModel = currentModel.getAnnotationModel();
        EditAnnotationLayer ca = new EditAnnotationLayer(
            selRow, currentModel, sessionModel);
        ca.center();
        ca.show();
        }
        
      }
      
    });
    
    createNew.addClickHandler(new ClickHandler() {

      @Override
      public void onClick(ClickEvent arg0) {
        CreateAnnotationLayer ca = new CreateAnnotationLayer(currentModel, sessionModel);
        ca.center();
        ca.show();
      }

    });
    
    remLayerButton.addClickHandler(new ClickHandler() {

      @Override
      public void onClick(ClickEvent arg0) {
        
        AnnotationGroup<Annotation> sel = annList.get(annList.getKeyboardSelectedRow());
        currentModel.getAnnotationModel().removeAnnotationGroup(sel);

        
      }

    });
    
   
  }


  @Override
  public void onChangedView(AstroPanel pane) {
    selectAnnotationGroup(pane.getDataSnapshotController().getState());
  }

  public void selectAnnotationGroup(final DataSnapshotModel model) {
    if (model == null)
      return;

    currentModel = model;//.getState();

    if (!haveModel) {
      annotations.addClickHandler(new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
//          model.listAnnotations(400);
        		boolean needToSave = false;
        		
        		for (Annotation ann: currentModel.getAnnotationModel().getAllEnabledAnnotations()) {
        			if (ann.getRevId() == null)
        				needToSave = true;
        		}
        		
        		ShowAnnotations s = new ShowAnnotations(currentModel,//studyID, getTraceInfo(), 
        				currentModel.getAnnotationDefinitions(), 400, needToSave,
        				clientFactory);
        		s.center();
        		s.show();
        }

      });
      haveModel = true;
    }

    SnapshotAnnotationModel ann = currentModel.getAnnotationModel();
    AnnotationGroup<Annotation> group = ann.getRootGroup();

    try {
      annList.clear();
      annGroups.clear();
    } catch (Exception e) {
      GWT.log(e.getMessage());
    }
    if (group != null) {
      for (AnnotationGroup<Annotation> sub: group.getSubGroups()) {
        annList.add(sub);
        if (sub.isEnabled())
          annList.getSelectionModel().setSelected(sub, true);
        annGroups.add(sub.getName(), sub.getName());
      }
    }
    if (currentModel.getAnnotationModel().getSelectedGroup() != null)
      theAnnGroup.setValue(currentModel.getAnnotationModel().getSelectedGroup().getName());
  }

  public void refresh() {
    annList.redraw();
  }

  @Override
  public void onAnnotationGroupCreated(DataSnapshotModel model,
      AnnotationGroup<Annotation> group) {

    boolean found = false;

    if (group == null)
      throw new RuntimeException("Null group!");
    if (model == null)
      throw new RuntimeException("No model!");

    for (AnnotationGroup<Annotation> g: annList.getList())
      if (g.getName().equals(group.getName())) {
        found = true;
        break;
      }

    if (!found && model == currentModel) {
      annList.add(group);
      if (group.isEnabled())
        annList.getSelectionModel().setSelected(group, true);
    }

    found = false;
    for (int i = 0; i < annGroups.getCount(); i++) {
      String one = (String)annGroups.get(i);
      String two = group.getName();
      if (one == null)
        System.err.println("Error - empty group item");
      if (two == null)
        System.err.println("Error - null group lookup");
      if ((one).equals(two)) {
        found = true;
        break;
      }
    }
    if (!found && model == currentModel) {
      // Add group to pull-down scheme list
      annGroups.add(group.getName(), group.getName());
      // Set Active Layer PullDown menu
      theAnnGroup.setValue(group.getName());
      
    }

  }
  
  

  @Override
  public void onAnnotationRemoved(DataSnapshotModel action,
      Annotation a) {
    if (action == currentModel)
      annList.redraw();
  }

  @Override
  public void onAnnotationLayerEdited(DataSnapshotModel action,
      AnnotationGroup<Annotation> group) {
    if (action == currentModel){
      annList.redraw();
      IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRefreshEvent(currentModel.getSnapshotID()));
    }
    
  }
  
  @Override
  public void onAnnotationCreated(DataSnapshotModel action,
      AnnotationGroup<Annotation> group, Annotation a) {
    if (action == currentModel)
      annList.redraw();
  }

  public AnnotationGroup<Annotation> getGroupFor(DataSnapshotViews snapshot) {
    return snapshot.getState().getAnnotationModel().getRootGroup();
    //		return groups.get(snapshot).getRootGroup();
  }

  @Override
  public void onAnnotationRefresh(String action) {
    annList.redraw();
  }

  @Override
  public void onChangedView() {
    annList.redraw();
  }

  public void close() {
	  IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
  }

  @Override
  public void onAnnotationSchemeEditedEvent() {
    annList.redraw();
    
  }


  @Override
  public void onAnnotationGroupRemoved(DataSnapshotModel action,
      AnnotationGroup<Annotation> group,
      AnnotationGroup<Annotation> selectedGroup) {

    // Remove from annGroups
    annGroups.remove(group.getName());
    
    // Remove from annList
    for (AnnotationGroup<Annotation> g: annList.getList())
      if (g.getName().equals(group.getName())) {
        annList.getProvider().getList().remove(g);
        annList.redraw();
        break;
      }
      
      if (selectedGroup != null){
        theAnnGroup.setValue(selectedGroup.getName());
      } else {
        theAnnGroup.setValue(null);
      }
      
      display.refreshAnnotationsOnGraph(true);
      display.refresh();
      display.resetFocus();
    
    
  }

}