/*
 * Copyright 2015 IEEG.org 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;


import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
//import edu.upenn.cis.db.mefview.shared.EEGMontage;

public class EegMontageChangedEvent extends GwtEvent<EegMontageChangedEvent.Handler> {
    private static final Type<EegMontageChangedEvent.Handler> TYPE = new Type<EegMontageChangedEvent.Handler>();
    
//    private final int selected;
    private final EEGMontage montage;
    IEEGViewerPresenter pane;
    
    public EegMontageChangedEvent(IEEGViewerPresenter window, EEGMontage montage){
        this.montage = montage;
        this.pane = window;
    }
    
    public static com.google.gwt.event.shared.GwtEvent.Type<EegMontageChangedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<EegMontageChangedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EegMontageChangedEvent.Handler handler) {
        handler.onMontageChanged(pane, montage);
    }

    public static interface Handler extends EventHandler {
        void onMontageChanged(IEEGViewerPresenter w, EEGMontage montage);
    }
}