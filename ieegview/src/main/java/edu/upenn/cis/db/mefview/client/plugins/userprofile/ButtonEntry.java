package edu.upenn.cis.db.mefview.client.plugins.userprofile;

public class ButtonEntry {
	  String icon;
	  String title;
	  String url;
	  String tooltip;
	  boolean inNewWindow;
	  
	  public ButtonEntry(String icon, String title, String url, String tooltip, boolean inNew) {
		this.icon = icon;
		this.title = title;
		this.tooltip = tooltip;
		this.url = url;
		this.inNewWindow = inNew;
	  }

	  
	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public boolean isInNewWindow() {
		return inNewWindow;
	}

	public void setInNewWindow(boolean inNewWindow) {
		this.inNewWindow = inNewWindow;
	}
	  
	  
  }