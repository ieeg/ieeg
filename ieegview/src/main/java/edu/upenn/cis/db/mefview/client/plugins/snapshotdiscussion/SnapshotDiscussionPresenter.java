/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.EEGDisplayAsync;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.DiscussionPostedEvent;
import edu.upenn.cis.db.mefview.client.events.DiscussionRepliedEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter.Display;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory.PresenterInfo;
import edu.upenn.cis.db.mefview.client.viewers.DiscussionViewer;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;

public class SnapshotDiscussionPresenter extends BasicPresenter implements DiscussionPostedEvent.Handler,
DiscussionRepliedEvent.Handler, DiscussionViewer.Handler {
	
	public interface Display extends BasicPresenter.Display {
		public void addPost(Post p);
		public void setPostList(List<Post> pl);
		public void setNextPost();
		public void setHandler(DiscussionViewer.Handler handler);
	}
	
	EEGDisplayAsync server;
	ClientFactory clientFactory;
	DataSnapshotModel model;
	Display display;
	
//	DiscussionViewer viewer;
	com.google.web.bindery.event.shared.HandlerRegistration last1;
	com.google.web.bindery.event.shared.HandlerRegistration last2;
	
	public final static String NAME = "snapshot-discussion";
	public final static Set<String> TYPES = Sets.newHashSet();
	final static SnapshotDiscussionPresenter seed = new SnapshotDiscussionPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public SnapshotDiscussionPresenter() { super(null, NAME, TYPES); }
	
	public SnapshotDiscussionPresenter(final ClientFactory clientFactory,
			final DataSnapshotModel model
			) {
		super(clientFactory, NAME, TYPES);
//		super(model, user, clientFactory.getControlPane().getMainPaneWidth(), 
//				clientFactory.getControlPane().getLeftPaneHeight());//, width, height);
//		this.clientFactory = clientFactory;
		this.model = model;
		server = clientFactory.getPortal();
	}
	
	public void postToServer(final Post p) {
		server.addPosting(clientFactory.getSessionModel().getSessionID(), 
				model.getSnapshotID(), 
				p, new SecFailureAsyncCallback<Long>(clientFactory) {

					@Override
					public void onFailureCleanup(Throwable arg0) {
						clientFactory.getSessionModel().getMainLogger().warning("Failed to save post");
						
						getDisplay().setNextPost();
					}

					@Override
					public void onSuccess(Long arg0) {
						clientFactory.getSessionModel().getMainLogger().info("Saved post");
//						refreshPosts();
						
						if (p.getRefId() == null)
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new DiscussionPostedEvent(model, p));
						else
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new DiscussionRepliedEvent(model, 
											p.getRefId(), p));
						getDisplay().setNextPost();
					}

					@Override
					protected void onNonSecFailure(Throwable caught) {}
			
		});
	}
	
	public void refreshPosts() {
//		postPanel.clear();
		
		server.getPostings( 
				clientFactory.getSessionModel().getSessionID(), 
				model.getSnapshotID(), 
				0, 
				-1, 
				new SecFailureAsyncCallback<List<Post>>(clientFactory) {

					@Override
					public void onFailureCleanup(Throwable arg0) {
						display.setNextPost();
					}

					
					@Override
					public void onSuccess(List<Post> posts) {
						display.setPostList(posts);
					}


					@Override
					protected void onNonSecFailure(Throwable caught) {}
			
		});
	}

	@Override
	public void onDiscussionPosted(DataSnapshotModel action, Post post) {
		if (action == model) {
			display.addPost(post);
		}
	}

	@Override
	public void onDiscussionReplied(DataSnapshotModel action, String refs,
			Post post) {
		if (action == model) {
			display.addPost(post);
		}
	}

	@Override
	public void onDiscussionReplied(Project project, String refs, Post post) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDiscussionPosted(Project project, Post post) {
		// TODO Auto-generated method stub
		
	}

	public void unbind() {
		if (last2 != null)
			last2.removeHandler();
		if (last1 != null)
			last1.removeHandler();
		
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(DiscussionPostedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(DiscussionRepliedEvent.getType(), this);
		
		GWT.log("Binding snapshot-discussion");
		display.setHandler(this);
		getDisplay().bindDomEvents();
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		return new SnapshotDiscussionPresenter(clientFactory, model.getState());
	}

	@Override
	public Display getDisplay() {
		return (Display)display;
	}

}
