package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.user.client.ui.IsWidget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

public interface IBlankToolbar extends IsWidget {

	void init(ClientFactory clientFactory, AstroPanel pane);

}
