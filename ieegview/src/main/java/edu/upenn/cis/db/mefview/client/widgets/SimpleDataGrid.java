/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;


import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;


public class SimpleDataGrid<T> extends DataGrid<T> implements Attachable {
  int page;
  ShowMorePagerPanel dataPager = new ShowMorePagerPanel();
  ListDataProvider<T> dataProvider = new ListDataProvider<T>();

  Set<T> favorites = new HashSet<T>();

  Map<Column<T,?>, Comparator<T>> comparators = new HashMap<Column<T,?>, Comparator<T>>();


  public interface DataGridResources extends DataGrid.Resources 
  { 
    @Source(value = { DataGrid.Style.DEFAULT_CSS, "../css/gwtDataGrid.css" }) 
    DataGrid.Style dataGridStyle(); 
  } 

  public static final DataGridResources DataGridResources = GWT.create(DataGridResources.class);
  //
  static {
    DataGridResources.dataGridStyle().ensureInjected();
  }


  public SimpleDataGrid(int pageSize, boolean multiselect, boolean useChecks, boolean useColNames,List<T> dataList, ProvidesKey<T> keyProvider,
      List<String> columnNames, List<? extends Column<T, ?>> columns) {
    super(pageSize, keyProvider );

    //TODO: Merge multiselect and selectable into single variable 
    
    setEmptyTableWidget(new HTML("No entries"));

    // Make selectable
    setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
      
    // Make pageable
    setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

    // If multSelect, use checkbox to select cell, otherwise select cell on click
    if (multiselect ) {
      final MultiSelectionModel<T> seriesSelectionModel = new MultiSelectionModel<T>(keyProvider);

      if (useChecks) {
        // Checkbox column. This table uses a checkbox column for selection.
        Column<T, Boolean> checkColumn = new Column<T, Boolean>(
            new CheckboxCell(true, false)) {
          @Override
          public Boolean getValue(T object) {
            // Get the value from the selection model.
            return seriesSelectionModel.isSelected(object);
          }
        };
        checkColumn.setCellStyleNames("ieeg-pageTableCheckColumn");
        addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        setColumnWidth(checkColumn, 30, Unit.PX);
        checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        setSelectionModel(seriesSelectionModel, 
            DefaultSelectionEventManager.<T> createCheckboxManager());
      } else {
        setSelectionModel(seriesSelectionModel);
      }
    } 

    if (useColNames){
      for (int i = 0; i < columnNames.size(); i++) {
        addColumn(columns.get(i), columnNames.get(i));
      }
    } else {
      for (int i = 0; i < columns.size(); i++) {
        addColumn(columns.get(i));
      }
    }

    dataPager.setDisplay(this);
    dataProvider.setList(dataList);
    setSize(dataList.size());
    dataProvider.addDataDisplay(this);
    dataProvider.flush();
    dataProvider.refresh();

  }

  public ShowMorePagerPanel getPager() {
    return dataPager;
  }

  @Override
  public int getRowCount() {
    int ret = dataProvider.getList().size();

    setPageStart(0);
    setPageSize(ret);
    setRowCount(ret);
    return ret;
  }

  public void setSize(int size) {
    setRowCount(size);
    setPageStart(0);
    setPageSize(size);
  }

  public List<T> getList() {
    return dataProvider.getList();
  }

  public ListDataProvider<T> getProvider() {
    return dataProvider;
  }

  public int size() {
    return getRowCount();
  }

  public T get(int index) {
    return getList().get(index);
  }

  public boolean isEmpty() {
    return getList().isEmpty();
  }

  public void clear() {
    getList().clear();
  }

  public Set<T> getSelectedItems() {
    Set<T> ret = new HashSet<T>();

    for (T item : getList())
      if (getSelectionModel().isSelected(item))
        ret.add(item);
    return ret;
  }

  public void setSelectedItems(Set<T> selected) {
    for (T item : getList())
      getSelectionModel().setSelected(item, selected.contains(item));
  }

  public HandlerRegistration addColumnSort(Column<T,?> c, Comparator<T> comp) {
    ListHandler<T> sortHandler = new ListHandler<T>(getList());

    sortHandler.setComparator(c, comp);
    comparators.put(c, comp);

    return addColumnSortHandler(sortHandler);
  }

  public ScrollPanel getScrollPanel() {
    HeaderPanel header = (HeaderPanel) getWidget();
    return (ScrollPanel) header.getContentWidget();
  }

  public void add(T item) {
    getList().add(item);
  }

  public void add(int pos, T item) {
    getList().add(pos, item);
  }
  
  
  @Override
  public void attach() {
	  onAttach();
  }
}
