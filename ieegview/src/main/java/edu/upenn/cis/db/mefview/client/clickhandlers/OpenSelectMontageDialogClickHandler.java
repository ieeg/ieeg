package edu.upenn.cis.db.mefview.client.clickhandlers;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.SecAlertCallback;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IEditMontage;
import edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage;
import edu.upenn.cis.db.mefview.client.events.EegMontageAddedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageRemovedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;

public class OpenSelectMontageDialogClickHandler implements ClickHandler {
	private ClientFactory clientFactory;
	private IEEGViewerPresenter presenter;
	private ISelectMontage dialog;
	private IEditMontage editDialog;
	private boolean editHandlerSet = false;

	public static OpenSelectMontageDialogClickHandler getHandler(
			ClientFactory cf, IEEGViewerPresenter presenter) {

		final OpenSelectMontageDialogClickHandler theHandler = new OpenSelectMontageDialogClickHandler(
				cf, presenter);

		theHandler.init();
		return theHandler;
	}

	public void init() {

		// Get Montages and populate Menu
		clientFactory.getPortal().getMontages(clientFactory.getSessionModel().getSessionID(),
				presenter.getModel().getSnapshotID(),
				new SecAlertCallback<List<EEGMontage>>(clientFactory) {

					@Override
					public void onSuccess(List<EEGMontage> result) {
						List<EEGMontage> montages = dialog.getMontages();
						dialog.clearList();
//						ListBox lb = dialog.getListBox();
						montages.clear();
//						lb.clear();

						// Add RawDataMontage
						EEGMontage rawMontage = new EEGMontage("As Recorded");
						List<String> channels = getChannelNames();

						List<EEGMontagePair> pairs = rawMontage.getPairs();
						for (String str : channels) {
							pairs.add(new EEGMontagePair(str, null));
						}

						dialog.addMontageToList(rawMontage);

						// Add persisted montages
						GWT.log("Starting to add montages: " + result);

						for (EEGMontage ui : result) {
							dialog.addMontageToList(ui);
							GWT.log("Adding Montage with name: " + ui.getName());
						}

					}

				});

	}

	private List<String> getChannelNames() {
		return presenter.getDisplay().getDataSnapshotState()
				.getChannelNames();
	}

	private OpenSelectMontageDialogClickHandler(final ClientFactory cf,
			final IEEGViewerPresenter presenter2) {
		clientFactory = cf;
		this.presenter = presenter2;
		
		// Create SelectDialog
		
		
		
		dialog = DialogFactory
				.getSelectMontageDialog(new DialogInitializer<ISelectMontage>() {

					@Override
					public void init(final ISelectMontage objectBeingInited) {

						objectBeingInited.createDialog(cf);

						objectBeingInited.init(new ISelectMontage.Handler() {

							@Override
							public void copyMontage(EEGMontage montage) {
								
								EEGMontage newMontage = new EEGMontage(montage.getName());
								newMontage.setIsPublic(false);
								newMontage.setOwnedByUser(true);
								List<EEGMontagePair> oldPairs = montage.getPairs();

								List<EEGMontagePair> pairs = newMontage.getPairs();
								for (EEGMontagePair p : oldPairs) {
									pairs.add(new EEGMontagePair(p.getEl1(), p.getEl2()));
								}
																
//								dialog.getListBox().setSelectedIndex(dialog.getListBox().getItemCount()-1);
								
								IEEGViewerPanel panel = ((IEEGViewerPanel) presenter2
										.getDisplay());
								
								
								IeegEventBusFactory.getGlobalEventBus()
										.fireEvent(
												new EegMontageAddedEvent(panel,
														newMontage));
								
							}
							
							@Override
							public void switchMontage(EEGMontage montage) {

								GWT.log("Handler switchMontage: ");

								IeegEventBusFactory.getGlobalEventBus()
										.fireEvent(
												new
												EegMontageChangedEvent(
														presenter2, montage));

							}

							@Override
							public void removeMontage(EEGMontage montage) {

								if (montage.getOwnedByUser()) {
									IeegEventBusFactory
											.getGlobalEventBus()
											.fireEvent(
													new
													EegMontageRemovedEvent(
															((IEEGViewerPanel) presenter2
																	.getDisplay()),
															montage));
								} else
									Window.alert("You aren't the creator of this montage, so you can't delete it.");
							}

							@Override
							public void editMontage(EEGMontage montage) {								
								editDialog.setMontage(montage);
								editDialog.open();
								editDialog.focus();

							}

							@Override
							public void openEditDialog() {
								// TODO Auto-generated method stub

							}

							@Override
							public void returnFocusToEeg() {
								presenter2.getDisplay().setFocusOnGraph();
							}

						}, new Command() {

							@Override
							public void execute() {
								presenter2.getDisplay().setFocusOnGraph();
							}
							
						});

					}

					@Override
					public void reinit(ISelectMontage objectBeingInited) {
						// TODO Auto-generated method stub
						
					}

				});

	}

	public ISelectMontage getDialog() {
		return dialog;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (!editHandlerSet) {
			dialog.setClickHandlers(OpenEditMontageDialogClickHandler
					.getHandler(clientFactory, presenter, this.dialog, false),
					OpenEditMontageDialogClickHandler
					.getHandler(clientFactory, presenter, this.dialog, true));
			editHandlerSet = true;
		}

		dialog.open();
		Timer t = new Timer() {
			@Override
			public void run() {
				dialog.focus();
			}
		};
		t.schedule(500);
	}

}
