package edu.upenn.cis.db.mefview.client.widgets;

public interface Attachable {
	public void attach();
}
