/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LayoutPanel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.commands.snapshot.DeriveSnapshotCommand;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class CreateExperimentSnapshotDialog extends DialogBox {
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");

	CheckBox annotations = new CheckBox("Include annotations");
	DataSnapshotViews snapshot;
	PagedTable<INamedTimeSegment> traceTable;

	String theID;
	
	public CreateExperimentSnapshotDialog(
			//final String origRevID,
			final SearchResult baseResult,
			final String tool, 
			final String type, final ToolDto toolInfo, 
			final List<INamedTimeSegment> channels, 
			final DisplayConfiguration defFilter,
			final DataSnapshotViews snapshot,
			final ClientFactory clientFactory) {
		setTitle("Create Snapshot for Experiment - Select Input Data");
		setText("Create Snapshot for Experiment - Select Input Data");
		
		LayoutPanel layout = new LayoutPanel();
		
		this.snapshot = snapshot;
		
		theID = baseResult.getId();//origRevID;
		
		HTML heading = new HTML("<b>Please select a set of channels to include:</b><br/><br/>");
		layout.add(heading);
		layout.setWidgetTopHeight(heading, 2, Unit.PX, 32, Unit.PX);

		List<String> traceColNames = new ArrayList<String>();
		List<TextColumn<INamedTimeSegment>> traceColumns = new ArrayList<TextColumn<INamedTimeSegment>>();
		
		traceColNames.add("Channel");
		traceColumns.add(TraceInfo.traceName);
		traceColNames.add("Comments");
		traceColumns.add(TraceInfo.traceComments);

		final ArrayList<INamedTimeSegment> traceList = new ArrayList<INamedTimeSegment>();
		traceTable = new PagedTable<INamedTimeSegment>(20, true, false, true,traceList, 
				TraceInfo.KEY_PROVIDER, traceColNames, traceColumns, false, null, null);
		traceTable.getList().addAll(channels);
		traceTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);//.BOUND_TO_SELECTION);
		traceTable.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

		layout.add(traceTable);
		layout.setWidgetTopBottom(traceTable, 34, Unit.PX, 68, Unit.PX);
		layout.setWidgetLeftRight(traceTable, 2, Unit.PX, 2, Unit.PX);

		for (INamedTimeSegment ch: channels)
			traceTable.getSelectionModel().setSelected(ch, true);
		
		layout.add(annotations);
		layout.setWidgetLeftRight(annotations, 2, Unit.PX, 2, Unit.PX);
		layout.setWidgetBottomHeight(annotations, 34, Unit.PX, 32, Unit.PX);
		
		setWidget(layout);

		HorizontalPanel hp = new HorizontalPanel();
		layout.add(hp);
		layout.setWidgetRightWidth(hp, 2, Unit.PX, 200, Unit.PX);
		layout.setWidgetBottomHeight(hp, 2, Unit.PX, 32, Unit.PX);
		
		layout.setSize("250px", "400px");
		hp.add(ok);
		hp.add(cancel);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				CreateExperimentSnapshotDialog.this.hide();
				
//				List<String> channelIDs = new ArrayList<String>();
//
//				for (TraceInfo ch: traceTable.getList()) {
//					if (traceTable.getSelectionModel().isSelected(ch))
//						channelIDs.add(ch.getRevId());
//				}
				
				DialogBox d = PanelFactory.getPleaseWaitDialog("Creating experiment snapshot");
				d.center();
				d.show();
				//snapshot.deriveSnapshotFrom
				
				Command dsc = new DeriveSnapshotCommand(
						clientFactory,
						snapshot,
						baseResult,//origRevID, 
						tool, 
						type, 
						toolInfo, //experimentList,
						//channelIDs,
						channels,
						annotations.getValue(), 
						defFilter, 
						d,
						null);
				
				dsc.execute();
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				CreateExperimentSnapshotDialog.this.hide();
			}
			
		});
	}
}
