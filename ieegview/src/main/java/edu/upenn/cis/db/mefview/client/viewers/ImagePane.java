/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.places.ImagePlace;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class ImagePane extends Composite implements AstroPanel, BasicPresenter.Display {
	Button button;
	Grid bar;
	HorizontalPanel contents;
	List<String> traces = null;
	ListBox imageType;
	Canvas canvas;
	Context2d context;
//	EEGDisplayAsync service;
	ImageElement currentImage;
	DataSnapshotViews model;
	DataSnapshotModel state;
	static CssColor background = CssColor.make("black");

	Presenter presenter;
	
	double brightness = 1.0;
	double contrast = 1.0;
	
	boolean initialized = false;
	Button prev;
	Button next;
	int height;
	int width;
//	String filename;
//	String studypath;
//	if (!getData().getTraceList().isEmpty())
//	miw.setTraceInfo(getData().getTraceList());
//
//if (!channel.isEmpty())
//	miw.getImagePane().visualizeTrace(channel, type);
	Map<String,Set<ImageInfo>> imageInfo;
	Map<String,Set<TraceImageInfo>> otherImageInfo;
//	EEGPane eeg;
//	TabPanel tab;
	AbsolutePanel abs;
	AbsolutePanel lPanel;
	ListBox channelList;
	CellPanel parent;
	VerticalPanel labels;
	Label label;
	ImageInfo theImage;
	Set<ImageInfo> images;
	List<ImageInfo> allImages;
	ListBox imageList;
	ImageElement lastImage = null;
	String lastTrace = null;
	String lastTyp = null;
//	String user;
//	String dataset;
	List<INamedTimeSegment> traceInfo;
	ImageViewerPanel control;
	FocusPanel focus;
	FlowPanel toolbar;
	
	double scale = 1.0;
	int offsetX = 0;
	int offsetY = 0;
	ClientFactory clientFactory;
	
	public static final int MINHEIGHT = 200; 
	public static final String NAME = "2d-image-internal";
	
	public ImagePane(final CellPanel parent, 
			final ImageViewerPanel control,
			final DataSnapshotViews model,
			final FocusPanel focus, 
//			final TabPanel tb, //String studyPath, //String fileName, //String study, String trace, 
//			final EEGDisplayAsync eegService,
			ClientFactory clientFactory,
			final int width, final int height 
			) {
//		setHTML("<b>3D Image pane for " + fileName + "</b>");

		this.clientFactory = clientFactory;
		this.model = model;
		this.state = model.getState();
//		this.user = user;
//		this.dataset = dataset;
		this.height = height;
		this.width = width;
//		this.tab = tb;
		this.focus = focus;
		this.parent = parent;
		this.control = control;
//		filename = fileName;
//		studypath = studyPath;
		traceInfo = new ArrayList<INamedTimeSegment>();
		
		allImages = new ArrayList<ImageInfo>();
		imageInfo = new HashMap<String,Set<ImageInfo>>();
		otherImageInfo = new HashMap<String,Set<TraceImageInfo>>();
//		service = eegService;
		
		FlowPanel toolbarWrapper = new FlowPanel();
        toolbar = new FlowPanel();
        toolbarWrapper.add(toolbar);
        toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
//      initToolbar(eegToolbarAndTracePanel);
        parent.add(toolbarWrapper);
        
		toolbar.setStyleName("IeegTabToolbar");    

		channelList = new ListBox();
		
		toolbar.add(new Label("Channel:"));
		toolbar.add(channelList);
		
		toolbar.add(new Label("Type:"));
		imageType = new ListBox();
		imageType.addItem("THREE_D_RENDERING", "THREE_D_RENDERING");
		imageType.addItem("MRI", "MRI");
		imageType.addItem("CT", "CT");
		imageType.addChangeHandler(new ChangeHandler() {

			public void onChange(ChangeEvent event) {
				handleChangedState();
				focus.setFocus(true);
			}

		});
		//				imageType.addItem("FMRI", "FMRI");
		//				imageType.addItem("MRS", "MRS");
		//				imageType.addItem("PET", "PET");
		//				imageType.addItem("Electrode Map", "ELECTRODE_MAP");
		//				imageType.addItem("Digital Picture", "DIGITAL_PICTURES");
		//				imageType.addItem("Ictal Spec", "ICATAL_SPECT");
		toolbar.add(imageType);
		prev = new Button("&larr;");
		toolbar.add(prev);

		imageList = new ListBox();
		toolbar.add(imageList);

		next = new Button("&rarr;");
		toolbar.add(next);

		imageList.setEnabled(false);

		canvas = Canvas.createIfSupported();

		if (canvas != null) {
			context = canvas.getContext2d();

			abs = new AbsolutePanel();
			parent.add(abs);
			parent.setCellHeight(abs, 0 + "px");

			abs.add(canvas, 0, 0);
			canvas.setWidth(width + "px");
			canvas.setHeight(height + "px");
			canvas.setCoordinateSpaceHeight(height);
			canvas.setCoordinateSpaceWidth(width);
			//					canvas.setBackgroundColor("gray");
			abs.setWidth(width + "px");
			abs.setHeight(height + "px");
			lPanel = abs;new AbsolutePanel();
//			abs.add(lPanel, 0, 0);
//			lPanel.setWidth(width + "px");
//			lPanel.setHeight(height + "px");

			canvas.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					selecting = false;
					handleClick();
				}

			});
			
			canvas.addMouseDownHandler(new MouseDownHandler() {

				@Override
				public void onMouseDown(MouseDownEvent event) {
					startSelect(event.getClientX(), event.getClientY());
				}
				
			});
			
			canvas.addMouseUpHandler(new MouseUpHandler() {

				@Override
				public void onMouseUp(MouseUpEvent event) {
					endSelect(event.getClientX(), event.getClientY());
				}
				
			});
			
			canvas.addMouseWheelHandler(new MouseWheelHandler() {

				@Override
				public void onMouseWheel(MouseWheelEvent event) {
					if (event.isSouth()) {
						zoomBy(0.9);
					} else
						zoomBy(1.11);
					event.preventDefault();
				}
				
			});
			
			canvas.addMouseOutHandler(new MouseOutHandler() {

				@Override
				public void onMouseOut(MouseOutEvent event) {
					selecting = false;
				}
				
			});
			
			canvas.addMouseMoveHandler(new MouseMoveHandler() {

				@Override
				public void onMouseMove(MouseMoveEvent event) {
					if (selecting)
						move(event.getClientX(), event.getClientY());
				}
				
			});
			
			focus.addKeyPressHandler(new KeyPressHandler() {

				public void onKeyPress(KeyPressEvent event) {
					selecting = false;
					if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEDOWN) {
						switchToNextImage();
						event.preventDefault();
					} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEUP) {
						switchToPrevImage();
						event.preventDefault();
					} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_DOWN) {
						zoomBy(0.5);
						event.preventDefault();
					} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_UP) {
						zoomBy(2.0);
						event.preventDefault();
					} else if (event.getCharCode() == '<') {
						decreaseContrast();
					} else if (event.getCharCode() == '>') {
						increaseContrast();
					} else if (event.getCharCode() == '+') {
						increaseBrightness();
					} else if (event.getCharCode() == '-') {
						decreaseBrightness();
					}
				}
			});
			
			focus.addKeyDownHandler(new KeyDownHandler() {

				public void onKeyDown(KeyDownEvent event) {
					if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEDOWN) {
						switchToNextImage();
						event.preventDefault();
					} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEUP) {
						switchToPrevImage();
						event.preventDefault();
					}
				}
				
			});
			
			prev.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					switchToPrevImage();
					event.preventDefault();
				}
				
			});
			next.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					switchToNextImage();
					event.preventDefault();
				}
				
			});
		}
	}
	
	public void resize(int width, int height) {
		int oldWidth = width;
		int oldHeight = height;
		
		this.height = height;
		this.width = width;
		
		canvas.setWidth(width + "px");
		canvas.setHeight(height + "px");
		canvas.setCoordinateSpaceHeight(height);
		canvas.setCoordinateSpaceWidth(width);
//					canvas.setBackgroundColor("gray");
		abs.setWidth(width + "px");
		abs.setHeight(height + "px");
		lPanel.setWidth(width + "px");
		lPanel.setHeight(height + "px");
		
		offsetX = (int)((((double)width) / oldWidth) * offsetX);
		offsetY = (int)((((double)height) / oldHeight) * offsetY);
		
		if (lastImage != null) {
			drawImageAndAnnotations(lastImage, lastTrace, lastTyp);
		}
	}

	private void handleClick() {
//		for (int i = 0; i < tab.getTabBar().getTabCount(); i++)
//			if (tab.getWidget(i) == control.getEEG())
//				tab.selectTab(i);
		//tab.selectTab(tab.getTabBar().getSelectedTab() ^ 1);
//		model.switchToEEGPane();
	}
	
//	public void setEEGPane(EEGPane e) {
//		eeg = e;
//	}
	
	public void handleChangedState() {
		if (channelList.getItemCount() == 0)
			return;
		
		String trace = channelList.getItemText(channelList.getSelectedIndex());
		String typ = imageType.getItemText(imageType.getSelectedIndex());
		
		visualizeTrace(trace, typ);
	}
	
	@Deprecated
	public void registerTraces(List<String> traces) {
		this.traces = traces;
		
		channelList.clear();
		for (String s : traces)
			channelList.addItem(s);
		
		if (channelList.getItemCount() == 0)
			return;

		channelList.addChangeHandler(new ChangeHandler() {

			public void onChange(ChangeEvent event) {
				handleChangedState();
				focus.setFocus(true);
			}
			
		});
		
		visualizeTrace(channelList.getItemText(channelList.getSelectedIndex()), 
				imageType.getItemText(imageType.getSelectedIndex()));
	}
	
	public boolean inited = false;
	
	public void init(List<INamedTimeSegment> traces) {
		if (inited)
			return;
		
		this.traceInfo = traces;
		
		channelList.clear();
		for (INamedTimeSegment t : traces)
			channelList.addItem(t.getLabel());
		
		if (channelList.getItemCount() == 0)
			return;

		channelList.addChangeHandler(new ChangeHandler() {

			public void onChange(ChangeEvent event) {
				handleChangedState();
			}
			
		});
		clientFactory.getPortalServices().getImagesForStudy(getSessionModel().getSessionID(), state.getSnapshotID(), 
				new SecFailureAsyncCallback.SecAlertCallback<Set<ImageInfo>>(clientFactory) {
			
			public void onSuccess(Set<ImageInfo> result) {
				allImages.clear();
				allImages.addAll(result);
				Collections.sort(allImages);
				
				visualizeTrace(channelList.getItemText(channelList.getSelectedIndex()), 
						imageType.getItemText(imageType.getSelectedIndex()));
			}
		});
	}
	
	public void visualizeTrace(String trace2, String typ) {
		if (typ.isEmpty()) {
			if (imageType == null || imageType.getSelectedIndex() < 0)
				typ = "THREE_D_RENDERING";
			else
				typ = imageType.getItemText(imageType.getSelectedIndex());
		}
		
		final String type = typ;
		
		if (trace2 == null)
			trace2 = "";
		final String trace = trace2;
		
		log(3, "Visualizing " + trace+ " (" + typ + ")");
		
		final String traceRev = state.getChannelRevId(trace);
		// Select the appropriate type
		for (int i = 0; i < imageType.getItemCount(); i++) {
			if (imageType.getItemText(i).equals(typ)) {
				imageType.setSelectedIndex(i);
				break;
			}
		}
		// Select the appropriate type
		for (int i = 0; i < channelList.getItemCount(); i++) {
			if (channelList.getItemText(i).equals(trace)) {
				channelList.setSelectedIndex(i);
				break;
			}
		}

		// Request
		if (!imageInfo.containsKey(trace)) {
			log(3, "Requesting info for " + trace + " in " + state.getFriendlyName());
			clientFactory.getSnapshotServices().getImageInfo(getSessionModel().getSessionID(), 
					state.getSnapshotID(), traceRev, trace, typ, new SecFailureAsyncCallback.SecAlertCallback<Set<ImageInfo>>(clientFactory) {
	
				public void onSuccess(Set<ImageInfo> result) {
					// TODO: hack due to limitations of WS client
					// Copy rev IDs
					for (ImageInfo ii: result) {
						for (ImageInfo i2: allImages) {
							if (ii.getName() != null && ii.getName().equals(i2.getName()))
								ii.setId(i2.getId());
						}
					}
					imageInfo.put(trace, result);
					if (result.isEmpty()) {
						log(3, "No image set");
						drawNoImage();
					} else {
						log(3, "Received image set");
						renderFromSet(trace, type, result);
					}
				}
			});
		} else {
			renderFromSet(trace, typ, imageInfo.get(trace));
		}
	}

	
	Image img = new Image();
	
	private void renderFromSet(final String trace, final String typ, final Collection<ImageInfo> info) {
		if (images == null) {
			images = new HashSet<ImageInfo>();
		} else
			images.clear();
		theImage = null;
		images.addAll(info);
		for (ImageInfo img : info) {
			if (img.getType().equals(typ)) {
				theImage = img;
				break;
			}
		}

		log(4, "Render from set: " + trace);
		if (theImage == null) {
			drawNoImage();
		} else {
			imageList.clear();
			String nam = theImage.getName();
			if (nam.contains("/"))
				nam = nam.substring(nam.lastIndexOf('/') + 1);
			
			imageList.addItem(nam);
			
			if (img.getUrl().equals(theImage.getUrl())) {
				ImageElement image = (ImageElement)img.getElement().cast();
				
				log(4, "Highlight " + trace);
				drawImageAndAnnotations(image, trace, typ);
			} else {
			
				log(4, "Try to load: " + theImage.getName());
				RootPanel.get().remove(img);
				img.setUrl(theImage.getUrl());
				
				img.addLoadHandler(new LoadHandler() {
	
					public void onLoad(LoadEvent event) {
						ImageElement image = (ImageElement)img.getElement().cast();
						
						log(4, "Highlight " + trace);
						drawImageAndAnnotations(image, trace, typ);
					}
			
					
					
				});
				
				img.setVisible(false);
				
	//			System.err.println("Trying to get " + prefix + theImage.name);
				RootPanel.get().add(img);
			}
			
		}
	}
	
	private void drawImageAndAnnotations(final ImageElement image, final String trace, final String typ) {
		lastImage = image;
		lastTrace = trace;
		lastTyp = typ;
		
		createCanvas(image);
		final double h = image.getHeight();
		final double w = image.getWidth();
		double hRatio = height / h;
		double wRatio = width / w;
		if (hRatio < wRatio)
			wRatio = hRatio;
		else
			hRatio = wRatio;
		
		wRatio = wRatio * scale;
		hRatio = hRatio * scale;

		final double hR = hRatio;
		final double wR = wRatio;
		
		// Remove labels other than the canvas
//		while (abs.getWidgetCount() > 1)
//			abs.remove(1);
//		lPanel.clear();
		for (int x = abs.getWidgetCount() - 1; x > 1; x--)
			abs.remove(x);

		// Request
		if (theImage == null) {
			drawNoImage();
		} else if (otherImageInfo == null || !otherImageInfo.containsKey(theImage.getName())) {
//			System.out.println("REQUESTING " + filename + ":" + theImage.name);
			clientFactory.getSnapshotServices().getTracesForImage(getSessionModel().getSessionID(),
					theImage.getName(), 
					new SecFailureAsyncCallback.SecAlertCallback<Set<TraceImageInfo>>(clientFactory) {
	
				public void onSuccess(Set<TraceImageInfo> result) {
					otherImageInfo.put(theImage.getName(), result);
					if (!result.isEmpty()) {
						renderElectrodeIndicators(trace, typ, result, w, h, wR, hR);
					} else {
						drawNoImage();
					}
				}
			});
		} else {
			renderElectrodeIndicators(trace, typ, otherImageInfo.get(theImage.getName()), w, h, wR, hR);
		}
	}
	
	private void drawNoImage() {
		context.setFillStyle(background);
		context.fillRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
		// Remove labels other than the canvas
//		while (abs.getWidgetCount() > 1)
//			abs.remove(1);
//		lPanel.clear();
		for (int x = abs.getWidgetCount() - 1; x > 1; x--)
			abs.remove(x);
		label = new Label("No image available");
		label.setStylePrimaryName("imageLabel");
		lPanel.add(label, (int)(width / 2), (int)(height / 2));
	}
	
	private void renderElectrodeIndicators(final String trace, final String typ, final Set<TraceImageInfo> info,
			double w, double h, double wRatio, double hRatio) {
		
		log(3, "Render " + trace + " / " + typ);
		for (TraceImageInfo ii: info) {
			log(4, "Image: " + ii.getTraceName() + " / trace: " + trace);
			
			if (ii.getTraceName().equals(trace)) {
				context.save();
				// Add a very wide shadow
				context.beginPath();
				context.setLineWidth(hRatio * 2);
				context.setStrokeStyle("#333333");
				context.setGlobalAlpha(0.5);
				
				// Draw the circle
				context.arc((width - (wRatio * w)) / 2 + 
						ii.getPosX() * wRatio + offsetX, 
						(height - (hRatio * h)) / 2 +
						ii.getPosY() * hRatio + offsetY, 5 * wRatio, 
						0, Math.PI*2, true);
				context.closePath();
				context.stroke();
				context.restore();

				context.save();
				context.setGlobalAlpha(1.0);
				context.beginPath();
				context.setLineWidth(hRatio);
				context.setStrokeStyle("#FFCC11");
				
				context.arc((width - (wRatio * w)) / 2 + 
						ii.getPosX() * wRatio + offsetX, 
						(height - (hRatio * h)) / 2 +
						ii.getPosY() * hRatio + offsetY, 5 * wRatio, 
						0, Math.PI*2, true);
				context.closePath();
				context.stroke();
				context.restore();
				
				
//				if (!trace.isEmpty()) {
					label = new Label(trace);
					label.setStylePrimaryName("imageShadow");
					lPanel.add(label, (int)((width - (wRatio * w)) / 2 + 
							ii.getPosX() * wRatio) + 1 + offsetX, (int)(((height - (hRatio * h)) / 2) +
									(ii.getPosY() * hRatio))+1 + offsetY);
			
					label = new Label(trace);
					label.setStylePrimaryName("imageLabel");
					lPanel.add(label, (int)((width - (wRatio * w)) / 2 + 
							ii.getPosX() * wRatio) + offsetX, (int)(((height - (hRatio * h)) / 2) +
									(ii.getPosY() * hRatio)) + offsetY);
					label.addClickHandler(new ClickHandler() {
						
						public void onClick(ClickEvent event) {
							handleClick();
						}
						
					});
//				}
			} else {
				context.save();
				// Add a very wide shadow
				context.beginPath();
				context.setLineWidth(hRatio);
				context.setStrokeStyle("#333333");
				context.setGlobalAlpha(0.5);
				
				context.arc((width - (wRatio * w)) / 2 + 
						ii.getPosX() * wRatio + offsetX, 
						(height - (hRatio * h)) / 2 +
						ii.getPosY() * hRatio + offsetY, 3 * wRatio, 
						0, Math.PI*2, true);
				context.closePath();
				context.stroke();
				context.restore();
		
				context.save();
				context.setGlobalAlpha(1.0);
				// Draw the circle
				context.beginPath();
				context.setLineWidth(hRatio / 2);
				context.setStrokeStyle("#FF1111");
				
				context.arc((width - (wRatio * w)) / 2 + 
						ii.getPosX() * wRatio + offsetX, 
						(height - (hRatio * h)) / 2 +
						ii.getPosY() * hRatio + offsetY, 3 * wRatio, 
						0, Math.PI*2, true);
				context.closePath();
				context.stroke();
				context.restore();
				
				if (ii.getTraceName() != null) {
					label = new Label(ii.getTraceName());
					label.setStylePrimaryName("auxImageShadow");
					lPanel.add(label, (int)((width - (wRatio * w)) / 2 + 
							ii.getPosX() * wRatio) + 1 + offsetX, (int)(((height - (hRatio * h)) / 2) +
									(ii.getPosY() * hRatio))+1 + offsetY);
		
					label = new Label(ii.getTraceName());
					label.addClickHandler(new ClickHandler() {
						
						public void onClick(ClickEvent event) {
							handleClick();
						}
						
					});
					label.setStylePrimaryName("auxImageLabel");
					lPanel.add(label, (int)((width - (wRatio * w)) / 2 + 
							ii.getPosX() * wRatio) + offsetX, (int)(((height - (hRatio * h)) / 2) +
									(ii.getPosY() * hRatio) + offsetY));
				}
			}
		}
	}
	
//	public void setTrace(String trace, String typ) {
//		visualizeTrace(trace, typ);
//	}
	
	private void createCanvas(ImageElement image) {
		double h = image.getHeight();
		double w = image.getWidth();
		double hRatio = height / h;
		double wRatio = width / w;
		if (hRatio < wRatio)
			wRatio = hRatio;
		else
			hRatio = wRatio;
		
		wRatio = wRatio * scale;
		hRatio = hRatio * scale;
		
		context.setFillStyle(background);
		context.fillRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
		context.drawImage(image, (width - (wRatio * w)) / 2 + offsetX, 
				(height - (hRatio * h)) / 2 + offsetY, wRatio * w, hRatio * h);
		
		adjust(brightness, contrast);
	}

//	public void setDataset(String ds) {
//		dataset = ds;
//	}
	
	public void hideStatus() {
		
	}
	
	// TODO: show a highlight rectangle
	public void showHighlight() {
		
	}
	
	// TODO: remove a highlight rectangle
	public void hideHighlight() {
		
	}
	
	public void switchToNextImage() {
		Set<ImageInfo> thisImage = new HashSet<ImageInfo>();
		int index = allImages.indexOf(theImage);
		
		theImage = allImages.get((index + 1) % allImages.size());
		thisImage.add(theImage);
		
		for (int i = 0; i < imageType.getItemCount(); i++) {
			imageType.setSelectedIndex(i);
			if (imageType.getItemText(i).equals(theImage.getType())) {
				break;
			}
		}
		// Select the appropriate type
		for (int i = 0; i < channelList.getItemCount(); i++) {
			if (channelList.getItemText(i).equals(lastTrace)) {
				channelList.setSelectedIndex(i);
				break;
			}
		}
	
		renderFromSet(lastTrace, theImage.getType(), thisImage);
	}
	
	public void switchToPrevImage() {
		Set<ImageInfo> thisImage = new HashSet<ImageInfo>();
		if (allImages == null || theImage == null)
			return;
		
		int index = allImages.indexOf(theImage);
		
		theImage = allImages.get((index + allImages.size() - 1) % allImages.size());
		thisImage.add(theImage);
		
		for (int i = 0; i < imageType.getItemCount(); i++) {
				imageType.setSelectedIndex(i);
				if (imageType.getItemText(i).equals(theImage.getType())) {
				break;
			}
		}
		// Select the appropriate type
		for (int i = 0; i < channelList.getItemCount(); i++) {
			if (channelList.getItemText(i).equals(lastTrace)) {
				channelList.setSelectedIndex(i);
				break;
			}
		}
		
		renderFromSet(lastTrace, theImage.getType(), thisImage);
	}
	
//	public String getUser() {
//		return user;
//	}

	public void updateRevId(final String oldId, final String newId) {
//		if (dataset.equals(oldId))
//			dataset = newId;
		state.updateSnapshotID(oldId, newId);
	}

//	@Override
//	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void setTimestamp(double timeUutc) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void setDuration(double duration) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void setChannels(Collection<String> channel) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
	public void zoomBy(double ratio) {
		scale = scale * ratio;
		refresh();
	}

//	@Override
	public void highlightMetadata(Collection<? extends PresentableMetadata> items) {
		if (!items.isEmpty()) {
			String trace = items.iterator().next().getId();
			this.visualizeTrace(trace, "");
		}
	}

	public void resetZoom() {
		
	}
	
	public void increaseContrast() {
		contrast *= 2.0;
		adjust(brightness, contrast);
	}
	
	public void decreaseContrast() {
		contrast *= 0.5;
		adjust(brightness, contrast);
	}
	
	public void increaseBrightness() {
		brightness *= 2.0;
		adjust(brightness, contrast);
	}
	
	public void decreaseBrightness() {
		brightness *= 0.5;
		adjust(brightness, contrast);
	}
	
	boolean selecting = false;
	int startX;
	int startY;
	int endX;
	int endY;
	
	private void startSelect(int x, int y) {
		startX = x;
		startY = y;
		selecting = true;
	}
	
	private void endSelect(int x, int y) {
		selecting = false;
	}
	
	private void move(int x, int y) {
		endX = x;
		endY = y;
		
		offsetX = x - startX;
		offsetY = y - startY;
		
		refresh();
	}
	
	/**
	 * Adjust image brightness and contrast
	 * 
	 * Loosely translated from Javascript source to Pixastic library
	 * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
	 * License: [http://www.pixastic.com/lib/license.txt]
	 * 
	 * @param brightness
	 * @param contrast
	 */
	public void adjust(double brightness, double contrast) {
// Commented out for Issue 242		
//		ImageData data = context.getImageData(0, 0, width, height);
//		
//		if (brightness == 1 && contrast == 1)
//			return;
//		
//		CanvasPixelArray arr = data.getData();
//		
//		data.getAlphaAt(0, 0);
//		
//		int pixels = width * height;
//		
//		double mul;
//		double add;
//		double brightMul = 1 + Math.min(150,Math.max(-150,brightness)) / 150;
//
//		if (contrast != 1) {
//			mul = brightMul * contrast;
//			add = - contrast * 128 + 128;
//		} else {  // this if-then is not necessary anymore, is it?
//			mul = brightMul;
//			add = 0;
//		}
//		
//		int pix = pixels * 4 - 1;
//		while (pix > 0) {
//			int r = (int)(arr.get(pix) * mul + add);
//			if (r > 255 )
//				arr.set(pix, 255);
//			else if (r < 0)
//				arr.set(pix, 0);
//			else
//				arr.set(pix, r);
//
//			int pix1 = pix + 1;
//			int g = (int)(arr.get(pix1) * mul + add);
//			if (g > 255 ) 
//				arr.set(pix1, 255);
//			else if (g < 0)
//				arr.set(pix1, 0);
//			else
//				arr.set(pix1, g);
//
//			int pix2 = pix+2;
//			int b = (int)(arr.get(pix2) * mul + add);
//			if (b > 255 ) 
//				arr.set(pix2, 255);
//			else if (b < 0)
//				arr.set(pix2, 0);
//			else
//				arr.set(pix2, b);
//
//			pix -= 4;
//		}
	}


//	@Override
//	public void registerPrefetchListener(Prefetcher p) {
//		// TODO Auto-generated method stub
//		
//	}
	
	public String getSnapshotID() {
		return state.getSnapshotID();
	}
	
	public void refresh() {
		if (lastImage != null) {
			drawImageAndAnnotations(lastImage, lastTrace, lastTyp);
		}
	}

	@Override
	public String getPanelType() {
		return NAME;// PanelType.IMAGE;
	}

	@Override
	public DataSnapshotViews getDataSnapshotController() {
		return model;
	}

	@Override
	public PanelDescriptor getDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof ImagePlace) {
			ImagePlace ip = (ImagePlace)place;
			
			for (int i = 0; i < imageList.getItemCount(); i++)
				if (imageList.getItemText(i).equals(ip.getImageName()))
					imageList.setItemSelected(i, true);
				else
					imageList.setItemSelected(i, false);
			
			for (int i = 0; i < imageType.getItemCount(); i++)
				if (i == ip.getImageMode())
					imageType.setItemSelected(i, true);
				else
					imageType.setItemSelected(i, false);
		}
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
//		GWT.log("ImagePane.getDataSnapshotState() == " + state);
		return state;
	}
	
	@Override
	public void close() {
		
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getImageTab();
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int getImageTypeIndex() {
		return imageType.getSelectedIndex();
	}
	
	public String getSelectedImage() {
		return imageList.getItemText(imageList.getSelectedIndex());
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	public String getTitle() {
		return "Image-pane";
	}
	
	public ImageResource getIcon() {
		return null;
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		init(model.getTraceInfo());
	}

	public boolean isSearchable() {
		return false;
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}
	
	public int getMinHeight() {
      return MINHEIGHT;
    }

	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
		/*
		for (Widget w: Arrays.asList(button, prev, next, toolbar, focus)) {
			final Widget wHere = w;
			if (wHere != null)
				Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
					
					@Override
					public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
						NativeEvent clickEvent = Converter.getNativeEvent(event);
						DomEvent.fireNativeEvent(clickEvent, wHere);
					}
				});
		}*/
	}
	
	protected SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
}
