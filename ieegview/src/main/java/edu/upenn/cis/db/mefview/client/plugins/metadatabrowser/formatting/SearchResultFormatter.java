/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;

import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.MetadataFormatter;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;

public class SearchResultFormatter implements MetadataFormatter {


	@Override
	public int getNumColumns() {
		return 5;
	}

	@Override
	public ImageResource getImage(PresentableMetadata meta) {
		return ResourceFactory.folder();
	}
	
	private String getSpan(long uutc) {
		double secs = uutc / 1000000.0;
		BigDecimal secsBd =
				new BigDecimal(secs).setScale(
						0,
						RoundingMode.HALF_UP);
		final int secsRounded = secsBd.intValue();
		int secsToDisplay = secsRounded % 60;

		final int mins = secsRounded / 60;
		int minsToDisplay = mins % 60;

		final int hours = mins / 60;
		int hoursToDisplay = hours % 24;

		final int days = hours / 24;
		int daysToDisplay = days;

		StringBuilder sb = new StringBuilder();
		
		if (daysToDisplay > 0) {
			if (secsToDisplay >= 30) {
				minsToDisplay++;
			}
			if (minsToDisplay >= 30) {
				hoursToDisplay++;
			}
			if (hoursToDisplay == 24) {
				hoursToDisplay = 0;
				daysToDisplay++;
			}
			sb.append(daysToDisplay);
			sb.append("d ");

			sb.append(hoursToDisplay);
			sb.append("h");
		} else if (hoursToDisplay > 0) {
			
			if (secsToDisplay >= 30) {
				minsToDisplay++;
			}
			if (minsToDisplay == 60) {
				minsToDisplay = 0;
				hoursToDisplay++;
			}
			
			sb.append(hoursToDisplay);
			sb.append("h ");

			sb.append(minsToDisplay);
			sb.append("m");
		} else {
			sb.append(minsToDisplay);
			sb.append("m ");

			sb.append(secsToDisplay);
			sb.append("s");
		}
		return sb.toString();
	}

	@Override
	public String getColumnContent(PresentableMetadata meta, int index) {
		if (meta instanceof SearchResult) {
		
			SearchResult sr = (SearchResult)meta;
			if (index == 0) {
				return meta.getFriendlyName();
			} else if (index == 1) {
//				if (sr.getOrganization() != null && !sr.getOrganization().isEmpty())
					return sr.getOrganization();
//				else
//					return sr.getId();
			} else if (index == 2) {
				return (sr.getAnnotationCount() > 0) ?
						"(" + String.valueOf(sr.getAnnotationCount()) + " ann.)"
    					  : "";
//			} else if (index == 3) {
//				return sr.getTimeSeriesLabels().size() + " chs" + (sr.getImageCount() > 0 ? ", " + String.valueOf(sr.getImageCount()) + " img"
//    					  : "");
			} else if (index == 3) {
//			} else if (index == 4) {
				if (sr.getEndUutc() != null && sr.getStartUutc() != null)
					return String.valueOf(getSpan(sr.getEndUutc() - sr.getStartUutc()));
				else
					return "";
			} else if (index == 4) {
				if (sr.getTermsMatched().isEmpty())
					return "";
				else
					return sr.getScore()
						+ sr.getTermsMatched().toString()
						+ (sr.getFeatures() == null
								? ""
								: " "
										+ sr.getFeatures().getFeaturesPresent());
			}
		} else
			return "<INCOMPATIBLE>";
		return "";
	}
	
	@Override
	public String getColumnContent(PresentableMetadata meta,
			String attrib) {
		return meta.getStringValue(attrib) == null ? "" : meta.getStringValue(attrib);
	}

	@Override
	public boolean isStarrable() {
		return true;
	}

	@Override
	public boolean isStarred(PresentableMetadata meta, MetadataPresenter presenter) {
		if (presenter == null)
			return false;
		else
			return presenter.isStarred(meta);
	}

	@Override
	public boolean showPrev(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean showNext(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void prevPage(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextPage(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toggleStar(PresentableMetadata meta, MetadataPresenter presenter) {
		if (presenter != null) {
			GWT.log("Toggle star on SearchResult, calling presenter");
			presenter.toggleStar(meta);
		} else
			GWT.log("Toggle star on SearchResult but no presenter");
	}

	@Override
	public boolean isRatable() {
		return true;
	}

	@Override
	public Boolean isRated(PresentableMetadata object, MetadataPresenter presenter) {
		return ((SearchResult)object).getIsValid();
	}

	@Override
	public void toggleRating(PresentableMetadata meta, MetadataPresenter presenter) {
		if (presenter != null && !((SearchResult)meta).getTermsMatched().isEmpty()) {
			GWT.log("Toggle rating on SearchResult, calling presenter");
			presenter.toggleRating(meta);
		} else
			GWT.log("Toggle rating on SearchResult but no presenter");
	}
}
