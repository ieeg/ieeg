/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class AnnotationSchemeCreatedEvent  extends GwtEvent<AnnotationSchemeCreatedEvent.Handler> {
    private static final Type<AnnotationSchemeCreatedEvent.Handler> TYPE = new Type<AnnotationSchemeCreatedEvent.Handler>();
    
    private final DataSnapshotModel model;
    
    private final AnnotationGroup<Annotation> group;
    
    public AnnotationSchemeCreatedEvent(DataSnapshotModel model,
            final AnnotationGroup<Annotation> group){
        this.model = model;
        this.group = group;
    }
    
    public DataSnapshotModel getDataSnapshotState() {
        return model;
    }
    
    public AnnotationGroup<Annotation> getGroup() {
        return group;
    }
    public static com.google.gwt.event.shared.GwtEvent.Type<AnnotationSchemeCreatedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<AnnotationSchemeCreatedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AnnotationSchemeCreatedEvent.Handler handler) {
        handler.onAnnotationSchemeCreatedEvent(model, group);
    }

    public static interface Handler extends EventHandler {
        void onAnnotationSchemeCreatedEvent(DataSnapshotModel action, 
                AnnotationGroup<Annotation> group);
    }

}

