/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;

import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.MetadataFormatter;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.TraceInfo;


public class TraceInfoFormatter implements MetadataFormatter {


	@Override
	public int getNumColumns() {
		return 3;
	}

	@Override
	public ImageResource getImage(PresentableMetadata meta) {
		return ResourceFactory.timeseries();
	}

	@Override
	public String getColumnContent(PresentableMetadata meta, int index) {
		if (meta instanceof TraceInfo) {
			TraceInfo tr = (TraceInfo)meta;
			if (index == 0)
				return tr.getFriendlyName();
			else if (index == 1) {
				if (tr.getSampleRate() >= 10000)
					return "(" + roundToSignificantFigures(tr.getSampleRate() / 1000, 3) + " KHz)";
				else
					return "(" + Math.round(tr.getSampleRate()) + " Hz)";
			} else if (index == 2) {
				if (tr.getParent() != null && 
						tr.getInstitution() != null &&
						!tr.getInstitution().equals(tr.getParent().getStringValue("organization")))
					if (tr.getInstitution().length() > 0)
						return " from " + tr.getInstitution();
					else
						return "";
			}
		} else
			return "<INCOMPATIBLE>";
		return "";
	}
	
	@Override
	public String getColumnContent(PresentableMetadata meta,
			String attrib) {
		return meta.getStringValue(attrib) == null ? "" : meta.getStringValue(attrib);
	}

	@Override
	public boolean isStarrable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isStarred(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean showPrev(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean showNext(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void prevPage(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextPage(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void toggleStar(PresentableMetadata meta, MetadataPresenter presenter) {
		GWT.log("Clicked star on TraceInfo");
	}

	@Override
	public boolean isRatable() {
		return false;
	}

	@Override
	public Boolean isRated(PresentableMetadata object, MetadataPresenter presenter) {
		return null;
	}

	@Override
	public void toggleRating(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}

	// From http://stackoverflow.com/questions/202302/rounding-to-an-arbitrary-number-of-significant-digits
	public static double roundToSignificantFigures(double num, int n) {
	    if (num == 0)
	        return 0;

	    final double d = Math.ceil(Math.log10(num < 0 ? -num: num));
	    final int power = n - (int) d;

	    final double magnitude = Math.pow(10, power);
	    final long shifted = Math.round(num*magnitude);
	    
	    return shifted/magnitude;
	}
}
