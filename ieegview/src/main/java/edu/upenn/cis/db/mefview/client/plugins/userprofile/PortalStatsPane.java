/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;

import edu.upenn.cis.db.mefview.client.plugins.news.widgets.HeatMapLayerWidget;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.PortalSummary;

public class PortalStatsPane extends FlowPanel implements PortalStatsPresenter.Display {
	HTML portalList;
	HTML statComment;
	Presenter presenter;
	
	public static final boolean show = true;
	
	public PortalStatsPane() {
		portalList = new HTML("Portal statistics still loading");
		statComment = new HTML();
		statComment.setStylePrimaryName("ieeg-info-text");
		add(portalList);
	}
	
	HeatMapLayerWidget widget;
	
	/**
	 * See the map widgets for different map configurations
	 */
	public void draw(List<LatLon> coordinates) {
		if (widget == null && show) {
			widget = new HeatMapLayerWidget(coordinates, 390, 190);
			Label mapLabel = new Label("Users around the world:");
			mapLabel.setStylePrimaryName("sideBarSubHeader");
			add(mapLabel);
			add(widget);
		}
	}
	

	@Override
	public void setPortalInfo(String displayThis) {
		portalList.setHTML(displayThis);
	}
	
	@Override
	public void setPortalInfo(PortalSummary summary, boolean isDataSets){
	  
	  FlowPanel statBucket = new FlowPanel();
	  statBucket.setStylePrimaryName("ieeg-stat-bucket");
	  
	  String ds = (isDataSets) ? "datasets" : "submissions";
	  
	  Grid statTable = new Grid(2,4);
	  statTable.setText(0, 0, Integer.toString(summary.getNumWorldReadableStudies() + summary.getNumWorldReadableExperiments()));
	  statTable.getCellFormatter().setStylePrimaryName(0, 0, "ieeg-stat-total");
	  statTable.setText(0, 1, "public " + ds);
	  statTable.setText(1, 0, Integer.toString(summary.getNumUsers()));
	  statTable.getCellFormatter().setStylePrimaryName(1, 0, "ieeg-stat-total");
	  statTable.setText(1, 1, "registered users");
	  
	  if (isDataSets) {
		  statTable.setText(0, 2, Integer.toString(summary.getNumUserReadableExperiments()));
		  statTable.getCellFormatter().setStylePrimaryName(0, 2, "ieeg-stat-total");
		  statTable.setText(0, 3, "academic* " + ds);
		  statTable.setText(1, 2, Integer.toString(summary.getNumUserReadableStudies()));
		  statTable.getCellFormatter().setStylePrimaryName(1, 2, "ieeg-stat-total");
		  statTable.setText(1, 3, "clinical** " + ds);
		  	  
		  
		  statComment.setHTML("* " + summary.getNumWorldReadableExperiments() + " and "+
		      "** " + summary.getNumWorldReadableStudies() + " " + ds + " are publicly accessible; others are shared within a group.");
		  statComment.setStylePrimaryName("ieeg-stat-comment");
	  }
	  
	  statBucket.add(statTable);
	  portalList.setHTML("");
	  insert(statBucket, 1);
	  insert(statComment, 2); 
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}

	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}


	public void bindDomEvents() {
	}
}
