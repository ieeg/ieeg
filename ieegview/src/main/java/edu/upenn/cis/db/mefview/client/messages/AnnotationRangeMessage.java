/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages;

import edu.upenn.cis.db.mefview.client.messages.requests.RequestAnnotations;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class AnnotationRangeMessage {
	RequestAnnotations request;
	
	AnnotationGroup<Annotation> relevantAnnotations;
	
	public AnnotationRangeMessage(RequestAnnotations request,
			AnnotationGroup<Annotation> relevantAnnotations, boolean isPartial) {
		this.request = request;
		setRelevantAnnotations(relevantAnnotations);
	}

	public RequestAnnotations getRequest() {
		return request;
	}

	public void setRequest(RequestAnnotations request) {
		this.request = request;
	}

	public AnnotationGroup<Annotation> getRelevantAnnotations() {
		return relevantAnnotations;
	}

	public void setRelevantAnnotations(
			AnnotationGroup<Annotation> relevantAnnotations) {
		this.relevantAnnotations = relevantAnnotations;
	}
}
