/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

public class OpenEEGAction extends PluginAction {
	
	Map<SerializableMetadata,Set<PresentableMetadata>> snapshots = new HashMap<SerializableMetadata,Set<PresentableMetadata>>();
	
	public static final String NAME = "View EEG";
	
	
	static {
		PluginActionFactory.register(NAME, new OpenEEGAction());
	}
	
	public OpenEEGAction() {
		super(NAME, null, null, null);
	}
	
	public OpenEEGAction(Set<PresentableMetadata> md, ClientFactory cf, Presenter pres) {
		super(NAME, cf, pres, null);
		// Two cases:
		//  1. set of channels with common snapshot -- open EEG, then shut down the channels
		//
		// 2. set of snapshots
		
		// Collect all of the snapshots and the particular channels
		for (PresentableMetadata m : md) {
			if (m instanceof SearchResult && !snapshots.containsKey(m)) {
				snapshots.put(m, new HashSet<PresentableMetadata>());
			} else if (m.getParent() != null && m.getParent().getId() != null) {
				if (!snapshots.containsKey(m.getParent()))
					snapshots.put(m.getParent(),
							new HashSet<PresentableMetadata>());

				snapshots.get(m.getParent()).add(m);
			}
		}
	}

	@Override
	public void execute() {
		for (SerializableMetadata snap : snapshots.keySet()) {
			if (snap instanceof SearchResult) {
				OpenDisplayPanel req = new OpenDisplayPanel(
						snap, true, EEGViewerPresenter.NAME,
						snapshots.get(snap), null);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(
						new OpenDisplayPanelEvent(req));
				break;
			}
		}
//		return null;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new OpenEEGAction(md, factory, pres);
	}
	
};

