/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.habitat;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.event.dom.client.ClickHandler;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter.Display;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory.PresenterInfo;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

/**
 * This is a generic template for a presenter
 * 
 * @author zives
 *
 */
public class RecruitmentPresenter extends BasicPresenter {
	AppModel control;
	DataSnapshotModel model;
	
	/**
	 * This is the interface implemented by the AnalysisPresenterPanel.  Use
	 * this (and add methods) to change the visual display, or to add behavior
	 * like event handlers to widgets.
	 */
	public interface Display extends BasicPresenter.Display {
		public void addClickHandlerToButton(ClickHandler handler);
		
		public WindowPlace getPlace();
	}
	
	public final static String NAME = "recruit-users";
	public final static Set<String> TYPES = Sets.newHashSet();
	final static RecruitmentPresenter seed = new RecruitmentPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public RecruitmentPresenter() { super(null, NAME, TYPES); }

	public RecruitmentPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory
			) {
		super(clientFactory, NAME, TYPES);
		this.control = control;
//		this.clientFactory = clientFactory;
		this.model = model;
	  }
	
	
	//////
	// The sequence of creating a panel is roughly as follows:
	//  1. The panel N is created by the PanelFactory, which is triggered with a name.
	//      Here it's "an1".
	//
	//	2. The presenter P is created, using the same name.
	//
	//  3. P.setDisplay(N) is called to associate the panel with the presenter.
	//
	//  4. P.bind() is called.  It should initialize all of the event handlers, and
	//     do any specific initialization that makes use of state.
	
	@Override
	public Display getDisplay() {
		return (Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		// In general we'll call clientFactory.registerHandler to respond to system-wide events
		// like panel open-requests, display refresh requests, etc. but you probably don't need these
		
		// So... Set ClickHandlers and the like here.
		//       Any kind of programmatic behavior can be initialized here.
		
		
		
		// Here's some sample code from the PDF viewer.  Please delete freely.
//		clientFactory.registerHandler(DocumentReadyEvent.getType(), this);
//
//		if (getDataSnapshotState() != null &&
//				!getDataSnapshotState().getDocuments().isEmpty()) {
//			loadDocument(getDataSnapshotState().getDocuments().get(0));
//		} else if (display.getPlace() != null && display.getPlace() instanceof PDFPlace) {
//			PDFPlace pl = (PDFPlace)display.getPlace();
//			openURL(pl.getPDF(), null, display.getPDFFrame());
//		}
		display.addPresenter(this);
		getDisplay().bindDomEvents();
	}

	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata project, boolean writePermissions) {
		return new RecruitmentPresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory);
	}
	
}
