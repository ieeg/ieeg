/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.models.AnnotationSet;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class RemapAnnotations extends DialogBox {
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	ListBox from = new ListBox();
	ListBox to = new ListBox();
	ListBox map = new ListBox();
	Button remove = new Button("Remove mapping");
	Button mapTo = new Button("Map to &rarr;");
	
//	AnnotationSet mappedAnn = new AnnotationSet();
//	AnnotationManager annManager;
//	String annSetName;
//	String target;
	
	public RemapAnnotations(//final List<String> sourceFriendly, final List<String> sourceDsId, 
			final String annSetName, final String target, final List<String> channels, 
			final List<AnnotationSet<Annotation>> annList, 
			//final AnnotationManager annManager,
			final DataSnapshotModel snapshot,
			final String annPrefix, final List<INamedTimeSegment> traces) {
		setTitle("Remap Annotations");
		setText("Remap Annotations: " + annSetName + " on " + target);
		
		VerticalPanel layout = new VerticalPanel();
//		DockLayoutPanel dp = new DockLayoutPanel(Unit.PX);
		
//		this.target = target;
//		this.annSetName = annSetName;
//		this.annManager = annManager;
//		
		Set<String> sourceChannels = new HashSet<String>();
		for (AnnotationSet<Annotation> as : annList)
			for (Annotation a : as.annotations)
				sourceChannels.addAll(a.getChannels());

		final List<String> sources = new ArrayList<String>();
		sources.addAll(sourceChannels);
		Collections.sort(sources);
		
		HorizontalPanel boxes = new HorizontalPanel();
		
		layout.add(boxes);

		from = new ListBox();
		for (String s: sources)
			from.addItem(s);
		VerticalPanel fromPane = new VerticalPanel();
		fromPane.add(new Label("Source series:"));
		fromPane.add(from);
		boxes.add(fromPane);
		boxes.add(mapTo);
		boxes.setCellVerticalAlignment(mapTo, HasVerticalAlignment.ALIGN_MIDDLE);
		from.setVisibleItemCount(20);

		Set<String> destChannels = new HashSet<String>();
		for (String c: channels)
			destChannels.add(c);

		final List<String> dests = new ArrayList<String>();
		dests.addAll(destChannels);
		Collections.sort(dests);
		for (String s: dests)
			to.addItem(s);
		
		VerticalPanel toPane = new VerticalPanel();
		toPane.add(new Label("Target series:"));
		toPane.add(to);
		boxes.add(toPane);
		to.setVisibleItemCount(20);
		VerticalPanel mapBox = new VerticalPanel();
		mapBox.add(new Label("Series mappings (source / target):"));
		mapBox.add(map);
		map.setVisibleItemCount(15);
		mapBox.add(remove);
		mapBox.setCellHorizontalAlignment(remove, HasHorizontalAlignment.ALIGN_CENTER);

		// Add mapping based on exact match
		for (String f : sources)
			for (String t : dests)
				if (f.equals(t))
					map.addItem(getMapString(f,t));
		
		// If no matches, try suffix match
		if (map.getItemCount() == 0) {
			for (String f : sources)
				for (String t : dests)
					if (f.endsWith(t))
						map.addItem(getMapString(f,t));
		}
		
		mapTo.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				String f = sources.get(from.getSelectedIndex());
				String t = dests.get(to.getSelectedIndex());
				map.addItem(getMapString(f,t));
			}
		});
		
		remove.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				map.removeItem(map.getSelectedIndex());
			}
			
		});
		
		boxes.add(mapBox);
		
		setWidget(layout);
//		dp.addSouth(layout, 20);

		HorizontalPanel hp = new HorizontalPanel();
		layout.add(hp);
		layout.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		hp.add(ok);
		hp.add(cancel);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				RemapAnnotations.this.hide();
				List<Annotation> annotations = new ArrayList<Annotation>();

				List<String> fromList = new ArrayList<String>();
				List<String> toList = new ArrayList<String>();
				for (int i = 0; i < map.getItemCount(); i++) {
					String str = map.getItemText(i);
					
					fromList.add(getFrom(str));
					toList.add(getTo(str));
				}
				for (AnnotationSet<Annotation> as : annList)
					for (Annotation a: as.annotations) {
						Annotation na = new Annotation(a, fromList, toList, traces);
						na.setType(annPrefix + ":" + na.getType());
//						na.setType("dummy");
//						na.setShortType("d");
						annotations.add(na);
					}

//				annManager.putAnnotationSet(annSetName, target, annotations);
				snapshot.getAnnotationModel().addAnnotationSet(annSetName,
						annotations);
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				RemapAnnotations.this.hide();
			}
			
		});
	}
	
	private String getMapString(String from, String to) {
		return from + " / " + to;
	}
	
	private String getFrom(String map) {
		return map.substring(0, map.indexOf(" / "));
	}
	
	private String getTo(String map) {
		return map.substring(map.indexOf(" / ") + 3);
	}
}
