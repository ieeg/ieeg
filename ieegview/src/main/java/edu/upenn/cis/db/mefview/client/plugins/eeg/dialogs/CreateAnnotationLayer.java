/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;


import java.util.Map;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.ComboBox;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class CreateAnnotationLayer extends DialogBox {
  ListBox datasetList = new ListBox();
  Button ok = new Button("OK");
  Button cancel = new Button("Cancel");
  TextBox layerName = new TextBox();
  AnnotationScheme curScheme;
  Map<String, AnnotationScheme> schemeSet;
  ComboBox<ComboBoxDataModel> schemeSelect;

  public CreateAnnotationLayer(
      final DataSnapshotModel currentModel,
      SessionModel sessionModel
      ) {
    setStyleName("ChanSelectDialog");

    // Get new default Scheme for potential new layer
    int nrGroups = currentModel.getAnnotationModel().getRootGroup().getSubGroups().size(); 

    curScheme = sessionModel.getUserPreferences().getAnnotationScheme("Scheme " + (nrGroups + 1) );

    VerticalPanel vp = new VerticalPanel();

    setTitle("Create Annotation Layer");
    setText("Create Annotation Layer");
    setWidget(vp);

    vp.add(new Label("Label: "));
    vp.add(layerName);
    
    layerName.setText("Layer " + (nrGroups + 1));

 // Create list of schemes
    schemeSet = sessionModel.getUserPreferences().getAnnSchemes();      
    final ComboBoxDataModel annSchemes = new ComboBoxDataModel();
    for(final String key : schemeSet.keySet()) {
      annSchemes.add(key, schemeSet.get(key).getName());
    }
    
    schemeSelect = new ComboBox<ComboBoxDataModel>();
    schemeSelect.setModel(annSchemes);//apply the model
    
    int index = 0;
    for (String key: schemeSet.keySet()){
      if(!key.equals(curScheme.getName()))
        index++;
      else
        break;
      
    }
    
    schemeSelect.select(index); 
    

    vp.add(new Label("Style: "));
    vp.add(schemeSelect);

    HorizontalPanel hp = new HorizontalPanel();
    hp.add(ok);
    hp.add(cancel);
    vp.add(hp);
    vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

    ok.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        hide();

        AnnotationGroup<Annotation> newGrp = new AnnotationGroup<Annotation>(
            schemeSet.get(schemeSelect.getSelected()), layerName.getText(), true);

        currentModel.getAnnotationModel().addOrExtendGroup(newGrp);
        currentModel.getAnnotationModel().selectGroup(newGrp);

      }

    });

    cancel.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        hide();
      }

    });
  }


  public String getTargetRevId() {
    return datasetList.getValue(datasetList.getSelectedIndex());
  }

  public String getTargetLabel() {
    return datasetList.getItemText(datasetList.getSelectedIndex());
  }
}
