package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.dom.client.Element;

import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.LoginSession.ILoginHandler;

public interface ILoginDialog extends DialogWithInitializer<ILoginDialog> {
	public void createDialog(ILoginHandler handler);
	
//	public void autoLogin(final String uid, final String pwd, final boolean hide);
	
//	public void login();
	
//	public void setSession(UserInfo result);
	
//	public void setup(IHeaderBar bar, ITabber tabber);
	
//	public String getUserID();
	
//	public String getPassword();

	public void show();
	
	public void hide();

//	public void initialize(LayoutPanel corePanel, IHeaderBar appHeader,
//			Map<String,ITabber> tabPanel, ITabSet tabset, ClientFactory clientFactory);

	public void showCentered();

	public String getUserID();

	public String getPassword();
	
	public Element getRoot();
	
	public void setUserID(String userID);
}
