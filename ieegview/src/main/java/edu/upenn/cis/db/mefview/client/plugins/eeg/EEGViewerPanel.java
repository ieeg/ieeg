/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.eeg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RequiresResize;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage;
import edu.upenn.cis.db.mefview.client.dialogs.IShowTraces;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.IEEGToolbar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.places.EEGPlace;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


//@SuppressWarnings("deprecation")
public class EEGViewerPanel extends AstroTab implements RequiresResize, EEGViewerPresenter.Display, IEEGViewerPanel {
	// msec to wait
	public static final int LAG = 800;
	PanelDescriptor desc;

	EEGPane eeg;

	int showHighlight;
	int hideHighlight;
	int cursor = 0;
	IEEGToolbar toolbar;
	
	boolean isWritable = false;
	public int graphHeight = 1000;
	public int graphWidth = 800;
	ListBox timeCombo;
	
	double traceScale = 1.0;
	
	LayoutPanel eegToolbarAndTracePanel = new LayoutPanel();

//	String user;
	List<INamedTimeSegment> traceList;
	
	private ISelectMontage selectMontageDialog;
	private IShowTraces showTracesDialog;
	
	public List<INamedTimeSegment> getTraceList() {
		return traceList;
	}

	DataSnapshotViews controller;	
	
	final static EEGViewerPanel seed = new EEGViewerPanel();
	public final static String NAME = "eeg";
	
	static {
		
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public EEGViewerPanel() {
		super();
	}
	
	public EEGViewerPanel( 
			final DataSnapshotViews controller, 
//			final String user, 
			final ClientFactory clientFactory,
			boolean isWritable,
			final String title) {
		
		super(clientFactory, controller, title);

		this.isWritable = isWritable;
//		this.user = user;
		this.controller = controller;
		desc = new PanelDescriptor("eeg", controller.getState().getSnapshotID());
//		Element el = Document.get().getElementById("mainContainer");
		
		computeSizes();	

		/* 
         * ToolbarWrapper can contain one or more Toolbars
         * Toolbar is a single row of elements.
         */
		FlowPanel toolbarWrapper = new FlowPanel();

		toolbar = GWT.create(IEEGToolbar.class);
		
		toolbarWrapper.add(toolbar);
        toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
		eegToolbarAndTracePanel.add(toolbarWrapper);
		
		add(eegToolbarAndTracePanel);
		eeg = new EEGPane(eegToolbarAndTracePanel, this,
				controller, 
				graphWidth, graphHeight,
				toolbar,
				null,
				clientFactory, title);
		
		toolbar.init(clientFactory, controller.getState() ,eeg);
	  }

	// Sync this with EEGPane.computeSizes()!
	public void computeSizes() {
		if (Document.get().getElementById("mainContainer") != null) {
			windowWidth = Document.get().getElementById("mainContainer").getOffsetWidth();
			windowHeight = Document.get().getElementById("mainContainer").getOffsetHeight() - 30;
		} else {
			windowWidth = Document.get().getElementById("rootLayoutPanel").getOffsetWidth();
			windowHeight = Document.get().getElementById("rootLayoutPanel").getOffsetHeight() - 30;
		}
//		windowWidth = Window.getClientWidth();
//		windowHeight = Window.getClientHeight();
		graphWidth =   windowWidth - 22;
		graphHeight =  windowHeight - 80;// - 120;
	}
	
	
	public void setTraceInfo(List<INamedTimeSegment> traces) {

		if (eeg != null)
			eeg.init(traces, null);
		
		if (traceList == null)
			traceList = new ArrayList<INamedTimeSegment>();
		else
			traceList.clear();
		Set<String> done = new HashSet<String>();
		for (INamedTimeSegment tr: traces) {
			if (!done.contains(tr.getId())) {
				traceList.add(tr);
				done.add(tr.getId());
			}
		}
	}
	
	public void setTracesAndPlace(final List<INamedTimeSegment> traces, final EEGPlace place) {
		setTraceInfo(traces);
	}
	
	Timer initEEG = null;
	public EEGPane getEEGPane() {
		return eeg;
	}
	
	public void hideStatus() {
		if (eeg != null)
			eeg.removeStatusMessage();
	}
	
	public boolean isSet() {
		return (eeg != null);//(traceList != null);
	}
	
	public List<String> getTraceLabels(){
	  List<String> labelList = new ArrayList<String>();
	  for (INamedTimeSegment i: traceList){
	    labelList.add(i.getLabel());
	    
	  }
	  return labelList;
	}
	
//	public String getUser() {
//		return user;
//	}

	public void updateRevId(final String oldId, final String newId) {
		getDataSnapshotState().updateSnapshotID(oldId, newId);
		
		if (getEEGPane() != null)
			getEEGPane().updateRevId(oldId, newId);
	}
	
	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
		if (getEEGPane() != null)
			getEEGPane().refreshAnnotations(newAnnotations);
	}

//	@Override
//	public void highlightMetadata(Collection<? extends PresentableMetadata> items) {
//		getEEGPane().highlightMetadata(items);
//	}


	@Override
	public void registerPrefetchListener(Prefetcher p) {
		if (eeg != null)
			eeg.registerPrefetchListener(p);
	}
	
	@Override
	public PanelDescriptor getDescriptor() {
		return desc;
	}

	@Override
	public void refresh() {
		if (eeg != null)
			eeg.refresh();
	}

	@Override
	public void onResize() {
		super.onResize();
		
		computeSizes();
		if (eeg != null)
			eeg.resize(graphWidth, graphHeight);
	}

	public String getPanelType() {
		return NAME;
	}

	@Override
	public DataSnapshotViews getDataSnapshotController() {
		if (eeg != null)
			return eeg.getDataSnapshotController();
		else
			return controller;
	}

	@Override
	public void setPlace(WindowPlace place) {
		eeg.setPlace(place);
	}

	public void close() {
		eeg.close();
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, //String userID,
			AppModel controller, DataSnapshotViews model,
//			Project project, 
			boolean writePermissions,
			final String title) {
		return new EEGViewerPanel(model, clientFactory, writePermissions, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getSignal();
	}

	@Override
	public WindowPlace getPlace() {
		return ((EEGViewerPresenter)getPresenter()).getPlace();
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void setClickHandlers(ClickHandler layer, ClickHandler trace,
			ClickHandler filter, ClickHandler montage,NumericEnterHandler vsize,
			ChangeHandler montageChange) {
		if (this.selectMontageDialog.getListBox()!=null)
			this.selectMontageDialog.getListBox().addChangeHandler(montageChange);
		
		this.toolbar.setClickHandlers(layer, trace, filter, montage, vsize);
		
	}

	@Override
    public void showLayerDialog() {
        eeg.layerPopup();
    }
	
//	@Override
//	public void showTraceDialog() {
//		eeg.tracePopup();
//	}

	@Override
	public void showFilterDialog() {
		eeg.filterPopup();
	}

//	@Override
//	public void showMontageDialog() {
//		eeg.montagePopup();
//	}

//	@Override
	public void setTraces(List<INamedTimeSegment> traces) {
		if (eeg != null)
//			eeg.setupLabels(traces);
			eeg.init(traces, null);
		
		traceList = traces;
	}

	@Override
	public void setScale(double scale) {
		eeg.setScale(scale);
	}


	@Override
	public void setFocusOnGraph() {
		getEEGPane().resetFocus();
	}
	
	public String getTitle() {
		return "EEG";
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		super.setDataSnapshotModel(model);
		if (eeg != null)
			eeg.setDataSnapshotModel(model);
		traceList = model.getTraceInfo();
	}
	
	@Override
	public DataSnapshotModel getDataSnapshotState() {
//		GWT.log("EEGViewerPane.getDataSnapshotState() == " + eeg.getDataSnapshotState() + ", " + controller.getState());
		if (eeg != null)
			return eeg.getDataSnapshotState();
		else
			return controller.getState();
	}

	public String getWindowTitle() {
		return getEEGPane().getWindowTitle();
	}

	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public void saveAndDeleteAnnotations() {
		getEEGPane().saveAndDeleteAnnotations();
	}

	@Override
	public IEEGToolbar getToolbar() {
		// TODO Auto-generated method stub
		return toolbar;
	}

	@Override
	public void updateMontage(EEGMontage montage) {
		GWT.log("Updating the montage in EEGViewerPanel - " + montage);
		eeg.updateMontage(montage);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
		toolbar.bindDomEvents();

	}

	@Override
	public void setShowTracesDialog(IShowTraces dialog) {
		this.showTracesDialog = dialog;
	}
	
	@Override
	public void setSelectMontageDialog(ISelectMontage dialog) {
		this.selectMontageDialog = dialog;
	}

	@Override
	public ISelectMontage getSelectMontageDialog() {
		return selectMontageDialog;
	}

	@Override
	public IShowTraces getShowTracesDialog() {
		return showTracesDialog;
	}


	
}
