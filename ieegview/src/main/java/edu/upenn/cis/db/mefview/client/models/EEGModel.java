/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayNumber;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.i18n.client.DateTimeFormat;

import edu.upenn.cis.db.mefview.client.util.DataConversion;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class EEGModel {
	JsArrayString traceLabels;
//	List<String> channels;
	List<INamedTimeSegment > traces;
	List<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();
	
	public EEGModel() {
//		annotations = new TreeMap<Double,HashMap<String,Set<String>>>();
//		channels = new ArrayList<String>();
		traces = new ArrayList<INamedTimeSegment >();
//		this.pane = pane;
	}

	public List<String> getChannels() {
//		return channels;
		List<String> ret = new ArrayList<String>();
		
		for (INamedTimeSegment  t: traces) {
			ret.add(t.getLabel());
		}
		
		return ret;
	}
	
	public List<String> getChannelPaths() {
		List<String> ret = new ArrayList<String>();
		
		for (INamedTimeSegment  t: traces) {
			ret.add(t.getId());
		}
		
		return ret;
	}
	
	public void setTraces(List<? extends INamedTimeSegment> traces) {
		this.traces.clear();
		this.traces.addAll(traces);
	}

	public int getChannelFor(String channel) {
		int ret = getChannels().indexOf(channel);
		
		return ret;
	}

	public int getNumChannels() {
		return traces.size();
	}
	
	public double getStartDate() {
		double start = Double.MAX_VALUE;
		
		for (INamedTimeSegment  t : traces)
			if (t.getStartTime() < start)
				start = t.getStartTime();
		
		return start;
	}
	
	public double getDurationMicros() {
		double duration = Double.MIN_VALUE;
		
		for (INamedTimeSegment  t : traces) {
			if (t.getDuration() > duration) {
				duration = t.getDuration();
			}
		}
		
		return duration;
	}

	public String getChannelName(int channelId) {
		return traces.get(channelId).getLabel();
	}
	
	public String getChannelRevId(int channelId) {
		return traces.get(channelId).getId();
	}
	
	public INamedTimeSegment  getChannelTraceInfo(int channelId) {
		return traces.get(channelId);
	}
	
	public INamedTimeSegment getChannelTraceInfo(String channelRevID) {
		for (int i = 0; i < traces.size(); i++)
			if (traces.get(i).getId().equals(channelRevID))
				return traces.get(i);
		return null;
	}
	
	public List<INamedTimeSegment> getTraceList() {
		return traces;
	}
	
	public String getChannelRevId(String channel) {
		if (channel.isEmpty())
			return getChannelRevId(0);
		else
			return getChannelRevId(getChannelFor(channel));
	}

	static native JsArray<?> adjustValues(final JsArray<?> arr,
			final JsArrayNumber gaps) /*-{
	            var len = arr.length;
	            var arr2 = new Array();
	            for (var i = 0; i < len; i++) {
		            var samples = arr[i].length;
		            arr2[i] = new Array();
		            arr2[i][0] = arr[i][0];

		            for (var j = 1; j < samples; j++) {
		            	if (arr[i][j] != null)
		            		arr2[i][j] = arr[i][j] + gaps[j-1];
		            }
	            }
	            return arr2;
	        }-*/;

	public JsArrayString getTraceLabels() {
		return traceLabels;
	}

	public void setTraceLabels(JsArrayString traceLabels) {
		this.traceLabels = traceLabels;
	}

	public native JsArrayString getTraceLabels(final int numChannels) /*-{
	    		var tl = ["time"];

	    		for (var i = 0; i < numChannels; i++ ) {
	    			tl.push(this.@edu.upenn.cis.db.mefview.client.models.EEGModel::getChannelName(I)(i));
	    		}

	    		return tl;
	    	}-*/;


	public List<INamedTimeSegment> getTraceInfo(List<String> channels) {
		List<INamedTimeSegment> ret = new ArrayList<INamedTimeSegment>();
		for (String ch: channels) {
			for (INamedTimeSegment  t: traces)
				if (t.getLabel().equals(ch)) {
					ret.add(t);
					break;
				}
		}
		return ret;
	}
	
	/**
	 * Gets a set of default values for the channels
	 * @param numChannels
	 * @return
	 */
	public static native JsArray<JsArrayInteger> getDefaultValues(final int numChannels) /*-{
			var tl = [];
			tl[0] = [];
		
			tl[0].push("0.0");
		
			for (var i = 0; i < numChannels; i++ ) {
				tl[0].push((numChannels - i - 1) * 100);
			}
		
			return tl;
		}-*/;

	// Looks at a JSON array of the form [[x,y1,y2], ...] for the first item
	public native static final double getStartDate(final double startTime,
			final JsArray<JsArrayInteger> data) /*-{
			if (data.length == 0)
				return 0;
			return data[0][0] - startTime;
		}-*/;

	// Looks at a JSON array of the form [[x,y1,y2], ...] for the first item
	public native static final double getPeriod(final JsArray<JsArrayInteger> data) /*-{
			if (data.length < 2)
				return 1;
			else
				return data[1][0] - data[0][0];
		}-*/;

	public native static final double getEndDate(final double startTime,
			final JsArray<JsArrayInteger> data) /*-{
			if (data.length == 0)
				return 0;
			return data[data.length - 1][0] - startTime;
		}-*/;

	// Looks at a JSON array of the form [[x,y1,y2], ...] for the first item
	native static final int getJsonStart(final JsArray<JsArrayInteger> data) /*-{
			if (data.length == 0)
				return 0;
			return parseInt(data[0][0]);
		}-*/;

	native static final int getJsonEnd(final JsArray<JsArrayInteger> data) /*-{
			if (data.length == 0)
				return 0;
			return parseInt(data[data.length - 1][0]);
		}-*/;

	native static final JsArray<?> concat(final JsArray<?> first, final JsArray<?> second) /*-{
			return first.concat(second);
		}-*/;
	
	public static native JsArray<JsArray<?>> parseArray(final String json) /*-{
		return JSON.parse(json);
	}-*/;
	
	public static native double getRequestedStart(final JsArray<JsArray<?>> ar) /*-{
		return ar[0][3];
	}-*/;

	static native double getRequestedDuration(final JsArray<JsArray<?>> ar) /*-{
		return ar[0][4];
	}-*/;

	static native JsArray<JsArrayInteger> transpose(final JsArray<JsArrayInteger> tempArray) /*-{
		var ret = [];
		for (var i = 0; i < tempArray[0].length; i++) {
			ret[i] = [];
		}
		
		for (var j = 0; j < tempArray.length; j++) {
			for (var i = 0; i < tempArray[0].length; i++)
				if (tempArray[j][i] && !isNaN(tempArray[j][i]))
					ret[i][j] = tempArray[j][i];
				else
					ret[i][j] = 0;
		}
		
		console.debug("Matrix " + ret.length + " by " + ret[0].length);
		
		return ret;
	}-*/;

	public static native boolean isMinMax(final JsArray<JsArray<?>> tempArray) /*-{
			var isMinMax = tempArray[0][5];
			return isMinMax;
	}-*/;
			
	public static native JsArray<JsArrayInteger> getParsedArray(final double startInx, 
			final JsArray<JsArray<?>> tempArray) /*-{
			var ret = [];

			try {		
				// Pointer on the temp array
				var index = 0;
				var outIndex = 0;
				var blockCount = 0;
				
				var start = (new Date()).getTime();
			
				var lastCount = 0;
				while (index < tempArray.length) {
			
					blockCount++;
					// We get a block:  first column gives us info about start time position, time period per
					// column, number of items 
			    	var pos = tempArray[index][0] - startInx;
			    	var period = tempArray[index][1];
			    	var count = tempArray[index][2];
	//		    	var start = tempArray[index][3];
	//		    	var length = tempArray[index][4];
					var isMinMax = tempArray[index][5];

				    console.debug("Decoding " + count + " entries [" + pos + "-" + (pos + (count-1) * period) + "] (period " + period + ") with minMax = " + isMinMax);
			
			    	index++;
			
			    	lastCount = outIndex;
			    	var periodFactor = 0
			    	for (var i = 0; i < count; i++ ) {
			    		var time = ((periodFactor*period + 0.5)|0) + pos;
			    		ret.push(new Array());
			    		ret[outIndex] = tempArray[index];
			    		ret[outIndex].unshift(time);
			    		index++;
			    		outIndex++;
			    		if (!isMinMax || (isMinMax && i % 2 != 0)) {
			    			periodFactor++;
			    		}
					}
				}
			} catch (err) {
				console.debug("Error " + err);
			}
		
			var t2 = (new Date()).getTime();
			console.debug("Decode timing: " + (t2 - start) + "msec");
			
			return ret;
		}-*/;

	public static String getDateValue(double date) {
		String fragment = String.valueOf(((long)date) % 1000000);
		while (fragment.length() < 6)
			fragment = "0" + fragment;
		// yyyy/MM/dd HH:mm:ss
		return DateTimeFormat.getFormat("yyyy/MM/dd HH:mm:ss").format(new Date((long)(date / 1000))) + "." + fragment;
//		return DateTimeFormat.getShortDateTimeFormat().format(new Date((long)(date / 1000))) + "." + fragment;
	}
	
	public static JsArrayInteger getValues(TimeSeries ts) {
		JsArrayInteger arr = DataConversion.wrapArray(ts.getSeries());
		
		return setNulls(arr, getGapStarts(ts), getGapEnds(ts));
	}
	
	public native static JsArrayInteger setNulls(final JsArrayInteger values, 
			final JsArrayInteger start, final JsArrayInteger end) /*-{
				
				return values;
		for (var i = 0; i < start.length; i++) {
			for (var pos = start[i]; pos < end[i]; pos++)
				//values[pos] = null;
//				values[pos] = Integer.MIN_VALUE;
				delete values[pos];
		}
		return values;
	}-*/;
	
	public static JsArrayInteger getGapStarts(TimeSeries ts) {
		return DataConversion.wrapArray(ts.getGapsStart());
	}

	public static JsArrayInteger getGapEnds(TimeSeries ts) {
		return DataConversion.wrapArray(ts.getGapsEnd());
	}

  public static JsArray<JsArrayInteger> getTimeSeriesArray(final TimeSeries[] ts) {
	  JsArrayInteger[] arrays = new JsArrayInteger[ts.length + 1];
	  
	  arrays[0] = DataConversion.createTimeVector(ts[0].getStartTime(), ts[0].getPeriod(), 
			  ts[0].getSeries().length);
	  for (int i = 0; i < ts.length; i++)
		  arrays[i+1] = EEGModel.getValues(ts[i]);

	  return DataConversion.wrapArrayOfArrays(arrays);
  }

	public List<DisplayConfiguration> getChannelDisplayConfigurations() {
		return filters;
	}
	
	public DisplayConfiguration getChannelDisplayConfiguration(int i) {
		return filters.get(i);
	}
	
	public void setChannelDisplayConfigurations(List<DisplayConfiguration> filters) {
		this.filters = filters;
	}
	
	public List<INamedTimeSegment> getTraces() {
		return traces;
	}
	
//	public void setChannels(List<String> channels) {
//		this.channels = channels;
//	}

	public void addTrace(INamedTimeSegment trace) {
		traces.add(trace);
//		if (filters.size() < traces.size())
//			filters.add(new FilterParameters());
	}
	
	public void addAllTraces(Collection<? extends INamedTimeSegment> traces) {
		for (INamedTimeSegment  ti: traces)
			addTrace(ti);
	}

//  public static JsArray<JsArrayInteger> getSpanArray(long startTime,
//		  double period, int count, final List<TimeSeriesSpan> tss) {
//	  JsArrayInteger[] arrays = new JsArrayInteger[tss.size() + 1];
//	  
//	  arrays[0] = DataConversion.createTimeVector(startTime, period, count);
//	  System.out.println(arrays[0].length());
//	  for (int i = 0; i < tss.size(); i++)
//		  arrays[i+1] = tss.get(i).getSeriesJs();
//
//	  return transpose(DataConversion.wrapArrayOfArrays(arrays));
//  }
  
  
}
