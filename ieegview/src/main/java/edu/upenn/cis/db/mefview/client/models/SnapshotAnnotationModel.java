/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.AnnotationCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationGroupCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationGroupRemovedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class SnapshotAnnotationModel {
  DataSnapshotModel model;
  AnnotationGroup<Annotation> annGroup;
  AnnotationGroup<Annotation> selectedGroup;	
  Set<Annotation> deleted;
  Set<AnnotationGroup<Annotation>> deletedLayer;


  public SnapshotAnnotationModel(DataSnapshotModel model) {
    this.model = model;

    annGroup = new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, 
        model.getFriendlyName(), false);

    deleted = new HashSet<Annotation>();
    deletedLayer = new HashSet<AnnotationGroup<Annotation>>();
  }

  public void selectGroup(AnnotationGroup<Annotation> group) {
    selectedGroup = group;
  }

  public AnnotationGroup<Annotation> getSelectedGroup() {
    return selectedGroup;
  }

  public AnnotationGroup<Annotation> getGroupFor(Annotation ann) {
    return annGroup.getGroupFor(ann);
  }
  
  public AnnotationGroup<Annotation> getGroupWithName(String name) {
	    for (AnnotationGroup<Annotation> g: annGroup.getSubGroups())
	        if (g.getName().equals(name))
	        	return g;
	  
	    return null;
  }

  public void addOrExtendGroup(AnnotationGroup<Annotation> group){
    for (AnnotationGroup<Annotation> g: annGroup.getSubGroups())
      if (g.getName().equals(group.getName())) {
        g.getAnnotations().addAll(group.getAnnotations());
        g.getSubGroups().addAll(group.getSubGroups());
        return;
      }

    // Group does not exist --> add group
    annGroup.addSubGroup(group);
    if (model == null)
      throw new RuntimeException("No model!");
    if (group == null)
      throw new RuntimeException("No group!");
    // Set Active Layer in annotationModel
    this.selectGroup(group);
    IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationGroupCreatedEvent(model, group));
  }

  public AnnotationGroup<Annotation> getRootGroup() {
    return annGroup;
  }

  public List<Annotation> getAllEnabledAnnotations() {
    return annGroup.getAllEnabledAnnotations();
  }

  public void addAnnotationSet(String groupName, List<Annotation> as) {
    AnnotationGroup<Annotation> subGroup = 
        new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, 
            groupName, as);
    annGroup.addSubGroup(subGroup);
  }

  public void removeAnnotationGroup(AnnotationGroup<Annotation> as) {

    if (!as.isNew()){
      deletedLayer.add(as);
      System.out.println("Added to DeleteGroup");
    }

    annGroup.removeSubGroup(as);
    annGroup.removeSubGroup(as.groupName);
    
    if (selectedGroup == as){
      selectedGroup = null;
    }

    IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationGroupRemovedEvent(model, as, selectedGroup));

  }

  @SuppressWarnings("unchecked")
  public List<SliderBar.AnnBlock> getAnnotationTimes() {
    //return annotations.keySet();
    List<SliderBar.AnnBlock> ret = new ArrayList<SliderBar.AnnBlock>();

    for (Annotation a : getAllEnabledAnnotations()) {
      ret.add(new SliderBar.AnnBlock(a.getStart(),a.getEnd()));
    }
    Collections.sort(ret);
    return ret;
  }

  @SuppressWarnings("unchecked")
  public List<SliderBar.AnnBlock> getAnnotationTimes(final String annType) {
    if (annType.equals("Any"))
      return getAnnotationTimes();
    List<SliderBar.AnnBlock> ret = new ArrayList<SliderBar.AnnBlock>();

    for (Annotation a : getAllEnabledAnnotations()) {
      if (a.getType().equals(annType))
        ret.add(new SliderBar.AnnBlock(a.getStart(),a.getEnd()));
    }
    Collections.sort(ret);
    return ret;
  }

  private static int count = 0;
  public AnnotationGroup<Annotation> addAnnotation(Annotation a) {
    if (a.getRevId() == null || a.getRevId().equals(""))
      a.setInternalId("- internal -" + (++count));

    selectedGroup.addAnnotation(a);
    IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationCreatedEvent(model, selectedGroup, a));

    return selectedGroup;
  }

  public void removeAnnotation(Annotation a) {
    if (!a.isNew())
      deleted.add(a);

    annGroup.removeAnnotation(a);
    if (!(selectedGroup == null))
      selectedGroup.removeAnnotation(a);
  }

  public Set<Annotation> getDeletedAnnotationList() {
    return deleted;
  }

  public void clearDeletedAnnotationList() {
    deleted.clear();
  }

  /**
   * Return the set of channels that have annotations
   * 
   * @return
   */
  public Set<String> getAnnotatedChannels() {
    Set<String> annotated = new HashSet<String>();

    for (Annotation a : getAllEnabledAnnotations()) {
      annotated.addAll(a.getChannels());
    }

    return annotated;
  }

  /**
   * Returns the (first) matching annotation
   */
  public Annotation getMatchingAnnotation(String annName) {//String series, double startTime, double stopTime) {
    for (Annotation a : getAllEnabledAnnotations()) {
      if (a.getInternalId().equals(annName)) {
        return a;
      }
    }
    return null;
  }

  /**
   * Returns a JSON string representing all annotations within the time range specified
   * 
   * @param startTime
   * @param stopTime
   * @return
   */
  public String getRelevantAnnotations(double startTime, double stopTime,
      List<AnnotationDefinition> defs) {
    //		boolean isFirst = true;
    //graph.setAnnotations("[]");
    StringBuilder annot = new StringBuilder("[");
    int count = 0;

    for (Annotation a : getAllEnabledAnnotations()) {
      if (! (a.getEnd() < startTime || a.getStart() > stopTime)) {
        if (count != 0)
          annot.append(',');
        String curColor = annGroup.getColorFor(a);
        if (curColor.isEmpty())
          curColor = "grey";
        annot.append(a.toJSON2(false, curColor, defs,
            model.getChannelNames()));
        count++;
      }
    }
    
    annot.append(']');
    GWT.log("Annotations: " + annot.toString());
    
    return annot.toString();
  }

  public Set<AnnotationGroup<Annotation>> getDeletedAnnotationLayerList() {
    return deletedLayer;
  }

}
