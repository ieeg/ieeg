/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;

public class MetadataUpdatedEvent extends GwtEvent<MetadataUpdatedEvent.Handler> {
	private static final Type<MetadataUpdatedEvent.Handler> TYPE = new Type<MetadataUpdatedEvent.Handler>();
	
	List<GeneralMetadata> newOrUpdatedResults;
	List<GeneralMetadata> deletedResults;
	
	private boolean doReplace = false;;
	
	public MetadataUpdatedEvent(List<GeneralMetadata> result,
			List<GeneralMetadata> deleted, boolean replace) {
		this.newOrUpdatedResults = result;
		deletedResults = deleted;
		doReplace = replace;
	}
	
	public MetadataUpdatedEvent(Collection<GeneralMetadata> results, boolean replace) {
		this.newOrUpdatedResults = new ArrayList<GeneralMetadata>();
		newOrUpdatedResults.addAll(results);
		deletedResults = null;
		doReplace = replace;
	}
	
	public void setDoReplace(boolean value) {
		doReplace = value;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<MetadataUpdatedEvent.Handler> getType() {
		return TYPE;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MetadataUpdatedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(MetadataUpdatedEvent.Handler handler) {
		handler.onMetadataChanged(newOrUpdatedResults, deletedResults, doReplace);
	}

	public static interface Handler extends EventHandler {
		void onMetadataChanged(List<GeneralMetadata> newResults,
				List<GeneralMetadata> deletedResults, boolean replaceResults);
	}
}
