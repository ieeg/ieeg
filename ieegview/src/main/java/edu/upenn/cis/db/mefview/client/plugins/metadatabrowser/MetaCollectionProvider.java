/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.viewers.MetadataTree;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * Generic collection node
 * 
 * @author zives
 *
 */
public class MetaCollectionProvider extends MetadataProviderBase {
		MetadataModel model;
		
		private String addToCollection;
		
		public MetaCollectionProvider(final CollectionNode item, final MetadataModel model, 
				final ClientFactory clientFactory, String coll, final MetadataTree notify,
				final Set<String> knownIds) {
			super(item, model, clientFactory, notify, knownIds);

			if (coll != null)
				addToCollection = coll;
			
			if (item != null) {
//				GWT.log("MDP cons expanding collection node");
				
				addAll(MetadataFactory.getDrillDown(item.getClass()).getChildren(item, this,
						0, PAGE_SIZE));
			}

			if (result != null) {
//				GWT.log("MDP Starting root with " + result.size() + " entries");
//				updateRowCount(result.size(), false);
				int showThis = result.size();
				if (showThis > startPage + PAGE_SIZE)
					showThis = startPage + PAGE_SIZE;
				updateRowData(0, result.subList(startPage, showThis));
				updateRowCount(showThis - startPage, true);
			} else {
				int showThis = result.size();
				if (showThis > startPage + PAGE_SIZE)
					showThis = startPage + PAGE_SIZE;
				updateRowData(0, result.subList(startPage, showThis));
				updateRowCount(showThis - startPage, true);
//				GWT.log("MDP starting with no details on root");
				
			}
		}

		public void clearSearchResults() {
			if (item != null && item.getLabel().equals(addToCollection)) {
				if (item instanceof CollectionNode) {
					CollectionNode collectionNode = (CollectionNode)item;
					for (GeneralMetadata md2 : collectionNode.getChildMetadata()) {
						if (md2.getId() != null) {
							existingIds.remove(md2.getId());
							treePresenter.onItemRemoved((PresentableMetadata) md2);
						}
					}
					collectionNode.clear();
				}
				result.clear();
			}
		}
	
		@Override
		protected void onRangeChanged(HasData<PresentableMetadata> display) {
			final Range range = display.getVisibleRange();
//			
			setStart(range.getStart());
			setEnd(getStartIndex() + range.getLength());
			
			Set<PresentableMetadata> removed;
			if (item != null) {
				removed = clearResults(result);
			} else
				removed = new HashSet<PresentableMetadata>();
			
			if (item == null) {
				if (model != null) {
					List<PresentableMetadata> ss = new ArrayList<PresentableMetadata>();
					ss.addAll(model.getKnownSnapshots());
					MetadataModel.sortMetadata(ss);
					addAll(ss);
					
					thresholdEnd();

					int showThis = result.size();
					if (showThis > startPage + PAGE_SIZE)
						showThis = startPage + PAGE_SIZE;
					updateRowData(getStartIndex(), result.subList(startPage, showThis));
					updateRowCount(showThis - startPage, true);
				}
				
			} else if (item instanceof CollectionNode) {
				GWT.log("MDP expanding collection node");

				addAll(MetadataFactory.getDrillDown(item.getClass()).getChildren(item, this,
						getStartIndex(), getStartIndex() + PAGE_SIZE));
				
				thresholdEnd();
				int showThis = result.size();
				if (showThis > startPage + PAGE_SIZE)
					showThis = startPage + PAGE_SIZE;
				
				if (result.size() == 0)
					updateRowCount(0, true);
				else {
					updateRowData(getStartIndex(), result.subList(startPage, showThis));
					updateRowCount(showThis - startPage, true);
				}
			}
			
			removeUnused(removed);
		}

		@Override
		public List<PresentableMetadata> getNewRootContent() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<PresentableMetadata> getNewItemContent() {
			GWT.log("MDP expanding collection node");

			List<PresentableMetadata> children = MetadataFactory.getDrillDown(item.getClass()).getChildren(item, this,
					getStartIndex(), getStartIndex() + PAGE_SIZE);
			
//			System.out.println("Preparing to add to collection " + children.size() + " items");
			
			return filter(children);
		}

		@Override
		public void categorizeResults(List<PresentableMetadata> result) {
			// TODO Auto-generated method stub
//			System.out.println("** Null categorize results ** ");
		}

		@Override
		public Set<PresentableMetadata> getSelected() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void restoreSelected(Collection<PresentableMetadata> sel) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public List<PresentableMetadata> getChildMetadata() {
			List<PresentableMetadata> ret = getNewItemContent();
			
//			System.out.println("COLLECTION PROVIDER / GET CHILD METADATA " + ret.size());
			result.clear();
			result.addAll(ret);
			return result;
		}

		@Override
		public void toggleRating(PresentableMetadata collectionItem) {
			// TODO Auto-generated method stub
			
		}
	}