package edu.upenn.cis.db.mefview.client.plugins.userprofile.cis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount.CreateAccountPresenter;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.BaseUserProfilePanelPlugin;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.ButtonEntry;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;

public class CisUserProfile extends BaseUserProfilePanelPlugin {
	public static final String NAME = "Studio455";


	public CisUserProfile(SessionModel model) {
		super(model);
	}

	@Override
	public FlowPanel getHighlightedButtons() {
	      FlowPanel startButtons = new FlowPanel();
	      
	      Button reqButton = new Button();
	      
	      reqButton.setHTML("Sign up");
	      
	      reqButton.setPixelSize(100, 50);
	      reqButton.getElement().getStyle().setFloat(Float.LEFT);
	      reqButton.setStylePrimaryName("buttonHolder");
	      reqButton.setStyleName("buttonGreen", true);
	      reqButton.addStyleName("startButtons");
	      reqButton.addClickHandler(new ClickHandler() {

	        @Override
	        public void onClick(ClickEvent event) {
//	          Window.open("https://main.ieeg.org/?q=user/register","IEEG-Portal Create user account",null);
				OpenDisplayPanel act = new OpenDisplayPanel(
						null,
						false,
						CreateAccountPresenter.NAME,
						null);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
	        }
	      });
	      
	      startButtons.add(reqButton);

	      return startButtons;
	}

	@Override
	public List<Widget> getWidgets() {
		return new ArrayList<Widget>();
	}

	@Override
	public FlowPanel getLogos() {
	      FlowPanel logoBanner = new FlowPanel();
	      logoBanner.setStylePrimaryName("ieeg-logoBannerHolder");

	      Grid g = new Grid(1,1);
	      g.addStyleName("ieeg-portal-collabTable");
	      Image penn = new Image(ResourceFactory.getPenn().getSafeUri());
	      g.setWidget(0, 0, penn);

	      penn.addStyleName("collaborator");
	      logoBanner.add(g);
	      
	      return logoBanner;
	}

	@Override
	protected List<String> getBannerList() {
		return Arrays.asList(new String[] {
				"pic1.jpg",
				"pic2.jpg",
				"pic3.jpg",
				"pic4.jpg",
				"screenshot-1.png",
				"team.jpg"
		});
	}

	@Override
	  public List<ButtonEntry> getButtons() {
		  return Arrays.asList(new ButtonEntry[] {
				  new ButtonEntry(
						  "http://www.cis.upenn.edu/~cis455/img/pic4.jpg",
						  "CIS 455 Page", 
						  "http://www.cis.upenn.edu/~cis455",
						  "CIS 455 Page",
						  false),
				  new ButtonEntry(
						  "http://www.cis.upenn.edu/~cis455/img/pic2.jpg",
						  "Assignments", 
						  "http://www.cis.upenn.edu/~cis455/assignments.html",
						  "Assignments",
						  false),
				  new ButtonEntry(
						  "http://piazza.com/images/splash/PageTop/Piazza-Icon.png",
						  "Piazza", 
						  "https://piazza.com/class/i4vn9s3ilfg2hs?cid=18",
						  "Piazza",
						  false)
		  });
	  }

}
