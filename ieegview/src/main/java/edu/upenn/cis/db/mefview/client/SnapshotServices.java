/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.IeegException;
import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationModification;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.DataBundle;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.SnapshotAnnotationInfo;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;
import edu.upenn.cis.db.mefview.shared.TsEventType;

/**
 * The client side stub for the RPC service for accessing a data snapshot.
 */
@RemoteServiceRelativePath("snapshot")
public interface SnapshotServices extends RemoteService {
	public DataSnapshotIds addAndRemoveAnnotations(SessionToken sessionID, String datasetID, 
	    Set<Annotation> annotationsToSave, Set<Annotation> annotationsToRemove,
	    Set<String> annotationLayersToRemove) 
	throws BtSecurityException;
	
	public String addDataSnapshotTimeSeries(SessionToken sessionID, String revID, Set<String> series)
	throws BtSecurityException;

	public SnapshotAnnotationInfo getAnnotationInfo(SessionToken sessionID, String snapshot, double startPosition,
			double endPosition) throws BtSecurityException;

	public List<Annotation> getDataSnapshotAnnotations(SessionToken sessionID, final String dataSnapshot) throws BtSecurityException;
	
	Set<DataBundle> getContentInfo(SessionToken sessionID, final String dataSnapshot) throws BtSecurityException;

	public StudyMetadata getEegStudyMetadata(SessionToken sessionID, final String studyRevId)
	throws BtSecurityException;
	
	public CaseMetadata getCaseMetadata(SessionToken sessionID, final String studyRevId)
			throws BtSecurityException;

	public ExperimentMetadata getExperimentMetadata(SessionToken sessionID, String experimentId) throws BtSecurityException;

	
	public SearchResult getSearchResultForSnapshot(
			SessionToken sessionID, 
			final String dataSnapshotRevId) throws 
			BtSecurityException;

	/**
	 * Get the images associated with the study + trace
	 * @param trace
	 * @param typ
	 * @param study
	 * 
	 * @return
	 * 
	 */
	Set<ImageInfo> getImageInfo(SessionToken sessionID, String studyPath, String traceRev, String trace, String typ) throws BtSecurityException;

	/**
	 * Get the images associated with this study
	 * @param studyPath Study directory
	 * @param typ Image type
	 * 
	 * @return
	 * 
	 */
	Set<ImageInfo> getImagesForStudy(SessionToken sessionID, String datasetID, String typ) 
			throws BtSecurityException;

	public Set<ImageInfo> getImagesForStudy(SessionToken sessionID,
			String datasetID) throws BtSecurityException;

	/**
	 * Get the traces associated with this image file
	 * @param study
	 * @param image
	 * 
	 * @return
	 * 
	 */
	Set<TraceImageInfo> getTracesForImage(SessionToken sessionID, String imageWithPath) throws BtSecurityException;

	/**
	 * TODO: (reserved) find the next position where we have non-null data
	 * 
	 * @param sessionID
	 * @param dataSetID
	 * @param position
	 * @return
	 * @throws BtSecurityException
	 */
	double getNextValidDataRegion(SessionToken sessionID, String dataSetID, double position) throws
	BtSecurityException;

	/**
	 * TODO: (reserved) find the previous position where we have non-null data
	 * 
	 * @param sessionID
	 * @param dataSetID
	 * @param position
	 * @return
	 * @throws BtSecurityException
	 */
	double getPreviousValidDataRegion(SessionToken sessionID, String dataSetID, double position) throws
	BtSecurityException;

	/**
	 * Get the analyses for a study
	 * @param studyRevId
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	public Set<DerivedSnapshot> getRelatedAnalyses(SessionToken sessionID, String studyRevId) throws BtSecurityException;
	
	public Set<SearchResult> getDerivedSnapshots(SessionToken sessionID, String studyRevId) throws BtSecurityException;

	TimeSeries[] getResults(
			SessionToken sessionID, 
			String contextID, 
			String datasetID, 
			List<String> traceids, 
			double start, 
			double width, 
			double samplingPeriod, 
			List<DisplayConfiguration> filter, 
			SignalProcessingStep processing) 
					throws BtSecurityException;

	/**
	 * Get the time series data
	 * @param datasetID
	 * @param traceids
	 * @param start
	 * @param width
	 * @param samplingPeriod
	 * 
	 * @return
	 * @throws BtSecurityException
	 * @throws ServerTimeoutException TODO
	 */
	String getResultsJSON(
			SessionToken sessionID, 
			String contextID, 
			String datasetID, 
			List<INamedTimeSegment> traceids, 
			double start, 
			double width, 
			double samplingPeriod, 
			List<DisplayConfiguration> filter, 
			SignalProcessingStep processing) 
					throws BtSecurityException, IeegException, ServerTimeoutException;

//	String getResultsJSON(
//			SessionToken sessionID, 
//			String contextID, 
//			String datasetID, 
//			List<String> traceids, 
//			double start, 
//			double width, 
//			int scaleFactor, 
//			SignalProcessingStep processing) 
//					throws BtSecurityException, IeegException, ServerTimeoutException;
//	
//	AnnotationsAndJson getAnnotatedResults(
//			SessionToken sessionID, 
//			String contextID, 
//			String datasetID, 
//			List<String> traceids, 
//			double start, 
//			double width, 
//			double samplingPeriod, 
//			List<DisplayConfiguration> filter, 
//			SignalProcessingStep processing) 
//					throws BtSecurityException, IeegException, ServerTimeoutException;
//
//	AnnotationsAndJson getAnnotatedResults(
//			SessionToken sessionID, 
//			String contextID, 
//			String datasetID, 
//			List<String> traceids, 
//			double start, 
//			double width, 
//			int scaleFactor, 
//			SignalProcessingStep processing) 
//					throws BtSecurityException, IeegException, ServerTimeoutException;

	public Set<AllowedAction> getPermittedActionsForDataset(SessionToken sessionID, String dsId) throws BtSecurityException;

	public SnapshotContents getSnapshotMetadata(SessionToken sessionID, String snapshotRevId) throws BtSecurityException;
	
	/**
	 * Remove Annotation Layers
	 * @param datasetID
	 * @param annotationLayersToRemove
	 * @return
	 * @throws BtSecurityException
	 */
	 Integer removeAnnotationLayers(SessionToken sessionID, String datasetID,
	      Set<String> annotationLayersToRemove) throws BtSecurityException;

	
	/**
	 * Remove the set of annotations
	 * @param datasetID
	 * @param annotations
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	String removeAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations) throws BtSecurityException;

	/**
	 * Remove series from the snapshot
	 * 
	 * @param sessionID
	 * @param revID
	 * @param series
	 * @return
	 * @throws BtSecurityException
	 */
	public String removeDataSnapshotContents(SessionToken sessionID, String revID, Set<String> series)
	throws BtSecurityException;
	
	/**
	 * Save a set of annotations for this dataset
	 * @param datasetID
	 * @param annotations
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	DataSnapshotIds saveAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations) throws BtSecurityException;

	/**
	 * Stores the XML CLOB for a data snapshot
	 * @param dataSnapshotRevId
	 * @param clob
	 * 
	 * @throws BtSecurityException
	 */
	public String storeDataSnapshotXml(SessionToken sessionID, String dataSnapshotRevId, String clob) 
	throws BtSecurityException;

	/**
	 * TODO: (reserved API call) get user places associated with this snapshot
	 * 
	 * @param sessionID
	 * @param dataSnapshotRevId
	 * @return
	 * @throws BtSecurityException
	 */
	List<Place> getPlaces(SessionToken sessionID, String dataSnapshotRevId)
			throws BtSecurityException;

	/**
	 * TODO: (reserved API call) save user places associated with this snapshot
	 * 
	 * @param sessionID
	 * @param dataSnapshotRevId
	 * @param places
	 * @return
	 * @throws BtSecurityException
	 */
	Boolean savePlaces(SessionToken sessionID, String dataSnapshotRevId,
			List<Place> places) throws BtSecurityException;

	List<ProvenanceLogEntry> getSnapshotProvenance(SessionToken sessionID,
			String snapshot) throws BtSecurityException;

	List<RecordingObject> getRecordingObjects(SessionToken sessionToken,
			String datasetId) throws BtSecurityException;
	
	void registerFiles(String tempId, String snapshotName, SessionToken sessionID, List<String> files)
			throws BtSecurityException;
	
	public String getUuid();

	String getContents(SessionToken sessionID, String datasetID, FileInfo file) throws IOException;

	void saveContents(SessionToken sessionID, String datasetID, FileInfo file, String contents) throws IOException;

	RecordingObject createRecordingObject(SessionToken sessionID,
			String datasetId, byte[] input, String filename,
			String contentType, boolean test);

	List<RecordingObject> attachFiles(String target, String snapshot,
			SessionToken sessionID, List<String> files);

	String getParentSnapshot(SessionToken sessionID, String studyRevId) throws BtSecurityException;
	
//	Map<TsEventType,INamedTimeSegment> getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, TsEventType partitionOn) throws IOException,
//		BtSecurityException;

//	Map<INamedTimeSegment, List<IDetection>> getEvents(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, TsEventType partitionOn) throws IOException,
//		BtSecurityException;

	Map<INamedTimeSegment, int[]> getEventsAsSamples(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, double start, double end, double period) throws IOException,
	BtSecurityException;

//	SortedSet<INamedTimeSegment> getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments)
//			throws IOException;

	List<INamedTimeSegment> getEventChannels(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> channels);

	Set<DatasetPreview> getMyDatasets(SessionToken sessionID);

	Set<AnnotationModification> updateAnnotations(SessionToken sessionID, String datasetID, Collection<AnnotationModification> updates, long lastTime);

	Set<AnnotationModification> getAnnotationsSince(SessionToken sessionID, String datasetID, long timestamp);
	
	boolean isExistingDatasetWithName(SessionToken sessionID, String datasetName) throws BtSecurityException;

	@Deprecated
	Set<String> inviteToShare(SessionToken sessionID, Set<String> email, String note, DatasetPreview dataset, Set<Integer> roles)
			throws BtSecurityException;

	Set<String> inviteToShare(SessionToken sessionID, Set<String> email, String note, PresentableMetadata other,
			Set<Integer> roles) throws BtSecurityException;

	/**
	 * Remove a data snapshot
	 * @param revID
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	Boolean removeDataSnapshot(SessionToken sessionID, String revID) throws BtSecurityException;
	
//	Boolean addDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels) throws BtSecurityException;
	
	Boolean addDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects) throws BtSecurityException;

	Boolean removeDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels) throws BtSecurityException;
	
	Boolean removeDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects) throws BtSecurityException;

	void acceptShare(SessionToken sessionID, BasicCollectionNode target, PresentableMetadata object)
			throws BtSecurityException;

	void rejectShare(SessionToken sessionID, PresentableMetadata object) throws BtSecurityException;

	void setPublicAccess(SessionToken sessionID, String datasetID, Set<Integer> access) throws BtSecurityException;

}
