/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class ProcessDerivedResultsAction implements AsyncCallback<Set<DerivedSnapshot>> {
	MetadataBrowserPresenter presenter;
	PresentableMetadata generalMetadata;
	
	public ProcessDerivedResultsAction(MetadataBrowserPresenter bp, PresentableMetadata md) {
		presenter = bp;
		generalMetadata = md;
	}
	
	private ClientFactory getClientFactory() {
		return presenter.getClientFactory();
	}
	
	private SessionModel getSessionModel() {
		return presenter.getClientFactory().getSessionModel();
	}
	
	@Override
	public void onFailure(Throwable caught) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccess(Set<DerivedSnapshot> toolResults) {
		Set<String> idSet = new HashSet<String>();
		for (DerivedSnapshot tr: toolResults)
			idSet.add(tr.getAnalysisId());
		
		getClientFactory().getSearchServices().getSearchResultsForSnapshots(getSessionModel().getSessionID(), 
				idSet, new AsyncCallback<Set<SearchResult>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onSuccess(
							Set<SearchResult> resultSet) {
						presenter.setResultsFor(generalMetadata, resultSet);
					}

		});
	}

	
}
