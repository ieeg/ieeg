/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.images;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Panel;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class MultiViewImagePresenter extends BasicPresenter {
	AppModel control;

	String baseUrl;
	DataSnapshotModel model;
	List<FileInfo> images = new ArrayList<FileInfo>();
	int index = 0;
	
	public interface Display extends BasicPresenter.Display {
//		public Frame getPDFFrame();
		public WindowPlace getPlace();
		
		public void setUrl(String url, SessionToken token);
		public String getUrl();
		
		public void addPrevHandler(ClickHandler handler);
		public void addNextHandler(ClickHandler handler);

		void setBaseUrl(String baseUrl, SessionToken token);
		void setImages(List<FileInfo> list);
		
		void unbindJsHandlers();

//		void selectImage(int inx);
	}
	
	public final static String NAME = "MultiViewImagePresenter";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.DICOM,
			BuiltinDataTypes.MRI, BuiltinDataTypes.CT);
	final static MultiViewImagePresenter seed = new MultiViewImagePresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public MultiViewImagePresenter() {
		super(null, NAME, TYPES);
	}

	public MultiViewImagePresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory
			) {

		super(clientFactory, NAME, TYPES);
		this.control = control;
		this.model = model;
	  }

	@Override
	public MultiViewImagePresenter.Display getDisplay() {
		return (MultiViewImagePresenter.Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);
		
		GWT.log("MultiViewImagePresenter binding with " + selected);

		for (PresentableMetadata md: selected) {
			if (md instanceof FileInfo)
				images.add((FileInfo)md);
		}
		if (getDisplay().getPlace() != null && getDisplay().getPlace() instanceof URLPlace) {
			URLPlace pl = (URLPlace)getDisplay().getPlace();
			baseUrl = pl.getDocument();
		}
		
		getDisplay().setBaseUrl(baseUrl, getSessionModel().getSessionID());

		getDisplay().addPrevHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (index > 0) {
//					getDisplay().selectImage(--index);
//					getDisplay().setUrl(baseUrl + images.get(index).getDetails(),
//							getSessionModel().getSessionID());
				}

			}
//			
		});
//		getDisplay().addNextHandler(new ClickHandler() {
//
//			@Override
//			public void onClick(ClickEvent event) {
//				if (index < images.size() - 1) {
//					getDisplay().selectImage(++index);
//					getDisplay().setUrl(
//							baseUrl + images.get(index).getDetails(),
//							getSessionModel().getSessionID());
//				}
//			}
//			
//		});
		
		List<FileInfo> list = new ArrayList<FileInfo>();
		for (PresentableMetadata md : selected)
			if (md instanceof FileInfo)
				list.add((FileInfo)md);
		
		getDisplay().setImages(list);
//		getDisplay().selectImage(index);

		getDisplay().bindDomEvents();
		getDisplay().setUrl(baseUrl + images.get(index).getDetails(),
				getSessionModel().getSessionID());
	}
	
	@Override
	public void unbind() {
		getDisplay().unbindJsHandlers();
	}

	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata project, boolean writePermissions) {
		return new MultiViewImagePresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory);
	}
}
