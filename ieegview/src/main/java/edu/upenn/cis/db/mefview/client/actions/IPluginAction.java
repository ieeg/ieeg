/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.actions;

import java.util.Set;

import edu.upenn.cis.db.mefview.Pluggable;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;


public interface IPluginAction extends ActionHandler, Pluggable {
	/**
	 * The action name
	 * 
	 * @return
	 */
	public String getName();
	
	/**
	 * Is the action still applicable?
	 * 
	 * @return
	 */
	public boolean isApplicable();
	
	/**
	 * Optional next action in a chain
	 * 
	 * @return
	 */
	public IPluginAction getNextAction();
	
	/**
	 * Construct a new plugin action
	 * @param md
	 * @param factory
	 * @return New plugin action, or null if not applicable to the set of PresentableMetadata
	 */
	public IPluginAction create(Set<PresentableMetadata> md, ClientFactory factory,
			Presenter activePresenter);
}
