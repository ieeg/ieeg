package edu.upenn.cis.db.mefview.client.dialogs;

import edu.upenn.cis.db.mefview.client.ClientFactory;

public interface IOpenSnapshotById extends DialogFactory.DialogWithInitializer<IOpenSnapshotById> {
	public static interface Handler {
		public void open(String dataset);
	}
	
	public void createDialog(ClientFactory clientFactory);
	
	public void init(final Handler responder);
	
	public String getDataset();
	
	public void open();
	
	public void close();

	public void focus();
}
