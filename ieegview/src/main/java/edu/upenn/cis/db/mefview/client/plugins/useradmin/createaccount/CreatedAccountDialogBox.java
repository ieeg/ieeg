package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class CreatedAccountDialogBox extends DialogBox {
	public CreatedAccountDialogBox() {
		super(true, true);
		
		setTitle("Account created or updated");
		add(new Label("Your user account has been created or updated."));
	}
}
