package edu.upenn.cis.db.mefview.shared;

import java.io.Serializable;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;

@GwtCompatible(serializable=true)
public class DatasetPreview implements Serializable, Comparable<DatasetPreview> {
	
	@GwtCompatible(serializable=true)
	public static class TypeAndMime implements Serializable, Comparable<TypeAndMime> {
		String mimeType;
		String fileType;
		public String getMimeType() {
			return mimeType;
		}
		public void setMimeType(String mimeType) {
			this.mimeType = mimeType;
		}
		public String getFileType() {
			return fileType;
		}
		public void setFileName(String fileType) {
			this.fileType = fileType;
		}
		
		public TypeAndMime() {}
		
		public TypeAndMime(String mimeType, String fileType) {
			super();
			this.mimeType = mimeType;
			this.fileType= fileType;
		}
		
		@Override
		public int hashCode() {
			return mimeType.hashCode();
		}
		
		@Override
		public boolean equals(Object o) {
			if (!(o instanceof TypeAndMime))
				return false;
			else
				return mimeType.equals(((TypeAndMime)o).mimeType) &&
						(fileType == null || fileType.equals(((TypeAndMime)o).fileType));
		}
		@Override
		public int compareTo(TypeAndMime o) {
			if (mimeType.equals(o.mimeType))
				return fileType.compareTo(o.fileType);
			else
				return mimeType.compareTo(o.mimeType);
		}

		@Override
		public String toString() {
			return "TypeAndMime{" +
					"fileType='" + fileType + '\'' +
					", mimeType='" + mimeType + '\'' +
					'}';
		}
	}
	
	Set<TypeAndMime> entries;
	String title;
	
	String datasetID;
	
	String path;

	PresentableMetadata metaObj;
	
	boolean isEditable;
	boolean isIncoming;
	boolean isPublic = false;
	
	// TODO: any other info??
	
	public DatasetPreview() {}

	public DatasetPreview(String title, String datasetID, String path, Set<TypeAndMime> entries, 
			PresentableMetadata obj, boolean isIncoming) {
		super();
		this.title = title;
		this.path = path;
		this.datasetID = datasetID;
		this.entries = entries;
		this.path = path;
		this.metaObj = obj;
		this.isIncoming = isIncoming;
	}

	public Set<TypeAndMime> getEntries() {
		return entries;
	}

	public void setEntries(Set<TypeAndMime> entries) {
		this.entries = entries;
	}

	public String getTitle() {
		return title;
	}

	public PresentableMetadata getMetaObj() {return metaObj;}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public int hashCode() {
		return getTitle().hashCode();
	}

	@Override
	public int compareTo(DatasetPreview o) {
		
		// Choose the dataset with the longer path [favors nested folders like incoming]
		if (getPath() != null && o.getPath() != null && o.getPath().length() != getPath().length()) {
			return -Integer.valueOf(getPath().length()).compareTo(o.getPath().length());
		}
		
		// Choose the dataset with more diversity
		if (entries.size() != o.entries.size()) {
			return -Integer.valueOf(entries.size()).compareTo(o.entries.size());
		}
		
		// Then choose by alpha order
		return getTitle().compareTo(o.getTitle());
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}
	
	
	
	
	public String getDatasetID() {
		return datasetID;
	}

	public void setDatasetID(String datasetID) {
		this.datasetID = datasetID;
	}
	
	

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	
	
	public boolean isIncoming() {
		return isIncoming;
	}

	public void setIncoming(boolean isIncoming) {
		this.isIncoming = isIncoming;
	}

	public void setMetaObj(PresentableMetadata metaObj) {
		this.metaObj = metaObj;
	}

	@Override
	public String toString() {
		return "DatasetPreview{" +
				"id=" + datasetID +
				", entries=" + entries +
				", title='" + title + '\'' +
				'}';
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}
	
	
}
