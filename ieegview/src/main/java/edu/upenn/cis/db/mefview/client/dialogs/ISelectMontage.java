package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.List;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.ListBox;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.clickhandlers.OpenEditMontageDialogClickHandler;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public interface ISelectMontage extends DialogFactory.DialogWithInitializer<ISelectMontage>{
	public static interface Handler {
		public void switchMontage(EEGMontage montage);
		public void removeMontage(EEGMontage montage);
		public void editMontage(EEGMontage montage);
		public void openEditDialog();
		public void returnFocusToEeg();
		public void copyMontage(EEGMontage montage);
	}
	
	public void init(final Handler responder, Command doAfter);
	
	public ListBox getListBox();
	
	public EEGMontage getMontage();
	
	public List<EEGMontage> getMontages();
	
	public void open();
	
	public void close();

	public void focus();

	public void removeMontageFromList(EEGMontage montage);

	public void createDialog(ClientFactory cf);
	
	public void clearList();
	
	public void setClickHandlers(OpenEditMontageDialogClickHandler newMHandler,OpenEditMontageDialogClickHandler editMHandler);

	void addMontageToList(EEGMontage montage);

	public void updateSelectMontage(EEGMontage montage, IEEGViewerPresenter presenter, Boolean isNew);

	public String getStrName(EEGMontage montage);
	
	
}
