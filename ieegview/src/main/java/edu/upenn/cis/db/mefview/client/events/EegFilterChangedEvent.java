/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.List;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class EegFilterChangedEvent extends GwtEvent<EegFilterChangedEvent.Handler> {
	private static final Type<EegFilterChangedEvent.Handler> TYPE = new Type<EegFilterChangedEvent.Handler>();
	
	private final List<INamedTimeSegment> traces;
	private final List<DisplayConfiguration> filters;
	private final IEEGViewerPanel window;
	
	public EegFilterChangedEvent(IEEGViewerPanel window, List<INamedTimeSegment> traces, List<DisplayConfiguration> filters){
		this.traces = traces;
		this.filters = filters;
		this.window = window;
	}
	

	public static com.google.gwt.event.shared.GwtEvent.Type<EegFilterChangedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EegFilterChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EegFilterChangedEvent.Handler handler) {
		handler.onFilterChanged(window, traces, filters);
	}

	public static interface Handler extends EventHandler {
		void onFilterChanged(IEEGViewerPanel window, List<INamedTimeSegment> traces, List<DisplayConfiguration> filters);
	}
}
