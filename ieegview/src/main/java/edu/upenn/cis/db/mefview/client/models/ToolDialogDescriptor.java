/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.braintrust.shared.ParallelDto;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.ToolLoc;

public class ToolDialogDescriptor {
	private HashMap<String,Object> values = new HashMap<String,Object>();
	private ParallelDto parallelism = ParallelDto.SERIAL;
	private ToolLoc location = ToolLoc.GITHUB;
	private String sourceRevID = "";
	private String toolRevID = "";
	private String command = "";
	private String creator = "";
	
	public Map<String,Object> getValueMap() { return values; }
	
	public Object getValue(String str) { return values.get(str); }
	public void putValue(String str, Object value) { values.put(str, value); }
	
//	public ToolDto.PARALLEL getParallelism() {
//		return parallelism;
//	}

	public ToolLoc getLocation() {
      return this.location;
    }
	
	public void setParallelism(ParallelDto parallelism) {
		this.parallelism = parallelism;
	}
	
	public void setSourceType(ToolLoc location) {
      this.location = location;
    }

	public String getToolRevID() {
		return toolRevID;
	}

	public void setToolRevID(String toolRevID) {
		this.toolRevID = toolRevID;
	}
	
	public String getSourceRevID() {
		return sourceRevID;
	}

	public void setSourceRevID(String sourceRevID) {
		this.sourceRevID = sourceRevID;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getCreator() {
		return creator;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getXML() {
		StringBuilder ret = new StringBuilder("<snapshot><input-parameters>\n");
		
		ret.append("<revID>" + toolRevID + "</revID><creator>" + creator + "</creator><sourceID>" + sourceRevID + "</sourceID><command>" + command + "</command>");
		
//		if (getParallelism() == ParallelDto.PAR_BLOCK)
//			ret.append("<parallelism type='block'/>\n");
//		else if (getParallelism() == ParallelDto.PAR_CHANNEL)
//			ret.append("<parallelism type='channel'/>\n");
//		else
//			ret.append("<parallelism type='none'/>\n");
		
		for (String k : values.keySet()) {
			ret.append("<parameter type='");
			if (values.get(k) instanceof List) {
				ret.append("StringList");
			} else if (values.get(k) instanceof Integer) {
				ret.append("Integer");
			} else if (values.get(k) instanceof Long) {
				ret.append("Long");
			} else if (values.get(k) instanceof String) {
				ret.append("String");
			} else if (values.get(k) instanceof Double) {
				ret.append("Double");
			} else if (values.get(k) instanceof Boolean) {
				ret.append("Boolean");
			} else {
				ret.append("Unknown");
			}
			ret.append("'>");
			ret.append("<key>" + k + "</key>");
			Object val = values.get(k);
			if (val instanceof List) {
				for (Object o : (List<?>)val)
					ret.append("<value>" + o.toString() + "</value>");
			} else
				ret.append("<value>" + val.toString() + "</value>");
			ret.append("</parameter>\n");
		}
		
		ret.append("</input-parameters></snapshot>\n");
		return ret.toString();
	}
}
