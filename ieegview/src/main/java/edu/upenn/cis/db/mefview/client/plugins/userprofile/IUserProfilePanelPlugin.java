package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.util.List;

import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

public interface IUserProfilePanelPlugin {
	public Panel getHighlightedButtons();
	
	public Panel getLogos();
	
	public Panel getBanner();
	
	public List<Widget> getWidgets();
	
	public List<ButtonEntry> getButtons();
}
