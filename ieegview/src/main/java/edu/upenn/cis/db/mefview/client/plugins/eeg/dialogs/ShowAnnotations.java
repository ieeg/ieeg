/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.commands.snapshot.RemoveAnnotationsFromSnapshotCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.RemoveSnapshotCommand;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.IEditAnnotation;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class ShowAnnotations extends DialogBox {
	PagedTable<Annotation> annTable;
	ListDataProvider<Annotation> annProvider = new ListDataProvider<Annotation>();
	
	Button ok = new Button("Close");

	ListBox map = new ListBox();
	Button removeAn = new Button("Remove");
	Button editAn = new Button("Edit");
	Button addAn = new Button("Add");
	Button download = new Button("CSV");

//	DataSnapshotViews ssm = null;
	
	ClientFactory clientFactory;
	
	String theID;
	final List<AnnotationDefinition> annotationDefs;

	public ShowAnnotations( 
			final DataSnapshotModel model,
			final List<AnnotationDefinition> annotationDefs,
			final boolean needToSave,
			final ClientFactory factory) {
		this(model, annotationDefs,  
				400, needToSave, factory);
	}
	
	public ShowAnnotations( 
//			final DataSnapshotViews model,
			final DataSnapshotModel snapshot,
			final List<AnnotationDefinition> annotationDefs,
			int height, final boolean needToSave,
			final ClientFactory factory) {
		
		clientFactory = factory;
		this.annotationDefs = annotationDefs;

//		ssm = model;

		final Set<Annotation> annotationsToRemove = new HashSet<Annotation>();
		
		final String revID = snapshot.getSnapshotID();
		
		setTitle("View Annotations " + snapshot.getFriendlyName() + "(" + revID + ")");
		setText("View Annotations " + snapshot.getFriendlyName() + "(" + revID + ")");
		
		setHeight(height + "px");
		
		VerticalPanel layout = new VerticalPanel();
		
		theID = revID;
		
		HorizontalPanel boxes = new HorizontalPanel();
		
		layout.add(boxes);
		boxes.setHeight("100%");
		
//		boxes.add(new HTML("&nbsp;"));
//		channelList.setVisibleItemCount(20);
		final List<Annotation> anns = new ArrayList<Annotation>();
		anns.addAll(snapshot.getAllEnabledAnnotations());//annotations);
		Collections.sort(anns);
		
		List<String> annColNames = new ArrayList<String>();
		List<TextColumn<Annotation>> annColumns = new ArrayList<TextColumn<Annotation>>();
		
		annColNames.add("Type");
		annColumns.add(Annotation.annType);
		annColNames.add("Start");// (μs)");
		annColumns.add(Annotation.annStart);
		annColNames.add("End");// (μs)");
		annColumns.add(Annotation.annEnd);
		annColNames.add("Description");
		annColumns.add(Annotation.annDesc);
		annColNames.add("Channels");
		annColumns.add(Annotation.annChannel);
		annColNames.add("Creator");
		annColumns.add(Annotation.annCreator);
//		annScroll = new ScrollPanel();
		ArrayList<Annotation> annotationsList = new ArrayList<Annotation>();
		annTable = new PagedTable<Annotation>(20, false, false, annotationsList, Annotation.KEY_PROVIDER, annColNames, annColumns);
		annTable.getList().addAll(anns);
		annTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);
		annTable.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		annTable.setRowCount(annTable.getList().size());
		annTable.setPageSize(annTable.getList().size());

		editAn.setEnabled(false);
//		VerticalPanel annVPanel = new VerticalPanel();
//		annScroll.add(annTable);
//		annScroll.setWidth("400px");
//		annVPanel.add(annTable);
		
		for (Annotation a : anns) {
			for (String c : a.getChannels()) {
				boolean haveAnnotation = false;
				for (INamedTimeSegment t: a.getChannelInfo())
					if (t.getLabel().equals(c))
						haveAnnotation = true;

				// Recover TraceInfo annotation from current data set
				if (!haveAnnotation)
					for (INamedTimeSegment t: snapshot.getTraceInfo())//traces)
						if (c.equals(t.getLabel()))
							a.getChannelInfo().add(t);
			}
		}
		
		
		VerticalPanel toPane = new VerticalPanel();
		toPane.add(new Label("Annotations:"));
		toPane.add(annTable);//Scroll);
		annTable.setHeight(height * 3 / 4+ "px");
		annTable.setWidth("500px");
//		toPane.setHeight(height * 3 / 4 + "px");
//		toPane.setHeight("80%");
		boxes.add(toPane);
//		VerticalPanel extraPane = new VerticalPanel();
//		extraPane.add(new Label("Annotations:"));
//		extraPane.add(annList);
//		boxes.add(extraPane);
		
//		annList.setVisibleItemCount(20);
		HorizontalPanel buttons = new HorizontalPanel(); 
		buttons.add(addAn);
		buttons.add(editAn);
		buttons.add(removeAn);
		toPane.add(buttons);
		HorizontalPanel downloads = new HorizontalPanel();
		downloads.add(new Label("Download:"));
		downloads.add(download);
//		downloads.add(downloadJ);
//		downloads.add(downloadM);
		toPane.add(downloads);
		removeAn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int in = annTable.getList().size() - 1; in >= 0; in--) {
					if (annTable.getSelectionModel().isSelected(annTable.getList().get(in))) {
						annotationsToRemove.add(annTable.getList().get(in));
//						annTable.getList().remove(in);
						anns.remove(in);
					}
				}
				
				for (Annotation a: annotationsToRemove)
					annTable.getList().remove(a);
				
				snapshot.removeAllAnnotations(annotationsToRemove);
			}
			
		});
		
		download.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DialogBox d = PanelFactory.getPleaseWaitDialog("Generating CSV download");
//				model.openURL("./services/timeseries/getAnnotationsListCSV/" + revID + ".csv", revID);
				saveAnnotationsForDownload(d, anns, false, needToSave);
			}
			
		});
//		downloadJ.addClickHandler(new ClickHandler() {
//
//			@Override
//			public void onClick(ClickEvent event) {
//				DialogBox d = ControlPane.getPleaseWaitDialog("Generating JSON download");
////				model.openURL("./services/timeseries/getAnnotationsListJSON/" + revID + ".json", revID);
//				saveAnnotationsForDownload(d, anns, true, needToSave);
//			}
//			
//		});
//		
//		downloadJ.setEnabled(false);
//		downloadM.setEnabled(false);
		
//		if (annTable.getList().isEmpty())
			removeAn.setEnabled(false);
			
		addAn.setEnabled(false);
		
		editAn.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int in = annTable.getList().size() - 1; in >= 0; in--) {
					if (annTable.getSelectionModel().isSelected(annTable.getList().get(in))) {
						IEditAnnotation ea = GWT.create(EditAnnotation.class);
						
						 ea.init(annTable.getList().get(in),
								annotationDefs,
								annTable.getList().get(in).getChannels(), 
								snapshot.getAnnotationTypes(), snapshot, false, null);
						ea.center();
						ea.show();
					}
				}
			}
			
		});
		
		VerticalPanel gap = new VerticalPanel();
		gap.setWidth("20px");
		boxes.add(gap);
		
		setWidget(layout);

		HorizontalPanel hp = new HorizontalPanel();
		layout.add(hp);
		layout.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		hp.add(ok);
//		hp.add(cancel);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				ShowAnnotations.this.hide();
				
//				for (EEGViewerPanel ep : model.getEEGs())
//					ep.getEEGPane().showGraph();

				if (!annotationsToRemove.isEmpty()) {
					DialogBox wait = PanelFactory.getPleaseWaitDialog("Removing annotations");
					wait.center();
					wait.show();
//					removeAnnotations(revID, annotationsToRemove, wait);
					RemoveAnnotationsFromSnapshotCommand cmd =
							new RemoveAnnotationsFromSnapshotCommand(
									clientFactory,
									revID,
									annotationsToRemove,
									wait,
									null);
				}
//				} else {
//					final DialogBox d = new ExperimentInfo(main, theID);
//					d.center();
//					d.show();
//				}
			}
			
		});
		
//		cancel.addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				ShowAnnotations.this.hide();
//			}
//			
//		});
	}
	
	private void saveTheAnnotations(final DialogBox d, final String studyId, final boolean inJson, final boolean keepResult) {
		if (!inJson) {
			openURL("Download annotations", "./services/timeseries/getAnnotationsListCSV/" + studyId + ".csv", studyId);
			/*
			if (!keepResult) {
				RemoveSnapshotCommand rsc = new RemoveSnapshotCommand(
						clientFactory,
						ssm,
						studyId,
						d,
						null
						);
				rsc.execute();

			}*/
//				ssm.removeResult(studyId, d);
		} else {
			openURL("Download annotations", "./services/timeseries/getAnnotationsListJSON/" + studyId + ".json", studyId);
			/*
			if (!keepResult) {
				RemoveSnapshotCommand rsc = new RemoveSnapshotCommand(
						clientFactory,
						ssm,
						studyId,
						d,
						null
						);
				rsc.execute();
			}*/
				//				ssm.removeResult(studyId, d);
		}
	}
	
	private void saveAnnotationsForDownload(final DialogBox d, final Collection<Annotation> annotations, 
			final boolean inJson, final boolean needToSave) {

		if (!needToSave)
			saveTheAnnotations(d, theID, inJson, true);
		else {
			// Create a new derived snapshot w/o annotations
			Window.alert("There are unsaved annotations. Please save before downloading.");
			//			ssm.deriveSnapshotFrom(theID, "ieeg", theID + " temp", null,
//					ssm.getChannelRevIds(), false, d, new OnDoneHandler() {
//	
//						@Override
//						public void doWork(String studyId1) {
//							// Save all the annotations
//							ssm.saveAnnotationsToDataSnapshot(studyId1, annotations,
//									new OnDoneHandler() {
//	
//										@Override
//										public void doWork(String studyId) {
//											saveTheAnnotations(d, studyId, inJson, false);
//										}
//							});
//							
//						}
//				
//			});
		}
	}

	public void openURL(final String title, final String url, final String snapshot) {
		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, null);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}
	
	public void openURL(final String title, final String url, final String snapshot, final String other) {
		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, other, null);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}
}
