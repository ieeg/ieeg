package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;

public class WidgetFactoryImpl implements IWidgetFactory {
	@Override
	public Widget createButton(String button) {
		return new Button(button);
	}
	

	@Override
	public Widget createIconButton(String button) {
		Image refreshImg = new Image(ResourceFactory.getRefresh().getSafeUri());
		refreshImg.setPixelSize(12, 12);
	    return new Button(refreshImg + "");
	}


	@Override
	public void addClickHandler(final Widget w, final WidgetClickHandler handler) {
		((HasClickHandlers)w).addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				handler.onClick(event.getRelativeX(w.getElement()), 
						event.getRelativeY(w.getElement()));
			}
			
		});
		
	}
	
}
