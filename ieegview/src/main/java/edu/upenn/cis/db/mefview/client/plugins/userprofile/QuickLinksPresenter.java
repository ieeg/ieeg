/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.util.Collection;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class QuickLinksPresenter implements Presenter {
    public interface Display extends Presenter.Display {

		void initialize(ClientFactory clientFactory,
				IUserProfilePluginFactory profile);

		void bindDomEvents();
        
    }
    
    Display display;
    ClientFactory clientFactory;
    
    public QuickLinksPresenter(ClientFactory cf) {
        clientFactory = cf;
    }

    @Override
    public Presenter create(ClientFactory clientFactory,
            AppModel controller, DataSnapshotViews model, PresentableMetadata project,
            boolean writePermissions) {
        return new QuickLinksPresenter(clientFactory);
    }

    @Override
    public void setDisplay(Presenter.Display display) {
        this.display = (Display)display;
    }

    @Override
    public String getType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void bind(Collection<? extends PresentableMetadata> selected) {
        display.addPresenter(this);

    }

    @Override
    public void unbind() {
        // TODO Auto-generated method stub

    }

    @Override
    public Set<String> getSupportedDataTypes() {
        return null;
    }

}