/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;

public class CreateProject extends DialogBox implements
		DialogWithInitializer<ICreateProject>, ICreateProject {
	ListBox datasetList = new ListBox();
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	TextBox theName = new TextBox();

	public CreateProject() {
		VerticalPanel vp = new VerticalPanel();

		setTitle("Create New Project");
		setText("Create New Project");
		setWidget(vp);

		vp.add(new Label("Project name (must be unique):"));
		vp.add(theName);

		HorizontalPanel hp = new HorizontalPanel();
		hp.add(ok);
		hp.add(cancel);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
	}

	@Override
	public void initialize(DialogInitializer<ICreateProject> initFn) {
		initFn.init(this);
	}

	@Override
	public void reinitialize(DialogInitializer<ICreateProject> initFn) {
		initFn.reinit(this);
	}

	@Override
	public void init(final Handler responder) {
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();

				responder.create(getProject());
			}

		});

		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}

		});
	}

	@Override
	public String getProject() {
		return theName.getText();
	}

	@Override
	public void open() {
		center();
	}

	@Override
	public void close() {
		hide();
	}

	@Override
	public void focus() {
		theName.setFocus(true);
	}

	@Override
	public void clearInput(){
		theName.setName("");
	}

	@Override
	public void setFactory(ClientFactory factory) {
		// TODO Auto-generated method stub
		
	}

}
