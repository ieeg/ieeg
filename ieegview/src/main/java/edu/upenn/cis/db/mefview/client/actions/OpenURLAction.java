/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class OpenURLAction extends PluginAction {
	
	Set<PresentableMetadata> urls = new HashSet<PresentableMetadata>();
	
	public static final String NAME = "View URL";
	
	
	static {
		PluginActionFactory.register(NAME, new OpenURLAction());
	}
	
	public OpenURLAction() {
		super(NAME, null, null, null);
	}
	
	public OpenURLAction(Set<PresentableMetadata> md, ClientFactory cf, Presenter pres) {
		super(NAME, cf, pres, null);
		
		urls.addAll(md);
	}

	@Override
	public void execute() {
		for (PresentableMetadata md: urls) {
			GWT.log("OpenURL firing on " + md.getFriendlyName() + " / " + md.getId());
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(md.getFriendlyName(), md));
			break;
		}
//		return null;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new OpenURLAction(md, factory, pres);
	}
	
};

