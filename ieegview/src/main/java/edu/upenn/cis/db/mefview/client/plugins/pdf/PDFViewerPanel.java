/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.pdf;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Frame;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.IBlankToolbar;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


//@SuppressWarnings("deprecation")
public class PDFViewerPanel extends AstroTab implements //DocumentReadyEvent.Handler, 
PDFPresenter.Display {
	Frame f = new Frame();

	ClientFactory clientFactory;
	HandlerRegistration hr;
	URLPlace place = new URLPlace("", datasetId, NAME);
	
	IBlankToolbar toolbar = GWT.create(IBlankToolbar.class);
	
	public final static String NAME = "pdf";
	final static PDFViewerPanel seed = new PDFViewerPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PDFViewerPanel() {
		super();
	}

	public PDFViewerPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, title);
		
		add(toolbar);
		add(f);
		f.setSize("100%", "100%");
		setWidgetLeftRight(f, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(f, 64, Unit.PX, 1, Unit.PX);
		
		toolbar.init(clientFactory, this);
		minHeight = 800;
	  }
	
	@Override
	public String getPanelType() {
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new PDFViewerPanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	@Override
	public WindowPlace getPlace() {
		return place;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof URLPlace)
			this.place = (URLPlace)place;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return PDFPlace.getFromSerialized(params);
//	}

	@Override
	public Frame getPDFFrame() {
		return f;
	}

	public String getTitle() {
		return "Documents";
	}

	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }
	
	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}
}
