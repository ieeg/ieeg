/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.projectsettings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.SecAlertCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.WorkspaceModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.DiscussionViewer;
import edu.upenn.cis.db.mefview.client.viewers.MetadataViewer;
import edu.upenn.cis.db.mefview.client.viewers.ProjectToolbar;
import edu.upenn.cis.db.mefview.client.viewers.ToolViewer;
import edu.upenn.cis.db.mefview.client.viewers.UserViewer;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.Project.STATE;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.places.ProjectPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class ProjectPanel extends AstroTab implements RequiresResize, 
	ProjectPresenter.Display {
	LayoutPanel mainPanel = new ResizingLayout();
	LayoutPanel snapshotsPanel = new ResizingLayout();
	LayoutPanel toolsPanel = new ResizingLayout();
	LayoutPanel adminsPanel = new ResizingLayout();
	LayoutPanel teamPanel = new ResizingLayout();
	LayoutPanel discussionContainer = new ResizingLayout();
	LayoutPanel discussionPanel = new ResizingLayout();
	
	StackLayoutPanel mainProjectPanel = new StackLayoutPanel(Unit.EM);
	
	DiscussionViewer.Handler handler;

	MetadataViewer snapshots;
	ToolViewer tools;
	UserViewer admins;
	UserViewer teams;
	DiscussionViewer discussion;
	private DecoratedPopupPanel projectLoadingPopup;
	
	ProjectToolbar toolbar;
	
	WorkspaceModel model;
	AppModel control;
	Button findMoreStudies = new Button("Find more studies...");

	Project theProject;
	
//	ToolBrowserPanel tools;
	
	public static final int MINHEIGHT = 400;
	public static final String NAME = "project";
	final static ProjectPanel seed = new ProjectPanel();
	
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, false));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ProjectPanel() {
		super();
	}

	public ProjectPanel( 
	    final AppModel parent,
	    final ClientFactory clientFactory,
	    final String title
	    ) {

	  super (clientFactory, title);

	  control = parent;
	  this.projectLoadingPopup = new DecoratedPopupPanel(false, true);
	  this.projectLoadingPopup.setWidget(new HTML("Please wait while project loads"));
	  this.projectLoadingPopup.setGlassEnabled(true);
	}
  
	
	@Override
	public void init(Project project) {
		  theProject = project;
		  
		  if (getSessionModel().isRegisteredUser()){
		    drawUserContent();
		  }else {
		    drawGuestContent();
		  }
	}
	
	private void drawUserContent() {

      
//    DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
//    add(mainPane);
//    
//    DockLayoutPanel contentPane = new DockLayoutPanel(Unit.PX);
//    DockLayoutPanel rightPane = new DockLayoutPanel(Unit.PX);
//    contentPane.addEast(rightPane, 400);
      
      
//      add(mainPanel);
//      setWidgetLeftRight(mainPanel, 2, Unit.PX, 2, Unit.PX);
//      setWidgetTopBottom(mainPanel, 2, Unit.PX, 2, Unit.PX);
//              
//      
////    Add Toolbar for opening new tabs.
//      FlowPanel toolbarWrapper = new FlowPanel();
//      
//      HorizontalPanel theTools = new HorizontalPanel();
//      toolbarWrapper.setStyleName("IeegTabToolbar");
//      toolbar = new ProjectToolbar();
//      toolbarWrapper.add(toolbar);
//      
//      theTools.add(toolbarWrapper);
//      mainPanel.add(theTools);
//      mainPanel.setWidgetTopHeight(theTools, 2, Unit.PX, 42, Unit.PX);
//      
////    Add Studies panel
//      mainPanel.add(snapshotsPanel);
//      mainPanel.setWidgetLeftWidth(snapshotsPanel, 2, Unit.PX, 28, Unit.PCT);
//      mainPanel.setWidgetTopHeight(snapshotsPanel, 5 + 22, Unit.PX, 45, Unit.PCT);
//
////    Add Tools panel
//      mainPanel.add(toolsPanel);
//      mainPanel.setWidgetLeftWidth(toolsPanel, 2, Unit.PX, 28, Unit.PCT);
//      mainPanel.setWidgetBottomHeight(toolsPanel, 2, Unit.PX, 45, Unit.PCT);
//      
//      addSnapshotsPanel();
//      addToolsPanel();
//
//      // Add Admins panel
//      mainPanel.add(adminsPanel);
//      mainPanel.setWidgetLeftRight(adminsPanel, 30, Unit.PCT, 45, Unit.PCT);
//      mainPanel.setWidgetTopHeight(adminsPanel, 5 + 22, Unit.PX, 45, Unit.PCT);
//
//      // Add Team panel
//      mainPanel.add(teamPanel);
//      mainPanel.setWidgetLeftRight(teamPanel, 30, Unit.PCT, 45, Unit.PCT);
//      mainPanel.setWidgetBottomHeight(teamPanel, 2, Unit.PX, 45, Unit.PCT);
//
//      // Add Discussion panel
//      mainPanel.add(discussionPanel);
//      mainPanel.setWidgetRightWidth(discussionPanel, 2, Unit.PX, 43, Unit.PCT);
//      mainPanel.setWidgetTopBottom(discussionPanel, 5 + 22, Unit.PX, 2, Unit.PX);
//
//      addAdminsPanel();
//      addUsersPanel();

	   DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
	   add(mainPane);
	   DockLayoutPanel contentPane = new DockLayoutPanel(Unit.PCT);
	   DockLayoutPanel rightPane = new DockLayoutPanel(Unit.PX);
	   contentPane.addEast(rightPane, 50);

	   // Add Toolbar
	   FlowPanel toolbarWrapper = new FlowPanel();
	   toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
	   toolbar = new ProjectToolbar();
	   toolbarWrapper.add(toolbar);
	   mainPane.addNorth(toolbarWrapper, 35);
	   mainPane.add(contentPane);

	   // StackWrapper
	   DockLayoutPanel contentPanel = new DockLayoutPanel(Unit.EM);

	   HTML panelheader1 = new HTML("Project Datasets");
	   panelheader1.setStylePrimaryName("ieeg-stack-header");
	   mainProjectPanel.add(snapshotsPanel, panelheader1, 1.5);

	   HTML panelheader2 = new HTML("Project Tools");
	   panelheader2.setStylePrimaryName("ieeg-stack-header");
	   mainProjectPanel.add(toolsPanel, panelheader2, 1.5);

       HTML panelheader3 = new HTML("Project Admins");
       panelheader3.setStylePrimaryName("ieeg-stack-header");
       mainProjectPanel.add(adminsPanel, panelheader3, 1.5);

       HTML panelheader4 = new HTML("Project Team");
       panelheader4.setStylePrimaryName("ieeg-stack-header");
       mainProjectPanel.add(teamPanel, panelheader4, 1.5);
	   
	   mainProjectPanel.setStylePrimaryName("ieeg-rec-fav-stack");
	   contentPanel.add(mainProjectPanel);
	   contentPane.add(contentPanel);

	   // DiscussionPane
	   // StackWrapper
	   DockLayoutPanel stackWrapper = new DockLayoutPanel(Unit.EM);

//	   FlowPanel infoPanel = new FlowPanel();
//	   infoPanel.setStyleName("ieeg-user-sidePane");
//	   infoPanel.addStyleName("ieeg-cornered-pane");
//	   Label infoLab = new Label("Information");
//	   infoLab.setStyleName("ieeg-user-sidebar-heading");
//	   infoLab.addStyleName("ieeg-cornered-pane-header");
//	   infoPanel.add(infoLab);
//	   HTML infoStr = new HTML("Projects can be used to group datasets and tools. " +
//	       "You can select which users have access to a project group and you can " +
//	       "use the project discussion thread to post questions, provide information "+
//	       "and discuss the project with your collaborators.<br><br> "+
//	       "Select one of the studies on the left, and click on 'View EEG' to open the EEG-Viewer.");
//	   infoStr.setStylePrimaryName("ieeg-info-text");
//	   infoPanel.add(infoStr);
//
//	   rightPane.addNorth(infoPanel, 165);

	   stackWrapper.setStyleName("ieeg-user-sidePane");
	   stackWrapper.addStyleName("ieeg-cornered-pane");
	   Label discLab = new Label("Project Discussion");
	   discLab.setStyleName("ieeg-user-sidebar-heading");
	   discLab.addStyleName("ieeg-cornered-pane-header");
	   stackWrapper.addNorth(discLab,2.5);
	   stackWrapper.add(discussionPanel);
	   discussionPanel.setStylePrimaryName("ieeg-disc-content");
	   rightPane.add(stackWrapper);


	   addSnapshotsPanel();
	   addToolsPanel();
	   addAdminsPanel();
	   addUsersPanel();
	  
      GWT.log("Study loaded with " + theProject.getSnapshots().size() + " studies and " + theProject.getTools().size() + " tools");  
	}
	
	private void drawGuestContent() {
	 
	  DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
	  add(mainPane);
	  DockLayoutPanel contentPane = new DockLayoutPanel(Unit.PCT);
	  DockLayoutPanel rightPane = new DockLayoutPanel(Unit.PX);
	  contentPane.addEast(rightPane, 50);
	  
	  // Add Toolbar
	  FlowPanel toolbarWrapper = new FlowPanel();
	  toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
	  toolbar = new ProjectToolbar();
	  toolbarWrapper.add(toolbar);
	  mainPane.addNorth(toolbarWrapper, 35);
	  mainPane.add(contentPane);
	  
	  // StackWrapper
      DockLayoutPanel contentPanel = new DockLayoutPanel(Unit.EM);
	  
	  HTML panelheader1 = new HTML("Project Datasets");
      panelheader1.setStylePrimaryName("ieeg-stack-header");
      mainProjectPanel.add(snapshotsPanel, panelheader1, 1.5);

      HTML panelheader2 = new HTML("Project Tools");
      panelheader2.setStylePrimaryName("ieeg-stack-header");
      mainProjectPanel.add(toolsPanel, panelheader2, 1.5);

      mainProjectPanel.setStylePrimaryName("ieeg-rec-fav-stack");
      contentPanel.add(mainProjectPanel);
      contentPane.add(contentPanel);
	  
      // DiscussionPane
      // StackWrapper
      DockLayoutPanel stackWrapper = new DockLayoutPanel(Unit.EM);
      
      FlowPanel infoPanel = new FlowPanel();
      infoPanel.setStyleName("ieeg-user-sidePane");
      infoPanel.addStyleName("ieeg-cornered-pane");
      Label infoLab = new Label("Information");
      infoLab.setStyleName("ieeg-user-sidebar-heading");
      infoLab.addStyleName("ieeg-cornered-pane-header");
      infoPanel.add(infoLab);
      HTML infoStr = new HTML("Projects can be used to group datasets and tools. " +
      "You can select which users have access to a project group and you can " +
          "use the project discussion thread to post questions, provide information "+
      "and discuss the project with your collaborators.<br><br> "+
          "Select one of the studies on the left, and click on 'View EEG' to open the EEG-Viewer.");
      infoStr.setStylePrimaryName("ieeg-info-text");
      infoPanel.add(infoStr);

      rightPane.addNorth(infoPanel, 165);
      
      stackWrapper.setStyleName("ieeg-user-sidePane");
//      infoPanel.addStyleName("ieeg-cornered-pane");
      Label discLab = new Label("Project Discussion");
      discLab.setStyleName("ieeg-user-sidebar-heading");
//      portLab.addStyleName("ieeg-cornered-pane-header");
      stackWrapper.addNorth(discLab,2.5);
      stackWrapper.add(discussionPanel);
      discussionPanel.setStylePrimaryName("ieeg-disc-content");
      rightPane.add(stackWrapper);
      
      
      addSnapshotsPanel();
      addToolsPanel();

	}

	
	
		
	
	
	// Only called for registered users on bind
	public void setHandler(DiscussionViewer.Handler handler) {
		this.handler = handler;
		
		addDiscussionsPanel(handler);
		
		if (getSessionModel().isRegisteredUser()){
		clientFactory.getPortal().getAllUsers(getSessionModel().getSessionID(), 
				new SecAlertCallback<List<UserInfo>>(clientFactory) {

			@Override
			public void onSuccess(List<UserInfo> result) {
				for (UserInfo ui: result)
					if (!admins.getTable().getList().contains(ui))
						admins.getTable().add(ui);
				for (UserInfo ui: result)
					if (!teams.getTable().getList().contains(ui))
						teams.getTable().add(ui);
			}
			
		});
		}
		
	}
	
	public void resizeAll(int width, int height) {
	}
	
	public PagedTable<PresentableMetadata> getStudyList() {
		return snapshots.getTable();
	}
	

	@Override
	public void updateRevId(String oldRevId, String newRevId) {
		for (PresentableMetadata sr: snapshots.getTable().getList()) {
			if (sr.getId().equals(oldRevId))
				sr.setId(newRevId);
		}
		
	}
	@Override
//	public PanelType getPanelType() {
//		return PanelType.PROJECT;
//	}
	
	
	public String getPanelType() {
		return NAME;
	}

	public WorkspaceModel getWorkspaceModel() {
		return model;
	}
	
	void addSnapshotsPanel() {
//		FlowPanel bSnapshots = new FlowPanel();
//		Label bookSnap = new Label("Project snapshots:");
//		bSnapshots.setStyleName("IeegSubHeader");
//		bSnapshots.add(bookSnap);
//
//		snapshotsPanel.add(bSnapshots);

		List<String> studyColNames = new ArrayList<String>();
		List<TextColumn<SearchResult>> studyColumns = new ArrayList<TextColumn<SearchResult>>();
		
		studyColNames.add("Name");
		studyColumns.add(SearchResult.studyColumn);
		
		if (getSessionModel().isRegisteredUser()){
		  snapshots = new MetadataViewer(clientFactory,
		      10, false, true, true);
		} else {
		  snapshots = new MetadataViewer(clientFactory,
		      10, false, false, true);  
		}

		snapshotsPanel.add(snapshots);
//		snapshotsPanel.setWidgetTopBottom(snapshots, 28, Unit.PX, 28, Unit.PX);
		if (theProject.getState() == STATE.LOADING) {
			projectLoadingPopup.center();
		}
		snapshots.getTable().addAll(theProject.getSnapshots());
		if (getSessionModel().isRegisteredUser()) {
			for (PresentableMetadata sr: snapshots.getTable().getList())
				snapshots.getSelectionModel().setSelected(sr, true);
		}

	}
	
	void addAdminsPanel() {
//		FlowPanel rSnapshots = new FlowPanel();
//		rSnapshots.setStyleName("IeegSubHeader");
//		Label recStud = new Label("Project administrators:");
//		rSnapshots.add(recStud);
//
//		adminsPanel.add(rSnapshots);
		admins = new UserViewer(
				//project.getAdmins()
				10, false, true, true);

		adminsPanel.add(admins);
//		adminsPanel.setWidgetTopBottom(admins, 28, Unit.PX, 28, Unit.PX);
		
		for (UserInfo ui: theProject.getAdmins()) {
			if (!admins.getTable().getList().contains(ui))
				admins.getTable().add(ui);
			admins.getSelectionModel().setSelected(ui, true);
		}

	}
	
	void addUsersPanel() {
//		FlowPanel rTools = new FlowPanel();
//		Label recTool = new Label("Project team:");
//		rTools.setStyleName("IeegSubHeader");
//
//		rTools.add(recTool);
//
//		teamPanel.add(rTools);

		teams = new UserViewer(
				//project.getTeam()
				10, false, true, true);


		teamPanel.add(teams);
//		teamPanel.setWidgetTopBottom(teams, 28, Unit.PX, 28, Unit.PX);
		
		for (UserInfo ui: theProject.getTeam()) {
			if (!teams.getTable().getList().contains(ui))
				teams.getTable().add(ui);
			teams.getSelectionModel().setSelected(ui, true);
		}

	}
	
	void addToolsPanel() {
//		FlowPanel bTools = new FlowPanel();
//		Label bookTool = new Label("Project tools:");
//		bTools.setStyleName("IeegSubHeader");
//		bTools.add(bookTool);
//
//		toolsPanel.add(bTools);
		List<String> toolColNames = new ArrayList<String>();
		List<TextColumn<ToolDto>> toolColumns = new ArrayList<TextColumn<ToolDto>>();
		
		toolColNames.add("Tool Name");
		toolColumns.add(ToolViewer.toolName);
		toolColNames.add("Description");
		toolColumns.add(ToolViewer.toolDescription);
		tools = new ToolViewer( 
				//project.getTools(),
				10, 0, false, true, true);

//		final SelectionModel<ToolInfo> selectionModel3 = 
//				tools.getSelectionModel();

		toolsPanel.add(tools);
//		toolsPanel.setWidgetTopBottom(tools, 28, Unit.PX, 28, Unit.PX);
		tools.getTable().addAll(theProject.getTools());


	}
	
	public void addDiscussionsPanel(DiscussionViewer.Handler handler) {
		GWT.log("Adding discussion handler");
//		FlowPanel pDiscussion = new FlowPanel();
//		Label discuss = new Label("Project discussion:");
//		pDiscussion.setStyleName("IeegSubHeader");
//		pDiscussion.add(discuss);

//		discussionPanel.add(pDiscussion);

		discussion = new DiscussionViewer("",//"Discussions for " + theProject.getName(),
				handler, theProject.getPubId(), getSessionModel() );
		
		discussionPanel.add(discussion);
//		discussionPanel.setWidgetTopBottom(discussion, 28, Unit.PX, 28, Unit.PX);
	}
	
	public void addSearchResults(Project proj, Collection<SearchResult> results) {
		if (theProject == null)
			init(proj);
		
		snapshots.getTable().addAll(results);
		GWT.log("Added " + results.size() + " search results to project");
		for (SearchResult sr: results) {
			if (theProject.getSnapshots().contains(sr)) {
				snapshots.getSelectionModel().setSelected(sr, true);
				GWT.log("Item set to selected");
			} else
				snapshots.getSelectionModel().setSelected(sr, false);
		}
	}
	
	public void addTools(Collection<ToolDto> toolSet) {
		tools.getTable().addAll(toolSet);
		GWT.log("Added " + toolSet.size() + " tools to project");
		for (ToolDto ti: toolSet) {
			if (theProject.getTools().contains(ti)) {
				tools.getSelectionModel().setSelected(ti, true);
				GWT.log("Item set to selected");
			} else
				tools.getSelectionModel().setSelected(ti, false);
		}
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		return new ProjectPanel(controller, clientFactory, //project, 
				title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	@Override
	public WindowPlace getPlace() {
		return new ProjectPlace(theProject.getPubId());
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return ProjectPlace.getFromSerialized(params);
//	}

	@Override
	public List<UserInfo> getAdmins() {
		return admins.getTable().getList();
	}

	@Override
	public void addAdmin(UserInfo user) {
		admins.getTable().add(user);
	}

	@Override
	public Set<UserInfo> getSelectedAdmins() {
		return admins.getTable().getSelectedItems();
	}

	@Override
	public void setSelectedAdmin(UserInfo user, boolean sel) {
		admins.getTable().getSelectionModel().setSelected(user, sel);
	}

	@Override
	public List<UserInfo> getTeamMembers() {
		return teams.getTable().getList();
	}

	@Override
	public void addTeamMember(UserInfo user) {
		teams.getTable().add(user);
	}

	@Override
	public Set<UserInfo> getSelectedTeamMembers() {
		return teams.getTable().getSelectedItems();
	}

	@Override
	public void setSelectedTeamMember(UserInfo user, boolean sel) {
		teams.getTable().getSelectionModel().setSelected(user, sel);
	}

	@Override
	public List<PresentableMetadata> getSnapshots() {
		return snapshots.getTable().getList();
	}

	@Override
	public void addSnapshots(Collection<? extends PresentableMetadata> coll) {
		snapshots.getTable().addAll(coll);
	}

	@Override
	public void addSnapshot(PresentableMetadata sr) {
		snapshots.getTable().add(sr);
	}

	@Override
	public Set<PresentableMetadata> getSelectedSnapshots() {
		return snapshots.getTable().getSelectedItems();
	}

	@Override
	public void setSelectedSnapshot(PresentableMetadata sr, boolean sel) {
		snapshots.getTable().getSelectionModel().setSelected(sr, sel);
	}

	@Override
	public void removeSnapshot(PresentableMetadata sr) {
		snapshots.getTable().remove(sr);
	}

	@Override
	public List<ToolDto> getTools() {
		return tools.getTable().getList();
	}

	@Override
	public void addTool(ToolDto ti) {
		tools.getTable().add(ti);
	}

	@Override
	public Set<ToolDto> getSelectedTools() {
		return tools.getTable().getSelectedItems();
	}

	@Override
	public void setSelectedTool(ToolDto ti, boolean sel) {
		tools.getTable().getSelectionModel().setSelected(ti, sel);
	}

	@Override
	public void setEegHandler(ClickHandler handler) {
		toolbar.setEegHandler(handler);
	}

	@Override
	public void setDicomHandler(ClickHandler handler) {
		toolbar.setDicomHandler(handler);
	}

	@Override
	public void setDocumentHandler(ClickHandler handler) {
		toolbar.setDocumentHandler(handler);
	}

	@Override
	public void setImageHandler(ClickHandler handler) {
		toolbar.setImageHandler(handler);
	}

	@Override
	public void enableEegHandler(boolean sel) {
		toolbar.enableEegHandler(sel);
	}

	@Override
	public void enableDicomHandler(boolean sel) {
		toolbar.enableDicomHandler(sel);
	}

	@Override
	public void enableDocumentHandler(boolean sel) {
		toolbar.enableDocumentHandler(sel);
	}

	@Override
	public void enableImageHandler(boolean sel) {
		toolbar.enableImageHandler(sel);
	}

	@Override
	public void addPost(Post p) {
		discussion.postMessage(p);
	}

	@Override
	public void setPostList(List<Post> pl) {
		discussion.renderPosts(pl);
	}

	@Override
	public void renderNextPost() {
		discussion.setNextPost();
	}

	@Override
	public void addSnapshotSelectionChangedHandler(Handler handler) {
		snapshots.getSelectionModel().addSelectionChangeHandler(handler);
		
		
	}

	@Override
	public void addToolSelectionChangedHandler(Handler handler) {
		tools.getSelectionModel().addSelectionChangeHandler(handler);
	}

	@Override
	public void addAdminSelectionChangedHandler(Handler handler) {
		admins.getSelectionModel().addSelectionChangeHandler(handler);
	}

	@Override
	public void addTeamMemberSelectionChangedHandler(Handler handler) {
		teams.getSelectionModel().addSelectionChangeHandler(handler);
	}

	@Override
	public void log(String msg) {
		GWT.log(msg);
	}

	public String getTitle() {
		return "Project";
	}
	
	public int getMinHeight() {
	  return minHeight;
	}

  @Override
  public void addSnapshotPreviewHandler(
      com.google.gwt.view.client.CellPreviewEvent.Handler<PresentableMetadata> handler) {
    // TODO Auto-generated method stub
    snapshots.getTable().addCellPreviewHandler(handler);
  }
  
	public boolean isSearchable() {
		return false;
	}

	@Override
	public void hideProjectLoadingPopup() {
		projectLoadingPopup.hide();
	}
	
	@Override
	public String getPanelCategory() {
		return PanelTypes.COLLABORATIONS;
	}

	@Override
	public void bindDomEvents() {
		toolbar.bindDomEvents();
	}

	@Override
	public boolean confirmDatasetRemoval(int numberToRemove) {
		final String datasetWord = numberToRemove == 1 ? "dataset" : "datasets";
		final String msg = "Really remove " + datasetWord
				+ " from project? Click OK to remove "
				+ datasetWord + " from project. Click Cancel to keep "
				+ datasetWord + " in project.";
		return Window.confirm(msg);
	}
}
