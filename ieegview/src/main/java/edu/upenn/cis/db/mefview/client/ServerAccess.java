/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.db.mefview.client.controller.SnapshotCache;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IDownloadAccept;
import edu.upenn.cis.db.mefview.client.events.AnnotationRangeReceivedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRangeRequestedEvent;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotReceivedEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotRequestedEvent;
import edu.upenn.cis.db.mefview.client.events.UpdateSavedAnnotationsEvent;
import edu.upenn.cis.db.mefview.client.messages.AnnotationRangeMessage;
import edu.upenn.cis.db.mefview.client.messages.SnapshotDataMessage;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestAnnotations;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;
import edu.upenn.cis.db.mefview.client.messages.requests.UpdateSavedAnnotations;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesFilterSpecifier;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanJs;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.util.DataConversion;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.mefpageservers3.TooMuchDataRequestedException;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationModification;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.DataBundle;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.SnapshotAnnotationInfo;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;
import edu.upenn.cis.db.mefview.shared.TsEventType;
import edu.upenn.cis.db.mefview.shared.parameters.LineLengthEchauzParameters;

/**
 * Main point of access to the time series data on the server-side.
 * Also handles prefetching and caching.
 * 
 * @author zives
 *
 */
public class ServerAccess implements SnapshotServicesAsync, SnapshotRequestedEvent.Handler, 
AnnotationRangeRequestedEvent.Handler, OpenSignedUrlEvent.Handler,  
UpdateSavedAnnotationsEvent.Handler {
	public static class Prefetch {
//		DataSnapshotViews model;

		DataSnapshotModel state;
		String snapshot;
		String context;
		List<String> channels;
		long start;
		long end;
		double period;
		long width;
		List<DisplayConfiguration> filters;
		
		public Prefetch(//DataSnapshotViews model,
				DataSnapshotModel model,
				String snapshot, 
				String context, 
				List<String> channels, 
				long start, 
				long end, 
				double period, 
				long width,
				List<DisplayConfiguration> filters) {
//			this.model = model;
			this.state = model;//.getState();
			this.width = width;
			this.snapshot = snapshot;
			this.context = context;
			this.channels = channels;
			this.start = start;
			this.end = end;
			this.period = period;
			this.filters = filters;
		}
	}
	public static class Prefetcher {
		public Prefetcher(AstroPanel a) {

		}

		public void addRequest(Prefetch p) {
			// Clear any prior prefetch requests that got queued up
			prefetchList.clear();
			prefetchList.add(p);
			//			System.out.println("ADDED - PREFETCH QUEUE LENGTH: " + prefetchList.size());
		}
	}
	public static native boolean getURL(String url)/*-{
		try {
			var newWindow = $wnd.open(url, 'target=_blank')
			
			return (newWindow != null);
		} catch (e) {
			return false;
		}
	}-*/;
	private SnapshotServicesAsync realService;
	
	
	//	HandlerManager messageBus;
	private ClientFactory clientFactory;
//	private String userid;
	private SessionToken sessionToken;
	private final Map<String, String> datasetIdToLastDataRequestId = new HashMap<>();
	public static final int DELAY = 800;
	public static final List<AstroPanel> viewers = new ArrayList<AstroPanel>();
	
//	Timer pending;
	
	public static final Map<String,String> channelsToSnapshots = new HashMap<String,String>();
	
	public static final Map<String,Set<AstroPanel>> snapshotsToViewers = new HashMap<String,Set<AstroPanel>>();
	
	private static final Map<String,SnapshotCache> snapshotCache = new HashMap<String,SnapshotCache>();
	
	static final Map<AstroPanel,Queue<Prefetch>> prefetchRequests = new HashMap<AstroPanel,Queue<Prefetch>>();
	
	static Queue<Prefetch> prefetchList = new LinkedList<Prefetch>();

	public ServerAccess(//String userid, //SessionToken sessionID, 
			SnapshotServicesAsync realService, 
			ClientFactory clientFactory) {//HandlerManager messageBus) {
		this.realService = realService;
//		this.userid = userid;
//		this.sessionToken = sessionID;
		
//		this.messageBus = messageBus;
		this.clientFactory = clientFactory;
		
		if (clientFactory != null) {
			IEventBus bus = IeegEventBusFactory.getGlobalEventBus();
			// Should always be true except with our JUnit server-side tests
			bus.registerHandler(SnapshotRequestedEvent.getType(), this);
			bus.registerHandler(AnnotationRangeRequestedEvent.getType(), this);
			bus.registerHandler(OpenSignedUrlEvent.getType(), this);
			bus.registerHandler(UpdateSavedAnnotationsEvent.getType(), this);
		}
		
	}
	
	public static void registerViewer(String snapshot, AstroPanel viewer) {
		GWT.log("Registering a viewer " + viewer.getTitle());
		viewers.add(viewer);
		Set<AstroPanel> viewers = snapshotsToViewers.get(snapshot);
		if (viewers == null) {
			viewers = new HashSet<AstroPanel>();
			if (!snapshotCache.containsKey(snapshot))
				snapshotCache.put(snapshot, new SnapshotCache());
			
			if (!prefetchRequests.containsKey(viewer))
				prefetchRequests.put(viewer, new LinkedList<Prefetch>());
		}
		viewers.add(viewer);
		if (viewer instanceof IEEGViewerPanel)
			((IEEGViewerPanel)viewer).registerPrefetchListener(new Prefetcher(viewer));
	}
	
	public static void unregisterViewer(String snapshot, AstroPanel viewer) {
		ServerAccess.viewers.remove(viewer);
		Set<AstroPanel> viewers = snapshotsToViewers.get(snapshot);
		if (viewers != null) {
			viewers.remove(viewer);
			if (viewers.isEmpty()) {
				snapshotsToViewers.remove(snapshot);
				snapshotCache.remove(snapshot);
			}
		}
		if (prefetchRequests.containsKey(viewer))
			prefetchRequests.remove(viewer);
	}
	
	public SessionToken getSessionToken() {
		return sessionToken;
	}
	
	public void setSessionToken(SessionToken token) {
		sessionToken = token;
	}


	@Override
	public void addAndRemoveAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotationsToSave,
			Set<Annotation> annotationsToRemove, Set<String> annotationLayersToRemove,AsyncCallback<DataSnapshotIds> callback) {
		realService.addAndRemoveAnnotations(sessionID, datasetID, annotationsToSave, annotationsToRemove, 
		    annotationLayersToRemove, callback);
		
	}

	@Override
	public void addDataSnapshotTimeSeries(SessionToken sessionID, String revID, Set<String> series,
			AsyncCallback<String> callback) {
		realService.addDataSnapshotTimeSeries(sessionID, revID, series, callback);

	}


	private void assembleFromCache(final SnapshotCache cache, int left, int right,
			final long start, final long end,
			final double per, final RequestSnapshotData action, boolean isPartial) {
		final ArrayList<TimeSeriesSpan> dataSpans = new ArrayList<TimeSeriesSpan>();
		SnapshotCache.debug2("** Reading from cache to get " + DataConversion.getUUValue(start) + " through " + DataConversion.getUUValue(end) + " **");
		
		JsArray<JsArrayInteger> temp = cache.getEntry(left, right, start, end, per, action.filters);
		
//		cache.debug("Returned from getEntry");
		
//		cache.checkIntegrity(temp);
		
		if (isPartial) {
			//Going to check if it makes sense to fire event to update graph since this 
			//is partial and a request for new data will be sent to server.

			//Seems like these need to be doubles for the calculations to work in Super Dev Mode
			final double foundStart = temp.get(0).get(0);
			final double foundEnd = temp.get(temp.length() - 1).get(0);
			if (Math.abs(start - foundStart) > per || Math.abs(foundEnd - end) > per) {
				GWT.log("Not firing SnapshotReceivedEvent. Available range in cache: [" 
						+ foundStart 
						+ ", " 
						+ foundEnd 
						+ "], Requested range: ["
						+ start
						+ ", "
						+ end
						+"]. Difference of "
						+ (start - foundStart)/per
						+ " periods on left and "
						+ (foundEnd - end)/per
						+ " on right.");
				return;
			}
		}
		
		int length = temp.get(0).length();
		
		GWT.log("Cache entry has " + length + " items");
		
		double scale = action.requestedSpans.get(0).getTimeSegment().getScale();
		
		for (int i = 1; i < length; i++) {
			try {
				dataSpans.add(new TimeSeriesSpanJs(action.requestedSpans.get(i-1),
						(long)EEGModel.getStartDate(0,//action.getModel().getSnapshotStart(),  
								temp),
						(long)EEGModel.getEndDate(0, //action.getModel().getSnapshotStart(),  
								temp),
						//st, 
						//per,
						EEGModel.getPeriod(temp),
						scale, i, temp));
			} catch (Exception e) {
				GWT.log(e.getMessage());
				GWT.log(temp.toString());
			}
		}
//		cache.debug("Checking for complete match");
//		boolean isPartial = !cache.haveMatch(start, end, per, action.filters);

		SnapshotCache.debug("Firing event");
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new SnapshotReceivedEvent(new SnapshotDataMessage(action, 
				dataSpans, isPartial, cache.isMinMax())));
	}

	/**
	 * Called by timer event -- check our request pool
	 */
	void checkPendingRequests() {
		
//		System.out.println("POLL - PREFETCH QUEUE LENGTH: " + prefetchList.size());
		if (!prefetchList.isEmpty()) {
			Prefetch p = prefetchList.remove();
			
//			System.out.println("** ENQUEUING PREFETCH REQUEST **");

			List<INamedTimeSegment> info = p.state.getTraceInfo();
//			ArrayList<TimeSeriesMetadata> meta = new ArrayList<TimeSeriesMetadata>();
			ArrayList<TimeSeriesSpanSpecifier> spec = new ArrayList<TimeSeriesSpanSpecifier>();
			int i = 0;
			int[] ids = new int[p.filters.size()];
			for (INamedTimeSegment t: info) {
//				TimeSeriesMetadata md = new TimeSeriesMetadata(t); 
//				meta.add(md);
				TimeSeriesSpanSpecifier tss = new TimeSeriesSpanSpecifier(t,//md,
						(long)p.start, (long)(p.end - p.start), 1.E6 / p.period,
						new TimeSeriesFilterSpecifier(p.filters.get(i++)));
				spec.add(tss);
			}
			
			if (p.width > 0) {
				RequestSnapshotData action = new RequestSnapshotData(
						p.state.getSnapshotID(), 
						p.context,
						spec, p.filters, 0, 
						p.width, 
						ids);
				action.setDontProcess();
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new SnapshotRequestedEvent(action));
			}			
//			requestItem(p.snapshot, p.channels, p.start, p.end, p.period, p.filters);
		}
	}

	private void evictAsNecessary(JsArray<JsArrayInteger> newData) {
		// TODO: check against the cap, evict what is necessary
	}

	@Override
	public void getDataSnapshotAnnotations(SessionToken sessionID, String dataSnapshot, AsyncCallback<List<Annotation>> callback) {
		realService.getDataSnapshotAnnotations(sessionID, dataSnapshot, callback);
	}

	/**
	 * Returns the requested data region iff it is in the cache
	 * 
	 * @param snapshotID
	 * @param start
	 * @param end
	 * @param per
	 * @param filters
	 * @return
	 */
	public JsArray<JsArrayInteger> getDirectData(final String snapshotID, final long start, final long end, 
			final double per, final List<DisplayConfiguration> filters) {
		final SnapshotCache cache = snapshotCache.get(snapshotID);
		try {
		if (cache != null) {
			if (cache.havePartialMatch(start, end, per, filters)) {
				return cache.getEntry(start, end, per, filters);
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void getEegStudyMetadata(SessionToken sessionID, String studyRevId, AsyncCallback<StudyMetadata> callback) {
		realService.getEegStudyMetadata(sessionID, studyRevId, callback);
	}

	@Override
	public void getExperimentMetadata(SessionToken sessionID, String experimentId,
			AsyncCallback<ExperimentMetadata> callback) {
		realService.getExperimentMetadata(sessionID, experimentId, callback);
	}

	@Override
	public void getImagesForStudy(SessionToken sessionID, String studyPath, AsyncCallback<Set<ImageInfo>> callback) {
		realService.getImagesForStudy(sessionID, studyPath, callback);
	}
	
	@Override
	public void getPlaces(SessionToken sessionID, String dataSnapshotRevId, AsyncCallback<List<Place>> callback) {
		realService.getPlaces(sessionID, dataSnapshotRevId, callback);
	}

	@Override
	public void getRelatedAnalyses(SessionToken sessionID, String studyRevId, AsyncCallback<Set<DerivedSnapshot>> callback) {
		realService.getRelatedAnalyses(sessionID, studyRevId, callback);
	}

	@Override
	public void getResults(
			SessionToken sessionID, 
			String contextID, 
			String datasetID, 
			List<String> traceids,
			double start, 
			double width, 
			double samplingPeriod,
			List<DisplayConfiguration> filter, 
			SignalProcessingStep processing, 
			AsyncCallback<TimeSeries[]> callback) {
		realService.getResults(sessionID, 
				contextID, 
				datasetID, 
				traceids, 
				start, 
				width, 
				samplingPeriod, 
				filter, 
				processing, 
				callback);
	}

	@Override
	public void getResultsJSON(
			SessionToken sessionID, 
			String contextID, 
			String datasetID, 
			List<INamedTimeSegment> traceids,
			double start, 
			double width, 
			double samplingPeriod,
			List<DisplayConfiguration> filter, 
			SignalProcessingStep processing, 
			AsyncCallback<String> callback) {
		realService.getResultsJSON(
				sessionID, 
				contextID, 
				datasetID, 
				traceids, 
				start, 
				width, 
				samplingPeriod, 
				filter, 
				processing, 
				callback);
	}

//	@Override
//	public void getResultsJSON(
//			SessionToken sessionID, 
//			String contextID, 
//			String datasetID, 
//			List<String> traceids,
//			double start, 
//			double width, 
//			int scaleFactor, 
//			SignalProcessingStep processing, 
//			AsyncCallback<String> callback) {
//		realService.getResultsJSON(
//				sessionID, 
//				contextID, 
//				datasetID, 
//				traceids, 
//				start, 
//				width, 
//				scaleFactor, 
//				processing, 
//				callback);
//	}

	@Override
	public void getSearchResultForSnapshot(SessionToken sessionID, String dataSnapshotRevId,
			AsyncCallback<SearchResult> callback) {
		realService.getSearchResultForSnapshot(sessionID, dataSnapshotRevId, callback);
	}

	@Override
	public void getSnapshotProvenance(SessionToken sessionToken,
			String Snapshot, AsyncCallback<List<ProvenanceLogEntry>> callback) {
		realService.getSnapshotProvenance(sessionToken, Snapshot, callback);
	}

//	@Override
//	public void getTraces(SessionToken sessionID, String datasetID, AsyncCallback<List<TraceInfo>> callback) {
//		realService.getTraces(sessionID, datasetID, callback);
//	}

	public boolean haveCached(String snapshotID, long start, long end, double period) {
		final SnapshotCache cache = snapshotCache.get(snapshotID);
		return cache.haveMatch(start, end, period, null);
	}

	/**
	 * Tests for an exact match of the data versus the cache
	 * 
	 * @param snapshotID
	 * @param start
	 * @param end
	 * @param per
	 * @param filters
	 * @return
	 */
	public boolean haveDirectData(final String snapshotID, final long start, final long end, final double per,
			final List<DisplayConfiguration> filters) {
		final SnapshotCache cache = snapshotCache.get(snapshotID);
		
		return cache.haveMatch(start, end, per, filters);
	}

	@Override
	public void onAnnotationRangeRequested(final RequestAnnotations action) {
		realService.getDataSnapshotAnnotations(sessionToken, action.snapshotID, new SecFailureAsyncCallback.SimpleSecFailureCallback<List<Annotation>>(clientFactory) {

			@Override
			public void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Error reading annotations", "Unable to fetch annotations for snapshot " + action.snapshotID);
			}

			@Override
			public void onSuccess(List<Annotation> result) {
				// TODO: do multiple layers
				AnnotationGroup<Annotation> ag = new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, action.layers.iterator().next(), false);
//				ag.addAll(tsa);
				ag.addAll(result);
				
				AnnotationRangeMessage ra = new AnnotationRangeMessage(action, ag, false);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRangeReceivedEvent(ra));
//				System.out.println("Fired off the event to return annotations");
			}
			
		});
	}

	@Override
	public void onOpenSignedUrl(final OpenSignedUrl action) {
		clientFactory.getSessionModel().getPeriodicEvents().ensureScreenRefreshed();
		
		Frame f = action.frame;

		if (action.frame == null) {

			IDownloadAccept accept = DialogFactory.getDownloadAcceptDialog(new DialogInitializer<IDownloadAccept>() {
				
				@Override
				public void init(IDownloadAccept objectBeingInited) {
//					objectBeingInited.getFrame().setAttribute("src", action.url
//							+ (action.url.contains("?") ? "&" : "?")
//							+ "sessionId="
//							+ sessionToken.getId()
//							+ (action.otherParameters == null ? "" : action.otherParameters));
				}
	
				@Override
				public void reinit(IDownloadAccept objectBeingInited) {
//					objectBeingInited.getFrame().setAttribute("src", action.url
//							+ (action.url.contains("?") ? "&" : "?")
//							+ "sessionId="
//							+ sessionToken.getId()
//							+ (action.otherParameters == null ? "" : action.otherParameters));
				}
				
			});
			if (accept.getFrame() != null) {

				String url = action.url
						+ (action.url.contains("?") ? "&" : "?")
						+ "sessionId="
						+ sessionToken.getId()
						+ (action.otherParameters == null ? "" : action.otherParameters);

				accept.show("Download?", action.title, url);

				return;
			}
				final DialogBox db = new DialogBox(true, true);

				db.setTitle(action.title);
				db.setText(action.title);

				VerticalPanel vp = new VerticalPanel();

				vp.add(new HTML(
						"Please accept the download from your browser. It can take up to 10 seconds before the download starts."));

				f = new Frame();
				f.setSize("1px", "1px");
				vp.add(f);
				db.add(vp);

				Widget close = WidgetFactory.get().createButton("Close");
//				Button close = new Button("Close");
				vp.add(close);
				((HasClickHandlers)close).addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						db.hide();
					}

				});
				db.center();
				db.show();
			}
//		}
		f.setUrl(action.url
				+ (action.url.contains("?") ? "&" : "?") 
				+ "sessionId="	
				+ sessionToken.getId()
				+ (action.otherParameters == null ? "" : action.otherParameters));
	}

	
	static SignalProcessingStep step = new SignalProcessingStep("LL_echauz",
			new LineLengthEchauzParameters(10, 100, 100, 10));

	/**
	 * EEG Viewer is asking for data -- determine whether to retrieve from the cache
	 * or from the server.
	 */
	@Override
	public void onSnapshotRequested(final RequestSnapshotData action) {
		final long startTime = new Date().getTime(); 
//		pending.cancel();
		clientFactory.getSessionModel().getPeriodicEvents().stopTimer();

//		clientFactory.getPeriodicEvents().ensureScreenRefreshed();
		
		final ArrayList<INamedTimeSegment> channels = new ArrayList<INamedTimeSegment>();
		
		
		long start = Long.MAX_VALUE;
		long end = Long.MIN_VALUE;
		double freq = -1;
		
		if (action.dontProcess()) {
			SnapshotCache.debug2("Handling prefetch request");
		}
		
	      for (TimeSeriesSpanSpecifier spec: action.requestedSpans)
			channels.add(spec.getTimeSegment());
		
		if (action.requestedSpans.isEmpty()) {
			SnapshotCache.debug("No spans requested");
		}
		
		// TODO: ensure that these always have the same specifiers
		for (TimeSeriesSpanSpecifier sp: action.requestedSpans) {
			if (start > sp.getStartTime())
				start = sp.getStartTime();
			if (sp.getStartTime() + sp.getWidth() > end)
				end = sp.getStartTime() + sp.getWidth();
			
			if (freq < sp.getFrequency())
				freq = sp.getFrequency();
			
			break;
		}
		
		final double per = 1.E6/freq;

		final SnapshotCache cache = snapshotCache.get(action.snapshotID);
		
		if (action.dontCache)
			cache.evict();
		try {
		if (cache != null && cache.haveData() && !action.dontCache) {
			int left = cache.getLeftIndex(start);
			int right = cache.getRightIndex(end);
//			String contextID = action.getContext();
			
			long search = new Date().getTime();
			
			SnapshotCache.debug("Did binsearch - " + (search - startTime) + " msec in that");
			
//			if (cache.havePartialMatch(start, end, per, action.filters)) {
			if (cache.isPartialMatch(left, right)) {
//				final ArrayList<TimeSeriesSpan> dataSpans = new ArrayList<TimeSeriesSpan>();
//				cache.debug("Reading from cache to get " + start + " through " + end);
//				JsArray<JsArrayInteger> temp = cache.getEntry(start, end, per, action.filters);
//				
//				cache.debug("Returned from getEntry");
//				
//				int length = temp.get(0).length();
//				
//				for (int i = 1; i < length; i++) {
//					double scale = action.channels.get(i-1).getVoltageConversion();
//					
//					dataSpans.add(new TimeSeriesSpanJs(action.requestedSpans.get(i-1),
//							(long)EEGPaneData.getStartDate(temp),
//							(long)EEGPaneData.getEndDate(temp),
//							//st, 
//							//per,
//							EEGPaneData.getPeriod(temp),
//							scale, i, temp));
//				}
//				cache.debug("Checking for complete match");
				long part = new Date().getTime();
				
				SnapshotCache.debug("Checked for partial match, found " + left + "-" + right + " in " + (part - search) + " msec");

				boolean isPartial = !cache.isExactMatch(left, right, start, end, per, action.filters);
				
				if (!isPartial)
					isPartial |= !cache.filtersEqual(action.filters);
				
				long now = new Date().getTime();
				
				SnapshotCache.debug("Checked for partial and complete match - " + (now - part) + " msec");
//
//				cache.debug("Firing event");
//				messageBus.fireEvent(new SnapshotReceivedEvent(new ReturnedSnapshotDataAction(action, 
//						dataSpans, isPartial)));
				
				if (!action.dontProcess())
					assembleFromCache(cache, left, right, start, end, per, action, isPartial);
				
				long returning = new Date().getTime();
				
				SnapshotCache.debug("Fired event for data available - " + (returning - startTime) + " msec in handler");
				
				// If the match is exact, we can just return
				if (!isPartial) {
					SnapshotCache.debug2("** Complete match: no need to fetch data **");
//					pending.scheduleRepeating(DELAY);
					clientFactory.getSessionModel().getPeriodicEvents().resumeTimer();
					return;
				}
			}
//			SnapshotCache.debug2("** Requesting data " + start +"-" + end + " from server @" + per + " **");
		} else {
			long prelim = new Date().getTime();
			
			SnapshotCache.debug("Preliminaries - " + (prelim - startTime) + " msec in handler");
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long width = end - start;
		final long st = start;
		final long en = end;
		
		long before =  width * SnapshotCache.LOOKBEHIND;
		long after = width * SnapshotCache.LOOKAHEAD;
		
		if (action.dontProcess()) {
			before = 0;
			after = width;
		}
		
		double start2 = start;// - before;
		if (start2 < 0)
			start2 = 0;
		
		double end2 = end;// + after;
		double temp = action.requestedSpans.get(0).getTimeSegment().getStartTime() + 
				action.requestedSpans.get(0).getTimeSegment().getDuration();
		if (end2 > temp)
			end2 = temp;
		
		if (end2 < start2)
			end2 = start2;
		
		final double start3 = start2;
		final double end3 = end2;
		
		SnapshotCache.debug2("** Requesting " + action.snapshotID + " range " + (long)start2 + "-" + (long)end2 + 
				" @" + per + " with " + channels.size() + " channels and " + action.filters.size() + " filters");
	
		final long beforeCall = new Date().getTime();
		final String requestId = UUID.randomUUID().toString();
		datasetIdToLastDataRequestId.put(action.snapshotID, requestId);
		GWT.log("Issuing request " 
				+ requestId 
				+ " for dataset "
				+ action.snapshotID
				+ " with start: " 
				+ start2 
				+ ", duration: " 
				+ (end2 - start2) 
				+ ", period: " 
				+ per);
		// Server-side call
		realService.getResultsJSON(sessionToken, action.getContext(), action.snapshotID, channels,
				(double)start2, (double)(end2 - start2), 
				per, 
				action.filters, step, 
				//new SecFailureAsyncCallback<AnnotationsAndJson>(clientFactory) {
				new SecFailureAsyncCallback<String>(clientFactory) {
			
			@Override
			public void onFailureCleanup(Throwable caught) {
				clientFactory.getSessionModel().getPeriodicEvents().resumeTimer();
			}
			
			@Override
			public void onNonSecFailure(Throwable caught) {
				GWT.log("EXCEPTION in server access: " + caught.getMessage(), caught);
				if (caught instanceof ServerTimeoutException) {
					Window.alert("You've sent too many requests and/or the server is too busy to process your request.");
				} else if (caught instanceof TooMuchDataRequestedException) {
					Window.alert("You've requested too many seconds. Please reduce your page size.");
				} else {
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new SnapshotReceivedEvent(action));
				}
			}

//			public void onSuccess(final AnnotationsAndJson results) {
			@Override
			public void onSuccess(final String results) {
				final String lastDataRequestId = datasetIdToLastDataRequestId.get(action.snapshotID);
				if (lastDataRequestId != null
						&& lastDataRequestId != requestId) {
					GWT.log(
							"Ignoring response for request: "
									+ requestId
									+ ". Does not match latest request: "
									+ lastDataRequestId
									+ " for dataset: "
									+ action.snapshotID);
					return;
				}
		
				GWT.log(
						"Handling request: "
								+ requestId
								+ " for dataset: "
								+ action.snapshotID);

	
				SnapshotCache.debug2("**** Returned data from getRequestJSON");
				
//				GWT.log(results./*getAnnotatedData().*/toString());
				
				long beforeParse = new Date().getTime();
				JsArray<JsArray<?>> arBase = EEGModel.parseArray(results);//.getData());
				
				long afterParse = new Date().getTime();
				
				JsArray<JsArrayInteger> temp = EEGModel.getParsedArray(0,//action.getModel().getStartDate(), 
						arBase);
				
				final boolean isMinMax = EEGModel.isMinMax(arBase);
				long afterShuffle = new Date().getTime();
				
				SnapshotCache.debug("Call took " + (beforeParse - beforeCall) + "; Parse took " + (afterShuffle - beforeParse) + " msec with " + (afterParse - beforeParse) + " in initial parse");
				
				if (temp == null || temp.length() == 0) {
					clientFactory.getSessionModel().getPeriodicEvents().resumeTimer();
					GWT.log("Could not parse results");
					return;
				}
				
				SnapshotCache cache = snapshotCache.get(action.snapshotID);
				
				if (cache == null) {
					cache = new SnapshotCache(temp, action.filters, isMinMax);
					snapshotCache.put(action.snapshotID, cache);
				}

				cache.setFilter(action.filters);
				cache.setMinMax(isMinMax);
				cache.add(temp);
//				cache.checkIntegrity();
				evictAsNecessary(temp);
				double start = EEGModel.getStartDate(0,  temp);
				double end = EEGModel.getEndDate(0,  temp);
//				cache.checkIntegrity((end - start) / (temp.length()-1));
				SnapshotCache.debug2("getRequestJSON adding new data to cache: " + DataConversion.getUUValue(start) + "-" + DataConversion.getUUValue(end) + " (requested " + DataConversion.getUUValue(start3) + "-" + DataConversion.getUUValue(end3) + ")");
				
				int left = cache.getLeftIndex(st);
				int right = cache.getRightIndex(en);
				
//				int length = temp.get(0).length();
				

//				for (int i = 1; i < length; i++) {
//					double scale = action.channels.get(i-1).getVoltageConversion();
//					
//					dataSpans.add(new TimeSeriesSpanJs(action.requestedSpans.get(i-1),
//							(long)EEGPaneData.getStartDate(temp),
//							(long)EEGPaneData.getEndDate(temp),
//							//st, 
//							per,
////							EEGPaneData.getPeriod(temp),
//							scale, i, temp));
//				}
//		
//				snapshotCache.get(action.snapshotID).debug("Start " + (long)EEGPaneData.getStartDate(temp) + ", end " + (long)EEGPaneData.getEndDate(temp));
//				
//				messageBus.fireEvent(new SnapshotReceivedEvent(new ReturnedSnapshotDataAction(action, 
//						dataSpans, false)));
				if (!action.dontProcess())
					assembleFromCache(cache, left, right, st, en, per, action, false);
				
				/*
				Map<String,String> channelIDMap = new HashMap<String,String>();
				for (int i = 0; i < action.getModel().getNumChannels(); i++)
					channelIDMap.put(action.getModel().getChannelRevId(i),
							action.getModel().getChannelNames().get(i));
				
				for (List<Annotation> annList : results.getAnnotatedData())
					for (Annotation ann : annList) {
//						clientFactory.fireEvent(new AnnotationCreatedEvent(action.getModel(),
//								null, ann));
						List<String> channelLabels = new ArrayList<String>();
						for (String channel: ann.getChannels()) {
							channelLabels.add(channelIDMap.get(channel));
						}
						ann.setChannels(channelLabels);
						action.getModel().getAnnotationModel().addAnnotation(ann);
					}*/

				long returning = new Date().getTime();
				SnapshotCache.debug("Fired data available - " + (returning - startTime) + " msec in handler, with " + (returning - beforeParse) + " in fetch");
//				clientFactory.getPeriodicEvents().ensureScreenRefreshed();
				clientFactory.getSessionModel().getPeriodicEvents().resumeTimer();
		}
		});
		
//		pending.scheduleRepeating(DELAY);
//		clientFactory.getPeriodicEvents().resumeTimer();

	}
	
	
	
	
	@Override
	public void onUpdateSavedAnnotations(final UpdateSavedAnnotations<Annotation> action) {
		clientFactory.getSessionModel().getPeriodicEvents().ensureScreenRefreshed();
		
		// Get Set of Strings for Annotation Groups to be deleted.
		Set<String> delGroupNames = new HashSet<String>();
		for (AnnotationGroup<Annotation> gr : action.removeLayers){
		  delGroupNames.add(gr.getName());
		}
		
		realService.addAndRemoveAnnotations(sessionToken, action.snapshotID, 
				action.addAnnotations.getAnnotationSet(), action.removeAnnotations.getAnnotationSet(), 
				delGroupNames,
				new SecFailureAsyncCallback.SimpleSecFailureCallback<DataSnapshotIds>(clientFactory) {
			
			@Override
			public void onNonSecFailure(Throwable arg0) {
				Dialogs.messageBox("Error Saving Annotations!", "Unable to save annotations");
			}

			@Override
			public void onSuccess(final DataSnapshotIds newIds) {
				clientFactory.getSessionModel().getPeriodicEvents().ensureScreenRefreshed();
				Dialogs.messageBox("Success!", "We successfully saved "
                  + action.addAnnotations.getAnnotationSet().size()
                  + " and deleted "
                  + action.removeAnnotations.getAnnotationSet().size()
                  + " annotations.");
				
				
				// Map client side annotations-IDs to server side annotations.
				Map<String, String> internalIdsToIds = newIds.getTsAnnExternalIdsToIds();
              
              for (Annotation annotationToSave : action.addAnnotations.getAnnotationSet()) {
                  String internalId = annotationToSave.getInternalId();
                  if (internalIdsToIds.containsKey(internalId)) {
                      annotationToSave.setRevId(internalIdsToIds.get(internalId));
                      annotationToSave.setSaved();
                  }
              }
              action.model.getAnnotationModel().clearDeletedAnnotationList();


//				SavedSnapshotMessage sa = new SavedSnapshotMessage(newIds.getDsRevId(), action.model, action);
//				clientFactory.fireEvent(new SavedSnapshotEvent(sa));
				
			}
			
		});
	}

	@Override
	public void removeAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations,
			AsyncCallback<String> callback) {
		realService.removeAnnotations(sessionID, datasetID, annotations, callback);
	}

	@Override
	public void removeDataSnapshotContents(SessionToken sessionID, String revID, Set<String> series,
			AsyncCallback<String> callback) {
		realService.removeDataSnapshotContents(sessionID, revID, series, callback);

	}

	@Override
	public void saveAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations,
			AsyncCallback<DataSnapshotIds> callback) {
		realService.saveAnnotations(sessionID, datasetID, annotations, callback);
	}

	@Override
	public void savePlaces(SessionToken sessionID, String dataSnapshotRevId, List<Place> places, AsyncCallback<Boolean> callback) {
		realService.savePlaces(sessionID, dataSnapshotRevId, places, callback);
	}

	@Override
	public void storeDataSnapshotXml(SessionToken sessionID, String dataSnapshotRevId, String clob,
			AsyncCallback<String> callback) {
		realService.storeDataSnapshotXml(sessionID, dataSnapshotRevId, clob, callback);
	}

	@Override
	public void getAnnotationInfo(SessionToken sessionID, String snapshot,
			double startPosition, double endPosition,
			AsyncCallback<SnapshotAnnotationInfo> callback) {
		realService.getAnnotationInfo(sessionID, snapshot, startPosition, endPosition, callback);
	}

	@Override
	public void getNextValidDataRegion(SessionToken sessionID,
			String dataSetID, double position, AsyncCallback<Double> callback) {
		realService.getNextValidDataRegion(sessionID, dataSetID, position, callback);
	}

	@Override
	public void getPreviousValidDataRegion(SessionToken sessionID,
			String dataSetID, double position, AsyncCallback<Double> callback) {
		realService.getPreviousValidDataRegion(sessionID, dataSetID, position, callback);
	}

	@Override
	public void getPermittedActionsForDataset(SessionToken sessionID,
			String dsId, AsyncCallback<Set<AllowedAction>> callback) {
		realService.getPermittedActionsForDataset(sessionID, dsId, callback);
	}

//	@Override
//	public void getAnnotatedResults(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, double samplingPeriod, List<DisplayConfiguration> filter,
//			SignalProcessingStep processing,
//			AsyncCallback<AnnotationsAndJson> callback) {
//		realService.getAnnotatedResults(sessionID, contextID, datasetID, traceids, start, width, samplingPeriod, filter, processing, callback);
//	}
//
//	@Override
//	public void getAnnotatedResults(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, int scaleFactor, SignalProcessingStep processing,
//			AsyncCallback<AnnotationsAndJson> callback) {
//		realService.getAnnotatedResults(sessionID, contextID, datasetID, traceids, start, width, scaleFactor, processing, callback);
//	}

	@Override
	public void getSnapshotMetadata(SessionToken sessionID,
			String snapshotRevId, AsyncCallback<SnapshotContents> callback) {
		realService.getSnapshotMetadata(sessionID, snapshotRevId, callback);
	}

	@Override
	public void getDerivedSnapshots(SessionToken sessionID, String studyRevId,
			AsyncCallback<Set<SearchResult>> callback) {
		realService.getDerivedSnapshots(sessionID, studyRevId, callback);
	}

  @Override
  public void removeAnnotationLayers(SessionToken sessionID, String datasetID,
      Set<String> annotationLayersToRemove, AsyncCallback<Integer> callback) {
    realService.removeAnnotationLayers(sessionID, datasetID, annotationLayersToRemove, callback);
    
    
  }

	@Override
	public void getRecordingObjects(SessionToken sessionToken,
			String datasetId,
			AsyncCallback<List<RecordingObject>> callback) {
		realService.getRecordingObjects(sessionToken, datasetId, callback);
	}

	@Override
	public void registerFiles(String tempId, String snapshotName, SessionToken sessionID,
			List<String> files, AsyncCallback<Void> callback) {
		realService.registerFiles(tempId, snapshotName, sessionID, files, callback);
	}

	@Override
	public void getUuid(AsyncCallback<String> callback) {
		realService.getUuid(callback);
	}

	@Override
	public void getImageInfo(SessionToken sessionID, String studyPath,
			String traceRev, String trace, String typ,
			AsyncCallback<Set<ImageInfo>> callback) {
		realService.getImageInfo(sessionID, studyPath, traceRev, trace, typ, callback);
	}

	@Override
	public void getTracesForImage(SessionToken sessionID, String imageWithPath,
			AsyncCallback<Set<TraceImageInfo>> callback) {
		realService.getTracesForImage(sessionID, imageWithPath, callback);
	}

	@Override
	public void getImagesForStudy(SessionToken sessionID, String studyPath,
			String typ, AsyncCallback<Set<ImageInfo>> callback) {
		realService.getImagesForStudy(sessionID, studyPath, typ, callback);
	}

	@Override
	public void getContents(SessionToken sessionID, String datasetID, FileInfo file,
			AsyncCallback<String> asyncCallback) {
		realService.getContents(sessionID, datasetID, file, asyncCallback);
	}

	@Override
	public void saveContents(SessionToken sessionID, String datasetID, FileInfo file,
			String contents, AsyncCallback<Void> callback) {
		realService.saveContents(sessionID, datasetID, file, contents, callback);
	}

	@Override
	public void createRecordingObject(SessionToken sessionID, String datasetId,
			byte[] input, String filename, String contentType, boolean test,
			AsyncCallback<RecordingObject> callback) {
		realService.createRecordingObject(sessionID, datasetId, input, filename, contentType, test, callback);
	}

	@Override
	public void attachFiles(String target, String snapshot,
			SessionToken sessionID, List<String> files,
			AsyncCallback<List<RecordingObject>> secFailureAsyncCallback) {
		realService.attachFiles(target, snapshot, sessionID, files, secFailureAsyncCallback);
	}

	@Override
	public void getParentSnapshot(SessionToken sessionID, String studyRevId, AsyncCallback<String> callback) {
		realService.getParentSnapshot(sessionID, studyRevId, callback);
	}

	@Override
	public void getContentInfo(SessionToken sessionID, String dataSnapshot, AsyncCallback<Set<DataBundle>> callback) {
		realService.getContentInfo(sessionID, dataSnapshot, callback);
	}

	@Override
	public void getCaseMetadata(SessionToken sessionID, String studyRevId, AsyncCallback<CaseMetadata> callback) {
		realService.getCaseMetadata(sessionID, studyRevId, callback);
	}

//	@Override
//	public void getEvents(SessionToken sessionID,  
//			IDataset dataset, List<INamedTimeSegment> segments, TsEventType partitionOn,
//			AsyncCallback<Map<INamedTimeSegment, List<IDetection>>> callback) {
//		realService.getEvents(sessionID, dataset, segments, partitionOn, callback);
//	}
//
//	@Override
//	public void getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, TsEventType partitionOn,
//			AsyncCallback<Map<TsEventType, INamedTimeSegment>> callback) {
//		realService.getSegments(sessionID, dataset, segments, partitionOn, callback);
//	}
//
//	@Override
//	public void getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments,
//			AsyncCallback<SortedSet<INamedTimeSegment>> callback) {
//		realService.getSegments(sessionID, dataset, segments, callback);
//	}

	@Override
	public void getEventsAsSamples(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, 
			double start, double end, double period, AsyncCallback<Map<INamedTimeSegment, int[]>> callback) {
		realService.getEventsAsSamples(sessionID, dataset, segments, start, end, period, callback);
	}

	@Override
	public void getEventChannels(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> channels,
			AsyncCallback<List<INamedTimeSegment>> asyncCallback) {
		realService.getEventChannels(sessionID, dataset, channels, asyncCallback);
	}

	@Override
	public void getMyDatasets(SessionToken sessionID, AsyncCallback<Set<DatasetPreview>> callback) {
		realService.getMyDatasets(sessionID, callback);
	}

	@Override
	public void updateAnnotations(SessionToken sessionID, String datasetID, Collection<AnnotationModification> updates,
			long lastTime,
			AsyncCallback<Set<AnnotationModification> > callback) {
		realService.updateAnnotations(sessionID, datasetID, updates, lastTime, callback);
	}

	@Override
	public void getAnnotationsSince(SessionToken sessionID, String datasetID, long timestamp,
			AsyncCallback<Set<AnnotationModification>> callback) {
		realService.getAnnotationsSince(sessionID, datasetID, timestamp, callback);
	}

	@Override
	public void isExistingDatasetWithName(SessionToken sessionID, String datasetName, AsyncCallback<Boolean> callback) {
		realService.isExistingDatasetWithName(sessionID, datasetName, callback);
	}

	@Override
	public void inviteToShare(SessionToken sessionID, Set<String> email, String note, DatasetPreview dataset, Set<Integer> roles,
			AsyncCallback<Set<String>> callback) {
		realService.inviteToShare(sessionID, email, note, dataset, roles, callback);
	}

	@Override
	public void removeDataSnapshot(SessionToken sessionID, String revID, AsyncCallback<Boolean> callback) {
		realService.removeDataSnapshot(sessionID, revID, callback);
	}

//	@Override
//	public void addDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels,
//			AsyncCallback<Boolean> callback) {
//		realService.addDatasetChannels(sessionID, datasetID, channels, callback);
//	}

	@Override
	public void addDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects,
			AsyncCallback<Boolean> callback) {
		realService.addDatasetObjects(sessionID, datasetID, objects, callback);
	}

	@Override
	public void removeDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels,
			AsyncCallback<Boolean> callback) {
		realService.removeDatasetChannels(sessionID, datasetID, channels, callback);
	}

	@Override
	public void removeDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects,
			AsyncCallback<Boolean> callback) {
		realService.removeDatasetObjects(sessionID, datasetID, objects, callback);
	}

	@Override
	public void inviteToShare(SessionToken sessionID, Set<String> email, String note, PresentableMetadata other,
			Set<Integer> roles, AsyncCallback<Set<String>> callback) {
		realService.inviteToShare(sessionID, email, note, other, roles, callback);
	}

	@Override
	public void rejectShare(SessionToken sessionID, PresentableMetadata object, AsyncCallback<Void> callback) {
		realService.rejectShare(sessionID, object, callback);
	}

	@Override
	public void acceptShare(SessionToken sessionID, BasicCollectionNode target, PresentableMetadata object,
			AsyncCallback<Void> callback) {
		realService.acceptShare(sessionID, target, object, callback);
	}

	@Override
	public void setPublicAccess(SessionToken sessionID, String datasetID, Set<Integer> access,
			AsyncCallback<Void> callback) {
		realService.setPublicAccess(sessionID, datasetID, access, callback);
	}
}
