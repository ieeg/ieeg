/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.images;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.JavascriptFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


//@SuppressWarnings("deprecation")
public class SimpleImagePanel extends AstroTab implements //DocumentReadyEvent.Handler, 
SimpleImagePresenter.Display {
	static boolean injected = false;

	//	Image theImage = new Image();

	VerticalPanel vPanel;
	FlowPanel toolbar;
	ListBox imageList = null;
	List<FileInfo> images = null;

	String baseUrl = "";
	SessionToken token = null;
	String urlInFrame = "";
	boolean isLoaded = false;

	Widget prev;
	Widget next;
	DivElement image;
	
	final static boolean isCornerstone = true;//true;

	URLPlace place = new URLPlace("", datasetId, NAME);

	public final static String NAME = "2d-image";
	final static SimpleImagePanel seed = new SimpleImagePanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public SimpleImagePanel() {
		super();
	}

	public SimpleImagePanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {

		super(clientFactory, model,  
				title);

		if (!injected && isCornerstone) {
			final Command loadImage = new Command() {

				@Override
				public void execute() {
					injected = true;
					if (!urlInFrame.isEmpty()) {
						setUrl(urlInFrame);
					}
				}
				
			};
			final Command loadCornerstoneTools = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/cornerstoneTools.min.js", 
							loadImage, null);
				}
				
			};
			final Command loadCornerstoneMath = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/cornerstoneMath.min.js", 
							loadCornerstoneTools, null);
				}
				
			};
			final Command loadWadoImageLoader = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/cornerstoneWADOImageLoader.min.js", 
							loadCornerstoneMath, null);
				}
				
			};
			final Command loadDicomParser = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/dicomParser.min.js", 
							loadWadoImageLoader, null);
				}
				
			};
			final Command loadWebImageLoader = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/cornerstoneWebImageLoader.min.js", 
							loadDicomParser, null);
				}
				
			};
			final Command loadCornerstone = new Command() {

				@Override
				public void execute() {
					JavascriptFactory.loadJavascript("cornerstone/cornerstone.min.js", 
							loadWebImageLoader, null);
				}
				
			};
			
			// Load jquery first
			JavascriptFactory.loadJavascript("cornerstone/jquery.min.js", 
					loadCornerstone, 
					null);
			
		}

		vPanel = new VerticalPanel();
		add(vPanel);

		if (isCornerstone) {
			DivElement element = Document.get().createDivElement();

			element.setAttribute("style", "width:512px;height:512px;position:relative;color: white;display:inline-block;border-style:solid;border-color:black;");
			element.setAttribute("oncontextmenu", "return false");
			element.setAttribute("class", "disable-selection noIbar");
			element.setAttribute("unselectable", "'on'");
			element.setAttribute("onselectstart", "'return false;'");
			element.setAttribute("onmousedown", "'return false;'");

			image = Document.get().createDivElement();
			element.appendChild(image);

			vPanel.getElement().appendChild(element);

			image.setAttribute("id", "theImage");
			image.setAttribute("style", "width:512px;height:512px");
		} else {
//			final Command loadImage = new Command() {
//
//				@Override
//				public void execute() {
//					injected = true;
//				}
//				
//			};
//			JavascriptFactory.loadJavascript("papaya/papaya.js?version=0.8&build=910", 
//					loadImage, 
//					null);
			
			DivElement element = Document.get().createDivElement();
			element.setAttribute("style", "width:1024px;height:512px;position:relative;");

			image = Document.get().createDivElement();
			element.appendChild(image);
			image.setAttribute("class", "papaya");
			image.setAttribute("data-params", "params");

			vPanel.getElement().appendChild(element);
		}
		
		initControls();

		minHeight = 800;
	}

	void initControls() {
		FlowPanel toolbarWrapper = new FlowPanel();
		toolbar = new FlowPanel();
		toolbarWrapper.add(toolbar);
		toolbarWrapper.setStyleName("IeegTabToolbarWrapper");

		vPanel.add(toolbarWrapper);

		toolbar.setStyleName("IeegTabToolbar");    

		prev = WidgetFactory.get().createButton("Prev");
		toolbar.add(prev);

		imageList = new ListBox();
		toolbar.add(imageList);

		next = WidgetFactory.get().createButton("Next");
		toolbar.add(next);

		imageList.setEnabled(false);
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new SimpleImagePanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getImageTab();
	}

	@Override
	public WindowPlace getPlace() {
		return place;
	}

	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof URLPlace)
			this.place = (URLPlace)place;
	}

	
	@Override
	public void setBaseUrl(String baseUrl, SessionToken token) {
		this.baseUrl = baseUrl;
		this.token = token;
	}

	void setUrl(String url) {
		if (!isCornerstone)
			return;
		
		String selected = imageList.getSelectedItemText();
		String dicom = (selected.endsWith(".dicom") || selected.endsWith(".dcm")) ? 
				"dicomweb:" : "";
		GWT.log("Selected: " + selected + ", dicom = " + dicom);
		GWT.log("URL = " + dicom + GWT.getHostPageBaseURL()
			+ url + "?sessionId=" + getSessionModel().getSessionID().getId());
		setCornerstoneUrl(dicom + GWT.getHostPageBaseURL()
			+ url + "?sessionId=" + getSessionModel().getSessionID().getId());
	}

	native void setCornerstoneUrl(String url) /*-{
            // enable the dicomImage element and activate a few tools

            var element = $wnd.$('#theImage').get(0);
            $wnd.cornerstone.enable(element);
            $wnd.cornerstone.loadImage(url).then(function(image) {
                $wnd.cornerstone.displayImage(element, image);
                $wnd.cornerstoneTools.mouseInput.enable(element);
                $wnd.cornerstoneTools.mouseWheelInput.enable(element);
                $wnd.cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
                $wnd.cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
                $wnd.cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
                $wnd.cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel
            });
	}-*/;

	public String getTitle() {
		return "Images";
	}

	public boolean isSearchable() {
		return false;
	}

	public int getMinHeight() {
		return minHeight;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}

	@Override
	public void setUrl(String url, SessionToken token) {
		urlInFrame = url;

		if(injected)
			setUrl(url);
	}

	@Override
	public void setImages(List<FileInfo> list) {
		for (FileInfo fi: list)
			imageList.addItem(fi.getFriendlyName());
		
		this.images = list;
		
		//if (injected)
			load(images);
	}
	
	void load(List<FileInfo> theList) {
		
		if (isCornerstone || isLoaded)
			return;
		
		StringBuilder str = new 
				StringBuilder("[");
		boolean first = true;
		for (FileInfo fi: theList) {
			if (!fi.getTitle().endsWith(".dcm") && !fi.getTitle().endsWith(".dicom"))
				continue;
			
			if (first)
				first = false;
			else
				str.append(",");
			
			str.append('\"');
			str.append(GWT.getHostPageBaseURL() + 
					baseUrl + fi.getDetails() +
					"?sessionId=" + getSessionModel().getSessionID().getId());
			str.append('\"');
		}
		str.append("];");
//		JavascriptFactory.loadJavascriptString(str.toString());
		
//		try {
//			eval(str.toString());
//		} catch (Exception e) {
//			GWT.log(e.getMessage());
//		}
		
		final String params = str.toString();
		Timer t = new Timer() {

			@Override
			public void run() {
				startPapaya(params);
			}
			
		};

		t.schedule(500);
	}
	
	native void startPapaya(String params1) /*-{
		$wnd.params = [];
		$wnd.params["worldSpace"] = true;
		$wnd.params["images"] = eval(params1);
		$wnd.papaya.Container.startPapaya();
	}-*/;
	
	void doit() {
		/*


		 */
		final Command loadDone = new Command() {
			@Override
			public void execute() {
			}
			
		};
		final Command loadPoint = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/core/point.js", 
						loadDone, 
						null);
			}
			
		};
		final Command loadCoordinate = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/core/coordinate.js", 
						loadPoint, 
						null);
			}
			
		};
		final Command loadUrl = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/url-utils.js", 
						loadCoordinate, 
						null);
			}
			
		};
		final Command loadString = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/string-utils.js", 
						loadUrl, 
						null);
			}
			
		};
		final Command loadPlatform = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/platform-utils.js", 
						loadString, 
						null);
			}
			
		};
		final Command loadObject = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/object-utils.js", 
						loadPlatform, 
						null);
			}
			
		};
		final Command loadMath = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/math-utils.js", 
						loadObject, 
						null);
			}
			
		};
		final Command loadArray = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/array-utils.js", 
						loadMath, 
						null);
			}
			
		};
		
		final Command loadConstants = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/constants.js", 
						loadArray, 
						null);
			}
			
		};
		
		final Command loadPako = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/pako-inflate.js", 
						loadConstants, 
						null);
			}
			
		};
		final Command loadNumerics = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/numerics.js", 
						loadPako, 
						null);
			}
			
		};
		final Command loadJQuery = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/jquery.js", 
						loadNumerics, 
						null);
			}
			
		};
		final Command loadDaikon = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/daikon.js", 
						loadJQuery, 
						null);
			}
			
		};
		final Command loadBowser = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/bowser.js", 
						loadDaikon, 
						null);
			}
			
		};
		JavascriptFactory.loadJavascript("js/base64-binary.js", 
				loadBowser, 
				null);
	}
	
	
	native void eval(String str) /*-{
	    eval(str);
	}-*/;

	@Override
	public void selectImage(int inx) {
		imageList.setSelectedIndex(inx);
	}

	@Override
	public String getUrl() {
		return urlInFrame;
	}

	@Override
	public void addPrevHandler(ClickHandler handler) {
		((HasClickHandlers)prev).addClickHandler(handler);
	}

	@Override
	public void addNextHandler(ClickHandler handler) {
		((HasClickHandlers)next).addClickHandler(handler);
	}
}
