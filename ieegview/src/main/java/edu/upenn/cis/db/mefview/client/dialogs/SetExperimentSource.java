/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class SetExperimentSource extends DialogBox {
	ListBox datasetList = new ListBox();
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
//	TextBox theLabel = new TextBox();
	TextBox theTool = new TextBox();
	TextBox theType = new TextBox();
	
	/**
	 * 
	 * @param datasets Set of datasets, in the form of serach results
	 * @param sourceFriendly Friendly name of the source annotations' study
	 * @param sourceDsId Rev ID of the source annotations' study
	 * @param annList List of possible annotations
	 * @param annManager Map of possible annotations
	 */
	public SetExperimentSource(final Collection<SearchResult> sources,
//			final String tool,
//			final String toolRevId,
			final ToolDto tool,
			final Map<String, DataSnapshotViews> snapshots,
			final ClientFactory clientFactory
//			,
//			final List<ToolResult> experimentList
			) {
		VerticalPanel vp = new VerticalPanel();
		
		setTitle("Run Analysis of Data Snapshot");
		setText("Run Analysis of Data Snapshot");
		setWidget(vp);
	
		vp.add(new Label("Source data snapshot:"));
		vp.add(datasetList);
		
//		for (SearchResult s: sources)
//			datasetList.addItem(s.getFriendlyName(), s.getDatasetRevId());
		
		for (String s: snapshots.keySet())
			datasetList.addItem(snapshots.get(s).getState().getFriendlyName(), s);
		
//		vp.add(new Label("Experiment name:"));
//		vp.add(theLabel);
		
		vp.add(new Label("Tool name and build date/version:"));
		vp.add(theTool);
//		theTool.setText("MATLAB tool my.m 1.0 9/1/2011");
		theTool.setText(tool.getLabel() + " " + tool.getAuthorName());
		
		if (tool.getId().isPresent())
			theTool.setEnabled(false);
		
		vp.add(new Label("Experiment name (must be unique):"));
		vp.add(theType);
		
		Date d = new Date();
		String str = DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(d);
		
		theType.setText(str + " - " + tool.getLabel());
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(ok);
		hp.add(cancel);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		
		// Disable OK button if no target snapshot
		if (snapshots.isEmpty())
			ok.setEnabled(false);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
				
//				for (ToolResult tr: experimentList) {
//					if (tr.getAnalysisType().equals(theType.getText())) {
//						ControlPane.messageBox("Unable to Create Result!", "Each analysis result must have a unique name");
//						return;
//					}
//				}
				
				String selected = datasetList.getValue(datasetList.getSelectedIndex());
				
				DataSnapshotViews snapshot = snapshots.get(selected);
				List<INamedTimeSegment> channels = snapshot.getState().getTraceInfo();
				
				IEEGViewerPanel vw = snapshot.getFirstEEG();
				
				DisplayConfiguration filter;
				if (vw == null)
					filter = new DisplayConfiguration();
				else
					filter = vw.getEEGPane().getFilters().get(0);
				
				CreateExperimentSnapshotDialog le = new CreateExperimentSnapshotDialog(
						//selected,
						snapshot.getSearchResult(),
						theTool.getText(), 
						theType.getText(), tool,
						channels, filter, snapshot, clientFactory);

				le.center();
				le.show();
						
//					}					
//				}

			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}
			
		});
	}
	
//	public String getTitle() {
//		return theLabel.getText();
//	}
//	
	public String getTargetRevId() {
		return datasetList.getValue(datasetList.getSelectedIndex());
	}
	
	public String getTargetLabel() {
		return datasetList.getItemText(datasetList.getSelectedIndex());
	}
}
