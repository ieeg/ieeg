/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ProvenanceSummary implements IsSerializable {
	private int numSnapshots;
	private int numAnnotations;
	
	private int snapshotViews;
	private int snapshotAnalyses;
	
	private double score;
	private int rank;
	
	private UserInfo topContributor;
	
	public ProvenanceSummary() {}
	
	public ProvenanceSummary(int numSnapshots, int numAnnotations, int snapshotViews,
			int snapshotAnalyses, double score, int rank) {
		super();
		this.numSnapshots = numSnapshots;
		this.snapshotViews = snapshotViews;
		this.numAnnotations = numAnnotations;
		this.snapshotAnalyses = snapshotAnalyses;
		this.score = score;
		this.rank = rank;
	}
	
	public int getNumSnapshots() {
		return numSnapshots;
	}
	public void setNumSnapshots(int numSnapshots) {
		this.numSnapshots = numSnapshots;
	}
	public int getSnapshotViews() {
		return snapshotViews;
	}
	public void setSnapshotViews(int snapshotViews) {
		this.snapshotViews = snapshotViews;
	}
	public int getNumAnnotations() {
		return numAnnotations;
	}
	public void setNumAnnotations(int snapshotAnnotations) {
		this.numAnnotations = snapshotAnnotations;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getSnapshotAnalyses() {
		return snapshotAnalyses;
	}

	public void setSnapshotAnalyses(int snapshotAnalyses) {
		this.snapshotAnalyses = snapshotAnalyses;
	}

	public UserInfo getTopContributor() {
		return topContributor;
	}

	public void setTopContributor(UserInfo topContributor) {
		this.topContributor = topContributor;
	}
	
	
}
