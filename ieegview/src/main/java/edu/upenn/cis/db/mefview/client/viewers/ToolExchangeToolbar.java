/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;

public class ToolExchangeToolbar extends FlowPanel {
	Button run;
	Button add;
	Button bookmark;
	Button remove;
	Button update;
	
	public ToolExchangeToolbar() {
		this(null, null, null, null);//, null);
	}
	
	public ToolExchangeToolbar(ClickHandler runHandler, ClickHandler addHandler,
			ClickHandler updateHandler, ClickHandler removeHandler//,
			//ClickHandler bookmarkHandler
			) {
	  setStyleName("IeegTabToolbar");
	  
		add = new Button("Add...");
		add(add);
		if (addHandler != null)
			add.addClickHandler(addHandler);
		else
			add.setEnabled(false);
		
		update = new Button("Update...");
		add(update);
		if (updateHandler != null)
			update.addClickHandler(updateHandler);
		else
			update.setEnabled(false);
		
		remove = new Button("Remove");
		add(remove);
		if (removeHandler != null)
			remove.addClickHandler(removeHandler);
		else
			remove.setEnabled(false);


	}


	public void setBookmarkHandler(ClickHandler bookmarkHandler) {
	}
	
	
	public void setAddHandler(ClickHandler addHandler) {
		if (addHandler != null)
			add.addClickHandler(addHandler);
		else
			add.setEnabled(false);
	}
	
	public void setUpdateHandler(ClickHandler updateHandler) {
		if (updateHandler != null)
			update.addClickHandler(updateHandler);
		else
			update.setEnabled(false);
	}
	
	public void setRemoveHandler(ClickHandler removeHandler) {
		if (removeHandler != null)
			remove.addClickHandler(removeHandler);
		else
			remove.setEnabled(false);
	}
	

	public void enableAddHandler(boolean enabled) {
		add.setEnabled(enabled);
	}
	
	public void enableUpdateHandler(boolean enabled) {
		update.setEnabled(enabled);
	}
	
	public void enableRemoveHandler(boolean enabled) {
		remove.setEnabled(enabled);
	}
	
	public void enableBookmarkHandler(boolean enabled) {

	}

	public Button getRunButton() {
		return run;
	}

	public Button getAddButton() {
		return add;
	}

	public Button getUpdateButton() {
		return update;
	}

	public Button getRemoveButton() {
		return remove;
	}

	public void bindDomEvents() {
		/*
		for (Widget w: Arrays.<Widget>asList(run, add, bookmark, remove, update)) {
			final Widget wHere = w;
			if (wHere != null)
			Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
				
				@Override
				public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
					NativeEvent clickEvent = Converter.getNativeEvent(event);
					DomEvent.fireNativeEvent(clickEvent, wHere);
				}
			});
		}*/
	}

}
