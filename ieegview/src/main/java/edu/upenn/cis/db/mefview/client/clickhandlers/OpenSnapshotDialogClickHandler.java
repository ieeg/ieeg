/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.clickhandlers;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IOpenSnapshotById;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class OpenSnapshotDialogClickHandler implements ClickHandler {
	ClientFactory clientFactory;
	IOpenSnapshotById dialog;
//	OpenSnapshot dialog2;
	
	static OpenSnapshotDialogClickHandler theHandler = null;
	
	public static synchronized OpenSnapshotDialogClickHandler getHandler(ClientFactory cf) {
		if (theHandler == null)
			theHandler = new OpenSnapshotDialogClickHandler(cf);
		
		return theHandler;
	}
	
	protected OpenSnapshotDialogClickHandler(final ClientFactory cf) {
		clientFactory = cf;
//		dialog = new OpenSnapshotByNameOrId(clientFactory.getUserId(), 
//				clientFactory);
		
		dialog = DialogFactory.getSnapshotIdDialog(new DialogInitializer<IOpenSnapshotById>() {

			@Override
			public void init(IOpenSnapshotById objectBeingInited) {
				objectBeingInited.createDialog(cf);
				
				objectBeingInited.init(new IOpenSnapshotById.Handler() {

					@Override
					public void open(final String dataset) {
						cf.getSnapshotServices().getSearchResultForSnapshot(
								cf.getSessionModel().getSessionID(),
								dataset, new AsyncCallback<SearchResult>() {
					
									@Override
									public void onFailure(Throwable caught) {
										Window.alert("Unable to find this snapshot");
									}
					
									@Override
									public void onSuccess(SearchResult result) {
										if (result != null) {
											OpenDisplayPanel act = new OpenDisplayPanel(
													result, false,
													// AstroPanel.PanelType.EEG,
													EEGViewerPanel.NAME,
													null);
					
											IeegEventBusFactory.getGlobalEventBus()
													.fireEvent(
															new OpenDisplayPanelEvent(
																	act));
										} else {
											Window.alert("Dataset " + dataset + " not found.");
										}
									}
					
								});

					}
					
				});
			}

			@Override
			public void reinit(IOpenSnapshotById objectBeingInited) {

				
			}
			
		});
		
//		dialog2 = new OpenSnapshot();
	}

	@Override
	public void onClick(ClickEvent event) {

		dialog.open();
		
		Timer t = new Timer() {
		      @Override
		      public void run() {
		      	dialog.focus();
//		  		dataset.focus();
		      }
		    };
			t.schedule(500);
//		dialog.center();
//		dialog.show();
//		dialog.setFocus(true);
	}
	
}
