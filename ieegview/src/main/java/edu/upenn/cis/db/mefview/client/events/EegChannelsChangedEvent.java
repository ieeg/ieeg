/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class EegChannelsChangedEvent extends GwtEvent<EegChannelsChangedEvent.Handler> {
	private static final Type<EegChannelsChangedEvent.Handler> TYPE = new Type<EegChannelsChangedEvent.Handler>();
	
	private final List<String> channels;
	private final boolean[] isSelected;
	IEEGViewerPanel window;
	
	public EegChannelsChangedEvent(IEEGViewerPanel window, List<?> channels, boolean[] isSelected){
		this.channels = new ArrayList<String>(channels.size());
		if (channels.size() > 0 && channels.get(0) instanceof TraceInfo) {
			for (TraceInfo ti: (List<TraceInfo>)channels) {
				this.channels.add(ti.getLabel());
			}
		} else
			for (Object o: channels) {
				this.channels.add(o.toString());
			}
		
		this.isSelected = isSelected;
		this.window = window;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<EegChannelsChangedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EegChannelsChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EegChannelsChangedEvent.Handler handler) {
		handler.onChannelsChanged(window, channels, isSelected);
	}

	public static interface Handler extends EventHandler {
		void onChannelsChanged(IEEGViewerPanel w, List<String> channels, boolean[] isSelected);
	}
}
