/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.List;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class WorkspaceModel {
	final List<SearchResult> favoriteStudies;
	final List<ToolDto> favoriteTools;
//	final List<SearchResult> recentStudies;
//	final List<ToolDto> recentTools;
	final List<WebLink> resources;
	
//	final List<AstroPanel> windows = new ArrayList<AstroPanel>();

//	HashMap<EEGViewerPanel,EEGPlace> context = new HashMap<EEGViewerPanel,EEGPlace>();
//	HashMap<ImageViewerPanel,ImagePlace> context2 = new HashMap<ImageViewerPanel,ImagePlace>();
//	
//	public EEGPlace getPlaceFor(EEGViewerPanel w) {
//		EEGPlace ret = context.get(w);
//		if (ret == null) {
//			ret = new EEGPlace(w.getEEGPane().getStudyID());
////			ret.setFilters(w.getEEGPane().getFilterParameters());
//			context.put(w, ret);
//		}
//		return ret;
//	}
	
	public WorkspaceModel(final List<SearchResult> favoriteStudies,
			final List<ToolDto> favoriteTools,
//			final List<SearchResult> recentStudies,
//			final List<ToolDto> recentTools,
			final List<WebLink> resources,
			final ClientFactory factory) {
		this.favoriteStudies = favoriteStudies;
		this.favoriteTools = favoriteTools;
//		this.recentStudies = recentStudies;
//		this.recentTools = recentTools;
		this.resources = resources;
		
	}
	
}
