/*
 * Copyright 2015 Ieeg.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.client.events.EegMontageAddedEvent;

public class EditMontageDialog extends DialogBox {
  Button ok = new Button("OK");
  Button cancel = new Button("Cancel");
  ListBox plotChannel = new ListBox();
  ListBox to = new ListBox();
  ListBox map = new ListBox();
  Button remove = new Button("Remove");
  Button plotAgainst = new Button("Plot vs");
  TextBox name = new TextBox();
  CheckBox relativeToAverage;
  EEGMontage montage;
  
  TextBox errMessage = new TextBox();
  
  List<Integer> channelsInMontage = new ArrayList<>();

  public EditMontageDialog(final List<TraceInfo> traces, final EEGPane display) {
        setTitle("Channel Remontage");
        setText("Channel Remontage");
        setStyleName("ChanSelectDialog");
        VerticalPanel layout = new VerticalPanel();
        errMessage.setText("");
        errMessage.setWidth("220px");
        errMessage.setReadOnly(true);
//      
        final List<String> sources = new ArrayList<String>();
       for (TraceInfo i:traces){
         sources.add(i.getLabel());
       }
       layout.add(name);
       name.setText("newMontage");
        
        Collections.sort(sources);
        
        HorizontalPanel boxes = new HorizontalPanel();
        
        layout.add(boxes);
        
        
        
        
        relativeToAverage = new CheckBox("Re-montage against average");
        relativeToAverage.setValue(display.doAverageRemontage());
        relativeToAverage.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
            	errMessage.setText("");
                if (event.getValue()) {
                    plotAgainst.setEnabled(false);
                    map.clear();
                    plotChannel.setEnabled(false);
                    to.setEnabled(false);
                    remove.setEnabled(false);
                } else {
                    plotAgainst.setEnabled(true);
                    plotChannel.setEnabled(true);
                    to.setEnabled(true);
                    remove.setEnabled(true);
                }
            }
            
        });
        layout.add(relativeToAverage);
        layout.add(new HTML("<p></p>"));

        plotChannel = new ListBox();
        for (String s: sources)
            plotChannel.addItem(s);
        VerticalPanel fromPane = new VerticalPanel();
        fromPane.add(new Label("Channels:"));
        fromPane.add(plotChannel);
        
        plotChannel.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				errMessage.setText("");
			}
        });
        to.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				errMessage.setText("");
			}
        });
        
        
        boxes.add(fromPane);
        boxes.add(plotAgainst);
        boxes.setCellVerticalAlignment(plotAgainst, HasVerticalAlignment.ALIGN_MIDDLE);
        plotChannel.setVisibleItemCount(20);

        final List<String> dests = new ArrayList<String>();
        for (TraceInfo i:traces){
          dests.add(i.getLabel());
        }
        
        Collections.sort(dests);
        for (String s: dests)
            to.addItem(s);
        
        VerticalPanel toPane = new VerticalPanel();
        toPane.add(new Label("Ref Channels:"));
        toPane.add(to);
        boxes.add(toPane);
        to.setVisibleItemCount(20);
        VerticalPanel mapBox = new VerticalPanel();
        mapBox.add(new Label("Remontage (channel / ref):"));
        mapBox.add(map);
        map.setWidth("100px");
        map.setVisibleItemCount(15);
        mapBox.add(remove);
        mapBox.setCellHorizontalAlignment(remove, HasHorizontalAlignment.ALIGN_CENTER);
        
        final int[] iMap = new int[plotChannel.getItemCount()];
        
        for (int i = 0; i < iMap.length; i++)
            iMap[i] = -1;
        

        
        montage = new EEGMontage();
        

        plotAgainst.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
            	errMessage.setText("");
                String f = sources.get(plotChannel.getSelectedIndex());
                String t = dests.get(to.getSelectedIndex());
                if(channelsInMontage.contains(plotChannel.getSelectedIndex())) {
                	// Channel already in Montage;
                	errMessage.setText("Channel already exists in montage.");
                	return;
                }
                
                map.addItem(getMapString(f,t), plotChannel.getSelectedIndex() + "/" + to.getSelectedIndex());
                montage.getPairs().add(new EEGMontagePair(f,t));
                channelsInMontage.add(plotChannel.getSelectedIndex());
                
            }
        });
        
        remove.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
            	errMessage.setText("");
                if (map.getSelectedIndex() >= 0){
                   int index = map.getSelectedIndex();
                    map.removeItem(index);
                    channelsInMontage.remove(index);
                    montage.getPairs().remove(index);
                    }
                
            }
            
        });
        
        VerticalPanel gap = new VerticalPanel();
        gap.setWidth("20px");
        boxes.add(gap);
        boxes.add(mapBox);
        
        setWidget(layout);

        HorizontalPanel hp = new HorizontalPanel();
        layout.add(hp);
        layout.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
        hp.add(errMessage);
        errMessage.setStylePrimaryName("errMessage");
        
        hp.add(ok);
        hp.add(cancel);
        
        ok.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
            	errMessage.setText("");
              montage.setName(name.getText());
              IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegMontageAddedEvent(display.getPanel(), montage));
              hide();
              
            }
            
        });
        
        cancel.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
            	errMessage.setText("");
                hide();
            }
            
        });
    }

  private String getMapString(String from, String to) {
    return from + " vs " + to;
  }

//  private int getFrom(String val) {
//    return Integer.valueOf(val.substring(0, val.indexOf("/")));
//  }
//
//  private int getTo(String val) {
//    return Integer.valueOf(val.substring(val.indexOf("/") + 1));
//  }
}
