/*******************************************************************************
 * Copyright 2013 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.QuickLinksPresenter.Display;
import edu.upenn.cis.db.mefview.client.presenters.AppPresenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.util.RequestHtml;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class UserProfilePanel extends AstroTab implements UserProfilePresenter.Display {
	HorizontalPanel userDataWrapper = new HorizontalPanel();
	FlowPanel guestWrapper = new FlowPanel();
	RecentActivitiesPane activities = new RecentActivitiesPane();
	UsageStatsPane usageStats = new UsageStatsPane();
	RecommendationsPane recs = new RecommendationsPane();
	PortalStatsPane portal = new PortalStatsPane();
	QuickLinksPresenter.Display quickLinks = GWT.create(QuickLinksPresenter.Display.class);//new LinksPage();//new QuickLinksPane();
	Panel bar;
	
	public static final IUserProfilePluginFactory profile = GWT.create(IUserProfilePluginFactory.class);
	
	public final static String NAME = "user-profile";
	final static UserProfilePanel seed = new UserProfilePanel();
	
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, false));
		} catch (PanelExistsException e) {
			e.printStackTrace();
		}
	}
	
	public UserProfilePanel() {
		super();
	}
	
	public UserProfilePanel(ClientFactory clientFactory,
			final String title//, 
			) {
		super(clientFactory, title);
		this.minHeight = 800;
		
		if (getSessionModel().isRegisteredUser()) {
		  drawUserContent();  
		} else {
		  drawGuestContent();
		  
		  // Initialize Slideshow, use timer to wait until page is rendered.
	        Timer timer = new Timer() {
	          public void run() {
	            slideInit();
	          }
	        };
	        
	        timer.schedule(200); 
		}
	
	     
		
	      
	}
	
	public void drawUserContent() {
	  GWT.log("Drawing USER-content");

	  DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX);
	  
	  // Right Panel
	  DockLayoutPanel rightPane = new DockLayoutPanel(Unit.PX);


	  // Left Panel
	  DockLayoutPanel leftPane = new DockLayoutPanel(Unit.PX);

	  mainPanel.addEast(rightPane, 350);
	  mainPanel.add(leftPane);
      add(mainPanel);
	  
	  // Add userProfile, populated in separate method.
	  leftPane.addNorth(userDataWrapper, 160);
	  userDataWrapper.addStyleName("ieeg-user-profile");

	  // Add recent activities panel...
	  LayoutPanel feedwrapper = new LayoutPanel();
	  feedwrapper.addStyleName("ieeg-user-updates");

	  Label welcomeLab = new Label("Timeline");
	  welcomeLab.addStyleName("ieeg-portal-welcome");
	  leftPane.addNorth(welcomeLab, 40);
	  leftPane.add(activities);
	  	  
	  
	  FlowPanel portP = new FlowPanel();
	  portP.addStyleName("ieeg-user-sidePane");
	  portP.addStyleName("ieeg-cornered-pane"); 

	  portP.add(portal);
	  rightPane.addNorth(portP,365);

	  FlowPanel contr = new FlowPanel();
	  contr.addStyleName("ieeg-user-sidePane");
	  Label contrLab = new Label("");
	  contr.add(contrLab);
	  contrLab.setStyleName("ieeg-user-sidebar-heading");
	  contr.add(usageStats);

	  FlowPanel recsP = new FlowPanel();
//	  recsP.addStyleName("ieeg-user-sidePane");
//	  recsP.addStyleName("ieeg-recs-and-messages"); //fix margin-bottom


	  quickLinks.initialize(clientFactory, profile);
	  
	  recsP.add((Widget)quickLinks);

	  recs.addStyleName("ieeg-portal-desc");
	  
	  
	  rightPane.addSouth(contr,150);
	  rightPane.add(recsP);
	  recsP.getElement().getStyle().setProperty("overflow", "scroll");

	}
	
	/**
	 * Draws contents for for Guest users.
	 */
	public void drawGuestContent(){
	  
	  DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX);
	  
	  GWT.log("Drawing GUEST-content");
	  
	  // SidePanel
	  DockLayoutPanel sideBar = new DockLayoutPanel(Unit.PX);
	  
	  // Add portal stats panel
      FlowPanel statPane = new FlowPanel();
      statPane.addStyleName("ieeg-user-sidePane");
      statPane.addStyleName("ieeg-cornered-pane"); //fix margin-bottom

      statPane.add(portal);
      sideBar.addNorth(statPane,365);

      
      FlowPanel recsP = new FlowPanel();
      recsP.addStyleName("ieeg-user-sidePane");

      Label recsLab = new Label("");
      recsP.add(recsLab);
      recsLab.addStyleName("ieeg-user-sidebar-heading");
      quickLinks.initialize(clientFactory, profile);
//      Label l = new Label("Highlighted Links");
//      l.setStylePrimaryName("heading");
//	  recsP.add(l);
      recsP.add((Widget)quickLinks);
      
      
      sideBar.add(recsP);
      
      mainPanel.addEast(sideBar, 350);
      add(mainPanel);
      
      Panel userData = profile.getPlugin(
    		  getSessionModel().getServerConfiguration(), 
    		  getSessionModel(), 
    		  getSessionModel().getUserInfo()).getBanner();

	  guestWrapper.setStylePrimaryName("userDataWrapper");
	  guestWrapper.add(userData);  
	  mainPanel.add(guestWrapper); // Add panel to page

      FlowPanel startButtonHolder = new FlowPanel();
  	
      bar = profile.getPlugin(
    		  getSessionModel().getServerConfiguration(), 
    		  getSessionModel(), 
    		  getSessionModel().getUserInfo()).getHighlightedButtons();
      
      startButtonHolder.add(bar);
      startButtonHolder.setStylePrimaryName("startButtonHolder");
      
      // Add startButtons to content
      guestWrapper.add(startButtonHolder);
	  
      RequestHtml.getHtmlContent(
    		  getSessionModel().getServerConfiguration().getServerName() + 
    		  "/guest-intro.hti",
    		  "ieeg-portal-desc", guestWrapper);
      
      // Add Logos to page
      Panel logoBanner = profile.getPlugin(
    		  getSessionModel().getServerConfiguration(), 
    		  getSessionModel(), 
    		  getSessionModel().getUserInfo()).getLogos();
      add(logoBanner);
      setWidgetLeftWidth(logoBanner, 0, Unit.PCT, 50, Unit.PCT);
      setWidgetBottomHeight(logoBanner, 0, Unit.PCT, 50, Unit.PX);
	}
	
	// Native javascript to initialize the slideshow.
	public static native void slideInit() /*-{
	
      var opts = {
      //auto-advancing slides? accepts boolean (true/false) or object
      auto : { 
          // speed to advance slides at. accepts number of milliseconds
          speed : 10000, 
          // pause advancing on mouseover? accepts boolean
          pauseOnHover : true 
      },
      // show fullscreen toggle? accepts boolean
      fullScreen : false, 
      // support swiping on touch devices? accepts boolean, requires hammer.js
      swipe : false 
      };
      $wnd.makeBSS('.bss-slides', opts); 
    }-*/;
	
	
	/**
	 * Draws contents for UserInfo when user is registered.
	 * @param info
	 */
	public void drawUserProfile(final UserInfo info) {
		String name = info.getName();
		userDataWrapper.clear();
		
		if (info.getProfile().getLastName().isPresent()
				&& info.getProfile().getFirstName().isPresent())
			name = info.getProfile().getFirstName().get() + " " +
				info.getProfile().getLastName().get();
		
		String degree = "";
		if (info.getProfile().getAcademicTitle().isPresent())
			degree = ", " + info.getProfile().getAcademicTitle().get();

		Label nameLabel = new Label(name + degree);
		
		if (info.getPhoto() == null || info.getPhoto().isEmpty()) {
			Image photo = new Image(ResourceFactory.getUnknownUser().getSafeUri());
			photo.setPixelSize(128, 128);
			userDataWrapper.add(photo);
		} else {
			Image photo;
			if (info.getPhoto().startsWith("http://"))
				photo = new Image(info.getPhoto());
			else
				photo = new Image(AppPresenter.URL + "/" + info.getPhoto());
			photo.setPixelSize(128, 128);
			userDataWrapper.add(photo);
		}
		
		FlowPanel userData = new FlowPanel();
		userDataWrapper.add(userData);	
		userData.add(nameLabel);
        nameLabel.addStyleName("ieeg-user-profile-name");
		
		String inst = "Unknown affiliation";
		if (info.getProfile().getInstitutionName().isPresent())
			inst = info.getProfile().getInstitutionName().get();
		userData.add(new Label(inst));
		
	}

	public String getPanelType() {
		return NAME;
	}

	@Override
	public void close() {
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			Project project, 
			boolean writePermissions,
			final String title) {
		return new UserProfilePanel(clientFactory, title);
		
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getUnknownUser();
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void setUserInfo(UserInfo info) {
		GWT.log("Drawing contents for the user");
		if (getSessionModel().isRegisteredUser()) {
		  drawUserProfile(info);
		} 
	}

	public String getTitle() {
		return "Network";
	}
	
	public static String getDefaultTitle() {
		return "Network";
	}
	
	public RecentActivitiesPane getRecentActivitiesPane() {
		return activities;
	}



	@Override
	public UsageStatsPane getUsageStatsPane() {
		return usageStats;
	}

	@Override
	public PortalStatsPane getPortalStatsPane() {
		return portal;
	}
    
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

  @Override
  public Display getQuickLinksPane() {
    return quickLinks;
  }

	@Override
	public String getPanelCategory() {
		return PanelTypes.NEWS;
	}

	@Override
	public void bindDomEvents() {
		usageStats.bindDomEvents();
		recs.bindDomEvents();
		portal.bindDomEvents();
		quickLinks.bindDomEvents();
	}
}
