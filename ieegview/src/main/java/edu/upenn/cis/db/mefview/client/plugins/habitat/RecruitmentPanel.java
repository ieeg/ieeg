/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.habitat;

import java.util.Arrays;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class RecruitmentPanel extends AstroTab implements  
RecruitmentPresenter.Display {
	Frame f = new Frame();
	ClientFactory clientFactory;
	HandlerRegistration hr;
	ExperimentConfiguration pane = new ExperimentConfiguration();


	public final static String NAME = "recruit-users";
	final static RecruitmentPanel seed = new RecruitmentPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public RecruitmentPanel() {
		super();
	}

	public RecruitmentPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, title);

		add(pane);

		initState();
	  }

	void initState() {
		pane.initValues();
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	/**
	 * This is the real constructor for the panel.  It gets called by the 
	 * PanelFactory, then calls the constructor with the appropriate
	 * parameters.
	 */
	@Override
	public RecruitmentPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		return new RecruitmentPanel(model, clientFactory, title);
	}

	/**
	 * This ultimately maps to a Resource that corresponds to a PNG in
	 * ...mefview.client.images.
	 */
	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	/**
	 * This is for saving state on shutdown, but can be ignored
	 */
	@Override
	public WindowPlace getPlace() {
		// TODO if we ever want to save on shutdown
		return null;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		// TODO if we ever support this
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return PDFPlace.getFromSerialized(params);
//	}

	/**
	 * A friendly name for what our panel does
	 */
	public String getTitle() {
		return "HABITAT";
	}
	
	public static String getDefaultTitle() {
		return "HABITAT";
	}

	/**
	 * This stub would be used to attach a handler to
	 * some control widget on the panel. 
	 */
	@Override
	public void addClickHandlerToButton(ClickHandler handler) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {


	}
}
