/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import org.gwt.advanced.client.datamodel.ListDataModel;
import org.gwt.advanced.client.ui.widget.ComboBox;
import org.gwt.advanced.client.ui.widget.combo.ComboBoxChangeEvent;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;

import edu.upenn.cis.db.mefview.client.util.DataConversion;

public class ComboBoxWithEnter<T extends ListDataModel> extends ComboBox<T> implements Attachable {
	NumericEnterHandler _handler = null;
	
	boolean textWasSelected = false;
	
	public ComboBoxWithEnter() {
		super();

		this.addKeyDownHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {
//				setHighlightRow(-1);
				textWasSelected = true;
				ListDataModel dm = getModel();
				dm.setSelectedIndex(-1);
				if (isListPanelOpened()) {
					hideList();
				}
				if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
					try {
						if (_handler != null) {
							_handler.handleEnterRequest(ComboBoxWithEnter.this.getValue());
							event.preventDefault();
							textWasSelected = false;
						}
					} catch (NumberFormatException nf) {
						setText("0");
						event.preventDefault();
					}
				}
			}
			
		});
		
		this.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
//				setHighlightRow(-1);
				textWasSelected = true;
				ListDataModel dm = getModel();
				dm.setSelectedIndex(-1);
				if (isListPanelOpened()) {
					hideList();
				}
				if (_handler != null && event.getCharCode() == KeyCodes.KEY_ENTER) {
					if (_handler != null) {
						_handler.handleEnterRequest(ComboBoxWithEnter.this.getValue());
						textWasSelected = false;
						event.preventDefault();
					}
				}
			}
		});
		
		this.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				if (event instanceof ComboBoxChangeEvent) {
					textWasSelected = false;
				}
				
			}
			
		});
	}
	
	public void setEnterHandler(NumericEnterHandler handler) {
		_handler = handler;
	}
	
	public double getValue() {
		try {
			if (!textWasSelected && getModel().getSelected() != null) {//getHighlightRow() >= 0) {
				double v = (Double)getModel().getSelected();
				return v;
			} else {
				if (getText().isEmpty())
					return 0;
				double v = Double.valueOf(getText());
				return v;
			}
		} catch (Exception e) {
			throw new NumberFormatException("Unable to convert " + getText());
		}
	}
	
	@Override
	public void setText(String s) {
//		setHighlightRow(-1);
		textWasSelected = true;
		ListDataModel dm = getModel();
		dm.setSelectedIndex(-1);
		super.setText(s);
	}
	
	
	
	public void setValue(double d, boolean update) {
		ListDataModel dm = getModel();
		textWasSelected = false;
		for (int i = 0; i < dm.getCount(); i++)
			if (((Double)dm.get(i)) == d) {
				setSelectedIndex(i);
				if (update && _handler != null)
					_handler.handleEnterRequest(d);
				return;
			}
		String val = DataConversion.getRoundedValue(d);
		setText(val);
		if (update && _handler != null)
			_handler.handleEnterRequest(Double.valueOf(val));
	}

	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
