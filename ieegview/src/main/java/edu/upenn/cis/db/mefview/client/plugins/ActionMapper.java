/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.shared.GWT;

import edu.upenn.cis.db.mefview.client.actions.IPluginAction;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * The main mapper between datatypes (GeneralMetadata.getMetadataCategory())
 * and plugins (Presenter).
 * 
 * @author zives
 *
 */
public class ActionMapper {
	static ActionMapper theMapper = null;
	
//	Map<String, List<IPluginAction> > presenterMap = new HashMap<String,List<IPluginAction>>();
	Map<String, List<String>> actionMap = new HashMap<String,List<String>>();
	
	/**
	 * The main factory-style method for getting the global mapper
	 * 
	 * @return
	 */
	public static synchronized ActionMapper getMapper() {
		if (theMapper == null)
			theMapper = new ActionMapper();
		
		return theMapper;
	}
	
	/**
	 * Register a presenter action for a named metadata type
	 * 
	 * @param metadataType
	 * @param presenterName
	 */
	public void registerMetadataAction(String metadataType, String action) {
		if (actionMap.get(metadataType) == null)
			actionMap.put(metadataType, new ArrayList<String>());
		
		actionMap.get(metadataType).add(action);
	}
	
	/**
	 * Register a presenter for a given metadata type
	 * 
	 * @param md
	 * @param presenterName
	 */
	public void registerMetadataAction(PresentableMetadata md, String action) {
		if (actionMap.get(md.getMetadataCategory()) == null)
			actionMap.put(md.getMetadataCategory(), new ArrayList<String>());
		
		actionMap.get(md.getMetadataCategory()).add(action);
	}

	/**
	 * Get a set of eligible presenters' names, each of which is invocable via the
	 * PresenterFactory.
	 * 
	 * @param selectedItems
	 * @return
	 */
	public List<String> getPresenters(Collection<PresentableMetadata> selectedItems) {
		List<String> ret = new ArrayList<String>();
		for (PresentableMetadata md : selectedItems) {
			List<String> presenterActions = actionMap.get(md.getMetadataCategory());
			for (String action: presenterActions)
				ret.add(action);
		}
		return ret;
	}
	
	public List<String> getPresenters(PresentableMetadata item) {
		List<PresentableMetadata> temp = new ArrayList<PresentableMetadata>();
		temp.add(item);
		return getPresenters(temp);
	}
	
	public List<String> getActions(Collection<PresentableMetadata> selectedItems) {
		List<String> ret = new ArrayList<String>();
		for (PresentableMetadata md : selectedItems) {
			if (md != null && md.getMetadataCategory() != null) {
				List<String> presenterActions;
				
				if (!(md instanceof FileInfo))
					presenterActions = actionMap.get(md.getMetadataCategory());
				else
					presenterActions = actionMap.get(((FileInfo)md).getMediaType());
					
				if (presenterActions != null) {
					GWT.log("PresenterMapper shows actions " + presenterActions.toString());
					for (String act: presenterActions)
						if (!ret.contains(act))
							ret.add(act);
				} else
					GWT.log("PresenterMapper shows no actions available");
			} else
				GWT.log("PresenterMapper shows no metadata category for item");
		}
		return ret;
	}
}
