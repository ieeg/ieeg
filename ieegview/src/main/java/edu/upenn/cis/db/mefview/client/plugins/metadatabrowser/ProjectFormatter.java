/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;

import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.MetadataFormatter;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;

public class ProjectFormatter implements MetadataFormatter {

	@Override
	public int getNumColumns() {
		return 1;
	}

	@Override
	public ImageResource getImage(PresentableMetadata meta) {
		return ResourceFactory.folder();
	}

	@Override
	public String getColumnContent(PresentableMetadata meta, int index) {
		if (index == 0)
			return meta.getLabel();
		else
			return "";
	}

	@Override
	public String getColumnContent(PresentableMetadata meta,
			String attrib) {
		return meta.getStringValue(attrib) == null ? "" : meta.getStringValue(attrib);
	}

	@Override
	public boolean isStarrable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isStarred(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean showPrev(PresentableMetadata meta, MetadataPresenter presenter) {
		return meta.getChildMetadata().size() > MetadataProvider.PAGE_SIZE &&
				!presenter.isFirstPage(meta);
	}

	@Override
	public boolean showNext(PresentableMetadata meta, MetadataPresenter presenter) {
		return meta.getChildMetadata().size() > MetadataProvider.PAGE_SIZE &&
				!presenter.isLastPage(meta);
	}

	@Override
	public void prevPage(PresentableMetadata meta, MetadataPresenter presenter) {
		presenter.prevPage(meta);
	}

	@Override
	public void nextPage(PresentableMetadata meta, MetadataPresenter presenter) {
		presenter.nextPage(meta);
	}

	@Override
	public void toggleStar(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		GWT.log("Toggle star on collection node -- should not happen");
	}

	@Override
	public boolean isRatable() {
		return false;
	}

	@Override
	public Boolean isRated(PresentableMetadata object, MetadataPresenter presenter) {
		return null;
	}

	@Override
	public void toggleRating(PresentableMetadata meta, MetadataPresenter presenter) {
		// TODO Auto-generated method stub
		
	}
}
