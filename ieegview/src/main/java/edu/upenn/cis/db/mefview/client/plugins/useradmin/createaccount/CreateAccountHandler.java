package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.CloseDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.util.Md5Hash;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class CreateAccountHandler implements ClickHandler {

	ClientFactory factory;
	CreateAccountPresenter.Display display;
	
	public CreateAccountHandler(ClientFactory factory, CreateAccountPresenter.Display display) {
		this.factory = factory;
		this.display = display;
	}

	@Override
	public void onClick(ClickEvent event) {
		if (factory.getSessionModel().isRegisteredUser() && display.getNewPassword().isEmpty()) {
			// A registered user can leave the password fields blank, so we don't change
			// the password
			;
		} else {
			// Check password match
			if (display.getNewPassword().isEmpty() || display.getNewPassword().length() < 6) {
				display.showIllegal();
				return;
			}
			
			if (!display.getNewPassword().equals(display.getNewPassword2())) {
				display.showMismatch();
				return;
			}

			if (!display.isInAgreement()) {
				display.showNoAgreement();
				return;
			}
			
		}
		
		if (factory.getSessionModel().isRegisteredUser()) {
			factory.getPortal().updateUser(
					factory.getSessionModel().getSessionID(),
					display.getAccountInfo(),
					display.getNewPassword().isEmpty() ? "" : Md5Hash
							.md5(display.getNewPassword()),
					new AsyncCallback<UserInfo>() {

						@Override
						public void onFailure(Throwable caught) {
							display.showErrorCreatingAccount();
						}

						@Override
						public void onSuccess(UserInfo result) {
							display.showCreatedAccount();

							factory.getSessionModel().setUserInfo(result);
							IeegEventBusFactory.getGlobalEventBus()
									.fireEvent(
											new CloseDisplayPanelEvent(
													display));
						}
					});
		} else {
			factory.getPortal().createUser(display.getAccountInfo(),
					Md5Hash.md5(display.getNewPassword()),
					new AsyncCallback<UserInfo>() {

						@Override
						public void onFailure(Throwable caught) {
							display.showErrorCreatingAccount();
						}

						@Override
						public void onSuccess(UserInfo result) {
							display.showCreatedAccount();
							Window.Location.assign(// "http://www.ieeg.org" +
									GWT.getHostPageBaseURL() + "main.html");

						}

					});
		}
	}
}
