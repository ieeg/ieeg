/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages.requests;

import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

public class UpdateSavedAnnotations<T> {
	public String snapshotID;
	public DataSnapshotModel model;
	public Set<String> layers = new HashSet<String>();
	public AstroPanel requestor;
	
	public AnnotationGroup<T> addAnnotations;
	public AnnotationGroup<T> removeAnnotations;
	public Set<AnnotationGroup<T>> removeLayers;
	
	public int[] requestIds;
  
	
//	public UpdateSavedAnnotations(String snapshotID, DataSnapshotModel model, String layer, 
//			AnnotationGroup<T> relevantAnnotations, 
//			AnnotationGroup<T> removeAnnotations, 
//			Set<AnnotationGroup<T>> removeLayers, AstroPanel requestor, int[] ids) {
//		this.snapshotID = snapshotID;
//		this.model = model;
//		this.layers.add(layer);
//		requestIds = ids;
//		this.requestor = requestor;
//		
//		this.addAnnotations = relevantAnnotations;
//		this.removeAnnotations = removeAnnotations;
//	}
	
	public UpdateSavedAnnotations(String snapshotID, DataSnapshotModel model, String layer, 
			AnnotationGroup<T> relevantAnnotations, 
			AnnotationGroup<T> removeAnnotations, 
			Set<AnnotationGroup<T>> removeLayers, AstroPanel requestor, int id) {
		this.snapshotID = snapshotID;
		this.model = model;
		this.layers.add(layer);
		this.requestor = requestor;
		
		requestIds = new int[1];
		requestIds[0] = id;
		
		this.addAnnotations = relevantAnnotations;
		this.removeAnnotations = removeAnnotations;
		this.removeLayers = removeLayers;
	}

//	public UpdateSavedAnnotations(String snapshotID, DataSnapshotModel model, Collection<String> layers, 
//			AnnotationGroup<T> relevantAnnotations, 
//			AnnotationGroup<T> removeAnnotations, 
//			Set<AnnotationGroup<T>> removeLayers, AstroPanel requestor, int[] ids) {
//		this.snapshotID = snapshotID;
//		this.model = model;
//		this.layers.addAll(layers);
//		requestIds = ids;
//		this.requestor = requestor;
//		
//		this.addAnnotations = relevantAnnotations;
//		this.removeAnnotations = removeAnnotations;
//		this.removeLayers = removeLayers;
//		
//	}
}
