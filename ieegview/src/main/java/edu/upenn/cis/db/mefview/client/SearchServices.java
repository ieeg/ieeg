/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Search;
import edu.upenn.cis.db.mefview.shared.SearchResult;

/**
 * The client side stub for the RPC service for search.
 */
@RemoteServiceRelativePath("search")
public interface SearchServices extends RemoteService {
	public ExperimentSearchCriteria getExperimentSearchCriteria(SessionToken sessionID) throws BtSecurityException;
	
	public Set<SearchResult> getSearchResultsForSnapshots(
			SessionToken sessionID, 
			final Set<String> dataSnapshotRevId) 
					throws BtSecurityException;

	public Set<SearchResult> getSnapshotsFor(SessionToken sessionID, ExperimentSearch expSearch) throws BtSecurityException;

	/**
	 * Get the set of snapshots matching the search parameters
	 * @param searchRestrictions
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	public Set<SearchResult> getSnapshotsFor(SessionToken sessionID, Search searchRestrictions) throws BtSecurityException;

	public List<LatLon> getUserLocations();

	List<? extends PresentableMetadata> getSearchMatches(SessionToken sessionID, String terms, int start, int max);


	public Set<SearchResult> getSnapshotsForUser(SessionToken sessionID);

	List<? extends PresentableMetadata> updateSearchMatches(Set<SearchResult> updatedResults, SessionToken sessionID,
			String terms, int start, int max);

	Set<? extends PresentableMetadata> getMyData(String where, SessionToken sessionID);
}
