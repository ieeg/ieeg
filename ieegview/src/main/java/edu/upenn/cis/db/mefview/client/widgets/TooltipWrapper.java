/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasAllMouseHandlers;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;

public class TooltipWrapper implements MouseOverHandler, MouseOutHandler, MouseMoveHandler, MouseDownHandler {
	Tooltip tip;
	int lastX = 0;
	int lastY = 0;

	public TooltipWrapper(HasAllMouseHandlers w, final String label) {
		w.addMouseOverHandler(this);
		w.addMouseOutHandler(this);
		w.addMouseMoveHandler(this);
		w.addMouseDownHandler(this);
		
		tip = new Tooltip(label);
	}
	
	public static class Tooltip extends PopupPanel {
		String label;
		Timer delay;
		boolean wantToShow = false;
		
		Tooltip(final String label) {
			super(true);
			this.label = label;
			
//			setText(label);
//			setTitle(label);
			setWidget(new Label(label));
			setStyleName("tooltip");
			
			delay = new Timer() {

				@Override
				public void run() {
//					GWT.log("Show tooltip!");
					Tooltip.super.show();
					wantToShow = false;
				}
				
			};
		}

		public void reset() {
			if (wantToShow) {
				delay.cancel();
				delay.schedule(10);
			}
		}
		
		@Override
		public void show() {
			wantToShow = true;
			delay.schedule(10);
		}
		
		@Override
		public void hide() {
//			GWT.log("Hide tooltip!");
			if (wantToShow)
				delay.cancel();
			wantToShow = false;
			super.hide();
		}
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
//		GWT.log("Mouse over");
		int x = event.getNativeEvent().getClientX() + 1;
		int y = event.getNativeEvent().getClientY() + 1;
		
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		
		tip.setPopupPosition(x, y);
		
		tip.show();
		if (x + tip.getOffsetWidth() > Window.getClientWidth()) {
			x = Window.getClientWidth() - tip.getOffsetWidth();
			tip.setPopupPosition(x, y);
		}
		if (y + tip.getOffsetHeight() > Window.getClientHeight()) {
			y = Window.getClientHeight() - tip.getOffsetHeight();
			tip.setPopupPosition(x, y);
		}

	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
//		GWT.log("Mouse out");
		tip.hide();
	}

	@Override
	public void onMouseMove(MouseMoveEvent event) {
		if (Math.abs(event.getNativeEvent().getClientX() - lastX) < 2 && Math.abs(event.getNativeEvent().getClientY() - lastY) < 2)
			return;
		
//		GWT.log("Mouse moved!");
		tip.reset();
	}

	@Override
	public void onMouseDown(MouseDownEvent event) {
		tip.hide();
	}
}
