/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SingleSelectionModel;

public class PagedTable<T> extends DataGrid<T> implements Attachable {
  int page;

  //	SimplePager dataPager = new SimplePager();
  ShowMorePagerPanel dataPager = new ShowMorePagerPanel();
  ListDataProvider<T> dataProvider = new ListDataProvider<T>();

  Set<T> favorites = new HashSet<T>();

  Map<Column<T,?>, Comparator<T>> comparators = new HashMap<Column<T,?>, Comparator<T>>();

  public interface StarToggleHandler<T> {
    public void onToggle(T object, boolean isSet);
  }

  public interface DataGridResources extends DataGrid.Resources 
  { 
    @Source(value = { DataGrid.Style.DEFAULT_CSS, "../css/gwtDataGrid.css" }) 
    DataGrid.Style dataGridStyle(); 
  } 

  public static final DataGridResources DataGridResources = GWT.create(DataGridResources.class);
  //
  static {
    DataGridResources.dataGridStyle().ensureInjected();
  }

  StarToggleHandler<T> handler = null;

    public PagedTable(int pageSize, boolean multiselect, boolean useChecks, boolean selectable, List<T> dataList, ProvidesKey<T> keyProvider,
      List<String> columnNames, List<? extends Column<T, ?>> columns, boolean addStars, 
          final ImageResource selected, final ImageResource unselected) {//ClientFactory factory) {
    super(pageSize, keyProvider );

    //TODO: Merge multiselect and selectable into single variable 
    
    setEmptyTableWidget(new HTML("No entries"));

    // Make selectable
    if (selectable){
      setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
    }
    // Make pageable
    setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

    // If multSelect, use checkbox to select cell, otherwise select cell on click
    if (multiselect & selectable) {
      final MultiSelectionModel<T> seriesSelectionModel = new MultiSelectionModel<T>(keyProvider);

      if (useChecks) {
        // Checkbox column. This table uses a checkbox column for selection.
        Column<T, Boolean> checkColumn = new Column<T, Boolean>(
            new CheckboxCell(true, false)) {
          @Override
          public Boolean getValue(T object) {
            // Get the value from the selection model.
            return seriesSelectionModel.isSelected(object);
          }
        };
        checkColumn.setCellStyleNames("ieeg-pageTableCheckColumn");
        addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        setColumnWidth(checkColumn, 30, Unit.PX);
        checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        setSelectionModel(seriesSelectionModel, 
            DefaultSelectionEventManager.<T> createCheckboxManager());
      } else {
        setSelectionModel(seriesSelectionModel);
      }
    } else if (selectable) {
      final SingleSelectionModel<T> seriesSelectionModel = new SingleSelectionModel<T>(keyProvider);
      setSelectionModel(seriesSelectionModel);
    } 

    if (addStars) {
      //addStar(factory.getStarSel(), factory.getStarUnsel());
      addStar(selected, unselected);
    }

    for (int i = 0; i < columnNames.size(); i++) {
      addColumn(columns.get(i), columnNames.get(i));
//      addColumnStyleName(i, "seriesCol");
    }

    // TODO: The SortHandler results does not seem to work correctly or interferes with other sorthandlers.
//    addColumnSortHandler(new ColumnSortEvent.ListHandler<T>(dataProvider.getList()) {
//      @Override
//      public void onColumnSort(final ColumnSortEvent ev) {
//        final ColumnSortEvent.ListHandler<T> me = this;
//
//        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
//
//          @Override
//          public void execute() {
//            me.onColumnSort(ev);
//            PagedTable.this.redraw();
//          }
//        });
//      }
//    });

    dataPager.setDisplay(this);
    dataProvider.setList(dataList);
    setSize(dataList.size());
    dataProvider.addDataDisplay(this);
    dataProvider.flush();
    dataProvider.refresh();

  }


  public PagedTable(int pageSize, boolean multiselect, boolean useChecks, List<T> dataList, ProvidesKey<T> keyProvider,
      List<String> columnNames, List<TextColumn<T>> columns) {
    super(pageSize, keyProvider);

	setWidth("100%");
	setHeight("200px");
    setEmptyTableWidget(new HTML("No entries"));


    setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);//.BOUND_TO_SELECTION);
    setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

    if (multiselect) {
      final MultiSelectionModel<T> seriesSelectionModel = new MultiSelectionModel<T>(keyProvider);

      if (useChecks) {
        // Checkbox column. This table uses a checkbox column for selection.
        Column<T, Boolean> checkColumn = new Column<T, Boolean>(
            new CheckboxCell(true, false)) {
          @Override
          public Boolean getValue(T object) {
            // Get the value from the selection model.
            return seriesSelectionModel.isSelected(object);
          }
        };
        checkColumn.setCellStyleNames("ieeg-pageTableCheckColumn");
        addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        setColumnWidth(checkColumn, 30, Unit.PX);
        checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

        setSelectionModel(seriesSelectionModel, 
            DefaultSelectionEventManager.<T> createCheckboxManager());
      } else {
        setSelectionModel(seriesSelectionModel);
      }
    } else {
      final SingleSelectionModel<T> seriesSelectionModel = new SingleSelectionModel<T>(keyProvider);
      setSelectionModel(seriesSelectionModel);
    }

    for (int i = 0; i < columnNames.size(); i++) {
      addColumn(columns.get(i), columnNames.get(i));
      addColumnStyleName(i, "seriesCol");
    }

    addColumnSortHandler(new ColumnSortEvent.ListHandler<T>(dataProvider.getList()) {
      @Override
      public void onColumnSort(final ColumnSortEvent ev) {
        final ColumnSortEvent.ListHandler<T> me = this;

        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {

          @Override
          public void execute() {
            me.onColumnSort(ev);
            PagedTable.this.redraw();
          }
        });
      }
    });

    //		setSelectionEnabled(true);
    dataPager.setDisplay(this);
    dataProvider.setList(dataList);
    setSize(dataList.size());
    dataProvider.addDataDisplay(this);
    dataProvider.flush();
    dataProvider.refresh();

  }

  public PagedTable(int pageSize, boolean multiselect, boolean useChecks, List<T> dataList, ProvidesKey<T> keyProvider,
      List<TextColumn<T>> columns) {
    super(pageSize, keyProvider);


    setEmptyTableWidget(new HTML("No entries"));


    setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);//.BOUND_TO_SELECTION);
    setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);

    if (multiselect) {
      final MultiSelectionModel<T> seriesSelectionModel = new MultiSelectionModel<T>(keyProvider);

      if (useChecks) {
        // Checkbox column. This table uses a checkbox column for selection.
        Column<T, Boolean> checkColumn = new Column<T, Boolean>(
            new CheckboxCell(true, false)) {
          @Override
          public Boolean getValue(T object) {
            // Get the value from the selection model.
            return seriesSelectionModel.isSelected(object);
          }
        };
        addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        setColumnWidth(checkColumn, 30, Unit.PX);
        checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
        setSelectionModel(seriesSelectionModel, 
            DefaultSelectionEventManager.<T> createCheckboxManager());
      } else {
        setSelectionModel(seriesSelectionModel);
      }
    } else {
      final SingleSelectionModel<T> seriesSelectionModel = new SingleSelectionModel<T>(keyProvider);
      setSelectionModel(seriesSelectionModel);
    }

    for (int i = 0; i < columns.size(); i++) {
      addColumn(columns.get(i));
      addColumnStyleName(i, "seriesCol");
    }

    //	  addColumnSortHandler(new ColumnSortEvent.ListHandler<T>(dataProvider.getList()) {
    //	    @Override
    //	    public void onColumnSort(final ColumnSortEvent ev) {
    //	      final ColumnSortEvent.ListHandler<T> me = this;
    //
    //	      Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
    //
    //	        @Override
    //	        public void execute() {
    //	          me.onColumnSort(ev);
    //	          PagedTable.this.redraw();
    //	        }
    //	      });
    //	    }
    //	  });

    //  setSelectionEnabled(true);
    dataPager.setDisplay(this);
    dataProvider.setList(dataList);
    setSize(dataList.size());
    dataProvider.addDataDisplay(this);
    dataProvider.flush();
    dataProvider.refresh();

  }






  @SuppressWarnings("unchecked")
  public void resort() {
    for (int i = getColumnSortList().size() - 1; i >= 0; i--) {
      final ColumnSortInfo c = getColumnSortList().get(i);

      final Column<T,?> col = (Column<T,?>)c.getColumn();
      final boolean ascending = c.isAscending();
      if (comparators.get(col) != null) {
        final Comparator<T> comp = comparators.get(col);
        if (ascending) {
        	Collections.sort(getList(), comp);
        } else {
        	Collections.sort(getList(), new Comparator<T>() {
				@Override
				public int compare(T o1, T o2) {
					return -comp.compare(o1, o2);
				}
			});
        }
        
      } 
    }
    dataProvider.refresh();
  }

  public ShowMorePagerPanel getPager() {
    return dataPager;
  }

  @Override
  public int getRowCount() {
    int ret = dataProvider.getList().size();
    //		if (ret < getPageSize())

    setPageStart(0);
    setPageSize(ret);
    setRowCount(ret);
    return ret;
  }

  public void setSize(int size) {
    setRowCount(size);
    setPageStart(0);
    setPageSize(size);
  }

  public List<T> getList() {
    return dataProvider.getList();
  }

  public ListDataProvider<T> getProvider() {
    return dataProvider;
  }

  public int size() {
    return getRowCount();
  }

  public T get(int index) {
    return getList().get(index);
  }

  public void add(T item) {
    getList().add(item);
    resort();
  }

  public void add(int pos, T item) {
    getList().add(pos, item);
    resort();
  }

  public void addAll(Collection<? extends T> items) {
    for (T item: items)
      if (!getList().contains(item))
        getList().add(item);
    resort();
  }

  public void remove(T item) {
    getList().remove(item);
  }

  public void remove(int index) {
    getList().remove(index);
  }

  public boolean isEmpty() {
    return getList().isEmpty();
  }

  public void clear() {
    getList().clear();
  }

  public Set<T> getSelectedItems() {
    Set<T> ret = new HashSet<T>();

    for (T item : getList())
      if (getSelectionModel().isSelected(item))
        ret.add(item);
    return ret;
  }

  public void setSelectedItems(Set<T> selected) {
    //		Set<T> ret = new HashSet<T>();

    for (T item : getList())
      getSelectionModel().setSelected(item, selected.contains(item));
  }

  public HandlerRegistration addColumnSort(Column<T,?> c, Comparator<T> comp) {
    ListHandler<T> sortHandler = new ListHandler<T>(getList());

    sortHandler.setComparator(c, comp);
    comparators.put(c, comp);

    return addColumnSortHandler(sortHandler);
  }

  public void setFavorites(Collection<T> fav) {
    favorites.clear();
    favorites.addAll(fav);
    redraw();
  }

  public void addFavorites(Collection<T> fav) {
    favorites.addAll(fav);
  }

  public void addFavorite(T fav) {
    favorites.add(fav);
  }

  public void removeFavorite(T fav) {
    favorites.remove(fav);
  }

  public Set<T> getFavorites() {
    return favorites;
  }

  public void addStar(final ImageResource sel, final ImageResource unsel) {
    Column<T, ImageResource> checkColumn = new Column<T, ImageResource>(
        new ClickImageResourceCell()) {

      @Override
      public ImageResource getValue(T object) {
        if (favorites.contains(object)) {
          return sel;
        } else
          return unsel;
      }

      @Override
      public void onBrowserEvent(Context context, Element elem,
          final T object, NativeEvent event) {
        super.onBrowserEvent(context, elem, object, event); 
        if ("click".equals(event.getType())) {
          //	            	GWT.log("Clicked on toggle");
          if (favorites.contains(object)) {
            favorites.remove(object);
            if (handler != null)
              handler.onToggle(object, false);
          } else {
            favorites.add(object);
            if (handler != null)
              handler.onToggle(object, true);
          }
          redraw();
        }
      }
    };
    checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
    addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
    setColumnWidth(checkColumn, 30, Unit.PX);
    checkColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
  }

  public ScrollPanel getScrollPanel() {
    HeaderPanel header = (HeaderPanel) getWidget();
    return (ScrollPanel) header.getContentWidget();
  }

  public void setFavoriteClickHandler(StarToggleHandler<T> h) {
    handler = h;
  }
  
  public ListDataProvider<T> getDataProvider() {
	  return dataProvider;
  }
  
  @Override
  public void attach() {
	  onAttach();
  }
}
