package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;

import edu.upenn.cis.db.mefview.client.DataLoadHandler;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanJs;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.TimeSeries;

public class FullResolutionDataHandler implements DataLoadHandler {
    /**
	 * 
	 */
	private final IEEGPresenter eegPresenter;

	/**
	 * @param eegPresenter
	 */
	public FullResolutionDataHandler(IEEGPresenter eegPresenter) {
		this.eegPresenter = eegPresenter;
	}

	public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final TimeSeries[] results, boolean isMinMax) {
      this.eegPresenter.getDisplay().addNewData(this.eegPresenter.refreshBufferFromSeries(results), true, isMinMax);
      //			refreshAnnotationsOnGraph(true);//false);
      this.eegPresenter.getDisplay().setRefreshGraph();
      this.eegPresenter.getDisplay().setRefreshAnnotations();
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final String results, boolean isMinMax) {
      JsArray<JsArrayInteger> ar = this.eegPresenter.refreshBufferFromJson(results);
      //			debug("New data " + EEGPaneData.getStartDate(ar) + " through " + EEGPaneData.getEndDate(ar));
      //			render(start, width, samplingPeriod, filter, ar);
      if (ar != null) {
        this.eegPresenter.getDisplay().addNewData(ar, true, isMinMax);
        //				refreshAnnotationsOnGraph(true);
        this.eegPresenter.getDisplay().setRefreshGraph();
        this.eegPresenter.getDisplay().setRefreshAnnotations();
      }
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final JsArray<JsArrayInteger> results, boolean isMinMax) {
      //			debug("New data " + EEGPaneData.getStartDate(ar) + " through " + EEGPaneData.getEndDate(ar));
      //			render(start, width, samplingPeriod, filter, results);
      if (results != null) {
        this.eegPresenter.getDisplay().addNewData(results, true, isMinMax);
        //				refreshAnnotationsOnGraph(true);
        this.eegPresenter.getDisplay().setRefreshGraph();
        this.eegPresenter.getDisplay().setRefreshAnnotations();
      }
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final List<TimeSeriesSpan> results, boolean isMinMax) {

      if (results.isEmpty()) {
        //				GWT.log("NULL DATA");
        return;
      }

      JsArray<JsArrayInteger> arr = ((TimeSeriesSpanJs)results.get(0)).getMaster();

      //			GWT.log("New data " + DataConversion.getUUValue(EEGPaneData.getStartDate(arr)) + 
      //					" through " + DataConversion.getUUValue(EEGPaneData.getEndDate(arr)) + " @" + samplingPeriod);
      //			render(start, width, samplingPeriod, filter, arr);
      if (arr != null) {
        //				GWT.log("Adding data to graph, marking refresh");
        this.eegPresenter.getDisplay().addNewData(arr, true, isMinMax);
        //				refreshAnnotationsOnGraph(true);
        this.eegPresenter.getDisplay().setRefreshGraph();
        this.eegPresenter.getDisplay().setRefreshAnnotations();
      }
    }
  }
