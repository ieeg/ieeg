package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.viewers.AnnotationGroupPane;

public interface IAnnotationGroupDialog  {

	public void init(AnnotationGroupPane groupPane,
		    final DataSnapshotModel currentModel,
		    final SessionModel sessionModel, 
		      final IEEGPane display, Command doAfter);
	
	public void show();
	
	public void center();
	
}