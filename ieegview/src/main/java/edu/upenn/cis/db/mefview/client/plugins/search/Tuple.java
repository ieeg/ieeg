/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.search;

import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.client.plugins.search.Schema.Type;

public class Tuple implements IsSerializable {
	Schema schema;
	
	List<IsSerializable> values;
	
	

	public Schema getSchema() {
		return schema;
	}

	public void setSchema(Schema schema) {
		this.schema = schema;
	}

	public List<IsSerializable> getValues() {
		return values;
	}

	public void setValues(List<IsSerializable> values) {
		this.values = values;
	}

	public List<Type> getAttribList() {
		return schema.getAttribList();
	}

	public void setAttribList(List<Type> attribList) {
		schema.setAttribList(attribList);
	}

	public List<String> getAttribNames() {
		return schema.getAttribNames();
	}

	public void setAttribNames(List<String> attribNames) {
		schema.setAttribNames(attribNames);
	}

	public IsSerializable get(int arg0) {
		return values.get(arg0);
	}
	
	public Object get(String column) {
		return values.get(schema.getAttribNames().indexOf(column));
	}

	public IsSerializable set(int arg0, IsSerializable arg1) {
		return values.set(arg0, arg1);
	}

	public int size() {
		return values.size();
	}

	public String getKey() {
		return schema.getKey(values);
	}

	public static final ProvidesKey<Tuple> KEY_PROVIDER = new ProvidesKey<Tuple>() {

		public Object getKey(Tuple item) {
			return (item == null) ? null : item.getKey();
		}
		
	};

	public static final TextColumn<Tuple> getColumn(final int i) {
		return  new TextColumn<Tuple> () {
			@Override
			public String getValue(Tuple object) {
				return object.getSchema().getAttribList().get(i).getFormatted(object.get(i));
			}
		};
	}
}
