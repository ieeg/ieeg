/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.upenn.cis.db.mefview.shared.places.EEGPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;

public class PlaceConverter {
	static ObjectMapper om = ObjectMapperFactory.getObjectMapper();
	
	static ObjectReader reader = om.reader(WindowPlace.class);
	static ObjectWriter writer = om.writer();
	
	
	public static PlaceSerializable getSerializableVersion(WindowPlace pl) {
//		if (pl instanceof EEGPlace) {
//			return getSerializableVersion((EEGPlace)pl);
//		} else
//			return new StringParamPlaceSerializable(pl.getType(), pl.getSerializableForm());
		try {
			return new StringParamPlaceSerializable(pl.getType(), 
					writer.writeValueAsString(pl));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new StringParamPlaceSerializable(pl.getType(), 
					"");
		}
	}
	
	public static WindowPlace getPlace(String value) throws JsonProcessingException, IOException {
		return (WindowPlace)reader.readValue(value);
	}

//	public static EEGPlaceSerializable getSerializableVersion(EEGPlace pl) {
//		EEGPlaceSerializable ret = new EEGPlaceSerializable();
//		
//		ret.setPosition(pl.getPosition());
//		ret.setWidth(pl.getWidth());
//		ret.setSnapshotID(pl.getSnapshotID());
//		ret.setScaleFactor(pl.getScaleFactor());
//		ret.setAutoScale(pl.getAutoScale());
//		
//		ret.setFilters(TraceServer.getFiltersFrom(pl.getFilters()));
//		
//		ArrayList<Boolean> channels = new ArrayList<Boolean>(pl.getChannelsEnabled().length);
//		for (boolean b: pl.getChannelsEnabled())
//			channels.add(b);
//		
//		ret.setChannelsEnabled(channels);
//		
//		return ret;
//	}

	public static WorkPlaceSerializable getSerializableVersion(WorkPlace pl) {
		WorkPlaceSerializable ret = new WorkPlaceSerializable();
		
		ret.setFavoriteStudies(pl.getFavoriteStudies());
		ret.setFavoriteTools(pl.getFavoriteTools());
		ret.setRecentStudies(pl.getRecentStudies());
		ret.setRecentTools(pl.getRecentTools());
		ret.setProject(pl.getProject());
//		ret.setWindows(pl.getWindows());
		
		List<PlaceSerializable> contexts = new ArrayList<PlaceSerializable>();
		for (WindowPlace w: pl.getContexts())
			contexts.add(getSerializableVersion(w));
		
		ret.setContexts(contexts);
		
		return ret;
	}

	public static WindowPlace getGWTVersion(PlaceSerializable p) {
		if (p instanceof EEGPlaceSerializable) {
			EEGPlaceSerializable pl = (EEGPlaceSerializable)p;
			EEGPlace ret = new EEGPlace();
			
			ret.setPosition(pl.getPosition());
			ret.setWidth(pl.getWidth());
			ret.setSnapshotID(pl.getSnapshotID());
			ret.setScaleFactor(pl.getScaleFactor());
//			ret.setAutoScale(pl.getAutoScale());
			
			ret.setFilters(TraceServer.getFiltersFromSpec(pl.getFilters()));
			
			boolean[] channels = new boolean[pl.getChannelsEnabled().size()];
			int i = 0;
			for (Boolean b: pl.getChannelsEnabled())
				channels[i++] = b;
			
			ret.setChannelsEnabled(channels);
			
			return ret;
		} else {
			StringParamPlaceSerializable pl = (StringParamPlaceSerializable)p;
			
//			return PanelFactory.getPlace(pl.getType(), pl.getParameters());
			try {
				return getPlace(pl.getParameters());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

	public static WorkPlace getGWTVersion(WorkPlaceSerializable pl) {
		WorkPlace ret = new WorkPlace();
		
		ret.setFavoriteStudies(pl.getFavoriteStudies());
		ret.setFavoriteTools(pl.getFavoriteTools());
		ret.setRecentStudies(pl.getRecentStudies());
		ret.setRecentTools(pl.getRecentTools());
		ret.setProject(pl.getProject());
//		ret.setWindows(pl.getWindows());
		
		ret.setAnnSchemes(pl.annSchemes);
		
		List<WindowPlace> contexts = new ArrayList<WindowPlace>();
		for (PlaceSerializable e: pl.getContexts())
			contexts.add(getGWTVersion(e));
		
		ret.setContexts(contexts);
		
		return ret;
	}
}
