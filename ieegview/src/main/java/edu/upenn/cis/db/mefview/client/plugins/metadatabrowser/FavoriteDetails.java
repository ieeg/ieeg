/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.BookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.UnbookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.viewers.SearchResultViewer;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

public class FavoriteDetails extends ResizingLayout implements ChildPane, BookmarkSnapshotEvent.Handler,
UnbookmarkSnapshotEvent.Handler {
	LayoutPanel snapshotsPanel = new ResizingLayout();

	SearchResultViewer snapshots;
	Label title = new Label("Favorite Snapshots");
	Button open = new Button("Open");

	ClientFactory clientFactory;

	MetadataBrowserPresenter presenter;

	public static final int MINHEIGHT = 400;
	public static final String NAME = "faves";
	final static FavoriteDetails seed = new FavoriteDetails();

	public FavoriteDetails() {
		super();
	}

	public FavoriteDetails(final ClientFactory clientFactory, MetadataBrowserPresenter presenter) {

		this.clientFactory = clientFactory;
		this.presenter = presenter;
		
		drawContent();
	}


	private void drawContent() {


		DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
		add(mainPane);
		mainPane.addNorth(title, 35);
		title.setStyleName("IeegSubHeader");
		mainPane.add(snapshotsPanel);

		addSnapshotsPanel();
		
	}

	void addSnapshotsPanel() {
		List<String> studyColNames = new ArrayList<String>();
		List<TextColumn<SearchResult>> studyColumns = new ArrayList<TextColumn<SearchResult>>();

		studyColNames.add("Name");
		studyColumns.add(SearchResult.studyColumn);

		snapshots = new SearchResultViewer(
					10, false, true, false);

		snapshots.setSelectionModel(new SingleSelectionModel<SearchResult>());
		
		snapshots.addDomHandler(new DoubleClickHandler() {

			@Override
			public void onDoubleClick(DoubleClickEvent event) {
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(
						new OpenDisplayPanel(
								getSelected(), true, EEGViewerPanel.NAME, null)));
			}}
		, DoubleClickEvent.getType());

		snapshotsPanel.add(snapshots);
	}
	
	public SearchResult getSelected() {
		if (snapshots.getSelectedItems().isEmpty())
			return null;
		
		return (snapshots.getSelectedItems().iterator().next());
	}

	public String getTitle() {
		return "Favorites";
	}

	public int getMinHeight() {
		return 800;
	}

	@Override
	public void setSelected(Set<SerializableMetadata> selectedItem) {
		if (selectedItem.size() == 1) {
			setSelected(selectedItem.iterator().next());
		}
		
	}

	@Override
	public void setSelected(SerializableMetadata selectedItem) {
		
	}

	@Override
	public ChildPane create(ClientFactory factory,
			MetadataBrowserPresenter presenter) {
		return new FavoriteDetails(factory, presenter);
	}

	@Override
	public void bind() {
		presenter.registerHandler(BookmarkSnapshotEvent.getType(), this);
		presenter.registerHandler(UnbookmarkSnapshotEvent.getType(), this);
	}

	@Override
	public void unbind() {
		presenter.releaseHandler(this);
	}

	@Override
	public void onUnbookmarkSnapshot(SearchResult snapshot) {
		snapshots.getTable().remove(snapshot);
		snapshots.refresh();
//		for (SearchResult sr: snapshots.getTable().getList())
//			snapshots.getSelectionModel().setSelected(sr, true);
	}

	@Override
	public void onBookmarkSnapshot(SearchResult snapshot) {
		snapshots.getTable().add(snapshot);
		snapshots.refresh();
//		for (SearchResult sr: snapshots.getTable().getList())
//			snapshots.getSelectionModel().setSelected(sr, true);
	}
}
