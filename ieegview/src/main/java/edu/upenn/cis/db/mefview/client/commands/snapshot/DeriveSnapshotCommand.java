package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.DialogBox;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ContextSupplier;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.dialogs.DialogCreator;
import edu.upenn.cis.db.mefview.client.dialogs.ExperimentInfo;
import edu.upenn.cis.db.mefview.client.dialogs.DialogCreator.DialogCallback;
import edu.upenn.cis.db.mefview.client.dialogs.DialogCreator.XMLDialog;
import edu.upenn.cis.db.mefview.client.events.ToolResultCreatedEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class DeriveSnapshotCommand implements Command {
	final SearchResult dataset;
	final ContextSupplier cx;
	final DialogBox waitDialog; 
	final ClientFactory clientFactory;
	final String tool; 
	final String type;
	final ToolDto toolInfo;
	final List<INamedTimeSegment> channels;
	final Optional<DisplayConfiguration> filter;
	final OnDoneHandler handler;
	boolean addAnnotations;

	public DeriveSnapshotCommand(
			final ClientFactory clientFactory,
			final ContextSupplier cx,
			final SearchResult dataset,
			final String tool, 
			final String type, 
			final ToolDto toolInfo,  
			final List<INamedTimeSegment> channels,
			boolean addAnnotations,  
			final DialogBox waitDialog,
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.cx = cx;
		this.waitDialog = waitDialog;
		this.clientFactory = clientFactory;
		this.tool = tool;
		this.type = type;
		this.toolInfo = toolInfo;
		this.channels = channels;
		this.addAnnotations = addAnnotations;
		this.filter = Optional.absent();
		this.handler = handler;
	}

	public DeriveSnapshotCommand(
			final ClientFactory clientFactory,
			final ContextSupplier cx,
			final SearchResult dataset,
			final String tool, 
			final String type, 
			final ToolDto toolInfo,  
			final List<INamedTimeSegment> channels,
			boolean addAnnotations,  
			final DisplayConfiguration filter,
			final DialogBox waitDialog,
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.cx = cx;
		this.waitDialog = waitDialog;
		this.clientFactory = clientFactory;
		this.tool = tool;
		this.type = type;
		this.toolInfo = toolInfo;
		this.channels = channels;
		this.addAnnotations = addAnnotations;
		this.filter = Optional.of(filter);
		this.handler = handler;
	}

	@Override
	public void execute() {
		final List<String> channelIDs = new ArrayList<String>();
		for (INamedTimeSegment c: channels) {
			channelIDs.add(c.getId());
		}

		clientFactory.getPortal().deriveSnapshot(
				clientFactory.getSessionModel().getSessionID(), 
				dataset.getId(), 
				type, 
				tool, 
				channelIDs, 
				addAnnotations, 
				new SecFailureAsyncCallback<SearchResult>(clientFactory) {

					public void onFailureCleanup(Throwable caught) {
						if (waitDialog != null)
							waitDialog.hide();
					}

					public void onNonSecFailure(Throwable caught) {
						Dialogs.messageBox("Error Creating Snapshot", "Sorry, there was an error creating the new snapshot.");
					}

					public void onSuccess(final SearchResult result) {
						if (waitDialog != null)
							waitDialog.hide();
						IeegEventBusFactory.getGlobalEventBus().fireEvent(
								new ToolResultCreatedEvent(
										dataset.getId(), 
										dataset.getFriendlyName(),
										new DerivedSnapshot(
												result.getDatasetRevId(), 
												type, 
												tool, 
												dataset.getId(), 
												result), 
												clientFactory.getSessionModel().getUserId()));

						if (filter.isPresent()) {
							if (toolInfo == null) {
								try {
									DialogCreator.createDefaultDialog(
											cx, 
											dataset.getId(), 
											clientFactory.getSessionModel().getUserId(), 
											toolInfo, 
											channels, 
											filter.get(), new DialogCallback() {

												@Override
												public void onClick(XMLDialog box, ClickEvent event) {

													clientFactory.getSnapshotServices().storeDataSnapshotXml(
															clientFactory.getSessionModel().getSessionID(), 
															result.getDatasetRevId(), 
															box.getXML(), 
															new SecFailureAsyncCallback.SimpleSecFailureCallback<String>(clientFactory) {

																@Override
																public void onNonSecFailure(Throwable caught) {
																	Dialogs.messageBox("Error Creating Snapshot", "Sorry, there was an error creating the new snapshot.");
																}

																@Override
																public void onSuccess(final String result) {
																	final DialogBox d = new ExperimentInfo(
																			type, 
																			result, 
																			toolInfo, 
																			clientFactory.getSessionModel().getUserId(), 
																			clientFactory);
																	d.center();
																	d.show();
																}

															});
												}

											});
								} catch (RequestException e) {
									onFailure(e);
								}
							} else {
								DialogCreator.createDialog(
										cx, 
										dataset.getId(), 
										clientFactory.getSessionModel().getUserId(), 
										toolInfo, 
										channels, 
										filter.get(), new DialogCallback() {

											@Override
											public void onClick(XMLDialog box, ClickEvent event) {

												clientFactory.getSnapshotServices().storeDataSnapshotXml(
														clientFactory.getSessionModel().getSessionID(), 
														dataset.getId(), 
														box.getXML(), 
														new SecFailureAsyncCallback.SimpleSecFailureCallback<String>(clientFactory) {

															@Override
															public void onNonSecFailure(Throwable caught) {
																Dialogs.messageBox("Error Creating Snapshot", "Sorry, there was an error creating the new snapshot.");
															}								

															@Override
															public void onSuccess(String result) {
																//									clientFactory.getEventBus().fireEvent(new LaunchedToolEvent(toolInfo));
																//									clientFactory.getEventBus().fireEvent(new JobStatusChangedEvent(result));
																final DialogBox d = new ExperimentInfo(
																		type, 
																		result, 
																		toolInfo, 
																		clientFactory.getSessionModel().getUserId(), 
																		clientFactory);
																d.center();
																d.show();
															}

														});
											}

										});
							}
						}
					}

				});
	}
}
