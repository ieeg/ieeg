package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.List;

import com.google.gwt.user.client.ui.ListBox;

import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

public interface IEditMontage extends DialogFactory.DialogWithInitializer<IEditMontage> {
	public static interface Handler {
		public void addChannelPair();
		public void updateMontage(EEGMontage montage, Boolean isNew);
		public void addSingleChannelPair();
	}
	
	public ListBox getPlotChannel();
	
	public void init(final Handler responder);
	
	public void setPane(EEGPane pane);
		
	public void open();
	
	public void close();

	public void focus();

	public void createDialog();

	public void setSources(List<INamedTimeSegment> traces);
	
	public void addChannelPair();
	
	public void addSingleChannelPair();
	
	public void setMontage(EEGMontage mt);
	
	public void reset();
}
