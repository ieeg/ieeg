/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.shared.DrugAdminMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.braintrust.shared.ContactGroupMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

public class ExperimentPane extends LayoutPanel implements ChildPane {
	private Label topLabel = new Label();
	private TextBox label = new TextBox();
	private TextBox snapshotID = new TextBox();
	private TextBox age = new TextBox();
	private TextBox species = new TextBox();
	private TextBox strain = new TextBox();
	private IntegerBox imageCount = new IntegerBox();

	private PagedTable<DrugAdminMetadata> drugAdminTable;

	private TextBox refElectrodeDescription = new TextBox();
	private PagedTable<ContactGroupMetadata> contactGroupTable;

	ScrollPanel detailsScroll = new ScrollPanel();
	FlowPanel detailFlowWrapper = new FlowPanel();
	FlowPanel detailFlow1 = new FlowPanel();
	FlowPanel detailFlow2 = new FlowPanel();
	FlowPanel detailFlow3 = new FlowPanel();

	@SuppressWarnings("unused")
	private ClientFactory clientFactory;

	private boolean showLabel = true;
	
	public ExperimentPane() {
		super();
	}

	public ExperimentPane(final ClientFactory factory) {
		clientFactory = factory;
		initializeDetailsPane(this);
	}

	public ExperimentPane(boolean showLabel, final ClientFactory factory) {
		this.showLabel = showLabel;
		clientFactory = factory;
		initializeDetailsPane(this);
	}

	private void initializeDetailsPane(LayoutPanel experimentPanel) {

		if (showLabel) {
			topLabel.setText("Dataset Details - no dataset has been selected.");
			topLabel.setStyleName("IeegSubHeader");
			detailFlowWrapper.add(topLabel);
		}

		// Create Table layout for details.
		detailFlow1.setStyleName("detailFlow");
		detailFlow2.setStyleName("detailFlow");
		detailFlow3.setStyleName("detailFlow");

		// Setup ScrollPanel with 3 detailFlow Panels
		experimentPanel.add(detailsScroll);
		detailsScroll.add(detailFlowWrapper);
		experimentPanel.setWidgetTopHeight(detailsScroll, 0, Unit.PX, 100,
				Unit.PCT);
		detailFlowWrapper.add(detailFlow1);
		detailFlowWrapper.add(detailFlow2);
		detailFlowWrapper.add(detailFlow3);

		// Setup DetailFLowPanel 1
		FlowPanel itemWrap11 = new FlowPanel();
		InlineLabel label1 = new InlineLabel("Animal Label:");
		label1.setStyleName("DetailLabel");
		itemWrap11.add(label1);
		itemWrap11.add(label);
		label.setStyleName("DetailsValue");
		label.setReadOnly(true);
		detailFlow1.add(itemWrap11);

		FlowPanel itemWrap12 = new FlowPanel();
		InlineLabel label12 = new InlineLabel("Age:");
		label12.setStyleName("DetailLabel");
		itemWrap12.add(label12);
		itemWrap12.add(age);
		age.setStyleName("DetailsValue");
		age.setReadOnly(true);
		detailFlow1.add(itemWrap12);

		FlowPanel itemWrap13 = new FlowPanel();
		InlineLabel label13 = new InlineLabel("Species:");
		label13.setStyleName("DetailLabel");
		itemWrap13.add(label13);
		itemWrap13.add(species);
		species.setStyleName("DetailsValue");
		species.setReadOnly(true);
		detailFlow1.add(itemWrap13);

		FlowPanel itemWrap14 = new FlowPanel();
		InlineLabel label14 = new InlineLabel("Strain:");
		label14.setStyleName("DetailLabel");
		itemWrap14.add(label14);
		itemWrap14.add(strain);
		strain.setStyleName("DetailsValue");
		strain.setReadOnly(true);
		detailFlow1.add(itemWrap14);

		FlowPanel itemWrap15 = new FlowPanel();
		InlineLabel label15 = new InlineLabel("Image count:");
		label15.setStyleName("DetailLabel");
		itemWrap15.add(label15);
		imageCount.setStyleName("DetailsValue");
		imageCount.setReadOnly(true);
		itemWrap15.add(imageCount);
		detailFlow1.add(itemWrap15);

		// Setup DetailFlowPanel 2
		FlowPanel itemWrap21 = new FlowPanel();
		Label label21 = new Label("Drug Admin:");
		label21.setStyleName("DetailLabel");
		itemWrap21.add(label21);
		List<String> drugAdminColNames = new ArrayList<String>();
		List<TextColumn<DrugAdminMetadata>> drugAdminColumns = new ArrayList<TextColumn<DrugAdminMetadata>>();

		drugAdminColNames.add("Drug");
		drugAdminColumns.add(DRUG_COL);
		drugAdminColNames.add("Admin Time");
		drugAdminColumns.add(ADMIN_TIME_COL);
		drugAdminColNames.add("Dose");
		drugAdminColumns.add(DOSE_COL);
		drugAdminTable = new PagedTable<DrugAdminMetadata>(
				5,
				false,
				false,
				new ArrayList<DrugAdminMetadata>(),
				DRUG_ADMIN_KEY_PROVIDER,
				drugAdminColNames,
				drugAdminColumns);
		itemWrap21.add(drugAdminTable);
		detailFlow2.add(itemWrap21);

		// Setup DetailFLowPanel 3
		FlowPanel itemWrap3 = new FlowPanel();
		InlineLabel label3 = new InlineLabel("Reference electrode:");
		label3.setStyleName("DetailLabel");
		itemWrap3.add(label3);
		itemWrap3.add(refElectrodeDescription);
		refElectrodeDescription.setStyleName("DetailsValue");
		refElectrodeDescription.setReadOnly(true);
		itemWrap3.add(refElectrodeDescription);
		detailFlow3.add(itemWrap3);

		FlowPanel contactGroupFlowPanel = new FlowPanel();
		Label contactGroupsLabel = new Label("Contact Groups:");
		contactGroupsLabel.setStyleName("DetailLabel");
		contactGroupFlowPanel.add(contactGroupsLabel);
		List<String> contactGroupColNames = new ArrayList<String>();
		List<TextColumn<ContactGroupMetadata>> contactGroupColumns = new ArrayList<TextColumn<ContactGroupMetadata>>();
		contactGroupColNames.add("Name");
		contactGroupColumns.add(ContactGroupMetadata.prefixColumn);
		contactGroupColNames.add("Side");
		contactGroupColumns.add(ContactGroupMetadata.sideColumn);
		contactGroupColNames.add("Location");
		contactGroupColumns.add(ContactGroupMetadata.locationColumn);
		contactGroupColNames.add("Rate");
		contactGroupColumns.add(ContactGroupMetadata.rateColumn);

		contactGroupTable =
				new PagedTable<ContactGroupMetadata>(
						5,
						false,
						false,
						new ArrayList<ContactGroupMetadata>(),
						ContactGroupMetadata.KEY_PROVIDER,
						contactGroupColNames, contactGroupColumns);
		contactGroupFlowPanel.add(contactGroupTable);
		detailFlow3.add(contactGroupFlowPanel);

	}

	public void setDetails(
			final String studyId, 
			final String friendlyName,
			final ExperimentMetadata experimentMetadata) {
		topLabel.setText("dataset details for " + friendlyName + ":");

		snapshotID.setText(studyId);
		label.setText(experimentMetadata.getAnimal().getLabel());
		age.setText(experimentMetadata.getAge());
		species.setText(experimentMetadata.getAnimal().getSpecies());
		strain.setText(experimentMetadata.getAnimal().getStrain());
		imageCount.setValue(experimentMetadata.getImageCount());

		drugAdminTable.clear();
		for (DrugAdminMetadata drugAdmin : experimentMetadata.getDrugAdmins()) {
			drugAdminTable.add(drugAdmin);
		}

		refElectrodeDescription.setText(experimentMetadata.getRefElectrodeDescription());

		contactGroupTable.clear();
		for (ContactGroupMetadata contactGroupMetadata : experimentMetadata
				.getContactGroups()) {
			contactGroupTable
					.add(contactGroupMetadata);
		}
	}

	public static final ProvidesKey<DrugAdminMetadata> DRUG_ADMIN_KEY_PROVIDER = new ProvidesKey<DrugAdminMetadata>() {

		public Object getKey(DrugAdminMetadata item) {
			return (item == null) ? null : item.getId();
		}

	};

	public static final TextColumn<DrugAdminMetadata> DRUG_COL = new TextColumn<DrugAdminMetadata>() {
		@Override
		public String getValue(DrugAdminMetadata object) {
			return object.getDrug();
		}
	};

	public static final TextColumn<DrugAdminMetadata> ADMIN_TIME_COL = new TextColumn<DrugAdminMetadata>() {
		@Override
		public String getValue(DrugAdminMetadata object) {
			if (object.getAdminTime() == null) {
				return "";
			}
			return DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(
					object.getAdminTime());
		}
	};

	public static final TextColumn<DrugAdminMetadata> DOSE_COL = new TextColumn<DrugAdminMetadata>() {
		@Override
		public String getValue(DrugAdminMetadata object) {
			if (object.getDoseMgPerKg() == null) {
				return "";
			}
			return object.getDoseMgPerKg().toString() + "mg/kg";
		}
	};


	@Override
	public void setSelected(Set<SerializableMetadata> selectedItem) {
		if (selectedItem.size() == 1) {
			setSelected(selectedItem.iterator().next());
		}
		
	}

	@Override
	public void setSelected(SerializableMetadata selectedItem) {
		SerializableMetadata item = (SerializableMetadata)selectedItem;
		setDetails(item.getId(), item.getLabel(), (ExperimentMetadata)selectedItem);
		
	}

	@Override
	public ChildPane create(ClientFactory factory,
			MetadataBrowserPresenter presenter) {
		return new ExperimentPane(true, factory);
	}

	@Override
	public void bind() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unbind() {
		// TODO Auto-generated method stub
		
	}
}
