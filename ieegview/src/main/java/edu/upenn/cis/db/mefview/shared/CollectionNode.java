/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.db.mefview.client.models.MetadataModel;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CollectionNode extends PresentableMetadata implements IsSerializable, JsonTyped {
		  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public static final String[] PLACEHOLDER = {"Favorites", "Recents"};

		public static Map<String, VALUE_TYPE> valueMap =
				new HashMap<String, VALUE_TYPE>();

		static {
			valueMap.put("id", VALUE_TYPE.STRING);
			valueMap.put("category", VALUE_TYPE.STRING);
			valueMap.put("count", VALUE_TYPE.INT);
		}

//		static MetadataFormatter formatter = null;
		transient MetadataPresenter notifyOnPage;
		String location = "habitat://main";
		
		int page = 0;
		PresentableMetadata parent;
		List<PresentableMetadata> metadata;
		  String category;
		  
		Multimap<String,Previewable> matches = HashMultimap.<String,Previewable>create();
//		  int count;
		
		
		@SuppressWarnings("unused")
		public CollectionNode() {
			  this.metadata = new ArrayList<PresentableMetadata>();
		}
		  
		  public CollectionNode(PresentableMetadata parent, List<PresentableMetadata> metadata, 
				  String category, int count, MetadataPresenter notifyOnPage) {
//			  GWT.log("new CollectionNode with parent " + parent + ": " + category + " and content " + metadata);
			  this.parent = parent;
			  this.metadata = new ArrayList<PresentableMetadata>();
			  addAllMetadata(metadata);
			  this.category = category;
//			  this.count = count;
			  this.notifyOnPage = notifyOnPage;
		  }

		  public CollectionNode(PresentableMetadata parent,  
				  String category, MetadataPresenter notifyOnPage) {
//			  GWT.log("new CollectionNode with parent " + parent + ": " + category + " and empty content");
			  this.parent = parent;
			  this.category = category;
			  this.metadata = new ArrayList<PresentableMetadata>();
//			  this.count = 0;
			  this.notifyOnPage = notifyOnPage;
		  }
		  
		  
		  @Override
		  public boolean isNested() {
			  return true;
		  }
		  
		  public void sort() {
			  MetadataModel.sortMetadata(metadata);
		  }

		public String getCategory() {
			if (matches.keySet().isEmpty())
				return category;
			else
				return category + "[" + matches.keySet() + "]";
		}
		
		public Multimap<String,Previewable> getTermsMatched() {
			return matches;
		}
		
		public void addTermMatched(String term, PresentableMetadata md) {
			matches.put(term, md);
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public int getCount() {
			return metadata.size();//count;
		}

//		public void setCount(int count) {
//			this.count = count;
//		}
		
		public void addAllMetadata(Collection<? extends PresentableMetadata> meta) {
			for (PresentableMetadata pm: meta)
				addMetadata(pm);
//			metadata.addAll(meta);
//			count += meta.size();
		}
		
		public void addMetadata(PresentableMetadata meta) {
			if (!metadata.contains(meta)) {
				metadata.add(meta);
//				count++;
			}
		}
		
		public void addMetadata(int inx, PresentableMetadata meta) {
			if (!metadata.contains(meta)) {
				metadata.add(inx, meta);
				
				if (meta instanceof SearchResult) {
					SearchResult sr = (SearchResult)meta;
					
					for (String key: sr.getTermsMatched().keySet())
						for (Previewable hit: sr.getTermsMatched().get(key))
							matches.put(key, hit);
				}
//				count++;
			}
		}

		public void removeMetadata(PresentableMetadata meta) {
			if (metadata.contains(meta)) {
				metadata.remove(meta);
//				count--;
			}
		}

		public void removeMetadata(int pos) {
			if (metadata.size() > pos) {
				metadata.remove(pos);
//				count--;
			}
		}
		
		@Override
		public List<PresentableMetadata> getChildMetadata() {
			return metadata;
		}
		  
		public PresentableMetadata getMetadata() {
			return metadata.get(0);
		}

		@Override
		public SerializableMetadata getParent() {
			return parent;
		}

		@Override
		public void setParent(GeneralMetadata p) {
			if (p instanceof PresentableMetadata)
				this.parent = (PresentableMetadata)p;
//			metadata = (PresentableMetadata)p;
		}

		@Override
		public String getMetadataCategory() {
			return BASIC_CATEGORIES.Collections.name();
		}

		@Override
		public String getLabel() {
			return getCategory();
		}

		@Override
		public void setId(String id) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Set<String> getKeys() {
			return valueMap.keySet();
		}

		@Override
		public String getStringValue(String key) {
			if (key.equals("id"))
				return getId();
			else if (key.equals("category"))
				return getMetadataCategory();

			return null;
		}

		@Override
		public VALUE_TYPE getValueType(String key) {
			return valueMap.get(key);
		}

		@Override
		public Double getDoubleValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Integer getIntegerValue(String key) {
			if (key.equals("count"))
				return getCount();
			return null;
		}

		@Override
		public GeneralMetadata getMetadataValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<String> getStringSetValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getId() {
			if (parent != null)
				return parent.getId() + "/" + getLabel();
			else
				return getLabel();
		}

		@Override
		public String getFriendlyName() {
			return getLabel();
		}

		@Override
		public List<String> getCreators() {
			return parent.getCreators();
		}

		@Override
		public String getContentDescriptor() {
			return getLabel();
		}

		@Override
		public Date getCreationTime() {
			return parent.getCreationTime();
		}

		@Override
		public double getRelevanceScore() {
			return parent.getRelevanceScore();
		}

		@Override
		public Map<String, Set<String>>  getUserPermissions() {
			return parent.getUserPermissions();
		}

		@Override
		public Set<String> getAssociatedDataTypes() {
			return null;
		}

//		@Override
//		public MetadataFormatter getFormatter() {
//			return formatter;
//		}
//		
//		public static void setFormatter(MetadataFormatter presenter) {
//			CollectionNode.formatter = presenter;
//		}
//
//		public static MetadataFormatter getDefaultFormatter() {
//			return formatter;
//		}
		
		

		public int getPage() {
			return page;
		}

		@Override
		public boolean isLeaf() {
			
			for (String h: PLACEHOLDER) {
				if (h.equals(getLabel()))
					return true;
			}
			return false;
		}

		@Override
		public MetadataPresenter getPresenter() {
			return notifyOnPage;
		}
		
		public void clear() {
			metadata.clear();
		}

		@Override
		public List<PresentableMetadata> getChildren() {
			return metadata;
		}

		public String getLocation() {
			return location;
		}
		
		public void setLocation(String location) {
			this.location = location;
		}

		@JsonIgnore
		@Override
		public String getObjectName() {
			return getCategory();
		}
}