/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.MenuItem;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.SecAlertCallback;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.clickhandlers.OpenSelectMontageDialogClickHandler;
import edu.upenn.cis.db.mefview.client.clickhandlers.OpenShowTracesDialogClickHandler;
import edu.upenn.cis.db.mefview.client.clickhandlers.SearchDialogClickHandler;
import edu.upenn.cis.db.mefview.client.commands.display.ShowDetailsCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowDiscussionCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowDocumentCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowImageCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.EditPermissionsCommand;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage;
import edu.upenn.cis.db.mefview.client.events.AnnotationCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRemovedEvent;
import edu.upenn.cis.db.mefview.client.events.EegChannelsChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegFilterChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageAddedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageEditedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageRemovedEvent;
import edu.upenn.cis.db.mefview.client.events.EegPositionChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegZoomChangedEvent;
import edu.upenn.cis.db.mefview.client.messages.EegPositionChangedMessage;
import edu.upenn.cis.db.mefview.client.messages.EegZoomChangedMessage;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.places.EEGPlace;

public class EEGViewerPresenter extends BasicPresenter implements 
AnnotationCreatedEvent.Handler, AnnotationRemovedEvent.Handler, IEEGViewerPresenter,
EegMontageEditedEvent.Handler, EegMontageChangedEvent.Handler,EegMontageAddedEvent.Handler, EegMontageRemovedEvent.Handler {

	public interface Display extends IEEGViewerPresenter.Display {

	};
	
  // msec to wait
  public static final int LAG = 800;
  boolean isWritable = false;

  DataSnapshotModel model;
  AppModel appModel;
  EEGPlace eegPlace;

  final static EEGViewerPresenter seed = new EEGViewerPresenter();
  public final static String NAME = "eeg";
  public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.EEG);

  static {
    try {
      PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
    } catch (PresenterExistsException e) {
      e.printStackTrace();
    }
  }

  public EEGViewerPresenter() { super(null, NAME, TYPES); }

  public EEGViewerPresenter( 
      final DataSnapshotModel model, 
      final ClientFactory clientFactory,
      final AppModel controller,
      boolean isWritable) {

    super(clientFactory, NAME, TYPES);
    this.isWritable = isWritable;
    this.model = model;
    this.appModel = controller;
  }

  public void updateRevId(final String oldId, final String newId) {
    model.updateSnapshotID(oldId, newId);
  }

  public void registerPrefetchListener(Prefetcher p) {
    getDisplay().registerPrefetchListener(p);
  }

  @Override
  public Presenter create(ClientFactory clientFactory,
      AppModel controller, DataSnapshotViews model,
      PresentableMetadata project, boolean writePermissions) {
    return new EEGViewerPresenter(model.getState(), clientFactory, 
        controller, writePermissions);
  }

  @Override
  public EEGViewerPresenter.Display getDisplay() {
    return (EEGViewerPresenter.Display)display;
  }

  @Override
  public void bind(Collection<? extends PresentableMetadata> selected) {
	  
	  GWT.log("IEEGViewerPresenter Bind");
	  
    display.addPresenter(this);
    eegPlace = new EEGPlace(model.getSnapshotID());


    if (getSessionModel().getHeaderBar() != null) {
	    addToContextMenu();
	    // Register self
	    getSessionModel().getHeaderBar().addLocalEntry((AstroPanel)getDisplay(), 
	        "search", SearchDialogClickHandler.getHandler(getClientFactory()));
	    getSessionModel().getHeaderBar().addLocalEntry((AstroPanel)getDisplay(), 
	        "save", new ClickHandler() {
	      public void onClick(ClickEvent event) {
	        getDisplay().saveAndDeleteAnnotations();
	      }
	    });
	
	    getSessionModel().getHeaderBar().addLocalEntry((AstroPanel)getDisplay(), 
	        "share", new ClickHandler() {
	      public void onClick(ClickEvent event) {
	        EditPermissionsCommand ph = 
	            new EditPermissionsCommand(
	                getClientFactory(),
	                getSessionModel().getUserId(), 
	                model.getSearchResult());
	
	        ph.execute();
	      }
	    });
    }  
    
    final OpenSelectMontageDialogClickHandler selectMontageHandler = OpenSelectMontageDialogClickHandler.getHandler(this.getClientFactory(), this);
    getDisplay().setSelectMontageDialog(selectMontageHandler.getDialog());
    
    final OpenShowTracesDialogClickHandler showTracesHandler = OpenShowTracesDialogClickHandler.getHandler(this.getClientFactory(), this);
    getDisplay().setShowTracesDialog(showTracesHandler.getDialog());
    
	getDisplay().setClickHandlers(
        new ClickHandler() {
          public void onClick(ClickEvent event) {
            getDisplay().showLayerDialog();
          }
        }, 
        showTracesHandler,
//        new ClickHandler() {
//          public void onClick(ClickEvent event) {
//            getDisplay().showTraceDialog();
//          }
//        },
        // Click on filters
        new ClickHandler() {

          public void onClick(ClickEvent event) {
            getDisplay().showFilterDialog();
          }

        },
        // Click on remontage
        selectMontageHandler,
       
        // Hit enter on new v-scale
        new NumericEnterHandler() {
          @Override
          public void handleEnterRequest(double value) {
            getDisplay().setScale(value);
            getDisplay().setFocusOnGraph();
          }

        },
        // change Montage 
        new ChangeHandler() {
          @Override
          public void onChange(ChangeEvent event) {

          }});

    getDisplay().setDataSnapshotModel(model);
    
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegMontageChangedEvent.getType(),this);
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegMontageAddedEvent.getType(), this);
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegMontageEditedEvent.getType(), this);
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegMontageRemovedEvent.getType(), this);
    IeegEventBusFactory.getGlobalEventBus().registerHandler(AnnotationCreatedEvent.getType(), this);
    IeegEventBusFactory.getGlobalEventBus().registerHandler(AnnotationRemovedEvent.getType(), this);
//    IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestAnnotationDefinitionsEvent(this, NAME));
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegChannelsChangedEvent.getType(), new EegChannelsChangedEvent.Handler() {
      
      @Override
      public void onChannelsChanged(IEEGViewerPanel w,
          List<String> channels, boolean[] isSelected) {
        if (w == (AstroPanel)getDisplay())
          eegPlace.setChannelsEnabled(isSelected);
      }

    });
    
    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegFilterChangedEvent.getType(), new EegFilterChangedEvent.Handler() {

      @Override
      public void onFilterChanged(IEEGViewerPanel window,
          List<INamedTimeSegment> traces, List<DisplayConfiguration> filters) {
        if (window == (AstroPanel)getDisplay())
          eegPlace.setFilters(filters);
      }

    });

    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegPositionChangedEvent.getType(), new EegPositionChangedEvent.Handler() {

      @Override
      public void onMove(EegPositionChangedMessage action) {
        if (action.window == (AstroPanel)getDisplay())
          eegPlace.setPosition(action.start);
      }

    });

    IeegEventBusFactory.getGlobalEventBus().registerHandler(EegZoomChangedEvent.getType(), new EegZoomChangedEvent.Handler() {

      @Override
      public void onZoom(EegZoomChangedMessage action) {
        if (action.window == (AstroPanel)getDisplay()) {
          eegPlace.setWidth(action.width);
          eegPlace.setPosition(action.start);
        }
      }

    });
      
    getDisplay().bindDomEvents();

  }

  
  
  
  @Override
  public void unbind() {
    super.unbind();
    getSessionModel().getHeaderBar().removeLocalEntries(getDisplay());
    GWT.log("Unbinding EEG viewer " + getDisplay().getWindowTitle());
    getDisplay().close();
  }

  @Override
  public void onAnnotationRemoved(DataSnapshotModel target, Annotation a) {
    if (target == model) {
      List<AnnBlock> times = model.getAnnotationModel().getAnnotationTimes();
      getDisplay().refreshAnnotations(times);
    }
  }

  @Override
  public void onAnnotationCreated(DataSnapshotModel target,
      AnnotationGroup<Annotation> group, Annotation a) {
    if (target == model) {
      List<AnnBlock> times = model.getAnnotationModel().getAnnotationTimes();
      getDisplay().refreshAnnotations(times);
    }
  }

  public EEGPlace getPlace() {
    return eegPlace;
  }
  
  private MenuItem createMenuItem(String name, Command cmd) {
    return new MenuItem(name, cmd);
  }

  private void addToContextMenu() {
    List<MenuItem> ourMenu = new ArrayList<MenuItem>();

    ourMenu.add(createMenuItem("Images",
        new ShowImageCommand(
            model.getSearchResult(), getSessionModel().getHeaderBar().getPopup())
        ));
    ourMenu.add(createMenuItem("Snapshot details",
        new ShowDetailsCommand(
            model.getSearchResult(), getSessionModel().getHeaderBar().getPopup())
        ));
    ourMenu.add(createMenuItem("Main document",
        new ShowDocumentCommand(
            model.getSearchResult(), getSessionModel().getHeaderBar().getPopup())
        ));
    ourMenu.add(createMenuItem("Discussion",
        new ShowDiscussionCommand(
            model.getSearchResult(), getSessionModel().getHeaderBar().getPopup())
        ));

    getSessionModel().getHeaderBar().addLocalMenuOptions((AstroPanel)getDisplay(), 
        ourMenu);
  }
    

  @Override
  public void onMontageAdded(IEEGViewerPanel w, final EEGMontage montage) {	  
	  
    if (!(this.getDisplay().equals(w)))
      return;
          
    // Save Montage in Database
    ClientFactory factory = this.getClientFactory();
    
    // Get Montages and populate Menu
    factory.getPortal().addMontage(getSessionModel().getSessionID(), montage, model.getSnapshotID(),
        new SecAlertCallback<EEGMontage>(factory) {

      @Override
      public void onSuccess(EEGMontage result) {

        // Add montage info to montageListMenu if it does not exist yet
        ISelectMontage popup = getDisplay().getSelectMontageDialog();
        
        popup.addMontageToList(result);

      }

    });
    
  }

  	public DataSnapshotModel getModel() {
		return model;
	}
  
	@Override
	public void onMontageRemoved(IEEGViewerPanel w, final EEGMontage toDeleteFinal) {
		if (!(this.getDisplay().equals(w)))
			return;

		// Save Montage in Database
		ClientFactory factory = this.getClientFactory();

		// Get Montages and populate Menu
		factory.getPortal().removeMontage(getSessionModel().getSessionID(), toDeleteFinal,
				new SecAlertCallback<Void>(factory) {

					@Override
					public void onSuccess(Void result) {
						ISelectMontage popup = getDisplay().getSelectMontageDialog();
						popup.removeMontageFromList(toDeleteFinal);
					}

				});

	}

	
	@Override
	public void onMontageChanged(IEEGViewerPresenter w, EEGMontage montage) {
	    
	    if (!(this.equals(w)))
	      return;
	    getDisplay().updateMontage(montage);    
	    
	}

	@Override
	public void onMontageEdited(IEEGViewerPresenter w, EEGMontage montage) {
		if (!(this.equals(w)))
			return;

		ClientFactory factory = this.getClientFactory();
		factory.getPortal().editMontage(getSessionModel().getSessionID(), montage, model.getSnapshotID(),
				new SecAlertCallback<EEGMontage>(factory) {

			@Override
			public void onSuccess(EEGMontage result) {
				GWT.log("Succesfully updated the Montage.");
			}

		});

		
		
	}
}
