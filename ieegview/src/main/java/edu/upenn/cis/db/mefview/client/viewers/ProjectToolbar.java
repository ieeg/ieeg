/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;

public class ProjectToolbar extends FlowPanel {
    Widget showEeg;
    Widget showImages;
    Widget documents;
    Widget dicoms;
    
    public ProjectToolbar() {
        this(null, null, null, null);
    }
    
    public ProjectToolbar(ClickHandler viewEegHandler,
        ClickHandler viewImageHandler, ClickHandler documentHandler, ClickHandler dicomHandler) {
      
      setStyleName("IeegTabToolbar");     


      showEeg = WidgetFactory.get().createButton("View EEG");
      add(showEeg);
      if (viewEegHandler != null)
    	  ((HasClickHandlers)showEeg).addClickHandler(viewEegHandler);
      else
    	  ((HasEnabled)showEeg).setEnabled(false);

      showImages = WidgetFactory.get().createButton("View Images");
      add(showImages);
      if (viewImageHandler != null)
        ((HasClickHandlers)showImages).addClickHandler(viewImageHandler);
      else
        ((HasEnabled)showImages).setEnabled(false);

      documents = WidgetFactory.get().createButton("View Documentation");
      add(documents);
      if (documentHandler != null)
        ((HasClickHandlers)documents).addClickHandler(documentHandler);
      else
        ((HasEnabled)documents).setEnabled(false);

      dicoms = WidgetFactory.get().createButton("Download DICOMs");
      add(dicoms);
      if (dicomHandler != null)
        ((HasClickHandlers)dicoms).addClickHandler(dicomHandler);
      else
        ((HasEnabled)dicoms).setEnabled(false);

    }
    
    public void setDicomHandler(ClickHandler dicomHandler) {
        if (dicomHandler != null) {
            ((HasClickHandlers)dicoms).addClickHandler(dicomHandler);
        } else
            ((HasEnabled)dicoms).setEnabled(false);
    }
    
    public void setDocumentHandler(ClickHandler documentHandler) {
        if (documentHandler != null) {
            ((HasClickHandlers)documents).addClickHandler(documentHandler);
        } else
            ((HasEnabled)documents).setEnabled(false);
    }
    
    public void setEegHandler(ClickHandler viewEegHandler) {
        if (viewEegHandler != null)
            ((HasClickHandlers)showEeg).addClickHandler(viewEegHandler);
        else
            ((HasEnabled)showEeg).setEnabled(false);
    }
    
    public void setImageHandler(ClickHandler viewImageHandler) {
        if (viewImageHandler != null)
            ((HasClickHandlers)showImages).addClickHandler(viewImageHandler);
        else
            ((HasEnabled)showImages).setEnabled(false);
    }
       
    public void enableImageHandler(boolean enabled) {
        ((HasEnabled)showImages).setEnabled(enabled);
    }
    
    public void enableEegHandler(boolean enabled) {
        ((HasEnabled)showEeg).setEnabled(enabled);
    }


    public HasClickHandlers getEegButton() {
        return (HasClickHandlers)showEeg;
    }

    public HasClickHandlers getImageButton() {
        return (HasClickHandlers)showImages;
    }

    public HasClickHandlers getDocumentButton() {
        return (HasClickHandlers)documents;
    }
    public HasClickHandlers getDicomButton() {
        return (HasClickHandlers)dicoms;
    }
    
    public void enableDocumentHandler(boolean enabled) {
        ((HasEnabled)documents).setEnabled(enabled);
    }

    public void enableDicomHandler(boolean enabled) {
        ((HasEnabled)dicoms).setEnabled(enabled);
    }

	public void bindDomEvents() {
		/*
			for (Widget w: Arrays.<Widget>asList(showEeg, showImages, documents, dicoms)) {
				final Widget wHere = w;
				if (wHere != null)
				Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
					
					@Override
					public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
						NativeEvent clickEvent = Converter.getNativeEvent(event);
						DomEvent.fireNativeEvent(clickEvent, wHere);
					}
				});
			}
			*/
	}


  }

  


