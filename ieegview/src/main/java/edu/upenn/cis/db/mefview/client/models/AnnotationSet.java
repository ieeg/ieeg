/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

public class AnnotationSet<T> {
		public String label;
		public String revId;
		public String onStudyRevId;
		public String parentFriendlyName;
		public List<T> annotations;
		
		public AnnotationSet() {
			label = null;
			revId = null;
			onStudyRevId = null;
			parentFriendlyName = null;
			annotations = new ArrayList<T>();
		}

		public final ProvidesKey<AnnotationSet<T>> KEY_PROVIDER = new ProvidesKey<AnnotationSet<T>>() {

			public Object getKey(AnnotationSet<T> item) {
				return (item == null) ? null : item.label + item.onStudyRevId;//.getFriendlyName();
			}
			
		};
		public final TextColumn<AnnotationSet<T>> annColumn = new TextColumn<AnnotationSet<T>> () {
			@Override
			public String getValue(AnnotationSet<T> object) {
				return object.label;
			}
		};
		public final TextColumn<AnnotationSet<T>> annCountColumn = new TextColumn<AnnotationSet<T>> () {
			@Override
			public String getValue(AnnotationSet<T> object) {
				if (object.annotations != null)
					return String.valueOf(object.annotations.size());
				else
					return "0";
			}
		};
		public final TextColumn<AnnotationSet<T>> annStudy = new TextColumn<AnnotationSet<T>> () {
			@Override
			public String getValue(AnnotationSet<T> object) {
//				if (object.onStudyRevId != null)
//					return parent.getStudyFriendlyName(object.onStudyRevId);
//				else
					return object.parentFriendlyName;
			}
		};
	}