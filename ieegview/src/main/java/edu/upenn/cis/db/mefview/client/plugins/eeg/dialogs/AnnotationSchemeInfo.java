/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.viewers.AnnotationSchemePane;

public class AnnotationSchemeInfo extends DialogBox {
  
  AnnotationSchemePane groups;
  
  public AnnotationSchemeInfo(final AnnotationSchemePane pane){
    super(false,false);
    
    groups = pane;
    setStyleName("ChanSelectDialog");
    
    VerticalPanel AnnLayerPaneWrapper = new VerticalPanel(); 
    
    setText("Annotation Layer Styles");

    setWidget(AnnLayerPaneWrapper);
    AnnLayerPaneWrapper.add(groups);
    groups.setSize("300px", "400px");
    
    Button close = new Button("Close");
    AnnLayerPaneWrapper.add(close);
    
    close.addClickHandler(new ClickHandler() {

      @Override
      public void onClick(ClickEvent event) {
          AnnotationSchemeInfo.this.hide();
          pane.updateScheme(); 
          pane.close();
      }
      
  });
  }

}
