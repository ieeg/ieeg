/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.logging.Logger;

import com.google.common.collect.Lists;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.controller.EventLogger;
import edu.upenn.cis.db.mefview.client.events.UserInfoReceivedEvent;
import edu.upenn.cis.db.mefview.client.models.AvailableSnapshotsModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.workspacesettings.WorkspacePanel;
import edu.upenn.cis.db.mefview.client.presenters.AppPresenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.UserPrefs;

public class ClientFactoryImpl implements ClientFactory {
	static class HandlerPair {
		public EventHandler object;
		public HandlerRegistration registration;
		public HandlerPair(EventHandler h, HandlerRegistration r) {
			object = h;
			registration = r;
		}
	}
	//	EventBus eventBus = null;
	PlaceController placeController = null;
	AvailableSnapshotsModel availableSnapshots = null;
	ServerAccess cbs = null;

	ProjectServicesAsync projects = null;
	SearchServicesAsync searchServer = null;
	TaskServicesAsync taskServer = null;
	SyncServicesAsync sync = null;
	
	SessionModel session = new SessionModel(this);

	EEGDisplayAsync portalServer = null;

	public ClientFactoryImpl() {
		//		eventBus = new SimpleEventBus();
		placeController = new PlaceController(
				(IeegEventBus)IeegEventBusFactory.getGlobalEventBus());
		availableSnapshots = new AvailableSnapshotsModel();
	}

	public ClientFactoryImpl(boolean dontConstructClientStuff) {
		//		eventBus = new SimpleEventBus();

	}

	@Override
	public SnapshotServicesAsync getSnapshotServices() {
		return cbs;
	}
	
	public void setSessionModel(SessionModel sess) {
		this.session = sess;
	}
	
	public SessionModel getSessionModel() {
		return session;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public EEGDisplayAsync getPortal() {
		return portalServer;
	}

	//	@Override
	//	public void setSnapshotsWindow(AstroPanel snapshots) {
	//		this.snapshots = snapshots;
	//	}
	//
	//	@Override
	//	public void setToolsWindow(AstroPanel window) {
	//		this.tools = window;
	//	}
	//
	//	@Override
	//	public void setWorkspaceWindow(WorkspacePanel window) {
	//		this.workspace = window;
	//	}

	@Override
	public ServerAccess getPortalServices() {
		return cbs;
	}

	@Override
	public ProjectServicesAsync getProjectServices() {
		return projects;
	}

	@Override
	public SearchServicesAsync getSearchServices() {
		return searchServer;
	}

	@Override
	public TaskServicesAsync getTaskServices() {
		return taskServer;
	}

	@Override
	public void setPortal(EEGDisplayAsync portal) {
		portalServer = portal;
	}

	@Override
	public void setPortalService(ServerAccess c) {
		cbs = c;
	}

	@Override
	public void setProjectServices(ProjectServicesAsync proj) {
		projects = proj;
	}

	@Override
	public void setSearchServices(SearchServicesAsync search) {
		searchServer = search;
	}

	@Override
	public void setTaskServices(TaskServicesAsync tasks) {
		taskServer = tasks;
	}

	@Override
	public SyncServicesAsync getSyncServices() {
		return sync;
	}

	@Override
	public void setSyncServices(SyncServicesAsync sync) {
		this.sync = sync;
	}
}
