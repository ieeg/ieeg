/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TabLayoutPanel;

import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;

public class SearchSnapshotPane extends TabLayoutPanel {
	ClientFactory clientFactory;
	ExperimentSearchComposite experimentSearch;
	StudySearch studySearch;

	public SearchSnapshotPane(final ClientFactory clientFactory) {
		super(240, Unit.PX);
		this.clientFactory = clientFactory;

		setTitle("Search for Studies or Experiments");

		studySearch = new StudySearch(
				clientFactory.getSessionModel().getUserId(),
				clientFactory.getSearchServices(),
				null,
				clientFactory,
				null);

		add(studySearch, "Study Search");

		clientFactory.getSearchServices().getExperimentSearchCriteria(clientFactory.getSessionModel().getSessionID(),
				new SecFailureAsyncCallback<ExperimentSearchCriteria>(clientFactory) {

					public void onFailureCleanup(Throwable caught) {
					}
					
					public void onNonSecFailure(Throwable caught) {

					}

					public void onSuccess(ExperimentSearchCriteria result) {
						experimentSearch =
								new ExperimentSearchComposite(
										clientFactory.getSessionModel().getUserId(),
										null,
										clientFactory,
										null,
										result);
						add(
								experimentSearch,
								"Experiment Search");
					}
				});
	}
}
