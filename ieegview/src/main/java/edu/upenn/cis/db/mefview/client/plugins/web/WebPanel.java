/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.web;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Frame;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.WebPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class WebPanel extends AstroTab implements WebPresenter.Display {
	Frame f = new Frame();

	HandlerRegistration hr;
	WebPlace place = new WebPlace("", datasetId);
	
	String title = "Link";
	
	public final static String NAME = "url";
	final static WebPanel seed = new WebPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public WebPanel() {
		super();
	}

	public WebPanel( 
			final DataSnapshotViews model, 
//			final ControllerState control, 
			final ClientFactory clientFactory,
//			final String user,
			final String title
			) {
		
		super(clientFactory, model,  
		    title);
		add(f);
		f.setSize("100%", "100%");
		setWidgetLeftRight(f, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(f, 1, Unit.PX, 1, Unit.PX);
	  }
	
	@Override
	public String getPanelType() {
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new WebPanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	@Override
	public WindowPlace getPlace() {
		return place;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof WebPlace)
			this.place = (WebPlace)place;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return WebPlace.getFromSerialized(params);
//	}

	@Override
	public Frame getWebFrame() {
		return f;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String t) {
		title = t;
	}

	public static String getDefaultTitle() {
		return "Link";
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	public boolean isSearchable() {
		return false;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.TOOLS;
	}

	@Override
	public void bindDomEvents() {
//		for (Widget w: Arrays.<Widget>asList(f)) {
//			final Widget wHere = w;
//			if (wHere != null)
//			Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
//				
//				@Override
//				public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
//					NativeEvent clickEvent = Converter.getNativeEvent(event);
//					DomEvent.fireNativeEvent(clickEvent, wHere);
//				}
//			});
//		}
	}
}
