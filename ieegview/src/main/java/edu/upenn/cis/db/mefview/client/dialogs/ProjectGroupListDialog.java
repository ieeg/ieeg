/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.db.mefview.client.IPermsView.IPermsPresenter;
import edu.upenn.cis.db.mefview.client.widgets.ShowMorePagerPanel;

public class ProjectGroupListDialog {

	private static ProjectGroupListDialogUiBinder uiBinder = GWT
			.create(ProjectGroupListDialogUiBinder.class);

	interface ProjectGroupListDialogUiBinder extends
			UiBinder<DialogBox, ProjectGroupListDialog> {}

	@UiField
	Button okButton;

	private final DialogBox dialogBox;
	private final CellTable<ProjectGroup> groupList;

	@UiField
	ShowMorePagerPanel pagerPanel;

	private final IPermsPresenter presenter;

	public ProjectGroupListDialog(IPermsPresenter presenter) {
		this.presenter = presenter;
		dialogBox = uiBinder.createAndBindUi(this);
		dialogBox
				.setText("Project Groups");
		// dialogBox.setWidth("400px");
		groupList = new CellTable<ProjectGroup>(20);
		groupList.setWidth("100%", false);
		groupList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		groupList
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		final SelectionModel<ProjectGroup> selectionModel = this.presenter
				.getProjectGroupListSelectionModel();
		final Column<ProjectGroup, Boolean> checkColumn = new Column<ProjectGroup, Boolean>(
				new CheckboxCell()) {

			@Override
			public Boolean getValue(ProjectGroup object) {
				return selectionModel.isSelected(
						object);
			}
		};
		groupList.addColumn(checkColumn);
		// usersList.setColumnWidth(checkColumn, 40, Unit.PX);
		final Column<ProjectGroup, String> usernameColumn = new Column<ProjectGroup, String>(
				new TextCell()) {

			@Override
			public String getValue(ProjectGroup object) {
				return object.getProjectName() + " "
						+ object.getType().name().toLowerCase();
			}
		};
		groupList.addColumn(usernameColumn);
		// usersList.setColumnWidth(usernameColumn, 100, Unit.PCT);
		pagerPanel.setDisplay(groupList);
		this.presenter.addProjectGroupListDisplay(groupList);
		groupList.setSelectionModel(selectionModel,
				DefaultSelectionEventManager
						.<ProjectGroup> createCheckboxManager());
	}

	@UiHandler({ "okButton" })
	void onClick(ClickEvent e) {
		presenter.addNewProjectAces();
		dialogBox.hide();
	}

	public void centerAndShow() {
		dialogBox.center();
		dialogBox.show();
	}

}
