/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages.requests;

import java.util.Collection;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class OpenDisplayPanel {
	public GeneralMetadata ds;
	public Collection<? extends PresentableMetadata> selected = null;
	public boolean addIt;
//	public AstroPanel.PanelType panel;
	public String panel;
	public WindowPlace winPlace;
	
	public OpenDisplayPanel(
		final GeneralMetadata ds,
		final boolean addIt,
//		final AstroPanel.PanelType panel,
		final String panel,
		final WindowPlace winPlace) {

		this.ds = ds;
		this.addIt = addIt;
		this.panel = panel;
		this.winPlace = winPlace;
	}

	public OpenDisplayPanel(
			final GeneralMetadata ds,
			final boolean addIt,
//			final AstroPanel.PanelType panel,
			final String panel,
			final Collection<? extends PresentableMetadata> restricted,
			final WindowPlace winPlace) {

			this.ds = ds;
			this.addIt = addIt;
			this.selected = restricted;
			this.panel = panel;
			this.winPlace = winPlace;
		}
}
