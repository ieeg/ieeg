/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.places.DownloadPlace;

public class DownloadAction extends PluginAction {
	
	Map<SerializableMetadata,Set<PresentableMetadata>> snapshots = new HashMap<SerializableMetadata,Set<PresentableMetadata>>();

	public static final String NAME = "Downloads";
	
	
	static {
		PluginActionFactory.register(NAME, new DownloadAction());
	}
	
	public DownloadAction() {
		super(NAME, null, null, null);
	}

	public DownloadAction(Set<PresentableMetadata> md, ClientFactory cf, Presenter pres) {
		super(NAME, cf, pres, null);
		
		// Collect all of the snapshots and the particular channels
		for (PresentableMetadata m : md) {
			if (m instanceof SearchResult && !snapshots.containsKey(m)) {
				snapshots.put(m, new HashSet<PresentableMetadata>());
			} else if (m.getParent() != null) {
				if (!snapshots.containsKey(m.getParent()))
					snapshots.put(m.getParent(),
							new HashSet<PresentableMetadata>());

				snapshots.get(m.getParent()).add(m);
			}
		}
	}

	@Override
	public void execute() {
		for (Entry<SerializableMetadata, Set<PresentableMetadata>> entry : snapshots.entrySet()) {
			SerializableMetadata key = entry.getKey();
			for (PresentableMetadata metadata : entry.getValue()) {
				if (metadata instanceof FileInfo 
						&& ((FileInfo)metadata).getMetadataCategory().equals(BASIC_CATEGORIES.Downloads.name())) {
					FileInfo download = (FileInfo)metadata;
					DownloadPlace place = new DownloadPlace(download.getId(), "./services/objects/" + 
							download.getDetails());
					OpenDisplayPanel req = new OpenDisplayPanel(
							key, true, AttachmentDownloader.NAME, null, place);
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(req));
				}
			}
			
		}
//		return null;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new DownloadAction(md, factory, pres);
	}
	
};

