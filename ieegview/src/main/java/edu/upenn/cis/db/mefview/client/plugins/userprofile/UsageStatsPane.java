/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.ProvenanceSummary;

public class UsageStatsPane extends FlowPanel implements UsageStatsPresenter.Display {
	HTML provenanceList;
	Presenter presenter;
	
	public UsageStatsPane() {
		provenanceList = new HTML("User statistics still loading");
		this.setStylePrimaryName("ieeg-stat-bucket");
		add(provenanceList);
	}

	@Override
	public void setProvenanceInfo(String displayThis) {
		provenanceList.setHTML(displayThis);
	}
	
	@Override
	public void setProvenanceInfo(ProvenanceSummary summary){
	  FlowPanel statBucket = new FlowPanel();
      
      
      HTML text1 = new HTML("You contributed:");
      statBucket.add(text1);
      
      Grid statTable = new Grid(1,4);
      statTable.setText(0, 0, Integer.toString(summary.getNumSnapshots()));
      statTable.getCellFormatter().setStylePrimaryName(0, 0, "ieeg-stat-total");
      statTable.setText(0, 1, "datasets, and ");
      statTable.setText(0, 2, Integer.toString(summary.getNumAnnotations()));
      statTable.getCellFormatter().setStylePrimaryName(0, 2, "ieeg-stat-total");
      statTable.setText(0, 3, "annotations");   
      
      Grid statTable2 = new Grid(1,2);
      statTable2.setText(0, 0, Integer.toString(summary.getSnapshotViews()));
      statTable2.getCellFormatter().setStylePrimaryName(0, 0, "ieeg-stat-total");
      statTable2.setText(0, 1, "other users viewed your datasets");

      
      statBucket.add(statTable);
      statBucket.add(statTable2);
      provenanceList.setHTML("");
      add(statBucket);
      
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}
	
	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	public void bindDomEvents() {
		// TODO Auto-generated method stub
		
	}
}
