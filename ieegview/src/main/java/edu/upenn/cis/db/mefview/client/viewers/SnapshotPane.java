/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class SnapshotPane extends LayoutPanel implements ChildPane {
	VerticalPanel elec = new VerticalPanel();

	Label topLabel = new Label();
	TextBox label = new TextBox();
	TextBox snapshotID = new TextBox();
	TextBox ageAtOnset = new TextBox();
	TextBox ageAtAdmission = new TextBox();
	TextBox gender = new TextBox();
	TextBox handedness = new TextBox();
	TextBox ethnicity = new TextBox();
	TextBox pathology = new TextBox();
	CheckBox devDisorders  = new CheckBox();
	CheckBox traumatic     = new CheckBox();
	CheckBox familyHistory = new CheckBox();
	VerticalPanel seizureInfo = new VerticalPanel();
	ListBox seizureHistory = new ListBox();
	ListBox precipitants = new ListBox();
	ListBox seizureSubtypes = new ListBox();
	private IntegerBox imageCount = new IntegerBox();
	TextBox refElectrodeDescription = new TextBox();
	PagedTable<edu.upenn.cis.braintrust.shared.ContactGroupMetadata> electrodes;
	
	ScrollPanel detailsScroll = new ScrollPanel();
	FlowPanel detailFlowWrapper = new FlowPanel();
	FlowPanel detailFlow1 = new FlowPanel();
	FlowPanel detailFlow2 = new FlowPanel();
	FlowPanel detailFlow3 = new FlowPanel();
	FlowPanel detailFlow4 = new FlowPanel();
	
	Image img = new Image();
	
	ClientFactory clientFactory;
	
	boolean showLabel = true;
	
	public SnapshotPane() {
		super();
	}
	
	public SnapshotPane(final ClientFactory factory) {
		
		clientFactory = factory;
		
		initializeDetailsPane(this);
	}
	
	public SnapshotPane(boolean showLabel, final ClientFactory factory) {
		
		this.showLabel = showLabel;
		
		clientFactory = factory;
		
		initializeDetailsPane(this);
	}


	private void initializeDetailsPane(LayoutPanel pan) {
		
		if (showLabel) {
			topLabel.setText("Dataset Details - no dataset has been selected.");
			topLabel.setStyleName("IeegSubHeader");
			detailFlowWrapper.add(topLabel);
		}
		
//		new
//		Create Table layout for details.
		detailFlow1.setStyleName("detailFlow");
		detailFlow2.setStyleName("detailFlow");
		detailFlow3.setStyleName("detailFlow");
		detailFlow4.setStyleName("detailFlow");
		
		
//		Setup ScrollPanel with 3 detailFlow Panels
        pan.add(detailsScroll);
		detailsScroll.add(detailFlowWrapper);
		pan.setWidgetTopHeight(detailsScroll, 0, Unit.PX, 100, Unit.PCT);
		detailFlowWrapper.add(detailFlow1);
	    detailFlowWrapper.add(detailFlow2);
		detailFlowWrapper.add(detailFlow3);
		detailFlowWrapper.add(detailFlow4);

//		Setup DetailFLowPanel 1
		FlowPanel itemWrap11 = new FlowPanel();
		InlineLabel label1 = new InlineLabel("Patient Label:");
		label1.setStyleName("DetailLabel");
		itemWrap11.add(label1);
		itemWrap11.add(label);
		label.setStyleName("DetailsValue");
		label.setReadOnly(true);
        detailFlow1.add(itemWrap11);
        
        FlowPanel itemWrap12 = new FlowPanel();
        InlineLabel label12 = new InlineLabel("Age at onset:");
        label12.setStyleName("DetailLabel");
        itemWrap12.add(label12);
        itemWrap12.add(ageAtOnset);
        ageAtOnset.setStyleName("DetailsValue");
        ageAtOnset.setReadOnly(true);
        detailFlow1.add(itemWrap12);
        
        FlowPanel itemWrap13 = new FlowPanel();
        InlineLabel label13 = new InlineLabel("Handedness:");
        label13.setStyleName("DetailLabel");
        itemWrap13.add(label13);
        itemWrap13.add(handedness);
        handedness.setStyleName("DetailsValue");
        handedness.setReadOnly(true);
        detailFlow1.add(itemWrap13);
        
        FlowPanel itemWrap14 = new FlowPanel();
        InlineLabel label14 = new InlineLabel("Gender:");
        label14.setStyleName("DetailLabel");
        itemWrap14.add(label14);
        itemWrap14.add(gender);
        gender.setStyleName("DetailsValue");
        gender.setReadOnly(true);
        detailFlow1.add(itemWrap14);
        
        FlowPanel itemWrap15 = new FlowPanel();
        InlineLabel label15 = new InlineLabel("Ethnicity:");
        label15.setStyleName("DetailLabel");
        itemWrap15.add(label15);
        itemWrap15.add(ethnicity);
        ethnicity.setStyleName("DetailsValue");
        ethnicity.setReadOnly(true);
        detailFlow1.add(itemWrap15);
        
        FlowPanel itemWrap23 = new FlowPanel();
        InlineLabel label23 = new InlineLabel("Seizure history:");
        label23.setStyleName("DetailLabel");
        itemWrap23.add(label23);
        seizureHistory.setVisibleItemCount(4);
        itemWrap23.add(seizureHistory);
        detailFlow1.add(itemWrap23);
        
        FlowPanel itemWrap24 = new FlowPanel();
        InlineLabel label24 = new InlineLabel("Precipitants:");
        label24.setStyleName("DetailLabel");
        itemWrap24.add(label24);
        precipitants.setVisibleItemCount(4);
        itemWrap24.add(precipitants);
        detailFlow1.add(itemWrap24);
        
        FlowPanel itemWrap16 = new FlowPanel();
        InlineLabel label16 = new InlineLabel("Developmental Disorders:");
        label16.setStyleName("DetailLabel");
        itemWrap16.add(label16);
        itemWrap16.add(devDisorders);
        devDisorders.setStyleName("DetailsValue");
        devDisorders.setEnabled(false);
        detailFlow1.add(itemWrap16);
        
        FlowPanel itemWrap17 = new FlowPanel();
        InlineLabel label17 = new InlineLabel("Traumatic Brain Injury:");
        label17.setStyleName("DetailLabel");
        itemWrap17.add(label17);
        itemWrap17.add(traumatic);
        traumatic.setStyleName("DetailsValue");
        traumatic.setEnabled(false);
        detailFlow1.add(itemWrap17);
        
        FlowPanel itemWrap18 = new FlowPanel();
        InlineLabel label18 = new InlineLabel("Fam.History of Epilepsy:");
        label18.setStyleName("DetailLabel");
        itemWrap18.add(label18);
        itemWrap18.add(familyHistory);
        familyHistory.setStyleName("DetailsValue");
        familyHistory.setEnabled(false);
        detailFlow1.add(itemWrap18);
    
//      Setup DetailFlowPanel 2
        
        FlowPanel itemWrap2 = new FlowPanel();
        InlineLabel label2 = new InlineLabel("Snapshot ID:");
        label2.setStyleName("DetailLabel");
        itemWrap2.add(label2);
        itemWrap2.add(snapshotID);
        snapshotID.setStyleName("DetailsValue");
        snapshotID.setReadOnly(true);	
        detailFlow2.add(itemWrap2);
        
        FlowPanel itemWrap22 = new FlowPanel();
        InlineLabel label22 = new InlineLabel("Age at admission:");
        label22.setStyleName("DetailLabel");
        itemWrap22.add(label22);
        itemWrap22.add(ageAtAdmission);
        ageAtAdmission.setStyleName("DetailsValue");
        ageAtAdmission.setReadOnly(true);   
        detailFlow2.add(itemWrap22);
        
        FlowPanel itemWrap25 = new FlowPanel();
        InlineLabel label25 = new InlineLabel("Seizure in Study:\n(location/etiology)");
        label25.setStyleName("DetailLabel");
        itemWrap25.add(label25);
        seizureSubtypes.setVisibleItemCount(4);
        itemWrap25.add(seizureSubtypes);
        detailFlow2.add(itemWrap25);
        
        FlowPanel itemWrap27 = new FlowPanel();
        InlineLabel label27 = new InlineLabel("Image count:");
        label27.setStyleName("DetailLabel");
        itemWrap27.add(label27);
        imageCount.setStyleName("DetailsValue");
        imageCount.setReadOnly(true);
        itemWrap27.add(imageCount);
        detailFlow2.add(itemWrap27);
        
        
//      Setup DetailFlowPanel 3
        
        FlowPanel itemWrap3 = new FlowPanel();
        InlineLabel label3 = new InlineLabel("Reference electrode:");
        label3.setStyleName("DetailLabel");
        itemWrap3.add(label3);
        itemWrap3.add(refElectrodeDescription);
        refElectrodeDescription.setStyleName("DetailsValue");
        refElectrodeDescription.setReadOnly(true);
        itemWrap3.add(refElectrodeDescription);
        detailFlow3.add(itemWrap3);
        
        FlowPanel itemWrap33 = new FlowPanel();
        Label label33 = new Label("Contact Groups:");
        label33.setStyleName("DetailLabel");
        itemWrap33.add(label33);
        List<String> electrodeColNames = new ArrayList<String>();
        List<TextColumn<edu.upenn.cis.braintrust.shared.ContactGroupMetadata>> electrodeColumns = 
        		new ArrayList<TextColumn<edu.upenn.cis.braintrust.shared.ContactGroupMetadata>>();
        electrodeColNames.add("Name");
        electrodeColumns.add(edu.upenn.cis.braintrust.shared.ContactGroupMetadata.prefixColumn);
        electrodeColNames.add("Side");
        electrodeColumns.add(edu.upenn.cis.braintrust.shared.ContactGroupMetadata.sideColumn);
        electrodeColNames.add("Location");
        electrodeColumns.add(edu.upenn.cis.braintrust.shared.ContactGroupMetadata.locationColumn);
        electrodeColNames.add("Rate");
        electrodeColumns.add(edu.upenn.cis.braintrust.shared.ContactGroupMetadata.rateColumn);

        electrodes = new PagedTable<edu.upenn.cis.braintrust.shared.ContactGroupMetadata>(5, false, false, 
                new ArrayList<edu.upenn.cis.braintrust.shared.ContactGroupMetadata>(), 
                edu.upenn.cis.braintrust.shared.ContactGroupMetadata.KEY_PROVIDER, 
                electrodeColNames, electrodeColumns);
        itemWrap33.add(electrodes);
        detailFlow3.add(itemWrap33);
        
//      Setup DetailFlowPanel 4
        FlowPanel itemWrap4 = new FlowPanel();
        Label label4 = new Label("Data condition number:");
        label4.setStyleName("DCNLabel");
        itemWrap4.add(label4);
        itemWrap4.add(new Label("(no dataset)"));
        itemWrap4.add(img);
        img.setPixelSize(0, 0);
        detailFlow4.add(itemWrap4);

	}

	public void setDetails(final String studyId, final String friendlyName, final StudyMetadata sm) {
		topLabel.setText("dataset details for " + friendlyName + ":");
		
		snapshotID.setText(studyId);
		label.setText(sm.getPatientLabel());
		ageAtOnset.setText(
				sm.getAgeAtOnset() != null
						? String.valueOf(sm.getAgeAtOnset())
						: "Unknown");
		ageAtAdmission.setText(
				sm.getAgeAtAdmission() != null
				? String.valueOf(sm.getAgeAtAdmission())
						: "Unknown");
		gender.setText(sm.getGender());
		handedness.setText(sm.getHandedness());
		ethnicity.setText(sm.getEthnicity());
		pathology.setText(sm.getEtiology());
		imageCount.setValue(sm.getImageCount());
		devDisorders.setValue(
				sm.getDevelopmentalDisorders() != null
						? sm.getDevelopmentalDisorders()
						: false);
		traumatic.setValue(
				sm.getTraumaticBrainInjury() != null
						? sm.getTraumaticBrainInjury()
						: false);
		familyHistory.setValue(
				sm.getFamilyHistory() != null
				? sm.getFamilyHistory()
						: false);
		
		final FlowPanel itemWrap4 = (FlowPanel) detailFlow4.getWidget(0);
		itemWrap4.remove(1);
		final String dcnImageUrl = sm.getDcnImageUrl();
		
		img = new Image(dcnImageUrl);
		itemWrap4.insert(img, 1);
		img.setStyleName("DCNImageSmall");
		img.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				final PopupPanel caption = new PopupPanel(false);
				
				caption.setWidget(new Image(ResourceFactory.getDcnCaption()));
				
				final DialogBox db = new DialogBox(false, true);
				
				db.setTitle("DCN plot for " + friendlyName);
				db.setText("DCN plot for " + friendlyName);
			
				VerticalPanel vp = new VerticalPanel();
				db.setWidget(vp);
				vp.add(new Image(dcnImageUrl));
				Button close = new Button("Close");
				vp.add(close);
				close.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						db.hide();
						caption.hide();
					}
					
				});
				db.center();
				db.show();
				caption.show();
			}
			
		});
		
		img.addErrorHandler(new ErrorHandler() {                
		    @Override
		    public void onError(ErrorEvent event) {
				itemWrap4.remove(1);
		        itemWrap4.insert(new Label("(not available)"), 1);
		    }
		});
		
		refElectrodeDescription.setText(sm.getRefElectrodeDescription());
		
		seizureHistory.clear();
		for (String s: sm.getSzHistory())
			seizureHistory.addItem(s);
		
		precipitants.clear();
		for (String p: sm.getPreciptants())
			precipitants.addItem(p);
		
		seizureSubtypes.clear();
		for (String s: sm.getSzSubtypes())
		  seizureSubtypes.addItem(s);
		
		electrodes.clear();
		electrodes.addAll(sm.getElectrodes());
	}

	@Override
	public void setSelected(Set<SerializableMetadata> selectedItem) {
		if (selectedItem.size() == 1) {
			setSelected(selectedItem.iterator().next());
		}
		
	}

	@Override
	public void setSelected(SerializableMetadata selectedItem) {
		SerializableMetadata item = (SerializableMetadata)selectedItem;
		setDetails(item.getId(), item.getLabel(), (StudyMetadata)item);
		
	}

	@Override
	public ChildPane create(ClientFactory factory,
			MetadataBrowserPresenter presenter) {
		return new SnapshotPane(true, factory);
	}

	@Override
	public void bind() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unbind() {
		// TODO Auto-generated method stub
		
	}
}
