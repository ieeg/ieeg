package edu.upenn.cis.db.mefview.client.commands.display;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.PopupPanel;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPanel;
import edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion.SnapshotDiscussionPanel;
import edu.upenn.cis.db.mefview.client.plugins.uploader.UploaderPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class ShowUploadCommand implements Command {
	PresentableMetadata searchResult;
	PopupPanel source;
	
	public ShowUploadCommand(PresentableMetadata md,
			PopupPanel source) {
		this.searchResult = md;
		this.source = source;
	}
	
	public void execute() {
		if (source != null)
			source.hide();
		IeegEventBusFactory.getGlobalEventBus().fireEvent(
				new OpenDisplayPanelEvent(new OpenDisplayPanel( 
				searchResult, false, 
				UploaderPanel.NAME, null)));
	}
}
