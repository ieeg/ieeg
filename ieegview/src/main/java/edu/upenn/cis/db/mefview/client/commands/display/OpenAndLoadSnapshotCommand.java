package edu.upenn.cis.db.mefview.client.commands.display;

import java.util.Collection;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.SnapshotOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter.CreateWindow;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.widgets.CollapsibleTab;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class OpenAndLoadSnapshotCommand implements Command {
	final ClientFactory clientFactory;

	final SearchResult ds;
	final DataSnapshotViews dataSnapshotViews;
	final MetadataModel metadata;
	final PanelUpdater updater;
	final String panelType;
	final WindowPlace winPlace;
	final boolean addIfExists;
	final CreateWindow win;
	final Collection<? extends PresentableMetadata> selected;
	
	public OpenAndLoadSnapshotCommand(
			ClientFactory cf,
			SearchResult dataset,
			Collection<? extends PresentableMetadata> selected,
			DataSnapshotViews dataSnapshotViews,
			MetadataModel metadata,
			final String panelType,
			final WindowPlace winPlace, 
			final boolean addIfExists,
			final CreateWindow win,
			PanelUpdater updater) {
		clientFactory = cf;
		this.ds = dataset;
		this.updater = updater;
		this.dataSnapshotViews = dataSnapshotViews;
		this.panelType = panelType;
		this.winPlace = winPlace;
		this.addIfExists = addIfExists;
		this.win = win;
		this.metadata = metadata;
		this.selected = selected;
	}
	
	@Override
	public void execute() {
		final String revID = ds.getDatasetRevId();
		final String friendlyName = ds.getFriendlyName();

		final DataSnapshotModel state = dataSnapshotViews.getState();

		clientFactory.getSnapshotServices().getSnapshotMetadata(
				clientFactory.getSessionModel().getSessionID(),
				revID,
				new SecFailureAsyncCallback.AlertCallback<SnapshotContents>(
						clientFactory,
						"There was an error while retrieving permissions.") {

					@Override
					public void onSuccess(SnapshotContents result) {
						GWT.log("Check write permitted on snapshot");
						boolean canWrite = result.getPermissions().contains(
								CorePermDefs.EDIT);

						state.setTraces(result.getTraces());
						state.setTimeOffsets(result.getTraceOffsets());
//						collectTimeseries(revID, friendlyName, result.getTraces(), state);
						// Add a viewer window
						final AstroPanel n = win.createWindow(
								dataSnapshotViews, ds, selected, canWrite,
								winPlace);
						final Presenter p = win.createPresenter(
								dataSnapshotViews, ds, selected, canWrite,
								winPlace);

						if (p != null) {
							p.setDisplay((Presenter.Display) n);
							p.bind(selected);
							GWT.log("Bound presenter " + panelType + " for "
									+ clientFactory.getSessionModel().getUserId() + "/"
									+ ds.getFriendlyName());
							updater.addPanel(
									n,
									new CollapsibleTab(win.getTitle(
											dataSnapshotViews, ds, selected, 
											canWrite), win.getIcon(
											dataSnapshotViews, ds, selected, 
											canWrite), false, true,
											new ClickHandler() {

												@Override
												public void onClick(
														ClickEvent event) {
													p.unbind();
													updater.removePanel(n);
													dataSnapshotViews.removePane(n);
												}

											}), true);
							GWT.log("Added " + panelType + " for "
									+ clientFactory.getSessionModel().getUserId() + "/"
									+ ds.getFriendlyName());

							

							dataSnapshotViews.setWritable(canWrite);
							dataSnapshotViews.addPane(n);

							ServerAccess.registerViewer(state.getSnapshotID(), n);
							
							updater.addSnapshotDescriptor(revID,
									n.getDescriptor());
							updater.addPanelDescriptor(n, n.getDescriptor());
							
							
						} //else
							collectTimeseries(revID, friendlyName, result.getTraces(), state);

						
						if (clientFactory.getSessionModel().isRegisteredUser()) {
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new ToolResultsUpdatedEvent(ds
											.getDatasetRevId(), result
											.getDerivedResults()));

							for (SearchResult s : result.getDerived()) {
								s.setBaseRevId(ds.getDatasetRevId());
								s.setBaseFriendly(ds.getFriendlyName());
								ds.addDerived(s);
								ds.addKnownChild(s);
								s.setParent(ds);
								metadata.addKnownContents(ds, s);
							}
						}
						collectFiles(revID, friendlyName, result
								.getRecordingObjects(), state);

						clientFactory.getSessionModel().getPeriodicEvents()
								.setForegroundWindow(n);
						IeegEventBusFactory.getGlobalEventBus().fireEvent(
								new SnapshotOpenedEvent(revID, state, ds));
					}

				});
	}
	
	private void collectTimeseries(
			String revID, 
			String friendlyName, 
			List<INamedTimeSegment> traces,
			DataSnapshotModel state) {
		metadata.addKnownContents(ds, traces);

		state.setTraces(traces);

		dataSnapshotViews.setChannelsAndPlace(traces, winPlace);
	}
	
	private void collectFiles(
			String revID, 
			String friendlyName, 
			Collection<RecordingObject> objects,
			DataSnapshotModel state) {
		for (RecordingObject recordingObject : objects) {
			FileInfo si = new FileInfo(recordingObject
					.getName(), revID, friendlyName,
					recordingObject.getId().toString(),
					recordingObject.getInternetMediaType());
			
			si.setParent(ds);
			state.addFile(si);
			metadata.addKnownContents(ds, si);
			
			ds.addDerived(si);
			ds.addKnownChild(si);
			clientFactory.getSessionModel().getMainLogger().info(
					"Adding file to " + friendlyName);

//			if (recordingObject.getInternetMediaType().equals(
//					"application/pdf")) {
//				clientFactory.getMainLogger()
//						.info("Adding report file to "
//								+ friendlyName);
//
//				GWT.log("Registering report");
//				metadata.addKnownContents(ds, si);
////				state.getDocuments().add(si);
//			} else {
//				clientFactory.getMainLogger().info(
//						"Adding file to " + friendlyName);
//
//				GWT.log("Registering file");
////				state.getDicoms().add(si);
//			}
		}
	}
}
