/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;

public class WebConsolePlace extends Place {
	String userid;
	String password;
//	List<PanelPlace> panelPlaces = new ArrayList<PanelPlace>();
//	List<AstroPanel> panels = new ArrayList<AstroPanel>();
	List<String> snapshots;// = new ArrayList<String>();
	
	List<String> recentSnapshots = new ArrayList<String>();
	List<String> recentTools  = new ArrayList<String>();
	
	List<String> favoriteSnapshots = new ArrayList<String>();
	List<String> favoriteTools = new ArrayList<String>();;
	
	public WebConsolePlace() {
		this.snapshots = new ArrayList<String>();
	}
	
	public WebConsolePlace(List<String> snapshots) {
		this.snapshots = snapshots;
	}
	
	public void setSnapshots(List<AstroPanel> panels) {
		snapshots.clear();
		for (AstroPanel p : panels) {
			if (p instanceof ImageViewerPanel) {
				snapshots.add("i" + ((ImageViewerPanel)p).getImagePane().getSnapshotID());
			} else if (p instanceof EEGViewerPanel) {
				snapshots.add("e" + ((EEGViewerPanel)p).getEEGPane().getStudyID());
			}
		}
	}
	
	public void addSnapshot(AstroPanel p) {
		if (p instanceof ImageViewerPanel) {
			snapshots.add("i" + ((ImageViewerPanel)p).getImagePane().getSnapshotID());
		} else if (p instanceof EEGViewerPanel) {
			snapshots.add("e" + ((EEGViewerPanel)p).getEEGPane().getStudyID());
		}
	}
	
	public void addSnapshot(String snapshotId) {
		snapshots.add(snapshotId);
	}
	public void removeSnapshot(AstroPanel p) {
		String t = null;
		if (p instanceof ImageViewerPanel) {
			t = ("i" + ((ImageViewerPanel)p).getImagePane().getSnapshotID());
		} else if (p instanceof EEGViewerPanel) {
			t = ("e" + ((EEGViewerPanel)p).getEEGPane().getStudyID());
		}
		if (t != null) {
			snapshots.remove(t);
		}
	}
	
	public List<String> getSnapshots() {
		return snapshots;
	}
	
	public String toString() {
		StringBuilder ret = new StringBuilder();
		boolean first = true;
		for (String s: snapshots) {
			if (first)
				first = false;
			else
				ret.append(';');
			ret.append(s);
			
		}
		return ret.toString();
	}
	
	public static class Tokenizer implements PlaceTokenizer<WebConsolePlace> {

		@Override
		public WebConsolePlace getPlace(String token) {
			ArrayList<String> items = new ArrayList<String>();
			
			/*
			StringTokenizer tok = new StringTokenizer(token, ";");
			while (tok.hasMoreTokens())
				items.add(tok.nextToken());
			*/
			String current = token;
			
			while (current.indexOf(';') != -1) {
				int i = current.indexOf(';');
				items.add(current.substring(0, i));
				current = current.substring(i + 1);
			}
			
			return new WebConsolePlace(items);
		}

		@Override
		public String getToken(WebConsolePlace place) {
			return place.toString();
		}
		
	}
}
