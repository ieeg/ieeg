/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.tools;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.DialogCreator.XMLDialog;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.JobPane;
import edu.upenn.cis.db.mefview.client.viewers.ToolExchangeToolbar;
import edu.upenn.cis.db.mefview.client.viewers.ToolViewer;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable.StarToggleHandler;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.client.widgets.ShowMorePagerPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class ToolBrowserPanel extends SplitLayoutPanel implements AstroPanel, RequiresResize,
ToolBrowserPresenter.Display {
	Presenter presenter;

	Set<String> openTools = new HashSet<String>();
	Set<String> recentTools = new HashSet<String>();
	
	LayoutPanel toolPanel = new ResizingLayout();
	ToolViewer toolViewer;
	ShowMorePagerPanel toolPager = new ShowMorePagerPanel();
	XMLDialog db;
	ToolExchangeToolbar toolbar ;

	AppModel control;
	ClientFactory clientFactory;

	com.google.web.bindery.event.shared.HandlerRegistration last1;
	com.google.web.bindery.event.shared.HandlerRegistration last2;

	String title;

	public static final int minHeight = 800;
	public static final String NAME = "tool-browser";
	static final ToolBrowserPanel seed = new ToolBrowserPanel();
	
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ToolBrowserPanel() {
		super();
	}
	
	public ToolBrowserPanel(final AppModel parent,
			final JobPane jobs,
			final ClientFactory factory,
			final String title
			/*int width,*/
			//int height,
			) {
		
		clientFactory = factory;
		control = parent;
		this.title = title;

		// Add Tools to ToolBrowserPanel (defined in this file)
		add(toolPanel);
		toolPanel.setStyleName("SnapshotBrowserTopPanel");
		
		/* 
		 * ToolbarWrapper can contain one or more Toolbars
		 * Toolbar is a single row of elements.
		 */
		FlowPanel toolbarWrapper = new FlowPanel();
        toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
        toolbar = new ToolExchangeToolbar();
        toolbarWrapper.add(toolbar);

        toolPanel.add(toolbarWrapper);


		toolViewer = new ToolViewer(10, 0, true, false, false);
		toolPanel.add(toolViewer);
		toolPanel.setWidgetLeftRight(toolViewer, 2, Unit.PX, 2, Unit.PX);
		toolPanel.setWidgetTopHeight(toolViewer, 35, Unit.PX, 95, Unit.PCT);

	}
	
	public List<ToolDto> getList() {
		return getToolList().getList();//getDataProvider().getList();
	}
	
	public void closeTool(final String tool) {
		if (openTools.remove(tool))
			recentTools.add(tool);
	}
	
	public void openTool(final String tool) {
		openTools.add(tool);
	}
	
	
//	@Override
//	public void updateRevId(String oldRevId, String newRevId) {
////		for (ToolDto sr: getToolList().getList()) {
////		//TODO: fix revID
//////			if (sr.getPubId().equals(oldRevId))
////			  
//////				sr.setRevId(newRevId);
////		}
//		
//	}

//	@Override
//	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void setTimestamp(double timeUutc) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void setDuration(double duration) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void setChannels(Collection<String> channel) {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void zoomBy(double ratio) {
//		// TODO Auto-generated method stub
//		
//	}

	public List<ToolDto> getToolInfo() {
		return getList();
	}

	public void addTool(ToolDto ti) {
		getToolList().add(ti);
	}

	public void removeTool(ToolDto ti) {
		getToolList().remove(ti);
	}

//	@Override
//	public void registerPrefetchListener(Prefetcher p) {
//		// TODO Auto-generated method stub
//		
//	}

	public void refresh() {
//		getToolList().setFavorites(control.getFavoriteTools());
	}

//	@Override
//	public void onResize() {
//		super.onResize();
//		toolList.setFavorites(control.getFavoriteTools());
////		setHeights(control.getLeftPaneHeight());
////		setWidths(control.getMainPaneWidth());
//		if (toolPanel.getOffsetHeight() >= 38 + 36)
//			toolList.setHeight((toolPanel.getOffsetHeight() - 38 - 36) + "px");
//		toolList.redraw();
//	}

	@Override
	public String getPanelType() {
		return NAME;// PanelType.TOOLS;
	}

//	@Override
//	public void onChangedTool(final ToolInfo tool) {
//		getToolList().setFavorites(control.getFavoriteTools());
//		getToolList().redraw();
//	}
	
	public void setFavorites(Collection<ToolDto> tools) {
		getToolList().setFavorites(tools);
	}

//	@Override
//	public void highlightMetadata(Collection<? extends PresentableMetadata> items) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public PanelDescriptor getDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlace(WindowPlace place) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DataSnapshotViews getDataSnapshotController() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
//		GWT.log("ToolBrowser.getDataSnapshotState() == null");
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public void onToolMarkedAsFavorite(ToolInfo tool) {
//		getToolList().addFavorite(tool);
//	}
//
//	@Override
//	public void onToolUnmarkedAsFavorite(ToolInfo tool) {
//		getToolList().removeFavorite(tool);
//	}

	public PagedTable<ToolDto> getToolList() {
		return toolViewer.getTable(); 
	}
	@Override
	public void close() {
//		if (last2 != null)
//			last2.removeHandler();
//		
//		if (last1 != null)
//			last1.removeHandler();
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			Project project, 
			boolean writePermissions,
			final String title) {
		return new ToolBrowserPanel(controller, null, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getTools();
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void enableAddHandler(boolean sel) {
		toolbar.enableAddHandler(sel);
	}

//	@Override
//	public void enableRunHandler(boolean sel) {
//		toolbar.enableRunHandler(sel);
//	}

	@Override
	public void enableRemoveHandler(boolean sel) {
		toolbar.enableRemoveHandler(sel);
	}

	@Override
	public void enableUpdateHandler(boolean sel) {
		toolbar.enableUpdateHandler(sel);
	}

	@Override
	public void enableBookmarkHandler(boolean sel) {
		toolbar.enableBookmarkHandler(sel);
	}

	@Override
	public void setAddHandler(ClickHandler handler) {
		toolbar.setAddHandler(handler);
	}

//	@Override
//	public void setRunHandler(ClickHandler handler) {
//		toolbar.setRunHandler(handler);
//	}

	@Override
	public void setRemoveHandler(ClickHandler handler) {
		toolbar.setRemoveHandler(handler);
	}

	@Override
	public void setUpdateHandler(ClickHandler handler) {
		toolbar.setUpdateHandler(handler);
	}

	@Override
	public void setBookmarkHandler(ClickHandler handler) {
		toolbar.setBookmarkHandler(handler);
	}

	@Override
	public void setToolSelectionChangeHandler(Handler handler) {
		toolViewer.getSelectionModel().addSelectionChangeHandler(handler);
	}

	@Override
	public ToolDto getSelectedTool() {
		return ((SingleSelectionModel<ToolDto>)toolViewer.getSelectionModel()).getSelectedObject();
	}

	@Override
	public boolean isSelectedTool(ToolDto ti) {
		return toolViewer.getSelectedItems().contains(ti);
	}

	@Override
	public List<ToolDto> getTools() {
		return toolViewer.getTable().getList();
	}

	@Override
	public void refreshTools() {
		toolViewer.getTable().redraw();
	}

	@Override
	public Set<ToolDto> getFavoriteTools() {
		return toolViewer.getTable().getFavorites();
	}

	@Override
	public void addFavoriteTool(ToolDto ti) {
		toolViewer.getTable().addFavorite(ti);
	}

	@Override
	public void removeFavoriteTool(ToolDto ti) {
		toolViewer.getTable().removeFavorite(ti);
	}

//	@Override
//	public void onToolMarkedAsFavorite(ToolInfo tool) {
//		toolViewer.getTable().addFavorite(tool);
//	}
//
//	@Override
//	public void onToolUnmarkedAsFavorite(ToolInfo tool) {
//		toolViewer.getTable().removeFavorite(tool);
//	}

	@Override
	public void setFavoriteClickHandler(StarToggleHandler<ToolDto> handler) {
		toolViewer.getTable().setFavoriteClickHandler(handler);
	}

	@Override
	public void addTools(Collection<ToolDto> ti) {
		toolViewer.getTable().addAll(ti);
	}

	@Override
	public void setFavoriteTools(Collection<ToolDto> ti) {
		toolViewer.getTable().setFavorites(ti);
	}

	public String getTitle() {
		return "Tools";
	}

	public static String getDefaultTitle() {
		return "Tools";
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		
	}

	public boolean isSearchable() {
		return true;
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}
	
	public int getMinHeight() {
      return minHeight;
    }


	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.TOOLS;
	}

	@Override
	public void bindDomEvents() {
	}
}
