/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.panels;

import java.util.Collection;
import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.Pluggable;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public interface AstroPanel extends Pluggable, Presenter.Display  {
	
//	public enum PanelType {
//		EEG, 
//		IMAGE, 
//		SEARCH,	// TODO 
//		TOOLS, 
//		STUDIES, 
//		WORKSPACE, 
//		PDF, 
//		DICOM,
//		DISCUSSION,
//		DETAILS, //TODO
//		SHARING, //TODO
//		USERPROFILE,
//		PROJECT
//		};
	
//	public String getUser();
	
	/*
	 * TODO: give an index number
	 * also add an exportLocations() and a setLocations()
	 */
	
	AstroPanel create(//final String panel,
			final ClientFactory clientFactory,
//			final String userID,
			final AppModel controller,
			final DataSnapshotViews model, 
//			final Project project,
			boolean writePermissions,
			final String title);
	
	public PanelDescriptor getDescriptor();

	public DataSnapshotViews getDataSnapshotController();
	
	public DataSnapshotModel getDataSnapshotState();
	
	/**
	 * Study just got updated -- revise its rev ID
	 * @param oldRevId
	 * @param newRevId
	 */
//	public void updateRevId(final String oldRevId, final String newRevId);
//	
//	/**
//	 * Annotations just got updated -- revise as necessary
//	 * @param newAnnotations
//	 */
//	public void refreshAnnotations(List<AnnBlock> newAnnotations);
//	
//	public void highlightMetadata(Collection<? extends PresentableMetadata> items);
	
//	public ClickHandler getSearchHandler();
//	
//	public ClickHandler getSaveHandler();
//	
//	public ClickHandler getShareHandler();
//	
//	public ClickHandler getBookmarkHandler();
	
//	public void registerPrefetchListener(ServerAccess.Prefetcher p);
	
	public void refresh();
	
	public String getPanelType();
	
	public String getPanelCategory();
	
	public ImageResource getIcon(ClientFactory clientFactory);

	public void setPlace(WindowPlace place);
	
	public WindowPlace getPlace();
	
//	public WindowPlace getPlaceFor(String params);
	
	public void close();
	
	public String getTitle(); 

//	public ClickHandler getHelpHandler();
	
	public void setDataSnapshotModel(DataSnapshotModel model);
	
//	public boolean isSearchable();
	
	public int getMinHeight();

	public Presenter getPresenter();
	
	public Widget asWidget();
}

