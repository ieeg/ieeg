package edu.upenn.cis.db.mefview.client.panels;

import java.util.Map;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.plugins.ITabSet;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface IAppPanel extends IAppPresenter.Display, IsWidget {
	public void init(final ClientFactory clientFactory);
	
	public void init(final ClientFactory clientFactory, IHeaderBar bar, 
			Map<String, ? extends ITabber> tabber, ITabSet tabset);
	
	public void bindEvents();
	
	public void setTabSelectionHandler(SelectionHandler<Integer> handler);

	public void login(UserInfo info);

	public void login(UserInfo info, String title);

//	void removeComposite(Composite w);

	public void setPanel(AstroPanel w);

	public void selectTab(Widget w);

	public int getCurrentPane();

	public void switchPane(int pane);

//	public void hideGraph();

//	public void showGraph();

	public void addTabPane(Panel panel, Widget header);

	public void addLogoutHandler(ClickHandler handler);

	public AstroPanel createPanel(AppModel controller, String panelType, PresentableMetadata target,
			DataSnapshotViews dsc, boolean isWritable, boolean isClosable, String title);

	public ITabber getTabPanel();

	public void setCloseHandler(Window.ClosingHandler closingHandler,
			CloseHandler<Window> closeHandler);

	public Element getElement();

}
