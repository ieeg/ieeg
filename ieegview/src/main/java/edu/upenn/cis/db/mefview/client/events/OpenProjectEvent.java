/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class OpenProjectEvent extends GwtEvent<OpenProjectEvent.Handler> {
	private static final Type<OpenProjectEvent.Handler> TYPE = new Type<OpenProjectEvent.Handler>();
	
	private Project project;
	
	public OpenProjectEvent(Project project){
		this.project = project;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<OpenProjectEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<OpenProjectEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OpenProjectEvent.Handler handler) {
		handler.onOpenedProject(project);
	}

	public interface Handler extends EventHandler {
		void onOpenedProject(Project project);
	}
}
