/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.task;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Task;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import gwtupload.client.IUploader;

/**
 * This is a generic template for a presenter
 * 
 * @author zives
 *
 */
public class TaskUploaderPresenter extends BasicPresenter {
	AppModel control;
	DataSnapshotModel model;
	Task task;

	/**
	 * This is the interface implemented by the AnalysisPresenterPanel.  Use
	 * this (and add methods) to change the visual display, or to add behavior
	 * like event handlers to widgets.
	 */
	public interface Display extends BasicPresenter.Display {
		public void addClickHandlerToButton(ClickHandler handler);
		public List<String> getSubmittedFiles();
		
		public WindowPlace getPlace();
		
		public void setFinishedHandler(IUploader.OnFinishUploaderHandler handler);
		public void setStartedHandler(IUploader.OnStartUploaderHandler handler);
		public void setChangeHandler(IUploader.OnChangeUploaderHandler handler);
		public String getTarget();
		public String getSnapshot();
		public void setUuid(String uuid);
		
		public void disable();
		public void clear();
		public void enable();
		void init(Task taskInfo);
	}
	
	public final static String NAME = "hw-uploader";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.PROJECT);
	final static TaskUploaderPresenter seed = new TaskUploaderPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public TaskUploaderPresenter() { super(null, NAME, TYPES); }

	public TaskUploaderPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory,
			final Task task
			) {
		super(clientFactory, NAME, TYPES);
		this.control = control;
		this.model = model;
		this.task = task;
	  }
	
	
	//////
	// The sequence of creating a panel is roughly as follows:
	//  1. The panel N is created by the PanelFactory, which is triggered with a name.
	//      Here it's "an1".
	//
	//	2. The presenter P is created, using the same name.
	//
	//  3. P.setDisplay(N) is called to associate the panel with the presenter.
	//
	//  4. P.bind() is called.  It should initialize all of the event handlers, and
	//     do any specific initialization that makes use of state.
	
	@Override
	public Display getDisplay() {
		return (Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		getDisplay().init(task);
		display.addPresenter(this);
		
		getClientFactory().getSnapshotServices().getUuid(
				new SecFailureAsyncCallback<String>(getClientFactory()){

			@Override
			public void onSuccess(String result) {
				getDisplay().setUuid(result);
				getDisplay().addClickHandlerToButton(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						getDisplay().disable();
						List<String> files = getDisplay().getSubmittedFiles();
						getClientFactory().getSnapshotServices().registerFiles(
								getDisplay().getTarget(),
								getDisplay().getSnapshot(),
								getSessionModel().getSessionID(),
								files, new SecFailureAsyncCallback<Void>(getClientFactory()) {

									@Override
									public void onSuccess(Void result) {
										getClientFactory().getSnapshotServices().getUuid(
												new SecFailureAsyncCallback<String>(getClientFactory()) {

											@Override
											public void onSuccess(String result2) {
												getDisplay().clear();
												getDisplay().setUuid(result2);
												getDisplay().enable();
											}

											@Override
											protected void onFailureCleanup(
													Throwable caught) {
												// TODO Auto-generated method stub
												
											}

											@Override
											protected void onNonSecFailure(
													Throwable caught) {
												// TODO Auto-generated method stub
												
											}
										}
										);
										
									}

									@Override
									protected void onFailureCleanup(Throwable caught) {
										// TODO Auto-generated method stub
										
									}

									@Override
									protected void onNonSecFailure(Throwable caught) {
										// TODO Auto-generated method stub
										
									}
							
						});
					}
					
				});
				getDisplay().bindDomEvents();
			}

			@Override
			protected void onFailureCleanup(Throwable caught) {
				getDisplay().enable();
			}

			@Override
			protected void onNonSecFailure(Throwable caught) {
				getDisplay().enable();
			}
			
		});
	}

	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata task, boolean writePermissions) {
		return new TaskUploaderPresenter(
				(snapController == null) ? null : snapController.getState(), 
				controller, clientFactory, (Task)task);
	}
	
}
