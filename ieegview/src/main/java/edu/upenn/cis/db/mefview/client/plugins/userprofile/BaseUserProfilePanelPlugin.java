package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.util.List;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.util.RequestHtml;

public abstract class BaseUserProfilePanelPlugin implements IUserProfilePanelPlugin {
	protected SessionModel sessionModel;
	
	public BaseUserProfilePanelPlugin(SessionModel model) {
		sessionModel = model;
	}
	
	public void addDescriptionToPanel(Panel panel) {
	      RequestHtml.getHtmlContent(
	    		  getSessionModel().getServerConfiguration().getServerName() + 
	    		  "/description.hti",
	    		  "ieeg-portal-desc", panel);

	}

	@Override
	public Panel getBanner() {
		FlowPanel userData = new FlowPanel();

		addBannerToPanel(userData);
		addDescriptionToPanel(userData);
		
		return userData;
	}

	protected abstract List<String> getBannerList();
	
	public void addBannerToPanel(FlowPanel panel) {
	      Label l = new Label("Welcome!");
	      l.setStylePrimaryName("heading");
	      panel.add(l);
	      
	      String basePath = 
	    		  RequestHtml.getPath(getSessionModel().getServerConfiguration().getServerName());

		  StringBuilder builder = new StringBuilder();
		  builder.append("<div class='bss-slides'>");
		  for (String str: getBannerList()) {
			  Image im = new Image(basePath + str);
			  im.removeStyleName("gwt-Image");
			  im.setWidth("100%");
			  builder.append("<figure>");
			  builder.append(im);
			  builder.append("</figure>");
		  }
		  builder.append("</div>");

		  HTML slideshow = new HTML(builder.toString());
		  
		  panel.add(slideshow);
		  
	}

	protected SessionModel getSessionModel() {
		return sessionModel;
	}
}
