/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.LayoutPanel;
import edu.upenn.cis.db.mefview.client.dialogs.LoginSession;
import edu.upenn.cis.db.mefview.client.plugins.ITabSet;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.shared.UserInfo;

import java.util.HashMap;
import java.util.Map;

public class WebConsoleActivity extends AbstractActivity {
	ClientFactory clientFactory;
	Place thePlace;
	LayoutPanel corePanel;
	IHeaderBar appHeader;
	Map<String, ? extends ITabber> tabPanels;
	ITabSet tabset;
	static boolean didLogin = false;
	String principal;

	private final EEGDisplayAsync eegService = GWT.create(EEGDisplay.class);

	public WebConsoleActivity(Place place, ClientFactory clientFactory, LayoutPanel thePanel) {
		this.clientFactory = clientFactory;
		thePlace = place;
		this.corePanel = thePanel;
		this.tabPanels = new HashMap<String, ITabber>();
	}
	
	public WebConsoleActivity(
			Place place,
			ClientFactory clientFactory,
			LayoutPanel thePanel,
			Map<String, ? extends ITabber> tabs,
			ITabSet tabset,
			IHeaderBar appHeader,
			String principal) {
		this.clientFactory = clientFactory;
		thePlace = place;
		this.corePanel = thePanel;
		this.appHeader = appHeader;
		this.tabPanels = tabs;
		this.tabset = tabset;
		this.principal = principal;
	}

	@Override
	public void start(AcceptsOneWidget parentPanel, EventBus eventBus) {
		GWT.log("WebConsoleActivity.start");
		final LoginSession session = new LoginSession();
		
		if (tabPanels != null) {
			corePanel.setSize("0px", "0px");
			corePanel.getElement().getStyle().setProperty("minHeight", "0px");
		}
		session.initialize(corePanel, appHeader, tabPanels, tabset, clientFactory);

		if (!didLogin) {
			didLogin = true;
			if (this.principal == null) {
				//we have not yet been logged in by the container
				GWT.log("WebConsoleActivity.start - not logged in");
				session.createDialog(tabset);
			} else {
				GWT.log("WebConsoleActivity.start - already logged in");
				eegService.getUserInfo(principal, new AsyncCallback<UserInfo>() {
					@Override
					public void onFailure(Throwable caught) {
						GWT.log("failed getUserInfo");
					}

					@Override
					public void onSuccess(UserInfo result) {
						GWT.log("setSession getUserInfo");
						session.setSession(result, tabset);
					}
				});
			}
		}
	}

	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
	
}
