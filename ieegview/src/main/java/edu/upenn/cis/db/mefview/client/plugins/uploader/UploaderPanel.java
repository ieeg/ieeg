/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.uploader;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.IUploader.OnChangeUploaderHandler;
import gwtupload.client.IUploader.OnFinishUploaderHandler;
import gwtupload.client.IUploader.OnStartUploaderHandler;


public class UploaderPanel extends AstroTab implements  
UploaderPresenter.Display {
	HabitatUploader uploader;
	TextBox box;
	
	HandlerRegistration hr;
	
	Button submit = new Button("Submit Snapshot");
	Button clear = new Button("Clear");
	ScrollPanel sp;
	String uuid;

	public final static String NAME = "uploader";
	final static UploaderPanel seed = new UploaderPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UploaderPanel() {
		super();
	}

	public UploaderPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, title);
		
		FlowPanel panel = new FlowPanel();
		add(panel);
		Label heading = new Label("Upload Files");
		panel.add(heading);
		heading.setStylePrimaryName("heading");

		panel.add(new Label("Snapshot name: "));
		box = new TextBox();
		panel.add(box);
		panel.add(new HTML("<p></p>"));
		
		Label upload = new Label("Files to upload:");
		panel.add(upload);
		panel.add(new HTML("<p></p>"));
		
		sp = new ScrollPanel();
		panel.add(sp);
		
		uploader = new HabitatUploader(new HabitatUploader.UploadTarget() {
			
			@Override
			public String getTarget() {
				return UploaderPanel.this.getTarget();
			}
		}, getSessionModel().getSessionID().getId());

		sp.add(uploader);
		uploader.setSize("100%", "100%");
		sp.setSize("50%", "300px");
		
		panel.add(new HTML("</p></p>"));
		panel.add(clear);
		panel.add(submit);
		
		clear.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				clear();
			}
			
		});
	  }
	
	void setSubmitHandler(ClickHandler handler) {
		submit.addClickHandler(handler);
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	/**
	 * This is the real constructor for the panel.  It gets called by the 
	 * PanelFactory, then calls the constructor with the appropriate
	 * parameters.
	 */
	@Override
	public UploaderPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new UploaderPanel(model, clientFactory, title);
	}

	/**
	 * This ultimately maps to a Resource that corresponds to a PNG in
	 * ...mefview.client.images.
	 */
	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	/**
	 * This is for saving state on shutdown, but can be ignored
	 */
	@Override
	public WindowPlace getPlace() {
		// TODO if we ever want to save on shutdown
		return null;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		// TODO if we ever support this
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return null;
//	}

	/**
	 * A friendly name for what our panel does
	 */
	public String getTitle() {
		return "Upload";
	}

	/**
	 * This stub would be used to attach a handler to
	 * some control widget on the panel. 
	 */
	@Override
	public void addClickHandlerToButton(ClickHandler handler) {
		submit.addClickHandler(handler);
	}
	
	public void clear() {
		uploader.clear();
		sp.remove(uploader);
		uploader = new HabitatUploader(new HabitatUploader.UploadTarget() {
			
			@Override
			public String getTarget() {
				return UploaderPanel.this.getTarget();
			}
		}, getSessionModel().getSessionID().getId());

		sp.add(uploader);
	}
	
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public void setFinishedHandler(OnFinishUploaderHandler handler) {
		uploader.addOnFinishUploadHandler(handler);
	}

	@Override
	public void setStartedHandler(OnStartUploaderHandler handler) {
		uploader.addOnStartUploadHandler(handler);
	}

	@Override
	public void setChangeHandler(OnChangeUploaderHandler handler) {
		uploader.addOnChangeUploadHandler(handler);
	}

	@Override
	public List<String> getSubmittedFiles() {
		List<String> ret = new ArrayList<String>();
		for (IUploader up: uploader.getUploaders()) {
			if (up.getStatus() == Status.SUCCESS)
				ret.add(up.getInputName());
		}
		return ret;
	}
	
	@Override
	public String getTarget() {
		return uuid;
	}

	@Override
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public String getSnapshot() {
		return box.getText();
	}

	@Override
	public void disable() {
		submit.setEnabled(false);
	}

	@Override
	public void enable() {
		submit.setEnabled(true);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}
}
