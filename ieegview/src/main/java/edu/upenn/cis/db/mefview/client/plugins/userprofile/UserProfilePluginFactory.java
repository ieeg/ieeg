package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.cis.CisUserProfile;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.ieeg.IeegUserProfile;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class UserProfilePluginFactory implements IUserProfilePluginFactory {
	static IUserProfilePanelPlugin plugin = null;
	
	public synchronized IUserProfilePanelPlugin getPlugin(ServerConfiguration config,
			SessionModel sessionModel, UserInfo info) {
		if (plugin == null) {
			GWT.log("Creating profile plugin for "+ config.getServerName());
			if (config.getServerName().equals(IeegUserProfile.NAME))
					plugin = new IeegUserProfile(sessionModel);
			else if (config.getServerName().equals(CisUserProfile.NAME))
					plugin = new CisUserProfile(sessionModel);
			else
				throw new RuntimeException("Unregistered portal profile");
		}
		
		return plugin;
	}
}
