package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.actions.ClickAction;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.presenters.AppPresenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.util.RequestHtml;
import edu.upenn.cis.db.mefview.client.widgets.PopupMenu;
import edu.upenn.cis.db.mefview.client.widgets.TooltipWrapper;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class HeaderBar extends LayoutPanel implements IHeaderBar {

	Image logo;
	PushButton uName;

	/**
	 * Names for the different widgets
	 */
	Map<String,Widget> widgets = new HashMap<String,Widget>();
	
	/**
	 * Canonical presentation order
	 */
	List<String> widgetOrder = new ArrayList<String>();
	
	/**
	 * Show the widget?
	 */
	Set<String> isEnabled = new HashSet<String>();
	
	PopupMenu menu = new PopupMenu(this);
//	MenuBar menu = new MenuBar(true);
	
	public class WidgetCommand implements IWidgetCommand {
		String name;
		ClickAction action;
		
		public WidgetCommand(String name, ClickAction action) {
			this.name = name;
			this.action = action;
		}
		
		public String getWidgetName() {
			return name;
		}
		public ClickAction getHandler() {
			return action;
		}
	}
	
	static Set<IWidgetCommand> defaultWidgets = new HashSet<IWidgetCommand>();
	
	Map<AstroPanel,Set<IWidgetCommand>> relevant = new HashMap<AstroPanel,Set<IWidgetCommand>>();
	Map<Widget,HandlerRegistration> lastHandler = new HashMap<Widget,HandlerRegistration>();

	Image pic;// = new Image("images/brain.png");
	Label programLabel = new Label("Habitat - Loading Portal Configuration");
	Label userName = new Label("");
	FlowPanel buttonHolder = new FlowPanel();
	// Controls from the last login, in case we log out and back in
	List<Widget> lastLogin = new ArrayList<Widget>();
	
	public static final String SEARCH = "search";
	public static final String SAVE = "save";
	public static final String SHARE = "share";
	public static final String HELP = "help";

	ClientFactory clientFactory;

	public HeaderBar(ClientFactory cf) {
		clientFactory = cf;
		init();
	}

	public void init() {
		// Header is simple flowPanel
		FlowPanel header = new FlowPanel();
		header.getElement().getStyle().setFloat(Float.LEFT);
		header.addStyleName("header");

		// Add Portal Logo
		logo = new Image(ResourceFactory.getSmallLogo().getSafeUri());
//

		logo.addStyleDependentName("Logo");
		header.add(logo);

		// Add IEEG-Portal Name
		programLabel.setStyleName("PortalTitle");
		header.add(programLabel);

		// Add Main Menu DIV
		buttonHolder.addStyleName("buttonHolder");

		addPushButton(SEARCH, "Search for Snapshots", ResourceFactory.getTinyMagnifier().getSafeUri());
		addPushButton(SAVE, "Save snapshot and annotations", ResourceFactory.getTinySave().getSafeUri());
		addPushButton(SHARE, "Share snapshot", ResourceFactory.getShare().getSafeUri());
		addPushButton(HELP, "Help", ResourceFactory.getHelp().getSafeUri());

		setButtons();

		// Create hamburger menu
		Image icon = new Image(ResourceFactory.getHamburger().getSafeUri());
		icon.setPixelSize(18, 18);
		icon.getElement().getStyle().setPadding(0, Unit.PX);
		final PushButton widget = new PushButton(icon);
		widget.setPixelSize(18, 18);
		new TooltipWrapper(widget, "Options");
		widget.getElement().getStyle().setFloat(Float.LEFT);

		buttonHolder.add(widget);
		widget.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (menu.isShowing())
					menu.hide();
				else
					menu.setPopupPositionAndShow(new PositionCallback() {
	
						@Override
						public void setPosition(int offsetWidth, int offsetHeight) {
							menu.setPopupPosition(
									widget.getAbsoluteLeft(), 
									widget.getAbsoluteTop() +
									widget.getOffsetHeight());
						}
						
					});
			}
			
		});

		// Combine divs 
		addStyleName("topPanel");
		add(header);
		add(buttonHolder);

		// Allow positioning of Buttonholder by CSS
		Style style = buttonHolder.getElement().getStyle();
		style.clearPosition();
		style.clearLeft();  
		initGlobals();
	}

	@Override
	public void setAppName(String newLabel) {
		programLabel.setText(newLabel);

	    logo.setUrl(UriUtils.fromString(RequestHtml.getPath(newLabel) + "logo.png"));
//	    logo.setPixelSize(72, 74);
//	    logo.setSize("72px", "74px");
		logo.addStyleDependentName("Logo");
	}

	@Override
	public void setUserInfo(UserInfo info, boolean isRegistered) {
		for (Widget w: lastLogin)
			buttonHolder.remove(w);
		lastLogin.clear();

		String photo = info.getPhoto();
		userName.setText(info.getName());

		if (!isRegistered) {
			Button login = new Button("Log In");
			login.setStylePrimaryName("ieeg-login-button");
			login.setHeight("24px");
			login.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					Window.Location.assign(//"http://www.ieeg.org" +
							GWT.getHostPageBaseURL() + "main.html");
				}

			});
			buttonHolder.add(login);
			lastLogin.add(login);
		} else
			addUserWidgets(photo);
	}

	public void addUserWidgets(String photo) {
		int off = 0;
		if (!photo.isEmpty()) {
			if (photo.startsWith("http://"))
				pic = new Image("images/brain.png");
			else
				pic = new Image(AppPresenter.URL + "/" + photo);

			pic.setPixelSize(24, 24);
			buttonHolder.add(pic);
			//       	 pic.setHeight("18px");
			pic.getElement().getStyle().setFloat(Float.LEFT);
			pic.getElement().getStyle().setMarginLeft(15, Unit.PX);
			pic.getElement().getStyle().setMarginRight(3, Unit.PX);
		} else
			off = 12;

		Image logoutImage = new Image(ResourceFactory.getLogout().getSafeUri());

		uName = new PushButton(logoutImage);//Log off " + cs.getUserID());
		uName.setStyleName("logoutButton");

		buttonHolder.add(userName);
		userName.getElement().getStyle().setFloat(Float.LEFT);
		userName.getElement().getStyle().setMarginLeft(3 + off, Unit.PX);
		userName.getElement().getStyle().setMarginRight(15, Unit.PX);
		userName.getElement().getStyle().setMarginTop(5, Unit.PX);
		userName.getElement().getStyle().setMarginBottom(3, Unit.PX);

		buttonHolder.add(uName);
		
		lastLogin.add(userName);
		lastLogin.add(uName);
	}


	public void addPushButton(String label, String bubble, String url) {
		Image image = new Image(url);
		image.setPixelSize(18, 18);
		image.getElement().getStyle().setPadding(0, Unit.PX);
		PushButton widget = new PushButton(image);
		widget.setPixelSize(18, 18);
		new TooltipWrapper(widget, bubble);
		widgets.put(label, widget);
		widgetOrder.add(label);
		widget.getElement().getStyle().setFloat(Float.LEFT);
	}

	public void addPushButton(String label, String bubble, SafeUri uri) {
		Image image = new Image(uri);
		image.setPixelSize(18, 18);
		image.getElement().getStyle().setPadding(0, Unit.PX);
		PushButton widget = new PushButton(image);
		widget.setPixelSize(18, 18);
		new TooltipWrapper(widget, bubble);
		widgets.put(label, widget);
		widgetOrder.add(label);
		widget.getElement().getStyle().setFloat(Float.LEFT);
	}

	public void setButtons() {
		int inx = 0;
		for (String str: widgetOrder) {
			if (isEnabled.contains(str))
				buttonHolder.insert(widgets.get(str), inx++);
			else
				buttonHolder.remove(widgets.get(str));
		}
	}
	
	@Override
	public void addGlobalEntry(String name, ClickHandler handler) {
		WidgetCommand newEntry = new WidgetCommand(name,
				new ClickAction("global-" + name, handler, null, null));
		
		defaultWidgets.add(newEntry);
	}
	
	@Override
	public void addLocalEntry(AstroPanel window, String name, ClickHandler handler) {
		WidgetCommand newEntry = new WidgetCommand(name,
				new ClickAction(window.getTitle() + name, handler, null, null));
	
		if (!relevant.containsKey(window))
			relevant.put(window, new HashSet<IWidgetCommand>());
		
		relevant.get(window).add(newEntry);
	}
	
	@Override
	public void removeLocalEntries(AstroPanel window) {
		relevant.remove(window);
		locals.remove(window);
	}
	
	public void initGlobals() {
	}
	
	public void setDefaults() {
		// Unregister active click handlers!
		for (Widget w: lastHandler.keySet()) { //isEnabled)
//			if (lastHandler.containsKey(str)) {
				lastHandler.get(w).removeHandler();
			}
		lastHandler.clear();
		isEnabled.clear();
		
		for (IWidgetCommand wc: defaultWidgets) {
			setHandler(wc);
		}
	}

	/**
	 * Changes the click handler based on a widget command
	 *  
	 * @param wc WidgetCommand entry
	 */
	private void setHandler(IWidgetCommand wc) {
		String name = wc.getWidgetName();
		isEnabled.add(name);
			
		if (widgets.get(name) != null && widgets.get(name) instanceof HasClickHandlers) {
			HasClickHandlers hc = (HasClickHandlers)widgets.get(name);
			
			if (lastHandler.get(widgets.get(name)) != null) {
				lastHandler.get(widgets.get(name)).removeHandler();
				lastHandler.remove(widgets.get(name));
			}
			
			lastHandler.put(widgets.get(name), 
					hc.addClickHandler(wc.getHandler().getClickHandler()));
		}
	}

	@Override
	public void setActivePanel(AstroPanel panel) {
		setDefaults();
		
		// Enable exactly the set of buttons that are relevant
		if (relevant.containsKey(panel))
			for (IWidgetCommand wc: relevant.get(panel)) {
				setHandler(wc);
			}
		setButtons();
		setMenu(panel);
	}

	@Override
	public void addLogoHandler(ClickHandler handler) {
        // Add ClickHandlers LOGO and NAME
        logo.addClickHandler(handler);
	}

	@Override
	public void addTitleHandler(ClickHandler handler) {
        programLabel.addClickHandler(handler);

	}

	@Override
	public void addLogoutHandler(ClickHandler handler) {
		if (uName != null)
			uName.addClickHandler(handler);
	}
	
	List<MenuItem> globals = new ArrayList<MenuItem>();
	Map<AstroPanel,List<MenuItem>> locals = new HashMap<AstroPanel,List<MenuItem>>();
	
	@Override
	public void addGlobalMenuOption(MenuItem item) {
		globals.add(item);
	}
	
	@Override
	public void addLocalMenuOptions(AstroPanel w, List<MenuItem> items) {
		if (!locals.containsKey(w))
			locals.put(w, new ArrayList<MenuItem>());
		
		locals.get(w).addAll(items);
	}
	
	public void setMenu(AstroPanel w) {
		menu.clearItems();
		
		for (MenuItem item: globals)
			menu.addItem(item);
		
		menu.addSeparator();
		
		if (locals.containsKey(w))
			for (MenuItem item: locals.get(w))
				menu.addItem(item);
	}

	@Override
	public PopupPanel getPopup() {
		return menu;
	}
}
