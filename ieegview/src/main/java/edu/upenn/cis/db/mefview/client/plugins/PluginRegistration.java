/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.actions.CreateProjectAction;
import edu.upenn.cis.db.mefview.client.actions.DownloadAction;
import edu.upenn.cis.db.mefview.client.actions.OpenEEGAction;
//import edu.upenn.cis.db.mefview.client.actions.OpenExperimentAction;
import edu.upenn.cis.db.mefview.client.actions.OpenImageAction;
import edu.upenn.cis.db.mefview.client.actions.OpenPDFAction;
import edu.upenn.cis.db.mefview.client.actions.OpenProjectAction;
import edu.upenn.cis.db.mefview.client.actions.OpenTextAction;
import edu.upenn.cis.db.mefview.client.actions.OpenURLAction;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.plugins.habitat.RecruitmentPanel;
import edu.upenn.cis.db.mefview.client.plugins.habitat.RecruitmentPresenter;
import edu.upenn.cis.db.mefview.client.plugins.images.MultiViewImagePanel;
import edu.upenn.cis.db.mefview.client.plugins.images.MultiViewImagePresenter;
import edu.upenn.cis.db.mefview.client.plugins.images.SimpleImagePanel;
import edu.upenn.cis.db.mefview.client.plugins.images.SimpleImagePresenter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.FavoriteDetails;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPanel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFPresenter;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.projectsettings.ProjectPanel;
import edu.upenn.cis.db.mefview.client.plugins.projectsettings.ProjectPresenter;
import edu.upenn.cis.db.mefview.client.plugins.sample.TemplatePanel;
import edu.upenn.cis.db.mefview.client.plugins.sample.TemplatePresenter;
import edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion.SnapshotDiscussionPanel;
import edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion.SnapshotDiscussionPresenter;
import edu.upenn.cis.db.mefview.client.plugins.texteditor.TextEditorPanel;
import edu.upenn.cis.db.mefview.client.plugins.texteditor.TextEditorPresenter;
import edu.upenn.cis.db.mefview.client.plugins.tools.ToolBrowserPanel;
import edu.upenn.cis.db.mefview.client.plugins.tools.ToolBrowserPresenter;
import edu.upenn.cis.db.mefview.client.plugins.uploader.UploadToTranscoderPanel;
import edu.upenn.cis.db.mefview.client.plugins.uploader.UploadToTranscoderPresenter;
import edu.upenn.cis.db.mefview.client.plugins.uploader.UploaderPanel;
import edu.upenn.cis.db.mefview.client.plugins.uploader.UploaderPresenter;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount.CreateAccountPanel;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount.CreateAccountPresenter;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.PasswordResetPanel;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.PasswordResetPresenter;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.UserProfilePanel;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.UserProfilePresenter;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPresenter;
import edu.upenn.cis.db.mefview.client.plugins.workspacesettings.WorkspacePanel;
import edu.upenn.cis.db.mefview.client.plugins.workspacesettings.WorkspacePresenter;
import edu.upenn.cis.db.mefview.client.util.MimeTypes;
import edu.upenn.cis.db.mefview.client.viewers.ExperimentPane;
import edu.upenn.cis.db.mefview.client.viewers.ProjectDetails;
import edu.upenn.cis.db.mefview.client.viewers.SnapshotDetailsPane;
import edu.upenn.cis.db.mefview.client.viewers.SnapshotPane;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

/**
 * Initialization for registering all plugins
 * 
 * @author zives
 *
 */
public class PluginRegistration implements IPluginRegistration {
	public void registerActions() {
		new OpenEEGAction();
		new OpenImageAction();
		new OpenPDFAction();
		new DownloadAction();
		new OpenProjectAction();
		new OpenURLAction();
		new CreateProjectAction();
		new OpenTextAction();
//		new OpenExperimentAction();
	}

	static Map<Class<?>,Set<ChildPane>> typeMap;
	
	public Map<Class<?>,Set<ChildPane>> getTypeMap() {
		if (typeMap == null) {
			typeMap = new HashMap<Class<?>,Set<ChildPane>>();
			
			typeMap.put(Project.class, new HashSet<ChildPane>());
			typeMap.get(Project.class).add(new ProjectDetails());
			
			typeMap.put(CollectionNode.class, new HashSet<ChildPane>());
			typeMap.get(CollectionNode.class).add(new FavoriteDetails());
			
			typeMap.put(SearchResult.class, new HashSet<ChildPane>());
			typeMap.get(SearchResult.class).add(new SnapshotDetailsPane());

			typeMap.put(StudyMetadata.class, new HashSet<ChildPane>());
			typeMap.get(StudyMetadata.class).add(new SnapshotPane());

			typeMap.put(ExperimentMetadata.class, new HashSet<ChildPane>());
			typeMap.get(ExperimentMetadata.class).add(new ExperimentPane());
		}

		return typeMap;
	}
	
	/**
	 * Set up which presenters 
	 */
	public void bindActionsToDataTypes() {
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Timeseries.name(), 
				OpenEEGAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Snapshot.name(), 
				OpenEEGAction.NAME);

		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Images.name(), 
				OpenImageAction.NAME);

		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.ImageStack.name(), 
				OpenImageAction.NAME);
		
		for (String type: MimeTypes.getImageTypes())
			ActionMapper.getMapper().registerMetadataAction(type, OpenImageAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Documents.name(), 
				OpenPDFAction.NAME);

		ActionMapper.getMapper().registerMetadataAction("application/pdf", 
				OpenPDFAction.NAME);

//		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Documents.name(), 
//				OpenTextAction.NAME);
		
		for (String type: MimeTypes.getTextTypes())
			ActionMapper.getMapper().registerMetadataAction(type, OpenTextAction.NAME);

		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Downloads.name(), 
				DownloadAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction("application/zip", 
				DownloadAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction("application/mat", 
            DownloadAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction("application/xml", 
	            DownloadAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction("application/dicom", 
				OpenImageAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Collections.name(),
				CreateProjectAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Projects.name(),
				CreateProjectAction.NAME);
		
		ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Projects.name(),
				OpenProjectAction.NAME);
	}
	
	/**
	 * These are the panels that are opened for everyone
	 * 
	 * @return
	 */
	public List<String> getDefaultPanels() {
		return Arrays.asList(new String[] {
				UserProfilePanel.NAME,
				MetadataBrowserPanel.NAME
//				CreateAccountPresenter.NAME
//				NewsPresenter.NAME
//				RecruitmentPresenter.NAME
			});
	}
	
	/**
	 * These are the panels that are opened for logged-in users
	 * 
	 * @return
	 */
	public List<String> getLoggedInPanels() {
		return Arrays.asList(new String[] {
				//MetadataBrowserPanel.NAME
//				TextEditorPresenter.NAME
//				UploaderPresenter.NAME,
//				CreateAccountPresenter.NAME,
//				PasswordResetPresenter.NAME
			});
	}
	
	/**
	 * Call constructors for each panel + presenter, to register them with the factory
	 */
	public void registerPanels() {
			new EEGViewerPresenter();
			new EEGViewerPanel();

//			new ImageViewerPresenter();
//			new ImageViewerPanel();
			
			new SimpleImagePanel();
			new SimpleImagePresenter();

			new SnapshotDiscussionPresenter();
			new SnapshotDiscussionPanel();

			new PDFPresenter();
			new PDFViewerPanel();

			new ProjectPresenter();
			new ProjectPanel();

			new MetadataBrowserPresenter();
			new MetadataBrowserPanel();

			//			new SnapshotInfoPresenter();
//			new SnapshotInfoPanel();

			new ToolBrowserPresenter();
			new ToolBrowserPanel();

			new UserProfilePresenter();
			new UserProfilePanel();

			new WorkspacePresenter();
			new WorkspacePanel();

			new TemplatePresenter();
			new TemplatePanel();
			
			new WebPresenter();
			new WebPanel();
			
			new RecruitmentPresenter();
			new RecruitmentPanel();
			
			new UploaderPresenter();
			new UploaderPanel();
			
			new UploadToTranscoderPresenter();
			new UploadToTranscoderPanel();
			
			new PasswordResetPresenter();
			new PasswordResetPanel();
			
			new CreateAccountPresenter();
			new CreateAccountPanel();
			
			new TextEditorPresenter();
			new TextEditorPanel();

			//			new NewsPresenter();
//			new NewsPanel();

			new AttachmentDownloader();
			
			new MultiViewImagePresenter();
			new MultiViewImagePanel();
	}

	@Override
	public List<String> getNotLoggedInPanels() {
		return Arrays.asList(new String[] {
		});
	}
}
