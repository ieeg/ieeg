package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Frame;

import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;

public interface IDownloadAccept extends DialogWithInitializer<IDownloadAccept> {
	public boolean isPreferredToFrame();
	
	public void hide();
	
	public void show(String titleText, String bodyText, final String url);
	
	public Element getFrame();
	
	public void initialize(DialogInitializer<IDownloadAccept> initFn);

	public void reinitialize(DialogInitializer<IDownloadAccept> initFn);

}
