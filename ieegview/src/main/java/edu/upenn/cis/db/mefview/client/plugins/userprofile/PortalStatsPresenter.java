/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.gwt.maps.client.LoadApi;
import com.google.gwt.maps.client.LoadApi.LoadLibrary;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.SecAlertCallback;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.PortalSummary;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class PortalStatsPresenter implements Presenter {
	public interface Display extends Presenter.Display {
		void setPortalInfo(String displayThis);

		void setPortalInfo(PortalSummary summary, boolean isDatasets);

		void draw(List<LatLon> result);
	}

	Display display;
	ClientFactory clientFactory;
	private final String googleMapsApiKey;

	public PortalStatsPresenter(ClientFactory cf) {
		clientFactory = cf;
		googleMapsApiKey = cf.getSessionModel().getServerConfiguration()
				.getOtherParameters().get("googleMapsApiKey");
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project,
			boolean writePermissions) {
		return new PortalStatsPresenter(clientFactory);
	}

	@Override
	public void setDisplay(Presenter.Display display) {
		this.display = (PortalStatsPresenter.Display) display;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);

		clientFactory.getPortal().getPortalSummary(
				getSessionModel().getSessionID(),
				new SecAlertCallback<PortalSummary>(clientFactory) {

					@Override
					public void onSuccess(PortalSummary summary) {

						// TODO: remove ultimately.
						// These are here so test versions of the portal
						// show numbers at least as good as the public
						// IEEG.org
						if (summary.getNumWorldReadableStudies() < 493)
							summary.setNumWorldReadableStudies(493);

						if (summary.getNumUsers() < 791)
							summary.setNumUsers(791);

						if (summary.getNumUserReadableExperiments() < 685)
							summary.setNumUserReadableExperiments(685);

						if (summary.getNumUserReadableStudies() < 756)
							summary.setNumUserReadableStudies(756);

						display.setPortalInfo(summary,
								!getSessionModel().getServerConfiguration()
										.isUniversity());
					}

				});

		loadMapApi();
	}

	@Override
	public void unbind() {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<String> getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	private void loadMapApi() {
		boolean sensor = false;

		// load all the libs for use in the maps
		ArrayList<LoadLibrary> loadLibraries = new ArrayList<LoadApi.LoadLibrary>();
		// loadLibraries.add(LoadLibrary.ADSENSE);
		loadLibraries.add(LoadLibrary.DRAWING);
		loadLibraries.add(LoadLibrary.GEOMETRY);
		// loadLibraries.add(LoadLibrary.PANORAMIO);
		loadLibraries.add(LoadLibrary.PLACES);
		// loadLibraries.add(LoadLibrary.WEATHER);
		loadLibraries.add(LoadLibrary.VISUALIZATION);

		Runnable onLoad = new Runnable() {
			@Override
			public void run() {
				// GWT.log("NEWS running");
				clientFactory.getSearchServices().getUserLocations(
						new AsyncCallback<List<LatLon>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onSuccess(List<LatLon> result) {
								display.draw(result);
							}

						});

			}
		};
		if (googleMapsApiKey != null) {
			final String otherParams = "key=" + googleMapsApiKey;
			LoadApi.go(onLoad, loadLibraries, sensor,
					otherParams);
		} else {
			LoadApi.go(onLoad, loadLibraries, sensor);
		}
	}

	protected SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
}
