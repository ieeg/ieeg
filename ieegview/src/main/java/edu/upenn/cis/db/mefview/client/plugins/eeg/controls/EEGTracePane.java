/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.controls;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.client.widgets.ResizableVerticalPanel;
import edu.upenn.cis.db.mefview.shared.SeriesData;

public class EEGTracePane extends ResizableVerticalPanel implements RequiresResize {
	public static final int BAR = 40;

	DataSnapshotModel model;
	Button viewSeries;
	Button dlCSV;
	Button dlJson;
	List<SeriesData> series = new ArrayList<SeriesData>();
	PagedTable<SeriesData> seriesList;
	
	
	public EEGTracePane(ClientFactory factory, DataSnapshotModel model) {
		initControls();
	}
	
	public void initControls() {
	    setStylePrimaryName("gwt-StackLayoutPanel gwt-StackLayoutPanelContent-managePanel");
		setHeight("100%");

		SeriesData.seriesColumn.setSortable(true);
		SeriesData.studyColumn.setSortable(true);
		
		List<String> seriesColNames = new ArrayList<String>();
		List<TextColumn<SeriesData>> seriesColumns = new ArrayList<TextColumn<SeriesData>>();
		
		seriesColNames.add("Series");
		seriesColumns.add(SeriesData.seriesColumn);
		seriesColNames.add("Snapshot");
		seriesColumns.add(SeriesData.studyColumn);

		seriesList = new PagedTable<SeriesData>(getNumRows(), true, false, series, 
				SeriesData.KEY_PROVIDER, seriesColNames, seriesColumns);
		seriesList.addStyleDependentName("listContent");
		
	    seriesList.addColumnSort(SeriesData.seriesColumn,
		        new Comparator<SeriesData>() {
	          public int compare(SeriesData o1, SeriesData o2) {
	            if (o1 == o2) {
	              return 0;
	            }

	            // Compare the name columns.
	            if (o1 != null) {
	              return (o2 != null) ? o1.label.compareTo(o2.label) : 1;
	            }
	            return -1;
	          }
	        });		

		seriesList.addColumnSort(SeriesData.studyColumn,
		        new Comparator<SeriesData>() {
		          public int compare(SeriesData o1, SeriesData o2) {
		            if (o1 == o2) {
		              return 0;
		            }
	
		            // Compare the name columns.
		            if (o1 != null) {
		              return (o2 != null) ? o1.friendly.compareTo(o2.friendly) : 1;
		            }
		            return -1;
		          }
		});
		seriesList.getColumnSortList().push(SeriesData.studyColumn);
		seriesList.getColumnSortList().push(SeriesData.seriesColumn);

		add(seriesList);//vpSeries);
		viewSeries = new Button("Show together");
		viewSeries.setEnabled(false);
		HorizontalPanel dls = new HorizontalPanel();
		dls.add(new Label("Download:"));
		dlCSV = new Button("CSV");
		dls.add(dlCSV);
		dlJson = new Button("JSON");
		dlJson.setEnabled(false);
		add(dls);

		HTML panelheader = new HTML("<img width=\"" + BAR + "\" height=\"" + BAR + "\" src=\"images/data.png\">Daaata");
		panelheader.addStyleDependentName("managePanel-Header");
		
		add(panelheader);
		setCellHorizontalAlignment(viewSeries, HasHorizontalAlignment.ALIGN_CENTER);
		setCellHorizontalAlignment(dls, HasHorizontalAlignment.ALIGN_CENTER);

		dlCSV.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
//				EEGTracePane.this.parent.generateDownloads(userID, true);
			}
			
		});
		dlJson.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
//				EEGTracePane.this.parent.generateDownloads(userID, false);
			}
			
		});
	}

	private int getNumRows() {
		if (Window.getClientHeight() < 900)
			return 4;
		else if (Window.getClientHeight() < 1050)
			return 6;
		else if (Window.getClientHeight() < 1200)
			return 10;
		else
			return 12;
	}
}
