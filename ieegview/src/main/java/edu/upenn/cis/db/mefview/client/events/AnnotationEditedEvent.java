/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class AnnotationEditedEvent  extends GwtEvent<AnnotationEditedEvent.Handler>
{
	private static final Type<AnnotationEditedEvent.Handler> TYPE = new Type<AnnotationEditedEvent.Handler>();
	
	private final DataSnapshotModel model;
	
	private final AnnotationGroup<Annotation> group;
	
	private final Annotation prior;
	private final Annotation a;
	private boolean wasRemoved = false;
	
	public AnnotationEditedEvent(DataSnapshotModel model,
			final AnnotationGroup<Annotation> group,
			final Annotation prev,
			final Annotation ann){
		this.model = model;
		this.group = group;
		this.prior = prev;
		a = ann;
	}
	
	public AnnotationEditedEvent(DataSnapshotModel model,
			final AnnotationGroup<Annotation> group,
			final Annotation prev,
			final Annotation ann, boolean wasDeleted){
		this(model, group, prev, ann);
		wasRemoved = true;
	}
	
	public DataSnapshotModel getDataSnapshotState() {
		return model;
	}
	
	public AnnotationGroup<Annotation> getGroup() {
		return group;
	}
	public static com.google.gwt.event.shared.GwtEvent.Type<AnnotationEditedEvent.Handler> getType() {
		return TYPE;
	}

	public boolean isRemoved() {
		return wasRemoved;
	}

	public void setRemoved(boolean wasRemoved) {
		this.wasRemoved = wasRemoved;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AnnotationEditedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AnnotationEditedEvent.Handler handler) {
		handler.onAnnotationEdited(model, group, prior, a, wasRemoved);
	}

	public static interface Handler extends EventHandler {
		void onAnnotationEdited(DataSnapshotModel action, 
				AnnotationGroup<Annotation> group, Annotation prev, Annotation next, boolean wasDeleted);
	}

}
