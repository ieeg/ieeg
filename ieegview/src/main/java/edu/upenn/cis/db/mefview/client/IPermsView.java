/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.List;

import com.google.common.base.Optional;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse.UserIdAndName;

public interface IPermsView {

	public static interface IPermsPresenter {

		public ListDataProvider<ExtUserAce> getUserAceProvider();

		public ListDataProvider<ExtWorldAce> getWorldAceProvider();

		public String getFriendlyName();

		public void setView(IPermsView view);

		public boolean userIsOwner();

		public List<String> getUserPermsOptions();

		public List<String> getProjectPermsOptions();

		public List<String> getWorldPermsOptions();

		public void removeUserAce(ExtUserAce toBeRemoved);

		public void updateUserAce(ExtUserAce orig, ExtPermission newPerms);
		
		public void updateProjectAce(ExtProjectAce orig, ExtPermission newPerms);

		public void updateWorldAce(ExtWorldAce orig,
				Optional<ExtPermission> newPerms);

		public void doSave();

		public void addUserListDisplay(HasData<UserIdAndName> usersList);

		public void addNewUserAces();

		public SelectionModel<UserIdAndName> getUserListSelectionModel();

		public void addProjectGroupListDisplay(HasData<ProjectGroup> groupList);

		public void addNewProjectAces();

		public SelectionModel<ProjectGroup> getProjectGroupListSelectionModel();

		public String worldPermToString(Optional<ExtPermission> worldPerm);

		public String userPermToString(ExtPermission userPerm);

		public String projectPermToString(ExtPermission projectPerm);

		public Optional<ExtPermission> stringToWorldPerm(String displayString);

		public ExtPermission stringToUserPerm(String displayString);

		public ExtPermission stringToProjectPerm(String displayString);

		/**
		 * DOCUMENT ME
		 * 
		 * @return
		 */
		ListDataProvider<ExtProjectAce> getProjectAceProvider();

		/**
		 * DOCUMENT ME
		 * 
		 * @param toBeRemoved
		 */
		void removeProjectAce(ExtProjectAce toBeRemoved);

	}

	public void closeDialog();

}
