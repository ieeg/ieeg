package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class ErrorCreatingAccountDialogBox extends DialogBox {
	public ErrorCreatingAccountDialogBox() {
		super(true, true);
		
		setTitle("Unable to Create Account");
		add(new Label("Sorry, we were not able to create your account."));
	}
}
