package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.DialogBox;

import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.Annotation;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class SaveAnnotationsToSnapshotCommand implements Command {
	final String dataset;
	final Collection<Annotation> annotationsToSave;
	final OnDoneHandler handler;
	final ClientFactory clientFactory;
	
	public SaveAnnotationsToSnapshotCommand(
			final ClientFactory clientFactory,
			final String dataset,
			final Collection<Annotation> annotationsToSave,
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.handler = handler;
		this.clientFactory = clientFactory;
		this.annotationsToSave = annotationsToSave;
	}

	@Override
	public void execute() {
		Set<Annotation> annSet;
		
		if (annotationsToSave instanceof Set)
			annSet = (Set<Annotation>)annotationsToSave;
		else {
			annSet = new HashSet<Annotation>();
			annSet.addAll(annotationsToSave);
		}
		for (Annotation a: annSet) {
			GWT.log("Annotation " + a.getRevId() + ": " + a.getColor());
		}

		clientFactory.getSnapshotServices().saveAnnotations(
				clientFactory.getSessionModel().getSessionID(), 
				dataset, 
				annSet, 
				new SecFailureAsyncCallback.SimpleSecFailureCallback<DataSnapshotIds>(clientFactory) {

			public void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Error Saving Annotations!", "Sorry, there was an error saving the annotations.");
			}

			public void onSuccess(DataSnapshotIds result) {
				Dialogs.messageBox("Success!", "We successfully saved the annotations.");
				if (handler != null)
					handler.doWork(result.getDsRevId());
			}

		});
	}

}
