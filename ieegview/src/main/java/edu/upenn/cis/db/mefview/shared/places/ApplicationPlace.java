/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.IsSerializable;

public class ApplicationPlace extends Place implements IsSerializable {
	List<String> snapshots;// = new ArrayList<String>();
	
	List<String> recentSnapshots = new ArrayList<String>();
	List<String> recentTools  = new ArrayList<String>();
	
	List<String> favoriteSnapshots = new ArrayList<String>();
	List<String> favoriteTools = new ArrayList<String>();
	
	HashMap<String,List<PanelDescriptor>> windows = new HashMap<String,List<PanelDescriptor>>();  
	
	public ApplicationPlace() {
		this.snapshots = new ArrayList<String>();
	}
	
	public ApplicationPlace(List<String> snapshots) {
		this.snapshots = snapshots;
	}
	
	public void addSnapshot(String p, PanelDescriptor firstWindow) {
		snapshots.add(p);
		addDescriptor(p, firstWindow);
	}
	
	public List<String> getSnapshots() {
		return snapshots;
	}

	public List<String> getRecentSnapshots() {
		return recentSnapshots;
	}
	
	public List<PanelDescriptor> getDescriptorsFor(String snapshot) {
		if (windows.get(snapshot) == null) {
			windows.put(snapshot, new ArrayList<PanelDescriptor>());
		}
		return windows.get(snapshot);
	}
	
	public void addDescriptor(String snapshot, PanelDescriptor p) {
		getDescriptorsFor(snapshot).add(p);
	}
	
	public void removeDescriptor(String snapshot, PanelDescriptor p) {
		List<PanelDescriptor> pl = windows.get(snapshot);
		
		pl.remove(p);
		
		if (pl.isEmpty()) {
			snapshots.remove(snapshot);
			windows.remove(snapshot);
		}
	}
	
	public void removeSnapshot(String p) {
		snapshots.remove(p);
		windows.remove(p);
	}

	public void setRecentSnapshots(List<String> recentSnapshots) {
		this.recentSnapshots = recentSnapshots;
	}

	public List<String> getRecentTools() {
		return recentTools;
	}

	public void setRecentTools(List<String> recentTools) {
		this.recentTools = recentTools;
	}

	public List<String> getFavoriteSnapshots() {
		return favoriteSnapshots;
	}

	public void setFavoriteSnapshots(List<String> favoriteSnapshots) {
		this.favoriteSnapshots = favoriteSnapshots;
	}

	public List<String> getFavoriteTools() {
		return favoriteTools;
	}

	public void setFavoriteTools(List<String> favoriteTools) {
		this.favoriteTools = favoriteTools;
	}

	public void setSnapshots(List<String> snapshots) {
		this.snapshots = snapshots;
	}
	
	
}
