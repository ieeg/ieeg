/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;

@GwtCompatible(serializable = false)
public class GithubRelease {
  private String tag_name;
  private String name;
  private String release_date;
  @GwtIncompatible("URL")
  private URL zipball_url;
  @GwtIncompatible("URL")
  private URL tarball_url;
  private Integer id;
    
 
  @GwtIncompatible("URL")
  public String toString(){
    return this.name + " - " + this.zipball_url;
  }
  
  @GwtIncompatible("ObjectMapper and others")
  public static Set<GithubRelease> fromURL(URL releaseUrl) throws JsonParseException, JsonMappingException, IOException{
    
    // Open connection and retrieve Release JSON info for Project
    URLConnection uc = releaseUrl.openConnection();
    BufferedReader in = new BufferedReader(new InputStreamReader(
        uc.getInputStream()));
    String retText = in.readLine();
    in.close();
    
    ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> relsIn =
                    mapper.readValue(
                        retText,
                                    List.class);
    
   Set<GithubRelease> releases = new HashSet<GithubRelease>();
   for (Map<String, Object> relIn: relsIn){
     GithubRelease curRel = new GithubRelease();
     curRel.tag_name = (String) relIn.get("tag_name") ; 
     curRel.name = (String) relIn.get("name") ; 
     curRel.release_date =(String) relIn.get("published_at") ; 
     curRel.zipball_url = new URL((String) relIn.get("zipball_url")) ; 
     curRel.tarball_url = new URL((String) relIn.get("tarball_url")) ; 
     curRel.id = (Integer) relIn.get("id") ; 
     releases.add(curRel);
   }

    return releases;

  }

  public String getTag_name() {
    return tag_name;
  }

  public String getName() {
    return name;
  }

  public String getRelease_date() {
    return release_date;
  }

  @GwtIncompatible("URL")
  public URL getZipball_url() {
    return zipball_url;
  }

  @GwtIncompatible("URL")
  public URL getTarball_url() {
    return tarball_url;
  }

  public Integer getId() {
    return id;
  }
  

  
} 
  

