/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.Set;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class SnapshotDetailsPane extends ResizingLayout implements ChildPane {
	
	SnapshotPane studyDetails;
	ExperimentPane experimentDetails;
	
	public SnapshotDetailsPane() {
		super();
	}
	
	public SnapshotDetailsPane(ClientFactory clientFactory, MetadataBrowserPresenter presenter) {
        // Details pane for Human studies
		studyDetails = new SnapshotPane(clientFactory);
		
		// Details pane for Animal Studies
		experimentDetails = new ExperimentPane(clientFactory);
	
		add(studyDetails);
		studyDetails.setVisible(false);
		add(experimentDetails);
		experimentDetails.setVisible(false);
	}
	
	@Override
	public void setSelected(Set<SerializableMetadata> selectedItem) {
		if (selectedItem.size() > 0)
			setSelected(selectedItem.iterator().next());
	}

	@Override
	public void setSelected(SerializableMetadata selectedItem) {
		if (selectedItem instanceof StudyMetadata) {
			studyDetails.setVisible(true);
			experimentDetails.setVisible(false);
			studyDetails.setSelected(selectedItem);
		} else if (selectedItem instanceof ExperimentMetadata) {
			studyDetails.setVisible(false);
			experimentDetails.setVisible(true);
			experimentDetails.setSelected(selectedItem);
		}
	}

	@Override
	public ChildPane create(ClientFactory factory,
			MetadataBrowserPresenter presenter) {
		return new SnapshotDetailsPane(factory, presenter);
	}

	@Override
	public void bind() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unbind() {
		// TODO Auto-generated method stub
		
	}
	

}
