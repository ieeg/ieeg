/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;


import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.danvk.DygraphOptions;
import org.danvk.GraphAnnotation;
import org.danvk.JsArrayDygraph;
import org.danvk.MouseEvent;
import org.danvk.Point;
import org.danvk.Points;
//import org.danvk.DygraphDoubleClickHandler;
//import org.danvk.DygraphOptions;
//import org.danvk.Dygraphs;
//import org.danvk.JsArrayDygraph;
import org.danvk.TsDygraphAnnotationClickHandler;
import org.danvk.TsDygraphDoubleClickHandler;
import org.danvk.TsDygraphMoveHandler;
import org.danvk.TsDygraphOutOfDataHandler;
import org.danvk.TsDygraphVisibilityChangeHandler;
import org.danvk.TsDygraphZoomHandler;
import org.danvk.TsDygraphZoomSelHandler;
import org.danvk.TsDygraphs;
import org.gwt.advanced.client.datamodel.ComboBoxDataModel;

import com.google.common.base.Optional;
import com.google.common.primitives.Doubles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayBoolean;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayNumber;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasAllKeyHandlers;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootPanel;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetch;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.controller.SnapshotCache;
import edu.upenn.cis.db.mefview.client.dialogs.IAnnotationGroupDialog;
import edu.upenn.cis.db.mefview.client.dialogs.IEditAnnotation;
import edu.upenn.cis.db.mefview.client.dialogs.IFilterDialog;
import edu.upenn.cis.db.mefview.client.events.AnnotationRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.EegPositionChangedEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotRequestedEvent;
import edu.upenn.cis.db.mefview.client.events.UpdateSavedAnnotationsEvent;
import edu.upenn.cis.db.mefview.client.messages.EegPositionChangedMessage;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;
import edu.upenn.cis.db.mefview.client.messages.requests.UpdateSavedAnnotations;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.models.SnapshotAnnotationModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesFilterSpecifier;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.IEEGToolbar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.AnnotationGroupDialog;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.EditAnnotation;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.SetFilterParameters;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.util.DataConversion;
import edu.upenn.cis.db.mefview.client.viewers.AnnotationGroupPane;
import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.DoubleBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.places.EEGPlace;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import edu.upenn.cis.db.mefview.shared.util.UUID;

/**
 * The main EEG viewer composite
 * 
 * @author zives
 *
 */
public class EEGPane extends Composite implements AstroPanel, IEEGPane,
EEGPresenter.Display 
{

	/**
	 * Used for backing out zoom / view levels
	 * 
	 * @author zives
	 *
	 */
	public static class ViewHistory {
		boolean[] selectedChannels;
		double location;
		double screenWidth;
		double oldWidth;
		double period;
	}

	HandlerRegistration resizer;
	ServerAccess.Prefetcher prefetch;
	DygraphOptions options;
	
	Presenter presenter;
	
	double period = 1;
	public static final int LOOKAHEAD = 2;
	private EEGMontage curMontage = null;
	
	public Stack<ViewHistory> previousViews = new Stack<ViewHistory>();
	
	public final static double SCALE = 1;
		
	static int minHeight = 400;
	
	int epoch = 0;
	
	public static final boolean debug = false;

	public static final double ANNOTATION_OFFSET = 0.1;

	public static final boolean oldMode = true;
	public final static int RATIO = 1;//3;
	public static final int RATIO2 = 1;
	
	// msec to wait
	public static final int LAG = 500;
	public static final int LAG2 = 2;

	public String contextID = null;
	final ClientFactory clientFactory;
	
	boolean refreshGraph = false;
	boolean refreshAnnotations = false;
	
	JsArrayDygraph graph;

	double bufferStartTime;
	double bufferEndTime;
	
	boolean isInited = false;

	public final static long DEFAULT_TIME_SPAN = 15000000;
	private static Optional<Double> maxReqHzChsSecs = null;

	public int graphHeight = 440; //Height in px of 
	public int graphWidth = 800;
	public SliderBar slider;
	HorizontalPanel scrollerPanel;
	int buttonWidth = 0;
	final int buttonWidthGuess = 2 * 25 + 2 * 72;
	//static int lWidth = 0;

	double pageWidth;
	double traceScale = 1.0;

	int traceHeight;

	final FocusPanel visFocusPanel;
	ComboBoxWithEnter<ComboBoxDataModel> timeBox;
	ComboBoxWithEnter<ComboBoxDataModel> uVoltMmBox;
	
	Timer requestHighres = null;
	Timer delayedParse = null;
	
	double dotsPerInch = 0;
	double uVoltCnvFactor = 0;
	double mMPerUVolt = 0;
	double uvMm = 0;

	DoubleBoxWithEnter posBox;
	Button prevAnnotation;
	Button leftButton;
	Button rightButton;
	Button nextAnnotation;
	HTML textMetric;
	HTML textMessage = new HTML("");
	
	EEGPresenter handler;
	DataSnapshotViews snapshotController;
	IEEGToolbar toolbar;
	LayoutPanel host;
	EEGViewerPanel panel;
		
	MenuBar favorites = new MenuBar();
	MenuBar recents = new MenuBar();
	
	final String title;
	
	int internalFocus = 0;
	List<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();

	final LayoutPanel parent;

	
	public static final String NAME = "eeg-internal";
	
	public EEGPane(final LayoutPanel parent, final EEGViewerPanel control, 
			final DataSnapshotViews model,
			int gWidth, int gHeight, 
			final IEEGToolbar toolbar,
			final ListBox ann, 
			final ClientFactory clientFactory,
			final String title
			) {

		this.title = title;
		this.clientFactory = clientFactory;
		this.panel = control;
		this.snapshotController = model;
		this.toolbar = toolbar;
		this.parent = parent;
		
		this.contextID = UUID.uuid(); 
		
		dotsPerInch = RootPanel.get("dpi").getOffsetHeight();

		RootPanel metrics = RootPanel.get("metrics");
		textMetric = new HTML();
		metrics.add(textMetric);
		
		
//		GWT.log("EEGPane init: set graphwidth to: " + gWidth);
//		graphHeight = gHeight;
//		graphWidth = gWidth;
		if (Document.get().getElementById("mainContainer") == null) {
			graphWidth = Document.get().getElementById("rootLayoutPanel").getOffsetWidth() - 22;
			graphHeight = Document.get().getElementById("rootLayoutPanel").getOffsetHeight() - 110;
		} else {
			graphWidth = Document.get().getElementById("mainContainer").getOffsetWidth() - 22;
			graphHeight = Document.get().getElementById("mainContainer").getOffsetHeight() - 110;
		}

		// Install the dygraphs code.
		TsDygraphs.install(getSessionModel().getServerConfiguration().getDygraphLogLevel());
		
		visFocusPanel = new FocusPanel();//focus;
		
		visFocusPanel.setStyleName("vis");
		parent.setStyleName("parent");
	
		parent.add(visFocusPanel);//HostPanel);
		parent.setWidgetLeftRight(visFocusPanel/*HostPanel*/, 2, Unit.PX, 2, Unit.PX);
		parent.setWidgetTopBottom(visFocusPanel/*HostPanel*/, 44+20, Unit.PX, 44, Unit.PX);
		host = parent;

		scrollerPanel = new HorizontalPanel();
		scrollerPanel.setTitle("scroller");
		parent.add(scrollerPanel);
		parent.setWidgetLeftRight(scrollerPanel, 2, Unit.PX, 2, Unit.PX);
		parent.setWidgetBottomHeight(scrollerPanel, 1, Unit.PX, 42, Unit.PX);

		parent.add(textMessage);
		parent.setWidgetTopHeight(textMessage, 40, Unit.PCT, 140, Unit.PX);
		parent.setWidgetLeftRight(textMessage, 30, Unit.PCT, 30, Unit.PCT);
		textMessage.addStyleName("status");
		
		textMessage.setHTML("Loading new time series data...");
		visFocusPanel.setFocus(true);
		
		handler = new EEGPresenter(model, clientFactory);
		
		handler.setDisplay(this);
		handler.bind();
		
		if (maxReqHzChsSecs == null) {
			final String maxReqHzChsSecsStr = getSessionModel()
					.getServerConfiguration()
					.getOtherParameters()
					.get("MEFPageServerS3.maxReqHzChsSecs");
			if (isNullOrEmpty(maxReqHzChsSecsStr)) {
				maxReqHzChsSecs = Optional.absent();
			} else {
				maxReqHzChsSecs = Optional.of(Double.valueOf(maxReqHzChsSecsStr));
			}
		}
	}
	
	
	
	public IEEGToolbar getToolbar() {
		return toolbar;
	}



	public void setToolbar(IEEGToolbar toolbar) {
		this.toolbar = toolbar;
	}

	public HasText getInvertSignalText() {
		return this.toolbar.getInvertButton();
	}

	@Override
	public void initToolbar(IEEGToolbar toolbar, 
			double maxFreq, 
			FocusHandler focusedHandler,
			BlurHandler blurredHandler,
			ChangeHandler voltsChangedHandler, 
			NumericEnterHandler voltsEnteredHandler,
			ChangeHandler timeChangedHandler, 
			NumericEnterHandler timeEnteredHandler,
			NumericEnterHandler posEnteredHandler,
			ClickHandler refreshHandler,
			ClickHandler invertHandler) {
		

		timeBox = toolbar.getTimeIntervalBox();//tBox;
		uVoltMmBox = toolbar.getVoltageScaleBox();//vBox;
		posBox = toolbar.getPosBox();//pBox;

		uVoltMmBox.addChangeHandler(voltsChangedHandler);
		uVoltMmBox.setEnterHandler(voltsEnteredHandler);
		uVoltMmBox.addFocusHandler(focusedHandler);
		uVoltMmBox.addBlurHandler(blurredHandler);
		
		timeBox.addChangeHandler(timeChangedHandler);
		timeBox.setEnterHandler(timeEnteredHandler);
		timeBox.addFocusHandler(focusedHandler);
		timeBox.addBlurHandler(blurredHandler);

		posBox.setEnterHandler(posEnteredHandler);
		posBox.addFocusHandler(focusedHandler);
		
		posBox.addBlurHandler(blurredHandler);

		toolbar.setRefreshHandler(refreshHandler);
		toolbar.setInvertHandler(invertHandler);
	}
	
	public void resize(int width, int height) {
		if (graph == null || slider == null || scrollerPanel == null)
			return;
		
		graphHeight = height;// - 72;
		graphWidth = width;
			
		GWT.log("Setting EEG size to " + graphWidth + " by " + graphHeight);
		setSize(graphWidth, graphHeight);

		setRefreshGraph();
		setRefreshAnnotations();
	}
	
	private void setSize(int width, int height) {
		GWT.log("Setting EEG size to " + width + " by " + height);
		
		final int scrollerPanelWidth = width - 12;
		buttonWidth = leftButton.getOffsetWidth() + rightButton.getOffsetWidth() + 
				prevAnnotation.getOffsetWidth() + nextAnnotation.getOffsetWidth();
		int wide = (scrollerPanelWidth - buttonWidth); // Set Slider width
		if (buttonWidth == 0 && wide > buttonWidthGuess) {
			wide = wide - buttonWidthGuess;
		}

		scrollerPanel.setWidth(scrollerPanelWidth  + "px");
		scrollerPanel.setCellHorizontalAlignment(slider, HasHorizontalAlignment.ALIGN_LEFT);
		
		slider.setWidth(wide + "px");
		
		// Make the parents bigger!
//		visFocusPanel.setSize(width + "px", (height + 0) + "px");
//		parent.setSize(width + "px", (height + 0) + "px");
//		parent.onResize();
		
		if (graph != null) {
			JsArrayDygraph.resizeDiv(graph.getRootDiv(), width , height - 120);
			graph.resize(width, height - 120);
		}
	}

	
	@Override
	public void scaleDown() {
		mMPerUVolt = graph.getYScale() / 1.2;
		graph.setYScale(mMPerUVolt);
		updateHeight(false);
	}
	
	@Override
	public void scaleUp() {
		mMPerUVolt = graph.getYScale() * 1.2;
		graph.setYScale(mMPerUVolt);
		updateHeight(false);
	}

	@Override
	public void setScale(double scaleFactorUvPerMm) {
		
		mMPerUVolt = 1/scaleFactorUvPerMm; 
		GWT.log("Setting mm per uv to " + mMPerUVolt + ". uv per mm: " + scaleFactorUvPerMm);
		graph.setYScale(mMPerUVolt);
//		updateHeight(false);
	}

	public double getScaleFactor() {
		return mMPerUVolt;
	}

	private native void setYTicks(final JavaScriptObject options, final JsArrayString labelArray, final JsArrayNumber gaps) /*-{
			var yticks = [];
			for (var i = 0; i < gaps.length; i++) {
				yticks.push({label: labelArray[i+1], v: gaps[i]} );
			}
			options.yTicks = yticks;
	    }-*/;

	public int getTraceHeight() {
		return traceHeight;
	}

	public EEGMontage getCurMontage() {
		return curMontage;
	}



	public void setTraceHeight(int h) {
		traceHeight = h;
	}
	
	/**
	 * Scrollbar
	 * 
	 * @param left
	 * @param right
	 */
	public void addScroller(double left, double right) {
		if (slider != null)
			return;
		
		prevAnnotation = new Button("&larr;&nbsp;Annot.");
		scrollerPanel.add(prevAnnotation);
		leftButton = new Button("&larr;");
		scrollerPanel.add(leftButton);
		removeStatusMessage();

		rightButton = new Button("&rarr;");
		scrollerPanel.add(rightButton);		
		nextAnnotation = new Button("&rarr;&nbsp;Annot.");
		scrollerPanel.add(nextAnnotation);
		buttonWidth = leftButton.getOffsetWidth() + 
		    rightButton.getOffsetWidth() + prevAnnotation.getOffsetWidth() + nextAnnotation.getOffsetWidth();
		int sliderWidth = graphWidth -  buttonWidth;
		if (buttonWidth == 0 && sliderWidth > buttonWidthGuess) {
			sliderWidth = sliderWidth - buttonWidthGuess;
		}
		slider = new SliderBar(left, right, String.valueOf(sliderWidth) + "px", 
				/*leftButton.getOffsetHeight() +*/ "26px"//,
		);

		scrollerPanel.insert(slider, 2);
		slider.setCurrentValue(0, false);
	}
	
	@Override
	public void setScrollerHandlers(
			ClickHandler annListHandler, ClickHandler prevAnnHandler,
			ClickHandler leftHandler, ClickHandler rightHandler, 
			ClickHandler nextAnnHandler, ValueChangeHandler<Double> sliderHandler) {
		prevAnnotation.addClickHandler(prevAnnHandler);
		leftButton.addClickHandler(leftHandler);
		rightButton.addClickHandler(rightHandler);
		nextAnnotation.addClickHandler(nextAnnHandler);
		slider.addValueChangeHandler(sliderHandler);
	}
	
	public void setLabels(List<? extends INamedTimeSegment> traces){
		// Create HTML labels, seems to only be used to calculate the width of the labels.
		StringBuilder html = new StringBuilder();
		for (INamedTimeSegment t : traces) {
			html.append("<div align='left'>" + t.getLabel() + "&nbsp;</div>");
		}
		textMetric.setHTML(html.toString());
		
		
	}
	
	public void setupLabels(List<INamedTimeSegment> traces) {
		filters = new ArrayList<DisplayConfiguration>(traces.size());
		for (int i = 0; i < traces.size(); i++)
			filters.add(new DisplayConfiguration());

		setLabels(traces);
	}

	public void init(List<INamedTimeSegment> traces, EEGPlace place) {
        if (isInited)
          return;
        else 
          isInited = true;
		
        if (traces.isEmpty()) {
        	showStatusMessage("Dataset contains no EEG channels.");
        	return;
        }
        
		if (place == null || place.getFilters() == null || place.getFilters().size() < traces.size()) {
			filters = new ArrayList<DisplayConfiguration>(traces.size());
			for (int i = 0; i < traces.size(); i++)
				filters.add(new DisplayConfiguration());
		} else {
			filters = place.getFilters();
		}

		setLabels(traces);

		// Init Graph with traces with 1 value =0
		initializeGraphWithDefaultTraces(EEGModel.getDefaultValues(getDataSnapshotState().getNumChannels()));
		
		if (place == null) {
			bufferStartTime = 0;//cursor;
			bufferEndTime = EEGPane.DEFAULT_TIME_SPAN;
		} else {
			bufferStartTime = place.getPosition();
			bufferEndTime = place.getWidth();
		}

		// Request the trace parameters, e.g., min and max time
		INamedTimeSegment value = traces.get(0);
		uVoltCnvFactor = value.getScale();

		// Add Scroller bar under EEG window
		addScroller(0, snapshotController.getSnapshotLength());
		
		// Initialize click-handlers for EEGPane
		handler.initHandlers(toolbar, value.getSampleRate());
		
		graph.setVoltConvFactor(uVoltCnvFactor);
		graph.setDPI(dotsPerInch);
		
		//Set base montage
		curMontage = new EEGMontage("As Recorded");
		List<EEGMontagePair> pairs = curMontage.getPairs();
		
		for (INamedTimeSegment n: traces){
			GWT.log("EEGPANE:INIT: " + n);
			pairs.add(new EEGMontagePair(n.getLabel(), null));
		}
		
		requestInitialPage();
	}
	
	@Override
	public double getNumberOfSamples() {
		
		double g = graph.getPlotWidth() * SCALE;
		
		// Set the maximum number of samples that are requested to 1500.
		if (g > 1500)
			g = 1500;
		return g;
	}
	
	public void requestInitialPage() {
		
		
		setPageWidth(EEGPane.DEFAULT_TIME_SPAN);
		period = EEGPane.DEFAULT_TIME_SPAN / getNumberOfSamples() * RATIO;
		
		SnapshotCache.debug2("Initial graph width " + graphWidth);
		
		if (period < 1.e6/snapshotController.getSampleRateHz())
			period = 1.e6/snapshotController.getSampleRateHz();
		showStatusMessage("Requesting start of EEG...");
		final long span = DEFAULT_TIME_SPAN; 

		GWT.log("RequestInitialPage() at: " + period);
		SnapshotCache.debug2("--> Requesting initial page at " + period);

		getData(getSessionModel().getUserId(), getDataSnapshotState().getSnapshotID(), 
				getDataSnapshotState().getChannelPaths(), 
				0, span, period, filters, false); 
	}

	/**
	 * Fetches the data and calls back a handler
	 * 
	 * @param user
	 * @param dataSnapshotRevId
	 * @param traceIds
	 * @param start
	 * @param width
	 * @param samplingPeriod
	 * @param filter
	 * @param succ
	 */
	@Override
	public void getData(final String user, final String dataSnapshotRevId, final List<String> traceIds,
			final double start, final double width, final double samplingPeriod, 
			final List<DisplayConfiguration> filter, boolean dontCache) {

			GWT.log("Requesting data with start: " + start + " :sampling: " + samplingPeriod + " :width: " + width);
		
			List<INamedTimeSegment> info = getDataSnapshotState().getTraceInfo();
			ArrayList<TimeSeriesSpanSpecifier> spec = new ArrayList<TimeSeriesSpanSpecifier>();
			int i = 0;
			int[] ids = new int[filter.size()];
			
			for (INamedTimeSegment t: info) {
				TimeSeriesSpanSpecifier tss = new TimeSeriesSpanSpecifier(t,
						(long)start, (long)width, 1.E6 / samplingPeriod,
						new TimeSeriesFilterSpecifier(filter.get(i++)));
				spec.add(tss);
			}
			
			RequestSnapshotData action = new RequestSnapshotData(
//					getDataSnapshotState(),
					getDataSnapshotState().getSnapshotID(), 
					contextID, spec, filters, 
					0,//snapshotController.getStartOffset(), 
					snapshotController.getWidth(), ids);
			if (dontCache)
				action.disableCache();
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new SnapshotRequestedEvent(action));
	}

//	// Sync this to EEGViewerPanel!!
//	public void computeSizes() {
//		int windowWidth = Document.get().getElementById("mainContainer").getOffsetWidth();
//		int windowHeight = Document.get().getElementById("mainContainer").getOffsetHeight() - 30;
//		graphWidth =   windowWidth - 12;
//		graphHeight =  windowHeight - 80;
//	}
	
	@Override
	public void updateGraphWithTraces(JsArray<JsArrayInteger> traceData, boolean isMinMax) {
		options.set("period", period);
		// Can't zoom below the actual sample rate
		options.set("minPeriod", 1.e6/snapshotController.getSampleRateHz());
		options.set("studyStartTime", 0);//snapshotController.getSnapshotStart());
		options.set("studyEndTime", //snapshotController.getSnapshotStart() + 
				snapshotController.getSnapshotLength());
		options.set("avoidRescale", 1);
		options.set("valueScale", uVoltCnvFactor);
		options.set("resizeY", true);

		// Resize event on parent
		panel.onResize();
		//computeSizes();
		//resize(graphWidth, graphHeight);
		

//		resizer = Window.addResizeHandler(
//			new ResizeHandler() {
//	
//				@Override
//				public void onResize(ResizeEvent event) {
//					graphWidth = Document.get().getElementById("mainContainer").getOffsetWidth();
//					graphHeight = Document.get().getElementById("mainContainer").getOffsetHeight() - 100;
//					parent.setSize(graphWidth + "px", graphHeight + 20 + "px");
//					parent.onResize();
//					resize(graphWidth, graphHeight);
//				}
//			});
		
		
		boolean[] vis = new boolean[getDataSnapshotState().getNumChannels()];
		for (int i = 0; i < vis.length; i++)
			vis[i] = true;
		options.setVisibility(vis);

		graph.update(traceData, bufferStartTime, bufferStartTime + getSpecifiedPageWidth() * 1e6, 
				options, isMinMax);
		
		mMPerUVolt = 1/graph.getVisScale();
		updateHeight(false);
	}
	

//	
	/**
	 * This 
	 * @param json
	 */
	public void initializeGraphWithDefaultTraces(JsArray<JsArrayInteger> json) {

		// Create the graph.
		options = new DygraphOptions();
		options.setWidth(graphWidth);
		options.setHeight(graphHeight);
	
	
		options.set("drawYGrid", false);
		options.set("exactY", true);
		options.set("xuUTC", true);
		
		options.set("yAxisLabelWidth", 30 + textMetric.getOffsetWidth());
		options.set("axisLabelWidth", 30 + textMetric.getOffsetWidth());
		
		options.set("xAxisLabelWidth", 200);
		textMetric.getElement().getStyle().setDisplay(Display.NONE);

		getDataSnapshotState().setTraceJSLabels(snapshotController.getChannelNames());

		showStatusMessage("Initializing the EEG...with labels: " + getDataSnapshotState().getTraceJSLabels());
		
		graph = new JsArrayDygraph(json, getDataSnapshotState().getTraceJSLabels(), options);
		graph.setDouble(true);
		double maxDurationMicros = getMaxRequestMicros();
		graph.setMaxDurationMicros(maxDurationMicros);
		//Keep graph at least 2 periods wide.
		graph.setMinDurationMicros(2e6/getDataSnapshotState().getSampleRateHz());
		
		graph.setCanHandleDoubleClickEvents(true);
		graph.addDoubleClickHandler(new TsDygraphDoubleClickHandler() {
			@Override
			public void onDoubleClick(MouseEvent event, Date start, Date date, Points points) {
			}

			@Override
			public void onDoubleClick(MouseEvent event, double start, double date, Points points) {
				if (event.shiftKey() || date == 0) {
//					model.switchToImagePane(points.get(0).getName(), "THREE_D_RENDERING");
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new
							OpenDisplayPanel(
									snapshotController.getSearchResult(), false,
									ImageViewerPanel.NAME, null)));
				} else {
					createAnnotation(points, start, date);
				}
			}
		});

		
		
		graph.setCanHandleOutOfDataEvents(true);
		graph.addOutOfDataHandler(new TsDygraphOutOfDataHandler() {

			@Override
			public void onOutOfData(double newMinX,
					double newMaxX, double oldMinX, double oldMaxX) {

				
				// Check against bounds
				if (newMinX < 0)//snapshotController.getSnapshotStart())//studyStartTime)
					newMinX = 0;//snapshotController.getSnapshotStart();//studyStartTime;
				if (newMaxX > //snapshotController.getSnapshotStart() + 
				snapshotController.getSnapshotLength() /*studyEndTime*/)
					newMaxX = //snapshotController.getSnapshotStart() + 
					snapshotController.getSnapshotLength() /*studyEndTime*/;

				period = (newMaxX - newMinX) / getNumberOfSamples();
				
				if (period < 1.e6/snapshotController.getSampleRateHz())
					period = 1.e6/snapshotController.getSampleRateHz();
				
				SnapshotCache.debug2("-- Out of Data --> requesting "+ newMinX +"-" + newMaxX + " @ " + period );
				options.set("period", period);

				double amt = (newMaxX - newMinX);
				//if (amt < 1E6)
				//	amt = 1E6;
				GWT.log("onOutofData()");
				getData(//user, dataset, 
						getSessionModel().getUserId(), getDataSnapshotState().getSnapshotID(), 
						getDataSnapshotState().getChannelPaths(), (long)newMinX, amt, period, filters,
						false);
			}

		});

		graph.setCanHandleZoomSelEvents(true);
		graph.addZoomSelHandler(new TsDygraphZoomSelHandler() {

			@Override
			public void onZoomSel(double startD, double endD, int startY, int endY,
					double oldStart, double oldEnd, double oldPeriod) {

				GWT.log("ZOOMING GRAPH.ZOOMSELHANDLER");
				
				ViewHistory vh = saveView();
				vh.location = oldStart;
				vh.screenWidth = oldEnd - oldStart;
				vh.oldWidth = timeBox.getValue();
				vh.period = oldPeriod;
				
//				if (startY <= endY) {
//					boolean[] v = new boolean[snapshotController.getChannelNames().size()];
//					for (int i = 0; i < v.length; i++)
//						v[i] = (i >= startY && i <= endY);
//					graph.showThese(v);
//				}
				
				setPageWidth(endD - startD);
				String val = DataConversion.getRoundedValue(startD / 1E6);
				posBox.setText(val);
				slider.setCurrentValue(startD, false);
				
				setRefreshGraph();
			}

		});

		graph.setCanHandleZoomEvents(true);
		
//		graph.addZoomYHandler(new DygraphZoomYHandler() {
//
//			@Override
//			public void onZoom(double scale) {
//				timeBox.setValue(1/scale, true);
//				
//			}
//			
//		});
		
		graph.addZoomHandler(new TsDygraphZoomHandler() {

			@Override
			public void onZoom(Double startD, Double endD, Double yScale) {
				GWT.log("ZOOMING GRAPH.ZOOMHANDLER: " + startD + " : " + endD + " : " + yScale);
				
				if (yScale!=-1){
					uVoltMmBox.setValue(1/yScale, false);
					
				}else{
					setPageWidth(endD - startD);

					String val = DataConversion.getRoundedValue((
					    startD
					    //- snapshotController.getSnapshotStart()
					    ) / 1E6);
					posBox.setText(val);
					slider.setCurrentValue(startD, false);
				}

//				setRefreshGraph();
			}

			@Override
			public void onZoom(Date startDate, Date endDate, Double yScale) {
				// TODO Auto-generated method stub
				
			}

//			


		});

		graph.setCanHandleMoveEvents(true);
		graph.addMoveHandler(new TsDygraphMoveHandler() {

			@Override
			public void onMove(double minX, double maxX, double period) {
				String val = DataConversion.getRoundedValue((minX
						//- snapshotController.getSnapshotStart()
						/*studyStartTime*/) / 1E6);
				posBox.setText(val);

				slider.setCurrentValue(minX, false);
//				refreshAnnotationsOnGraph(true);
				setRefreshGraph();
				setRefreshAnnotations();
			}

		});

		graph.setCanHandleVisibilityChangeEvents(true);
		graph.addVisibilityChangeHandler(new TsDygraphVisibilityChangeHandler() {

			@Override
			public void onVisibilityChange(String trace, boolean isVisible) {
//				System.err.println("Visibility callback on " + trace);
				updateHeight(false);

				refreshAnnotationsOnGraph(false);
				
				if (!trace.equals("multi")) {

					ViewHistory vh = saveView();
					
					// Toggle back the selected trace
					for (int i = 0; i < getDataSnapshotState().getNumChannels(); i++)
						if (getDataSnapshotState().getTraceInfo().get(i).getLabel().equals(trace))
							vh.selectedChannels[i] = !isVisible;

				}
			}

		});
		
		graph.setCanHandleAnnotationClickEvents(true);
		graph.addAnnotationClickHandler(new TsDygraphAnnotationClickHandler() {

			@Override
			public void onClick(GraphAnnotation annotation, Point point,
					MouseEvent event) {
				
				Annotation ann = getDataSnapshotState().getMatchingAnnotation(annotation.getInternalId());
				visFocusPanel.setFocus(true);
				
				if (ann != null) {
					editAnnotation(ann, false);
				} else {
					showErrorMessage("Unable to find annotation " + point.getName());
				}
			}
			
		});
//		}

		visFocusPanel.add(graph);

	}
	
	@Override
	public void requestDataAgain() {
		GWT.log("RequestDataAgain()");
		getData(getSessionModel().getUserId(), getDataSnapshotState().getSnapshotID(), getDataSnapshotState().getChannelRevIDs(),
				getPosition(), graph.getWindowWidth(), period, 
				filters, true);
	}
	
	/**
	 * Add an annotation to the graph, at the designated place
	 * 
	 * @param points
	 * @param start
	 * @param date
	 */
	private void createAnnotation(Points points, double start, double date){
		
	  // Make sure start < end time.
	  if (start > date) {
			double temp = date;
			date = start;
			start = temp;
		}
		
		//Check if there is an active, inform user.
		SnapshotAnnotationModel annModel = snapshotController.getState().getAnnotationModel();
		if (annModel.getSelectedGroup() == null) {
		  showErrorMessage("You need to select an Annotation Layer first." +
		  		"You can do this in the 'Layers' menu.");
		}
		
		// Create annotation in active layer.
		Annotation a = new Annotation(getSessionModel().getUserId(), 
				EditAnnotation.defaultType, 
				start, date, new HashSet<String>(), true, getDataSnapshotState().getTraceInfo());
		
		Set<String> thePoint = new HashSet<String>();
		for (int index = 0; index < points.length(); index++) {
			Point point = points.get(index);

			if (!graph.isHidden(point.getName())) {
				a.getChannels().add(point.getName());
				thePoint.add(point.getName());
			}
		}
		a.getChannelInfo().addAll(getDataSnapshotState().getTraceInfo(a.getChannels()));
		AnnotationGroup<Annotation> group = annModel.addAnnotation(a);
				
		a.setLayer(group.getName());
		
		// Open annotation edit dialog
		editAnnotation(a, true);
		
		// Print to system.out
		display(a.toString());

		// Add annotation to slider bar.
		slider.addAnnotation(new SliderBar.AnnBlock(start, date));
		
		graph.addAnnotations('[' + a.toJSON2(false, group.getColorFor(a), 
				snapshotController.getState().getAnnotationDefinitions(),
				snapshotController.getState().getChannelNames()) + ']');
	}
	
	/**
	 * User clicked on an annotation -- bring up the edit dialog
	 * @param ann
	 * @param name
	 */
	private void editAnnotation(Annotation ann, boolean isNew) {
		graph.clearHighlightedY();
		
		IEditAnnotation e = GWT.create(EditAnnotation.class);
		e.init(ann, 
				snapshotController.getState().getAnnotationDefinitions(),
				ann.getChannels(), 
				getAnnotationTypes(),   
				getDataSnapshotState(),
				isNew,
				null);


		e.setModal(true);
		e.setGlassEnabled(true);
		e.center();
		e.show();
	}
	
	/**
	 * Show an error message, hiding the "please wait" message in the meantime
	 * 
	 * @param msg
	 */
	private void showErrorMessage(String msg) {
		removeStatusMessage();

		Dialogs.messageBox("Sorry, An Error Occurred", msg);
	}

	@Override
	public JsArray<JsArrayInteger> refreshBufferFromSeries(TimeSeries[] results) {
		double bufferStart;
		double bufferEnd;
		JsArray<JsArrayInteger> ar = EEGModel.getTimeSeriesArray(results);

		if (results.length > 0) {
			bufferStartTime = results[0].getStartTime();
			bufferEndTime = results[0].getStartTime() + results[0].getPeriod() * results[0].getSeries().length;
			period = results[0].getPeriod();
			for (int i = 1; i < results.length; i++) {
				bufferStart = results[i].getStartTime();
				bufferEnd = results[i].getStartTime() + results[0].getPeriod() * results[0].getSeries().length;
				
				if (bufferStart < bufferStartTime)
					bufferStartTime = bufferStart;
				if (bufferEnd > bufferEndTime)
					bufferEndTime = bufferEnd;
			}
		}
		
		return ar;
	}
	
	private static native void gotHere(String msg) /*-{
		console.debug(msg);
	}-*/;

	
	@Override
	public JsArray<JsArrayInteger> refreshBufferFromJson(String results) {
		JsArray<JsArray<?>> arBase = EEGModel.parseArray(results);
		
		double reqStart = EEGModel.getRequestedStart(arBase);
		
		JsArray<JsArrayInteger> ar = EEGModel.getParsedArray(0,//snapshotController.getSnapshotStart(),
				arBase);
		
		reqStart = reqStart / 1E6; 
		
		bufferStartTime = EEGModel.getStartDate(0, ar);//snapshotController.getSnapshotStart(), ar);
		bufferEndTime = EEGModel.getEndDate(0, ar);//snapshotController.getSnapshotStart(), ar);
		
//		System.out.println("Snapshot start is " + snapshotController.getSnapshotStart());
//		System.out.println("Request buffer start is " + bufferStartTime);
		
		period = EEGModel.getPeriod(ar);
		return ar;
	}

	@Override
	public void setPageWidth(double width) {
		pageWidth = width;
		timeBox.setText(DataConversion.getRoundedValue((width) / 1E6));
		slider.setCursorWidth(width);
	}

	@Override
	public double getPageWidth() {
		return pageWidth;
	}
	
	@Override
	public double getSpecifiedPageWidth() {
		return Double.valueOf(timeBox.getText());
	}
	
	@Override
	public void prefetchAsNeeded(long start, long end, int direction) {
		if (start >= snapshotController.getSnapshotLength())
			return;
		if (end > //snapshotController.getSnapshotStart() + 
			snapshotController.getSnapshotLength() /*studyEndTime*/ - getPageWidth())
			end = (long)(//snapshotController.getSnapshotStart() + 
					snapshotController.getSnapshotLength() /*studyEndTime*/ - getPageWidth());
		
		if (start < getPosition())
			end = (long)getPosition();
		else
			end = (long)(start + LOOKAHEAD * getPageWidth());

		long st = ( start);// + getPageWidth() * direction);
		int p = 0;
		for (p = 0; p < LOOKAHEAD; p++) {
			st = (long)(start + p * getPageWidth() * direction);
			if (!clientFactory.getPortalServices().haveCached(getDataSnapshotState().getSnapshotID(), st, 
					(long)( st + getPageWidth() ), period)) {
				SnapshotCache.debug2("-- Could not find in cache " + st + "-" + (long)(st + getPageWidth()));
				break;
			} else {
				SnapshotCache.debug2("-- Have cached " + st);
			}
		}
		start = st;
		
		
		if (p < LOOKAHEAD && start < end) {
			end = Math.min((long)(start + getPageWidth() * LOOKAHEAD * 2 * direction), (long)getMaxRequestMicros());
			if (start < end) {
			SnapshotCache.debug2("-- Initiating prefetch " + (start) + " for " + (end - start) + " @ " + period + " --");
			prefetch.addRequest(
					new Prefetch(
							snapshotController.getState(), 
							getDataSnapshotState().getSnapshotID(), 
							contextID, getDataSnapshotState().getChannelRevIDs(), 
							start, end, period, 
							(long) getPageWidth(),//snapshotController.getWidth(), 
							filters));
			}
		}
		
	}

	@Override
	public void nextPage() {
		double width = getPageWidth();
//		double posOld = getPosition();
		double posNew = getPosition() + width;// * 3 / 4;
		
		SnapshotCache.debug2("==>  NEXT PAGE <==");
		
		// Try to prevent us from going off the end of the file
		if (posNew + getPageWidth() > //snapshotController.getSnapshotStart() + 
			snapshotController.getSnapshotLength() /*studyEndTime*/)
			posNew = //snapshotController.getSnapshotStart() + 
			snapshotController.getSnapshotLength() /*studyEndTime*/ - getPageWidth();
		
		// ... But if the file is less than a screen, we need to start at the start!
		if (posNew < 0)//snapshotController.getSnapshotStart() /*studyStartTime*/)
			posNew = 0;//snapshotController.getSnapshotStart();//studyStartTime;
		
//		System.err.println("Offset = " + (posNew - posOld));
		graph.setPosition(posNew, posNew + getPageWidth(), 
				graph.getPeriod(), true);
		setPositionDisplay(getPosition());

//		if (newEEG != null) {
			EegPositionChangedMessage action = new EegPositionChangedMessage(panel, //null,//newEEG, 
					(long)(posNew), 0);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegPositionChangedEvent(action));
//		}
			
		if (prefetch != null) {
//			System.out.println("**PREFETCH REQUEST**");
			double start = posNew + getPageWidth();// * (LOOKAHEAD); 
			prefetchAsNeeded((long)start, (long)(start + getPageWidth() * LOOKAHEAD), 1);
		}
		
//		refreshAnnotationsOnGraph(true);
//		updateView();
	}
	
	@Override
	public void prevPage() {
		double width = getPageWidth();
		double posNew = getPosition() - width;// * 3 / 4;
		
		if (posNew < 0)//snapshotController.getSnapshotStart())//studyStartTime)
			posNew = 0;//snapshotController.getSnapshotStart();//studyStartTime;
		
		graph.setPosition(posNew, posNew + 
				getPageWidth(), graph.getPeriod(), true);
		setPositionDisplay(getPosition());
//		refreshAnnotationsOnGraph(true);
		
//		if (newEEG != null){
			EegPositionChangedMessage action = new EegPositionChangedMessage(panel, //null,//newEEG, 
					(long)(posNew), 0);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegPositionChangedEvent(action));
//		}

			if (prefetch != null) {
//				System.out.println("**PREFETCH REQUEST**");
//				SnapshotCache.debug2("-- Prefetching backward @" + (posNew - getPageWidth() * (LOOKBEHIND)) + "@ " + graph.getPeriod() + " --");
//				double start = posNew - getPageWidth() * (LOOKAHEAD);
				double start = posNew - getPageWidth();// + getPageWidth() * (LOOKAHEAD); 
				if (start < 0)
					start = 0;
				if (start < posNew)
					prefetchAsNeeded((long)start, (long)(start + getPageWidth() * LOOKAHEAD), -1);
			}
//			updateView();
	}

	@Override
	public void jumpTo(double timestamp) {
		if (timestamp + getPageWidth() > //snapshotController.getSnapshotStart() + 
			snapshotController.getSnapshotLength() /*studyEndTime*/)
			timestamp = //snapshotController.getSnapshotStart() +
			snapshotController.getSnapshotLength() /*studyEndTime*/ - getPageWidth();
			
		if (timestamp < 0)//snapshotController.getSnapshotStart() /*studyStartTime*/)
			timestamp = 0;//snapshotController.getSnapshotStart() /*studyStartTime*/;
		
		if (timestamp != getPosition())
			graph.setPosition(timestamp, timestamp + getPageWidth(), graph.getPeriod(), true);

//		if (newEEG != null) {
			EegPositionChangedMessage action = new EegPositionChangedMessage(panel, //null,//newEEG, 
					(long)(timestamp), 0);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegPositionChangedEvent(action));
//		}
		
			if (prefetch != null) {
//				System.out.println("**PREFETCH REQUEST**");
//				SnapshotCache.debug2("-- Prefetching forward @" + (timestamp + getPageWidth() * (LOOKAHEAD)) + "@ " + graph.getPeriod() + " --");
				double start = timestamp + getPageWidth();// * (LOOKAHEAD-1); 
//				double start = timestamp;// + getPageWidth() * (LOOKAHEAD); 
				prefetchAsNeeded((long)start, (long)(start + getPageWidth() * LOOKAHEAD), 1);
			}

			setPositionDisplay(timestamp);
//		refreshAnnotationsOnGraph(true);
//			updateView();
	}
	
	@Override
	public void setPositionDisplay(double timestamp) {
		slider.setCurrentValue(timestamp, false);
		String val = DataConversion.getRoundedValue((timestamp) / 1E6);
//				- snapshotController.getSnapshotStart() /*studyStartTime*/) / 1E6);
		posBox.setText(val);
	}

	@Override
	public void jumpToNextAnnotation() {
		boolean done = false;
		double pos = getPosition() + getPageWidth();
		int offset = (int)(getPageWidth() * ANNOTATION_OFFSET);
		for (SliderBar.AnnBlock d : getDataSnapshotState().getAnnotationTimes("Any")) {//getEventMarker())) {
			if (d.getStart() > pos + offset) {
				jumpTo(d.getStart() - offset);
				done = true;
				return;
			}
		}
		if (!done)
			jumpTo(//snapshotController.getSnapshotStart() + 
					snapshotController.getSnapshotLength() /*studyEndTime*/ - getPageWidth());
	}

	@Override
	public void jumpToPrevAnnotation() {
		boolean done = false;
		double pos = getPosition();
		ArrayList<SliderBar.AnnBlock> l = new ArrayList<SliderBar.AnnBlock>();
		l.addAll(getDataSnapshotState().getAnnotationTimes());
		Collections.reverse(l);
		int offset = (int)(getPageWidth() * ANNOTATION_OFFSET);
		for (SliderBar.AnnBlock d : l) {
			//				System.out.println("Compare " + pos + " with " + d);
			if (d.getStart() - offset < pos) {
				//					System.out.println("Match " + pos + " with " + d);
				jumpTo(d.getStart() - offset);
				done = true;
				return;
			}
		}
		if (!done)
			jumpTo(0);//snapshotController.getSnapshotStart() /*studyStartTime*/);
	}

	/**
	 * Refresh the graph display to include any relevant
	 * annotations
	 */
	@Override
	public void refreshAnnotationsOnGraph(boolean refreshAlways) {
		if (graph == null)
			return;
		
		double left = getPosition();
		double right = left + getPageWidth();
		
		String annot = getDataSnapshotState().getRelevantAnnotations(left, right);
		
		if (refreshAlways || annot.length() > 2) {
			graph.setAnnotations(annot);
		}

	}
	
	public ViewHistory saveView() {
		ViewHistory vh = new ViewHistory();
		if (graph != null) {
			vh.selectedChannels = new boolean[getDataSnapshotState().getTraceInfo().size()];
			JsArrayBoolean b = graph.getVisibility();
			for (int i = 0; i < b.length(); i++)
				vh.selectedChannels[i] = b.get(i);
			
			vh.oldWidth = timeBox.getValue();
			vh.screenWidth = graph.getWindowWidth();
			vh.location = getPosition();
			vh.period = graph.getPeriod();
			
			previousViews.push(vh);
		}
		return vh;
	}
	
	@Override
	public void restoreView() {

		if (!previousViews.isEmpty()) {
			ViewHistory last = previousViews.pop();

			graph.showThese(last.selectedChannels);

			graph.setPosition(last.location, last.location + last.screenWidth, 
					last.period, true);
			graph.setZoom(last.location, last.location + last.screenWidth, false);
			setPositionDisplay(last.location);
			setDuration(last.oldWidth);
			setRefreshAnnotations();
		}
	}

	public void layerPopup() {
	  if (graph != null)
	    graph.clearHighlightedY();
	  saveView();
       
	  
	     AnnotationGroupPane grpPane = new AnnotationGroupPane(this, clientFactory.getSessionModel(), snapshotController.getState(), clientFactory);
	     
	     IAnnotationGroupDialog dialog = GWT.create(AnnotationGroupDialog.class);
	     dialog.init(grpPane, snapshotController.getState(), clientFactory.getSessionModel(), this, null);
	     
	     
//	     AnnotationGroupDialog dialog = new AnnotationGroupDialog(grpPane, snapshotController.getState(), clientFactory.getSessionModel(), this );

	        grpPane.selectAnnotationGroup(snapshotController.getState());
	        
	        dialog.center();
	        dialog.show();

	}
	
//	public void tracePopup() {
////		if (graph != null)
////			graph.clearHighlightedY();
//		saveView();
//		
//		JsArrayBoolean ar;
//		
//
//		if (graph != null)
//			ar = graph.getVisibility();
//		else
//			ar = newArray(getDataSnapshotState().getTraceInfo().size());
//		
//		graph.getElement().getStyle().setDisplay(Display.NONE);
//		
//		IShowTraces chooseTraces = DialogFactory.getShowTracesDialog()
//		chooseTraces.setPane(pane);
//		
//		
////		ChooseTraces chooseTraces = new ChooseTraces(getDataSnapshotState().getTraceInfo(),
////				ar, getDataSnapshotState().getAnnotatedChannels(),
////				clientFactory, this
////				);
//		chooseTraces.center();
//		chooseTraces.show();
//	}
//	
	private final native JsArrayBoolean newArray(int size) /*-{
		var ar = [];
		for (var i = 0; i < size; i++)
			ar.push(false);
			
		return ar;
	}-*/;

	public void filterPopup() {
		graph.clearHighlightedY();
		
		IFilterDialog sfp = GWT.create(SetFilterParameters.class);
		
		sfp.init(graph.getLabels(), filters, 
				getDataSnapshotState().getTraceInfo(), curMontage,
				this, clientFactory, null);
		
		
//		SetFilterParameters sfp = new SetFilterParameters(graph.getLabels(), filters, 
//				getDataSnapshotState().getTraceInfo(), 
//				this, clientFactory);

		sfp.setModal(false);
		sfp.center();
		sfp.show();
	}
	
	
	@Override
	public void updateMontage(EEGMontage montage) {

		GWT.log("Setting Montage in EEGPane : " + montage);

		curMontage = montage;

		// get Traces
		List<String> traces = panel.getTraceLabels();

		int[] r = new int[montage.getPairs().size()]; // rank
		boolean[] v = new boolean[montage.getPairs().size()]; // visibility
		int[] s = new int[montage.getPairs().size()]; // sourceVec
		int[] a = new int[montage.getPairs().size()]; // Plot Against
		
		// Setting rank to lowest for all traces.
		Arrays.fill(r, r.length);
		Arrays.fill(a, -1);

		int curIndex = 0;
		for (EEGMontagePair p : montage.getPairs()) {
			String el1 = p.getEl1();
			String el2 = p.getEl2();

			// find EL1 in traces
			final int i = caseInsensitiveIndexOf(traces, el1);
			if (i == -1) {
				GWT.log("el1: ["
						+ el1
						+ "] not found in traces. Montage channel "
						+ curIndex
						+ " will not be visible");
			} else {
				// find EL2 in traces
				int j = -1;
				if (el2 != null) {
					j = caseInsensitiveIndexOf(traces, el2);
				}

				r[curIndex] = curIndex;
				v[curIndex] = true;
				a[curIndex] = j;
				s[curIndex] = i;
			}
			curIndex++;

		}

		GWT.log("Setting Montage : " + r);
		GWT.log("r[i] :" + r);
		GWT.log("v[i] :" + v);
		GWT.log("s[i] :" + s);
		GWT.log("a[i] :" + a);

		// graph.showThese(v);
		graph.setRank(r);
		graph.setPlotAgainst(a);
		graph.setSourceVec(s);
		graph.setVisibility(v);
		graph.resetRowScale();
		graph.refresh();

	}
		
//	public void montageAgainst(int base, int ref) {
//		graph.setRemontage(base, ref);
//	}
	
	public JsArrayInteger getMontage() {
		return graph.getRemontage();
	}
	
	private void display(String s) {
	  System.out.println(s);
	}

	// Updates the scale box in toolbar
	@Override
	public void updateHeight(boolean refresh) {
		
		if (graph != null) {
			uvMm = 1/graph.getVisScale();
			mMPerUVolt = uvMm;
			
			GWT.log("Updating height: "+ uvMm + "refresh: " + refresh);
			uVoltMmBox.setValue(uvMm, refresh);
		}

	}
	
	public void setAutosize(boolean b) {
		if (graph == null)
			return;
		
		graph.setAutosizeY(b);
		if (b)
			graph.resetYScale();
	}
	
	public void saveAndDeleteAnnotations() {
		final Set<Annotation> annotationsToSave = new HashSet<Annotation>();
		for (Annotation a: getDataSnapshotState().getAllEnabledAnnotations()) {
			if (a.isModified()) {
				a.setChannelInfo(getDataSnapshotState().getTraceInfo(a.getChannels()));
				annotationsToSave.add(a);
			} else if (a.isNew()) {
				a.setChannelInfo(getDataSnapshotState().getTraceInfo(a.getChannels()));
				annotationsToSave.add(a);
			}
		}
		Set<Annotation> annotationsToDelete = getDataSnapshotState().getDeletedAnnotationList();
		
		Set<AnnotationGroup<Annotation>> layersToDelete = getDataSnapshotState().getDeletedAnnotationLayerList();
		
		UpdateSavedAnnotations<Annotation> act = 
			new UpdateSavedAnnotations<Annotation>(getDataSnapshotState().getSnapshotID(), getDataSnapshotState(),
					"layer", new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, "add", annotationsToSave), 
					new AnnotationGroup<Annotation>(AnnotationScheme.defaultScheme, "delete", annotationsToDelete), 
					layersToDelete,this, 0);
		
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new UpdateSavedAnnotationsEvent(act));
	}
	
	public JsArrayDygraph getGraph() { return graph; }

	public void setShade(boolean setIt) {
		graph.setShading(setIt);
		setRefreshAnnotations();
		setRefreshGraph();
	}
	
	public void setRelativeToAverage(boolean setIt) {
		if (setIt)
			graph.setMontageAverage();
		else
			graph.clearMontageAverage();
	}
	
	public boolean doAverageRemontage() {
		return graph.getAverageRemontage();
	}
	
	public void updateRevId(final String oldId, final String newId) {
		if (getDataSnapshotState().getSnapshotID().equals(oldId))
			getDataSnapshotState().updateSnapshotID(oldId, newId);
	}
	
	@Override
	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
		if (slider != null)
			slider.setAnnotations(newAnnotations);
		refreshAnnotationsOnGraph(true);
	}

	public Map<String,String> getAnnotationTypes() {
		return getDataSnapshotState().getAnnotationTypes();
	}
	
	@Override
	public DataSnapshotViews getDataSnapshotController() {
		return snapshotController;
	}


	public void setDuration(double duration) {
		if (requestHighres != null)
			requestHighres.cancel();

		timeBox.setValue(duration,false);//((scale / 1.E6)));

	}

//	public void zoomBy(double ratio) {
//		if (requestHighres != null)
//			requestHighres.cancel();
//
//		double scale = graph.getPeriod() * ratio;
//
//		double current = getPosition();
//		timeBox.setValue(((scale / 1.E6)),false);
//		graph.setZoom(current, current + scale, false);
//		setPageWidth(scale);
//		EegZoomChangedMessage action = new EegZoomChangedMessage(panel,  
//				(long)(current), (long)scale, 0);
//		IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegZoomChangedEvent(action));
//	}

	@Override
	public double getPosition() {
		if (graph == null)
			return 0;
		else
			return graph.getPosition();
	}
	
	public double getPeriod() {
		return graph.getPeriod();
	}
	
	@Override
	public native void debug(String s) /*-{
		if (console && console.debug)
			  console.debug(s);
	}-*/;
	
	@Override
	public void setRefreshAnnotations() {
//		debug("Marked to refresh annotations");
		refreshAnnotations = true;
	}
	
	@Override
	public void setRefreshGraph() {
//		debug("Marked to refresh graph");
		refreshGraph = true;
	}
	
	@Override
	public void refresh() {
		if (refreshGraph && !refreshAnnotations) {
			graph.refresh();
		} else
		if (refreshGraph || refreshAnnotations) {
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRefreshEvent(handler.model.getSnapshotID()));
		  
//			refreshAnnotationsOnGraph(true);
		} 
		refreshAnnotations = false;
		refreshGraph = false;
	}


//	@Override
	public void registerPrefetchListener(Prefetcher p) {
		prefetch = p;
	}

	public String getStudyID() {
		return getDataSnapshotState().getSnapshotID();
	}
	
	public List<Boolean> getCurIsVisible() {
		JsArrayBoolean visArr = graph.getVisibility();
		List<Boolean> arr = new ArrayList<Boolean>();
		for (int i=0;i<visArr.length();i++){
			arr.add(visArr.get(i));
		}
		return arr;
	}



	public void goTo(EEGPlace place) {
		if (place.getPosition() >= 0) {
			String val = DataConversion.getRoundedValue((place.getPosition()
					//- snapshotController.getSnapshotStart() /*studyStartTime*/
					) / 1E6);
			posBox.setText(val);
			slider.setCurrentValue(place.getPosition(), false);
		}
		
		if (place.getWidth() > 0) {	
			graph.setZoom(place.getPosition(), place.getPosition() + place.getWidth(), false);
			setPageWidth(place.getWidth());
		}
		
    	if (place.getChannelsEnabled() != null && place.getChannelsEnabled().length > 0) {
    		graph.showThese(place.getChannelsEnabled());
    	}
    	
    	if (place.getFilters() != null && !place.getFilters().isEmpty())
    		filters = place.getFilters();
    	
    	setAutosize(place.getAutoScale());
		graph.setYScale(place.getScaleFactor());
	
		setRefreshGraph();
		setRefreshAnnotations();
		visFocusPanel.setFocus(true);
	}

	public double getRealScaleFactor() {
	  if (graph != null) {
		return graph.getYScale();} else return 1;
	}
	
	public boolean doAutoScale() {
	  if (graph != null) {
		return graph.getAutosizeY();}else return false;
	  
	}
	
	@Override
	public void hideGraph() {
//		graph.getElement().getStyle().setDisplay(Display.NONE);
	}
	
	public void showGraph() {
//		graph.getElement().getStyle().setDisplay(Display.INLINE);
	}
	
	@Override
	public void resetFocus() {
//		graph.getElement().getStyle().setDisplay(Display.INLINE);
		visFocusPanel.setFocus(true);
		internalFocus = 0;
	}

	@Override
	public String getPanelType() {
		return NAME;//PanelType.EEG;
	}

	public String getSelectedChannel() {
		return graph.getSelectedLabel();
	}

	@Override
	public PanelDescriptor getDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlace(WindowPlace place) {
		// TODO Auto-generated method stub
		
	}
	
	public EEGViewerPanel getPanel() {
		return panel;
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
//		GWT.log("EEGPane.getDataSnapshotState() == " + snapshotController.getState());
		return snapshotController.getState();
	}
	
	private double getMaxRequestMicros() {
		if (maxReqHzChsSecs.isPresent()) {
			final double maxReqSecs = maxReqHzChsSecs.get()/(getDataSnapshotState().getSampleRateHz() * snapshotController.getChannelNames().size());
			return maxReqSecs * 1e6;
		}
		return snapshotController.getState().getDurationMicros();
	}

	@Override
	public void close() {
		handler.unbind();
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getSignal();
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public String getTitle() {
		return "EEG-pane";
	}
	
	public ImageResource getIcon() {
		return null;
	}
	
	@Override
	public double getWindowWidth() {
		return graph.getWindowWidth();
	}
	
	@Override
	public void removeStatusMessage() {
		parent.remove(textMessage);
	}

	
	@Override
	public void showStatusMessage(String msg) {
		parent.remove(textMessage);
		parent.add(textMessage);
		parent.setWidgetTopHeight(textMessage, 40, Unit.PCT, 140, Unit.PX);
		parent.setWidgetLeftRight(textMessage, 30, Unit.PCT, 30, Unit.PCT);
		textMessage.addStyleName("status");
		
		textMessage.setHTML(msg);
	}

	@Override
	public void addNewData(JsArray<JsArrayInteger> data, boolean refresh, boolean isMinMax) {
		log(3, "Adding new data to display at " + graph.getPosition());
		graph.addNewData(data, refresh, isMinMax);
	}
	
	@Override
	public void setRequestPeriod(double p) {
		period = p;
	}
	
	@Override
	public double getRequestPeriod() {
		return period;
	}
	
	@Override
	public List<DisplayConfiguration> getFilters() {
		return filters;
	}
	
	@Override
	public void setFilters(List<DisplayConfiguration> filt) {
		filters = filt;
	}
	
	@Override
	public void clearFirstRequest() {
		removeStatusMessage();
	}
	
//	public void setPositionText(String str) {
//		posBox.setText(str);
//	}
	
	@Override
	public double getVoltageText(int row) {
		return (Double)uVoltMmBox.getModel().get(row);
	}
	
	@Override
	public double getTimeText(int row) {
		return (Double)timeBox.getModel().get(row);
	}
	
	@Override
	public void setVoltageValue(double val) {
		GWT.log("Setting Voltbox to:" + val);
		uVoltMmBox.setValue(val,false);
	}
	
	@Override
	public void setTimeValue(double val) {
		timeBox.setValue(val,false);
	}

	@Override
	public boolean hasFocus() {
		return internalFocus == 0;
	}
	
	@Override
	public void incFocus() {
		internalFocus++;
	}
	
	@Override
	public void setZoom(double left, double right, boolean redraw) {
		graph.setZoom(left, right, redraw);
	}

	@Override
	public EEGViewerPanel getHostPanel() {
		return panel;
	}
	
	@Override
	public HasAllKeyHandlers getKeyHandler() {
		return visFocusPanel;
	}
	
	@Override
	public void showThese(boolean[] enabled) {
		GWT.log("SHOWTHESE: "+enabled);
		graph.showThese(enabled);
		
		
		
	}

//	@Override
//	public FullResolutionDataHandler getFullDataHandler() {
//		return theFullDataHandler;
//	}
//
//	@Override
//	public PreviewDataHandler getPreviewDataHandler() {
//		return theDataHandler;
//	}

//	@Override
//	public String getUser() {
//		return clientFactory.getUserId();
//	}

	@Override
	public void setAnnotationDefinitions(List<AnnotationDefinition> ann) {
		if (ann != null)
			log(3, getWindowTitle() + " registered " + ann.size() + " annotations");
		else
			log(3, getWindowTitle() + " got null annotations");
	}

	@Override
	public void initPosition(double start, double width) {
		graph.initPosition(start, start + width);
	}

	@Override
	public MenuItem addMenuItemToToolbar(String label, Command cmd) {
		return toolbar.addMenuOption(label, cmd);
	}

	@Override
	public MenuItem addMenuHtmlToToolbar(String html, Command cmd) {
		return toolbar.addMenuOptionHtml(html, cmd);
	}

	@Override
	public MenuItem addSubMenuToToolbar(String label, MenuBar menu) {
		return toolbar.addSubMenu(label, menu);
	}
	
//	@Override
	public MenuBar getFavorites() {
		return favorites;
	}
	
//	@Override
	public MenuBar getRecents() {
		return recents;
	}
	
	@Override
	public void addMenuSpacer() {
		toolbar.addMenuSpacer();
	}
	
	@Override
	public void invertSignal(boolean inv) {
		graph.invertSignal(inv);
	}
	
	@Override
	public boolean isInvertedSignal() {
		return graph.isInvertedSignal();
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		if (getDataSnapshotState() == null && model.getTraceInfo() != null && model.getTraceInfo().size() > 0)
			init(model.getTraceInfo(), null);
	}
	
	@Override
	public String getWindowTitle() {
		return getTitle() + title;
	}
	
	public boolean isSearchable() {
		return false;
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}
	
	@Override
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	public SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
	

	private static int caseInsensitiveIndexOf(List<String> strings, String key) {
		int foundAt = -1;
		int i = 0;
		for (final String string : strings) {
			if (key.equalsIgnoreCase(string)) {
				foundAt = i;
				break;
			}
			i++;
		}
		return foundAt;
	}
}
