/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.presenters;

import java.util.Collection;
import java.util.Set;

import edu.upenn.cis.db.mefview.Pluggable;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public interface Presenter extends Pluggable {
	public interface Display {
		public void addPresenter(Presenter pres);
		public Presenter getPresenter();
		
		public void log(int level, String string);
	}
	
	Presenter create(
			final ClientFactory clientFactory,
			final AppModel controller,
			final DataSnapshotViews model, 
			final PresentableMetadata moreMetadata,
			boolean writePermissions);
	
	void setDisplay(Display display);
	
	public String getType();

	public void bind(Collection<? extends PresentableMetadata> selected);

	public void unbind();
	
	public Set<String> getSupportedDataTypes();

}
