/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SearchServicesAsync;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.Search;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class StudySearch extends Composite {
	// ListBox imageTypes = new ListBox(true);
	ListBox seizureTypes = new ListBox(true);
	ListBox surgeryTypes = new ListBox(true);
	ListBox postopTypes = new ListBox();
	ListBox locationTypes = new ListBox(true);
	ListBox electrodeTypes = new ListBox(true);
	ListBox contactTypes = new ListBox(true);
	TextBox etiology = new TextBox();
	FocusPanel fp = new FocusPanel();
	DockLayoutPanel dp = new DockLayoutPanel(Unit.PX);
//	HorizontalPanel hp = new HorizontalPanel();
	VerticalPanel east = new VerticalPanel();
	VerticalPanel west = new VerticalPanel();
	HorizontalPanel counts = new HorizontalPanel();
	VerticalPanel patient = new VerticalPanel();
	HorizontalPanel ages = new HorizontalPanel();
	HorizontalPanel sides = new HorizontalPanel();
	VerticalPanel operations = new VerticalPanel();
	Button searchButton = new Button("Search");
	CheckBox cbLeft = new CheckBox("Left");
	CheckBox cbRight = new CheckBox("Right");
//	Button replaceButton = new Button("Replace");
	Button cancelButton = new Button("Cancel");
	CheckBox cbMale = new CheckBox("Male");
	CheckBox cbFemale = new CheckBox("Female");
	TextBox from = new TextBox();
	TextBox to = new TextBox();
	TextBox sampleRate = new TextBox();
	TextBox fromAge = new TextBox();
	TextBox toAge = new TextBox();
	DialogBox nothing = new DialogBox();
	CheckBox cbReplace = new CheckBox("Replace results?");
	DialogBox parent;
	boolean wasShowingInstructions = false;

	public StudySearch(final String user, 
			final SearchServicesAsync searcher, //final ListDataProvider<SearchResult> studies, 
//			final CellList<SearchResult> studyList,
//			final PagedTable<SearchResult> studyList,
			final Button mainSearchButton, 
			final ClientFactory clientFactory,
			final DialogBox parent) {
		super();
		
		//setTitle("Search for Studies' Data Snapshots");
		//setText("Search for Studies' Data Snapshots");
		
		this.parent = parent;
		
//		imageTypes.addItem("3D Rendering");
//		imageTypes.addItem("CT");
//		imageTypes.addItem("FMRI");
//		imageTypes.addItem("MEG");
//		imageTypes.addItem("MRI");
//		imageTypes.addItem("MRS");
//		imageTypes.addItem("PET");
//		imageTypes.addItem("Ictal Spect");
//		imageTypes.addItem("Digital Pictures");
//		imageTypes.addItem("Electrode Map");
		
		seizureTypes.addItem("Generalized");
		seizureTypes.addItem("Generalized/Absence");
		seizureTypes.addItem("Generalized/Tonic");
		seizureTypes.addItem("Generalized/Clonic");
		seizureTypes.addItem("Generalized/Myoclonic");
		seizureTypes.addItem("Generalized/Atonic");
		seizureTypes.addItem("Generalized/Tonic-clonic");
		seizureTypes.addItem("Partial");
		seizureTypes.addItem("Partial/Simple");
		seizureTypes.addItem("Partial/Complex");
		seizureTypes.addItem("Partial/With secondary generalization");
		seizureTypes.addItem("Non-epileptic");
		
		surgeryTypes.addItem("Device implant");
		surgeryTypes.addItem("Hemispherectomy");
		surgeryTypes.addItem("Lesionectomy");
		surgeryTypes.addItem("Lobectomy");
		surgeryTypes.addItem("Shunt");
		surgeryTypes.addItem("Subpial Transection");
		surgeryTypes.addItem("Tumor Resection");
		
		postopTypes.addItem("Class 1");
		postopTypes.addItem("Class 1a");
		postopTypes.addItem("Class 2");
		postopTypes.addItem("Class 3");
		postopTypes.addItem("Class 4");
		postopTypes.addItem("Class 5");
		postopTypes.addItem("Class 6");

		east.add(new HTML("<b>Study data</b>"));
		///east.add(new HTML("&nbsp;"));
		// east.add(new Label("With images of type:"));
		// east.add(imageTypes);
		east.add(new HTML("&nbsp;"));
		east.add(new Label("With seizure counts:"));
		east.add(counts);
//		east.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		from.setMaxLength(3);
		from.setWidth("40px");
		counts.add(from);
		Label toLabel = new Label("-");
		toLabel.setWidth(toLabel.getOffsetWidth() * 2 + "px");
		counts.add(toLabel);
		to.setMaxLength(4);
		to.setWidth("40px");
		counts.add(to);
		east.add(new HTML("&nbsp;"));
		Label sample = new Label("With sampling rate:");
		east.add(sample);
		sampleRate.setMaxLength(5);
		sampleRate.setWidth("100px");
		east.add(sampleRate);
		dp.addEast(east, 150);
		dp.addWest(west, 150);
//		dp.addSouth(hp, 50);
		dp.setSize("550px", "600px"); // east.getOffsetHeight() + 50 + "px");//
		
		patient.add(new HTML("<b>Patient demographics</b>"));
//		patient.add(new HTML("&nbsp;"));
		patient.add(ages);
		ages.add(new Label("Onset age:"));
		fromAge.setMaxLength(3);
		fromAge.setWidth("40px");
		ages.add(fromAge);
		Label toLabel2 = new Label("-");
		toLabel2.setWidth(toLabel2.getOffsetWidth() * 2 + "px");
		ages.add(toLabel2);
		toAge.setMaxLength(3);
		toAge.setWidth("40px");
		ages.add(toAge);
//		patient.add(new HTML("&nbsp;"));
		HorizontalPanel mf = new HorizontalPanel();
		mf.add(new Label("Gender:"));
//		RadioButton rbMale = new RadioButton("sex", "Male");
//		RadioButton rbFemale = new RadioButton("sex", "Female");
//		mf.add(rbFemale);
//		mf.add(rbMale);
		mf.add(cbFemale);
		mf.add(cbMale);
		patient.add(mf);
//		patient.add(new HTML("&nbsp;"));
		patient.add(new Label("Seizure history:"));
		patient.add(seizureTypes);

//		HorizontalPanel et = new HorizontalPanel();
//		patient.add(et);
//		et.add(new Label("Etiology:"));
//		et.add(etiology);
//		etiology.setEnabled(false);
		
		patient.add(new HTML("&nbsp;"));
		patient.add(new HTML("<b>Contact Group Placement and Types</b>"));
//		patient.add(new HTML("&nbsp;"));
		patient.add(sides);
		sides.add(new Label("Side:"));
		sides.add(cbLeft);
		sides.add(cbRight);
//		RadioButton rbLeft = new RadioButton("side", "Left");
//		RadioButton rbRight = new RadioButton("side", "Right");
//		sides.add(rbLeft);
//		sides.add(rbRight);
		patient.add(new Label("Locations:"));
//		CheckBox cbFrontal = new CheckBox("Frontal");
//		CheckBox cbInterhemispheric = new CheckBox("Interhemispheric");
//		CheckBox cbOccipital = new CheckBox("Occipital");
//		CheckBox cbParietal = new CheckBox("Parietal");
//		CheckBox cbTemporal = new CheckBox("Temporal");
//		patient.add(cbFrontal);
//		patient.add(cbInterhemispheric);
//		patient.add(cbOccipital);
//		patient.add(cbParietal);
//		patient.add(cbTemporal);
		locationTypes.addItem("Frontal");
		locationTypes.addItem("Interhemispheric");
		locationTypes.addItem("Occipital");
		locationTypes.addItem("Parietal");
		locationTypes.addItem("Temporal");
		patient.add(locationTypes);
		
		electrodeTypes.addItem("Strip");
		electrodeTypes.addItem("Grid");
		electrodeTypes.addItem("Depth");
		for (final ContactType contactType : ContactType.values()) {
			contactTypes.addItem(contactType.toString());
		}
		
		HorizontalPanel elec = new HorizontalPanel();
//		patient.add(new HTML("&nbsp;"));
		patient.add(elec);
		VerticalPanel el = new VerticalPanel();
		elec.add(el);
		elec.add(new HTML("&nbsp;"));
		VerticalPanel co = new VerticalPanel();
		elec.add(co);
		el.add(new Label("Electrodes:"));
		el.add(electrodeTypes);
		co.add(new Label("Contacts:"));
		co.add(contactTypes);
		
		electrodeTypes.setEnabled(false);
		
		Image mag = new Image(ResourceFactory.getMagnifier());//"images/magnifying_glass.png");
		mag.setPixelSize(100, 100);
		west.add(mag);
		west.add(operations);
		west.add(new HTML("&nbsp;"));
		operations.add(new HTML("<b>Intervention</b>"));
		operations.add(new HTML("&nbsp;"));
		operations.add(new Label("Surgery types:"));
		operations.add(surgeryTypes);
		operations.add(new HTML("&nbsp;"));
		operations.add(new Label("ILAE postop rating:"));
		operations.add(postopTypes);
		postopTypes.setEnabled(false);
//		west.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		
		surgeryTypes.setEnabled(false);
		
		
		
		dp.add(patient);
		
		VerticalPanel vp = new VerticalPanel();
		HorizontalPanel buttons = new HorizontalPanel();
		east.add(vp);
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
//		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		HTML h = new HTML("&nbsp;");
		vp.add(h);
		vp.setCellHeight(h, "90%");
		vp.add(cbReplace);
		cbReplace.setValue(true);
		vp.add(buttons);
		vp.setCellVerticalAlignment(buttons, HasVerticalAlignment.ALIGN_BOTTOM);
//		east.setCellHeight(vp, "90%");
		east.setCellVerticalAlignment(vp, HasVerticalAlignment.ALIGN_BOTTOM);
//		buttons.add(replaceButton);
		buttons.add(searchButton);
		buttons.add(cancelButton);
//		searchButton.setEnabled(false);
//		replaceButton.setEnabled(false);
		final boolean prev = (mainSearchButton == null) ? false : mainSearchButton.isEnabled();
		cancelButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(prev);
//				if (showInstructions && wasShowingInstructions)
//					instruct.show();
				if (parent != null)
					parent.hide();
			}
			
		});
		
		searchButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				cancelButton.setEnabled(false);
				searchButton.setEnabled(false);
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(false);
				
//				if (instruct.isVisible()) {
//					wasShowingInstructions = showInstructions;//true;
//					instruct.hide();
//				}
				
				if (parent != null)
					parent.hide();
				
				searcher.getSnapshotsFor(clientFactory.getSessionModel().getSessionID(), getSearchSpec(), new SecFailureAsyncCallback<Set<SearchResult>>(clientFactory) {

					public void onFailureCleanup(Throwable caught) {
						if (mainSearchButton != null)
							mainSearchButton.setEnabled(prev);
						cancelButton.setEnabled(true);
						searchButton.setEnabled(true);
					}
					
					public void onNonSecFailure(Throwable caught) {
						cancelButton.setEnabled(true);
						searchButton.setEnabled(true);
					}

					public void onSuccess(Set<SearchResult> result) {
						cancelButton.setEnabled(true);
						searchButton.setEnabled(true);
						if (result.isEmpty()) {
							nothing.setText("Sorry, no results found");
							VerticalPanel vp = new VerticalPanel();
							vp.add(new HTML("Perhaps your search was too restrictive, or this feature is not available."));
							Button ok = new Button("OK");
							vp.setCellHorizontalAlignment(ok, HasHorizontalAlignment.ALIGN_CENTER);
							ok.addClickHandler(new ClickHandler() {

								public void onClick(ClickEvent event) {
									nothing.hide();
								}
								
							});
							if (mainSearchButton != null)
								mainSearchButton.setEnabled(prev);
							vp.add(ok);
							nothing.setWidget(vp);
							nothing.center();
						} else {
							IeegEventBusFactory.getGlobalEventBus().fireEvent(new SearchResultsUpdatedEvent("(forms)", result, cbReplace.getValue()));
						}
					}});
			}});

		fp.add(dp);
		fp.addFocusHandler(new FocusHandler() {

			public void onFocus(FocusEvent event) {
				searchButton.setFocus(true);
			}
			
		});
		initWidget(fp);
		fp.setFocus(true);
	}
	
	public Search getSearchSpec() {
		Search ret = new Search();
		
		// Study images
//		for (int i = 0; i < imageTypes.getItemCount(); i++)
//			if (imageTypes.isItemSelected(i))
//				ret.getImageTypes().add(imageTypes.getItemText(i));
		
		// Study seizure counts
		if (from.getText().length() > 0)
			ret.setMinRecordedSzCnt(Integer.valueOf(from.getText()));
			//ret.setMinAnnotationCount(Integer.valueOf(from.getText()));
		if (to.getText().length() > 0)
			ret.setMaxRecordedSzCnt(Integer.valueOf(to.getText()));
			//ret.setMaxAnnotationCount(Integer.valueOf(to.getText()));
		
		// Study min sampling rate
		if (sampleRate.getText().length() > 0) {
			ret.setSamplingRateMin(Double.valueOf(sampleRate.getText()));
		}
		// TODO: support max sampling rate??
		
		// Patient onset age
		if (fromAge.getText().length() > 0)
			ret.setAgeOfOnsetMin(Integer.valueOf(fromAge.getText()));
		if (toAge.getText().length() > 0)
			ret.setAgeOfOnsetMax(Integer.valueOf(toAge.getText()));
		
		if (cbFemale.getValue())
			ret.getGenders().add(Gender.FEMALE.toString());//"Female");
		if (cbMale.getValue())
			ret.getGenders().add(Gender.MALE.toString());//"Male");
		
		for (int i = 0; i < seizureTypes.getItemCount(); i++)
			if (seizureTypes.isItemSelected(i))
				ret.getSzTypes().add(seizureTypes.getItemText(i));
		
		int index = findSeizure("Generalized");
		if (seizureTypes.isItemSelected(index))
			ret.setGenSzTypes(true);
		
		index = findSeizure("Partial");
		if (seizureTypes.isItemSelected(index))
			ret.setPartialSzTypes(true);
		
		if (cbLeft.getValue())
			ret.getSides().add(Side.LEFT.toString());//"Left");
		if (cbRight.getValue())
			ret.getSides().add(Side.RIGHT.toString());//"Right");

		for (int i = 0; i < locationTypes.getItemCount(); i++)
			if (locationTypes.isItemSelected(i))
				ret.getLocations().add(locationTypes.getItemText(i));
		
		for (int i = 0; i < surgeryTypes.getItemCount(); i++)
			if (surgeryTypes.isItemSelected(i))
				ret.getSurgeryTypes().add(surgeryTypes.getItemText(i));
		
		ret.setPostopRating(postopTypes.getItemText(postopTypes.getSelectedIndex()));
		
		for (int i = 0; i < contactTypes.getItemCount(); i++) 
			if (contactTypes.isItemSelected(i))
				ret.getContactTypes().add(contactTypes.getItemText(i));
		
		return ret;
	}
	
	private int findSeizure(String typ) {
		for (int i = 0; i < seizureTypes.getItemCount(); i++)
			if (seizureTypes.getItemText(i).equals(typ))
				return i;
		
		return -1;
	}
}
