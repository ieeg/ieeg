package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IShowTraces;
import edu.upenn.cis.db.mefview.client.events.EegChannelsChangedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.viewers.MontageChannelListPane;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.MontagePairInfo;

public class ShowTraces extends PopupPanel implements
DialogWithInitializer<IShowTraces>, IShowTraces{
	
	private MontageChannelListPane dialog;
	private VerticalPanel TracePaneWrapper = new VerticalPanel(); 
	private Button close = new Button("OK");
	private Button cancel = new Button("Cancel");
	private EEGPane display;
	private List<String> channels = new ArrayList<String>();
	
	public ShowTraces(){
		this.setAutoHideEnabled(true);
	}
	
	@Override
	public void init(final Handler responder, final Command doAfter) {
		// TODO Auto-generated method stub
		//dialog.init(channels, selected, factory);
		
		dialog.getTraceTable().getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler(){
		    
			@Override 
		    public void onSelectionChange(SelectionChangeEvent event){
		    	Set<MontagePairInfo> selPairs = dialog.getTraceTable().getSelectedItems();
		    	
		    	Set<EEGMontagePair> pairs = new HashSet<EEGMontagePair>();
		    	for (MontagePairInfo m: selPairs)
		    		pairs.add(m.getPair());
		    	
		    	responder.channelsChanged(pairs);
		      }
		});
		
		
	    cancel.addClickHandler(new ClickHandler() {

	        public void onClick(ClickEvent event) {
	          hide();
	          display.resetFocus();
	        }

	      });
	    
	    close.addClickHandler(new ClickHandler() {

	        public void onClick(ClickEvent event) {
	        	

	          boolean[] results = dialog.getSelected();
	          boolean isAtLeastOne = false;
	          for (boolean b: results)
	            isAtLeastOne |= b;

	          if (isAtLeastOne) {
	          	IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegChannelsChangedEvent(display.getPanel(), channels, dialog.getSelected()));
	            hide();
	            display.resetFocus();
	          } else {
	            Dialogs.messageBox("No channels selected", "Please select at least one channel");
	          }
	        }

	      });
		
	}

	@Override
	public void setPane(EEGPane pane) {
		this.display = pane;
		
	}

	@Override
	public void open() {
		center();
		
	}

	@Override
	public void close() {
		hide();
		
	}

	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createDialog() {

		setStyleName("ChanSelectDialog");

	    dialog = new MontageChannelListPane();
	    setWidget(TracePaneWrapper);

	    dialog.setSize("200px", "280px");

	    /* Add Button in bottom of DialogBox */
//	    HorizontalPanel buttonBar = new HorizontalPanel();
//	    buttonBar.add(close);
//	    buttonBar.add(cancel);

	    TracePaneWrapper.add(dialog);
//	    TracePaneWrapper.add(buttonBar);
	
		
	}

	@Override
	public void initialize(DialogInitializer<IShowTraces> initFn) {
		initFn.init(this);
	}

	@Override
	public void reinitialize(DialogInitializer<IShowTraces> initFn) {
		initFn.reinit(this);
	}

	@Override
	public void initTracePane(EEGMontage montage, List<Boolean> isVisible, ClientFactory factory) {
		GWT.log("IN INIT SHOWTRACES");
		dialog.init(montage, isVisible, factory);
		
	}
	

}
