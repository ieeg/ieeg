/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.Stack;

import org.gwt.advanced.client.ui.widget.combo.ComboBoxChangeEvent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasKeyDownHandlers;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.DataLoadHandler;
import edu.upenn.cis.db.mefview.client.IEventBus;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.DownloadTracesDialog;
import edu.upenn.cis.db.mefview.client.events.AnnotationRangeReceivedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRangeRequestedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.EegChannelsChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegFilterChangedEvent;
import edu.upenn.cis.db.mefview.client.events.EegZoomChangedEvent;
import edu.upenn.cis.db.mefview.client.events.RequestRefreshAnnotationsEvent;
import edu.upenn.cis.db.mefview.client.events.SavedSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotReceivedEvent;
import edu.upenn.cis.db.mefview.client.messages.AnnotationRangeMessage;
import edu.upenn.cis.db.mefview.client.messages.EegZoomChangedMessage;
import edu.upenn.cis.db.mefview.client.messages.SavedSnapshotMessage;
import edu.upenn.cis.db.mefview.client.messages.SnapshotDataMessage;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestAnnotations;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.IEEGToolbar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class EEGPresenter implements IEEGPresenter,
SnapshotReceivedEvent.Handler, AnnotationRangeReceivedEvent.Handler, SavedSnapshotEvent.Handler,
EegFilterChangedEvent.Handler, AnnotationRefreshEvent.Handler, EegChannelsChangedEvent.Handler,
//ReceivedAnnotationDefinitionsEvent.Handler, 
RequestRefreshAnnotationsEvent.Handler
{
	public interface Display extends IEEGPresenter.Display {
	    void getData(final String user, final String dataSnapshotRevId, final List<String> traceIds,
		        final double start, final double width, final double samplingPeriod, 
		        final List<DisplayConfiguration> filter, boolean dontCache);
	    public void requestDataAgain();

	    MenuItem addMenuItemToToolbar(String label, Command cmd);
	    MenuItem addMenuHtmlToToolbar(String html, Command cmd);
	    MenuItem addSubMenuToToolbar(String label, MenuBar menu);
	    void addMenuSpacer();
	    
	    public JsArray<JsArrayInteger> refreshBufferFromSeries(TimeSeries[] results);
	    public JsArray<JsArrayInteger> refreshBufferFromJson(String json);

	    public void prefetchAsNeeded(long start, long end, int direction);

	    public void jumpTo(double pos);
	    public void prevPage();
	    public void nextPage();
	    public void jumpToNextAnnotation();
	    public void jumpToPrevAnnotation();
	    public void hideGraph();
	    public HasText getInvertSignalText();
	};

  Stack<HandlerRegistration> handlers = new Stack<HandlerRegistration>();

  MenuItem invertItem;

  Display display;
  Timer requestHighres = null;
  String snapshotId;
  DataSnapshotViews controller;
  DataSnapshotModel model;
  ClientFactory clientFactory;

  
  boolean firstRequest = true;

  ServerAccess.Prefetcher prefetch;
	PreviewDataHandler theDataHandler;
	FullResolutionDataHandler theFullDataHandler;
	AsyncDataHandler theAsyncDataHandler;
	

  public EEGPresenter(DataSnapshotViews controller, ClientFactory factory) {
    this.controller = controller;
    this.model = controller.getState();
    this.snapshotId = model.getSnapshotID();
    clientFactory = factory;
  }

  PreviewDataHandler getPreviewDataHandler() {
	  if (theDataHandler == null)
		  theDataHandler = new PreviewDataHandler(this);
	  return theDataHandler;
  }

  public FullResolutionDataHandler getFullDataHandler() {
	  if (theFullDataHandler == null)
		  theFullDataHandler = new FullResolutionDataHandler(this);
	  return theFullDataHandler;
  }

  AsyncDataHandler getAsyncDataHandler() {
	  if (theAsyncDataHandler == null)
		  theAsyncDataHandler = new AsyncDataHandler(this);
	  
	  return theAsyncDataHandler;
  }

  void setKeyPressHandlers(HasKeyPressHandlers visFocusPanel) {
    visFocusPanel.addKeyPressHandler(new KeyPressHandler() {

      public void onKeyPress(KeyPressEvent event) {
        if (!display.hasFocus())
          return;

        if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_LEFT ||
            event.getCharCode() == 'p') {
          event.preventDefault();
          display.prevPage();
        } else if (event.getNativeEvent().getKeyCode() == (KeyCodes.KEY_RIGHT) ||
            event.getCharCode() == 'n') {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.nextPage();
        } else if (//event.getCharCode() == '.' || 
            event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEDOWN) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.jumpToNextAnnotation();
        } else if (//event.getCharCode() == ',' || 
            event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEUP) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.jumpToPrevAnnotation();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_UP) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.scaleUp();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_DOWN) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.scaleDown();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_BACKSPACE) {
          event.preventDefault();
          display.restoreView();
//        } else if (event.getCharCode() == 't') {
//          event.preventDefault();
//          display.tracePopup();
          //				} else if (event.getCharCode() == 'z' || event.getCharCode() == 'm' || 
          //						event.getCharCode() == 'a' || event.getCharCode() == 'r' ||
          //						event.getCharCode() == 'f' || event.getCharCode() == 'h' || 
          //						event.getCharCode() == 's') {
          //					setAnnotationMode(event.getCharCode());
          //					event.preventDefault();
        }

      }

    });
  }

  public void setKeyDownHandlers(HasKeyDownHandlers visFocusPanel) {
    visFocusPanel.addKeyDownHandler(new KeyDownHandler() {

      public void onKeyDown(KeyDownEvent event) {
        if (!display.hasFocus())
          return;
        if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_LEFT ||
            event.isLeftArrow()) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.prevPage();
        } else if (event.getNativeEvent().getKeyCode() == (KeyCodes.KEY_RIGHT) || 
            event.isRightArrow()) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.nextPage();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEDOWN) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.jumpToNextAnnotation();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_PAGEUP) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.jumpToPrevAnnotation();
        } else if (event.isUpArrow() || 
            event.getNativeEvent().getKeyCode() == KeyCodes.KEY_UP) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.scaleUp();
        } else if (event.isDownArrow() || 
            event.getNativeEvent().getKeyCode() == KeyCodes.KEY_DOWN) {
          if (requestHighres != null)
            requestHighres.cancel();
          event.preventDefault();
          display.scaleDown();
        } else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_BACKSPACE) {
          event.preventDefault();
          display.restoreView();
        }
      }

    });

  }

  void initHandlers(IEEGToolbar toolbar, double maxFreq) {

    display.initToolbar(toolbar, maxFreq,
        (new FocusHandler() {
          public void onFocus(FocusEvent event) {
            display.incFocus();
          }
        }),

        (new BlurHandler() {
          public void onBlur(BlurEvent arg0) {
            display.resetFocus();
          }
        }),
        // Voltage handlers
        new ChangeHandler() {

      @Override
      public void onChange(ChangeEvent event) {
        if (event instanceof ComboBoxChangeEvent) {
          int row = ((ComboBoxChangeEvent)event).getRow();

          double val = display.getVoltageText(row);
          display.setVoltageValue(val);
          display.setScale(val);
          display.resetFocus();
        }
      }

    },
    new NumericEnterHandler() {
      @Override
      public void handleEnterRequest(double val) {
        display.setScale(val);
        display.resetFocus();
      }

    },
    // Time handlers
    new ChangeHandler() {

      @Override
      public void onChange(ChangeEvent event) {
        if (event instanceof ComboBoxChangeEvent) {
          int row = ((ComboBoxChangeEvent)event).getRow();

          double value = (Double)display.getTimeText(row);
          //							GWT.log("Time change to " + value);
          if (requestHighres != null)
            requestHighres.cancel();

          double scale = value * 1.E6;

          if (scale < 1.E6 / (model.getSampleRateHz() * 100)) {
            scale = model.getSampleRateHz() * 100;
            display.setTimeValue(scale / 1.E6);
          }
          double current = display.getPosition();
          display.setZoom(current, current + scale, false);
          display.setPageWidth(scale);
          display.resetFocus();
          //							if (newEEG != null) {
          EegZoomChangedMessage action = new EegZoomChangedMessage(display.getHostPanel(), 
              (long)(current), (long)scale, 0);
          IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegZoomChangedEvent(action));
          //							}
        }
      }

    },
    new NumericEnterHandler() {

      @Override
      public void handleEnterRequest(double value) {
        if (requestHighres != null)
          requestHighres.cancel();

        //						GWT.log("ENTER change to " + value);
        double scale = value * 1.E6;

        double current = display.getPosition();
        display.setZoom(current, current + scale, false);
        display.setPageWidth(scale);
        display.resetFocus();
        //						if (newEEG != null) {
        EegZoomChangedMessage action = new EegZoomChangedMessage(display.getHostPanel(), 
            (long)(current), (long)scale, 0);
        IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegZoomChangedEvent(action));
        //						}	
        //							internalFocus--;
      }

    },
    // Pos Handlers
    new NumericEnterHandler() {

      @Override
      public void handleEnterRequest(double value) {
        if (requestHighres != null)
          requestHighres.cancel();
        display.jumpTo(value * 1E6/* + model.getSnapshotStart()*/);//studyStartTime);
        display.resetFocus();
      }

    },
    // Click to refresh
    new ClickHandler() {

      @Override
      public void onClick(ClickEvent event) {
        display.setRefreshGraph();
        display.requestDataAgain();
      }

    },
    //Invert handler
    new ClickHandler() {

		@Override
		public void onClick(ClickEvent event) {
			doInvert(display.getInvertSignalText());
		}
    	
    }
        );
    addScrollHandlers();
    rereadAnnotations();
    
  }

  public void setDisplay(Presenter.Display display) {
    this.display = (Display)display;
  }

  void bind() {
	  GWT.log("Binding EEGPresenter with display: " + display.getWindowTitle());
	  
    setKeyDownHandlers(display.getKeyHandler());
    setKeyPressHandlers(display.getKeyHandler()); 
    
    // Invert view
    invertItem = display.addMenuItemToToolbar("Invert signal",
        new Command() {

      @Override
      public void execute() {
        doInvert(invertItem);
      }

    });
    display.addMenuItemToToolbar("Export to CSV", new Command() {

      @Override
      public void execute() {
        final DownloadTracesDialog downloadDialog = new DownloadTracesDialog(clientFactory, 
//            model.getTraceInfo(), 
        		model,
            model.getSnapshotID(), display.getHostPanel(), true);
        downloadDialog.center();
        downloadDialog.show();
      }

    });

    IEventBus bus = IeegEventBusFactory.getGlobalEventBus();
    bus.registerHandler(SnapshotReceivedEvent.getType(), this);
    bus.registerHandler(AnnotationRangeReceivedEvent.getType(), this);
    bus.registerHandler(SavedSnapshotEvent.getType(), this);
    bus.registerHandler(EegFilterChangedEvent.getType(), this);
    bus.registerHandler(AnnotationRefreshEvent.getType(), this);
    bus.registerHandler(EegChannelsChangedEvent.getType(), this);
//    bus.registerHandler(ReceivedAnnotationDefinitionsEvent.getType(), this);   
    model.setAnnotationDefinitions(clientFactory.getSessionModel().getServerConfiguration().getAnnotationDefinitions());
    display.setAnnotationDefinitions(clientFactory.getSessionModel().getServerConfiguration().getAnnotationDefinitions());
    
    bus.registerHandler(RequestRefreshAnnotationsEvent.getType(), this);
  
    GWT.log(display.getWindowTitle() + " is bound");
  }

  public void unbind() {
    GWT.log(display.getWindowTitle() + " is unbinding");
    IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
  }

  @Override
  public void onAnnotationRangeReceived(AnnotationRangeMessage action) {
    if (action.getRequest().snapshotID.equals(model.getSnapshotID())) {
      GWT.log("Window " + display.getWindowTitle() + " received annotations");

      HashMap<String,List<Annotation>> annotationGroups = new HashMap<String,List<Annotation>>(); 

      SortedSet<Annotation> annotations = action.getRelevantAnnotations().getAnnotations();

      // Sort annotations by Annotation Groups.
      for (Annotation a: annotations) {
        String name = a.getLayer();

        if (name == null || name.isEmpty())
          a.setLayer("Default");

        List<Annotation> annHere = annotationGroups.get(a.getLayer());

        if (annHere == null) {
          annHere = new ArrayList<Annotation>();
          annotationGroups.put(a.getLayer(), annHere);
        }
        annHere.add(a);
      }

      // Create annotationGroups objects. Use default Style Names: e.g. "Scheme [x]"
      if (!annotations.isEmpty()) {

        int i = 1;
        for (String group: annotationGroups.keySet()) {
          AnnotationScheme scheme = clientFactory.getSessionModel().getUserPreferences().getAnnotationScheme("Scheme " + i);

          AnnotationGroup<Annotation> thisGroup = new AnnotationGroup<Annotation>(scheme,
              group, annotationGroups.get(group)); 
          model.getAnnotationModel().addOrExtendGroup(thisGroup);
          i++;
        }
        
        // Update Display
        IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRefreshEvent(model.getSnapshotID()));

      }
    }
  }

  @Override
  public void onSnapshotReceived(SnapshotDataMessage action) {
    GWT.log(display.getWindowTitle() + " handler triggered for data from " + action.getRequest().snapshotID);
    try {
      if (!action.getRequest().snapshotID.equals(model.getSnapshotID())) {
        GWT.log(display.getWindowTitle() + " is ignoring data for " + action.getRequest().snapshotID);
        return;
      }

      long start2 = Long.MAX_VALUE;
      long end2 = Long.MIN_VALUE;
      double samplingPeriod = Double.MAX_VALUE;

      for (int i = 0; i < action.getDataSpans().size(); i++) {
        TimeSeriesSpan span = action.getDataSpans().get(i);
        if (start2 > span.getStartTime())
          start2 = span.getStartTime();
        if (end2 < span.getEnd())
          end2 = (long)(span.getEnd());

        if (span.getPeriod() < samplingPeriod)
          samplingPeriod = span.getPeriod();
      }

      ArrayList<String> channels = new ArrayList<String>();
//      for (INamedTimeSegment ch: action.getRequest().channels){
      for (TimeSeriesSpanSpecifier spec: action.getRequest().requestedSpans) {
        channels.add(spec.getTimeSegment().getId());
      }

      GWT.log("Window " + display.getWindowTitle() + " received data response");
      
      if (firstRequest) {
    	  GWT.log("SNAPSHOTRECEIVED FIRSTREQUEST");
        //				debug("Initial handler");
        getPreviewDataHandler().process(clientFactory.getSessionModel().getUserId(), model.getSnapshotID(), channels,
            start2, end2 - start2, samplingPeriod, 
            action.getRequest().filters, action.getDataSpans(), action.isMinMax());
      } else {
    	  GWT.log("SNAPSHOTRECEIVED FULL HANDLER");
        //				debug("Full handler");
        getFullDataHandler().process(clientFactory.getSessionModel().getUserId(), model.getSnapshotID(), channels,
            start2, end2 - start2, samplingPeriod, 
            action.getRequest().filters, action.getDataSpans(), action.isMinMax());
      }
    } catch (Exception e) {
      GWT.log("Error processing data", e);
    }
  }

  @Override
  public void onSavedSnapshot(SavedSnapshotMessage action) {
    if (action.priorAction.snapshotID.equals(model.getSnapshotID()) &&
        action.priorAction.requestor == display) {

      Dialogs.messageBox("Success!", "Annotations successfully persisted; new dataset ID is " + 
          action.snapshotID);

      model.updateSnapshotID(action.priorAction.snapshotID, action.snapshotID);
      //			model.refreshRevId(dataset, action.snapshotID);

      rereadAnnotations();
    }
  }

  @Override
  public void onFilterChanged(IEEGViewerPanel window,
      List<INamedTimeSegment> traces, List<DisplayConfiguration> filters) {
	  GWT.log("GETDATA in ONFILTERCHANGED");
    if (window == display.getHostPanel()) {
      ArrayList<String> traceIds = new ArrayList<String>();
      for (INamedTimeSegment t: traces)
        traceIds.add(t.getId());

      display.debug(model.getSnapshotID() + " filter change to:");
      for (DisplayConfiguration fp: filters)
        display.debug(fp.paramsToURL());

      display.setFilters(filters);
      display.getData(clientFactory.getSessionModel().getUserId(), model.getSnapshotID(), traceIds, display.getPosition(), 
          display.getWindowWidth() * 4, display.getRequestPeriod(), filters, 
          false);
    }
  }

  @Override
  public void onAnnotationRefresh(String datasetId) {
	  if (datasetId.equals(snapshotId)) {
		  GWT.log("Window " + display.getWindowTitle() + " refreshing annotations");
  //	  slider.setAnnotations(model.getAnnotationTimes());
    //    display.setRefreshAnnotations();
		  display.refreshAnnotations(model.getAnnotationTimes());
	  }
  }

  @Override
  public void onChannelsChanged(IEEGViewerPanel w, List<String> channels,
      boolean[] isSelected) {
    if (w == display.getHostPanel())
      display.showThese(isSelected);
  }

  @Override
  public void onSnapshotFailed(RequestSnapshotData request) {
    if (request.snapshotID.equals(model.getSnapshotID()))
      display.showStatusMessage("Unable to read time series data...");
  }

  public void rereadAnnotations() {
    int[] ids = new int[model.getChannelNames().size()];
    for (int i = 0; i < ids.length; i++)
      ids[i] = 0;
    ArrayList<TimeSeriesSpanSpecifier> reqSpans = new ArrayList<TimeSeriesSpanSpecifier>();

    List<INamedTimeSegment> reqChannels = model.getTraceInfo();

    for (INamedTimeSegment ti: reqChannels) {
      reqSpans.add(new TimeSeriesSpanSpecifier(ti, (long)display.getPosition(),
          (long)display.getPageWidth(), model.getSampleRateHz(), null));
    }

    // Get the annotations for specific time-span
    RequestAnnotations raa = new RequestAnnotations(model.getSnapshotID(), 
        model.getFriendlyName() + " annotations", reqChannels, reqSpans, ids);

    // Fire event for handling loaded annotations.
    IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRangeRequestedEvent(raa));

  }

  public void addScrollHandlers() {
    ClickHandler listClickHandler = new ClickHandler() {

      @Override
      public void onClick(ClickEvent event) {
        display.hideGraph();

      }

    };
    ClickHandler prevAnnotationHandler = (new ClickHandler() {

      public void onClick(ClickEvent event) {
        display.jumpToPrevAnnotation();
        display.resetFocus();
      }

    });

    ClickHandler leftHandler = (new ClickHandler() {

      public void onClick(ClickEvent event) {
        display.prevPage();
        display.resetFocus();
      }

    });

    ClickHandler rightHandler = (new ClickHandler() {

      public void onClick(ClickEvent event) {
        display.nextPage();
        display.resetFocus();
      }

    });
    ClickHandler nextAnnotationHandler = (new ClickHandler() {

      public void onClick(ClickEvent event) {
        display.jumpToNextAnnotation();
        display.resetFocus();
      }

    });


    ValueChangeHandler<Double> sliderHandler = (new ValueChangeHandler<Double>() {

      public void onValueChange(ValueChangeEvent<Double> event) {
        display.jumpTo(event.getValue().doubleValue());
        display.resetFocus();
      }

    });
    display.setScrollerHandlers(listClickHandler, prevAnnotationHandler, leftHandler,
        rightHandler, nextAnnotationHandler, sliderHandler);

  }

//  @Override
//  public void onReceivedAnnotationDefinitions(Presenter requester,
//      String viewerType, List<AnnotationDefinition> annotations) {
//    if (viewerType.equals(EEGViewerPanel.NAME)){
//      model.setAnnotationDefinitions(annotations);
//      display.setAnnotationDefinitions(annotations);
//    }
//  }

	@Override
	public Display getDisplay() {
		return (EEGPresenter.Display)display;
	}

	private void doInvert(HasText hasText) {
		if (!display.isInvertedSignal())
			hasText.setText("Restore signal");
		else
			hasText.setText("Invert signal");
		display.invertSignal(!display.isInvertedSignal());
		display.setRefreshGraph();
}

	@Override
	public DataSnapshotModel getModel() {
		return model;
	}

	@Override
	public Timer getRequestHighres() {
		return requestHighres;
	}

	@Override
	public void setRequestHighres(Timer t) {
		requestHighres = t;
	}

	@Override
	public String getSnapshotId() {
		return snapshotId;
	}

	@Override
	public void clearFirstRequest() {
		firstRequest = false;
	}

	@Override
	public JsArray<JsArrayInteger> refreshBufferFromSeries(TimeSeries[] results) {
		return getDisplay().refreshBufferFromSeries(results);
	}

	@Override
	public JsArray<JsArrayInteger> refreshBufferFromJson(String json) {
		return getDisplay().refreshBufferFromJson(json);
	}

	@Override
	public void onRefreshAnnotations(String snapshotId, List<AnnBlock> annotations) {
		if (snapshotId.equals(this.snapshotId))
			display.refreshAnnotations(annotations);
	}

	@Override
	public void getData(String user, String dataSnapshotRevId, List<String> traceIds, double start, double width,
			double samplingPeriod, List<DisplayConfiguration> filter, boolean dontCache) {
		getDisplay().getData(user, dataSnapshotRevId, traceIds, start, width, samplingPeriod, filter, dontCache);
	}

	@Override
	public void prefetchAsNeeded(long start, long end, int direction) {
		getDisplay().prefetchAsNeeded(start, end, direction);
	}

	@Override
	public void jumpTo(double i) {
		getDisplay().jumpTo(i);
	}
}

