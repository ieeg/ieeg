/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.MultiViewImagePresenter;
import edu.upenn.cis.db.mefview.client.plugins.images.SimpleImagePresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;

public class OpenImageAction extends PluginAction {
	
	Map<SerializableMetadata,Set<PresentableMetadata>> snapshots = new HashMap<SerializableMetadata,Set<PresentableMetadata>>();

	public static final String NAME = "View Images";
		
	static {
		PluginActionFactory.register(NAME, new OpenImageAction());
	}
	
	public OpenImageAction() {
		super(NAME, null, null, null);
	}
	
	public OpenImageAction(Set<PresentableMetadata> md, ClientFactory cf, Presenter pres) {
		super(NAME, cf, pres, null);
		// Two cases:
		//  1. set of channels with common snapshot -- open EEG, then shut down the channels
		//
		// 2. set of snapshots
		
		// Collect all of the snapshots and the particular channels
		for (PresentableMetadata m : md) {
			if (m instanceof SearchResult && !snapshots.containsKey(m)) {
				snapshots.put(m, new HashSet<PresentableMetadata>());
			} else if (m.getParent() != null) {
				if (!snapshots.containsKey(m.getParent()))
					snapshots.put(m.getParent(),
							new HashSet<PresentableMetadata>());

				snapshots.get(m.getParent()).add(m);
			}
		}
	}

	@Override
	public void execute() {
//		for (SerializableMetadata snap : snapshots.keySet()) {
//			OpenDisplayPanel req = new OpenDisplayPanel(
//					snap, true, ImageViewerPresenter.NAME, snapshots.get(snap), null);
//			IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(req));
//		}
		GWT.log("OpenImageAction: EXECUTE");
		
		for (Entry<SerializableMetadata, Set<PresentableMetadata>> entry : snapshots.entrySet()) {
			SerializableMetadata key = entry.getKey();
			List<PresentableMetadata> toShow = new ArrayList<PresentableMetadata>();
			List<PresentableMetadata> toShowStack = new ArrayList<PresentableMetadata>();
			String id = "";
			GWT.log(entry.getValue().toString());
			
			for (PresentableMetadata metadata : entry.getValue()) {
				GWT.log(metadata.getClass().toString());
				
				if (metadata instanceof FileInfo 
						&& ((FileInfo)metadata).getMetadataCategory().equals(
								BASIC_CATEGORIES.Images.name())){
					
					id = ((FileInfo)metadata).getId();
					toShow.add((FileInfo)metadata);
				} else if (metadata instanceof FileInfo 
						&& ((FileInfo)metadata).getMetadataCategory().equals(
								BASIC_CATEGORIES.ImageStack.name())) {
					id = ((FileInfo)metadata).getId();
					toShowStack.add((FileInfo)metadata);
				}
			}
			if (!toShow.isEmpty()) {
				GWT.log("OpenImageAction Sending data to SimpleViewer" + toShow);
				URLPlace place = new URLPlace(id, "./services/objects/", SimpleImagePresenter.NAME);
				OpenDisplayPanel req = new OpenDisplayPanel(
						key, true, SimpleImagePresenter.NAME, toShow, place);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(req));
			}	
			if (!toShowStack.isEmpty()) {
				GWT.log("OpenImageAction Sending data to MultiImageViewer" + toShowStack);
				URLPlace place = new URLPlace(id, "./services/objects/", MultiViewImagePresenter.NAME);
				OpenDisplayPanel req = new OpenDisplayPanel(
						key, true, MultiViewImagePresenter.NAME, toShowStack, place);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(req));
			}	
		}
//		return null;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new OpenImageAction(md, factory, pres);
	}
	
};

