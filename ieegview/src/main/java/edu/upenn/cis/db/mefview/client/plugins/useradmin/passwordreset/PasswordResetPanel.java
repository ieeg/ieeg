/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class PasswordResetPanel extends AstroTab implements  
PasswordResetPresenter.Display {
	HandlerRegistration hr;
	TextBox oldPasswordBox = new PasswordTextBox();
	TextBox passwordBox = new PasswordTextBox();
	TextBox passwordBox2 = new PasswordTextBox();
	Button button = new Button("Change password");

	public final static String NAME = "PasswordReset";
	final static PasswordResetPanel seed = new PasswordResetPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PasswordResetPanel() {
		super();
	}

	public PasswordResetPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, title);
		
		FlowPanel fp = new FlowPanel();
		
		add(fp);
		fp.setSize("100%", "100%");
		setWidgetLeftRight(fp, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(fp, 1, Unit.PX, 1, Unit.PX);

		Label heading = new Label("Password reset");
		
		fp.add(heading);
		heading.setStyleName("IeegSubHeader");

		fp.add(new HTML("<p></p>"));
		fp.add(new Label("Old password: "));
		fp.add(oldPasswordBox);
		fp.add(new Label("New password: "));
		fp.add(passwordBox);
		fp.add(new Label("New password (again: "));
		fp.add(passwordBox2);
		fp.add(new HTML("<p></p>"));
		fp.add(button);
	  }

	void initState() {
		// Initialization.  Note that getDataSnapshotState() should
		// give you access to the current metadata.
		
		// You'll probably just add widgets here, and whether you populate them depends
		
		
		// List of all predefined annotation types (not specific to the study)
		//getDataSnapshotState().getAnnotationDefinitions()
		
		// This model holds all of the annotations, which are in a set of Groups
		// (~ layers) that can be nested tree-style
		// getDataSnapshotState().getAnnotationModel()
		
		// The detailed metadata on the channels
		// getDataSnapshotState().getTraceInfo()
		
		// Also revIDs, labels, ...
		
		// This is also metadata but includes things like number of annotations,
		// number of associated images, and a few other things
		// getDataSnapshotState().getSearchResult()
	}
	
	@Override
	public void setHandler(ClickHandler buttonHandler) {
		button.addClickHandler(buttonHandler);
	}
	
	@Override
	public String getOldPassword() {
		return oldPasswordBox.getText();
	}

	@Override
	public String getNewPassword() {
		return passwordBox.getText();
	}

	@Override
	public String getNewPassword2() {
		return passwordBox2.getText();
	}
	
	
	@Override
	public void showIllegal() {
		new IllegalDialogBox().center();
	}

	@Override
	public void showMismatch() {
		new MismatchDialogBox().center();
	}
	
	@Override
	public void showBadPassword() {
		new BadPasswordDialogBox().center();
	}

	@Override
	public void showChangedPassword() {
		new PasswordChangedDialogBox().center();
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	/**
	 * This is the real constructor for the panel.  It gets called by the 
	 * PanelFactory, then calls the constructor with the appropriate
	 * parameters.
	 */
	@Override
	public AstroPanel create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		return new PasswordResetPanel(model, clientFactory, title);
	}

	/**
	 * This ultimately maps to a Resource that corresponds to a PNG in
	 * ...mefview.client.images.
	 */
	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getHelp();
	}

	/**
	 * This is for saving state on shutdown, but can be ignored
	 */
	@Override
	public WindowPlace getPlace() {
		// TODO if we ever want to save on shutdown
		return null;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		// TODO if we ever support this
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return null;
//	}

	/**
	 * A friendly name for what our panel does
	 */
	public String getTitle() {
		return "Password Reset";
	}

	/**
	 * This stub would be used to attach a handler to
	 * some control widget on the panel. 
	 */
	@Override
	public void addClickHandlerToButton(ClickHandler handler) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getPanelCategory() {
		return PanelTypes.MODAL_DIALOG;
	}

	@Override
	public void bindDomEvents() {
	}
}
