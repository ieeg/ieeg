/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import static com.google.common.collect.Maps.newHashMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class MetadataModel {
	
	public static void sortMetadata(List<PresentableMetadata> ss) {
		Collections.sort(ss, new Comparator<PresentableMetadata>() {

			@Override
			public int compare(PresentableMetadata o1,
					PresentableMetadata o2) {
				if (o1 instanceof SearchResult && o2 instanceof SearchResult) {
					SearchResult s1 = (SearchResult)o1;
					SearchResult s2 = (SearchResult)o2;
					if  (s1.getScore() == s2.getScore()) {
						return o1.getLabel().compareTo(o2.getLabel());
					} else
						return -(Double.valueOf(s1.getScore()).compareTo(Double.valueOf(s2.getScore())));
				} else {
					return o1.getLabel().compareTo(o2.getLabel());
				}
			}
			
		});
	}
	
	Set<String> openStudies = new HashSet<String>();
	Set<String> recentStudies = new HashSet<String>();
	Set<PresentableMetadata> inList = new HashSet<PresentableMetadata>();
	
	HashMap<String, PresentableMetadata> favoriteSnapshots = new HashMap<String,PresentableMetadata>();
	HashMap<String, PresentableMetadata> knownSnapshots = new HashMap<String,PresentableMetadata>();
	HashMap<String, StudyMetadata> studyData = new HashMap<String,StudyMetadata>();
	HashMap<String, ExperimentMetadata> experimentData = newHashMap();
	
	HashMap<PresentableMetadata, List<PresentableMetadata>> contents = new HashMap<PresentableMetadata, List<PresentableMetadata>>();
	public void addStudyData(String key, StudyMetadata sm) {
		studyData.put(key, sm);
	}
	
	public StudyMetadata getStudyData(String key) {
		return studyData.get(key);
	}

	public void addExperimentData(String key, ExperimentMetadata sm ) {
		experimentData.put(key, sm);
	}
	
	public ExperimentMetadata getExperimentData(String key) {
		return experimentData.get(key);
	}

	public void addToKnownList(PresentableMetadata study) {
		inList.add(study);
	}
	
	public void addToKnownList(Collection<? extends PresentableMetadata> study) {
		inList.addAll(study);
	}

	public void removeFromKnownList(PresentableMetadata study) {
		inList.remove(study);
		
		contents.remove(study);
	}
	
	public Set<PresentableMetadata> getKnownList() {
		return inList;
	}
	
	public Collection<PresentableMetadata> getFavoriteSnapshots() {
		return favoriteSnapshots.values();
	}
	public void setFavoriteSnapshots(Set<? extends PresentableMetadata> favoriteSnapshots) {
		for (PresentableMetadata g: favoriteSnapshots)
			this.favoriteSnapshots.put(g.getId(), g);
	}
	public void addFavorite(PresentableMetadata md) {
		favoriteSnapshots.put(md.getId(), md);
	}
	public void removeFavorite(PresentableMetadata md) {
		favoriteSnapshots.remove(md.getId());
	}
	
	public Collection<PresentableMetadata> getKnownSnapshots() {
		return knownSnapshots.values();
	}
	public void setKnownSnapshots(Set<? extends PresentableMetadata> knownSnapshots) {
		for (PresentableMetadata g: knownSnapshots)
			this.knownSnapshots.put(g.getId(), g);
	}
	
	public void addKnownSnapshot(PresentableMetadata ss) {
		this.knownSnapshots.put(ss.getId(), ss);
	}
	
	public void updateRecentStudies(String study) {
		if (openStudies.remove(study))
			recentStudies.add(study);
	}

	public void updateOpenStudies(String study) {
		openStudies.add(study);
	}
	
	public void addKnownContents(PresentableMetadata parent, SerializableMetadata child) {
		if (parent == null || child == null)
			return;
		if (parent == child || parent.getId().equals(child.getId()))
			return;
		
		if (contents.get(parent) == null) {
			GWT.log("MM: Creating entry for parent " + parent.getLabel());
			contents.put(parent, new ArrayList<PresentableMetadata>());
		}
		
		if (parent instanceof SearchResult)
			addKnownSnapshot(parent);
		
		List<PresentableMetadata> list = contents.get(parent);
		
		Set<String> added = new HashSet<String>();
		added.add(parent.getId());
		for (PresentableMetadata c: contents.get(parent)) {
//			GWT.log("MM: Add contents to existing " + c.getLabel());
			added.add(c.getId());
			
			if (c instanceof SearchResult)
				addKnownSnapshot(c);
		}
		if (!added.contains(child.getId())) {
			GWT.log("MM: Adding contents to " + parent.getLabel() + ": " + child.getLabel() + " (" + child.getId() + ")");
			child.setParent(parent);
			list.add((PresentableMetadata)child);
			added.add(child.getId());
		}
	}

	public Set<PresentableMetadata> addKnownContents(PresentableMetadata parent, Collection<? extends SerializableMetadata> children) {
		if (contents.get(parent) == null) {
			GWT.log("MM: List / Creating entry for parent " + parent.getLabel());
			contents.put(parent, new ArrayList<PresentableMetadata>());
		}
		if (parent instanceof SearchResult)
			addKnownSnapshot(parent);
		
		List<PresentableMetadata> list = contents.get(parent);
		
		Set<String> added = new HashSet<String>();
		added.add(parent.getId());
		for (PresentableMetadata c: contents.get(parent)) {
			if (c == null)
				continue;
			if (parent == c || parent.getId().equals(c.getId()))
				continue;
//			GWT.log("MM: List / Add contents to existing " + c.getLabel());
			added.add(c.getId());
			if (c instanceof SearchResult)
				addKnownSnapshot(c);
		}
		Set<PresentableMetadata> theDiff = new HashSet<PresentableMetadata>();
		for (SerializableMetadata ch : children)
			if (!added.contains(ch.getId())) {
				GWT.log("MM: List / Adding contents to " + parent.getLabel() + ": " + ch.getLabel() + " (" + ch.getId() + ")");
				ch.setParent(parent);
				list.add((PresentableMetadata)ch);
				added.add(ch.getId());
				theDiff.add((PresentableMetadata)ch);
			}
		return theDiff;
	}
	
	public List<PresentableMetadata> getKnownContents(PresentableMetadata parent) {
		
		if (!contents.containsKey(parent)) {
			contents.put(parent, new ArrayList<PresentableMetadata>());
			GWT.log("MM: Get Creating entry for parent " + parent.getLabel());
		} else
			GWT.log("MM: Retrieving entry for parent " + parent.getLabel() + " with " + contents.get(parent).size());
		
		return contents.get(parent);
	}
}
