package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.clickhandlers.OpenEditMontageDialogClickHandler;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;
import edu.upenn.cis.db.mefview.client.events.EegMontageChangedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public class SelectMontage extends PopupPanel implements
DialogWithInitializer<ISelectMontage>, ISelectMontage, EegMontageChangedEvent.Handler{

	private ListBox lb;
	private List<EEGMontage> montages; 

	private Button newM = new Button("New Montage");
	private Button editM = new Button("Edit Montage");
	private Button remM = new Button("Remove Montage");
	private Button copyM = new Button("Copy Montage");

	public SelectMontage() {
		this.setAutoHideEnabled(true);
	}


	public void createDialog(final ClientFactory factory){
		VerticalPanel vp = new VerticalPanel();

		setTitle("Select Montage");

		lb = new ListBox();
		lb.setVisibleItemCount(20);
		this.setWidth("20em");
		lb.setWidth("100%");
		vp.add(lb);


		montages = new ArrayList<EEGMontage>();

		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(newM);
		buttonPanel.add(copyM);
		buttonPanel.add(editM);
		buttonPanel.add(remM);
		if (!factory.getSessionModel().isRegisteredUser()) {
			newM.setEnabled(false);
			copyM.setEnabled(false);
			editM.setEnabled(false);
			remM.setEnabled(false);
		}

		vp.add(buttonPanel);	
		setWidget(vp);
	}


	@Override
	public void init(final Handler responder, Command doAfter) {

		GWT.log("Init SelectMontage");

		editM.setEnabled(false);
		remM.setEnabled(false);

		IeegEventBusFactory.getGlobalEventBus().registerHandler(EegMontageChangedEvent.getType(), this);

		lb.addChangeHandler(new ChangeHandler(){

			@Override
			public void onChange(ChangeEvent arg0) {

				EEGMontage selectedMontage = montages.get(lb.getSelectedIndex());
				GWT.log("Change to selected Montage: " + selectedMontage);
				responder.switchMontage(selectedMontage);

			}
		});

		remM.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent arg0) {

				EEGMontage selectedMontage = montages.get(lb.getSelectedIndex());
				responder.removeMontage(selectedMontage);

			}

		});

		copyM.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				EEGMontage selectedMontage = montages.get(lb.getSelectedIndex());
				responder.copyMontage(selectedMontage);

			}

		});

	}

	/*
	 * Set when user click on OpenMontage button. This cannot be set during init because clickhandler needs to be updated for different panels.
	 * (non-Javadoc)
	 * @see edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage#setClickHandlers(edu.upenn.cis.db.mefview.client.clickhandlers.OpenEditMontageDialogClickHandler)
	 */
	@Override
	public void setClickHandlers(OpenEditMontageDialogClickHandler newMHandler, OpenEditMontageDialogClickHandler editMHandler){
		newM.addClickHandler(newMHandler);
		editM.addClickHandler(editMHandler);
	}

	@Override
	public void initialize(DialogInitializer<ISelectMontage> initFn) {
		initFn.init(this);

	}

	@Override
	public void reinitialize(DialogInitializer<ISelectMontage> initFn) {
		initFn.reinit(this);

	}

	@Override
	public void open() {
		center();

	}



	@Override
	public void close() {      
		hide();

	}



	@Override
	public void focus() {
		lb.setFocus(true);

	}



	public List<EEGMontage> getMontages() {
		return montages;
	}

	public Button getNewM() {
		return newM;
	}

	@Override
	public EEGMontage getMontage() {
		return montages.get(lb.getSelectedIndex());
	}

	@Override
	public void addMontageToList(EEGMontage montage){
		montages.add(montage);
		String strName = getStrName(montage);

		lb.addItem( strName, Objects.toString(montage.getServerId()));
	}

	@Override
	public ListBox getListBox() {
		return lb;
	}

	@Override
	public String getStrName(EEGMontage montage){
		String strName = montage.getName();
		GWT.log("Getting StrName + public: " + montage + ":" + montage.getIsPublic());

		Boolean isPublic = montage.getIsPublic();
		if (isPublic !=null && isPublic){
			strName = strName + " (shared";
			if (montage.getOwnedByUser())
				strName += " by me";
			strName +=")";
		}

		return strName;
	}

	@Override
	public void removeMontageFromList(EEGMontage montage) {

		final String idAsString = Objects.toString(montage.getServerId());
		for (int i = 0; i < lb.getItemCount(); i++) {
			if (lb.getValue(i).equals(idAsString)) {
				lb.removeItem(i);
				montages.remove(i);
				break;
			}
		}

	}

	@Override
	public void updateSelectMontage(EEGMontage montage, IEEGViewerPresenter presenter, Boolean isNew) {
		// TODO Auto-generated method stub

		GWT.log("Change to selected Montage: " + montage);

		if(!isNew){
			int i=0;
			for (EEGMontage m: montages){
				if(m == montage)
					break;
				i++;
			}

			lb.setSelectedIndex(i);
			lb.setItemText(i, getStrName(montage));
		}else {
			lb.setSelectedIndex(lb.getItemCount()-1);

		}


		IeegEventBusFactory.getGlobalEventBus()
		.fireEvent(
				new
				EegMontageChangedEvent(
						presenter, montage));


	}


	@Override
	public void onMontageChanged(IEEGViewerPresenter w, EEGMontage montage) {

		if (montage.getOwnedByUser()==true){
			editM.setEnabled(true);
			remM.setEnabled(true);
		} else {
			editM.setEnabled(false);
			remM.setEnabled(false);
		}

	}


	@Override
	public void clearList() {
		lb.clear();
	}

}
