package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;

import edu.upenn.cis.db.mefview.client.DataLoadHandler;
import edu.upenn.cis.db.mefview.client.controller.SnapshotCache;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanJs;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.TimeSeries;

public class PreviewDataHandler implements DataLoadHandler {

    /**
	 * 
	 */
	private final IEEGPresenter eegPresenter;

	/**
	 * @param eegPresenter
	 */
	public PreviewDataHandler(IEEGPresenter eegPresenter) {
		this.eegPresenter = eegPresenter;
	}

	private void init() {
      this.eegPresenter.getDisplay().showStatusMessage("Parsing data...");
    }

    private void handleData(final String user, final String dataSnapshotRevId, 
        final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, JsArray<JsArrayInteger> ar, boolean isMinMax) {
      
      // Update points in graph
      this.eegPresenter.getDisplay().updateGraphWithTraces(ar, isMinMax);

      SnapshotCache.debug2("Received initial preview data @" + this.eegPresenter.getDisplay().getRequestPeriod());

      this.eegPresenter.getDisplay().clearFirstRequest();
      this.eegPresenter.clearFirstRequest();
      if (ar != null) {
        this.eegPresenter.getDisplay().addNewData(ar, true, isMinMax);
      }
      
      this.eegPresenter.jumpTo(0);
      this.eegPresenter.getDisplay().initPosition(0, this.eegPresenter.getDisplay().getSpecifiedPageWidth() * 1.E6);

      // Prefetch next data
      this.eegPresenter.prefetchAsNeeded((long)this.eegPresenter.getDisplay().getPageWidth(), 
          (long)(2 * this.eegPresenter.getDisplay().getPageWidth()), 1);

    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final TimeSeries[] results, boolean isMinMax) {
      init();

      JsArray<JsArrayInteger> ar = this.eegPresenter.refreshBufferFromSeries(results);

      handleData(user, dataSnapshotRevId, traceIds,
          start, width, samplingPeriod, 
          filter, ar, isMinMax);
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final String results, boolean isMinMax) {
      init();

      JsArray<JsArrayInteger> ar = this.eegPresenter.refreshBufferFromJson(results);

      handleData(user, dataSnapshotRevId, traceIds,
          start, width, samplingPeriod, 
          filter, ar, isMinMax);
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final JsArray<JsArrayInteger> results, boolean isMinMax) {
      init();

      handleData(user, dataSnapshotRevId, traceIds,
          start, width, samplingPeriod, 
          filter, results, isMinMax);
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final List<TimeSeriesSpan> results, boolean isMinMax) {

      if (results.isEmpty())
        return;

      init();

      JsArray<JsArrayInteger> arr = ((TimeSeriesSpanJs)results.get(0)).getMaster();

      handleData(user, dataSnapshotRevId, traceIds,
          EEGModel.getStartDate(0, arr),
          EEGModel.getEndDate(0, arr),
          EEGModel.getPeriod(arr),
          filter,
          arr,
          isMinMax);
    }


  }
