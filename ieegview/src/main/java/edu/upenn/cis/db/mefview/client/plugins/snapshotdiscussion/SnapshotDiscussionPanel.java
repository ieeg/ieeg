/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion;

import java.util.List;

import com.google.gwt.resources.client.ImageResource;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.DiscussionViewer;
import edu.upenn.cis.db.mefview.client.viewers.DiscussionViewer.Handler;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.places.SnapshotDiscussionPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class SnapshotDiscussionPanel extends AstroTab implements
SnapshotDiscussionPresenter.Display {
	DataSnapshotViews model;
//	String userid;
	
	DiscussionViewer viewer;
	com.google.web.bindery.event.shared.HandlerRegistration last1;
	com.google.web.bindery.event.shared.HandlerRegistration last2;
	
	public final static String NAME = "snapshot-discussion";
	final static SnapshotDiscussionPanel seed = new SnapshotDiscussionPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public SnapshotDiscussionPanel() {
		super();
	}

	public SnapshotDiscussionPanel(final ClientFactory clientFactory,
			final DataSnapshotViews model, 
//			final String user,
			final String title
			) {
		super(clientFactory, model,  
		    title);

		this.model = model;
//		userid = user;
	}
	
	@Override
	public String getPanelType() {
		return NAME;//PanelType.DISCUSSION;
	}
	@Override
	public void close() {
		if (last2 != null)
			last2.removeHandler();
		if (last1 != null)
			last1.removeHandler();
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			Project project, 
			boolean writePermissions,
			final String title) {
		return new SnapshotDiscussionPanel(clientFactory, model, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDiscussion();
	}

	@Override
	public WindowPlace getPlace() {
		return new SnapshotDiscussionPlace(datasetId);
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return SnapshotDiscussionPlace.getFromSerialized(params);
//	}

	@Override
	public void addPost(Post p) {
		viewer.postMessage(p);
	}

	@Override
	public void setPostList(List<Post> pl) {
		viewer.renderPosts(pl);
	}

	@Override
	public void setNextPost() {
		viewer.setNextPost();
	}

	public String getTitle() {
		return "Discussion";
	}

	@Override
	public void setHandler(Handler handler) {
		viewer = new DiscussionViewer("Discussion for Snapshot " + model.getSearchResult().getFriendlyName(),
				//user, 
				handler, model.getSearchResult().getDatasetRevId(), clientFactory.getSessionModel());
		add(viewer);
		
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	public boolean isSearchable() {
		return false;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {

	}
}
