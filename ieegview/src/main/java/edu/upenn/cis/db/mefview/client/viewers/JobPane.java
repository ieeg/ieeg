/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import static com.google.common.collect.Sets.newHashSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.ActiveJobs;
import edu.upenn.cis.db.mefview.client.events.JobStatusChangedEvent;
import edu.upenn.cis.db.mefview.client.events.RequestReloadSnapshotEvent;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.JobInfo;

public class JobPane extends LayoutPanel implements JobStatusChangedEvent.Handler {
	Button stop = new Button("Kill");
	Button refresh = new Button("Refresh");
	PagedTable<JobInfo> jobTable;
	ActiveJobs theJobModel;
	List<JobInfo> jobs = new ArrayList<JobInfo>();
	ClientFactory factory;
	
	public JobPane(final ActiveJobs jobModel, final ClientFactory factory) {
		theJobModel = jobModel;
		this.factory = factory;

		FlowPanel labPan = new FlowPanel();
        Label lab = new Label("Executing Tasks:");
        labPan.setStyleName("IeegSubHeader");
        labPan.add(lab);
		
        add(labPan);
        this.setWidgetLeftRight(labPan, 4, Unit.PX, 50, Unit.PCT);
        this.setWidgetTopHeight(labPan, 2, Unit.PX, 32, Unit.PX);
        
//		HorizontalPanel hLabel = new HorizontalPanel();
//		Label l = new Label("Executing Tasks");
//		l.setStyleName("heading");
////		l.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
//		Image img = new Image(factory.getTools().getSafeUri());
//		img.setPixelSize(30, 30);
//		img.getElement().getStyle().setPaddingLeft(8, Unit.PX);
//		img.getElement().getStyle().setPaddingRight(4, Unit.PX);
//		hLabel.add(img);
//		hLabel.add(l);
//		add(hLabel);
//		setWidgetLeftRight(hLabel, 2, Unit.PX, 2, Unit.PX);
//		setWidgetTopHeight(hLabel, 2, Unit.PX, 34, Unit.PX);
		
		List<String> jobColNames = new ArrayList<String>();
		List<TextColumn<JobInfo>> jobColumns = new ArrayList<TextColumn<JobInfo>>();
		
		jobColNames.add("Output Snapshot");
		jobColumns.add(JobInfo.jobIdentifier);
		jobColNames.add("Tool");
		jobColumns.add(JobInfo.jobTool);
		jobColNames.add("Description");
		jobColumns.add(JobInfo.jobDescription);
		jobColNames.add("Tasks");
		jobColumns.add(JobInfo.jobTasks);

		jobTable = new PagedTable<JobInfo>(20, false, false, true,jobs, JobInfo.KEY_PROVIDER, jobColNames, jobColumns, true, 
				ResourceFactory.getStarSel(), ResourceFactory.getStarUnsel());
		jobTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);
		jobTable.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
				
		add(jobTable);
		setWidgetTopBottom(jobTable, 34, Unit.PX, 34, Unit.PX);
		
		HorizontalPanel buttons = new HorizontalPanel(); 
		buttons.add(refresh);
		buttons.add(new HTML("&nbsp;"));
		buttons.add(stop);
		
		stop.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (JobInfo ji: jobTable.getList()) {
					if (jobTable.getSelectionModel().isSelected(ji))
						theJobModel.terminate(ji.getJobId());
				}
			}
			
		});
		add(buttons);
		setWidgetBottomHeight(buttons, 1, Unit.PX, 32, Unit.PX);
		
		//theJobModel.refresh(true);
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(JobStatusChangedEvent.getType(), this);
	}

	public void close() {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
	}
	
	
	@Override
	public void onJobStatusChanged(String job) {
		final String m = "onJobStatusChanged()";
		Set<JobInfo> completedJobs = newHashSet();
		//We'll remove the uncompleted jobs below.
		completedJobs.addAll(jobTable.getList());
		jobTable.clear();
		jobTable.addAll(theJobModel.getActiveJobs());
		jobTable.redraw();
		
		for (JobInfo j : jobTable.getList()) {
			completedJobs.remove(j);
		}
		
		// Open any jobs that have completed
		for (JobInfo j : completedJobs) {
			factory.getSessionModel().getMainLogger().info(m + ": reloading snapshot " + j.getJobId());
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestReloadSnapshotEvent(j.getJobId(), j.getJobId()));
//			factory.getControlPane().reloadStudy(j.getJobId());
		}
	}
}
