/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;

public class EEGPlace extends WindowPlace implements IsSerializable {
	boolean[] channelsEnabled = new boolean[0];
	List<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();
	long position = -1;
	long width = -1;
	boolean autoScale = false;
	double scaleFactor = 1.0;
	
	public EEGPlace() {
		super();
	}
	
	public EEGPlace(String snapshot) {
		super(snapshot);
	}

	public boolean[] getChannelsEnabled() {
		return channelsEnabled;
	}

	public void setChannelsEnabled(boolean[] channelsEnabled) {
		this.channelsEnabled = channelsEnabled;
	}

	public List<DisplayConfiguration> getFilters() {
		return filters;
	}

	public void setFilters(List<DisplayConfiguration> filters) {
		this.filters = filters;
	}

	public long getPosition() {
		return position;
	}

	public void setPosition(long position) {
		this.position = position;
	}

	public long getWidth() {
		return width;
	}

	public void setWidth(long width) {
		this.width = width;
	}

	public boolean getAutoScale() {
		return autoScale;
	}

	public void setAutoScale(boolean autoScale) {
		this.autoScale = autoScale;
	}

	public double getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(double scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
	

	@Override
	public String getType() {
		return EEGViewerPanel.NAME;
	}

//	@Override
//	public String getSerializableForm() {
//		// TODO Auto-generated method stub
//		return null;
//	}

//	@Override
//	public WindowPlace getFromSerialized(String serial) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
}
