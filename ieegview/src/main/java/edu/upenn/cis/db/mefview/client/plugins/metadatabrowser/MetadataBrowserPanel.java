/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import static com.google.common.collect.Sets.newHashSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.SnapshotServicesAsync;
import edu.upenn.cis.db.mefview.client.actions.ClickAction;
import edu.upenn.cis.db.mefview.client.actions.IPluginAction;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.panels.AppPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.DynamicToolbar;
import edu.upenn.cis.db.mefview.client.viewers.MetadataTree;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable.StarToggleHandler;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class MetadataBrowserPanel extends AstroTab implements MetadataBrowserPresenter.Display {
	CellTree tree;
	Button searchButton;
	
	public static final String FAVORITES = "Favorites";
	public static final String RESULTS = "Datasets";
	public static final String PROJECTS = "Projects";

	ResizeLayoutPanel searchResultsPanel ;

	SplitLayoutPanel mainSplitPanel = new SplitLayoutPanel();
	MetadataBrowserChildPanel recordingDetails;// = new MetadataBrowserSimpleLayoutPanel();
	final SingleSelectionModel<PresentableMetadata> selModel =
			new SingleSelectionModel<PresentableMetadata>();

//	AppModel parent;
	MetadataModel model;

	MetadataTree srt;

	DynamicToolbar toolbar;
	String title;

	ClientFactory clientFactory;
	final SnapshotServicesAsync service;
	List<IPluginAction> defaultActions = new ArrayList<IPluginAction>();

	List<Project> queuedProjects = new ArrayList<Project>();

	public static final String NAME = "metadata-browser";
	static final MetadataBrowserPanel seed = new MetadataBrowserPanel();
	
	List<Widget> containedWidgets = new ArrayList<Widget>();

	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	interface TreeResources extends CellTree.Resources { 
		@Source("../../css/IEEG_Result_Tree.css") 
		CellTree.Style cellTreeStyle(); 

	} 

	public MetadataBrowserPanel() {
		super();
		service = null;
	}

	public MetadataBrowserPanel( 
			final SnapshotServicesAsync service,
			final Button search,
			final ClientFactory factory,
			final String title) {

		log(4, "Constructed Dataset Browser panel");
		clientFactory = factory;

		this.searchButton = search;
		this.service = service;
		this.title = title;

		this.addDomHandler(new ContextMenuHandler() {

			@Override public void onContextMenu(ContextMenuEvent event) {
				event.preventDefault();
				event.stopPropagation();
			}
		}, ContextMenuEvent.getType());
	}
	
	@Override
	public void addPresenter(Presenter pres) {
		super.addPresenter(pres);
		if (recordingDetails == null) {
			recordingDetails = new MetadataBrowserChildPanel(clientFactory, 
					(MetadataBrowserPresenter)pres);//, parent);
		
			mainSplitPanel.add(recordingDetails);
			containedWidgets.add(recordingDetails);
		}
	}

	public void resizeAll(int width, int height) {
		setHeights(height - AppPanel.HEADER);
		setWidths(width - AppPanel.LEFT_PANE);
	}

	public void closeStudy(final String study) {
		model.updateRecentStudies(study);
	}

	public void openStudy(final String study) {
		model.updateOpenStudies(study);
	}

	private int getTextHeight() {
		return 36;
	}

	public void setHeights(int height) {

		int rowHeight = getTextHeight();


		height -= getTextHeight() * 4;

		if (height < 100)
			height = 100;

		int pageSize = height / rowHeight;

		recordingDetails.setHeight((int)(pageSize * rowHeight * 3 /5)  + "px");

	}

	public void setWidths(int width) {
		int panelWidth = (int)(width - 40);
		searchResultsPanel/*Scroll*/.setWidth(panelWidth + "px");
		recordingDetails.setWidth(panelWidth + "px");

	}


	@Override
	public void updateRevId(String oldRevId, String newRevId) {
		throw new RuntimeException("Rev ID does not update");

	}

//	@Override
	public void highlightMetadata(Collection<? extends PresentableMetadata> items) {
		for (PresentableMetadata i: items) {
			for (PresentableMetadata md : srt.getProvider().getList()) {
				if (md.getId().equals(i.getId()))
					getSelectionModel().setSelected(md, true);
			}
		}
	}

	public SelectionModel<PresentableMetadata> getSelectionModel() {
		return selModel;
	}

//	@Override
//	public void registerPrefetchListener(Prefetcher p) {
//		// TODO Auto-generated method stub
//
//	}

	public void setDetails(String experimentId, String friendlyName, ExperimentMetadata em) { 
		recordingDetails.setDetails(experimentId, friendlyName, getSelectedResult(), em);
	}

	public void setDetails(final String studyId, final String friendlyName, final StudyMetadata sm) {
		recordingDetails.setDetails(studyId, friendlyName, getSelectedResult(), sm);
	}

	public void refresh() {

	}

	@Override
	public void onResize() {
		super.onResize();
		if (srt != null && srt.getProvider() != null)
			srt.getProvider().refresh();
	}

	@Override
	public String getPanelType() {
		return NAME;//PanelType.STUDIES;
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
		return null;//controller.getState();
	}

	public void enableSearch(boolean enable) {
		if (searchButton != null)
			searchButton.setEnabled(enable);
	}

	@Override
	public PanelDescriptor getDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlace(WindowPlace place) {
		// TODO Auto-generated method stub

	}

	@Override
	public DataSnapshotViews getDataSnapshotController() {
		return null;//controller;
	}

	@Override
	public void close() {
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			//			Project project, 
			boolean writePermissions,
			final String title) {
		return new MetadataBrowserPanel(clientFactory.getSnapshotServices(),
				null, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getStudy();
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void addSnapshotSelectionChangedHandler(Handler handler) {
		getSelectionModel().addSelectionChangeHandler(handler);
	}

	@Override
	public void addOpenHandler(ClickHandler handler, Presenter pres) {
		//		toolbar.setOpenHandler(handler);
		IPluginAction openAction = new ClickAction("Open Dataset by ID", handler, clientFactory, pres);
		toolbar.addGlobalAction(openAction);
	}

	@Override
	public void addSearchHandler(ClickHandler handler, Presenter pres) {
		//      toolbar.setOpenHandler(handler);
		IPluginAction openAction = new ClickAction("Find Datasets", handler, clientFactory, pres);
		toolbar.addGlobalAction(openAction);
	}


	//	@Override
	//	public void setTestHandler(ClickHandler handler) {
	////		toolbar.setTestHandler(handler);
	//	}

	@Override
	public void addFavoriteClickHandler(StarToggleHandler<PresentableMetadata> handler) {
		srt.addFavoriteClickHandler(handler);
	}

	@Override
	public List<? extends PresentableMetadata> getResults() {
		List<PresentableMetadata> ret = new ArrayList<PresentableMetadata>();

		if (srt == null || srt.getProvider() == null || srt.getProvider().getList() == null)
			return ret;

		for (PresentableMetadata md: srt.getProvider().getList())
			if (md instanceof CollectionNode)
				ret.addAll(((CollectionNode)md).getChildMetadata());
			else
				ret.add(md);

		return ret;
	}

	/**
	 * Update the search results.
	 */
	public void addSearchResults(List<? extends PresentableMetadata> results, boolean replace) {
		if (replace) {
			CollectionNode searchResult = srt.getSearchResultCollection();
			srt.getProvider(searchResult).clearSearchResults();
		}

		//		System.out.println("Adding search results " + results);
		// These will be added and auto-categorized
		srt.getProvider().refreshList(results);

		PresentableMetadata key = srt.getProvider(srt.getSearchResultCollection()).getItem();
		setSelectedResult(key, false);
		if (tree.getRootTreeNode().getChildCount() > 0)
			tree.getRootTreeNode().setChildOpen(tree.getRootTreeNode().getChildCount() - 1, false);
		setSelectedResult(key, true);//srt.getSearchResultCollection(), true);
	}

	public Set<PresentableMetadata> getSelected() {
		Set<PresentableMetadata> selectedSet = newHashSet();
		PresentableMetadata selectedObject = selModel.getSelectedObject();
		if (selectedObject != null) { 
			selectedSet.add(selectedObject);
		}
		return selectedSet;
	}

	@Override
	public void addResult(PresentableMetadata sr) {
		if (!model.getKnownList().contains(sr)) {
			model.addToKnownList(sr);

			if (!srt.getOrReuseProvider(srt.getSearchResultCollection()).contains(sr)) {
				srt.getDefaultProvider().add(null, sr);

				log(3, ">> MetadataBrowserPanel Adding " + sr.getFriendlyName());
				//				srt.getDefaultProvider().refresh();
				srt.getOrReuseProvider(srt.getSearchResultCollection()).refresh();
			}
		}
	}

	@Override
	public void removeResult(PresentableMetadata sr) {
		//		inList.remove(sr);
		model.removeFromKnownList(sr);
		srt.getProvider().remove(sr);
		if (sr.getId() != null)
			log(3, ">> Removing " + sr.getId());
	}

	@Override
	public void removeResult(int inx) {
		// TODO Auto-generated method stub

	}


	public MetadataProviderBase getProjectsProvider() {
		for (PresentableMetadata md: srt.getProvider().getList()) {
			if (md.getLabel().equals(PROJECTS))
				return srt.getOrReuseProvider(md);
		}
		return null;
	}

	CollectionNode proj = null;

	public CollectionNode getProjectsNode() {
		if (proj != null)
			return proj;
		else
			for (PresentableMetadata md: srt.getProvider().getList()) {
				if (md.getLabel().equals(PROJECTS))
					return (CollectionNode)md;
			}
		return null;
	}

	CollectionNode faves = null;

	public CollectionNode getFavoritesNode() {
		if (faves != null)
			return faves;
		else
			for (PresentableMetadata md: srt.getProvider().getList()) {
				if (md.getLabel().equals(FAVORITES))
					return (CollectionNode)md;
			}
		return null;
	}

	@Override
	public void removeResults(Collection<? extends PresentableMetadata> sr) {
		for (PresentableMetadata res: sr) {
			srt.getProvider().remove(res);
		}
	}

	@Override
	public void addResults(Collection<? extends PresentableMetadata> sr) {		
		List<PresentableMetadata> novel = new ArrayList<PresentableMetadata>();
		for (PresentableMetadata s: sr)
			if (!srt.getProvider().contains(s)) {
				//				getStudyList().add(s);
				novel.add(s);
				//				Collections.sort(getStudyList());
			}

		srt.getProvider().addStudies(novel);

		srt.getOrReuseProvider(srt.getSearchResultCollection()).redraw();

		//		GWT.log(">> Adding " + novel.size());
	}

	@Override
	public PresentableMetadata getSelectedResult() {
		if (!getSelected().isEmpty())
			return getSelected().iterator().next();

		return null;
	}

	@Override
	public void setSelectedResult(PresentableMetadata sr, boolean sel) {
		getSelectionModel().setSelected(sr, sel);
	}

	@Override
	public boolean isSelectedResult(PresentableMetadata sr) {
		return (getSelectionModel().isSelected(sr));
	}

	@Override
	public void refreshSnapshots() {
		srt.getProvider().refresh();
		log(4, "Refresh snapshots list");// - total items: " + getStudyList().size());
	}

	@Override
	public void setExperimentDetails(String snapshotId, String friendlyName,
			ExperimentMetadata em) {
		recordingDetails.setDetails(snapshotId, friendlyName, getSelectedResult(), em);
	}

	@Override
	public void setStudyDetails(String snapshotId, String friendlyName,
			StudyMetadata sm) {
		recordingDetails.setDetails(snapshotId, friendlyName, getSelectedResult(), sm);
	}

	@Override
	public void setDerivedResults(PresentableMetadata sr,
			Collection<? extends PresentableMetadata> subresults) {
		Set<PresentableMetadata> newStuff = model.addKnownContents(sr, subresults);
		if (!newStuff.isEmpty()) {
			System.out.println("** Adding derived results for " + sr.getFriendlyName());
			srt.getOrReuseProvider(sr).addStudies(newStuff);
		}
		srt.getOrReuseProvider(sr).redraw();
	}

	@Override
	public void setFavoriteResults(Collection<? extends PresentableMetadata> sr) {
		//		getStudyList().setFavorites(sr);
	}

	public String getTitle() {
		return "Data";
	}

	public static String getDefaultTitle() {
		return "Data";
	}

	@Override
	public boolean haveDerivedResultsFor(PresentableMetadata sr) {
		return srt.getProvider().contains(sr);
	}

	@Override
	public void setHistoryAndModel(MetadataModel history) {
		GWT.log("setHistoryAndModel is being set");
		model = history;

		List<String> rootCollections = Lists.newArrayList(
//				FAVORITES, 
				//				"Open",
				PROJECTS, 
				//				"Recent",
				RESULTS);
		srt = new MetadataTree(selModel, model, clientFactory, rootCollections);

		// Add Dataset Tree to SearchResultsPanel
		ScrollPanel sp = new ScrollPanel();
		CellTree.Resources resource = GWT.create(TreeResources.class); 
		tree = new CellTree(srt, null, resource, new CellTree.CellTreeMessages() {

			@Override
			public String showMore() {
				return "";
			}

			@Override
			public String emptyTree() {
				return "";
			}

		}, 20);
		tree.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);//.ENABLED);
		tree.setDefaultNodeSize(50);
		sp.add(tree);
		searchResultsPanel = new ResizeLayoutPanel();
		searchResultsPanel.add(sp);

		// Add the toolbar after the srt is created
		// Add Toolbar
		FlowPanel toolbarWrapper = new FlowPanel();
		toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
		toolbar = new DynamicToolbar(selModel, defaultActions, clientFactory);
		toolbarWrapper.add(toolbar);
		this.add(toolbarWrapper);
		this.setWidgetLeftRight(toolbarWrapper, 0, Unit.PCT, 0, Unit.PCT);
		this.setWidgetTopBottom(toolbarWrapper, 0, Unit.PX, 33, Unit.PCT);

		// Add the splitPanel to tab.
		mainSplitPanel.addWest(searchResultsPanel, 600);
		if (recordingDetails != null)
			mainSplitPanel.add(recordingDetails);
		mainSplitPanel.setStyleName("SnapshotBrowserSplitPanel");
		add(mainSplitPanel);
		this.setWidgetLeftRight(mainSplitPanel, 0, Unit.PCT, 0, Unit.PCT);
		this.setWidgetTopBottom(mainSplitPanel, 31, Unit.PX, 0, Unit.PCT);

		containedWidgets.add(toolbar);
		containedWidgets.add(searchResultsPanel);
		if (recordingDetails != null) {
			containedWidgets.add(recordingDetails);
		}
	}

	public boolean isSearchable() {
		return true;
	}

	@Override
	public void addProject(int inx, PresentableMetadata p) {
		if (getProjectsNode() != null) {
			getProjectsNode().addMetadata(inx, p);
			getProjectsProvider().refresh();
		}
//		if (recordingDetails == null)
//			queuedProjects.add((Project)p);
//		else
//			recordingDetails.addProject(inx, (Project)p);
	}

	@Override
	public void addProject(PresentableMetadata p) {
		if (getProjectsNode() != null) {
			getProjectsNode().addMetadata(p);
			//The srt refresh seems to ensure that 
			//the project count is updated even if 
			//the user selects this tab before the projects arrive from the server.
			srt.refresh(null);
			getProjectsProvider().refresh();
		}
//		if (recordingDetails == null)
//			queuedProjects.add((Project)p);
//		else
//			recordingDetails.addProject((Project)p);
	}

	@Override
	public List<? extends PresentableMetadata> getProjects() {
		if (getProjectsNode() != null)
			return getProjectsNode().getChildMetadata();
		else
			return new ArrayList<PresentableMetadata>();
	}

	@Override
	public void removeProject(int pos) {
		if (getProjectsNode() != null) {
			PresentableMetadata proj = getProjectsNode().getChildMetadata().get(pos);
//			if (recordingDetails == null)
//				queuedProjects.remove((Project)proj);
//			else
//				recordingDetails.removeProject((Project)proj);
			getProjectsNode().removeMetadata(pos);
			getProjectsProvider().refresh();
		}
	}

	@Override
	public void bindToolbar() {
		getSelectionModel().addSelectionChangeHandler(toolbar);
	}

	@Override
	public void refreshItem(PresentableMetadata item) {
		if (isSelectedResult(item)) {
			setSelectedResult(item, false);
			setSelectedResult(item, true);
		}
	}

	public MetadataProviderBase getMetadataProviderFor(PresentableMetadata item) {
		return srt.getProvider(item);
	}

	public int getMinHeight() {
		return minHeight;
	}

	@Override
	public void updateChildPane() {
		if (recordingDetails != null)
			recordingDetails.setView(getSelectedResult());
	}

	@Override
	public void updateChildPane(SerializableMetadata item) {
		if (recordingDetails != null)
			recordingDetails.setView(item);
	}
	
	@Override
	public Set<ChildPane> getChildPanes() {
		if (recordingDetails == null)
			return null;
		else
			return recordingDetails.getChildPanes();
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public Iterator<Widget> iterator() {
		return containedWidgets.iterator();
	}

	@Override
	public void bindDomEvents() {

	}
}
