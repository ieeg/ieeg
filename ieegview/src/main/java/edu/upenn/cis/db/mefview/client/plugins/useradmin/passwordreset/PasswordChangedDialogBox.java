package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class PasswordChangedDialogBox extends DialogBox {
	public PasswordChangedDialogBox() {
		super(true, true);
		
		setTitle("Password Changed");
		add(new Label("Your password has been changed."));
	}
}
