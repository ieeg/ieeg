/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.db.mefview.client.IPermsView.IPermsPresenter;
import edu.upenn.cis.db.mefview.client.widgets.ShowMorePagerPanel;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse.UserIdAndName;

public class UserListDialog {

	private static UserListDialogUiBinder uiBinder = GWT
			.create(UserListDialogUiBinder.class);

	interface UserListDialogUiBinder extends
			UiBinder<DialogBox, UserListDialog> {}

	@UiField
	Button okButton;

	private final DialogBox dialogBox;
	private final CellTable<UserIdAndName> usersList;

	@UiField
	ShowMorePagerPanel pagerPanel;

	private final IPermsPresenter presenter;


	public UserListDialog(IPermsPresenter presenter) {
		this.presenter = presenter;
		dialogBox = uiBinder.createAndBindUi(this);
		dialogBox
				.setText("Users");
//		dialogBox.setWidth("400px");
		usersList = new CellTable<UserIdAndName>(20);
		usersList.setWidth("100%", false);
		usersList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		usersList
				.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		final SelectionModel<UserIdAndName> selectionModel = this.presenter
				.getUserListSelectionModel();
		final Column<UserIdAndName, Boolean> checkColumn = new Column<UserIdAndName, Boolean>(
				new CheckboxCell()) {

			@Override
			public Boolean getValue(UserIdAndName object) {
				return selectionModel.isSelected(
						object);
			}
		};
		usersList.addColumn(checkColumn);
//		usersList.setColumnWidth(checkColumn, 40, Unit.PX);
		final Column<UserIdAndName, String> usernameColumn = new Column<UserIdAndName, String>(new TextCell()) {

			@Override
			public String getValue(UserIdAndName object) {
				return object.getUsername();
			}
		};
		usersList.addColumn(usernameColumn);
//		usersList.setColumnWidth(usernameColumn, 100, Unit.PCT);
		pagerPanel.setDisplay(usersList);
		this.presenter.addUserListDisplay(usersList);
		usersList.setSelectionModel(selectionModel,
				DefaultSelectionEventManager
						.<UserIdAndName> createCheckboxManager());
	}

	@UiHandler({ "okButton" })
	void onClick(ClickEvent e) {
		presenter.addNewUserAces();
		dialogBox.hide();
	}

	public void centerAndShow() {
		dialogBox.center();
		dialogBox.show();
	}

}
