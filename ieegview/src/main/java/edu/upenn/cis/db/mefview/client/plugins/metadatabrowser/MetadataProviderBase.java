/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.MetadataContentsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.filters.IMetadataFilter;
import edu.upenn.cis.db.mefview.client.viewers.MetadataTree;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

/**
 * The metadata provider is responsible for fetching and paging through the set of child
 * nodes related to a metadata item
 * 
 * @author zives
 *
 */
public abstract class MetadataProviderBase extends AsyncDataProvider<PresentableMetadata> 
implements MetadataPresenter, HasMetadataChildren {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Providers for children
	 */
	Map<PresentableMetadata,MetadataProviderBase> children = 
			new HashMap<PresentableMetadata,MetadataProviderBase>();
	ClientFactory clientFactory;
	
	MetadataTree treePresenter;
	final MetadataModel model;
	final PresentableMetadata item;
	MetadataPresenter parent;
	
	IMetadataFilter filter;
	
	private static final boolean addIt = true;

	final List<PresentableMetadata> result = new ArrayList<PresentableMetadata>();
	static Set<String> existingIds = new HashSet<String>();
	
	public int startPage = 0;
	
	private int start;
	private int end;
	
	public static int PAGE_SIZE = 20;
	
	
	public MetadataProviderBase(final PresentableMetadata root, final MetadataModel model, 
			final ClientFactory clientFactory, final MetadataTree notify,
			final Set<String> knownIds) {
		this.model = model;
		this.item = root;
		this.clientFactory = clientFactory;
		this.treePresenter = notify;
		this.existingIds.addAll(knownIds);
		
		if (root != null && root.getId() != null)
			existingIds.add(root.getId());

		start = 0;
		end = 0;//result.size();
	}
	
	public void setParent(MetadataPresenter parent) {
		this.parent = parent;
	}
	
	
	public void addAll(Collection<? extends PresentableMetadata> md) {
		for (PresentableMetadata m : md)
			add(m);
	}
	
	public boolean contains(PresentableMetadata md) {
		return result.contains(md);
	}
	
	public void remove(PresentableMetadata md) {
		result.remove(md);
		if (md.getId() != null)
			existingIds.remove(md.getId());
		
		children.remove(md);
		treePresenter.onItemRemoved(md);
	}
	
	public void remove(int pos) {
		PresentableMetadata md = result.get(pos);
		result.remove(pos);
		if (md.getId() != null)
			existingIds.remove(md.getId());
		
		children.remove(md);
		treePresenter.onItemRemoved(md);
	}
	boolean containsTraces(SearchResult sr) {
		List<PresentableMetadata> md = model.getKnownContents(sr);
		if (md == null || md.size() == 0)
			return false;
		for (PresentableMetadata m: md)
			if (m instanceof TraceInfo)
				return true;
		
		return false;
	}

	@Override
    public void addDataDisplay(HasData<PresentableMetadata> display) {
      super.addDataDisplay(display);

//      GWT.log("MDP Set data display for " + (item == null ? "root" : item.getFriendlyName()) + 
//    		  " given " + result.size() + " rows");
      
		int showThis = result.size();
		if (showThis > startPage + PAGE_SIZE)
			showThis = startPage + PAGE_SIZE;
		updateRowData(0, result.subList(startPage, showThis));
		updateRowCount(showThis - startPage, true);
    }		
	
	public void addStudies(Collection<? extends PresentableMetadata> md) {
//		GWT.log("MDP Add studies triggered: " + md.size());
		List<PresentableMetadata> temp = new ArrayList<PresentableMetadata>(md.size());
		temp.addAll(md);
		refreshList(temp);
	}

	public void addNoParent(PresentableMetadata md) {
		if (!result.contains(md)) {
			if (addIt)
				result.add(md);
			if (md.getParent() != null && md.getParent().getId().equals(md.getId()))
				md.setParent(null);

			existingIds.add(md.getId());
		}
	}
	
    public void addAllNoParent(Collection<? extends PresentableMetadata> md) {
		for (PresentableMetadata m : md)
			addNoParent(m);
	}
	
	public void add(PresentableMetadata parent, PresentableMetadata md) {
		if (parent == null)
			addNoParent(md);
		else
			add(md);
	}
	
	public void add(PresentableMetadata md) {
		if (!result.contains(md)) {
			if (md.getParent() == null)
				md.setParent(getItem());
//			GWT.log("MDP Adding " + md.getLabel() + " w/parent " + md.getParent().getLabel());
			if (addIt)
				result.add(md);
			existingIds.add(md.getId());
		}
	}
	
	public void add(int inx, PresentableMetadata md) {
		if (!result.contains(md)) {
			if (md.getParent() == null)
				md.setParent(getItem());

			if (addIt)
				result.add(inx, md);
			existingIds.add(md.getId());
		}
	}
	
	@Override
	public void onItemRemoved(PresentableMetadata collectionItem) {
		result.remove(collectionItem);
	}

	public PresentableMetadata getItem() {
		return item;
	}

	public List<PresentableMetadata> getList() {
		return result;
	}
	
	public int getStartIndex() {
		return start;
	}
	
	public int getEndIndex() {
		return end;
	}
	
	public void setStart(int st) {
		start = st;
	}
	
	public void setEnd(int en) {
		end = en;
	}
	
	public void thresholdEnd() {
		if (getEndIndex() == 0 || getEndIndex() > result.size())
			setEnd(result.size());
	}

	
	protected Set<PresentableMetadata> clearResults(Collection<PresentableMetadata> result) {
		Set<PresentableMetadata> removed = new HashSet<PresentableMetadata>();
		for (PresentableMetadata mdc : result)
			if (mdc.getId() != null) {
				existingIds.remove(mdc.getId());
			}
		
		result.clear();
		return removed;
	}
	
	protected void removeUnused(Set<PresentableMetadata> removed) {
		for (PresentableMetadata md: removed)
			if (!result.contains(md))
				treePresenter.onItemRemoved(md);
	}
	
	
	public void refresh() {
		if (end == 0 || end > result.size())
			end = result.size();
		
		if (end == 0)
			return;
		
		Set<PresentableMetadata> sel = treePresenter.getSelected();
		
		GWT.log("MDP Refresh " + ((item == null) ? "all" : item.getFriendlyName()) + " with " + 
		(end - start) + " rows @ " + startPage + "/" + start);

		while (startPage > result.size())
			startPage -= PAGE_SIZE;
		if (startPage < 0)
			startPage = 0;
		int showThis = result.size();
		if (showThis > startPage + PAGE_SIZE)
			showThis = startPage + PAGE_SIZE;
		
		List<PresentableMetadata> ss = new ArrayList<PresentableMetadata>();
		ss.addAll(result.subList(startPage, showThis)); 
		updateRowData(startPage, ss);
		updateRowCount(showThis - startPage, true);

		for (HasData<PresentableMetadata> disp: getDataDisplays())
			disp.setVisibleRangeAndClearData(new Range(start, showThis - startPage + start), true);
	}
	
	@Override
	public void refresh(PresentableMetadata item) {
		refresh();
	}
	
	public abstract void categorizeResults(List<PresentableMetadata> result);

	@Override
	public void refreshList(List<? extends PresentableMetadata> newContent) {
		boolean hasNew = false;
		
			for (PresentableMetadata md: newContent)
				if (!result.contains(md) && 
						(md.getId() == null || !existingIds.contains(md.getId()))) {
					result.add(md);
					hasNew = true;
				}
			
			if (!hasNew) {
//				System.out.println("No new items during refresh");
				return;
			}
			
//			GWT.log("** refreshList called externally with " + newContent + " ** ");
			
//			for (PresentableMetadata md: result)
//				if (md instanceof SearchResult)
//					GWT.log("** refreshList with child " + md + " **");
//			System.out.println("Refreshed size " + result.size());
			categorizeResults(result);
			while (startPage > result.size())
				startPage -= PAGE_SIZE;
			if (startPage < 0)
				startPage = 0;
			if (end == 0 || end > result.size())
				end = result.size();

//			System.out.println("MDP RefreshList " + 
//					((item == null) ? "all" : item.getFriendlyName()) + 
//					" with " + (end - start) + " rows out of " + newContent.size());
//			System.out.println("Refreshed size " + result.size());

			int showThis = result.size();
			if (showThis > startPage + PAGE_SIZE)
				showThis = startPage + PAGE_SIZE;
			List<PresentableMetadata> sublist  = new ArrayList<PresentableMetadata>();
			sublist.addAll(result.subList(startPage, showThis));
			updateRowData(startPage, sublist);
			updateRowCount(showThis - startPage, true);
			
			for (HasData<PresentableMetadata> disp: getDataDisplays())
				disp.setVisibleRangeAndClearData(new Range(start, showThis - startPage + start), true);
//		}
			
			if (item != null)
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new MetadataContentsUpdatedEvent(item));
	}

	@Override
	public void prevPage(PresentableMetadata collectionItem) {
		if (!collectionItem.equals(item))
			return;
		
		startPage -= PAGE_SIZE;
		if (startPage < 0)
			startPage = 0;
		
//		GWT.log("MDP Prev page to " + startPage);

		if (parent != null)
			parent.refresh(null);
		else
			refresh();
	}

	@Override
	public void nextPage(PresentableMetadata collectionItem) {
		if (!collectionItem.equals(item))
			return;
		
		startPage += PAGE_SIZE;
		while (startPage > result.size())
			startPage -= PAGE_SIZE;
		if (startPage < 0)
			startPage = 0;
		
//		GWT.log("MDP Next page to " + startPage);
		
		if (parent != null)
			parent.refresh(null);
		else
			refresh();
	}
	
	public int getCursor() {
		return startPage;
	}

	@Override
	public void toggleStar(PresentableMetadata collectionItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isStarred(PresentableMetadata collectionItem) {
		return (model.getFavoriteSnapshots().contains(collectionItem));
	}

	/**
	 * Returns true if the pagination is such that we're on the first page
	 */
	@Override
	public boolean isFirstPage(PresentableMetadata collectionItem) {
		return (start + startPage) == 0;
	}

	/**
	 * Returns true if the pagination is such that we're on the last
	 * page
	 */
	@Override
	public boolean isLastPage(PresentableMetadata collectionItem) {
		boolean ret = startPage + PAGE_SIZE >= getList().size();

		return ret;
	}

	public void addChildProvider(PresentableMetadata md, MetadataProvider child) {
		children.put(md,  child);
	}
	
	public abstract List<PresentableMetadata> getNewRootContent();
	
	public abstract List<PresentableMetadata> getNewItemContent();
	
	/**
	 * Return the list of items satisfying the filter
	 * @param source
	 * @return
	 */
	public List<PresentableMetadata> filter(List<PresentableMetadata> source) {
		if (filter == null)
			return source;
		
		List<PresentableMetadata> filtered = new ArrayList<PresentableMetadata>();
		
		for (PresentableMetadata md: source)
			if (satisfiesFilter(md))
				filtered.add(md);

		return filtered;
	}
	
	@Override
	protected void onRangeChanged(HasData<PresentableMetadata> display) {
		final Range range = display.getVisibleRange();
//		
		setStart(range.getStart());
		setEnd(getStartIndex() + range.getLength());
		
		Set<PresentableMetadata> removed;
		if (item != null) {
			removed = clearResults(result);
		} else
			removed = new HashSet<PresentableMetadata>();
		
		if (item == null)
			addAll(getNewRootContent());
		else 
			addAll(getNewItemContent());
		
		thresholdEnd();
		
		int showThis = result.size();
		if (showThis > startPage + PAGE_SIZE)
			showThis = startPage + PAGE_SIZE;
		updateRowData(getStartIndex(), result.subList(startPage, showThis));
		updateRowCount(showThis - startPage, true);
//		}
		
		removeUnused(removed);
	}


	public abstract void clearSearchResults() ;
	
	public boolean satisfiesFilter(PresentableMetadata item) {
		if (filter == null)
			return true;
		else return filter.transitivelySatisfiesFilter(item);
	}
	
	public void redraw() {
		int showThis = result.size();
		if (showThis > startPage + PAGE_SIZE)
			showThis = startPage + PAGE_SIZE;

		for (HasData<PresentableMetadata> disp: getDataDisplays())
			disp.setVisibleRangeAndClearData(new Range(start, showThis - startPage + start), true);
	}

	@Override
	public void toggleRating(PresentableMetadata collectionItem) {
		// TODO Auto-generated method stub
		
	}
}
