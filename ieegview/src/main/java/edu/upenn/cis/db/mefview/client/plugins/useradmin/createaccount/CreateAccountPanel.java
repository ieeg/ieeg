/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import java.util.Arrays;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.BadPasswordDialogBox;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.IllegalDialogBox;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.MismatchDialogBox;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.PasswordChangedDialogBox;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class CreateAccountPanel extends AstroTab implements  
CreateAccountPresenter.Display {
	HandlerRegistration hr;
	ServerConfiguration config;

	FlowPanel fp = new FlowPanel();
	TextBox login = new TextBox();
	TextBox password = new PasswordTextBox();
	TextBox password2 = new PasswordTextBox();
	TextBox email = new TextBox();
	TextBox firstName = new TextBox();
	TextBox surname = new TextBox();
	TextBox phone = new TextBox();
	TextArea dataUseDescription = new TextArea();
	TextBox awsInbox = new TextBox();
	TextBox awsAccessKey = new TextBox();
	TextBox institution = new TextBox();
	TextBox street = new TextBox();
	TextBox city = new TextBox();
	TextBox state = new TextBox();
	TextBox zip = new TextBox();
	TextBox country = new TextBox();
	TextBox photoUrl = new TextBox();
	
	List<String> titles = Arrays.asList(new String[] {"PhD", "MD/PhD", "JD", "MS/MPhil/MSE", "BS/BPhil/BSE", "Other"});
	
	ListBox titleBox = new ListBox(false); 
	TextBox job = new TextBox();
	TextArea termsBox = new TextArea();
	
	CheckBox acknowledge;
	CheckBox research;
	CheckBox terms = new CheckBox("I accept the terms and conditions of use");
	Button button;
	ScrollPanel main = new ScrollPanel();

	public final static String NAME = "CreateAccount";
	final static CreateAccountPanel seed = new CreateAccountPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public CreateAccountPanel() {
		super();
	}

	public CreateAccountPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
//			final String user,
			final String title
			) {
		
		super(clientFactory, model, title);
		
		add(main);
		
		// Load the list of academic titles
		for (String i: titles)
			titleBox.addItem(i);
		
		main.add(fp);
		main.setSize("100%", "100%");
		setWidgetLeftRight(main, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(main, 1, Unit.PX, 1, Unit.PX);

		Label heading;
		if (getSessionModel().isRegisteredUser())
			heading = (new Label("User Account Settings"));
		else
			heading = (new Label("User Account Creation"));

		fp.add(heading);
		heading.setStyleName("IeegSubHeader");
		
		fp.add(new HTML("<p></p>"));
		
		// TODO: allow for reg via Facebook / Google+ ?
		
		fp.add(new Label("Login name (username):"));
		fp.add(login);

		if (getSessionModel().isRegisteredUser()) {
			login.setEnabled(false);
		} else {
			fp.add(new Label("Password:"));
			fp.add(password);
			fp.add(new Label("Password (again):"));
			fp.add(password2);
		}
		
		fp.add(new Label("Email address:"));
		fp.add(email);
		fp.add(new Label("URL to photograph:"));
		fp.add(photoUrl);
		
		fp.add(new Label("First name:"));
		fp.add(firstName);
		fp.add(new Label("Last name / surname:"));
		fp.add(surname);
		
		if (getSessionModel().isRegisteredUser())
			button = new Button("Update account");
		else
			button = new Button("Create account");
	}

	@Override
	public
	void initState(ServerConfiguration config) {

		this.config = config;
		acknowledge = new CheckBox("I agree to acknowledge  " + config.getServerName() + " on my project's Web site and reference it in any publications associated with images or data obtained from  " + config.getServerName() + ".");
		research = new CheckBox("I agree to use the images and data obtained from  " + config.getServerName() + ".");
		
		termsBox.setText("The individual researchers who upload and/or contribute images to " + 
				config.getServerName() + 
				" retain ownership of their respective individual images. The University of Pennsylvania and the Mayo Clinic (the '" + config.getServerName() + " Administrators') own and retain ownership of all right, title and interest in and to the database itself, and the collection of images and data comprising " + config.getServerName() + " itself, subject to any reserved rights of the U.S. government as a result of its funding of the creation of " + config.getServerName() + ". The owners retain all ownership rights in all images and data downloaded from " + config.getServerName() + ".  " + config.getServerName() + " is the sole distributor for images and data contained in the database. Copying and distributing images or data from " + config.getServerName() + " without authorization from the " + config.getServerName() + " Administrator is strictly prohibited. To request permission, please contact the " + config.getServerName() + " administrator." + 
				"You will use your username and password solely to obtain access to " + config.getServerName() + " for the research project listed on your application. You will not share your username and password with any other person, even another researcher in your research group or institution. (Additional researchers in your group or institution may apply for separate usernames and passwords.) You will use the images and data solely to conduct the research identified in this application, and/or solely to teach or other educational purposes. You will not publish any image or data from " + config.getServerName() + " that you do not own, without the prior consent from the " + config.getServerName() + " Administrator. For permission to publish an image or data, please contact the " + config.getServerName() + " administrator." +  
				"You will not upload or contribute any content that you do not own or have the legal right to upload and contribute to the  " + config.getServerName() + ". You are solely responsible for defending any allegation regarding content that you upload or contribute, including but not limited to copyright or patent infringement allegations."  +
				"The University of Pennsylvania and the Mayo Clinic and the individual contributors of images or data, are not responsible for the correctness or usability of the images and data, or for the functionality of the database. The  " + config.getServerName() + " and access to the  " + config.getServerName() + " are provided 'AS IS,' without any representation or warranty. The  " + config.getServerName() + " Administrators do not warrant or represent the accuracy of the images or data or functionality of " + config.getServerName() + ", and hereby waive all representations and warranties, express or implied, including but not limited to any warranty of noninfringement, merchantability, or fitness for a particular purpose." + 
				"You are solely responsible for any harm or damages that arise from or are related to your use or misuse of " + config.getServerName() + ". The " + config.getServerName() + " Administrators are not liable (whether in an action for negligence, other tort, breach of contract or any other legal theory), for any loss, damage, expense, loss of profits, loss of revenue, loss of reputation, or loss or inaccuracy of data, or any indirect, incidental, punitive, special or consequential damages, even if an " + config.getServerName() + " Administrator is advised of the possibility of such damages, in connection with: (A) your reliance on or use or inability to use " + config.getServerName() + " or any images or data or other aspects of this website; or (B) any failure of performance, error, omission, interruption, defect, delay in operation or transmission, computer virus or line or system failure, " + config.getServerName() + ", or any images or data or other aspects of this website." + 
				"An " + config.getServerName() + " Administrator evaluates each application, and may reject a request for access or for a username and password, for any or no reason, without giving any explanation for its determination." + 
				"An " + config.getServerName() + " Administrator may block or terminate your access to " + config.getServerName() + " at any time, for any or no reason, without giving any explanation for its determination."  +
				"The data in this application form will be transmitted in an unsecure session. The information transmitted in the application form will be retained for administrative purposes. The " + config.getServerName() + " Administrator will use reasonable efforts to maintain the confidentiality of the information submitted in the application form, but is not responsible for any disclosure or misuse of such information. Every effort has been taken to de-­‐identify the data sets contained in " + config.getServerName() + ". By applying for a username and password to access " + config.getServerName() + ", you agree to report to the " + config.getServerName() + " Administrators any potential discovery of Personal Health Information (PHI) or other information which identifies a human being. If you do discover any PHI or other information which identifies a human being, you also will not further use or disclose the PHI. (For a complete PHI listing and privacy rule explanation, please refer to the U.S. Department of Health and Human Services website: http://privacyruleandresearch.nih.gov/pr_02.asp) If you discover another aspect of " + config.getServerName() + " or its images or data that you believe violates a law or standard of conduct in your country, please contact the " + config.getServerName() + " Administrators via email." +
				"You agree to link to the " + config.getServerName() + " home page if you have a website for your research project. You also agree to acknowledge the use of " + config.getServerName() + " on your project's website and reference it in any publications associated with the images or data obtained from " + config.getServerName() + ". You also agree to include the following statement in any publication or presentation made that uses an image or data from " + config.getServerName() + ": 'Access to the image(s) and/or data was supported by Award number U24NS06930 from the U.S. National Institute of Neurological Disorders and Stroke. The content of this publication/presentation is solely the responsibility of the authors, and does not necessarily represent the official views of the U.S. National Institute for Neurological Disorders and Stroke or the U.S. National Institutes of Health.'" +
				"The " + config.getServerName() + " Administrators may amend or change these terms and conditions at any time, and you agree to abide by the amended or changed terms when they are posted. The " + config.getServerName() + " Administrators will post the amended or changed terms on the " + config.getServerName() + " web site. By using  " + config.getServerName() + "  after an amendment or change is posted, you will be deemed to have agreed to the amended or changed terms and conditions. If you do not agree to the amendment or change, do not access or use  " + config.getServerName() + ", and please return your username and password to  " + config.getServerName() + " Administrator." +
				"Each provision of these terms and conditions is severable. If, for any reason, any provision of these terms and conditions shall be held invalid or unenforceable in whole or in part in any jurisdiction, such provision shall, as to such jurisdiction, be ineffective to the extent of such invalidity or un-enforceability, without in any manner affecting the validity or enforceability thereof in any other jurisdiction or the remaining provisions hereof in any jurisdiction." +
				"These terms and conditions are the final and entire agreement between a user and his/her employer or research organization, and the " + config.getServerName() + " Administrators, and supersede all prior understandings, communications or agreements on the subject matter in these terms and conditions. These terms and conditions, and your access to and use of " + config.getServerName() + ", shall be governed by Pennsylvania law, without regard to its conflicts of law principles, except where prohibited by law.");
		
		
		if (!config.isUniversity()) {
			fp.add(new Label("Academic degree title:"));
			fp.add(titleBox);
			fp.add(new Label("Job position or academic role:"));
			fp.add(job);
		}
		
		if (config.needsResearchAgreement()) {
			fp.add(new Label("Phone number (for validation purposes only):"));
			fp.add(phone);
			fp.add(new Label("Data use purposes:"));
			fp.add(dataUseDescription);
		}
		
		if (!config.isUniversity()) {
			fp.add(new Label("Amazon AWS `inbox':"));
			fp.add(awsInbox);
			fp.add(new Label("Amazon AWS access key ID:"));
			fp.add(awsAccessKey);
		}
		
		fp.add(new Label("Affiliated institution / organization"));
		fp.add(institution);
		fp.add(new Label("Street address"));
		fp.add(street);
		fp.add(new Label("City/town:"));
		fp.add(city);
		fp.add(new Label("Zip/postal code:"));
		fp.add(zip);
		fp.add(new Label("State/province:"));
		fp.add(state);
		fp.add(new Label("Country:"));
		fp.add(country);
		
		if (config.isNeedsAuthorizedParty()) {
			fp.add(new Label("Institutional homepage:"));

			fp.add(new Label("Institutional contact"));
			fp.add(new Label("First name"));
			fp.add(new Label("Surname"));
			fp.add(new Label("Job position or academic role:"));
			fp.add(new Label("Department:"));
			fp.add(new Label("Postal mailing address:"));
			fp.add(new Label("City/town:"));
			fp.add(new Label("Zip/postal code:"));
			fp.add(new Label("State/province:"));
			fp.add(new Label("Country:"));
			fp.add(new Label("Email:"));
			fp.add(new Label("Phone number (for validation purposes only):"));
			fp.add(new Label("Fax number (for validation purposes only):"));
		 }

		if (!getSessionModel().isRegisteredUser()) {
			fp.add(new Label("Terms & conditions"));
			fp.add(termsBox);
			termsBox.setSize("100%", "400px");
		}
		fp.add(new HTML("<p></p>"));

		if (!getSessionModel().isRegisteredUser()) {
			if (config.needsResearchAgreement()) {
				fp.add(acknowledge);
				fp.add(research);
			}
			
			fp.add(terms);
		}
		fp.add(new HTML("<p></p>"));
		
		// Captcha?
		
		fp.add(button);
	}
	
	@Override
	public void setHandler(ClickHandler buttonHandler) {
		button.addClickHandler(buttonHandler);
	}
	
	@Override
	public void showIllegal() {
		new IllegalDialogBox().center();
	}

	@Override
	public void showMismatch() {
		new MismatchDialogBox().center();
	}
	
	@Override
	public void showBadPassword() {
		new BadPasswordDialogBox().center();
	}

	@Override
	public void showChangedPassword() {
		new PasswordChangedDialogBox().center();
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	/**
	 * This is the real constructor for the panel.  It gets called by the 
	 * PanelFactory, then calls the constructor with the appropriate
	 * parameters.
	 */
	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		return new CreateAccountPanel(model, clientFactory, title);
	}

	/**
	 * This ultimately maps to a Resource that corresponds to a PNG in
	 * ...mefview.client.images.
	 */
	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getUnknownUser();
	}

	/**
	 * This is for saving state on shutdown, but can be ignored
	 */
	@Override
	public WindowPlace getPlace() {
		// TODO if we ever want to save on shutdown
		return null;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		// TODO if we ever support this
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return null;
//	}

	/**
	 * A friendly name for what our panel does
	 */
	public String getTitle() {
		if (getSessionModel().isRegisteredUser())
			return "Update Account";
		else
			return "Create Account";
	}

	/**
	 * This stub would be used to attach a handler to
	 * some control widget on the panel. 
	 */
	@Override
	public void addClickHandlerToButton(ClickHandler handler) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getNewPassword() {
		return password.getText();
	}

	@Override
	public String getNewPassword2() {
		return password2.getText();
	}

	@Override
	public UserAccountInfo getAccountInfo() {
		return new UserAccountInfo(
				login.getText(),
				email.getText(),
				firstName.getText(),
				surname.getText(),
				institution.getText(),
				job.getText(),
				street.getText(),
				city.getText(),
				state.getText(),
				zip.getText(),
				country.getText(),
				(!config.isUniversity() && titleBox.getSelectedIndex() > -1) ? 
							titleBox.getItemText(titleBox.getSelectedIndex()) : null,
				photoUrl.getText(),
				!config.isUniversity() ? 
						awsInbox.getText() : null,
				!config.isUniversity() ? 
						awsAccessKey.getText() : null,
				null // DropBox
				);
	}
	
	@Override
	public boolean isInAgreement() {
		return (!config.needsResearchAgreement() || (acknowledge.getValue() &&
				research.getValue())) &&
				terms.getValue();
	}

	@Override
	public void showErrorCreatingAccount() {
		new ErrorCreatingAccountDialogBox().center();
	}

	@Override
	public void showCreatedAccount() {
		new CreatedAccountDialogBox().center();
	}

	@Override
	public void showNoAgreement() {
		new NoAgreementDialogBox().center();
	}

	@Override
	public void setAccountInfo(UserAccountInfo result) {
		/**
			String street,
			String city,
			String state,
			String zip,
			String country,
			String academicTitle,
			String photo,
			@Nullable String inboxBucket,
			@Nullable String inboxAwsKey) {
		 */
		login.setText(result.getLogin());
		email.setText(result.getEmail());
		if (result.getProfile().getFirstName().isPresent())
			firstName.setText(result.getProfile().getFirstName().get());
		if (result.getProfile().getLastName().isPresent())
			surname.setText(result.getProfile().getLastName().get());
		if (result.getProfile().getInstitutionName().isPresent())
			institution.setText(result.getProfile().getInstitutionName().get());
		if (result.getProfile().getStreet().isPresent())
			street.setText(result.getProfile().getStreet().get());
		if (result.getProfile().getCity().isPresent())
			city.setText(result.getProfile().getCity().get());
		if (result.getProfile().getState().isPresent())
			state.setText(result.getProfile().getState().get());
		if (result.getProfile().getZip().isPresent())
			zip.setText(result.getProfile().getZip().get());
		if (result.getProfile().getCountry().isPresent())
			country.setText(result.getProfile().getCountry().get());
		if (result.getProfile().getJob().isPresent())
			job.setText(result.getProfile().getJob().get());
		
		photoUrl.setText(result.getPhoto());
		
		if (result.getProfile().getAcademicTitle().isPresent())
			for (int i = 0; i < titles.size(); i++) {
				if (titles.get(i).equals(result.getProfile().getAcademicTitle().get()))
					titleBox.setSelectedIndex(i);
			}
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.MODELESS_DIALOG;
	}

	@Override
	public void bindDomEvents() {
	}
}
