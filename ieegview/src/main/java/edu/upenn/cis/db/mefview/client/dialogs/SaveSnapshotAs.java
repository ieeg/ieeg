/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.commands.snapshot.DeriveSnapshotCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.SaveAnnotationsToSnapshotCommand;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.events.CloseDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class SaveSnapshotAs extends DialogBox {
	ListBox datasetList = new ListBox();
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
//	TextBox theLabel = new TextBox();
	TextBox theTool = new TextBox();
	TextBox theType = new TextBox();
	
	AstroPanel activePanel;
	
	/**
	 * 
	 * @param datasets Set of datasets, in the form of serach results
	 * @param sourceFriendly Friendly name of the source annotations' study
	 * @param sourceDsId Rev ID of the source annotations' study
	 * @param annList List of possible annotations
	 * @param annManager Map of possible annotations
	 */
	public SaveSnapshotAs(final String revID,
			final String userID,
			final DataSnapshotViews model,
//			final ToolInfo tool,
			final Collection<Annotation> annotations,
			final ClientFactory factory,//,
			final AstroPanel activePanel
//			final List<ToolResult> experimentList
			) {
		VerticalPanel vp = new VerticalPanel();
		this.activePanel = activePanel;
		
		setTitle("Save Data Snapshot As");
		setText("Save Data Snapshot As");
		setWidget(vp);
	
		vp.add(new Label("Description of manual curation:"));
		vp.add(theTool);
		theTool.setText("Manual annotations by " + userID);
		
		vp.add(new Label("Snapshot name (must be unique):"));
		vp.add(theType);
		Date d = new Date();
		String str = DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(d);
		
		theType.setText(str + " - " + userID + "-annotated");
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(ok);
		hp.add(cancel);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
				
//				ArrayList<String> revIDs = new ArrayList<String>();
//				for (TraceInfo ti: model.getState().getTraceInfo())
//					revIDs.add(ti.getRevId());

				final DialogBox d = PanelFactory.getPleaseWaitDialog("Deriving new snapshot");
				d.center();
				d.show();
				
				// Create a new derived snapshot w/o annotations
//				model.deriveSnapshotFrom(
				Command snap =
						new DeriveSnapshotCommand(
								factory,
								model,
								model.getState().getSearchResult(),
								theTool.getText(), 
								theType.getText(), 
								null, //experimentList,
								model.getState().getTraceInfo(),//revIDs, 
								false, 
								d, 
								new OnDoneHandler() {

							@Override
							public void doWork(final String studyId) {
								SaveAnnotationsToSnapshotCommand cmd = 
										new SaveAnnotationsToSnapshotCommand(
												factory,
												studyId, 
												annotations,
										new OnDoneHandler() {

											@Override
											public void doWork(String studyId2) {
												d.hide();
												SearchResult sr = new SearchResult();
												sr.setBaseFriendly(model.getState().getFriendlyName());
												sr.setBaseRevId(model.getState().getSnapshotID());
												sr.setCreator(theTool.getText());
												sr.setFriendlyName(theType.getText());
												sr.setDatasetRevId(studyId2);
												
												sr.setAnnotationCount(model.getState().getAnnotationModel().getAnnotationTimes().size());
												sr.setTimeSeries(model.getSearchResult().getTimeSeries());
//												sr.setTimeSeriesLabels(model.getSearchResult().getTimeSeriesLabels());
//												sr.setTimeSeriesRevIds(model.getSearchResult().getTimeSeriesRevIds());
												sr.setType(model.getSearchResult().getType());
												
												sr.setParent(model.getSearchResult());
												
												model.getSearchResult().addDerived(sr);
												
												List<SearchResult> resultsList = new ArrayList<SearchResult>();
												resultsList.add(sr);
												
												IeegEventBusFactory.getGlobalEventBus().fireEvent(new SearchResultsUpdatedEvent(null, resultsList, false));
												//model.reload(studyId, studyId2);//, model.getState().getFriendlyName() + "-" + theType.getText());
//												model.removeThisStudy();
												IeegEventBusFactory.getGlobalEventBus().fireEvent(new CloseDisplayPanelEvent(activePanel));
												OpenDisplayPanel act = new OpenDisplayPanel( 
														sr, false, 
														EEGViewerPanel.NAME,
														null);
												IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
											}
								});
								cmd.execute();
								
							}
					
				});
				snap.execute();
			}
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}
			
		});

	}
	
//	public String getTitle() {
//		return theLabel.getText();
//	}
//	
	public String getTargetRevId() {
		return datasetList.getValue(datasetList.getSelectedIndex());
	}
	
	public String getTargetLabel() {
		return datasetList.getItemText(datasetList.getSelectedIndex());
	}
}
