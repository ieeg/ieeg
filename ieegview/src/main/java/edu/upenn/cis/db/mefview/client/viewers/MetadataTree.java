/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.AbstractSafeHtmlCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.ImageResourceCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.SafeHtmlRenderer;
import com.google.gwt.user.client.ui.ImageResourceRenderer;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetaCollectionProvider;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataProvider;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataProviderBase;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagingCellTree;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.MetadataFormatter;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;


/**
 * Tree viewer for metadata.  Includes checkbox, clickable star, icon, entry, etc. in an expandable
 * tree.
 *   
 * @author zives
 *
 */
public class MetadataTree extends PagingCellTree<PresentableMetadata,MetadataProviderBase>
implements MetadataPresenter {
	/**
	 * Clickable image cell
	 * 
	 * @author zives
	 *
	 * @param <T>
	 * @param <O>
	 */
	public static class ClickableImageResourceCell<T extends ImageResource,O> extends 
	AbstractSafeHtmlCell<T> {

		public static interface ClickHandler<O> {
			public void handleClick(O key);
		}

		ClickHandler<O> handler;

		public ClickableImageResourceCell(ClickHandler<O> handler) {
			super(new ClickableImageResourceRenderer<T>(), "click");
			this.handler = handler;
		}

		@Override
		public void onBrowserEvent(Context context, Element parent, T value,
				NativeEvent event, ValueUpdater<T> valueUpdater) {
			super.onBrowserEvent(context, parent, value, event, valueUpdater);
			if ("click".equals(event.getType())) {
				handler.handleClick((O)context.getKey());
			}
		}

		@Override
		public boolean isEditing(Context context, Element parent, T value) {
			return true;
		}

		@Override
		protected void render(com.google.gwt.cell.client.Cell.Context context,
				SafeHtml data, SafeHtmlBuilder sb) {
			if (data != null) {
				sb.append(data);
			}
		}

		@Override
		public boolean dependsOnSelection() {
			return true;
		}

	}
	public static class ClickableImageResourceRenderer<T extends ImageResource> implements SafeHtmlRenderer<T> {
		//Just delegate to standard ImageResourceRendere since we are not doing anything custom here
		private static ImageResourceRenderer imageRenderer = new ImageResourceRenderer();

		@Override
		public SafeHtml render(T yourObject) {
			return imageRenderer.render(yourObject);
		}

		@Override
		public void render(T yourObject, SafeHtmlBuilder builder) {
			builder.append(render(yourObject));
		}

	}

	/**
	 * The cell used to render categories.
	 */
	private static class StringCell extends AbstractCell<String> {

		public StringCell() {
		}

		@Override
		public void render(Context context, String value, SafeHtmlBuilder sb) {
			if (value != null) {
				sb.appendEscaped(value);
			}
		}
	}

//	public static final int MAX_COLUMNS = 5;

	/**
	 * Handler for the Favorites star
	 */
//	StarToggleHandler<PresentableMetadata> starHandler;
	private MetadataModel model;

//	Map<PresentableMetadata,MetadataProviderBase> providers = 
//			new HashMap<PresentableMetadata,MetadataProviderBase>();

//	private final MetadataProviderBase categoryDataProvider;
//	private final Cell<PresentableMetadata> snapshotCell;
//	private final DefaultSelectionEventManager<PresentableMetadata> selectionManager =
//			DefaultSelectionEventManager.createCheckboxManager();
//	private final SingleSelectionModel<PresentableMetadata> selectionModel;
//	ClientFactory clientFactory;

	private String defaultCategory;

	Set<String> knownIds = new HashSet<String>();
	CollectionNode searchResultsCollection = null;

	public MetadataTree(final SingleSelectionModel<PresentableMetadata> selectionModel, 
			MetadataModel bmodel, ClientFactory cf, List<String> defaultCollections) {

		super(selectionModel, 
				cf);
		MetadataFactory.getFactory(cf, bmodel);

//		this.selectionModel = selectionModel;
//		clientFactory = cf;

		this.model = bmodel;

		defaultCategory = defaultCollections.get(defaultCollections.size() - 1);

		// Create a data provider that provides categories.
		rootDataProvider = new MetadataProvider(null, bmodel, cf, 
				defaultCategory, this, knownIds);

		searchResultsCollection = new CollectionNode(null, defaultCategory, this);;
		rootDataProvider.addNoParent(searchResultsCollection);
		getOrReuseProvider(searchResultsCollection);


		for (String coll: defaultCollections) {
			if (!coll.equals(defaultCategory)) {
				CollectionNode collNode = new CollectionNode(null, coll, this);
				rootDataProvider.addNoParent(collNode);

				MetadataProviderBase provider = getOrReuseProvider(collNode);
			}
		}

		//	    System.out.println("Search results collection " + searchResultsCollection);

		if (searchResultsCollection != null)
			selectionModel.setSelected(searchResultsCollection, true);
	}


	@Override
	public <T> NodeInfo<?> getNodeInfo(T value) {
		// Contents of NULL are top-level entries (currently snapshots)
		if (value == null) {
			// Return top level categories.
			GWT.log("Default root node");
			return new DefaultNodeInfo<PresentableMetadata>(rootDataProvider,
					basicCell, selectionModel, 
					selectionManager, null);

			// Contents of SearchResults
		} else if (value instanceof SearchResult) {
			SearchResult result = (SearchResult) value;

			MetadataProviderBase provider = getOrReuseProvider(result);

			GWT.log("Snapshot node for " + result.getFriendlyName());
			return new DefaultNodeInfo<PresentableMetadata>(provider, basicCell, selectionModel, 
					selectionManager, null);

		} else if (value instanceof CollectionNode) {
			CollectionNode category = (CollectionNode) value;

			GWT.log("Generating collection node for " + category.getId() + "/" + category.getAllMetadata().size());

			MetadataProviderBase provider = getOrReuseProvider(category);

			return new DefaultNodeInfo<PresentableMetadata>( 
					provider,
					basicCell, selectionModel, 
					selectionManager, null);

			// Contents of other stuff
		} else if (value instanceof PresentableMetadata) {//if (value instanceof BasicMetadata && ((BasicMetadata)value).isNested()) {
			PresentableMetadata category = (PresentableMetadata) value;

			GWT.log("TS node for " + category.getId());
			MetadataProviderBase provider = getOrReuseProvider(category);
			return new DefaultNodeInfo<PresentableMetadata>(provider, basicCell, selectionModel, 
					selectionManager, null);
		}

		// Unhandled type.
		String type = value.getClass().getName();
		throw new IllegalArgumentException("Unsupported object type: " + type);
	}


	// Each contents item for a snapshot
	@Override
	protected Cell<PresentableMetadata> createBasicCells() {
		// Construct a composite cell for contacts that includes a checkbox.
		List<HasCell<PresentableMetadata, ?>> snapshotCells = new ArrayList<HasCell<PresentableMetadata, ?>>();

		// The rating
		snapshotCells.add(new HasCell<PresentableMetadata, ImageResource>() {
			private ClickableImageResourceCell<ImageResource,PresentableMetadata> cell = 
					new ClickableImageResourceCell<ImageResource,PresentableMetadata>(
							new ClickableImageResourceCell.ClickHandler<PresentableMetadata>() {

								@Override
								public void handleClick(
										PresentableMetadata key) {
									GWT.log("Snapshot cell handler -- clicked rate");
									MetadataFactory.getFormatter(key.getClass()).toggleRating(key, MetadataTree.this);
								}

							}
							);

			public Cell<ImageResource> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, ImageResource> getFieldUpdater() {
				return null;
			}

			public ImageResource getValue(PresentableMetadata object) {
				MetadataFormatter formatter = MetadataFactory.getFormatter(object.getClass());
				if ((object instanceof SearchResult) && !((SearchResult)object).getTermsMatched().isEmpty() && 
						formatter != null && formatter.isRatable()) {
					if (formatter.isRated(object, MetadataTree.this) == Boolean.TRUE)
						return ResourceFactory.thumbsUp();
					else if (formatter.isRated(object, MetadataTree.this) == Boolean.FALSE)
						return ResourceFactory.thumbsDown();
					else
						return ResourceFactory.thumbsUnknown();
				} else
					return null;
			}
		});

		// The star
		snapshotCells.add(new HasCell<PresentableMetadata, ImageResource>() {
			private ClickableImageResourceCell<ImageResource,PresentableMetadata> cell = 
					new ClickableImageResourceCell<ImageResource,PresentableMetadata>(
							new ClickableImageResourceCell.ClickHandler<PresentableMetadata>() {

								@Override
								public void handleClick(
										PresentableMetadata key) {
									GWT.log("Snapshot cell handler -- clicked star");
									MetadataFactory.getFormatter(key.getClass()).toggleStar(key, MetadataTree.this);
								}

							}
							);

			public Cell<ImageResource> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, ImageResource> getFieldUpdater() {
				return null;
			}

			public ImageResource getValue(PresentableMetadata object) {
				MetadataFormatter formatter = MetadataFactory.getFormatter(object.getClass());
				if (formatter != null && formatter.isStarrable()) {
					if (formatter.isStarred(object, MetadataTree.this))
						return ResourceFactory.star();
					else
						return ResourceFactory.unstar();
				} else
					return null;
			}
		});

		// The icon
		snapshotCells.add(new HasCell<PresentableMetadata, ImageResource>() {
			private ImageResourceCell cell = new ImageResourceCell();

			public Cell<ImageResource> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, ImageResource> getFieldUpdater() {
				return null;
			}

			public ImageResource getValue(PresentableMetadata object) {
				if (MetadataFactory.getFormatter(object.getClass()) != null)
					return MetadataFactory.getFormatter(object.getClass()).getImage(object);
				else
					return ResourceFactory.document();
			}
		});
		// The optional left arrow
		snapshotCells.add(new HasCell<PresentableMetadata, ImageResource>() {
			private ClickableImageResourceCell<ImageResource,PresentableMetadata> cell = 
					new ClickableImageResourceCell<ImageResource,PresentableMetadata>(
							new ClickableImageResourceCell.ClickHandler<PresentableMetadata>() {

								@Override
								public void handleClick(
										PresentableMetadata key) {
									if (MetadataFactory.getFormatter(key.getClass()) != null)
										MetadataFactory.getFormatter(key.getClass()).prevPage(key, getOrReuseProvider(key));
								}

							}
							);

			public Cell<ImageResource> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, ImageResource> getFieldUpdater() {
				return null;
			}

			public ImageResource getValue(PresentableMetadata object) {
				if (MetadataFactory.getFormatter(object.getClass()) != null && 
						MetadataFactory.getFormatter(object.getClass()).showPrev(object, getOrReuseProvider(object))) {
					return ResourceFactory.left();
				} else
					return null;
			}
		});
		for (int i = 0; i < MAX_COLUMNS; i++) {
			snapshotCells.add(createValueCell(i));
		}

		// The optional right arrow
		snapshotCells.add(new HasCell<PresentableMetadata, ImageResource>() {
			private ClickableImageResourceCell<ImageResource,PresentableMetadata> cell = 
					new ClickableImageResourceCell<ImageResource,PresentableMetadata>(
							new ClickableImageResourceCell.ClickHandler<PresentableMetadata>() {

								@Override
								public void handleClick(
										PresentableMetadata key) {
									if (MetadataFactory.getFormatter(key.getClass()) != null)
										MetadataFactory.getFormatter(key.getClass()).nextPage(key, getOrReuseProvider(key));
								}

							}
							);

			public Cell<ImageResource> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, ImageResource> getFieldUpdater() {
				return null;
			}

			public ImageResource getValue(PresentableMetadata object) {
				if (MetadataFactory.getFormatter(object.getClass()) != null && 
						MetadataFactory.getFormatter(object.getClass()).showNext(object, getOrReuseProvider(object))) {
					return ResourceFactory.right();
				} else
					return null;
			}

		});
		return new CompositeCell<PresentableMetadata>(snapshotCells) {
			@Override
			public void render(Context context, PresentableMetadata value, SafeHtmlBuilder sb) {
				sb.appendHtmlConstant("<table><tbody><tr>");
				super.render(context, value, sb);
				sb.appendHtmlConstant("</tr></tbody></table>");
			}

			@Override
			protected Element getContainerElement(Element parent) {
				// Return the first TR element in the table.
				return parent.getFirstChildElement().getFirstChildElement().getFirstChildElement();
			}

			@Override
			protected <X> void render(Context context, PresentableMetadata value,
					SafeHtmlBuilder sb, HasCell<PresentableMetadata, X> hasCell) {
				Cell<X> cell = (hasCell instanceof ClickableCell<?, ?>) ? 
						((ClickableCell<PresentableMetadata, X>)hasCell).getCell(value) :
							hasCell.getCell();
						sb.appendHtmlConstant("<td>");
						cell.render(context, hasCell.getValue(value), sb);
						sb.appendHtmlConstant("</td>");
			}
		};
	}

	@Override
	public boolean isLeaf(Object value) {
		if (value == null)
			return false;

		return ((PresentableMetadata)value).isLeaf();
	}

	@Override
	public MetadataProviderBase getDefaultProvider() {
		for (PresentableMetadata md: getProvider().getList()) {
			if (md.getLabel().equals(defaultCategory))
				return getOrReuseProvider(md);
		}
		return null;
	}

	@Override
	public void removeFavorite(PresentableMetadata collectionItem) {
		model.removeFavorite(collectionItem);
	}

	@Override
	public void addFavorite(PresentableMetadata collectionItem) {
		model.addFavorite(collectionItem);
	}

	@Override
	public boolean isStarred(PresentableMetadata collectionItem) {
		boolean ret = (model.getFavoriteSnapshots().contains(collectionItem));

		return ret;
	}

	private ClickableCell createValueCell(final int i) {
		return (new ClickableCell<PresentableMetadata, String>() {

			private StringCell cell = new StringCell();

			public Cell<String> getCell() {
				return cell;
			}

			public FieldUpdater<PresentableMetadata, String> getFieldUpdater() {
				return null;
			}

			public String getValue(PresentableMetadata object) {
				if (MetadataFactory.getFormatter(object.getClass()) != null)
					return MetadataFactory.getFormatter(object.getClass()).getColumnContent(object, i);
				else if (i == 0)
					return object.getFriendlyName();
				else
					return "";
			}

			@Override
			public Cell<String> getCell(PresentableMetadata result) {
				return cell;
			}
		});
	}


	@Override
	public MetadataProviderBase createProvider(PresentableMetadata md) {
		return (!(md instanceof CollectionNode)) ? 
				new MetadataProvider(md, model, 
						clientFactory, defaultCategory, this, knownIds)
		:		new MetaCollectionProvider((CollectionNode)md, 
				model, clientFactory, defaultCategory, this, knownIds);
	}

	public CollectionNode getSearchResultCollection() {
		return searchResultsCollection;
	}


	@Override
	public void updateFeedback(PresentableMetadata item) {
		if (item instanceof SearchResult) {
			if (((SearchResult) item).getIsValid() == null) {
				((SearchResult) item).setIsValid(Boolean.TRUE);
			} else if (((SearchResult) item).getIsValid() == Boolean.TRUE) {
				((SearchResult) item).setIsValid(Boolean.FALSE);
			} else 
				((SearchResult) item).setIsValid(null);
		}
	}


}
