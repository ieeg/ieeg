/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.Project;

public class ProjectViewer extends TableViewer<Project> {
	boolean favorites;
	boolean selectable;

	public ProjectViewer(
			Collection<Project> users, int pageSize, boolean favorites, boolean selectable) {
		super(users, pageSize);
		this.favorites = favorites;
		this.selectable = selectable;

		initialize();
	}
	
	public ProjectViewer(
			int pageSize, boolean favorites, boolean selectable) {
		super(pageSize);
		this.favorites = favorites;
		this.selectable = selectable;
		
		initialize();
	}
	
	@Override
	protected void init() {
		clear();

		List<String> userColNames = newArrayList();
		List<TextColumn<Project>> userColumns = newArrayList();

		userColNames.add("Project");
		userColumns.add(NAME_COL);


		if (selectable) {
			table = new PagedTable<Project>(
					5,
					true,
					true,
					values,
					KEY_PROVIDER,
					userColumns);
		} else {
			table = new PagedTable<Project>(
					pageSize,
					false,
					false,
					values,
					KEY_PROVIDER,
					userColumns);
		}

		add(table);
	}
	
	public static final ProvidesKey<Project> KEY_PROVIDER = 
			new ProvidesKey<Project>() {

		public Object getKey(Project item) {
			return (item == null) ? null : item.getProjectId();
		}

	};

	public static final TextColumn<Project> NAME_COL = new TextColumn<Project>() {
		@Override
		public String getValue(Project object) {
			return (object == null) ? null : object.getName();
		}
	};

	public static final TextColumn<Project> TAGS_COL = new TextColumn<Project>() {
		@Override
		public String getValue(Project object) {
			if (object == null)
				return null;
			else {
				List<String> tags = newArrayList();
				tags.addAll(object.getTags());
				java.util.Collections.sort(tags);
				return tags.toString();
			}
		}
	};

}
