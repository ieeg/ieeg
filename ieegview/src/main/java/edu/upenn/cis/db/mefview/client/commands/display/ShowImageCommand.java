package edu.upenn.cis.db.mefview.client.commands.display;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.PopupPanel;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class ShowImageCommand implements Command {
	PresentableMetadata searchResult;
	PopupPanel source;
	
	public ShowImageCommand(PresentableMetadata md,
			PopupPanel source) {
		this.searchResult = md;
		this.source = source;
	}
	
	public void execute() {
		if (source != null)
			source.hide();
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new
				OpenDisplayPanel(
						searchResult, false,
						ImageViewerPanel.NAME, null)));
	}
}
