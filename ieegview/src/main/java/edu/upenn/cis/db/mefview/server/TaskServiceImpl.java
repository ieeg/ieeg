/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.JsonKeyValueSet;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.db.mefview.client.TaskServices;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Task;
import edu.upenn.cis.db.mefview.shared.TaskSubmission;
import edu.upenn.cis.db.mefview.shared.WebLink;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class TaskServiceImpl extends RemoteServiceServlet implements TaskServices {

	private final static Logger logger = LoggerFactory
			.getLogger(TaskServiceImpl.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ TaskServiceImpl.class);
	
	private static IIeegUserService uService;
	
	@VisibleForTesting
	TaskServiceImpl(final IUserService userService,
			final IDataSnapshotServer dsServer) {

		uService = HabitatUserServiceFactory.getUserService(userService, dsServer);
	}		

	
	public TaskServiceImpl() {
		
	}
	
	public void init() {
		if (uService == null) {
			uService = HabitatUserServiceFactory.getUserService();
		}
	}


	@Override
	public List<Task> getActiveTasks(SessionToken session) {
		final String M = "getActiveTasks()";
		try {
			uService.verifyUser(M, session, Role.USER);
			
			List<Task> ret = new ArrayList<Task>();
			
			// TODO: query for eligible tasks
			
			Calendar cal = Calendar.getInstance();
			cal.set(2015, 11, 11);
			Date due = cal.getTime();

			Calendar cal2 = Calendar.getInstance();
			cal2.set(2016, 1, 11);
			Date due2 = cal2.getTime();
			
			List<PresentableMetadata> resources = new ArrayList<PresentableMetadata>();
			List<String> allowed = new ArrayList<String>();
			List<String> patterns = new ArrayList<String>();
			JsonKeyValueSet config = null;
			JsonKeyValueSet validation = null;
			JsonKeyValueSet forms = null;

			ret.add(new Task(BtUtil.newUuid(), 
					"Test task",
					new Date(),
					due,
					due,
					"This is a descriptive task",
					new WebLink("","Destination", "http://www.cis.upenn.edu/~cis455/assignments"),
					"Homework 1 Project",
					resources,
					allowed,
					patterns,
					config,
					validation,
					forms
					));
			
			ret.add(new Task(BtUtil.newUuid(), 
					"Test 2nd task",
					new Date(),
					due2,
					due2,
					"This is a second task",
					new WebLink("","Destination", "http://www.cis.upenn.edu/~nets212/assignments"),
					"Homework 2 Project",
					resources,
					allowed,
					patterns,
					config,
					validation,
					forms
					));
			
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}


	@Override
	public List<TaskSubmission> getSubmittedTasks(SessionToken session) {
		final String M = "getActiveTasks()";
		try {
			uService.verifyUser(M, session, Role.USER);

			// TODO: query for tasks by user
			return new ArrayList<TaskSubmission>();
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}


	@Override
	public void submitForTask(SessionToken session, TaskSubmission submission) {
		final String M = "getActiveTasks()";
		try {
			uService.verifyUser(M, session, Role.USER);

			// TODO: register that a task was submitted and catalog its info

		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

}
