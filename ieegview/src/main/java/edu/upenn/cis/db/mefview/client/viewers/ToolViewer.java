/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;

public class ToolViewer extends TableViewer<ToolDto> {
	int offset;
	boolean favorites;
	boolean selectable;
	boolean showLess;

	public ToolViewer(
			Collection<ToolDto> tools, int pageSize,
			int offset, boolean favorites, boolean selectable,
			boolean showLess) {
		super(tools, pageSize);
		this.offset = offset;
		this.selectable = selectable;
		this.favorites = favorites;
		this.showLess = showLess;

		initialize();
	}

	public ToolViewer(
			int pageSize, int offset, boolean favorites, boolean selectable,
			boolean showLess) {
		super(pageSize);
		this.offset = offset;
		this.selectable = selectable;
		this.favorites = favorites;
		this.showLess = showLess;

		initialize();
	}

	@Override
	protected void init() {
		clear();

		List<String> toolColNames = new ArrayList<String>();
		List<Column<ToolDto, ?>> toolColumns = new ArrayList<Column<ToolDto, ?>>();

		toolColNames.add("Tool Name");
		toolColumns.add(toolName);

		if (!showLess) {
			toolColNames.add("Description");
			toolColumns.add(toolDescription);
		}

		if (!showLess) {
			toolColNames.add("Author");
			toolColumns.add(toolAuthor);
			toolColNames.add("External Link");
			toolColumns.add(toolSourceLink);
		}

		toolColNames.add("Download");
		toolColumns.add(toolDownloadLink);

		if (favorites) {
			table = new PagedTable<ToolDto>(pageSize,
					false, false, true, new ArrayList<ToolDto>(), KEY_PROVIDER,
					toolColNames, toolColumns, true,
					ResourceFactory.getStarSel(),
					ResourceFactory.getStarUnsel());

		} else if (selectable) {
			table = new PagedTable<ToolDto>(pageSize,
					true, true, true, new ArrayList<ToolDto>(), KEY_PROVIDER,
					toolColNames, toolColumns, false, null,
					null);
		} else {
			table = new PagedTable<ToolDto>(pageSize,
					false, false, true, new ArrayList<ToolDto>(), KEY_PROVIDER,
					toolColNames, toolColumns, false, null,
					null);
		}

		// Set column widths
		table.setColumnWidth(toolName, 100, Unit.PX);
		table.setColumnWidth(toolDescription, 200, Unit.PX);
		table.setColumnWidth(toolAuthor, 100, Unit.PX);
		table.setColumnWidth(toolDownloadLink, 60, Unit.PX);
		table.setColumnWidth(toolSourceLink, 80, Unit.PX);

		add(table);

		toolName.setSortable(true);
		table.addColumnSort(toolName, new Comparator<ToolDto>() {

			@Override
			public int compare(ToolDto o1, ToolDto o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}

		});
		table.getColumnSortList().push(toolName);

		if (!showLess) {
			toolAuthor.setSortable(true);
			table.addColumnSort(toolAuthor, new Comparator<ToolDto>() {

				@Override
				public int compare(ToolDto o1, ToolDto o2) {
					return o1.getAuthorName().compareTo(o2.getAuthorName());
				}

			});
		}
	}

	public static final TextColumn<ToolDto> toolName = new TextColumn<ToolDto>() {
		@Override
		public String getValue(ToolDto object) {
			return object.getLabel();
		}
	};

	public static final TextColumn<ToolDto> toolDescription = new TextColumn<ToolDto>() {
		@Override
		public String getValue(ToolDto object) {
			return object.getDescription().orNull();
		}
	};
	private static final TextColumn<ToolDto> toolAuthor = new TextColumn<ToolDto>() {
		@Override
		public String getValue(ToolDto object) {
			return object.getAuthorName();
		}
	};

	// @SuppressWarnings("unused")
	// private static final TextColumn<ToolDto> toolLocation = new
	// TextColumn<ToolDto>() {
	// @Override
	// public String getValue(ToolDto object) {
	// switch (object.getToolLoc()) {
	// case GITHUB:
	// return "Github";
	// default:
	// return "Other";
	// }
	// }
	// };

	private static final Column<ToolDto, SafeHtml> toolDownloadLink = new Column<ToolDto, SafeHtml>(
			new SafeHtmlCell()) {

		@Override
		public SafeHtml getValue(ToolDto object) {
			if (isNullOrEmpty(object.getBinUrl())) {
				return SafeHtmlUtils.fromSafeConstant("");
			}
			return SafeHtmlUtils
					.fromTrustedString("<a href='https://org-ieeg-appsupport-prod.s3.amazonaws.com/www/"
							+ object.getBinUrl()
							+ "' target='_blank'>download</a>");
		}
	};

	private static final Column<ToolDto, SafeHtml> toolSourceLink = new Column<ToolDto, SafeHtml>(
			new SafeHtmlCell()) {

		@Override
		public SafeHtml getValue(ToolDto object) {
			if (isNullOrEmpty(object.getSourceUrl())) {
				return SafeHtmlUtils.fromSafeConstant("");
			}
			return SafeHtmlUtils
					.fromTrustedString("<a href='http://"
							+ object.getSourceUrl()
							+ "' target='_blank'>Link</a>");

		}
	};

	public static final ProvidesKey<ToolDto> KEY_PROVIDER = new ProvidesKey<ToolDto>() {

		public Object getKey(ToolDto item) {
			return (item == null) ? null : item.getId().get();
		}

	};

}
