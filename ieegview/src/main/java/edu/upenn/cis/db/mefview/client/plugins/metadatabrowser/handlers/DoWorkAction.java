/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;

public class DoWorkAction<T> extends SecFailureAsyncCallback.SimpleSecFailureCallback<T> {
	ClientFactory clientFactory;
	final OnDoneHandler<T> doneHandler;
	String study;
	String name;

	public DoWorkAction(ClientFactory cf, final OnDoneHandler<T> done,
			String study, String name) {
		super(cf);
		this.study = study;
		this.name = name;
		clientFactory = cf;
		doneHandler = done;
	}

	@Override
	protected void onNonSecFailure(Throwable caught) {
		Dialogs.messageBox("Error", "Unable to retrieve snapshot experiment details");

	}

	@Override
	public void onSuccess(T result) {
		doneHandler.doWork(study, name, result);
	}

}
