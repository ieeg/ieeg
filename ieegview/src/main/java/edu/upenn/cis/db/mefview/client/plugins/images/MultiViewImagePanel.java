package edu.upenn.cis.db.mefview.client.plugins.images;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.JavascriptFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.IBlankToolbar;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;



/*
 * MultiView Image Panel uses Papaya viewer
 */
public class MultiViewImagePanel extends AstroTab implements 
MultiViewImagePresenter.Display {

	FlowPanel vPanel;
	FlowPanel toolbar;
//	ListBox imageList = null;
	List<FileInfo> images = null;

	String baseUrl = "";
	SessionToken token = null;
	String urlInFrame = "";
	boolean isLoaded = false;

	Widget prev;
	Widget next;
	DivElement image;
	
	IBlankToolbar aToolbar = GWT.create(IBlankToolbar.class);
	
	final static boolean isCornerstone = false;//true;

	URLPlace place = new URLPlace("", datasetId, NAME);

	public final static String NAME = "MultiViewImagePresenter";
	final static MultiViewImagePanel seed = new MultiViewImagePanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public MultiViewImagePanel() {
		super();
	}

	public MultiViewImagePanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {

		super(clientFactory, model,  
				title);

		

		vPanel = new FlowPanel();
		add(vPanel);
		vPanel.add(aToolbar);

		image = Document.get().createDivElement();
		image.setAttribute("class", "papaya");
		image.setAttribute("data-params", "params");

		vPanel.getElement().appendChild(image);
		aToolbar.init(clientFactory, this);
		
		initControls();

		minHeight = 100;
	}

	void initControls() {
		FlowPanel toolbarWrapper = new FlowPanel();
		toolbar = new FlowPanel();
		toolbarWrapper.add(toolbar);
		toolbarWrapper.setStyleName("IeegTabToolbarWrapper");

//		vPanel.add(toolbarWrapper);

		toolbar.setStyleName("IeegTabToolbar");    

		prev = WidgetFactory.get().createButton("Prev");
		toolbar.add(prev);

//		imageList = new ListBox();
//		toolbar.add(imageList);

		next = WidgetFactory.get().createButton("Next");
		toolbar.add(next);

//		imageList.setEnabled(false);
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new MultiViewImagePanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getImageTab();
	}

	@Override
	public WindowPlace getPlace() {
		return place;
	}

	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof URLPlace)
			this.place = (URLPlace)place;
	}

	
	@Override
	public void setBaseUrl(String baseUrl, SessionToken token) {
		this.baseUrl = baseUrl;
		this.token = token;
	}

	void setUrl(String url) {
		return;}

	public String getTitle() {
		return "Images";
	}

	public boolean isSearchable() {
		return false;
	}

	public int getMinHeight() {
		return minHeight;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}

	@Override
	public void setUrl(String url, SessionToken token) {
		urlInFrame = url;

	}

	@Override
	public void setImages(List<FileInfo> list) {
//		for (FileInfo fi: list)
//			imageList.addItem(fi.getFriendlyName());
		
		this.images = list;
		
			load(images);
	}
	
	void load(List<FileInfo> theList) {
		
		if (isLoaded)
			return;
		
		StringBuilder str = new 
				StringBuilder("[");
		boolean first = true;
		for (FileInfo fi: theList) {
			if (!fi.getTitle().endsWith(".dcm") && !fi.getTitle().endsWith(".dicom") &&
					!fi.getTitle().endsWith("nii.gz") 	)
				continue;
			
			
			
			if (first)
				first = false;
			else
				str.append(",");
			
			str.append('\"');
			str.append(GWT.getHostPageBaseURL() + 
					baseUrl + fi.getDetails() +
					"?sessionId=" + getSessionModel().getSessionID().getId());
			str.append('\"');
		}
		str.append("];");
//		JavascriptFactory.loadJavascriptString(str.toString());
		
//		try {
//			eval(str.toString());
//		} catch (Exception e) {
//			GWT.log(e.getMessage());
//		}
		
		
		
		final String params = str.toString();
		Timer t = new Timer() {

			@Override
			public void run() {
				startPapaya(params, images.get(0).getMediaType());
			}
			
		};

		t.schedule(500);
	}
	
	native void startPapaya(String params1, String mime) /*-{
		$wnd.params = [];
		$wnd.params["worldSpace"] = true;
		$wnd.params["images"] = eval(params1);
		$wnd.params["mimeType"] = mime;
		$wnd.papaya.Container.startPapaya();
	}-*/;
	
	public native void unbindJsHandlers() /*-{
		$wnd.papaya.viewer.Viewer.prototype.closeDownViewer();
	}-*/;
	
	void doit() {
		/*


		 */
		final Command loadDone = new Command() {
			@Override
			public void execute() {
			}
			
		};
		final Command loadPoint = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/core/point.js", 
						loadDone, 
						null);
			}
			
		};
		final Command loadCoordinate = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/core/coordinate.js", 
						loadPoint, 
						null);
			}
			
		};
		final Command loadUrl = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/url-utils.js", 
						loadCoordinate, 
						null);
			}
			
		};
		final Command loadString = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/string-utils.js", 
						loadUrl, 
						null);
			}
			
		};
		final Command loadPlatform = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/platform-utils.js", 
						loadString, 
						null);
			}
			
		};
		final Command loadObject = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/object-utils.js", 
						loadPlatform, 
						null);
			}
			
		};
		final Command loadMath = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/math-utils.js", 
						loadObject, 
						null);
			}
			
		};
		final Command loadArray = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/utilities/array-utils.js", 
						loadMath, 
						null);
			}
			
		};
		
		final Command loadConstants = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/constants.js", 
						loadArray, 
						null);
			}
			
		};
		
		final Command loadPako = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/pako-inflate.js", 
						loadConstants, 
						null);
			}
			
		};
		final Command loadNumerics = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/numerics.js", 
						loadPako, 
						null);
			}
			
		};
		final Command loadJQuery = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/jquery.js", 
						loadNumerics, 
						null);
			}
			
		};
		final Command loadDaikon = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/daikon.js", 
						loadJQuery, 
						null);
			}
			
		};
		final Command loadBowser = new Command() {
			@Override
			public void execute() {
				JavascriptFactory.loadJavascript("js/bowser.js", 
						loadDaikon, 
						null);
			}
			
		};
		JavascriptFactory.loadJavascript("js/base64-binary.js", 
				loadBowser, 
				null);
	}
	
	
	native void eval(String str) /*-{
	    eval(str);
	}-*/;

//	@Override
//	public void selectImage(int inx) {
//		imageList.setSelectedIndex(inx);
//	}

	@Override
	public String getUrl() {
		return urlInFrame;
	}

	@Override
	public void addPrevHandler(ClickHandler handler) {
		((HasClickHandlers)prev).addClickHandler(handler);
	}

	@Override
	public void addNextHandler(ClickHandler handler) {
		((HasClickHandlers)next).addClickHandler(handler);
	}
}
