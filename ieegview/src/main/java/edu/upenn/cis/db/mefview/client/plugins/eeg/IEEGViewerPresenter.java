package edu.upenn.cis.db.mefview.client.plugins.eeg;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;

import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage;
import edu.upenn.cis.db.mefview.client.dialogs.IShowTraces;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.IEEGToolbar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public interface IEEGViewerPresenter {
	  public interface Display extends BasicPresenter.Display {
		    public void setClickHandlers(ClickHandler layer, ClickHandler trace, 
		        ClickHandler filter, ClickHandler montage, NumericEnterHandler vsize, ChangeHandler montageChange);

//		    public void showTraceDialog();

		    public IEEGToolbar getToolbar();

//		    public void setTraces(List<TraceInfo> tr);

		    public void setScale(double scale);
		    public void setFocusOnGraph();
		    public void registerPrefetchListener(Prefetcher p);

		    public String getWindowTitle();

		    public void setDataSnapshotModel(DataSnapshotModel model);

		    public void refreshAnnotations(List<AnnBlock> annotations);
		    
		    public void updateMontage(EEGMontage montage);

		    public void saveAndDeleteAnnotations();

		    public void close();
		    
			public IEEGPane getEEGPane();

		    public void showLayerDialog();
		    public void showFilterDialog();

		    public void setSelectMontageDialog(ISelectMontage dialog);
		    public void setShowTracesDialog(IShowTraces dialog);
		    
		    public ISelectMontage getSelectMontageDialog();
		    public IShowTraces getShowTracesDialog();
		  }

	  public DataSnapshotModel getModel();

	public IEEGViewerPresenter.Display getDisplay();
}
