/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;

public abstract class TableViewer<T> extends ResizingLayout {
//	protected ClientFactory clientFactory;
	protected List<T> values = Lists.newArrayList();
	protected PagedTable<T> table;
	protected int pageSize;
	protected boolean inited = false;
	
	public TableViewer(//ClientFactory clientFactory, 
			Collection<T> values, int pageSize) {
		this(pageSize);//clientFactory, pageSize);
		this.values.addAll(values);
	}
	
	public TableViewer(//ClientFactory clientFactory,
			int pageSize) {
//		this.clientFactory = clientFactory;
		this.pageSize = pageSize;
	}
	
	public void initialize() {
		if (!inited) {
			init();
//			init2();
		}
	}
	
	protected abstract void init();
	
//	public void init2() {
//	  setHandler(
//	      new RequiresResize() {
//
//	        @Override
//	        public void onResize() {
//	          if (table != null && 
//	              getOffsetHeight() >= 36) {
//	            table.setHeight((getOffsetHeight() - 36) + "px");
//	          }
//	        }
//
//	      });
//	}

	public SelectionModel<? super T> getSelectionModel() {
		return table.getSelectionModel();
	}
	
	public void setSelectionModel(SelectionModel<? super T> selectionModel) {
		table.setSelectionModel(selectionModel);
	}
	
	public Set<T> getSelectedItems() {
		return table.getSelectedItems();
	}
	
	public void selectItem(T info) {
		table.getSelectionModel().setSelected(info, true);
	}
	
	public void deselectItem(T info) {
		table.getSelectionModel().setSelected(info, false);
	}

	public PagedTable<T> getTable() {
		return table;
	}
}
