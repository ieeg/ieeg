/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.panels;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.places.DownloadPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class AttachmentDownloader {
	ClientFactory clientFactory;

	public static final String NAME = "attachment-download";

	public AttachmentDownloader() {

	}

	public AttachmentDownloader(ClientFactory clientFactory,
			DataSnapshotModel model,
			WindowPlace place) {
		checkNotNull(clientFactory);
		checkNotNull(model);
		checkNotNull(place);
		checkArgument(place instanceof DownloadPlace);
		final DownloadPlace downloadPlace = (DownloadPlace) place;
		this.clientFactory = clientFactory;

		openURL(downloadPlace.getDownloadName() ,
				downloadPlace.getDownloadUrl(),
				place.getSnapshotID());

	}

	void openURL(String title, String url, String snapshot) {
		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, "&disposition=attachment", null);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}

}