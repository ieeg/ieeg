/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;

public class CreateIndex extends DialogBox {
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	DoubleBox frequency = new DoubleBox();
	DoubleBox width = new DoubleBox();
	DataSnapshotViews snapshot;
	
	public CreateIndex( 
			final DataSnapshotViews snapshot) {//, final EEGPane display) {
		setTitle("Create Index for Snapshot");
		setText("Create Index for Snapshot");
		
		VerticalPanel layout = new VerticalPanel();
		
		this.snapshot = snapshot;
		
		layout.add(new HTML("<b>Please select parameters for the indx:</b><br/><br/>"));
		
		layout.add(new Label("Sample frequency:"));
		layout.add(frequency);
		frequency.setValue(new Double(100));
		
		layout.add(new Label("Page width (usec)"));
		layout.add(width);
		width.setValue(new Double(60.0));
		
		setWidget(layout);

		HorizontalPanel hp = new HorizontalPanel();
		layout.add(hp);
		layout.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		hp.add(ok);
		hp.add(cancel);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				CreateIndex.this.hide();
				
				DialogBox d = PanelFactory.getPleaseWaitDialog("Creating index");
				d.center();
				d.show();
				snapshot.createIndexFor(frequency.getValue(), width.getValue());
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				CreateIndex.this.hide();
			}
			
		});
	}
}
