/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

public class UserCredentials {
	private String userId;
	private String password;
	private String name;
	
	public UserCredentials(final String userid, final String password) {
		this.userId = userid;
		this.password = password;
		this.name = userid;
	}
	
	public UserCredentials(final String userid, final String fullname, final String password) {
		this.userId = userid;
		this.password = password;
		this.name = fullname;
	}
	
	String getUserId() {
		return userId;
	}
	void setUserId(String userId) {
		this.userId = userId;
	}
	
	String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name;
	}

	String getPassword() {
		return password;
	}
	void setPassword(String password) {
		this.password = password;
	}
	
}
