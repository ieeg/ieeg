package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.DataLoadHandler;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanJs;
import edu.upenn.cis.db.mefview.client.util.DataConversion;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.TimeSeries;

public class AsyncDataHandler implements DataLoadHandler {
    /**
	 * 
	 */
	private final IEEGPresenter eegPresenter;

	/**
	 * @param eegPresenter
	 */
	public AsyncDataHandler(IEEGPresenter eegPresenter) {
		this.eegPresenter = eegPresenter;
	}

	@Override
    public void process(String user, String dataSnapshotRevId,
        List<String> traceIds, double start, double width,
        double samplingPeriod, List<DisplayConfiguration> filter,
        TimeSeries[] results, boolean isMinMax) {
      JsArray<JsArrayInteger> ar = this.eegPresenter.refreshBufferFromSeries(results);
      handleData(user, dataSnapshotRevId, traceIds, start, width, samplingPeriod, filter, ar, isMinMax);
    }

    @Override
    public void process(String user, String dataSnapshotRevId,
        List<String> traceIds, double start, double width,
        double samplingPeriod, List<DisplayConfiguration> filter,
        String results, boolean isMinMax) {
      JsArray<JsArrayInteger> ar = this.eegPresenter.refreshBufferFromJson(results);
      handleData(user, dataSnapshotRevId, traceIds, start, width, samplingPeriod, filter, ar, isMinMax);
    }

    @Override
    public void process(String user, String dataSnapshotRevId,
        List<String> traceIds, double start, double width,
        double samplingPeriod, List<DisplayConfiguration> filter,
        JsArray<JsArrayInteger> ar, boolean isMinMax) {
      handleData(user, dataSnapshotRevId, traceIds, start, width, samplingPeriod, filter, ar, isMinMax);
    }

    public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, final List<TimeSeriesSpan> results, boolean isMinMax) {

      if (results.isEmpty())
        return;

      JsArray<JsArrayInteger> arr = ((TimeSeriesSpanJs)results.get(0)).getMaster();

      this.eegPresenter.getDisplay().debug("New data " + DataConversion.getUUValue(EEGModel.getStartDate(0/*controller.getSnapshotStart()*/, arr)) + " through " + 
      DataConversion.getUUValue(EEGModel.getEndDate(0/*controller.getSnapshotStart()*/, arr)) + " @" + samplingPeriod);
      //			render(start, width, samplingPeriod, filter, arr);
      if (arr != null) {
        this.eegPresenter.getDisplay().addNewData(arr, true, isMinMax);
        //				refreshAnnotationsOnGraph(true);
        this.eegPresenter.getDisplay().setRefreshGraph();
        this.eegPresenter.getDisplay().setRefreshAnnotations();
      }
    }

    private void handleData(final String user, final String dataSnapshotRevId, final List<String> traceIds,
        final double start, final double width, final double samplingPeriod, 
        final List<DisplayConfiguration> filter, JsArray<JsArrayInteger> ar, boolean isMinMax) {
      if (ar == null)
        return;
      
      this.eegPresenter.getDisplay().addNewData(ar, true, isMinMax);

      if (this.eegPresenter.getDisplay().getPosition() != start) {
        this.eegPresenter.getDisplay().setPositionDisplay(this.eegPresenter.getDisplay().getPosition());
      }
      this.eegPresenter.getDisplay().updateHeight(false);
      this.eegPresenter.getDisplay().setRefreshGraph();
      this.eegPresenter.getDisplay().setRefreshAnnotations();

      // Is this the optimal resolution?  If not, do a subsequent request.
      if (samplingPeriod < this.eegPresenter.getDisplay().getRequestPeriod() && 
          samplingPeriod > 1.E6 / this.eegPresenter.getModel().getSampleRateHz()) {
        this.eegPresenter.setRequestHighres(new Timer() {

          @Override
          public void run() {
            AsyncDataHandler.this.eegPresenter.getDisplay().setRequestPeriod(samplingPeriod * EEGPane.RATIO2);

            if (AsyncDataHandler.this.eegPresenter.getDisplay().getRequestPeriod() < 1.E6/AsyncDataHandler.this.eegPresenter.getModel().getSampleRateHz())
              AsyncDataHandler.this.eegPresenter.getDisplay().setRequestPeriod(1.E6/AsyncDataHandler.this.eegPresenter.getModel().getSampleRateHz());

            double amt = width;
            if (amt < AsyncDataHandler.this.eegPresenter.getDisplay().getPageWidth())
              amt = AsyncDataHandler.this.eegPresenter.getDisplay().getPageWidth();
            double min = start;
            if (min > AsyncDataHandler.this.eegPresenter.getDisplay().getPosition())
              min = AsyncDataHandler.this.eegPresenter.getDisplay().getPosition();

            
            GWT.log("EEGPRESENTER: in timer");
            AsyncDataHandler.this.eegPresenter.getData(user, 
            		AsyncDataHandler.this.eegPresenter.getSnapshotId(), AsyncDataHandler.this.eegPresenter.getModel().getChannelPaths(), 
                (long)min, amt, 
                AsyncDataHandler.this.eegPresenter.getDisplay().getRequestPeriod() , 
                AsyncDataHandler.this.eegPresenter.getDisplay().getFilters(), 
//                AsyncDataHandler.this.eegPresenter.getFullDataHandler(), 
                false);
          }
        });
        this.eegPresenter.getRequestHighres().schedule(EEGPane.LAG);
      }							
    }

  }
