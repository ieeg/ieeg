/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.workspacesettings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.WorkspaceModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.ProjectViewer;
import edu.upenn.cis.db.mefview.client.viewers.ToolViewer;
import edu.upenn.cis.db.mefview.client.viewers.WorkspaceToolbar;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.WebLink;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class WorkspacePanel extends AstroTab implements RequiresResize,
//ProjectCreatedEvent.Handler, ProjectUpdatedEvent.Handler, 
WorkspacePresenter.Display {
  LayoutPanel resourcesPanel = new ResizingLayout();
  LayoutPanel favoriteStudiesPanel = new ResizingLayout();
  LayoutPanel favoriteToolsPanel = new ResizingLayout();
//  LayoutPanel recentStudiesPanel = new ResizingLayout();
//  LayoutPanel recentToolsPanel = new ResizingLayout();
  StackLayoutPanel recentsPanel = new StackLayoutPanel(Unit.EM);

  LayoutPanel projectVWrapper = new ResizingLayout();
  ProjectViewer projectViewer;

  PagedTable<SearchResult> favoriteStudies;
  PagedTable<ToolDto> favoriteTools;
//  PagedTable<SearchResult> recentStudies;
//  PagedTable<ToolDto> recentTools;
  PagedTable<WebLink> resources;

  WorkspaceToolbar toolbar;

  WorkspaceModel model;
//  WorkspacePresenter presenter;
  AppModel control;

  final SingleSelectionModel<SearchResult> snapshotSelector = new 
		  SingleSelectionModel<SearchResult>(SearchResult.KEY_PROVIDER);

  final SingleSelectionModel<ToolDto> toolSelector = 
		  new SingleSelectionModel<ToolDto>(ToolViewer.KEY_PROVIDER);
  
  public static final ProvidesKey<WebLink> WEBLINK_KEY = new ProvidesKey<WebLink>() {

	  public Object getKey(WebLink item) {
		  return (item == null) ? null : item.getDetails();
	  }

  };
  final SingleSelectionModel<WebLink> linkSelector = 
		  new SingleSelectionModel<WebLink>(WEBLINK_KEY);
	public static final TextColumn<WebLink> WebLinkResource = new TextColumn<WebLink>() {
		@Override
		public String getValue(WebLink object) {
			return object.getFriendlyName();
		}
	};

	public static final TextColumn<WebLink> WebLinkUrl = new TextColumn<WebLink>() {
		@Override
		public String getValue(WebLink object) {
			return object.getDetails();
		}
	};

  String user;

  public final static String NAME = "workspace";
  final static WorkspacePanel seed = new WorkspacePanel();

  static {
    try {
      PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, false));
    } catch (PanelExistsException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public WorkspacePanel() {
    super();
//    closingHandlerReg = null;
//    closeHandlerReg = null;
  }
  
  private void setDefaults(PagedTable<?> p) {
	    p.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);//BOUND_TO_SELECTION);
	    p.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);    
  }

  public WorkspacePanel( 
//      final String user, 
      final AppModel parent,
      final ClientFactory clientFactory,
      final String title) {

    super (clientFactory,  
        title);


    /*
     *  General WorkSpace elements
     */
//    this.user = user;
    control = parent;

    // Favorite Studies 
    List<String> studyColNames = new ArrayList<String>();
    List<TextColumn<SearchResult>> studyColumns = new ArrayList<TextColumn<SearchResult>>();
    studyColNames.add("Name");
    studyColumns.add(SearchResult.studyColumn);
    favoriteStudies = new PagedTable<SearchResult>(10, false, false, new ArrayList<SearchResult>(), 
        SearchResult.KEY_PROVIDER, studyColNames, studyColumns);
    setDefaults(favoriteStudies);
    favoriteStudies.setSelectionModel(snapshotSelector);

//    // Recent Studies 
//    recentStudies = new PagedTable<SearchResult>(10, false, false, new ArrayList<SearchResult>(), 
//        SearchResult.KEY_PROVIDER, studyColNames, studyColumns);
//    setDefaults(recentStudies);
//    recentStudies.setSelectionModel(snapshotSelector);

    // Favorite Tools
    List<String> toolColNames = new ArrayList<String>();
    List<TextColumn<ToolDto>> toolColumns = new ArrayList<TextColumn<ToolDto>>();
    favoriteTools = new PagedTable<ToolDto>(10, false, false, new ArrayList<ToolDto>(), ToolViewer.KEY_PROVIDER, 
        toolColNames, toolColumns);
    setDefaults(favoriteTools);
    favoriteTools.setSelectionModel(toolSelector);

//    // Recent Tools
//    recentTools = new PagedTable<ToolDto>(10, false, false, new ArrayList<ToolDto>(), ToolViewer.KEY_PROVIDER, 
//        toolColNames, toolColumns);
//    setDefaults(recentTools);
//    recentTools.setSelectionModel(toolSelector);

    // Web resources 
    List<String> webColNames = new ArrayList<String>();
    List<TextColumn<WebLink>> webColumns = new ArrayList<TextColumn<WebLink>>();
    webColNames.add("Resource");
    webColumns.add(WebLinkResource);
    webColNames.add("URL");
    webColumns.add(WebLinkUrl);
    
    resources = new PagedTable<WebLink>(10, false, false, new ArrayList<WebLink>(), 
        WEBLINK_KEY, webColNames, webColumns);
    setDefaults(resources);
    resources.setSelectionModel(linkSelector);
    
    populateLinks(resources);

    /* 
     * Panel Layout: Three panels: 1) Toolbar 2) fixed left 3) resizing right
     */

    DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
    add(mainPane);

    DockLayoutPanel contentPane = new DockLayoutPanel(Unit.PX);
    DockLayoutPanel rightPane = new DockLayoutPanel(Unit.PX);
    contentPane.addEast(rightPane, 400);

    // Left Panel
    DockLayoutPanel leftPane = new DockLayoutPanel(Unit.PX);
    contentPane.add(leftPane);

    // Add Toolbar
    FlowPanel toolbarWrapper = new FlowPanel();
    toolbarWrapper.setStyleName("IeegTabToolbarWrapper");
    toolbar = new WorkspaceToolbar();
    toolbarWrapper.add(toolbar);
    mainPane.addNorth(toolbarWrapper, 35);
    mainPane.add(contentPane);

    // Add Project Table
    projectViewer = new ProjectViewer(10, false, false);

//    DockLayoutPanel projects = new DockLayoutPanel(Unit.PX);
    leftPane.setStyleName("ieeg-project-table");

    FlowPanel labelWrapper = new FlowPanel(); 
    Label projLabel = new Label("Available projects");
    projLabel.setStylePrimaryName("ieeg-project-label");
    labelWrapper.add(projLabel);
    
    leftPane.addNorth(labelWrapper,30);
    leftPane.add(projectVWrapper);


    projectVWrapper.add(projectViewer);

    // Add Recent/Favorite panels if registered user.
    if (getSessionModel().isRegisteredUser()) {


      DockLayoutPanel stackWrapper = new DockLayoutPanel(Unit.PX);

      FlowPanel infoPanel = new FlowPanel();
      infoPanel.setStyleName("ieeg-user-sidePane");
      infoPanel.addStyleName("ieeg-cornered-pane");
      Label portLab = new Label("Information");
      portLab.setStyleName("ieeg-user-sidebar-heading");
      portLab.addStyleName("ieeg-cornered-pane-header");
      infoPanel.add(portLab);
      HTML infoStr = new HTML("Datasets can be grouped in projects and you can invite other people to join projects groups." + 
          " The list of available projects include all projects that you created and those that include you as a member. ");
      infoStr.setStylePrimaryName("ieeg-info-text");
      infoPanel.add(infoStr);

      stackWrapper.addNorth(infoPanel, 120);

      DockLayoutPanel favPanel = new DockLayoutPanel(Unit.EM);
      favPanel.setStyleName("ieeg-user-sidePane");
      Label favLab = new Label("Favorites");
      favLab.setStyleName("ieeg-user-sidebar-heading");
      favPanel.addNorth(favLab,2.5);
      favPanel.add(recentsPanel);
      recentsPanel.setStylePrimaryName("ieeg-rec-fav-stack");
      stackWrapper.add(favPanel);

      // stackWrapper: all right panel panels
      rightPane.add(stackWrapper);

//      HTML panelheader3 = new HTML("Recent Datasets");
//      panelheader3.setStylePrimaryName("ieeg-stack-header");
//      recentsPanel.add(recentStudiesPanel,panelheader3, 1.5);

//      HTML panelheader4 = new HTML("Recent Tools");
//      panelheader4.setStylePrimaryName("ieeg-stack-header");
//      recentsPanel.add(recentToolsPanel,panelheader4, 1.5);

      HTML panelheader1 = new HTML("Favorite Datasets");
      panelheader1.setStylePrimaryName("ieeg-stack-header");
      recentsPanel.add(favoriteStudiesPanel,panelheader1, 1.5);

      HTML panelheader2 = new HTML("Favorite Tools");
      panelheader2.setStylePrimaryName("ieeg-stack-header");
      recentsPanel.add(favoriteToolsPanel,panelheader2, 1.5);

      HTML panelheader5 = new HTML("Web Resources");
      panelheader5.setStylePrimaryName("ieeg-stack-header");
      recentsPanel.add(resourcesPanel,panelheader5, 1.5);

      favoriteStudiesPanel.add(favoriteStudies);
//      recentStudiesPanel.add(recentStudies);

      toolColNames.add("Tool Name");
      toolColumns.add(ToolViewer.toolName);
      toolColNames.add("Description");
      toolColumns.add(ToolViewer.toolDescription);

      favoriteToolsPanel.add(favoriteTools);
//      recentToolsPanel.add(recentTools);
      resourcesPanel.add(resources);


    } else {
      /*
       * Guest Panel
       */

      FlowPanel infoPane = new FlowPanel();
      infoPane.setStyleName("ieeg-user-sidePane");
      infoPane.addStyleName("ieeg-cornered-pane");
      Label infoLab = new Label("Information");
      infoLab.setStyleName("ieeg-user-sidebar-heading");
      infoLab.addStyleName("ieeg-cornered-pane-header");
      infoPane.add(infoLab);
      HTML infoStr = new HTML("As a guest user, you can explore the data in the sample projects listed on this page. "+
          "Click on 'Open Project' to access the data.  As a portal user, you can add users, tools, and dataset to projects-groups to facilitate collaborative research. " +
          "<br><br>Select one of the projects on the left and click on 'Open Project' to see the contents of the project.");
      infoStr.setStylePrimaryName("ieeg-info-text");
      infoPane.add(infoStr);
      rightPane.addNorth(infoPane,180);;
    };

    /*
     * Handlers 
     */
    projectViewer.getSelectionModel().addSelectionChangeHandler(new Handler() {

      @Override
      public void onSelectionChange(SelectionChangeEvent event) {
        toolbar.setOpenProjectEnable(true);
      }

    }
    );
  }


  public void resizeAll(int width, int height) {
  }

  public PagedTable<SearchResult> getStudyList() {
    return favoriteStudies;
  }

  @Override
  public void updateRevId(String oldRevId, String newRevId) {
    for (SearchResult sr: favoriteStudies.getList()) {
      if (sr.getDatasetRevId().equals(oldRevId))
        sr.setDatasetRevId(newRevId);
    }

  }

  public void bookmarkSnapshot(SearchResult s) {
    if (s != null && !favoriteStudies.getList().contains(s))
      favoriteStudies.add(s);
  }

  public void unbookmarkSnapshot(SearchResult s) {
    if (s != null && favoriteStudies.getList().contains(s))
      favoriteStudies.remove(s);
  }

  public void bookmarkTool(ToolDto t) {
    if (getSessionModel().isRegisteredUser() && t != null && !favoriteTools.getList().contains(t))
      favoriteTools.add(t);
  }

  public void unbookmarkTool(ToolDto t) {
    if (getSessionModel().isRegisteredUser() && t != null && favoriteTools.getList().contains(t))
      favoriteTools.remove(t);
  }

  public List<ToolDto> getFavoriteTools() {
    if (getSessionModel().isRegisteredUser())
      return favoriteTools.getList();
    else
      return null;
  }

  public List<SearchResult> getFavoriteSnapshots() {
    return favoriteStudies.getList();
  }

  @Override
  public String getPanelType() {
    return NAME;
  }

  public WorkspaceModel getWorkspaceModel() {
    return model;
  }

  @Override
  public AstroPanel create(ClientFactory clientFactory,
      AppModel controller, DataSnapshotViews model,
//      Project project, 
      boolean writePermissions,
      final String title) {
    return new WorkspacePanel(//userID, 
    		controller, //null, 
        clientFactory, title);
  }

  @Override
  public ImageResource getIcon(ClientFactory clientFactory) {
    return ResourceFactory.getWork();
  }

  @Override
  public WindowPlace getPlace() {
    // TODO Auto-generated method stub
    return null;
  }

//  @Override
//  public WindowPlace getPlaceFor(String params) {
//    // TODO Auto-generated method stub
//    return null;
//  }

  @Override
  public void setEegHandler(ClickHandler handler) {
    toolbar.setEegHandler(handler);
  }

  @Override
  public void setDicomHandler(ClickHandler handler) {
    toolbar.setDicomHandler(handler);
  }

  @Override
  public void setDocumentHandler(ClickHandler handler) {
    toolbar.setDocumentHandler(handler);
  }

  @Override
  public void setImageHandler(ClickHandler handler) {
    toolbar.setImageHandler(handler);
  }

  @Override
  public void setLinkHandler(ClickHandler handler) {
    toolbar.setLinkHandler(handler);
  }

  @Override
  public void enableEegHandler(boolean sel) {
    toolbar.enableEegHandler(sel);
  }

  @Override
  public void enableLinkHandler(boolean sel) {
    toolbar.enableLinkHandler(sel);
  }

  @Override
  public void enableDicomHandler(boolean sel) {
    toolbar.enableDicomHandler(sel);
  }

  @Override
  public void enableDocumentHandler(boolean sel) {
    toolbar.enableDocumentHandler(sel);
  }

  @Override
  public void enableImageHandler(boolean sel) {
    toolbar.enableImageHandler(sel);
  }

  @Override
  public SearchResult getSelectedSnapshot() {
    return snapshotSelector.getSelectedObject();
  }

  @Override
  public void addFavoriteSnapshot(SearchResult sr) {
    favoriteStudies.add(sr);
  }

  @Override
  public void removeFavoriteSnapshot(SearchResult sr) {
    favoriteStudies.remove(sr);
  }

  @Override
  public void setFavoriteSnapshotList(List<SearchResult> sr) {
    favoriteStudies.clear();
    favoriteStudies.addAll(sr);
  }

  @Override
  public boolean isSelectedFavoriteSnapshot(SearchResult sr) {
    return favoriteStudies.getSelectedItems().contains(sr);
  }

  @Override
  public void refreshFavoriteSnapshots() {
    favoriteStudies.redraw();
  }

  @Override
  public ToolDto getSelectedTool() {
    if (getSessionModel().isRegisteredUser())
      return toolSelector.getSelectedObject();
    else
      return null;
  }

  @Override
  public void addFavoriteTool(ToolDto ti) {
    if (getSessionModel().isRegisteredUser())
      favoriteTools.add(ti);
  }

  @Override
  public void removeFavoriteTool(ToolDto ti) {
    if (getSessionModel().isRegisteredUser())
      favoriteTools.remove(ti);
  }

  @Override
  public void setFavoriteToolList(List<ToolDto> ti) {
    if (getSessionModel().isRegisteredUser()) {
      favoriteTools.clear();
      favoriteTools.addAll(ti);
    }
  }

  @Override
  public void refreshFavoriteTools() {
    if (getSessionModel().isRegisteredUser())
      favoriteTools.redraw();
  }

  @Override
  public void addProject(int inx, Project p) {
    projectViewer.getTable().add(inx, p);
  }

  @Override
  public void addProject(Project p) {
    projectViewer.getTable().add(p);
  }

  @Override
  public void setProjects(Collection<Project> pl) {
    projectViewer.getTable().clear();
    projectViewer.getTable().addAll(pl);
  }

  @Override
  public List<Project> getProjects() {
    return projectViewer.getTable().getList();
  }

  @Override
  public void removeProject(Project p) {
    projectViewer.getTable().remove(p);
  }

  @Override
  public void removeProject(int pos) {
    projectViewer.getTable().remove(pos);
  }

  @Override
  public Set<Project> getSelectedProjects() {
    return projectViewer.getSelectedItems();
  }

  @Override
  public void addFavoriteSnapshotSelectionChangedHandler(Handler handler) {
    favoriteStudies.getSelectionModel().addSelectionChangeHandler(handler);
  }

  @Override
  public void addFavoriteToolSelectionChangedHandler(Handler handler) {
    if (getSessionModel().isRegisteredUser())
      favoriteTools.getSelectionModel().addSelectionChangeHandler(handler);
  }


  @Override
  public void addProjectOpenedHandler(ClickHandler handler) {
    toolbar.setOpenProjectHandler(handler);
  }

  @Override
  public void addProjectCreatedHandler(ClickHandler handler) {
    toolbar.setCreateProjectHandler(handler);

  }

  @Override
  public void log(String msg) {
    GWT.log(msg);
  }

  public String getTitle() {
    return "Work";
  }

  public static String getDefaultTitle() {
    return "Work";
  }

	@Override
	public void setWorkspaceModel(WorkspaceModel model) {
		this.model = model;
	}

	public boolean isSearchable() {
		return true;
	}
	
	void populateLinks(PagedTable<WebLink> table) {
		table.add(new WebLink("", "LONI-ADNI", "https://adni.loni.usc.edu"));
		table.add(new WebLink("", "LONI-IDA", "https://ida.loni.usc.edu"));
		table.add(new WebLink("", "GenBank", "https://www.ncbi.nlm.nih.gov/genbank"));
	}

	@Override
	public List<WebLink> getResources() {
		return resources.getList();
	}

	@Override
	public WebLink getSelectedLink() {
		return ((SingleSelectionModel<WebLink>)resources.getSelectionModel()).getSelectedObject();
	}

	@Override
	public void addLinkSelectionChangedHandler(Handler handler) {
		resources.getSelectionModel().addSelectionChangeHandler(handler);
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getPanelCategory() {
		return PanelTypes.COLLABORATIONS;
	}

	@Override
	public void bindDomEvents() {
	}
}
