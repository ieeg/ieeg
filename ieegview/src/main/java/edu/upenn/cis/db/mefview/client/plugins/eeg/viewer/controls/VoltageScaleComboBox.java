package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.controls;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;

import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;

public class VoltageScaleComboBox extends ComboBoxWithEnter<ComboBoxDataModel> {
	public VoltageScaleComboBox() {
		ComboBoxDataModel voltModel = new ComboBoxDataModel();
		voltModel.add("1.0 uV/mm", new Double(1.0));
		voltModel.add("2.0 uV/mm", new Double(2.0));
		voltModel.add("3.0 uV/mm", new Double(3.0));
		voltModel.add("5.0 uV/mm", new Double(5.0));
		voltModel.add("7.0 uV/mm", new Double(7.0));
		voltModel.add("10 uV/mm", new Double(10));
		voltModel.add("15 uV/mm", new Double(15));
		voltModel.add("20 uV/mm", new Double(20));
		voltModel.add("30 uV/mm", new Double(30));
		voltModel.add("50 uV/mm", new Double(50));
		voltModel.add("70 uV/mm", new Double(70));
		voltModel.add("100 uV/mm", new Double(100));
		voltModel.add("200 uV/mm", new Double(200));
		voltModel.add("500 uV/mm", new Double(500));
		voltModel.add("1000 uV/mm", new Double(1000));
		voltModel.add("1500 uV/mm", new Double(1500));
		voltModel.add("2000 uV/mm", new Double(2000));
		
		
		setModel(voltModel);
		setLazyRenderingEnabled(false);
		setCustomTextAllowed(true);
	}
}
