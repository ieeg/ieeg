/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;

public interface MefResources extends ClientBundle {
//	public static final MefResources INSTANCE = GWT.create(MefResources.class);
	
	public static final String PATH = "images/";
	
	@Source(PATH + "left-arrow.png")
	public ImageResource getLeftImage();
	
	@Source(PATH + "right-arrow.png")
	public ImageResource getRightImage();

	@Source(PATH + "data.png")
	public ImageResource getData();

	@Source(PATH + "brain.png")
	public ImageResource getBrain();

	@Source(PATH + "images.png")
	public ImageResource getImageTab();
	
	@Source(PATH + "chat.png")
	public ImageResource getChat();

	@Source(PATH + "page.png")
	public ImageResource getDocument();

	@Source(PATH + "filter.png")
	public ImageResource getFilter();

	@Source(PATH + "marker.png")
	public ImageResource getMarker();

	@Source(PATH + "magnifying_glass.png")
	public ImageResource getMagnifier();

	@Source(PATH + "magnifier_tiny.png")
	public ImageResource getTinyMagnifier();

	@Source(PATH + "folder_magnify.png")
	public ImageResource getStudy();

	@Source(PATH + "folder_wrench.png")
	public ImageResource getTools();

	@Source(PATH + "IEEGlogo_small.png")
	public ImageResource getSmallLogo();
	
	@Source(PATH + "chart_line.png")
	public ImageResource getSignal();

	@Source(PATH + "penn.png")
	public ImageResource getPenn();
	
	@Source(PATH + "mayo.png")
	public ImageResource getMayo();
	
	@Source(PATH + "ninds.png")
	public ImageResource getNinds();
	
//	@Source(PATH + "europeanEegDatabase.png")
//	public ImageResource getFreiburg();

	@Source(PATH + "star.png")
	public ImageResource getStarSel();
	
	@Source(PATH + "starUnsel.png")
	public ImageResource getStarUnsel();
	
	@Source(PATH + "legend.png")
	public ImageResource getDcnCaption();
	
//	@Source(PATH + "disk.png")
	@Source(PATH + "ic_backup_black_18dp.png")
	public ImageResource getSave();

	@Source(PATH + "tools_tiny.png")
	public ImageResource getTinyTools();
	
	@Source(PATH + "book.png")
	public ImageResource getBook();
	   
	@Source(PATH + "book_open.png")
	public ImageResource getOpenBook();
	
	@Source(PATH + "book_addresses.png")
	public ImageResource getAddrBook();

//	@Source(PATH + "IEEG_Logo_login.png")
	@Source(PATH + "Tree_icon.jpg")
	public ImageResource getLoginLogo();
	
//	@Source(PATH + "disk.png")
	@Source(PATH + "ic_backup_black_18dp.png")
	public ImageResource getTinySave();

	//@Source("images/question-128x128.png")
	@Source(PATH + "silhouette.png")
	public ImageResource getUnknownUser();
	
	@Source(PATH + "house.png")
	public ImageResource getHouse();
	
//	@Source(PATH + "world_link.png")
	@Source(PATH + "ic_person_add_black_18dp.png")
	public ImageResource getShare();
	
	@Source(PATH + "cross.png")
	public ImageResource getLogout();

    @Source(PATH + "discuss.png")
    public ImageResource getDiscussion();
	
	@Source(PATH + "bold.gif")
    public ImageResource bold();

	@Source(PATH + "createLink.gif")
    public ImageResource createLink();

	@Source(PATH + "hr.gif")
    public ImageResource hr();

	@Source(PATH + "indent.gif")
    public ImageResource indent();

    @Source(PATH + "insertImage.gif")
    public ImageResource insertImage();

    @Source(PATH + "italic.gif")
    public ImageResource italic();

    @Source(PATH + "justifyCenter.gif")
    public ImageResource justifyCenter();

    @Source(PATH + "justifyCenter.gif")
    public ImageResource justifyLeft();

    @Source(PATH + "justifyRight.gif")
    public ImageResource justifyRight();

    @Source(PATH + "ol.gif")
    public ImageResource ol();

    @Source(PATH + "outdent.gif")
    public ImageResource outdent();

    @Source(PATH + "removeFormat.gif")
    public ImageResource removeFormat();

    @Source(PATH + "removeLink.gif")
    public ImageResource removeLink();

    @Source(PATH + "strikethrough.gif")
    public ImageResource strikeThrough();

    @Source(PATH + "subscript.gif")
    public ImageResource subscript();

    @Source(PATH + "superscript.gif")
    public ImageResource superscript();

    @Source(PATH + "ul.gif")
    public ImageResource ul();

    @Source(PATH + "underline.gif")
    public ImageResource underline();
    
    @Source(PATH + "help.png")
    public ImageResource getHelp();
    
    @Source(PATH + "book.png")
    public ImageResource getWork();
    
    @Source(PATH + "menu-64.png")
    public ImageResource getHamburger();

    @Source(PATH + "refresh.png")
    public ImageResource getRefresh();
    
//    @Source(PATH + "ReqUserAcc_button.png")
//    public ImageResource getUserAccReq();
//    
//    @Source(PATH + "DwnToolbox_button.png")
//    public ImageResource getDwnToolbox();
//    
//    @Source(PATH + "readDoc_button.png")
//    public ImageResource getDocButton();
    
//    @Source(PATH + "BrainMapperButton.png")
//    public ImageResource getCoregButton();
//    
//    @Source(PATH + "GithubButton.png")
//    public ImageResource getGithubButton();
//    
//    @Source(PATH + "MatlabButton.png")
//    public ImageResource getMatlabButton();
    
//    @Source(PATH + "portalEEGImage2.png")
//    public ImageResource getPortalEEGIm();
    
//    @Source(PATH + "LoniButton.png")
//    public ImageResource getLoniButton();

	@Source(PATH + "folder32.png")
	ImageResource folder();
	
	@Source(PATH + "timeseries32.png")
    ImageResource timeseries();

	@Source(PATH + "document32.png")
    ImageResource document();
	
	@Source(PATH + "starUnsel.png")
	ImageResource unstar();
	
	@Source(PATH + "star.png")
	ImageResource star();
	
	@ImageOptions(height=35,width=96)
	@Source(PATH + "thumbs-up.png")
	ImageResource thumbsUp();

	@ImageOptions(height=35,width=96)
	@Source(PATH + "thumbs-down.png")
	ImageResource thumbsDown();

	@ImageOptions(height=35,width=96)
	@Source(PATH + "question.png")
	ImageResource thumbsUnknown();
	
	@ImageOptions(height=24,width=24)
	@Source(PATH + "left-arrow.png")
	ImageResource left();

	@ImageOptions(height=24,width=24)
	@Source(PATH + "right-arrow.png")
	ImageResource right();
	
//	@Source(PATH + "brainMapperBanner.png")
//    ImageResource getBrainMapperIm();
	
//	@Source(PATH + "sloganBanner.png")
//    ImageResource getSloganBannerIm();
	
//	@Source(PATH + "matlabBanner.png")
//    ImageResource getMatlabBannerIm();
	
}



