/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

// dir=Mayo_IEED_001&mefDir=IC_001&mafFile=MayoIEED_001_9-999-001_import.xml 

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;

import edu.upenn.cis.db.mefview.client.plugins.IPluginRegistration;
import edu.upenn.cis.db.mefview.shared.places.WebConsolePlace;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class WebConsole implements EntryPoint {
	
	private WebConsolePlace defaultPlace = new WebConsolePlace();
	
	public static ClientFactory clientFactory = GWT.create(ClientFactory.class);
	
	public static IPluginRegistration registry = GWT.create(IPluginRegistration.class);
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		registry.registerActions();
		registry.registerPanels();
		registry.bindActionsToDataTypes();

		// Start ActivityManager for the main widget with our ActivityMapper
		SimplePanel root = new SimplePanel();
		LayoutPanel lp = new LayoutPanel();
		RootLayoutPanel.get().add(lp);
        ActivityMapper activityMapper = new WebConsoleActivityMapper(lp, clientFactory);
        ActivityManager activityManager = new ActivityManager(activityMapper, 
        		(IeegEventBus)IeegEventBusFactory.getGlobalEventBus());
        activityManager.setDisplay(root);
        
		 // Start PlaceHistoryHandler with our PlaceHistoryMapper
        EegPlaceHistoryMapper historyMapper = GWT.create(EegPlaceHistoryMapper.class);
        PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
        historyHandler.register(clientFactory.getPlaceController(), 
        		(IeegEventBus)IeegEventBusFactory.getGlobalEventBus(), 
        		defaultPlace);
        
		// Goes to the place represented on URL else default place
        historyHandler.handleCurrentHistory();
	}
	
	/*
	public void setup() {

		//		SuperDevModeUtil.showDevMode();
		
		// Use GIN, then can do MockClickHandlers, MockHasValue, etc.
		
		FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName("login_page");
        RootPanel.get().add(wrapper);
        wrapper.getElement().setAttribute("id", "wrapper");
		
        FlowPanel hp = new FlowPanel();
        
		hp.setStyleName("login_header");
		
		////
//	    String path = Window.Location.getPath();
//	    
//	    if (path.contains("/")) {
//	  	  path = path.substring(0, path.lastIndexOf('/'));
//	  	  if (!path.endsWith("/"))
//	  		  path = path + "/";
//	    }
//	    String protocol = Window.Location.getProtocol();
//	    String urlBase = protocol + "//" + Window.Location.getHost() + "/" + path +
//	    		clientFactory.getServerConfiguration().getServerName() + "/";
	    String urlBase = RequestHtml.getPath(clientFactory.getServerConfiguration().getServerName());
		Image logo = new Image(urlBase + "logo.png");
//
		logo.setSize("72px", "74px");
//		img.setAltText(be.getTooltip());

		////
//		Image logo = new Image(ResourceFactory.getLoginLogo().getSafeUri());

		hp.add(logo);

		Label l = new Label("HABITAT Core Platform");
		l.setStyleName("login_title");
		hp.add(l);
		wrapper.add(hp);

		// Start ActivityManager for the main widget with our ActivityMapper
		SimplePanel root = new SimplePanel();
        ActivityMapper activityMapper = new WebConsoleActivityMapper(root, clientFactory);
        ActivityManager activityManager = new ActivityManager(activityMapper, 
        		(IeegEventBus)IeegEventBusFactory.getGlobalEventBus());
        activityManager.setDisplay(root);
        
		 // Start PlaceHistoryHandler with our PlaceHistoryMapper
        EegPlaceHistoryMapper historyMapper = GWT.create(EegPlaceHistoryMapper.class);
        PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
        historyHandler.register(clientFactory.getPlaceController(), 
        		(IeegEventBus)IeegEventBusFactory.getGlobalEventBus(), 
        		defaultPlace);
        
		// Goes to the place represented on URL else default place
        historyHandler.handleCurrentHistory();
     }*/
}
