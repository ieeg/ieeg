/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class PortalSummary implements IsSerializable {
	private int numSnapshots;
	private int numAnnotations;
	
	private int numUserReadableExperiments;
	private int numUserReadableStudies;
	
	private int numWorldReadableExperiments;
	private int numWorldReadableStudies;
	
	private int numUsers;

	public PortalSummary() {
		
	}
	
	public PortalSummary(
			int numSnapshots, 
			int numAnnotations, 
			int numUserReadableExperiments,
			int numUserReadableStudies, 
			int numWorldReadableExperiments, 
			int numWorldReadableStudies, 
			int numUsers) { 
		super();
		this.numSnapshots = numSnapshots;
		this.numAnnotations = numAnnotations;
		this.numUserReadableExperiments = numUserReadableExperiments;
		this.numUserReadableStudies = numUserReadableStudies;
		this.numWorldReadableExperiments = numWorldReadableExperiments;
		this.numWorldReadableStudies = numWorldReadableStudies;
		this.numUsers = numUsers;
	}

	public int getNumSnapshots() {
		return numSnapshots;
	}

	public int getNumAnnotations() {
		return numAnnotations;
	}

	public int getNumUserReadableExperiments() {
		return numUserReadableExperiments;
	}

	public int getNumUserReadableStudies() {
		return numUserReadableStudies;
	}
	
	public int getNumWorldReadableExperiments() {
		return numWorldReadableExperiments;
	}

	public int getNumWorldReadableStudies() {
		return numWorldReadableStudies;
	}

	public int getNumUsers() {
		return numUsers;
	}

	public void setNumSnapshots(int numSnapshots) {
		this.numSnapshots = numSnapshots;
	}

	public void setNumAnnotations(int numAnnotations) {
		this.numAnnotations = numAnnotations;
	}

	public void setNumWorldReadableExperiments(int numWorldReadableExperiments) {
		this.numWorldReadableExperiments = numWorldReadableExperiments;
	}

	public void setNumWorldReadableStudies(int numWorldReadableStudies) {
		this.numWorldReadableStudies = numWorldReadableStudies;
	}

	public void setNumUsers(int numUsers) {
		this.numUsers = numUsers;
	}

	public void setNumUserReadableExperiments(int numUserReadableExperiments) {
		this.numUserReadableExperiments = numUserReadableExperiments;
	}

	public void setNumUserReadableStudies(int numUserReadableStudies) {
		this.numUserReadableStudies = numUserReadableStudies;
	}

}
