/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;

public class OpenSnapshotByNameOrId extends DialogBox implements IOpenSnapshotById {
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	TextBox theName = new TextBox();
	
	Handler theHandler;
	
	public OpenSnapshotByNameOrId() {
		
	}

//	public OpenSnapshotByNameOrId(
//			final String userID,
//			final ClientFactory factory) {
//		createDialog(factory);
//	}
	
	public void createDialog(final ClientFactory factory) {
		//theName.setWidth("32em");
		final String userID = factory.getSessionModel().getUserId();
		VerticalPanel vp = new VerticalPanel();

		setTitle("Open Dataset");
		setText("Open Dataset");
		setWidget(vp);

		vp.add(new Label("Dataset Name or ID:"));
		vp.add(theName);

		HorizontalPanel hp = new HorizontalPanel();
		hp.add(ok);
		hp.add(cancel);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				open(userID, factory);
			}

		});

		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}

		});

		theName.addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					open(userID, factory);
				}

			}
		});

	}

	public void setFocus(boolean setFocus) {
		theName.setFocus(true);
	}

	private void open(final String userID,
			final ClientFactory factory) {
		hide();
		theHandler.open(getDataset());
	}

	@Override
	public void initialize(DialogInitializer<IOpenSnapshotById> initFn) {
		initFn.init(this);
	}

	@Override
	public void reinitialize(DialogInitializer<IOpenSnapshotById> initFn) {
		initFn.reinit(this);
	}

	@Override
	public void init(Handler responder) {
		theHandler = responder;
		
	}

	@Override
	public String getDataset() {
		return theName.getText();
	}
	
	public void open() {
		center();
//		setFocus();
	}
	
	public void close() {
		hide();
	}

	@Override
	public void focus() {
		setFocus(true);
	}

//	@Override
//	public void show() {
//		super.center();
//		setFocus(true);
//	}
}
