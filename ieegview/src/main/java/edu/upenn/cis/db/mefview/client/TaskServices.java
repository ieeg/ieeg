package edu.upenn.cis.db.mefview.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.shared.Task;
import edu.upenn.cis.db.mefview.shared.TaskSubmission;

@RemoteServiceRelativePath("tasks")
public interface TaskServices extends RemoteService {

	/** Tasks I can submit to */
	public List<Task> getActiveTasks(SessionToken session);
	
	List<TaskSubmission> getSubmittedTasks(SessionToken session);
	
	public void submitForTask(SessionToken session, TaskSubmission submission);

}
