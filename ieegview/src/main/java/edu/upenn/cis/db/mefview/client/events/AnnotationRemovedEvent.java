/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class AnnotationRemovedEvent extends GwtEvent<AnnotationRemovedEvent.Handler>{
	private static final Type<AnnotationRemovedEvent.Handler> TYPE = new Type<AnnotationRemovedEvent.Handler>();
	
	private final DataSnapshotModel model;
	
	private final Annotation a;
	
	public AnnotationRemovedEvent(DataSnapshotModel model,
			final Annotation ann){
		this.model = model;
		a = ann;
	}
	
	public DataSnapshotModel getDataSnapshotModel() {
		return model;
	}
	
	public Annotation getAnnotation() {
		return a;
	}
	public static com.google.gwt.event.shared.GwtEvent.Type<AnnotationRemovedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AnnotationRemovedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AnnotationRemovedEvent.Handler handler) {
		handler.onAnnotationRemoved(model, a);
	}

	public static interface Handler extends EventHandler {
		void onAnnotationRemoved(DataSnapshotModel action, 
				Annotation a);
	}
}
