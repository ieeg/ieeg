package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.core.client.GWT;

public class DialogFactory {
	static ILoginDialog login = null;
	static IOpenSnapshotById snapshotOpen = null;
	static ICreateProject createProject = null;
	static IShowTraces showTraces = null;
//	static IFilterDialog filterDialog = null;
	static IDownloadAccept downloadAccept = null;
	
	/**
	 * Simple interface that gets triggered to initialize
	 * a new dialog
	 * 
	 * @author zives
	 *
	 */
	public static interface DialogInitializer<T extends DialogWithInitializer<?>> {
		public void init(T objectBeingInited);
		public void reinit(T objectBeingInited);

	}
	
	/**
	 * Dialog that accepts an init closure
	 * 
	 * @author zives
	 *
	 */
	public static interface DialogWithInitializer<T extends DialogWithInitializer<?>> {
		public void initialize(DialogInitializer<T> initFn);
		public void reinitialize(DialogInitializer<T> initFn);
	}
	
	public static synchronized ILoginDialog getLoginDialog(DialogInitializer<ILoginDialog> initializer) {
		if (login == null) {
			login = GWT.create(ILoginDialog.class);
			login.initialize(initializer);
		}
		return login;
	}
	
	public static synchronized IOpenSnapshotById getSnapshotIdDialog(DialogInitializer<IOpenSnapshotById> initializer) {
		if (snapshotOpen == null) {
			snapshotOpen = GWT.create(IOpenSnapshotById.class);//new OpenSnapshotByNameOrId();
			snapshotOpen.initialize(initializer);
		} else
			snapshotOpen.reinitialize(initializer);
		return snapshotOpen;
	}

	public static synchronized ICreateProject getCreateProjectDialog(DialogInitializer<ICreateProject> initializer) {
		if (createProject == null) {
			createProject = GWT.create(ICreateProject.class);
			createProject.initialize(initializer);
		} else
			createProject.reinitialize(initializer);
		return createProject;
	}
	
	public static synchronized ISelectMontage getSelectMontageDialog(DialogInitializer<ISelectMontage> initializer) {
		final ISelectMontage selectMontage = GWT.create(ISelectMontage.class);
		selectMontage.initialize(initializer);
		return selectMontage;
	}
	
	public static synchronized IEditMontage getEditMontageDialog(DialogInitializer<IEditMontage> initializer) {
		
		final IEditMontage editMontage = GWT.create(IEditMontage.class);
		editMontage.initialize(initializer);
		
		return editMontage;
	}
	
	public static synchronized IShowTraces getShowTracesDialog(DialogInitializer<IShowTraces> initializer) {
//	if (showTraces == null) {
		showTraces = GWT.create(IShowTraces.class);	
		showTraces.initialize(initializer);
//	}
		
	return showTraces;
	}
	
	public static synchronized IDownloadAccept getDownloadAcceptDialog(DialogInitializer<IDownloadAccept> initializer) {
		if (downloadAccept == null) {
			downloadAccept = GWT.create(IDownloadAccept.class);
			downloadAccept.initialize(initializer);
		} else
			downloadAccept.reinitialize(initializer);
			
		return downloadAccept;
	}
	//	public static synchronized IFilterDialog getFilterDialog(DialogInitializer<IFilterDialog> initializer) {
//		if (filterDialog == null) {
//			filterDialog = GWT.create(ICreateProject.class);//new OpenSnapshotByNameOrId();
//			filterDialog.initialize(initializer);
//		} else
//			filterDialog.initialize(initializer);
//		return filterDialog;
//	}
	
}
