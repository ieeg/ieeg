package edu.upenn.cis.db.mefview.client.plugins.news.widgets;

/*
 * #%L
 * GWT Maps API V3 - Showcase
 * %%
 * Copyright (C) 2011 - 2012 GWT Maps API V3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

import com.google.gwt.ajaxloader.client.ArrayHelper;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapTypeId;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.geometrylib.EncodingUtils;
import com.google.gwt.maps.client.visualizationlib.HeatMapLayer;
import com.google.gwt.maps.client.visualizationlib.HeatMapLayerOptions;
import com.google.gwt.maps.client.visualizationlib.WeightedLocation;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.shared.LatLon;

/**
 * See <a href=
 * "https://developers.google.com/maps/documentation/javascript/layers#JSHeatMaps"
 * >HeatMapLayer API Doc</a>
 */
public class HeatMapLayerWidget extends Composite {

  private final FlowPanel pWidget;
  private MapWidget mapWidget;
  
  List<LatLon> coordinates;
  
  int xSize = 750;
  int ySize = 500;

  public HeatMapLayerWidget(List<LatLon> coordinates, int xsize, int ysize) {
	  this.coordinates = coordinates;
	  xSize = xsize;
	  ySize = ysize;
	  
    pWidget = new FlowPanel();
    initWidget(pWidget);
    pWidget.setHeight(ySize +"px");
    pWidget.setStylePrimaryName("mapWrapper");
//    pWidget.setWidth("100%");

    draw();
  }

  public HeatMapLayerWidget(List<LatLon> coordinates) {
	  this.coordinates = coordinates;
    pWidget = new FlowPanel();
    pWidget.setHeight(ySize +"px");
//    pWidget.setWidth("100%");
    initWidget(pWidget);

    draw();
  }

  private void draw() {

    pWidget.clear();

    drawMap();
  }

  private void drawMap() {
    // zoom out for the clouds
    LatLng center = LatLng.newInstance(19.95,9);//-75.1666667);
    MapOptions opts = MapOptions.newInstance();
//    opts.setZoom(2);
//    opts.setCenter(center);
    opts.setStreetViewControl(false);
    opts.setPanControl(false);
    opts.setZoomControl(false);
    opts.setMapTypeControl(false);
    opts.setMapTypeId(MapTypeId.TERRAIN);

    mapWidget = new MapWidget(opts);
    mapWidget.setWidth("100%");
    mapWidget.setHeight(ySize + "px");
//    mapWidget.setSize(xSize + "px", ySize + "px");
//    pWidget.add(new Label("Map widget:"));
    pWidget.add(mapWidget);
    

    // show transit layer
//    TransitLayer transitLayer = TransitLayer.newInstance();
//    transitLayer.setMap(mapWidget);

    // create layer
    HeatMapLayerOptions options = HeatMapLayerOptions.newInstance();
    options.setOpacity(0.9);
    options.setRadius(5);
    options.setGradient(getSampleGradient());
    options.setMaxIntensity(3);
    options.setMap(mapWidget);

    HeatMapLayer heatMapLayer = HeatMapLayer.newInstance(options);
    // set data
    JsArray<LatLng> dataPoints = getData();
    heatMapLayer.setData(dataPoints);

    mapWidget.setCenter(center);
    mapWidget.setZoom(1);
    mapWidget.triggerResize();
  }

  /**
   * Sample gradient from <a href=
   * "https://google-developers.appspot.com/maps/documentation/javascript/examples/layer-heatmap"
   * >Google Maps Example</a>
   * 
   * @return
   */
  private JsArrayString getSampleGradient() {
    String[] sampleColors = new String[] { "rgba(0, 255, 255, 0)", "rgba(0, 255, 255, 1)", "rgba(0, 191, 255, 1)",
        "rgba(0, 127, 255, 1)", "rgba(0, 63, 255, 1)", "rgba(0, 0, 255, 1)", "rgba(0, 0, 223, 1)",
        "rgba(0, 0, 191, 1)", "rgba(0, 0, 159, 1)", "rgba(0, 0, 127, 1)", "rgba(63, 0, 91, 1)", "rgba(127, 0, 63, 1)",
        "rgba(191, 0, 31, 1)", "rgba(255, 0, 0, 1)" };
    return ArrayHelper.toJsArrayString(sampleColors);
  }
  
  

  /**
   * Data from the server
   * 
   * @return
   */
  private JsArray<LatLng> getData() {
	JsArray<LatLng> ret = JavaScriptObject.createArray().cast();
	
	for (LatLon res: coordinates)
		ret.push(LatLng.newInstance(res.getLat(), res.getLon()));

    return ret;
  }

}