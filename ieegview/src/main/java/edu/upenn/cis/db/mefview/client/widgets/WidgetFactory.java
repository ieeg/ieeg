package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.core.client.GWT;

public class WidgetFactory {

	public static IWidgetFactory factory = null;
	
	public static synchronized IWidgetFactory get() {
		if (factory == null)
			factory = GWT.create(IWidgetFactory.class);
		
		return factory;
	}
	
	
}
