/*
 * Copyright 2015 Ieeg.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Strings;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogWithInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IEditMontage;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

public class EditMontage extends DialogBox implements
		DialogWithInitializer<IEditMontage>, IEditMontage {
	private Button ok = new Button("OK");
	private Button cancel = new Button("Cancel");
	private ListBox plotChannel = new ListBox();
	private ListBox to = new ListBox();
	private ListBox map = new ListBox();
	private List<String> sources;
	private CheckBox isGlobal = new CheckBox("Share Montage?");

	private Button remove = new Button("Remove");
	private Button plotAgainst = new Button("Against Ref"); // Plot channel
															// against selected
															// ref
	private Button plotAsRecorded = new Button("As Recorded"); // Plot channel
																// as recorded
	private TextBox name = new TextBox();
	private EEGMontage montage = new EEGMontage();
	private Boolean isNewMontage = true;

	private TextBox errMessage = new TextBox();

	private List<Integer> channelsInMontage = new ArrayList<>();
	
	
	public EditMontage() {
	}

	public void createDialog() {

		setTitle("Channel Remontage");
		setText("Channel Remontage");
		setStyleName("ChanSelectDialog");
		VerticalPanel layout = new VerticalPanel();

		// Used to display error messages when creating montage list
		errMessage.setText("");
		errMessage.setWidth("220px");
		errMessage.setReadOnly(true);

		// Add Name textBox
		HorizontalPanel topP = new HorizontalPanel();
		topP.add(new Label("Montage Name:"));
		topP.add(name);
		topP.add(isGlobal);
		topP.setHeight("40px");
		topP.setSpacing(10);
		
		layout.add(topP);
		name.setText("newMontage");

		HorizontalPanel boxes = new HorizontalPanel();
		layout.add(boxes);

		plotChannel.setWidth("200px");
		plotChannel.setVisibleItemCount(20);

		VerticalPanel fromPane = new VerticalPanel();
		fromPane.add(new Label("Channels:"));
		fromPane.add(plotChannel);

		// Setup ToPane
		VerticalPanel toPane = new VerticalPanel();
		toPane.add(new Label("Ref Channels:"));
		toPane.add(to);
		to.setWidth("200px");
		to.setVisibleItemCount(20);

		// Setup Montage info
		VerticalPanel mapBox = new VerticalPanel();
		mapBox.add(new Label("Remontage (channel / ref):"));
		mapBox.add(map);
		map.setWidth("200px");
		map.setVisibleItemCount(20);
		mapBox.add(remove);
		mapBox.setCellHorizontalAlignment(remove,
				HasHorizontalAlignment.ALIGN_CENTER);

		VerticalPanel gap = new VerticalPanel();
		gap.setWidth("20px");

		// Setup ButtonPane
		VerticalPanel buttonPane = new VerticalPanel();
		buttonPane.add(plotAsRecorded);
		buttonPane.add(plotAgainst);

		boxes.add(fromPane);
		boxes.add(buttonPane);
		boxes.add(toPane);
		boxes.setCellVerticalAlignment(buttonPane,
				HasVerticalAlignment.ALIGN_MIDDLE);
		boxes.add(gap);
		boxes.add(mapBox);

		setWidget(layout);

		HorizontalPanel hp = new HorizontalPanel();
		layout.add(hp);
		layout.setCellHorizontalAlignment(hp,
				HasHorizontalAlignment.ALIGN_RIGHT);
		hp.add(errMessage);
		errMessage.setStylePrimaryName("errMessage");

		hp.add(ok);
		hp.add(cancel);
		
		

	}

	public EEGMontage getMontage() {
		return montage;
	}

	@Override
	public void setSources(List<INamedTimeSegment> traces) {
		sources = new ArrayList<String>();
		for (INamedTimeSegment i : traces) {
			sources.add(i.getLabel());
		}

		for (String s : sources) {
			plotChannel.addItem(s);
			to.addItem(s);
		}
	}

	@Override
	public void setMontage(EEGMontage montage) {
		this.montage = montage;
		setIsNewMontage(false);
		map.clear();
		name.setValue(montage.getName());
		isGlobal.setValue(montage.getIsPublic());

		for (EEGMontagePair p : montage.getPairs()) {
			String f = p.getEl1();
			String t = p.getEl2();
			map.addItem(getMapString(f, t));
			
			int i=0;
			for (String s:sources) {
				if (s.equals(f))
					break;
			}
			
			channelsInMontage.add(i);
		}
	}


	
	public Boolean getIsNewMontage() {
		return isNewMontage;
	}

	public void setIsNewMontage(Boolean isNewMontage) {
		this.isNewMontage = isNewMontage;
	}

	private String getMapString(String from, String to) {
		if (to != null && !from.equals(to)) {
			return from + " vs " + to;
		} else {
			return from;
		}
	}

	@Override
	public void init(final Handler responder) {

		isGlobal.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				montage.setIsPublic(isGlobal.getValue());
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				close();
				
			}
			
		});
		
		plotAgainst.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				errMessage.setText("");
				responder.addChannelPair();

			}

		});

		plotAsRecorded.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				errMessage.setText("");
				responder.addSingleChannelPair();

			}

		});

		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				errMessage.setText("");
				String montageName = Strings.nullToEmpty(name.getText()).trim();
				if ("".equals(montageName)) {
					errMessage.setText("Please enter a name for the montage");
					return;
				}
				montage.setName(montageName);
				
				responder.updateMontage(montage, isNewMontage);
			
				
				close();

			}
		});
		
		remove.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				errMessage.setText("");
				if (map.getSelectedIndex() >= 0){
					 int index = map.getSelectedIndex();
					 map.removeItem(index);
					 channelsInMontage.remove(index);
					 montage.getPairs().remove(index);
				}
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				errMessage.setText("");
				hide();

			}
		});
	}

	@Override
	public void open() {
		center();

	}

	@Override
	public void close() {
		hide();

	}

	@Override
	public void focus() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(DialogInitializer<IEditMontage> initFn) {
		initFn.init(this);

	}

	@Override
	public void reinitialize(DialogInitializer<IEditMontage> initFn) {
		initFn.reinit(this);

	}

	@Override
	public ListBox getPlotChannel() {
		return plotChannel;
	}

	@Override
	public void setPane(EEGPane pane) {
		// eegPane = pane;

	}

	@Override
	public void addSingleChannelPair() {
		errMessage.setText("");
		String f = sources.get(plotChannel.getSelectedIndex());

		map.addItem(getMapString(f, null));
		montage.getPairs().add(new EEGMontagePair(f, null));
		channelsInMontage.add(plotChannel.getSelectedIndex());

	}
	
	@Override
	public void addChannelPair() {
		errMessage.setText("");
		String f = sources.get(plotChannel.getSelectedIndex());
		String t = sources.get(to.getSelectedIndex());


		map.addItem(getMapString(f, t));
		montage.getPairs().add(new EEGMontagePair(f, t));
		channelsInMontage.add(plotChannel.getSelectedIndex());

	}

	@Override
	public void reset() {
		plotChannel.clear();
		to.clear();
		map.clear();
		isNewMontage = true;
		isGlobal.setValue(false);
		name.setValue("New Montage");
		channelsInMontage.clear();
		montage = new EEGMontage();
		montage.setOwnedByUser(true);
		
	}
}
