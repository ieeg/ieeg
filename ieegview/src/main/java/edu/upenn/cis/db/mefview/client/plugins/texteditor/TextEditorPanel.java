/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.texteditor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.JavascriptFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.thirdparty.orion.Orion;
import edu.upenn.cis.db.mefview.client.widgets.IBlankToolbar;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.places.TextPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class TextEditorPanel extends AstroTab implements  
TextEditorPresenter.Display {
	FlowPanel p;
	TextPlace place;
	Label l = new Label();
	
	ClientFactory clientFactory;
	HandlerRegistration hr;
	String title = "Text";
	
	boolean readOnly = true;
	
	HTML content;
	IBlankToolbar toolbar = GWT.create(IBlankToolbar.class);
	
	Button save = new Button("Save");
	
	static boolean injected = false;
	
	public final static String NAME = "text-edit";
	final static TextEditorPanel seed = new TextEditorPanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TextEditorPanel() {
		super();
	}

	public TextEditorPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, //getWidth(), getHeight(), 
		    title);

		if (!readOnly)
			l.setText("Editor - " + getTitle());
		else  
			l.setText("Text Viewer - " + getTitle());
		l.setStylePrimaryName("heading");
		add(toolbar);
		setWidgetLeftRight(toolbar, 2, Unit.PX, 2, Unit.PX);
		add(l);
		setWidgetLeftRight(l, 2, Unit.PX, 66, Unit.PX);
		
		setWidgetTopHeight(l, 2, Unit.PX, 42, Unit.PX);
		if (readOnly) {
			save.setVisible(false);
		}
		
		toolbar.init(clientFactory, this);
		
	}

	public void init(boolean readonly) {
		content = Orion.createContainer(getElementId(), "editor", readonly);
		if (readonly) {
			disableSave();
		}

		add(content);
		loadEditor();

		setWidgetLeftRight(content, 2, Unit.PX, 2, Unit.PX);
		if (readonly)
			setWidgetTopBottom(content, 44, Unit.PX, 4, Unit.PX);
		else
			setWidgetTopBottom(content, 44, Unit.PX, 44, Unit.PX);

		add(save);
		setWidgetLeftRight(save, 2, Unit.PX, 2, Unit.PX);
		setWidgetBottomHeight(save, 2, Unit.PX, 40, Unit.PX);
		
	  }
	
	@Override
	public String getPanelType() {
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new TextEditorPanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	@Override
	public WindowPlace getPlace() {
		return place;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		if (place instanceof TextPlace) {
			this.place = (TextPlace)place;
		}
	}

	public ITextEditorPresenter getPresenter() {
		return (ITextEditorPresenter)super.getPresenter();
	}
	
	public void loadEditor() {
		
		if (injected) {
			populate();
		} else {
			
			JavascriptFactory.loadJavascript("orion/built-editor.min.js",
					new Command() {
						@Override
						public void execute() {
							injected = true;
							populate();

						}
					}, null);
		}		
	}
	
	public void populate() {

		if (place != null && place.getFile() != null)
			getPresenter().loadContent(this.place.getFile());
		else {
			final FileInfo file = new FileInfo(
					"Example", "-", 
					"Example file", 
					"file.txt", 
					"text/text");

			getPresenter().loadContent(file);
		}

	}


	public String getTitle() {
		return title;
	}

	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getContent() {
		Element e = Orion.getEditorBufferParent(getElementId());
		
		return e.getInnerText();
	}
	
	@Override 
	public void setTitle(String title) {
		this.title = title;
		if (!readOnly)
			l.setText("Editor - " + getTitle());
		else  
			l.setText("Text Viewer - " + getTitle());
	}

	@Override
	public void setFileContent(String fileContent) {
		Orion.setFileContent(getElementId(), fileContent);
	}
	
	@Override
	public void disableSave() {
		save.setEnabled(false);
		save.setVisible(false);
		setWidgetTopHeight(l, 2, Unit.PX, 42, Unit.PX);
	}

	@Override
	public void setSaveHandler(ClickHandler handler) {
		save.addClickHandler(handler);
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}

	@Override
	public FileInfo getFile() {
		return place.getFile();
	}
	
	private String getElementId() {
		return "div_parent_" + place.getFile().getDetails();
	}
}
