/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.presenters.AppPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class RecentActivitiesPane extends ScrollPanel 
implements RecentActivitiesPresenter.Display {
	ClientFactory clientFactory;
	VerticalPanel eventPanel = new VerticalPanel();
	Presenter presenter;
	
	public void initialize(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;

		// Add activities to Activities Panel...
		this.setStyleName("ieeg-user-profile-feed");
		this.setHeight("100%");
		clear();
		add(eventPanel);
	}
	

	public void addEvents(List<UserProfilePresenter.StampedEvent> events) {
		eventPanel.clear();

		for (UserProfilePresenter.StampedEvent event: events) {
			String e = event.event;
			
			if (e.startsWith("<b>You</b>")) {
				UserInfo info = getSessionModel().getUserInfo();
				if (info.getPhoto() == null || info.getPhoto().isEmpty()) {
					Image photo = new Image(ResourceFactory.getUnknownUser().getSafeUri());
					photo.setPixelSize(32, 32);
					
					e = photo.toString() + e;
				} else {
					Image photo;
					if (info.getPhoto().startsWith("http://"))
						photo = new Image(info.getPhoto());
					else
						photo = new Image(AppPresenter.URL + "/" + info.getPhoto());
					photo.setPixelSize(32, 32);
					
					e = photo.toString() + e;
				}
			} else {
				Image photo = new Image(ResourceFactory.getUnknownUser().getSafeUri());
				photo.setPixelSize(32, 32);
				
				e = photo.toString() + e;
			}
			
			eventPanel.add(new HTML("<div class='ieeg-event'>" + e + " @ " + 
					"<span class='ieeg-stamp'>" +
					DateTimeFormat.getMediumDateTimeFormat().format(new Timestamp(event.time)) 
					//+ " / " + eventStamps.get(0)
			+ "</span></div>"));
		}
	}
	
	public void refresh(List<UserProfilePresenter.StampedEvent> events) {
		Collections.sort(events);
		
		for (int i = 0; i < events.size() - 1; i++) {
			for (int j = i+1; j < events.size() && 
					events.get(j).time.longValue() == events.get(i).time.longValue(); j++) {
				if (events.get(i).event.equals(events.get(j).event)) {
//					GWT.log("Want to remove " + events.get(j).event);
					events.remove(j);
					j--;
				}
			}
			if (--events.get(i).ttl == 0) {
				events.remove(i);
				i--;
			}
		}
		
		if (events.size() > 50)
			events = events.subList(0, 50);
		
		addEvents(events);
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}

	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}
	
	public SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
}
