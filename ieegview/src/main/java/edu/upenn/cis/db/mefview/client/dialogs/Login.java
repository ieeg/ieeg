/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.LoginSession.ILoginHandler;

/**
 * Login dialog box
 * 
 * @author zives
 *
 */
public class Login extends DialogBox implements ILoginDialog {
	TextBox uid = new TextBox();
	TextBox password = new PasswordTextBox();
	
	LoginSession.ILoginHandler handler;
	
	ClientFactory clientFactory;
//	AppModel theModel;
//	IAppPanel appPanel;
//	IAppPresenter appPres;
//	
//	final String AUTH_URL = "https://accounts.google.com/o/oauth2/auth";
//	final String CLIENT_ID = "1041279770671-9lmt7rtqjpt2n87v23r1t9k2cqh65n05.apps.googleusercontent.com";
//
//	AuthRequest req = new AuthRequest(AUTH_URL, CLIENT_ID)
//		.withScopes("https://www.googleapis.com/auth/plus.me");
	
//	LayoutPanel myPanel;
	
	/**
	 * Create a remote service proxy to talk to the server-side data access service.
	 */
//	private final EEGDisplayAsync eegService = GWT
//			.create(EEGDisplay.class);
//	
//	private final ProjectServicesAsync projectService = GWT
//			.create(ProjectServices.class);
//	
//	private final SearchServicesAsync searchService = GWT
//			.create(SearchServices.class);
//	
//	private final SnapshotServicesAsync snapshotService = GWT
//			.create(SnapshotServices.class);
//	
//	private final TaskServicesAsync taskService = GWT
//			.create(TaskServices.class);
	
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
//	private static final String SERVER_ERROR = "An error occurred while "
//			+ "attempting to contact the server. Please check your network "
//			+ "connection and try again.";

	class LoginPanel extends FocusPanel {
		public LoginPanel(final FocusWidget def) {
			addFocusHandler(new FocusHandler() {

				public void onFocus(FocusEvent arg0) {
					def.setFocus(true);
				}
				
			});
		}
	}
	
	/*
	public Login(LayoutPanel myParent, final ClientFactory clientFactory) {
		this(myParent, null, null, clientFactory);
	}
	
	public Login(
			LayoutPanel myParent, 
			IHeaderBar bar, 
			ITabber tabber,
			final ClientFactory clientFactory
			) {
		super(true, true);
		this.clientFactory = clientFactory;
		this.myPanel = myParent;
		
		setup(bar, tabber);
	}*/
	
//	public void initialize(
//			LayoutPanel myParent, 
//			IHeaderBar bar, 
//			Map<String,ITabber> tabber,
//			ITabSet tabset,
//			final ClientFactory clientFactory
//			) {
//		this.clientFactory = clientFactory;
//		this.myPanel = myParent;
//		
//		setup(bar, tabber, tabset);
//	}

	@Override
	public void createDialog(final ILoginHandler handler) {
		this.getStyleElement().getStyle().setZIndex(99);
		setAutoHideEnabled(false);
		setTitle("Welcome!");
		setText("Please Log in...");
		Label uName = new Label("User ID: ");
		Label pwd = new Label("Password: ");
		
//		Cookies.setCookie("ieeg-use-login", "true");
		
		final FocusPanel fp = new LoginPanel(uid);
		final Button ok = new Button("Log in");
		final Button logInAsGuest = new Button("Continue as guest");
		
//		this.appWidget = appWidget;
		Grid g = new Grid(4,2);
		
		g.setWidget(0, 0, uName);
		g.setWidget(0, 1, uid);
		g.setWidget(1, 0, pwd);
		g.setWidget(1, 1, password);

		uid.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER || event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
//					Panel p = RootPanel.get();
//					Login.this.hide();
//					ControlPane sw = new ControlPane(p, getUserID(), getPassword(),
//							datasetID, friendlyName);
					handler.login();//datasetID, friendlyName);
				}
			}
			
		});
		uid.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent arg0) {
				if (arg0.getCharCode() == '\t' || arg0.getNativeEvent().getCharCode() == KeyCodes.KEY_TAB) {
					password.setFocus(true);
				} else if (arg0.getCharCode() == '\r' || arg0.getCharCode() == '\n' || arg0.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER) {
//					Panel p = RootPanel.get();
//					Login.this.hide();
//					ControlPane sw = new ControlPane(p, getUserID(), getPassword(),
//							datasetID, friendlyName);
					handler.login();//datasetID, friendlyName);
				}
			}
			
		});
		password.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent arg0) {
				if (arg0.getCharCode() == '\t' || arg0.getNativeEvent().getCharCode() == KeyCodes.KEY_TAB) {
					ok.setFocus(true);
				} else if (arg0.getCharCode() == '\r' || arg0.getCharCode() == '\n' || arg0.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER) {
//					ok.setFocus(true);
					handler.login();//datasetID, friendlyName);
				}
			}
			
		});
		password.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER || event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handler.login();//datasetID, friendlyName);
				}
			}
		});

		HorizontalPanel p = new HorizontalPanel();
		p.add(ok);
		p.add(logInAsGuest);
		g.setWidget(2, 1, p);
		setWidget(fp);
		fp.setWidget(g);
		fp.setFocus(true);
		uid.setAccessKey('u');
		password.setAccessKey('p');
		ok.setFocus(true);
		ok.setAccessKey('o');
		uid.setFocus(true);
		
//		final Login self = this;
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				handler.login();//datasetID, friendlyName);
			}
			
		});
		
		logInAsGuest.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
//				autoLogin(Globals.GUEST_USERNAME, "");
				Login.this.hide();
			}
			
		});
		
		Timer t = new Timer() {
		      @Override
		      public void run() {
		        fp.setFocus(true);
		      }
		    };
		    
		t.schedule(1000);
//		parentPanel.add(appPanel);

		setModal(true);
		setGlassEnabled(false);
		center();
	}
	
//	public void autoLogin(final String uid, final String pwd, final boolean hide) {
//		eegService.getServerConfiguration(new AsyncCallback<ServerConfiguration>() {
//
//			@Override
//			public void onFailure(Throwable caught) {
//				Window.alert("Error: unable to access the server to get config info");
//
//				autoLogin(uid, pwd, hide);
//			}
//
//			@Override
//			public void onSuccess(final ServerConfiguration config) {
//				clientFactory.setServerConfiguration(config);
//				if (Document.get() != null) {
//					Document.get().setTitle(config.getServerName());
//				}
////				IeegEventBusFactory.getGlobalEventBus().fireEvent(new ServerTitleObtainedEvent(result.getServerName()));
//				eegService.getUserId(uid, Md5Hash.md5(pwd), new AsyncCallback<UserInfo>() {
//
//					public void onFailure(Throwable caught) {
//						Window.alert("Error: guest account is disabled. "
//								+ caught.getMessage());//Please contact the ieeg administrator");
//
//						Login.this.center();
//						Login.this.show();
//					}
//
//					public void onSuccess(UserInfo result) {
//						GWT.log("Logged in successfully");
//						setSession(result);
//						IeegEventBusFactory.getGlobalEventBus().fireEvent(new ServerTitleObtainedEvent(config.getServerName()));
//						if (hide)
//							Login.this.hide();
//									
//					}
//
//				});
//			}
//			
//		});
//	}
//	
//	public void login() {
//		Login.this.hide();
//
//		eegService.getUserId(getUserID(), Md5Hash.md5(getPassword()), new AsyncCallback<UserInfo>() {
//
//			public void onFailure(Throwable caught) {
//				Window.alert("Error: invalid user ID or password: " + caught.getMessage());
//
//				Login.this.center();
//				Login.this.show();
//			}
//
//			public void onSuccess(UserInfo result) {
//				setSession(result);
//			}
//
//		});
//	}
//	
//	public void setSession(UserInfo result) {
//		final SessionToken sessionToken = result.getSessionToken();
//		GWT.log("Setting session prior to calling App Presenter");
//		
//		clientFactory.setSessionID(sessionToken);
//		Cookies.setCookie(
//				ToolUploadCookies.COOKIE_NAME,
//				sessionToken.getId(),
//				null,
//				null,
//				ToolUploadCookies.SERVLET_PATH,
//				true);
//		clientFactory.setUserId(result.getName());
//		clientFactory.setUserInfo(result);
//		
//		clientFactory.setSessionID(sessionToken);
//		theModel.setSessionToken(sessionToken);
//		theModel.setUserID(result.getLogin());
//		appPanel.login(result,
//				clientFactory.getServerConfiguration().getServerName());
//		GWT.log("Setting session and binding App Presenter");
//		appPres.bind();
//	}
//	
//	public void setup(IHeaderBar bar, Map<String,ITabber> tabber, ITabSet tabset) {
////		RootPanel.get().clear();
//		myPanel.clear();
//
//		clientFactory.setPortal(eegService);
//		clientFactory.setPortalService(new ServerAccess(snapshotService, clientFactory));
//		clientFactory.setSearchServices(searchService);
//		clientFactory.setTaskServices(taskService);
//		clientFactory.setProjectServices(projectService);
//
//		theModel = new AppModel((ClientFactoryImpl)clientFactory);
//		//appPanel = new AppPanel(clientFactory);
//		appPanel = GWT.create(IAppPanel.class);
//		appPanel.init(clientFactory, bar, tabber, tabset);
//		appPres = GWT.create(IAppPresenter.class);
//		appPres.init(clientFactory, theModel);//new AppPresenter(clientFactory, theModel);
//		appPres.setDisplay(appPanel);
//
//		appPanel.getElement().setAttribute("id", "main_wrapper");
//		myPanel.getElement().setAttribute("id", "rootLayoutPanel");
//		
//		// Make the host panels go away if we have our own controls
//		if (tabber != null) {
//			myPanel.setSize("0px", "0px");
//			myPanel.getElement().getStyle().setProperty("minHeight", "0px");
//		} else
//			myPanel.add(appPanel);
//
//		GWT.log("Requesting guest autologin");
//		autoLogin(Globals.GUEST_USERNAME, "", false);
//	}
	
	public String getUserID() {
		return uid.getText();
	}
	
	public String getPassword() {
		return password.getText();
	}

	@Override
	public void showCentered() {
		show();
		center();
	}

	@Override
	public void initialize(DialogInitializer initFn) {
		initFn.init(this);
	}

	@Override
	public void reinitialize(DialogInitializer<ILoginDialog> initFn) {
		initFn.reinit(this);
	}

	@Override
	public Element getRoot() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserID(String userID) {
		// TODO Auto-generated method stub
		
	}
}
