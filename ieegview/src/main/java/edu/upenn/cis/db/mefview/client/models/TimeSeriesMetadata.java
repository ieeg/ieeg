/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class TimeSeriesMetadata implements Comparable<TimeSeriesMetadata> {
	public TimeSeriesMetadata(long startTime, double duration, double sampleRate, double voltageConversion, 
			int minSampleValue, int maxSampleValue, String institution,	String acquisitionSystem,
			String channelName, String channelComments, String subjectId, String label, String revId) {
		this.startTime = startTime;
		this.duration = duration;
		this.startTime = startTime;
		this.voltageConversion = voltageConversion;
		this.revId = revId;
		this.minSampleValue = minSampleValue;
		this.maxSampleValue = maxSampleValue;
		this.institution = institution;
		this.acquisitionSystem = acquisitionSystem;
		this.channelName = channelName;
		this.channelComments = channelComments;
		this.subjectId = subjectId;
		this.sampleRate = sampleRate;

	}
	
	public TimeSeriesMetadata(TraceInfo t) {
		this.startTime = t.getStartTime();
		this.duration = t.getDuration();
		this.startTime = t.getStartTime();
		this.voltageConversion = t.getVoltageConversion();
		this.revId = t.getRevId();
		this.minSampleValue = t.getMinSampleValue();
		this.maxSampleValue = t.getMaxSampleValue();
		this.institution = t.getInstitution();
		this.acquisitionSystem = t.getAcquisitionSystem();
		this.channelName = t.getLabel();
		this.channelComments = t.getChannelComments();
		this.subjectId = t.getSubjectId();
		this.sampleRate = t.getSampleRate();

	}
	
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public double getSampleRate() {
		return sampleRate;
	}
	public void setSampleRate(double sampleRate) {
		this.sampleRate = sampleRate;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public double getVoltageConversion() {
		return voltageConversion;
	}
	public void setVoltageConversion(double voltageConversion) {
		this.voltageConversion = voltageConversion;
	}
	public String getRevId() {
		return revId;
	}
	public void setRevId(String revId) {
		this.revId = revId;
	}
	
	
	public int getMinSampleValue() {
		return minSampleValue;
	}

	public void setMinSampleValue(int minSampleValue) {
		this.minSampleValue = minSampleValue;
	}

	public int getMaxSampleValue() {
		return maxSampleValue;
	}

	public void setMaxSampleValue(int maxSampleValue) {
		this.maxSampleValue = maxSampleValue;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getAcquisitionSystem() {
		return acquisitionSystem;
	}

	public void setAcquisitionSystem(String acquisitionSystem) {
		this.acquisitionSystem = acquisitionSystem;
	}

	public String getFriendlyName() {
		return channelName;
	}

	public void setFriendlyName(String channelName) {
		this.channelName = channelName;
	}

	public String getChannelComments() {
		return channelComments;
	}

	public void setChannelComments(String channelComments) {
		this.channelComments = channelComments;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}


	double duration;
	double sampleRate;
	long startTime;
	double voltageConversion;
	String revId;

	int minSampleValue;
	int maxSampleValue;
	String institution;
	String acquisitionSystem;
	String channelName;
	String channelComments;
	String subjectId;
	
	@Override
	public int compareTo(TimeSeriesMetadata o) {
		return revId.compareTo(((TimeSeriesMetadata)o).revId);
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof TimeSeriesMetadata))
			return false;
		else
			return (compareTo((TimeSeriesMetadata)o) == 0);
	}
	
	@Override
	public int hashCode() {
		return revId.hashCode();
	}
}
