/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;

public class FavoriteMetadataEvent extends GwtEvent<FavoriteMetadataEvent.Handler> {
	private static final Type<FavoriteMetadataEvent.Handler> TYPE = new Type<FavoriteMetadataEvent.Handler>();
	
	private final GeneralMetadata snapshot;
	
	public FavoriteMetadataEvent(GeneralMetadata sr){
		this.snapshot = sr;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<FavoriteMetadataEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<FavoriteMetadataEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FavoriteMetadataEvent.Handler handler) {
		handler.onFavoriteResult(snapshot);
	}

	public interface Handler extends EventHandler {
		void onFavoriteResult(GeneralMetadata snapshot);
	}
}
