/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.braintrust.shared.ToolDto;

public class ToolMarkedAsFavoriteEvent extends GwtEvent<ToolMarkedAsFavoriteEvent.Handler> {
	private static final Type<ToolMarkedAsFavoriteEvent.Handler> TYPE = new Type<ToolMarkedAsFavoriteEvent.Handler>();
	
	ToolDto tool;
	boolean isFavorite;
	
	public ToolMarkedAsFavoriteEvent(ToolDto tool, boolean isFavorite) {
		this.tool = tool;
		this.isFavorite = isFavorite;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<ToolMarkedAsFavoriteEvent.Handler> getType() {
		return TYPE;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ToolMarkedAsFavoriteEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ToolMarkedAsFavoriteEvent.Handler handler) {
		if (isFavorite)
			handler.onToolMarkedAsFavorite(tool);
		else
			handler.onToolUnmarkedAsFavorite(tool);
	}

	public static interface Handler extends EventHandler {
		void onToolMarkedAsFavorite(ToolDto tool);
		void onToolUnmarkedAsFavorite(ToolDto tool);
	}
}
