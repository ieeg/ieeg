/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationModification;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.DataBundle;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.SnapshotAnnotationInfo;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;
import edu.upenn.cis.db.mefview.shared.TsEventType;

public interface SnapshotServicesAsync {

	void addAndRemoveAnnotations(SessionToken sessionID, String datasetID,
			Set<Annotation> annotationsToSave,
			Set<Annotation> annotationsToRemove,
			Set<String> removeLayers, 
			AsyncCallback<DataSnapshotIds> callback);
	

	void addDataSnapshotTimeSeries(SessionToken sessionID, String revID,
			Set<String> series, AsyncCallback<String> callback);

//	void getAnnotatedResults(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, double samplingPeriod, List<DisplayConfiguration> filter,
//			SignalProcessingStep processing,
//			AsyncCallback<AnnotationsAndJson> callback);
//
//	void getAnnotatedResults(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, int scaleFactor, SignalProcessingStep processing,
//			AsyncCallback<AnnotationsAndJson> callback);

	void getAnnotationInfo(SessionToken sessionID, String snapshot,
			double startPosition, double endPosition,
			AsyncCallback<SnapshotAnnotationInfo> callback);

	void getDataSnapshotAnnotations(SessionToken sessionID,
			String dataSnapshot, AsyncCallback<List<Annotation>> callback);

	void getEegStudyMetadata(SessionToken sessionID, String studyRevId,
			AsyncCallback<StudyMetadata> callback);

	void getExperimentMetadata(SessionToken sessionID, String experimentId,
			AsyncCallback<ExperimentMetadata> callback);

	/**
	 * Get the images associated with the study + trace
	 * @param trace
	 * @param typ
	 * @param study
	 * 
	 * 
	 */
	void getImageInfo(SessionToken sessionID, String studyPath, String traceRev, String trace, String typ, AsyncCallback<Set<ImageInfo>> callback);

	/**
	 * Get the traces associated with this image file
	 * @param study
	 * @param image
	 * 
	 * 
	 */
	void getTracesForImage(SessionToken sessionID, String imageWithPath, AsyncCallback<Set<TraceImageInfo>> callback);

	/**
	 * Get the images associated with this study
	 * @param studyPath Study directory
	 * @param typ Image type
	 * 
	 * 
	 */
	void getImagesForStudy(SessionToken sessionID, String studyPath, String typ, AsyncCallback<Set<ImageInfo>> callback);
	
	
	void getNextValidDataRegion(SessionToken sessionID, String dataSetID,
			double position, AsyncCallback<Double> callback);

	void getPermittedActionsForDataset(SessionToken sessionID, String dsId,
			AsyncCallback<Set<AllowedAction>> callback);

	void getPreviousValidDataRegion(SessionToken sessionID, String dataSetID,
			double position, AsyncCallback<Double> callback);

	void getRelatedAnalyses(SessionToken sessionID, String studyRevId,
			AsyncCallback<Set<DerivedSnapshot>> callback);

	void getResults(SessionToken sessionID, String contextID, String datasetID,
			List<String> traceids, double start, double width,
			double samplingPeriod, List<DisplayConfiguration> filter,
			SignalProcessingStep processing,
			AsyncCallback<TimeSeries[]> callback);

	void getResultsJSON(SessionToken sessionID, String contextID, String datasetID, List<INamedTimeSegment> traceids,
			double start, double width, double samplingPeriod, List<DisplayConfiguration> filter,
			SignalProcessingStep processing, AsyncCallback<String> callback);

//	void getResultsJSON(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, int scaleFactor, SignalProcessingStep processing,
//			AsyncCallback<String> callback);

	void getSearchResultForSnapshot(SessionToken sessionID,
			String dataSnapshotRevId, AsyncCallback<SearchResult> callback);

	void getSnapshotMetadata(SessionToken sessionID, String snapshotRevId,
			AsyncCallback<SnapshotContents> callback);

//	void getTraces(SessionToken sessionID, String datasetID,
//			AsyncCallback<List<TraceInfo>> callback);

	void removeAnnotationLayers(SessionToken sessionID,
	      String datasetID, Set<String> annotationLayersToRemove, AsyncCallback<Integer> callback);
	
	void removeAnnotations(SessionToken sessionID, String datasetID,
			Set<Annotation> annotations, AsyncCallback<String> callback);

	void removeDataSnapshotContents(SessionToken sessionID, String revID,
			Set<String> series, AsyncCallback<String> callback);

	void saveAnnotations(SessionToken sessionID, String datasetID,
			Set<Annotation> annotations, AsyncCallback<DataSnapshotIds> callback);

	void storeDataSnapshotXml(SessionToken sessionID, String dataSnapshotRevId,
			String clob, AsyncCallback<String> callback);

	void getImagesForStudy(SessionToken sessionID, String datasetID,
			AsyncCallback<Set<ImageInfo>> callback);

	void getPlaces(SessionToken sessionID, String dataSnapshotRevId,
			AsyncCallback<List<Place>> callback);

	void savePlaces(SessionToken sessionID, String dataSnapshotRevId,
			List<Place> places, AsyncCallback<Boolean> callback);

	void getSnapshotProvenance(SessionToken sessionID, String snapshot,
			AsyncCallback<List<ProvenanceLogEntry>> callback);

	void getDerivedSnapshots(SessionToken sessionID, String studyRevId,
			AsyncCallback<Set<SearchResult>> callback);


	void getRecordingObjects(SessionToken sessionToken, String datasetId,
			AsyncCallback<List<RecordingObject>> callback);


	void registerFiles(String tempId, String snapshotName,
			SessionToken sessionID, List<String> files,
			AsyncCallback<Void> callback);


	void getUuid(AsyncCallback<String> callback);


	void getContents(SessionToken sessionID, String datasetID, FileInfo file,
			AsyncCallback<String> asyncCallback);


	void saveContents(SessionToken sessionID, String datasetID, FileInfo file,
			String contents, AsyncCallback<Void> callback);


	void createRecordingObject(SessionToken sessionID, String datasetId,
			byte[] input, String filename, String contentType, boolean test,
			AsyncCallback<RecordingObject> callback);


	void attachFiles(String target, String snapshot, SessionToken sessionID,
			List<String> files,
			AsyncCallback<List<RecordingObject>> secFailureAsyncCallback);


	void getParentSnapshot(SessionToken sessionID, String studyRevId, AsyncCallback<String> callback);


	void getContentInfo(SessionToken sessionID, String dataSnapshot, AsyncCallback<Set<DataBundle>> callback);


	void getCaseMetadata(SessionToken sessionID, String studyRevId, AsyncCallback<CaseMetadata> callback);


//	void getEvents(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, TsEventType partitionOn,
//			AsyncCallback<Map<INamedTimeSegment, List<IDetection>>> callback);
//
//
//	void getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments,
//			TsEventType partitionOn, AsyncCallback<Map<TsEventType, INamedTimeSegment>> callback);
//
//
//	void getSegments(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments,
//			AsyncCallback<SortedSet<INamedTimeSegment>> callback);


	void getEventsAsSamples(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> segments, double start,
			double end, double period, AsyncCallback<Map<INamedTimeSegment, int[]>> callback);


	void getEventChannels(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> channels,
			AsyncCallback<List<INamedTimeSegment>> asyncCallback);


	void getMyDatasets(SessionToken sessionID, AsyncCallback<Set<DatasetPreview>> callback);


	void updateAnnotations(SessionToken sessionID, String datasetID, Collection<AnnotationModification> updates,
			long lastTime, AsyncCallback<Set<AnnotationModification>> callback);


	void getAnnotationsSince(SessionToken sessionID, String datasetID, long timestamp,
			AsyncCallback<Set<AnnotationModification>> callback);


	void isExistingDatasetWithName(SessionToken sessionID, String datasetName, AsyncCallback<Boolean> callback);


	@Deprecated
	void inviteToShare(SessionToken sessionID, Set<String> email, String note, DatasetPreview dataset,
			Set<Integer> roles, AsyncCallback<Set<String>> callback);


	void removeDataSnapshot(SessionToken sessionID, String revID, AsyncCallback<Boolean> callback);


//	void addDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels,
//			AsyncCallback<Boolean> callback);


	void addDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects,
			AsyncCallback<Boolean> callback);


	void removeDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels,
			AsyncCallback<Boolean> callback);


	void removeDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects,
			AsyncCallback<Boolean> callback);


	void inviteToShare(SessionToken sessionID, Set<String> email, String note, PresentableMetadata other,
			Set<Integer> roles, AsyncCallback<Set<String>> callback);


	void rejectShare(SessionToken sessionID, PresentableMetadata object, AsyncCallback<Void> callback);


	void acceptShare(SessionToken sessionID, BasicCollectionNode target, PresentableMetadata object,
			AsyncCallback<Void> callback);


	void setPublicAccess(SessionToken sessionID, String datasetID, Set<Integer> access, AsyncCallback<Void> callback);
}
