/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import com.google.common.collect.ImmutableSortedSet;
import com.google.gwt.core.client.JsArrayString;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.EEGDisplay;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SeriesData;
import edu.upenn.cis.db.mefview.shared.SnapshotAnnotationInfo;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class DataSnapshotModel {
	SnapshotAnnotationModel annotations = new SnapshotAnnotationModel(this);
	EEGModel vitals;
	
	List<DerivedSnapshot> derivedResults = new ArrayList<DerivedSnapshot>();
	String parentResult = null;
	List<AnnotationDefinition> annotationDefs;
		
	List<FileInfo> files;
	List<Long> timeOffsets;

	String snapshotID;
	String friendlyName;
	String userID;
	String creatorID;
	StudyMetadata metadata;
	SearchResult theSearchResult;
	
	boolean isEditableByMe = true;
	
	Long length = null;

	SnapshotAnnotationInfo annotationInfo;
	
	boolean isWritable = true;

	public static SortedSet<String> CASE_INSENSITIVE_DEFAULT_ANN_TYPES = ImmutableSortedSet
			.orderedBy(String.CASE_INSENSITIVE_ORDER)
			.addAll(EEGDisplay.DEFAULT_ANN_TYPE_TO_ABBREV.keySet()).build();
	
	public DataSnapshotModel(final String studyID,
			final String friendlyName,
			final String userID,
			final String creatorID,
			final EEGModel data
			) {
		
		this.snapshotID = studyID;
		this.friendlyName = friendlyName;
		this.userID = userID;
		this.creatorID = creatorID;
//		this.traces = Lists.newArrayList();
		this.vitals = data;
		this.files = new ArrayList<FileInfo>();
//		this.imageList = new ArrayList<FileInfo>();
	}
	
	public void setAnnotations(final SnapshotAnnotationModel annotations) {
		this.annotations = annotations;
	}
	
	public boolean isWritable() {
		return this.isWritable;
	}
	
	public void setWritable(boolean writ) {
		this.isWritable = writ;
	}
	
	public EEGModel getData() {
		return vitals;
	}
	
	public double getStartDate() {
		return getData().getStartDate();
	}
	
	public double getDurationMicros() {
		return getData().getDurationMicros();
	}
	
	public List<String> getChannelPaths() {
		return getData().getChannelPaths();
	}
	
	public List<AnnBlock> getAnnotationTimes() {
		return annotations.getAnnotationTimes();
	}
	
	public List<AnnBlock> getAnnotationTimes(final String annType) {
		return annotations.getAnnotationTimes(annType);
	}
	
	public int getNumChannels() {
		return getData().getNumChannels();
	}

	public void clearDeletedAnnotationList() {
		annotations.clearDeletedAnnotationList();
	}
	
	public Set<Annotation> getDeletedAnnotationList() {
		return annotations.getDeletedAnnotationList();
	}
	
	public Set<String> getAnnotatedChannels() {
		return annotations.getAnnotatedChannels();
	}
	
	public List<Annotation> getAllEnabledAnnotations() {
		return annotations.getAllEnabledAnnotations();
	}
	
//	public void setAnnotations(final List<Annotation> a) {
//		annotations.setAnnotations(a);
//	}
	
	public List<INamedTimeSegment> getTraceInfo(final List<String> channels) {
		return getData().getTraceInfo(channels);
	}
	
	public List<INamedTimeSegment> getTraceInfo() {
		return getData().getTraceList();
	}
	
	public String getRelevantAnnotations(double start, double end) {
		return annotations.getRelevantAnnotations(start, end, getAnnotationDefinitions());
	}
	
	public Annotation getMatchingAnnotation(String annName) {
//		System.err.println("At model " + friendlyName + " / " + studyID);
		return annotations.getMatchingAnnotation(annName);
	}
	
	public String getChannelRevId(int channel) {
		return getData().getChannelRevId(channel);
	}
	
	public String getChannelRevId(String channel) {
		return getData().getChannelRevId(channel);
	}

	public List<String> getChannelNames() {
		return getData().getChannels();
	}
	
	public List<String> getChannelRevIDs() {
		List<String> ret = new ArrayList<String>(vitals.getTraceList().size());
		
		for (INamedTimeSegment ti: vitals.getTraceList())
			ret.add(ti.getId());
		
		return ret;
	}

	public void setTraceJSLabels(final List<String> channelNames) {
		getData().setTraceLabels(getData().getTraceLabels(channelNames.size()));
	}
	
	public JsArrayString getTraceJSLabels() {
		return getData().getTraceLabels();
	}

	/**
	 * Returns the highest sample rate in Hz
	 * 
	 * @return
	 */
	public double getSampleRateHz() {
		double sampleRate = Double.MIN_VALUE;
		for (INamedTimeSegment t: getTraceInfo()) {
			if (t.getSampleRate() > sampleRate)
				sampleRate = t.getSampleRate();
		}
		return sampleRate;
	}

	public String getSnapshotID() {
		return snapshotID;
	}
	
	public void updateSnapshotID(String old, String n) {
		if (snapshotID.equals(old))
			snapshotID = n;
	}

	public void setSnapshotID(String studyID) {
		this.snapshotID = studyID;
	}

	public String getFriendlyName() {
		return friendlyName;
	}

	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Map<String,String> getAnnotationTypes() {
		final HashMap<String, String> ret = new HashMap<String, String>(
				EEGDisplay.DEFAULT_ANN_TYPE_TO_ABBREV);
		for (Annotation a : getAllEnabledAnnotations()) {
			final String type = a.getType();
			if (!CASE_INSENSITIVE_DEFAULT_ANN_TYPES.contains(type)) {
				ret.put(a.getType(), "");//a.getShortType());
			}
		}

		return ret;
	}
	
	public SnapshotAnnotationModel getAnnotationModel() {
		return annotations;
	}
	
	public StudyMetadata getStudyMetadata() {
		return metadata;
	}
	
	public void setStudyMetadata(StudyMetadata sm) {
		metadata = sm;
	}
	
	public void removeAllAnnotations(Collection<Annotation> annotationColl) {
		for (Annotation a: annotationColl)
			annotations.removeAnnotation(a);
	}
	
//	public SearchResult getSearchResult() {
//		return theSearchResult;
//	}
	
	public long getSnapshotStart() {
		long start = Long.MAX_VALUE;
		for (INamedTimeSegment ti: getTraceInfo()) {
			if (ti.getStartTime() < start)
				start = ti.getStartTime();
		}
		return start;
	}
	
	public long getSnapshotLength() {
		if (length != null)
			return length.longValue();
		
		long start = getSnapshotStart();
		long end = start;
		for (INamedTimeSegment ti: getTraceInfo()) {
			if ((double)(ti.getStartTime() + ti.getDuration()) > end)
				end = (long)(ti.getDuration() + ti.getStartTime());
		}
		length = new Long(end - start);
		
		return length.longValue();
	}

	public List<DerivedSnapshot> getDerivedResults() {
		return derivedResults;
	}
	
	public void setDerivedResults(Collection<DerivedSnapshot> res) {
		derivedResults.clear();
		derivedResults.addAll(res);
//		clientFactory.getEventBus().fireEvent(new ToolResultRefreshEvent());
	}
	
	public void addDerivedResult(DerivedSnapshot res) {
		derivedResults.add(res);
	}
	
	public void addDerivedResults(Collection<DerivedSnapshot> res) {
		derivedResults.addAll(res);
//		clientFactory.getEventBus().fireEvent(new ToolResultRefreshEvent());
	}

	public void removeDerivedResults(Collection<DerivedSnapshot> res) {
		for (DerivedSnapshot r: res)
			derivedResults.remove(r);
//		clientFactory.getEventBus().fireEvent(new ToolResultRefreshEvent());
	}

	public String getParentResultID() {
		return parentResult;
	}
	
	public void setParentResultID(String par) {
		parentResult = par;
	}

//	public List<FileInfo> getDocuments() {
//		return docList;
//	}
//	
//	public List<FileInfo> getDicoms() {
//		return imageList;
//	}
	
	public List<FileInfo> getFiles() {
		return files;
	}
	
	public void addFile(FileInfo file) {
		files.add(file);
	}
	
	public void clearRelated(final String snapshotID) {
		if (snapshotID.equals(this.snapshotID)) {
			files.clear();
			vitals.getTraces().clear();
		}		
	}

	public String getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(String creatorID) {
		this.creatorID = creatorID;
	}
	
	public List<SeriesData> getSeriesList() {
		List<SeriesData> ret = new ArrayList<SeriesData>();
		for (INamedTimeSegment t : vitals.getTraces()) {
			SeriesData sd = new SeriesData();
			sd.friendly = friendlyName;
			sd.studyId = snapshotID;
			sd.label = t.getLabel();
			sd.revId = t.getId();
			ret.add(sd);
		}

		return ret;
	}
	
//	public void addSeriesToList(SeriesData sd) {
//		listOfSeries.add(sd);
//	}
	
	public void addTrace(INamedTimeSegment trace) {
		getData().addTrace(trace);
	}
	
	public List<INamedTimeSegment> getTraces() {
		return vitals.getTraces();
	}

	public SnapshotAnnotationInfo getAnnotationInfo() {
		return annotationInfo;
	}

	public void setAnnotationInfo(SnapshotAnnotationInfo annotationInfo) {
		this.annotationInfo = annotationInfo;
	}

	public void setSearchResult(SearchResult sr) {
		theSearchResult = sr;
		setEditableByMe(sr.isEditable());
	}
	
	public SearchResult getSearchResult() {
		return theSearchResult;
	}

	public void setAnnotationDefinitions(List<AnnotationDefinition> list) {
		annotationDefs = list;
	}
	
	public List<AnnotationDefinition> getAnnotationDefinitions() {
		return annotationDefs;
	}
	
	public String toString() {
		return "(Snapshot " + friendlyName + " / " + snapshotID + ")";
	}

	public Set<AnnotationGroup<Annotation>> getDeletedAnnotationLayerList() {
		return annotations.getDeletedAnnotationLayerList();
	}

	public void setTraces(List<? extends INamedTimeSegment> traces2) {
		getData().addAllTraces(traces2);	
	}

	public void setData(EEGModel eegModel) {
		vitals = eegModel;
	}

	public void setTimeOffsets(List<Long> traceOffsets) {
		timeOffsets = traceOffsets;
	}
	
	public List<Long> getTimeOffsets() {
		return timeOffsets;
	}

	public boolean isEditableByMe() {
		return isEditableByMe;
	}

	public void setEditableByMe(boolean isEditableByMe) {
		this.isEditableByMe = isEditableByMe;
	}
	
	
}
