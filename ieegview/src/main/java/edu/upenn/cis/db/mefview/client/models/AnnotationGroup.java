/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class AnnotationGroup<T> {
  String groupName;
  ArrayList<AnnotationGroup<T>> subGroups = new ArrayList<AnnotationGroup<T>>();

  SortedSet<T> annotations = new TreeSet<T>();
  AnnotationScheme scheme;

  private boolean newItem = false;
  boolean isEnabled = true;

  public AnnotationGroup(){
    // For GWT
  }
  
  public AnnotationGroup(String groupName) {
    scheme = new AnnotationScheme();
    this.groupName = groupName;
  }

  public AnnotationGroup(AnnotationScheme s, String groupName, boolean newGroup) {
    scheme = s;
    this.groupName = groupName;
    setNew(newGroup);
  }

  public AnnotationGroup(AnnotationScheme s, String groupName,
      Collection<T> annotations) {
    scheme = s;
    this.groupName = groupName;
    this.addAnnotations(annotations);
  }

  public void setEnabled(boolean sel) {
    isEnabled = sel;
  }

  public boolean isEnabled() {
    return isEnabled;
  }

  public String getName() {
    return groupName;
  }

  public void setName(String newName) {
    groupName = newName;
  }

  public boolean isAnnotationActive(T a) {
    if (isEnabled) {
      if (annotations.contains(a))
        return true;

      for (AnnotationGroup<T> sub: subGroups) {
        if (sub.isAnnotationActive(a))
          return true;
      }
    }
    return false;
  }

  public void addAnnotation(T aNew) {
	annotations.add(aNew);
  }
  
  public void addAnnotations(Collection<T> newSet) {
	annotations.addAll(newSet);
  }

  public void replaceStyle(AnnotationScheme oldStyle, AnnotationScheme style){
    if (scheme == oldStyle){
      scheme = style;
    }

    for (AnnotationGroup<T> sub: subGroups) {
      if (sub.scheme == oldStyle){
        sub.scheme = style;
      }
    }

  }

  public void replaceAnnotation(T aOld, T aNew) {
    if (annotations.contains(aOld)) {
      annotations.remove(aOld);
      addAnnotation(aNew);
    }

    for (AnnotationGroup<T> sub: subGroups) {
      sub.replaceAnnotation(aOld, aNew);
    }
  }

  public void removeAnnotation(T aOld) {
    if (annotations.contains(aOld)) {
      annotations.remove(aOld);
    }

    for (AnnotationGroup<T> sub: subGroups) {
      sub.removeAnnotation(aOld);
    }
  }

  public AnnotationGroup<T> getGroup(String name) {
    if (groupName.equals(name))
      return this;

    for (AnnotationGroup<T> sg : subGroups) {
      AnnotationGroup<T> ret = sg.getGroup(name);
      if (ret != null)
        return ret;
    }

    return null;
  }

  public void removeSubGroup(AnnotationGroup<T> subGroup) {
    subGroups.remove(subGroup);
    annotations.remove(subGroup);
  }

  public void removeSubGroup(String name) {
    for (AnnotationGroup<T> ann: subGroups) {
      if (ann.getName().equals(name)) {
        subGroups.remove(ann);
        break;
      }
    }
  }

  public ArrayList<AnnotationGroup<T>> getSubGroups() {
    return subGroups;
  }


  public void setSubGroups(ArrayList<AnnotationGroup<T>> subGroups) {
    this.subGroups = subGroups;
  }

  public void addSubGroup(AnnotationGroup<T> subGroup) {
    subGroups.add(subGroup);
  }

  public void addAll(Collection<T> partialGroup) {
    annotations.addAll(partialGroup);
  }

  public SortedSet<T> getAnnotations() {
    return annotations;
  }

  public ArrayList<T> getAllEnabledAnnotations() {
    ArrayList<T> ret = new ArrayList<T>();
    if (isEnabled()) {
      for (AnnotationGroup<T> ag: subGroups) {
        if (ag.isEnabled()) {
          //				GWT.log("Annotation group " + ag.getName() + " INCLUDED");
          ret.addAll(ag.getAllEnabledAnnotations());
        } 
        //			else
        //				GWT.log("Annotation group " + ag.getName() + " EXCLUDED");
      }
      ret.addAll(annotations);
    }
    return ret;
  }

  public HashSet<T> getAnnotationSet() {
    HashSet<T> ret = new HashSet<T>();
    ret.addAll(annotations);
    return ret;
  }

  public void setAnnotations(Collection<T> annotations) {
	  if (this.annotations == null)
		  this.annotations = new TreeSet<T>();

	  this.annotations.addAll(annotations);
  }

  public AnnotationScheme getScheme() {
    return scheme;
  }

  public void setScheme(AnnotationScheme scheme) {
    this.scheme = scheme;
  }

  /**
   * Return color for annotation; if no color for annotation,
   * return color for annotation group scheme.
   * 
   * @param ann
   * @return
   */
  public String getColorFor(Annotation ann) {
    if (ann.getColor() != null && !ann.getColor().isEmpty())
      return ann.getColor();
    else {
      // Check current group
      for (T mem: annotations) {
        String str = "";
        if (((Annotation)mem) == ann)
          str = scheme.getColor();
        if (!str.isEmpty())
          return str;
      }
      // check subgroups
      for (AnnotationGroup<T> sub: subGroups) {
        String str = sub.getColorFor(ann);
        if (!str.isEmpty())
          return str;
      }
      return "";
    }
  }

  public AnnotationGroup<T> getGroupFor(Annotation ann) {
    if (annotations.contains(ann))
      return this;

    for (AnnotationGroup<T> sub: subGroups) {
      AnnotationGroup<T> g = sub.getGroupFor(ann);
      if ( g != null)
        return g;
    }
    return null;
  }

  public static final ProvidesKey<AnnotationGroup<Annotation>> KEY_PROVIDER = new ProvidesKey<AnnotationGroup<Annotation>>() {

    public Object getKey(AnnotationGroup<Annotation> item) {
      return (item == null) ? null : item.groupName;
    }

  };
  public static final TextColumn<AnnotationGroup<Annotation>> annColumn = new TextColumn<AnnotationGroup<Annotation>> () {
    @Override
    public String getValue(AnnotationGroup<Annotation> object) {
      return object.groupName;
    }
  };
  public static final TextColumn<AnnotationGroup<Annotation>> annCountColumn = new TextColumn<AnnotationGroup<Annotation>> () {
    @Override
    public String getValue(AnnotationGroup<Annotation> object) {
      if (object.annotations != null)
        return String.valueOf(object.annotations.size());
      else
        return "0";
    }
  };

  public static final TextColumn<AnnotationGroup<Annotation>> annScheme = new TextColumn<AnnotationGroup<Annotation>> () {
    @Override
    public String getValue(AnnotationGroup<Annotation> object) {
      return object.scheme.getName();
    }
  };

  public void updateNames(String oldName, String newName) {
    if (groupName == oldName){
      groupName = newName;
    }

    for (AnnotationGroup<T> sub: subGroups) {
      if (sub.groupName == oldName){
        sub.groupName = newName;
      }
    }
    
  }
  
  public void setNew(boolean newItem) {
    this.newItem = newItem;
  }

  public boolean isNew() {
    return newItem;
  }

}
