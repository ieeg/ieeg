package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface IUserProfilePluginFactory {
	public IUserProfilePanelPlugin getPlugin(ServerConfiguration config,
			SessionModel sessionModel, UserInfo info);
}
