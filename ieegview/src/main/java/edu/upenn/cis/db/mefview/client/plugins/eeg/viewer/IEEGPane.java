package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.List;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;

public interface IEEGPane {
	public interface ParentPane {
		public void showMessage(String message);
		public void clearMessage();
	};
	
    void refreshAnnotationsOnGraph(boolean refresh);
    void refresh();
    void resetFocus();
    
	IEEGViewerPanel getPanel();
	
	double getPosition();
	double getPageWidth();
	List<DisplayConfiguration> getFilters();
	DataSnapshotModel getDataSnapshotState();
	EEGMontage getCurMontage();
	List<Boolean> getCurIsVisible();
	void showThese(boolean[] sel);
}
