/*
 * Copyright 2015 IEEG.org 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;


import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public class EegMontageAddedEvent extends GwtEvent<EegMontageAddedEvent.Handler> {
    private static final Type<EegMontageAddedEvent.Handler> TYPE = new Type<EegMontageAddedEvent.Handler>();
    
    private final EEGMontage montage;
    IEEGViewerPanel window;
    
    public EegMontageAddedEvent(IEEGViewerPanel window, EEGMontage montage){
        this.montage = montage;
        this.window = window;
    }
    
    public static com.google.gwt.event.shared.GwtEvent.Type<EegMontageAddedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<EegMontageAddedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EegMontageAddedEvent.Handler handler) {
        handler.onMontageAdded(window, montage);
    }

    public static interface Handler extends EventHandler {
        void onMontageAdded(IEEGViewerPanel w, EEGMontage montage);
    }
}