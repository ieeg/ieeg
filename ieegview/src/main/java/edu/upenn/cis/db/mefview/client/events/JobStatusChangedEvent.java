/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class JobStatusChangedEvent extends GwtEvent<JobStatusChangedEvent.Handler> {
	private static final Type<JobStatusChangedEvent.Handler> TYPE = new Type<JobStatusChangedEvent.Handler>();
	
	private final String jobId;
	
	public JobStatusChangedEvent(String job){
		this.jobId = job;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<JobStatusChangedEvent.Handler> getType() {
		return TYPE;
	}
	
	/**
	 * ID of job that changed, null if all jobs
	 * 
	 * @return
	 */
	public String getJobChanged() {
		return jobId;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<JobStatusChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(JobStatusChangedEvent.Handler handler) {
		handler.onJobStatusChanged(jobId);
	}

	public static interface Handler extends EventHandler {
		void onJobStatusChanged(String job);
	}
}
