package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class MismatchDialogBox extends DialogBox {
	public MismatchDialogBox() {
		super(true, true);
		
		setTitle("Error with new passwords");
		add(new Label("New passwords do not match."));
	}
}
