/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class RequestShowPermissionsEvent extends GwtEvent<RequestShowPermissionsEvent.Handler>{
	private static final Type<RequestShowPermissionsEvent.Handler> TYPE = new Type<RequestShowPermissionsEvent.Handler>();
	
	private final String userId;
	private SearchResult snapshot = null;
	private DerivedSnapshot toolResult = null;
	
	public RequestShowPermissionsEvent(final String userId, final SearchResult snapshot) {
		this.userId = userId;
		this.snapshot = snapshot;
	}
	
	public RequestShowPermissionsEvent(final String userId, final DerivedSnapshot snapshot) {
		this.userId = userId;
		this.toolResult = snapshot;
	}

	public static com.google.gwt.event.shared.GwtEvent.Type<RequestShowPermissionsEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<RequestShowPermissionsEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RequestShowPermissionsEvent.Handler handler) {
		if (snapshot != null)
			handler.onPermissionsRequested(userId, snapshot);
		else
			handler.onPermissionsRequested(userId, toolResult);
	}

	public static interface Handler extends EventHandler {
		void onPermissionsRequested(String userId, SearchResult snapshot);
		void onPermissionsRequested(String userId, DerivedSnapshot result);
	}
}
