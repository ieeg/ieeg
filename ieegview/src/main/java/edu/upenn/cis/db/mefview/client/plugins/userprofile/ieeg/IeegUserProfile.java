package edu.upenn.cis.db.mefview.client.plugins.userprofile.ieeg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount.CreateAccountPresenter;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.BaseUserProfilePanelPlugin;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.ButtonEntry;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class IeegUserProfile extends BaseUserProfilePanelPlugin {
	public static final String NAME = "IEEG.org";

	Button docButton = new Button(); 
	Button dwnToolButton = new Button();
	Button reqButton = new Button();
	
	public IeegUserProfile(SessionModel model) {
		super(model);
	}

	@Override
	public FlowPanel getHighlightedButtons() {
	      FlowPanel startButtons = new FlowPanel();
	      
	      reqButton.setHTML("Sign up");
	      
	      reqButton.setPixelSize(100, 50);
	      reqButton.getElement().getStyle().setFloat(Float.LEFT);
	      reqButton.setStylePrimaryName("buttonHolder");
	      reqButton.setStyleName("buttonGreen", true);
	      reqButton.addStyleName("startButtons");
	      reqButton.addClickHandler(new ClickHandler() {

	        @Override
	        public void onClick(ClickEvent event) {
//	          Window.open("https://main.ieeg.org/?q=user/register","IEEG-Portal Create user account",null);
				OpenDisplayPanel act = new OpenDisplayPanel(
						null,
						false,
						CreateAccountPresenter.NAME,
						null);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
	        }
	      });
	      
	      startButtons.add(reqButton);

	      dwnToolButton.setHTML("Download Toolbox");
	      dwnToolButton.setPixelSize(100, 50);
	      dwnToolButton.getElement().getStyle().setFloat(Float.LEFT);
	      dwnToolButton.setStylePrimaryName("buttonHolder");
	      dwnToolButton.setStyleName("buttonBlue", true);
	      dwnToolButton.addStyleName("startButtons");
	      dwnToolButton.addClickHandler(new ClickHandler() {

	          @Override
	          public void onClick(ClickEvent event) {
	            Window.open("https://bitbucket.org/ieeg/ieeg/wiki/Downloads.md",
	            		getSessionModel().getServerConfiguration().getServerName() + " Toolbox Download",null);
	          }
	        });
	      startButtons.add(dwnToolButton);
	      
	      docButton.setHTML("Read Documentation");
	      docButton.setPixelSize(100, 50);
	      docButton.getElement().getStyle().setFloat(Float.LEFT);
	      docButton.setStylePrimaryName("buttonHolder");
	      docButton.setStyleName("buttonRed", true);
	      docButton.addStyleName("startButtons");
		  docButton.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					OpenDisplayPanel act = new OpenDisplayPanel(
							new WebLink(
									"",
									"IEEG-Documentation",
									"https://main.ieeg.org/sites/default/files/IEEGDocumentation.pdf"),
							false,
							WebPanel.NAME,
							null);
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
				}
			});
	      startButtons.add(docButton);
	      
	      return startButtons;
	}

	@Override
	public List<Widget> getWidgets() {
		List<Widget> ret = new ArrayList<Widget>();

		ret.add(reqButton);
		ret.add(docButton);
		ret.add(dwnToolButton);
		
		return ret;
	}

	@Override
	public FlowPanel getLogos() {
	      FlowPanel logoBanner = new FlowPanel();
	      logoBanner.setStylePrimaryName("ieeg-logoBannerHolder");

	      Grid g = new Grid(1,3);
	      g.addStyleName("ieeg-portal-collabTable");
	      Image penn = new Image(ResourceFactory.getPenn().getSafeUri());
	      g.setWidget(0, 0, penn);//"images/penn.png"));//, 0, 0, 267, 88));
	      Image mayo = new Image(ResourceFactory.getMayo().getSafeUri());
	      g.setWidget(0, 1, mayo);//"images/mayo.png"));//, 0, 0, 372, 88));
	      Image ninds = new Image(ResourceFactory.getNinds().getSafeUri());
	      g.setWidget(0, 2, ninds);//"images/ninds.png"));//, 0, 0, 407, 88));

	      for (int j = 0; j <= 0; j++)
	          for (int k = 0; k <= 2; k++)
	              if (g.getWidget(j, k) != null)
	                  g.getWidget(j, k).addStyleName("collaborator");
	      logoBanner.add(g);
	      
	      return logoBanner;
	}
	
	@Override
	protected List<String> getBannerList() {
		  return Arrays.asList(new String[] {
				  "sloganBanner.png",
				  "brainMapperBanner.png",
				  "matlabBanner.png",
				  "portalEEGImage2.png"
			  });
		  
	}

	@Override
  public List<ButtonEntry> getButtons() {
	  return Arrays.asList(new ButtonEntry[] {
			  new ButtonEntry(
					  "BrainMapper.png",
					  "Coregistration", 
					  "https://s3.amazonaws.com/org-ieeg-html-pages/CoregistrationApp.html",
					  "Coregistering CT and MRI Images",
					  false),
			  new ButtonEntry(
					  "Github.png",
					  getSessionModel().getServerConfiguration().getServerName() + " Tools on GitHub", 
					  "https://github.com/ieeg-portal/",
					  "Repository of " + getSessionModel().getServerConfiguration().getServerName() + " tools",
					  true),
			  new ButtonEntry(
					  "Matlab.png",
					  "Matlab Tools", 
					  "https://bitbucket.org/ieeg/ieeg/wiki/Downloads.md",
					  "Toolbox for accessing data in Matlab",
					  true),
			  new ButtonEntry(
					  "Loni.png",
					  "LONI IDA", 
					  "https://ida.loni.usc.edu/",
					  "LONI images",
					  false)
	  });
  }
}
