/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;

import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;

public class SnapshotToolbar extends FlowPanel {
  
	Button openSnapshot;
    Button snapDetails;
    
	Button showEeg;
	Button showImages;
	Button documents;
	Button dicoms;
	Button bookmark;
//	Button addToProject;
	
	Button test;
	
	public SnapshotToolbar() {
		this(null, null, null, null, null);//, null);
	}
	
	public SnapshotToolbar(ClickHandler viewEegHandler, ClickHandler viewImageHandler,
			ClickHandler documentHandler, ClickHandler dicomHandler,
			ClickHandler testHandler) {
//		super(1,13);
	    setStyleName("IeegTabToolbar");		
				
//		snapDetails = new Button("Show Details");
//		add(snapDetails);
//        if (viewEegHandler != null)
//          snapDetails.addClickHandler(viewEegHandler);
//        else
//          snapDetails.setEnabled(false);

		showEeg = new Button("EEG");
		add(showEeg);
		if (viewEegHandler != null)
			showEeg.addClickHandler(viewEegHandler);
		else
			showEeg.setEnabled(false);

		showImages = new Button("Images");
		add(showImages);
		if (viewImageHandler != null)
			showImages.addClickHandler(viewImageHandler);
		else
			showImages.setEnabled(false);

		documents = new Button("Documentation");
		add(documents);
		if (documentHandler != null)
			documents.addClickHandler(documentHandler);
		else
			documents.setEnabled(false);

		dicoms = new Button("DICOMs");
		add(dicoms);
		if (dicomHandler != null)
			dicoms.addClickHandler(dicomHandler);
		else
			dicoms.setEnabled(false);

		openSnapshot = new Button("Other Dataset");
		add(openSnapshot);
		
		test = new Button("Test feature");
		add(test);
		if (testHandler != null)
			test.addClickHandler(testHandler);

//		bookmark = new Button("Bookmark");
//		if (bookmarkHandler != null)
//			bookmark.addClickHandler(bookmarkHandler);
//		else
//			bookmark.setEnabled(false);
	}
	
	public void setTestHandler(ClickHandler testHandler) {
		test.addClickHandler(testHandler);
	}

	public void setOpenHandler(ClickHandler openHandler) {
		openSnapshot.addClickHandler(openHandler);
	}

	public void setBookmarkHandler(ClickHandler bookmarkHandler) {
//		if (bookmarkHandler != null) {
//			bookmark.addClickHandler(bookmarkHandler);
//		} else
//			bookmark.setEnabled(false);
	}
	
	public void setEegHandler(ClickHandler viewEegHandler) {
		if (viewEegHandler != null) {
			showEeg.addClickHandler(viewEegHandler);
//			showEeg.setEnabled(true);
		} else
			showEeg.setEnabled(false);
	}
	
//	public void setProjectHandler(ClickHandler openProjectHandler) {
//		if (openProjectHandler != null) {
//			addToProject.addClickHandler(openProjectHandler);
////			showEeg.setEnabled(true);
//		} else
//			addToProject.setEnabled(false);
//	}
	
	public void setImageHandler(ClickHandler viewImageHandler) {
		if (viewImageHandler != null) {
			showImages.addClickHandler(viewImageHandler);
//			showImages.setEnabled(true);
		} else
			showImages.setEnabled(false);
	}

	public void setDicomHandler(ClickHandler dicomHandler) {
		if (dicomHandler != null) {
			dicoms.addClickHandler(dicomHandler);
//			dicoms.setEnabled(true);
		} else
			dicoms.setEnabled(false);
	}
	
	public void setDocumentHandler(ClickHandler documentHandler) {
		if (documentHandler != null) {
			documents.addClickHandler(documentHandler);
//			documents.setEnabled(true);
		} else
			documents.setEnabled(false);
	}
	
//	public void enableProjectHandler(boolean enabled) {
//		addToProject.setEnabled(enabled);
//	}
	
	public void enableImageHandler(boolean enabled) {
		showImages.setEnabled(enabled);
	}
	
	public void enableEegHandler(boolean enabled) {
		showEeg.setEnabled(enabled);
	}
	
	public void enableBookmarkHandler(boolean enabled) {
//		bookmark.setEnabled(enabled);
	}

	public void enableDocumentHandler(boolean enabled) {
		documents.setEnabled(enabled);
	}

	public void enableDicomHandler(boolean enabled) {
		dicoms.setEnabled(enabled);
	}

	public Button getSnapDetailsButton() {
      return snapDetails;
    }
	
	public Button getEegButton() {
		return showEeg;
	}

	public Button getImageButton() {
		return showImages;
	}

	public Button getDocumentButton() {
		return documents;
	}
	public Button getDicomButton() {
		return dicoms;
	}
//	public Button getProjectButton() {
//		return addToProject;
//	}
	
//	public Button getBookmarkButton() {
//		return bookmark;
//	}
}
