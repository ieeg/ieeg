/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.Date;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.messages.SnapshotDataMessage;
import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;

public class SnapshotReceivedEvent extends GwtEvent<SnapshotReceivedEvent.Handler> {
	private static final Type<SnapshotReceivedEvent.Handler> TYPE = new Type<SnapshotReceivedEvent.Handler>();
	
	private final SnapshotDataMessage action;
	private final RequestSnapshotData request;
	private long time;
	private boolean failed = false;
	
	public SnapshotReceivedEvent(SnapshotDataMessage action){
		this.action = action;
		this.request = null;
		time = new Date().getTime();
	}
	
	public SnapshotReceivedEvent(RequestSnapshotData action){
		this.action = null;
		this.request = action;
		time = new Date().getTime();
		failed = true;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public static com.google.gwt.event.shared.GwtEvent.Type<SnapshotReceivedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SnapshotReceivedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SnapshotReceivedEvent.Handler handler) {
		if (request == null)
			handler.onSnapshotReceived(action);
		else
			handler.onSnapshotFailed(request);
	}

	public interface Handler extends EventHandler {
		void onSnapshotReceived(SnapshotDataMessage action);
		void onSnapshotFailed(RequestSnapshotData request);
	}
}
