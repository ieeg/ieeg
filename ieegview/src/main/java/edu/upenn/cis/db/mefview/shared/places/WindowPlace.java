/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Optional;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class WindowPlace extends Place implements IsSerializable, JsonTyped {
	
	Optional<String> snapshot = Optional.absent();
	
	WindowPlace() {}
	
	WindowPlace(String snapshot) {
		this.snapshot = Optional.fromNullable(snapshot); 
	}
	
	@JsonIgnore
	public abstract String getType();
	
	public String getSnapshotID() {
		return snapshot.get();
	}
	
	@JsonIgnore
	public Optional<String> getSnapshotIfExists() {
		return snapshot;
	}
	
	
	public void setSnapshotID(String id) {
		snapshot = Optional.fromNullable(id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((snapshot == null) ? 0 : snapshot.hashCode());
		return result;
	}

	//Needs to be overridden in your Place if require more than dataset's id to distinguish instances.
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof WindowPlace)) {
			return false;
		}
		WindowPlace other = (WindowPlace) obj;
		if (snapshot == null) {
			if (other.snapshot != null) {
				return false;
			}
		} else if (!snapshot.equals(other.snapshot)) {
			return false;
		}
		return true;
	}
	
	
	
//	public abstract String getSerializableForm();
	
//	public abstract WindowPlace getFromSerialized(final String serial);
}
