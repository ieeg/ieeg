package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.List;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public interface IFilterDialog  {

	public void init(final JsArrayString channels, 
			final List<DisplayConfiguration> parms, 
			final List<INamedTimeSegment> traces,
			final EEGMontage montage,
			final IEEGPane display, final ClientFactory factory,
			final Command doAfter);
	
	public void show();
	
	public void center();
	
	public void setModal(Boolean value);

}


