package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class LoadSnapshotDetailsCommand implements Command {
	public interface ISnapshotLoader {
		public void selectSnapshot(PresentableMetadata md);
	}
	
	Set<String> snapshotsList = new HashSet<String>();
	ClientFactory clientFactory;
	ISnapshotLoader loader;
	
	public LoadSnapshotDetailsCommand(ClientFactory cf, ISnapshotLoader loader, 
			Collection<String> snapshots) {
		clientFactory = cf;
		snapshotsList.addAll(snapshots);
		this.loader = loader;
	}
	
	@Override
	public void execute() {
        clientFactory.getSearchServices().getSearchResultsForSnapshots(
            clientFactory.getSessionModel().getSessionID(), snapshotsList,
            new AsyncCallback<Set<SearchResult>>() {

              @Override
              public void onFailure(Throwable caught) {
                GWT.log("Failure " + caught);
                // Ignore; this was a best-effort
              }

              @Override
              public void onSuccess(Set<SearchResult> result) {
//                GWT.log("Got back " + result);
                for (SearchResult sr : result)
                	loader.selectSnapshot(sr);
              }

            });
	}
}
