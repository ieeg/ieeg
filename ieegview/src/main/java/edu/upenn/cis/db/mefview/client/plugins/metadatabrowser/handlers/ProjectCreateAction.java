/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers;

import java.util.HashSet;

import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.ICreateProject;
import edu.upenn.cis.db.mefview.client.events.CreateProjectEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectCreatedEvent;
import edu.upenn.cis.db.mefview.shared.Project;

public class ProjectCreateAction implements CreateProjectEvent.Handler {
    ICreateProject proj;
	
	public ProjectCreateAction(final ClientFactory clientFactory) {
		proj = DialogFactory.getCreateProjectDialog(new DialogInitializer<ICreateProject>() {

			@Override
			public void init(ICreateProject objectBeingInited) {
				objectBeingInited.init(new ICreateProject.Handler() {

					@Override
					public void create(String project) {
						final Project newProject = new Project(null, 
								project,
								new HashSet<String>(), 
								new HashSet<String>(), 
								false);
						
						newProject.getAdmins().add(clientFactory.getSessionModel().getUserInfo());
						newProject.getTeam().add(clientFactory.getSessionModel().getUserInfo());
						
						clientFactory.getProjectServices().createProject(clientFactory.getSessionModel().getSessionID(), 
								newProject, 
								new SecFailureAsyncCallback.SecAlertCallback<Project>(clientFactory) {

									@Override
									public void onSuccess(Project arg0) {
//										newProject.setPubId(arg0);
										IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
												ProjectCreatedEvent(arg0));
									}
							
						});
					}
					
				});
			}

			@Override
			public void reinit(ICreateProject objectBeingInited) {
				
			}
			
		});
	}



	@Override
	public void onCreateProject() {
//	      proj.center();
//	      proj.show();
		proj.open();
		
		Timer t = new Timer() {

			@Override
			public void run() {
				proj.focus();
			}
			
		};
		
		t.schedule(500);
	}

}
