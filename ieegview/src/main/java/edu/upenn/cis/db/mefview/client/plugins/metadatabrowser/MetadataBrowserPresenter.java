/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.web.bindery.event.shared.Event.Type;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SnapshotServicesAsync;
import edu.upenn.cis.db.mefview.client.actions.ClickAction;
import edu.upenn.cis.db.mefview.client.actions.IPluginAction;
import edu.upenn.cis.db.mefview.client.clickhandlers.FavoriteToggleHandler;
import edu.upenn.cis.db.mefview.client.clickhandlers.OpenSnapshotDialogClickHandler;
import edu.upenn.cis.db.mefview.client.clickhandlers.SearchDialogClickHandler;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.BookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.ClosedSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.CreateProjectEvent;
import edu.upenn.cis.db.mefview.client.events.MetadataContentsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectLoadedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.UnbookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.DoWorkAction;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.ProcessDerivedResultsAction;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.ProcessProjectsAction;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.ProjectCreateAction;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.SnapshotCloseHandlerAction;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers.SnapshotOpenHandlerAction;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.Project.STATE;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class MetadataBrowserPresenter extends BasicPresenter implements SearchResultsUpdatedEvent.Handler,
BookmarkSnapshotEvent.Handler, UnbookmarkSnapshotEvent.Handler, ProjectCreatedEvent.Handler, 
ProjectUpdatedEvent.Handler, MetadataContentsUpdatedEvent.Handler {
	public interface Display extends BasicPresenter.Display {
		public void setHistoryAndModel(MetadataModel history);

		public void bindToolbar();
		
		public void addSnapshotSelectionChangedHandler(SelectionChangeEvent.Handler handler);
		public void addSearchHandler(ClickHandler handler, Presenter pres);
		public void addOpenHandler(ClickHandler handler, Presenter pres);
		public void addFavoriteClickHandler(PagedTable.StarToggleHandler<PresentableMetadata> handler);

		public Set<? extends PresentableMetadata> getSelected();
		public List<? extends PresentableMetadata> getResults();
		public void addResult(PresentableMetadata sr);
		public void removeResult(PresentableMetadata sr);
		public void removeResult(int inx);
		public void removeResults(Collection<? extends PresentableMetadata> sr);
		public void addResults(Collection<? extends PresentableMetadata> sr);
		public PresentableMetadata getSelectedResult();
		public void setSelectedResult(PresentableMetadata sr, boolean sel);
		public boolean isSelectedResult(PresentableMetadata sr);
		
		public boolean haveDerivedResultsFor(PresentableMetadata sr);
		public void addSearchResults(List<? extends PresentableMetadata> results, boolean replace);
		
		public void refreshSnapshots();
		
		public void setExperimentDetails(String snapshotId, String friendlyName, ExperimentMetadata em);
		public void setStudyDetails(String snapshotId, String friendlyName, StudyMetadata em);

		public void setDerivedResults(PresentableMetadata sr, Collection<? extends PresentableMetadata> subresults);
		public void setFavoriteResults(Collection<? extends PresentableMetadata> sr);
		
		public void addProject(PresentableMetadata p);
		public List<? extends PresentableMetadata> getProjects();
		public void addProject(int pos, PresentableMetadata p);
		public void removeProject(int pos);
		
		public CollectionNode getFavoritesNode();
		
		public void enableSearch(boolean en);
		
		public void refreshItem(PresentableMetadata item);
		public MetadataProviderBase getMetadataProviderFor(PresentableMetadata item);

		public void updateChildPane();
		public void updateChildPane(SerializableMetadata md);

		Set<ChildPane> getChildPanes();
	}
	
	static class LRUCache<K,V> extends LinkedHashMap<K,V> {
		int capacity;
		
		public LRUCache(int size) {
			super(size, 0.75F, true);
			capacity = size;
		}
		
		@Override
		public boolean removeEldestEntry(Map.Entry eldest) {
			return size() > capacity;
		}
	}
	
	/**
	 * How often to refresh the study metadata, in msecd
	 */
	public static final long UPDATE_RATE = 10000;
	
	AppModel parent;
	
	MetadataModel model;
	
//	Stack<HandlerRegistration> handlers = new Stack<HandlerRegistration>();
	List<EventHandler> eventHandlers = new ArrayList<EventHandler>();
	
	SnapshotServicesAsync service;
	
	public static final String NAME = "metadata-browser";
	public static final Set<String> TYPES = Sets.newHashSet();
	static final MetadataBrowserPresenter seed = new MetadataBrowserPresenter();
	
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public MetadataBrowserPresenter() { super(null, NAME, TYPES); }
	
	public MetadataBrowserPresenter( 
			final SnapshotServicesAsync service,
			final Button search,
			final AppModel parent,
			final ClientFactory factory) {
		
		super(factory, NAME, TYPES);
		
		model = parent.getSnapshotBrowserModel();
		this.parent = parent;
		this.service = service;

	}
	
	public Display getDisplay() {
		return (Display)display;
	}
	
	public void closeStudy(final String study) {
		model.updateRecentStudies(study);
	}
	
	public void openStudy(final String study) {
		model.updateOpenStudies(study);
	}
	
	public void updateRevId(String oldRevId, String newRevId) {
		for (GeneralMetadata sr: getDisplay().getResults()) {

			if (sr instanceof PresentableMetadata)
				((PresentableMetadata)sr).updateRevId(oldRevId, newRevId);
		}
		
	}

	/**
	 * Record the subresults for a given search result
	 * @param sr
	 * @param subresults
	 */
	public void setResultsFor(PresentableMetadata sr, Collection<? extends PresentableMetadata> subresults) {
		getDisplay().setDerivedResults(sr, subresults);
	}

	@Override
	public void onSearchResultsChanged(String search, List<PresentableMetadata> newResults,
			List<PresentableMetadata> deletedResults, boolean replace) {
		
		getDisplay().log(3, "MetadataBrowserPresenter received search results changed");
		
		if (deletedResults != null)
			for (PresentableMetadata sr: deletedResults)
				getDisplay().removeResult(sr);
		
		getDisplay().addSearchResults(newResults, replace);
		getDisplay().enableSearch(true);
	}

	
	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		return new MetadataBrowserPresenter(clientFactory.getSnapshotServices(), null,
				controller, clientFactory);
	}

	/**
	 * Handle a click on an item
	 * 
	 * @param event
	 * @param cache
	 */
	void updateSelection(SelectionChangeEvent event, LRUCache<PresentableMetadata,Long> cache) {
		getDisplay().log(3, "MetadataBrowserPresenter received dataset selection changed");

		for (PresentableMetadata item: getDisplay().getSelected()) {
			if (item instanceof SearchResult) {
				Long time = cache.get(item);
				if (time == null || System.currentTimeMillis() - time > UPDATE_RATE) {

					getDisplay().log(4, "MetadataBrowserPresenter requesting details on " + item.getFriendlyName());
					
					updateDetailsPane((SearchResult)item);
					cache.put(item, System.currentTimeMillis());

					break;
				}
			} else if (item instanceof Project) {
				final Project project = (Project) item;
				if (project.getState() == STATE.UNLOADED) {
					project.setState(STATE.LOADING);
					getDisplay()
					.updateChildPane();
					getClientFactory()
							.getProjectServices()
							.getProjectSearchResultsForUser(
									getSessionModel().getSessionID(),
									project.getId(),
									new DoWorkAction<Set<SearchResult>>(
											getClientFactory(),
											new OnDoneHandler<Set<SearchResult>>() {

												@Override
												public void doWork(
														String studyId,
														String friendlyName,
														Set<SearchResult> searchResults) {
													project.getSnapshots()
															.addAll(searchResults);
													
													project.setState(STATE.LOADED);
													IeegEventBusFactory.getGlobalEventBus().fireEvent(
															new ProjectLoadedEvent(project));
												}
											},
											project.getId(),
											project.getName()));
				} else {
					getDisplay().updateChildPane();
				}
			} else if (item instanceof CollectionNode && item.getLabel().equals("Favorites")) {
				getDisplay().updateChildPane();
			}
		}
	}
	
	
	
	private void updateDetailsPane(final SearchResult item) {
		String revId;
		String base = (item.getParent() == null) ? "" : 
			item.getParent().getId();
		if (base.length() > 0 && item.getParent() instanceof SearchResult)
			revId = base;
		else
			revId = item.getId();

		String friendlyName = item.getLabel();

		// Cached
		if (model.getStudyData(revId) != null) {
			GWT.log("MetadataBrowserPresenter refetching study details");
//			setDetails(revId, friendlyName, model.getStudyData(revId));
			getDisplay().updateChildPane(model.getStudyData(revId));
		} else if (model.getExperimentData(revId) != null) {
			GWT.log("MetadataBrowserPresenter refetching experiment details");
//			setDetails(revId, friendlyName,
//					model.getExperimentData(revId));
			getDisplay().updateChildPane(model.getExperimentData(revId));
			
			
		// Fetch details
		} else if (item.getStringValue("type") == "EEG_STUDY") {
			GWT.log("MetadataBrowserPresenter requesting details on EEG");
			final PresentableMetadata it = item;
			requestDetails(
					revId,
					friendlyName,
					new OnDoneHandler<StudyMetadata>() {

						@Override
						public void doWork(String studyId,
								String friendlyName,
								StudyMetadata meta) {
							model.addStudyData(studyId, meta);
//							setDetails(studyId, friendlyName,
//									meta);
							
							getDisplay().getMetadataProviderFor(it).getNewItemContent();
							getDisplay().updateChildPane(meta);
						}
					});
		} else if (item.getStringValue("type") == "EXPERIMENT") {
			GWT.log("MetadataBrowserPresenter requesting details on EEG");
			final PresentableMetadata it = item;
			requestDetailsExperiment(
					revId,
					friendlyName,
					new OnDoneHandler<ExperimentMetadata>() {

						@Override
						public void doWork(String expId,
								String friendlyName,
								ExperimentMetadata meta) {
							model.addExperimentData(expId, meta);
//							setDetails(expId, friendlyName,
//									meta);
							
							getDisplay().getMetadataProviderFor(it).getNewItemContent();
							getDisplay().updateChildPane(meta);
						}
					});
		}
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		GWT.log("Binding metadata browser");
		getDisplay().setHistoryAndModel(model);
		getDisplay().addPresenter(this);
		getDisplay().addSnapshotSelectionChangedHandler(new SelectionChangeEvent.Handler() {
			LRUCache<PresentableMetadata,Long> cache =
					new LRUCache<PresentableMetadata, Long>(100);

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				updateSelection(event, cache);
			}
		}
		);
		
		getDisplay().addSearchHandler(SearchDialogClickHandler.getHandler(getClientFactory()), this);
		getDisplay().addOpenHandler(OpenSnapshotDialogClickHandler.getHandler(getClientFactory()), this);
		getDisplay().addFavoriteClickHandler(FavoriteToggleHandler.getHandler());
		
		getDisplay().bindToolbar();
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(SearchResultsUpdatedEvent.getType(), this);
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(MetadataContentsUpdatedEvent.getType(), this);

		{
			SnapshotOpenedEvent.Handler eHandle = new SnapshotOpenHandlerAction(getDisplay());
			eventHandlers.add(eHandle);
			IeegEventBusFactory.getGlobalEventBus().registerHandler(SnapshotOpenedEvent.getType(),
					eHandle);
		}

		{
			ClosedSnapshotEvent.Handler eHandle = new SnapshotCloseHandlerAction(getDisplay());
			eventHandlers.add(eHandle);
			IeegEventBusFactory.getGlobalEventBus().registerHandler(ClosedSnapshotEvent.getType(), 
					eHandle);
		}
		
		{
			ProjectCreateAction eHandle = new ProjectCreateAction(getClientFactory());
			eventHandlers.add(eHandle);
			IeegEventBusFactory.getGlobalEventBus().registerHandler(CreateProjectEvent.getType(), 
					eHandle);
		}
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(BookmarkSnapshotEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(UnbookmarkSnapshotEvent.getType(), this);
		
		
		getDisplay().log(3, "Requesting Projects in metadata browser");
		getClientFactory().getProjectServices().getLazyProjectsForUser(getSessionModel().getSessionID(), 
			    new ProcessProjectsAction(getClientFactory(), getDisplay()));
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ProjectCreatedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ProjectUpdatedEvent.getType(), this);

		// Register self
		getSessionModel().getHeaderBar().addLocalEntry((AstroPanel)getDisplay(), 
				"search", SearchDialogClickHandler.getHandler(getClientFactory()));
		
		for (ChildPane p: getDisplay().getChildPanes())
			p.bind();
	}
	
	@Override
	public void unbind() {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
		for (ChildPane p: getDisplay().getChildPanes())
			p.unbind();
		
		for (EventHandler h: eventHandlers)
			IeegEventBusFactory.getGlobalEventBus().unregisterHandler(h);
	}

	@Override
	public void onUnbookmarkSnapshot(SearchResult snapshot) {
		model.removeFavorite(snapshot);
	}

	@Override
	public void onBookmarkSnapshot(SearchResult snapshot) {
		model.addFavorite(snapshot);
	}

	public void requestDetails(final String study, final String friendlyName, 
			final OnDoneHandler<StudyMetadata> done) {
		getClientFactory().getSnapshotServices().getEegStudyMetadata(getSessionModel().getSessionID(), study,
				new DoWorkAction<StudyMetadata>(getClientFactory(), done, study, friendlyName));
	}
	
	public void requestDetailsExperiment(final String study, final String friendlyName, 
			final OnDoneHandler<ExperimentMetadata> done) {
		getClientFactory().getSnapshotServices().getExperimentMetadata(getSessionModel().getSessionID(), study, 
				new DoWorkAction<ExperimentMetadata>(getClientFactory(), done, study, friendlyName));
	}

	@Override
	public Set<String> getSupportedDataTypes() {
		return Sets.newHashSet();
	}
	
	public void getDerivedResults(final PresentableMetadata generalMetadata) {
		getClientFactory().getSnapshotServices().getRelatedAnalyses(getSessionModel().getSessionID(), 
				generalMetadata.getId(),
				new ProcessDerivedResultsAction(this, generalMetadata));
	}

	@Override
	public void onCreatedProject(Project project) {
		getDisplay().addProject(project);
	}

	@Override
	public void onUpdatedProject(Project project) {
		int pos = 0;
		for (PresentableMetadata old: getDisplay().getProjects()) {
			if (old.getId().equals(project.getPubId())) {
				getDisplay().log(3, "Removing existing project during update");
				getDisplay().removeProject(pos);
				break;
			} else
				pos++;
		}
		getDisplay().addProject(pos, project);
	}

	@Override
	public void onMetadataContentsUpdated(PresentableMetadata item) {
		getDisplay().refreshItem(item);
	}

	public <H extends EventHandler> void registerHandler(Type<H> handlerType, H handler) {
		IeegEventBusFactory.getGlobalEventBus().registerHandler(handlerType, handler);
	}

	public void releaseHandler(EventHandler handler) {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(handler);
	}
}
