package edu.upenn.cis.db.mefview.client.controller;

import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.db.mefview.client.ClientFactory;

public interface IEventLogger {
	public void initialize(ClientFactory factory);
	
	public void logProvenance(ProvenanceLogEntry.UsageType usage, String snapshot,
			String derivedSnapshot, String annotation);

	public void logToServer(String recipient, String msg);
}
