/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singleton;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import edu.upenn.cis.events.UserEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.hash.HashCode;
import com.google.common.io.ByteStreams;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IeegException;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.model.DataSnapshotEntity;
import edu.upenn.cis.braintrust.model.DataUsageEntity;
import edu.upenn.cis.braintrust.security.AllowedAction;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtAuthzHandler;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.CaseMetadata;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataRequestType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DatasetAndTool;
import edu.upenn.cis.braintrust.shared.DatasetIdAndVersion;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.EpilepsySubtypeMetadata;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.Image;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Precipitant;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.exception.BadDigestException;
import edu.upenn.cis.braintrust.shared.exception.BadRecordingObjectNameException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingObjectNotFoundException;
import edu.upenn.cis.braintrust.webapp.IeegFilter;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.persistence.mapping.AnnotationAssembler;
import edu.upenn.cis.db.mefview.client.SnapshotServices;
import edu.upenn.cis.db.mefview.server.TimeSeriesPageServer.ChSpecTimeSeriesDto;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.IeegWsExceptionUtil;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationModification;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.ControlFileRegistration;
import edu.upenn.cis.db.mefview.shared.DataBundle;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.DatasetPreview.TypeAndMime;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IDataset;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.MimeTypeRecognizer;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SecurityRoles;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.SnapshotAnnotationInfo;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;
import edu.upenn.cis.db.mefview.shared.SpecialPaths;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;
import edu.upenn.cis.db.mefview.shared.TimeSeries;
import edu.upenn.cis.db.mefview.shared.TraceImageInfo;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.TsEventType;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm.AnnotatedTimeSeries;
import edu.upenn.cis.eeg.processing.SignalProcessingFactory;
import edu.upenn.cis.thirdparty.db.mefview.server.SnapshotUploadServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class SnapshotServicesImpl extends RemoteServiceServlet implements
		SnapshotServices {

	private final static Logger logger = LoggerFactory
			.getLogger(SnapshotServicesImpl.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ SnapshotServicesImpl.class);

	private static HabitatUserService uService;
	private static ImageServer images;
	private TraceServer traces = TraceServerFactory.getTraceServer();
	
	private SmtpServer mailServer;
	
	private IEventServer events;
	private static IGraphServer crService;
	
	public static Map<UserId, Map<String, DataSnapshot>> snapshots = Collections
			.synchronizedMap(new LruCache<UserId, Map<String, DataSnapshot>>(
					500));

	private static Map<String, INamedTimeSegment> traceDetails = Collections
			.synchronizedMap(new LruCache<String, INamedTimeSegment>(500));

	private transient IUrlFactory imageUrlFactory;
	DirectoryBucketContainer fc; 
	

	@VisibleForTesting
	SnapshotServicesImpl(final IUserService userService,
			final IDataSnapshotServer dsServer) {

		uService = HabitatUserServiceFactory.getUserService(userService,
				dsServer);
		crService = CoralReefServiceFactory.getGraphServer();
	}

	public SnapshotServicesImpl() {

	}

	public void init() {
		// Only initialize if there isn't an existing instance
		if (uService == null) {
			logger.info("IEEG STARTUP: Launching new server process");
			@SuppressWarnings("unchecked")
			Map<String, String> ivProps = ((Map<String, String>) getServletContext()
					.getAttribute(IvProps.ATTR_KEY));

			// sourcePath = BtUtil.get(ivProps, IvProps.SOURCEPATH, sourcePath);

			final int sessionTimeoutMinutes = IvProps
					.getSessionExpirationMins();
			final int sessionCleanupMins = IvProps.getSessionCleanupMins();
			final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();

			uService = HabitatUserServiceFactory.getUserService();

			uService.createSessionManager(
					uService.getDataSnapshotAccessSession(),
					sessionTimeoutMinutes, sessionCleanupMins,
					sessionCleanupDenom);

			final boolean usingS3 = IvProps.isUsingS3();

			final String imagesBucket = BtUtil.get(ivProps,
					IvProps.IMAGES_BUCKET, IvProps.IMAGES_BUCKET_DFLT);
			final String imagesPath = BtUtil.get(ivProps, IvProps.IMAGES_PATH,
					IvProps.IMAGES_PATH_DFLT);
			final int imagesExpHrs = BtUtil.getInt(ivProps,
					IvProps.IMAGES_EXP_HRS, IvProps.IMAGES_EXP_HRS_DFLT);

			if (usingS3)
				imageUrlFactory = new S3PresignedUrlFactory(imagesBucket,
						imagesPath, imagesExpHrs);
			else
				imageUrlFactory = new UrlFactory(
						BtUtil.getConfigPath("imagepath"));

			images = new ImageServer(uService.getDataSnapshotAccessSession(), imageUrlFactory);
			fc = new DirectoryBucketContainer(IvProps.getDataBucket(),
					IvProps.getSourcePath());
			
			mailServer = SmtpServerFactory.getSmtpServer();
			
			try {
				Class<IEventServer> c = (Class<IEventServer>) Class.forName("com.blackfynn.dsp.server.timeseries.EventTimeSeriesServer");
				events = c.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
			crService = CoralReefServiceFactory.getGraphServer();

		} else
			logger.info("IEEG STARTUP: Reusing server process");
	}

	@VisibleForTesting
	void initTest(ISessionManager manager) {
		// Only initialize if there isn't an existing instance
		if (uService == null) {
			IvProps.init(".");
			logger.info("IEEG STARTUP: Launching new server process");

			uService.setDataSnapshotAccessSession(DataSnapshotServerFactory
					.getDataSnapshotServer());

			uService.setSessionManager(manager);
			imageUrlFactory = new UrlFactory("http://nowhere.com/");

			images = new ImageServer(uService.getDataSnapshotAccessSession(), imageUrlFactory);
			fc = new DirectoryBucketContainer(IvProps.getDataBucket(),
					IvProps.getSourcePath()); 
		} else
			logger.info("IEEG STARTUP: Reusing server process");
	}

	private List<ChannelSpecifier> getChannelSpecifiers(User user,
			SnapshotSpecifier snap, Collection<TimeSeriesDto> tsList) {
		List<ChannelSpecifier> ret = new ArrayList<ChannelSpecifier>();
		for (TimeSeriesDto ts : tsList)
			ret.add(traces.getChannelSpecifier(user, snap, ts.getId(), 0));

		return ret;
	}

	private List<ChannelSpecifier> getChannelSpecifiers(User user, String snap,
			Collection<TimeSeriesDto> tsList) {
		return getChannelSpecifiers(user,
				traces.getSnapshotSpecifier(user, snap), tsList);
	}

	private List<String> getData(User user, String datasetID, List<INamedTimeSegment> traceLabels,
			List<ChannelSpecifier> tracefileNames, double requestedStart,
			double requestedDuration, double requestedSamplingPeriod,
			int scaleFactor, List<DisplayConfiguration> filters, boolean isExact,
			SessionCacheEntry se, SignalProcessingStep processing,
			SessionToken sessionToken, List<List<Annotation>> annotations)
			throws ServerTimeoutException, IOException {
		final String M = "getData(...)";
		int r = 0;
		List<List<TimeSeriesData>> series = new ArrayList<List<TimeSeriesData>>(
				tracefileNames.size());
		
		for (int i = 0; i < filters.size(); i++)
			tracefileNames.get(i).setTimeOffset(filters.get(i).getTimeOffset());

		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Requesting " + tracefileNames + " at "
					+ requestedStart + " with period "
					+ requestedSamplingPeriod + " ("
					+ (requestedDuration / requestedSamplingPeriod) + ")");
		}
		if (tracefileNames.isEmpty())
			return new ArrayList<String>();

		long time = System.nanoTime();
		if (!isExact) {
			final DataSnapshotSearchResult dataset = 
					uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
			series = traces.getTimeSeriesSet(user, events, dataset, tracefileNames,
					requestedStart, requestedDuration,
					1.E6 / requestedSamplingPeriod, null, se, processing,
					sessionToken);
		} else
			series = traces.getTimeSeriesSetRaw(user, tracefileNames,
					requestedStart, requestedDuration, scaleFactor,
					sessionToken);
		if (logger.isDebugEnabled()) {
			logger.debug(M + ": Request returned in "
					+ ((System.nanoTime() - time) / 1000) + " usec");
		}
		timeLogger.trace("{} traces.getTimeSeriesSetX {} seconds", M,
				BtUtil.diffNowThenSeconds(time));

		final long getSampleRateStart = System.nanoTime();
		
		double highestRate = 0;
		for (ChannelSpecifier fn: tracefileNames) {
			double rate = traces.getSampleRate(fn); 
			if (rate > highestRate)
				highestRate = rate;
		}
		
		double origPeriod = 1000000. / highestRate;
		timeLogger.trace("{}: traces.getSampleRate {} seconds", M,
				BtUtil.diffNowThenSeconds(getSampleRateStart));

		double thresholdedPeriod = requestedSamplingPeriod;

		// We don't up-sample beyond max
		if (thresholdedPeriod < origPeriod) {
			logger.trace(M + ": Updating sample period");
			thresholdedPeriod = origPeriod;
		}
		//

		if (logger.isTraceEnabled()) {
			logger.trace(M + ": Read " + r + " traces at " + origPeriod);
		}

		if (processing != null && false) {
			try {
				System.err
						.println("Processing via " + processing.getStepName());
				ISignalProcessingAlgorithm algo = SignalProcessingFactory
						.getAlgorithm(processing);
				annotations.clear();
				int inx = 0;
				for (List<TimeSeriesData> channel : series) {
					annotations.add(new ArrayList<Annotation>());
					for (TimeSeriesData segment : channel) {
						List<AnnotatedTimeSeries> annList = algo.process(
								traceLabels.get(inx).getId(), tracefileNames.get(inx)
										.getId(), requestedStart,
								thresholdedPeriod, segment);
						for (AnnotatedTimeSeries ann : annList) {
							for (IDetection det: ann.getAnnotations()) {
								annotations.get(annotations.size() - 1).add(
										(Annotation)det);
							}
						}
					}
					inx++;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		long resultCreationIn = System.nanoTime();
		List<String> results = DataMarshalling.convertTimeSeriesToJson(series,
				requestedStart, requestedSamplingPeriod, requestedDuration,
				thresholdedPeriod);
		
//		for (String str: results)
//			System.out.println(str);

		timeLogger.trace("{} results creation {} seconds", M,
				BtUtil.diffNowThenSeconds(resultCreationIn));

		return results;
	}


	@Override
	public Set<ImageInfo> getImageInfo(SessionToken sessionID,
			String datasetId, String traceId, String trace, String typ)
			throws BtSecurityException {
		final String M = "getImageInfo(...)";
		try {
			return images.getImageInfo(uService.verifyUser(M, sessionID, null),
					datasetId, traceId, trace, typ);
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<TraceImageInfo> getTracesForImage(SessionToken sessionID,
			String imageWithPath) throws BtSecurityException {
		final String M = "getTracesForImage(...)";
		try {
			return images.getTracesForImage(uService.verifyUser(M, sessionID, null),
					imageWithPath);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}

	}

//	@Override
//	public Set<ImageInfo> getImagesForStudy(SessionToken sessionID,
//			String datasetID) throws BtSecurityException {
//		final String M = "getImagesForStudy(...)";
//		try {
//			Set<ImageInfo> ret = new HashSet<ImageInfo>();
//			User user = uService.verifyUser(M, sessionID, null);
//			DataSnapshot ds = getSnapshot(user, datasetID);
//
//			for (Image i : ds.getImages()) {
//				final ImageType it = i.getType();
//				final String imageFileKey = i.getFileKey();
//				final String imageUrl = imageUrlFactory.getUrl(imageFileKey);
//				ImageInfo ii = new ImageInfo(imageUrl, i.getFileKey(),
//						it.name(), 0, 0, i.getRevId());
//
//				logger.info("Adding image " + i.getFileKey() + ": "
//						+ i.getRevId() + " -> " + ii.getName());
//				ret.add(ii);
//			}
//
//			return ret;
//		} catch (Throwable t) {
//			logger.error(M + ": caught exception", t);
//			propagateIfInstanceOf(t, BtSecurityException.class);
//			throw propagate(t);
//		}
//	}
//
	@Override
	public Set<ImageInfo> getImagesForStudy(SessionToken sessionID,
			String studyPath, String typ) throws BtSecurityException {
		final String M = "getImagesForStudy(...)";
		try {
			return images.getImagesForStudy(uService.verifyUser(M, sessionID, null),
					studyPath, typ);
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	private List<String> getTimeSeries(SessionToken sessionToken,
			String contextID, String datasetID, List<INamedTimeSegment> traceids,
			double start, double width, double samplingPeriod, int scaleFactor,
			List<DisplayConfiguration> filters,
			final SignalProcessingStep processing, final String M,
			List<List<Annotation>> processingResults)
			throws BtSecurityException, IeegException, ServerTimeoutException {

		long startTime = System.nanoTime();
		double getChannelsSec = -1;
		double getDataSec = -1;
		double stringBuilderSec = -1;
		User user = null;
		Optional<String> dataSnapshotName = Optional.absent();

		try {
			user = uService.verifyUser(M, sessionToken, null);
			DataSnapshotEntity dsEntity = uService
					.getDataSnapshotAccessSession().getDataSnapshotFromCache(
							user, datasetID);
			if (dsEntity != null) {
				dataSnapshotName = Optional.of(dsEntity.getLabel());
			}
			checkArgument(samplingPeriod > 0, "samplingPeriod is <= 0");

			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.READ,
					datasetID);

			checkArgument(samplingPeriod > 0, "samplingPeriod is <= 0");

			if (logger.isTraceEnabled()) {
				logger.trace("Handling request from " + contextID + " for "
						+ datasetID + " index "
						+ (new Date((long) (start / 1000))).toString() + "."
						+ (int) (start % 1000000) + "(" + start + ") for "
						+ width + " usec, samplingPeriod: " + samplingPeriod);
			}

			final long getChannelsStart = System.nanoTime();
			
			List<ChannelSpecifier> tracefileIds = traces.getChannelsFromSegments(user,
					datasetID, traceids, samplingPeriod);

			List<String> ids = DataMarshalling.getTimeSegmentIds(traceids);
			SessionCacheEntry se = new SessionCacheEntry(ids,
					tracefileIds, filters, 1.E6 / samplingPeriod, width);

			getChannelsSec = BtUtil.diffNowThenSeconds(getChannelsStart);

			if (traceids.size() != filters.size()) {
				throw new RuntimeException("Mismatched sizes for " + datasetID);
			}

			final long getDataStart = System.nanoTime();
			List<String> ar = null;
			try {
				if (scaleFactor == 0)
					ar = getData(user, datasetID, traceids, tracefileIds, start, width,
							samplingPeriod, 0, filters, false, se, processing,
							sessionToken, processingResults);
				else
					ar = getData(user, datasetID, traceids, tracefileIds, start, width, 0,
							scaleFactor, null, true, null, processing,
							sessionToken, processingResults);
			} catch (OutOfMemoryError ome) {
				logger.error("caught OutOfMemoryError", ome);
				// server.clearAll();
				System.gc();
				System.gc();
				if (scaleFactor == 0)
					ar = getData(user, datasetID, traceids, tracefileIds, start, width,
							samplingPeriod, 0, filters, false, se, processing,
							sessionToken, processingResults);
				else
					ar = getData(user, datasetID, traceids, tracefileIds, start, width, 0,
							scaleFactor, null, true, null, processing,
							sessionToken, processingResults);
			}
			getDataSec = BtUtil.diffNowThenSeconds(getDataStart);

			HttpServletRequest httpServletRequest = getThreadLocalRequest();
			if (httpServletRequest != null) {
				DataUsageEntity dataUsage = new DataUsageEntity(
						DataRequestType.VIEWER, datasetID, (long) start,
						(long) width);
				IeegFilter.setUserAndDataUsage(httpServletRequest, user,
						dataUsage);
			}

			return ar;
		} catch (ServerTimeoutException e) {
			logger.error(M + ": Caught Exception", e);
			throw e;
		} catch (Exception e) {
			logger.error(M + ": Caught Exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			propagateIfInstanceOf(e, IeegException.class);
			throw propagate(e);
		} finally {
			logger.info(
					"{}: [user: {} snapshot label: {} snapshot id: {} start: {} duration: {} samplingPeriod: {}] getChannels {} seconds, getData {} seconds, stringBuilder {} seconds, {} seconds",
					new Object[] { M,
							user != null ? user.getUsername() : "null",
							dataSnapshotName, datasetID, start, width,
							samplingPeriod, getChannelsSec, getDataSec,
							stringBuilderSec,
							BtUtil.diffNowThenSeconds(startTime) });
		}
	}

	private DataSnapshot getSnapshot(User u, String datasetID) {

		DataSnapshot ds;
		UserId userId = u.getUserId();

		Map<UserId, Map<String, DataSnapshot>> snapshotsLocal = snapshots;

		// Don't use the cache for data enterers - so they see changes during
		// data
		// entry
		if (u.isAdmin()) {
			snapshotsLocal = newHashMap();
		}

		if (snapshotsLocal.containsKey(userId)
				&& snapshotsLocal.get(userId).containsKey(datasetID))
			return snapshotsLocal.get(userId).get(datasetID);

		ds = uService.getDataSnapshotAccessSession().getDataSnapshot(u,
				datasetID);

		if (!snapshotsLocal.containsKey(userId))
			snapshotsLocal.put(userId, new HashMap<String, DataSnapshot>());
		snapshotsLocal.get(userId).put(datasetID, ds);
		return ds;
	}

	private List<TraceInfo> getTraceInfo(User user, String dataSnapshot)
			throws IllegalArgumentException, IOException {
		List<TraceInfo> channelList = new ArrayList<TraceInfo>();

		SearchResult parent = getSearchResultForSnapshotForAuthenticatedUser(
				user, dataSnapshot);
		DataSnapshot dss = uService.getDataSnapshotAccessSession()
				.getDataSnapshot(user, dataSnapshot);
		Set<TimeSeriesDto> series = dss.getTimeSeries();
		List<TimeSeriesDto> seriesList = new ArrayList<TimeSeriesDto>();
		seriesList.addAll(series);

		List<ChannelSpecifier> channels = getChannelSpecifiers(user,
				dataSnapshot, seriesList);

		List<ChannelInfoDto> channelDtos = uService
				.getDataSnapshotAccessSession().getChannelInfo(dataSnapshot);

		if (channelDtos.isEmpty()) {
			try {
				channelDtos = traces.getTimeSeriesChannelInfo(seriesList, channels);
				channelDtos = uService.getDataSnapshotAccessSession()
						.writeChannelInfo(dataSnapshot, channelDtos);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// channelList = DataMarshalling
			// .convertTimeSeriesToTraceInfo(seriesList,
			// channels, traces);

		}
		channelList = DataMarshalling.convertChannelInfoToTraceInfo(
				getSearchResultForSnapshotForAuthenticatedUser(user,
						dataSnapshot), channelDtos);

		Set<String> existing = new HashSet<String>();
		List<Integer> remove = new ArrayList<Integer>();
		int i = 0;
		for (TraceInfo trace : channelList) {
			trace.setParent(parent);
			if (existing.contains(trace.getRevId()))
				remove.add(i);
			else
				existing.add(trace.getRevId());
			i++;
		}

		for (int j = remove.size() - 1; j >= 0; j--)
			channelList.remove(remove.get(j));

		return channelList;
	}

//	private static Set<ContactGroupMetadata> getCGMData(
//			Set<edu.upenn.cis.braintrust.shared.ContactGroupMetadata> contactGroupMetadataSet) {
//		Set<ContactGroupMetadata> ret = new HashSet<ContactGroupMetadata>();
//
//		for (ContactGroupMetadata contactGroupMetadata : contactGroupMetadataSet)
//			ret.add(new ContactGroupMetadata(contactGroupMetadata
//					.getChannelPrefix(), contactGroupMetadata.getLocation()
//					.toString(), contactGroupMetadata.getSide().toString(),
//					contactGroupMetadata.getSamplingRate(),
//					contactGroupMetadata.getLffSetting().orNull(),
//					contactGroupMetadata.getHffSetting().orNull()));
//
//		return ret;
//	}

	private static Set<String> getPrecipitants(Set<Precipitant> precip) {
		Set<String> ret = new HashSet<String>();

		for (Precipitant p : precip)
			ret.add(p.toString());

		return ret;
	}

	private static Set<String> getSeizureHistory(Set<SeizureType> sz) {
		Set<String> ret = new HashSet<String>();

		for (SeizureType s : sz)
			ret.add(s.toString());

		return ret;
	}

	private static Set<String> getSzSubtypes(Set<EpilepsySubtypeMetadata> szSubT) {
		Set<String> ret = new HashSet<String>();

		for (EpilepsySubtypeMetadata s : szSubT)
			ret.add(s.getEpilepsyType().toString() + "/"
					+ s.getEtiology().toString());

		return ret;
	}

	@Override
	public DataSnapshotIds addAndRemoveAnnotations(SessionToken sessionID,
			String datasetID, Set<Annotation> annotationsToSave,
			Set<Annotation> annotationsToRemove,
			Set<String> annotationLayersToRemove) throws BtSecurityException {
		final String M = "addAndRemoveAnnotations(...)";
		try {

			// Verify user
			final User user = uService.verifyUser(M, sessionID, Role.USER);

			// annMap maps client-side temporary ids to server IDs for
			// annotations
			DataSnapshotIds annMap = null;
			if (!annotationsToSave.isEmpty()) {
				annMap = traces.saveAnnotations(user, datasetID,
						annotationsToSave);
			} else {
				annMap = new DataSnapshotIds(datasetID,
						new HashMap<String, String>());
			}

			if (!annotationsToRemove.isEmpty()) {
				traces.removeAnnotations(user, datasetID, annotationsToRemove);
			}

			if (!annotationLayersToRemove.isEmpty()) {
				traces.removeAnnotationsByLayer(user, datasetID,
						annotationLayersToRemove);
			}

			return annMap;

		} catch (Throwable t) {
			logger.error(M + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String addDataSnapshotTimeSeries(SessionToken sessionID,
			String revID, Set<String> series) throws BtSecurityException {
		final String M = "addDataSnapshotTimeSeries()";
		try {
			User u = uService.verifyUser(M, sessionID, Role.USER);
			String ret = uService.getDataSnapshotAccessSession()
					.addTimeSeriesToDataset(u, revID, series);

			UserId userId = u.getUserId();
			if (snapshots.containsKey(userId))
				snapshots.get(userId).remove(revID);

			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public SnapshotAnnotationInfo getAnnotationInfo(SessionToken sessionID,
			String snapshot, double startPosition, double endPosition)
			throws BtSecurityException {
		final String m = "getAnnotationInfo()";
		try {
			SnapshotAnnotationInfo ret = new SnapshotAnnotationInfo();
			ret.setSnapshotId(snapshot);
			DisplayConfiguration fp = new DisplayConfiguration();

			User user = uService.verifyUser(m, sessionID, null);
			DataSnapshot ds = getSnapshot(user, snapshot);

			// Retrieve channel info, which unfortunately isn't immediately
			// returned

			List<TimeSeriesDto> chSpecsToRetrieve = Lists.newArrayList();
			for (TimeSeriesDto ts : ds.getTimeSeries()) {
				chSpecsToRetrieve.add(ts);
			}

			List<ChannelSpecifier> tsIds2ChSpecs = traces.getChannels(
					user,
					snapshot,
					newArrayList(transform(chSpecsToRetrieve,
							TimeSeriesDto.getId)), 0);

			List<ChSpecTimeSeriesDto> toRetrieve = newArrayList();

			for (int i = 0; i < chSpecsToRetrieve.size(); i++) {
				toRetrieve.add(new ChSpecTimeSeriesDto(tsIds2ChSpecs.get(i),
						chSpecsToRetrieve.get(i)));
			}

			List<TraceInfo> retrievedTraceInfos = getTraceInfo(user, snapshot);

			// Iterate over each channel and find the earliest start and latest
			// end points
			for (TraceInfo ti : retrievedTraceInfos) {
				// Start time index
				if (ret.getSnapshotStartIndex() == 0
						|| ret.getSnapshotStartIndex() < ti.getStartTime()) {
					ret.setSnapshotStartIndex(ti.getStartTime());
				}

				// End time index
				if (ret.getSnapshotEndIndex() == 0
						|| ret.getSnapshotEndIndex() < ti.getDuration()
								+ ti.getStartTime()) {
					ret.setSnapshotEndIndex(ti.getStartTime()
							+ ti.getDuration());
				}
			}

			List<TsAnnotationDto> tsas = ds.getTsAnnotations();

			double span = (ret.getSnapshotEndIndex() - ret
					.getSnapshotStartIndex()) * 0.1;
			for (TsAnnotationDto tsa : tsas) {
				Annotation ann = AnnotationAssembler.toAnnotation(tsa);
				ret.markAnnotation(ann);

				// Either one endpoint of the interval falls within our window,
				// or the entire
				// annotation spans our window
				if ((ann.getStart() >= startPosition - span && ann.getStart() <= endPosition
						+ span)
						|| (ann.getEnd() >= startPosition - span && ann
								.getEnd() <= endPosition + span)
						|| (ann.getStart() <= startPosition && ann.getEnd() >= endPosition)) {
					ret.addAnnotation(ann);

					if (ret.getWindowStart() > ann.getStart())
						ret.setWindowStart(ann.getStart());

					if (ret.getWindowEnd() < ann.getEnd())
						ret.setWindowEnd(ann.getEnd());

					// if (ann.getType().equals("valid_data")) {
					// ret.setSnapshotStartIndex(ann.getStart());
					// ret.setSnapshotEndIndex(ann.getEnd());
					// }
					if (ann.getType().equals("time_segment")) {
						ret.setSnapshotStartIndex(ann.getStart());
						ret.setSnapshotEndIndex(ann.getEnd());
					}

					if (ann.getType().equals("snapshot_frequency")) {
						ret.setDefaultPeriod(ann.getStart());

						if (ann.getDescription().equals("locked"))
							ret.setPeriodIsModifiable(false);
					}

					if (ann.getType().equals("filter_type")) {
						if (ann.getDescription().equals("lowpass"))
							fp.setFilterType(DisplayConfiguration.LOWPASS_FILTER);
						else if (ann.getDescription().equals("highpass"))
							fp.setFilterType(DisplayConfiguration.HIGHPASS_FILTER);
						else if (ann.getDescription().equals("bandpass"))
							fp.setFilterType(DisplayConfiguration.BANDPASS_FILTER);
						else if (ann.getDescription().equals("notch"))
							fp.setFilterType(DisplayConfiguration.BANDSTOP_FILTER);

						if (ann.getDescription().equals("lowpass-locked")) {
							fp.setFilterType(DisplayConfiguration.LOWPASS_FILTER);
							ret.setFiltersAreModifiable(false);
						} else if (ann.getDescription().equals(
								"highpass-locked")) {
							fp.setFilterType(DisplayConfiguration.HIGHPASS_FILTER);
							ret.setFiltersAreModifiable(false);
						} else if (ann.getDescription().equals(
								"bandpass-locked")) {
							fp.setFilterType(DisplayConfiguration.BANDPASS_FILTER);
							ret.setFiltersAreModifiable(false);
						} else if (ann.getDescription().equals("notch-locked")) {
							fp.setFilterType(DisplayConfiguration.BANDSTOP_FILTER);
							ret.setFiltersAreModifiable(false);
						}

						fp.setNumPoles((int) ann.getStart());
					}

					if (ann.getType().equals("filter_pass")) {
						fp.setBandpassLowCutoff(ann.getStart());
						fp.setBandpassHighCutoff(ann.getEnd());
					}
					if (ann.getType().equals("filter_stop")) {
						fp.setBandstopLowCutoff(ann.getStart());
						fp.setBandstopHighCutoff(ann.getEnd());
					}
				}
			}
			ret.setDefaultSnapshotFilters(fp);

			return ret;
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public List<Annotation> getDataSnapshotAnnotations(SessionToken sessionID,
			String dataSnapshot) throws BtSecurityException {
		final String M = "getDataSnapshotAnnotations()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			DataSnapshot dss = uService.getDataSnapshotAccessSession()
					.getDataSnapshot(user, dataSnapshot);

			if (dss == null) {
				throw new IllegalArgumentException("DataSnapshot "
						+ dataSnapshot + " does not exist");
			}

			List<TraceInfo> channelList = getTraceInfo(user, dataSnapshot);

			PriorityQueue<Annotation> retQueue = new PriorityQueue<Annotation>();
			for (TsAnnotationDto ann : dss.getTsAnnotations()) {
				Annotation a = AnnotationAssembler.toAnnotation(ann,
						channelList);
				retQueue.add(a);
			}
			List<Annotation> ret = new ArrayList<Annotation>();
			ret.addAll(retQueue);
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public List<ProvenanceLogEntry> getSnapshotProvenance(
			SessionToken sessionID, String snapshot) throws BtSecurityException {
		final String m = "getSnapshotProvenance()";
		try {
			List<ProvenanceLogEntry> entries = uService
					.getDataSnapshotAccessSession()
					.getProvenanceEntriesForSnapshot(
							uService.verifyUser(m, sessionID, Role.USER),
							snapshot);

			return entries;
		} catch (Exception e) {
			logger.error(m + ": Caught Exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	@Override
	public StudyMetadata getEegStudyMetadata(SessionToken sessionID,
			final String studyRevId) throws BtSecurityException {
		final String M = "getEegStudyMetadata()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			EegStudyMetadata meta = uService.getDataSnapshotAccessSession()
					.getEegStudyMetadata(u, studyRevId);
			
			if (meta == null)
				return null;

			return getStudyMetadata(meta,
					getSeizureHistory(meta.getSzHistory()),
					getPrecipitants(meta.getPreciptants()),
					meta.getContactGroups(),
//					getCGMData(meta.getContactGroups()),
					getSzSubtypes(meta.getEpilepsySubtypes()));
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	private String getSnapshotDcnImageUrl(String snapshotName) {
		final String fileKey = "DCN_static_plots/" + snapshotName + ".png";
		final String ret = imageUrlFactory.getUrl(fileKey);
		return ret;
	}

	private StudyMetadata getStudyMetadata(EegStudyMetadata meta,
			Set<String> seizureHistory, Set<String> precipitants,
			Set<edu.upenn.cis.braintrust.shared.ContactGroupMetadata> electrodes, Set<String> szSubType) {
		final String dcnImageUrl = getSnapshotDcnImageUrl(meta.getLabel());
		return new StudyMetadata(meta.getPatientLabel(), meta.getAgeAtOnset()
				, meta.getGender().toString(), meta.getHandedness()
				.toString(), meta.getEthnicity().toString(),
				meta.getEtiology(), meta.getImageCount(), meta
						.getDevelopmentalDisorders(), meta
						.getTraumaticBrainInjury(), meta
						.getFamilyHistory(), seizureHistory,
				precipitants, meta.getAgeAtAdmission(), meta
						.getRefElectrodeDescription(), electrodes,
				szSubType, dcnImageUrl);
	}

	@Override
	public ExperimentMetadata getExperimentMetadata(SessionToken sessionID,
			String experimentId) throws BtSecurityException {
		final String M = "getExperimentMetadata()";
		try {
			User user = uService.verifyUser(M, sessionID, Role.USER);
			return uService.getDataSnapshotAccessSession()
					.getExperimentMetadata(user, experimentId);
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public SearchResult getSearchResultForSnapshot(SessionToken sessionID,
			final String datasetNameOrId) throws BtSecurityException {
		final String M = "getSearchResultForSnapshot()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			return getSearchResultForSnapshotForAuthenticatedUser(u,
					datasetNameOrId);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	private SearchResult getSearchResultForSnapshotForAuthenticatedUser(
			User authenticatedUser, final String datasetNameOrId) {
		String dataSnapshotId = null;

		try {
			dataSnapshotId = uService.getDataSnapshotAccessSession()
					.getDataSnapshotId(authenticatedUser, datasetNameOrId, false);
		} catch (DataSnapshotNotFoundException e) {
			// yes I'm using an exception incorrectly for normal program
			// flow
			dataSnapshotId = datasetNameOrId;
		}

		Set<String> dsRevIds = singleton(dataSnapshotId);

		Set<SearchResult> sr = DataMarshalling.convertSearchResults(
				uService.getDataSnapshotAccessSession().getLatestSnapshots(
						authenticatedUser, dsRevIds), authenticatedUser,
				uService);

		// There should be 0 or 1 elements
		if (sr.isEmpty())
			return null;
		else
			return sr.iterator().next();
	}

	@Override
	public Set<ImageInfo> getImagesForStudy(SessionToken sessionID,
			String datasetID) throws BtSecurityException {
		final String M = "getImagesForStudy(...)";
		try {
			Set<ImageInfo> ret = new HashSet<ImageInfo>();
			User user = uService.verifyUser(M, sessionID, null);
			DataSnapshot ds = getSnapshot(user, datasetID);

			for (Image i : ds.getImages()) {
				final ImageType it = i.getType();
				final String imageFileKey = i.getFileKey();
				final String imageUrl = imageUrlFactory.getUrl(imageFileKey);
				ImageInfo ii = new ImageInfo(imageUrl, i.getFileKey(),
						it.name(), 0, 0, i.getRevId());

				logger.info("Adding image " + i.getFileKey() + ": "
						+ i.getRevId() + " -> " + ii.getName());
				ret.add(ii);
			}

			return ret;
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public double getNextValidDataRegion(SessionToken sessionID,
			String dataSetID, double position) throws BtSecurityException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Set<AllowedAction> getPermittedActionsForDataset(SessionToken sessionID,
			String dsId) throws BtSecurityException {
		final String m = "getPermittedActionsForDataSnapshot(...)";
		try {
			final User user = uService.verifyUser(m, sessionID, null);
			return uService.getDataSnapshotAccessSession()
					.getPermittedActionsOnDataset(user, dsId);
		} catch (Throwable t) {
			logger.error(m + " : Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}

	}

	@Override
	public List<Place> getPlaces(SessionToken sessionID,
			String dataSnapshotRevId) throws BtSecurityException {
		final String M = "getPlaces()";
		try {
			uService.verifyUser(M, sessionID, null);
			List<Place> ret = new ArrayList<Place>();

			// TODO Auto-generated method stub
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public double getPreviousValidDataRegion(SessionToken sessionID,
			String dataSetID, double position) throws BtSecurityException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Set<DerivedSnapshot> getRelatedAnalyses(SessionToken sessionID,
			String studyRevId) throws BtSecurityException {
		final String M = "getRelatedAnalyses()";
		try {
			Set<DerivedSnapshot> ret = new HashSet<DerivedSnapshot>();
			Set<DatasetAndTool> results;

			User authenticatedUser = uService.verifyUser(M, sessionID,
					Role.USER);
			results = uService.getDataSnapshotAccessSession()
					.getRelatedAnalyses(authenticatedUser, studyRevId);
			for (DatasetAndTool d : results) {
				SearchResult res = getSearchResultForSnapshotForAuthenticatedUser(
						authenticatedUser, d.getDsRevId());
				ret.add(DataMarshalling.convertFromDatasetAndTool(studyRevId,
						res, d));
			}

			return ret;
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String getParentSnapshot(SessionToken sessionID,
			String studyRevId) throws BtSecurityException {
		final String M = "getParentSnapshot()";
		try {
			User authenticatedUser = uService.verifyUser(M, sessionID,
					Role.USER);
			return  uService.getDataSnapshotAccessSession()
					.getParentShapshot(authenticatedUser, studyRevId);
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	public List<RecordingObject> getRecordingObjects(SessionToken sessionToken,
			String datasetId) throws BtSecurityException {
		final String m = "getRecordingObjects()";
		try {
			User u = uService.verifyUser(m, sessionToken, null);

			return newArrayList(uService.getDataSnapshotAccessSession()
					.getRecordingObjects(u, new DataSnapshotId(datasetId)));
		} catch (Throwable t) {
			logger.error(m + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public TimeSeries[] getResults(SessionToken sessionToken, String contextID,
			String datasetID, List<String> traceids, double start,
			double width, double samplingPeriod, List<DisplayConfiguration> filter,
			SignalProcessingStep processing) throws BtSecurityException {
		final String M = "getResults(...)";
		try {
			TimeSeries[] ret = new TimeSeries[traceids.size()];

			logger.trace("Handling request from " + contextID + " for "
					+ datasetID + " index "
					+ (new Date((long) (start / 1000))).toString() + "."
					+ (int) (start % 1000000) + "(" + start + ") for " + width
					+ " usec");

			User user = uService.verifyUser(M, sessionToken, null);
			List<ChannelSpecifier> tracefileIds = traces.getChannels(user,
					datasetID, traceids, samplingPeriod);
			List<List<TimeSeriesData>> series = new ArrayList<List<TimeSeriesData>>(
					traceids.size());

			logger.trace("Requesting " + traceids + " at " + start);
			if (traceids.isEmpty())
				return ret;

			final DataSnapshotSearchResult dataset = 
					uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
			long time = System.nanoTime();
			series = traces.getTimeSeriesSet(user, events, dataset, tracefileIds, start, width,
					1.E6 / samplingPeriod, TraceServer.getFiltersFrom(filter),
					null, processing, sessionToken);
			logger.debug("Request returned in "
					+ ((System.nanoTime() - time) / 1000) + " usec");

			return DataMarshalling.convertTimeSeriesToGwtSegments(series);
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	/**
	 * Decimated
	 */
	@Override
	public String getResultsJSON(SessionToken sessionToken, String contextID,
			String datasetID, List<INamedTimeSegment> traceids, double start,
			double width, double samplingPeriod,
			List<DisplayConfiguration> filters, SignalProcessingStep processing)
			throws BtSecurityException, IeegException, ServerTimeoutException {
		String M = "getResultsJSON(String, String, List<String>, double, double, double, List<FilterParameters>)";

		List<List<Annotation>> ann = new ArrayList<List<Annotation>>();

		List<String> data = getTimeSeries(sessionToken, contextID, datasetID,
				traceids, start, width, samplingPeriod, 0, filters, processing,
				M, ann);

		StringBuilder ret = DataMarshalling.concatenateJson(data);

		logger.debug("* Returned " + ret.length() + " bytes, period = "
				+ samplingPeriod + ", duration = " + (width));

		return ret.toString();
	}

//	@Override
//	public String getResultsJSON(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, int scaleFactor, SignalProcessingStep processing)
//			throws BtSecurityException, IeegException, ServerTimeoutException {
//		final String M = "getResultsJSON(String, String, List<String>, double, double, int)";
//
//		List<List<Annotation>> ann = new ArrayList<List<Annotation>>();
//		List<String> data = getTimeSeries(sessionID, contextID, datasetID,
//				traceids, start, width, scaleFactor, 0, null, processing, M,
//				ann);
//
//		StringBuilder ret = DataMarshalling.concatenateJson(data);
//
//		logger.debug("* Returned " + ret.length() + " bytes, scale = "
//				+ scaleFactor + ", duration = " + (width));
//
//		return ret.toString();
//	}

//	@Override
//	public AnnotationsAndJson getAnnotatedResults(SessionToken sessionID,
//			String contextID, String datasetID, List<String> traceids,
//			double start, double width, double samplingPeriod,
//			List<DisplayConfiguration> filters, SignalProcessingStep processing)
//			throws BtSecurityException, IeegException, ServerTimeoutException {
//
//		String M = "getAnnotatedResults(String, String, List<String>, double, double, double, List<FilterParameters>)";
//
//		List<List<Annotation>> ann = new ArrayList<List<Annotation>>();
//		List<String> data = getTimeSeries(sessionID, contextID, datasetID,
//				traceids, start, width, samplingPeriod, 0, filters, processing,
//				M, ann);
//
//		StringBuilder ret = DataMarshalling.concatenateJson(data);
//
//		logger.debug("* Returned " + ret.length() + " bytes, period = "
//				+ samplingPeriod + ", duration = " + (width));
//
//		return new AnnotationsAndJson(ann, ret.toString());
//	}
//
//	@Override
//	public AnnotationsAndJson getAnnotatedResults(SessionToken sessionID,
//			String contextID, String datasetID, List<String> traceids,
//			double start, double width, int scaleFactor,
//			SignalProcessingStep processing) throws BtSecurityException,
//			IeegException, ServerTimeoutException {
//
//		final String M = "getAnnotatedResults(String, String, List<String>, double, double, int)";
//
//		List<List<Annotation>> ann = new ArrayList<List<Annotation>>();
//		List<String> data = getTimeSeries(sessionID, contextID, datasetID,
//				traceids, start, width, scaleFactor, 0, null, processing, M,
//				ann);
//
//		StringBuilder ret = DataMarshalling.concatenateJson(data);
//
//		logger.debug("* Returned " + ret.length() + " bytes, scale = "
//				+ scaleFactor + ", duration = " + (width));
//
//		return new AnnotationsAndJson(ann, ret.toString());
//	}

	@Override
	public SnapshotContents getSnapshotMetadata(SessionToken sessionID,
			String snapshotRevId) throws BtSecurityException {
		final String m = "getSnapshotMetadata(...)";
		SearchResult metadata = null;
		Set<AllowedAction> actions = new HashSet<AllowedAction>();
		List<INamedTimeSegment> traces = new ArrayList<INamedTimeSegment>();
		List<SearchResult> derived = new ArrayList<SearchResult>();
		Set<DerivedSnapshot> toolResults = newHashSet();
		List<RecordingObject> recordingObjects = newArrayList();
		boolean readOnly = true;
		
		try {
			User user = uService.verifyUser(m, sessionID, null);
			metadata = getSearchResultForSnapshotForAuthenticatedUser(user,
					snapshotRevId);
			actions = uService.getDataSnapshotAccessSession()
					.getPermittedActionsOnDataset(user, snapshotRevId);
			traces = getTraces(user, snapshotRevId);
			
			if (crService != null) {
				// Add raster channels
				traces.addAll(this.getEventChannels(sessionID, metadata, traces));
			}
			
			if (AuthCheckFactory.getHandler().isPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, snapshotRevId, true),
					CorePermDefs.EDIT,
					snapshotRevId))
				readOnly = false;
			
			if (readOnly && crService != null)
				readOnly &= !crService.isObjectAnnotator(user, metadata);
			
			metadata.setEditable(!readOnly);
			
			derived = new ArrayList<SearchResult>();
			toolResults = newHashSet();
			// Only users with USER role are allowed to call
			// getRelatedAnalyses() so we
			// need this check to allow guest users to call this method
			// successfully.
			if (user.getRoles().contains(Role.USER)) {
				toolResults
						.addAll(getRelatedAnalyses(sessionID, snapshotRevId));

				for (DerivedSnapshot tr : toolResults) {
					derived.add(getSearchResultForSnapshotForAuthenticatedUser(
							user, tr.getAnalysisId()));
				}
			}

			recordingObjects.addAll(uService.getDataSnapshotAccessSession()
					.getRecordingObjects(user,
							new DataSnapshotId(snapshotRevId)));
			
//			Set<Image> images = uService.getDataSnapshotAccessSession().getImages(user, snapshotRevId);
//			recordingObjects.addAll(convertImagesToRecordingObjects(snapshotRevId, images, recordingObjects.size()));

			Set<IJsonKeyValue> set = uService.getDataSnapshotAccessSession()
					.getJsonKeyValues(user, snapshotRevId);

			SnapshotContents ret = new SnapshotContents(metadata, actions,
					traces, toolResults, derived, recordingObjects);

			ret.setOtherContent(set);
			
			// Calibrate the trace offsets
			ret.setTraceOffsets();

			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(m + ": Error getting metadata", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			logger.info(
					"{}: Returning empty SnapshotMetadata because of previous exception",
					m);
			return new SnapshotContents(metadata, actions, traces, toolResults,
					derived, recordingObjects);
		}
	}

	private Set<RecordingObject> convertImagesToRecordingObjects(String dsId,
			Set<Image> images, int existingObjectCount) {
		Set<RecordingObject> ret = new HashSet<RecordingObject>();
		
		List<Image> sortedImages = new ArrayList<Image>();
		sortedImages.addAll(images);
		Collections.sort(sortedImages);
		int i = existingObjectCount;
		for (Image img: images) {
			String mimeType = "application/binary";
			final File path = new File(img.getFileKey());

			if (img.getFileKey().endsWith("jpg"))
				mimeType = "image/jpeg";
			else if (img.getFileKey().endsWith("png"))
				mimeType = "image/png";
			else if (img.getFileKey().endsWith("dcm"))
				mimeType = "image/dicom";
			
			RecordingObject obj = new RecordingObject(path.getName(), "",
					0, "unknown", mimeType, dsId, i, "", new Date(), img.getType() + "-" + (i - existingObjectCount));
			
			ret.add(obj);
		}
		return ret;
	}

	private List<INamedTimeSegment> getTraces(User authenticatedUser, String datasetID)
			throws IllegalArgumentException, IOException {
		List<INamedTimeSegment> ret = new ArrayList<INamedTimeSegment>();
		DataSnapshot ds = getSnapshot(authenticatedUser, datasetID);

		Set<TimeSeriesDto> tss = ds.getTimeSeries();

		List<TimeSeriesDto> chSpecsToRetrieve = newArrayList();

		Map<String, INamedTimeSegment> traceDetailsLocal = traceDetails;

		// Don't use the cache for enterers - so they see changes during
		// data
		// entry
		if (authenticatedUser.isAdmin()) {
			traceDetailsLocal = newHashMap();
		}

		for (TimeSeriesDto ts : tss) {
			if (traceDetailsLocal.containsKey(ts.getId())) {
				ret.add(traceDetailsLocal.get(ts.getId()));
			} else {
				chSpecsToRetrieve.add(ts);
			}
		}

		List<ChannelSpecifier> tsIds2ChSpecs = traces
				.getChannels(
						authenticatedUser,
						datasetID,
						newArrayList(transform(chSpecsToRetrieve,
								TimeSeriesDto.getId)), 0);

		List<ChSpecTimeSeriesDto> toRetrieve = newArrayList();

		for (int i = 0; i < chSpecsToRetrieve.size(); i++) {
			toRetrieve.add(new ChSpecTimeSeriesDto(tsIds2ChSpecs.get(i),
					chSpecsToRetrieve.get(i)));
		}

		// List<TraceInfo> retrievedTraceInfos = server
		// .getTraceInfos(toRetrieve);
		Set<String> existingTraces = new HashSet<String>();
		for (INamedTimeSegment ti : ret)
			existingTraces.add(ti.getId());
		List<TraceInfo> retrievedTraceInfos = getTraceInfo(authenticatedUser,
				datasetID);
		for (TraceInfo ti : retrievedTraceInfos) {
			if (!existingTraces.contains(ti.getRevId())) {
				ret.add(ti);
				existingTraces.add(ti.getRevId());
			}
		}

		SearchResult parent = getSearchResultForSnapshotForAuthenticatedUser(
				authenticatedUser, datasetID);
		for (final INamedTimeSegment ti : retrievedTraceInfos) {
			// Replace the TraceInfo parent metadata with the SearchResult
			// corresponding to our snapshot
			ti.setParent(parent);
			traceDetailsLocal.put(ti.getId(), ti);
		}
		Collections.sort(ret, new Comparator<INamedTimeSegment>() {

			@Override
			public int compare(INamedTimeSegment o1, INamedTimeSegment o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
			
		});

		String logInfo = datasetID + " - " + ret.size() + " traces: ";
		for (INamedTimeSegment t : ret)
			logInfo = logInfo + " " + t.getId().toString();

		logger.debug(logInfo);
		// /
		return ret;
	}

	/**
	 * Deletes a set of annotations, returning a revised dataset ID
	 */
	@Override
	public String removeAnnotations(SessionToken sessionID, String datasetID,
			Set<Annotation> annotations) throws BtSecurityException {
		final String M = "removeAnnotations(...)";
		try {
			final User user = uService.verifyUser(M, sessionID, Role.USER);
			return traces.removeAnnotations(user, datasetID, annotations);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Integer removeAnnotationLayers(SessionToken sessionID,
			String datasetID, Set<String> annotationLayersToRemove)
			throws BtSecurityException {
		final String M = "removeAnnotationLayers(...)";
		try {
			final User user = uService.verifyUser(M, sessionID, Role.USER);
			return traces.removeAnnotationsByLayer(user, datasetID,
					annotationLayersToRemove);

		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}

	}

	@Override
	public String removeDataSnapshotContents(SessionToken sessionID,
			String revID, Set<String> series) throws BtSecurityException {
		final String M = "removeDataSnapshotTimeSeries()";
		try {
			User u = uService.verifyUser(M, sessionID, Role.USER);
			String ret = uService.getDataSnapshotAccessSession()
					.removeTimeSeriesFromDataset(u, revID, series);

			UserId userId = u.getUserId();
			if (snapshots.get(userId) != null)
				snapshots.get(userId).remove(revID);
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	/**
	 * Saves a set of annotations, returning a revised dataset ID
	 */
	@Override
	public DataSnapshotIds saveAnnotations(SessionToken sessionID,
			String datasetID, Set<Annotation> annotations)
			throws BtSecurityException {
		final String M = "saveAnnotations(...)";
		try {
			final User user = uService.verifyUser(M, sessionID, Role.USER);
			// for (Annotation a: annotations) {
			// System.err.println("Annotation " + a.getRevId() + " has color " +
			// a.getColor());
			// }
			return traces.saveAnnotations(user, datasetID, annotations);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Boolean savePlaces(SessionToken sessionID, String dataSnapshotRevId,
			List<Place> places) throws BtSecurityException {
		final String M = "savePlaces()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			uService.getDataSnapshotAccessSession().storeClob(u,
					dataSnapshotRevId, "");
			return new Boolean(true);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String storeDataSnapshotXml(SessionToken sessionID,
			String dataSnapshotRevId, String clob) throws BtSecurityException {
		final String M = "storeDataSnapshotXml()";
		try {
			return uService.getDataSnapshotAccessSession().storeClob(
					uService.verifyUser(M, sessionID, Role.USER),
					dataSnapshotRevId, clob);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<SearchResult> getDerivedSnapshots(SessionToken sessionID,
			String studyRevId) throws BtSecurityException {
		final String M = "getDerivedSnapshots()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			Set<DataSnapshotSearchResult> relatedSearchResults = uService
					.getDataSnapshotAccessSession().getRelatedSearchResults(u,
							studyRevId);
			return DataMarshalling.convertSearchResults(relatedSearchResults,
					u, uService);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void registerFiles(String tempId, String snapshotName,
			SessionToken sessionID, List<String> files)
			throws BtSecurityException {
		final String M = "registerFiles()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			String studyRevId = null;

			try {
				studyRevId = traces.getDataSnapshotId(u, snapshotName);
				AuthCheckFactory.getHandler().checkPermitted(
						uService.getDataSnapshotAccessSession().getPermissions(
								u, HasAclType.DATA_SNAPSHOT,
								CorePermDefs.CORE_MODE_NAME, studyRevId, true),
						CorePermDefs.EDIT,
						studyRevId);
			} catch (DataSnapshotNotFoundException dse) {

			}

			DirectoryBucketContainer cont = (DirectoryBucketContainer) StorageFactory
					.getPersistentServer().getDefaultContainer();

			ControlFileRegistration registration = new ControlFileRegistration(
					cont.getBucket(), SnapshotUploadServlet.getCanonicalName(
							u.getUsername(), tempId, "") + ".");
			// u.getUsername(),
			// new Date(),
			// "Ready",
			// null,
			// null);

			traces.createRegistration(u, registration);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String getUuid() {
		return BtUtil.newUuid();
	}
	
	RecordingObject getRecordingObjectFromFileInfo(User user, String datasetID, FileInfo file) {
		Set<RecordingObject> objects = uService.getDataSnapshotAccessSession()
				.getRecordingObjects(user, new DataSnapshotId(datasetID));

		return uService.getDataSnapshotAccessSession().getRecordingObject(user, new DataSnapshotId(datasetID), 
				file.getFileName());
//		for (RecordingObject o: objects) {
//			Path p = Paths.get(o.getName());
//			if (p.getFileName().toString().equals(file.getLabel())) {
//				return o;
//			}
//		}
//		return null;
	}

	@Override
	public String getContents(SessionToken sessionID, String datasetID, FileInfo file) 
			throws IOException {
		final String M = "loadContents()";
		User user = uService.verifyUser(M, sessionID, Role.USER);
		
		if (datasetID == null)
			throw new IOException("Snapshot not found");
		
		AuthCheckFactory.getHandler().checkPermitted(
				uService.getDataSnapshotAccessSession().getPermissions(
						user, HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME, datasetID, true),
				CorePermDefs.READ,
				datasetID);
		
		Set<RecordingObject> objects = uService.getDataSnapshotAccessSession()
				.getRecordingObjects(user, new DataSnapshotId(datasetID));
		
		for (RecordingObject o: objects) {
			if (o.getName().equals(file.getLabel())) {
				IStoredObjectReference handle = null;
				IInputStream stream = null;
				try {
					handle = uService.getDataSnapshotAccessSession()
							.getRecordingObjectHandle(user, o);

					if (MimeTypeRecognizer.getMimeTypeForFile(file.getLabel()).equals("application/mat")) {
						try {
							Class<IJsonExtractor> cl = (Class<IJsonExtractor>) 
									Class.forName("com.blackfynn.dsp.processing.actions.convert.matlab.MatlabToJson");
							IJsonExtractor extractor = cl.newInstance();
							JsonNode node = null;
							synchronized (this) {
								extractor.initialize((FileReference)handle);
								
								node = extractor.createJson(500, true);
							}
							if (node != null)
								return node.toString();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					IObjectServer server =
							StorageFactory.getBestServerFor(handle);

					stream = server.openForInput(handle);

					try (InputStream inputStream = stream.getInputStream()) {
						final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
						ByteStreams.copy(inputStream, outStream);
						return outStream.toString("UTF-8");
					}
					
				} catch (RecordingObjectNotFoundException e) {
					throw new IOException("File object not found", e);
				} finally {
					if (stream != null) {
						stream.close();
					}
				}
			}
		}
		
		throw new IOException("File object not found");
//		return "";
	}

	
	@Override
	public void saveContents(SessionToken sessionID, String datasetID, FileInfo file,
			String contents) throws IOException {
		final String M = "saveContents()";
		User user = uService.verifyUser(M, sessionID, Role.USER);
		
		DataSnapshotEntity dsEntity = uService
				.getDataSnapshotAccessSession().getDataSnapshotFromCache(
						user, datasetID);

		AuthCheckFactory.getHandler().checkPermitted(
				uService.getDataSnapshotAccessSession().getPermissions(
						user, HasAclType.DATA_SNAPSHOT,
						CorePermDefs.CORE_MODE_NAME, datasetID, true),
				CorePermDefs.EDIT,
				datasetID);

		Set<RecordingObject> objects = uService.getDataSnapshotAccessSession()
				.getRecordingObjects(user, new DataSnapshotId(datasetID));
		
		for (RecordingObject o: objects) {
			if (o.getName().equals(file.getLabel())) {
				FileReference handle = new FileReference(fc, o.getName(), o.getName());
				IObjectServer server = 
						StorageFactory.getBestServerFor(handle);
				
				IOutputStream stream = server.openForOutput(handle);
				
				PrintWriter writer = new PrintWriter(stream.getOutputStream());
				writer.print(contents);
				break;
			}
		}
	}

	@Override
	public RecordingObject createRecordingObject(
			SessionToken sessionID,
			String datasetId,
			byte[] input,
			String filename,
			String contentType,
			boolean test) {
		int contentLength = input.length;
		String md5Hash;
		final String M = "createRecordingObject(...)";
		try {
			md5Hash = HashCode.fromBytes(
					MessageDigest.getInstance("MD5").digest(input)).toString();
		} catch (NoSuchAlgorithmException e) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					e,
					M,
					logger);
		}

		return createRecordingObject(sessionID,
				datasetId,
				new ByteArrayInputStream(input),
				contentLength,
				md5Hash,
				filename,
				contentType,
				test);
	}

	public RecordingObject createRecordingObject(SessionToken sessionID,
			String datasetId, InputStream input, long contentLength, String md5Hash, String filename,
			String contentType, boolean test) {
		final String M = "createRecordingObject(...)";
		try {
			final RecObjectUploadSemaphoresFactory.InProgressKey upload = new RecObjectUploadSemaphoresFactory.InProgressKey(
					datasetId,
					filename);

			final User user = uService.verifyUser(M, sessionID, null);

			final RecordingObject recordingObject = new RecordingObject(
					filename,
					md5Hash,
					contentLength,
					user.getUsername(),
					contentType,
					datasetId);
			final RecordingObject createdObject = uService.getDataSnapshotAccessSession()
					.createRecordingObject(
							user,
							recordingObject,
							input,
							true);

			return createdObject;
		} catch (AuthorizationException e) {
			throw IeegWsExceptionUtil.logAndConvertToAuthzFailure(
					e,
					M,
					logger);
		} catch (BadDigestException e) {
			throw IeegWsExceptionUtil.logAndConvertToBadDigest(
					e,
					M,
					logger);
		} catch (BadRecordingObjectNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToBadRecordingObjectName(
					e,
					M,
					logger);
		} catch (DuplicateNameException e) {
			throw IeegWsExceptionUtil.logAndConvertToDuplicateName(
					e,
					M,
					logger);
		} catch (RecordingNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchRecordingFailure(
					e,
					M,
					logger);
		} catch (DataSnapshotNotFoundException e) {
			throw IeegWsExceptionUtil.logAndConvertToNoSuchDsFailure(
					e,
					M,
					logger);
		} catch (RuntimeException t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					M,
					logger);
		} catch (Error t) {
			throw IeegWsExceptionUtil.logAndConvertToInternalError(
					t,
					M,
					logger);
		}
	}

	@Override
	public List<RecordingObject> attachFiles(String target, String snapshot,
			SessionToken sessionID, List<String> files) {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<DataBundle> getContentInfo(SessionToken sessionToken, String dataSnapshot) throws BtSecurityException {
		final String M = "getContentInfo";
		Set<DataBundle> ret = new HashSet<DataBundle>();
		try {
			User user = uService.verifyUser(M, sessionToken, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, dataSnapshot, true),
					CorePermDefs.READ,
					dataSnapshot);

			DataSnapshot ds = uService.getDataSnapshotAccessSession().getDataSnapshot(user, dataSnapshot);

			// All of the time series data
			if (!ds.getTimeSeries().isEmpty()) {
				DataBundle bundle = new DataBundle("Time Series");
				for (TimeSeriesDto ts: ds.getTimeSeries()) {
					bundle.getObjectMimeTypes().put(
							ts.getId(), "application/x-mef");
					
					bundle.addObjectMetadata(ts.getId(), 
							new TraceInfo(null, ts.getId()));
				}
				List<TraceInfo > channelList = getTraceInfo(user, dataSnapshot);
				for (TraceInfo ch: channelList)
					bundle.addObjectMetadata(ch.getId(), ch);
				
				ret.add(bundle);
			}
				

			// All of the "recording objects"
			//
			// TODO: we can "bundle" some of these together by prefix if that makes sense, e.g.,
			//       by type, path prefix, etc.
			Set<RecordingObject> recObj = uService.getDataSnapshotAccessSession().getRecordingObjects(user, new DataSnapshotId(dataSnapshot));
			for (RecordingObject obj: recObj) {
				DataBundle bundle = new DataBundle(obj.getDescription());
				bundle.getObjectMimeTypes().put(
						dataSnapshot + "/" +
						String.valueOf(obj.getId()), obj.getInternetMediaType());
				ret.add(bundle);
				
				FileInfo si = new FileInfo(obj
						.getName(), dataSnapshot, obj.getDescription(),//ds.getLabel(),
						obj.getId().toString(),
						obj.getInternetMediaType());
				
				bundle.addObjectMetadata(dataSnapshot + "/" +
						obj.getId(), si);
			}
			return ret;
		} catch (Throwable t) {
			propagate(t);
		}
		
		return ret;
	}

	@Override
	public CaseMetadata getCaseMetadata(SessionToken sessionID, String studyRevId) throws BtSecurityException {
		CaseMetadata ret = null;
		try {
			User user = uService.verifyUser("getCaseMetadata", sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, studyRevId, true),
					CorePermDefs.READ,
					studyRevId);
		} catch (Throwable t) {
			propagate(t);
		}
		
		try {
			ret = this.getEegStudyMetadata(sessionID, studyRevId);
		} catch (Exception e) {
			
		}

		try {
			if (ret == null)
				ret = this.getExperimentMetadata(sessionID, studyRevId);
		} catch (Exception e) {
			
		}
		return ret;
	}
	
//	@Override
//	public Map<TsEventType, INamedTimeSegment> getSegments(
//			SessionToken sessionID, 
//			IDataset dataset, 
//			List<INamedTimeSegment> segments, 
//			TsEventType partitionOn) throws IOException {
//		try {
//			User user = uService.verifyUser("getSegments", sessionID, null);
//			AuthCheckFactory.getHandler().checkPermitted(
//					uService.getDataSnapshotAccessSession().getPermissions(
//							user, HasAclType.DATA_SNAPSHOT,
//							CorePermDefs.CORE_MODE_NAME, dataset.getId(), true),
//					CorePermDefs.READ,
//					dataset.getId());
//			
//			IStoredObjectReference eventFile = getEventHandle(user, dataset);
//			
//			if (eventFile != null)
//				return events.getSegments(dataset, eventFile, segments, partitionOn);
//
//		} catch (Throwable t) {
//			propagate(t);
//		}
//
//		return new HashMap<TsEventType, INamedTimeSegment>();
//	}
//	
//	@Override
//	public SortedSet<INamedTimeSegment> getSegments(
//			SessionToken sessionID, 
//			IDataset dataset, 
//			List<INamedTimeSegment> segments) throws IOException {
//		try {
//			User user = uService.verifyUser("getSegments", sessionID, null);
//			AuthCheckFactory.getHandler().checkPermitted(
//					uService.getDataSnapshotAccessSession().getPermissions(
//							user, HasAclType.DATA_SNAPSHOT,
//							CorePermDefs.CORE_MODE_NAME, dataset.getId(), true),
//					CorePermDefs.READ,
//					dataset.getId());
//
//			IStoredObjectReference eventFile = getEventHandle(user, dataset);
//			
//			if (eventFile != null)
//				return events.getSegments(dataset, eventFile, segments);
//
//		} catch (Throwable t) {
//			propagate(t);
//		}
//
//		return new TreeSet<INamedTimeSegment>();
//	}

//	@Override
//	public Map<INamedTimeSegment, List<IDetection>> getEvents(SessionToken sessionID, 
//			IDataset dataset, 
//			List<INamedTimeSegment> segments, 
//			TsEventType partitionOn)
//			throws IOException {
//		try {
//			User user = uService.verifyUser("getEvents", sessionID, null);
//			AuthCheckFactory.getHandler().checkPermitted(
//					uService.getDataSnapshotAccessSession().getPermissions(
//							user, HasAclType.DATA_SNAPSHOT,
//							CorePermDefs.CORE_MODE_NAME, dataset.getId(), true),
//					CorePermDefs.READ,
//					dataset.getId());
//
//			IStoredObjectReference eventFile = getEventHandle(user, dataset);
//			
//			if (eventFile != null)
//				return events.getEvents(dataset, eventFile, segments, partitionOn);
//
//		} catch (Throwable t) {
//			propagate(t);
//		}
//
//		return new HashMap<INamedTimeSegment, List<IDetection>>();
//	}

	@Override
	public Map<INamedTimeSegment, int[]> getEventsAsSamples(SessionToken sessionID, IDataset dataset,
			List<INamedTimeSegment> segments, double start, double end, double period) throws IOException, BtSecurityException {
		try {
			User user = uService.verifyUser("getEvents", sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, dataset.getId(), true),
					CorePermDefs.READ,
					dataset.getId());

			IStoredObjectReference eventFile = getEventHandle(user, dataset);
			
			if (eventFile != null)
				return events.getEventVector(dataset, eventFile, segments, start, start, end, period, false);

		} catch (Throwable t) {
			propagate(t);
		}

		return new HashMap<INamedTimeSegment, int[]>();
	}
	
	IStoredObjectReference getEventHandle(User user, IDataset dataset) throws RecordingObjectNotFoundException {
		Set<RecordingObject> objects = uService.getDataSnapshotAccessSession()
				.getRecordingObjects(user, new DataSnapshotId(dataset.getId()));

		String filename = dataset.getFriendlyName() + ".events";
		if (events != null) {
				for (RecordingObject o: objects) {
					if (o.getName().equals(filename)) {
						IStoredObjectReference handle =
								uService
								.getDataSnapshotAccessSession().getRecordingObjectHandle(user, o);
						
						return handle;
					}
				}
		}
		return null;
	}

	@Override
	public List<INamedTimeSegment> getEventChannels(SessionToken sessionID, IDataset dataset, List<INamedTimeSegment> channels) {
		try {
			User user = uService.verifyUser("getEvents", sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, dataset.getId(), true),
					CorePermDefs.READ,
					dataset.getId());
			
			List<INamedTimeSegment> segments = crService.getEventChannels(user, dataset.getId(), true);
			
//			if (!(segments == null || segments.isEmpty()))
//				return segments;
			
			List<INamedTimeSegment> ret;
			IStoredObjectReference eventFile = getEventHandle(user, dataset);
			
			long start = Long.MAX_VALUE;
			long end = 0;
			double freq = Double.MAX_VALUE;

			for (INamedTimeSegment ts: channels) {
				if (ts.getStartTime() < start)
					start = ts.getStartTime();
				if (ts.getStartTime() + ts.getDuration() > end)
					end = (long)(ts.getStartTime() + ts.getDuration());
				
				if (ts.getSampleRate() < freq)
					freq = ts.getSampleRate();
			}

			if ((segments == null || segments.isEmpty())) {
				if (eventFile != null) {
					ret = events.getEventChannels(dataset, channels, eventFile);
					
					crService.storeEventChannels(user, dataset.getId(), ret, true);
				} else
					ret = new ArrayList<INamedTimeSegment>();
			} else
				ret = segments;
			
//			if (!ret.isEmpty()) {
//				Map<TsEventType, List<INamedTimeSegment>> windows = 
//						events.getSegmentMap(dataset, eventFile, channels, 0, start, end, 1.e6 / freq);
//				
//				for (TsEventType ev: windows.keySet())
//					ret.addAll(windows.get(ev));
//			}
			
			return ret;
		} catch (Throwable t) {
			propagate(t);
		}

		return new ArrayList<INamedTimeSegment>();
	}

//	@Override
//	public edu.upenn.cis.db.mefview.shared.TimeSeriesData getResultsArrayBuffer(SessionToken sessionID,
//			String contextID, IDataset dataset, List<INamedTimeSegment> traceids, double start, double width,
//			double samplingPeriod, List<DisplayConfiguration> filter, SignalProcessingStep processing)
//					throws BtSecurityException, IeegException, ServerTimeoutException {
//		edu.upenn.cis.db.mefview.shared.TimeSeriesData tsData = new edu.upenn.cis.db.mefview.shared.TimeSeriesData();
//		
//		tsData.setDatasetID(dataset.getId());
//		tsData.setContextID(contextID);
//		tsData.setPeriod(samplingPeriod);
//		tsData.setStartIndex(start);
//		
//		int[] arr = new int[100];
//		for (int i = 0; i < 100; i++)
//			arr[i] = i;
//		
//		tsData.getBuffer().set(arr);
//		
//		return tsData;
//	}


	/*
	GetMyDatasets is used by ActivityPresenter to populate cards.
	It currently loads all datasets uploaded by the user as well as datasets shared by OrcaBot.
	 */
	@Override
	public Set<DatasetPreview> getMyDatasets(SessionToken sessionID) {
		final String M = "getMyDataset()";
		try {

			// Get user Data
			User u = uService.verifyUser(M, sessionID, null);
			Set<PresentableMetadata> search = crService.getMyData(uService, u, null);
			
			Set<PresentableMetadata> publicData = new HashSet<PresentableMetadata>();
			try {
				publicData.addAll(crService.getPublicData(u));
			} catch (Throwable t) {
				t.printStackTrace();
			}

			try {
				// Get Public data from orcabot
				User botUser = uService.getEnabledUser("orcabot@blackfynn.com");
	
				// Filter public data by the user
				Set<PresentableMetadata> botData = crService.getMyData(uService, botUser, null);
	
				Set<PresentableMetadata> intersect = new HashSet<PresentableMetadata>();
				for (PresentableMetadata pd: publicData)
					if (botData.contains(pd))
						intersect.add(pd);
				
				for (PresentableMetadata pd: search)
					if (publicData.contains(pd))
						intersect.add(pd);
	
				publicData = intersect;
				botData.clear();
			} catch (Throwable t) {
				t.printStackTrace();
			}
			
			Set<PresentableMetadata> shared = null;
			
			try {
				BasicCollectionNode in = crService.getIncomingFolder(u.getUsername());
				shared = crService.getIncomingFolderContents(u, in);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getLocalizedMessage());
			}
			
			if (shared == null)
				shared = new HashSet<PresentableMetadata>();
			
			if (search != null)
				shared.addAll(search);
			
			shared.addAll(publicData);
			
			String home = IvProps.getSiteUrl() + 
					"/homes/" + u.getUsername();
					

			Set<DatasetPreview> dsets = new HashSet<DatasetPreview>();
			
				for (PresentableMetadata sr: shared) {
					
					// Skip "Shared with Me" folder...
					if (sr instanceof BasicCollectionNode && sr.getFriendlyName().equals(SpecialPaths.Incoming))// && ((BasicCollectionNode)sr).getKey().endsWith(SpecialPaths.PIncoming))
						continue;
					try {
						Set<TypeAndMime> entries = new HashSet<TypeAndMime>();
						
						if (sr instanceof SearchResult) {
							SnapshotContents cont = this.getSnapshotMetadata(sessionID, sr.getId());
							
							// TODO: get details
							for (FileInfo fi: cont.getFileContent()) {
								if (fi.getMediaType() == null || fi.getMediaType().isEmpty()) {
									entries.add(new TypeAndMime(MimeTypeRecognizer.getMimeTypeForFile(fi.getDetails()), ""));
								} else
									entries.add(new TypeAndMime(fi.getMediaType(), ""));
							}
							
							for (INamedTimeSegment seg: cont.getTraces()) {
								if (seg.isEventBased())
									entries.add(new TypeAndMime("application/x-blackfynn-events", ""));
								else
									entries.add(new TypeAndMime("application/mef", ""));
							}
						}
						String path = home;
						if (publicData.contains(sr))
							path = path + SpecialPaths.PPublic;
						else if (!search.contains(sr))
							path = path + SpecialPaths.PIncoming;
						
						DatasetPreview ds = new DatasetPreview(sr.getFriendlyName(), sr.getId(), 
								path,
								entries,sr, !search.contains(sr));
						
						if (sr instanceof SearchResult) {
							if (search.contains(sr)) {
								ds.setEditable(((SearchResult)sr).isEditable());
							} else if (shared.contains(sr)) {
								ds.setEditable(((SearchResult)sr).isEditable());
								ds.setIncoming(true);
							} 
						}
						
						// If we don't own it directly, or it wasn't shared with us,
						// for now we mandate it's public
						if (!search.contains(sr) && publicData.contains(sr))
							ds.setEditable(false);
						
						// If it's officially public, we want to mark this
						if (publicData.contains(sr))
							ds.setPublic(true);
						
						dsets.add(ds);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			return dsets;

		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	  @Override
	  public Boolean removeDataSnapshot(SessionToken sessionID, String revID)
	      throws BtSecurityException {
	    final String M = "removeDataSnapshot()";
	    try {
	      User u = uService.verifyUser(M, sessionID, Role.USER);
	    	try {
		      uService.getDataSnapshotAccessSession().deleteDataset(
						u, 
						new DatasetIdAndVersion(revID, -1),
						false,
						false);
	    	} catch (DataSnapshotNotFoundException dnf) {
	    		
	    	}

	      UserId userId = u.getUserId();
	      if (snapshots.containsKey(userId))
	        snapshots.get(userId).remove(revID);
	      
	      if (crService != null)
	    	  crService.removeDataset(revID, u);
	      return true;
	    } catch (Throwable t) {
	      logger.error(M + ": Caught exception", t);
	      propagateIfInstanceOf(t, BtSecurityException.class);
	      throw propagate(t);
	    }
	  }

	@Override
	public Set<AnnotationModification> updateAnnotations(
			SessionToken sessionID, 
			String datasetID, 
			Collection<AnnotationModification> updates,
			long lastTime) {
		final String M = "updateAnnotations()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.EDIT,
					datasetID);
			
			long time = new Date().getTime();
			for (AnnotationModification ann: updates) {
				if (ann.getWhen() == 0 || ann.getId().isEmpty()) {
					ann.setWhen(time);
					
					ann.setId(BtUtil.newUuid());
				}
				// Create a new UUID for each revision or insertion
				if (ann.getNext() != null) {
					if (ann.getNext().isNew() || 
							(ann.getPrior() != null && 
								ann.getPrior().getRevId().equals(ann.getNext().getRevId()))) {
						ann.getNext().setRevAndInternalId(BtUtil.newUuid());
					}
				}
				if (ann.getPrior() != null)
					ann.getPrior().setNew(false);
				if (ann.getNext() != null)
					ann.getNext().setNew(false);
			}

			if (!updates.isEmpty())
				logger.info("updateAnnotations: modification " + updates.iterator().next());
			crService.saveAnnotationModifications(user, datasetID, updates);
			
			Set<AnnotationModification> ret = getAnnotationsSince(sessionID, datasetID, lastTime);
			
			for (AnnotationModification ann: ret) {
				if (updates.contains(ann)) {
					if (ann.getPrior() != null)
						ann.getPrior().setNew(true);
					if (ann.getNext() != null)
						ann.getNext().setNew(true);
				}
			}
			
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public Set<AnnotationModification> getAnnotationsSince(
			SessionToken sessionID, 
			String datasetID, 
			long timestamp) {
		Set<AnnotationModification> set = new HashSet<AnnotationModification>();
		final String M = "getAnnotationsSince()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
		
			Set<AnnotationModification> fullSet = crService.getAnnotationModifications(user, datasetID);
			
			for (AnnotationModification mod: fullSet) {
				if (mod.getWhen() > timestamp) {
					set.add(mod);
					logger.info("getAnnotations: modification " + mod);
				}
			}
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
		return set;
	}

	@Override
	public boolean isExistingDatasetWithName(SessionToken sessionID, String datasetName) throws BtSecurityException {
		final String M = "isExistingDatasetWithName()";
		try {
			User user = uService.verifyUser(M, sessionID, null);

			try {
				return uService.getDataSnapshotAccessSession().getDataSnapshotId(user, datasetName, true) != null;
			} catch (DataSnapshotNotFoundException dnf) {
				return false;
			}
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void acceptShare(SessionToken sessionID, BasicCollectionNode target, PresentableMetadata object) throws BtSecurityException {
		final String M = "acceptShare()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			
			if (crService == null)
				return;
			
			if (target == null)
				target = crService.getHomeFolder(user);
			
			crService.acceptShare(object, target, user);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void rejectShare(SessionToken sessionID, PresentableMetadata object) throws BtSecurityException {
		final String M = "rejectShare()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			
			if (crService == null)
				return;
			
			crService.rejectShare(object, user);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public Set<String> inviteToShare(SessionToken sessionID, Set<String> email, String note, 
			PresentableMetadata other, Set<Integer> roles) throws BtSecurityException {
		final String M = "inviteToShare()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			
			if (crService == null)
				return new HashSet<String>();
			
			// Need GRANT permissions to share
			if (!crService.isObjectGranter(user, other)) {
				throw new BtSecurityException("No authorization to grant rights to " + other.getFriendlyName());
			}
			
			// An Admin can grant all rights
			if (crService.isObjectAdmin(user, other)) {
				;
				
			// Updater + Granter an grant all rights except admin
			} else if (!crService.isObjectUpdater(user, other)) {
				roles.remove(SecurityRoles.DB_ADMIN);
				roles.remove(SecurityRoles.ORG_ADMIN);
//				roles.remove(SecurityRoles.GRANTER);
				
			} else {
				roles.remove(SecurityRoles.DB_ADMIN);
				roles.remove(SecurityRoles.ORG_ADMIN);
				roles.remove(SecurityRoles.UPDATER);
				
			}

			Set<String> failedToDeliver = new HashSet<String>();
			
			for (String address: email)
				try {
					UserEvent.logEvent(UserEvent.EventType.SHARE, user.getUsername()+" sharing with "+address);

					if (crService != null) {
//						BasicCollectionNode incoming = crService.getIncomingFolder(address);
						
						crService.addToIncoming(other, address, user, roles);
					}
					
					if (!address.endsWith("test.com"))
					mailServer.sendMessage(address, 
							user.getUsername() + " wants to share data with you via Blackfynn", 
							"Blackfynn Orca <invites@blackfynn.com>", 
							"Hi there!\n\nYour colleague " + user.getUsername() + " wants to share " +
							" a dataset " + other.getFriendlyName() + " with you on Blackfynn Orca.\n\n" +
							((note == null) ? "" : note +
							"\n\n\n===================\n") +
							"\n\nBlackfynn makes scientific data management simple. Upload, search, view, analyze and share your data." +
							"\n\nJoin for a free trial of Blackfynn's Orca platform: sign up at http://www.blackfynn.net\n\n"
							);
					
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Failed share or send: " + e.getMessage());
					failedToDeliver.add(address);
				}
			return failedToDeliver;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Deprecated
	@Override
	public Set<String> inviteToShare(SessionToken sessionID, Set<String> email, String note, 
			DatasetPreview dataset, Set<Integer> roles) throws BtSecurityException {
		final String M = "inviteToShare()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, dataset.getDatasetID(), true),
					CorePermDefs.READ,
					dataset.getDatasetID());


			Set<String> failedToDeliver = new HashSet<String>();
			
			for (String address: email)
				try {
					UserEvent.logEvent(UserEvent.EventType.SHARE, user.getUsername()+" sharing with "+address);

					if (crService != null) {
						BasicCollectionNode incoming = crService.getIncomingFolder(address);
						
						final DataSnapshotSearchResult dsr = 
								uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, dataset.getDatasetID());
						
						crService.addToIncoming(dsr, address, user, roles);
					}
					
					if (!address.endsWith("test.com"))
					mailServer.sendMessage(address, 
							user.getUsername() + " wants to share data with you via Blackfynn", 
							"Blackfynn Orca <invites@blackfynn.com>", 
							"Hi there!\n\nYour colleague " + user.getUsername() + " wants to share " +
							" a dataset " + dataset.getTitle() + " with you on Blackfynn Orca.\n\n" +
							((note == null) ? "" : note +
							"\n\n\n===================\n") +
							"\n\nBlackfynn makes scientific data management simple. Upload, search, view, analyze and share your data." +
							"\n\nJoin for a free trial of Blackfynn's Orca platform: sign up at http://www.blackfynn.net\n\n"
							);
					
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Failed share or send: " + e.getMessage());
					failedToDeliver.add(address);
				}
			return failedToDeliver;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

//	@Override
//	public Boolean addDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels)
//			throws BtSecurityException {
//		final String M = "addDatasetChannels()";
//		try {
//			User user = uService.verifyUser(M, sessionID, null);
//			AuthCheckFactory.getHandler().checkPermitted(
//					uService.getDataSnapshotAccessSession().getPermissions(
//							user, HasAclType.DATA_SNAPSHOT,
//							CorePermDefs.CORE_MODE_NAME, datasetID, true),
//					CorePermDefs.EDIT,
//					datasetID);
//			
//			// TODO  We need to get low-level ChannelInfoDto info etc.
//			throw new UnsupportedOperationException("Operation not yet supported");
//		} catch (Throwable t) {
//			logger.error(M + ": Caught exception", t);
//			propagateIfInstanceOf(t, BtSecurityException.class);
//			throw propagate(t);
//		}
//	}

	@Override
	public Boolean addDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects) throws BtSecurityException {
		final String M = "addDatasetObjects()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.EDIT,
					datasetID);
			Set<RecordingObject> recObjects = new HashSet<RecordingObject>();
			for (FileInfo fi: objects)
				recObjects.add(getRecordingObjectFromFileInfo(user, datasetID, fi));
			
			IObjectServer os = StorageFactory.getPersistentServer();
			final DataSnapshotSearchResult dataset = 
					uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
			SearchResult sr = DataMarshalling.convertSearchResult(dataset, user, null);
			
			for (FileInfo fo: objects) {
				FileReference ref = new FileReference((DirectoryBucketContainer)os.getDefaultContainer(),
						fo.getFileName(), fo.getFileName());
				IInputStream str = os.openForInput(ref);

				uService.getDataSnapshotAccessSession().addAsRecordingObject(user, fo.getFileName(), 
						fo.getDetails(), fo.getMediaType(), (int)str.getLength(), datasetID);
				str.close();

				if (crService != null) {
					crService.storeMetadataObject(user, sr, fo, StorageFactory.getPersistentServer(), 
							ref);
					
					crService.indexDocument(user, sr, fo, StorageFactory.getPersistentServer(), 
							ref);
				}
			}
			
			
			return new Boolean(true);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Boolean removeDatasetChannels(SessionToken sessionID, String datasetID, Collection<INamedTimeSegment> channels)
			throws BtSecurityException {
		final String M = "addDatasetChannels()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.EDIT,
					datasetID);
			
			uService.getDataSnapshotAccessSession().deleteDatasetChannels(user, 
					new DatasetIdAndVersion(datasetID,-1), channels);
			
			if (crService != null) {
				final DataSnapshotSearchResult dataset = 
						uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
				SearchResult sr = DataMarshalling.convertSearchResult(dataset, user, null);
				for (INamedTimeSegment seg: channels)
					crService.removeTimeSeriesObject(user, sr, (TraceInfo)seg);
			}

			return new Boolean(true);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Boolean removeDatasetObjects(SessionToken sessionID, String datasetID, Collection<FileInfo> objects)
			throws BtSecurityException {
		final String M = "inviteToShare()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.EDIT,
					datasetID);
			Set<RecordingObject> recObjects = new HashSet<RecordingObject>();
			for (FileInfo fi: objects)
				recObjects.add(getRecordingObjectFromFileInfo(user, datasetID, fi));
			
			uService.getDataSnapshotAccessSession().deleteDatasetObjects(user, 
					new DatasetIdAndVersion(datasetID,-1), recObjects);
			
			if (crService != null) {
				final DataSnapshotSearchResult dataset = 
						uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
				SearchResult sr = DataMarshalling.convertSearchResult(dataset, user, null);
				for (RecordingObject object: recObjects)
					crService.removeMetadata(user, sr, object);
			}
			
			return new Boolean(true);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	

	@Override
	public void setPublicAccess(SessionToken sessionID, String datasetID, Set<Integer> access) throws BtSecurityException {
		final String M = "setPublicAccess()";
		try {
			User user = uService.verifyUser(M, sessionID, null);
			AuthCheckFactory.getHandler().checkPermitted(
					uService.getDataSnapshotAccessSession().getPermissions(
							user, HasAclType.DATA_SNAPSHOT,
							CorePermDefs.CORE_MODE_NAME, datasetID, true),
					CorePermDefs.EDIT,
					datasetID);

			final DataSnapshotSearchResult dataset = 
					uService.getDataSnapshotAccessSession().getDataSnapshotForId(user, datasetID);
			SearchResult sr = DataMarshalling.convertSearchResult(dataset, user, null);

			crService.setPublicRoles(user, sr, access);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
}
