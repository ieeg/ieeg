/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;

public class ResourceFactory {
	static MefResources mefRes = null;
	
	static ResourceFactory instance = null;
	
	public static MefResources getResources() {
		if (mefRes == null)
			mefRes = GWT.create(MefResources.class);
		
		return mefRes;
	}
	
	public static ImageResource getLeftImage() {
		return getResources().getLeftImage();
	}

	public static ImageResource getRightImage() {
		return getResources().getRightImage();
	}

	public static ImageResource getData() {
		return getResources().getData();
	}

	
	public static ImageResource getBrain() {
		return getResources().getBrain();
	}
	
	
    public static ImageResource getImageTab() {
        return getResources().getImageTab();
    }

	
	public static ImageResource getHouse() {
	    return getResources().getHouse();
	}
	
	
	public static ImageResource getChat() {
		return getResources().getChat();
	}

	
	public static ImageResource getDocument() {
		return getResources().getDocument();
	}

	
	public static ImageResource getFilter() {
		return getResources().getFilter();
	}

	
	public static ImageResource getMarker() {
		return getResources().getMarker();
	}

	
	public static ImageResource getMagnifier() {
		return getResources().getMagnifier();
	}

	
	public static ImageResource getTinyMagnifier() {
		return getResources().getTinyMagnifier();
	}
	
	
	public static ImageResource getShare() {
	    return getResources().getShare();
	}

	
	public static ImageResource getStudy() {
		return getResources().getStudy();
	}

	
	public static ImageResource getTools() {
		return getResources().getTools();
	}

	
	public static ImageResource getSignal() {
		return getResources().getSignal();
	}

	
	public static ImageResource getPenn() {
		return getResources().getPenn();
	}

	
	public static ImageResource getMayo() {
		return getResources().getMayo();
	}

	
	public static ImageResource getNinds() {
		return getResources().getNinds();
	}

	
//	public static ImageResource getFreiburg() {
//		return getResources().getFreiburg();
//	}

	
	public static ImageResource getStarSel() {
		return getResources().getStarSel();
	}

	
	public static ImageResource getStarUnsel() {
		// TODO Auto-generated method stub
		return getResources().getStarUnsel();
	}

	
	public static ImageResource getDcnCaption() {
		return getResources().getDcnCaption();
	}

	
	public static ImageResource getSmallLogo() {
	    return getResources().getSmallLogo();
	}
	
	
    public static ImageResource getLoginLogo() {
        return getResources().getLoginLogo();
    }
	
	
	public static ImageResource getSave() {
		return getResources().getSave();
	}

	
	public static ImageResource getTinySave() {
		return getResources().getTinySave();
	}

	
    public static ImageResource getBook() {
        return getResources().getBook();
    }
	
	
	public static ImageResource getOpenBook() {
	  return getResources().getOpenBook();
	}

	
	public static ImageResource getAddrBook() {
	  return getResources().getAddrBook();
	}
	
	
	public static ImageResource getTinyTools() {
		return getResources().getTinyTools();
	}
	
	
    public static ImageResource getLogout() {
        return getResources().getLogout();
    }

	
	public static ImageResource getUnknownUser() {
		return getResources().getUnknownUser();
	}
	
	
    public static ImageResource getHelp() {
        return getResources().getHelp();
    }
	
	
    public static ImageResource getWork() {
        return getResources().getWork();
    }
	
	
	public static ImageResource getDiscussion() {
		return getResources().getDiscussion();
	}

	
//	public static ImageResource getUserAccReq() {
//	  return getResources().getUserAccReq();
//	}
//	
//	public static ImageResource getDwnToolbox() {
//	  return getResources().getDwnToolbox();
//	}
//
//	
//	public static ImageResource getDocButton() {
//	  return getResources().getDocButton();
//	}
	
	
//	public static ImageResource getGithubButton() {
//	  return getResources().getGithubButton();
//	}
//
//	public static ImageResource getMatlabButton() {
//      return getResources().getMatlabButton();
//    }
//	
//	
//    public static ImageResource getCoregButton() {
//        return getResources().getCoregButton();
//    }
//
//    public static ImageResource getLoniButton() {
//      return getResources().getLoniButton();
//  }
    
	
	public static ImageResource getHamburger() {
        return getResources().getHamburger();
	}

	
	public static ImageResource getRefresh() {
		return getResources().getRefresh();
	}

	
	public static ImageResource folder() {
		return getResources().folder();
	}

	
	public static ImageResource timeseries() {
		return getResources().timeseries();
	}

	
	public static ImageResource document() {
		return getResources().document();
	}
	
//	public static ImageResource getPortalEEGIm() {
//      return getResources().getPortalEEGIm();
//    }
//
//	public static ImageResource getBrainMapperIm() {
//      return getResources().getBrainMapperIm();
//    }
	
	public static ImageResource unstar() {
		return getResources().unstar();
	}

	
	public static ImageResource star() {
		return getResources().star();
	}

	public static ImageResource thumbsUp() {
		return getResources().thumbsUp();
	}
	public static ImageResource thumbsDown() {
		return getResources().thumbsDown();
	}
	public static ImageResource thumbsUnknown() {
		return getResources().thumbsUnknown();
	}
	
	public static ImageResource left() {
		return getResources().left();
	}

	
	public static ImageResource right() {
		return getResources().right();
	}
	

    public static ImageResource bold() {
    	return getResources().bold();
    }
    public static ImageResource createLink() {
    	return getResources().createLink();
    }
    public static ImageResource hr() {
    	return getResources().hr();
    }

    public static ImageResource indent() {
    	return getResources().indent();
    }

    public static ImageResource insertImage() {
    	return getResources().insertImage();
    }

    public static ImageResource italic() {
    	return getResources().italic();
    }

    public static ImageResource justifyCenter() {
    	return getResources().justifyCenter();
    }

    public static ImageResource justifyLeft() {
    	return getResources().justifyLeft();
    }

    public static ImageResource justifyRight() {
    	return getResources().justifyRight();
    }

    public static ImageResource ol() {
    	return getResources().ol();
    }
    public static ImageResource outdent() {
    	return getResources().outdent();
    }
    public static ImageResource removeFormat() {
    	return getResources().removeFormat();
    }

    public static ImageResource removeLink() {
    	return getResources().removeLink();
    }
    public static ImageResource strikeThrough() {
    	return getResources().strikeThrough();
    }
    public static ImageResource subscript() {
    	return getResources().subscript();
    }
    public static ImageResource superscript() {
    	return getResources().superscript();
    }
    public static ImageResource ul() {
    	return getResources().ul();
    }
  
    public static ImageResource underline() {
    	return getResources().underline();
    }
    
//    public static ImageResource getSloganBannerIm() {
//      return getResources().getSloganBannerIm();
//    }
    
//    public static ImageResource getMatlabBannerIm() {
//      return getResources().getMatlabBannerIm();
//    }
    
    
}
