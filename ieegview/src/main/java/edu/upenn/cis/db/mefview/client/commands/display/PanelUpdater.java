package edu.upenn.cis.db.mefview.client.commands.display;

import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.widgets.CollapsibleTab;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;

public interface PanelUpdater {
	public void addPanel(AstroPanel panel, CollapsibleTab tab, boolean closeable);
	public void removePanel(AstroPanel panel);
	
	public void addSnapshotDescriptor(String snapshot, PanelDescriptor sec);
	public void addPanelDescriptor(AstroPanel win, PanelDescriptor desc);
}
