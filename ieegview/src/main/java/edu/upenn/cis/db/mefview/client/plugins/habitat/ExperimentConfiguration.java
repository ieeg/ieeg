/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.habitat;

import java.util.Arrays;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.TextButtonCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class ExperimentConfiguration extends Composite implements HasText {

	private static ExperimentConfigurationUiBinder uiBinder = GWT
			.create(ExperimentConfigurationUiBinder.class);

	interface ExperimentConfigurationUiBinder extends
			UiBinder<Widget, ExperimentConfiguration> {
	}

	public ExperimentConfiguration() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	Button newCollection;
//	@UiField
//	Button editFuzzy;
//	@UiField
//	Button editMatchers2;
	@UiField
	Button initiate;
//	@UiField
//	Button qualityPoll;
//	@UiField
//	Button loginPoll;
	@UiField
	TextBox expName;
	@UiField
	ListBox groups;
	@UiField
	TextBox duration;
	
	public static class ConfigExperiment {
		String name;
		String details;
		Integer percent;
		
		public ConfigExperiment(String n, String d, Integer p) {
			name = n;
			details = d;
			percent = p;
		}
	}
	
	@UiField
	CellTable<ConfigExperiment> configs;

	public ExperimentConfiguration(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	/**
	 * The cell used to render categories.
	 */
	private static class IntCell extends AbstractCell<Integer> {

		public IntCell() {
		}

		@Override
		public void render(Context context, Integer value, SafeHtmlBuilder sb) {
			if (value != null) {
				sb.appendEscaped(String.valueOf(value));
			}
		}
	}
	
	  private static final List<ConfigExperiment> CONFIGS = Arrays.asList(
		      new ConfigExperiment("baseline", null, 50),
		      new ConfigExperiment("k-grams", "Edit...", 30),
		      new ConfigExperiment("matchers2", "Edit...", 20));


	public void initValues() {
		configs.addColumn(new TextColumn<ConfigExperiment>() {

			@Override
			public String getValue(ConfigExperiment object) {
				return object.name;
			}
			
		}, "Name");
		TextButtonCell bcell = new TextButtonCell();
		configs.addColumn(new Column<ConfigExperiment,String>(bcell) {

			@Override
			public String getValue(ConfigExperiment object) {
				return object.details;
			}
			
		}, "Details");
		IntCell cell = new IntCell();
		configs.addColumn(new Column<ConfigExperiment,Integer>(cell) {

			@Override
			public Integer getValue(ConfigExperiment object) {
				return object.percent;
			}
			
		}, "Name");
		
		configs.setRowCount(CONFIGS.size(), true);

	    // Push the data into the widget.
	    configs.setRowData(0, CONFIGS);		
		expName.setValue("exp2");
		duration.setValue("10");
		
		groups.addItem("group-a");
		groups.addItem("group-b");
		groups.addItem("collaborators");
		groups.addItem("Penn-CNT");
	}

	@UiHandler({
	"initiate" })
	void onClick(ClickEvent e) {
		final Object source = e.getSource();
		if (source == initiate) {

		}
	}

	public void setText(String text) {
//		button.setText(text);
	}

	public String getText() {
//		return button.getText();
		return "";
	}

}
