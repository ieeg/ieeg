/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;

import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

@JsonIgnoreProperties(ignoreUnknown=true)
public class JsonMetadata extends PresentableMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	PresentableMetadata parent;
//	MetadataFormatter presenter;
	String metadataCategory;
	
	// Must have:
	//  { id: ..., label: ..., description: ..., category: 'cat'}
	JavaScriptObject object;
	
	static HashMap<String, VALUE_TYPE> map = new HashMap<String, VALUE_TYPE>();
	static {
		map.put("id", VALUE_TYPE.STRING);
		map.put("label", VALUE_TYPE.STRING);
		map.put("description", VALUE_TYPE.STRING);
		map.put("category", VALUE_TYPE.STRING);
		map.put("type", VALUE_TYPE.STRING_SET);
	}
	
	HashMap<String, VALUE_TYPE> thisObjectMap = new HashMap<String, VALUE_TYPE>();
	HashMap<String, String> thisCatMap = new HashMap<String, String>();
	String location = "habitat://main";
	
	public JsonMetadata(PresentableMetadata parent, //MetadataFormatter presenter,
			JavaScriptObject object) {
		this.parent = parent;
//		this.presenter = presenter;
		this.object = object;
		
		// Initialize our type map based on the defaults
		for (String key: map.keySet()) {
			thisObjectMap.put(key, map.get(key));
			
			if (getKeyValue(key) == null)
				throw new RuntimeException("Key " + key + " is required in the JSON object");
		}
		
		// traverse the top-level JSON elements and populate the type map
		// and the category map
		populateMap(object);
	}
	
	native void populateMap(JavaScriptObject object) /*-{
		this.metadataCategory = object.category;
		
		for (var key in object) {
			var attrName = key;
			var attrValue = object[key]; 
			
			if (typeof(attrValue) == 'string') {
				this.thisObjectMap.put(attrName, VALUE_TYPE.STRING);
			} else if (typeof(attrValue) == 'number') {
				if (attrValue == parseInt(attrValue))
					this.thisObjectMap.put(attrName, VALUE_TYPE.INTEGER);
				else
					this.thisObjectMap.put(attrName, VALUE_TYPE.DOUBLE);
			} else if (typeOf(attrValue) == 'boolean') {
				this.thisObjectMap.put(attrName, VALUE_TYPE.STRING);
			} else if (typeOf(attrValue) == 'object' && attrValue instanceof Array) {
				if (typeof attrValue[0] == 'string') {
					this.thisObjectMap.put(attrName, VALUE_TYPE.STRING_SET);
				} else {
					this.thisObjectMap.put(attrName, VALUE_TYPE.META_SET);
				} 
			} else if (typeOf(attrValue) == 'object') {
				this.thisObjectMap.put(attrName, VALUE_TYPE.META_SET);
			}
		}
	}-*/;

	@Override
	public PresentableMetadata getParent() {
		return parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (parent instanceof PresentableMetadata)
			parent = (PresentableMetadata)p;
		else
			throw new RuntimeException("Parent must be presentable");
	}

	@Override
	public String getMetadataCategory() {
		return metadataCategory;
	}

	@Override
	public native String getLabel() /*-{
		return this.object.label;
	}-*/;

	@Override
	public native void setId(String id) /*-{
		this.object.id = id;
	}-*/;

	@Override
	public Set<String> getKeys() {
		return thisObjectMap.keySet();
	}

	@Override
	public native String getStringValue(String key) /*-{
		return this.object[key];
	}-*/;

	@Override
	public VALUE_TYPE getValueType(String key) {
		return thisObjectMap.get(key);
	}

	@Override
	public native Double getDoubleValue(String key) /*-{
		return this.object[key];
	}-*/;

	@Override
	public native Integer getIntegerValue(String key) /*-{
		return this.object[key];
	}-*/;

	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return new JsonMetadata(this, //presenter, 
				getKeyValue(key));
	}
	
	native JavaScriptObject getKeyValue(String key) /*-{
		return this.object[key];
	}-*/;

	native JsArrayString getKeyArrayStringValue(String key) /*-{
		return this.object[key];
	}-*/;

	native JsArray<JavaScriptObject> getKeyArrayObjectValue(String key) /*-{
		return this.object[key];
	}-*/;

	@Override
	public List<String> getStringSetValue(String key) {
		List<String> ret = new ArrayList<String>();
		JsArrayString strArr = getKeyArrayStringValue(key);
		
		for (int i = 0; i < strArr.length(); i++)
			ret.add(strArr.get(i));
		return ret;
	}

	@Override
	public List<? extends GeneralMetadata> getMetadataSetValue(String key) {
		List<JsonMetadata> ret = new ArrayList<JsonMetadata>();
		JsArray<JavaScriptObject> objArr = getKeyArrayObjectValue(key);
		
		for (int i = 0; i < objArr.length(); i++)
			ret.add(new JsonMetadata(this, //presenter, 
					objArr.get(i)));
		return ret;
	}

	@Override
	public native String getId() /*-{
		return this.object.id;
	}-*/;

	@Override
	public String getFriendlyName() {
		return getLabel();
	}

	@Override
	public List<String> getCreators() {
		return (getParent() == null) ? null : getParent().getCreators();
	}

	@Override
	public native String getContentDescriptor() /*-{
		return this.object.description;
	}-*/;

	@Override
	public Date getCreationTime() {
		return (getParent() == null) ? null : getParent().getCreationTime();
	}

	@Override
	public double getRelevanceScore() {
		return (getParent() == null) ? 0 : getParent().getRelevanceScore();
	}

	@Override
	public Map<String, Set<String>>  getUserPermissions() {
		return (getParent() == null) ? null : getParent().getUserPermissions();
	}

	@Override
	public Set<String> getAssociatedDataTypes() {
		return (getParent() == null) ? null : getParent().getAssociatedDataTypes();
	}

//	@Override
//	public MetadataFormatter getFormatter() {
//		return presenter;
//	}

	@Override
	public boolean isLeaf() {
		return true;
	}

	@JsonIgnore
	@Override
	public MetadataPresenter getPresenter() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public MetadataDrillDown getDrillDown() {
//		return null;
//	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	@JsonIgnore
	@Override
	public String getObjectName() {
		return "JSON";
	}
}
