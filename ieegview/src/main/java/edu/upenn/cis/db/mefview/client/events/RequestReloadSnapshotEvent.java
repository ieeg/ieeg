/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

//import edu.upenn.cis.db.mefview.shared.Project;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
//import edu.upenn.cis.db.mefview.shared.ToolResult;

public class RequestReloadSnapshotEvent extends GwtEvent<RequestReloadSnapshotEvent.Handler> {
	private static final Type<RequestReloadSnapshotEvent.Handler> TYPE = new Type<RequestReloadSnapshotEvent.Handler>();
	
	private final String oldId;
	private final String newId;
	
	public RequestReloadSnapshotEvent(String originalId, String newId) {
		this.oldId = originalId;
		this.newId = newId;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<RequestReloadSnapshotEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<RequestReloadSnapshotEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RequestReloadSnapshotEvent.Handler handler) {
		handler.onReloadSnapshot(oldId, newId);
	}

	public interface Handler extends EventHandler {
		void onReloadSnapshot(String oldId, String newId);
	}
}
