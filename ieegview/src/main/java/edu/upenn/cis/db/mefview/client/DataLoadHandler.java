/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;

import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.TimeSeries;

public interface DataLoadHandler {
	public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
			final double start, final double width, final double samplingPeriod, 
			final List<DisplayConfiguration> filter, final TimeSeries[] results, boolean isMinMax);
	public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
			final double start, final double width, final double samplingPeriod, 
			final List<DisplayConfiguration> filter, final String results, boolean isMinMax);
	public void process(final String user, final String dataSnapshotRevId, final List<String> traceIds,
			final double start, final double width, final double samplingPeriod, 
			final List<DisplayConfiguration> filter, final JsArray<JsArrayInteger> results, boolean isMinMax);
}


