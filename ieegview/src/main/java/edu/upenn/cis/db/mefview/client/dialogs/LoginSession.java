package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.HashSet;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.LayoutPanel;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ClientFactoryImpl;
import edu.upenn.cis.db.mefview.client.EEGDisplay;
import edu.upenn.cis.db.mefview.client.EEGDisplayAsync;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.ProjectServices;
import edu.upenn.cis.db.mefview.client.ProjectServicesAsync;
import edu.upenn.cis.db.mefview.client.SearchServices;
import edu.upenn.cis.db.mefview.client.SearchServicesAsync;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.SnapshotServices;
import edu.upenn.cis.db.mefview.client.SnapshotServicesAsync;
import edu.upenn.cis.db.mefview.client.TaskServices;
import edu.upenn.cis.db.mefview.client.TaskServicesAsync;
import edu.upenn.cis.db.mefview.client.events.ServerTitleObtainedEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.IAppPanel;
import edu.upenn.cis.db.mefview.client.plugins.ITabSet;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter;
import edu.upenn.cis.db.mefview.client.util.Md5Hash;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.shared.*;
import edu.upenn.cis.db.mefview.shared.util.ToolUploadCookies;

public class LoginSession {
	public static interface ILoginHandler {
		public void login();
	}
	
	ILoginDialog loginDialog;
	
	ClientFactory clientFactory;
	AppModel theModel;
	IAppPanel appPanel;
	IAppPresenter appPres;
	
	/**
	 * Create a remote service proxy to talk to the server-side data access service.
	 */
	private final EEGDisplayAsync eegService = GWT
			.create(EEGDisplay.class);
	
	private final ProjectServicesAsync projectService = GWT
			.create(ProjectServices.class);
	
	private final SearchServicesAsync searchService = GWT
			.create(SearchServices.class);
	
	private final SnapshotServicesAsync snapshotService = GWT
			.create(SnapshotServices.class);
	
	private final TaskServicesAsync taskService = GWT
			.create(TaskServices.class);
	
	public LoginSession() {
	}

	/**
	 * Initialize the basic session
	 * 
	 * @param myParent
	 * @param bar
	 * @param tabber
	 * @param tabset
	 * @param clientFactory
	 */
	public void initialize(
			LayoutPanel rootLayoutPanel,
			IHeaderBar bar, 
			Map<String, ? extends ITabber> tabber,
			ITabSet tabset,
			final ClientFactory clientFactory
			) {
		this.clientFactory = clientFactory;
		
		setup(rootLayoutPanel, bar, tabber, tabset);
	}

	public void autoLogin(final String uid, final String pwd, final boolean hide) {
		eegService.getServerConfiguration(null, new AsyncCallback<ServerConfiguration>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error: unable to access the server to get config info");

				autoLogin(uid, pwd, hide);
			}

			@Override
			public void onSuccess(final ServerConfiguration config) {
				getSessionModel().setServerConfiguration(config);
				if (config.getServerName().equals("IEEG.org")) {
					if (Document.get() != null) {

						Document.get().setTitle(config.getServerName());

					}
					eegService.getUserId(uid, Md5Hash.md5(pwd), new AsyncCallback<UserInfo>() {
	
						public void onFailure(Throwable caught) {
							Window.alert("Error: guest account is disabled. "
									+ caught.getMessage());
	
							loginDialog.showCentered();
						}
	
						public void onSuccess(UserInfo result) {
							GWT.log("Logged in successfully");
							setSession(result, null);
							IeegEventBusFactory.getGlobalEventBus().fireEvent(new ServerTitleObtainedEvent(config.getServerName()));
							if (hide)
								loginDialog.hide();
	
						}
	
					});
				} else {
					Document.get().setTitle(config.getServerName());
				}
			}
			
		});
	}
	
	
	public void login(@Nullable final ITabSet tabManager) {
		loginDialog.hide();

		eegService.getUserId(
				loginDialog.getUserID(), 
				Md5Hash.md5(loginDialog.getPassword()), new AsyncCallback<UserInfo>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error: invalid user ID or password: " + caught.getMessage());
				loginDialog.showCentered();
			}

			public void onSuccess(UserInfo result) {
				setSession(result, tabManager);
//				if (tabManager != null) {
//					tabManager.setLogin(result);
//				}
			}

		});
	}
	
	public void setSession(final UserInfo result, final ITabSet tabManager) {
		final SessionToken sessionToken = result.getSessionToken();
		GWT.log("Setting session prior to calling App Presenter");
		
		Cookies.setCookie(
				ToolUploadCookies.COOKIE_NAME,
				sessionToken.getId(),
				null,
				null,
				ToolUploadCookies.SERVLET_PATH,
				true);
		getSessionModel().setUserId(result.getName());
		getSessionModel().setSessionID(sessionToken);
		getSessionModel().setUserInfo(result);
		
		theModel.setSessionToken(sessionToken);
		theModel.setUserID(result.getLogin());
		eegService.getServerConfiguration(sessionToken, new AsyncCallback<ServerConfiguration>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error: failed server config info retrieval: " + caught.getMessage());
			}

			@Override
			public void onSuccess(final ServerConfiguration config) {
				getSessionModel().setServerConfiguration(config);
				appPanel.login(result,
						getSessionModel().getServerConfiguration().getServerName());
				GWT.log("Setting session and binding App Presenter");
				if (tabManager != null) {
					tabManager.setLogin(result);
				}
				appPres.bind(new HashSet<PresentableMetadata>());
				if (tabManager != null)
					tabManager.bindAll();
			}
		});
	}
	
	public void setup(
			LayoutPanel rootLayoutPanel, 
			IHeaderBar bar, 
			Map<String, ? extends ITabber> tabber, 
			ITabSet tabset) {
		clientFactory.setPortal(eegService);
		clientFactory.setPortalService(new ServerAccess(snapshotService, clientFactory));
		clientFactory.setSearchServices(searchService);
		clientFactory.setTaskServices(taskService);
		clientFactory.setProjectServices(projectService);

		theModel = new AppModel((ClientFactoryImpl)clientFactory);
		//appPanel = new AppPanel(clientFactory);
		appPanel = GWT.create(IAppPanel.class);
		appPanel.init(clientFactory, bar, tabber, tabset);
		appPres = GWT.create(IAppPresenter.class);
		appPres.init(clientFactory, theModel);//new AppPresenter(clientFactory, theModel);
		appPres.setDisplay(appPanel);

		appPanel.getElement().setAttribute("id", "main_wrapper");
		rootLayoutPanel.getElement().setAttribute("id", "rootLayoutPanel");
		rootLayoutPanel.add(appPanel);
		
		GWT.log("Requesting guest autologin");
		autoLogin(Globals.GUEST_USERNAME, "", false);
	}
	
	public void createDialog(final ITabSet tabManager) {
		loginDialog = DialogFactory.getLoginDialog(
				new DialogFactory.DialogInitializer<ILoginDialog>() {
				@Override
				public void init(ILoginDialog objectBeingInited) {
					objectBeingInited.createDialog(new ILoginHandler() {
	
						@Override
						public void login() {
							LoginSession.this.login(tabManager);
						}
					}
					);
			}

				@Override
				public void reinit(ILoginDialog objectBeingInited) {
					
				}
			
		});

		loginDialog.showCentered();
	}

	public SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
}
