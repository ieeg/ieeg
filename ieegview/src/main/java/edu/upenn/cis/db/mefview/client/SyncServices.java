/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.braintrust.shared.WorkerResponse;
import edu.upenn.cis.braintrust.shared.exception.ObjectNotFoundException;
import edu.upenn.cis.db.mefview.shared.*;

/**
 * The client side stub for the RPC service for search.
 */
@RemoteServiceRelativePath("sync")
public interface SyncServices extends RemoteService {
	
	/**
	 * Get a directory listing for the path from DropBox
	 * 
	 * @param token Session token with user credentials
	 * @param dropBoxToken Dropbox token for the user
	 * @param path Path within the Dropbox
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public Set<? extends IStoredObjectReference> getDropBoxContent(SessionToken token, 
			String dropBoxToken, String path) throws ObjectNotFoundException;
	
	/**
	 * Check that we are alive in our connection to the event bus
	 * 
	 * @param token
	 * @return
	 */
	public boolean isCommunicationActive(SessionToken token);

//	public WorkerResponse pollChannel(SessionToken token, String channel) throws ObjectNotFoundException;
	
	/**
	 * Send a request along the named channel, and queue up a reservation for an async response
	 * 
	 * @param token User credentials
	 * @param channel Channel (e.g., WorkerRequest.ETL)
	 * @param request The request (don't use the user and ID fields)
	 * @return The ID for the request, so we can get back a response
	 * 
	 * @throws BtSecurityException
	 */
	public String sendRequest(SessionToken token, String channel, WorkerRequest request) throws
	BtSecurityException;

	/**
	 * What clients does the user have online?
	 * 
	 * @param token
	 * @return
	 */
	public Set<SyncClientInfo> getSyncClients(SessionToken token);
	
	/**
	 * 
	 * @param token
	 * @param syncClient
	 * @param path
	 * @return
	 */
	public List<PresentableMetadata> getSyncMetadata(SessionToken token, String syncClient, String path);
	
//	public List<SyncClientRequestStatus> getPendingRequests(SessionToken token, String syncClient);
	
	public void requestSyncClients(SessionToken token);
	
	public void requestSyncMetadata(SessionToken token, String syncClient, String path);

	public SyncClientRequestStatus requestSync(SessionToken token, String syncClient, String path);
	
	public SyncClientRequestStatus requestIndex(SessionToken token, String syncClient, String path);

	/**
	 * Get responses to any active requests
	 * 
	 * @param token Request token
	 * @return Hash map, with key --> set of responses (might be > 1 due to multiple responders)
	 */
	Map<String, Set<WorkerResponse>> getSyncResponses(SessionToken token);

	/**
	 *
	 */
	String getCurrentUser();
}
