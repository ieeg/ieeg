package edu.upenn.cis.db.mefview.client.viewers;

import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;

import edu.upenn.cis.db.mefview.client.actions.ClickAction;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface IHeaderBar {
	public interface IWidgetCommand {
		public String getWidgetName();
		public ClickAction getHandler();
	}
	
	/**
	 * Set the portal title
	 * 
	 * @param newLabel
	 */
	public void setAppName(String newLabel);

	public void setUserInfo(UserInfo info, boolean isRegistered);
	
	public void setActivePanel(AstroPanel panel);

	public void addLogoHandler(ClickHandler handler);

	public void addTitleHandler(ClickHandler handler);
	
	public void addLogoutHandler(ClickHandler handler);

	public void addGlobalEntry(String name, ClickHandler handler);

	public void addLocalEntry(AstroPanel window, String name, ClickHandler handler);

	void removeLocalEntries(AstroPanel window);

	void addGlobalMenuOption(MenuItem item);

	void addLocalMenuOptions(AstroPanel w, List<MenuItem> items);
	
	PopupPanel getPopup();
}
