package edu.upenn.cis.db.mefview.client.dialogs;

import edu.upenn.cis.db.mefview.client.ClientFactory;

public interface ICreateProject  extends DialogFactory.DialogWithInitializer<ICreateProject> {
	public static interface Handler {
		public void create(String project);
	}
	
	public void init(final Handler responder);
	
	public String getProject();
	
	public void open();
	
	public void close();

	public void focus();

	public void clearInput();

	void setFactory(ClientFactory factory);
}
