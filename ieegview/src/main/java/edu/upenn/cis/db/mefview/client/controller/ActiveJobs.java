/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.user.client.Timer;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.events.LaunchedToolEvent;
import edu.upenn.cis.db.mefview.shared.JobInfo;

public class ActiveJobs implements LaunchedToolEvent.Handler {
	List<JobInfo> activeJobs;
	Timer poller;
	ServerAccess service;
	ClientFactory clientFactory;
	String userid;
	
	String currentSnapshot;

	public ActiveJobs(final String user, final ClientFactory factory) {
		this.service = factory.getPortalServices();
		clientFactory = factory;
		userid = user;

		activeJobs = new ArrayList<JobInfo>();
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(LaunchedToolEvent.getType(), this);
	}
	
	public void addAll(Collection<JobInfo> jobs) {
		activeJobs.addAll(jobs);
	}

	public void add(JobInfo job) {
		activeJobs.add(job);
	}
	
	public void remove(JobInfo job) {
		activeJobs.remove(job);
	}

	public void terminate(JobInfo job) {
		terminate(job.getJobId());
	}

	public void refresh(final boolean initial) {
//		if (clientFactory.isRegisteredUser())
//		service.getActiveJobs(clientFactory.getSessionID(), new SecFailureAsyncCallback.SecAlertCallback<List<JobInfo>>(clientFactory) {
//
//			@Override
//			public void onSuccess(List<JobInfo> result) {
//				
//				if (initial && !result.isEmpty()) {
//					// Create poller
//					poller = new Timer() {
//						public void run() {
//							refresh(false);
//						}
//					};
//					poller.scheduleRepeating(10000);
//				}
//				
//				System.err.println("Jobs: " + result.toString());
//				
//				HashSet<String> completedJobs = new HashSet<String>();
//				
//				for (JobInfo ji: activeJobs) {
//					for (JobInfo j: result)
//						if (j.getJobId().equals(ji.getJobId()) && j.getTasks() > 0) {
//							completedJobs.add(ji.getJobId());
//						}
//				}
//				for (String job: completedJobs) {
//					SnapshotChangedAction sna = new SnapshotChangedAction();
//					sna.originalSnapshot = job;
//					sna.newSnapshot = job;
//					clientFactory.fireEvent(new SnapshotChangedEvent(sna));
//				}
//				
//				boolean doRefresh = !completedJobs.isEmpty() ||
//					result.size() != activeJobs.size();
//
//				if (doRefresh) {
//					// Replace jobs list with the new one
//					activeJobs.clear();
//					activeJobs.addAll(result);
//					
//					// Trigger jobs list updated event -- for all jobs
//					clientFactory.fireEvent(new JobStatusChangedEvent(null));
//				}
//			}
//			
//		});
	}
	
	public void terminate(final String jobId) {
//		service.terminateJob(clientFactory.getSessionID(), jobId, new SecFailureAsyncCallback.SimpleSecFailureCallback<Boolean>(clientFactory) {
//
//			@Override
//			protected void onNonSecFailure(Throwable caught) {
//				Dialogs.messageBox("Job Termination Error", "Error terminating Snapshot " + jobId);				
//			}
//
//			@Override
//			public void onSuccess(Boolean result) {
//				Dialogs.messageBox("Job Terminated", "Snapshot " + jobId + " has been terminated");
//				
//				// TODO: remove it?
//			}
//			
//		});
	}
	
	public List<JobInfo> getActiveJobs() {
		return activeJobs;
	}
	
	public void setSnapshot(String snapshot) {
		currentSnapshot = snapshot;
	}

	@Override
	public void onLaunchedTool(ToolDto tool) {
		refresh(activeJobs.isEmpty());
	}



}
