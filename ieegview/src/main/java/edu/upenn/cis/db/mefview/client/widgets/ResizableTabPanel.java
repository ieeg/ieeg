/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.ChangedViewEvent;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

public class ResizableTabPanel extends ScrolledTabLayoutPanel implements ITabber, Attachable {
	
	int selectedTab;
	
	public ResizableTabPanel(final ClientFactory clientFactory, ImageResource leftImage, ImageResource rightImage)
	{
//		super(40, Unit.PX);
		
		super(40, Unit.PX, leftImage, rightImage);
		
		// Track the current tab selection -- there's no API for this
		addSelectionHandler(new SelectionHandler<Integer>() {
			public void onSelection(SelectionEvent<Integer> event) {
				Widget w = getWidget(event.getSelectedItem());
				selectedTab = event.getSelectedItem();
				
				if (w != null && (w instanceof AstroPanel))
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new ChangedViewEvent((AstroPanel)w));
				if (w != null && w instanceof RequiresResize)
					try {
						((RequiresResize)w).onResize();
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		});
//		
//		selectedTab = 0;
	}
	
/*
	@Override
	public void onResize() {
		for (int i = 0; i < getWidgetCount(); i++){
			Widget child = getWidget(i);
		      if (child instanceof RequiresResize) {
		        ((RequiresResize) child).onResize();
		      }
		 }
	}
	*/
	
	public int getSelectedTab() {
//		return selectedTab;
		return super.getSelectedIndex();
	}
	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
