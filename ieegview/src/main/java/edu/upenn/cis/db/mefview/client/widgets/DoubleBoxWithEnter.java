/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.DoubleBox;

public class DoubleBoxWithEnter extends DoubleBox implements Attachable {
	NumericEnterHandler _handler = null;
	
	public DoubleBoxWithEnter() {
		super();

		this.addKeyDownHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
					try {
						if (_handler != null)
							_handler.handleEnterRequest(DoubleBoxWithEnter.this.getValue());
					} catch (NumberFormatException nf) {
						setText("0");
						event.preventDefault();
					}
				}
			}
			
		});
		
		this.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (_handler != null && event.getCharCode() == KeyCodes.KEY_ENTER) {
					if (_handler != null)
						_handler.handleEnterRequest(DoubleBoxWithEnter.this.getValue());
				}
			}
		});
	}
	
	public void setEnterHandler(NumericEnterHandler handler) {
		_handler = handler;
	}

	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
