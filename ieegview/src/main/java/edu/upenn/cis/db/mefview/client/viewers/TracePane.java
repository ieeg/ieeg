/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.JsArrayBoolean;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.widgets.SimpleDataGrid;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class TracePane extends LayoutPanel {
	Button all;
	Button none;
	Button invert;

	SimpleDataGrid<INamedTimeSegment> traceTable;
	ListDataProvider<INamedTimeSegment> traceProvider = new ListDataProvider<INamedTimeSegment>();
	
	final static int HEIGHT = 8;
	public static final TextColumn<INamedTimeSegment> traceName = new TextColumn<INamedTimeSegment> () {
		@Override
		public String getValue(INamedTimeSegment object) {
			return object.getLabel();
		}
	};

	
	public TracePane() {
		
		setStyleName("ChanSelectDialog");
	  
		final HorizontalPanel selectorBar = new HorizontalPanel();

		all = new Button("All");
		selectorBar.add(all);
		all.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				
				for (INamedTimeSegment item: traceTable.getList())
					traceTable.getSelectionModel().setSelected(item, true);
			}
			

		});

		none = new Button("None");
		selectorBar.add(none);
		none.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				for (INamedTimeSegment item: traceTable.getList())
					traceTable.getSelectionModel().setSelected(item, false);
			}
			
		});
		invert = new Button("Toggle");
		selectorBar.add(invert);
		invert.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				for (INamedTimeSegment item: traceTable.getList())
					traceTable.getSelectionModel().setSelected(item, 
							!traceTable.getSelectionModel().isSelected(item));
			}
			
		});
		
		

		// Trace list
		
//		List<String> traceColNames = new ArrayList<String>();
		List<TextColumn<INamedTimeSegment>> traceColumns = new ArrayList<TextColumn<INamedTimeSegment>>();
		
//		traceColNames.add("Channel");
		traceColumns.add(TraceInfo.traceName);

		final ArrayList<INamedTimeSegment> traceList = new ArrayList<INamedTimeSegment>();
		traceTable = new SimpleDataGrid<INamedTimeSegment>(20, true, true, false,
		    traceList, TraceInfo.KEY_PROVIDER, null, 
		    traceColumns);
		
		
		// Select the current 
		
		add(traceTable);
		this.setWidgetTopHeight(traceTable, 5, Unit.PX,275, Unit.PX);
		add(selectorBar);
        this.setWidgetTopHeight(selectorBar, 0, Unit.PX,32, Unit.PX);
		
		
	}
	
	public void init(final List<? extends INamedTimeSegment> channels,
			final JsArrayBoolean selected, 
			final ClientFactory factory){
		
		setSelected(selected);
		
		traceTable.getList().addAll(channels);
		traceTable.setRowCount(traceTable.getList().size());
		
	}
	
	public void setSelected(JsArrayBoolean s) {
		int i = 0;
		for (INamedTimeSegment item: traceTable.getList()) {
			traceTable.getSelectionModel().setSelected(item, s.get(i));
			i++;
		}
	}

	public boolean[] getSelected() {
		boolean[] ret = new boolean[traceTable.getList().size()];
		int i = 0;
		for (INamedTimeSegment item: traceTable.getList()) {
			ret[i++] = traceTable.getSelectionModel().isSelected(item);
		}
		return ret;
	}

	public void setCorrectHeight(int wide, int high) {
		traceTable.setSize(wide + "px", high + "px");
		traceTable.redraw();
	}


	private native static void disableTextSelectInternal(Element e, boolean disable)/*-{
	    if (disable) {
	        e.ondrag = function () { return false; };
	        e.onselectstart = function () { return false; };
	        e.style.MozUserSelect="none"
	    } else {
	        e.ondrag = null;
	        e.onselectstart = null;
	        e.style.MozUserSelect="text"
	    }
	}-*/;

}
