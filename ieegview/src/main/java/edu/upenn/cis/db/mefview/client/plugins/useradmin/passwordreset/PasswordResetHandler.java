package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.events.CloseDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.util.Md5Hash;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class PasswordResetHandler implements ClickHandler {

	ClientFactory factory;
	PasswordResetPresenter.Display display;
	
	public PasswordResetHandler(ClientFactory factory, PasswordResetPresenter.Display display) {
		this.factory = factory;
		this.display = display;
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
		// Check password match
		if (display.getNewPassword().isEmpty() || display.getNewPassword().length() < 6) {
			display.showIllegal();
			return;
		}
		
		if (!display.getNewPassword().equals(display.getNewPassword2())) {
			display.showMismatch();
			return;
		}
		
		// TODO: Validate old password
		factory.getPortal().getUserId(
				factory.getSessionModel().getUserId(), 
				Md5Hash.md5(display.getOldPassword()), 
				new AsyncCallback<UserInfo>() {

				public void onFailure(Throwable caught) {
					display.showBadPassword();
				}
	
				public void onSuccess(UserInfo result) {
					factory.getPortal().changePassword(
							factory.getSessionModel().getSessionID(),
							Md5Hash.md5(display.getOldPassword()),
							Md5Hash.md5(display.getNewPassword()),
							new SecFailureAsyncCallback<Void>(factory) {

								@Override
								public void onSuccess(Void result) {
									display.showChangedPassword();
									IeegEventBusFactory.getGlobalEventBus().fireEvent(
											new CloseDisplayPanelEvent(display));
									
								}

								@Override
								protected void onFailureCleanup(Throwable caught) {
									display.showBadPassword();
								}

								@Override
								protected void onNonSecFailure(Throwable caught) {
									display.showBadPassword();
								}
								
							}
							);
				}

		});
		
	}
}
