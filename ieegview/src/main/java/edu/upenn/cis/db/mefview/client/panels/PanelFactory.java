/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.panels;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.MenuItem;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.PresenterSelectionHandler;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class PanelFactory {
	
	public static class PanelInfo {
		public AstroPanel panelClass;
		public boolean isSnapshotSpecific;
		
		public PanelInfo(AstroPanel myClass, boolean snapshotSpecific) {
			isSnapshotSpecific = snapshotSpecific;
			panelClass = myClass;
		}
	}
	
	final static Map<String, PanelInfo> panelMap = Maps.newHashMap();
	final static List<String> panelOrder = Lists.newArrayList();
	
	public static void register(String key, PanelInfo info) throws PanelExistsException {
		if (panelMap.get(key) != null)
			throw new PanelExistsException("Panel " + key + " was already registered!");
		
		panelOrder.add(key);
		
//		GWT.log("Registering panel for " + key);
		panelMap.put(key, info);
	}
	
	public static AstroPanel getPanel(
			final String panel,
			final ClientFactory clientFactory,
			final AppModel controller,
			final DataSnapshotViews model, 
			boolean writePermissions,
			final String title) {
		PanelInfo myClass = panelMap.get(panel);//.panelClass;

		if (myClass != null) {
			clientFactory.getSessionModel().getMainLogger().info("Creating " + panel);
			AstroPanel tab = myClass.panelClass.create(clientFactory, controller, model, 
//					project, 
					writePermissions, title);
			
			return tab;
		} else {
			clientFactory.getSessionModel().getMainLogger().log(Level.SEVERE, "Error finding panel " + panel);
			return null;
		}
	}
	
	public static ImageResource getIcon(final String panel, final ClientFactory clientFactory) {
		PanelInfo myClass = panelMap.get(panel);

		if (myClass != null) {
			return myClass.panelClass.getIcon(clientFactory);
		} else
			return null;
	}
	
//	public static WindowPlace getPlace(String type, String params) {
//		PanelInfo myClass = panelMap.get(type);
//
//		if (myClass != null) {
//			return myClass.panelClass.getPlaceFor(params);
//		} else
//			return null;
//	}

	public static DialogBox getPleaseWaitDialog(final String operation) {
		final DialogBox d = new DialogBox();
		d.setTitle("Please Wait...  " + operation + "...");
		d.setText("Please Wait...  " + operation + "...");
		return d;
	}
	
	public List<Button> generateToolbarButtons(final DataSnapshotModel model,
			final PresenterSelectionHandler handler) {
		List<Button> toolbarButtons = Lists.newArrayList();
		for (String panelType: panelOrder)
			if (panelMap.get(panelType).isSnapshotSpecific) {
				Button b = new Button(panelMap.get(panelType).panelClass.getTitle());
				toolbarButtons.add(b);
				final String pType = panelType;
				b.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						handler.select(model, pType);
					}
					
				});
			}
		
		return toolbarButtons;
	}
	
	public List<MenuItem> generateMenu(final DataSnapshotModel model, 
			final PresenterSelectionHandler handler) {
		List<MenuItem> menuItems = Lists.newArrayList();
		for (String panelType: panelOrder)
			if (panelMap.get(panelType).isSnapshotSpecific) {
				final String pType = panelType;
				menuItems.add(new MenuItem(panelMap.get(panelType).panelClass.getTitle(), 
						new Command() {

							@Override
							public void execute() {
								handler.select(model, pType);
							}
					
				}));
			}
		
		return menuItems;
	}
	
}
