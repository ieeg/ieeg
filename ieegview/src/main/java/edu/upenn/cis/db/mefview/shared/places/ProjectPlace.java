/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import edu.upenn.cis.db.mefview.client.plugins.projectsettings.ProjectPanel;

public class ProjectPlace extends WindowPlace {
	String project;
	
	public ProjectPlace() {}
	
	public ProjectPlace(String proj) {
		super();
		project = proj;
	}
	
	public String getProject() {
		return project;
	}

	@Override
	public String getType() {
		return ProjectPanel.NAME;
	}

	@Override
	public String getSnapshotID() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public String getSerializableForm() {
//		return project;
//	}
//
//	public static WindowPlace getFromSerialized(String serial) {
//		return new ProjectPlace(serial);
//	}
}
