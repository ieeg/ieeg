/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.sql.Timestamp;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class CommunityPostEvent extends GwtEvent<CommunityPostEvent.Handler>{
	private static final Type<CommunityPostEvent.Handler> TYPE = new Type<CommunityPostEvent.Handler>();
	
	private final String userId;
	private final String message;
	private final String followCode;
	private final Timestamp time;
	private final boolean isLocal;
	
	public CommunityPostEvent(final String message, final String initiator, final String followCode,
			final Timestamp time, final boolean isLocal){
		userId = initiator;
		this.message = message;
		this.followCode = followCode;
		this.time = time;
		this.isLocal = isLocal;
	}
	

	public static com.google.gwt.event.shared.GwtEvent.Type<CommunityPostEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CommunityPostEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(CommunityPostEvent.Handler handler) {
		handler.onMessagePosted(message, userId, followCode, time, isLocal);
	}

	public static interface Handler extends EventHandler {
		void onMessagePosted(String message, String userId, String followCode, Timestamp t,
				boolean isLocal);
	}

	public String getUserId() {
		return userId;
	}


	public String getMessage() {
		return message;
	}


	public String getFollowCode() {
		return followCode;
	}


	public Timestamp getTime() {
		return time;
	}


	public boolean isLocal() {
		return isLocal;
	}

}
