package edu.upenn.cis.db.mefview.client.util;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;

public class RequestHtml {
	public static String getPath(String newLabel) {
	    String path = Window.Location.getPath();
	    
	    if (path.contains("/")) {
	  	  path = path.substring(0, path.lastIndexOf('/'));
	  	  
	  	  if (!path.isEmpty())
	  		  path = path + "/";
	    }
	    if (path.startsWith("/"))
	    	path = path.substring(1);
	    
	    String protocol = Window.Location.getProtocol();
	    String urlBase = protocol + "//" + Window.Location.getHost() + "/" + path +
	    		newLabel + "/";
	    
	    return urlBase;
	}
	
	public static void getHtmlContent(
			final String url, 
			final String style, 
			final Panel container) {
		  String protocol = Window.Location.getProtocol();
	      String path = Window.Location.getPath();
	      
	      if (path.contains("/")) {
	    	  path = path.substring(0, path.lastIndexOf('/'));
	    	  if (!path.isEmpty())
	    		  path = path + "/";
	      }

	      RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(
				protocol + "//" + Window.Location.getHost() + "/" + path + url));

		try {
			builder.sendRequest(null, new RequestCallback() {
		    public void onError(Request request, Throwable exception) {
		       // Couldn't connect to server (could be timeout, SOP violation, etc.)     
		    }

		    public void onResponseReceived(Request request, Response response) {
		      if (200 == response.getStatusCode()) {
		    	  HTML result = new HTML(response.getText());
		    	  if (style != null)
		    		  result.addStyleName(style);

		          container.add(result);
		      } else {
		        // Handle the error.  Can get the status text from response.getStatusText()
		      }
		    }       
		  });
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}
	}
	


}
