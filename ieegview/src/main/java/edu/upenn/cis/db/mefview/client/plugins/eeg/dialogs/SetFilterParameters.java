/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.combo.ComboBoxChangeEvent;

import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.dialogs.IFilterDialog;
import edu.upenn.cis.db.mefview.client.events.EegFilterChangedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

public class SetFilterParameters extends DialogBox implements IFilterDialog{
    FlowPanel dialog;
    
    ListBox filters = new ListBox(false);
	
	ListBox channel = new ListBox(false);
	DoubleBox bandstopHigh = new DoubleBox();
	DoubleBox bandstopLow = new DoubleBox();
	ComboBoxWithEnter<ComboBoxDataModel> bandpassHigh = new ComboBoxWithEnter<ComboBoxDataModel>();
	ComboBoxWithEnter<ComboBoxDataModel> bandpassLow = new ComboBoxWithEnter<ComboBoxDataModel>();
	IntegerBox poles = new IntegerBox();
	
	RadioButton linear;
	RadioButton noFilter;
	RadioButton bandpassFilter;
	RadioButton highpassFilter;
	RadioButton lowpassFilter;
	CheckBox addBandstopFilter;
	
	Button okay;  // Okay Button
	Button cancel; // Cancel Button
	Button copyToAll;
	int sel = 0;  // Selector for Channel Group
	FocusPanel fp = new FocusPanel();
	List<INamedTimeSegment> traces;
	private List<DisplayConfiguration> pendingParms = new ArrayList<>();
	
	double maxFreq = 0;
	
	
	public SetFilterParameters(){
		
	}
	
	@Override
	public void init(final JsArrayString channels, 
			final List<DisplayConfiguration> parms, 
			final List<INamedTimeSegment> traces,
			final EEGMontage montage,
			final IEEGPane display, final ClientFactory factory,
			final Command doAfter){
		for (INamedTimeSegment ti: traces)
			if (maxFreq < ti.getSampleRate())
				maxFreq = ti.getSampleRate();
		
				
		setStyleName("ChanSelectDialog");
		this.traces = traces;
		setTitle("Set Channel Filters");
		setText("Set Channel Filters");

		// Add Channel Selector. The first element in channels is not a time series channel.
		for (int i = 1; i < channels.length(); i++)
			channel.addItem(channels.get(i));
		channel.setItemSelected(sel, true);
		
		for (final DisplayConfiguration initParm : parms) {
			pendingParms.add(new DisplayConfiguration(initParm));
		}

		
		final HorizontalPanel selectorBar = new HorizontalPanel();
		Label chLabel = new Label("Apply filters to channel(s):");
		chLabel.setStyleName("filterOptionLabel");
		selectorBar.add(chLabel);
		selectorBar.add(channel);
		
		final HorizontalPanel filterBar = new HorizontalPanel();
		Label filtLabel = new Label("Filter type:");
		chLabel.setStyleName("filterOptionLabel");
		filterBar.add(filtLabel);
		filterBar.add(filters);
		for (String filt: factory.getSessionModel().getFilterTypes())
			filters.addItem(filt);
		filters.setVisibleItemCount(1);
		
		// Add Filter Options
		dialog = new FlowPanel();
		dialog.add(selectorBar);
		dialog.add(filterBar);

		VerticalPanel filterType = new VerticalPanel();
		noFilter = new RadioButton("group1", "Antialias at screen res.");
		noFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					addBandstopFilter.setEnabled(false);
					bandpassLow.setEnabled(false);
					bandpassHigh.setEnabled(false);
					filters.setEnabled(false);
					poles.setEnabled(false);
//				} else {
//					bandpassLow.setEnabled(true);
//					bandpassHigh.setEnabled(true);
				}
			}
			
		});
		linear = new RadioButton("group1", "Linear interpolation");
		linear.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					addBandstopFilter.setEnabled(false);
					bandpassLow.setEnabled(false);
					bandpassHigh.setEnabled(false);
					filters.setEnabled(false);
					poles.setEnabled(false);
//				} else {
//					bandpassLow.setEnabled(true);
//					bandpassHigh.setEnabled(true);
				}
			}
			
		});
		
		ComboBoxDataModel lowpass = new ComboBoxDataModel();
		lowpass.add("10Hz", new Double(10));
		lowpass.add("15Hz", new Double(15));
		lowpass.add("25Hz", new Double(25));
		lowpass.add("30Hz", new Double(30));
		lowpass.add("35Hz", new Double(35));
		lowpass.add("40Hz", new Double(40));
		lowpass.add("50Hz", new Double(50));
		lowpass.add("60Hz", new Double(60));
		lowpass.add("70Hz", new Double(70));
		lowpass.add("100Hz", new Double(100));
		
		if (maxFreq >= 150 * 2)
			lowpass.add("150Hz", new Double(150));
		if (maxFreq >= 200 * 2)
			lowpass.add("200Hz", new Double(200));
		if (maxFreq >= 300 * 2)
			lowpass.add("300Hz", new Double(300));
		if (maxFreq >= 500 * 2)
			lowpass.add("500Hz", new Double(500));
		if (maxFreq >= 1000 * 2)
			lowpass.add("1000Hz", new Double(1000));
		if (maxFreq >= 1500 * 2)
			lowpass.add("1500Hz", new Double(1500));
		bandpassLow.setCustomTextAllowed(true);

		ComboBoxDataModel highpass = new ComboBoxDataModel();
		highpass.add("0.160 Hz", new Double(0.16));
		highpass.add("0.300 Hz", new Double(0.3));
		highpass.add("0.500 Hz", new Double(0.5));
		highpass.add("1.00 Hz", new Double(1.0));
		highpass.add("1.60 Hz", new Double(1.6));
		highpass.add("2.00 Hz", new Double(2));
		highpass.add("3.00 Hz", new Double(3));
		highpass.add("5.00 Hz", new Double(5));
		bandpassHigh.setCustomTextAllowed(true);
		
		bandpassLow.setModel(highpass);
		bandpassHigh.setModel(lowpass);
		
		bandpassLow.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				if (event instanceof ComboBoxChangeEvent){
					ComboBoxChangeEvent e = (ComboBoxChangeEvent)event;
					
					bandpassLow.setValue((Double)bandpassLow.getModel().get(e.getRow()),true);
				}
				
			}
			
		});
		bandpassHigh.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				if (event instanceof ComboBoxChangeEvent){
					ComboBoxChangeEvent e = (ComboBoxChangeEvent)event;
					
					bandpassHigh.setValue((Double)bandpassHigh.getModel().get(e.getRow()),true);
				}
				
			}
			
		});

		bandpassFilter = new RadioButton("group1", "Bandpass");
		bandpassFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					bandpassLow.setEnabled(true);
					bandpassHigh.setEnabled(true);
					addBandstopFilter.setEnabled(true);
					filters.setEnabled(true);
					poles.setEnabled(true);
				}
			}
			
		});
		highpassFilter = new RadioButton("group1", "High-pass");
		highpassFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					bandpassLow.setEnabled(true);
					bandpassHigh.setEnabled(false);
					bandpassLow.setFocus(true);
					addBandstopFilter.setEnabled(true);
					filters.setEnabled(true);
					poles.setEnabled(true);
				}
			}
			
		});
		lowpassFilter = new RadioButton("group1", "Low-pass");
		lowpassFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					bandpassLow.setEnabled(false);
					bandpassHigh.setEnabled(true);
					bandpassHigh.setFocus(true);
					addBandstopFilter.setEnabled(true);
					filters.setEnabled(true);
					poles.setEnabled(true);
				}
			}
			
		});
		

		Label selLabel = new Label("Filter selection:");
		selLabel.setStyleName("filterOptionLabel");
		filterType.add(selLabel);
		
		filterType.add(bandpassFilter);
		filterType.add(highpassFilter);
		filterType.add(lowpassFilter);
		filterType.add(noFilter);
		filterType.add(linear);
		
		Grid cutOffGrid = new Grid(3,2); 
		cutOffGrid.setText(0, 0, "Low cutoff freq.");
		cutOffGrid.setText(1, 0, "High cutoff freq.");
		cutOffGrid.setWidget(0, 1, bandpassLow);
		cutOffGrid.setWidget(1, 1, bandpassHigh);
		cutOffGrid.setText(2, 0, "Butterworth order");
        cutOffGrid.setWidget(2, 1, poles);
		
		channel.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				// Copy the current values from GUI into the parms before
				// updating GUI.
				final int oldSel = sel;
				sel = channel.getSelectedIndex();
				final boolean valid = setChannelParameters(oldSel);
				if (!valid) {
					//Stay on the bad channel
					channel.setSelectedIndex(oldSel);
				} else {
					getChannelParameters(pendingParms.get(sel));
				}
			}
			
		});
		
		poles.setWidth("5em");
		poles.setMaxLength(1);
		filterType.add(cutOffGrid);

		
		Label notchLabel = new Label("Notch Filter:");
		notchLabel.setStyleName("filterOptionLabel");
        filterType.add(notchLabel);
		
        addBandstopFilter = new CheckBox();
        Grid GridnotchGrid = new Grid(3,2); 
        GridnotchGrid.setText(0, 0, "Notch Filter");
        GridnotchGrid.setText(1, 0, "Low cutoff freq.");
        GridnotchGrid.setWidget(0, 1, addBandstopFilter);
        GridnotchGrid.setWidget(1, 1, bandstopLow);
        GridnotchGrid.setText(2, 0, "High cutoff freq.");
        GridnotchGrid.setWidget(2, 1, bandstopHigh);
        
        filterType.add(GridnotchGrid);
        addBandstopFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				bandstopLow.setEnabled(event.getValue());
				bandstopHigh.setEnabled(event.getValue());
			}
		});

		getChannelParameters(pendingParms.get(sel));

		// Add Filter selection panel to main panel.
		dialog.add(filterType);
		
		HorizontalPanel buttonBar = new HorizontalPanel();
		okay = new Button("OK");
		buttonBar.add(okay);
		okay.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				//Make sure we have the most recent settings for selected channel
				final boolean valid = setChannelParameters(channel.getSelectedIndex());
				if (valid) {
					applyFilters(pendingParms, traces, display);
					hide();
				}
			}
			
		});
		
		VerticalPanel blank1 = new VerticalPanel();
		blank1.setHeight("20px");
		filterType.add(blank1);
		filterType.add(buttonBar);//, 30);
		
//		applyAll  = new Button("Apply to All");
//		buttonBar.add(applyAll);
//		applyAll.addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				for (int i = 0; i < parms.size(); i++)
//					setChannelParameters(parms.get(i));
//				hide();
//			}
//			
//		});
		cancel = new Button("Cancel");
		buttonBar.add(cancel);
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
				display.resetFocus();
			}
			
		});
		
		copyToAll = new Button("Copy settings to all channels");
		buttonBar.add(copyToAll);
		copyToAll.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (int i = 0; i < pendingParms.size(); i++) {
					setChannelParameters(i);
				}
			}
			
		});
		
		//dialog.setSize("500px", "500px"); // east.getOffsetHeight() + 50 + "px");//
		fp.add(dialog);
		fp.addFocusHandler(new FocusHandler() {

			public void onFocus(FocusEvent event) {
				okay.setFocus(true);
			}
			
		});
		setWidget(fp);
		fp.setFocus(true);
	}
	
//	public SetFilterParameters(final JsArrayString channels, 
//			final List<FilterParameters> parms, 
//			final List<TraceInfo> traces,
//			final EEGPane display, final ClientFactory factory) {
//		
//		
//	}
//	
	private void getChannelParameters(DisplayConfiguration fp){

		for (int i = 0; i < filters.getItemCount(); i++)
			filters.setItemSelected(i, filters.getItemText(i).equals(fp.getFilterName()));
		bandstopLow.setValue((fp.getBandstopLowCutoff()));
		bandstopHigh.setValue((fp.getBandstopHighCutoff()));
		bandpassLow.setValue((fp.getBandpassLowCutoff()), false);
		bandpassHigh.setValue((fp.getBandpassHighCutoff()), false);

		if (fp.getFilterType() == 0) {
			linear.setValue(true, true);
			addBandstopFilter.setEnabled(false);
		} else if (fp.getFilterType() == DisplayConfiguration.NO_FILTER) {
			noFilter.setValue(true, true);
			addBandstopFilter.setEnabled(false);
			bandpassHigh.setEnabled(false);
			bandpassLow.setEnabled(false);
		} else if ((fp.getFilterType() & DisplayConfiguration.LOWPASS_FILTER) != 0) {
			lowpassFilter.setValue(true, true);
			addBandstopFilter.setEnabled(true);
			bandpassHigh.setEnabled(true);
			bandpassLow.setEnabled(false);
		} else if ((fp.getFilterType() & DisplayConfiguration.HIGHPASS_FILTER) != 0) {
			addBandstopFilter.setEnabled(true);
			highpassFilter.setValue(true, true);
			bandpassHigh.setEnabled(false);
			bandpassLow.setEnabled(true);
		} else if ((fp.getFilterType() & DisplayConfiguration.BANDPASS_FILTER) != 0) {
			addBandstopFilter.setEnabled(true);
			bandpassFilter.setValue(true, true);
			bandpassHigh.setEnabled(true);
			bandpassLow.setEnabled(true);
		}
		
		if ((fp.getFilterType() & DisplayConfiguration.BANDSTOP_FILTER) != 0) {
			addBandstopFilter.setValue(true, true);
			bandstopLow.setEnabled(true);
			bandstopHigh.setEnabled(true);
		} else {
			addBandstopFilter.setValue(false, true);
			bandstopLow.setEnabled(false);
			bandstopHigh.setEnabled(false);
		}
		
		poles.setValue(fp.getNumPoles());
	}
	
	private boolean setChannelParameters(int channelIdx) {
		final boolean valid = validateInput(channelIdx);
		if (!valid) {
			return false;
		} else {
			final DisplayConfiguration fp = pendingParms.get(channelIdx);
			for (int i = 0; i < filters.getItemCount(); i++)
				if (filters.isItemSelected(i))
					fp.setFilterName(filters.getItemText(i));

			
			
			

			fp.setFilterType(0);
			if (linear.getValue())
				fp.setFilterType(0);
			else if (noFilter.getValue())
				fp.setFilterType(DisplayConfiguration.NO_FILTER);
			else {
				fp.setNumPoles(Integer.valueOf(poles.getText()));
				if (lowpassFilter.getValue()) {
					fp.addFilterType(DisplayConfiguration.LOWPASS_FILTER);
					fp.setBandpassHighCutoff(bandpassHigh.getValue());
				} else if (highpassFilter.getValue()) {
					fp.addFilterType(DisplayConfiguration.HIGHPASS_FILTER);
					fp.setBandpassLowCutoff(bandpassLow.getValue());
				} else if (bandpassFilter.getValue()) {
					fp.addFilterType(DisplayConfiguration.BANDPASS_FILTER);
					fp.setBandpassLowCutoff(bandpassLow.getValue());
					fp.setBandpassHighCutoff(bandpassHigh.getValue());
				}
				if (addBandstopFilter.getValue()) {
					fp.addFilterType(DisplayConfiguration.BANDSTOP_FILTER);
					fp.setBandstopLowCutoff(bandstopLow.getValue());
					fp.setBandstopHighCutoff(bandstopHigh.getValue());
				}
			}

			
			return true;
		}
	}
	
	private double getNyquistFrequency(int channelIdx) {
		return traces.get(channelIdx).getSampleRate() / 2;
	}

	private void applyFilters(final List<DisplayConfiguration> parms,
			final List<INamedTimeSegment> traces, final IEEGPane display) {
		display.resetFocus();
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new EegFilterChangedEvent(display.getPanel(), traces, parms));
	}

	private boolean validateInput(int channelIdx) {
		boolean valid = true;
		if (noFilter.getValue() || linear.getValue()) {
			return valid;
		}
		final double nyquistFrequency = getNyquistFrequency(channelIdx);
		if ((lowpassFilter.getValue() || bandpassFilter.getValue()) && bandpassHigh.getValue() >=
			nyquistFrequency) {
			final String channelLabel = channel.getItemText(channelIdx);
			Dialogs.messageBox(
					"Illegal filter setting",
					"Low-pass cutoff must be below " 
					+ nyquistFrequency
					+ " = 1/2 of lowest sampling frequency of " + channelLabel);
			valid = false;
		} else if (highpassFilter.getValue() && bandpassLow.getValue() >=
				nyquistFrequency) {
			final String channelLabel = channel.getItemText(channelIdx);
			Dialogs.messageBox(
					"Illegal filter setting",
					"High-pass cutoff must be below " 
					+ nyquistFrequency
					+ " = 1/2 of lowest sampling frequency of " + channelLabel);
			valid = false;
		} else if ((highpassFilter.getValue() || bandpassFilter.getValue()) && bandpassLow.getValue() == 0) {
			Dialogs.messageBox("Illegal filter setting", "High- or bandpass must have a minimum cutoff freq. above 0");
			valid = false;
		} else if ((lowpassFilter.getValue() || bandpassFilter.getValue()) && bandpassHigh.getValue() == 0) {
			Dialogs.messageBox("Illegal filter setting", "Low- or bandpass must have a maximum cutoff freq. above 0");
			valid = false;
		} else if (bandpassFilter.getValue() && bandpassLow.getValue() >= bandpassHigh.getValue()) {
			Dialogs.messageBox("Illegal filter setting", "Bandpass low cutoff must be less than high cutoff");
			valid = false;
		} else if (addBandstopFilter.getValue() && bandstopLow.getValue() >= bandstopHigh.getValue()) {
			Dialogs.messageBox("Illegal filter setting", "Bandstop low cutoff must be less than high cutoff");
			valid = false;
		}
		return valid;
	}

	@Override
	public void setModal(Boolean value) {
		// TODO Auto-generated method stub
		
	}

}
