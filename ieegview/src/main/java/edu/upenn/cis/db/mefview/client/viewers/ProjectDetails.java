/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.events.ProjectLoadedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectUpdatedEvent;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.Project.STATE;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

public class ProjectDetails extends ResizingLayout implements ChildPane, ProjectUpdatedEvent.Handler, ProjectLoadedEvent.Handler {
	LayoutPanel snapshotsPanel = new ResizingLayout();

	MetadataViewer snapshots;
	Label title = new Label("No Project Has Been Selected...");

	ClientFactory clientFactory;

	Project theProject;
	MetadataBrowserPresenter presenter;
	
	private Widget origEmptyTableWidget;

	public static final int MINHEIGHT = 400;
	public static final String NAME = "project";
	final static ProjectDetails seed = new ProjectDetails();

	public ProjectDetails() {
		super();
	}
	
	public ProjectDetails create(final ClientFactory clientFactory,
			MetadataBrowserPresenter presenter) {
		return new ProjectDetails(clientFactory, presenter);
	}

	public ProjectDetails(final ClientFactory clientFactory,
			MetadataBrowserPresenter presenter) {
		this.clientFactory = clientFactory;
		this.presenter = presenter;
		drawContent();
		this.origEmptyTableWidget = snapshots.getTable().getEmptyTableWidget();
	}


	private void drawContent() {


		DockLayoutPanel mainPane = new DockLayoutPanel(Unit.PX);
		add(mainPane);
		mainPane.addNorth(title, 35);
		title.setStyleName("IeegSubHeader");
		mainPane.add(snapshotsPanel);

//		discussionPanel.setStylePrimaryName("ieeg-disc-content");

		addSnapshotsPanel();
	}

	void addSnapshotsPanel() {
		List<String> studyColNames = new ArrayList<String>();
		List<TextColumn<SearchResult>> studyColumns = new ArrayList<TextColumn<SearchResult>>();

		studyColNames.add("Name");
		studyColumns.add(SearchResult.studyColumn);

		if (clientFactory.getSessionModel().isRegisteredUser()){
			snapshots = new MetadataViewer(clientFactory,
					10, false, false, false);
		} else {
			snapshots = new MetadataViewer(clientFactory,
					10, false, false, false);  
		}


		snapshotsPanel.add(snapshots);
		//		snapshotsPanel.setWidgetTopBottom(snapshots, 28, Unit.PX, 28, Unit.PX);

//		if (clientFactory.isRegisteredUser()) {
			for (PresentableMetadata sr: snapshots.getTable().getList())
				snapshots.getSelectionModel().setSelected(sr, true);
//		}

	}

//	public void addDiscussionsPanel(DiscussionViewer.Handler handler) {
//		GWT.log("Adding discussion handler");
//
//		discussion = new DiscussionViewer("",
//				clientFactory.getUserId(), handler, (theProject == null) ? null : theProject.getPubId(), 
//						clientFactory );
//
//		discussionPanel.add(discussion);
//	}

	public void log(String msg) {
		GWT.log(msg);
	}

	public String getTitle() {
		return "Project";
	}

	public int getMinHeight() {
		return 800;
	}

	@Override
	public void setSelected(Set<SerializableMetadata> item) {
		if (!item.isEmpty())
			setSelected(item.iterator().next());
	}
	
	@Override
	public void setSelected(SerializableMetadata item) {
		if (item != null && item instanceof Project) {
			theProject = (Project)item;
			title.setText("Project " + theProject.getFriendlyName() + " contents");
			if (theProject.getState() == STATE.LOADING) {
				setLoadingProject();
			} else if (theProject.getState() == STATE.LOADED) {
				setLoadedProject();
			}
		}
	}

	@Override
	public void onUpdatedProject(Project project) {
		if (project == theProject) {
			snapshots.getTable().clear();
			snapshots.getTable().addAll(theProject.getSnapshots());
		}
	}

	@Override
	public void bind() {
		presenter.registerHandler(ProjectUpdatedEvent.getType(), this);
		presenter.registerHandler(ProjectLoadedEvent.getType(), this);
	}

	@Override
	public void unbind() {
		presenter.releaseHandler(this);
	}

	@Override
	public void onLoadedProject(Project project) {
		if (theProject == project) {
			setLoadedProject();
		}
	}
	
	private void setLoadedProject() {
		snapshots.getTable().setEmptyTableWidget(origEmptyTableWidget);
		snapshots.getTable().clear();
		final Set<PresentableMetadata> projectDatasets = theProject.getSnapshots();
		snapshots.getTable().addAll(projectDatasets);
	}
	
	private void setLoadingProject() {
		//A hack to get the loading indicator to show while waiting for project datasets to load.
		snapshots.getTable().setEmptyTableWidget(snapshots.getTable().getLoadingIndicator());
		snapshots.getTable().clear();
	}
}
