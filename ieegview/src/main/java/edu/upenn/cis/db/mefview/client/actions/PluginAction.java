/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.ClassFromStringFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;

/**
 * Generic base class for plugin actions.
 * @author zives
 *
 */
public abstract class PluginAction implements IPluginAction, Command {
	IPluginAction nextAction;
	String name;
	final ClientFactory clientFactory;
	final Presenter activePresenter;
	
	public PluginAction(String name, ClientFactory cf, Presenter activePresenter,
			IPluginAction next) {
		this.name = name;
		this.clientFactory = cf;
		this.activePresenter = activePresenter;
		nextAction = next;
	}

	@Override
	public boolean isApplicable() {
		return true;
	}

	@Override
	public IPluginAction getNextAction() {
		return nextAction;
	}
	
	public void setNextAction(IPluginAction next) {
		nextAction = next;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
	
	public ClientFactory getFactory() {
		return clientFactory;
	}
	
	public Presenter getActivePresenter() {
		return activePresenter;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
