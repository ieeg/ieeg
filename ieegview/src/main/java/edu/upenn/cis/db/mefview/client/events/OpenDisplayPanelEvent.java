/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/***
 * Event signifying a request to open a new panel with the named type
 * 
 * @author zives
 *
 */
public class OpenDisplayPanelEvent  extends GwtEvent<OpenDisplayPanelEvent.Handler> {
	private static final Type<OpenDisplayPanelEvent.Handler> TYPE = new Type<OpenDisplayPanelEvent.Handler>();
	
	private final OpenDisplayPanel action;
	private final PresentableMetadata url;
	private final String title;
	
	public OpenDisplayPanelEvent(OpenDisplayPanel action){
		this.action = action;
		this.url = null;
		this.title = null;
	}
	
	public OpenDisplayPanelEvent(String title, PresentableMetadata url) {
		this.action = null;
		this.title = title;
		this.url = url;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<OpenDisplayPanelEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<OpenDisplayPanelEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OpenDisplayPanelEvent.Handler handler) {
		if (action != null)
			handler.onOpenNewPanel(action);
		else if (url != null)
			handler.onOpenNewPanel(title, url);
	}

	public static interface Handler extends EventHandler {
		void onOpenNewPanel(OpenDisplayPanel action);
		void onOpenNewPanel(String title, PresentableMetadata url);
	}
}
