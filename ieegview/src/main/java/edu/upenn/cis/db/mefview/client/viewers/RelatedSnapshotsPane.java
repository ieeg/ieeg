/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.ChangedViewEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.RequestRemoveExperimentEvent;
import edu.upenn.cis.db.mefview.client.events.RequestShowPermissionsEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultRefreshEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class RelatedSnapshotsPane extends LayoutPanel 
implements ChangedViewEvent.Handler, ToolResultRefreshEvent.Handler {
	PagedTable<DerivedSnapshot> toolOutputList;
	Label parentLabel = new Label("Parent:");
	TextBox parentBox = new TextBox();
	Label resultsLabel = new Label("Derived snapshots:");
	Button analysisDataButton = new Button("View");
	Button analysisAnnotationsButton = new Button("Annotations");
	List<DerivedSnapshot> analyses = new ArrayList<DerivedSnapshot>();
	ClientFactory clientFactory;
	
	AppModel control;
	
	DataSnapshotModel currentModel = null;

//	TextColumn<ToolResult> toolFor = new TextColumn<ToolResult> () {
//		@Override
//		public String getValue(ToolResult object) {
//			if (object.getSnapshotRevId() != null)
//				return parent.getStudyFriendlyName(object.getSnapshotRevId());
//			else
//				return null;
//		}
//	};
	TextColumn<DerivedSnapshot> toolAnalysisType = new TextColumn<DerivedSnapshot> () {
		@Override
		public String getValue(DerivedSnapshot object) {
			if (object.getAnalysisType() != null)
				return object.getAnalysisType();
			else
				return null;
		}
	};
	TextColumn<DerivedSnapshot> toolType = new TextColumn<DerivedSnapshot> () {
		@Override
		public String getValue(DerivedSnapshot object) {
			if (object.getAnalysisTool() != null)
				return object.getAnalysisTool();
			else
				return null;
		}
	};
	
	public RelatedSnapshotsPane(AppModel controlPane, ClientFactory factory) {
		List<String> toolColNames = new ArrayList<String>();
		List<TextColumn<DerivedSnapshot>> toolColumns = new ArrayList<TextColumn<DerivedSnapshot>>();
		this.control = controlPane;
		this.clientFactory = factory;
		this.setStylePrimaryName("gwt-StackLayoutPanel gwt-StackLayoutPanelContent-managePanel");

		
//		toolColNames.add("Name");
//		toolColumns.add(toolID);
		toolColNames.add("Analysis");
		toolColumns.add(toolAnalysisType);
		toolAnalysisType.setSortable(true);
//		toolColNames.add("Snapshot");
//		toolColumns.add(toolFor);
//		toolFor.setSortable(true);
		toolColNames.add("Tool");
		toolColumns.add(toolType);
		toolType.setSortable(true);

		toolOutputList = new PagedTable<DerivedSnapshot>(50, false, false, 
				analyses, 
				DerivedSnapshot.KEY_PROVIDER, toolColNames, toolColumns);
//		VerticalPanel tSeries = new VerticalPanel();

		    toolOutputList.addColumnSort(toolAnalysisType,
			        new Comparator<DerivedSnapshot>() {
		          public int compare(DerivedSnapshot o1, DerivedSnapshot o2) {
		            if (o1 == o2) {
		              return 0;
		            }

		            // Compare the name columns.
		            if (o1 != null) {
		              return (o2 != null) ? o1.getAnalysisType().compareTo(o2.getAnalysisType()) : 1;
		            }
		            return -1;
		          }
		        });
		    
		    toolOutputList.addColumnSort(toolType,
			        new Comparator<DerivedSnapshot>() {
		          public int compare(DerivedSnapshot o1, DerivedSnapshot o2) {
		            if (o1 == o2) {
		              return 0;
		            }

		            // Compare the name columns.
		            if (o1 != null) {
		              return (o2 != null) ? o1.getAnalysisTool().compareTo(o2.getAnalysisTool()) : 1;
		            }
		            return -1;
		          }
		    });		

//		    toolOutputList.addColumnSort(toolFor,
//			        new Comparator<ToolResult>() {
//		          public int compare(ToolResult o1, ToolResult o2) {
//		            if (o1 == o2) {
//		              return 0;
//		            }
//
//		            // Compare the name columns.
//		            if (o1 != null) {
//		              return (o2 != null) ? ManagerPane.this.parent.getStudyFriendlyName(o1.getSnapshotRevId()).compareTo(ManagerPane.this.parent.getStudyFriendlyName(o2.getSnapshotRevId())) : 1;
//		            }
//		            return -1;
//		          }
//			});		
		
			toolOutputList.getColumnSortList().push(toolAnalysisType);
//		tSeries.add(resultsLabel);
		
		HorizontalPanel par = new HorizontalPanel();
		par.add(parentLabel);
		par.add(parentBox);
		parentBox.setEnabled(false);
		add(par);
		setWidgetLeftRight(par, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopHeight(par, 2, Unit.PX, 32, Unit.PX);
		
		add(resultsLabel);
		setWidgetLeftRight(resultsLabel, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopHeight(resultsLabel, 36, Unit.PX, 32, Unit.PX);
			
		add(toolOutputList);
		setWidgetLeftRight(toolOutputList, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(toolOutputList, 68, Unit.PX, 34, Unit.PX);

		HorizontalPanel hpT = new HorizontalPanel();
		hpT.setSpacing(2);
		add(hpT);
		setWidgetLeftRight(hpT, 2, Unit.PX, 2, Unit.PX);
		setWidgetBottomHeight(hpT, 2, Unit.PX, 32, Unit.PX);

		Button removeExp = new Button("Remove");
		removeExp.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (DerivedSnapshot tr: toolOutputList.getList())
					if (toolOutputList.getSelectionModel().isSelected(tr)) {
						final DialogBox d = new DialogBox();
						d.setTitle("Delete " + tr.getAnalysisType()+ ": Are you sure?");
						VerticalPanel pan = new VerticalPanel();
						d.setWidget(pan);
						pan.add(new Label("Delete " + tr.getAnalysisType()+ ": Are you sure?"));
						HorizontalPanel hp = new HorizontalPanel();
						pan.add(hp);
						Button yes = new Button("Yes");
						hp.add(yes);
						hp.add(new HTML("&nbsp;&nbsp;"));
						Button no = new Button("No");
						hp.add(no);
						final DerivedSnapshot res = tr;
						yes.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent arg0) {
								d.hide();
//								control.removeExperiment(res);
								IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestRemoveExperimentEvent(res));
							}
							
						});
						no.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent arg0) {
								d.hide();
							}
							
						});
						d.center();
						d.show();
					}
			}
			
		});
		Button viewPerms = new Button("Share...");
		viewPerms.setTitle("Access Control List: view/manage permissions");
		viewPerms.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				for (DerivedSnapshot tr : toolOutputList.getList()) {
					if (toolOutputList.getSelectionModel().isSelected(tr)) {
						//control.showPermissions(currentModel.getUserID(), tr);
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestShowPermissionsEvent(currentModel.getUserID(), tr));
					}
				}
			}});
//		hpT.add(newExp);
		hpT.add(analysisDataButton);
		hpT.add(removeExp);
		hpT.add(viewPerms);
//		hpT.add(redoExp);
//		analysisPanel.add(expT);
//		analysisPanel.setCellHorizontalAlignment(expT, HasHorizontalAlignment.ALIGN_CENTER);
		
		analysisDataButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				for (DerivedSnapshot tr : toolOutputList.getList()) {
						SearchResult sr = new SearchResult();
						sr.setDatasetRevId(tr.getAnalysisId());
						sr.setBaseRevId(currentModel.getSnapshotID());
						sr.setFriendlyName(control.getStudyFriendlyName(tr.getSnapshotRevId()) + "-" + 
								tr.getAnalysisType());
						
						currentModel.getSearchResult().addDerived(sr);
						sr.setParent(currentModel.getSearchResult());
						
//						parent.clearDataWindow(sr);
//						studyProvider.getList().add(sr);
//						studyList.setRowCount(studyProvider.getList().size());
						
						/*
						control.selectStudy(currentModel.getUserID(), sr, true, 
								//AstroPanel.PanelType.EEG, 
								EEGViewerPanel.NAME, null);
								*/
					if (toolOutputList.getSelectionModel().isSelected(tr)) {
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new
								OpenDisplayPanel(
										sr, true, EEGViewerPanel.NAME, null)));
					}
				}
//				studyProvider.flush();
			}
			
		});
		
		analysisAnnotationsButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				for (DerivedSnapshot tr : toolOutputList.getList()) {
					if (toolOutputList.getSelectionModel().isSelected(tr)) {
						SearchResult sr = new SearchResult();
						sr.setDatasetRevId(tr.getAnalysisId());
						sr.setFriendlyName(control.getStudyFriendlyName(tr.getSnapshotRevId()) + "-" + 
								tr.getAnalysisType());
						
						currentModel.getSearchResult().addDerived(sr);
						sr.setParent(currentModel.getSearchResult());

						//						control.loadAndCopyAnnotations(currentModel.getUserID(), sr, 
//								tr.getAnalysisType());
						
					}
				}
			}
			
		});
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ChangedViewEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ToolResultRefreshEvent.getType(), this);
	}
	
	public void setDataSnapshotState(DataSnapshotModel model) {
//		if (model != null)
//			System.err.println("Setting model to " + model.getStudyID());
//		else
//			System.err.println("Clear model");
		currentModel = model;
		
		toolOutputList.clear();
		if (model != null) {
			if (model.getParentResultID() == null)
				parentBox.setText("-");
			else
				parentBox.setText(model.getParentResultID());
			toolOutputList.addAll(model.getDerivedResults());

//			System.err.println("Model " + currentModel.getStudyID() + " with " + currentModel.getDerivedResults().size() + " results, of which " + toolOutputList.getList().size() + " are in the table");
		}
		toolOutputList.redraw();
	}

	@Override
	public void onChangedView(AstroPanel pane) {
		setDataSnapshotState(pane.getDataSnapshotState());
	}

	@Override
	public void onChangedView(String newResultID) {
		System.err.println("View changed -- redraw");
		
		if (currentModel != null) {
			toolOutputList.clear();
			toolOutputList.addAll(currentModel.getDerivedResults());
		}
		
//		if (currentModel == null)
//			System.err.println("No model");
//		else
//			System.err.println("Model " + currentModel.getStudyID() + " with " + currentModel.getDerivedResults().size() + " results, of which " + toolOutputList.getList().size() + " are in the table");
		toolOutputList.redraw();
	}
	
	public void close() {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
	}
}
