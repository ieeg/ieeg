/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;

public class TimeSeriesSpanJs extends TimeSeriesSpan {
//	JsArrayInteger series;
	int index;
	JsArray<JsArrayInteger> master;
	
	public TimeSeriesSpanJs(TimeSeriesSpanSpecifier specifier, long startTime, long endTime, double period, 
			double scale, int index, JsArray<JsArrayInteger> master) {
		
		super(specifier, startTime, endTime, period, scale);
		
//		this.series = samples;
		this.index = index;
		this.master = master;
	}
	
	public JsArray<JsArrayInteger> getMaster() {
		return master;
	}
	
	@Override
	public boolean isInGap(int i) {
		return isInGap(master.get(i), index);
	}

	@Override
	public int[] getSeries() {
		int[] ret = new int[master.length()];
		
		for (int i = 0; i < master.length(); i++)
			if (isInGap(master.get(i), index))
				ret[i] = 0;
			else
				ret[i] = master.get(i).get(index);

		return ret;
	}

//	@Override
//	public JsArrayInteger getSeriesJs() {
//		return series;
//	}
	
	public static native boolean isInGap(JavaScriptObject series, int index) /*-{
		if (series[index])
			return false;
		else
			return true;
	}-*/;
}
