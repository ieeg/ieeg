package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.Command;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public interface IEditAnnotation {

	public void init(final Annotation a, 
		      final List<AnnotationDefinition> definitions,
		      final List<String> channels, final Map<String,String> schema,
		      final DataSnapshotModel model,
		      boolean isNew, Command doAfter);
	
	
	public void show();
	
	public void center();
	
	public void setModal(Boolean value);
	
	public void setGlassEnabled(Boolean value);

	
}
