/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataFactory;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.client.viewers.MetadataTree;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;

public class MetadataProvider extends MetadataProviderBase {
		private String addToCollection = null;
		
		public MetadataProvider(final PresentableMetadata item, final MetadataModel model, 
				final ClientFactory clientFactory, String coll, final MetadataTree notify,
				final Set<String> knownIds) {
			super(item, model, clientFactory, notify, knownIds);
			
			if (coll != null)
				addToCollection = coll;
			
			// Root node -- need to fetch top-level snapshots
			if (item == null) {
				if (model != null)
					addAllNoParent(model.getKnownSnapshots());
				else
					; // We shouldn't really end up here, this means we know nothing
					  // but it is possible if the user cancels a search

			} else {
			
			}
			
			if (result != null) {
				GWT.log("MDP Starting " + (item == null ? "root" : item.getFriendlyName())
						+ " with " + result.size() + " entries");

				int showThis = result.size();
				if (showThis > startPage + PAGE_SIZE)
					showThis = startPage + PAGE_SIZE;
				updateRowData(0, result.subList(startPage, showThis));
				updateRowCount(showThis - startPage, true);
			} else {
				int showThis = result.size();
				if (showThis > startPage + PAGE_SIZE)
					showThis = startPage + PAGE_SIZE;
				updateRowData(0, result.subList(startPage, showThis));
				updateRowCount(showThis - startPage, true);
//				GWT.log("MDP starting with no details on " + (item == null ? "?" : item.getLabel()));
				
			}
		}

		@Override
		public void categorizeResults(List<PresentableMetadata> resultList) {
			
			String name = (item == null) ? " root" : " " + item.getFriendlyName();

//			System.out.println("** " + name + 
//					" categorizing " + resultList.size() + " **");
			
			if (resultList.size() == 0) {
//				GWT.log("MDP Cat" + name + " returning on empty");
				return;
			}
			
			// Record the set of children in the metadata
			if (item != null) {
				item.clearKnownChildren();
				for (PresentableMetadata pm: resultList) {
					item.addKnownChild(pm);
				}
			}
			
			HashMap<String, CollectionNode> map = 
					new HashMap<String, CollectionNode>();
			
			HashMap<MetaCollectionProvider, List<PresentableMetadata>> pending = 
					new HashMap<MetaCollectionProvider, List<PresentableMetadata>>();

			// Take each sub-result and identify the list of sub-collections (categories)
			for (PresentableMetadata pm : resultList) {
				if (pm instanceof CollectionNode) {
//					GWT.log("MDP Cat" + name + ": Keeping " + pm.getLabel());
//					GWT.log("Inherited from previous: " + pm.getId() + "," + pm.getChildMetadata());
					map.put(pm.getLabel(), (CollectionNode)pm);
				} else {
					String key = pm.getMetadataCategory();
					if (pm instanceof SearchResult && 
							BASIC_CATEGORIES.Snapshot.name().equals(key) && item == null)
						key = addToCollection;//"Search Results";
					
					if (!map.containsKey(key)) {
//						GWT.log("MDP Cat " + name + ": Adding category " + key);
//						GWT.log("Creating collection node for " + key);
						map.put(key, new CollectionNode(item, key, treePresenter));
					}
//					GWT.log("Adding " + pm + " to " + key);

					map.get(key).addMetadata(pm);
					MetaCollectionProvider mc = 
							(MetaCollectionProvider) treePresenter.getOrReuseProvider(map.get(key));
					if (!pending.containsKey(mc))
						pending.put(mc,  new ArrayList<PresentableMetadata>());
					pending.get(mc).add(pm);
				}
			}
			
			// For each metadata item, process its child set
			for (MetaCollectionProvider mp: pending.keySet()) {
				System.out.print(name + ": ");// + " refreshing " + mp.getItem() + " with  " + pending.get(mp));
				mp.refreshList(pending.get(mp));
			}
			
//			GWT.log("MDP Cat" + name + ": adding " + map.values().size() + " categories from " + resultList.size() + " items");

//			if (map.values().size() > 1) {
			List<PresentableMetadata> keys = new ArrayList<PresentableMetadata>();
			keys.addAll(map.values());
			MetadataModel.sortMetadata(keys);
			for (PresentableMetadata pm : keys)
				((CollectionNode)pm).sort();

			Set<PresentableMetadata> removed = clearResults(resultList);
//			GWT.log("MDP Cat" + name + ": removed " + removed.size());
			resultList.addAll(keys);
//			GWT.log("MDP Cat" + name + ": final list includes " + resultList.size());
			
			removeUnused(removed);
//			}
		}
		
		@Override
		public List<PresentableMetadata> getNewRootContent() {
			List<PresentableMetadata> ss = new ArrayList<PresentableMetadata>();
			ss.addAll(model.getKnownSnapshots());
			MetadataModel.sortMetadata(ss);
			categorizeResults(ss);
			return filter(ss);
		}

		@Override
		public List<PresentableMetadata> getNewItemContent() {
			Set<PresentableMetadata> sel = treePresenter.getSelected();
			
			for (PresentableMetadata md: sel)
				treePresenter.getSelectionModel().setSelected(md, false);

			List<PresentableMetadata> ss = MetadataFactory.getDrillDown(item.getClass()).getChildren(item, this,
					getStartIndex(), getStartIndex() + PAGE_SIZE);

			categorizeResults(ss);
			
			for (PresentableMetadata md: sel)
				treePresenter.getSelectionModel().setSelected(md, true);

			return filter(ss);
		}

		@Override
		public void clearSearchResults() {
			for (PresentableMetadata md: result) {
				if (md instanceof CollectionNode && md.getLabel().equals(addToCollection)) {
					for (GeneralMetadata md2: md.getAllMetadata())
						if (md2.getId() != null) {
							existingIds.remove(md2.getId());
							treePresenter.onItemRemoved((PresentableMetadata)md2);
						}
					md.getAllMetadata().clear();
				}
			}
		}

		@Override
		public Set<PresentableMetadata> getSelected() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void restoreSelected(Collection<PresentableMetadata> sel) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void refreshList(List<? extends PresentableMetadata> md) {
			super.refreshList(md);
			
			// Check integrity
			for (PresentableMetadata m: result) {
				if (!(m instanceof CollectionNode)) {
					assert(false);
				}
			}
		}

		@Override
		public List<PresentableMetadata> getChildMetadata() {
			result.clear();
			result.addAll(getNewItemContent());
			return result;//getNewItemContent();
		}

		@Override
		public void toggleRating(PresentableMetadata collectionItem) {
			// TODO Auto-generated method stub
			
		}
	}