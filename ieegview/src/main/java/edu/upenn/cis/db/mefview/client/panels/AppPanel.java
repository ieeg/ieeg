/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.panels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ClientFactoryImpl;
import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.WebConsole;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.ITabSet;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPanel;
import edu.upenn.cis.db.mefview.client.plugins.projectsettings.ProjectPanel;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.UserProfilePanel;
import edu.upenn.cis.db.mefview.client.presenters.IAppPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.HeaderBar;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.client.viewers.JobPane;
import edu.upenn.cis.db.mefview.client.viewers.RelatedSnapshotsPane;
import edu.upenn.cis.db.mefview.client.widgets.CollapsibleTab;
import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.client.widgets.ResizableTabPanel;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class AppPanel extends DockLayoutPanel implements IAppPanel {

	public static int LEFT_PANE = 0;
	public static final int HEADER = 45;
	public static final int MIN_HEIGHT = 800;
	
	public static final int HEADER_HEIGHT = 45;
	
	IHeaderBar topBar;
	
    private HandlerRegistration closingHandlerReg;
    private HandlerRegistration closeHandlerReg;

	LayoutPanel content = new LayoutPanel();
	
	ClientFactoryImpl clientFactory;
	SessionModel sessionModel;
	
	JobPane jobPane;
	
	RelatedSnapshotsPane relatedPane;
	ProjectPanel projectPanel;

	ITabber tabPanel;

	AstroPanel profile;
	AstroPanel searchWindow;
	
	IAppPresenter presenter;
	
	public List<AstroPanel> otherPanels = new ArrayList<AstroPanel>();

	public AppPanel() {
		super(Unit.PX);
	}
	
	public AppPanel(final ClientFactory clientFactory) {
		this();
		init(clientFactory);
	}
	
	public void init(
			final ClientFactory clientFactory) {
		init(clientFactory, null, null, null);
	}
	
	public void init(
			final ClientFactory clientFactory,
			IHeaderBar hb,
			Map<String, ? extends ITabber> panel,
			ITabSet tabset
			) {
		this.clientFactory = (ClientFactoryImpl)clientFactory;
		sessionModel = clientFactory.getSessionModel();
		
		if (panel == null) {
			tabPanel = new ResizableTabPanel(clientFactory,
					ResourceFactory.getLeftImage(),
					ResourceFactory.getRightImage());//DecoratedTabPanel();
			tabPanel.addStyleDependentName("tabPanel");
		}
		
		if (hb == null) {
			topBar = new HeaderBar(clientFactory);
			addNorth((HeaderBar)topBar, HEADER);
		} else
			topBar = hb;
		
		if (panel == null) {
			add(content);
			
			// Add TabPanel
			content.add((Widget)tabPanel);
		}
	}

	/**
	 * Take the list of panel names, and open each panel
	 * 
	 * @param panelList
	 */
	private void openPanelsFrom(List<String> panelList, final AppModel cstate) {
		for (String panelName : panelList) {
			CollectionNode title = new CollectionNode(null, 
					MetadataBrowserPanel.getDefaultTitle(), 
					null);
			
			try {
				AstroPanel newWindow = createPanel(cstate, 
								panelName,
								title, // target
								null, // dsc
								false,
								false
								);
				
				title.setCategory(newWindow.getTitle());
				
				otherPanels.add(newWindow);
				if (panelName.equals(MetadataBrowserPanel.NAME)) {
					searchWindow = newWindow;
				} else if (panelName.equals(UserProfilePanel.NAME)) {
					profile = newWindow;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void initPanels(final AppModel cstate) {
		
		openPanelsFrom(WebConsole.registry.getDefaultPanels(), cstate);
		if (sessionModel.isRegisteredUser())
			openPanelsFrom(WebConsole.registry.getLoggedInPanels(), cstate);

		if (tabPanel.getWidgetCount() > 0)
			tabPanel.selectTab(0);
			
	}
	
	@Override
	public List<AstroPanel> getPanels() {
		return otherPanels;
	}
	
	public void setTabSelectionHandler(SelectionHandler<Integer> handler) {
		tabPanel.addSelectionHandler(handler);
	}

	public void login(UserInfo info) {
		
		// We might have already been logged in; remove all of this
		tabPanel.clear();
		for (AstroPanel tab : otherPanels) {
			tab.getPresenter().unbind();
		}
		otherPanels.clear();
		topBar.setUserInfo(info, sessionModel.isRegisteredUser());
	}

	public void login(UserInfo info, String title) {
		login(info);
		topBar.setAppName(title);
	}

	public void removeComposite(Composite w) {
		tabPanel.remove(
				tabPanel.getWidgetIndex(w));
	}
	
	@Override
	public void insertPanel(AstroPanel w, Widget tab, int inx) {
		GWT.log("Adding panel of type " + w.getPanelType() + " (" + w.getTitle() + ")");
		otherPanels.add(inx, w);
		tabPanel.insert((Panel)w, tab, inx);
	}
	
	@Override
	public void addPanel(AstroPanel w, Widget tab, boolean select) {
		GWT.log("Adding panel of type " + w.getPanelType() + " (" + w.getTitle() + ")");
		otherPanels.add(w);
		tabPanel.add((Panel)w, tab);
		if (select)
			tabPanel.selectTab(tabPanel.getWidgetIndex((Widget)w));
	}
	
	public void setPanel(AstroPanel w) {
		if (tabPanel.getWidgetIndex((Widget)w) >= 0)
			tabPanel.selectTab(tabPanel.getWidgetIndex((Widget)w));
	}

	@Override
	public void removePanel(AstroPanel w) {
		otherPanels.remove(w);
		int inx = tabPanel.getSelectedIndex();
//		cs.getDescriptors().remove(w);
		tabPanel.remove(
				tabPanel.getWidgetIndex((Widget)w));
		
		if (inx < tabPanel.getWidgetCount())
			tabPanel.selectTab(inx);
		else
			tabPanel.selectTab(tabPanel.getWidgetCount() - 1);
	}

	public static DialogBox getPleaseWaitDialog(final String operation) {
		final DialogBox d = new DialogBox();
		d.setTitle("Please Wait...  " + operation + "...");
		d.setText("Please Wait...  " + operation + "...");
		return d;
	}

	public void selectTab(Widget w) {
		tabPanel.selectTab(w);
	}
	
	public int getCurrentPane() {
		return tabPanel.getSelectedIndex();
	}
	
	public void switchPane(int pane) {
		tabPanel.selectTab(pane);
	}
	
	public void hideGraph() {
		sessionModel.getMainLogger().info("Request to hide graph");
		Widget w = tabPanel.getWidget(tabPanel.getSelectedIndex());
		if (w instanceof EEGViewerPanel) {
			sessionModel.getMainLogger().info("Hiding graph");
			EEGViewerPanel v = (EEGViewerPanel)tabPanel.getWidget(tabPanel.getSelectedIndex());
			v.getEEGPane().hideGraph();
		}
	}
	
	public void showGraph() {
		sessionModel.getMainLogger().info("Restoring graph");
		if (tabPanel.getWidget(tabPanel.getSelectedIndex()) instanceof EEGViewerPanel) {
			EEGViewerPanel v = (EEGViewerPanel)tabPanel.getWidget(tabPanel.getSelectedIndex());
			v.getEEGPane().showGraph();
		}
	}
	
	public void addTabPane(Panel panel, Widget header) {
		tabPanel.add(panel, header);
	}

	@Override
	public Widget getSelectedWidget() {
		// TODO Auto-generated method stub
	  Widget w = tabPanel.getWidget(tabPanel.getSelectedIndex());
		return w;
	}

	@Override
	public void addLogoHandler(ClickHandler handler) {
        topBar.addLogoHandler(handler);
	}

	@Override
	public void addTitleHandler(ClickHandler handler) {
        topBar.addTitleHandler(handler);

	}

	public void addLogoutHandler(ClickHandler handler) {
		topBar.addLogoutHandler(handler);
	}

	@Override
	public void removeWorkspaceHandlers() {
	    closingHandlerReg.removeHandler();
	    closeHandlerReg.removeHandler();
	}

//	@Override
//	public WorkPlace getCurrentWorkPlace() {
//		return model.getWorkPlace();
//	}

	@Override
	public void selectSearchTab() {
//		if (searchWindow != null && tabPanel.getWidgetIndex((Widget)searchWindow) >= 0)
//			tabPanel.selectTab(tabPanel.getWidgetIndex((Widget)searchWindow));
	}

	@Override
	public void closeProject() {
		if (projectPanel != null) {
			tabPanel.remove(projectPanel);
		}
	}

	@Override
	public AstroPanel openProject(Project project, AppModel controller,
			Collection<SearchResult> contents) {
		projectPanel = (ProjectPanel)createPanel(controller, 
				ProjectPanel.NAME,
				new CollectionNode(null, project.getName(), null), // target
				null, 
				true,
				true
				);

		if (projectPanel != null) {
			tabPanel.selectTab(tabPanel.getWidgetIndex(projectPanel));

			projectPanel.addSearchResults(project, contents);
		}
		
		return projectPanel;
	}

	@Override
	public AstroPanel getActivePanel() {
		final int selectedIndex = tabPanel.getSelectedIndex();
		if (selectedIndex < 0) {
			return null;
		}
		return (AstroPanel)tabPanel.getWidget(selectedIndex);
	}
	
	@Override
	public List<? extends PresentableMetadata> getFavorites() {
		return getPresenter().getFavoriteSnapshots();
	}

	@Override
	public AstroPanel createPanel(AppModel controller, String panelType, PresentableMetadata target,
			DataSnapshotViews dsc, boolean isWritable, boolean isClosable) {
		return createPanel(controller, panelType, target, dsc, isWritable, isClosable, null);
	}
	
	public AstroPanel createPanel(AppModel controller, String panelType, PresentableMetadata target,
			DataSnapshotViews dsc, boolean isWritable, boolean isClosable, String title) {
		final AstroPanel panel = 
				PanelFactory.getPanel(panelType, clientFactory, 
						controller, dsc, //project, 
						isWritable, target.getLabel());
		
		if (panel != null) {
			tabPanel.add((Panel)panel, new CollapsibleTab((title == null) ? panel.getTitle() : title, 
					panel.getIcon(clientFactory), 
					false, isClosable, !isClosable ? null : new ClickHandler() {
	
				@Override
				public void onClick(ClickEvent event) {
					int target = tabPanel.getSelectedIndex();
					tabPanel.remove((Widget)panel);
//					projectPanel = null;
					
					if (panel.getPresenter() != null)
						panel.getPresenter().unbind();
					panel.close();
	
					if (target < tabPanel.getWidgetCount())
						tabPanel.selectTab(target);
					else
						tabPanel.selectTab(tabPanel.getWidgetCount() - 1);
	
				}
	
			}));
			if (dsc != null)
				dsc.addPane(panel);

			tabPanel.selectTab(tabPanel.getWidgetIndex((Widget)panel));
		}
		return panel;
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = (IAppPresenter)pres;
	}
	
	@Override
	public IAppPresenter getPresenter() {
		return presenter;
	}

	  public ITabber getTabPanel() {
	    return tabPanel;
	  }

	  @Override
	  public void setCloseHandler(Window.ClosingHandler closingHandler,
		      CloseHandler<Window> closeHandler) {
		    GWT.log("Close handler set");
		    closingHandlerReg = Window.addWindowClosingHandler(
		        closingHandler
		        );

		    closeHandlerReg = Window.addCloseHandler(
		        closeHandler
		        );

		  }

	@Override
	public IHeaderBar getHeaderBar() {
		return topBar;
	}
	  
	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions, String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PanelDescriptor getDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataSnapshotViews getDataSnapshotController() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public void updateRevId(String oldRevId, String newRevId) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void highlightMetadata(
//			Collection<? extends PresentableMetadata> items) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void registerPrefetchListener(Prefetcher p) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPlace(WindowPlace place) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public WindowPlace getPlace() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getMinHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.MASTER;
	}

	@Override
	public void bindEvents() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void bindDomEvents() {
		bindEvents();
	}
}
