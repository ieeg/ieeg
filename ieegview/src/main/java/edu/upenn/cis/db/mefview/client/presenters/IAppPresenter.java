package edu.upenn.cis.db.mefview.client.presenters;

import java.util.Collection;
import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public interface IAppPresenter extends Presenter {
	public static interface CreateWindow {
		public AstroPanel createWindow(
				final DataSnapshotViews model, final SearchResult ds,
				final Collection<? extends PresentableMetadata> selected,
				boolean writePermissions, WindowPlace place);

		public Presenter createPresenter(
				final DataSnapshotViews model, final SearchResult ds,
				final Collection<? extends PresentableMetadata> selected,
				boolean writePermissions, WindowPlace place);

		public boolean switchToWindow(
				final DataSnapshotViews model, final SearchResult ds,
				final Collection<? extends PresentableMetadata> selected,
				WindowPlace place);

		public String getTitle(
				final DataSnapshotViews model, final SearchResult ds,
				final Collection<? extends PresentableMetadata> selected,
				boolean writePermissions);

		public ImageResource getIcon(
				final DataSnapshotViews model, final SearchResult ds,
				final Collection<? extends PresentableMetadata> selected,
				boolean writePermissions);
	}

	public static interface OnDoneHandler<T> {
		public void doWork(final String studyId, final String friendlyName,
				final T studyInfo);
	}

	public interface Display extends BasicPresenter.Display {
		Widget getSelectedWidget();

		public IHeaderBar getHeaderBar();

		public void setTabSelectionHandler(SelectionHandler<Integer> handler);

		public void addLogoHandler(ClickHandler handler);

		public void addTitleHandler(ClickHandler handler);

		public void addLogoutHandler(ClickHandler handler);
		
		public void bindEvents();

		// public void addWorkTab(AppModel cstate, String userID);

		public List<AstroPanel> getPanels();

		public AstroPanel getActivePanel();

		public void removeWorkspaceHandlers();

//		public void showGraph();

//		public void hideGraph();

		public void selectSearchTab();

		public void addPanel(AstroPanel w, Widget tab, boolean select);

		public void insertPanel(AstroPanel w, Widget tab, int inx);

		public void removePanel(AstroPanel w);

		public void closeProject();

		public AstroPanel openProject(Project project, AppModel controller,
				Collection<SearchResult> contents);

		public void initPanels(final AppModel cstate);

		public void setPanel(AstroPanel w);

		public ITabber getTabPanel();

		public List<? extends PresentableMetadata> getFavorites();

		public AstroPanel createPanel(AppModel controller, String panelType,
				PresentableMetadata target, DataSnapshotViews dsc,
				boolean isWritable, boolean isClosable);

		public void setCloseHandler(Window.ClosingHandler closingHandler,
				CloseHandler<Window> closeHandler);
	}

	public void init(
			final ClientFactory factory,
			final AppModel controller);

	public List<? extends PresentableMetadata> getFavoriteSnapshots();
}
