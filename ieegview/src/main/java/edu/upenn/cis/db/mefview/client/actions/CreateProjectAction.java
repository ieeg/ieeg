/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.CreateProjectEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenProjectEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

/**
 * Initiate project-create event dialog
 * 
 * @author zives
 *
 */
public class CreateProjectAction extends PluginAction {

	Project proj = null;
	
	public static final String NAME = "Create Project";
	
	
	static {
		PluginActionFactory.register(NAME, new CreateProjectAction());
	}
	
	public CreateProjectAction() {
		super(NAME, null, null, null);
	}
	
	public CreateProjectAction(ClientFactory cf, Presenter pres) {
		super(NAME, cf, pres, null);
	}

	@Override
	public void execute() {
		CreateProjectEvent projEvent = new CreateProjectEvent();

		IeegEventBusFactory.getGlobalEventBus().fireEvent(projEvent);
		
//		return null;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new CreateProjectAction(factory, pres);
	}
	
};

