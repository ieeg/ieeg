/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.controller;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;

import edu.upenn.cis.db.mefview.client.util.DataConversion;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;

public class SnapshotCache {
	public static int LOOKAHEAD = 2;
	public static int LOOKBEHIND = 1;
	
	// The cache -- a DDataSet
	JsArray<JsArrayInteger> data;
	List<DisplayConfiguration> filter;
	private boolean minMax = false;
	
	public SnapshotCache() {
		GWT.log("Snapshot cache activated");
		data = emptyArray();
		this.filter = new ArrayList<DisplayConfiguration>();
	}
	
	private native JsArray<JsArrayInteger> emptyArray() /*-{
		return [];
	}-*/;
	
	public SnapshotCache(JsArray<JsArrayInteger> arr, List<DisplayConfiguration> filter, boolean isMinMax) {
		GWT.log("Snapshot cache activated");
		data = arr;
//		this.filter = filter;
		this.filter = new ArrayList<DisplayConfiguration>();
		for (DisplayConfiguration fp: filter)
			this.filter.add(new DisplayConfiguration(fp));
		this.minMax = isMinMax;
//		this.filter.addAll(filter);
	}
	
	public void setFilter(List<DisplayConfiguration> filter) {
		if (!filtersEqual(filter)) {
//			this.filter = filter;
			this.filter.clear();
//			this.filter.addAll(filter);
			for (DisplayConfiguration fp: filter)
				this.filter.add(new DisplayConfiguration(fp));

			evict();
		}
	}
	
	public void setMinMax(boolean isMinMax) {
		if (isMinMax != this.minMax) {
			this.minMax = isMinMax;
			evict();
		}
	}
	
	public void invalidate() {
		evict();
	}
	
	/**
	 * Evict data items between start and end times (inclusive)
	 * 
	 * @param start
	 * @param end
	 */
	public native void evict(double start, double end) /*-{
		@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Evict");
		var left = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(start);
		if (left < 0)
			left = -left - 2;
		if (left < 0)
			left = 0;
		var count = 0;
		
		var len = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length;
		while (this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[left + count][0] < end &&
			left + count < len)
			count++;
			
		this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.splice(left,count);
	}-*/;
	
	public native void evict() /*-{
		this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data = [];
		@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("** FLUSHED CACHE **");
	}-*/;
	
	public native void add(JsArray<JsArrayInteger> arr) /*-{

		var len = arr.length;
		if (!len)
			return;
		
		var minMax = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::minMax;
		var incomingValid = $wnd.DDataSet.validate(arr, minMax);
		if (!incomingValid) {
			throw "Incoming array not valid for: " + minMax;
		}
		
		@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Adding " + len + " samples by " + arr[0].length + " channels");
		
		var left = arr[0][0];
		var right = arr[len - 1][0];
		
		var d = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
		var dlen = d.length;
		
		if (!dlen) {
			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data = arr;
			return;
		}
		
		
		@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Existing range: " + d[0][0] + " through " + d[dlen-1][0]);

		var index = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(left);
		var index2 = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::lastIndexOf(D)(right);
		
		if (index2 == -1) {
			@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Prepend");
			// Everything is to start
			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data				
			  = arr.concat(this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data);
//			d.unshift(arr);
		} else {
			if (index == -1) {
				index = 0;
			} else if (minMax && index < 0) {
				index = -index - 2 + 2;	// Advance *past* the start position
			} else if (!minMax && index < 0) {
				index = -index - 2 + 1;	// Advance *past* the start position
			}
				
			if (index2 >= 0) {
				//Move past matching timestamp so that we use the new sample value
				index2++;
			} else if (minMax && index2 < 0) {
				index2 = -index2 - 2 + 2;
			} else if (!minMax && index2 < 0) {
				index2 = -index2 - 2 + 1;
			}

			@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Left index: " + index);
			@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Right index: " + index2);
		
			if (index < dlen)
			  @edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Overlapping values: " + d[index][0]);
			else
			  @edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)(" from eoa");
			if (index2 < dlen)
			  @edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)(" through " + d[index2][0]);
			else
			  @edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)(" through eoa");

			if (index < dlen)
				@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("Replacing starting at index " + d[index][0]);
			  
			// Some overlap?
			
//			d.splice(index,(index2 - index ), arr[0]);
//			for (var i = 1; i < arr.length; i++)
//				d.splice(index+i, 0, arr[i]);

			var leftSeg = [];
			if (index > 0)
				leftSeg = d.slice(0, index);
				
			var rightSeg = []
			if (index2 < dlen)
				rightSeg = d.slice(index2);

			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data				
			  = leftSeg.concat(arr, rightSeg);
			  
//			 d = this.@edu.upenn.cis.db.mefview.client.SnapshotCache::data;
		}
		var valid = $wnd.DDataSet.validate(this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data, minMax);
		if (!valid) {
			throw "Resulting array not valid for minMax == " + minMax;
		}

	}-*/;
	
	public boolean filtersEqual(List<DisplayConfiguration> filters) {
		if (this.filter == null || this.filter.size() != filters.size())
			return false;
		
		for (int i = 0; i < filters.size(); i++) {
			if (!(this.filter.get(i).equals(filters.get(i))))
				return false;
		}
		
		return true;
	}

	public native JsArray<JsArrayInteger> getEntry(double start, double end, double period, 
			List<DisplayConfiguration> filter) /*-{
		var left = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(start);
		var right = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::lastIndexOf(D)(end);
		
		if (left < 0)
			left = -left - 2;
		if (left < 0)
			left = 0;
			
		if (right < 0)
			right = -right - 2;
			
		if (right < 0)
			right = 0;
		
		// TODO: interpolate??
		
		var d = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
		
		if (right > d.length)
			right = d.length;
			
			// TODO: test that this works and doesn't side effect!!
		var entry = d.slice(left,right);
		var valid = $wnd.DDataSet.validate(entry, this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::minMax);
		if (!valid) {
			throw "SnapshotCache.getEntry(double ...): Invalid slice";
		} 
		return entry;
	}-*/;
	
	public native JsArray<JsArrayInteger> getEntry(int left, int right, 
			double start, double end, double period, 
			List<DisplayConfiguration> filter) /*-{
		
		// TODO: interpolate??
		
		var d = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
		var dlen = d.length;
		
		if (right > dlen)
			right = dlen;
			
			// TODO: test that this works and doesn't side effect!!
		var entry = d.slice(left,right);
		var valid = $wnd.DDataSet.validate(entry, this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::minMax);
		if (!valid) {
			throw "SnapshotCache.getEntry(int ...): Invalid slice";
		} 
		return entry
	}-*/;
	
//	public JsArray<JsArrayInteger> getEntry(List<String> channels, long start, long end, double period,  
//			List<FilterParameters> filter) {
//		return null;
//	}
	
	public void checkIntegrity(double period) {
		checkIntegrity(data, period);
	}
	
	public native void checkIntegrity(JsArray<JsArrayInteger> data, double period) /*-{
		return;
		if (!data.length) {
			console.error("* INTEGRITY * WARNING: EMPTY DATA");
			return;
		}
		
		var count = data[0].length;
		var i;
		for (i = 1; i < data.length; i++) {
			if (data[i].length != count) {
				console.error("* INTEGRITY * WARNING: IRREGULAR COUNT");
			}
		}
		
		period = (period + 1) |0;

		var last = data[0][0];
		var msg = "### Integrity wrt " + period + ": " + last;
		for (i = 1; i < data.length; i++) {
			if (data[i][0] <= data[i-1][0]) {
				console.error("Error: integrity violation at position " + i + " (" + data[i][0] + ")");
			} else if (data[i][0] - last > period) {
				msg = msg + "-" + last + ", " + data[i][0];
				last = data[i][0];
			} else if (i == data.length-1) {
				msg = msg + "-" + last;
			} else
				last = data[i][0];
		}
		
		if (console && console.debug)
		    console.debug(msg);
		
//		var j;
//		for (i = 0; i < data.length; i++) {
//			for (j = 0; j < data[i].length; j++) {
//				if (data[i][j] == null)
//					;
//				else if (isNaN(data[i][j]))
//					console.error("* INTEGRITY * WARNING: NON-NUMERIC " + i + "," + j + ": " + data[i][j]);
//			}
//		}
	}-*/; 

	public boolean haveMatch(long start, long end, double period, List<DisplayConfiguration> filter) {
		try {
			int left = indexOf(start);
			int right = lastIndexOf(end);
			
			if (left < 0)
				left = -left - 2;
			if (left < 0)
				left = 0;
			
			if (right < 0)
				right = -right - 2;
			
			return isExactMatch(left, right, start, end, period, filter);
		} catch (Exception e) {
			return false;
		}
	}
	
	public native boolean isExactMatch(int left, int right, double start, double end, 
			double period, List<DisplayConfiguration> filter) /*-{
		var d = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
		var len = d.length;
		
		period = (period + 1) |0;
		
		if (len == 0)
			return false;
		
		if (right < 0) {
			right = 0;
			return false;
		}
		
		if (left > len - 1)
			return false;

		if (right > len - 1)
			right = len - 1;
			
		var early = d[left][0];
		var late = d[right][0];
		
		console.debug("Cache bounds: " + d[0][0] + "-" + d[len-1][0] +" and we found " + early + "-" + late);
		
		if (Math.abs(start - early) > period) {
			console.debug("-- left bound is too far");
			return false;
		}
			
		if (Math.abs(end - late) > period) {
			console.debug("-- right bound is too far");
			return false;
		}
		var isMinMax = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::minMax;
		var numXValues = isMinMax ? (right - left) / 2 : (right - left);
		var ratio = (late - early) / numXValues; 
		
		
		if (ratio > period || ratio < period * 0.9) {
			console.debug("-- period of " + period + " doesn't multiply");
			return false;
		}
		
//		for (var i = left + 1; i++; i < right) {
//			if (d[i][0] - d[i-1][0] < period2)
//				return false;
//		} 
		
//		if (!this.@edu.upenn.cis.db.mefview.client.SnapshotCache::filtersEqual(Ljava/util/List;)(filter))
//			return false;
		
			console.debug("-- exact match");
		return true;
	}-*/;
	

	public boolean isMatch(int left, int right, long start, long end, double period, List<DisplayConfiguration> filter) {
		
		debug("Looking for " + start + "-" + end + ": positions " + left + " and " + right + " (size = " + data.length() + " with " + DataConversion.getUUValue(getTimeFor(0)) + "-" + DataConversion.getUUValue(getTimeFor(data.length() - 1)) + ")");
		
		int len = data.length();

		if (len == 0)
			return false;
		
		if (filter != null && !filtersEqual(filter))
			return false;
		
		if (right < 0) {
			right = 0;
			return false;
		}
		
		if (left > len - 1)
			return false;

		if (right > len - 1)
			right = len - 1;
		
		period = period + 1;
		
		long early = (long)getTimeFor(left);
		long late = (long)getTimeFor(right);
		
//		debug("Comparing versus endpoints: at " + left + " and " + right + ", vs " + start + " and " + end);
		debug("Found values " + early + " and " + late);

		try {
			if (!isInRange(start, left, period)) {
				debug("* Left coordinate not within period: " + start + " vs " + DataConversion.getUUValue(getTimeFor(left)) + " and period " + period);
				return false;
			}
//			else
//				debug("Passed left");
			
			if (!isInRange(end, right, period)) {
				debug("* Right coordinate not within period: " + end + " vs " + DataConversion.getUUValue(getTimeFor(right)) + " and period " + period);
				return false;
			}
//			else
//				debug("Passed right");
			
//			debug("Looking at relative differences");
			for (int i = left + 1; i <= right; i++) {
				if ((getTimeFor(i) - getTimeFor(i-1)) > period) {
					debug("* Too big gap between " + (long)getTimeFor(i) + " and " + (long)getTimeFor(i-1));
					return false;
				}
			}
			
			debug("* Complete match");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public boolean isMinMax() {
		return this.minMax;
	}
	
	private native double getValueAt(int pos, int channel) /*-{
		if (pos >= this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length)
			throw("getValueAt out of bounds");
			
		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos][channel];
	}-*/;
	
	private native double getTimeFor(int pos) /*-{
//		if (pos >= this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length) {
//			@edu.upenn.cis.db.mefview.client.SnapshotCache::debug(Ljava/lang/String;)("ERROR: OUT OF BOUNDS");
//			throw("getTimeFor out of bounds");
//		} else if (!this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos]
//		|| this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos].length == 0) {
//			@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::debug(Ljava/lang/String;)("ERROR: MISSING ELEMENT");
//			throw("getTimeFor out of bounds");
//		} else if (isNaN(this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos][0])) {
//			@edu.upenn.cis.db.mefview.client.models.SnapshotCache::debug(Ljava/lang/String;)("ERROR: Non-number: " + this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos][0]);
//			return 0;
//		}

		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos][0];
	}-*/;
	
	private native boolean isInRange(double start, int pos, double period) /*-{
		if (pos >= this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length)
			return false;
			
		return Math.abs(start - this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[pos][0]) <= period;
	}-*/;
	
	public boolean haveData() {
		return data.length() > 0;
	}

	public int getLeftIndex(long start) {
		int left = indexOf(start);
		
		if (left < 0)
			left = -left - 2;
		if (left < 0)
			left = 0;
		
		return left;
	}
	
	public int getRightIndex(long end) {
		int right = lastIndexOf(end);
		
		if (right < 0) {
			right = -right - 2;
		} else if (minMax && right > 0 ){
			//If there was a match we got the second sample so deincrement to the first sample so we maintain pairs
			right--; 
		}
		if (right < 0)
			right = 0;
		
		return right;
	}
	
	public native boolean isPartialMatch(int left, int right) /*-{
		return (left < right);
	}-*/;

	public boolean havePartialMatch(long start, long end, double period, 
			List<DisplayConfiguration> filter) {
		if (data.length() == 0)
			return false;
		
		int left = indexOf(start);
		int right = lastIndexOf(end);
		
		if (left < 0)
			left = -left - 2;
		if (left < 0)
			left = 0;
		if (right < 0)
			right = -right - 2;
		if (right < 0)
			right = 0;
			
		return (left < right);
	}
	
//	public boolean havePartialMatch(List<String> channels, long start, long end, 
//			List<FilterParameters> filter) {
//		return false;
//	}
	
	// Data set constructor requires data
	//  to be in an array of arrays, of the
	//  form [[x1,y1,y2,y3],[x2,y1,y2,y3]...]
	//  where the x's are in ascending order
	//
//	public native void setValues(JsArray<JsArrayInteger> data) /*-{
//		this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data = [];
//	  var dl = data.length;
//	  
//	  if (data.length == 0)
//	    return;
//	  for (var i = 0; i < dl; i++) {
//	    var dil = data[i].length;
//	    
//	    this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.push(new Array(dil));
//	    this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[i][0] = data[i][0];
//	
//	    for (var j = 1; j < dil; j++) {
//	    	this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[i][j] = data[i][j];
//	    }
//	  }
//	}-*/;

	public native int getNumXValues() /*-{
		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length;
	}-*/;

	public native int getNumYValues() /*-{
		if (this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length == 0)
			return 0;
		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[0].length;
	}-*/;

	public native int getMinX() /*-{
		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[0][0];
	}-*/;

	public native int getMaxX() /*-{
		return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length - 1][0];
	}-*/;


	/**
	 * Binary search for an x value within the data set, returning the index of
	 * the first occurrence of value, or its predecessor if the value is
	 * missing. Uses negative numbers for predecessors!
	 * 
	 * Return code status: 
	 * Positive number: first index of the value 
	 * Negative 1 (-1): belongs before the start of the dataset 
	 * Other negative value: belongs after (-value - 2)
	 */
	public int indexOf(double val) {
		return indexOf_(val, 0, data.length() - 1, true);
	}
	
	/**
	 * Binary search for an x value within the data set, returning the index of
	 * the last occurrence of value, or its predecessor if the value is
	 * missing. Uses negative numbers for predecessors!
	 * 
	 * Return code status: 
	 * Positive number: last index of the value 
	 * Negative 1 (-1): belongs before the start of the dataset 
	 * Other negative value: belongs after (-value - 2)
	 */
	public int lastIndexOf(double val) {
		return indexOf_(val, 0, data.length() - 1, false);
	}

	/**
	 * Binary search for an x value within the data set. If the value is found
	 * and {@code firstIndex} is true the first index of the value is returned.
	 * If the value is found and {@code firstIndex} is false the last index is
	 * returned. If the value is missing the predecessor is returned. Uses negative numbers for
	 * predecessors!
	 * 
	 * Return code status: 
	 * Positive number: index of the value 
	 * Negative 1 (-1): belongs before the start of the dataset 
	 * Other negative value: belongs after (-value - 2)
	 */
	private native int indexOf_(double val, int min, int max, boolean firstIndex) /*-{
//		console.debug("Index of: " + val + " between " + min + " and " + max);
		var d = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
		 
		if (val > d[d.length - 1][0]) {
			var pred = d.length - 1;
			var lastVal = d[pred][0];
			while (pred >= 0 && d[pred][0] == lastVal) {
				pred--;
			}
			pred++;
			return -pred - 2;
		}
		
		while (true) {
			if (max < min) {
				var pred;
				if (max >= 0) {
					if (max < d.length - 1 && d[max + 1][0] < val) {
						//return (-max - 1);
						pred = max == 0 ? -1 : max - 1;
					} else {
						//return (-max - 2);
						pred = max;
					}
				} else {
					//return max;
					pred = -max - 2;
				}

				if (pred == -1) {
					return pred;
				}
				var predVal = d[pred][0];
				while (pred >= 0 && d[pred][0] == predVal) {
					pred--;
				}
				pred++;

				return -pred - 2;
			}
			
			var mid = parseInt((min + max) / 2);
			
			if (d[mid][0] > val) {
				max = mid - 1;
			} else if (d[mid][0] < val) {
				min = mid + 1;
			} else {
				var index = mid;
				if (firstIndex) {
					while (index >= 0 && d[index][0] == val) {
						index--;
					}
					index++;
				} else {
					while (index < d.length && d[index][0] == val) {
						index++;
					}
					index--;
				}
				return index;
			}
		}
	}-*/;

//	private native void insertCol(JsArrayInteger colData) /*-{
//		var pos = colData[0];
//		var thisData = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
//	
//		var index = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(pos);
//	
//		if (index == -1) {
////			debug("insertCol - Adding column to start");
//			return this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.unshift(colData);
//		} else {
//			
//			if (index >= 0) {
//				var count = 0;
//				while (index + count < thisData.length && thisData[index + count][0] == colData[0]) {
//					count++;
//				}
////				debug("insertCol - Inserting position " + pos + " at index " + index);
//				this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.splice(index,count,colData);
//			} else {
////				debug("insertCol - Inserting position " + pos + " at index " + (-index - 1));
//				this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.splice(-index - 1,0,colData);
//			}
//		}
//	}-*/;
	
	///////////////////
	// Insert a column worth of data (assumed to be the right "height").
	// Optional position attribute specifies what x-value to use.
	// Optional flag insertInstedofReplace specifies to leave duplicate
	//   values instead of replacing them.
	//
//	private native void insertCol(JsArrayInteger colData, int pos,
//			int insertInsteadofReplace) /*-{
//		if (!pos)
//			pos = colData[0];
//			
//		var thisData = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
//	
//		var index = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(pos);
//	
//		if (index == -1) {
//			return thisData.unshift(colData);
//		} else {
//			if (index >= 0) {
//				var count = 0;
//				if (!insertInsteadofReplace) {
//					while (index + count < thisData.length && thisData[index + count][0] == colData[0]) {
//						count++;
//					}
//				}
//	//	 	 	debug("Inserting position " + pos + " at index " + index);
//				thisData.splice(index,count,colData);
//			} else {
//	//	 	 	debug("Inserting position " + pos + " at index " + (-index - 1));
//				thisData.splice(-index - 1,0,colData);
//			}
//		}
//	}-*/;

	////////////////////
	// Adds a column of data (assumed to be of the right height) to
	// the end of the list.  This value must have an x-coordinate
	// greater than the last element in the list to maintain the
	// list invariants.
	//
//	public native void appendCol(JsArrayInteger colData) /*-{ 
//		 this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.push(colData);
//	}-*/;
		 
	////////////////////////
	//Inserts a row of data, assuming that the
	// x-coordinates are already aligned
//	public native void insertRow(JsArrayInteger rowData, int pos) /*-{ 
//		var len = rowData.length;
//		
//		if (len != this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length) {
//			throw("Mismatched row count in inserting data!");
//			return;
//		}
//		
//		for (var i = 0; i < len; i++) {
//			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[i].splice(pos,0,rowData[i]);
//		}
//	}-*/;

	////////////////////////////
	// Appends a row of data, assuming that the
	// x-coordinates are already aligned
	//
//	public native void appendRow(JsArrayInteger rowData) /*-{
//		var len = rowData.length;
//		
//		if (len != this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length) {
//			throw ("Mismatched row count in inserting data!");
//			return;
//		}
//		
//		for (var i = 0; i < len; i++) {
//			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[i].push(rowData[i]);
//		}
//	}-*/;

	////////////////////////////
	// Removes the specified x-value's data, shifting
	// the list as necessary.  
//	private native void deleteCol(double pos) /*-{
//		var index = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::indexOf(D)(pos);
//		
//		if (index < 0)
//			return;
//		else {
//			var thisData = this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data;
//			var count = 0;
//			while (index + count < thisData.length && thisData[index + count][0] == pos) {
//				count++;
//			}
//			thisData.splice(index,count);
//		}
//	}-*/;

	////////////////////////////
	//Removes the specified row's data, shifting
	//the list as necessary.  Recomputes the minimum and
	//maximum y-values in the graph.
//	public native void deleteRow(int rowToDelete) /*-{
//		for (var i = 0; i < this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data.length; i++) {
//			this.@edu.upenn.cis.db.mefview.client.controller.SnapshotCache::data[i].splice(rowToDelete, 1);
//		}
//	}-*/;
	
//Test driver
//DDataSet.prototype.test = function() {
//	var test = new DDataSet([[1,2,3,4,5,6,7],
//	                         [4,1,2,3,4,5,6],
//	                         [8,9,10,11,12,13,14],
//	                         [20,21,22,23,24,25,26]]);//,
//	                        //['a','b','c','d','e','f']);
//	
//	// Search tests
//	Dygraph.log(TsDygraph.DEBUG,"Initial array: " + JSON.stringify(test.data));
//	var result = test.indexOf(1);
//	if (test.data[result][0] != 1)
//		console.error("ERROR: Search for 1: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(4);
//	if (test.data[result][0] != 4)
//		console.error("ERROR: Search for 4: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(8);
//	if (test.data[result][0] != 8)
//		console.error("ERROR: Search for 8: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(20);
//	if (test.data[result][0] != 20)
//		console.error("ERROR: Search for 20: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(18);
//	if (result > 0 || test.data[-result - 2][0] != 8)
//		console.error("ERROR: Search for missing value 8 does not show predecessor: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(21);
//	if (test.data[-result - 2][0] != 20)
//		console.error("ERROR: Search for missing value 21 does not show predecessor: " + result + "/" + test.data[result]);
//	
//	result = test.indexOf(0);
//	if (result != -1)
//		console.error("ERROR: Search for missing value 0 does not before-list value: " + result + "/" + test.data[result]);
//	
//	// Col insert tests
//	test.insertCol([5,6,7,8,9,10,11]);
//	if (test.data[test.indexOf(5)][0] != 5)
//		console.error("ERROR: Insert of col position 5 failed: " + JSON.stringify(test.data));
//	test.insertCol([0,7,8,9,10,11,12]);
//	if (test.data[test.indexOf(0)][0] != 0)
//		console.error("ERROR: Insert of col position 0 failed: " + JSON.stringify(test.data));
//	test.insertCol([19,26,27,28,29,30,31]);
//	if (test.data[test.indexOf(19)][0] != 19)
//			console.error("ERROR: Insert of col position 19 failed: " + JSON.stringify(test.data));
//	test.insertCol([50,56,57,58,59,50,51]);
//	if (test.data[test.indexOf(50)][0] != 50)
//			console.error("ERROR: Insert of col position 50 failed: " + JSON.stringify(test.data));
//	
//	// Row insert tests
//	var height = test.data[0].length;
//	test.appendRow([75,76,77,78,79,80,81,82]);
//	if (height +1 != test.data[0].length || test.data[0][test.data[0].length - 1] != 75)
//			console.error("ERROR: Append of row failed: " + JSON.stringify(test.data));
//	test.insertRow([45,46,47,48,49,40,41,42],3);
//	if (height +2 != test.data[0].length || test.data[0][3] != 45)
//			console.error("ERROR: Insert of row position 5 failed: " + JSON.stringify(test.data));
//	
//	test.deleteRow(5);
//	// Row delete tests
//	if (test.data[0].length != height + 1)
//			console.error("ERROR: Delete of row position 5 failed: " + JSON.stringify(test.data));
//	
//	 // Col delete tests
//	test.deleteCol(5);
//	if (test.indexOf(5) >= 0)
//			console.error("ERROR: Delete of col position 5 failed: " + JSON.stringify(test.data));
//	var len = test.data.length;
//	test.deleteCol(5);
//	if (test.data.length != len)
//			console.error("ERROR: Delete of col position 5, now missing, affected array: " + JSON.stringify(test.data));
//}
	public static void debug(String str) {
		GWT.log(str);
	}
	
	public static void debug2(String str) {
		GWT.log(str);
	}
	
//	public static native void debug(String str) /*-{
////		if (console && console.debug)
////		  console.debug(str);
//	}-*/;
//
//	public static native void debug2(String str) /*-{
////	if (console && console.debug)
////	  console.debug(str);
//	}-*/;
}
