/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.messages.SnapshotChangedMessage;

public class SnapshotChangedEvent extends GwtEvent<SnapshotChangedEvent.Handler> {
	private static final Type<SnapshotChangedEvent.Handler> TYPE = new Type<SnapshotChangedEvent.Handler>();
	
	private final SnapshotChangedMessage action;
	
	public SnapshotChangedEvent(SnapshotChangedMessage action){
		this.action = action;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<SnapshotChangedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SnapshotChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SnapshotChangedEvent.Handler handler) {
		handler.onSnapshotChanged(action);
	}

	public static interface Handler extends EventHandler {
		void onSnapshotChanged(SnapshotChangedMessage action);
	}
}
