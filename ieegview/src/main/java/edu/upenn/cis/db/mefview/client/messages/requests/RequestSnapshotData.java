/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages.requests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class RequestSnapshotData {
	public String snapshotID;
//	DataSnapshotModel model;
	
	public List<TimeSeriesSpanSpecifier> requestedSpans;
	
	public List<DisplayConfiguration> filters;
	
	public long start;
	public long duration;
	
	public int[] requestIds;
	
	public boolean dontProcess = false;
	
	public boolean dontCache = false;
	
	String context;
	
	public RequestSnapshotData(//DataSnapshotModel model, 
			String snapshotID,
			String contextID, //List<INamedTimeSegment> channels,
			List<TimeSeriesSpanSpecifier> requestedSpans, List<DisplayConfiguration> filters,
			long start, long duration, 
			int[] ids) {
		requestIds = ids;
//		this.model = model;
	
		this.start = start;
		this.duration = duration;
		this.snapshotID = snapshotID;
		this.filters = filters;
//		this.channels = channels;
		this.requestedSpans = requestedSpans;
		context = contextID;
	}
	
	public RequestSnapshotData(String snapshotID,
			String contextID, //INamedTimeSegment channel,
			TimeSeriesSpanSpecifier requestedSpan, DisplayConfiguration filter, long start,
			long duration, int id) {
		this.start = start;
		this.duration = duration;
		this.snapshotID = snapshotID;
		context = contextID;
		requestIds = new int[1];
		requestIds[0] = id;
//		channels = new ArrayList<INamedTimeSegment>();//TimeSeriesMetadata>();
//		channels.add(channel);
		filters = new ArrayList<DisplayConfiguration>();
		filters.add(filter);
		
		requestedSpans = new ArrayList<TimeSeriesSpanSpecifier>();
		requestedSpans.add(requestedSpan);
	}

	/**
	 * Merges together multiple requests
	 * 
	 * @param requests
	 * @return
	 */
	public static RequestSnapshotData union(Collection<RequestSnapshotData> requests) {
//		ArrayList<TimeSeriesMetadata> channels = new ArrayList<TimeSeriesMetadata>();
//		ArrayList<INamedTimeSegment> channels = new ArrayList<INamedTimeSegment>();
		ArrayList<TimeSeriesSpanSpecifier> requestedSpans = new ArrayList<TimeSeriesSpanSpecifier>();
		ArrayList<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();

		int size = 0;
		for (RequestSnapshotData action: requests) {
			size += action.requestIds.length;
		}
		int[] ids = new int[size];
		
		String snapshot = "";
		String context = "";
		
		long start = Long.MAX_VALUE;
		long duration = 0;
		double voltConv = 0;
		DataSnapshotModel model = null;
		for (RequestSnapshotData action: requests) {
			int req = 0;
//			List<TimeSeriesSpanSpecifier> spans = action.requestedSpans;
			for (int i = 0; i < action.requestedSpans.size(); i++) {
//				channels.add(spans.get(i).getTimeSegment());
				requestedSpans.add(action.requestedSpans.get(i));
				filters.add(action.filters.get(i));
				ids[req++] = action.requestIds[i];
			}
			if (snapshot.isEmpty())
				snapshot = action.snapshotID;
			else if (!snapshot.equals(action.snapshotID))
				throw new RuntimeException("Union from different snapshots");
			
			if (start > action.start)
				start = action.start;
			
			if (start + duration < action.start + action.duration)
				duration = action.start + action.duration - start;
			
			context = action.context;
//			model = action.getModel();
		}
		
		return new RequestSnapshotData(//model, 
				snapshot, context, //channels, 
				requestedSpans, filters, 
				start, duration, ids);
	}
	
	public void setDontProcess() {
		this.dontProcess = true;
	}
	
	public boolean dontProcess() {
		return this.dontProcess;
	}
	
	public long getStart() {
		return start;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public String getContext() {
		return context;
	}
	
	public void disableCache() {
		this.dontCache = true;
	}
	
	public boolean isCacheEnabled() {
		return !dontCache;
	}
	
//	public void setModel(DataSnapshotModel model) {
//		this.model = model;
//	}
//	
//	public DataSnapshotModel getModel() {
//		return model;
//	}
}
