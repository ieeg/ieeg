/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import com.google.gwt.place.shared.PlaceController;

import edu.upenn.cis.db.mefview.shared.places.WorkPlace;

public interface ClientFactory {
	public interface IAppSaveAndShutdown {
		public WorkPlace getWorkPlace();
		public void removeCloseHandlers();
	}
	
	public PlaceController getPlaceController();
	
	public SnapshotServicesAsync getSnapshotServices();
	
	public ProjectServicesAsync getProjectServices();
	public void setProjectServices(ProjectServicesAsync proj);
	
	public SyncServicesAsync getSyncServices();
	public void setSyncServices(SyncServicesAsync sync);
	
	public EEGDisplayAsync getPortal();
	public void setPortal(EEGDisplayAsync portal);

	public SearchServicesAsync getSearchServices();
	public void setSearchServices(SearchServicesAsync search);
	
	public ServerAccess getPortalServices();
	public void setPortalService(ServerAccess c);

	public SessionModel getSessionModel();
	public void setSessionModel(SessionModel session);
	
	TaskServicesAsync getTaskServices();

	void setTaskServices(TaskServicesAsync tasks);
  
}
