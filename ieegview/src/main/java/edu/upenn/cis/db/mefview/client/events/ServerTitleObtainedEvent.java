/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class ServerTitleObtainedEvent extends GwtEvent<ServerTitleObtainedEvent.Handler>{
	private static final Type<ServerTitleObtainedEvent.Handler> TYPE = new Type<ServerTitleObtainedEvent.Handler>();

	private final String newTitle;
	
	public ServerTitleObtainedEvent(String title){
		newTitle = title;
	}

	

	public String getTitle() {
		return newTitle;
	}



	public static com.google.gwt.event.shared.GwtEvent.Type<ServerTitleObtainedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ServerTitleObtainedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ServerTitleObtainedEvent.Handler handler) {
		handler.onNewTitle(newTitle);
	}

	public static interface Handler extends EventHandler {
		void onNewTitle(String newTitle);
	}
}
