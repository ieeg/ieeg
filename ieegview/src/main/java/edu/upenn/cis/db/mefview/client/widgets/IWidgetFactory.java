package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.user.client.ui.Widget;

public interface IWidgetFactory {
	public interface WidgetClickHandler {
		public void onClick(int clientX, int clientY);
	};
	
	public Widget createButton(String button);
	
	public Widget createIconButton(String imageUri);
	
	public void addClickHandler(Widget w, WidgetClickHandler handler);
}
