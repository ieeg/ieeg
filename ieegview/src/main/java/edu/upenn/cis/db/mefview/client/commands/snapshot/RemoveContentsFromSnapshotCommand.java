package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.Set;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.DialogBox;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class RemoveContentsFromSnapshotCommand implements Command {
	final String dataset;
	final Set<String> seriesToRemove;
	final DialogBox waitDialog; 
	final OnDoneHandler handler;
	final ClientFactory clientFactory;
	
	public RemoveContentsFromSnapshotCommand(
			final ClientFactory clientFactory,
			final String dataset,
			final Set<String> seriesToRemove, 
			final DialogBox waitDialog, 
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.seriesToRemove = seriesToRemove;
		this.waitDialog = waitDialog;
		this.handler = handler;
		this.clientFactory = clientFactory;
	}

	@Override
	public void execute() {
		clientFactory.getSnapshotServices().removeDataSnapshotContents(
				clientFactory.getSessionModel().getSessionID(), 
				dataset, 
				seriesToRemove, 
				new SecFailureAsyncCallback<String>(clientFactory) {

			public void onSuccess(String result) {
				if (waitDialog != null)
					waitDialog.hide();
				if (handler != null)
					handler.doWork(dataset);
			}

			@Override
			protected void onFailureCleanup(Throwable caught) {
				if (waitDialog != null)
					waitDialog.hide();
			}

			@Override
			protected void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Unable to Remove Series", "Sorry, there was an error removing the series.");				
			}

		});
	}

}
