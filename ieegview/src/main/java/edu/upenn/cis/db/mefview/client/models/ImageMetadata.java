/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.annotations.GwtCompatible;

import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.ImageInfo;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.VALUE_TYPE;

@GwtCompatible(serializable=true)
public class ImageMetadata implements Comparable<ImageMetadata>, SerializableMetadata {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	String id;
	String url;
	String type;
	int posX;
	int posY;
	
	SerializableMetadata snapshot;
	
	String nextSlice;
	String prevSlice;

	public static Map<String, VALUE_TYPE> valueMap =
			new HashMap<String, VALUE_TYPE>();
			
	static {
		valueMap.put("id", VALUE_TYPE.STRING);
		valueMap.put("label", VALUE_TYPE.STRING);
		valueMap.put("url", VALUE_TYPE.STRING);
		valueMap.put("type", VALUE_TYPE.STRING);
		valueMap.put("nextSlice", VALUE_TYPE.STRING);
		valueMap.put("prevSlice", VALUE_TYPE.STRING);

		valueMap.put("posX", VALUE_TYPE.INT);
		valueMap.put("posY", VALUE_TYPE.INT);
	}
	
	public ImageMetadata(String url, String name, String type) {
		this.name = name;
		this.type = type;
		this.url = url;
		posX = 0;
		posY = 0;
	}
	public ImageMetadata(String url, String name, String type, int posX, int posY) {
		this.name = name;
		this.url = url;
		this.type = type;
		this.posX = posX;
		this.posY = posY;
	}
	
	public ImageMetadata(ImageInfo i) {
		setName(i.getName());
		setId(i.getId());
		
		setUrl(i.getUrl());
		
		setType(i.getType());
		setPosX(i.getPosX());
		setPosY(i.getPosY());
	}
	
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int compareTo(ImageMetadata o) {
		if (id == null && o.id != null)
			return -1;
		else if (id == null && o.id == null)
			return 0;
		else if (o.id == null)
			return 1;
		else 
			return id.compareTo(o.id);
	}
	@Override
	public String getLabel() {
		return getName();
	}
	@Override
	public Set<String> getKeys() {
		return valueMap.keySet();
	}
	@Override
	public String getStringValue(String key) {
		if (key.equals("id"))
			return getId();
		if (key.equals("label"))
			return getLabel();
		if (key.equals("url"))
			return getUrl();
		if (key.equals("type"))
			return getType();
		if (key.equals("nextSlice"))
			return nextSlice;
		if (key.equals("prevSlice"))
			return prevSlice;

		return null;
	}
	@Override
	public VALUE_TYPE getValueType(String key) {
		return valueMap.get(key);
	}
	@Override
	public Double getDoubleValue(String key) {
		return null;
	}
	@Override
	public Integer getIntegerValue(String key) {
		if (key.equals("posX"))
			return getPosX();
		if (key.equals("posY"))
			return getPosY();

		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public GeneralMetadata getMetadataValue(String key) {
		return null;
	}
	@Override
	public List<String> getStringSetValue(String key) {
		return null;
	}
	@Override
	public List<GeneralMetadata> getMetadataSetValue(String key) {
		return null;
	}
	@Override
	public SerializableMetadata getParent() {
		return snapshot;
	}

	public void setParent(SerializableMetadata parent) {
		snapshot = parent;
	}

	@Override
	public void setParent(GeneralMetadata p) {
		if (p instanceof SerializableMetadata)
			setParent((SerializableMetadata)p);
		else
			throw new RuntimeException("Attempted to set non-serializable parent");
	}

	@Override
	public String getMetadataCategory() {
		return BASIC_CATEGORIES.Images.name();
	}
}
