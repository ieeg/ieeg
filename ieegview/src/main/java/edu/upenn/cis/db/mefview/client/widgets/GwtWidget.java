package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.user.client.ui.Widget;

public class GwtWidget extends Widget implements Attachable {
	  public void attach() {
	    onAttach();
	  }
}