/*
 * Copyright 2015 IEEG.org 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;


import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public class EegMontageRemovedEvent extends GwtEvent<EegMontageRemovedEvent.Handler> {
    private static final Type<EegMontageRemovedEvent.Handler> TYPE = new Type<>();
    
    private final EEGMontage montage;
    private final IEEGViewerPanel window;
    
    public EegMontageRemovedEvent(IEEGViewerPanel window, EEGMontage montage){
        this.montage = montage;
        this.window = window;
    }
    
    public static com.google.gwt.event.shared.GwtEvent.Type<EegMontageRemovedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<EegMontageRemovedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EegMontageRemovedEvent.Handler handler) {
        handler.onMontageRemoved(window, montage);
    }

    public static interface Handler extends EventHandler {
        void onMontageRemoved(IEEGViewerPanel w, EEGMontage montage);
    }
}