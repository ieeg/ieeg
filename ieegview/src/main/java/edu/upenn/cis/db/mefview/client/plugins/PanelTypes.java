package edu.upenn.cis.db.mefview.client.plugins;

public class PanelTypes {
	public final static String MASTER = "masterPanel";
	public final static String NEWS = "newsItem";
	public final static String SYNC = "syncItem";
	public final static String DATA = "dataItem";
//	public final static String VIEW = "viewItem";
	public final static String BROWSE = "browseItem";
	public final static String TOOLS = "toolsItem";
	public final static String COLLABORATIONS = "collabItem";
	public final static String TUTORIALS = "tutorialItem";
	public final static String HELP = "helpItem";
	
	public final static String MODELESS_DIALOG = "modelessDialogBox";
	public final static String MODAL_DIALOG = "modalDialogBox";
}
