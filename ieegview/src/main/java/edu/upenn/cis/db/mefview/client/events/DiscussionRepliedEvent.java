/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;

public class DiscussionRepliedEvent extends GwtEvent<DiscussionRepliedEvent.Handler>{
	private static final Type<DiscussionRepliedEvent.Handler> TYPE = new Type<DiscussionRepliedEvent.Handler>();
	
	private final DataSnapshotModel model;
	private final Project project;
	private final String refs;
	private final Post post;
	
	public DiscussionRepliedEvent(DataSnapshotModel model,
			final String refs, final Post post){
		this.model = model;
		this.project = null;
		this.refs = refs;
		this.post = post;
	}
	
	public DiscussionRepliedEvent(Project project,
			final String refs, final Post post){
		this.model = null;
		this.project = project;
		this.refs = refs;
		this.post = post;
	}

	public DataSnapshotModel getDataSnapshotState() {
		return model;
	}
	
	public Post getPost() {
		return post;
	}
	public static com.google.gwt.event.shared.GwtEvent.Type<DiscussionRepliedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DiscussionRepliedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DiscussionRepliedEvent.Handler handler) {
		if (project == null)
			handler.onDiscussionReplied(model, refs, post);
		else
			handler.onDiscussionReplied(project, refs, post);
	}

	public static interface Handler extends EventHandler {
		void onDiscussionReplied(DataSnapshotModel action, 
				String refs, Post post);
		void onDiscussionReplied(Project project, 
				String refs, Post post);
	}}
