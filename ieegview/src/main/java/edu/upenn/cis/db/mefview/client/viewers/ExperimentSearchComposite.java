/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import static com.google.common.collect.Iterables.get;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class ExperimentSearchComposite extends Composite {
	private ListBox species = new ListBox(true);
	private ListBox drugTypes = new ListBox(true);
	private ListBox stimTypes = new ListBox(true);
	private ListBox stimRegions = new ListBox(true);
	private ListBox organizations = new ListBox(true); 
	private FocusPanel fp = new FocusPanel();
	private DockLayoutPanel dp = new DockLayoutPanel(Unit.PX);
	// HorizontalPanel hp = new HorizontalPanel();
	private VerticalPanel east = new VerticalPanel();
	private VerticalPanel west = new VerticalPanel();
	private VerticalPanel patient = new VerticalPanel();
	private VerticalPanel operations = new VerticalPanel();
	private Button searchButton = new Button("Search");
	private Button cancelButton = new Button("Cancel");
	private DialogBox nothing = new DialogBox();
	private CheckBox cbReplace = new CheckBox("Replace results?");
	private DialogBox parent;
	private ExperimentSearchCriteria expSearchCrit;

	private boolean wasShowingInstructions = false;

	public ExperimentSearchComposite(final String user,
			final Button mainSearchButton,
			final ClientFactory clientFactory,
			final DialogBox parent,
			ExperimentSearchCriteria expSearchCrit) {
		this.parent = parent;
		this.expSearchCrit = expSearchCrit;

		for (Map.Entry<Long, String> thisSpecies : expSearchCrit.getSpecies()
				.entrySet()) {
			species.addItem(thisSpecies.getValue());
		}

		for (Map.Entry<Long, String> drug : expSearchCrit.getDrugs().entrySet()) {
			drugTypes.addItem(drug.getValue());
		}

		for (Map.Entry<Long, String> stimType : expSearchCrit.getStimTypes()
				.entrySet()) {
			stimTypes.addItem(stimType.getValue());
		}

		for (Map.Entry<Long, String> stimRegion : expSearchCrit
				.getStimRegions().entrySet()) {
			stimRegions.addItem(stimRegion.getValue());
		}

		for (Map.Entry<String, String> organization : expSearchCrit
				.getOrganizations().entrySet()) {
			organizations.addItem(organization.getValue());
		}

		dp.addEast(east, 150);
		dp.addWest(west, 150);
		// dp.addSouth(hp, 50);
		dp.setSize("550px", "600px"); // east.getOffsetHeight() + 50 + "px");//

		patient.add(new HTML("<strong>Experiment info</strong>"));

		patient.add(new HTML("&nbsp;"));
		patient.add(new Label("Species:"));
		patient.add(species);

		patient.add(new HTML("&nbsp;"));
		patient.add(new Label("Drugs:"));
		patient.add(drugTypes);

		patient.add(new HTML("&nbsp;"));
		patient.add(new Label("Stimulation types:"));
		patient.add(stimTypes);

		patient.add(new HTML("&nbsp;"));
		patient.add(new Label("Stimulation regions:"));
		patient.add(stimRegions);

		HorizontalPanel elec = new HorizontalPanel();
		patient.add(elec);
		VerticalPanel el = new VerticalPanel();
		elec.add(el);
		elec.add(new HTML("&nbsp;"));
		VerticalPanel co = new VerticalPanel();
		elec.add(co);

		dp.add(patient);

		VerticalPanel vp = new VerticalPanel();
		HorizontalPanel buttons = new HorizontalPanel();
		east.add(vp);
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));
		vp.add(new HTML("&nbsp;"));

		HTML h = new HTML("&nbsp;");
		vp.add(h);
		vp.setCellHeight(h, "90%");
		vp.add(cbReplace);
		cbReplace.setValue(true);
		vp.add(buttons);
		vp.setCellVerticalAlignment(buttons, HasVerticalAlignment.ALIGN_BOTTOM);

		east.setCellVerticalAlignment(vp, HasVerticalAlignment.ALIGN_BOTTOM);

		buttons.add(searchButton);
		buttons.add(cancelButton);
		final boolean prev = mainSearchButton == null ? false : mainSearchButton.isEnabled();
		cancelButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(prev);
				parent.hide();
			}

		});

		searchButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				cancelButton.setEnabled(false);
				searchButton.setEnabled(false);
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(false);

				parent.hide();

				clientFactory.getSearchServices().getSnapshotsFor(clientFactory.getSessionModel().getSessionID(),
						getSearchSpec(),
						new SecFailureAsyncCallback<Set<SearchResult>>(clientFactory) {

							public void onFailureCleanup(Throwable caught) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
								if (mainSearchButton != null)
									mainSearchButton.setEnabled(prev);
							}
							
							public void onNonSecFailure(Throwable caught) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
							}

							public void onSuccess(Set<SearchResult> result) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
								if (result.isEmpty()) {
									nothing.setText("Sorry, no results found");
									VerticalPanel vp = new VerticalPanel();
									vp.add(new HTML(
											"Perhaps your search was too restrictive, or this feature is not available."));
									Button ok = new Button("OK");
									vp.setCellHorizontalAlignment(ok,
											HasHorizontalAlignment.ALIGN_CENTER);
									ok.addClickHandler(new ClickHandler() {

										public void onClick(ClickEvent event) {
											nothing.hide();
										}

									});
									if (mainSearchButton != null)
										mainSearchButton.setEnabled(prev);
									vp.add(ok);
									nothing.setWidget(vp);
									nothing.center();
								} else {
									IeegEventBusFactory.getGlobalEventBus().fireEvent(
											new SearchResultsUpdatedEvent("(forms)",
													result, cbReplace
															.getValue()));
								}
							}
						});
			}
		});

		fp.add(dp);
		fp.addFocusHandler(new FocusHandler() {

			public void onFocus(FocusEvent event) {
				searchButton.setFocus(true);
			}

		});
		setWidget(fp);
		fp.setFocus(true);
	}

	public edu.upenn.cis.braintrust.shared.ExperimentSearch getSearchSpec() {

		Set<Long> drugIds = newHashSet();
		for (int i = 0; i < drugTypes.getItemCount(); i++) {
			if (drugTypes.isItemSelected(i)) {
				drugIds.add(get(expSearchCrit.getDrugs().keySet(), i));
			}
		}

		Set<Long> speciesIds = newHashSet();
		for (int i = 0; i < species.getItemCount(); i++) {
			if (species.isItemSelected(i)) {
				speciesIds.add(get(expSearchCrit.getSpecies().keySet(), i));
			}
		}

		Set<Long> stimTypeIds = newHashSet();
		for (int i = 0; i < stimTypes.getItemCount(); i++) {
			if (stimTypes.isItemSelected(i)) {
				stimTypeIds.add(get(expSearchCrit.getStimTypes().keySet(), i));
			}
		}

		Set<Long> stimRegionIds = newHashSet();
		for (int i = 0; i < stimRegions.getItemCount(); i++) {
			if (stimRegions.isItemSelected(i)) {
				stimRegionIds
						.add(get(expSearchCrit.getStimRegions().keySet(), i));
			}
		}

		ExperimentSearch eSearch = new ExperimentSearch(
				speciesIds, drugIds, stimTypeIds, stimRegionIds, 
				Collections.<String>emptySet());

		return eSearch;
	}

}
