/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.Date;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;

public class SnapshotRequestedEvent extends GwtEvent<SnapshotRequestedEvent.Handler> {
	private static final Type<SnapshotRequestedEvent.Handler> TYPE = new Type<SnapshotRequestedEvent.Handler>();
	
	private final RequestSnapshotData action;
	private long time;
	
	public SnapshotRequestedEvent(RequestSnapshotData action){
		this.action = action;
		time = new Date().getTime();
	}
	

	public long getTime() {
		return time;
	}


	public void setTime(long time) {
		this.time = time;
	}


	public static com.google.gwt.event.shared.GwtEvent.Type<SnapshotRequestedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SnapshotRequestedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SnapshotRequestedEvent.Handler handler) {
		handler.onSnapshotRequested(action);
	}

	public interface Handler extends EventHandler {
		void onSnapshotRequested(RequestSnapshotData action);
	}
}
