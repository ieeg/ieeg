/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import edu.upenn.cis.db.mefview.shared.SearchResult;

public class SearchResultTreeModel implements TreeViewModel {

	private static class StudyCell extends AbstractCell<SearchResult> {
		@Override
		public void render(Context context, SearchResult value,
				SafeHtmlBuilder sb) {
			if (value != null && value.getFriendlyName() != null) {
				sb.appendHtmlConstant("<table>");
				sb.appendHtmlConstant("<tr>");
				sb.appendHtmlConstant("<td width=125px>");
				sb.appendEscaped(value.getFriendlyName());
				sb.appendHtmlConstant("</td>");
				sb.appendHtmlConstant("<td width=200px>");
				sb.appendEscaped(value.getOrganization() == null ? "" : value
						.getOrganization());
				sb.appendHtmlConstant("</td>");
				sb.appendHtmlConstant("<td width=75px>");
				sb.append(value.getTimeSeriesLabels().size());
				sb.appendHtmlConstant(" chs</td>");
				sb.appendHtmlConstant("<td width=75px>");
				sb.append(value.getAnnotationCount());
				sb.appendHtmlConstant(" anns</td>");
				sb.appendHtmlConstant("<td width=75px>");

				long startUutc = value.getStartUutc() == null ? 0 : value
						.getStartUutc();
				long endUutc = value.getEndUutc() == null ? 0 : value
						.getEndUutc();

				double secs = (endUutc - startUutc) / 1000000.0;
				BigDecimal secsBd =
						new BigDecimal(secs).setScale(
								0,
								RoundingMode.HALF_UP);
				final int secsRounded = secsBd.intValue();
				int secsToDisplay = secsRounded % 60;

				final double mins = secsRounded / 60;
				final int minsTruncated = (int) mins;
				int minsToDisplay = minsTruncated % 60;

				final double hours = mins / 60;
				final int hoursTruncated = (int) hours;
				int hoursToDisplay = hoursTruncated % 24;

				final double days = hours / 24;
				final int daysTruncated = (int) days;
				int daysToDisplay = daysTruncated;

				if (daysToDisplay > 0) {
					if (secsToDisplay >= 30) {
						minsToDisplay++;
					}
					if (minsToDisplay >= 30) {
						hoursToDisplay++;
					}
					if (hoursToDisplay == 24) {
						hoursToDisplay = 0;
						daysToDisplay++;
					}
					sb.append(daysToDisplay);
					sb.appendHtmlConstant("d ");

					sb.append(hoursToDisplay);
					sb.appendHtmlConstant("h");
				} else if (hoursToDisplay > 0) {
					
					if (secsToDisplay >= 30) {
						minsToDisplay++;
					}
					if (minsToDisplay == 60) {
						minsToDisplay = 0;
						hoursToDisplay++;
					}
					
					sb.append(hoursToDisplay);
					sb.appendHtmlConstant("h ");

					sb.append(minsToDisplay);
					sb.appendHtmlConstant("m");
				} else {
					sb.append(minsToDisplay);
					sb.appendHtmlConstant("m ");

					sb.append(secsToDisplay);
					sb.appendHtmlConstant("s");
				}
				sb.appendHtmlConstant("</td>");
				sb.appendHtmlConstant("</tr></table></div>");
			}
		}
	}

	private static class AnalysisCell extends AbstractCell<SearchResult> {

		@Override
		public void render(Context context, SearchResult value,
				SafeHtmlBuilder sb) {
			if (value != null) {
				// sb.appendHtmlConstant(imageHtml).appendEscaped(" ");
				sb.appendEscaped(value.getFriendlyName() + " (" +
						value.getAnnotationCount() + " annotations)");
			}
		}
	}

	// Set<BasicMetadata> favorites;
	// public interface StarToggleHandler<T> {
	// public void onToggle(T object, boolean isSet);
	// }
	//
	// StarToggleHandler<BasicMetadata> handler = null;
	//
	// private class ImageMapper implements HasCell<BasicMetadata,
	// ImageResource> {
	// ClickImageResourceCell cell;
	// ImageResource sel;
	// ImageResource unsel;
	//
	// public ImageMapper(ImageResource selected, ImageResource unselected) {
	// cell = new ClickImageResourceCell();
	// sel = selected;
	// unsel = unselected;
	// }
	//
	// @Override
	// public Cell<ImageResource> getCell() {
	// return cell;
	// }
	//
	// @Override
	// public FieldUpdater<BasicMetadata, ImageResource> getFieldUpdater() {
	// return null;
	// }
	//
	// @Override
	// public ImageResource getValue(BasicMetadata object) {
	// if (favorites.contains(object)) {
	// return sel;
	// } else
	// return unsel;
	// }
	//
	// }
	//
	// private static class CellMapper implements HasCell<BasicMetadata,
	// BasicMetadata> {
	// ActionCell<BasicMetadata> cell;
	//
	// public CellMapper(String field, Delegate<BasicMetadata> delegate) {
	// cell = new ActionCell<BasicMetadata>(field, delegate);
	// }
	//
	// @Override
	// public Cell<BasicMetadata> getCell() {
	// return cell;
	// }
	//
	// @Override
	// public FieldUpdater<BasicMetadata, BasicMetadata> getFieldUpdater() {
	// return null;
	// }
	//
	// @Override
	// public BasicMetadata getValue(BasicMetadata object) {
	// return object;
	// }
	//
	// }
	//
	// {
	// List<HasCell<BasicMetadata, ?>> cells = new
	// LinkedList<HasCell<BasicMetadata, ?>>());
	//
	// cells.add(CellMapper("first", new SearchResult()));
	// }
	//
	// private static class TestCell extends CompositeCell<BasicMetadata> {
	// List<HasCell<BasicMetadata, ?>> cells;
	//
	// public TestCell(List<HasCell<BasicMetadata, ?>> hasCells) {
	// super(hasCells);
	// cells = hasCells;
	// }
	//
	// @Override
	// public void render(Context context, BasicMetadata value, SafeHtmlBuilder
	// sb) {
	// sb.appendHtmlConstant("<table><tbody><tr>");
	// for (HasCell<BasicMetadata, ?> hasCell : cells) {
	// render(context, value, sb, hasCell);
	// }
	// sb.appendHtmlConstant("</tr></tbody></table>");
	// }
	//
	// @Override
	// protected <X> void render(Context context, BasicMetadata value,
	// SafeHtmlBuilder sb, HasCell<BasicMetadata, X> hasCell) {
	// Cell<X> cell = hasCell.getCell();
	// sb.appendHtmlConstant("<td>");
	// cell.render(context, hasCell.getValue(value), sb);
	// sb.appendHtmlConstant("</td>");
	// }
	//
	// @Override
	// protected Element getContainerElement(Element parent) {
	// return
	// parent.getFirstChildElement().getFirstChildElement().getFirstChildElement();
	// }
	//
	// }

	/**
	 * This selection model is shared across all leaf nodes. A selection model
	 * can also be shared across all nodes in the tree, or each set of child
	 * nodes can have its own instance. This gives you flexibility to determine
	 * how nodes are selected.
	 */
	private final SelectionModel<SearchResult> selectionModel;

	private ListDataProvider<SearchResult> results;

	private Map<SearchResult, ListDataProvider<SearchResult>> subresults = new HashMap<SearchResult, ListDataProvider<SearchResult>>();

	public SearchResultTreeModel(ListDataProvider<SearchResult> results) {
		this.results = results;
		selectionModel = new SingleSelectionModel<SearchResult>();
	}

	public SearchResultTreeModel(ListDataProvider<SearchResult> results,
			SelectionModel<SearchResult> selectionModel) {
		this.results = results;
		this.selectionModel = selectionModel;
	}

	public void setStudyContents(SearchResult parent,
			ListDataProvider<SearchResult> subresult) {
		GWT.log("Updating contents of " + parent.getFriendlyName() + " with "
				+ subresult.getList().size() + " sub-studies");
		subresults.put(parent, subresult);
		results.refresh();
		subresult.refresh();

		// Can we force a refresh?
		int i = results.getList().indexOf(parent);
		if (i >= 0) {
			List<SearchResult> lst = new ArrayList<SearchResult>();
			lst.add(parent);
			for (HasData<SearchResult> disp : results.getDataDisplays()) {
				disp.setRowData(i, lst);
			}
		}
	}

	public List<SearchResult> getAllSubresults() {
		List<SearchResult> ret = new ArrayList<SearchResult>();
		for (ListDataProvider<SearchResult> ldp : subresults.values())
			ret.addAll(ldp.getList());

		return ret;
	}

	public boolean haveSearchResultsFor(SearchResult sr) {
		return subresults.containsKey(sr);
	}

	// private final SingleSelectionModel<SnapshotMetadata> selectionModel = new
	// SingleSelectionModel<SnapshotMetadata>();

	@Override
	public <T> NodeInfo<?> getNodeInfo(T value) {
		if (value == null) {
			// LEVEL 0.
			// We passed null as the root value. Return the string.

			ListDataProvider<String> dataProvider = new ListDataProvider<String>();

			dataProvider.getList().add("Dataset Explorer");

			return new DefaultNodeInfo<String>(dataProvider, new TextCell());
		} else if (value instanceof String) {

			for (SearchResult sr : results.getList()) {
				if (sr == null || sr.getFriendlyName() == null
						|| sr.getFriendlyName().isEmpty()) {
					results.getList().remove(sr);
					break;
				}
			}

			// Return a node info that pairs the data provider and the cell.
			return new DefaultNodeInfo<SearchResult>(results,
					new StudyCell(), selectionModel, null);

		} else if (value instanceof SearchResult) {
			// LEVEL 1.
			// We want the children of the SnapshotMetadata.
			ListDataProvider<SearchResult> dataProvider = subresults.get(value);

			if (dataProvider == null)
				dataProvider = new ListDataProvider<SearchResult>();

			// Use the shared selection model.
			return new DefaultNodeInfo<SearchResult>(dataProvider,
					new AnalysisCell(),
					selectionModel, null);
		}

		return null;
	}

	@Override
	public boolean isLeaf(Object value) {
		if (value == null || value instanceof String)
			return false;

		return ((SearchResult) value).getBaseRevId() != null
				&& !((SearchResult) value).getBaseRevId().isEmpty();
		// return (subresults.containsKey(value));
	}

	public ListDataProvider<SearchResult> getResults() {
		return results;
	}

}
