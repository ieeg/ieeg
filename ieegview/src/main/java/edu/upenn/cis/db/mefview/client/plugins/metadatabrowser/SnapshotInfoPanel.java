/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import com.google.gwt.resources.client.ImageResource;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.SnapshotInfoPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class SnapshotInfoPanel extends AstroTab {

	public static final String NAME = "snapshot-info";
	final static SnapshotInfoPanel seed = new SnapshotInfoPanel();
	
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected SnapshotInfoPanel() {
		super();
	}

	public SnapshotInfoPanel(ClientFactory factory, 
			DataSnapshotViews model, 
//			String user,
			final String title) {

		super(factory, model, title);
	}

	@Override
	public String getPanelType() {
		// TODO Auto-generated method stub
		return NAME;
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new SnapshotInfoPanel(clientFactory, model, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getData();
	}

	@Override
	public WindowPlace getPlace() {
		return new SnapshotInfoPlace(datasetId);
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return SnapshotInfoPlace.getFromSerialized(params);
//	}

	public String getTitle() {
		return "Details";
	}
	
	public int getMinHeight() {
      return minHeight;
    }
	
	public boolean isSearchable() {
		return true;
	}

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
		/*
		for (Widget w: Arrays.<Widget>asList()) {
			final Widget wHere = w;
			if (wHere != null)
			Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
				
				@Override
				public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
					NativeEvent clickEvent = Converter.getNativeEvent(event);
					DomEvent.fireNativeEvent(clickEvent, wHere);
				}
			});
		}*/
	}
}
