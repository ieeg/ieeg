package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer;

import java.util.List;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasAllKeyHandlers;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HasText;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.IEEGToolbar;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.TimeSeries;

public interface IEEGPresenter {
	  public static interface Display extends IEEGPane {
			public void log(int level, String string);
		    public void debug(String str);

		    public void setRefreshGraph();
		    public void setRefreshAnnotations();

		    public void updateHeight(boolean refresh);

		    public double getSpecifiedPageWidth();
		    public void initPosition(double start, double width);
		    public double getWindowWidth();
		    public void setPageWidth(double width);
		    public double getPageWidth();
		    public double getPosition();
		    public void removeStatusMessage();
		    public void showStatusMessage(String msg);
		    public void addNewData(JsArray<JsArrayInteger> data, boolean refresh, boolean isMinMax);
		    public void setRequestPeriod(double p);
		    public double getRequestPeriod();
		    public List<DisplayConfiguration> getFilters();
		    public void setFilters(List<DisplayConfiguration> filters);
		    public void clearFirstRequest();
		    public void setPositionDisplay(double value);
		    public double getVoltageText(int row);
		    public double getTimeText(int row);
		    public void setVoltageValue(double val);
		    public void setTimeValue(double val);
		    public HasAllKeyHandlers getKeyHandler();
		    boolean hasFocus();
		    public void incFocus();
		    public void setZoom(double left, double right, boolean redraw);
		    public IEEGViewerPanel getHostPanel();
		    public String getWindowTitle();

		    public double getNumberOfSamples();

		    public void updateGraphWithTraces(JsArray<JsArrayInteger> ar, boolean isMinMax);
		    public void scaleUp();
		    public void scaleDown();
		    public void restoreView();

//		    public void tracePopup();
		    public void setScale(double scale);
		    
		    public void updateMontage(EEGMontage montage);

		    public void resetFocus();
		    public void refreshAnnotationsOnGraph(boolean redraw);
		    public void showThese(boolean[] enabled);

		    public void invertSignal(boolean inv);
		    public boolean isInvertedSignal();

		    public void refreshAnnotations(List<AnnBlock> newAnnotations);

		    void initToolbar(IEEGToolbar toolbar, 
		        double maxFreq,
		        FocusHandler focusedHandler,
		        BlurHandler blurredHandler,
		        ChangeHandler voltsChangedHandler, 
		        NumericEnterHandler voltsEnteredHandler,
		        ChangeHandler timeChangedHandler, 
		        NumericEnterHandler timeEnteredHandler,
		        NumericEnterHandler posEnteredHandler,
		        ClickHandler refreshHandler,
		        ClickHandler invertHandler);
		    public void setScrollerHandlers(
		        ClickHandler annListHandler, ClickHandler prevAnnHandler,
		        ClickHandler leftHandler, ClickHandler rightHandler, 
		        ClickHandler nextAnnHandler, ValueChangeHandler<Double> sliderHandler);

		    public void setAnnotationDefinitions(List<AnnotationDefinition> ann);

		    //		public CheckBox getInvertedCheckBox();
//		    public MenuBar getFavorites();
//		    public MenuBar getRecents();

		    public DataSnapshotModel getDataSnapshotState();
		  }

	Display getDisplay();
	DataSnapshotModel getModel();
	Timer getRequestHighres();
	void setRequestHighres(Timer t);
	
	String getSnapshotId();
	
	void clearFirstRequest();

    public JsArray<JsArrayInteger> refreshBufferFromSeries(TimeSeries[] results);
    public JsArray<JsArrayInteger> refreshBufferFromJson(String json);

    public FullResolutionDataHandler getFullDataHandler();
	public void getData(final String user, final String dataSnapshotRevId, final List<String> traceIds,
			final double start, final double width, final double samplingPeriod, 
			final List<DisplayConfiguration> filter, boolean dontCache);
	void prefetchAsNeeded(long start, long end, int direction);
	void jumpTo(double i);
}
