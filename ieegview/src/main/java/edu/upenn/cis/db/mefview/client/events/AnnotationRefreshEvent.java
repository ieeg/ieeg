/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

public class AnnotationRefreshEvent  extends GwtEvent<AnnotationRefreshEvent.Handler> {
	private static final Type<AnnotationRefreshEvent.Handler> TYPE = new Type<AnnotationRefreshEvent.Handler>();
	
	private final String studyId;
	
	public AnnotationRefreshEvent(String study){
		studyId = study;
	}
	
	public String getStudy() {
		return studyId;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<AnnotationRefreshEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<AnnotationRefreshEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(AnnotationRefreshEvent.Handler handler) {
		handler.onAnnotationRefresh(studyId);
	}

	public static interface Handler extends EventHandler {
		void onAnnotationRefresh(String datasetId);
	}
}
