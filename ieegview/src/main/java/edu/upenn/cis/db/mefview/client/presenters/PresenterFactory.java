/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.presenters;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.google.common.collect.Maps;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;

public class PresenterFactory {
	
	public static class PresenterInfo {
		public Presenter presenterClass;
		public boolean isSnapshotSpecific;
		
		public PresenterInfo(Presenter myClass, boolean snapshotSpecific) {
			isSnapshotSpecific = snapshotSpecific;
			presenterClass = myClass;
		}
	}
	
	final static Map<String, PresenterInfo> presenterMap = Maps.newHashMap();
	
	public static void register(String key, PresenterInfo info) throws PresenterExistsException {
		if (presenterMap.get(key) != null)
			throw new PresenterExistsException("Presenter " + key + " was already registered in PresenterFactory!");
		
		GWT.log("Registering presenter for " + key);
		presenterMap.put(key, info);
	}
	
	public PresenterInfo getInfo(String key) {
		return presenterMap.get(key);
	}

	public static Presenter getPresenter(
			final String panel,
			final ClientFactory clientFactory,
			final AppModel controller,
			final DataSnapshotViews model, 
			final PresentableMetadata project,
			boolean writePermissions) {
		PresenterInfo myClass = presenterMap.get(panel);//.panelClass;

		if (myClass != null) {
			clientFactory.getSessionModel().getMainLogger().info("Creating " + panel);
			Presenter pres = myClass.presenterClass.create(clientFactory, controller, model, 
					project, writePermissions);
			
			return pres;
		} else {
			clientFactory.getSessionModel().getMainLogger().log(Level.SEVERE, "Error finding presenter " + panel);
			return null;
		}
	}


	public void addToolbarHandlers(List<HasClickHandlers> toolbarButtons) {
		
	}
}
