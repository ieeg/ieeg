package edu.upenn.cis.db.mefview.client;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.google.common.collect.Lists;
import com.google.gwt.core.client.GWT;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.client.ClientFactory.IAppSaveAndShutdown;
import edu.upenn.cis.db.mefview.client.controller.EventLogger;
import edu.upenn.cis.db.mefview.client.controller.IEventLogger;
import edu.upenn.cis.db.mefview.client.events.UserInfoReceivedEvent;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.workspacesettings.WorkspacePanel;
import edu.upenn.cis.db.mefview.client.presenters.AppPresenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.IHeaderBar;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.UserPrefs;

public class SessionModel {
	Logger myLogger = null;

	IHeaderBar contextBar = null;

	IAppSaveAndShutdown save = null;

	List<String> filterTypes = Lists.newArrayList();
	AstroPanel snapshots = null;
	AstroPanel tools = null;

	WorkspacePanel workspace = null;

	String userId = "";
	PanelFactory pf = new PanelFactory();

	PresenterFactory pf2 = new PresenterFactory();

	UserPrefs preferences = new UserPrefs();

	PeriodicEvents delayedEvents;

	SessionToken sessionID = null;

	UserInfo theUser = null;

	EventLogger logger;

	ServerConfiguration configuration;
	
	Map<String,String> remoteClients = new HashMap<String,String>();
	
	ClientFactory factory;
	
	public SessionModel(ClientFactory theFactory) {
		myLogger = Logger.getLogger("ieeg");
		factory = theFactory;
	}

//	@Override
	public List<String> getFilterTypes() {
		return filterTypes;
	}
	
//	@Override
	public IHeaderBar getHeaderBar() {
		return contextBar;
	}

//	@Override
	public Logger getMainLogger() {
		return myLogger;
	}

	//	@Override
	//	public AstroPanel getSnapshotsWindow() {
	//		return snapshots;
	//	}
	//
	//	@Override
	//	public AstroPanel getToolsWindow() {
	//		return tools;
	//	}

	//	@Override
	//	public WorkspacePanel getWorkspaceWindow() {
	//		return workspace;
	//	}

	//	@Override
	//	public AvailableSnapshotsModel getSnapshotsModel() {
	//		return availableSnapshots;
	//	}

//	@Override
	public PeriodicEvents getPeriodicEvents() {
		return delayedEvents;
	}

//	@Override
	public ServerConfiguration getServerConfiguration() {
		return configuration;
	}

//	@Override
	public SessionToken getSessionID() {
		return sessionID;
	}

//	@Override
	public String getUserId() {
		return userId;
	}

//	@Override
	public UserInfo getUserInfo() {
		return theUser;
	}

//	@Override
	public String getUserPhoto() {		
		return ((getUserInfo() == null || getUserInfo().getPhoto() == null ||
				getUserInfo().getPhoto().isEmpty()) ?
						ResourceFactory.getUnknownUser().getSafeUri().asString() :
							(getUserInfo().getPhoto().startsWith("http://") ?
									AppPresenter.URL + getUserInfo().getPhoto()
									: getUserInfo().getPhoto()));
	}

//	@Override
	public UserPrefs getUserPreferences() {
		return preferences;
	}

//	@Override
	public boolean isRegisteredUser() {
		if (getUserInfo() == null)
			return false;
		else
			return !getUserInfo().isGuest();
	}

//	@Override
	public void setFilterTypes(List<String> fTypes) {
		filterTypes.clear();
		filterTypes.addAll(fTypes);
	}
	
//	@Override
	public void setHeaderBar(IHeaderBar bar) {
		contextBar = bar;
	}

	public void setPeriodicEvents(PeriodicEvents delayedEvents) {
		this.delayedEvents = delayedEvents;
	}


//	@Override
	public IAppSaveAndShutdown getSaveAndShutdownController() {
		return save;
	}

	public void setSaveAndShutdownController(IAppSaveAndShutdown x) {
		this.save = x;
	}

//	@Override
	public void setServerConfiguration(ServerConfiguration config) {
		configuration = config;
	}

//	@Override
	public void setSessionID(SessionToken id) {
		sessionID = id;
		if (factory.getSnapshotServices() != null)
			((ServerAccess)factory.getSnapshotServices()).setSessionToken(id);
		logger = GWT.create(IEventLogger.class);//new EventLogger(factory);
		logger.initialize(factory);
	}

//	@Override
	public void setUserId(String id) {
		userId = id;
	}

//	@Override
	public void setUserInfo(UserInfo user) {
		theUser = user;
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new UserInfoReceivedEvent(user));
	}

//	@Override
	public void setUserPreferences(UserPrefs prefs) {
		preferences = prefs;

	}

	public void addRemoteClient(Timestamp whenSeen, String clientName, String clientId) {
		remoteClients.put(clientName, clientId);
	}
	
	public Collection<String> getRemoteClients() {
		return remoteClients.keySet();
	}
}
