/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;


import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.util.RequestHtml;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class QuickLinksPane extends ScrollPanel 
implements QuickLinksPresenter.Display {
  Presenter presenter;
  ClientFactory clientFactory;
  
  List<Button> buttons = new ArrayList<Button>();
  
//  public static IUserProfilePluginFactory userProfile = GWT.create(IUserProfilePluginFactory.class);

  public QuickLinksPane() {
  }
  
  public void initialize(ClientFactory factory, IUserProfilePluginFactory userProfile) {

    this.clientFactory = factory; 
//    FlowPanel buttonHolder = new FlowPanel();
    
    List<ButtonEntry> entries = 
    		userProfile.getPlugin(
    	    		  clientFactory.getSessionModel().getServerConfiguration(), 
    	    		  clientFactory.getSessionModel(), 
    	    		  clientFactory.getSessionModel().getUserInfo()).getButtons();

    int width = (int)Math.sqrt(entries.size() + entries.size() - 1);
    int height = (entries.size() + width - 1) / width;
    
    VerticalPanel content = new VerticalPanel();
    Label l = new Label("Highlighted Links");
    l.setStylePrimaryName("heading");
    content.add(l);
  
    Grid grid = new Grid(height, width);
    grid.setCellPadding(2);
    grid.setCellSpacing(2);
    grid.setBorderWidth(0);
    
    double inches = 3.35 / width;
    double iconSize = (inches < 2) ? inches / 2 : 1.0;
    String urlBase = RequestHtml.getPath(
    		clientFactory.getSessionModel().getServerConfiguration().getServerName());

    for (int row = 0; row < height; row++) {
    	for (int col = 0; col < width && row * width + col < entries.size(); col++) {
    		final ButtonEntry be = entries.get(row * width + col);
    		Image img = new Image((be.getIcon().startsWith("http:") ||
    				be.getIcon().startsWith("https:") ? "" : urlBase) + be.getIcon());
    		img.setSize(iconSize + "in", iconSize + "in");
    		img.setAltText(be.getTooltip());
    		Button but = new Button();
    		but.setHTML(img.toString() + "<br/>"
    				+ new Label(be.getTitle()));
    		but.setSize(inches + "in", inches + "in");
//    	    but.setStylePrimaryName("buttonHolder");
//    	    but.addStyleName("quickLinkButtons");
    	    but.addStyleName("btnLink");

    	    if (be.isInNewWindow()) {
    	    	but.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
		    	        Window.open(be.getUrl(), 
		    	        		be.getTitle(),
		    	        		null);
					}
    	    		
    	    	});
    		} else {
    			but.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
		    	        OpenDisplayPanel action = new OpenDisplayPanel(
		    	                new WebLink(
		    	                    "",
		    	                    be.getTitle(),
		    	                    be.getUrl()),
		    	                    false,
		    	                    WebPanel.NAME,
		    	                    null);
		    	            IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(action));
					} 
    				
    			});
    		}
    	    buttons.add(but);

    		grid.setWidget(row, col, but);
    	}
    }
    content.add(grid);
    add(content);
  }

  @Override
  public void addPresenter(Presenter pres) {
    this.presenter = pres;
  }

  @Override
  public Presenter getPresenter() {
    return presenter;
  }


	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	public void bindDomEvents() {
		
	}
}
