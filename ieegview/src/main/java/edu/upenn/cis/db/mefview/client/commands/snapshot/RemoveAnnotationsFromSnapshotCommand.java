package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.DialogBox;

import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.Annotation;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class RemoveAnnotationsFromSnapshotCommand implements Command {
	final String dataset;
	final Set<Annotation> annotationsToRemove;
	final OnDoneHandler handler;
	final DialogBox waitDialog; 
	final ClientFactory clientFactory;
	
	public RemoveAnnotationsFromSnapshotCommand(
			final ClientFactory clientFactory,
			final String dataset,
			final Collection<Annotation> annotationsToRemove,
			final DialogBox waitDialog, 
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.handler = handler;
		this.waitDialog = waitDialog;
		this.clientFactory = clientFactory;
		this.annotationsToRemove = new HashSet<Annotation>();
		this.annotationsToRemove.addAll(annotationsToRemove);
	}

	@Override
	public void execute() {
		clientFactory.getSnapshotServices().removeAnnotations(
				clientFactory.getSessionModel().getSessionID(), 
				dataset, annotationsToRemove, 
				new SecFailureAsyncCallback<String>(clientFactory) {

			public void onFailureCleanup(Throwable caught) {
				if (waitDialog != null)
					waitDialog.hide();
			}

			public void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Unable to Remove Annotations", "Sorry, there was an error removing the annotations.");
			}

			public void onSuccess(String result) {
				if (waitDialog != null)
					waitDialog.hide();
				//				ControlPane.messageBox("Success!", "We successfully removed the annotations.");
				if (handler != null)
					handler.doWork(result);
			}

		});
	}

}
