/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.clickhandlers;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.BookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.FavoriteMetadataEvent;
import edu.upenn.cis.db.mefview.client.events.UnbookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.UnfavoriteMetadataEvent;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class FavoriteToggleHandler implements PagedTable.StarToggleHandler<PresentableMetadata> {
	public static FavoriteToggleHandler theHandler = null;
	
	public static synchronized FavoriteToggleHandler getHandler() {
		if (theHandler == null)
			theHandler = new FavoriteToggleHandler();
		
		return theHandler;
	}
	
	protected FavoriteToggleHandler() {
		
	}

	@Override
	public void onToggle(PresentableMetadata res, boolean isSet) {
		if (isSet)
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new FavoriteMetadataEvent(res));
		else
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new UnfavoriteMetadataEvent(res));
		
		if (res instanceof SearchResult) {
			if (isSet)
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new BookmarkSnapshotEvent((SearchResult)res));
			else
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new UnbookmarkSnapshotEvent((SearchResult)res));
		}
	}

}
