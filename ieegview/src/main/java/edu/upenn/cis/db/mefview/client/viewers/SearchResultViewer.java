/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class SearchResultViewer extends TableViewer<SearchResult> {
	boolean favorites;
	boolean selectable;
	boolean subset;
	boolean readOnly;
	
	public SearchResultViewer(
			Collection<SearchResult> users, int pageSize, boolean favorites, 
			boolean selectable, boolean subset) {
		super(users, pageSize);

		this.selectable = selectable;
		this.favorites = favorites;
		this.subset = subset;
		this.readOnly = false;
		initialize();
	}
	
	public SearchResultViewer(
			int pageSize, boolean favorites, boolean selectable, boolean subset) {
		super(pageSize);

		this.selectable = selectable;
		this.favorites = favorites;
		this.subset = subset;
		this.readOnly = false;
		
		initialize();
	}
	
	public SearchResultViewer(
	    int pageSize, boolean favorites, boolean selectable, boolean subset, boolean readOnly) {
	  super(pageSize);

	  this.selectable = selectable;
	  this.favorites = favorites;
	  this.subset = subset;
	  this.readOnly = readOnly;

	  initialize();
	}
	
	@Override
	protected void init() {
		clear();

//		Label l = new Label("Data snapshots:"); 
//		add(l);
//		setWidgetLeftRight(l, 5, Unit.PX, 5, Unit.PX);
//		setWidgetTopHeight(l, 5, Unit.PX, 35, Unit.PX);
		
		List<String> studyColNames = new ArrayList<String>();
		List<TextColumn<SearchResult>> studyColumns = new ArrayList<TextColumn<SearchResult>>();
		
		studyColNames.add("Name");
		studyColumns.add(SearchResult.studyColumn);
		studyColNames.add("Channels");
		studyColumns.add(SearchResult.studyChannels);
		if (!subset) {
			studyColNames.add("Images");
			studyColumns.add(SearchResult.studyImages);
			studyColNames.add("Annotations");
			studyColumns.add(SearchResult.studyAnnotations);
		}
//		studyColNames.add("Analyzed Snapshot");
//		studyColumns.add(SearchResult.baseColumn);

		if (favorites) {
		  GWT.log("Adding favorites");
		  table = new PagedTable<SearchResult>(pageSize, false, false,true, 
		      new ArrayList<SearchResult>(), SearchResult.KEY_PROVIDER, studyColNames, 
		      studyColumns, true, ResourceFactory.getStarSel(), ResourceFactory.getStarUnsel());
		} else if (selectable) {
		  GWT.log("Adding multi-selectable");
		  table = new PagedTable<SearchResult>(pageSize, true, true, true, 
		      new ArrayList<SearchResult>(), SearchResult.KEY_PROVIDER, studyColNames, 
		      studyColumns, false, null, null);
		} else if (readOnly){
		  GWT.log("Adding ReadOnly searchResultViewer");
		  table = new PagedTable<SearchResult>(pageSize, false, false, false, 
		      new ArrayList<SearchResult>(), SearchResult.KEY_PROVIDER, studyColNames, 
		      studyColumns, false, null, null);

		} else {
		  GWT.log("Adding no checkbox but selectable");
		  table = new PagedTable<SearchResult>(pageSize, false, false,false, 
		    new ArrayList<SearchResult>(), SearchResult.KEY_PROVIDER, studyColNames, 
		    studyColumns, false, null, null);
	
	    }
		
		SearchResult.studyColumn.setSortable(true);
//		SearchResult.baseColumn.setSortable(true);
		
	    table.addColumnSort(SearchResult.studyColumn,
		        new Comparator<SearchResult>() {
	          public int compare(SearchResult o1, SearchResult o2) {
	            if (o1 == o2) {
	              return 0;
	            }

	            // Compare the name columns.
	            if (o1 != null) {
	              return (o2 != null) ? o1.getFriendlyName().compareTo(o2.getFriendlyName()) : 1;
	            }
	            return -1;
	          }
	        });
	    
		if (!subset) {
			table.setColumnWidth(SearchResult.studyChannels, "80px");
			table.setColumnWidth(SearchResult.studyImages, "70px");
			table.setColumnWidth(SearchResult.studyAnnotations, "90px");
		}

		add(table);
		setWidgetLeftRight(table, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(table, 2, Unit.PX, 2, Unit.PX);
	}

	public void refresh() {
		table.getDataProvider().refresh();
		table.redraw();
	}
}
