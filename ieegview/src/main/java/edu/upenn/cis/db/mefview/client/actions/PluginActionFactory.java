/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class PluginActionFactory {
	public static final Map<String, IPluginAction> actions = new HashMap<String, IPluginAction>();
	
	public static void register(String name, IPluginAction act) {
		if (actions.containsKey(name))
			throw new RuntimeException("PluginActionFactory registry: key already exists!");
		else
			actions.put(name, act);
	}
	
	public static IPluginAction create(String name, ClientFactory cf, Set<PresentableMetadata> md, 
			Presenter pres) {
		if (actions.containsKey(name)) {
			return actions.get(name).create(md, cf, pres);
		} else
			throw new RuntimeException("Unknown action " + name);
	}
}
