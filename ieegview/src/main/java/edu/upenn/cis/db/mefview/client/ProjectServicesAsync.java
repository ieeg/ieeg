/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface ProjectServicesAsync {

	void createProject(SessionToken sessionID, Project project,
			AsyncCallback<Project> callback);

	void getProject(SessionToken sessionID, String projectId,
			AsyncCallback<Project> callback);

	void getProjectAdmins(SessionToken sessionID, String projectId,
			AsyncCallback<Set<UserInfo>> callback);

	void getProjectDiscussions(SessionToken sessionID, String projectId,
			int postIndex, int numPosts, AsyncCallback<List<Post>> callback);

	void getProjectGroups(SessionToken sessionID,
			Set<ProjectGroup> excludedGroups,
			AsyncCallback<List<ProjectGroup>> callback);

	void getProjectTags(SessionToken sessionID, String projectId,
			AsyncCallback<Set<String>> callback);

	void getProjectTools(SessionToken sessionID, String projectId,
			AsyncCallback<Set<ToolDto>> callback);

	void getProjectUsers(SessionToken sessionID, String projectId,
			AsyncCallback<Set<UserInfo>> callback);

	void getLazyProjectsForUser(SessionToken sessionID,
			AsyncCallback<Set<Project>> callback);

	void updateProjectAdministrators(SessionToken sessionID, String projectId,
			Set<String> userids, AsyncCallback<String> callback);

	void updateProjectSnapshots(SessionToken sessionID, String projectId,
			Set<String> snapshotIds, AsyncCallback<String> callback);

	void updateProjectTags(SessionToken sessionID, String projectId,
			Set<String> tags, AsyncCallback<String> callback);

	void updateProjectTeam(SessionToken sessionID, String projectId,
			Set<String> userid, AsyncCallback<String> callback);

	void updateProjectTools(SessionToken sessionID, String projectId,
			Set<String> toolIds, AsyncCallback<String> callback);

	void addDiscussionToProject(SessionToken sessionID, String projectId,
			Post post, AsyncCallback<String> callback);

	void getProjectSearchResultsForUser(SessionToken sessionID,
			String projectId, AsyncCallback<Set<SearchResult>> callback);

}
