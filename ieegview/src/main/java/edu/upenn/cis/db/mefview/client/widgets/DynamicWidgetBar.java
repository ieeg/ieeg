/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.widgets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

import edu.upenn.cis.db.mefview.Pluggable;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * A widget bar that responds to changes in selected metadata and adjusts the bar accordingly
 * 
 * @author zives
 *
 */
public class DynamicWidgetBar<D extends PresentableMetadata, P extends Pluggable> extends FlowPanel 
	implements SelectionChangeEvent.Handler, Attachable {
	Presenter activePresenter = null;

	/**
	 * Simple adapter class
	 * @author zives
	 *
	 * @param <D> PresentableMetadata type
	 * @param <P> Pluggable type
	 */
	public interface ActionMapper<D,P> {
		/**
		 * Returns a list of candidate actions for the selected item set
		 * @param selected
		 * @return
		 */
		public List<String> getActions(Collection<D> selected);
		
		/**
		 * Creates an actual parameterized action object for the selected items
		 * and the presenter
		 * 
		 * @param app
		 * @param selected
		 * @param activePresenter
		 * @return
		 */
		public P createAction(String app, Set<D> selected, Presenter activePresenter);
		
		/**
		 * Looks up string name of the object
		 * @param action
		 * @return
		 */
		public String getName(P action);
		
		/**
		 * Creates a widget corresponding to the action
		 * 
		 * @param action
		 * @return
		 */
		public HasClickHandlers getWidget(P action);
		
		/**
		 * Determines if the object is relevant
		 * @param action
		 * @return
		 */
		public boolean isApplicable(P action);
		
		/**
		 * Invokes the action
		 * @param action
		 */
		public void execute(P action);
	}

	
	SelectionModel<? extends D> theSelectionModel;
	
	List<HasClickHandlers> buttons = new ArrayList<HasClickHandlers>();
	List<HasClickHandlers> globalButtons = new ArrayList<HasClickHandlers>();
	List<P> actions = new ArrayList<P>();
	List<P> globalActions  = new ArrayList<P>();
	
	ActionMapper<D,P> mapper;
	
	public DynamicWidgetBar(SelectionModel<? extends D> model,
			List<? extends P> globalActions, ActionMapper<D,P> mapper) {
		
		this.mapper = mapper;
		this.globalActions.addAll(globalActions);
		this.theSelectionModel = model;
		actions.addAll(globalActions);
		
		updateButtons(globalButtons, globalActions);
		for (HasClickHandlers b: globalButtons)
			add((Widget)b);
	}
	
	public void addGlobalAction(P action) {
		globalActions.add(action);
		updateGlobalButtons();
	}
	
	public void updateGlobalButtons() {
		for (HasClickHandlers b: globalButtons)
			remove((Widget)b);
		globalButtons.clear();
		updateButtons(globalButtons, globalActions);
		for (HasClickHandlers b: globalButtons)
			add((Widget)b);
	}

	public void updateLocalButtons() {
		updateLocalButtons(actions);
	}
	
	public void updateLocalButtons(List<? extends P> applicable) {
		for (HasClickHandlers b: buttons)
			remove((Widget)b);
		buttons.clear();
		
		updateButtons(buttons, applicable);
		
		for (int i = 0; i < buttons.size(); i++)
			insert((Widget)buttons.get(i), i);
	}
	
	private void updateButtons(List<HasClickHandlers> list, List<? extends P> actions) {
		for (P plug: actions) {
			HasClickHandlers newButton = mapper.getWidget(plug);
			list.add(newButton);
			createClickHandler(newButton, plug);
			GWT.log("Adding button " + mapper.getName(plug));
		}
	}
	
	public Set<D> getSelected() {
		Set<D> md = new HashSet<D>();
		if (theSelectionModel instanceof SingleSelectionModel) {
			GWT.log("DynamicToolbar - single selection model - " + 
					(PresentableMetadata)((SingleSelectionModel)theSelectionModel).getSelectedObject());
			md.add((D)((SingleSelectionModel)theSelectionModel).getSelectedObject());
		} else if (theSelectionModel instanceof MultiSelectionModel) {
			GWT.log("DynamicToolbar - multi selection model");
			for (Object o : ((MultiSelectionModel)theSelectionModel).getSelectedSet()) {
				md.add((D)o);						
			}
		}
		else
			throw new RuntimeException("Unknown selection model!");
		
		// Expand composite metadata into its members
		for (D pm : md) {
			if (pm != null) {
				for (PresentableMetadata c: pm.getChildren())
					if (!c.getMetadataCategory().equals(BASIC_CATEGORIES.Collections.name()) &&
							!c.getMetadataCategory().equals(BASIC_CATEGORIES.Snapshot.name()))
						md.add((D)c);
			}
		}
		return md;
	}
	
	public void createClickHandler(HasClickHandlers widget, final P action) {
		widget.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (mapper.isApplicable(action))
					mapper.execute(action);
			}
			
		});
		
		final Widget wHere = (Widget)widget;
//		Converter.getHTMLFromElement(wHere.getElement()).addEventListener("click", new EventListener() {
//			
//			@Override
//			public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
//				NativeEvent clickEvent = Converter.getNativeEvent(event);
//				DomEvent.fireNativeEvent(clickEvent, wHere);
//			}
//		});
	}
	
	@Override
	public void onSelectionChange(SelectionChangeEvent event) {
		Set<D> selected = getSelected();
		List<String> applicable = mapper.getActions(selected);
		
		GWT.log("DynamicWidgetBar updating toolbar for " + selected);
		
		actions.clear();
		for (String app: applicable) {
			P act = mapper.createAction(app, selected, activePresenter);
			if (act != null)
				actions.add(act);
		}

		updateLocalButtons(actions);
	}

	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
