/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.search;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Schema implements IsSerializable {
	public abstract static class Type implements IsSerializable {
		public abstract String getViewer();
		public abstract String getFormatted(IsSerializable value);
		public abstract byte[] getBlob();
		
		public abstract boolean isKey();
	}
	
	List<Type> attribList;
	List<String> attribNames;
	
	
	public List<Type> getAttribList() {
		return attribList;
	}
	public void setAttribList(List<Type> attribList) {
		this.attribList = attribList;
	}
	public List<String> getAttribNames() {
		return attribNames;
	}
	public void setAttribNames(List<String> attribNames) {
		this.attribNames = attribNames;
	}

	public String getKey(List<IsSerializable> values) {
		StringBuffer ret = new StringBuffer();
		
		for (int i = 0; i < attribList.size(); i++)
			if (attribList.get(i).isKey())
				ret.append(values.get(i));
		
		return ret.toString();
	}
}
