/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;

public class RefreshList implements IsSerializable {
	List<Post> newPosts = Lists.newArrayList();
	
	List<Project> updatedProjects = Lists.newArrayList();
	
	List<LogEntry> newEvents = Lists.newArrayList();
	
	List<ProvenanceLogEntry> newProvenance = Lists.newArrayList();

	public List<Post> getNewPosts() {
		return newPosts;
	}

	public void setNewPosts(List<Post> newPosts) {
		this.newPosts = newPosts;
	}

	public List<Project> getUpdatedProjects() {
		return updatedProjects;
	}

	public void setUpdatedProjects(List<Project> updatedProjects) {
		this.updatedProjects = updatedProjects;
	}

	public List<LogEntry> getNewEvents() {
		return newEvents;
	}

	public void setNewEvents(List<LogEntry> newEvents) {
		this.newEvents = newEvents;
	}

	public List<ProvenanceLogEntry> getNewProvenance() {
		return newProvenance;
	}

	public void setNewProvenance(List<ProvenanceLogEntry> newProvenance) {
		this.newProvenance = newProvenance;
	}
	
	
}
