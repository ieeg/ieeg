/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.util;

import com.google.gwt.ajaxloader.client.ArrayHelper;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.i18n.client.NumberFormat;

public class DataConversion {

	/**
	 * Take an hsv value and return an rgb format string
	 * 
	 * @param hue
	 * @param saturation
	 * @param value
	 * @return
	 */
	public static String hsvToRgb(double hue, double saturation, double value) {
		double red = 0;
		double green = 0;
		double blue = 0;
		if (saturation == 0) {
			red = value;
			green = value;
			blue = value;
		} else {
			int i = (int)Math.floor(hue * 6);
			double f = (hue * 6) - i;
			double p = value * (1 - saturation);
			double q = value * (1 - (saturation * f));
			double t = value * (1 - (saturation * (1 - f)));
		    switch (i) {
		      case 1: red = q; green = value; blue = p; break;
		      case 2: red = p; green = value; blue = t; break;
		      case 3: red = p; green = q; blue = value; break;
		      case 4: red = t; green = p; blue = value; break;
		      case 5: red = value; green = p; blue = q; break;
		      default:
//		      case 6: // fall through
//		      case 0: 
		    	  red = value; green = t; blue = p; break;
		    }
		}
	  red = Math.floor(255 * red + 0.5);
	  green = Math.floor(255 * green + 0.5);
	  blue = Math.floor(255 * blue + 0.5);
	  return "rgb(" + (int)red + ',' + (int)green + ',' + (int)blue + ')';
	}
	
	 /**
	   * Wraps a Java int Array to a JsArrayInteger for dev mode.
	   * 
	   * An excerpt from http://www.java2s.com/Code/Java/GWT/JsArrayUtil.htm
	   * 
	   * @param srcArray the array to wrap
	   * @return the wrapped array
	   */
	  public static JsArrayInteger wrapArray(int[] srcArray) {
		  return ArrayHelper.toJsArrayInteger(srcArray);
//	    JsArrayInteger result = JavaScriptObject.createArray().cast();
//	    for (int i = 0; i < srcArray.length; i++) {
//	      result.set(i, srcArray[i]);
//	    }
//	    return result;
	  }
	  
	  public static JsArray<JsArrayInteger> wrapArrayOfArrays(JsArrayInteger[] srcArray) {
		    JsArray<JsArrayInteger> result = JavaScriptObject.createArray().cast();
		    for (int i = 0; i < srcArray.length; i++) {
		      result.set(i, srcArray[i]);
		    }
		    return result;
		  }

	  public static native JsArrayInteger createTimeVector(double start, double period, double length) /*-{
	  	var ret = [];
	  	var time = start;
	  	
		  for (var i = 0; i < length; i++) {
		  	ret.push(time);
		  	time += period;
		  }
		  return ret;
	  }-*/;

		public static String getRoundedValue(double d) {
			if (d == 0)
				return "0";
//			if (d > 10)
//				return NumberFormat.getFormat("#####0").format(d);
//			else
				return NumberFormat.getFormat("0.00").format(d);
		}

		public static final NumberFormat fmt = NumberFormat.getFormat("##00000000");
		public static String getUUValue(double d) {
			return fmt.format(d);
		}
}
