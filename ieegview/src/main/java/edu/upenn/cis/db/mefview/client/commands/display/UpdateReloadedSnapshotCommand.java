package edu.upenn.cis.db.mefview.client.commands.display;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class UpdateReloadedSnapshotCommand implements Command {
	final ClientFactory clientFactory;
	final String oldId;
	final String newId;
	final Map<String,DataSnapshotViews> dataSnapshots;
	final List<AstroPanel> panels;
	
	public UpdateReloadedSnapshotCommand(
			ClientFactory cf, 
			String oldId, 
			String newId,
			Map<String,DataSnapshotViews> dataSnapshotsMap,
			List<AstroPanel> panels) {
		clientFactory = cf;
		this.oldId = oldId;
		this.newId = newId;
		this.dataSnapshots = dataSnapshotsMap;
		this.panels = panels;
	}
	
	@Override
	public void execute() {
		List<String> existing = Lists.newArrayList();
		List<WindowPlace> places = Lists.newArrayList();
		for (AstroPanel p : panels) {
			if (p.getDataSnapshotState() != null
					&& p.getDataSnapshotState().getSnapshotID().equals(oldId)) {
				existing.add(p.getPanelType());
				places.add(p.getPlace());
			}
		}
		SearchResult sr = dataSnapshots.get(oldId).getSearchResult();
		if (dataSnapshots.containsKey(oldId)) {
			// existing = cs.getDataSnapshots().get(study).getPanelTypes();
			dataSnapshots.get(oldId).removeThisStudy();
			dataSnapshots.remove(oldId);
		}
		sr.setDatasetRevId(newId);
		// loadStudy(study, existing);
		for (int i = 0; i < places.size(); i++) {
			IeegEventBusFactory.getGlobalEventBus().fireEvent(
					new OpenDisplayPanelEvent(new OpenDisplayPanel(
							sr, true, existing
									.get(i), places.get(i))));
		}
	}
}
