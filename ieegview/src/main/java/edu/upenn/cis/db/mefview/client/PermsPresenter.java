/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.collect.BiMap;
import com.google.gwt.user.client.Window;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.NewProjectAce;
import edu.upenn.cis.braintrust.shared.NewUserAce;
import edu.upenn.cis.braintrust.shared.RemoveProjectAce;
import edu.upenn.cis.braintrust.shared.RemoveUserAce;
import edu.upenn.cis.braintrust.shared.UpdateProjectAce;
import edu.upenn.cis.braintrust.shared.UpdateUserAce;
import edu.upenn.cis.braintrust.shared.UpdateWorldAce;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.client.IPermsView.IPermsPresenter;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.SecAlertCallback;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse.UserIdAndName;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class PermsPresenter implements IPermsPresenter {

	// ImmutableBiMap will preserve this order when iterating.
	private BiMap<String, ExtPermission> stringToUserPerm;

	// ImmutableBiMap will preserve this order when iterating.
	private BiMap<String, Optional<ExtPermission>> stringToWorldPerm;

	// ImmutableBiMap will preserve this order when iterating.
	private BiMap<String, ExtPermission> stringToProjectPerm;

	private final ListDataProvider<ExtUserAce> userAceProvider = new ListDataProvider<ExtUserAce>();
	private final ListDataProvider<ExtProjectAce> projectAceProvider = new ListDataProvider<ExtProjectAce>();
	private final ListDataProvider<ExtWorldAce> worldAceProvider = new ListDataProvider<ExtWorldAce>();
	private final ListDataProvider<ProjectGroup> groupProvider = new ListDataProvider<ProjectGroup>();
	private final AsyncDataProvider<UserIdAndName> userProvider = new AsyncDataProvider<UserIdAndName>() {

		@Override
		protected void onRangeChanged(HasData<UserIdAndName> display) {
			final Range visibleRange = display.getVisibleRange();
			final int start = visibleRange.getStart();
			final int length = visibleRange.getLength();
			final Set<UserId> excludedUsers = newHashSet();
			for (final ExtUserAce ace : userAceProvider.getList()) {
				excludedUsers.add(ace.getUserId());
			}
			clientFactory.getPortal().getEnabledUsers(
					clientFactory.getSessionModel().getSessionID(),
					start,
					length,
					excludedUsers,
					new SecFailureAsyncCallback.AlertCallback<GetUsersResponse>(
							clientFactory,
							"Unable to load user data.") {

						@Override
						public void onSuccess(GetUsersResponse result) {
							updateRowData(start, result.getUsers());
							updateRowCount(result.getTotalCount(), true);
						}
					});
		}
	};
	private final MultiSelectionModel<UserIdAndName> userSelectionModel = new MultiSelectionModel<UserIdAndName>(
			new ProvidesKey<UserIdAndName>() {
				@Override
				public Object getKey(UserIdAndName item) {
					return item == null ? null : item.getUserId();
				}
			});
	private final MultiSelectionModel<ProjectGroup> projectGroupSelectionModel = new MultiSelectionModel<ProjectGroup>(
			new ProvidesKey<ProjectGroup>() {
				@Override
				public Object getKey(ProjectGroup item) {
					return item == null ? null : item.getProjectId().toString()
							+ "-" + item.getType().toString();
				}
			});
//	private final SnapshotServicesAsync service;
	private IPermsView view;
	private final String revID;
	private final String friendlyName;
	private final boolean aclIsEditable;
	private final List<IEditAclAction<?>> pendingEdits = newArrayList();

	private final ClientFactory clientFactory;

	public PermsPresenter(
			ClientFactory factory,
			boolean aclIsEditable,
			ExtWorldAce worldAce,
			List<ExtProjectAce> projectAces,
			List<ExtUserAce> userAces,
			DerivedSnapshot toolResult,
			BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm,
			BiMap<String, ExtPermission> displayStrToProjectPerm,
			BiMap<String, ExtPermission> displayStrToUserPerm) {
		this(
				factory,
				aclIsEditable,
				worldAce,
				projectAces,
				userAces,
				toolResult
						.getAnalysisId(),
				toolResult.getAnalysisType(),
				displayStrToWorldPerm,
				displayStrToProjectPerm,
				displayStrToUserPerm);
	}

	public PermsPresenter(
			ClientFactory factory,
			boolean aclIsEditable,
			ExtWorldAce worldAce,
			List<ExtProjectAce> projectAces,
			List<ExtUserAce> userAces,
			String revID,
			String friendlyName,
			BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm,
			BiMap<String, ExtPermission> displayStrToProjectPerm,
			BiMap<String, ExtPermission> displayStrToUserPerm) {
		this.clientFactory = checkNotNull(factory);
//		this.service = checkNotNull(factory.getCacheManager());
		checkNotNull(worldAce);
		checkNotNull(userAces);
		checkNotNull(projectAces);
		this.aclIsEditable = aclIsEditable;
		this.revID = checkNotNull(revID);
		this.friendlyName = checkNotNull(friendlyName);
		worldAceProvider.setList(Collections.singletonList(worldAce));
		projectAceProvider.setList(projectAces);
		userAceProvider.setList(userAces);
		this.stringToUserPerm = checkNotNull(displayStrToUserPerm);
		this.stringToWorldPerm = checkNotNull(displayStrToWorldPerm);
		this.stringToProjectPerm = checkNotNull(displayStrToProjectPerm);
	}

	@Override
	public ListDataProvider<ExtUserAce> getUserAceProvider() {
		return userAceProvider;
	}

	@Override
	public ListDataProvider<ExtProjectAce> getProjectAceProvider() {
		return projectAceProvider;
	}

	@Override
	public void setView(IPermsView view) {
		this.view = view;
	}

	@Override
	public String getFriendlyName() {
		return friendlyName;// toolResult.getAnalysisType();
	}

	@Override
	public boolean userIsOwner() {
		return aclIsEditable;
	}

	@Override
	public List<String> getUserPermsOptions() {
		return newArrayList(stringToUserPerm.keySet());
	}

	@Override
	public List<String> getWorldPermsOptions() {
		return newArrayList(stringToWorldPerm.keySet());
	}

	@Override
	public ListDataProvider<ExtWorldAce> getWorldAceProvider() {
		return worldAceProvider;
	}

	@Override
	public void addNewUserAces() {
		final Set<UserIdAndName> selectedUsers = userSelectionModel
				.getSelectedSet();
		final ExtWorldAce worldAce = worldAceProvider.getList().get(0);
		final Integer aclVersion = worldAce.getAclVersion();
		for (final UserIdAndName user : selectedUsers) {
			final ExtUserAce newUserAce = new ExtUserAce(
					user.getUserId(),
					user.getUsername(),
					revID,
					aclVersion,
					null,
					null);
			newUserAce.getPerms().add(CorePermDefs.READ_PERM);
			userAceProvider.getList().add(
					newUserAce);
			pendingEdits.add(new NewUserAce(newUserAce));
		}
		userSelectionModel.clear();
	}

	@Override
	public void removeUserAce(ExtUserAce toBeRemoved) {
		userAceProvider.getList().remove(toBeRemoved);
		pendingEdits.add(new RemoveUserAce(toBeRemoved));
	}

	@Override
	public void removeProjectAce(ExtProjectAce toBeRemoved) {
		projectAceProvider.getList().remove(toBeRemoved);
		pendingEdits.add(new RemoveProjectAce(toBeRemoved));
	}

	@Override
	public void updateUserAce(ExtUserAce orig, ExtPermission newPerms) {
		pendingEdits.add(new UpdateUserAce(orig, newPerms));
	}

	@Override
	public void updateWorldAce(ExtWorldAce orig,
			Optional<ExtPermission> newPerms) {
		pendingEdits.add(new UpdateWorldAce(orig, newPerms.orNull()));
	}

	@Override
	public void doSave() {
		if (pendingEdits.isEmpty()) {
			view.closeDialog();
			return;
		}
		clientFactory.getPortal().editAcl(
				clientFactory.getSessionModel().getSessionID(), 
				"DataSnapshot",
				CorePermDefs.CORE_MODE_NAME,
				pendingEdits,
				new SecFailureAsyncCallback<List<EditAclResponse>>(
						clientFactory) {

					@Override
					public void onFailureCleanup(Throwable caught) {
						pendingEdits.clear();
						view.closeDialog();
					}

					@Override
					public void onNonSecFailure(Throwable caught) {
						Window.alert("Your changes could not be saved.");
					}

					@Override
					public void onSuccess(List<EditAclResponse> result) {
						boolean errors = false;
						final StringBuffer sb = new StringBuffer();
						for (final EditAclResponse response : result) {
							if (!response.isSuccess()) {
								errors = true;
								sb.append(response.getUserMessage() == null ? "There was an error while saving your modifications."
										: response.getUserMessage());
								sb.append("\n");
							}
						}
						pendingEdits.clear();
						if (errors) {
							Window.alert(sb.toString());
						}
						view.closeDialog();

					}
				});
	}

	@Override
	public void addUserListDisplay(HasData<UserIdAndName> usersList) {
		userProvider.addDataDisplay(usersList);
	}

	@Override
	public SelectionModel<UserIdAndName> getUserListSelectionModel() {
		return userSelectionModel;
	}

	@Override
	public String worldPermToString(Optional<ExtPermission> worldPerm) {
		return stringToWorldPerm.inverse().get(worldPerm);
	}

	@Override
	public String userPermToString(ExtPermission userPerm) {
		return stringToUserPerm.inverse().get(userPerm);
	}

	@Override
	public Optional<ExtPermission> stringToWorldPerm(String displayString) {
		return stringToWorldPerm.get(displayString);
	}

	@Override
	public ExtPermission stringToUserPerm(String displayString) {
		return stringToUserPerm.get(displayString);
	}

	@Override
	public void addProjectGroupListDisplay(final HasData<ProjectGroup> groupList) {
		final Set<ProjectGroup> excludedGroups = newHashSet();
		for (final ExtProjectAce projAce : projectAceProvider.getList()) {
			excludedGroups.add(projAce.getProjectGroup());
		}
		clientFactory.getProjectServices().getProjectGroups(
				clientFactory.getSessionModel().getSessionID(),
				excludedGroups,
				new SecAlertCallback<List<ProjectGroup>>(clientFactory) {

					@Override
					public void onSuccess(List<ProjectGroup> result) {
						groupProvider.setList(result);
						groupProvider.addDataDisplay(groupList);
					}
				});

	}

	@Override
	public void addNewProjectAces() {
		final Set<ProjectGroup> selectedGroups = projectGroupSelectionModel
				.getSelectedSet();
		final ExtWorldAce worldAce = worldAceProvider.getList().get(0);
		final Integer aclVersion = worldAce.getAclVersion();
		for (final ProjectGroup group : selectedGroups) {
			final ExtProjectAce newProjectAce = new ExtProjectAce(
					group.getProjectId(),
					group.getProjectName(),
					group.getType(),
					revID,
					aclVersion,
					null,
					null);
			newProjectAce.getPerms().add(CorePermDefs.READ_PERM);
			projectAceProvider.getList().add(
					newProjectAce);
			pendingEdits.add(new NewProjectAce(newProjectAce));
		}
		projectGroupSelectionModel.clear();
	}

	@Override
	public SelectionModel<ProjectGroup> getProjectGroupListSelectionModel() {
		return projectGroupSelectionModel;
	}

	@Override
	public List<String> getProjectPermsOptions() {
		return newArrayList(stringToProjectPerm.keySet());
	}

	@Override
	public String projectPermToString(ExtPermission projectPerm) {
		return stringToProjectPerm.inverse().get(projectPerm);
	}

	@Override
	public ExtPermission stringToProjectPerm(String displayString) {
		return stringToProjectPerm.get(displayString);
	}

	@Override
	public void updateProjectAce(ExtProjectAce orig, ExtPermission newPerms) {
		pendingEdits.add(new UpdateProjectAce(orig, newPerms));
	}
}
