/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.filters;

import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * Simple metadata filter, returns true if the name substring-matches
 * the given subString.
 * 
 * @author zives
 *
 */
public class MetadataNameContainsFilter extends BaseMetadataFilter {
	String subString;
	
	public MetadataNameContainsFilter(String subString) {
		this.subString = subString;
	}
	
	public void setString(String str) {
		subString = str;
	}
	
	public String getString() {
		return subString;
	}

	@Override
	public boolean satisfiesFilter(PresentableMetadata md) {
		return md.getFriendlyName().contains(subString);
	}

}
