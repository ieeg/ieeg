package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

public class PopupMenu extends PopupPanel implements Attachable {
	private MenuBar menu;

	public PopupMenu(Widget parent) {
		super(true);

		menu = new MenuBar(true);
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendEscaped("Options");
		menu.addItem(new MenuItem(sb.toSafeHtml()));
		setWidget(menu);
	}
	
	public MenuBar getMenu() {
		return menu;
	}
	
	public void addItem(MenuItem item) {
//		item.addStyleName("contextMenu");
		menu.addItem(item);
	}
	
	public void addSeparator() {
		menu.addSeparator();
	}
	
	public void clearItems() {
		menu.clearItems();
	}

	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
