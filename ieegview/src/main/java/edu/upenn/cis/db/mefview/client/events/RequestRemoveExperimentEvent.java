/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

//import edu.upenn.cis.db.mefview.shared.Project;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class RequestRemoveExperimentEvent extends GwtEvent<RequestRemoveExperimentEvent.Handler> {
	private static final Type<RequestRemoveExperimentEvent.Handler> TYPE = new Type<RequestRemoveExperimentEvent.Handler>();
	
	private final DerivedSnapshot result;
	
	public RequestRemoveExperimentEvent(DerivedSnapshot result){
		this.result = result;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<RequestRemoveExperimentEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<RequestRemoveExperimentEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RequestRemoveExperimentEvent.Handler handler) {
		handler.onRemoveExperiment(result);
	}

	public interface Handler extends EventHandler {
		void onRemoveExperiment(DerivedSnapshot result);
	}
}
