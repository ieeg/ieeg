/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.projectsettings;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.view.client.CellPreviewEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback.AlertCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.DiscussionPostedEvent;
import edu.upenn.cis.db.mefview.client.events.DiscussionRepliedEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectLoadedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotAddedtoProjectEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.WorkspaceModel;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFViewerPanel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.viewers.DiscussionViewer;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.Project.STATE;

public class ProjectPresenter extends BasicPresenter implements  
SnapshotAddedtoProjectEvent.Handler, DiscussionViewer.Handler, 
DiscussionPostedEvent.Handler, DiscussionRepliedEvent.Handler,
SearchResultsUpdatedEvent.Handler, ProjectLoadedEvent.Handler {
	public interface Display extends BasicPresenter.Display {
		public void init(Project project);
		
		public List<UserInfo> getAdmins();
		public void addAdmin(UserInfo user);
		public Set<UserInfo> getSelectedAdmins();
		public void setSelectedAdmin(UserInfo user, boolean sel);
		
		public void setHandler(DiscussionViewer.Handler handler);
		
		public List<UserInfo> getTeamMembers();
		public void addTeamMember(UserInfo user);
		public Set<UserInfo> getSelectedTeamMembers();
		public void setSelectedTeamMember(UserInfo user, boolean sel);
		
		public List<PresentableMetadata> getSnapshots();
		public void addSnapshots(Collection<? extends PresentableMetadata> coll);
		public void addSnapshot(PresentableMetadata sr);
		public Set<PresentableMetadata> getSelectedSnapshots();
		public void setSelectedSnapshot(PresentableMetadata sr, boolean sel);
		public void removeSnapshot(PresentableMetadata sr);
		
		public List<ToolDto> getTools();
		public void addTools(Collection<ToolDto> tools);
		public void addTool(ToolDto ti);
		public Set<ToolDto> getSelectedTools();
		public void setSelectedTool(ToolDto ti, boolean sel);
		
		void setEegHandler(ClickHandler handler);
		void setDicomHandler(ClickHandler handler);
		void setDocumentHandler(ClickHandler handler);
		void setImageHandler(ClickHandler handler);
//		void setRunHandler(ClickHandler handler);
		void enableEegHandler(boolean sel);
		void enableDicomHandler(boolean sel);
		void enableDocumentHandler(boolean sel);
		void enableImageHandler(boolean sel);
//		void enableRunHandler(boolean sel);
		
		public void addPost(Post p);
		public void setPostList(List<Post> pl);
		public void renderNextPost();
		
		public void addSnapshotSelectionChangedHandler(Handler handler);
		public void addToolSelectionChangedHandler(Handler handler);
		public void addAdminSelectionChangedHandler(Handler handler);
		public void addTeamMemberSelectionChangedHandler(Handler handler);
		
		public void log(String msg);
        void addSnapshotPreviewHandler(
            com.google.gwt.view.client.CellPreviewEvent.Handler<PresentableMetadata> handler);
        void hideProjectLoadingPopup();
        boolean confirmDatasetRemoval(int numberToRemove);

	}
	
	WorkspaceModel model;
	AppModel control;

	Project theProject;
	
	PresentableMetadata lastClick;
	
	public static final String NAME = "project";
	public static final Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.PROJECT);
	final static ProjectPresenter seed = new ProjectPresenter();
	
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, false));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ProjectPresenter() { super(null, NAME, TYPES); }
	
	public ProjectPresenter( 
			final AppModel parent,
			final ClientFactory clientFactory,
			final PresentableMetadata project) {
		
		super(clientFactory, NAME, TYPES);
		
//		super (user, clientFactory.getControlPane().getMainPaneWidth(), 
//				clientFactory.getControlPane().getLeftPaneHeight());
		
		theProject = (Project)project;
		control = parent;
//		this.clientFactory = clientFactory;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		
		getDisplay().addPresenter(this);
		getDisplay().setHandler(this);
		
		  
//		  clientFactory.getAsync().getAllUsers(clientFactory.getSessionID(), 
//		      new SecAlertCallback<Set<UserInfo>>(clientFactory) {
//
//		    @Override
//		    public void onSuccess(Set<UserInfo> result) {
//		      display.log("ProjectPresenter read users");
//		      for (UserInfo ui: result)
//		        if (!display.getAdmins().contains(ui))
//		          display.addAdmin(ui);
//		      for (UserInfo ui: result)
//		        if (!display.getTeamMembers().contains(ui))
//		          display.addTeamMember(ui);
//		    }
//
//		  });
//		  
		
		getDisplay().setEegHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
			  OpenDisplayPanel act = new OpenDisplayPanel(lastClick, false, 
                EEGViewerPanel.NAME, null);
			  IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			  
//				for (SearchResult sr: display.getSelectedSnapshots()) {
//					OpenDisplayPanelAction act = new OpenDisplayPanelAction(user, sr, false, 
//							EEGViewerPanel.NAME, null);
//				
//					clientFactory.fireEvent(new OpenDisplayPanelEvent(act));
//				}
			}

		});
		getDisplay().setDocumentHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
					OpenDisplayPanel act = new OpenDisplayPanel(lastClick, false, 
							PDFViewerPanel.NAME,
							null);
					
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
						}
			
		});
		getDisplay().setDicomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
					OpenDisplayPanel act = new OpenDisplayPanel(lastClick, false, 
							AttachmentDownloader.NAME,
							null);
				
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
				
			}
			
		});
		getDisplay().setImageHandler(new ClickHandler() {
			//
			public void onClick(ClickEvent arg0) {
					OpenDisplayPanel act = new OpenDisplayPanel(lastClick, false, 
							ImageViewerPanel.NAME, null);
					
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
				
			}

		});
		if (getSessionModel().isRegisteredUser()){
			getDisplay().addSnapshotSelectionChangedHandler(new Handler() {
		    @Override
		    public void onSelectionChange(SelectionChangeEvent event) {
		      onSnapshotSelectionChange(event);
		    }
		  });

		
			getDisplay().addAdminSelectionChangedHandler(new Handler() {
		    @Override
		    public void onSelectionChange(SelectionChangeEvent event) {
		      onAdminSelectionChanged(event);
		    }
		  });
			getDisplay().addTeamMemberSelectionChangedHandler(new Handler() {
		    @Override
		    public void onSelectionChange(SelectionChangeEvent event) {
		      onTeamMemberSelectionChanged(event);
		    }
		  });
		}
//		display.setRunHandler(new ClickHandler() {
//			
//			public void onClick(ClickEvent arg0) {
//				for (ToolInfo ti: display.getSelectedTools()) {
//					if (!control.haveSnapshotsLoaded()) {
//						Dialogs.messageBox("No snapshots loaded", "Sorry, you must load a data snapshot to analyze");
//					} else if (ti != null)
//						clientFactory.fireEvent(new RequestLaunchToolEvent(ti));
//				}
//			}
//			
//		});

		getDisplay().addToolSelectionChangedHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				onToolSelectionChanged(event);
			}
		});
		
		getDisplay().addSnapshotPreviewHandler(new CellPreviewEvent.Handler<PresentableMetadata>() {

		  @Override
		  public void onCellPreview(CellPreviewEvent<PresentableMetadata> event) {
		    // TODO Auto-generated method stub
		    boolean isClick = "click".equals(event.getNativeEvent().getType());
		    if (isClick) {
		      GWT.log("Setting project buttons.");
		      
		      lastClick = event.getValue(); // Store last clicked dataset for button clickhandlers.
		      PresentableMetadata sr = lastClick;
		      getDisplay().enableEegHandler(true);
		      getDisplay().enableDocumentHandler(false);
		      getDisplay().enableDicomHandler(false);
		      getDisplay().enableImageHandler(false);
		      
		      String revId = sr.getId();
		      DataSnapshotViews model = control.getSnapshotModel(revId);

		      if (model != null) {
		    	  boolean hasPDF = false;
		    	  boolean hasNonPDF = false;
		    	  for (FileInfo f: model.getState().getFiles()) {
		    		  if (f.getContentDescriptor().equals("application/pdf"))
		    			  hasPDF = true;
		    		  
		    		  if (!f.getContentDescriptor().equals("application/pdf"))
		    			  hasNonPDF = true;
		    	  }
		    	  
		        if (hasPDF)//!model.getState().getDocuments().isEmpty())
		        	getDisplay().enableDocumentHandler(true);
		        if (hasNonPDF && getSessionModel().isRegisteredUser())//!model.getState().getFiles().isEmpty()){
		          // always keep Download DICOM disabled for Guest
		           getDisplay().enableDicomHandler(true);
//		        }
		      } else {
		    	  getClientFactory().getPortalServices().getRecordingObjects(getSessionModel().getSessionID(), revId, 
		            new SecFailureAsyncCallback.SecAlertCallback<List<RecordingObject>>(getClientFactory()) {

		          @Override
		          public void onSuccess(List<RecordingObject> result) {
		        	boolean documents = false;
		        	for (RecordingObject recordingObject : result) {
		        		if (recordingObject.getInternetMediaType().equals("application/pdf")) {
		        			documents = true;
		        			break;
		        		}
		        	}
		            if (!documents)
		              return;
		            else
		            	getDisplay().enableDocumentHandler(true);
		          }

		        });

		        // always keep Download DICOM disabled for Guest
//		        clientFactory.getCacheManager().getImagesFile(clientFactory.getSessionID(), revId, new SecFailureAsyncCallback.SecAlertCallback<String>(clientFactory) {
//
//		          @Override
//		          public void onSuccess(String result) {
//		            if (result == null)
//		              return;
//		            else
//		              display.enableDicomHandler(true);
//		          }
//
//		        });
		      }

		      if (sr instanceof SearchResult && ((SearchResult)sr).getImageCount() > 0){
		    	  getDisplay().enableImageHandler(true);
		      }
		      
		      
		      
            }
		   
		  }
		  
		});
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ProjectLoadedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(SnapshotAddedtoProjectEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(DiscussionPostedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(DiscussionRepliedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(SearchResultsUpdatedEvent.getType(), this);
		
		getDisplay().bindDomEvents();
	}
	
	public void updateRevId(String oldRevId, String newRevId) {
		for (PresentableMetadata sr: getDisplay().getSnapshots()) {
			if (sr.getId().equals(oldRevId))
				sr.setId(newRevId);
		}
		
	}

	public WorkspaceModel getWorkspaceModel() {
		return model;
	}
	
	@Override
	public void onAddedSnapshotToProject(final Project project,
			final SearchResult result) {
		//Don't change anything if the project has not loaded.
		if (theProject.getState() == STATE.LOADED) {
			final Set<PresentableMetadata> oldResults = Sets.newHashSet();
			oldResults.addAll(theProject.getSnapshots());

			Set<String> snapshotIds = Sets.newHashSet();
			for (PresentableMetadata sr : theProject.getSnapshots())
				snapshotIds.add(sr.getId());
			if (!getDisplay().getSelectedSnapshots().contains(result) ||
					!snapshotIds.contains(result.getDatasetRevId())) {
				snapshotIds.add(result.getDatasetRevId());
				getDisplay().addSnapshot(result);

				getDisplay().setSelectedSnapshot(result, true);

				getClientFactory().getProjectServices().updateProjectSnapshots(
						getSessionModel().getSessionID(),
						theProject.getName(),
						snapshotIds,
						new SecFailureAsyncCallback.SecAlertCallback<String>(
								getClientFactory()) {

							@Override
							public void onSuccess(String arg0) {
								getDisplay().addSnapshot(result);
							}


							@Override
							public void onFailureCleanup(Throwable caught) {
								theProject.setSnapshots(oldResults);
								for (PresentableMetadata sr : getDisplay()
										.getSnapshots())
									getDisplay().setSelectedSnapshot(sr,
											oldResults.contains(sr));								
							}
							
							

						});
			}
		}
	}
	
	public void onSnapshotSelectionChange(SelectionChangeEvent event) {
		//Don't do anything if the project has not finished loading.
		if (theProject.getState() == STATE.LOADED) {
			final Set<PresentableMetadata> selectedSnapshots = Sets.newHashSet();
			selectedSnapshots.addAll(getDisplay().getSelectedSnapshots());
			boolean isDifferent = false;

			final Set<String> snaps = Sets.newHashSet();
			for (PresentableMetadata sr : selectedSnapshots) {
				snaps.add(sr.getId());
				if (!theProject.getSnapshots().contains(sr)) {
					isDifferent = true;
				}
			}
			final Set<PresentableMetadata> toRemove = Sets.newHashSet();
			if (!isDifferent)
				for (PresentableMetadata sr : theProject.getSnapshots())
					if (!selectedSnapshots.contains(sr)) {
						isDifferent = true;
						toRemove.add(sr);
					}

			if (!isDifferent)
				return;
			if (!toRemove.isEmpty()) {
				final boolean removalConfirmed = getDisplay().confirmDatasetRemoval(toRemove.size());
				if (!removalConfirmed) {
					for (PresentableMetadata sr : getDisplay()
							.getSnapshots())
						getDisplay().setSelectedSnapshot(sr,
								theProject.getSnapshots().contains(sr));	
					return;
				}
			}

			GWT.log("Adding Snapshots to project");
			getDisplay().log("Adding snapshots for " + snaps.toString());

			getClientFactory().getProjectServices().updateProjectSnapshots(
					getSessionModel().getSessionID(), theProject.getPubId(),
					snaps,
					new SecFailureAsyncCallback.SecAlertCallback<String>(getClientFactory()) {

						@Override
						public void onFailureCleanup(Throwable arg0) {
							for (PresentableMetadata sr : getDisplay()
									.getSnapshots())
								getDisplay().setSelectedSnapshot(sr,
										theProject.getSnapshots().contains(sr));	
						}

						@Override
						public void onSuccess(String arg0) {
							theProject.getSnapshots().addAll(selectedSnapshots);
							theProject.getSnapshots().removeAll(toRemove);
							getDisplay().log(
									"Recorded project with " + snaps.size()
											+ " items");
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new ProjectUpdatedEvent(theProject));
						}

					});
		}
	}
	
	void onAdminSelectionChanged(SelectionChangeEvent event) {
		boolean isDifferent = false;
		
		final Set<UserInfo> selectedUsers = Sets.newHashSet();
		selectedUsers.addAll(getDisplay().getSelectedAdmins());
		if (selectedUsers.isEmpty()) {
			//Don't let us end up with no admins.
			for (UserInfo ui: getDisplay().getAdmins()) {
				getDisplay().setSelectedAdmin(ui, theProject.getAdmins().contains(ui));
			}
			Window.alert("There must be at least one admin for a project");
			return;
		}
		final Set<String> adminSet = Sets.newHashSet();
		for (UserInfo ui: selectedUsers) {
			adminSet.add(ui.getName());
			if (!theProject.getAdmins().contains(ui)) {
				isDifferent = true;
			}
		}
		final Set<UserInfo> toRemove = Sets.newHashSet();
		if (!isDifferent)
			for (UserInfo ui: theProject.getAdmins())
				if (!selectedUsers.contains(ui)) {
					isDifferent = true;
					toRemove.add(ui);
				}
		
		if (!isDifferent)
			return;
		
		getDisplay().log("Updating admins: " + adminSet.toString());

		getClientFactory().getProjectServices().updateProjectAdministrators(getSessionModel().getSessionID(), theProject.getPubId(), 
				adminSet, new AlertCallback<String>(getClientFactory(), "Insufficient permissions or server error") {

					@Override
					public void onFailureCleanup(Throwable caught) {
						for (UserInfo ui: getDisplay().getAdmins())
							getDisplay().setSelectedAdmin(ui, theProject.getAdmins().contains(ui));
					}

					@Override
					public void onSuccess(String arg0) {
						theProject.getAdmins().addAll(selectedUsers);
						theProject.getAdmins().removeAll(toRemove);
						getDisplay().log("Updated admins");
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new ProjectUpdatedEvent(theProject));
					}
			
		});
	}
	
	void onTeamMemberSelectionChanged(SelectionChangeEvent event) {
		boolean isDifferent = false;
		
		final Set<UserInfo> selectedUsers = Sets.newHashSet();
		selectedUsers.addAll(getDisplay().getSelectedTeamMembers());
		
		final Set<String> team = Sets.newHashSet();
		for (UserInfo ui: selectedUsers) {
			team.add(ui.getName());
			if (!theProject.getTeam().contains(ui)) {
				isDifferent = true;
			}
		}
		final Set<UserInfo> toRemove = Sets.newHashSet();
		if (!isDifferent)
			for (UserInfo ui: theProject.getTeam())
				if (!selectedUsers.contains(ui)) {
					isDifferent = true;
					toRemove.add(ui);
				}
		
		if (!isDifferent)
			return;
		
		getDisplay().log("Updating team: " + team.toString());
		
		getClientFactory().getProjectServices().updateProjectTeam(getSessionModel().getSessionID(), theProject.getPubId(), 
				team, new AlertCallback<String>(getClientFactory(), "Insufficient permissions or server error") {

					@Override
					public void onSuccess(String arg0) {
						theProject.getTeam().addAll(selectedUsers);
						theProject.getTeam().removeAll(toRemove);
						getDisplay().log("Updated team");
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new ProjectUpdatedEvent(theProject));
					}
					
					@Override
					public void onFailureCleanup(Throwable caught) {
						for (UserInfo user : getDisplay()
								.getTeamMembers())
							getDisplay().setSelectedTeamMember(user,
									theProject.getTeam().contains(user));
					}
			
		});
	}
	
	public void onToolSelectionChanged(SelectionChangeEvent event) {
//		display.enableRunHandler(true);
		boolean isDifferent = false;
		
		final Set<ToolDto> oldTools = Sets.newHashSet();
		oldTools.addAll(theProject.getTools());
		
		final Set<String> toolList = Sets.newHashSet();
		for (ToolDto ti: getDisplay().getSelectedTools()) {
			toolList.add(ti.getId().get());
			if (!theProject.getTools().contains(ti)) {
				isDifferent = true;
				theProject.getTools().add(ti);
			}
		}
		Set<ToolDto> toRemove = Sets.newHashSet();
		if (!isDifferent)
			for (ToolDto ti: theProject.getTools())
				if (!getDisplay().getSelectedTools().contains(ti)) {
					isDifferent = true;
					toRemove.add(ti);
				}
		
		for (ToolDto ti: toRemove)
			theProject.getTools().remove(ti);
		if (!isDifferent)
			return;
		
		getDisplay().log("Updating tools: " + toolList.toString());
		
		getClientFactory().getProjectServices().updateProjectTools(getSessionModel().getSessionID(), theProject.getPubId(), 
				toolList, new AlertCallback<String>(getClientFactory(), "Insufficient permissions or unable to update team") {
					@Override
					public void onNonSecFailure(Throwable caught) {
						theProject.setTools(oldTools);
						for (ToolDto ti: getDisplay().getTools())
							getDisplay().setSelectedTool(ti, oldTools.contains(ti));

						super.onNonSecFailure(caught);
					}

					@Override
					public void onSuccess(String arg0) {
						// TODO Auto-generated method stub
						getDisplay().log("Updated tools list");
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new ProjectUpdatedEvent(theProject));
					}
			
		});
	}
	
	
	@Override
	public void onDiscussionPosted(Project action, Post post) {
		GWT.log("Discussion posted");
		if (theProject.getPubId() == action.getPubId()) {
			GWT.log("Should be posted");
			getDisplay().addPost(post);
		} else
			GWT.log("Incompatible project " + action.getName() + " vs " + theProject.getName());
	}

	@Override
	public void onDiscussionReplied(Project action, String refs,
			Post post) {
		GWT.log("Discussion replied");
		if (theProject.getPubId() == action.getPubId()) {
			GWT.log("Should be posted");
			getDisplay().addPost(post);
		} else
			GWT.log("Incompatible project " + action.getName() + " vs " + theProject.getName());
	}

	@Override
	public void postToServer(final Post p) {
		GWT.log("Posting to server");
		getClientFactory().getProjectServices().addDiscussionToProject(getSessionModel().getSessionID(), 
				theProject.getPubId(), 
				p, new SecFailureAsyncCallback<String>(getClientFactory()) {

					@Override
					public void onFailureCleanup(Throwable arg0) {
						getSessionModel().getMainLogger().warning("Failed to save post");
						
						getDisplay().renderNextPost();
					}

					@Override
					public void onSuccess(String arg0) {
						getSessionModel().getMainLogger().info("Saved post to " + theProject.getName());
						GWT.log("Saved post to " + theProject.getName());
//						refreshPosts();
						p.setPostId(arg0);
						
						if (p.getRefId() == null)
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new DiscussionPostedEvent(theProject, p));
						else
							IeegEventBusFactory.getGlobalEventBus().fireEvent(
									new DiscussionRepliedEvent(theProject, 
											p.getRefId(), p));
						getDisplay().renderNextPost();
					}

					@Override
					protected void onNonSecFailure(Throwable caught) {}
			
		});
	}

	@Override
	public void refreshPosts() {
		getClientFactory().getProjectServices().getProjectDiscussions( 
				getSessionModel().getSessionID(), 
				theProject.getPubId(), 
				0, 
				-1, 
				new SecFailureAsyncCallback<List<Post>>(getClientFactory()) {

					@Override
					public void onFailureCleanup(Throwable arg0) {
						getDisplay().renderNextPost();
					}

					
					@Override
					public void onSuccess(List<Post> posts) {
						getDisplay().setPostList(posts);
					}


					@Override
					protected void onNonSecFailure(Throwable caught) {}
			
		});
	}

	@Override
	public void onDiscussionReplied(DataSnapshotModel action, String refs,
			Post post) {
		
	}

	@Override
	public void onDiscussionPosted(DataSnapshotModel action, Post post) {
		
	}
	
	public void addSearchResults(Collection<SearchResult> results) {
		getDisplay().addSnapshots(results);
		getDisplay().log("Added " + results.size() + " search results to project");
		for (SearchResult sr: results) {
			if (theProject.getSnapshots().contains(sr)) {
				getDisplay().setSelectedSnapshot(sr, true);
				getDisplay().log("Item set to selected");
			} else
				getDisplay().setSelectedSnapshot(sr, false);
		}
	}
	
	public void addTools(Collection<ToolDto> toolSet) {
		getDisplay().addTools(toolSet);
		getDisplay().log("Added " + toolSet.size() + " tools to project");
		for (ToolDto ti: toolSet) {
			if (theProject.getTools().contains(ti)) {
				getDisplay().setSelectedTool(ti, true);
				getDisplay().log("Item set to selected");
			} else
				getDisplay().setSelectedTool(ti, false);
		}
	}

	@Override
	public void onSearchResultsChanged(String search, List<PresentableMetadata> newResults,
			List<PresentableMetadata> deletedResults, boolean replace) {
		
		for (PresentableMetadata newResult : newResults) {
			if (!getDisplay().getSnapshots().contains(newResult)) {
				getDisplay().getSnapshots().add(newResult);
			}
		}
		//getDisplay().getSnapshots().addAll(newResults);
		
		if (deletedResults != null)
			for (PresentableMetadata sr: deletedResults) {
				if (!getDisplay().getSelectedSnapshots().contains(sr))
					getDisplay().removeSnapshot(sr);
			}

	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		return new ProjectPresenter(controller, clientFactory, project);
	}

	@Override
	public Display getDisplay() {
		return (Display)display;
	}

	@Override
	public void onLoadedProject(Project project) {
		if (project == theProject) {
			if (getSessionModel().isRegisteredUser()) {
				for (PresentableMetadata searchResult : theProject
						.getSnapshots()) {
					getDisplay().addSnapshot(searchResult);
					getDisplay().setSelectedSnapshot(searchResult, true);
				}
			}
			getDisplay().hideProjectLoadingPopup();

		}
	}
}
