/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.sql.Timestamp;
import java.util.List;

import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ResizeLayoutPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.widgets.RichTextToolbar;
import edu.upenn.cis.db.mefview.shared.Post;

public class DiscussionViewer extends ResizeLayoutPanel {
	RichTextToolbar bar;
	RichTextArea textEditor = new RichTextArea();
	VerticalPanel postPanel = new VerticalPanel();
	TextBox title = new TextBox();
	String refId = null;
	VerticalPanel postBox = new VerticalPanel();
	VerticalPanel discussionContent = new VerticalPanel();
	
	Handler handler;
	
//	String user;
	String snapshotId;
//	ClientFactory clientFactory;
	SessionModel sessionModel;
	
	Button postNewButton = new Button("Post new");
	Button postMessageButton = new Button("Post");
	
	public static interface Handler {
		void postToServer(final Post p);
		
		void refreshPosts();
	}
	
	public DiscussionViewer(String titleText, //String user, 
			Handler handler,
			String snapshotId, final SessionModel theSessionModel) {
		this.handler = handler;
//		this.user = user;
		this.snapshotId = snapshotId;
		this.sessionModel = theSessionModel;	

//		HorizontalPanel labPan = new HorizontalPanel();
//		Label lab = new Label(titleText);
//		lab.setStyleName("header");
//		lab.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
//		labPan.add(lab);
//		labPan.add(new HTML("<p></p>"));

		/////////////////////// Post box

		bar = new RichTextToolbar(textEditor);
		bar.addStyleName("ieeg-text-editor-bar");
		
//		discussionContent.setWidth("100%");
//		discussionContent.add(labPan);
		

		discussionContent.add(postPanel);
		discussionContent.add(postNewButton);
		discussionContent.add(postBox);

//		postBox.setWidth("100%");
        //Only allow posting as user
		if (sessionModel.isRegisteredUser()) {
          HorizontalPanel tPanel = new HorizontalPanel();
          postBox.add(tPanel);
          tPanel.add(new Label("Title: "));
          tPanel.add(title);
          title.setMaxLength(255);
          title.setWidth("100%");
          postBox.add(bar);
		
		
		  postBox.add(textEditor);
		  textEditor.addStyleName("ieeg-style-text-editor");
		  postBox.add(postMessageButton);
		  textEditor.setWidth("90%");
		  postBox.setCellHorizontalAlignment(textEditor, HasHorizontalAlignment.ALIGN_CENTER);
		}
		/////////////////
		
		ScrollPanel scroll = new ScrollPanel();
		scroll.setWidth("100%");
		scroll.add(discussionContent);
		add(scroll);
		
		postMessageButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				post(refId);
			}
			
		});
		postNewButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				setNextPost();
			}
			
		});

		if (snapshotId != null)
			refreshPosts();
	}
	
	public void setSnapshotId(String sid) {
		snapshotId = sid;
		refreshPosts();
	}

	protected void post(final String refId) {
		if (snapshotId == null)
			return;
		
		final Post p = new Post(sessionModel.getUserId(),//user,
				title.getText(),
				textEditor.getHTML(),
				snapshotId,
				null,
				
				refId,
				null);
		if (handler != null)
			handler.postToServer(p);
	}
	
	public void setNextPost() {
		postNewButton.setVisible(false);
		discussionContent.remove(postBox);
		postPanel.remove(postBox);
		discussionContent.add(postBox);
		textEditor.setText("");
		textEditor.setTitle("");
		title.setText("");
		refId = null;
	}
	
	private void setReplyPost(int inx, String titleText) {
		discussionContent.remove(postBox);
		postPanel.remove(postBox);
		postPanel.insert(postBox, inx);
		title.setText("RE: " + titleText);
		textEditor.setText("");
		postNewButton.setVisible(true);
	}
	
	private VerticalPanel createPost(Post p) {
		Timestamp t = p.getTime();

		VerticalPanel postMsg = new VerticalPanel();
		HorizontalPanel heading = new HorizontalPanel();
		Label auth = new Label(p.getAuthorId());
		auth.addStyleName("ieeg-user");
		heading.add(auth);
		Label sep = new Label("   :   ");
		sep.addStyleName("ieeg-title");
		heading.add(sep);
		Label title = new Label(p.getTitle());
		title.addStyleName("ieeg-title");
		heading.add(title);
		
		postMsg.add(heading);
		
		HTML h = new HTML(p.getPost() + "<br/><span class='ieeg-stamp'>" +
				DateTimeFormat.getMediumDateTimeFormat().format(t) +
		"</span><br/>");
		h.addStyleName("ieeg-text-box");
		postMsg.add(h);

		return postMsg;
	}
	
	public void renderPosts(final List<Post> posts) {
		postPanel.clear();
		if (posts.isEmpty()) {
			HTML empty = new HTML("<i>No posts yet exist</i>");
			postPanel.add(empty);
		} else {
			for (Post p : posts) {
				postPanel.add(createPost(p));
				if (sessionModel.isRegisteredUser()) {
				  Button reply = new Button("Reply");
				  reply.addStyleName("ieeg-reply-style");
				  postPanel.add(reply);
				
				  postPanel.setCellHorizontalAlignment(reply, HasHorizontalAlignment.ALIGN_RIGHT);

				  postPanel.add(new HTML("<hr/>"));

				  final int inx = postPanel.getWidgetIndex(reply);
				  final String reference = p.getPostId();
				  final String titleText = p.getTitle();
				  reply.addClickHandler(new ClickHandler() {

				    @Override
				    public void onClick(ClickEvent arg0) {
				      setReplyPost(inx, titleText);
				      refId = reference;
				    }

				  });
				}
			}
		}
		setNextPost();
	}
	
	public void refreshPosts() {
		postPanel.clear();
		
		if (handler != null)
			handler.refreshPosts();
	}
	
	public void postMessage(Post post) {
		postPanel.insert(createPost(post), 0);
		
		if (sessionModel.isRegisteredUser()) {
		  Button reply = new Button("Reply");
		  reply.addStyleName("ieeg-reply-style");
		  postPanel.insert(reply, 1);
		  postPanel.setCellHorizontalAlignment(reply, HasHorizontalAlignment.ALIGN_RIGHT);
		  postPanel.insert(new HTML("<hr/>"), 2);
		}

		setNextPost();
	}
}
