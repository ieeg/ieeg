/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;

public class WebPlace extends WindowPlace {
	String url;
	
	public WebPlace() {}
	
	public WebPlace(String url) {
		super();
		this.url = url;
	}

	public WebPlace(String snapshotID, String url) {
		super(snapshotID);
		this.url = url;
	}
	
	public String getURL() {
		return url;
	}

	@Override
	public String getType() {
		return WebPanel.NAME;
	}

//	@Override
//	public String getSerializableForm() {
//		return url + "\\" + snapshot;
//	}
//
//	public static WindowPlace getFromSerialized(String serial) {
//		String items[] = serial.split("\\");
//		return new WebPlace(items[0], items[1]);
//	}
}
