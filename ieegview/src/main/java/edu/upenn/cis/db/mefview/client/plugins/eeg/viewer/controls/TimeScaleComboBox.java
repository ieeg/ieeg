package edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.controls;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;

import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;

public class TimeScaleComboBox extends ComboBoxWithEnter<ComboBoxDataModel> {
	public TimeScaleComboBox() {
		ComboBoxDataModel timeModel = new ComboBoxDataModel();
		timeModel.add("1 sec/screen", new Double(1.0));
		timeModel.add("2 sec/screen", new Double(2));
		timeModel.add("3 sec/screen", new Double(3));
		timeModel.add("5 sec/screen", new Double(5));
		timeModel.add("10 sec/screen", new Double(10));
		timeModel.add("15 sec/screen", new Double(15));
		timeModel.add("20 sec/screen", new Double(20));
		timeModel.add("30 sec/screen", new Double(30));
		timeModel.add("60 sec/screen", new Double(60));
		timeModel.add("100 sec/screen", new Double(100));
		timeModel.add("200 sec/screen", new Double(200));
		timeModel.add("500 sec/screen", new Double(500));
		
		setModel(timeModel);
		setLazyRenderingEnabled(false);
		setCustomTextAllowed(true);
	}
}
