/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages;

import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import edu.upenn.cis.db.mefview.client.messages.requests.RequestSnapshotData;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpan;
import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

public class SnapshotDataMessage {
	RequestSnapshotData request;
	
	List<INamedTimeSegment> segmentsReturned;
	List<TimeSeriesSpan> dataSpans;
	
	SortedMap<Double,Set<? extends IDetection>> relevantAnnotations;
	
	boolean isPartial;
	
	private boolean minMax;
	
	public SnapshotDataMessage(RequestSnapshotData request,
			List<TimeSeriesSpan> dataSpans, boolean isPartial, boolean isMinMax) {
		this.request = request;
		this.dataSpans = dataSpans;
		this.isPartial = isPartial;
		this.minMax = isMinMax;
	}

	public RequestSnapshotData getRequest() {
		return request;
	}

	public void setRequest(RequestSnapshotData request) {
		this.request = request;
	}

	public List<TimeSeriesSpan> getDataSpans() {
		return dataSpans;
	}
	
	public TimeSeriesSpan getDataSpanFor(INamedTimeSegment specifier) {
		int inx = 0;
		for (TimeSeriesSpanSpecifier spec: request.requestedSpans) {
			if (spec.getTimeSegment().getId().equals(specifier.getId()))
				return dataSpans.get(inx);
			inx++;
		}
		return null;
	}
	
	public boolean isMinMax() {
		return minMax;
	}

	public void setDataSpans(List<TimeSeriesSpan> dataSpans) {
		this.dataSpans = dataSpans;
	}

	public SortedMap<Double, Set<? extends IDetection>> getRelevantAnnotations() {
		return relevantAnnotations;
	}

	public void setMinMax(boolean isMinMax) {
		this.minMax = isMinMax;
	}
	
	public void setRelevantAnnotations(
			SortedMap<Double, Set<? extends IDetection>> relevantAnnotations) {
		this.relevantAnnotations = relevantAnnotations;
	}

	public boolean isPartial() {
		return isPartial;
	}

	public void setPartial(boolean isPartial) {
		this.isPartial = isPartial;
	}

	public List<INamedTimeSegment> getSegmentsReturned() {
		return segmentsReturned;
	}

	public void setSegmentsReturned(List<INamedTimeSegment> segmentsReturned) {
		this.segmentsReturned = segmentsReturned;
	}
	
	
}
