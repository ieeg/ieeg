/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages.requests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.models.TimeSeriesSpanSpecifier;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class RequestAnnotations {
	public String snapshotID;
	public Set<String> layers = new HashSet<String>();
//	public ArrayList<TimeSeriesMetadata> channels;
	public List<INamedTimeSegment> channels;
	
	public List<TimeSeriesSpanSpecifier> requestedSpans;
	
	public int[] requestIds;
	
	public RequestAnnotations(String snapshotID, String layer, 
//			ArrayList<TimeSeriesMetadata> channels,
			List<INamedTimeSegment> channels,
			List<TimeSeriesSpanSpecifier> requestedSpans, int[] ids) {
		this.snapshotID = snapshotID;
		this.layers.add(layer);
		requestIds = ids;
		
		this.channels = channels;
		this.requestedSpans = requestedSpans;
	}
	
	public RequestAnnotations(String snapshotID, String layer, 
//			TimeSeriesMetadata channel, 
			INamedTimeSegment channel,
			TimeSeriesSpanSpecifier requestedSpan, int id) {
		this.snapshotID = snapshotID;
		this.layers.add(layer);
		
		requestIds = new int[1];
		requestIds[0] = id;
		channels = new ArrayList<INamedTimeSegment>();
		channels.add(channel);
		
		requestedSpans = new ArrayList<TimeSeriesSpanSpecifier>();
		requestedSpans.add(requestedSpan);
	}

	public RequestAnnotations(String snapshotID, Collection<String> layers, 
//			ArrayList<TimeSeriesMetadata> channels, 
			List<INamedTimeSegment> channels, 
			List<TimeSeriesSpanSpecifier> requestedSpans, int[] ids) {
		this.snapshotID = snapshotID;
		this.layers.addAll(layers);
		requestIds = ids;
		
		this.channels = channels;
		this.requestedSpans = requestedSpans;
	}
	
	public RequestAnnotations(String snapshotID, Collection<String> layers, 
//			TimeSeriesMetadata channel,
			TraceInfo channel,
			TimeSeriesSpanSpecifier requestedSpan, int id) {
		this.snapshotID = snapshotID;
		this.layers.addAll(layers);
		
		requestIds = new int[1];
		requestIds[0] = id;
//		channels = new ArrayList<TimeSeriesMetadata>();
		channels = new ArrayList<INamedTimeSegment>();
		channels.add(channel);
		
		requestedSpans = new ArrayList<TimeSeriesSpanSpecifier>();
		requestedSpans.add(requestedSpan);
	}

	/**
	 * Merges together multiple requests
	 * 
	 * @param requests
	 * @return
	 */
	public static RequestAnnotations union(Collection<RequestAnnotations> requests) {
//		ArrayList<TimeSeriesMetadata> channels = new ArrayList<TimeSeriesMetadata>();
		List<INamedTimeSegment> channels = new ArrayList<INamedTimeSegment>();
		List<TimeSeriesSpanSpecifier> requestedSpans = new ArrayList<TimeSeriesSpanSpecifier>();
		
		Set<String> layers = new HashSet<String>();
		String snapshotID = "";

		int size = 0;
		for (RequestAnnotations action: requests) {
			size += action.requestIds.length;
		}
		int[] ids = new int[size];
		
		for (RequestAnnotations action: requests) {
			layers.addAll(action.layers);
			int req = 0;
			for (int i = 0; i < action.channels.size(); i++) {
				channels.add(action.channels.get(i));
				requestedSpans.add(action.requestedSpans.get(i));
				ids[req++] = action.requestIds[i];
			}
			if (snapshotID.isEmpty())
				snapshotID = action.snapshotID;
			else if (!snapshotID.equals(action.snapshotID))
				throw new RuntimeException("Union from different snapshots");
		}
		
		return new RequestAnnotations(snapshotID, layers, channels, requestedSpans, ids);
	}
}
