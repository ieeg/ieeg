/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable.StarToggleHandler;
import edu.upenn.cis.db.mefview.shared.PagingPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;


/**
 * Generic Tree Viewer class.  Supports several features beyond the standard scheme:
 * 1. Checkbox, clickable "favorites", icon
 * 2. Asynchronous loading of items
 * 3. Clickable pager for large items.
 *   
 * @author zives
 *
 */
public abstract class PagingCellTree<D,P extends AsyncDataProvider<PresentableMetadata> & 
PagingPresenter<D>> 
	implements TreeViewModel, PagingPresenter<D> {
	
	protected interface ClickableCell<T,C> extends HasCell<T,C> {
		public Cell<C> getCell(T result); 
	}
	public static final int MAX_COLUMNS = 5;

	/**
	 * Handler for the Favorites star
	 */
	StarToggleHandler<D> starHandler;

	Map<D,P> providers = new HashMap<D,P>();
	
	protected P rootDataProvider = null;
	protected final Cell<D> basicCell;
	protected final DefaultSelectionEventManager<D> selectionManager =
			DefaultSelectionEventManager.createCheckboxManager();
	protected final SelectionModel<D> selectionModel;

	protected ClientFactory clientFactory;

	public PagingCellTree(
			final SelectionModel<D> selectionModel, 
			ClientFactory cf) {

		this.selectionModel = selectionModel;
		clientFactory = cf;

		basicCell = createBasicCells();
	}

	abstract public void addFavorite(D item);
	
	public void addFavoriteClickHandler(StarToggleHandler<D> handler) {
		starHandler = handler;
	}

	protected abstract Cell<D> createBasicCells();

	public abstract P createProvider(D md);

	public P getDefaultProvider() {
		return getProvider();
	}
	
	@Override
	public abstract <T> NodeInfo<?> getNodeInfo(T value);

	public P getOrReuseProvider(D md) {
		P ret = getProvider(md);

		if (ret == null) {
			P provider = createProvider(md);
			providers.put(md, provider);
		}

		return getProvider(md);
	}

	public P getProvider() {
		return rootDataProvider;
	}

	public P getProvider(D md) {
		if (md == null)
			return rootDataProvider;
		else
			return providers.get(md);
	}

	/**
	 * Save and clear the cursor.  Needed to force a render/ update.
	 */
	public Set<D> getSelected() {
		Set<D> selected;
		if (selectionModel instanceof SingleSelectionModel) {
			selected = ((SingleSelectionModel<D>)selectionModel).getSelectedSet();
			((SingleSelectionModel<D>)selectionModel).clear();
		} else {
			selected = ((MultiSelectionModel<D>)selectionModel).getSelectedSet();
			((MultiSelectionModel<D>)selectionModel).clear();
		}

		return selected;
	}

	public SelectionModel<D> getSelectionModel() {
		return selectionModel;
	}

	/**
	 * Checks if the item is on its first page, according to its presenter
	 */
	@Override
	public boolean isFirstPage(D collectionItem) {
		return getProvider(collectionItem) == null ||
				getProvider(collectionItem).isFirstPage(collectionItem);
	}

	/**
	 * Checks if the item is on its last page, according to its presenter
	 */
	@Override
	public boolean isLastPage(D collectionItem) {
		return getProvider(collectionItem) == null ||
				getProvider(collectionItem).isLastPage(collectionItem);
	}

	/**
	 * If the item has a provider, pages it forward
	 */
	@Override
	public void nextPage(D collectionItem) {
		if (getProvider(collectionItem) != null) {
			getProvider(collectionItem).nextPage(collectionItem);
		} else
			GWT.log("Next page called but no provider");
		//		getProvider(null).refresh();
	}
	
	public void onItemRemoved(D md) {
		Set<D> selected = getSelected();

		providers.remove(md);
		selected.remove(md);

		restoreSelected(selected);
	}
	
	/**
	 * If the item has a provider, pages it back
	 */
	@Override
	public void prevPage(D collectionItem) {
		if (getProvider(collectionItem) != null) {
			getProvider(collectionItem).prevPage(collectionItem);
		} else
			GWT.log("Prev page called but no provider");
		//		getProvider(null).refresh();
	}

	/**
	 * Redraw from the root
	 */
	@Override
	public void refresh(D rootItem) {
		if (getProvider(rootItem) != null)
			getProvider(rootItem).refresh(rootItem);
	}

	@Override
	public void refreshList(List<? extends D> newContent) {
		getProvider(null).refreshList(newContent);
	}

	abstract public void removeFavorite(D item);

	/**
	 * Restore saved cursor selections
	 */
	public void restoreSelected(Collection<D> sel) {
		for (D item: sel)
			selectionModel.setSelected(item, true);
	}
	
	abstract public void updateFeedback(D item);

	@Override
	public void toggleStar(D collectionItem) {
		boolean was = selectionModel.isSelected(collectionItem);
		selectionModel.setSelected(collectionItem, false);
		boolean hasStar = isStarred(collectionItem);

		starHandler.onToggle(collectionItem, !hasStar);

		if (hasStar)
			removeFavorite(collectionItem);
		else
			addFavorite(collectionItem);
		if (!was)
			selectionModel.setSelected(collectionItem, true);
	}

	@Override
	public void toggleRating(D collectionItem) {
		boolean was = selectionModel.isSelected(collectionItem);
		selectionModel.setSelected(collectionItem, false);
		
		updateFeedback(collectionItem);
		if (!was)
			selectionModel.setSelected(collectionItem, true);
	}
}
