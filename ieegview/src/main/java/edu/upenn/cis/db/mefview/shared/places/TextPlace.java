/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import edu.upenn.cis.db.mefview.client.plugins.texteditor.TextEditorPanel;
import edu.upenn.cis.db.mefview.shared.FileInfo;

public class TextPlace extends WindowPlace {
	private FileInfo file;

	private int line;
	private int col;
	
	public TextPlace() {
		this(new FileInfo(null, null, null, null, null), 0, 0);
	}
	
	public TextPlace(FileInfo file) {
		this(file, 0, 0);
	}
	
	public TextPlace(
			FileInfo file,
			int line,
			int col) {
		super(file.getStudy());
		this.file = file;
		this.line = line;
		this.col = col;
	}
	
	public FileInfo getFile() {
		return file;
	}

	@Override
	public String getType() {
		return TextEditorPanel.NAME;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + (file.getDetails() == null ? 0 : file.getDetails().hashCode());
		result = prime * result + line;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof TextPlace)) {
			return false;
		}
		TextPlace other = (TextPlace) obj;
		if (col != other.col) {
			return false;
		}
		if (file.getDetails() == null) {
			if (other.file.getDetails() != null) {
				return false;
			}
		} else if (!file.getDetails().equals(other.file.getDetails())) {
			return false;
		}
		if (line != other.line) {
			return false;
		}
		return true;
	}
	
	
}
