/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;

@GwtIncompatible("URL, ObjectMapper, and others")
public class GithubProject {
  private String name;
  private String owner;
  private URL html_URL;
  private String description;
  private Set<GithubRelease> releases;
  private GithubRelease latestRelease;
  
  static final String DEFAULT_OWNER = "ieeg-portal";
  static final String API_URL = "api.github.com";

  /**
   * Class constructor using default owner for project.
   * 
   * @param reposName Name of the repository in Github.
   * @throws IOException
   */
  public GithubProject(String reposName) throws IOException{
    owner = DEFAULT_OWNER;
    name = reposName;
    String fullName = "/repos/"+ owner + "/" + name;
    
    URL projectUrl = new URL("https", API_URL, fullName);
    populateObj(projectUrl);
  }
  
  /**
   * Class constructor using arbitrary owner for project
   * 
   * @param ownerStr Name of the owner of the repository on Github.
   * @param reposName Name of the repository in Github.
   * @throws IOException
   */
  public GithubProject(String ownerStr, String reposName) throws IOException{
    owner = ownerStr;
    name = reposName;
    String fullName = "/repos/"+ owner + "/" + name;
    
    URL projectUrl = new URL("https", API_URL, fullName);
    populateObj(projectUrl);
  }
  
  private void populateObj(URL projectUrl) throws IOException{
    // Open connection and retrieve JSON info for Project
    URLConnection uc = projectUrl.openConnection();
    BufferedReader in = new BufferedReader(new InputStreamReader(
        uc.getInputStream()));
    String retText = in.readLine();
    in.close();
    
    ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();
    @SuppressWarnings("unchecked")
    Map<String, Object> map = mapper.readValue(retText, Map.class);
    
    html_URL = new URL((String) map.get("html_url"));
    description = (String) map.get("description");
    
    // Get release information
    String ReleaseFullName = "/repos/"+ owner + "/" + name + "/releases";    
    URL releaseUrl = new URL("https", API_URL, ReleaseFullName);
    releases = GithubRelease.fromURL(releaseUrl);
    
    // Find latest release based on ID number
    if (releases != null) {
      int id = 0;
      for (GithubRelease rel: releases){
        if(rel.getId() > id){
          this.latestRelease = rel;
          id = rel.getId();
        }
      }
    }
  }
  
  public URL getLatestZipLink() {
    if(this.latestRelease==null){
      return null;
    }
    return latestRelease.getZipball_url();
  }
  
  public URL getLatestTarLink(){
    if(this.latestRelease==null){
      return null;
    }
    return latestRelease.getTarball_url();
  }
  
  public String getOwner() {
    return owner;
  }

  public URL getHtml_URL() {
    return html_URL;
  }

  public String getDescription() {
    return description;
  }

  public GithubRelease getLatestRelease() {
    return latestRelease;
  }

  public String getName(){
    return name;
  }
    
  public Set<GithubRelease> getReleases(){
    return this.releases;
  }
  
  public String toString(){
    return this.name + " - " + this.html_URL;
  }
  
  

}
