/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;

public class ClosedPanelEvent extends GwtEvent<ClosedPanelEvent.Handler> {
	private static final Type<ClosedPanelEvent.Handler> TYPE = new Type<ClosedPanelEvent.Handler>();
	
	PanelDescriptor descriptor;
	AstroPanel pane;
	String name;
	
	public ClosedPanelEvent(PanelDescriptor desc, AstroPanel pane) {
		descriptor = desc;
		this.pane = pane;
		this.name = pane.getPanelType();
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<ClosedPanelEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ClosedPanelEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ClosedPanelEvent.Handler handler) {
		handler.onClosedPanel(name, descriptor, pane);
	}

	public interface Handler extends EventHandler {
		void onClosedPanel(String panelType, PanelDescriptor desc, AstroPanel pane);
	}
}
