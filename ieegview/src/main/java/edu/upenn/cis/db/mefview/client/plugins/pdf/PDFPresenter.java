/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.pdf;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.user.client.ui.Frame;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class PDFPresenter extends BasicPresenter {
	AppModel control;
//	ClientFactory clientFactory;
//	HandlerRegistration hr;
	DataSnapshotModel model;
	
	public interface Display extends BasicPresenter.Display {
		public Frame getPDFFrame();
		public WindowPlace getPlace();
	}
	
	public final static String NAME = "pdf";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.PDF);
	final static PDFPresenter seed = new PDFPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public PDFPresenter() {
		super(null, NAME, TYPES);
	}

	public PDFPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory
			) {

		super(clientFactory, NAME, TYPES);
//		super(model, user, clientFactory.getControlPane().getMainPaneWidth(), 
//				clientFactory.getControlPane().getLeftPaneHeight());//, width, height);
		this.control = control;
//		this.clientFactory = clientFactory;
		this.model = model;
	  }

	@Override
	public PDFPresenter.Display getDisplay() {
		return (PDFPresenter.Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);

		if (getDisplay().getPlace() != null && getDisplay().getPlace() instanceof URLPlace) {
			URLPlace pl = (URLPlace)getDisplay().getPlace();
			if (pl.getDocument() != null) {
				openURL(pl.getDocument(), null, getDisplay().getPDFFrame());
			}
		}
		getDisplay().bindDomEvents();
	}

	public void openURL(String url, String snapshot, Frame parent) {
		OpenSignedUrl action = new OpenSignedUrl("", url, snapshot, "&disposition=inline", parent);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}
	
	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata project, boolean writePermissions) {
		return new PDFPresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory);
	}
}
