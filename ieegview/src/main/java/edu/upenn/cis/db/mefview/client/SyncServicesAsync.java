package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.braintrust.shared.WorkerResponse;
import edu.upenn.cis.db.mefview.shared.*;

public interface SyncServicesAsync {

	void getDropBoxContent(SessionToken token, String dropBoxToken, String path,
			AsyncCallback<Set<? extends IStoredObjectReference>> callback);

//	void pollChannel(SessionToken token, String channel, AsyncCallback<WorkerResponse> callback);

	void isCommunicationActive(SessionToken token, AsyncCallback<Boolean> callback);

	void getSyncClients(SessionToken token, AsyncCallback<Set<SyncClientInfo>> callback);

	void getSyncMetadata(SessionToken token, String syncClient, String path,
			AsyncCallback<List<PresentableMetadata>> callback);

	void requestSync(SessionToken token, String syncClient, String path,
			AsyncCallback<SyncClientRequestStatus> callback);

	void requestIndex(SessionToken token, String syncClient, String path,
			AsyncCallback<SyncClientRequestStatus> callback);

	void requestSyncMetadata(SessionToken token, String syncClient, String path, AsyncCallback<Void> callback);

	void requestSyncClients(SessionToken token, AsyncCallback<Void> callback);

	void sendRequest(SessionToken token, String channel, WorkerRequest request, AsyncCallback<String> callback);

	void getSyncResponses(SessionToken token, AsyncCallback<Map<String, Set<WorkerResponse>>> callback);

	void getCurrentUser(AsyncCallback<String> callback);

}
