/*******************************************************************************
 * Copyright 2015 Zachary G. Ives
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.thirdparty.orion;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.ui.HTML;

import edu.upenn.cis.db.mefview.client.JavascriptFactory;

/**
 * GWT interface over Eclipse Orion text editor
 * 
 * @author zives
 *
 */
public class Orion {

	public interface Resources extends ClientBundle {
		@Source("built-editor.js")
		TextResource orion();
	}

	private static boolean installed = false;
	
	private static boolean isReadOnly = false;

	private static final Resources RESOURCES = GWT.create(Orion.Resources.class);

	public static synchronized void install() {
		if (!installed) {
			ScriptElement e = Document.get().createScriptElement();

			e.setText(RESOURCES.orion().getText());
			Document.get().getBody().appendChild(e);

			
			installed = true;
		}
	}
	
	/**
	 * Sets the editor "buffer" to the contents of the file
	 * 
	 * @param id
	 * @param fileContent
	 */
	public static void setFileContent(String id, String fileContent) {
		Element e = Orion.getEditorBufferParent(id);
		e.appendChild(Document.get().createTextNode(fileContent));
		
		ScriptElement script2 = Document.get().createScriptElement();
		script2.setInnerText("require([\"orion/editor/edit\"], function(edit) {"
				+
				"edit({className: \"editor\", " 
				+ "parent: \""
				+ id
				+"\"});" +
				"});");
		Element head = Document.get().getElementsByTagName("head")
				.getItem(0);
		head.appendChild(script2);
		//The script did its work. We can remove it to avoid a buildup when files are closed and re-opened.
		head.removeChild(script2);
	}
	
	/**
	 * Creates the container for the editor
	 * 
	 * @param id
	 * @param containerClassName
	 * @return
	 */
	public static HTML createContainer(String id, String containerClassName, boolean readonly) {
		isReadOnly = readonly;
		if (readonly)
			return new HTML("<pre id=\"" + id + "\" class=\"" + containerClassName +
					"\" fullSelection=\"true\" title=\"View text\" " +
					"data-editor-readonly=\""+ readonly + "\" style=\"height:100%\"></pre>");
		else			
			return new HTML("<pre id=\"" + id + "\" class=\"" + containerClassName +
					"\" fullSelection=\"true\" title=\"Edit text\" " +
					"data-editor-readonly=\""+ readonly + "\" style=\"height:100%\"></pre>");
	}
	
	public static boolean isReadOnly() {
		return isReadOnly;
		
	}
	
	public static void setReadOnly(boolean ro) {
		isReadOnly = ro;
	}
	
	/**
	 * Get the PRE element by its ID.  We can set its content as a text node.
	 * 
	 * @param id
	 * @return
	 */
	public static Element getEditorBufferParent(String id) {
		return Document.get().getElementById(id);//.getFirstChildElement();
	}

	/**
	 * Prevent construction
	 */
	private Orion() {
		super();
	}

}
