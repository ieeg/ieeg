/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.controls;


import org.gwt.advanced.client.datamodel.ComboBoxDataModel;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.controls.TimeScaleComboBox;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.controls.VoltageScaleComboBox;
import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.DoubleBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;

public class EEGToolbar extends FlowPanel implements IEEGToolbar {
	TextBox detectBox;
	ComboBoxWithEnter<ComboBoxDataModel> scaleBox;
	ComboBoxWithEnter<ComboBoxDataModel> vScaleBox;
	DoubleBoxWithEnter posBox;
	CheckBox autoSize;
	Widget refresh;
	IEEGPane pane;
	
	Widget layerButton;
	Widget showAll;
	Widget filters;
	Widget remontage;
	Widget invertSignal;
	MenuBar menu = new MenuBar(true);
	
	public EEGToolbar() {
		setStyleName("IeegTabToolbar");

		layerButton = WidgetFactory.get().createButton("Layers");
		add(layerButton);
		
		showAll = WidgetFactory.get().createButton("Channels");
		add(showAll);

		filters = WidgetFactory.get().createButton("Filters");
		add(filters);

		remontage = WidgetFactory.get().createButton("Montage");
		add(remontage);
		
		invertSignal = WidgetFactory.get().createButton("Invert signal");
		add(invertSignal);
		
	}
	
	@Override
	public void init(ClientFactory factory, DataSnapshotModel dataSnapshotModel, IEEGPane pane) {
		
		this.pane = pane;
		
		refresh = WidgetFactory.get().createIconButton("autorenew");//ResourceFactory.getRefresh().getSafeUri().toString());
	    add(refresh);

		Label vScaleLabel = new Label("Gain (uV/mm): ");
		add(vScaleLabel);
		
		vScaleBox = new VoltageScaleComboBox();//ComboBoxWithEnter<ComboBoxDataModel>();
//		vScaleBox.select(10);
        add(vScaleBox);
			
		Label posLabel = new Label("Start (s):");
		posBox = new DoubleBoxWithEnter();
		posBox.setStyleName("Toolbar_Single_Input");
		posBox.setVisibleLength(8);
		add(posLabel);
		add(posBox);
		
		Label scaleLabel = new Label("Width (s): ");
		scaleBox = new TimeScaleComboBox();//ComboBoxWithEnter<ComboBoxDataModel>();
		add(scaleLabel);
		add(scaleBox);

	}
	
	
	@Override
	public MenuItem addMenuOption(String legend, Command cmd) {
		MenuItem test = new MenuItem(legend, cmd);
		menu.addItem(test);
		return test;
	}

	@Override
	public MenuItem addMenuOptionHtml(String legend, Command cmd) {
		MenuItem test = new MenuItem(legend, true, cmd);
		menu.addItem(test);
		return test;
	}

	@Override
	public MenuItem addSubMenu(String legend, MenuBar menuI) {
		MenuItem test = new MenuItem(legend, menuI);
		menu.addItem(test);
		return test;
	}
	
	@Override
	public void addMenuSpacer() {
		menu.addSeparator();
	}
	
	@Override
	public void setRefreshHandler(ClickHandler handler) {
		((HasClickHandlers)refresh).addClickHandler(handler);
	}


	@Override
	public TextBox getDetectBox() {
		return detectBox;
	}


	@Override
	public ComboBoxWithEnter<ComboBoxDataModel> getTimeIntervalBox() {
		return scaleBox;
	}


	@Override
	public ComboBoxWithEnter<ComboBoxDataModel> getVoltageScaleBox() {
		return vScaleBox;
	}


	@Override
	public DoubleBoxWithEnter getPosBox() {
		return posBox;
	}
	
	
//	public SelectMontage getMontagePopup() {
//	  return montagePopup;
//	}

    public Widget getRemontage() {
      return remontage;
  }
	
	@Override
	public void bindDomEvents() {
	}

	@Override
	public void setExecHandler(ClickHandler exec) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setClickHandlers(ClickHandler layerHandler,
			ClickHandler traceHandler, ClickHandler filterHandler,
			ClickHandler montageHandler, NumericEnterHandler scaleHandler) {
		
		((HasClickHandlers)layerButton).addClickHandler(layerHandler);
		((HasClickHandlers)showAll).addClickHandler(traceHandler);
		((HasClickHandlers)filters).addClickHandler(filterHandler);
		((HasClickHandlers)remontage).addClickHandler(montageHandler);
		vScaleBox.setEnterHandler(scaleHandler);

	}

	@Override
	public void setCloseHandler(ClickHandler exec) {
		// TODO Auto-generated method stub
		
	}


	public void setInvertHandler(ClickHandler invertHandler) {
		((HasClickHandlers)invertSignal).addClickHandler(invertHandler);
	}

	@Override
	public HasText getInvertButton() {
		return (HasText) invertSignal;
	}
	
}
