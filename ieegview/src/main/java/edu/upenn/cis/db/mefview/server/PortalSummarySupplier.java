/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Supplier;

import edu.upenn.cis.db.mefview.shared.PortalSummary;

class PortalSummarySupplier implements
		Supplier<PortalSummary> {

	private final HabitatUserService uService;

	public PortalSummarySupplier(HabitatUserService uService) {
		this.uService = checkNotNull(uService);
	}

	@Override
	public PortalSummary get() {
		final int numSnapshots = (int) uService
				.getDataSnapshotAccessSession()
				.countSnapshots();
		final int numAnnotations = (int) uService
				.getDataSnapshotAccessSession()
				.countAnnotations();
		final int numWorldReadableExps = (int) uService
				.getDataSnapshotAccessSession()
				.countWorldReadableExperiments();
		final int numUserReadableExps = (int) uService
				.getDataSnapshotAccessSession()
				.countUserReadableExperiments();
		final int numWorldReadableStudies = (int) uService
				.getDataSnapshotAccessSession().countWorldReadableStudies();
		final int numUserReadableStudies = (int) uService
				.getDataSnapshotAccessSession().countUserReadableStudies();
		final int numUsers = (int) uService.getUserCount();
		return new PortalSummary(
				numSnapshots,
				numAnnotations,
				numUserReadableExps,
				numUserReadableStudies,
				numWorldReadableExps,
				numWorldReadableStudies,
				numUsers);

	}

}