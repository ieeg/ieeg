/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.CommunityPostEvent;
import edu.upenn.cis.db.mefview.client.events.UserInfoReceivedEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class UserProfilePresenter extends BasicPresenter 
	implements UserInfoReceivedEvent.Handler,
	CommunityPostEvent.Handler {
	public interface Display extends BasicPresenter.Display {
		void setUserInfo(UserInfo info);		
		RecentActivitiesPresenter.Display getRecentActivitiesPane();
		UsageStatsPresenter.Display getUsageStatsPane();
		PortalStatsPresenter.Display getPortalStatsPane();
		QuickLinksPresenter.Display getQuickLinksPane();
		
	}
	
	RecentActivitiesPresenter activitiesPresenter;
	RecommendationsPresenter recommendationsPresenter;
	UsageStatsPresenter usageStatsPresenter;
	PortalStatsPresenter portalStatsPresenter;
	QuickLinksPresenter quickLinksPresenter;

	com.google.web.bindery.event.shared.HandlerRegistration last1;
	com.google.web.bindery.event.shared.HandlerRegistration last2;
	
//	Display display;
	
	public static class StampedEvent implements Comparable<StampedEvent> {
		public Long time;
		public String event;
		public int ttl;
		
		public StampedEvent(Long time, String event, boolean isLocal) {
			this.time = time;
			this.event = event;
			if (isLocal)
				ttl = 1;
			else
				ttl = -1;
		}
		
		@Override
		public int compareTo(StampedEvent arg0) {
			return -time.compareTo(arg0.time);
		}
	}
	
	public final static String NAME = "user-profile";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.USER);
	final static UserProfilePresenter seed = new UserProfilePresenter();
	
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, false));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UserProfilePresenter() { super(null, NAME, TYPES); }
	
	public UserProfilePresenter(ClientFactory clientFactory
			) {
		super(clientFactory, NAME, TYPES);
	}
	
	
	@Override
	public void onUserInfoReceived(UserInfo info) {
//		GWT.log("User profile received");
		getDisplay().setUserInfo(info);
	}

	@Override
	public void onMessagePosted(String message, String userId, String followCode, Timestamp time,
			boolean isLocal) {
//		GWT.log("Received post event for " + userId + " vs " + clientFactory.getUserId());
		
		if (activitiesPresenter != null) {
//			GWT.log("Posted" + ": " + message);
			activitiesPresenter.addEvent(message, userId, followCode, time, isLocal);
		}
	}
	
	public void unbind() {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
		activitiesPresenter.unbind();
		quickLinksPresenter.unbind();
		portalStatsPresenter.unbind();
		usageStatsPresenter.unbind();
	}

	@Override
	public Presenter create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		return new UserProfilePresenter(clientFactory);
	}

	@Override
	public Display getDisplay() {
		return (Display)display;
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		getDisplay().addPresenter(this);
		GWT.log("Binding user profile presenter");

		activitiesPresenter = new RecentActivitiesPresenter(getClientFactory());
		activitiesPresenter.setDisplay(getDisplay().getRecentActivitiesPane());
		activitiesPresenter.bind(selected);

		quickLinksPresenter = new QuickLinksPresenter(getClientFactory());
		quickLinksPresenter.setDisplay(getDisplay().getQuickLinksPane());
		quickLinksPresenter.bind(selected);
		
		portalStatsPresenter = new PortalStatsPresenter(getClientFactory());
		portalStatsPresenter.setDisplay(getDisplay().getPortalStatsPane());
		portalStatsPresenter.bind(selected);
		
		usageStatsPresenter = new UsageStatsPresenter(getClientFactory());
		usageStatsPresenter.setDisplay(getDisplay().getUsageStatsPane());
		usageStatsPresenter.bind(selected);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(UserInfoReceivedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(CommunityPostEvent.getType(), this);

		
		UserInfo info = getClientFactory().getSessionModel().getUserInfo();
		if (info != null)
			getDisplay().setUserInfo(info);
		
		getDisplay().bindDomEvents();
	}
}
