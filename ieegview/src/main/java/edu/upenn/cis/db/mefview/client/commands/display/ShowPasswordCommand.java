package edu.upenn.cis.db.mefview.client.commands.display;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.PopupPanel;

import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset.PasswordResetPanel;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class ShowPasswordCommand implements Command {
	PresentableMetadata searchResult;
	PopupPanel source;
	
	public ShowPasswordCommand(PresentableMetadata md,
			PopupPanel source) {
		this.searchResult = md;
		this.source = source;
	}
	
	public void execute() {
		if (source != null)
			source.hide();
		Window.alert("Sorry: Not yet implemented");
//		IeegEventBusFactory.getGlobalEventBus().fireEvent(
//				new OpenDisplayPanelEvent(new OpenDisplayPanel( 
//				searchResult, false, 
//				PasswordResetPanel.NAME, null)));
	}
}
