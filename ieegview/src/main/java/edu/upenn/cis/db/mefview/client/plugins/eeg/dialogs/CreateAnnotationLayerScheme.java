
/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;


import java.util.Map;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.ComboBox;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.AnnotationSchemeCreatedEvent;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class CreateAnnotationLayerScheme extends DialogBox {
  ListBox datasetList = new ListBox();
  Button ok = new Button("OK");
  Button cancel = new Button("Cancel");
  TextBox schemeName = new TextBox();
  TextBox schemeColor = new TextBox();
//  ClientFactory clientFactory;
  SessionModel sessionModel;
  AnnotationScheme newScheme;

  public CreateAnnotationLayerScheme(
      final DataSnapshotModel currentModel,
      SessionModel theSessionModel
      ) {
	  
	this.sessionModel = theSessionModel;
//    clientFactory = factory;
    setStyleName("ChanSelectDialog");

    // Get new default Scheme for potential new layer
    int nrGroups = currentModel.getAnnotationModel().getRootGroup().getSubGroups().size(); 

    Map<String, AnnotationScheme> curScheme = sessionModel.getUserPreferences().getAnnSchemes();
    
    // Find first non-existing default name
    int i=1;
    while (curScheme.containsKey("Scheme " +i)) 
      i++;
    
    newScheme = AnnotationScheme.getDefaultScheme(i-1);
    schemeName.setText(newScheme.getName());
    schemeColor.setText(newScheme.getColor());

    VerticalPanel vp = new VerticalPanel();

    setTitle("Create New Style");
    setText("Create New Style");
    setWidget(vp);

    vp.add(new Label("Label: "));
    vp.add(schemeName);
    
    vp.add(new Label("Color: "));
    vp.add(schemeColor);


    HorizontalPanel hp = new HorizontalPanel();
    hp.add(ok);
    hp.add(cancel);
    vp.add(hp);
    vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

    ok.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        hide();

        sessionModel.getUserPreferences().addAnnotationScheme(newScheme);
        IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationSchemeCreatedEvent(currentModel, null));

      }

    });

    cancel.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        hide();
      }

    });
  }


  public String getTargetRevId() {
    return datasetList.getValue(datasetList.getSelectedIndex());
  }

  public String getTargetLabel() {
    return datasetList.getItemText(datasetList.getSelectedIndex());
  }
}
