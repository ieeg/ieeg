/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

/**
 * This is a generic template for a presenter
 * 
 * @author zives
 *
 */
public class CreateAccountPresenter extends BasicPresenter {
	AppModel control;
	DataSnapshotModel model;

	/**
	 * Account signup info display
	 */
	public interface Display extends BasicPresenter.Display {
		public void addClickHandlerToButton(ClickHandler handler);
		
		public WindowPlace getPlace();

		void setHandler(ClickHandler buttonHandler);

		String getNewPassword();

		String getNewPassword2();

		void showIllegal();

		void showMismatch();

		void showBadPassword();

		void showChangedPassword();

		void initState(ServerConfiguration config);
		
		UserAccountInfo getAccountInfo();

		public void showErrorCreatingAccount();

		public void showCreatedAccount();

		boolean isInAgreement();

		public void showNoAgreement();

		public void setAccountInfo(UserAccountInfo result);
	}
	
	public final static String NAME = "CreateAccount";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.PDF);
	final static CreateAccountPresenter seed = new CreateAccountPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public CreateAccountPresenter() { super(null, NAME, TYPES); }

	public CreateAccountPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory
			) {
		super(clientFactory, NAME, TYPES);
		this.control = control;
//		this.clientFactory = clientFactory;
		this.model = model;
	  }
	
	
	@Override
	public Display getDisplay() {
		return (Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);
		
		getDisplay().initState(getSessionModel().getServerConfiguration());

		if (getSessionModel().isRegisteredUser()) {
			getClientFactory().getPortal().getUserAccountInfo(getSessionModel().getSessionID(), 
					getSessionModel().getUserId(), new AsyncCallback<UserAccountInfo>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Unable to contact server for account details");
				}

				@Override
				public void onSuccess(UserAccountInfo result) {
					getDisplay().setAccountInfo(result);
					getDisplay().setHandler(new CreateAccountHandler(getClientFactory(), getDisplay()));
				}

			});

		} else
			// Create account handler
			getDisplay().setHandler(new CreateAccountHandler(getClientFactory(), getDisplay()));
		
		getDisplay().bindDomEvents();
	}

	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata project, boolean writePermissions) {
		return new CreateAccountPresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory);
	}
	
}
