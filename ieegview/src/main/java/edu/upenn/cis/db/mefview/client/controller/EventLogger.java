/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.controller;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IEventBus;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.AnnotationEditedEvent;
import edu.upenn.cis.db.mefview.client.events.CommunityPostEvent;
import edu.upenn.cis.db.mefview.client.events.DiscussionPostedEvent;
import edu.upenn.cis.db.mefview.client.events.DiscussionRepliedEvent;
import edu.upenn.cis.db.mefview.client.events.JobStatusChangedEvent;
import edu.upenn.cis.db.mefview.client.events.LaunchedToolEvent;
import edu.upenn.cis.db.mefview.client.events.OpenedPanelEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.SavedSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolMarkedAsFavoriteEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.UserInfoReceivedEvent;
import edu.upenn.cis.db.mefview.client.messages.SavedSnapshotMessage;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.shared.Annotation;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.LogEntry;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.RefreshList;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;

public class EventLogger 
implements IEventLogger,
		   AnnotationEditedEvent.Handler,
		   ProjectCreatedEvent.Handler,
		   DiscussionPostedEvent.Handler,
		   DiscussionRepliedEvent.Handler,
		   JobStatusChangedEvent.Handler,
		   LaunchedToolEvent.Handler,
		   OpenedPanelEvent.Handler,
//		   ImageOpenedEvent.Handler,
		   ProjectOpenedEvent.Handler,
		   SnapshotOpenedEvent.Handler, 
		   SavedSnapshotEvent.Handler,
		   ToolMarkedAsFavoriteEvent.Handler,
		   ToolUpdatedEvent.Handler,
		   ToolResultCreatedEvent.Handler,		
		   //ToolResultsUpdatedEvent.Handler,			// Annotations changed
		   UserInfoReceivedEvent.Handler
{
	protected ClientFactory clientFactory;
//	private String username = "(unknown)";
	protected Timer poller;
	protected Timestamp lastPoll = new Timestamp(new java.util.Date().getTime());
	
	
	public EventLogger() {
	}
	
	public void initialize(ClientFactory factory) {
		clientFactory = factory;
		
		IEventBus bus = IeegEventBusFactory.getGlobalEventBus();
		
		//clientFactory.getEventBus().addHandler(AnnotationCreatedEvent.getType(), this);
		bus.registerHandler(ProjectCreatedEvent.getType(), this);
		bus.registerHandler(DiscussionPostedEvent.getType(), this);
		bus.registerHandler(DiscussionRepliedEvent.getType(), this);
		bus.registerHandler(JobStatusChangedEvent.getType(), this);
		bus.registerHandler(LaunchedToolEvent.getType(), this);
		bus.registerHandler(ProjectOpenedEvent.getType(), this);
		bus.registerHandler(SnapshotOpenedEvent.getType(), this);
		bus.registerHandler(SavedSnapshotEvent.getType(), this);
		bus.registerHandler(ToolUpdatedEvent.getType(), this);
		bus.registerHandler(ToolResultCreatedEvent.getType(), this);
		bus.registerHandler(UserInfoReceivedEvent.getType(), this);
		
		registerMore();
		
		poller = new Timer() {
	
			@Override
			public void run() {
				pollEventsOnServer();
			}
			
		};
		
		final SessionModel sessionModel = clientFactory.getSessionModel();

		pollEventsOnServer();
		//Don't poll for guest
//		if (sessionModel.isRegisteredUser()) {
			clientFactory.getPortal().getLogEntries(sessionModel.getSessionID(), 0, 50, 
					new SecFailureAsyncCallback.SecAlertCallback<List<LogEntry>>(clientFactory){
		
				@Override
				public void onSuccess(List<LogEntry> arg0) {
//						GWT.log("Polled server and got " + arg0.size() + " events");
					lastPoll = new Timestamp(new java.util.Date().getTime());
					poller.schedule(10000);
		
					List<LogEntry> lst = arg0;
					Collections.sort(lst);
					for (LogEntry entry: lst) {
						String sender = entry.getSender();
						String recipient = entry.getRecipient();
		//					GWT.log("Log entry for '" + recipient + "' vs '" + clientFactory.getUserId() + "'");
						if (recipient == null || recipient.isEmpty() || 
								recipient.equals(sessionModel.getUserId())) {
//								GWT.log("Broadcasting log entry");
							IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
									CommunityPostEvent(entry.getMessage(), sender, "", entry.getTime(), false));
						}
					}
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
							CommunityPostEvent("logged in to " + clientFactory.getSessionModel().getServerConfiguration().getServerName()
									+ "!", clientFactory.getSessionModel().getUserId(), 
									"", new Timestamp(new Date().getTime()), false));
				}
				
			});
//		}
	}
	
	private void handleEntries(RefreshList arg0) {
		
	}
	
	private void pollEventsOnServer() {
		clientFactory.getPortal().pollForUpdates(getSessionModel().getSessionID(), lastPoll, 
				new SecFailureAsyncCallback.SecAlertCallback<RefreshList>(clientFactory) {

			@Override
			public void onSuccess(RefreshList arg0) {
//				GWT.log("Polled server and got " + arg0.getNewEvents().size() + " events");
				List<LogEntry> lst = arg0.getNewEvents();
				Collections.sort(lst);
				for (LogEntry entry: lst) {
					String sender = entry.getSender();
					String recipient = entry.getRecipient(); 
					if (recipient == null || recipient.isEmpty() || 
							recipient.equals(getSessionModel().getUserId())) {
						GWT.log("FIRE message " + entry.getMessage());
						IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
								CommunityPostEvent(entry.getMessage(), sender, "", entry.getTime(), false));
					}
				}
				lastPoll = new Timestamp(new java.util.Date().getTime());
				poller.schedule(10000);

			}
			
		});
	}
	
	public void logProvenance(ProvenanceLogEntry.UsageType usage, String snapshot,
			String derivedSnapshot, String annotation) {
		Timestamp time = new Timestamp(new java.util.Date().getTime());
		
		ProvenanceLogEntry op = new ProvenanceLogEntry(getSessionModel().getUserId(),
				usage, 
				snapshot, 
				time,
				derivedSnapshot, annotation);
		
		clientFactory.getPortal().addProvenanceRecord( 
				getSessionModel().getSessionID(), op, 
				new SecFailureAsyncCallback.SecAlertCallback<Void>(clientFactory) {

					@Override
					public void onSuccess(Void arg0) {
						// TODO Auto-generated method stub
						
					}
			
		});
	}

	public void logToServer(String recipient, String msg) {
		logToServer(recipient, msg, true, false);
	}
	
	protected void logToServer(String recipient, String msg, boolean post, boolean meToo) {
		if (!getSessionModel().isRegisteredUser()) {
			return;
		}
		LogEntry entry = new LogEntry();
		Timestamp t = new Timestamp(new java.util.Date().getTime());
		
		entry.setSender(getSessionModel().getUserId());
		entry.setTime(t);
		entry.setMessage(msg);

		if (recipient != null)
			entry.setRecipient(recipient);
		else
			entry.setRecipient("");
		
//		clientFactory.getMainLogger().info("TO: " + recipient + " -- " + username + msg);
		
		String sender = entry.getSender();
		if (sender.equals(getSessionModel().getUserId()))
			sender = "You";
		
		
		if (recipient == null || recipient.isEmpty() || 
				recipient.equals(getSessionModel().getUserId())) {
			
			if (post)
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
						CommunityPostEvent(msg, sender, "", t, true));
			entry.setRecipient(getSessionModel().getUserId());
			
			// Notify sender + recipient
		} else if (meToo && post)
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new 
					CommunityPostEvent(msg, sender, "", t, true));
		
		clientFactory.getPortal().addLogEntry( 
				getSessionModel().getSessionID(), entry, 
				new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
			
		});
	}
	
	@Override
	public void onOpenedSnapshot(String snapshot, DataSnapshotModel model, SearchResult sr) {
		logToServer(model.getCreatorID(), " opened snapshot " + model.getFriendlyName());
		logProvenance(ProvenanceLogEntry.UsageType.VIEWS, model.getSnapshotID(),
				null, null);
	}
	@Override
	public void onLaunchedTool(ToolDto tool) {
		logToServer(tool.getAuthorId().get().toString(), " launched tool " + tool.getLabel() + "/" + tool.getId().get());
		
	}
	@Override
	public void onSavedSnapshot(SavedSnapshotMessage action) {
		logToServer(action.model.getCreatorID(), " saved snapshot " + action.model.getFriendlyName());
		logProvenance(ProvenanceLogEntry.UsageType.ADDSTO, action.getSnapshotID(),
				null, null);
	}

	@Override
	public void onAnnotationEdited(DataSnapshotModel model,
			AnnotationGroup<Annotation> group, Annotation old, Annotation a, boolean wasDeleted) {
		if (!wasDeleted)
			logToServer(model.getCreatorID(), " created or edited annotation of type " + a.getType() + " on channels " + a.getChannels() + " in group " + group.getName() + " for " + model.getFriendlyName());
	}

	@Override
	public void onToolResultCreated(String parentSnapshot, String friendlyName,
			String parentCreator, DerivedSnapshot result) {
		logToServer(parentCreator, " derived a new snapshot from " + friendlyName + ", called " + result.getAnalysisType());
		logProvenance(ProvenanceLogEntry.UsageType.DERIVES, parentSnapshot,
				result.getAnalysisId(), null);
	}
	@Override
	public void onChangedTool(final ToolDto tool) {
		logToServer(tool.getAuthorName(), " updated the tool " + tool.getLabel());		
	}
	@Override
	public void onJobStatusChanged(String job) {
		logToServer(null, " job status changed for " + job);		
	}

	@Override
	public void onDiscussionPosted(DataSnapshotModel model, Post post) {
		logToServer(model.getCreatorID(), " posted a message in a discussion on " + 
				model.getFriendlyName() + ": " + post.getTitle());		
		logProvenance(ProvenanceLogEntry.UsageType.DISCUSSES, model.getSnapshotID(),
				null, null);
	}

	@Override
	public void onOpenedPanel(String panelType, PanelDescriptor desc, AstroPanel pane) {
		logToServer(pane.getDataSnapshotState().getCreatorID(),
				" viewed snapshot " + pane.getDataSnapshotState().getFriendlyName());
		
	}

	@Override
	public void onUserInfoReceived(UserInfo info) {
//		username = info.getName();
	}

	@Override
	public void onToolMarkedAsFavorite(ToolDto tool) {
		logToServer(tool.getAuthorId().get().toString(), " marked your tool " + tool.getLabel() + " as a favorite");
	}

	@Override
	public void onToolUnmarkedAsFavorite(ToolDto tool) {
		logToServer(tool.getAuthorId().get().toString(), " unmarked your tool " + tool.getLabel() + " as a favorite");
	}

	@Override
	public void onCreatedProject(Project project) {
		for (UserInfo u: project.getTeam())
			logToServer(u.getName(), " created a new project " + project.getName());
	}

	@Override
	public void onDiscussionPosted(Project project, Post post) {
		for (UserInfo u: project.getTeam())
			logToServer(u.getName(), " posted a discussion on " + project.getName());
	}

	@Override
	public void onDiscussionReplied(DataSnapshotModel model, String refs,
			Post post) {
		logToServer(model.getCreatorID(), " posted a reply to a message in a discussion on " + 
				model.getFriendlyName() + ": " + post.getTitle());		
		logProvenance(ProvenanceLogEntry.UsageType.DISCUSSES, model.getSnapshotID(),
				null, null);
		
		// TODO: get the prior messages in the thread and notify all message senders!
	}

	@Override
	public void onDiscussionReplied(Project project, String refs, Post post) {
		for (UserInfo u: project.getTeam())
			logToServer(u.getName(), " posted a discussion on " + project.getName());
		
		// TODO: get the prior messages in the thread and notify all message senders!
	}

	@Override
	public void onOpenedProject(Project project) {
		for (UserInfo u: project.getTeam())
			logToServer(u.getName(), " opened the project " + project.getName());
	}

	protected SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}
	
	protected void registerMore() {
		
	}
}
