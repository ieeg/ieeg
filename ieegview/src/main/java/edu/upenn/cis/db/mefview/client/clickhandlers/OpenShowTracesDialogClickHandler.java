package edu.upenn.cis.db.mefview.client.clickhandlers;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IShowTraces;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;


public class OpenShowTracesDialogClickHandler implements ClickHandler {
	private ClientFactory clientFactory;
	private IEEGViewerPresenter presenter;
	private IShowTraces dialog;
	private boolean editHandlerSet = false;

	public static OpenShowTracesDialogClickHandler getHandler(
			ClientFactory cf, IEEGViewerPresenter presenter) {

		final OpenShowTracesDialogClickHandler theHandler = new OpenShowTracesDialogClickHandler(
				cf, presenter);

		theHandler.init();
		return theHandler;
	}

	public void init() {
	
	}

	private List<String> getChannelNames() {
		return presenter.getDisplay().getDataSnapshotController()
				.getChannelNames();
	}

	private OpenShowTracesDialogClickHandler(final ClientFactory cf,
			final IEEGViewerPresenter presenter) {
		clientFactory = cf;
		this.presenter = presenter;
		
		// Create SelectDialog
		dialog = DialogFactory
				.getShowTracesDialog(new DialogInitializer<IShowTraces>() {

					@Override
					public void init(IShowTraces objectBeingInited) {
						
						objectBeingInited.createDialog();
						
						objectBeingInited.init(new IShowTraces.Handler() {

							
							@Override
							public void updateChannels(EEGMontage montage, Boolean[] isVisible) {
								// TODO Auto-generated method stub
								
							}

							@Override
							public void channelsChanged(Set<EEGMontagePair> selPairs) {
								IEEGPane thisPane = ((IEEGViewerPanel) presenter.getDisplay()).getEEGPane();
								EEGMontage curMont = thisPane.getCurMontage();
								
								int sz = curMont.getPairs().size();
								boolean[] sel = new boolean[sz];
								int idx = 0;
								for(EEGMontagePair p: curMont.getPairs()){
									
									
									Iterator<EEGMontagePair> iter = selPairs.iterator();
									while (iter.hasNext()) {
										EEGMontagePair curInfo = iter.next();
										if (curInfo == p){
											sel[idx] = true;
											
											break;
										}
									    
									}
									idx++;	
								}

								thisPane.showThese(sel);
								
							}},
								new Command() {

									@Override
									public void execute() {
										presenter.getDisplay().setFocusOnGraph();
										
									}
								
							}
								);
						
					}

					@Override
					public void reinit(IShowTraces objectBeingInited) {
						// TODO Auto-generated method stub
						
					}

				});

	}

	public IShowTraces getDialog() {
		return dialog;
	}

	@Override
	public void onClick(ClickEvent event) {
		
		IEEGPane pane = presenter.getDisplay().getEEGPane();
		
		dialog.initTracePane(pane.getCurMontage(), pane.getCurIsVisible(),this.clientFactory);
		
		dialog.open();
		Timer t = new Timer() {
			@Override
			public void run() {
				dialog.focus();
			}
		};
		t.schedule(500);
	}

}
