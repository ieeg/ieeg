
/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Annotation;

public class AnnotationGroupRemovedEvent  extends GwtEvent<AnnotationGroupRemovedEvent.Handler> {
    private static final Type<AnnotationGroupRemovedEvent.Handler> TYPE = new Type<AnnotationGroupRemovedEvent.Handler>();
    
    private final DataSnapshotModel model;
    
    private final AnnotationGroup<Annotation> group;
    
    private final AnnotationGroup<Annotation> selectedGroup;
    
    public AnnotationGroupRemovedEvent(DataSnapshotModel model,
            final AnnotationGroup<Annotation> group,
            final AnnotationGroup<Annotation> selectedGroup){
        this.model = model;
        this.group = group;
        this.selectedGroup = selectedGroup;
    }
    
    public DataSnapshotModel getDataSnapshotState() {
        return model;
    }
    
    public AnnotationGroup<Annotation> getGroup() {
        return group;
    }
    public static com.google.gwt.event.shared.GwtEvent.Type<AnnotationGroupRemovedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<AnnotationGroupRemovedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AnnotationGroupRemovedEvent.Handler handler) {
        handler.onAnnotationGroupRemoved(model, group, selectedGroup);
    }

    public static interface Handler extends EventHandler {
        void onAnnotationGroupRemoved(DataSnapshotModel action, 
                AnnotationGroup<Annotation> group,
                AnnotationGroup<Annotation> selectedGroup);
    }
}
