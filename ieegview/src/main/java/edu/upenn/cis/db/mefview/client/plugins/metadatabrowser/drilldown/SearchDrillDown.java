/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.drilldown;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;

import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.JsonKeyValue;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.MetadataDrillDown;
import edu.upenn.cis.db.mefview.shared.MetadataPresenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SnapshotContents;


/**
 * This class is responsible for fetching on-demand the derived snapshots,
 * time series content, etc. for a SearchResult.
 * 
 * @author zives
 *
 */
public class SearchDrillDown implements MetadataDrillDown {
	final MetadataModel model;
	final ClientFactory clientFactory;


	static Map<PresentableMetadata,Long> cache = new HashMap<PresentableMetadata,Long>();

	public static boolean isCached(PresentableMetadata md, int expiresAfter) {
		if (!cache.containsKey(md))
			return false;
		else if (cache.containsKey(md)) {
			if (new Date().getTime() - expiresAfter > cache.get(md)) {
				cache.remove(md);
				return false;
			}
		}

		return true;
	}


	public SearchDrillDown(MetadataModel model, ClientFactory factory) {
		this.model = model;
		this.clientFactory = factory;
	}

	@Override
	public List<PresentableMetadata> getChildren(final PresentableMetadata item,
			final MetadataPresenter notify, int start, int end) {
		final List<PresentableMetadata> contents = new ArrayList<PresentableMetadata>();

		// Perhaps we have some cached info on what's in the metadata?
		if (model != null) {


			for (PresentableMetadata s: model.getKnownContents(item)) {
				if (s != item && !s.getId().equals(item.getId())) {
//					System.out.println("Know that " + item + " has child content " + s);
					contents.add(s);
				}
			}
		}

		// Let's test to see if known contents includes anything other than
		// derived studies.  If so we'll have to request time series, etc.
		//		  boolean hasContent = false;
		//		  for (PresentableMetadata md: contents)
		//			  if (!md.getMetadataCategory().equals(BASIC_CATEGORIES.Snapshot.name()) &&
		//					  !md.getMetadataCategory().equals(BASIC_CATEGORIES.Collections.name()))
		//				  hasContent = true;

		if (item instanceof SearchResult && !isCached(item, 5000)) {
			GWT.log("SearchDrillDown doing initial request for " + item.getFriendlyName());
			cache.put(item, new Date().getTime());
			SearchResult sr = (SearchResult)item;

			for (PresentableMetadata s: sr.getDerived()) {
				if (s != item && !s.getId().equals(item.getId())) {
//					System.out.println("Know that " + item + " has child " + s);
					contents.add(s);
				}
			}


			//			  System.out.println("** Contents before fetch: " + contents);

			if (true/*contents.isEmpty()*/) {
				clientFactory.getSessionModel().getMainLogger().info("MDF drill-down for SearchResult requesting children for " + item.getFriendlyName());

				getContents(item, notify);

			} else {
				//				  contents.addAll(categorizeResults((SearchResult)item, contents));
				contents.addAll(contents);
				((SearchResult)item).setLeafStatus(!contents.isEmpty());

			}
		}
		//		  contents.add(new WebLink(item == null ? "" : item.getId(), "Penn", "http://www.cis.upenn.edu"));
		
//		System.err.println("Search drill-down results in " + contents);
		return contents;
	}
	
	public void getContents(final PresentableMetadata item,
			final MetadataPresenter notify) {
		clientFactory.getSnapshotServices().getSnapshotMetadata(clientFactory.getSessionModel().getSessionID(), item.getId(), 
				new SecFailureAsyncCallback.AlertCallback<SnapshotContents> (clientFactory, 
						"There was an error while retrieving permissions.") {

			@Override
			public void onSuccess(SnapshotContents result) {
				final List<PresentableMetadata> contents = new ArrayList<PresentableMetadata>();
				
				GWT.log("Snapshot contents for " + result.getLabel() + " received, with " + result.getRecordingObjects().size() + " recording objects");
				
				model.addKnownContents(item, result.getTraces());
				for (INamedTimeSegment ns: result.getTraces())
					contents.add((PresentableMetadata)ns);

				for (RecordingObject recordingObject : result.getRecordingObjects()) {
					FileInfo si = new FileInfo(
							recordingObject.getName(), 
							item.getId(), 
							item.getFriendlyName(), 
							recordingObject.getId().toString(),
							recordingObject.getInternetMediaType());
					si.setParent(item);
					((SearchResult)item).addDerived(si);
					item.addKnownChild(si);
					model.addKnownContents(item, si);
					contents.add(si);
				}
				
				List<SearchResult> derived = result.getDerived();
				model.addKnownContents(item, derived);
				for (SearchResult sr: derived) {
					model.addKnownSnapshot(sr);
				}
				contents.addAll(derived);

				// For all other aux info with the snapshot,
				// see if it's metadata we can add
				for (IJsonKeyValue kv: result.getOtherContent()) {
					if (kv instanceof JsonKeyValue) {
						if (((JsonKeyValue)kv).getJson() instanceof PresentableMetadata)
							contents.add((PresentableMetadata)((JsonKeyValue)kv).getJson());
					}
				}
				if (!contents.isEmpty())
					((SearchResult)item).setLeafStatus(false);

				//									  System.out.println("** Contents after fetch: " + contents);
				notify.refreshList(contents);
			}
		});
	}
}
