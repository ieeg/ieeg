package edu.upenn.cis.db.mefview.client.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MimeTypes {
	public static final Map<String,String> MIME_TYPES = new HashMap<String,String>();
	public static final Map<String,String> TEXT_TYPES = new HashMap<String,String>();
	public static final Map<String,String> IMAGE_TYPES = new HashMap<String,String>();
	
	static {
		TEXT_TYPES.put("java", "text/x-java-source");
		TEXT_TYPES.put("js", "application/javascript");
		TEXT_TYPES.put("es", "application/ecmascript");
		TEXT_TYPES.put("json", "application/json");
		TEXT_TYPES.put("c", "application/x-c");
		TEXT_TYPES.put("m", "application/x-matlab");
		TEXT_TYPES.put("cpp", "application/x-cpp");
		TEXT_TYPES.put("cxx", "application/x-cpp");
		TEXT_TYPES.put("csh", "application/x-csh");
		
		TEXT_TYPES.put("tex", "application/x-latex");
		TEXT_TYPES.put("latex", "application/x-latex");

		TEXT_TYPES.put("txt", "text/plain");
		TEXT_TYPES.put("rtf", "application/rtf");
		TEXT_TYPES.put("csv", "text/plain");
		TEXT_TYPES.put("tsv", "text/plain");

		TEXT_TYPES.put("html", "text/html");
		TEXT_TYPES.put("xml", "text/xml");
		TEXT_TYPES.put("dtd", "application/xml-dtd");
		TEXT_TYPES.put("css", "text/css");
		
		MIME_TYPES.put("zip", "application/zip");
		MIME_TYPES.put("bz", "application/x-bzip");
		MIME_TYPES.put("bz2", "application/x-bzip2");
		MIME_TYPES.put("gz", "application/gzip");
		MIME_TYPES.put("7z", "application/x-7z-compressed");
		MIME_TYPES.put("tar", "application/x-tar");
		MIME_TYPES.put("jar", "application/java-archive");
		MIME_TYPES.put("war", "application/java-archive");
		
		
		MIME_TYPES.put("pdf", "application/pdf");
		
		MIME_TYPES.put("dicom", "image/dicom");
		MIME_TYPES.put("svg", "image/svg+xml");

		IMAGE_TYPES.put("jpg", "image/jpeg");
		IMAGE_TYPES.put("jpeg", "image/jpeg");
		IMAGE_TYPES.put("gif", "image/gif");
		IMAGE_TYPES.put("png", "image/png");
		IMAGE_TYPES.put("dcm", "application/dicom");
		IMAGE_TYPES.put("nii", "application/x-nifti");
		IMAGE_TYPES.put("nii", "application/x-nifti-gz");
		
		for (String key: TEXT_TYPES.keySet())
			TEXT_TYPES.put(key, TEXT_TYPES.get(key));

		for (String key: IMAGE_TYPES.keySet())
			MIME_TYPES.put(key, IMAGE_TYPES.get(key));
	}
		
	public static Collection<String> getImageTypes() {
		return IMAGE_TYPES.values();
	}

	public static Collection<String> getTextTypes() {
		return TEXT_TYPES.values();
	}
}
