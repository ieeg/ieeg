/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

import edu.upenn.cis.db.mefview.shared.AnnotationScheme;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class WorkPlace implements IsSerializable, JsonTyped {
	List<String> favoriteStudies = new ArrayList<String>();
	List<String> favoriteTools = new ArrayList<String>();
	List<String> recentStudies = new ArrayList<String>();
	List<String> recentTools = new ArrayList<String>();
	
	Map<String, AnnotationScheme> annSchemes = new HashMap<String, AnnotationScheme>();
		
	String theProject = "";
//	List<String> windows = new ArrayList<String>();

	List<WindowPlace> contexts = new ArrayList<WindowPlace>();
	
	public WorkPlace() {
		
	}

	public List<String> getFavoriteStudies() {
		return favoriteStudies;
	}

	public void setFavoriteStudies(List<String> favoriteStudies) {
		this.favoriteStudies = favoriteStudies;
	}

	public List<String> getFavoriteTools() {
		return favoriteTools;
	}

	public void setFavoriteTools(List<String> favoriteTools) {
		this.favoriteTools = favoriteTools;
	}

	public List<String> getRecentStudies() {
		return recentStudies;
	}

	public void setRecentStudies(List<String> recentStudies) {
		this.recentStudies = recentStudies;
	}

	public List<String> getRecentTools() {
		return recentTools;
	}

	public void setRecentTools(List<String> recentTools) {
		this.recentTools = recentTools;
	}
	
	public void setProject(final String project) {
		theProject = project;
	}
	
	public String getProject() {
		return theProject;
	}

//	public List<String> getWindows() {
//		return windows;
//	}
//
//	public void setWindows(List<String> windows) {
//		this.windows = windows;
//	}

	public List<WindowPlace> getContexts() {
		return contexts;
	}

	public void setContexts(List<WindowPlace> contexts) {
		this.contexts = contexts;
	}

  public Map<String, AnnotationScheme> getAnnSchemes() {
    return annSchemes;
  }

  public void setAnnSchemes(Map<String, AnnotationScheme> annSchemes) {
    this.annSchemes = annSchemes;
  }

}
