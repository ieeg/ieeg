/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.client;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.Event.Type;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.Event.Type;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.SimpleEventBus;

import edu.upenn.cis.db.mefview.client.ClientFactoryImpl.HandlerPair;

public class IeegEventBus extends SimpleEventBus implements IEventBus {
	@Override
	public void fireEvent(Event<?> event) { 
		try {
			super.fireEvent(event);
		} catch (Exception ue) {
			GWT.log("EXCEPTION: " + ue);
			ue.printStackTrace();
		}
	}

	// Handler --> list of events it handles
	static Map<EventHandler, Set<Type<? extends EventHandler>>> handlersEvents = new HashMap<EventHandler, Set<Type<? extends EventHandler>>>();
	// Event --> stack of handlers
	static Map<Type<? extends EventHandler>, Stack<HandlerPair>> eventHandlers = 
			new HashMap<Type<? extends EventHandler>, Stack<HandlerPair>>();
	
	public <H extends EventHandler> void registerHandler(Type<H> handlerType, H handler) {
		HandlerRegistration reg = super.addHandler(handlerType, handler);
		
		// Store that we handle this event
		
		Set<Type<? extends EventHandler>> eventsHandled = handlersEvents.get(handler);
		if (eventsHandled == null) {
			eventsHandled = new HashSet<Type<? extends EventHandler>>();
			handlersEvents.put(handler, eventsHandled);
		}
		eventsHandled.add(handlerType);
		
		// Add to the stack of handlers registered for this event
		Stack<HandlerPair> handlers = eventHandlers.get(handlerType); 
		if (handlers == null) {
			handlers = new Stack<HandlerPair>();
			eventHandlers.put(handlerType, handlers);
		}
		handlers.push(new HandlerPair(handler, reg));
	}
	
	public <H extends EventHandler> void unregisterHandler(H handler) {
		if (handlersEvents.get(handler) == null)
			return;
		
		// Take each event, drop through the stack until we find the
		// item, unregister it, put the items back on the stack
		for (Type<? extends EventHandler> event : handlersEvents.get(handler)) {
			Stack<HandlerPair> unrolled = new Stack<HandlerPair>();
			Stack<HandlerPair> current = eventHandlers.get(event);
			
			while (current != null && !current.isEmpty()) {
				HandlerPair top = current.pop();
				
				// unroll this one!
				top.registration.removeHandler();
				if (top.object != handler)
					unrolled.push(top);
			}
			while (!unrolled.isEmpty()) {
				HandlerPair top = unrolled.pop();
				
				H h = (H)top.object;
				
				HandlerRegistration newReg = super.addHandler((Type<H>)event, h);
				
				// Re-register this one!
				current.push(new HandlerPair(h, newReg));
			}
		}
		handlersEvents.remove(handler);
	}

}
