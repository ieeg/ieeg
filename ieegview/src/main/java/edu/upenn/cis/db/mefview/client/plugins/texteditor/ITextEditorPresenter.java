package edu.upenn.cis.db.mefview.client.plugins.texteditor;

import com.google.gwt.dom.client.Element;

import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.FileInfo;

public interface ITextEditorPresenter extends Presenter {
	public void loadContent(FileInfo file);
}
