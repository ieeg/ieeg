/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.IProjectUpdater;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.client.ProjectServices;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.Project.STATE;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class ProjectServiceImpl extends RemoteServiceServlet implements ProjectServices {

	private final static Logger logger = LoggerFactory
			.getLogger(ProjectServiceImpl.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ ProjectServiceImpl.class);
	
	private static HabitatUserService uService;
	
	
	
	@VisibleForTesting
	ProjectServiceImpl(final IUserService userService,
			final IDataSnapshotServer dsServer) {

		uService = HabitatUserServiceFactory.getUserService(userService, dsServer);
	}		

	
	public ProjectServiceImpl() {
		
	}
	
	public void init() {
		if (uService == null) {
			uService = HabitatUserServiceFactory.getUserService();
		}
	}

	
	@Override
	public String addDiscussionToProject(SessionToken sessionID,
			String projectId, Post post) throws BtSecurityException {
		
		User u = uService.verifyUserError("addDiscussionToProject()", sessionID, null, logger);
		
		return uService.getDataSnapshotAccessSession().addProjectPosting(u, projectId,
				post);
	}

	@Override
	public Project createProject(SessionToken sessionID, Project project)
			throws BtSecurityException {
		final String m = "createProject()";
		try {
			User user = uService.verifyUser(m, sessionID, Role.USER);
			return DataMarshalling.convertEegProject(
					user,
					uService.getDataSnapshotAccessSession().addProject(user,
							DataMarshalling.convertToEegProject(project)),
							uService,
					uService.getDataSnapshotAccessSession());
		} catch (Exception e) {
			logger.error(m + ": Caught exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	@Override
	public Project getProject(SessionToken sessionID, String projectId)
			throws BtSecurityException {
		final String m = "getProject()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			EegProject p = uService.getDataSnapshotAccessSession().getProject(user, projectId);
			return DataMarshalling.convertEegProject(user, p, uService, uService.getDataSnapshotAccessSession());
		} catch (Exception e) {
			logger.error(m + ": Caught exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	@Override
	public Set<UserInfo> getProjectAdmins(SessionToken sessionID,
			String projectId) throws BtSecurityException {
		final String m = "getProjectAdmins()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			EegProject p = uService.getDataSnapshotAccessSession().getProject(user, projectId);
			Set<UserId> admins = p.getAdmins();

			Set<UserInfo> team = Sets.newHashSet();
			for (UserId u : admins) {
				User user2 = uService.getEnabledUser(u);
				if (user != null)
					team.add(uService.getUserInfoHelper(user2.getUsername()));
			}
			return team;
		} catch (Exception e) {
			logger.error(m + ": Caught exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	@Override
	public List<Post> getProjectDiscussions(SessionToken sessionID,
			String projectId, int postIndex, int numPosts)
			throws BtSecurityException {
		final String m = "getProjectDiscussions()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			return uService.getDataSnapshotAccessSession().getProjectDiscussions(user,
							projectId, postIndex, numPosts);
		} catch (Exception e) {
			logger.error(m + ": Caught exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}


	@Override
	public List<ProjectGroup> getProjectGroups(SessionToken sessionID,
			Set<ProjectGroup> excludedGroups) throws BtSecurityException {
		final String m = "getProjectGroups(...)";
		try {
			uService.verifyUser(m, sessionID, null);
			return uService.getDataSnapshotAccessSession().getProjectGroups(excludedGroups);
		} catch (Throwable t) {
			logger.error(m + " : Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<Project> getLazyProjectsForUser(SessionToken sessionID)
			throws BtSecurityException {
		final long startNanos = System.nanoTime();
		final String m = "getProjectsForUser()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			Set<EegProject> projects = uService.getDataSnapshotAccessSession().getProjectsForUser(user);
			
			final Set<String> projectPubIds = newHashSet();
			for (final EegProject project : projects) {
				projectPubIds.add(project.getPubId());
			}
			final Map<String, Set<DataSnapshotSearchResult>> pubIdToSearchResults = Collections.emptyMap();

			Set<Project> ret = Sets.newHashSet();
			for (EegProject proj : projects) {
				final Project lazyProject = DataMarshalling.convertEegProject(
						user, 
						proj, 
						uService,
						pubIdToSearchResults);
				lazyProject.setState(STATE.UNLOADED);
				ret.add(lazyProject);
			}
			return ret;
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		} finally {
			logger.info("{}: {} seconds", m, BtUtil.diffNowThenSeconds(startNanos));
		}
	}
	
	@Override
	public Set<SearchResult> getProjectSearchResultsForUser(SessionToken sessionID, String projectId)
			throws BtSecurityException {
		final long startNanos = System.nanoTime();
		final String m = "getProjectSearchResultsForUser(...)";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			
			final Set<DataSnapshotSearchResult> searchResults = uService.getDataSnapshotAccessSession().getSearchableDatasetsThatExist(user, projectId);
			final Set<SearchResult> ret = DataMarshalling.convertSearchResults(searchResults, user, uService);

			return ret;
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		} finally {
			logger.info("{}: {} seconds", m, BtUtil.diffNowThenSeconds(startNanos));
		}
	}

	@Override
	public Set<String> getProjectTags(SessionToken sessionID, String projectId)
			throws BtSecurityException {
		final String m = "getProjectTags()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			EegProject p = uService.getDataSnapshotAccessSession().getProject(user, projectId);

			return p.getTags();
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<ToolDto> getProjectTools(SessionToken sessionID,
			String projectId) throws BtSecurityException {
		final String m = "getProjectTools()";
		try {
			User user = uService.verifyUser(m, sessionID, Role.USER);
			EegProject p = uService.getDataSnapshotAccessSession().getProject(user, projectId);

			Set<String> toolIDs = p.getTools();

			return null;
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																
	public Set<UserInfo> getProjectUsers(SessionToken sessionID,
			String projectId) throws BtSecurityException {
		final String m = "getProjectUsers()";
		try {
			User user = uService.verifyUser(m, sessionID, null);
			EegProject p = uService.getDataSnapshotAccessSession().getProject(user, projectId);

			Set<UserInfo> team = Sets.newHashSet();
			Set<UserId> users = p.getTeam();
			for (UserId u : users) {
				User user2 = uService.getEnabledUser(u);
				if (user != null)
					team.add(uService.getUserInfoHelper(user2.getUsername()));
			}
			return team;
		} catch (Exception e) {
			logger.error(m + ": Caught exception", e);
			propagateIfInstanceOf(e, BtSecurityException.class);
			throw propagate(e);
		}
	}

	@Override
	public String updateProjectAdministrators(SessionToken sessionID,
			String projectId, final Set<String> userids)
			throws BtSecurityException {
		final String m = "updateProjectAdministrators()";
		try {
			final User user = uService.verifyUser(m, sessionID, Role.USER);

			uService.getDataSnapshotAccessSession().updateProject(user, projectId, new IProjectUpdater() {

				@Override
				public boolean update(EegProject project) {
					if (project.getAdmins().contains(user.getUserId())) {
						project.getAdmins().clear();
						for (String userid : userids) {
							User user2 = uService.getEnabledUser(userid);
							project.getAdmins().add(user2.getUserId());
						}
						if (project.getAdmins().isEmpty()) {
							project.getAdmins().add(user.getUserId());
						}
						return true;
					} else
						throw new AuthorizationException("User "
								+ user.getUsername()
								+ " is not a project administrator");
				}

			});
			return projectId;
		} catch (Throwable t) {
			logger.error(m + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}

	}

	@Override
	public String updateProjectSnapshots(SessionToken sessionID,
			String projectId, final Set<String> snapshotIds)
			throws BtSecurityException {
		final String m = "updateProjectSnapshots()";
		try {
			final User user = uService.verifyUser(m, sessionID, Role.USER);
			uService.getDataSnapshotAccessSession().updateProject(user, projectId, new IProjectUpdater() {

				@Override
				public boolean update(EegProject project) {
					if (project.getAdmins().contains(user.getUserId())) {
						project.getSnapshots().clear();
						for (String snapshotId : snapshotIds)
							project.getSnapshots().add(snapshotId);
						return true;
					} else
						throw new AuthorizationException("User "
								+ user.getUsername()
								+ " is not a project admin");
				}

			});
			return projectId;
		} catch (Throwable t) {
			logger.error(m + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String updateProjectTags(SessionToken sessionID, String projectId,
			final Set<String> tags) throws BtSecurityException {
		final String m = "updateProjectTags()";
		try {
			final User user = uService.verifyUser(m, sessionID, Role.USER);

			uService.getDataSnapshotAccessSession().updateProject(user, projectId, new IProjectUpdater() {

				@Override
				public boolean update(EegProject project) {
					if (project.getTeam().contains(user.getUserId())) {
						project.getTags().clear();
						project.getTags().addAll(tags);
						return true;
					} else
						throw new AuthorizationException("User "
								+ user.getUsername()
								+ " is not a project member");
				}

			});
			return projectId;
		} catch (Throwable t) {
			logger.error(m + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String updateProjectTeam(SessionToken sessionID, String projectId,
			final Set<String> userids) throws BtSecurityException {
		final String m = "updateProjectTeam()";
		try {
			final User user = uService.verifyUser(m, sessionID, Role.USER);

			uService.getDataSnapshotAccessSession().updateProject(user, projectId, new IProjectUpdater() {

				@Override
				public boolean update(EegProject project) {
					if (project.getAdmins().contains(user.getUserId())) {
						project.getTeam().clear();
						for (String u : userids) {
							User user2 = uService.getEnabledUser(u);
							project.getTeam().add(user2.getUserId());
						}
						return true;
					} else
						throw new AuthorizationException("User "
								+ user.getUsername()
								+ " is not a project administrator");
				}

			});
			return projectId;
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public String updateProjectTools(SessionToken sessionID, String projectId,
			final Set<String> toolIds) throws BtSecurityException {
		final String m = "updateProjectTools()";
		try {
			final User user = uService.verifyUser(m, sessionID, Role.USER);

			uService.getDataSnapshotAccessSession().updateProject(user, projectId, new IProjectUpdater() {

				@Override
				public boolean update(EegProject project) {
					if (project.getTeam().contains(user.getUserId())) {
						project.getTools().clear();
						for (String toolId : toolIds)
							project.getTools().add(toolId);
						return true;
					} else
						throw new AuthorizationException("User "
								+ user.getUsername()
								+ " is not a project member");
				}

			});
			return projectId;
		} catch (Throwable t) {
			logger.error(m + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
}
