package edu.upenn.cis.db.mefview.client.commands.snapshot;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.PermsPresenter;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.IPermsView.IPermsPresenter;
import edu.upenn.cis.db.mefview.client.dialogs.PermsDialog;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class EditPermissionsCommand implements Command {
	ClientFactory clientFactory;
	PresentableMetadata md;
	String userid;
	
	public EditPermissionsCommand(ClientFactory cf, final String userID,
			final PresentableMetadata md) {
		clientFactory = cf;
		this.userid = userID;
		this.md = md;
	}
	
	  public void showPermissions(final String userId, final DerivedSnapshot toolResult) {
		    AsyncCallback<GetAcesResponse> callback = new SecFailureAsyncCallback.AlertCallback<GetAcesResponse>(
		        clientFactory, "There was an error while retrieving permissions.") {

		      @Override
		      public void onSuccess(GetAcesResponse result) {
		        // hideGraph();
		        IPermsPresenter presenter = new PermsPresenter(clientFactory,
		            result.canEditAcl(), result.getWorldAce(), result.getProjectAces(),
		            result.getUserAces(), toolResult,
		            result.getDisplayStrToWorldPerm(),
		            result.getDisplayStrToProjectGroupPerm(),
		            result.getDisplayStrToUserPerm());
		        final PermsDialog permsDialog = new PermsDialog(presenter,
		            clientFactory);
		        // IeegEventBusFactory.getGlobalEventBus().fireEvent(new
		        // OpenedComplexDialogEvent(permsDialog.getDialogBox()));
		        permsDialog.centerAndShow();

		      }
		    };
		    clientFactory.getPortal().getAces(clientFactory.getSessionModel().getSessionID(),
		        "DataSnaphost", CorePermDefs.CORE_MODE_NAME,
		        toolResult.getAnalysisId(), callback);
		  }

		  public void showPermissions(final String userId, final String revId,
		      final String friendly) {
		    AsyncCallback<GetAcesResponse> callback = new SecFailureAsyncCallback.AlertCallback<GetAcesResponse>(
		        clientFactory, "There was an error while retrieving permissions.") {

		      @Override
		      public void onSuccess(GetAcesResponse result) {

		        IPermsPresenter presenter = new PermsPresenter(clientFactory,
		            result.canEditAcl(), result.getWorldAce(), result.getProjectAces(),
		            result.getUserAces(), revId, friendly,
		            result.getDisplayStrToWorldPerm(),
		            result.getDisplayStrToProjectGroupPerm(),
		            result.getDisplayStrToUserPerm());

		        final PermsDialog permsDialog = new PermsDialog(presenter,
		            clientFactory);
		        // IeegEventBusFactory.getGlobalEventBus().fireEvent(new
		        // OpenedComplexDialogEvent(permsDialog.getDialogBox()));
		        permsDialog.centerAndShow();

		      }
		    };
		    clientFactory.getPortal().getAces(clientFactory.getSessionModel().getSessionID(),
		        "DataSnapshot", CorePermDefs.CORE_MODE_NAME, revId, callback);
		  }

		@Override
		public void execute() {
			if (md instanceof SearchResult) {
				showPermissions(userid, ((SearchResult)md).getId(),
						((SearchResult)md).getFriendlyName());
			} else if (md instanceof DerivedSnapshot) {
				showPermissions(userid, (DerivedSnapshot)md);
			} else
				throw new RuntimeException("Unsupported permissions on this object!  " + md.getFriendlyName());
		}

}
