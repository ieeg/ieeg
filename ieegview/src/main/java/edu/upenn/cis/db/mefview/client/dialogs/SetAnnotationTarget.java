/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AnnotationSet;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.RemapAnnotations;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SeriesData;

public class SetAnnotationTarget extends DialogBox {
	ListBox datasetList = new ListBox();
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	TextBox theLabel = new TextBox();
	TextBox prefix = new TextBox();
	
	/**
	 * 
	 * @param datasets Set of datasets, in the form of serach results
	 * @param sourceFriendly Friendly name of the source annotations' study
	 * @param sourceDsId Rev ID of the source annotations' study
	 * @param annList List of possible annotations
	 * @param annManager Map of possible annotations
	 */
	public SetAnnotationTarget(final Collection<SearchResult> datasets, 
			final List<SeriesData> traces, final List<AnnotationSet<Annotation>> annList,
			final String annPrefix, final Map<String, DataSnapshotViews> snapshots) { 
		VerticalPanel vp = new VerticalPanel();
		
		setTitle("Apply Annotations to New Data Snapshot");
		setText("Apply Annotations to New Data Snapshot");
		setWidget(vp);
	
		vp.add(new Label("Target data snapshot:"));
		vp.add(datasetList);
		
		for (SearchResult s: datasets)
			datasetList.addItem(s.getFriendlyName(), s.getDatasetRevId());
		
		vp.add(new Label("New name for resulting annotation group:"));
		vp.add(theLabel);
		
		vp.add(new Label("Prefix annotation labels with:"));
		vp.add(prefix);
		prefix.setText(annPrefix);
		
		HorizontalPanel hp = new HorizontalPanel();
		hp.add(ok);
		hp.add(cancel);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		
		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
				
				String target = getTargetRevId();
//				String target = getTargetLabel();
				List<String> channels = new ArrayList<String>();
				
				for (SeriesData t : traces) {
//					System.out.println("Checking series data for " + target + " against " + t.label + "/" + t.revId + "/" + t.friendly + "/" + t.studyId);
					if (t.studyId.equals(target))
						channels.add(t.label);
				}
				
				DataSnapshotModel snapshot = snapshots.get(target).getState();
				RemapAnnotations rma = new RemapAnnotations( 
						 getTitle(), target, channels, annList, snapshot, //annManager, 
						 prefix.getText(),
						 snapshot.getTraceInfo());
				 
				 rma.center();
			}
			
		});
		
		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}
			
		});
	}
	
	public String getTitle() {
		return theLabel.getText();
	}
	
	public String getTargetRevId() {
		return datasetList.getValue(datasetList.getSelectedIndex());
	}
	
	public String getTargetLabel() {
		return datasetList.getItemText(datasetList.getSelectedIndex());
	}
}
