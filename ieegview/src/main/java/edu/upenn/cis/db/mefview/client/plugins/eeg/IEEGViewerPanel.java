package edu.upenn.cis.db.mefview.client.plugins.eeg;

import java.util.List;

import edu.upenn.cis.db.mefview.client.ServerAccess.Prefetcher;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.places.EEGPlace;

public interface IEEGViewerPanel extends AstroPanel {

	IEEGPane getEEGPane();

	void onResize();

	List<String> getTraceLabels();

	void registerPrefetchListener(Prefetcher prefetcher);

	void setTraceInfo(List<INamedTimeSegment> traces);

	void setTracesAndPlace(List<INamedTimeSegment> traces, EEGPlace place);

	void refreshAnnotations(List<AnnBlock> times);

}
