/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import com.google.gwt.core.client.GWT;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactoryImpl;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.PeriodicEvents;
import edu.upenn.cis.db.mefview.client.ServerAccess;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.ActiveJobs;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.ClosedSnapshotEvent;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.places.ApplicationPlace;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;

public class AppModel //implements RequestAnnotationDefinitionsEvent.Handler 
{
	private PeriodicEvents delayedEvents;
	
	private ClientFactoryImpl clientFactory;
	
	private ApplicationPlace place = new ApplicationPlace();
	private Map<AstroPanel,PanelDescriptor> descriptors = Maps.newHashMap(); 

	private Map<String, DataSnapshotViews> dataSnapshots = Maps.newHashMap();
	
	private ActiveJobs jobModel;
	private String userID;
	private SessionToken sessionToken;
	
	private Project currentProject;
	
	private MetadataModel snapshots;
	Map<String, List<AnnotationDefinition>> annotationDefs = new HashMap<String, List<AnnotationDefinition>>();
	List<AnnotationDefinition> cachedDefinitions = null;


	final List<AstroPanel> windows = new ArrayList<AstroPanel>();

	List<SearchResult> favoriteStudies = new ArrayList<SearchResult>();
	List<ToolDto> favoriteTools = new ArrayList<ToolDto>();
	
	public AppModel(ClientFactoryImpl cf) {
//		userID = user;
		
		clientFactory = cf;
		
		delayedEvents = new PeriodicEvents((ServerAccess)cf.getPortalServices());
		
		snapshots = new MetadataModel();
		
		this.clientFactory.getSessionModel().setPeriodicEvents(delayedEvents);
		
//		IeegEventBusFactory.getGlobalEventBus().registerHandler(RequestAnnotationDefinitionsEvent.getType(), this);
		MetadataFactory.getFactory(clientFactory, getSnapshotBrowserModel());
	}

	public PeriodicEvents getDelayedEvents() {
		return delayedEvents;
	}

	public void setDelayedEvents(PeriodicEvents delayedEvents) {
		this.delayedEvents = delayedEvents;
	}

//	public ClientFactoryImpl getClientFactory() {
//		return clientFactory;
//	}
	
	public SessionModel getSessionModel() {
		return clientFactory.getSessionModel();
	}

//	public void setClientFactory(ClientFactoryImpl clientFactory) {
//		this.clientFactory = clientFactory;
//	}

	public ApplicationPlace getPlace() {
		return place;
	}

	public void setPlace(ApplicationPlace place) {
		this.place = place;
	}

	public Map<AstroPanel, PanelDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(
			Map<AstroPanel, PanelDescriptor> descriptors) {
		this.descriptors = descriptors;
	}

	public Map<String, DataSnapshotViews> getDataSnapshots() {
		return dataSnapshots;
	}

	public void setDataSnapshots(Map<String, DataSnapshotViews> dataSnapshots) {
		this.dataSnapshots = dataSnapshots;
	}

	public ActiveJobs getJobModel() {
		return jobModel;
	}

	public void setJobModel(ActiveJobs jobModel) {
		this.jobModel = jobModel;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
		if (jobModel == null)
			jobModel = new ActiveJobs(userID, clientFactory);
	}

	public void setSessionToken(SessionToken sessionToken) {
		this.sessionToken = sessionToken;
	}

	public String getStudyFriendlyName(String revId) {
		DataSnapshotViews sm = dataSnapshots.get(revId);
		if (sm == null)
			return revId;
		else
			return sm.getState().getFriendlyName();
	}

	public boolean haveSnapshotsLoaded() {
		return !dataSnapshots.keySet().isEmpty();
	}

	public DataSnapshotViews getSnapshotModel(final String revID) {
		return dataSnapshots.get(revID);
	}
	
	public DataSnapshotModel getSnapshotState(final String revID) {
		return getSnapshotModel(revID).getState();
	}

	public void removeSnapshotModel(final String revID) {
		if (dataSnapshots.get(revID) != null) {
			DataSnapshotModel m = dataSnapshots.get(revID).getState();
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new ClosedSnapshotEvent(revID, m));
		}
		dataSnapshots.remove(revID);
//		manager.pruneFromStateStack(revID);
		place.removeSnapshot(revID);
	}

	public SessionToken getSessionToken() {
		return sessionToken;
	}

	public Set<String> getOpenStudies() {
		return dataSnapshots.keySet();
	}
	
	/*
	public Set<String> getStudiesInList() {
		Set<String> ret = new HashSet<String>();
		for (SearchResult sr:searchWindow.getStudyList().getList()) {
			ret.add(sr.getDatasetRevId());
		}
		return ret;
	}*/

	
	public Project getCurrentProject() {
		return currentProject;
	}
	
	public void setCurrentProject(Project p) {
		currentProject = p;
	}
	
	public MetadataModel getSnapshotBrowserModel() {
		return snapshots;
	}

//	@Override
//	public void getAnnotationTypes(SessionToken sessionID, final String viewerType,
//			final AsyncCallback<List<AnnotationDefinition>> callback) {
//		if (annotationDefs.get(viewerType) == null) {
//			GWT.log("Requesting annotation definitions from server");
//			clientFactory.getPortal().getAnnotationTypes(sessionID, viewerType, new AsyncCallback<List<AnnotationDefinition>>() {
//
//				@Override
//				public void onFailure(Throwable caught) {
//					callback.onFailure(caught);
//				}
//
//				@Override
//				public void onSuccess(List<AnnotationDefinition> result) {
//					GWT.log("Received annotation definitions from server");
//					annotationDefs.put(viewerType, result);
//					callback.onSuccess(result);
//				}
//				
//			});
//		} else
//			callback.onSuccess(annotationDefs.get(viewerType));
//	}
	
//	@Override
//	public void onRequestAnnotationDefinitions(final Presenter requester,
//			final String viewerType) {
//		
//		if (cachedDefinitions != null) {
//			IeegEventBusFactory.getGlobalEventBus().fireEvent(new ReceivedAnnotationDefinitionsEvent(requester, viewerType, cachedDefinitions));
//		} else {
//			getAnnotationTypes(clientFactory.getSessionModel().getSessionID(), viewerType, 
//					new AsyncCallback<List<AnnotationDefinition>>() {
//	
//				@Override
//				public void onFailure(Throwable caught) {
//					// TODO Auto-generated method stub
//					
//				}
//	
//				@Override
//				public void onSuccess(List<AnnotationDefinition> result) {
//					cachedDefinitions = result;
//					IeegEventBusFactory.getGlobalEventBus().fireEvent(new ReceivedAnnotationDefinitionsEvent(requester, viewerType, result));
//				};
//			});
//		}
//	}

	public WorkPlace getWorkPlace() {
		WorkPlace ret = new WorkPlace();
		
		for (SearchResult sr: favoriteStudies)
			ret.getFavoriteStudies().add(sr.getDatasetRevId());
		
//		for (SearchResult sr: recentStudies)
//			ret.getRecentStudies().add(sr.getDatasetRevId());
		
		for (ToolDto ti: favoriteTools)
			ret.getFavoriteTools().add(ti.getId().get());
		
//		for (ToolDto ti: recentTools)
//			ret.getRecentTools().add(ti.getId().get());
		
		for (AstroPanel ap: windows) {
			WindowPlace pl = ap.getPlace();
			if (pl != null) {
				GWT.log("Saving window place for " + ap.getPanelType() + " / " + ap.getDataSnapshotState().getFriendlyName());
				ret.getContexts().add(pl);
			}
		}
		
		return ret;
	}
	
	public List<AstroPanel> getWindows() {
		return windows;
	}

	public void unbookmarkSnapshot(SearchResult snapshot) {
		favoriteStudies.remove(snapshot);
	}

	public void bookmarkSnapshot(SearchResult snapshot) {
		favoriteStudies.add(snapshot);
	}

	public void bookmarkTool(ToolDto tool) {
		favoriteTools.add(tool);
	}

	public void unbookmarkTool(ToolDto tool) {
		favoriteTools.remove(tool);
	}

	public List<SearchResult> getFavoriteSnapshots() {
		return favoriteStudies;
	}

	public void addWindow(AstroPanel pane) {
		if (!windows.contains(pane))
			windows.add(pane);
	}

	public void removeWindow(AstroPanel pane) {
		windows.remove(pane);
	}
}
