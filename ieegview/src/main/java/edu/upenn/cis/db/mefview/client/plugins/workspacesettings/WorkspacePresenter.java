/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.workspacesettings;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.CreateProject;
import edu.upenn.cis.db.mefview.client.events.BookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.ClosedSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenProjectEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.SnapshotOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolMarkedAsFavoriteEvent;
import edu.upenn.cis.db.mefview.client.events.UnbookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.WorkspaceModel;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.RecordingObject;
import edu.upenn.cis.db.mefview.shared.SearchResult;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class WorkspacePresenter extends BasicPresenter implements ProjectCreatedEvent.Handler, 
ProjectUpdatedEvent.Handler, ToolMarkedAsFavoriteEvent.Handler, 
BookmarkSnapshotEvent.Handler, UnbookmarkSnapshotEvent.Handler {
	public interface Display extends BasicPresenter.Display {
		public void setEegHandler(ClickHandler handler);
		public void setLinkHandler(ClickHandler handler);
		public void setDicomHandler(ClickHandler handler);
		public void setDocumentHandler(ClickHandler handler);
		public void setImageHandler(ClickHandler handler);
		public void enableEegHandler(boolean sel);
		public void enableLinkHandler(boolean sel);
		public void enableDicomHandler(boolean sel);
		public void enableDocumentHandler(boolean sel);
		public void enableImageHandler(boolean sel);
		
		public SearchResult getSelectedSnapshot();
		public List<SearchResult> getFavoriteSnapshots();
//		public List<SearchResult> getRecentSnapshots();
		public List<WebLink> getResources();
		public void addFavoriteSnapshot(SearchResult sr);
		public void removeFavoriteSnapshot(SearchResult sr);
//		public void addRecentSnapshot(SearchResult sr);
//		public void removeRecentSnapshot(SearchResult sr);
		public void setFavoriteSnapshotList(List<SearchResult> sr);
//		public void setRecentSnapshotList(List<SearchResult> sr);
		public boolean isSelectedFavoriteSnapshot(SearchResult sr);
//		public boolean isSelectedRecentSnapshot(SearchResult sr);
		public void refreshFavoriteSnapshots();
//		public void refreshRecentSnapshots();

		public WebLink getSelectedLink();
		public ToolDto getSelectedTool();
		public List<ToolDto> getFavoriteTools();
//		public List<ToolDto> getRecentTools();
		public void addFavoriteTool(ToolDto ti);
		public void removeFavoriteTool(ToolDto ti);
//		public void addRecentTool(ToolDto ti);
//		public void removeRecentTool(ToolDto ti);
		public void setFavoriteToolList(List<ToolDto> ti);
//		public void setRecentToolList(List<ToolDto> ti);
//		public void refreshRecentTools();
		public void refreshFavoriteTools();

		public void addProject(int inx, Project p);
		public void addProject(Project p);
		public void setProjects(Collection<Project> pl);
		public List<Project> getProjects();
		public void removeProject(Project p);
		public void removeProject(int pos);
		public Set<Project> getSelectedProjects();
		
		public void addFavoriteSnapshotSelectionChangedHandler(Handler handler);
		public void addLinkSelectionChangedHandler(Handler handler);
		public void addFavoriteToolSelectionChangedHandler(Handler handler);
//		public void addRecentSnapshotSelectionChangedHandler(Handler handler);
//		public void addRecentToolSelectionChangedHandler(Handler handler);
		public void addProjectOpenedHandler(ClickHandler handler);
		public void addProjectCreatedHandler(ClickHandler handler);

		public void setWorkspaceModel(WorkspaceModel model);
		
		public void log(String msg);
	}

	WorkspaceModel model;
	AppModel control;

	Stack<HandlerRegistration> handlers = new Stack<HandlerRegistration>();

	public final static String NAME = "workspace";
	public final static Set<String> TYPES = Sets.newHashSet();
	final static WorkspacePresenter seed = new WorkspacePresenter();
	
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, false));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public WorkspacePresenter() {
		super(null, NAME, TYPES);
	}

	public WorkspacePresenter( 
			final AppModel parent,
			final ClientFactory factory) {
		super(factory, NAME, TYPES);
		control = parent;
	}
	
	public void updateRevId(String oldRevId, String newRevId) {
		for (SearchResult sr: getDisplay().getFavoriteSnapshots()) {
			if (sr.getDatasetRevId().equals(oldRevId))
				sr.setDatasetRevId(newRevId);
		}
		
	}

	public void bookmarkSnapshot(SearchResult s) {
		if (s != null && !getDisplay().getFavoriteSnapshots().contains(s))
			getDisplay().addFavoriteSnapshot(s);
	}

	public void unbookmarkSnapshot(SearchResult s) {
		if (s != null && getDisplay().getFavoriteSnapshots().contains(s))
			getDisplay().removeFavoriteSnapshot(s);
	}

	public void bookmarkTool(ToolDto t) {
		if (getSessionModel().isRegisteredUser() && t != null && !getDisplay().getFavoriteTools().contains(t))
			getDisplay().addFavoriteTool(t);
	}
	
	public void unbookmarkTool(ToolDto t) {
		if (getSessionModel().isRegisteredUser() && t != null && getDisplay().getFavoriteTools().contains(t))
			getDisplay().removeFavoriteTool(t);
	}
	
	public WorkspaceModel getWorkspaceModel() {
		return model;
	}
	
	@Override
	public void onCreatedProject(Project project) {
		getDisplay().addProject(project);
	}

	@Override
	public void onUpdatedProject(Project project) {
		int pos = 0;
		for (Project old: getDisplay().getProjects()) {
			if (old.getPubId().equals(project.getPubId())) {
				getDisplay().log("Removing existing project during update");
				getDisplay().removeProject(pos);
				break;
			} else
				pos++;
		}
		getDisplay().addProject(pos, project);
	}


	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {

		return new WorkspacePresenter(controller, clientFactory);
	}

	@Override
	public Display getDisplay() {
		return (Display)display;
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		model = new WorkspaceModel(getDisplay().getFavoriteSnapshots(),
				getDisplay().getFavoriteTools(),
//				getDisplay().getRecentSnapshots(),
//				getDisplay().getRecentTools(), 
				getDisplay().getResources(), 
				getClientFactory());
		
		getDisplay().setWorkspaceModel(model);
		getDisplay().addPresenter(this);
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ProjectCreatedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ProjectUpdatedEvent.getType(), this);
		if (getSessionModel().isRegisteredUser())
			IeegEventBusFactory.getGlobalEventBus().registerHandler(ToolMarkedAsFavoriteEvent.getType(), this);
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(BookmarkSnapshotEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(UnbookmarkSnapshotEvent.getType(), this);

		getDisplay().setEegHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				OpenDisplayPanel act = new OpenDisplayPanel(getDisplay().getSelectedSnapshot(), false, 
						EEGViewerPanel.NAME,
						null);
				
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			}

		});
		getDisplay().setLinkHandler(new ClickHandler() {

			public void onClick(ClickEvent arg0) {
				OpenDisplayPanel act = new OpenDisplayPanel(getDisplay().getSelectedLink(), false, 
						WebPanel.NAME,
						null);
				
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			}

		});
		getDisplay().setDocumentHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				OpenDisplayPanel act = new OpenDisplayPanel(getDisplay().getSelectedSnapshot(), false, 
						PDFViewerPanel.NAME,
						null);
				
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			}
			
		});
		getDisplay().setDicomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				SearchResult thisOne = getDisplay().getSelectedSnapshot();
				
				if (thisOne == null)
					return;
				
				OpenDisplayPanel act = new OpenDisplayPanel(getDisplay().getSelectedSnapshot(), false, 
						AttachmentDownloader.NAME,
						null);
				
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			}
			
		});
		getDisplay().setImageHandler(new ClickHandler() {
			public void onClick(ClickEvent arg0) {
				OpenDisplayPanel act = new OpenDisplayPanel( 
						getDisplay().getSelectedSnapshot(), false, 
						ImageViewerPanel.NAME,
						null);
				
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(act));
			}

		});
		getDisplay().addLinkSelectionChangedHandler(new Handler() {

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if (getDisplay().getSelectedLink() == null)
					getDisplay().enableLinkHandler(false);
				else
					getDisplay().enableLinkHandler(true);
			}
		});
		getDisplay().addFavoriteSnapshotSelectionChangedHandler(new Handler() {

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				if (getDisplay().getSelectedSnapshot() == null || 
						getDisplay().getSelectedSnapshot().getDatasetRevId() == null)
					return;
				
				getDisplay().enableEegHandler(true);
				getDisplay().enableDocumentHandler(false);
				
				getDisplay().enableDicomHandler(false);
				getDisplay().enableImageHandler(false);
				
				String revId = getDisplay().getSelectedSnapshot().getDatasetRevId();
                DataSnapshotViews model = control.getSnapshotModel(revId);
				
				if (model != null) {
			    	  boolean hasPDF = false;
			    	  boolean hasNonPDF = false;
			    	  for (FileInfo f: model.getState().getFiles()) {
			    		  if (f.getContentDescriptor().equals("application/pdf"))
			    			  hasPDF = true;
			    		  
			    		  if (!f.getContentDescriptor().equals("application/pdf"))
			    			  hasNonPDF = true;
			    	  }
			    	  
			        if (hasPDF)//!model.getState().getDocuments().isEmpty())
			        	getDisplay().enableDocumentHandler(true);
			        if (hasNonPDF && getSessionModel().isRegisteredUser())//!model.getState().getFiles().isEmpty()){
			          // always keep Download DICOM disabled for Guest
			           getDisplay().enableDicomHandler(true);
				} else {
					getClientFactory().getPortalServices().getRecordingObjects(getSessionModel().getSessionID(), revId, 
							new SecFailureAsyncCallback.SecAlertCallback<List<RecordingObject>>(getClientFactory()) {

						@Override
						public void onSuccess(List<RecordingObject> result) {
							boolean downloads = false;
							boolean documents = false;
							for (RecordingObject recObj : result) {
								final String mediaType = recObj.getInternetMediaType();
								if (mediaType.equals("application/pdf")) {
									documents = true;
								} else {
									downloads = true;
								}
								if (documents && downloads) {
									break;
								}
							}
							getDisplay().enableDocumentHandler(documents);
							getDisplay().enableDicomHandler(downloads);
						}

					});
				}
//				viewImages.setEnabled(false);
				for (int i = 0; i < getDisplay().getFavoriteSnapshots().size(); i++)
					if (getDisplay().isSelectedFavoriteSnapshot(getDisplay().getFavoriteSnapshots().get(i))) {
							if (getDisplay().getFavoriteSnapshots().get(i).getImageCount() > 0)
								getDisplay().enableImageHandler(true);
//								viewImages.setEnabled(true);
					}
			}
		});
//		getDisplay().addRecentSnapshotSelectionChangedHandler(new Handler() {
//
//			@Override
//			public void onSelectionChange(SelectionChangeEvent event) {
//				getDisplay().enableEegHandler(true);
//				getDisplay().enableImageHandler(false);
//				getDisplay().enableDicomHandler(false);
//				getDisplay().enableDocumentHandler(false);
//				
//				String revId = getDisplay().getSelectedSnapshot().getDatasetRevId();
//		        DataSnapshotController model = control.getSnapshotModel(revId);
//		
//		        
//		        if (model != null) {
//		          getDisplay().enableDocumentHandler(!model.getState().getDocuments().isEmpty());
//		          getDisplay().enableDicomHandler(!model.getState().getDicoms().isEmpty());
//		        } else {
//		          getDisplay().enableDocumentHandler(false);
//		          getDisplay().enableDicomHandler(false);
//		          getClientFactory().getCacheManager().getRecordingObjects(getClientFactory().getSessionID(), revId, 
//							new SecFailureAsyncCallback.SecAlertCallback<List<RecordingObject>>(getClientFactory()) {
//
//						@Override
//						public void onSuccess(List<RecordingObject> result) {
//							boolean downloads = false;
//							boolean documents = false;
//							for (RecordingObject recObj : result) {
//								final String mediaType = recObj.getInternetMediaType();
//								if (mediaType.equals("application/pdf")) {
//									documents = true;
//								} else {
//									downloads = true;
//								}
//								if (documents && downloads) {
//									break;
//								}
//							}
//							getDisplay().enableDocumentHandler(documents);
//							getDisplay().enableDicomHandler(downloads);
//						}
//
//					});
//		        }
//
//				for (int i = 0; i < getDisplay().getRecentSnapshots().size(); i++)
//					if (getDisplay().isSelectedRecentSnapshot(getDisplay().getRecentSnapshots().get(i))) {
//							if (getDisplay().getRecentSnapshots().get(i).getImageCount() > 0)
////								viewRImages.setEnabled(true);
//								getDisplay().enableImageHandler(true);
//					}
//			}
//		});

		getDisplay().log("Requesting Projects");
		getClientFactory().getProjectServices().getLazyProjectsForUser(getSessionModel().getSessionID(), 
		    new SecFailureAsyncCallback.SecAlertCallback<Set<Project>>(getClientFactory()) {

		  @Override
		  public void onNonSecFailure(Throwable caught) {
		    GWT.log("Project Request Failed");
		    getDisplay().log("Project request failed! " + caught.getMessage());
		  }

		  @Override
		  public void onSuccess(Set<Project> arg0) {
		    GWT.log("Project Request Successful");
		    getDisplay().log("Adding " + arg0.size() + " projects");

		    getDisplay().setProjects(arg0);
		  }

		});
		
		if (getSessionModel().isRegisteredUser())
		  getDisplay().addProjectCreatedHandler(new ClickHandler() {

		    @Override
		    public void onClick(ClickEvent arg0) {
		      CreateProject proj = new CreateProject();
		      proj.center();
		      proj.show();
		    }

		  });
		
		getDisplay().addProjectOpenedHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent arg0) {
				Set<Project> selected = getDisplay().getSelectedProjects();
				if (selected.size() == 1) {
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenProjectEvent(selected.iterator().next()));
				}
			}
			
		});
		
		IeegEventBusFactory.getGlobalEventBus().registerHandler(SnapshotOpenedEvent.getType(), 
				new SnapshotOpenedEvent.Handler() {
			
			@Override
			public void onOpenedSnapshot(String snapshot, DataSnapshotModel model, SearchResult sr) {
				// Remove if it already exists
//				getDisplay().removeRecentSnapshot(sr);
//				getDisplay().addRecentSnapshot(sr);

				getDisplay().refreshFavoriteSnapshots();
//				getDisplay().refreshRecentSnapshots();
			}
		});
		IeegEventBusFactory.getGlobalEventBus().registerHandler(ClosedSnapshotEvent.getType(), 
				new ClosedSnapshotEvent.Handler() {
			
			@Override
			public void onClosedSnapshot(String snapshot, DataSnapshotModel model) {
				getDisplay().refreshFavoriteSnapshots();
//				getDisplay().refreshRecentSnapshots();
			}
		});
		
		getDisplay().bindDomEvents();
	}

	@Override
	public void unbind() {
		while (!handlers.isEmpty()) {
			handlers.pop().removeHandler();
		}
		
	}

	@Override
	public void onUnbookmarkSnapshot(SearchResult snapshot) {
		unbookmarkSnapshot(snapshot);
	}

	@Override
	public void onBookmarkSnapshot(SearchResult snapshot) {
		bookmarkSnapshot(snapshot);
	}

	@Override
	public void onToolMarkedAsFavorite(ToolDto tool) {
		if (getSessionModel().isRegisteredUser())
			bookmarkTool(tool);
	}

	@Override
	public void onToolUnmarkedAsFavorite(ToolDto tool) {
		if (getSessionModel().isRegisteredUser())
			unbookmarkTool(tool);
	}


}
