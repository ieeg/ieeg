/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.db.mefview.client.EEGDisplay.DIRECTION;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.LogEntry;
import edu.upenn.cis.db.mefview.shared.PortalSummary;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.ProvenanceSummary;
import edu.upenn.cis.db.mefview.shared.RefreshList;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.StripeCustomer;
import edu.upenn.cis.db.mefview.shared.TimeSeriesBookmark;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.UserPrefs;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;

/**
 * The client side stub for the RPC service.
 */
public interface EEGDisplayAsync {
	void getUserId(String userName, String password,
			AsyncCallback<UserInfo> callback);

	void getUserInfo(String userName, AsyncCallback<UserInfo> callback);
	
	/**
	 * Get information on the traces in this snapshot
	 * @param datasetID
	 * 
	 * 
	 * 
	 */
//	void getTraces(SessionToken sessionID, String datasetID, AsyncCallback<List<TraceInfo>> callback);

	/**
	 * Save a set of annotations for this dataset
	 * @param datasetID
	 * @param annotations
	 * 
	 * 
	 * 
	 */
//	void saveAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations, AsyncCallback<DataSnapshotIds> callback);

	/**
	 * Remove the set of annotations
	 * @param datasetID
	 * @param annotations
	 * 
	 * 
	 */
//	void removeAnnotations(SessionToken sessionID, String datasetID, Set<Annotation> annotations, AsyncCallback<String> callback);

//	void getResultsJSON(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, double samplingPeriod, List<FilterParameters> filter,
//			SignalProcessingStep processing, AsyncCallback<String> callback);

//	void getResultsJSON(SessionToken sessionID, String contextID,
//			String datasetID, List<String> traceids, double start,
//			double width, int scaleFactor, SignalProcessingStep processing,
//			AsyncCallback<String> callback);

//	Integer[][] getResults(String userid, String datasetID, List<String> traceids, double start, double width, double samplingPeriod,
//			List<FilterParameters> filter) throws IllegalArgumentException, UnauthorizedException;
	
//	void getResults(SessionToken sessionID, String contextID, String datasetID,
//			List<String> traceids, double start, double width,
//			double samplingPeriod, List<FilterParameters> filter,
//			SignalProcessingStep processing,
//			AsyncCallback<TimeSeries[]> callback);

	//	String getFirstPageJSON(String studyPath, String subPath, String study, long timeResolution, double samplingPeriod) throws IllegalArgumentException;

//	int[][] getResultsArray(String study, int start, int width, int height) throws IllegalArgumentException;

//	List<Annotation> getAnnotations(String studyPath, String subPath, String study, String type) throws IllegalArgumentException;
	
	/**
	 * Get the annotations for a dataset
	 * @param datasetid
	 * @param type
	 * 
	 * 
	 */
//	@Deprecated
//	void getAnnotations(SessionToken sessionID, String datasetid, String type, AsyncCallback<List<Annotation>> callback);
	
//	SortedMap<Double,HashMap<String,Set<String>>> getEvents(String studyPath, String subPath, String study, String type) throws IllegalArgumentException;
	
//	ArrayList<Sample> getResults(String study, String trace, int start, int width) throws IllegalArgumentException;

//	ArrayList<Sample> getResults(String study, List<String> traces, int start, int width) throws IllegalArgumentException;

//	TraceInfo getTraceInfo(String user, String studyPath, String subPath, String study, String trace) throws IllegalArgumentException;

//	TraceInfo getTraceInfo(String user, String datasetid, String traceid) throws IllegalArgumentException, UnauthorizedException;

//	List<TraceInfo> getTraceInfo(String user, String datasetid, List<String> traceid) throws IllegalArgumentException, UnauthorizedException;
	
//	long getStartDate(String studyPath, String subPath, String study, String trace) throws IllegalArgumentException;
//	double getDuration(String studyPath, String subPath, String study) throws IllegalArgumentException;
//	double getSampleRate(String studyPath, String subPath, String study, String trace) throws IllegalArgumentException;
//	double getVoltageConversionFactor(String studyPath, String subPath, String study, String trace) throws IllegalArgumentException;

//	String getImagePath() throws IllegalArgumentException;
	
	/**
	 * Get the set of snapshots matching the search parameters
	 * @param searchRestrictions
	 * 
	 * 
	 * 
	 */
//	void getSnapshotsFor(SessionToken sessionID, Search searchRestrictions, AsyncCallback<Set<SearchResult>> callback);

	/**
	 * Get the analyses for a study
	 * @param studyRevId
	 * 
	 * 
	 * 
	 */
	
	void deriveSnapshot(SessionToken sessionID, String studyRevId,
			String friendlyName, String toolName,
			AsyncCallback<SearchResult> callback);

	public void deriveSnapshot(SessionToken sessionID, final String studyRevId, final String friendlyName,
			final String toolName, final List<String> channelIDs, boolean includeAnnotations, 
			AsyncCallback<SearchResult> callback);

	public void getSystemState(SessionToken sessionID, AsyncCallback<List<String>> callback);
	
	public void getRegisteredTools(SessionToken sessionID, AsyncCallback<List<ToolDto>> callback);
	
	public void registerTool(SessionToken sessionID, final List<ToolDto> tool, AsyncCallback<List<String>> callback);

	public void removeTool(SessionToken sessionID, final Set<String> toolRevIDs, AsyncCallback<Boolean> callback);

	void getAces(SessionToken sessionID, String entityType, String mode,
			String targetId, AsyncCallback<GetAcesResponse> callback);

	void getEnabledUsers(SessionToken sessionID, int start, int length, Set<UserId> excludedUsers, AsyncCallback<GetUsersResponse> callback);

	void editAcl(SessionToken sessionID, String entityType, String mode,
			List<IEditAclAction<?>> actions,
			AsyncCallback<List<EditAclResponse>> callback);

	void getPlaces(SessionToken sessionID, String dataSnapshotRevId, AsyncCallback<List<Place>> callback);

	void saveWorkPlace(SessionToken sessionID, WorkPlace place, UserPrefs prefs, AsyncCallback<Boolean> callback);

	void loadWorkPlace(SessionToken sessionID, AsyncCallback<WorkPlace> callback);

	void getTools(SessionToken sessionID, Set<String> toolRevIds, AsyncCallback<List<ToolDto>> callback);

	void indexDataSnapshot(SessionToken sessionID, String dataSnapshotRevId, double sampleRate,
			double pageLength, AsyncCallback<Boolean> callback);

	void registerActivity(SessionToken sessionID, DIRECTION direction,
			long time, List<String> channels,
			List<DisplayConfiguration> filters, Set<Annotation> activeAnnotations, AsyncCallback<Void> callback);

	void logoutSession(SessionToken sessionID, AsyncCallback<Void> callback);

	void saveWorkPlaceAndLogout(SessionToken sessionID, WorkPlace place, UserPrefs prefs,
			AsyncCallback<Void> callback);

	void getPostings(SessionToken sessionID, String dsRevId, int postIndex,
			int numPosts, AsyncCallback<List<Post>> callback);

	void addPosting(SessionToken sessionID, String dsRevId, Post post,
			AsyncCallback<Long> callback);


	void pollForUpdates(SessionToken sessionID, Timestamp sinceWhen,
			AsyncCallback<RefreshList> callback);

	void addShortURL(SessionToken sessionID, TimeSeriesBookmark book,
			AsyncCallback<String> callback);

	void addUserBookmark(SessionToken sessionID, TimeSeriesBookmark book,
			AsyncCallback<String> callback);

	void addUserRestoreMarks(SessionToken sessionID, TimeSeriesBookmark book,
			AsyncCallback<String> callback);

	void getUserBookmarks(SessionToken sessionID,
			AsyncCallback<Set<TimeSeriesBookmark>> callback);

	void getUserRestoreMarks(SessionToken sessionID,
			AsyncCallback<Set<TimeSeriesBookmark>> callback);

	void getLogEntries(SessionToken sessionID, int startIndex, int count,
			AsyncCallback<List<LogEntry>> callback);

	void addLogEntry(SessionToken sessionID, LogEntry entry,
			AsyncCallback<Void> callback);


	void addProvenanceRecord(SessionToken sessionID, ProvenanceLogEntry op, AsyncCallback<Void> callback);

	void getUserInfo(SessionToken sessionID, String requestedUserId,
			AsyncCallback<UserInfo> callback);

	void getUserProvenanceSummary(SessionToken sessionID,
			AsyncCallback<ProvenanceSummary> callback);

	void getAllUsers(SessionToken sessionID,
			AsyncCallback<List<UserInfo>> callback);

	void getMontages(SessionToken sessionID, String dataSetID,
	        AsyncCallback<List<EEGMontage>> callback);
	
	void addMontage(SessionToken sessionID, EEGMontage montage, String dataSetID,
	        AsyncCallback<EEGMontage> callback);
	
	void getNextValidDataRegion(SessionToken sessionID, String dataSetID,
			double position, AsyncCallback<Double> callback);

	void getPreviousValidDataRegion(SessionToken sessionID, String dataSetID,
			double position, AsyncCallback<Double> callback);

	void getAnnotationTypes(SessionToken sessionID, String viewerType,
			AsyncCallback<List<AnnotationDefinition>> callback);

	void getPortalSummary(SessionToken sessionID,
			AsyncCallback<PortalSummary> callback);

	void getAvailableFilterTypes(AsyncCallback<List<String>> callback);

	void getSignalProcessingTypes(SessionToken sessionID, String viewerType,
			AsyncCallback<List<SignalProcessingStep>> callback);


	void reIndexDatabase(SessionToken sessionID, AsyncCallback<Void> callback);

	void changePassword(SessionToken sessionID, String oldMd5, String newMd5,
						AsyncCallback<Void> callback);

	void changePassword(String oldplainText, String newPlaintext, AsyncCallback<Void> callback);

	void getServerConfiguration(SessionToken sessionID, AsyncCallback<ServerConfiguration> callback);


	void createUser(UserAccountInfo accountInfo, String encryptedPassword,
			AsyncCallback<UserInfo> asyncCallback);


	void createSubscription(String token, String userID, AsyncCallback<StripeCustomer> asyncCallback);

	void getSubscription(String userID, AsyncCallback<StripeCustomer> asyncCallback);

	void getUserAccountInfo(SessionToken sessionID, String userID,
			AsyncCallback<UserAccountInfo> callback);


	void getUserAccountInfoForAll(SessionToken sessionID,
			AsyncCallback<List<UserAccountInfo>> callback);


	void setUserAccountInfoForAll(SessionToken sessionID,
			List<UserAccountInfo> info, AsyncCallback<Void> callback);


	void removeMontage(SessionToken sessionID, EEGMontage montage,
			AsyncCallback<Void> callback);

	void editMontage(SessionToken sessionID, EEGMontage montage, String dataSetID,
			AsyncCallback<EEGMontage> callback);	

	void updateUser(SessionToken sessionID, UserAccountInfo newUserInfo,
			String hashedNewPassword, AsyncCallback<UserInfo> callback);


	void getKnownUsers(SessionToken sessionID, AsyncCallback<Set<? extends PresentableMetadata>> callback);


	void getRecommendedUsers(SessionToken sessionID, AsyncCallback<Set<? extends PresentableMetadata>> callback);


	void addUserToGroup(SessionToken sessionID, UserInfo userInfo, String groupName, AsyncCallback<Void> callback);


	void addFriend(SessionToken sessionID, UserInfo friendInfo, AsyncCallback<Void> callback);


	void shareWith(SessionToken sessionID, PresentableMetadata object, int levelIndex, UserInfo sharee,
			AsyncCallback<Void> callback);

	void shareWith(SessionToken sessionID, PresentableMetadata shared, int shareLevel, String shareWith,
			AsyncCallback<Void> callback);


	void changeOwner(SessionToken sessionID, PresentableMetadata object, UserInfo newOwner,
			AsyncCallback<Void> callback);


	void createGroup(SessionToken sessionID, String groupName, AsyncCallback<BasicCollectionNode> callback);


	void removeGroup(SessionToken sessionID, BasicCollectionNode group, AsyncCallback<Void> callback);


	void unshareWith(SessionToken sessionID, PresentableMetadata object, UserInfo sharee, AsyncCallback<Void> callback);


	void removeUserFromGroup(SessionToken sessionID, UserInfo userInfo, String groupName, AsyncCallback<Void> callback);


	void createFolder(SessionToken sessionID, String label, String folderPathName,
			AsyncCallback<BasicCollectionNode> callback);


	void removeFolder(SessionToken sessionID, BasicCollectionNode folder, AsyncCallback<Void> callback);


	void removeFriend(SessionToken sessionID, UserInfo friendInfo, AsyncCallback<Void> callback);


	void getGroups(SessionToken sessionID, AsyncCallback<Set<? extends PresentableMetadata>> callback);


	void getFolderContents(SessionToken sessionID, String path,
			AsyncCallback<Set<? extends PresentableMetadata>> callback);


	void storeMetadata(SessionToken sessionID, String path, PresentableMetadata object, AsyncCallback<Void> callback);


	void removeMetadata(SessionToken sessionID, String path, PresentableMetadata object, AsyncCallback<Void> callback);


	void storeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo info, String container,
			String fileKey, byte[] object, AsyncCallback<Integer> callback);


	void removeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo info, String container,
			String fileKey, AsyncCallback<Void> callback);


	void storeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object,
			AsyncCallback<Void> callback);


	void removeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object,
			AsyncCallback<Void> callback);


	void getFolderContents(SessionToken sessionID, BasicCollectionNode folder,
			AsyncCallback<Set<? extends PresentableMetadata>> callback);


	void getSharingSet(SessionToken sessionID, PresentableMetadata folder,
			AsyncCallback<Map<UserInfo, Set<Integer>>> callback);


	void addToFolder(SessionToken sessionID, BasicCollectionNode folder, PresentableMetadata item,
			AsyncCallback<Void> callback);


	void addDerivation(SessionToken sessionID, SearchResult parent, String provenanceStep, SearchResult child,
			AsyncCallback<Void> callback);


	void createInboxFolder(SessionToken sessionID, String project, String path,
			AsyncCallback<IStoredObjectReference> asyncCallback);

	void moveInboxToFolder(SessionToken sessionID, String project, String path,
			AsyncCallback<IStoredObjectReference> asyncCallback);

	void getInboxFolderContents(SessionToken sessionID, IStoredObjectReference folder,
			AsyncCallback<Set<? extends IStoredObjectReference>> callback);


	void getInboxFolderContents(SessionToken sessionID, String thePath, SessionToken sessionID2,
			AsyncCallback<Set<? extends IStoredObjectReference>> asyncCallback);

	void sendFeedback(String user, String message, AsyncCallback<Void> callback);

	void validatePassword(String plaintext, AsyncCallback<Boolean> callback);

}
