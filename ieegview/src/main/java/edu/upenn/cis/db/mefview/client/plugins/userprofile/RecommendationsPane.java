/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Float;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.WebLink;

public class RecommendationsPane extends VerticalPanel 
implements RecommendationsPresenter.Display {
	Presenter presenter;
	ClientFactory clientFactory;

	public RecommendationsPane() {
//		Label recLab = new Label("Messages and Recommendations");
//		recLab.setStyleName("ieeg-user-profile-heading");
//		add(recLab);
	// Add Start Buttons getUserAccReq
      
	  

	  
	  
		ScrollPanel rp = new ScrollPanel();
		HTML h = new HTML("Invite your collaborators and colleagues!");//<b>Marie Curie</b><br/><br/>Look at <b>Snapshot 22</b>");
		add(rp);
		rp.add(h);
//		addStyleName("ieeg-user-recommendations");
	}
	
	public void initialize() {
		
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}

	@Override
	public Presenter getPresenter() {
		return presenter;
	}


	@Override
	public void log(int level, String message) {
		GWT.log(message);
	}

	public void bindDomEvents() {
		// TODO Auto-generated method stub
		
	}
}
