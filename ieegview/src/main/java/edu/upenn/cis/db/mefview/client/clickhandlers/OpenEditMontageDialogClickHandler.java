package edu.upenn.cis.db.mefview.client.clickhandlers;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory;
import edu.upenn.cis.db.mefview.client.dialogs.DialogFactory.DialogInitializer;
import edu.upenn.cis.db.mefview.client.dialogs.IEditMontage;
import edu.upenn.cis.db.mefview.client.dialogs.ISelectMontage;
import edu.upenn.cis.db.mefview.client.events.EegMontageAddedEvent;
import edu.upenn.cis.db.mefview.client.events.EegMontageEditedEvent;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;

public class OpenEditMontageDialogClickHandler implements ClickHandler {
//	private ClientFactory clientFactory;
	private IEditMontage dialog;
	private ISelectMontage selectDialog;
	private IEEGViewerPresenter presenter;
	private Boolean isEdit;

	public static OpenEditMontageDialogClickHandler getHandler(
			ClientFactory cf, IEEGViewerPresenter presenter, ISelectMontage selectDialog, Boolean isEdit) {
		
		final OpenEditMontageDialogClickHandler theHandler = new OpenEditMontageDialogClickHandler(
				cf, selectDialog);

		theHandler.presenter = presenter;
		theHandler.selectDialog = selectDialog;
		theHandler.isEdit = isEdit;
		
		return theHandler;
	}

	protected void init() {
		final List<INamedTimeSegment> traces = presenter.getModel().getTraceInfo();
		
		dialog.reset();
		dialog.setSources(traces);

	}
	
	protected void init(EEGMontage montage) {
		final List<INamedTimeSegment> traces = presenter.getModel().getTraceInfo();
		
		dialog.reset();
		dialog.setSources(traces);
		dialog.setMontage(montage);

	}

	private OpenEditMontageDialogClickHandler(final ClientFactory cf, final ISelectMontage selectDialog) {
//		clientFactory = cf;

		dialog = DialogFactory
				.getEditMontageDialog(new DialogInitializer<IEditMontage>() {

					@Override
					public void init(final IEditMontage objectBeingInited) {

						objectBeingInited.createDialog();

						objectBeingInited.init(new IEditMontage.Handler() {

							@Override
							public void addChannelPair() {
								objectBeingInited.addChannelPair();

							}

							@Override
							public void updateMontage(EEGMontage montage, Boolean isNew) {
								IEEGViewerPanel panel = ((IEEGViewerPanel) presenter
										.getDisplay());
								
								if (isNew){
									IeegEventBusFactory.getGlobalEventBus()
										.fireEvent(
												new EegMontageAddedEvent(panel,
														montage));
								} else {
									IeegEventBusFactory.getGlobalEventBus()
									.fireEvent(
											new EegMontageEditedEvent(presenter,
													montage));
								}
								
								selectDialog.updateSelectMontage(montage, presenter, isNew);

							}

							@Override
							public void addSingleChannelPair() {
								objectBeingInited.addSingleChannelPair();

							}
							
						});

					}

					@Override
					public void reinit(IEditMontage objectBeingInited) {
						// TODO Auto-generated method stub
						
					}

				});
			
	}

	
	public IEditMontage getDialog(){
		return dialog;
	}
	
	
	@Override
	public void onClick(ClickEvent arg0) {
		
		GWT.log("Edit montage opened");
		if (isEdit){
			GWT.log("Can edit");
			this.init(selectDialog.getMontage());
		}else {
			GWT.log("Shouldn't edit");
			this.init();
		}
		
		dialog.open();
		Timer t = new Timer() {
			@Override
			public void run() {
				dialog.focus();
			}
		};
		t.schedule(500);

	}
}
