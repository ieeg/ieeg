/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.web;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Frame;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.actions.OpenURLAction;
import edu.upenn.cis.db.mefview.client.actions.PluginActionFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.DocumentReadyEvent;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.plugins.ActionMapper;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter.Display;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory.PresenterInfo;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.WebLink;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class WebPresenter extends BasicPresenter {
	AppModel control;
	DataSnapshotModel model;
	WebLink url = null; 
//			new WebLink("0", "LONI-IDA", "https://ida.loni.usc.edu/"); 
	
	public interface Display extends BasicPresenter.Display {
		public Frame getWebFrame();
		public WindowPlace getPlace();
	}
	
	public final static String NAME = "url";
	public static final Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.URL);
	final static WebPresenter seed = new WebPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
			
			ActionMapper.getMapper().registerMetadataAction(BASIC_CATEGORIES.Links.name(), 
					OpenURLAction.NAME);
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public WebPresenter() {  super(null, NAME, TYPES); }

	public WebPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory,
			final PresentableMetadata item
			) {
		 super(clientFactory, NAME, TYPES); 
		 GWT.log("WebPresenter");
		this.control = control;
		this.model = model;
		if (item != null && item instanceof WebLink)
			this.url = (WebLink)item;
	  }
	
	@Override
	public Display getDisplay() {
		return (Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		getDisplay().addPresenter(this);
		
		if (url != null) {
			GWT.log("WebPresenter - URL is " + url + " / " + url.getDetails());

			openURL(url.getDetails(), (model == null) ? "" : model.getSnapshotID(), 
					getDisplay().getWebFrame());
		}
		getDisplay().bindDomEvents();
	}

	public void openURL(String url, String snapshot, Frame parent) {
		OpenSignedUrl action = new OpenSignedUrl("", url, snapshot, parent);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}
	
	public WebLink getLink() {
		return url;
	}
	
	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata project, boolean writePermissions) {
		return new WebPresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory, null);
	}
}
