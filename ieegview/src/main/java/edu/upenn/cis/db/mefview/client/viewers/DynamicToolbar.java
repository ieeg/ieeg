/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.viewers;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.view.client.SelectionModel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.actions.IPluginAction;
import edu.upenn.cis.db.mefview.client.actions.PluginActionFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.widgets.DynamicWidgetBar;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

/**
 * A toolbar that responds to changes in selected metadata and adjusts the bar accordingly
 * 
 * @author zives
 *
 */
public class DynamicToolbar extends DynamicWidgetBar<PresentableMetadata, IPluginAction> {
	
	
	public static class ToolbarActionMapper implements 
	DynamicWidgetBar.ActionMapper<PresentableMetadata, IPluginAction> {
		final ClientFactory clientFactory;
		
		public ToolbarActionMapper(ClientFactory cf) {
			this.clientFactory = cf;
		}
		
		edu.upenn.cis.db.mefview.client.plugins.ActionMapper theMapper = 
				edu.upenn.cis.db.mefview.client.plugins.ActionMapper.getMapper();

		@Override
		public List<String> getActions(Collection<PresentableMetadata> selected) {
			return theMapper.getActions(selected);
		}

		@Override
		public IPluginAction createAction(String app,
				Set<PresentableMetadata> selected, Presenter activePresenter) {
			return PluginActionFactory.create(app, clientFactory, selected, activePresenter);
		}

		@Override
		public String getName(IPluginAction action) {
			return action.getName();
		}

		@Override
		public boolean isApplicable(IPluginAction action) {
			return action.isApplicable();
		}

		@Override
		public void execute(IPluginAction action) {
			action.execute();
		}

		@Override
		public HasClickHandlers getWidget(IPluginAction action) {
			
//			Button b = new Button(action.getName());
			
//			b.setHTML("<paper-button>" + action.getName() + "</paper-button>");
			return (HasClickHandlers)WidgetFactory.get().createButton(action.getName());
		}
		
	}


	public DynamicToolbar(SelectionModel<? extends PresentableMetadata> model,
			List<IPluginAction> globalActions, ClientFactory factory) {
		
		super(model, globalActions, new ToolbarActionMapper(factory));
		setStyleName("IeegTabToolbar");     
	}
	
}
