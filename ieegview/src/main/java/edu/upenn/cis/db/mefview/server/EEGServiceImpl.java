/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.Collections.singleton;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.Place;
import com.google.gwt.thirdparty.guava.common.base.Strings;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.JsonKeyValueMapper;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.AuthenticationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.DisabledAccountException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IUserService.UsersAndTotalCount;
import edu.upenn.cis.braintrust.security.IncorrectCredentialsException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.ContactGroupSearch;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.IJsonKeyValue;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.JsonKeyValue;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.LogMessage;
import edu.upenn.cis.braintrust.shared.PatientSearch;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserProfile;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.persistence.IGraphServer.ShareLevel;
import edu.upenn.cis.db.mefview.client.EEGDisplay;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.server.search.SearchServer;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.SnapshotSpecifier;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse.UserIdAndName;
import edu.upenn.cis.db.mefview.shared.Globals;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.LogEntry;
import edu.upenn.cis.db.mefview.shared.PortalSummary;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.ProvenanceSummary;
import edu.upenn.cis.db.mefview.shared.RefreshList;
import edu.upenn.cis.db.mefview.shared.Search;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.SpecialPaths;
import edu.upenn.cis.db.mefview.shared.StripeCustomer;
import edu.upenn.cis.db.mefview.shared.TimeSeriesBookmark;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.UserPrefs;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;
import edu.upenn.cis.eeg.filters.TimeSeriesFilterFactory;
import edu.upenn.cis.eeg.processing.SignalProcessingFactory;
import edu.upenn.cis.events.UserEvent;

//import edu.upenn.cis.db.mefview.shared.ToolInfo;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class EEGServiceImpl extends RemoteServiceServlet implements EEGDisplay {

    //UserInfo Comparator based on how users are displayed for projects.
	private final static class ProjectUserComparator implements
			Comparator<UserInfo> {
		@Override
		public int compare(UserInfo o1, UserInfo o2) {
			return stringToCompare(o1).compareToIgnoreCase(stringToCompare(o2));
		}

		private String stringToCompare(UserInfo ui) {
			final UserProfile p = ui.getProfile();
			String initialString = p.getLastName().isPresent() ? p.getLastName().get() : ui.getName();
			return initialString;
		}
	}

	private final static ProjectUserComparator projectUserComparator = new ProjectUserComparator();

    private final static Logger logger = LoggerFactory.getLogger(EEGServiceImpl.class);
    private final static Logger timeLogger = LoggerFactory.getLogger("time." + EEGServiceImpl.class);

//  private static DocumentBuilderFactory dbf;

  // public static Map<String,List<String>> channels;
  // public static Map<String,Map<String,String>> channelSources;

  public static Map<UserId, Map<String, DataSnapshot>> snapshots = Collections
      .synchronizedMap(new LruCache<UserId, Map<String, DataSnapshot>>(500));

  private static Map<String, TraceInfo> traceDetails = Collections
      .synchronizedMap(new LruCache<String, TraceInfo>(500));

  // public Map<Integer,Event> markedEvents;
  // public Map<String,String> paths;

  private TraceServer traces = TraceServerFactory.getTraceServer();

  private static HabitatUserService uService = null;
  private static IGraphServer crService; 
  private Supplier<PortalSummary> expiringPortalSummarySupplier;

  // private transient IDataSnapshotServer ;
  private transient IUrlFactory imageUrlFactory;
  public static String sourcePath = "traces/";

	// protected transient IDataSnapshotServer
	// uService.getDataSnapshotAccessSession();

	// public static URI brainTrustServicesUri;

  /**
   * Converts from a GWT search parameter object to one used by the Hibernate
   * services
   * 
   * @param s
   * @return
   */
  private static EegStudySearch convertSearch(Search s) {
    EegStudySearch dss = new EegStudySearch();

    ContactGroupSearch electrodeSearch = new ContactGroupSearch();
    dss.setContactGroupSearch(electrodeSearch);

    Set<Location> locations = electrodeSearch.getLocations();
    for (String l : s.getLocations()) {
      locations.add(Location.fromString.get(l));
    }

    electrodeSearch.setSamplingRateMin(s.getSamplingRateMin());
    logger.debug("Min sampling " + electrodeSearch.getSamplingRateMin());

    electrodeSearch.setSamplingRateMax(s.getSamplingRateMax());
    logger.debug("Max sampling " + electrodeSearch.getSamplingRateMax());

    Set<Side> sides = electrodeSearch.getSides();
    for (String side : s.getSides()) {
      sides.add(Side.fromString.get(side));
    }
    logger.debug("Adding sides " + electrodeSearch.getSides().toString());

    Set<ContactType> contactTypes = electrodeSearch.getContactTypes();
    for (String contactType : s.getContactTypes()) {
      contactTypes.add(ContactType.FROM_STRING.get(contactType));
    }
    logger.debug("Adding contact types " + electrodeSearch.getContactTypes());

    PatientSearch patientSearch = new PatientSearch();
    dss.setPatientSearch(patientSearch);
    patientSearch.setAgeOfOnsetMin(s.getAgeOfOnsetMin());
    patientSearch.setAgeOfOnsetMax(s.getAgeOfOnsetMax());

    if (s.getPartialSzTypes()) {
      patientSearch.setPartialSzTypes();
    }
    if (s.getGenSzTypes()) {
      patientSearch.setGeneralizedSzTypes();
    }
    Set<Gender> genders = patientSearch.getGenders();
    for (String g : s.getGenders())
      genders.add(Gender.fromString.get(g));

    Set<SeizureType> szTypes = patientSearch.getSzTypes();
    for (String sz : s.getSzTypes()) {
      SeizureType t = SeizureType.fromString.get(sz);
      szTypes.add(t);
    }

    Set<ImageType> imageTypes = dss.getImageTypes();
    for (String itn : s.getImageTypes())
      imageTypes.add(ImageType.fromString.get(itn));

    dss.setMinSzCount(s.getMinRecordedSzCnt());
    dss.setMaxSzCount(s.getMaxRecordedSzCnt());

    return dss;
  }

  @Override
  public List<LogEntry> getLogEntries(SessionToken sessionID, int startIndex,
      int count) throws BtSecurityException {
    final String m = "getLogEntries()";
    try {
      List<LogMessage> entries = uService.getDataSnapshotAccessSession()
          .getLogEntries(uService.verifyUser(m, sessionID, null), startIndex,
              count);

      return DataMarshalling.convertLogMessagesToEntries(entries, uService);
    } catch (Throwable t) {
      logger.error(m + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  public final String path = "traces/Mayo_IEED_data/Mayo_IEED_";
  //
  Integer remaining = -1;
  //
  ITimeSeriesPageServer server;

  public EEGServiceImpl() {
//    if (dbf == null) {
//      dbf = DocumentBuilderFactory.newInstance();
//      dbf.setIgnoringElementContentWhitespace(true);
//    }
  }

  // //////////////////// Image services
  // ///////////////////////////////////////

  @VisibleForTesting
  EEGServiceImpl(final IUserService userService,
      final IDataSnapshotServer dsServer) {

    uService = HabitatUserServiceFactory.getUserService(userService, dsServer);
    crService = CoralReefServiceFactory.getGraphServer();
    // this.dus = userService;
//    if (dbf == null) {
//      dbf = DocumentBuilderFactory.newInstance();
//      dbf.setIgnoringElementContentWhitespace(true);
//    }
  }

  // /////////////////////////// Dataset services
  // /////////////////////////////

  @Override
  public void addLogEntry(SessionToken sessionID, LogEntry entry)
      throws BtSecurityException {
    User u = uService.verifyUserError("addLogEntry()", sessionID, null, logger);
    logger.info("LogEntry: "+entry.toString() );
// todo: restore this
//    uService.getDataSnapshotAccessSession().addLogEntry(u,
//        DataMarshalling.convertLogEntryToMessage(entry, uService));
  }

  @Override
  public Long addPosting(SessionToken sessionID, String dsRevId, Post post)
      throws BtSecurityException {
    final String m = "addPosting()";
    try {
      User user = uService.verifyUser(m, sessionID, null);
      Long ret = uService.getDataSnapshotAccessSession().addPosting(user,
          dsRevId, post);
      GWT.log("Posting " + ret);
      return ret;
    } catch (Exception e) {
      logger.error(m + ": Caught exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public void addProvenanceRecord(SessionToken sessionID, ProvenanceLogEntry op)
      throws BtSecurityException {
    final String m = "addProvenanceRecord()";
    try {
      User user = uService.verifyUser(m, sessionID, null);

      uService.getDataSnapshotAccessSession().addProvenanceEntry(user,
          op);
    } catch (DataSnapshotNotFoundException dnf) {
    	// This is OK
    } catch (Exception e) {
      logger.error(m + ": Caught exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public String addShortURL(SessionToken sessionID, TimeSeriesBookmark book)
      throws BtSecurityException {
    final String m = "addShortURL(...)";
    try {
      uService.verifyUser(m, sessionID, Role.USER);
      return null;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public String addUserBookmark(SessionToken sessionID, TimeSeriesBookmark book)
      throws BtSecurityException {
    final String m = "addUserBookmark(...)";
    try {
      uService.verifyUser(m, sessionID, Role.USER);
      return null;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }

  }

  @Override
  public String addUserRestoreMarks(SessionToken sessionID,
      TimeSeriesBookmark book) throws BtSecurityException {
    final String m = "addUserRestoreMarks(...)";
    try {
      uService.verifyUser(m, sessionID, Role.USER);
      return null;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public SearchResult deriveSnapshot(SessionToken sessionID,
      final String studyRevId, final String friendlyName, final String toolName)
      throws BtSecurityException {

    final String M = "deriveSnapshot";
    try {
      String snap = uService.getDataSnapshotAccessSession().deriveDataset(
          uService.verifyUser(M, sessionID, Role.USER), studyRevId,
          friendlyName, toolName, null, null);

      return getSearchResultForSnapshot(sessionID, snap);
    } catch (Throwable t) {
      logger.error("Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public SearchResult deriveSnapshot(SessionToken sessionID,
      final String studyRevId, final String friendlyName,
      final String toolName, final List<String> channelIDs,
      boolean includeAnnotations) throws BtSecurityException {

    String m = "deriveSnapshot";
    try {

      Set<String> seriesToAdd = new HashSet<String>();
      seriesToAdd.addAll(channelIDs);

      // Null means include all annotations from seriesToAdd, empty means
      // includes no annotations
      Set<String> annToAdd = includeAnnotations ? null : new HashSet<String>();

      final String resultsDataSnapshotRevId = uService
          .getDataSnapshotAccessSession().deriveDataset(
              uService.verifyUser(m, sessionID, Role.USER), studyRevId,
              friendlyName, toolName, seriesToAdd, annToAdd);
      return getSearchResultForSnapshot(sessionID, resultsDataSnapshotRevId);
    } catch (Throwable t) {
      logger.error("Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public void destroy() {
    // server.shutdown();
    // server = null;
    traces.closeDown();
    // channels.clear();
    // channelSources.clear();
    // markedEvents.clear();
    // paths.clear();
  }

  @Override
  public List<EditAclResponse> editAcl(SessionToken sessionID,
      String entityType, String mode, List<IEditAclAction<?>> actions)
      throws BtSecurityException {
    final String m = "editAcl(...)";
    try {
      final User user = uService.verifyUser(m, sessionID, Role.USER);
      final List<EditAclResponse> results = uService
          .getDataSnapshotAccessSession().editAcl(user,
              HasAclType.fromString(entityType), mode, actions);
      return results;
    } catch (Throwable t) {
      logger.error("Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }

  }

	@Override
	public EEGMontage editMontage(SessionToken sessionID, EEGMontage montage,
			String snapshotLabel) throws EegMontageNotFoundException {
		final String m = "editMontage(...)";
		try {

			User user = uService.verifyUser(m, sessionID, Role.USER);

			// logger.debug("{} Adding to DB: {}",m,newMontage);
			IDataSnapshotServer dsServer = uService.datasets;

			EEGMontage out = dsServer.createEditEegMontage(user, montage,
					snapshotLabel);
			return out;
		} catch (EegMontageNotFoundException e) {
			logger.error(m + ": Could not find montage to edit", e);
			throw e;
		} catch (RuntimeException e) {
			logger.error(m + ": Caught Exception", e);
			throw e;
		}
		
	}
  
  @Override
  public GetAcesResponse getAces(SessionToken sessionID, String entityType,
      String mode, String targetId) throws BtSecurityException {
    final String M = "getAces(...)";
    try {
      final User loggedInUser = uService.verifyUser(M, sessionID, Role.USER);
      final GetAcesResponse acesResponse = uService
          .getDataSnapshotAccessSession().getAcesResponse(loggedInUser,
              HasAclType.fromString(entityType), mode, targetId);
      return acesResponse;
    } catch (Throwable t) {
      logger.error(M + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<UserInfo> getAllUsers(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getAllUsers()";
    try {
      uService.verifyUser(m, sessionID, Role.USER);

      UsersAndTotalCount uac = uService.getUsersAndCount();// .findEnabledUsers(0,
                                                           // Integer.MAX_VALUE,
      // new HashSet<UserId>());

      List<UserInfo> ret = new ArrayList<>();
      for (User user2 : uac.users) {
        UserInfo ui = new UserInfo(user2.getUsername(), user2.getPhoto(),
            user2.getUserId());

        ui.setProfile(user2.getProfile());
        ret.add(ui);
      }
      Collections.sort(ret, projectUserComparator);
      return ret;
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<AnnotationDefinition> getAnnotationTypes(SessionToken sessionID,
      String viewerType) throws BtSecurityException {
    final String m = "getAnnotationInfo()";
    try {
      User user = uService.verifyUser(m, sessionID, null);

      return uService.getDataSnapshotAccessSession().getAnnotationDefinitions(
          user, viewerType);
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<SignalProcessingStep> getSignalProcessingTypes(
      SessionToken sessionID, String viewerType) throws BtSecurityException {
    final String m = "getAnnotationInfo()";
    try {
      uService.verifyUser(m, sessionID, null);

      return SignalProcessingFactory.getRegisteredAlgorithms();
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<String> getAvailableFilterTypes() {
    List<String> ret = Lists.newArrayList();
    ret.addAll(TimeSeriesFilterFactory.getFilterTypes());
    Collections.sort(ret);

    return ret;
  }

  /**
   * Retrieves data as a list of strings (one per channel) using
   * differentially-encoded integers
   * 
   * @param study
   *          Name of study
   * @param traceNames
   *          List of trace names
   * @param requestedStart
   *          Start time (UUTC)
   * @param requestedDuration
   *          Duration (usec)
   * @param requestedSamplingPeriod
   *          Sampling period (usec)
   * @return
   * @throws ServerTimeoutException
   *           TODO
   */

  @Override
  public List<EEGMontage> getMontages(SessionToken sessionID,
      String dataSnapshot) throws BtSecurityException {
    final String M = "getMontages()";
    try {
      User theUser = uService.verifyUser(M, sessionID, null);
      return uService.getMontagesHelper(theUser, dataSnapshot);
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

//  List<TraceInfo> getTraceInfo(SessionToken sessionID, User user,
//      String dataSnapshot) throws IllegalArgumentException, IOException {
//    List<TraceInfo> channelList = new ArrayList<TraceInfo>();
//
//    SearchResult parent = getSearchResultForSnapshot(sessionID, dataSnapshot);
//    DataSnapshot dss = uService.getDataSnapshotAccessSession().getDataSnapshot(
//        user, dataSnapshot);
//    Set<TimeSeriesDto> series = dss.getTimeSeries();
//    List<TimeSeriesDto> seriesList = new ArrayList<TimeSeriesDto>();
//    seriesList.addAll(series);
//
//    List<ChannelSpecifier> channels = getChannelSpecifiers(user, dataSnapshot,
//        seriesList);
//
//    List<ChannelInfoDto> channelDtos = uService.getDataSnapshotAccessSession()
//        .getChannelInfo(dataSnapshot);
//
//    if (channelDtos.isEmpty()) {
//      channelDtos = traces.getTimeSeriesChannelInfo(seriesList, channels);
//      channelDtos = uService.getDataSnapshotAccessSession().writeChannelInfo(
//          dataSnapshot, channelDtos);
//
//    }
//    channelList = DataMarshalling.convertChannelInfoToTraceInfo(
//        getSearchResultForSnapshot(sessionID, dataSnapshot), channelDtos);
//
//    Set<String> existing = new HashSet<String>();
//    List<Integer> remove = new ArrayList<Integer>();
//    int i = 0;
//    for (TraceInfo trace : channelList) {
//      trace.setParent(parent);
//      if (existing.contains(trace.getRevId()))
//        remove.add(i);
//      else
//        existing.add(trace.getRevId());
//      i++;
//    }
//
//    for (int j = remove.size() - 1; j >= 0; j--)
//      channelList.remove(remove.get(j));
//
//    return channelList;
//  }

  @Override
  public GetUsersResponse getEnabledUsers(SessionToken sessionID, int start,
      int length, Set<UserId> excludedUsers) throws BtSecurityException {
    final String M = "getEnabledUsers(...)";
    try {
      uService.verifyUser(M, sessionID, Role.USER);

      UsersAndTotalCount usersAndTotalCount = uService.getUsersAndCount(start, length, excludedUsers);
      final List<UserIdAndName> users = newArrayList();
      for (final User user : usersAndTotalCount.users) {
        users.add(new UserIdAndName(user.getUserId(), user.getUsername()));
      }
      return new GetUsersResponse(users, (int) usersAndTotalCount.totalCount);


    } catch (Throwable t) {
      logger.error(M + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public double getNextValidDataRegion(SessionToken sessionID,
      String dataSetID, double position) throws BtSecurityException {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public List<Place> getPlaces(SessionToken sessionID, String dataSnapshotRevId)
      throws BtSecurityException {
    final String M = "getPlaces()";
    try {
      uService.verifyUser(M, sessionID, null);
      List<Place> ret = new ArrayList<Place>();

      // TODO Auto-generated method stub
      return ret;
    } catch (Throwable t) {
      logger.error(M + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }
  
  @Override
  public PortalSummary getPortalSummary(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getPortalSummary(...)";
    try {
      uService.verifyUser(m, sessionID, null);
      final PortalSummary summary = expiringPortalSummarySupplier.get();
      return summary;
    } catch (Throwable t) {
      logger.error(m + " : Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<Post> getPostings(SessionToken sessionID, String dsRevId,
      int postIndex, int numPosts) throws BtSecurityException {
    final String m = "getPostings()";
    try {
      User user = uService.verifyUser(m, sessionID, null);
      List<Post> postList = uService.getDataSnapshotAccessSession()
          .getPostings(user, dsRevId, postIndex, numPosts);

      return postList;
    } catch (Exception e) {
      logger.error(m + ": Caught exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public double getPreviousValidDataRegion(SessionToken sessionID,
      String dataSetID, double position) throws BtSecurityException {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public List<ToolDto> getRegisteredTools(SessionToken sessionID)
      throws BtSecurityException {
    final String M = "getRegisteredTools()";
    try {
      User user = uService.verifyUser(M, sessionID, Role.USER);
      Set<ToolDto> tools = uService.getDataSnapshotAccessSession().getTools(
          user);
      List<ToolDto> toolsList = new ArrayList<ToolDto>();
      for (ToolDto toolDto : tools) {
        toolsList.add(toolDto);
      }
      return toolsList;
    } catch (Throwable t) {
      logger.error(M + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  /**
   * Note that this is something of a duplicate for the method in
   * SnapshotServiceImpl
   * 
   * @param sessionID
   * @param datasetNameOrId
   * @return
   * @throws BtSecurityException
   */
  private SearchResult getSearchResultForSnapshot(SessionToken sessionID,
      final String datasetNameOrId) throws BtSecurityException {
    final String M = "getSearchResultForSnapshot()";
    try {
      User u = uService.verifyUser(M, sessionID, null);

      String dataSnapshotId = null;

      try {
        dataSnapshotId = uService.getDataSnapshotAccessSession()
            .getDataSnapshotId(u, datasetNameOrId, false);
      } catch (DataSnapshotNotFoundException e) {
        // yes I'm using an exception incorrectly for normal program
        // flow
        dataSnapshotId = datasetNameOrId;
      }

      Set<String> dsRevIds = singleton(dataSnapshotId);

      Set<SearchResult> sr = DataMarshalling.convertSearchResults(uService
          .getDataSnapshotAccessSession().getLatestSnapshots(u, dsRevIds), u,
          uService);

      // There should be 0 or 1 elements
      if (sr.isEmpty())
        return null;
      else
        return sr.iterator().next();
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  private DataSnapshot getSnapshot(User u, String datasetID) {

    DataSnapshot ds;
    UserId userId = u.getUserId();

    Map<UserId, Map<String, DataSnapshot>> snapshotsLocal = snapshots;

    // Don't use the cache for data enterers - so they see changes during
    // data
    // entry
    if (u.isAdmin()) {
      snapshotsLocal = newHashMap();
    }

    if (snapshotsLocal.containsKey(userId)
        && snapshotsLocal.get(userId).containsKey(datasetID))
      return snapshotsLocal.get(userId).get(datasetID);

    ds = uService.getDataSnapshotAccessSession().getDataSnapshot(u, datasetID);

    if (!snapshotsLocal.containsKey(userId))
      snapshotsLocal.put(userId, new HashMap<String, DataSnapshot>());
    snapshotsLocal.get(userId).put(datasetID, ds);
    return ds;
  }

  @Override
  public List<String> getSystemState(SessionToken sessionID)
      throws BtSecurityException {
    final String M = "getSystemState()";
    try {
      ArrayList<String> ret = new ArrayList<String>();

      uService.verifyUser(M, sessionID, Role.ADMIN);

      ret.add("*** Snapshots (" + snapshots.size() + "): ***");
      for (UserId userId : snapshots.keySet()) {
        ret.add("> Snapshot " + userId + ":");
        for (String series : snapshots.get(userId).keySet()) {
          ret.add(series);
        }
      }

      ret.add("*** Page Server Info: ***");
      ret.addAll(traces.getPageServerState());

      return ret;
      // } catch (UnauthorizedException e) {
      // logger.error("User not authorized to perform this operation", e);
      // throw e;
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<ToolDto> getTools(SessionToken sessionID, Set<String> toolRevIds)
      throws BtSecurityException {
    final String M = "getTools()";
    try {
      User theUser = uService.verifyUser(M, sessionID, Role.USER);
      return uService.getToolsHelper(theUser, toolRevIds);
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public Set<TimeSeriesBookmark> getUserBookmarks(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getUserBookmarks(...)";
    try {
      uService.verifyUser(m, sessionID, Role.USER);
      return null;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  /**
   * Temporary convenience method to support getting an ieeg session for a
   * a Blackfynn logged in user
   * @param userName
   * @return
     */
  @Override
  public UserInfo getUserInfo(String userName) {
    User user = uService.getUserByName(userName);
    final SessionToken sessionToken = uService.getSessionManager().createSession(user);
    UserInfo ret = new UserInfo(sessionToken, user.getPhoto(), user.getUserId());
    ret.setName(user.getUsername());
    ret.setProfile(user.getProfile());
    return ret;
  }

  @Override
  public UserInfo getUserId(String userName, String password)
      throws AuthenticationException {
    final String M = "getUserId(...)";

    boolean foundUser = false;
    boolean userIsEnabled = false;
    try {

      User user = uService.getUserByName(userName);

      if (user == null)
        throw new IncorrectCredentialsException(
            "Account or password not found!");
      foundUser = true;

      if (user.getStatus() != User.Status.ENABLED) {
        UserEvent.logEvent(UserEvent.EventType.FAILED_LOGIN, userName +" login attempt, currently disabled.");
        throw new DisabledAccountException("Account or password not found!");
      }
      userIsEnabled = true;

      uService.getDataSnapshotAccessSession().createUserIfNecessary(user);

      String pwd = password;// DigestUtils.md5Hex(password);

      if (pwd.equals(user.getPassword()) || user.getUsername().equals(Globals.GUEST_USERNAME)) {
        final SessionToken sessionToken = uService.getSessionManager().createSession(user);
        UserInfo ret = new UserInfo(sessionToken, user.getPhoto(), user.getUserId());

        ret.setName(user.getUsername());
        ret.setProfile(user.getProfile());

        if (user.getUsername().equals(Globals.GUEST_USERNAME))
          // if (user.getRoles().size() == 1 &&
          // user.getRoles().contains(Role.GUEST))
          ret.setGuest(true);

        UserEvent.logEvent(UserEvent.EventType.LOGIN, userName);

        return ret;
      } else {
        throw new IncorrectCredentialsException("Account or password not found!");
      }
    } catch (AuthenticationException e) {
      String msg = null;

      if (foundUser) {
        if (userIsEnabled) {
          msg = "Password for user [" + userName + "] did not match.";
        } else {
          msg = "user [" + userName + "] was not enabled";
        }
      } else {
        msg = "User [" + userName + "] not found.";
      }
      logger.info("{}: {}", M, msg);
      UserEvent.logEvent(UserEvent.EventType.FAILED_LOGIN, msg);
      throw e;
    } catch (Throwable t) {
      logger.error(M + ": Caught Exception", t);
      throw propagate(t);
    }
  }

  @Override
  public UserInfo getUserInfo(SessionToken sessionID, String requestedUserName)
      throws BtSecurityException {
    final String m = "getUserInfo(...)";
    try {
      uService.verifyUser(m, sessionID, null);
      return uService.getUserInfoHelper(requestedUserName);
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public ProvenanceSummary getUserProvenanceSummary(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getUserProvenanceSummary()";
    try {
      User user = uService.verifyUser(m, sessionID, null);

      int numSnapshots = 0;
      int snapshotViews = 0;
      int snapshotAnnotations = 0;
      int snapshotAnalyses = 0;
      double score = 0;
      int rank = 0;

      numSnapshots = uService.getDataSnapshotAccessSession()
          .getSnapshotsByUser(user);
      snapshotAnnotations = uService.getDataSnapshotAccessSession()
          .getAnnotationsByUser(user);

      List<ProvenanceLogEntry> prov = uService.getDataSnapshotAccessSession()
          .getProvenanceEntriesForUser(user);

      Set<String> snapshots = Sets.newHashSet();
      for (ProvenanceLogEntry p : prov) {
        if (p.getUsage() == ProvenanceLogEntry.UsageType.VIEWS)
          snapshotViews++;

        if (p.getUsage() == ProvenanceLogEntry.UsageType.DERIVES) {
          snapshots.add(p.getDerivedSnapshot());
        }
      }
      snapshotAnalyses = snapshots.size();

      ProvenanceSummary ret = new ProvenanceSummary(numSnapshots,
          snapshotAnnotations, snapshotViews, snapshotAnalyses, score, rank);

      return ret;
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public Set<TimeSeriesBookmark> getUserRestoreMarks(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getUserRestoreMarks(...)";
    try {
      uService.verifyUser(m, sessionID, Role.USER);
      return null;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public Boolean indexDataSnapshot(SessionToken sessionID,
      String dataSnapshotRevId, double sampleRate, double pageLength)
      throws BtSecurityException {
    final String M = "indexDataSnapshot()";
    try {
      User u = uService.verifyUser(M, sessionID, Role.USER);

      traces.indexDataset(u, dataSnapshotRevId, sampleRate, pageLength);
      return new Boolean(true);
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public void init() {

    // Only initialize if there isn't an existing instance
    if (uService == null) {
      logger.info("IEEG STARTUP: Launching new server process");
      @SuppressWarnings("unchecked")
      Map<String, String> ivProps = ((Map<String, String>) getServletContext()
          .getAttribute(IvProps.ATTR_KEY));

      sourcePath = BtUtil.get(ivProps, IvProps.SOURCEPATH, sourcePath);

      final int sessionTimeoutMinutes = IvProps.getSessionExpirationMins();
      final int sessionCleanupMins = IvProps.getSessionCleanupMins();
      final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();

      uService = HabitatUserServiceFactory.getUserService();

      uService.createSessionManager(uService.getDataSnapshotAccessSession(),
          sessionTimeoutMinutes, sessionCleanupMins, sessionCleanupDenom);

      server = traces.getMEFServer();
      crService = CoralReefServiceFactory.getGraphServer();
      
      expiringPortalSummarySupplier = Suppliers.memoizeWithExpiration(
    		  new PortalSummarySupplier(uService), 
    		  7, 
    		  TimeUnit.DAYS);
      
    } else
      logger.info("IEEG STARTUP: Reusing server process");
  }

  @VisibleForTesting
  void initTest(ISessionManager manager) {
    // Only initialize if there isn't an existing instance
    if (sourcePath.equals("traces/")) {
      IvProps.init(".");
      sourcePath = BtUtil.get(IvProps.getIvProps(), IvProps.SOURCEPATH,
          sourcePath);
      logger.info("IEEG STARTUP: Launching new server process");

      boolean prefetch = false;
      boolean lazyIndex = true;
      int threads = 10;
      int pages = MultithreadedPageServer.DEFAULT_CACHE;

      // final int sessionTimeoutMinutes =
      // IvProps.getSessionExpirationMins();
      // final int sessionCleanupMins = IvProps.getSessionCleanupMins();
      // final int sessionCleanupDenom = IvProps.getSessionCleanupDenom();

      // if (uService.getDataSnapshotAccessSession() == null)
      uService.setDataSnapshotAccessSession(DataSnapshotServerFactory
          .getDataSnapshotServer());

      uService.setSessionManager(manager);
      // new SessionManager(
      // uService.getDataSnapshotAccessSession(),
      // sessionTimeoutMinutes,
      // sessionCleanupMins,
      // sessionCleanupDenom);

      server = traces.getMEFServer();
    } else
      logger.info("IEEG STARTUP: Reusing server process");
  }

  @Override
  public WorkPlace loadWorkPlace(SessionToken sessionID) throws BtSecurityException {
    final String M = "loadWorkPlace()";
    try {
      String clob = uService.getDataSnapshotAccessSession().getUserClob(uService.verifyUser(M, sessionID, null));

      WorkPlaceSerializable wp = new WorkPlaceSerializable();

      JsonKeyValueMapper mapper = JsonKeyValueMapper.getMapper();

      // if the clob is null, avoid an NPE
      Set<IJsonKeyValue> theSet = null;
      if (clob != null) {
    	  try {
		        theSet = mapper.getKeyValueSet(clob);
		
		        for (IJsonKeyValue pair : theSet) {
		          if (pair.getKey().equals("userPreferences")) {
		            UserPrefs prefs = (UserPrefs) ((JsonKeyValue) pair).getJson();
		            wp.annSchemes = prefs.getAnnSchemes();
		
		          }
		          if (pair.getKey().equals("savedState")) {
		            WorkPlaceSerializable wp2 = (WorkPlaceSerializable) ((JsonKeyValue) pair).getJson();
		            wp.favoriteStudies = wp2.favoriteStudies;
		            wp.contexts = wp2.contexts;
		          }
		        }
    	  } catch (Exception e) {
    		  logger.info("Unable to restore workplace CLOB");
    		  return null;
    	  }
      }

      if (wp != null)
        return PlaceConverter.getGWTVersion(wp);
      else
        return null;
//    } catch (JsonMappingException jme) {
//      return null;
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public void logoutSession(SessionToken sessionID) throws BtSecurityException {
    final String M = "logoutSession(...)";
    try {
      final User user = uService.verifyUser(M, sessionID, null);
      uService.getSessionManager().logoutSession(sessionID);
      logger.info("{}: Logged out session for user {}", M, user.getUsername());
    } catch (Throwable t) {
      logger.error(M + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public RefreshList pollForUpdates(SessionToken sessionID, Timestamp sinceWhen)
      throws BtSecurityException {
    final String m = "pollForUpdates()";
    try {
      RefreshList ret = new RefreshList();

      List<LogMessage> entries = uService.getDataSnapshotAccessSession()
          .getLogEntriesSince(uService.verifyUser(m, sessionID, null),
                  sinceWhen);

      ret.setNewEvents(DataMarshalling.convertLogMessagesToEntries(entries,
              uService));

      return ret;
    } catch (Exception e) {
      logger.error(m + ": Caught Exception", e);
      propagateIfInstanceOf(e, BtSecurityException.class);
      throw propagate(e);
    }
  }

  @Override
  public void registerActivity(SessionToken sessionID, DIRECTION direction,
      long time, List<String> channels, List<DisplayConfiguration> filters,
      Set<Annotation> activeAnnotations) throws BtSecurityException {
    final String M = "registerActivity()";
    try {
      uService.verifyUser(M, sessionID, null);

    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<String> registerTool(SessionToken sessionID,
      List<ToolDto> toolList) throws BtSecurityException {
    final String M = "registerTool()";
    try {
      User theUser = uService.verifyUser(M, sessionID, Role.USER);
      List<ToolDto> tools = new ArrayList<ToolDto>();
      for (ToolDto tool : toolList)
        tools.add(tool);

      List<String> results;

      results = uService.getDataSnapshotAccessSession().storeTools(theUser,
          tools);

      if (results.isEmpty())
        return null;
      else
        return results;
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public Boolean removeTool(SessionToken sessionID, Set<String> toolRevIds)
      throws BtSecurityException {
    final String M = "removeTool()";
    try {
      uService.getDataSnapshotAccessSession().removeTools(
          uService.verifyUser(M, sessionID, Role.USER), toolRevIds);
      return new Boolean(true);
    } catch (Throwable t) {
      logger.error(M + ": Caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }

  }

  @Override
  public Boolean saveWorkPlace(SessionToken sessionID, WorkPlace place,
      UserPrefs prefs) throws BtSecurityException {
    final String M = "saveWorkPlace()";
    try {
      // We allow expired sessions to save workspace
      User user = uService.verifyUser(M, sessionID, true, Role.USER);
      saveWorkPlaceHelper(user, place, prefs);
      return new Boolean(true);
    } catch (Throwable t) {
      logger.error(M + ": Caught Exception", t);
      Throwables.propagateIfInstanceOf(t, BtSecurityException.class);
      throw Throwables.propagate(t);
    }
  }

  @Override
  public void saveWorkPlaceAndLogout(SessionToken sessionID, WorkPlace place,
      UserPrefs prefs) throws BtSecurityException {
    final String M = "saveWorkPlaceAndLogout(...)";
    try {
      final User user = uService.verifyUser(M, sessionID, null);
      saveWorkPlaceHelper(user, place, prefs);
      uService.getSessionManager().logoutSession(sessionID);
      logger.info("{}: Saved WorkPlace and logged out session for user {}", M,
              user.getUsername());
    } catch (Throwable t) {
      logger.error(M + ": caught exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }

  }

  private void saveWorkPlaceHelper(User user, WorkPlace place, UserPrefs prefs) {
    final String m = "saveWorkPlaceHelper(...)";
    WorkPlaceSerializable wp = PlaceConverter.getSerializableVersion(place);

    JsonKeyValueMapper mapper = JsonKeyValueMapper.getMapper();

    // Workspace state
    JsonKeyValue jkv = new JsonKeyValue("savedState", wp);

    JsonKeyValue jkv2 = new JsonKeyValue("userPreferences", prefs);

    Set<JsonKeyValue> valueSet = new HashSet<JsonKeyValue>();

    valueSet.add(jkv);
    valueSet.add(jkv2);

    String toSerialize;
    try {
      toSerialize = mapper.createKeyValueSet(valueSet);

      System.out.println("Serialized workspace as " + toSerialize);

      logger.debug("{}: WorkPlace: {}", m, toSerialize);// sw.toString());
      uService.getDataSnapshotAccessSession().storeUserClob(user, toSerialize);
    } catch (JsonProcessingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  private List<ChannelSpecifier> getChannelSpecifiers(User user,
      SnapshotSpecifier snap, Collection<TimeSeriesDto> tsList) {
    List<ChannelSpecifier> ret = new ArrayList<ChannelSpecifier>();
    for (TimeSeriesDto ts : tsList)
      ret.add(traces.getChannelSpecifier(user, snap, ts.getId(), 0));

    return ret;
  }

  private List<ChannelSpecifier> getChannelSpecifiers(User user, String snap,
      Collection<TimeSeriesDto> tsList) {
    return getChannelSpecifiers(user, traces.getSnapshotSpecifier(user, snap),
        tsList);
  }

  @Override
  public void reIndexDatabase(SessionToken sessionID)
      throws BtSecurityException {
    try {
      SearchServer.getServer().reIndexDatabase();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

	@Override
	public ServerConfiguration getServerConfiguration(SessionToken token) {
		Map<String,String> otherParameters = new HashMap<String,String>();

		boolean includeDetails = false;
		// Include all parameters except those dealing with paths
		User user = null;
		try {
			if (token != null) {
		      user = uService.verifyUser("getServerConfiguration", token, null);
		      includeDetails = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (includeDetails)
			for (String p: IvProps.getIvProps().keySet()) {
				if (!p.toLowerCase().contains("path") &&
						!p.toLowerCase().contains("salt") &&
						!p.toLowerCase().contains("pass"))
					otherParameters.put(p, IvProps.getIvProps().get(p));
			}
		ServerConfiguration ret = new ServerConfiguration(IvProps.isUniversity(), IvProps.doNeedResearchAgreement(),
				IvProps.doNeedAuthorization(), IvProps.getSiteName(), IvProps.getSiteUrl(), IvProps.getDygraphLogLevel(), otherParameters);
		
		if (user != null) {
			List<AnnotationDefinition> defs = uService.getDataSnapshotAccessSession().getAnnotationDefinitions(
              user, "eeg");
			ret.setAnnotationDefinitions(defs);
		}
		try {
			// This is just to see if we get a classnotfoundexception
            @SuppressWarnings("unchecked")
			Class<IGraphServer> exampleClass = 
            		(Class<IGraphServer>) Class.forName("edu.upenn.cis.db.habitat.persistence.CoralReefServer");
            
            ret.setKeywordSearch(true);
            ret.setServerName("Blackfynn");
        } catch (ClassNotFoundException e) {
            ret.setKeywordSearch(false);
        }		
		return ret;
	}


  @Override
  public void changePassword(SessionToken sessionID, String oldMd5, String newMd5) throws BtSecurityException {
    throw new RuntimeException("not implemented");
  }

  @Override
  public void changePassword(String oldPlaintext, String newPlaintext) {
    String user = SecurityUtils.getSubject().getPrincipal().toString();
    logger.info("Attempting change password for user: "+user);
    boolean validOld = validatePassword(oldPlaintext);
    if (validOld) {
      logger.info("Valid old password for change, setting new");
      ChangePassword changePassword= new ChangePassword(user, newPlaintext);
      HibernateUtil.getSessionFactory().getCurrentSession().doWork(changePassword);
    } else {
      //application flow should make it difficult to get here, so this is not a normal error flow path
      logger.info("Invalid old password change for user:"+user);
      throw new RuntimeException("Invalid old password");
    }

  }

  /**
   * Helper class change a password
   */
  private static class ChangePassword implements Work {
    private final String userID;
    private final String password;

    public ChangePassword(String userID, String password) {
      this.password = password;
      this.userID = userID;
    }

    public void execute(Connection connection) throws SQLException {
      PreparedStatement ps = connection.prepareStatement("UPDATE dbGroup.users SET pass=? WHERE name=?");
      String hash = new Md5Hash(this.password).toHex();
      ps.setString(1, hash);
      ps.setString(2, userID);
      int r = ps.executeUpdate();
      if (r != 1) {
        throw new RuntimeException("Unexpected results updating users - expected 1 got "+r);
      }
      logger.info("Update completed:" + r);
    }
  }

  @Override
  public boolean validatePassword(String plaintext) {
    String user = SecurityUtils.getSubject().getPrincipal().toString();
    ValidatePassword validatePassword = new ValidatePassword(user, plaintext);
    HibernateUtil.getSessionFactory().getCurrentSession().doWork(validatePassword);
    return validatePassword.success;
  }

  /**
   * Helper class validate a password
   */
  private static class ValidatePassword implements Work {
    private final String userID;
    private final String password;
    boolean success = false;

    public ValidatePassword(String userID, String password) {
      this.password = password;
      this.userID = userID;
    }

    public void execute(Connection connection) throws SQLException {
      PreparedStatement ps = connection.prepareStatement("select name from dbGroup.users where `name`=? and pass=?");
      ps.setString(1, userID);
      final String hex = new Md5Hash(this.password).toHex();
      ps.setString(2, hex);
      logger.info("select name from dbGroup.users where uid=? and pass=? "+userID+" "+hex);
      ResultSet rs = ps.executeQuery();
      if (rs.next()){
        success = true;
        logger.info("Validated existing pwd for "+userID);
      } else {
        logger.info("Failed validation on existing pwd for "+userID);
      }
    }
  }

  @Override
  public UserInfo createUser(UserAccountInfo accountInfo, String encryptPassword) throws BtSecurityException {
    logger.info("createUser");
    try {

      // create Drupal/mysql user
      User theUser = uService.createUser(accountInfo, encryptPassword);
      logger.info("Mysql user created:"+theUser.toString());

      // Create CoralReef user, this is a little hokey, since we don't have 2PC if one step fails
      if (crService != null) {
        crService.createUser(theUser, encryptPassword);
      }

      UserEvent.logEvent(UserEvent.EventType.REGISTRATION, accountInfo.getLogin());

      return uService.getUserInfoHelper(accountInfo.getLogin());
    } catch (Throwable t) {
      logger.error("Createuser: Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  /**
   * Create a Stripe subscription for the user
   * @param token
   * @param userID
   */
  @Override
  public StripeCustomer createSubscription(String token, String userID) {
    Stripe.apiKey = IvProps.getStripeLiveSecretKey();

    logger.debug("EEGServiceImpl.createSubscription: "+userID);
    StripeCustomer stripeCustomer = new StripeCustomer();

    Map<String, Object> customerParams = new HashMap<String, Object>();
    customerParams.put("email", userID);
    customerParams.put("plan", "basic");
    customerParams.put("source", token);

    try {
      Customer c = Customer.create(customerParams);
      HibernateUtil.getSessionFactory().getCurrentSession().doWork(new CreateStripeUser(userID, c.getId()));
      stripeCustomer.stripe_id = c.getId();
      UserEvent.logEvent(UserEvent.EventType.SUBSCRIBE,userID);

    } catch (com.stripe.exception.AuthenticationException e) {
      logger.error(this.getClass().getName(),e);
      throw new RuntimeException(e);
    } catch (InvalidRequestException e) {
      logger.error(this.getClass().getName(), e);
      throw new RuntimeException(e);
    } catch (APIConnectionException e) {
      logger.error(this.getClass().getName(), e);
      throw new RuntimeException(e);
    } catch (CardException e) {
      logger.error(this.getClass().getName(), e);
      throw new RuntimeException(e);
    } catch (APIException e) {
      logger.error(this.getClass().getName(), e);
      throw new RuntimeException(e);
    }
    return stripeCustomer;
  }

  /**
   * Helper class to create a user/stripe link
   */
  private static class CreateStripeUser implements Work {
    private final String userID;
    private final String stripeID;

    public CreateStripeUser(String userID, String stripeID) {
      this.userID = userID;
      this.stripeID = stripeID;
    }

    public void execute(Connection connection) throws SQLException {
      PreparedStatement ps = connection.prepareStatement("insert into user_stripe_link (user_id, stripe_id) values (?,?)");
      ps.setString(1, userID);
      ps.setString(2, stripeID);
      ps.executeUpdate();
      connection.commit();
      logger.debug("CreateStripeUser.execute "+userID+" "+stripeID);
    }
  }

  /**
   * Helper class to lookup a user/stripe link
   */
  private static class FindStripeUser implements Work {
    private final String userID;
    String stripeID = null;

    public FindStripeUser(String userID) {
      this.userID = userID;
    }

    public void execute(Connection connection) throws SQLException {
      PreparedStatement ps = connection.prepareStatement("select stripe_id from user_stripe_link where user_id =?");
      ps.setString(1, userID);
      ResultSet rs = ps.executeQuery();
      if (rs.next()){
        stripeID = rs.getString(1);
        logger.debug("Got stripe info for user "+userID+" str:"+stripeID);
      } else {
        logger.debug("No stripe data found for user "+userID);
      }
    }
  }

  /**
   * Get information about the specified user's stripe configuration
   * @param userID
   * @return StripeCustomer, null if not found
   */
  @Override
  public StripeCustomer getSubscription(final String userID) {

    FindStripeUser work = new FindStripeUser(userID);
    HibernateUtil.getSessionFactory().getCurrentSession().doWork(work);
    StripeCustomer stripeCustomer = null;

    if (work.stripeID != null) {
      // we have existing billing info
      Stripe.apiKey = IvProps.getStripeLiveSecretKey();
      stripeCustomer = new StripeCustomer();
      try {
        Customer c = Customer.retrieve(work.stripeID);
        stripeCustomer.stripe_id = c.getId();
        logger.debug("Stripe user found");
        logger.debug("id:" +c.getId());
        logger.debug("getDefaultCard:" +c.getDefaultCard());
        logger.debug("getSubscription:" +c.getSubscription());
        logger.debug("getNextRecurringCharge:" +c.getNextRecurringCharge());
        logger.debug("getDescription:" +c.getDescription());
      } catch (com.stripe.exception.AuthenticationException e) {
        logger.error(this.getClass().getName(), e);
        throw new RuntimeException(e);
      } catch (InvalidRequestException e) {
        logger.error(this.getClass().getName(), e);
        throw new RuntimeException(e);
      } catch (APIConnectionException e) {
        logger.error(this.getClass().getName(), e);
        throw new RuntimeException(e);
      } catch (CardException e) {
        logger.error(this.getClass().getName(), e);
        throw new RuntimeException(e);
      } catch (APIException e) {
        logger.error(this.getClass().getName(), e);
        throw new RuntimeException(e);
      }
    }
    return stripeCustomer;
  }

  @Override
  public UserAccountInfo getUserAccountInfo(SessionToken sessionID,
      String userName) throws BtSecurityException {
    final String m = "getUserAccountInfo(...)";
    try {
      uService.verifyUser(m, sessionID, null);
      return uService.getUserAccountInfoHelper(userName);
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public List<UserAccountInfo> getUserAccountInfoForAll(SessionToken sessionID)
      throws BtSecurityException {
    final String m = "getUserAccountInfoForAll(...)";
    try {
      uService.verifyUser(m, sessionID, Role.ADMIN);
      return uService.getUserAccountInfoHelperForAll();
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

  @Override
  public void setUserAccountInfoForAll(SessionToken sessionID,
      List<UserAccountInfo> info) throws BtSecurityException {
    final String m = "setUserAccountInfoForAll(...)";
    try {
      uService.verifyUser(m, sessionID, Role.ADMIN);
      uService.setUserAccountInfoHelperForAll(info);
    } catch (Throwable t) {
      logger.error(m + ": Caught Exception", t);
      propagateIfInstanceOf(t, BtSecurityException.class);
      throw propagate(t);
    }
  }

	@Override
	public EEGMontage addMontage(SessionToken sessionID, EEGMontage montage,
			String snapshotLabel) throws BtSecurityException, EegMontageNotFoundException {
		final String m = "addMontage(...)";
		try {

			User user = uService.verifyUser(m, sessionID, Role.USER);

			// logger.debug("{} Adding to DB: {}",m,newMontage);
			IDataSnapshotServer dsServer = uService.datasets;

			EEGMontage out = dsServer.createEditEegMontage(user, montage,
                    snapshotLabel);
			return out;
		} catch (EegMontageNotFoundException e) {
			logger.error(m + ": unexpected error while creating new montage", e);
			throw e;
		} catch (RuntimeException e) {
			logger.error(m + ": Caught Exception", e);
			throw e;
		}

	}
	
	@Override
	public UserInfo updateUser(SessionToken sessionID, UserAccountInfo newUserInfo, String hashedNewPassword)
			throws BtSecurityException {
		final String m = "updateUser(...)";
		try {
			User user = uService.verifyUser(m, sessionID, Role.USER);
			User updatedUser = uService.updateUser(user, newUserInfo, hashedNewPassword);
			return uService.getUserInfoHelper(updatedUser.getUsername());
		} catch (Throwable t) {
			logger.error(m + ": Caught Exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void removeMontage(SessionToken sessionID, EEGMontage montage)
			throws BtSecurityException, EegMontageNotFoundException {
		final String m = "removeMontage(...)";
		try {
		User user = uService.verifyUser(m, sessionID, Role.USER);

		IDataSnapshotServer dsServer = uService.datasets;
		dsServer.deleteEegMontage(user, montage.getServerId());
		} catch (RuntimeException | EegMontageNotFoundException e) {
			logger.error(m + ": Caught Exception", e);
			throw e;
		}
	}


	@Override
	public Set<? extends PresentableMetadata> getKnownUsers(SessionToken sessionID) 
			throws BtSecurityException {
		final String M = "getKnownUsers()";
		try {
			logger.info("Loading known users");
			User u = uService.verifyUser(M, sessionID, null);
			
			Set<? extends PresentableMetadata> ret = crService.getKnownUsers(u, null);

			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends PresentableMetadata> getRecommendedUsers(SessionToken sessionID) 
			throws BtSecurityException {
		final String M = "getRecommendedUsers()";
		try {
			logger.info("Loading recommended friends");
			User u = uService.verifyUser(M, sessionID, null);
			
			Set<? extends PresentableMetadata> ret = crService.getRecommendedUsers(u);

			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void addFriend(SessionToken sessionID, UserInfo friendInfo) throws BtSecurityException {
		final String M = "addFriend()";
		try {
			logger.info("Adding friend");
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.addFriend(u, uService.getEnabledUser(friendInfo.getName()));
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void removeFriend(SessionToken sessionID, UserInfo friendInfo) throws BtSecurityException {
		final String M = "addFriend()";
		try {
			logger.info("Adding friend");
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.removeFriend(u, uService.getEnabledUser(friendInfo.getName()));
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void addUserToGroup(SessionToken sessionID, UserInfo userInfo, String groupName) throws BtSecurityException {
		final String M = "addToGroup()";
		try {
			logger.info("Loading recommended friends");
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.addToGroup(uService.getEnabledUser(userInfo.getName()), groupName, u);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void removeUserFromGroup(SessionToken sessionID, UserInfo userInfo, String groupName) throws BtSecurityException {
		final String M = "removeUserFromGroup()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.removeFromGroup(uService.getEnabledUser(userInfo.getName()), groupName, u);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void shareWith(SessionToken sessionID, PresentableMetadata object, int levelInx, UserInfo sharee) throws BtSecurityException {
		final String M = "shareWith()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			if (levelInx < 0 || levelInx > 2)
				throw new UnsupportedOperationException("Level index must be 0-2");
			
			ShareLevel level = ShareLevel.values()[levelInx];
			crService.shareWith(u, object, level, uService.getEnabledUser(sharee.getName()));
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public void shareWith(SessionToken sessionID, PresentableMetadata object, int levelInx, String group) throws BtSecurityException {
		final String M = "shareWith()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			if (levelInx < 0 || levelInx > 2)
				throw new UnsupportedOperationException("Level index must be 0-2");
			
			ShareLevel level = ShareLevel.values()[levelInx];
			crService.shareWithGroup(u, object, level, group);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void unshareWith(SessionToken sessionID, PresentableMetadata object, UserInfo sharee) throws BtSecurityException {
		final String M = "un shareWith()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.unshareWith(u, object, uService.getEnabledUser(sharee.getName()));
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void changeOwner(SessionToken sessionID, PresentableMetadata object, UserInfo newOwner) throws BtSecurityException {
		final String M = "changeOwner()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			crService.changeOwner(u, object, uService.getEnabledUser(newOwner.getName()));
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	@Override
	public BasicCollectionNode createGroup(SessionToken sessionID, String groupName) throws BtSecurityException {
		final String M = "createGroup()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.createGroup(u, groupName);
			
			return new BasicCollectionNode(crService.getUrlBase() + "/groups/" + groupName, 
					groupName, SpecialPaths.UserFolderType);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
//		return null;
	}

	@Override
	public void removeGroup(SessionToken sessionID, BasicCollectionNode group) throws BtSecurityException {
		final String M = "removeGroup()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.removeGroup(u, group.getLabel());
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public BasicCollectionNode createFolder(SessionToken sessionID, String label, String folderPathName) throws BtSecurityException {
		final String M = "createFolder()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			if (label.contains("/") || label.contains("\\"))
				throw new IOException("Cannot create a folder label with a slash or backslash");
			
			crService.createFolder(u, label, folderPathName);
			
//			if (folderPathName.startsWith(crService.getUrlBase()))
//				folderPathName = folderPathName.substring(crService.getUrlBase().length() + 1);

			return new BasicCollectionNode(folderPathName, 
					label, SpecialPaths.FolderType);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
		
	}

	@Override
	public void removeFolder(SessionToken sessionID, BasicCollectionNode folder) throws BtSecurityException {
		final String M = "removeFolder()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
//			// Skip URL base plus leading slash
			String folderPathName = folder.getId();
			
			if (folder.getLabel() != null && !folder.getLabel().isEmpty())
				folderPathName = folderPathName + "/" + folder.getLabel();
			
			if (folderPathName.equals(crService.getUrlBase() + "/homes/" + u.getUsername()))
				throw new BtSecurityException("Cannot remove user home folder!");
			
//			if (folderPathName.startsWith(crService.getUrlBase()))
//				folderPathName = folderPathName.substring(crService.getUrlBase().length() + 1);
			
			crService.removeFolder(u, folderPathName);//folder.getLabel(), folderPathName);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
		
	}

	@Override
	public Set<BasicCollectionNode> getGroups(SessionToken sessionID) {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "getGroups()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			return crService.getGroups(u);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends PresentableMetadata> getFolderContents(SessionToken sessionID, String path) {
		Set<PresentableMetadata> ret = new HashSet<PresentableMetadata>();
		final String M = "getFolderContents()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			return crService.getFolderContents(u, path);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends PresentableMetadata> getFolderContents(SessionToken sessionID, BasicCollectionNode folder) {
		Set<PresentableMetadata> ret = new HashSet<PresentableMetadata>();
		final String M = "getFolderContents()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			return crService.getFolderContents(u, folder);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void storeMetadata(SessionToken sessionID, String path, PresentableMetadata object)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "storeMetadata()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			// Skip URL base plus leading slash
			String folderPathName = path;
			String label = "";
			
			if (path.indexOf('/') >= 0) {
				label = folderPathName.substring(folderPathName.lastIndexOf('/') + 1);
			} else {
				label = folderPathName;
				folderPathName = crService.getUrlBase() + "/homes/" + u.getUsername() + "/" + label;
			}
			
			if (folderPathName.startsWith(crService.getUrlBase()))
				folderPathName = folderPathName.substring(crService.getUrlBase().length() + 1);
			
			BasicCollectionNode tempNode = new BasicCollectionNode(folderPathName, label, SpecialPaths.FolderType);
			
			crService.storeMetadata(u, tempNode, object);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void removeMetadata(SessionToken sessionID, String path, PresentableMetadata object)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "removeMetadata()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			// Skip URL base plus leading slash
			String folderPathName = path;
			String label = "";
			
			if (path.indexOf('/') >= 0) {
				label = folderPathName.substring(folderPathName.lastIndexOf('/') + 1);
			} else {
				label = folderPathName;
				folderPathName = crService.getUrlBase() + "/homes/" + u.getUsername() + "/" + label;
			}
			
			if (folderPathName.startsWith(crService.getUrlBase()))
				folderPathName = folderPathName.substring(crService.getUrlBase().length() + 1);
			
			BasicCollectionNode tempNode = new BasicCollectionNode(folderPathName, label, SpecialPaths.FolderType);
			
			crService.removeMetadata(u, tempNode, object);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void storeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "storeMetadata()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			crService.storeMetadata(u, parent, object);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void removeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "removeMetadata()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.removeMetadata(u, parent, object);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public int storeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo metadataInfo, String container, String fileKey, byte[] object)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "storeMetadataObject()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			IObjectServer os = StorageFactory.getPersistentServer();
			
			IStoredObjectReference ref = new FileReference(new DirectoryBucketContainer(container, 
					container), fileKey, fileKey);
			
			crService.storeMetadataObject(u, parent, metadataInfo, os, ref);
			return 1;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public void removeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo metadataInfo, String container, String fileKey)
			throws BtSecurityException {
		Set<BasicCollectionNode> ret = new HashSet<BasicCollectionNode>();
		final String M = "removeMetadataObject()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.removeMetadataObject(u, parent, metadataInfo);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Map<UserInfo, Set<Integer>> getSharingSet(SessionToken sessionID, PresentableMetadata folder)
			throws BtSecurityException {
		Map<UserInfo, Set<Integer>> ret = new HashMap<UserInfo, Set<Integer>>();
		final String M = "getSharingSet()";

		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			Map<String, Set<Integer>> map = crService.getUserPermissions(u, folder);
			
			for (String sharee : map.keySet()) {
				User user2 = uService.getEnabledUser(sharee);
				
		        UserInfo ui = new UserInfo(user2.getUsername(), user2.getPhoto(),
		                user2.getUserId());

		            ui.setProfile(user2.getProfile());
				
		        ret.put(ui, map.get(sharee));
			}
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
		
		return ret;
	}

	@Override
	public void addToFolder(SessionToken sessionID, BasicCollectionNode folder, PresentableMetadata item)
			throws BtSecurityException {
		final String M = "addToFolder()";

		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.addToFolder(item, folder, u);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
		
	}

	@Override
	public void addDerivation(SessionToken sessionID, SearchResult parent, String provenanceStep, SearchResult child)
			throws BtSecurityException {
		final String M = "addDerivation()";

		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			crService.addDerivation(u, parent, provenanceStep, child);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public IStoredObjectReference createInboxFolder(SessionToken sessionID, String project, String path) 
	throws IOException {
		final String M = "createInboxFolder()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			IObjectServer bestServer = StorageFactory.getUploadServer();
			IStoredObjectContainer db = bestServer.getDefaultContainer();
			
			Path parent = Paths.get(u.getUsername(), path);
			Path initial = Paths.get(u.getUsername(), path, project);
			
			String dirName = initial.toString();

			dirName = dirName.replaceAll("\\\\", "/").replaceAll("\\.\\.", "\\.");
			
			bestServer.removeDirectory(db, dirName);
			
			if (bestServer.createDirectory(db, dirName)) {
			
				String pathName = parent.toString();
	
				pathName.replaceAll("\\\\", "/").replaceAll("..", ".");
	
	//			Path dir =
	//					Paths.get(dirName);
				
				FileReference ref = new FileReference(new DirectoryBucketContainer(null, pathName), 
								project, project);
				
				ref.setContainer();
				
				return ref;
			} else
				throw new IOException("Could not create folder " + project + ", perhaps it already exists?");
		} catch (Throwable t) {
			// TODO Auto-generated catch block
			t.printStackTrace();
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, IOException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends IStoredObjectReference> getInboxFolderContents(SessionToken sessionID,
			IStoredObjectReference folder) {
		IObjectServer bestServer = StorageFactory.getUploadServer();
		final String M = "getInboxFolderContents()";
		
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			String dirName = folder.getFilePath();
			
			if (!dirName.startsWith(u.getUsername()))
				throw new RuntimeException("Path must start with user folder");
			
			if (dirName.endsWith("/."))
				dirName = dirName.substring(0, dirName.length() - 2);
		
			Set<IStoredObjectReference> contents = 
					bestServer.getDirectoryContents(folder.getDirBucketContainer(),
                            dirName);

			return contents;
		} catch (Throwable t) {
			// TODO Auto-generated catch block
			t.printStackTrace();
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends IStoredObjectReference> getInboxFolderContents(SessionToken sessionID, String thePath,
			SessionToken sessionID2) {
		IObjectServer bestServer = StorageFactory.getUploadServer();
		final String M = "getInboxFolderContents()";
		
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			String dirName = thePath;
			if (!dirName.startsWith(u.getUsername()))
				throw new RuntimeException("Path must start with user folder");
			
			if (dirName.endsWith("/."))
				dirName = dirName.substring(0, dirName.length() - 2);
		
			try {
				Set<IStoredObjectReference> contents = 
						bestServer.getDirectoryContents(bestServer.getDefaultContainer(), 
								dirName);
				
				// If it's empty, we might need to create it, e.g., on S3...
				if (contents.isEmpty() && bestServer.emptyDirectoryMeansMissing()) {
					try {
						bestServer.createDirectory(bestServer.getDefaultContainer(), dirName);
						bestServer.createDirectory(bestServer.getDefaultContainer(), dirName + "/Imports");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				Set<IStoredObjectReference> ret = new HashSet<IStoredObjectReference>();
				for (IStoredObjectReference item: contents) {
					// Skip archives
					if (!item.isContainer() || !item.getFilePath().endsWith("Imports")) {
						Path fullPath = Paths.get(item.getFilePath());
						String path = fullPath.getFileName().toString().replaceAll("\\\\", "/");
						DirectoryBucketContainer bucket = new DirectoryBucketContainer(
								null, Strings.nullToEmpty(fullPath.getParent() == null ? "" :
									fullPath.getParent().toString().replaceAll("\\\\", "/")));
						FileReference ref = new FileReference(bucket, item.getObjectKey().replaceAll("\\\\", "/"), path);
						ref.setContainer(item.isContainer());
						ret.add(ref);
					}
				}
				
				return ret;
			} catch (NoSuchFileException nsf) {
				bestServer.createDirectory(bestServer.getDefaultContainer(), dirName);
				
//				Set<IStoredObjectReference> contents = 
//						bestServer.getDirectoryContents(bestServer.getDefaultContainer(), 
//								dirName);
				return Sets.<IStoredObjectReference>newHashSet();
			}

		} catch (Throwable t) {
			// TODO Auto-generated catch block
			t.printStackTrace();
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public IStoredObjectReference moveInboxToFolder(SessionToken sessionID, String project, String path) 
	throws IOException {
		String M = "moveInboxToFolder";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			IObjectServer bestServer = StorageFactory.getUploadServer();
			IStoredObjectContainer db = bestServer.getDefaultContainer();

			Set<IStoredObjectReference> contents = 
					bestServer.getDirectoryContents(bestServer.getDefaultContainer(), 
							u.getUsername() + "/Imports");
			
			FileReference ref = (FileReference)createInboxFolder(sessionID, project, "Imports/" + path);
			
			String dirPath = (db.getFileDirectory() == null) ? ref.getDirBucketContainer().getFileDirectory().replaceAll("\\\\", "/") :
					db.getFileDirectory() + "/" + ref.getDirBucketContainer().getFileDirectory().replaceAll("\\\\", "/");
			ref.setDirBucketContainer(new DirectoryBucketContainer(db.getBucket(), dirPath));
			
			Exception last = null;
			if (ref != null) {
				for (IStoredObjectReference cont: contents) {
					// Skip archives
					if (cont.getFilePath().endsWith("Imports/"))
						; // Skip
					else if (!cont.isContainer() || (!cont.getFilePath().endsWith("Imports")))
						try {
							Path p = Paths.get(cont.getFilePath());
							FileReference target = new FileReference(ref.getDirBucketContainer(), ref.getObjectKey(), ref.getFilePath() + "/" + p.getFileName().toString());
							bestServer.moveObject(cont, target);
						} catch (Exception e) {
							last = e;
						}
				}
			}
			
			if (last != null)
				throw last;
			
			return ref;
		} catch (Throwable t) {
			// TODO Auto-generated catch block
			t.printStackTrace();
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			propagateIfInstanceOf(t, IOException.class);
			
			throw propagate(t);
		}
	}

  /**
   * Send user feedback to slack and to BF support
   * @param user
   * @param message
   */
  @Override
  public void sendFeedback(String user, String message) {
    try {
      SmtpServerFactory.getSmtpServer().sendMessage("support@blackfynn.com", "User Feedback", "support@blackfynn.com", "From: "+user+"\n"+message);
      String jsonMsg = "{\"text\": \"User Feedback\nFrom: "+user+"\n"+message+"\"}";
      Request.Post("https://hooks.slack.com/services/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")
              .bodyForm(Form.form().add("payload", jsonMsg).build()).execute();
    } catch (IOException e) {
      logger.error("Error sending feedback: "+message, e);
    }
  }
}
