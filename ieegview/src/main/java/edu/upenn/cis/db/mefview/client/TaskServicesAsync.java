package edu.upenn.cis.db.mefview.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.db.mefview.shared.Task;
import edu.upenn.cis.db.mefview.shared.TaskSubmission;

public interface TaskServicesAsync {

	void getActiveTasks(SessionToken session, AsyncCallback<List<Task>> callback);

	void submitForTask(SessionToken session, TaskSubmission submission,
			AsyncCallback<Void> callback);

	void getSubmittedTasks(SessionToken session,
			AsyncCallback<List<TaskSubmission>> callback);

}
