

package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.MontagePairInfo;

public interface IShowTraces extends DialogFactory.DialogWithInitializer<IShowTraces> {
	public static interface Handler {
		public void updateChannels(EEGMontage montage, Boolean[] isVisible);
		public void channelsChanged(Set<EEGMontagePair> selPairs);
	}
		
	public void init(final Handler responder, Command doAfter);
	
	public void setPane(EEGPane pane);
		
	public void open();
	
	public void close();

	public void focus();

	public void initTracePane(EEGMontage montage, List<Boolean> isVisible, ClientFactory factory);
	
	public void createDialog();


}
