package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class BadPasswordDialogBox extends DialogBox {
	public BadPasswordDialogBox() {
		super(true, true);
		
		setTitle("Incorrect password");
		add(new Label("Your old password is incorrect."));
	}
}
