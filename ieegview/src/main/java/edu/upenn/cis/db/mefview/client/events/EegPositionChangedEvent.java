/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.messages.EegPositionChangedMessage;

public class EegPositionChangedEvent extends GwtEvent<EegPositionChangedEvent.Handler> {
	private static final Type<EegPositionChangedEvent.Handler> TYPE = new Type<EegPositionChangedEvent.Handler>();
	
	private final EegPositionChangedMessage action;
	
	public EegPositionChangedEvent(EegPositionChangedMessage action){
		this.action = action;
	}
	

	public static com.google.gwt.event.shared.GwtEvent.Type<EegPositionChangedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EegPositionChangedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EegPositionChangedEvent.Handler handler) {
		handler.onMove(action);
	}

	public static interface Handler extends EventHandler {
		void onMove(EegPositionChangedMessage action);
	}
}
