/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.controls;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.TextBox;

import edu.upenn.cis.db.mefview.client.ClientFactory;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;

import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;

import edu.upenn.cis.db.mefview.client.widgets.ComboBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.DoubleBoxWithEnter;
import edu.upenn.cis.db.mefview.client.widgets.NumericEnterHandler;

public interface IEEGToolbar extends IsWidget {
	

	public void init(ClientFactory factory, DataSnapshotModel currentModel, IEEGPane pane);

	public void setClickHandlers(ClickHandler layerHandler, ClickHandler traceHandler, ClickHandler filterHandler,
			ClickHandler montageHandler, NumericEnterHandler scaleHandler);
	
	public MenuItem addMenuOption(String legend, Command cmd);

	public MenuItem addMenuOptionHtml(String legend, Command cmd);

	public MenuItem addSubMenu(String legend, MenuBar menuI);
	
	public void addMenuSpacer();
	
	public void setRefreshHandler(ClickHandler handler);


	public TextBox getDetectBox();


	public ComboBoxWithEnter<ComboBoxDataModel> getTimeIntervalBox();


	public ComboBoxWithEnter<ComboBoxDataModel> getVoltageScaleBox();


	public DoubleBoxWithEnter getPosBox();

//	public void setAutoSizeHandler(ValueChangeHandler<Boolean> autoSizeHandler) {
//		autoSize.addValueChangeHandler(autoSizeHandler);
//	}

	
	public void bindDomEvents();
	
	public void setExecHandler(ClickHandler exec);

	void setCloseHandler(ClickHandler exec);

	public void setInvertHandler(ClickHandler invertHandler);

	public HasText getInvertButton();
}
