/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.presenters;

import java.util.Set;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.EventHandler;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

public abstract class BasicPresenter implements Presenter, EventHandler {
	ClientFactory clientFactory;
	
	public interface Display extends Presenter.Display, AstroPanel {
		public void bindDomEvents();
	}
	
	protected Display display;
	String name;
	Set<String> types;
	
	public BasicPresenter(ClientFactory cf, String name, Set<String> types) {
		clientFactory = cf;
		this.name = name;
		this.types = types;
	}
	
	@Override
	public String getType() {
		return name;
	}
	
	@Override
	public Set<String> getSupportedDataTypes() {
		return types;
	}
	
	@Override
	public void setDisplay(Presenter.Display disp) {
		display = (Display)disp;
	}
	
	public abstract Display getDisplay();

	@Override
	public void unbind() {
		IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
		if (getSessionModel() != null && getSessionModel().getHeaderBar() != null && getDisplay() != null)
			getSessionModel().getHeaderBar().removeLocalEntries(getDisplay());
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	protected void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
	
	protected SessionModel getSessionModel() {
		return getClientFactory().getSessionModel();
	}
}
