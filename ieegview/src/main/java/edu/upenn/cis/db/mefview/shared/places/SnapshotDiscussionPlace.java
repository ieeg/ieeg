/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import edu.upenn.cis.db.mefview.client.plugins.snapshotdiscussion.SnapshotDiscussionPanel;

public class SnapshotDiscussionPlace extends WindowPlace {
	
	public SnapshotDiscussionPlace() {}
	
	public SnapshotDiscussionPlace(String snapshot) {
		super(snapshot);
	}

	@Override
	public String getType() {
		return SnapshotDiscussionPanel.NAME;
	}

//	@Override
//	public String getSerializableForm() {
//		return snapshot;
//	}
//
//	public static WindowPlace getFromSerialized(String serial) {
//		return new SnapshotDiscussionPlace(serial);
//	}
}
