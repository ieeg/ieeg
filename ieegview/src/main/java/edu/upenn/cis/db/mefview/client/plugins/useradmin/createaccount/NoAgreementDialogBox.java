package edu.upenn.cis.db.mefview.client.plugins.useradmin.createaccount;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class NoAgreementDialogBox extends DialogBox {
	public NoAgreementDialogBox() {
		super(true, true);
		
		setTitle("No Agreement");
		add(new Label("You need to agree to all terms and conditions."));
	}
}
