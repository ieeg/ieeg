/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.eeg.controls;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.LayoutPanel;

import edu.upenn.cis.db.mefview.client.widgets.Attachable;
import edu.upenn.cis.db.mefview.client.widgets.IWidgetFactory.WidgetClickHandler;
import edu.upenn.cis.db.mefview.client.widgets.WidgetFactory;

public class SliderBar extends LayoutPanel implements HasValueChangeHandlers<Double> {
	public final static class AnnBlock implements Comparable<AnnBlock> {
		public AnnBlock(double st, double en) {
			start = st;
			stop = en;
		}
		
		public double getStart() { 
			return start;
		}
		
		public double getStop() {
			return stop;
		}
		
		double start;
		double stop;
		public int compareTo(AnnBlock o) {
//			if (o instanceof AnnBlock)
				return Double.compare(start, ((AnnBlock)o).start);
//			else
//				throw new RuntimeException("Incomparable");
		}
	}
	
	double min;
	double max;
	double pos;
	double window = 20;
	Set<ValueChangeHandler<Double>> vc;
//	Set<Double> annotations;
	Set<AnnBlock> annotations;
	Image i;
	Canvas canvas;
	Context2d context;
	Canvas cursorCanvas;
	Context2d cursorContext;
	int wide;
	int high;
	
	List<Pair> pairs;
	
	public SliderBar() {
		
	}
	
	public void update(double minX, double maxX, String width, String height) {
		this.min = minX;
		this.max = maxX;
		window = 1.0;
		pos = min;
		vc = new HashSet<ValueChangeHandler<Double>>();
		
		annotations = new HashSet<AnnBlock>();
		
		canvas = Canvas.createIfSupported();
		cursorCanvas = Canvas.createIfSupported();
		
		if (canvas != null) {
			context = canvas.getContext2d();
		}
		if (cursorCanvas != null) {
			cursorContext = cursorCanvas.getContext2d();
		}
		wide = Integer.valueOf(width.substring(0, width.length() - 2));
		high = Integer.valueOf(height.substring(0, height.length() - 2));
		canvas.setCoordinateSpaceHeight(high);
		canvas.setCoordinateSpaceWidth(wide);
		cursorCanvas.setCoordinateSpaceHeight(high);
		cursorCanvas.setCoordinateSpaceWidth(wide);
		
		setHeight(height);
		setWidth(width);
		
		add(canvas);
		canvas.getElement().setId("Slider-canvas");
		setWidgetTopBottom(canvas, 0, Unit.PX, 0, Unit.PX);
		setWidgetLeftRight(canvas, 0, Unit.PX, 0, Unit.PX);
		
		add(cursorCanvas);
		cursorCanvas.getElement().setId("Slider-Cursor-canvas");
		setWidgetTopBottom(cursorCanvas, 0, Unit.PX, 0, Unit.PX);
		setWidgetLeftRight(cursorCanvas, 0, Unit.PX, 0, Unit.PX);

		WidgetFactory.get().addClickHandler(cursorCanvas, 
				new WidgetClickHandler() {

			public void onClick(int x, int y) {
//				double x = e.getRelativeX(getElement());
//				GWT.log("SLIDER: Clicked on image at " + x);
				
				pos = (((double)x) / wide) * (max - min) + min;
				setCurrentValue(pos, true);
			}
			
		});
		
		//this.add(i);

		updatePositionBox();
	}
	
	public SliderBar(double minX, double maxX, String width, String height) {
		this.min = minX;
		this.max = maxX;
		window = 1.0;
		pos = min;
		vc = new HashSet<ValueChangeHandler<Double>>();
		
		annotations = new HashSet<AnnBlock>();
		
		canvas = Canvas.createIfSupported();
		cursorCanvas = Canvas.createIfSupported();
		
		if (canvas != null) {
			context = canvas.getContext2d();
		}
		if (cursorCanvas != null) {
			cursorContext = cursorCanvas.getContext2d();
		}
//		System.err.println("Slider bar " + width + " by " + height);
		wide = Integer.valueOf(width.substring(0, width.length() - 2));
		high = Integer.valueOf(height.substring(0, height.length() - 2));
		canvas.setCoordinateSpaceHeight(high);
		canvas.setCoordinateSpaceWidth(wide);
		cursorCanvas.setCoordinateSpaceHeight(high);
		cursorCanvas.setCoordinateSpaceWidth(wide);
		
		setHeight(height);
		setWidth(width);
		
		add(canvas);
		canvas.getElement().setId("Slider-canvas");
		setWidgetTopBottom(canvas, 0, Unit.PX, 0, Unit.PX);
		setWidgetLeftRight(canvas, 0, Unit.PX, 0, Unit.PX);
		
		add(cursorCanvas);
		cursorCanvas.getElement().setId("Slider-Cursor-canvas");
		setWidgetTopBottom(cursorCanvas, 0, Unit.PX, 0, Unit.PX);
		setWidgetLeftRight(cursorCanvas, 0, Unit.PX, 0, Unit.PX);

		cursorCanvas.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent e) {
//				System.out.println("Clicked on image");
				double x = e.getRelativeX(getElement());
				
				pos = (x / wide) * (max - min) + min;
				setCurrentValue(pos, true);
			}
			
		});
		//this.add(i);

		updatePositionBox();
	}
	
	public void setMinMax(double min, double max){
		this.min = min;
		this.max = max;
	}
	
	public void setCursorWidth(double width) {
		window = width;
	}
	
	@Override
	public void setWidth(String width) {
		super.setWidth(width);
//		canvas.setWidth(width);
//		cursorCanvas.setWidth(width);
		wide = Integer.valueOf(width.substring(0, width.length() - 2));
		canvas.setCoordinateSpaceWidth(wide);
		cursorCanvas.setCoordinateSpaceWidth(wide);
//		System.out.println("Slider resized to " + width + " / " + canvas.getOffsetWidth());
			updateBar();
			updatePositionBox();
	}

	@Override
	public void setHeight(String height) {
		super.setHeight(height);
//		canvas.setHeight(height);
		high = Integer.valueOf(height.substring(0, height.length() - 2));
		canvas.setCoordinateSpaceHeight(high);
		cursorCanvas.setCoordinateSpaceHeight(high);
			updateBar();
			updatePositionBox();
	}
	
	public void setCurrentValue(double value, boolean update) {
		if (value > max)
			value = max;
		if (value < min)
			value = min;
		
//		if (pos != value) {
			pos = value;
			updatePositionBox();
			if (update)
				callValueChangeHandler(pos);
//		}
	}
	
	class Pair implements Comparable<Pair> {
		public Pair(int s, int w) {
			start = s;
			width = w;
		}
		
		int start;
		int width;
		public int compareTo(Pair o) {
			if (!(o instanceof Pair))
				throw new RuntimeException("Incompatible types");
			return Integer.valueOf(start).compareTo(Integer.valueOf(((Pair)o).start));
		}
	}
	
	public void updatePairs() {
		Map<Integer,Pair> rects = new HashMap<Integer,Pair>();
		
		double wid;
		// Condense the set of annotations to a minimal one, eliminating
		// 100% overlapping rectangles
		for (AnnBlock a: annotations) {
			wid = ((a.getStop() - a.getStart()) / (max - min)) * wide;
			if (wid < 1)
				wid = 1;
			int s = (int)(((a.start - min) / (max - min)) * wide);
			
			if (rects.containsKey(s)) {
				Pair p = rects.get(s);
				if (p.width < (int)wid)
					p.width = (int)wid;
			} else 
				rects.put(s, new Pair(s,(int)wid));
		}
		pairs = new ArrayList<Pair>();
		pairs.addAll(rects.values());
		Collections.sort(pairs);
	}
	
	public void updateBar() {
		context.save();
		context.setStrokeStyle("gray");
		context.strokeRect(0, 0, wide, canvas.getCoordinateSpaceHeight());
		context.setFillStyle("lightgray");
		context.fillRect(1, 1, wide - 2, canvas.getCoordinateSpaceHeight() - 2);
		
//		context.setStrokeStyle("white");
//		context.moveTo(0, 0);
//		context.lineTo(wide, 0);
		context.restore();
		
		
		context.save();
		context.setFillStyle("black");

//		if (false) {// && annotations.size() < 500) {
			updatePairs();
			
			int h = canvas.getCoordinateSpaceHeight() - 4;

			for (Pair p: pairs)
				context.fillRect(p.start, 2, 
						p.width, h);
//		}
		context.restore();
	}
	
	public void updatePositionBox() {
		cursorContext.clearRect(0, 0, cursorCanvas.getCoordinateSpaceWidth(), 
				cursorCanvas.getCoordinateSpaceHeight());
		
		cursorContext.save();
		cursorContext.setStrokeStyle("#D68F00");
		cursorContext.setFillStyle("transparent");
		
		double wid = ((double)(window) / (max - min)) * wide;
		if (wid < 1)
			wid = 1;
		cursorContext.fillRect((((double)pos - min) / (max - min)) * wide, 0, 
				wid + 1, canvas.getCoordinateSpaceHeight());
		cursorContext.strokeRect((((double)pos - min) / (max - min)) * wide - 1, 0, 
				wid + 2, canvas.getCoordinateSpaceHeight());
		
		cursorContext.restore();
	}
	
	public void setAnnotations(Collection<AnnBlock> annotations) {
		this.annotations.clear();
		this.annotations.addAll(annotations);
		updateBar();
		updatePositionBox();
	}
	
	public void addAnnotations(Collection<AnnBlock> annotations) {
		this.annotations.addAll(annotations);
		updateBar();
		updatePositionBox();
	}
	
	public void addAnnotation(AnnBlock a) {
		this.annotations.add(a);
		updateBar();
		updatePositionBox();
	}
	
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
//		System.out.println("Registered");
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	private void callValueChangeHandler(double pos) {
//		System.out.println("Calling at " + pos);
		ValueChangeEvent.fire(this, pos);
	}
}
