package edu.upenn.cis.db.mefview.client.plugins;

import java.util.Collection;

import edu.upenn.cis.db.mefview.client.widgets.ITabber;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public interface ITabSet {
	public int getSelectedTabber();
	
	public void setLogin(UserInfo user);
	
	public ITabber getSelectedTabPane();
	
	public ITabber getTabForCategory(String paneName);
	
	public void selectTabForCategory(String name);
	
	public Collection<? extends ITabber> getAllPanes();
	
	public void bindAll();
}
