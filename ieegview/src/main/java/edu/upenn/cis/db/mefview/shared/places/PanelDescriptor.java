package edu.upenn.cis.db.mefview.shared.places;

public class PanelDescriptor {
	public PanelDescriptor(String snapshot, String type) {
		this.snapshot = snapshot;
		this.type = type;
	}
	
	String type;
	String snapshot;
	
	public String getType() {
		return type;
	}
}