/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.presenters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ClientFactory.IAppSaveAndShutdown;
import edu.upenn.cis.db.mefview.client.IEventBus;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.commands.display.OpenAndLoadSnapshotCommand;
import edu.upenn.cis.db.mefview.client.commands.display.PanelUpdater;
import edu.upenn.cis.db.mefview.client.commands.display.ShowAccountCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowPasswordCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowPrefsCommand;
import edu.upenn.cis.db.mefview.client.commands.display.ShowUploadCommand;
import edu.upenn.cis.db.mefview.client.commands.display.UpdateReloadedSnapshotCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.EditPermissionsCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.LoadAnnotationsAndCopyCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.LoadSnapshotDetailsCommand;
import edu.upenn.cis.db.mefview.client.commands.snapshot.RemoveSnapshotCommand;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.SaveSnapshotAs;
import edu.upenn.cis.db.mefview.client.dialogs.SetExperimentSource;
import edu.upenn.cis.db.mefview.client.events.BookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.ChangedViewEvent;
import edu.upenn.cis.db.mefview.client.events.CloseDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.ClosedComplexDialogEvent;
import edu.upenn.cis.db.mefview.client.events.ClosedPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenProjectEvent;
import edu.upenn.cis.db.mefview.client.events.OpenedComplexDialogEvent;
import edu.upenn.cis.db.mefview.client.events.OpenedPanelEvent;
import edu.upenn.cis.db.mefview.client.events.ProjectOpenedEvent;
import edu.upenn.cis.db.mefview.client.events.RequestLaunchToolEvent;
import edu.upenn.cis.db.mefview.client.events.RequestReloadSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.RequestRemoveExperimentEvent;
import edu.upenn.cis.db.mefview.client.events.RequestShowPermissionsEvent;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.events.ServerTitleObtainedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolMarkedAsFavoriteEvent;
import edu.upenn.cis.db.mefview.client.events.UnbookmarkSnapshotEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.ActionMapper;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFPresenter;
import edu.upenn.cis.db.mefview.client.plugins.projectsettings.ProjectPresenter;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPanel;
import edu.upenn.cis.db.mefview.client.plugins.web.WebPresenter;
import edu.upenn.cis.db.mefview.client.widgets.CollapsibleTab;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserPrefs;
import edu.upenn.cis.db.mefview.shared.WebLink;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.URLPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;
import edu.upenn.cis.db.mefview.shared.util.ToolUploadCookies;

//import edu.upenn.cis.db.mefview.shared.ToolInfo;

public class AppPresenter extends BasicPresenter implements IAppPresenter,
		OpenDisplayPanelEvent.Handler, CloseDisplayPanelEvent.Handler,
		OpenedComplexDialogEvent.Handler, ClosedComplexDialogEvent.Handler,
		ChangedViewEvent.Handler, RequestLaunchToolEvent.Handler,
		RequestRemoveExperimentEvent.Handler,
		RequestShowPermissionsEvent.Handler, OpenProjectEvent.Handler,
		RequestReloadSnapshotEvent.Handler, ServerTitleObtainedEvent.Handler,
		PresenterSelectionHandler, IAppSaveAndShutdown,
		ToolMarkedAsFavoriteEvent.Handler, BookmarkSnapshotEvent.Handler,
		UnbookmarkSnapshotEvent.Handler,
		LoadSnapshotDetailsCommand.ISnapshotLoader,
		SearchResultsUpdatedEvent.Handler {
	public static final String URL = "http://main.ieeg.org";

	// TODO: exploit this
	public static Map<String, List<String>> typePanelMap = new HashMap<String, List<String>>();

	Map<AstroPanel, Presenter> presenters = Maps.newHashMap();
	// Display display;
	// ClientFactory clientFactory;
	// String username;

	ProjectPresenter project;

	// TODO: move this out
	AppModel controller;
	
	PanelUpdater updater;

	HandlerRegistration lastSave;
	HandlerRegistration lastSearch;
	HandlerRegistration lastShare;
	HandlerRegistration lastHelp = null;
	
	private boolean handlersBound = false;
	// Stack<HandlerRegistration> handlers = new Stack<HandlerRegistration>();

	public final static String NAME = "main-app";
	public final static Set<String> TYPES = Sets.newHashSet();
	
	public AppPresenter() {
		super(null, NAME, TYPES);
	}

	public AppPresenter(
			final ClientFactory factory,
			final AppModel controller) {
		super(factory, NAME, TYPES);
		init(factory, controller);
	}
	
	public void init(
			final ClientFactory factory,
			final AppModel controller) {
		this.clientFactory = factory;
		this.controller = controller;
		
		updater = new PanelUpdater() {

			@Override
			public void addPanel(AstroPanel panel,
					CollapsibleTab tab, boolean closeable) {
				getDisplay().addPanel(panel, tab, closeable);
			}

			@Override
			public void removePanel(AstroPanel panel) {
				getDisplay().removePanel(panel);
			}

			@Override
			public void addSnapshotDescriptor(String snapshot,
					PanelDescriptor sec) {
				controller.getPlace().addDescriptor(snapshot,
						sec);
			}

			@Override
			public void addPanelDescriptor(AstroPanel win,
					PanelDescriptor desc) {
				controller.getDescriptors().put(win, desc);
			}
			
		};
	}

	public IAppPresenter.Display getDisplay() {
		return (IAppPresenter.Display) display;
	}

	@Override
	public Presenter create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		return new AppPresenter(clientFactory,
				controller);
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		clientFactory.getPortal().getAvailableFilterTypes(
				new AsyncCallback<List<String>>() {

					@Override
					public void onFailure(Throwable caught) {
					}

					@Override
					public void onSuccess(List<String> result) {
						getSessionModel().setFilterTypes(result);
					}

				});
		getDisplay().addPresenter(this);
		getSessionModel().setHeaderBar(getDisplay().getHeaderBar());

		// Help handler always active.
		getSessionModel().getHeaderBar().addGlobalEntry("help", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				OpenDisplayPanel action = new OpenDisplayPanel(
						new WebLink("", "Documentation",
								"https://main.ieeg.org/sites/default/files/IEEGDocumentation.pdf"),
						false, WebPanel.NAME, null);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(
						new OpenDisplayPanelEvent(action));
			}
		});

		if (getSessionModel().isRegisteredUser()) {
			installGlobalMenusAndToolbar();
		}
		getDisplay().initPanels(controller);
		if (getDisplay().getActivePanel() != null) {
			getSessionModel().getHeaderBar().setActivePanel(getDisplay().getActivePanel());
		}

		List<AstroPanel> ret = getDisplay().getPanels();

		for (AstroPanel panel : ret) {
			if (panel == null)
				GWT.log("Null panel");
			if (controller == null)
				GWT.log("Null controller state");
			Presenter pres = PresenterFactory.getPresenter(
					panel.getPanelType(), clientFactory,
					controller, null,
					controller.getCurrentProject(), false);

			pres.setDisplay((Presenter.Display) panel);
			pres.bind(selected);
			presenters.put(panel, pres);
			controller.addWindow(panel);
		}

		IeegEventBusFactory.getGlobalEventBus().registerHandler(
				OpenedPanelEvent.getType(), new OpenedPanelEvent.Handler() {

					@Override
					public void onOpenedPanel(String panelType,
							PanelDescriptor desc, AstroPanel pane) {
						controller.addWindow(pane);
						IeegEventBusFactory.getGlobalEventBus().fireEvent
							(new ChangedViewEvent(pane));
					}

				});
		IeegEventBusFactory.getGlobalEventBus().registerHandler(
				ClosedPanelEvent.getType(), new ClosedPanelEvent.Handler() {

					@Override
					public void onClosedPanel(String panelType,
							PanelDescriptor desc, AstroPanel pane) {
						controller.removeWindow(pane);
					}

				});
		if (getSessionModel().isRegisteredUser())
			IeegEventBusFactory.getGlobalEventBus().registerHandler(
					ToolMarkedAsFavoriteEvent.getType(), this);

		IeegEventBusFactory.getGlobalEventBus().registerHandler(
				BookmarkSnapshotEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(
				UnbookmarkSnapshotEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus().registerHandler(
				SearchResultsUpdatedEvent.getType(), 
				this);

		getDisplay().setTabSelectionHandler(new SelectionHandler<Integer>() {
			public void onSelection(SelectionEvent<Integer> event) {
				Widget w = getDisplay().getSelectedWidget();

				if (w instanceof FocusPanel) {
					((FocusPanel) w).setFocus(true);
				}
				if (w instanceof AstroPanel) {
					getSessionModel().getPeriodicEvents().setForegroundWindow(
							(AstroPanel) w);
					int minH = ((AstroPanel) w).getMinHeight();
					DOM.getElementById("rootLayoutPanel").getStyle()
							.setProperty("minHeight", minH + "px");
				}
			}

		});

		getDisplay().addLogoHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(URL);

			}

		});
		getDisplay().addTitleHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(URL);

			}

		});
		GWT.log("Close handler test: " + getSessionModel().isRegisteredUser());

		if (getSessionModel().isRegisteredUser()) {
			getDisplay().setCloseHandler((new Window.ClosingHandler() {

				@Override
				public void onWindowClosing(ClosingEvent event) {

					event.setMessage("Are you sure you want to log out and leave?");

					// Chrome does not always make RPC calls in CloseHandlers,
					// so to make sure the workspace is saved we do it here.
					GWT.log("Closing... saving workspace");
					if (getSessionModel().isRegisteredUser())
						getClientFactory()
								.getPortal()
								.saveWorkPlace(
										getSessionModel().getSessionID(),
										controller.getWorkPlace(),
										getSessionModel().getUserPreferences(),
										new SecFailureAsyncCallback.SimpleSecFailureCallback<Boolean>(
												getClientFactory()) {

											@Override
											public void onNonSecFailure(
													Throwable caught) {
												if (caught instanceof StatusCodeException
														&& (((StatusCodeException) caught))
																.getStatusCode() == 0) {
													// This can happen when the
													// user has chosen to
													// navigate away and
													// the browser does not wait
													// for the server's
													// response.
												} else {
													Dialogs.messageBox(
															"Unable to save workspace",
															"Error in saving workspace");
												}
											}

											@Override
											public void onSuccess(Boolean result) {
											}

										});

				}
			}), (new CloseHandler<Window>() {

				@Override
				public void onClose(CloseEvent<Window> event) {
					// Do not make any critical RPC calls here. We have found
					// that some browsers do not make RPC calls from this
					// handler.
					GWT.log("Closed... removing cookie and attempting session expiration");
					Cookies.removeCookie(ToolUploadCookies.COOKIE_NAME,
							ToolUploadCookies.SERVLET_PATH);
					getClientFactory().getPortal().logoutSession(
							getSessionModel().getSessionID(),
							new SecFailureAsyncCallback<Void>(
									getClientFactory()) {

								@Override
								public void onFailureCleanup(Throwable caught) {
									GWT.log("Failed to expire session: "
											+ caught.getMessage());
								}

								@Override
								public void onNonSecFailure(Throwable caught) {
								}

								@Override
								public void onSuccess(Void result) {
									GWT.log("Expired session");
								}
							});

				}

			}));
		}
		getDisplay().addLogoutHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				clientFactory.getPortal().saveWorkPlaceAndLogout(
						getSessionModel().getSessionID(),
						controller.getWorkPlace(),// getDisplay().getCurrentWorkPlace(),
						getSessionModel().getUserPreferences(),
						new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								unbind();
								Window.Location.assign(GWT.getHostPageBaseURL()
										+ "main.html");
							}

							@Override
							public void onSuccess(Void result) {
								unbind();
								Window.Location.assign(GWT.getHostPageBaseURL()
										+ "main.html");
							}
						});

			}

		});

		

		// Handlers get called more than once if they are registered more than
		// once.
		if (!handlersBound) {
			IEventBus bus = IeegEventBusFactory.getGlobalEventBus();

			bus.registerHandler(ServerTitleObtainedEvent.getType(), this);
			bus.registerHandler(RequestReloadSnapshotEvent.getType(), this);
			bus.registerHandler(OpenProjectEvent.getType(), this);
			bus.registerHandler(RequestLaunchToolEvent.getType(), this);
			bus.registerHandler(RequestRemoveExperimentEvent.getType(), this);
			bus.registerHandler(ChangedViewEvent.getType(), this);
			bus.registerHandler(OpenDisplayPanelEvent.getType(), this);
			bus.registerHandler(CloseDisplayPanelEvent.getType(), this);
			bus.registerHandler(OpenedComplexDialogEvent.getType(), this);
			bus.registerHandler(ClosedComplexDialogEvent.getType(), this);
			handlersBound = true;
		}
		// Only load workspace if user is not Guest.
		if (!getSessionModel().getUserInfo().getName().equals("guest")) {

			clientFactory
					.getPortal()
					.loadWorkPlace(
							getSessionModel().getSessionID(),
							new SecFailureAsyncCallback.SimpleSecFailureCallback<WorkPlace>(
									clientFactory) {

								@Override
								public void onNonSecFailure(Throwable caught) {
									// Do not show error.
								}

								@Override
								public void onSuccess(WorkPlace result) {
									if (result != null) {
										goTo(result);
									}
								}
							});
		}

		if (com.google.gwt.user.client.Window.Location
				.getParameter("snapshots") != null) {
			String[] snapshots = com.google.gwt.user.client.Window.Location
					.getParameter("snapshots").split(",");

			if (snapshots != null && snapshots.length > 0) {
				Set<String> toLoad = new HashSet<String>();
				for (int i = 0; i < snapshots.length; i++)
					toLoad.add(snapshots[i]);

				clientFactory.getSearchServices().getSearchResultsForSnapshots(
						getSessionModel().getSessionID(), toLoad,
						new AsyncCallback<Set<SearchResult>>() {

							@Override
							public void onFailure(Throwable caught) {
								GWT.log("Failure " + caught);
								// Ignore; this was a best-effort
							}

							@Override
							public void onSuccess(Set<SearchResult> result) {
								GWT.log("Got back " + result);
								for (SearchResult sr : result)
									openDisplayPanel(
											sr, true, EEGViewerPanel.NAME, result, null);
							}

						});
			}

		}
	}

	private void installGlobalMenusAndToolbar() {
		// Global option: account
		getSessionModel().getHeaderBar().addGlobalMenuOption(
				new MenuItem("Account settings", new ShowAccountCommand(
						null, getSessionModel()
								.getHeaderBar().getPopup())));

		// Global option: change password
		getSessionModel().getHeaderBar().addGlobalMenuOption(
				new MenuItem("Change password", new ShowPasswordCommand(
						null, getSessionModel()
								.getHeaderBar().getPopup())));

		// Global option: change password
		getSessionModel().getHeaderBar().addGlobalMenuOption(
				new MenuItem("Preferences", new ShowPrefsCommand(
						null, getSessionModel().getHeaderBar()
						.getPopup())));

		// Global option: upload. Not completed
		//getSessionModel().getHeaderBar().addGlobalMenuOption(
		//		new MenuItem("Upload data", new ShowUploadCommand(null, getSessionModel().getHeaderBar()
		//				.getPopup())));
	}

	@Override
	public void unbind() {
		super.unbind();
		// Remove closing/close handlers so that reassignment is uninterrupted.
		Cookies.removeCookie(ToolUploadCookies.COOKIE_NAME,
				ToolUploadCookies.SERVLET_PATH);
		getDisplay().removeWorkspaceHandlers();

		// while (!handlers.isEmpty()) {
		// handlers.pop().removeHandler();
		// }
		// clientFactory.unregisterHandler(this);

		if (lastSave != null)
			lastSave.removeHandler();

		if (lastShare != null)
			lastShare.removeHandler();

		if (lastSearch != null)
			lastSearch.removeHandler();

		lastSave = null;
		lastShare = null;
		lastSearch = null;
	}

	@Override
	public void onOpenNewPanel(OpenDisplayPanel action) {
		openDisplayPanel(action.ds, action.addIt, action.panel, action.selected,
				action.winPlace);
	}

	@Override
	public void onOpenedDialog(Widget action) {
//		getDisplay().hideGraph();
	}

	@Override
	public void onClosedDialog(Widget action) {
//		getDisplay().showGraph();
	}

	@Override
	public void onChangedView(final AstroPanel pane) {
		getSessionModel().getHeaderBar().setActivePanel(pane);
	}

//	public void launchSearch(final String userID) {
//		if (!clientFactory.isRegisteredUser()
//				|| clientFactory.getServerConfiguration().isUniversity())
//			return;
//
//		getDisplay().selectSearchTab();
//		SearchDialog ss = new SearchDialog(userID, null, clientFactory);
//
//		ss.center();
//		ss.show();
//	}

	public void saveAs(final String userid, final String revId,
			AnnotationGroup<Annotation> annotationGroup) {
		DataSnapshotViews model = controller.getDataSnapshots().get(revId);
		final Set<Annotation> annotationsToSave = new HashSet<Annotation>();

		for (AnnotationGroup<Annotation> a : annotationGroup
		/* s.getGroupFor(model) */.getSubGroups()) {
			if (a.isEnabled()) {
				for (Annotation ann : a.getAllEnabledAnnotations()) {
					Annotation ac = ann.createCopy();
					ac.setLayer(a.getName());
					annotationsToSave.add(ac);
				}
			}
		}

		if (!annotationsToSave.isEmpty()) {
			SaveSnapshotAs ssa = new SaveSnapshotAs(revId, userid, model,
					annotationsToSave, clientFactory, getDisplay()
							.getActivePanel());
			ssa.center();
			ssa.show();
		}
	}

	public void launchExperiment(final ToolDto tool) {

		List<SearchResult> studies = new ArrayList<SearchResult>();
		for (GeneralMetadata md : controller.getSnapshotBrowserModel()
				.getKnownSnapshots())
			if (md instanceof SearchResult)
				studies.add((SearchResult) md);

		SetExperimentSource ses = new SetExperimentSource(
				studies,
				tool, controller.getDataSnapshots(), clientFactory
		);
		ses.center();
		ses.show();
	}

	public void openUrlInPDFViewer(final String panelType,
			final String title, final String url) {
		WindowPlace place = new URLPlace(null, url, PDFPresenter.NAME);
		final AstroPanel panel = PanelFactory.getPanel(panelType,
				clientFactory, controller, null, // controller.getCurrentProject(),
				false, getNextWindow());
		final Presenter p = PresenterFactory.getPresenter(panelType,
				clientFactory, controller, null,
				controller.getCurrentProject(), false);

		panel.setPlace(place);
		if (p != null) {
			p.setDisplay((Presenter.Display) panel);
			p.bind(new HashSet<PresentableMetadata>());

			getDisplay().addPanel(
					panel,
					new CollapsibleTab(title, panel.getIcon(clientFactory),
							false, true, new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									p.unbind();
									presenters.remove(panel);
									getDisplay().removePanel(panel);
								}

							}), true);

			IeegEventBusFactory.getGlobalEventBus().fireEvent(
					new OpenedPanelEvent(panel.getDescriptor(), panel));

			getSessionModel().getPeriodicEvents().setForegroundWindow(panel);
		}
	}

	public void openUrl(final String panelType,
			final String title, final PresentableMetadata url) {

		final AstroPanel panel = PanelFactory
				.getPanel(panelType, clientFactory, controller, null,
						false, getNextWindow());
		final Presenter p = PresenterFactory.getPresenter(panelType,
				clientFactory, controller, null, url, false);

		if (p != null) {
			p.setDisplay((Presenter.Display) panel);
			p.bind(new HashSet<PresentableMetadata>());

			getDisplay().addPanel(
					panel,
					new CollapsibleTab(title, panel.getIcon(clientFactory),
							false, true, new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									p.unbind();
									presenters.remove(panel);
									getDisplay().removePanel(panel);
								}

							}), true);

			IeegEventBusFactory.getGlobalEventBus().fireEvent(
					new OpenedPanelEvent(panel.getDescriptor(), panel));

			getSessionModel().getPeriodicEvents().setForegroundWindow(panel);
		}
	}

	/**
	 * As necessary, loads the DataSnapshotModel corresponding to the
	 * SearchResult. Then opens the requested panelType and sets the
	 * WindowPlace.
	 * 
	 * @param ds
	 * @param addIt
	 * @param panelType
	 * @param winPlace
	 */
	private void openDisplayPanel(final GeneralMetadata ds,
			boolean addIt, final String panelType, 
			final Collection<? extends PresentableMetadata> selected,
			final WindowPlace winPlace) {

		CreateWindow cw = new CreateWindow() {

			@Override
			public AstroPanel createWindow(
					DataSnapshotViews model, SearchResult ds,
					final Collection<? extends PresentableMetadata> selected,
					boolean writePermissions, WindowPlace wPlace) {

				// DICOM is weird in that there's no actual panel
				if (panelType.equals(AttachmentDownloader.NAME)) {
					return null;
				} else {
					// Create the panel
					AstroPanel pan = PanelFactory.getPanel(
							panelType,
							clientFactory, 
							controller, 
							model,
							writePermissions, 
							getNextWindow());

					pan.setPlace(winPlace);
					return pan;
				}
			}

			@Override
			public boolean switchToWindow(
					DataSnapshotViews model, SearchResult ds,
					final Collection<? extends PresentableMetadata> selected,
					WindowPlace place) {

//				GWT.log("Looking to switch to window of type " + panelType);
				boolean exists = false;
				for (AstroPanel pan : getDisplay().getPanels()) {
					if (pan.getPanelType().equals(panelType)
							&& (model == null || pan.getDataSnapshotState() == model
									.getState())) {
						// Don't try to switch to existing window place if the
						// places are different. So we can have multiple documents open from the same dataset.
						if (place != null && !place.equals(pan.getPlace())) {
							continue;
						}
						exists = true;
						getDisplay().setPanel(pan);

//					} else {
//						getDisplay().log(4, "Found " + pan.getPanelType()
//								+ " with snapshot "
//								+ pan.getDataSnapshotState());
					}
				}

				if (!exists) {
					GWT.log("NOT FOUND");
					return false;
				}

				getSessionModel().getMainLogger().info(
						"Switching to " + model.getState().getFriendlyName()
								+ "/" + panelType);// .name());

				// clientFactory.fireEvent(new OpenDisplayPanelEvent(new
				// OpenDisplayPanelAction(clientFactory.getUserId(),
				// model.getSearchResult(), false,
				// panelType, null)));

				return true;
			}

			@Override
			public String getTitle(DataSnapshotViews model,
					SearchResult ds, 
					final Collection<? extends PresentableMetadata> selected,
					boolean writePermissions) {
				if (panelType.equals(EEGViewerPanel.NAME))
					return model.getState().getFriendlyName()
							+ (writePermissions ? "" : " (R/O)");
				else if (model != null && model.getState() != null)
					return model.getState().getFriendlyName();
				else
					return "";
			}

			@Override
			public ImageResource getIcon(
					DataSnapshotViews model, SearchResult ds,
					final Collection<? extends PresentableMetadata> selected,
					boolean writePermissions) {

				return PanelFactory.getIcon(panelType, clientFactory);
			}

			@Override
			public Presenter createPresenter(
					DataSnapshotViews model, SearchResult ds,
					final Collection<? extends PresentableMetadata> selected,
					boolean writePermissions, WindowPlace place) {

				if (panelType.equals(AttachmentDownloader.NAME)) {
					getSessionModel().getMainLogger().info("DICOM download");
					new AttachmentDownloader(clientFactory, model.getState(),
							place);
					return null;
				} else
					return PresenterFactory.getPresenter(panelType,
							clientFactory, controller, model,
							controller.getCurrentProject(), writePermissions);
			}

		};

		GeneralMetadata md = ds;
		if (md != null) {
			while (md != null && !(md instanceof SearchResult)
					&& md.getParent() != null)
				md = md.getParent();
		}
		SearchResult study = null;

		if (md == null || md instanceof SearchResult)
			study = (SearchResult) md;
		else {
			if (md instanceof WebLink) {
				WebLink wl = (WebLink) md;
				WebPresenter wp = (WebPresenter) addViewerWindow(null, 
						null, new HashSet<PresentableMetadata>(),
						cw, winPlace, panelType, wl.getFriendlyName());

				wp.openURL(wl.getDetails(), "", wp.getDisplay().getWebFrame());

			}
			return;
		}

		loadStudyIfNecessary(study, selected, panelType, winPlace, addIt, cw);

	}

	public DataSnapshotViews loadStudyIfNecessary(
			final SearchResult ds, 
			final Collection<? extends PresentableMetadata> selected,
			final String panelType,
			final WindowPlace winPlace, final boolean addIfExists,
			final CreateWindow win) {

		final String revID = (ds == null) ? null : ds.getDatasetRevId();
		final String friendlyName = (ds == null) ? null : ds.getFriendlyName();
		final String baseID = (ds == null) ? null : ds.getBaseRevId();

		GWT.log("Open snapshot requested");
		if (ds != null && controller.getDataSnapshots().get(revID) == null) {
			// GWT.log("Snapshot is not yet open so we'll load it");
			EEGModel data = new EEGModel();

			// manager.popContext();

			// if (ds != null)
			// GWT.log("Creator is " + ds.getCreator());

			final DataSnapshotViews dsm = new DataSnapshotViews(
					revID, friendlyName, getSessionModel().getUserId(), ds.getCreator(),
					clientFactory.getSnapshotServices(), clientFactory.getPortal(), data,
					controller, ds, clientFactory);

			final DataSnapshotModel state = dsm.getState();

			state.setParentResultID(baseID);
			controller.getDataSnapshots().put(revID, dsm);

			Command cmd = new OpenAndLoadSnapshotCommand(
					clientFactory,
					ds,
					selected,
					dsm,
					controller.getSnapshotBrowserModel(),
					panelType, winPlace, addIfExists, win,
					updater);
			cmd.execute();
			
//			openSnapshot(dsm, ds, panelType, winPlace, addIfExists, win);
			return dsm;
		} else {
			// GWT.log("Snapshot is already open so we'll reuse it");
			final DataSnapshotViews model = (revID == null) ? null
					: controller.getDataSnapshots().get(revID);

			// boolean done = false;
			// if (!addIfExists) {
			// // clientFactory.getMainLogger().info("Model for " +
			// model.getFriendlyName() +
			// " exists...  Trying to switch to window");
			boolean done = win.switchToWindow(model, ds, selected, winPlace);
			//
			// }

			if (!done) {
				// clientFactory.getMainLogger().info("Model for " +
				// model.getFriendlyName() + " exists... Need to add a window");
				// Add a viewer window

				addViewerWindow(model, ds, selected, win, winPlace, panelType,
						win.getTitle(model, ds, selected,
								(model == null) ? true : model.isWritable()));

			}
		}
		return controller.getDataSnapshots().get(revID);
	}

	private Presenter addViewerWindow(final DataSnapshotViews model,
			SearchResult ds, 
			final Collection<? extends PresentableMetadata> selected,
			CreateWindow win, WindowPlace winPlace,
			String panelType, String title) {
		boolean writable = (model != null) ? model.isWritable() : true;
		final AstroPanel n = win.createWindow(model,
				ds, selected, writable, winPlace);

		final Presenter p = win.createPresenter(
				model, ds, selected, writable, winPlace);

		if (p != null) {
			getDisplay().addPanel(
					n,
					new CollapsibleTab(title, win.getIcon(
							model, ds, selected, writable),
							false, true, new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									p.unbind();
									if (model != null)
										model.removePane(n);
									getDisplay().removePanel(n);
								}

							}), true);
			String nam = "";
			if (ds != null)
				nam = "/" + ds.getFriendlyName();
			GWT.log("Added " + panelType + " for " + getSessionModel().getUserId()
					+ nam);

			p.setDisplay((Presenter.Display) n);
			p.bind(selected);
			GWT.log("Bound presenter " + panelType + " for "
					+ getSessionModel().getUserId() + nam);

			// tabPanel.selectTab(tabPanel.getWidgetIndex(n));
			if (model != null)
				model.addPane(n);
			if (ds != null)
				controller.getPlace().addDescriptor(ds.getDatasetRevId(),
						n.getDescriptor());
			controller.getDescriptors().put(n, n.getDescriptor());

			getSessionModel().getPeriodicEvents().setForegroundWindow(n);

			if (model != null)
				n.setDataSnapshotModel(model.getState());
		}
		return p;
	}

//	private void openSnapshot(
//			final DataSnapshotViews dsm,
//			// final String username,
//			final SearchResult ds, final String panelType,
//			final WindowPlace winPlace, final boolean addIfExists,
//			final CreateWindow win) {
//		final String revID = ds.getDatasetRevId();
//		final String friendlyName = ds.getFriendlyName();
////		final String baseID = ds.getBaseRevId();
//		final DataSnapshotModel state = dsm.getState();
//
//		clientFactory.getSnapshotServices().getSnapshotMetadata(
//				clientFactory.getSessionID(),
//				revID,
//				new SecFailureAsyncCallback.AlertCallback<SnapshotContents>(
//						clientFactory,
//						"There was an error while retrieving permissions.") {
//
//					@Override
//					public void onSuccess(SnapshotContents result) {
//						GWT.log("Check write permitted on snapshot");
//						boolean canWrite = result.getPermissions().contains(
//								CorePermDefs.EDIT);
//
//						// Add a viewer window
//						final AstroPanel n = win.createWindow(
//								clientFactory.getUserId(), dsm, ds, canWrite,
//								winPlace);
//						final Presenter p = win.createPresenter(
//								clientFactory.getUserId(), dsm, ds, canWrite,
//								winPlace);
//
//						if (p != null) {
//							getDisplay().addPanel(
//									n,
//									new CollapsibleTab(win.getTitle(
//											clientFactory.getUserId(), dsm, ds,
//											canWrite), win.getIcon(
//											clientFactory.getUserId(), dsm, ds,
//											canWrite), false, true,
//											new ClickHandler() {
//
//												@Override
//												public void onClick(
//														ClickEvent event) {
//													p.unbind();
//													getDisplay().removePanel(n);
//													dsm.removePane(n);
//												}
//
//											}), true);
//							GWT.log("Added " + panelType + " for "
//									+ clientFactory.getUserId() + "/"
//									+ ds.getFriendlyName());
//
//							p.setDisplay((Presenter.Display) n);
//							p.bind();
//							GWT.log("Bound presenter " + panelType + " for "
//									+ clientFactory.getUserId() + "/"
//									+ ds.getFriendlyName());
//
//							// tabPanel.selectTab(tabPanel.getWidgetIndex(n));
//
//							dsm.setWritable(canWrite);
//							dsm.addPane(n);
//							controller.getPlace().addDescriptor(revID,
//									n.getDescriptor());
//							controller.getDescriptors().put(n,
//									n.getDescriptor());
//						}
//
//						controller.getSnapshotBrowserModel().addKnownContents(
//								ds, result.getTraces());
//
//						for (TraceInfo t : result.getTraces()) {
//							SeriesData sd = new SeriesData();
//							sd.friendly = friendlyName;
//							sd.studyId = revID;
//							sd.label = t.getLabel();
//							sd.revId = t.getRevId();
//							state.addSeriesToList(sd);
//						}
//
//						dsm.setChannelsAndPlace(result.getTraces(), winPlace);
//
//						if (clientFactory.isRegisteredUser()) {
//							IeegEventBusFactory.getGlobalEventBus().fireEvent(
//									new ToolResultsUpdatedEvent(ds
//											.getDatasetRevId(), result
//											.getDerivedResults()));
//
//							for (SearchResult s : result.getDerived()) {
//								s.setBaseRevId(ds.getDatasetRevId());
//								s.setBaseFriendly(ds.getFriendlyName());
//								ds.addDerived(s);
//								ds.addKnownChild(s);
//								s.setParent(ds);
//								controller.getSnapshotBrowserModel()
//										.addKnownContents(ds, s);
//							}
//							// getDisplay().setResultsFor(ds,
//							// result.getDerived());
//						}
//						for (RecordingObject recordingObject : result
//								.getRecordingObjects()) {
//							FileInfo si = new FileInfo(recordingObject
//									.getName(), revID, friendlyName,
//									recordingObject.getId().toString(),
//									recordingObject.getInternetMediaType());
//							si.setParent(ds);
//							ds.addDerived(si);
//							ds.addKnownChild(si);
//							if (recordingObject.getInternetMediaType().equals(
//									"application/pdf")) {
//								clientFactory.getMainLogger()
//										.info("Adding report file to "
//												+ friendlyName);
//
//								GWT.log("Registering report");
//								controller.getSnapshotBrowserModel()
//										.addKnownContents(ds, si);
//								state.getDocuments().add(si);
//							} else {
//								clientFactory.getMainLogger().info(
//										"Adding file to " + friendlyName);
//
//								GWT.log("Registering file");
//								controller.getSnapshotBrowserModel()
//										.addKnownContents(ds, si);
//								state.getDicoms().add(si);
//							}
//						}
//
//						clientFactory.getPeriodicEvents()
//								.setForegroundWindow(n);
//						IeegEventBusFactory.getGlobalEventBus().fireEvent(
//								new SnapshotOpenedEvent(revID, state, ds));
//						// Do not fire these events.
//						// Allow the event in the call to Presenter.bind() to
//						// handle the
//						// document.
//						// clientFactory.fireEvent(
//						// new DocumentReadyEvent(state));
//						// Allow the event in the DicomDownloader constructor to
//						// handle the
//						// download.
//						// clientFactory.fireEvent(
//						// new DicomReadyEvent(state));
//
//					}
//
//				});
//	}

	public void openProject(final Project project) {
		getDisplay().closeProject();
		controller.setCurrentProject(project);

		List<SearchResult> searchSnapshots = new ArrayList<SearchResult>();
		if (getSessionModel().isRegisteredUser()) {
			for (GeneralMetadata g : controller.getSnapshotBrowserModel()
					.getKnownSnapshots())
				if (g instanceof SearchResult)
					searchSnapshots.add((SearchResult) g);
		}
		AstroPanel panel = getDisplay().openProject(
				controller.getCurrentProject(), controller, searchSnapshots);

		this.project = new ProjectPresenter(
				controller, clientFactory, project);

		this.project.setDisplay((Presenter.Display) panel);
		this.project.bind(new HashSet<PresentableMetadata>());

		IeegEventBusFactory.getGlobalEventBus().fireEvent(
				new ProjectOpenedEvent(project));
	}

	@Override
	public void onLaunchTool(ToolDto tool) {
		launchExperiment(tool);
	}

	public void removeExperiment(DerivedSnapshot tr) {

		// Allow for a text box that's an XML editor
		// Specify a target name
		// Specify a set of channels that go into the input
		// Specify a set of annotations that go into the input?

		// Then create a new data snapshot (if one exists, prompt for overwrite)
		// Copy in the channels
		// Copy in the annotations
		// Copy in the XML

		// Prompt with a URL
		// Wait for the cs.getUserID() to press [Done]

		String snap = tr.getSnapshotRevId();

		DialogBox d = PanelFactory.getPleaseWaitDialog("Removing result");
		d.center();
		d.show();

//		controller.getDataSnapshots().get(snap).removeResult(tr, d);
		RemoveSnapshotCommand rsc = new RemoveSnapshotCommand(
				clientFactory,
				controller,
				tr.getAnalysisId(),
				d,
				null
				);
		rsc.execute();

		List<AstroPanel> lst = getDisplay().getPanels();
		for (int i = 0; i < lst.size(); i++) {
			AstroPanel panel = lst.get(i);
			if (panel.getDataSnapshotState() != null
					&& panel.getDataSnapshotState().getSnapshotID()
							.equals(snap)) {
				lst.remove(i);
				i--;
			}
		}
	}

	@Override
	public void onRemoveExperiment(DerivedSnapshot result) {
		removeExperiment(result);
	}

	@Override
	public void onPermissionsRequested(String userId, SearchResult snapshot) {
		EditPermissionsCommand ch = new EditPermissionsCommand(clientFactory,
				userId, snapshot);

		ch.execute();
	}

	@Override
	public void onPermissionsRequested(String userId, DerivedSnapshot result) {
		EditPermissionsCommand ch = new EditPermissionsCommand(clientFactory,
				userId, result);

		ch.execute();
	}

	@Override
	public void onOpenedProject(Project project) {
		openProject(project);
	}

	public void loadAndCopyAnnotations(String userid, final SearchResult sr,
			final String prefix) {
		Command cmd = new LoadAnnotationsAndCopyCommand(
				clientFactory,
				sr, 
				prefix, 
				controller.getSnapshotBrowserModel(),
				controller.getSnapshotState(sr.getDatasetRevId()),
				controller.getDataSnapshots());
		cmd.execute();

		//		clientFactory.getSnapshotServices().getDataSnapshotAnnotations(
//				clientFactory.getSessionID(),
//				sr.getDatasetRevId(),
//				new SecFailureAsyncCallback.SecAlertCallback<List<Annotation>>(
//						clientFactory) {
//
//					public void onSuccess(List<Annotation> tracesIn) {
//						List<AnnotationSet<Annotation>> anns = new ArrayList<AnnotationSet<Annotation>>();
//						AnnotationSet<Annotation> nA = new AnnotationSet<Annotation>();
//						nA.label = sr.getFriendlyName();
//						nA.onStudyRevId = sr.getDatasetRevId();
//						nA.annotations = tracesIn;
//						anns.add(nA);
//
//						List<SearchResult> studies = new ArrayList<SearchResult>();
//						for (GeneralMetadata md : controller
//								.getSnapshotBrowserModel().getKnownSnapshots())
//							if (md instanceof SearchResult)
//								studies.add((SearchResult) md);
//
//						SetAnnotationTarget sta = new SetAnnotationTarget(
//								// manager.getStudies(), // Set of studies
//								studies,
//								// manager.getSeriesList(),
//								controller.getSnapshotState(
//										sr.getDatasetRevId()).getSeriesList(),
//								anns, // List of annotation sets
//								// dsm.getAnnotationMap(),
//								prefix, controller.getDataSnapshots()); // Map
//						// of
//						// annotation
//						// sets
//
//						sta.center();
//						sta.show();
//					}
//
//				});
	}

	@Override
	public void onReloadSnapshot(String oldId, String newId) {
		Command cmd = new UpdateReloadedSnapshotCommand(clientFactory, oldId, newId,
				controller.getDataSnapshots(),
				getDisplay().getPanels());
		cmd.execute();
//		List<String> existing = Lists.newArrayList();
//		List<WindowPlace> places = Lists.newArrayList();
//		for (AstroPanel p : getDisplay().getPanels()) {
//			if (p.getDataSnapshotState() != null
//					&& p.getDataSnapshotState().getSnapshotID().equals(oldId)) {
//				existing.add(p.getPanelType());
//				places.add(p.getPlace());
//			}
//		}
//		SearchResult sr = controller.getDataSnapshots().get(oldId)
//				.getSearchResult();
//		if (controller.getDataSnapshots().containsKey(oldId)) {
//			// existing = cs.getDataSnapshots().get(study).getPanelTypes();
//			controller.getDataSnapshots().get(oldId).removeThisStudy();
//			controller.getDataSnapshots().remove(oldId);
//		}
//		sr.setDatasetRevId(newId);
//		// loadStudy(study, existing);
//		for (int i = 0; i < places.size(); i++) {
//			IeegEventBusFactory.getGlobalEventBus().fireEvent(
//					new OpenDisplayPanelEvent(new OpenDisplayPanel(
//							clientFactory.getUserId(), sr, true, existing
//									.get(i), places.get(i))));
//		}
	}

	public void select(DataSnapshotModel model, String panelType) {
		this.openDisplayPanel(
				model.getSearchResult(), 
				false, panelType, new HashSet<PresentableMetadata>(), null);
	}

	@Override
	public void onOpenNewPanel(String title, PresentableMetadata url) {
		if (url instanceof FileInfo)
			openUrlInPDFViewer(PDFPresenter.NAME,
					title, ((FileInfo) url).getDetails());
		else
			openUrl(WebPresenter.NAME, title, url);
	}

	static int count = 0;

	private String getNextWindow() {
		return "Data " + count++;
	}

	@Override
	public void onClosePanel(AstroPanel panel) {
		getDisplay().removePanel(panel);
		if (presenters.get(panel) != null) {
			presenters.get(panel).unbind();
			presenters.remove(panel);
		}
	}

	public Presenter createNewPanelAndPresenter(AppModel controller,
			String panelType, PresentableMetadata target,
			DataSnapshotViews dsc, PresentableMetadata project,
			boolean isWritable) {
		AstroPanel panel = getDisplay().createPanel(controller, panelType,
				target, dsc, isWritable, true);
		Presenter pres = PresenterFactory.getPresenter(panelType,
				clientFactory, controller, dsc,
				project,// controller.getCurrentProject(),
				isWritable);

		pres.setDisplay((Presenter.Display) panel);
		HashSet<PresentableMetadata> set = new HashSet<PresentableMetadata>();
		set.add(target);
		pres.bind(set);
		presenters.put(panel, pres);

		controller.getPlace().addDescriptor(target.getId(),
				panel.getDescriptor());
		controller.getDescriptors().put(panel, panel.getDescriptor());

		return pres;
	}

	public void removePanel(String revID, AstroPanel w) {
		controller.getPlace().removeDescriptor(revID, w.getDescriptor());
		controller.getDescriptors().remove(w);
		getDisplay().removePanel(w);
		presenters.remove(w);
	}

	@Override
	public void onNewTitle(String newTitle) {
		if (!newTitle.isEmpty())
			getSessionModel().getHeaderBar().setAppName(newTitle);
	}

	/**
	 * Favorite snapshots
	 * 
	 * @param faves
	 * @param known
	 */
	private void restoreFavorites(Set<String> faves,
			final Map<String, SearchResult> known) {
		getClientFactory()
				.getSearchServices()
				.getSearchResultsForSnapshots(
						getSessionModel().getSessionID(),
						faves,
						new SecFailureAsyncCallback.SimpleSecFailureCallback<Set<SearchResult>>(
								getClientFactory()) {

							@Override
							public void onNonSecFailure(Throwable caught) {
								Dialogs.messageBox(
										"Error restoring favorite snapshots for "
												+ getSessionModel().getUserId(),
										caught.getMessage());
							}

							@Override
							public void onSuccess(Set<SearchResult> result) {

								for (SearchResult sr : result) {
									System.out.println("Bookmark: " + sr);
									IeegEventBusFactory.getGlobalEventBus()
											.fireEvent(
													new BookmarkSnapshotEvent(
															sr));
									known.put(sr.getDatasetRevId(), sr);
								}
							}
						});

	}

	/**
	 * Recently used snapshots
	 * 
	 * @param recents
	 * @param known
	 */
	// Disabled -- do we still want this?
	private void restoreRecents(Set<String> recents,
			final Map<String, SearchResult> known) {
		getClientFactory()
				.getSearchServices()
				.getSearchResultsForSnapshots(
						getSessionModel().getSessionID(),
						recents,
						new SecFailureAsyncCallback.SimpleSecFailureCallback<Set<SearchResult>>(
								getClientFactory()) {

							@Override
							public void onNonSecFailure(Throwable caught) {
								Dialogs.messageBox(
										"Error restoring recent snapshots for "
												+ getSessionModel().getUserId(),
										caught.getMessage());
							}

							@Override
							public void onSuccess(Set<SearchResult> result) {
								List<SearchResult> lst = new ArrayList<SearchResult>();
								lst.addAll(result);
								// getDisplay().setRecentSnapshotList(lst);

								for (SearchResult sr : result)
									known.put(sr.getDatasetRevId(), sr);
							}
						});

	}

	private void restoreFavoriteTools(Set<String> faveT) {
		if (getSessionModel().isRegisteredUser())
			getClientFactory()
					.getPortal()
					.getTools(
							getSessionModel().getSessionID(),
							faveT,
							new SecFailureAsyncCallback.SimpleSecFailureCallback<List<ToolDto>>(
									getClientFactory()) {

								@Override
								public void onNonSecFailure(Throwable caught) {
									Dialogs.messageBox(
											"Error restoring favorite tools for "
													+ getSessionModel().getUserId(),
											caught.getMessage());
								}

								@Override
								public void onSuccess(List<ToolDto> result) {
									// getDisplay().setFavoriteToolList(result);

									for (ToolDto t : result)
										IeegEventBusFactory
												.getGlobalEventBus()
												.fireEvent(
														new ToolMarkedAsFavoriteEvent(
																t, true));
								}
							});
	}

	// Disabled -- do we still want this?
	private void restoreRecentTools(Set<String> recentT) {
		if (getSessionModel().isRegisteredUser())
			getClientFactory()
					.getPortal()
					.getTools(
							getSessionModel().getSessionID(),
							recentT,
							new SecFailureAsyncCallback.SimpleSecFailureCallback<List<ToolDto>>(
									getClientFactory()) {

								@Override
								public void onNonSecFailure(Throwable caught) {
									Dialogs.messageBox(
											"Error finding recently used tools for "
													+ getSessionModel().getUserId(),
											caught.getMessage());
								}

								@Override
								public void onSuccess(List<ToolDto> result) {
									// getDisplay().setRecentToolList(result);
								}
							});
	}

	public void goTo(final WorkPlace place) {
		final Map<String, SearchResult> known = new HashMap<String, SearchResult>();

		UserPrefs prefs = new UserPrefs();
		prefs.setAnnSchemes(place.getAnnSchemes());
		getSessionModel().setUserPreferences(prefs);

		Set<String> faves = new HashSet<String>();
		faves.addAll(place.getFavoriteStudies());
		restoreFavorites(faves, known);

		// Set<String> recents = new HashSet<String>();
		// recents.addAll(place.getRecentStudies());
		// restoreRecents(faves, known);

		Set<String> faveT = new HashSet<String>();
		faveT.addAll(place.getFavoriteTools());
		restoreFavoriteTools(faveT);

		// Set<String> recentT = new HashSet<String>();
		// recentT.addAll(place.getRecentTools());
		// restoreRecentTools(recentT);

		HashSet<String> unknown = new HashSet<String>();
		for (WindowPlace win : place.getContexts()) {
			String nam = win.getSnapshotID();
			if (!known.containsKey(nam))
				unknown.add(nam);
		}

		// Broadcast the known set of snapshots
		if (!known.values().isEmpty())
			IeegEventBusFactory.getGlobalEventBus().fireEvent(
					new SearchResultsUpdatedEvent(null, known.values(), false));

		if (unknown.isEmpty())
			openSnapshots(known, place);
		else {
			getClientFactory()
					.getSearchServices()
					.getSearchResultsForSnapshots(
							getSessionModel().getSessionID(),
							unknown,
							new SecFailureAsyncCallback.SimpleSecFailureCallback<Set<SearchResult>>(
									getClientFactory()) {

								@Override
								public void onNonSecFailure(Throwable caught) {
									Dialogs.messageBox(
											"Error restoring workspace search results for "
													+ getSessionModel().getUserId(),
											caught.getMessage());
								}

								@Override
								public void onSuccess(Set<SearchResult> result) {
									for (SearchResult sr : result)
										known.put(sr.getDatasetRevId(), sr);

									IeegEventBusFactory
											.getGlobalEventBus()
											.fireEvent(
													new SearchResultsUpdatedEvent(null, 
															result, false));

									openSnapshots(known, place);
								}

							});
		}
	}

	private void openSnapshots(Map<String, SearchResult> snapshots,
			WorkPlace place) {
		int inx = 0;

		for (WindowPlace win : place.getContexts()) {
			String nam = win.getSnapshotID();
			SearchResult sr = snapshots.get(nam);

			if (inx >= place.getContexts().size())
				break;

			WindowPlace w = place.getContexts().get(inx++);
			OpenDisplayPanel act = new OpenDisplayPanel(
					sr, true, w.getType(), w);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(
					new OpenDisplayPanelEvent(act));
		}

	}

	@Override
	public WorkPlace getWorkPlace() {
		return controller.getWorkPlace();
	}

	@Override
	public void removeCloseHandlers() {
		getDisplay().removeWorkspaceHandlers();
	}

	@Override
	public void onUnbookmarkSnapshot(SearchResult snapshot) {
		if (getSessionModel().isRegisteredUser())
			controller.unbookmarkSnapshot(snapshot);
	}

	@Override
	public void onBookmarkSnapshot(SearchResult snapshot) {
		if (getSessionModel().isRegisteredUser())
			controller.bookmarkSnapshot(snapshot);
	}

	@Override
	public void onToolMarkedAsFavorite(ToolDto tool) {
		if (getSessionModel().isRegisteredUser())
			controller.bookmarkTool(tool);
	}

	@Override
	public void onToolUnmarkedAsFavorite(ToolDto tool) {
		if (getSessionModel().isRegisteredUser())
			controller.unbookmarkTool(tool);
	}

//	@Override
//	public void triggerSearch(AstroPanel sourceWindow) {
//		getDisplay().selectSearchTab();
//		SearchDialog ss = new SearchDialog(getClientFactory().getUserId(),
//				null, clientFactory);
//
//		ss.center();
//		ss.show();
//	}

	public List<SearchResult> getFavoriteSnapshots() {
		return controller.getFavoriteSnapshots();
	}

	@Override
	public void selectSnapshot(PresentableMetadata md) {
		// look at the metadata types, find the best operation
		// for the metadata
		List<String> types = ActionMapper.getMapper().getPresenters(md);

		if (!types.isEmpty())
			openDisplayPanel(md, true,
			// EEGViewerPanel.NAME
					types.get(0), new HashSet<PresentableMetadata>(),  null);
	}

	@Override
	public void onSearchResultsChanged(String search, List<PresentableMetadata> newResults,
			List<PresentableMetadata> deletedResults, boolean replaceResults) {
		getDisplay().selectSearchTab();
	}
}
