package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.user.client.ui.Composite;

public class GwtComposite extends Composite implements Attachable {
	  public void attach() {
	    onAttach();
	  }
}