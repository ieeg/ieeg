/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.db.mefview.shared.DatasetPreview;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Search;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public interface SearchServicesAsync {

	void getExperimentSearchCriteria(SessionToken sessionID,
			AsyncCallback<ExperimentSearchCriteria> callback);

	void getSearchResultsForSnapshots(SessionToken sessionID,
			Set<String> dataSnapshotRevId,
			AsyncCallback<Set<SearchResult>> callback);

	void getSnapshotsFor(SessionToken sessionID, ExperimentSearch expSearch,
			AsyncCallback<Set<SearchResult>> callback);

	void getSnapshotsFor(SessionToken sessionID, Search searchRestrictions,
			AsyncCallback<Set<SearchResult>> callback);

	void getUserLocations(AsyncCallback<List<LatLon>> callback);

	void getSearchMatches(SessionToken sessionID, String terms, int start,
			int max, AsyncCallback<List<? extends PresentableMetadata>> callback);

	void getSnapshotsForUser(SessionToken sessionID,
			AsyncCallback<Set<SearchResult>> callback);

	void updateSearchMatches(Set<SearchResult> updatedResults, SessionToken sessionID, String terms, int start,
			int max, AsyncCallback<List<? extends PresentableMetadata>> callback);

	void getMyData(String where, SessionToken sessionID, AsyncCallback<Set<? extends PresentableMetadata>> callback);

}
