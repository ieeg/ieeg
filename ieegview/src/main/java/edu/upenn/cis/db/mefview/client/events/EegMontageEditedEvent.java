/*
 * Copyright 2015 IEEG.org 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;


import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPresenter;
import edu.upenn.cis.db.mefview.shared.EEGMontage;

public class EegMontageEditedEvent extends GwtEvent<EegMontageEditedEvent.Handler> {
    private static final Type<EegMontageEditedEvent.Handler> TYPE = new Type<EegMontageEditedEvent.Handler>();
    
//    private final int selected;
    private final EEGMontage montage;
    IEEGViewerPresenter presenter;
    
    public EegMontageEditedEvent(IEEGViewerPresenter window, EEGMontage montage){
        this.montage = montage;
        this.presenter = window;
    }
    
    public static com.google.gwt.event.shared.GwtEvent.Type<EegMontageEditedEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<EegMontageEditedEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EegMontageEditedEvent.Handler handler) {
        handler.onMontageEdited(presenter, montage);
    }

    public static interface Handler extends EventHandler {
        void onMontageEdited(IEEGViewerPresenter w, EEGMontage montage);
    }
}