/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.UserInfo;

/**
 * The client side stub for the RPC service for project management.
 */
@RemoteServiceRelativePath("projects")
public interface ProjectServices extends RemoteService {
	
	/**
	 * Post a message on the discussion board associated with a project
	 * 
	 * @param sessionID
	 * @param projectId
	 * @param post
	 * @return
	 * @throws BtSecurityException
	 */
	public String addDiscussionToProject(SessionToken sessionID, String projectId, Post post)
	throws BtSecurityException;

	/**
	 * Create / save a new Project
	 * 
	 * @param sessionID
	 * @param project
	 * @return
	 * @throws BtSecurityException
	 */
	public Project createProject(SessionToken sessionID, Project project)
			throws BtSecurityException;

	
	/**
	 * Returns a list of all project groups less those in {@code excludedGroups}. 
	 * Ordered by project name, with admin group before team group and excluding groups in
	 * {@code excludedGroups}.
	 * @param excludedGroups
	 * @return
	 */
	public List<ProjectGroup> getProjectGroups(SessionToken sessionID, Set<ProjectGroup> excludedGroups) throws BtSecurityException ;
	

	/**
	 * Get the Project DTO
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	public Project getProject(SessionToken sessionID, String projectId) throws BtSecurityException;

	/**
	 * Get the set of users who are project admins
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	public Set<UserInfo> getProjectAdmins(SessionToken sessionID, String projectId) throws BtSecurityException;

	/**
	 * Get a list containing up to numPosts postings, starting at index postIndex (0 = last)
	 * 
	 * @param sessionID
	 * @param projectId
	 * @param postIndex
	 * @param numPosts
	 * @return
	 * @throws BtSecurityException
	 */
	public List<Post> getProjectDiscussions(SessionToken sessionID, String projectId,
			int postIndex, int numPosts) throws BtSecurityException;

	/**
	 * Get the user's set of projects
	 * 
	 * @param sessionID
	 * @return
	 * @throws BtSecurityException
	 */
	public Set<Project> getLazyProjectsForUser(SessionToken sessionID) throws BtSecurityException;

	/**
	 * String tags associated with a project
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	public Set<String> getProjectTags(SessionToken sessionID, String projectId) throws BtSecurityException;

	/**
	 * Tools associated with a project
	 * 
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	public Set<ToolDto> getProjectTools(SessionToken sessionID, String projectId) throws BtSecurityException;
	
	/**
	 * Users with authorization to project
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	public Set<UserInfo> getProjectUsers(SessionToken sessionID, String projectId) throws BtSecurityException;


	public String updateProjectAdministrators(SessionToken sessionID, String projectId, Set<String> userids)
			throws BtSecurityException;
	
	public String updateProjectSnapshots(SessionToken sessionID, String projectId, Set<String> snapshotIds)
			throws BtSecurityException;
	
	public String updateProjectTags(SessionToken sessionID, String projectId, Set<String> tags)
			throws BtSecurityException;
	
	public String updateProjectTeam(SessionToken sessionID, String projectId, Set<String> userid)
			throws BtSecurityException;
	
	public String updateProjectTools(SessionToken sessionID, String projectId, Set<String> toolIds)
			throws BtSecurityException;

	/**
	 * DOCUMENT ME
	 * 
	 * @param sessionID
	 * @param projectId
	 * @return
	 * @throws BtSecurityException
	 */
	Set<SearchResult> getProjectSearchResultsForUser(
			SessionToken sessionID, String projectId)
			throws BtSecurityException;
	
}
