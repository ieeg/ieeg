/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import edu.upenn.cis.braintrust.security.AuthenticationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.ProvenanceLogEntry;
import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.LogEntry;
import edu.upenn.cis.db.mefview.shared.PortalSummary;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.ProvenanceSummary;
import edu.upenn.cis.db.mefview.shared.RefreshList;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.ServerConfiguration;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.StripeCustomer;
import edu.upenn.cis.db.mefview.shared.TimeSeriesBookmark;
import edu.upenn.cis.db.mefview.shared.UserAccountInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.UserPrefs;
import edu.upenn.cis.db.mefview.shared.places.WorkPlace;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("eeg")
public interface EEGDisplay extends RemoteService {
	public enum DIRECTION {LEFT, RIGHT, ANNLEFT, ANNRIGHT}
	
	public static final Map<String, String> DEFAULT_ANN_TYPE_TO_ABBREV = new ImmutableMap.Builder<String, String>()
			.put("Seizure", "Z")
			.put("Microseizure", "M")
			.put("Artifact", "A")
			.put("Fast ripple", "F")
			.put("HFO", "H")
			.put("Spike", "S")
			.put("Label", "")
			.build();
	
	public void addLogEntry(SessionToken sessionID, LogEntry entry)
			throws BtSecurityException;

	public Long addPosting(SessionToken sessionID, String dsRevId, 
			Post post) throws BtSecurityException;

	public void addProvenanceRecord(SessionToken sessionID, ProvenanceLogEntry op)
			throws BtSecurityException;

	public String addShortURL(SessionToken sessionID, TimeSeriesBookmark book)
			throws BtSecurityException;

	public String addUserBookmark(SessionToken sessionID, TimeSeriesBookmark book)
			throws BtSecurityException;

	public String addUserRestoreMarks(SessionToken sessionID, TimeSeriesBookmark book)
			throws BtSecurityException;

	public SearchResult deriveSnapshot(SessionToken sessionID, final String studyRevId, final String friendlyName,
			final String toolName) throws BtSecurityException;

	public SearchResult deriveSnapshot(SessionToken sessionID, final String studyRevId, final String friendlyName,
			final String toolName, final List<String> channelIDs, boolean includeAnnotations) throws BtSecurityException;

	List<EditAclResponse> editAcl(SessionToken sessionID, String entityType,
			String mode, List<IEditAclAction<?>> actions);
	
	/**
	 * Returns the ACL for the acl target with type entityType and id
	 * {@code targetId}. Only perms with mode {@code mode} will be returned. The
	 * user aces are in alphabetical order by user name. The project aces are in
	 * alphabetical order by project name.
	 * 
	 * @param entityType
	 * @param mode
	 * @param targetId
	 * 
	 * @return
	 * @throws BtSecurityException
	 */
	public GetAcesResponse getAces(SessionToken sessionID, String entityType, String mode, String targetId) throws BtSecurityException;

	// public List<JobInfo> getActiveJobs(SessionToken sessionID)
	// throws BtSecurityException;

	List<UserInfo> getAllUsers(SessionToken sessionID)
			throws BtSecurityException;
	
	/**
	 * The response object will contain the total count of enabled users less those in {@code excludedUsers}. It will
	 * also contain a range of enabled user info of length {@code length} starting at
	 * index {@code start} when ordered by username and excluding users in
	 * {@code excludedUsers}.
	 * @param start
	 * @param length
	 * @param excludedUsers
	 * 
	 * @return
	 */
	public GetUsersResponse getEnabledUsers(SessionToken sessionID, int start, int length, Set<UserId> excludedUsers) throws BtSecurityException ;
	
//	public ExperimentSearchCriteria getExperimentSearchCriteria(SessionToken sessionID) throws BtSecurityException;
	
	List<LogEntry> getLogEntries(SessionToken sessionID,
			int startIndex, int count)
			throws BtSecurityException;
	
	public List<Place> getPlaces(SessionToken sessionID, String dataSnapshotRevId) throws BtSecurityException;

	public List<Post> getPostings(SessionToken sessionID, String dsRevId,
			int postIndex, int numPosts) throws BtSecurityException;

	double getNextValidDataRegion(SessionToken sessionID, String dataSetID, double position) throws
	BtSecurityException;

	double getPreviousValidDataRegion(SessionToken sessionID, String dataSetID, double position) throws
	BtSecurityException;


	public List<ToolDto> getRegisteredTools(SessionToken sessionID) throws BtSecurityException;
	
//	public Set<SearchResult> getSearchResultsForSnapshots(
//			SessionToken sessionID, 
//			final Set<String> dataSnapshotRevId) 
//					throws BtSecurityException;
	
	public List<SignalProcessingStep> getSignalProcessingTypes(
			SessionToken sessionID, String viewerType)
			throws BtSecurityException;
	
//	public Set<SearchResult> getSnapshotsFor(SessionToken sessionID, ExperimentSearch expSearch) throws BtSecurityException;

	/**
	 * Get the set of snapshots matching the search parameters
	 * @param searchRestrictions
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
//	Set<SearchResult> getSnapshotsFor(SessionToken sessionID, Search searchRestrictions) throws BtSecurityException;
	
	/**
	 * Debugging info
	 * 
	 * @return
	 * 
	 * @throws BtSecurityException
	 */
	public List<String> getSystemState(SessionToken sessionID) throws BtSecurityException; 

	public List<ToolDto> getTools(SessionToken sessionID, final Set<String> toolRevIds) throws BtSecurityException;
	
	public Set<TimeSeriesBookmark> getUserBookmarks(SessionToken sessionID)
			throws BtSecurityException;
	
	/**
	 * Get the session token after logging in / validating the password
	 * 
	 * @param userName
	 * @param password
	 * @return
	 * @throws AuthenticationException
	 */
	UserInfo getUserId(String userName, String password) throws AuthenticationException;

	UserInfo getUserInfo(String userName);

	UserInfo getUserInfo(SessionToken sessionID, String requestedUserId)
			throws BtSecurityException;
	
	public ProvenanceSummary getUserProvenanceSummary(SessionToken sessionID) 
			throws BtSecurityException;

	public PortalSummary getPortalSummary(SessionToken sessionID) 
			throws BtSecurityException;

	public Set<TimeSeriesBookmark> getUserRestoreMarks(SessionToken sessionID)
			throws BtSecurityException;

	public Boolean indexDataSnapshot(SessionToken sessionID, final String dataSnapshotRevId, double sampleRate,
			double pageLength) throws BtSecurityException;

	public WorkPlace loadWorkPlace(SessionToken sessionID) throws BtSecurityException;

	public void logoutSession(SessionToken sessionID) throws BtSecurityException;

	public RefreshList pollForUpdates(SessionToken sessionID, Timestamp sinceWhen)
			throws BtSecurityException;
	
	public void registerActivity(SessionToken sessionToken, DIRECTION direction, long time, 
			List<String>channels, List<DisplayConfiguration> filters, Set<Annotation> activeAnnotations) throws BtSecurityException;

	public List<String> registerTool(SessionToken sessionID, final List<ToolDto> tool) throws BtSecurityException;

	public Boolean removeTool(SessionToken sessionID, final Set<String> toolRevIDs) throws BtSecurityException;
	
	public Boolean saveWorkPlace(SessionToken sessionID, WorkPlace place, UserPrefs prefs) throws BtSecurityException;
	
	public void saveWorkPlaceAndLogout(SessionToken sessionID, WorkPlace place, UserPrefs prefs) throws BtSecurityException;
	
	//public Boolean terminateJob(SessionToken sessionID, String dataSnapshotRevId)
	//throws BtSecurityException;
	
	public List<AnnotationDefinition> getAnnotationTypes(SessionToken sessionID,
			String viewerType) throws BtSecurityException;
	
//	public Set<Action> getPermittedActionsForDataset(SessionToken sessionID, String dsId) throws BtSecurityException;

	public List<String> getAvailableFilterTypes();
	
	public void reIndexDatabase(SessionToken sessionID) throws BtSecurityException;

	public void changePassword(SessionToken sessionID, String oldMd5, String newMd5) throws BtSecurityException;

	void changePassword(String oldPlainText, String newPlainText) ;
	
	public ServerConfiguration getServerConfiguration(SessionToken sessionID);

	public UserInfo createUser(UserAccountInfo accountInfo, String encrytedPassword)
			throws BtSecurityException;

	StripeCustomer createSubscription(String token, String userID);

	StripeCustomer getSubscription(String userID);

	public UserAccountInfo getUserAccountInfo(SessionToken sessionID,
			String userID) throws BtSecurityException;
	
	public List<UserAccountInfo> getUserAccountInfoForAll(SessionToken sessionID) 
			throws BtSecurityException;

	public void setUserAccountInfoForAll(SessionToken sessionID, List<UserAccountInfo> info) 
			throws BtSecurityException;

    public List<EEGMontage> getMontages(SessionToken sessionID, String dataSnapshotID)
      throws BtSecurityException;
    
    public EEGMontage addMontage(SessionToken sessionID, EEGMontage montage, String dataSetID)
      throws BtSecurityException, EegMontageNotFoundException;

    public void removeMontage(SessionToken sessionID, EEGMontage montage)
			throws BtSecurityException, EegMontageNotFoundException;

	public EEGMontage editMontage(SessionToken sessionID, EEGMontage montage, String dataSetID) throws EegMontageNotFoundException;
	
	public UserInfo updateUser(SessionToken sessionID, UserAccountInfo newUserInfo, String hashedNewPassword)
			throws BtSecurityException;

	public Set<? extends PresentableMetadata> getKnownUsers(SessionToken sessionID);

	public Set<? extends PresentableMetadata> getRecommendedUsers(SessionToken sessionID);
	
	public Set<? extends PresentableMetadata> getGroups(SessionToken sessionID);

	public void addUserToGroup(SessionToken sessionID, UserInfo userInfo, String groupName);

	public void removeUserFromGroup(SessionToken sessionID, UserInfo userInfo, String groupName) throws BtSecurityException;

	public void addFriend(SessionToken sessionID, UserInfo friendInfo);

	public void removeFriend(SessionToken sessionID, UserInfo friendInfo) throws BtSecurityException;

	public void shareWith(SessionToken sessionID, PresentableMetadata object, int levelIndex, UserInfo sharee);

	void shareWith(SessionToken sessionID, PresentableMetadata shared, int shareLevel, String shareWith);

	public void unshareWith(SessionToken sessionID, PresentableMetadata object, UserInfo sharee) throws BtSecurityException;

	public void changeOwner(SessionToken sessionID, PresentableMetadata object, UserInfo newOwner);

	public BasicCollectionNode createGroup(SessionToken sessionID, String groupName) throws BtSecurityException;

	public void removeGroup(SessionToken sessionID, BasicCollectionNode group) throws BtSecurityException;

	public BasicCollectionNode createFolder(SessionToken sessionID, String label, String folderPathName) throws BtSecurityException;

	public void removeFolder(SessionToken sessionID, BasicCollectionNode folder) throws BtSecurityException;
	
	public void addToFolder(SessionToken sessionID, BasicCollectionNode folder, PresentableMetadata item)
			 throws BtSecurityException;

	public Set<? extends PresentableMetadata> getFolderContents(SessionToken sessionID, String path) throws BtSecurityException;

	public Set<? extends PresentableMetadata> getFolderContents(SessionToken sessionID,BasicCollectionNode folder) throws BtSecurityException;

	public void storeMetadata(SessionToken sessionID, String path, PresentableMetadata object) throws BtSecurityException;
	
	public void removeMetadata(SessionToken sessionID, String path, PresentableMetadata object) throws BtSecurityException;
	
	public Map<UserInfo, Set<Integer>> getSharingSet(SessionToken sessionID, PresentableMetadata folder) throws BtSecurityException;
	
	public int storeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo info, String container, String fileKey, byte[] object) throws BtSecurityException;

	public void removeMetadataObject(SessionToken sessionID, SearchResult parent, FileInfo info, String container, String fileKey) throws BtSecurityException;

	public void storeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object)
			throws BtSecurityException;

	public void removeMetadata(SessionToken sessionID, SearchResult parent, PresentableMetadata object)
			throws BtSecurityException;
	
	public void addDerivation(SessionToken sessionID, SearchResult parent, String provenanceStep, SearchResult child)
			throws BtSecurityException;

	IStoredObjectReference createInboxFolder(SessionToken sessionID, String project, String path)
			throws IOException;

	Set<? extends IStoredObjectReference> getInboxFolderContents(SessionToken sessionID, IStoredObjectReference folder);

	Set<? extends IStoredObjectReference> getInboxFolderContents(SessionToken sessionID, String thePath,
			SessionToken sessionID2);

	IStoredObjectReference moveInboxToFolder(SessionToken sessionID, String project, String path)
	throws IOException;

	void sendFeedback(String user, String message);

	boolean validatePassword(String plaintext);

}
