/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.mefview.client;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory that returns singletons for the global event bus
 * and a set of custom event buses
 * 
 * @author zives
 *
 */
public class IeegEventBusFactory {
	static IEventBus eventBus = null;
	
	static Map<Object,IEventBus> localEventBuses = new HashMap<Object,IEventBus>();
	
	/**
	 * Return the global event bus, constructing it if necessary
	 * 
	 * @return
	 */
	public static IEventBus getGlobalEventBus() {
		if (eventBus == null)
			eventBus = new IeegEventBus();
		
		return eventBus;
	}
	
	/**
	 * Return an event bus specifically for a given object
	 * 
	 * @param key
	 * @return
	 */
	public static IEventBus getLocalEventBus(Object key) {
		if (!localEventBuses.containsKey(key))
			localEventBuses.put(key, new IeegEventBus());
		
		return eventBus;
	}
	
	/**
	 * Deallocate the event bus
	 * @param key
	 */
	public static void removeLocalEventBus(Object key) {
		localEventBuses.remove(key);
	}
}
