/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.ContextSupplier;
import edu.upenn.cis.db.mefview.client.EEGDisplay;
import edu.upenn.cis.db.mefview.client.EEGDisplayAsync;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.SnapshotServicesAsync;
import edu.upenn.cis.db.mefview.client.events.ClosedPanelEvent;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.events.OpenedPanelEvent;
import edu.upenn.cis.db.mefview.client.events.RequestReloadSnapshotEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.ToolResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.EEGModel;
import edu.upenn.cis.db.mefview.client.models.SnapshotAnnotationModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.ShowAnnotations;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.Annotation;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.places.EEGPlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public class DataSnapshotViews implements ContextSupplier, 
ToolResultsUpdatedEvent.Handler, ToolResultCreatedEvent.Handler {
	
	public static interface OnDoneHandler {
		public void doWork(final String studyId);
	}
	
	AppModel cs;
	
	SnapshotServicesAsync eegService;
	EEGDisplayAsync portal;
	List<AstroPanel> panes = new ArrayList<AstroPanel>();
	
	DataSnapshotModel state;
	
	boolean isWritable = true;
	ClientFactory clientFactory;
	public static SortedSet<String> CASE_INSENSITIVE_DEFAULT_ANN_TYPES = ImmutableSortedSet
				.orderedBy(String.CASE_INSENSITIVE_ORDER)
				.addAll(EEGDisplay.DEFAULT_ANN_TYPE_TO_ABBREV.keySet()).build();
	
	public DataSnapshotViews(final String studyID,
			final String friendlyName,
			final String userID,
			final String creatorID,
			final SnapshotServicesAsync service,
			final EEGDisplayAsync portal,
			final EEGModel data,
			final AppModel cstate,
			final SearchResult searchResult,
			final ClientFactory clientFactory
			) {
		
		cs = cstate;
		this.portal = portal;
		state = new DataSnapshotModel(studyID, friendlyName, userID, creatorID, //traces,//listOfSeries,
				data);
		
		state.setAnnotations(new SnapshotAnnotationModel(getState()));
		state.setSearchResult(searchResult);
//		this.clientFactory = clientFactory;
		eegService = service;
	
		// NOTE: this was disabled 10/11/2015 because we aren't using this functionality
//		IeegEventBusFactory.getGlobalEventBus().registerHandler(ToolResultCreatedEvent.getType(), this);
//		IeegEventBusFactory.getGlobalEventBus().registerHandler(ToolResultsUpdatedEvent.getType(), this);
	}
	
	public boolean isWritable() {
		return this.isWritable;
	}
	
	public void setWritable(boolean writ) {
		this.isWritable = writ;
	}

	public void removePane(AstroPanel pane) {
			panes.remove(pane);

			IeegEventBusFactory.getGlobalEventBus().fireEvent(new ClosedPanelEvent(pane.getDescriptor(), pane));
			if (panes.isEmpty())
				removeThisStudy();
//		}
	}
	
	public void addPane(AstroPanel pane) {
//			if (pane.getPanelType().equals(EEGViewerPanel.NAME))
//				ServerAccess.registerViewer(state.getSnapshotID(), pane);
			
			panes.add(pane);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenedPanelEvent(pane.getDescriptor(), pane));
//		}
	}
	
	public DataSnapshotModel removeThisStudy() {
		while (!panes.isEmpty()) {
			removePane(panes.get(panes.size() - 1));
		}
		
		state.clearRelated(state.getSnapshotID());
		this.cs.removeSnapshotModel(state.getSnapshotID());
//		parent.removeSnapshotModel(state, state.getSnapshotID());
		return state;
	}
	
	public void listAnnotations(int height) {
		boolean needToSave = false;
		
		for (Annotation ann: state.getAnnotationModel().getAllEnabledAnnotations()) {
			if (ann.getRevId() == null)
				needToSave = true;
		}
		
//		for (AstroPanel pan: panes)
//			if (pan.getPanelType().equals(EEGViewerPanel.NAME))
//				((IEEGViewerPanel)pan).getEEGPane().hideGraph();
		
		ShowAnnotations s = new ShowAnnotations(//studyID, getTraceInfo(), 
				getState(), state.getAnnotationDefinitions(), height, needToSave,
				clientFactory);
		s.center();
		s.show();
	}
	
	
	public void refreshAnnotationDisplay() {
		List<AnnBlock> times = state.getAnnotationModel().getAnnotationTimes();

		for (AstroPanel pan: panes)
			if (pan.getPanelType().equals(EEGViewerPanel.NAME))
				((IEEGViewerPanel)pan).refreshAnnotations(times);
	}
	
	public void setChannels(final List<INamedTimeSegment> traces) {
		state.getData().setTraces(traces);
		for (IEEGViewerPanel mvw : getEEGs())
			mvw.setTraceInfo(traces);
//		for (ImageViewerPanel mvw : getImages())
//			mvw.setTraceInfo(traces);
	}
	
	public void setChannelsAndPlace(final List<INamedTimeSegment> traces, final WindowPlace place) {
		state.getData().setTraces(traces);
		for (IEEGViewerPanel mvw : getEEGs())
//			mvw.setTraces(traces);
			mvw.setTracesAndPlace(traces, (EEGPlace)place);
//		for (ImageViewerPanel mvw : getImages())
//			mvw.setTraceInfo(traces);
	}
	
	public void reload() {
		removeThisStudy();

//		reloadStudy(state.getSnapshotID(), state.getSnapshotID());
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestReloadSnapshotEvent(state.getSnapshotID(),
				state.getSnapshotID()));
	}
	
	public void reload(final String oldStudyID, final String studyID) {
//		Collection<String> existing = getPanelTypes();  
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestReloadSnapshotEvent(state.getSnapshotID(),
				state.getSnapshotID()));
		removeThisStudy();

//		reloadStudy(oldStudyID, studyID);//, existing);
	}
	
	public void clearDeletedAnnotationList() {
		state.getAnnotationModel().clearDeletedAnnotationList();
	}
	
	/**
	 * Returns the highest sample rate in Hz
	 * 
	 * @return
	 */
	public double getSampleRateHz() {
		double sampleRate = Double.MIN_VALUE;
		for (INamedTimeSegment t: state.getTraceInfo()) {
			if (t.getSampleRate() > sampleRate)
				sampleRate = t.getSampleRate();
		}
		return sampleRate;
	}

//	public void openURL(final String title, final String url, final String snapshot) {
//		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, null);
//		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
//	}
//
//	public void openURL(final String title, final String url, final String snapshot, final String other) {
//		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, other, null);
//		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
//	}
	
	@Override
	public List<String> getChannelRevIDs() {
		List<String> ret = new ArrayList<String>();
		
		for (INamedTimeSegment ti: state.getData().getTraceList())
			ret.add(ti.getId());
		
		return ret;
	}

	@Override
	public long getStartOffset() {
		IEEGViewerPanel viewer = getFirstEEG();
		//if (this.eegs.isEmpty())
		if (viewer == null)
			return 0;
		else
			return (long)viewer.getEEGPane().getPosition();
	}

	@Override
	public long getWidth() {
		IEEGViewerPanel viewer = getFirstEEG();
		if (viewer == null)//this.eegs.isEmpty())
			return 10000000;
		else
			return (long)viewer.getEEGPane().getPageWidth();
	}

	@Override
	public double getFrequency() {
		if (state.getData().getTraceList().isEmpty())
			return 0;
		else
			return state.getData().getTraceList().get(0).getSampleRate();
	}

	public SearchResult getSearchResult() {
		return state.getSearchResult();
	}
	
	public void createIndexFor(double frequency, double width) {
		portal.indexDataSnapshot(clientFactory.getSessionModel().getSessionID(), state.getSnapshotID(), frequency, width, new SecFailureAsyncCallback.SimpleSecFailureCallback<Boolean>(clientFactory) {

			@Override
			public void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Index created", "Successfully created an index on " + state.getSnapshotID());
			}

			@Override
			public void onSuccess(Boolean result) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	@Override
	public long getSnapshotStart() {
		long start = Long.MAX_VALUE;
		for (INamedTimeSegment ti: state.getTraceInfo()) {
			if (ti.getStartTime() < start)
				start = ti.getStartTime();
		}
		return start;
	}

	@Override
	public long getSnapshotLength() {
		long start = getSnapshotStart();
		long end = start;
		for (INamedTimeSegment ti: state.getTraceInfo()) {
			if ((double)(ti.getStartTime() + ti.getDuration()) > end)
				end = (long)(ti.getDuration() + ti.getStartTime());
		}
		return end - start;
	}

	@Override
	public void onToolResultsChanged(String snapshotId, List<DerivedSnapshot> newResults,
			List<DerivedSnapshot> deletedResults) {
//		System.err.println("Update to tool results for " + snapshotId);
		if (snapshotId.equals(state.getSnapshotID())) {
			if (newResults != null)
				state.addDerivedResults(newResults);
			
			if (deletedResults != null)
				state.removeDerivedResults(deletedResults);
		}
	}
	
	@Override
	public void onToolResultCreated(String theStudy, String friendly,
			String creator, DerivedSnapshot result) {
		if (theStudy.equals(state.getSnapshotID())) {
			state.addDerivedResult(result);
			IeegEventBusFactory.getGlobalEventBus().fireEvent(new ToolResultRefreshEvent(result.getAnalysisId()));
		}
	}
	
	public List<IEEGViewerPanel> getEEGs() {
		List<IEEGViewerPanel> ret = Lists.newArrayList();
		for (AstroPanel pan: panes)
			if (pan.getPanelType().equals(EEGViewerPanel.NAME))
				ret.add((IEEGViewerPanel)pan);
		
		return ret;
	}

	public IEEGViewerPanel getFirstEEG() {
		for (AstroPanel pan: panes)
			if (pan.getPanelType().equals(EEGViewerPanel.NAME))
				return ((IEEGViewerPanel)pan);
		
		return null;
	}

//	public List<ImageViewerPanel> getImages() {
//		List<ImageViewerPanel> ret = Lists.newArrayList();
////		return eegs;
//		for (AstroPanel pan: panes)
//			if (pan.getPanelType().equals(ImageViewerPanel.NAME))
//				ret.add((ImageViewerPanel)pan);
//		
//		return ret;
//	}
//
//	public ImageViewerPanel getFirstImage() {
//		for (AstroPanel pan: panes)
//			if (pan.getPanelType().equals(ImageViewerPanel.NAME))
//				return ((ImageViewerPanel)pan);
//		
//		return null;
//	}

	public List<AstroPanel> getPanels(String type) {
		List<AstroPanel> ret = Lists.newArrayList();
		for (AstroPanel pan: panes)
			if (pan.getPanelType().equals(type))
				ret.add(pan);
		
		return ret;
	}

	public AstroPanel getFirstPanel(String type) {
		for (AstroPanel pan: panes)
			if (pan.getPanelType().equals(type))
				return (pan);
		
		return null;
	}

	@Override
	public List<String> getChannelNames() {
		return state.getChannelNames();
	}
	
	public DataSnapshotModel getState() {
		return state;
	}
	
	public AppModel getAppState() {
		return cs;
	}

	public EEGModel getEEGModel() {
		return state.getData();	
	}
}
