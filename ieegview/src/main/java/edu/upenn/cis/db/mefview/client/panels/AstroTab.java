/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.LayoutPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.widgets.Attachable;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.places.PanelDescriptor;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;

public abstract class AstroTab extends ResizingLayout// LayoutPanel 
	implements AstroPanel, BasicPresenter.Display, Attachable {
	
	private DataSnapshotModel model = null;
	private DataSnapshotViews controller = null;
	protected ClientFactory clientFactory = null;
	private Presenter presenter;
	
	PanelDescriptor desc;
	
	public int graphHeight = 1000;
	public int graphWidth = 800;
	public int windowWidth;
	public int windowHeight;
	public int minHeight = 400;
	
	int pageWidth;

//	protected String user;
	protected String datasetId;
	protected String friendlyName;
	protected String title;
	
	public AstroTab()
	{
		
	}
	
	public AstroTab( 
			final ClientFactory factory,
			final DataSnapshotViews model, 
//			final String user,
			final String title) {
		this(factory, //user, 
				title);
		setDataSnapshotController(model);
		if (model != null)
			setDataSnapshotState(model.getState());
		else
			setDataSnapshotState(null);
	}
	
	public AstroTab(final ClientFactory factory, //final String user, 
			final String title) {

		this.clientFactory = factory;
//		this.user = user;
		this.title = title;

		this.addDomHandler(new ContextMenuHandler() {

			@Override public void onContextMenu(ContextMenuEvent event) {
				event.preventDefault();
				event.stopPropagation();
			}
		}, ContextMenuEvent.getType());
		
	  }
	
	PanelDescriptor createDescriptor(final DataSnapshotViews model, final String user) {
		return new PanelDescriptor(getPanelType(), 
				getDataSnapshotState().getSnapshotID());
	}
	
	public static int getWidth() {
		return Window.getClientWidth();
	}
	
	public static int getHeight() {
		return Window.getClientHeight();
	}
	
//	public String getUser() {
//		return user;
//	}

	public void updateRevId(final String oldId, final String newId) {
		if (datasetId.equals(oldId))
			datasetId = newId;
	}
	
	public void setPlace(WindowPlace place) {
		
	}

	public PanelDescriptor getDescriptor() {
		return desc;
	}

	public void refresh() {
		
	}
	
	public DataSnapshotViews getDataSnapshotController() {
		return controller;
	}

	public void setDataSnapshotController(final DataSnapshotViews model) {
		this.controller = model;
	}
	
	public void setDataSnapshotState(final DataSnapshotModel model) {
		this.model = model;
	}
	
	@Override
	public DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	public String getDatasetID() {
		return model.getSnapshotID();
	}
	
	public String getFriendlyName() {
		return model.getFriendlyName();
	}

	@Override
	public void close() {
		
	}

	public static String getBasename(String path) {
		final int last = path.lastIndexOf("/");
		if (last == -1) {
			return path;
		} else {
			return path.substring(last + 1);
		}
	}
	
	public void setDataSnapshotModel(DataSnapshotModel model) {
		this.model = model;
	}

	@Override
	public void addPresenter(Presenter pres) {
		this.presenter = pres;
	}
	
	public Presenter getPresenter() {
		return presenter;
	}

//	@Override
	public void log(int level, String string) {
		if (level < 3)
			GWT.log(string);
	}

	@Override
	public void attach() {
		onAttach();
	}
	
	public SessionModel getSessionModel() {
		return this.clientFactory.getSessionModel();
	}
}
