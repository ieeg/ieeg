/*
s * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.combo.ComboBoxChangeEvent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.AnnotationDefinition;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.dialogs.IEditAnnotation;
import edu.upenn.cis.db.mefview.client.events.AnnotationEditedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationRemovedEvent;
import edu.upenn.cis.db.mefview.client.events.RequestRefreshAnnotationsEvent;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.controls.SliderBar.AnnBlock;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.StringComboBox;
import edu.upenn.cis.db.mefview.shared.Annotation;

/**
 * Dialog box for editing the properties of an annotation
 * 
 * @author zives, jbwagenaar
 *
 */
public class EditAnnotation extends DialogBox implements IEditAnnotation {
  VerticalPanel dialog;
  Button close;
  Button cancel;
  Button delete;
  TextBox color = new TextBox();
  TextBox start;
  TextBox stop;
  StringComboBox<ComboBoxDataModel> groupList; 
  StringComboBox<ComboBoxDataModel> nameList; 
  TextBox description = new TextBox();
  TextBox creator;
  Grid grid;

  DataSnapshotModel snapshot;
//  DataSnapshotViews controller;
  final static int HEIGHT = 11;
  Annotation annotation;
  List<String> channelNames;
  ListBox channelBox = new ListBox(true);
  AnnotationGroup<Annotation> assignedGroup;
  List<AnnotationDefinition> annotationDefs;

//  ClientFactory clientFactory;

  static int lastPosition = 0;

  public static String defaultType = "Seizure";

  public EditAnnotation(){}
  
  
  public void init(final Annotation a, 
      final List<AnnotationDefinition> definitions,
      final List<String> channels, final Map<String,String> schema,
      final DataSnapshotModel snapshot,
//      final ClientFactory factory,
      boolean isNew, Command doAfter) {
    
//    clientFactory = factory;
    annotationDefs = definitions;
    this.snapshot = snapshot;//controller.getState();

    assignedGroup = snapshot.getAnnotationModel().getGroupFor(a);
    annotation = a;
    channelNames = channels;

    setStyleName("ChanSelectDialog");
    setHTML("Edit Annotation");

    grid = new Grid(HEIGHT, 2);
    int row = 0;

    row++;
    grid.setWidget(row, 0, new Label("Onset usec"));
    start = new TextBox();
    String val = NumberFormat.getFormat("#######0").format(a.getStart());
    start.setValue(val);
    grid.setWidget(row, 1, start);

    row++;
    grid.setWidget(row, 0, new Label("Offset usec"));
    stop = new TextBox();
    val = NumberFormat.getFormat("#######0").format(a.getEnd());
    stop.setValue(val);
    grid.setWidget(row, 1, stop);

    row++;
    grid.setWidget(row, 0, new Label("Type"));
    List<String> names = new ArrayList<String>();
    for (AnnotationDefinition ann: definitions)
      if (ann.getCategory().equals("clinical"))
        names.add(ann.getAnnotationName());
    Collections.sort(names);

    ComboBoxDataModel dm = new ComboBoxDataModel(); 
    for (String n : names)
      dm.add(n, n);
    nameList = new StringComboBox<ComboBoxDataModel>(true);
    nameList.setModel(dm);
    grid.setWidget(row, 1, nameList);

    nameList.setValue(assignedGroup.getName());

    nameList.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(ChangeEvent event) {
        if (event instanceof ComboBoxChangeEvent) {
          int row = ((ComboBoxChangeEvent)event).getRow();
          String name = (String)nameList.getModel().get(row);
          nameList.setValue(name);

          AnnotationDefinition def = getDefinition(name); 
          if (def != null) {
            stop.setEnabled(def.getNumberOfPoints() > 1);
            channelBox.setEnabled(def.getGlobal() != true);
          } else {
            stop.setEnabled(true);
            channelBox.setEnabled(true);
          }
        }
      }

    });

    nameList.setValue(a.getType());

    // Blank
    row++;

    row++;
    grid.setWidget(row, 0, new Label("Description"));
    description.setValue(a.getDescription());
    grid.setWidget(row, 1, description);

    row++;
    grid.setWidget(row, 0, new Label("Color"));
    color.setValue(a.getColor());
    grid.setWidget(row, 1, color);

    row++;
    grid.setWidget(row, 0, new Label("Creator"));
    creator = new TextBox();
    creator.setValue(a.getCreator());
    creator.setReadOnly(true);
    grid.setWidget(row, 1, creator);

    row++;
    row++;
    grid.setWidget(row, 0, new Label("Layer"));

    //Collections.sort(names);
    List<String> groups = getAnnotationGroups(snapshot.getAnnotationModel().getRootGroup());
    Collections.sort(groups);
    ComboBoxDataModel gm = new ComboBoxDataModel(); 
    for (String n : groups){
      gm.add(n, n);
    }

    groupList = new StringComboBox<ComboBoxDataModel>(false);
    groupList.setModel(gm);
    grid.setWidget(row, 1, groupList);

    boolean foundGroup = false;
    for (int i = 0; i < groups.size(); i++)
      if (groups.get(i).equals(assignedGroup.getName())) {
        groupList.setSelectedIndex(i);
        foundGroup = true;
      }

    if (!foundGroup && assignedGroup != null)
      GWT.log("Could not find group " + assignedGroup.getName());
    else
      GWT.log("Could not find group because it's null");

    groupList.addChangeHandler(new ChangeHandler() {
      @Override
      public void onChange(ChangeEvent event) {
        if (event instanceof ComboBoxChangeEvent) {
          int row = ((ComboBoxChangeEvent)event).getRow();
          String name = (String)groupList.getModel().get(row);
          groupList.setValue(name);

        }
      }

    });


    final HorizontalPanel selectorBar = new HorizontalPanel();
    final EditAnnotation self = this;
    dialog = new VerticalPanel();

    HorizontalPanel hp = new HorizontalPanel();
    dialog.add(hp);

    channelBox.setVisibleItemCount(12);

    for (String c: snapshot.getChannelNames())
      channelBox.addItem(c);

    for (int i = 0; i < channelBox.getItemCount(); i++)
      if (channels.contains(channelBox.getItemText(i)))
        channelBox.setItemSelected(i, true);

    VerticalPanel channelPanel = new VerticalPanel();
    channelPanel.add(new Label("Select channels:"));
    channelPanel.add(channelBox);
    hp.add(channelPanel);
    hp.add(grid);
    dialog.add(selectorBar);
    setWidget(dialog);

    close = new Button("OK");
    selectorBar.add(close);
    close.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        if (isNullOrEmpty(nameList.getValue())) { 
          Dialogs.messageBox("Error saving annotations", "You must enter a value for the annotation type."); 
        } else if (channelBox.getSelectedIndex() == -1){
          Dialogs.messageBox("Error saving annotations", "You must select at least one channel."); 
        } else {
          defaultType = nameList.getValue();

          annotation.setModified(true);
          annotation.setStart(Double.valueOf(start.getValue()).doubleValue());
          annotation.setEnd(Double.valueOf(stop.getValue()).doubleValue());
          annotation.setType(nameList.getValue());
          annotation.setDescription(description.getValue());
          annotation.setCreator(creator.getValue());
          annotation.setColor(color.getValue());					
          annotation.setLayer(groupList.getValue());

          // Change AnnotationGroup if required				
          AnnotationGroup<Annotation> newGroup = snapshot.getAnnotationModel().getRootGroup().getGroup( 
              groupList.getValue());
          if (newGroup != assignedGroup){
            assignedGroup.removeAnnotation(annotation);
            newGroup.addAnnotation(annotation);
            assignedGroup = newGroup;
          }

          // Re-populate channels for annotation
          annotation.getChannels().clear();
          annotation.getChannelInfo().clear();
          for (int i = 0; i < channelBox.getItemCount(); i++) {
            if (channelBox.isItemSelected(i)) {
              annotation.getChannels().add(snapshot.getChannelNames().get(i));
              annotation.getChannelInfo().add(snapshot.getTraceInfo().get(i));
            }
          }	

          IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationEditedEvent(
        		  snapshot, assignedGroup, null, annotation));

          self.hide();
          refreshAnnotationDisplay();
        }
      }
    });

    if (!isNew) {
      cancel = new Button("Cancel");
      selectorBar.add(cancel);
      cancel.addClickHandler(new ClickHandler() {

        public void onClick(ClickEvent event) {
          self.hide();
        }
      });
      delete = new Button("Remove");
    } else
      delete = new Button("Cancel");

    selectorBar.add(delete);
    delete.addClickHandler(new ClickHandler() {

      public void onClick(ClickEvent event) {
        snapshot.getAnnotationModel().removeAnnotation(annotation);
        self.hide();
        refreshAnnotationDisplay();
        IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRemovedEvent(self.snapshot, annotation));
      }

    });

    AnnotationDefinition def = getDefinition(a.getType()); 
    if (def != null) {
      stop.setEnabled(def.getNumberOfPoints() > 1);
      channelBox.setEnabled(def.getGlobal() != true);
    } else {
      stop.setEnabled(true);
      channelBox.setEnabled(true);
    }

  }

  private AnnotationDefinition getDefinition(final String annName) {
    for (AnnotationDefinition ann: annotationDefs)
      if (ann.getAnnotationName().equals(annName))
        return ann;

    return null;
  }


  public List<String> getAnnotationGroups(AnnotationGroup<Annotation> group) {
    List<String> annList = new ArrayList<String>();

    if (group != null) {
      for (AnnotationGroup<Annotation> sub: group.getSubGroups()) {
        annList.add(sub.getName());
        annList.addAll(getAnnotationGroups(sub));
      }
    }
    return annList;
  }


@Override
public void setModal(Boolean value) {
	// TODO Auto-generated method stub
	
}


@Override
public void setGlassEnabled(Boolean value) {
	// TODO Auto-generated method stub
	
}
  
  void refreshAnnotationDisplay() {
	  List<AnnBlock> times = snapshot.getAnnotationModel().getAnnotationTimes();
	  IeegEventBusFactory.getGlobalEventBus().fireEvent(
			  new RequestRefreshAnnotationsEvent(snapshot.getSnapshotID(), 
			  times));
  }
}
