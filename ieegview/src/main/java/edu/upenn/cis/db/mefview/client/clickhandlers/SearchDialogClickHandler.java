/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.clickhandlers;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.dialogs.SearchDialog;

public class SearchDialogClickHandler implements ClickHandler {
	ClientFactory clientFactory;
	SearchDialog ss;
	
	static SearchDialogClickHandler theHandler = null;
	
	public static synchronized SearchDialogClickHandler getHandler(ClientFactory cf) {
		if (theHandler == null)
			theHandler = new SearchDialogClickHandler(cf);

		return theHandler;
	}

	public SearchDialogClickHandler(ClientFactory cf) {
		clientFactory = cf;
		if (clientFactory == null)
			throw new RuntimeException("Unable to work with empty client factory");
	}
	
	public void onClick(ClickEvent arg0) {
		if (!clientFactory.getSessionModel().isRegisteredUser()) {
			Window.alert("You can't search as a guest");
			return;
		} else if (ss == null) {
			ss = new SearchDialog(
					clientFactory.getSessionModel().getUserId(),
					null,
					clientFactory,
					true);
		}
		ss.center();
		ss.show(); 

	}
}
