/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.userprofile;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.plugins.userprofile.UserProfilePresenter.StampedEvent;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class RecentActivitiesPresenter implements Presenter {
	public interface Display extends Presenter.Display {
		public void initialize(ClientFactory factory);
		public void refresh(List<UserProfilePresenter.StampedEvent> events);
	}
	
	Display display = null;
	ClientFactory clientFactory;
	List<StampedEvent> events = Lists.newArrayList();
	Timer refresh;
	boolean cont = true;
	
	public RecentActivitiesPresenter(ClientFactory cf) {
		clientFactory = cf;
	}
	
	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model, PresentableMetadata project,
			boolean writePermissions) {
		return new RecentActivitiesPresenter(clientFactory);
	}

	@Override
	public void setDisplay(Presenter.Display display) {
		this.display = (RecentActivitiesPresenter.Display)display;
	}
	
	public void addEvent(String message, String userId, String followCode, Timestamp time,
			boolean isLocal) {
		String user = userId;
		if (user.equals(clientFactory.getSessionModel().getUserId()))
			user = "You";
		events.add(0, new StampedEvent(time.getTime(), "<b>" + user + 
				"</b>" + message, isLocal));
	}

	@Override
	public String getType() {
		return "ACT";
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.initialize(clientFactory);
		display.addPresenter(this);
		
		refresh = new Timer() {

			@Override
			public void run() {
				display.refresh(events);
				
				if (cont)
					refresh.schedule(2000);
			}
			
		};
		
		refresh.schedule(2000);

	}

	@Override
	public void unbind() {
		cont = false;
	}

	@Override
	public Set<String> getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return null;
	}

}
