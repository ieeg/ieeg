/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;

public class ClosedSnapshotEvent extends GwtEvent<ClosedSnapshotEvent.Handler> {
	private static final Type<ClosedSnapshotEvent.Handler> TYPE = new Type<ClosedSnapshotEvent.Handler>();
	
	private String snapshot;
	private DataSnapshotModel model;
	
	public ClosedSnapshotEvent(String snapId, DataSnapshotModel model){
		snapshot = snapId;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<ClosedSnapshotEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ClosedSnapshotEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ClosedSnapshotEvent.Handler handler) {
		handler.onClosedSnapshot(snapshot, model);
	}

	public interface Handler extends EventHandler {
		void onClosedSnapshot(String snapshot, DataSnapshotModel model);
	}
}
