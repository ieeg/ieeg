package edu.upenn.cis.db.mefview.client.commands.snapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.SetAnnotationTarget;
import edu.upenn.cis.db.mefview.client.models.AnnotationSet;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.models.MetadataModel;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class LoadAnnotationsAndCopyCommand implements Command {
	final ClientFactory clientFactory;
	final SearchResult sr;
	final MetadataModel metadata;
	final DataSnapshotModel snapshotModel;
	final String prefix;
	final Map<String, DataSnapshotViews> snapshotViews;
	
	public LoadAnnotationsAndCopyCommand(
			final ClientFactory clientFactory,
			final SearchResult searchResult,
			final String prefixToAdd,
			final MetadataModel metadata,
			final DataSnapshotModel snapshotModel,
			final Map<String, DataSnapshotViews> snapshotViews) {
		this.clientFactory = clientFactory;

		this.sr = searchResult;
		this.prefix = prefixToAdd;
		this.metadata = metadata;
		this.snapshotModel = snapshotModel;
		this.snapshotViews = snapshotViews;
	}

	@Override
	public void execute() {	
	clientFactory.getSnapshotServices().getDataSnapshotAnnotations(
			clientFactory.getSessionModel().getSessionID(),
			sr.getDatasetRevId(),
			new SecFailureAsyncCallback.SecAlertCallback<List<Annotation>>(
					clientFactory) {

				public void onSuccess(List<Annotation> tracesIn) {
					List<AnnotationSet<Annotation>> anns = new ArrayList<AnnotationSet<Annotation>>();
					AnnotationSet<Annotation> nA = new AnnotationSet<Annotation>();
					nA.label = sr.getFriendlyName();
					nA.onStudyRevId = sr.getDatasetRevId();
					nA.annotations = tracesIn;
					anns.add(nA);

					List<SearchResult> studies = new ArrayList<SearchResult>();
					for (GeneralMetadata md : metadata.getKnownSnapshots())
						if (md instanceof SearchResult)
							studies.add((SearchResult) md);

					SetAnnotationTarget sta = new SetAnnotationTarget(
							studies,
							snapshotModel.getSeriesList(),
							anns,
							prefix, 
							snapshotViews);

					sta.center();
					sta.show();
				}

			});
	}
}
