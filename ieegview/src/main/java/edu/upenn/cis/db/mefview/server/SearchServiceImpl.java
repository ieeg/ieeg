/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.server;

import static com.google.common.base.Throwables.propagate;
import static com.google.common.base.Throwables.propagateIfInstanceOf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.events.UserEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthCheckFactory;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtAuthzHandler;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.ContactGroupSearch;
import edu.upenn.cis.braintrust.shared.ContactType;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudySearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearch;
import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.ImageType;
import edu.upenn.cis.braintrust.shared.Location;
import edu.upenn.cis.braintrust.shared.PatientSearch;
import edu.upenn.cis.braintrust.shared.Role;
import edu.upenn.cis.braintrust.shared.SeizureType;
import edu.upenn.cis.braintrust.shared.Side;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.persistence.IGraphServer.SearchComponent;
import edu.upenn.cis.db.habitat.persistence.IGraphServer.SearchComponent.Match;
import edu.upenn.cis.db.mefview.client.SearchServices;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.IFeatureVector;
import edu.upenn.cis.db.mefview.shared.LatLon;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Search;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SpecialPaths;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class SearchServiceImpl extends RemoteServiceServlet implements SearchServices {

	private final static Logger logger = LoggerFactory
			.getLogger(SearchServiceImpl.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ SearchServiceImpl.class);
	
	private static IIeegUserService uService;

	private static IGraphServer crService;
	
	
	@VisibleForTesting
	SearchServiceImpl(final IUserService userService,
			final IDataSnapshotServer dsServer) {

		uService = HabitatUserServiceFactory.getUserService(userService, dsServer);
		
		crService = CoralReefServiceFactory.getGraphServer();
	}
	
	public SearchServiceImpl() {
		
	}
	
	public void init() {
		if (uService == null) {
			uService = HabitatUserServiceFactory.getUserService();

			crService = CoralReefServiceFactory.getGraphServer();
		}
	}

	@Override
	public ExperimentSearchCriteria getExperimentSearchCriteria(
			SessionToken sessionID) throws BtSecurityException {
		final String M = "getExperimentSearchCriteria()";
		try {
			uService.verifyUser(M, sessionID, Role.USER);
			return uService.getDataSnapshotAccessSession().getExperimentSearchCriteria();
		} catch (Throwable t) {
			logger.error(M + ": caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<? extends PresentableMetadata> getMyData(String where, SessionToken sessionID) {
		final String M = "getSearchResultsForSnapshots()";
		try {
			logger.info("Loading my data for " + where);
			User u = uService.verifyUser(M, sessionID, null);
			String home = crService.getUrlBase() + "/homes/"+ u.getUsername();
			
			Set<PresentableMetadata> ret = new HashSet<PresentableMetadata>();
			if (SpecialPaths.isHome(where, home)) {
				Set<? extends PresentableMetadata> search = crService.getMyData(uService, u, null);
				if (search != null)
					ret.addAll(search);
				
				// Make sure we actually have the incoming folder set up...
				crService.getIncomingFolder(u.getUsername());

				for (PresentableMetadata md: ret) {
					if (md instanceof SearchResult) {
						SearchResult sr = (SearchResult)md;
						
						sr.setBaseRevId(uService.getDataSnapshotAccessSession().getParentShapshot(u, sr.getId()));
					}
				}
				
				Set<EegProject> projects = uService.getDataSnapshotAccessSession().getProjectsForUser(u);
				for (EegProject eProj: projects) {
					ret.add(DataMarshalling.convertEegProject(u, eProj, uService, uService.getDataSnapshotAccessSession()));
				}
				
				logger.info("Found " + ret.size() + " datasets and folders");
				
				ret.add(new BasicCollectionNode(
						where, 
						SpecialPaths.Incoming, 
						SpecialPaths.FolderType));

				ret.add(new BasicCollectionNode(
						where, 
						SpecialPaths.Public, 
						SpecialPaths.FolderType));

				// Disabled
//				ret.add(new BasicCollectionNode(
//						where, 
//						SpecialPaths.Data, 
//						SpecialPaths.FolderType));
				
//				ret.add(new BasicCollectionNode(
//						where, 
//						SpecialPaths.Recs, 
//						SpecialPaths.UserFolderType));
//				ret.add(new BasicCollectionNode(
//						where, 
//						SpecialPaths.Colleagues, 
//						SpecialPaths.UserFolderType));
//				ret.add(new BasicCollectionNode(
//						where, 
//						SpecialPaths.Groups, 
//						SpecialPaths.UserFolderType));
				
				return ret;
				
			} else if (SpecialPaths.isData(where, home)) {
				// NOTE: this isn't currently public data; it's accessible data
				
				Set<DataSnapshotSearchResult> newSnaps = uService.getDataSnapshotAccessSession().
						getDataSnapshotsForUser(u);
				newSnaps.addAll(uService.getDataSnapshotAccessSession().getDataSnapshotsNoCounts(u, new EegStudySearch()));
				
				newSnaps.addAll(uService.getDataSnapshotAccessSession().getExperimentsNoCounts(u, new ExperimentSearch(new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<Long>(),
						new HashSet<String>())));
				ret.addAll(DataMarshalling.convertSearchResults(
						newSnaps, 
						u, uService));

				for (PresentableMetadata md: ret) {
					SearchResult sr = (SearchResult)md;
					
					sr.setBaseRevId(uService.getDataSnapshotAccessSession().getParentShapshot(u, sr.getId()));

					if (sr.getId() != null) {
						if (uService.getDataSnapshotAccessSession().getChannelInfo(sr.getId()).isEmpty())
							sr.setLocation("http://www.ieeg.org");
					}
				}
				
				logger.info("Found " + ret.size() + " 'public' datasets");

			} else if (SpecialPaths.isPublic(where, home)) {

				// NOTE this is the new method, we look only at data shared with us
				ret.addAll(crService.getPublicData(u));
				
				// NOTE this is the old method, namely we find all transitively accessible data
//				ret.addAll(crService.getIncoming(u));
				

			} else if (SpecialPaths.isIncoming(where, home)) {

				// NOTE this is the new method, we look only at data shared with us
				BasicCollectionNode in = crService.getIncomingFolder(u.getUsername());
				
				ret.addAll(crService.getIncomingFolderContents(u, in));
				
				// NOTE this is the old method, namely we find all transitively accessible data
//				ret.addAll(crService.getIncoming(u));
				
				// Read project contents
			} else if (SpecialPaths.isProjects(where, home)) {
//			
				String id = where.substring(where.lastIndexOf('/') + 1);
				
				EegProject proj = uService.getDataSnapshotAccessSession().getProject(u, id);
				
				ret.addAll(this.getSearchResultsForSnapshots(sessionID, proj.getSnapshots()));
//				Set<EegProject> projects = uService.getDataSnapshotAccessSession().getProjectsForUser(u);
//				for (EegProject eProj: projects) {
//					ret.add(DataMarshalling.convertEegProject(u, eProj, uService, uService.getDataSnapshotAccessSession()));
//				}
//				logger.info("Found " + ret.size() + " projects");

			} else if (SpecialPaths.isColleagues(where, home)) {
				ret.addAll(crService.getKnownUsers(u, uService));
				logger.info("Found " + ret.size() + " known users");
				
			} else if (SpecialPaths.isGroups(where, home)) {
				ret.addAll(crService.getGroups(u));
				logger.info("Found " + ret.size() + " groups for the user");
				
			} else if (SpecialPaths.isAGroup(where, home)) {
				String groupName = where.substring((home + "/groups/").length());
				
				ret.addAll(crService.getUsersInGroup(groupName, u));
				logger.info("Found " + ret.size() + " members of the group " + groupName);
				
			} else if (SpecialPaths.isShared(where, home)) {
				Set<BasicCollectionNode> groups = crService.getGroups(u);
				for (BasicCollectionNode grp: groups)
					grp.setId(grp.getId().replaceFirst("/groups", "/shared"));
				ret.addAll(groups);
				logger.info("Found " + ret.size() + " groups for the user");
				
			} else if (SpecialPaths.isASharedItem(where, home)) {
				String groupName = where.substring((home + "/shared/").length());
				
				ret.addAll(crService.getGroupAccessible(groupName, u, 0));
				logger.info("Found " + ret.size() + " members of the group " + groupName);
				
			} else if (SpecialPaths.isRecs(where, home)) {//where.equals(home + "/Candidate colleagues")) {
				ret.addAll(crService.getRecommendedUsers(u));
				logger.info("Found " + ret.size() + " recommended users");
				
			} else {//if (where.startsWith(home)) {
				ret.addAll(crService.getFolderContents(u, where));
			}

//			ret.add(new BasicCollectionNode(
//					SpecialPaths.getHomePath(home),//"/", 
//					SpecialPaths.VirtualLab,//"(Virtual Lab)", 
//					SpecialPaths.FolderType//"Folders"
//					));
			return ret;
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<SearchResult> getSearchResultsForSnapshots(
			SessionToken sessionID, final Set<String> dsRevIds)
			throws BtSecurityException {
		final String M = "getSearchResultsForSnapshots()";
		try {
			User u = uService.verifyUser(M, sessionID, null);

			Set<SearchResult> results = DataMarshalling.convertSearchResults(
					uService.getDataSnapshotAccessSession().getLatestSnapshots(u, dsRevIds), u, uService);
			
			Set<SearchResult> iCanSee = new HashSet<SearchResult>();
			for (SearchResult result: results)
				try {
					AuthCheckFactory.getHandler().checkPermitted(
							uService.getDataSnapshotAccessSession().getPermissions(
									u, HasAclType.DATA_SNAPSHOT,
									CorePermDefs.CORE_MODE_NAME, result.getId(), true),
							CorePermDefs.READ,
							result.getId());
					iCanSee.add(result);
				} catch (AuthorizationException e) {}
			
			return iCanSee;
			
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}
	
	Set<SearchComponent> getSearchComponents(String searchString) {
		Map<String, Match> opMap = new HashMap<String, Match>();
		
		opMap.put("~", Match.Approx);
		opMap.put("=", Match.Equal);
		opMap.put("<", Match.Less);
		opMap.put("<=", Match.LessEq);
		opMap.put(">", Match.Greater);
		opMap.put(">=", Match.GreaterEq);
		opMap.put("!=", Match.NotEq);
		opMap.put("<>", Match.NotEq);
		opMap.put("contains", Match.Contains);
		
		searchString = searchString
		           .replaceAll("<>", " != ")
		           .replaceAll("!=", " != ")
		           .replaceAll("~", " ~ ")
		           .replaceAll("=", " = ")
		           .replaceAll("<=", " <= ")
		           .replaceAll(">=", " >= ")
		           .replaceAll("<", " < ")
		           .replaceAll(">", " > ");
		
		searchString = searchString
		           .replaceAll("< =", " <= ")
		           .replaceAll("> =", " <= ");
		
		searchString = searchString
		           .replaceAll("  ", " ");

		
		Set<SearchComponent> stringMatches = new HashSet<SearchComponent>();
		String[] searchWords = searchString.split(" ");
		
		for (int i = 0; i < searchWords.length; i++) {
			// If we aren't the last word, do a lookahead of 2 and see if we are there
			if (i + 2 < searchWords.length) {
				if (opMap.keySet().contains(searchWords[i + 1])) {
					stringMatches.add(new IGraphServer.ValueSearchComponent(searchWords[i], searchWords[i+2], 
							opMap.get(searchWords[i+1])));
					
					i += 2;
				} else {
					stringMatches.add(new IGraphServer.StringSearchComponent(searchWords[i]));
				}
			} else
				stringMatches.add(new IGraphServer.StringSearchComponent(searchWords[i]));
		}
		
		return stringMatches;
	}
	
	@Override
	public List<? extends PresentableMetadata> getSearchMatches(SessionToken sessionID,
			final String searchString, int start, int max) {
		final String M = "getSearchResultsForSnapshots()";
		UserEvent.logEvent(UserEvent.EventType.SEARCH, searchString);
		try {
			logger.info("Searching for " + searchString);
			User u = uService.verifyUser(M, sessionID, null);
			
			Set<SearchComponent> stringMatches = getSearchComponents(searchString);
			
			List<? extends PresentableMetadata> results = crService.getSnapshotsMatching(uService, u, stringMatches);
			
//			return ret;
			List<PresentableMetadata> iCanSee = new ArrayList<PresentableMetadata>();
			for (PresentableMetadata result: results)
					if (AuthCheckFactory.getHandler().isPermitted(
							uService.getDataSnapshotAccessSession().getPermissions(
									u, HasAclType.DATA_SNAPSHOT,
									CorePermDefs.CORE_MODE_NAME, result.getId(), true),
							CorePermDefs.READ,
							result.getId())) {
						iCanSee.add(result);
					} else {
						logger.info("No permissions on " + result.getFriendlyName());
					}
			
			logger.info("Returned " + iCanSee.size() + " search results (of " + results.size() + ")");

			return iCanSee;
//			return uService.getDataSnapshotAccessSession().getSearchMatches(u, terms, start, max);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public List<? extends PresentableMetadata> updateSearchMatches(
			Set<SearchResult> updatedResults,
			SessionToken sessionID,
			final String searchString, int start, int max) {
		final String M = "updateSearchMatches()";
		try {
			User u = uService.verifyUser(M, sessionID, null);
			
			List<IFeatureVector> positive = new ArrayList<IFeatureVector> ();
			List<IFeatureVector> negative = new ArrayList<IFeatureVector> ();
			
			for (SearchResult sr: updatedResults) {
				if (sr.getIsValid() == null)
					; 			// Skip
				else if (sr.getIsValid()) {
					positive.add(sr.getFeatures());
				} else {
					negative.add(sr.getFeatures());
				}
			}
			
			crService.setFeedbackOnFeatures(u.getUserId().getIdentifier(), positive, negative);
			
			Set<SearchComponent> stringMatches = getSearchComponents(searchString);

			List<? extends PresentableMetadata> results = crService.getSnapshotsMatching(uService, u, stringMatches);
			
			List<PresentableMetadata> iCanSee = new ArrayList<PresentableMetadata>();
			for (PresentableMetadata result: results)
				try {
					AuthCheckFactory.getHandler().checkPermitted(
							uService.getDataSnapshotAccessSession().getPermissions(
									u, HasAclType.DATA_SNAPSHOT,
									CorePermDefs.CORE_MODE_NAME, result.getId(), true),
							CorePermDefs.READ,
							result.getId());
					iCanSee.add(result);
				} catch (AuthorizationException e) {}
			
			logger.info("Returned " + iCanSee.size() + " search results (of " + results.size() + ")");

			return iCanSee;
			

//			return uService.getDataSnapshotAccessSession().getSearchMatches(u, terms, start, max);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	@Override
	public Set<SearchResult> getSnapshotsFor(SessionToken sessionID,
			ExperimentSearch expSearch) throws BtSecurityException {
		final String M = "getSnapshotsFor()";
		try {
			User user = uService.verifyUser(M, sessionID, Role.USER);
			return DataMarshalling.convertSearchResults(
					uService.getDataSnapshotAccessSession().getExperiments(user, expSearch), user, uService);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}

	/**
	 * Converts from a GWT search parameter object to one used by the Hibernate
	 * services
	 * 
	 * @param s
	 * @return
	 */
	private static EegStudySearch convertSearch(Search s) {
		EegStudySearch dss = new EegStudySearch();

		ContactGroupSearch electrodeSearch = new ContactGroupSearch();
		dss.setContactGroupSearch(electrodeSearch);

		Set<Location> locations = electrodeSearch.getLocations();
		for (String l : s.getLocations()) {
			locations.add(Location.fromString.get(l));
		}

		electrodeSearch.setSamplingRateMin(s.getSamplingRateMin());
		logger.debug("Min sampling " + electrodeSearch.getSamplingRateMin());

		electrodeSearch.setSamplingRateMax(s.getSamplingRateMax());
		logger.debug("Max sampling " + electrodeSearch.getSamplingRateMax());

		Set<Side> sides = electrodeSearch.getSides();
		for (String side : s.getSides()) {
			sides.add(Side.fromString.get(side));
		}
		logger.debug("Adding sides " + electrodeSearch.getSides().toString());

		Set<ContactType> contactTypes = electrodeSearch.getContactTypes();
		for (String contactType : s.getContactTypes()) {
			contactTypes.add(ContactType.FROM_STRING.get(contactType));
		}
		logger.debug("Adding contact types "
				+ electrodeSearch.getContactTypes());

		PatientSearch patientSearch = new PatientSearch();
		dss.setPatientSearch(patientSearch);
		patientSearch.setAgeOfOnsetMin(s.getAgeOfOnsetMin());
		patientSearch.setAgeOfOnsetMax(s.getAgeOfOnsetMax());

		if (s.getPartialSzTypes()) {
			patientSearch.setPartialSzTypes();
		}
		if (s.getGenSzTypes()) {
			patientSearch.setGeneralizedSzTypes();
		}
		Set<Gender> genders = patientSearch.getGenders();
		for (String g : s.getGenders())
			genders.add(Gender.fromString.get(g));

		Set<SeizureType> szTypes = patientSearch.getSzTypes();
		for (String sz : s.getSzTypes()) {
			SeizureType t = SeizureType.fromString.get(sz);
			szTypes.add(t);
		}

		Set<ImageType> imageTypes = dss.getImageTypes();
		for (String itn : s.getImageTypes())
			imageTypes.add(ImageType.fromString.get(itn));

		dss.setMinSzCount(s.getMinRecordedSzCnt());
		dss.setMaxSzCount(s.getMaxRecordedSzCnt());

		return dss;
	}


	/**
	 * Searches for snapshots meeting search criteria
	 */
	@Override
	public Set<SearchResult> getSnapshotsFor(SessionToken sessionID,
			Search searchRestrictions) throws BtSecurityException {
		final String M = "getSnapshotsFor(...)";
		try {
			User user = uService.verifyUser(M, sessionID, Role.USER);
			return DataMarshalling.convertSearchResults(
					uService.getDataSnapshotAccessSession().getDataSnapshots(user,
							convertSearch(searchRestrictions)), user, uService);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}


	@Override
	public List<LatLon> getUserLocations() {
		return uService.getLatLon();
	}


	@Override
	public Set<SearchResult> getSnapshotsForUser(SessionToken sessionID) {
		final String M = "getSnapshotsFor(...)";
		try {
			User user = uService.verifyUser(M, sessionID, Role.USER);
			return DataMarshalling.convertSearchResults(
					uService.getDataSnapshotAccessSession().getDataSnapshotsForUser(user), user, uService);
		} catch (Throwable t) {
			logger.error(M + ": Caught exception", t);
			propagateIfInstanceOf(t, BtSecurityException.class);
			throw propagate(t);
		}
	}


}
