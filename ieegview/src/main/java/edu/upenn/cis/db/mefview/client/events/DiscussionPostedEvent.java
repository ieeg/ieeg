/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.shared.Post;
import edu.upenn.cis.db.mefview.shared.Project;

public class DiscussionPostedEvent extends GwtEvent<DiscussionPostedEvent.Handler>{
	private static final Type<DiscussionPostedEvent.Handler> TYPE = new Type<DiscussionPostedEvent.Handler>();
	
	private final DataSnapshotModel model;
	private final Project project;
	private final Post post;
	
	public DiscussionPostedEvent(DataSnapshotModel model,
			final Post post) {
		this.model = model;
		this.project = null;
		this.post = post;
	}
	
	public DiscussionPostedEvent(Project project,
			final Post post) {
		this.project = project;
		this.model = null;
		this.post = post;
	}
	
	public DataSnapshotModel getDataSnapshotState() {
		return model;
	}
	
	public Post getPost() {
		return post;
	}
	public static com.google.gwt.event.shared.GwtEvent.Type<DiscussionPostedEvent.Handler> getType() {
		return TYPE;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DiscussionPostedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DiscussionPostedEvent.Handler handler) {
		if (model != null)
			handler.onDiscussionPosted(model, post);
		else
			handler.onDiscussionPosted(project, post);
	}

	public static interface Handler extends EventHandler {
		void onDiscussionPosted(DataSnapshotModel action, 
				Post post);
		
		void onDiscussionPosted(Project project, 
				Post post);
	}
}
