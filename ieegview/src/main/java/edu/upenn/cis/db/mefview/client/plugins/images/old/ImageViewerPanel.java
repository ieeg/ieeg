/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.images.old;

import java.util.Collection;
import java.util.List;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPanel;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFViewerPanel;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.viewers.ImagePane;
import edu.upenn.cis.db.mefview.client.widgets.ContextMenu;
import edu.upenn.cis.db.mefview.client.widgets.ResizableFocusPanel;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.places.ImagePlace;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


//@SuppressWarnings("deprecation")
public class ImageViewerPanel extends AstroTab implements ImageViewerPresenter.Display {
	// msec to wait
	public static final int LAG = 800;

	ImagePane image;
	
	int showHighlight;
	int hideHighlight;
	int cursor = 0;

	TextBox detectBox;

	ListBox eventCombo;
	ListBox timeCombo;
	
	int pageWidth;
	//int traceGap = 10;
	double traceScale = 1.0;

	FocusPanel imageFocus;
	VerticalPanel imageToolbarAndViewPanel = new VerticalPanel();
//	EEGDisplayAsync webService;
	List<INamedTimeSegment> traceList;
	
	ContextMenu pop;
	MenuItem showImage;
	MenuItem showDocuments;
	MenuItem showDetails;
	MenuItem showDcn;
	MenuItem showRelated;
	MenuItem dlDicoms;
	
	DataSnapshotViews controller;
	
	public static final String NAME = "2d-image";
	final static ImageViewerPanel seed = new ImageViewerPanel();

	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ImageViewerPanel() {
		super();
	}

	public ImageViewerPanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title) {
		
		super (clientFactory, model, 
		    title);//width, height);
		
		controller = model;

//		webService = service;
//		computeSizes(/*Window.getClientWidth() - width*/
//				getWidth(), 
//				getHeight());//Window.getClientWidth(), Window.getClientHeight());
		imageFocus = new ResizableFocusPanel();
		add(imageFocus);
		imageFocus.setWidget(imageToolbarAndViewPanel);
		imageToolbarAndViewPanel.setSize(String.valueOf(windowWidth) + "px", String.valueOf(windowHeight) + "px");
//		setWidgetLeftRight(imageFocus, 2, Unit.PX, 2, Unit.PX);
//		setWidgetTopBottom(imageFocus, 1, Unit.PX, 1, Unit.PX);

		image = new ImagePane(imageToolbarAndViewPanel, this, model, imageFocus,// pan, 
//				service,
				clientFactory,
				graphWidth, graphHeight);//, model.getStudyID());
		
		initContextMenu();
		
		if (!getDataSnapshotState().getTraceInfo().isEmpty())
			setTraceInfo(getDataSnapshotState().getTraceInfo());
	  }
	
	private void initContextMenu() {
		Command eegCmd = new Command() {
			public void execute() {
				pop.hide();
//				ImageViewerPanel.this.controller.switchToEEGPane();
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new
						OpenDisplayPanel(
								controller.getSearchResult(), false,
								EEGViewerPanel.NAME, null)));
			}
		};
		Command detailsCmd = new Command() {
			public void execute() {
				pop.hide();
//				clientFactory.getSnapshotsWindow().highlightMetadata(
//						Sets.newHashSet(ImageViewerPanel.this.getDataSnapshotState().getSearchResult()));
//				clientFactory.getControlPane().setSnapshotsPane();
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new OpenDisplayPanel( 
						ImageViewerPanel.this.controller.getSearchResult(), false, 
						MetadataBrowserPanel.NAME, null)));
			}
		};
		Command documentsCmd = new Command() {
			public void execute() {
				pop.hide();

				IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenDisplayPanelEvent(new OpenDisplayPanel( 
						ImageViewerPanel.this.controller.getSearchResult(), false, 
						PDFViewerPanel.NAME, null)));
//				clientFactory.getControlPane().createPDFPane(MainImageWindow.this.getDataSnapshotModel());
//				clientFactory.getControlPane().selectStudy(user, 
//						ImageViewerPanel.this.controller.getSearchResult(), false, 
////						PanelType.PDF,
//						PDFViewerPanel.NAME,
//						null);
			}
		};
		Command cmd = new Command() {
			public void execute() {
				pop.hide();
			}
		};
		showImage = new MenuItem("Show EEG", eegCmd);
		showDetails = new MenuItem("Show snapshot details", detailsCmd);
		
		showDocuments = new MenuItem("Show related documents", documentsCmd);
		dlDicoms = new MenuItem("Download DICOMs", cmd);
		showDcn = new MenuItem("Show snapshot DCN", cmd);
		showRelated = new MenuItem("Show related snapshots...", cmd);
		
		dlDicoms.setEnabled(false);
		showRelated.setEnabled(false);
		showDcn.setEnabled(false);
		
		pop = new ContextMenu(this);
		pop.addMenuItem(showImage);
		pop.addMenuItem(showDocuments);
		pop.addMenuItem(showDetails);
		pop.addMenuItem(showDcn);
		pop.addMenuItem(showRelated);
		pop.addMenuSeparator();
		pop.addMenuItem(dlDicoms);
	}
	
	
	
	
//	@Override
//	public void computeSizes(int width, int height) {
//		width -= 10;
//		height -= 56;
//
//		windowHeight = height - 48;
//		windowWidth = width - 8;// - 70;
//		graphWidth = windowWidth - 48;//32;//12;
//		graphHeight = windowHeight - 72;//100;//64;//230;
//		
//		pageWidth = graphWidth;
//	}
	
	public void setTraceInfo(List<INamedTimeSegment> traces) {
//		System.err.println("initializing traces");

		image.init(traces);
		
		traceList = traces;
	}
	
	public ImagePane getImagePane() {
		return image;
	}
	
	public void hideStatus() {
		image.hideStatus();
	}
	
	public boolean isSet() {
		return (image != null);//(traceList != null);
	}
	
	public void updateRevId(final String oldId, final String newId) {
		super.updateRevId(oldId, newId);
		
		if (getImagePane() != null)
			getImagePane().updateRevId(oldId, newId);
	}
	
//	public void refreshAnnotations(List<AnnBlock> newAnnotations) {
//		if (getImagePane() != null)
//			getImagePane().refreshAnnotations(newAnnotations);
//	}

//	@Override
//	public void setTimestamp(double timeUutc) {
//		getImagePane().setTimestamp(timeUutc);
//	}

//	@Override
//	public void setDuration(double duration) {
//		getImagePane().setDuration(duration);
//	}

//	@Override
//	public void setChannels(Collection<String> channel) {
//		getImagePane().setChannels(channel);
//	}

//	@Override
//	public void zoomBy(double ratio) {
//		getImagePane().zoomBy(ratio);
//	}

//	@Override
	public void highlightMetadata(Collection<? extends PresentableMetadata> items) {
		getImagePane().highlightMetadata(items);
		
	}


//	@Override
//	public void registerPrefetchListener(Prefetcher p) {
//		image.registerPrefetchListener(p);
//	}

	public void refresh() {
		image.refresh();
	}

//	@Override
//	public PanelType getPanelType() {
//		return PanelType.IMAGE;
//	}
	public String getPanelType() {
		return NAME;
	}

	@Override
	public DataSnapshotModel getDataSnapshotState() {
//		GWT.log("ImageViewerPanel.getDataSnapshotState() == " + image.getDataSnapshotState());
		return image.getDataSnapshotState();
	}


	@Override
	public void setPlace(WindowPlace place) {
		image.setPlace(place);
	}

	@Override
	public AstroPanel create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
//			final Project project,
			boolean writePermissions,
			final String title) {
		return new ImageViewerPanel(model, clientFactory, title);
	}

	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getImageTab();
	}

	@Override
	public WindowPlace getPlace() {
		return new ImagePlace(getImagePane().getSelectedImage(), getDatasetID(), getImagePane().getImageTypeIndex());
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return ImagePlace.getFromSerialized(params);
//	}

	public String getTitle() {
		return "Images";
	}

	@Override
	public void setDataSnapshotModel(DataSnapshotModel model) {
		image.setDataSnapshotModel(model);
		traceList = model.getTraceInfo();
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
		image.bindDomEvents();
//		for (Widget w: Arrays.asList(detectBox, eventCombo, timeCombo, imageFocus, pop)) {
//			final Widget wHere = w;
//			if (wHere != null)
//				Converter.getHTMLFromElement(w.getElement()).addEventListener("click", new EventListener() {
//					
//					@Override
//					public void handleEvent(final com.blackfynn.dsp.client.dom.Event event) {
//						NativeEvent clickEvent = Converter.getNativeEvent(event);
//						DomEvent.fireNativeEvent(clickEvent, wHere);
//					}
//				});
//		}
	}
}
