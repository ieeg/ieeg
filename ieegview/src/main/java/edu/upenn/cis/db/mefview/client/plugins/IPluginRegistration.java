package edu.upenn.cis.db.mefview.client.plugins;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserChildPanel.ChildPane;

public interface IPluginRegistration {
	public void registerActions();
	
	public Map<Class<?>,Set<ChildPane>> getTypeMap();

	public void bindActionsToDataTypes();
	public List<String> getDefaultPanels();
	public List<String> getLoggedInPanels();
	public void registerPanels();

	public List<String> getNotLoggedInPanels();
	
}
