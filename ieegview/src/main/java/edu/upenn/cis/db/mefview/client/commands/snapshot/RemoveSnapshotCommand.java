package edu.upenn.cis.db.mefview.client.commands.snapshot;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.DialogBox;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews.OnDoneHandler;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;

/**
 * Delete contents from a snapshot
 * 
 * @author zives
 *
 */
public class RemoveSnapshotCommand implements Command {
	final String dataset;
	final AppModel cs;
	final DialogBox waitDialog; 
	final OnDoneHandler handler;
	final ClientFactory clientFactory;
	
	public RemoveSnapshotCommand(
			final ClientFactory clientFactory,
			final AppModel cs,
			final String dataset,
			final DialogBox waitDialog, 
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.cs = cs;
		this.waitDialog = waitDialog;
		this.handler = handler;
		this.clientFactory = clientFactory;
	}

	public RemoveSnapshotCommand(
			final ClientFactory clientFactory,
			final DataSnapshotViews dsv,
			final String dataset,
			final DialogBox waitDialog, 
			final OnDoneHandler handler) {
		this.dataset = dataset;
		this.cs = dsv.getAppState();
		this.waitDialog = waitDialog;
		this.handler = handler;
		this.clientFactory = clientFactory;
	}

	@Override
	public void execute() {
		clientFactory.getSnapshotServices().removeDataSnapshot(
				clientFactory.getSessionModel().getSessionID(), 
				dataset, 
				new SecFailureAsyncCallback<Boolean>(clientFactory) {

			@Override
			public void onFailureCleanup(Throwable caught) {
				if (waitDialog != null)
					waitDialog.hide();	
			}
			
			@Override
			public void onNonSecFailure(Throwable caught) {
				Dialogs.messageBox("Error Removing Snapshot", "Sorry, there was an error removing the snapshot.");
			}

			@Override
			public void onSuccess(final Boolean success) {
				if (waitDialog != null)
					waitDialog.hide();
				if (success) {
					DataSnapshotViews dsm = cs.getSnapshotModel(dataset);
					dsm.removeThisStudy();
				} else
					Dialogs.messageBox("Error Removing Snapshot", "Sorry, there was an error removing the snapshot.");
			}

		});
	}

}
