/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.ToolUpdatedEvent;

public class RegisterTool extends DialogBox {
	Button ok = new Button("OK");
	Button cancel = new Button("Cancel");
	// TextBox theLabel = new TextBox();
	TextBox theTool = new TextBox();
	TextArea description = new TextArea();
	// TextBox theVersion = new TextBox();
	// TextBox theDate = new TextBox();
	TextBox theAuthor = new TextBox();
	TextBox theType = new TextBox();
	TextBox sourceURL = new TextBox();
	TextBox binURL = new TextBox();
	ListBox lb = new ListBox();

	ToolDto toolInf = null;
	// SingleUploader uploader = new SingleUploader();

	String uploadedPath = "";

	static int count = 0;

	/**
	 * Constructor for updating a tool.
	 * 
	 * @param userName
	 * @param clientFactory
	 * @param ti
	 */
	public RegisterTool(
			ClientFactory clientFactory, ToolDto ti) {
		toolInf = ti;

		// Creating dialog box.
		init(clientFactory, true);

		// Setting fields in Dialog Box.
		theTool.setText(ti.getLabel());
		binURL.setText(ti.getBinUrl());
		binURL.setReadOnly(true);
		description.setText(ti.getDescription().orNull());
		sourceURL.setText(ti.getSourceUrl());
	}

	/**
	 * Constructor for adding a new tool
	 * 
	 * @param userid
	 * @param clientFactory
	 */
	public RegisterTool(
			ClientFactory clientFactory) {
		init(clientFactory, false);
	}

	private void init(
			final ClientFactory clientFactory,
			boolean isUpdate) {

		// uploader.setServletPath(uploader.getServletPath() + "?user="+
		// userid);
		VerticalPanel vp = new VerticalPanel();

		if (isUpdate) {
			setTitle("Update Tool Registration");
			setText("Update Tool Registration");
		} else {
			setTitle("Register New Tool");
			setText("Register New Tool");
		}
		setWidget(vp);

		lb.addItem("Github Repository");
		lb.addItem("External Link");
		// lb.addItem("Internal");

		vp.add(new Label("Source Location Type:"));
		vp.add(lb);

		vp.add(new Label("Tool name (must be unique):"));
		vp.add(theTool);
		theTool.setText("ToolName...");

		vp.add(new Label("Description:"));
		vp.add(description);
		description.setText("Tool description...");
		// description.setWidth("95%");

		vp.add(new Label("Author:"));
		vp.add(theAuthor);
		if (isUpdate)
			theAuthor.setText(toolInf.getAuthorName());
		else
			theAuthor.setText(clientFactory.getSessionModel().getUserId());

		vp.add(new Label("Sourcecode Link:"));
		vp.add(sourceURL);

		vp.add(new Label("Download Link:"));
		vp.add(binURL);

		HorizontalPanel hp2 = new HorizontalPanel();
		hp2.add(ok);
		hp2.add(cancel);
		vp.add(hp2);
		vp.setCellHorizontalAlignment(hp2, HasHorizontalAlignment.ALIGN_RIGHT);

		ok.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				// Match id when updating
				if (toolInf == null) {
					toolInf = new ToolDto(
							theTool.getText(),
							null,
							clientFactory.getSessionModel().getUserId(),
							sourceURL.getText(),
							binURL.getText(),
							null,
							description.getText());

				}

				ToolDto ti = toolInf;

				hide();

				/*
				 * if (ti.getRevId() == null) { for (ToolInfo tool:
				 * control.getToolInfo()) { if
				 * (tool.getName().equals(ti.getName()) &&
				 * tool.getVersion().equals(ti.getVersion())) {
				 * Dialogs.messageBox("Unable to Save Tool",
				 * "There is already another tool with the same name and version"
				 * ); return; } } } // Register, but only add to the tool table
				 * display if it isn't an update control.addTool(ti,
				 * ti.getRevId() == null);
				 */
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new ToolUpdatedEvent(ti));
			}

		});

		cancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				hide();
			}

		});

		// Clickhandler for SourceLocType control
		lb.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				switch (lb.getSelectedIndex()) {
					case 0:
						// commercial.setValue(true);
						// break;
					case 1:
						sourceURL.setEnabled(!sourceURL.isEnabled());
						// apache.setValue(true);
						// break;
					case 2:
						// gpl.setValue(true);
						// break;
					default:
						// other.setValue(true);
				}

				sourceURL.setEnabled(!sourceURL.isEnabled());
			}
		});
	}
}
