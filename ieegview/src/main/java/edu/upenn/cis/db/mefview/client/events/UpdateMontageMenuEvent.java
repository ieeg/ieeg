package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;


import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPresenter;
//import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPresenter;

public class UpdateMontageMenuEvent extends GwtEvent<UpdateMontageMenuEvent.Handler> {
    private static final Type<UpdateMontageMenuEvent.Handler> TYPE = new Type<UpdateMontageMenuEvent.Handler>();
    
    private final EEGViewerPresenter presenter;
    
    public UpdateMontageMenuEvent(EEGViewerPresenter eegViewerPresenter){
        this.presenter = eegViewerPresenter;
    }
    
    public static com.google.gwt.event.shared.GwtEvent.Type<UpdateMontageMenuEvent.Handler> getType() {
        return TYPE;
    }

    @Override
    public com.google.gwt.event.shared.GwtEvent.Type<UpdateMontageMenuEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UpdateMontageMenuEvent.Handler handler) {
        handler.onOpenedProject(presenter);
    }

    public interface Handler extends EventHandler {
        void onOpenedProject(EEGViewerPresenter presenter);
    }
}