/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.messages;

import edu.upenn.cis.db.mefview.client.messages.requests.UpdateSavedAnnotations;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;


public class SavedSnapshotMessage<AnnType> {
	public String snapshotID;
	public DataSnapshotModel model;
	public UpdateSavedAnnotations<AnnType> priorAction;
	
	public SavedSnapshotMessage(String snapshotID, DataSnapshotModel model, UpdateSavedAnnotations<AnnType> act) {
		this.snapshotID = snapshotID;
		this.model = model;
		this.priorAction = act;
	}

	public String getSnapshotID() {
		return snapshotID;
	}

	public void setSnapshotID(String snapshotID) {
		this.snapshotID = snapshotID;
	}

	public DataSnapshotModel getModel() {
		return model;
	}

	public void setModel(DataSnapshotModel model) {
		this.model = model;
	}

	public UpdateSavedAnnotations getPriorAction() {
		return priorAction;
	}

	public void setPriorAction(UpdateSavedAnnotations<AnnType> priorAction) {
		this.priorAction = priorAction;
	}
}
