/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;

public class ToolResultCreatedEvent extends GwtEvent<ToolResultCreatedEvent.Handler> {
	private static final Type<ToolResultCreatedEvent.Handler> TYPE = new Type<ToolResultCreatedEvent.Handler>();

	String parentSnapshot;
	String parentCreator;
	String parentFriendly;
	DerivedSnapshot result;
	
	public ToolResultCreatedEvent(String parentSnapshot, String parentFriendly,
			DerivedSnapshot result, final String sourceUser) {
		this.parentSnapshot = parentSnapshot;
		this.parentFriendly = parentFriendly;
		this.parentCreator = sourceUser;
		this.result = result;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<ToolResultCreatedEvent.Handler> getType() {
		return TYPE;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ToolResultCreatedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ToolResultCreatedEvent.Handler handler) {
		handler.onToolResultCreated(parentSnapshot, parentFriendly, parentCreator, result);
	}

	public static interface Handler extends EventHandler {
		void onToolResultCreated(String parentSnapshot, String parentFriendly, 
				String parentCreator, DerivedSnapshot result);
	}
}
