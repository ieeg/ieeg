/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import static com.google.common.collect.Sets.newHashSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FocusWidget;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ContextSupplier;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class DialogCreator {
	
	public interface DialogCallback {
		public void onClick(XMLDialog box, ClickEvent event);
	}
	
	public static class XMLDialog extends DialogBox {
		private HashMap<String,FocusWidget> widgets = new HashMap<String,FocusWidget>();
		private HashMap<String,Object> values = new HashMap<String,Object>();
		
		private HashMap<String,Object> minValues = new HashMap<String,Object>();
		private HashMap<String,Object> maxValues = new HashMap<String,Object>();
		
//		private ToolInfo.PARALLEL parallelism = ToolInfo.PARALLEL.SERIAL;
		private String sourceRevID = "";
		private String toolRevID = "";
		private String command = "";
		private String creator = "";
		
//		private FilterSpec filter = new FilterSpec();
		
		public Map<String,Object> getValueMap() { return values; }
		
		public Object getValue(String str) { return values.get(str); }
		public void putValue(String str, Object value) { values.put(str, value); }
		
		public FocusWidget getWidget(String str) { return widgets.get(str); }
		public void putWidget(String str, FocusWidget widget) { widgets.put(str, widget); }
//		public FilterSpec getFilter() {
//			return filter;
//		}
//		
//		public void setFilter(FilterSpec filter) {
//			this.filter = filter;
//		}
		
//		public ToolInfo.PARALLEL getParallelism() {
//			return parallelism;
//		}
		
		public void setMin(String name, Object value) {
			minValues.put(name, value);
		}

		public void setMax(String name, Object value) {
			maxValues.put(name, value);
		}

		public Object getMin(String name) {
			return minValues.get(name);
		}

		public Object getMax(String name) {
			return maxValues.get(name);
		}

//		public void setParallelism(ToolInfo.PARALLEL parallelism) {
//			this.parallelism = parallelism;
//		}

		public String getToolRevID() {
			return toolRevID;
		}

		public void setToolRevID(String toolRevID) {
			this.toolRevID = toolRevID;
		}
		
		public String getSourceRevID() {
			return sourceRevID;
		}

		public void setSourceRevID(String sourceRevID) {
			this.sourceRevID = sourceRevID;
		}

		public void setCreator(String creator) {
			this.creator = creator;
		}
		
		public String getCreator() {
			return creator;
		}

		public String getCommand() {
			return command;
		}

		public void setCommand(String command) {
			this.command = command;
		}

		public String getXML() {
			StringBuilder ret = new StringBuilder("<snapshot><input-parameters>\n");
			
			ret.append("<revID>" + toolRevID + "</revID><creator>" + creator + "</creator><sourceID>" + sourceRevID + "</sourceID><command>" + command + "</command>");
			
//			if (getParallelism() == ToolInfo.PARALLEL.PAR_BLOCK)
//				ret.append("<parallelism type='block'/>\n");
//			else if (getParallelism() == ToolInfo.PARALLEL.PAR_CHANNEL)
//				ret.append("<parallelism type='channel'/>\n");
//			else
//				ret.append("<parallelism type='none'/>\n");
//			
			for (String k : values.keySet()) {
				ret.append("<parameter type='");
				if (values.get(k) instanceof List) {
					ret.append("StringList");
				} else if (values.get(k) instanceof Integer) {
					ret.append("Integer");
				} else if (values.get(k) instanceof Long) {
					ret.append("Long");
				} else if (values.get(k) instanceof String) {
					ret.append("String");
				} else if (values.get(k) instanceof Double) {
					ret.append("Double");
				} else if (values.get(k) instanceof Boolean) {
					ret.append("Boolean");
				} else {
					ret.append("Unknown");
				}
				ret.append("'>");
				ret.append("<key>" + k + "</key>");
				Object val = values.get(k);
				if (val instanceof List) {
					for (Object o : (List<?>)val)
						ret.append("<value>" + o.toString() + "</value>");
				} else
					ret.append("<value>" + val.toString() + "</value>");
				ret.append("</parameter>\n");
			}
			
			ret.append("</input-parameters></snapshot>\n");
			return ret.toString();
		}
	}
	
	public static void createDefaultDialog(final ContextSupplier cx, final String sourceID, final String creator, ToolDto toolInfo, 
			final List<INamedTimeSegment> traces, final DisplayConfiguration filter, final DialogCallback handler) throws RequestException {
		createDefaultDialog(cx, sourceID, creator, toolInfo.getId().get(), 
				toolInfo.getBinUrl(), traces, filter, handler);
	}

	public static void createDefaultDialog(final ContextSupplier cx, final String sourceID, final String creator,
			final String revID, final String commandLine,
			final List<INamedTimeSegment> traces, final DisplayConfiguration filter, final DialogCallback handler) throws RequestException {
		new RequestBuilder(RequestBuilder.GET, "parametersDialog.xml").sendRequest("", new RequestCallback() {

			  @Override
			  public void onError(Request res, Throwable throwable) {
			    Window.alert("Unable to read parameters dialog box");
			  }

			@Override
			public void onResponseReceived(Request request, Response response) {
			    String text = response.getText();
			    createDialog(cx, sourceID, creator, revID, 
			    		commandLine, traces, filter, handler);
			}
			});
	}

	public static void createDialog(final ContextSupplier cx, final String sourceID, final String creator, ToolDto toolInfo, 
			 final List<INamedTimeSegment> traces, final DisplayConfiguration filter, final DialogCallback handler) {
		createDialog(cx, sourceID, creator, toolInfo.getId().get(), 
				toolInfo.getBinUrl(), traces, filter, handler);
	}
	
	// Format:
	// type is one of:
	//  * clob (TextArea)
	//  * string (TextBox)
	//  * int
	//  * double
	//  * boolean (check box)
	//  {set}  (drop-list)
	//  (set)  (radio button)
	//  [set]  check boxes if < 10, multi-select list otherwise
	
	public static void createDialog(final ContextSupplier cx, 
			final String sourceID, final String creator, 
			String revID, String commandLine, 
			final List<INamedTimeSegment> traces,
			final DisplayConfiguration filter,
			final DialogCallback handler) {
		final XMLDialog ret = new XMLDialog();
		
//		Document d = XMLParser.parse(xmlDescriptor);
//		Node dialogRoot = getChildElement(d, "dialog");
		
//		Node dialogName = getChildElement(dialogRoot, "name");
		
//		ret.setTitle(getTextChild(dialogName));
//		ret.setText(getTextChild(dialogName));
		
//		System.err.println("Creating command-line info for " + revID + ", command " + commandLine + ", parallelism " + parallelism.toString());
		ret.setCommand(commandLine);
		ret.setToolRevID(revID);
		ret.setSourceRevID(sourceID);
		ret.setCreator(creator);
		
//		NodeList list = d.getElementsByTagName("item");

		VerticalPanel vp = new VerticalPanel();
		ret.add(vp);
		final Set<CheckBox> checkBoxes = newHashSet();
//		for (int i = 0; i < list.getLength(); i++) {
//			Node n = list.item(i);
//			
//			Node key = getChildElement(n, "key");
//			Node desc = getChildElement(n, "description");
//			Node def = getChildElement(n, "default");
//			Node type = getChildElement(n, "type");
//			Node editable = getChildElement(n, "editable");
////			Node width = getChildElement(n, "width");
////			Node height = getChildElement(n, "height");
//			Node min = getChildElement(n, "min");
//			Node max = getChildElement(n, "max");
//			
//			// Used only by checkboxes
//			final Node invert = getChildElement(n, "invert");
//			final Node enable = getChildElement(n, "enable");
//			
//			if (key == null || type == null || desc == null)
//				continue;
//			
//			HorizontalPanel pan = new HorizontalPanel();
//			vp.add(pan);
//
//			String typ = getTextChild(type);
//			typ = expandMacros(typ, cx, traces, filter);
//			String[] items = null;
//			
//			// Boolean -> checkbox, so we don't add a label for it.
//			// For all other cases we add a label.
//			pan.add(new Label(getTextChild(desc)));
//			if (typ.charAt(0) == '{' || typ.charAt(0) == '(' || typ.charAt(0) == '[') {
//				items = getList(typ);
//			}
//			
//			final String keyName = getTextChild(key);
//			if (typ.equals("clob")) {
//				TextArea tb = new TextArea();
//				ret.putWidget(keyName, tb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					if (!str.isEmpty())
//						ret.putValue(keyName, str);
//					tb.setValue(str);
//				}
//				if (editable != null)
//					tb.setEnabled(false);
//				pan.add(tb);
//				
//				tb.addValueChangeHandler(new ValueChangeHandler<String>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<String> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						ret.putValue(keyName, event.getValue());
//					}
//					
//				});
//			} else if (typ.equals("string")) {
//				TextBox tb = new TextBox();
//				ret.putWidget(keyName, tb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					if (!str.isEmpty())
//						ret.putValue(keyName, str);
//					tb.setValue(str);
//				}
//				if (editable != null)
//					tb.setEnabled(false);
//				pan.add(tb);
//				
//				tb.addValueChangeHandler(new ValueChangeHandler<String>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<String> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						ret.putValue(keyName, event.getValue());
//					}
//					
//				});
//			} else if (typ.equals("boolean")) {
//				CheckBox cb = new CheckBox();
//				checkBoxes.add(cb);
//				ret.putWidget(keyName, cb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					
//					if (str.equals("true")) {
//						ret.putValue(keyName, Boolean.TRUE);
//						cb.setValue(Boolean.TRUE);
//					} else {
//						ret.putValue(keyName, Boolean.FALSE);
//						cb.setValue(Boolean.FALSE);
//					}
//				}
//				if (editable != null)
//					cb.setEnabled(false);
//				pan.add(cb);
//				
//				cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Boolean> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						ret.putValue(keyName, event.getValue());
//
//						boolean setTo = event.getValue();
//						if (invert != null)
//							setTo = !setTo;
//						if (invert != null || enable != null) {
//							Node nod = (invert != null)? invert : enable;
//							// find all items, enable / disable them
//							List<String> items = getTextGrandchildren(nod, "item");
//							
//							for (String item : items) {
//								FocusWidget w = ret.getWidget(item);
//								if (w != null)
//									w.setEnabled(setTo);
//							}
//						}
//					}
//					
//				});
//			} else if (typ.equals("int")) {
////				TextBox tb = new NumericTextBox();
//				final IntegerBox tb = new IntegerBox();
//				ret.putWidget(keyName, tb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Integer value = Integer.valueOf(str);
//							ret.putValue(keyName, value);
//							tb.setValue(value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (min != null) {
//					String str = getTextChild(min);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Integer value = Integer.valueOf(str);
//							ret.setMin(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (max != null) {
//					String str = getTextChild(max);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Integer value = Integer.valueOf(str);
//							ret.setMax(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (editable != null)
//					tb.setEnabled(false);
//				pan.add(tb);
//				tb.addValueChangeHandler(new ValueChangeHandler<Integer>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Integer> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						Integer min = (Integer)ret.getMin(keyName);
//						Integer max = (Integer)ret.getMax(keyName);
//						
//						if (min != null && tb.getValue() < min) {
//							Dialogs.messageBox("Illegal value", "Cannot have value less than " + min);
//							tb.setText(String.valueOf(min));
//							ret.putValue(keyName, event.getValue());
//						} else if (max != null && tb.getValue() > max) {
//							Dialogs.messageBox("Illegal value", "Cannot have value greater than " + max);
//							tb.setText(String.valueOf(max));
//							ret.putValue(keyName, event.getValue());
//						} else
//						try {
//							ret.putValue(keyName, event.getValue());
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					
//				});
//			} else if (typ.equals("long")) {
//				final LongBox tb = new LongBox();
//				ret.putWidget(keyName, tb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Long value = Long.valueOf(str);
//							ret.putValue(keyName, value);
//							tb.setValue(value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (min != null) {
//					String str = getTextChild(min);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Long value = Long.valueOf(str);
//							ret.setMin(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (max != null) {
//					String str = getTextChild(max);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Long value = Long.valueOf(str);
//							ret.setMax(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (editable != null)
//					tb.setEnabled(false);
//				pan.add(tb);
//				tb.addValueChangeHandler(new ValueChangeHandler<Long>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Long> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						Long min = (Long)ret.getMin(keyName);
//						Long max = (Long)ret.getMax(keyName);
//						
//						if (min != null && tb.getValue() < min) {
//							Dialogs.messageBox("Illegal value", "Cannot have value less than " + min);
//							tb.setText(String.valueOf(min));
//							ret.putValue(keyName, event.getValue());
//						} else if (max != null && tb.getValue() > max) {
//							Dialogs.messageBox("Illegal value", "Cannot have value greater than " + max);
//							tb.setText(String.valueOf(max));
//							ret.putValue(keyName, event.getValue());
//						} else
//						try {
//							ret.putValue(keyName, event.getValue());
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					
//				});
//			} else if (typ.equals("double")) {
//				final DoubleBox tb = new DoubleBox();
//				ret.putWidget(keyName, tb);
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Double value = Double.valueOf(str);
//							ret.putValue(keyName, value);
//							tb.setValue(value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (min != null) {
//					String str = getTextChild(min);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Double value = Double.valueOf(str);
//							ret.setMin(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (max != null) {
//					String str = getTextChild(max);
//					str = expandMacros(str, cx, traces, filter);
//					try {
//						if (!str.isEmpty()) {
//							Double value = Double.valueOf(str);
//							ret.setMax(keyName, value);
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				if (editable != null)
//					tb.setEnabled(false);
//				pan.add(tb);
//				tb.addValueChangeHandler(new ValueChangeHandler<Double>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Double> event) {
//						System.out.println("Putting " + keyName + ":" + event.getValue());
//						Double min = (Double)ret.getMin(keyName);
//						Double max = (Double)ret.getMax(keyName);
//						
//						if (min != null && tb.getValue() < min) {
//							Dialogs.messageBox("Illegal value", "Cannot have value less than " + min);
//						} else if (max != null && tb.getValue() > max) {
//							Dialogs.messageBox("Illegal value", "Cannot have value greater than " + max);
//						} else
//						try {
//							ret.putValue(keyName, event.getValue());
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//					
//				});
//			} else if (items != null && items.length > 0) {
//				boolean val = false;
//				if (typ.charAt(0) == '[')
//					val = true;
//				final ListBox lb = new ListBox(val);
//				for (int x = 0; x < items.length; x++)
//					lb.addItem(items[x]);
//				if (editable != null)
//					lb.setEnabled(false);
//				pan.add(lb);
//				ret.putWidget(keyName, lb);
//				
//				if (def != null) {
//					String str = getTextChild(def);
//					str = expandMacros(str, cx, traces, filter);
//					List<String> selected = new ArrayList<String>();
//					if (!str.isEmpty() && str.charAt(0) != '(' && str.charAt(0) != '{' && str.charAt(0) != '[') {
//						selected.add(str);
//					} else if (!str.isEmpty()) {
//						String[] str2 = getList(str);
//						for (int x = 0; x < str2.length; x++)
//							selected.add(str2[x]);
//					}
//					ret.putValue(keyName, selected);
//					for (int x = 0; x < items.length; x++) {
//						if (str.charAt(0) != '(' && str.charAt(0) != '{' && str.charAt(0) != '[') {
//							if (lb.getItemText(x).equals(str))
//								lb.setSelectedIndex(x);
//						} else {
//							if (selected.contains(lb.getItemText(x)))
//								lb.setItemSelected(x, true);
//						}
//					}
//				}
//				lb.addChangeHandler(new ChangeHandler() {
//					@Override
//					public void onChange(ChangeEvent event) {
//						List<String> selected = new ArrayList<String>();
//						for (int x = 0; x < lb.getItemCount(); x++) {
//							if (lb.isItemSelected(x))
//								selected.add(lb.getItemText(x));
//						}
//						ret.putValue(keyName, selected);
//					}
//					
//				});
//			}
//		}
		//Fire checkbox events so that inverts and enableds are correct
		for (final CheckBox cb : checkBoxes) {
			ValueChangeEvent.fire(cb, cb.getValue());
		}
		HorizontalPanel hp = new HorizontalPanel();
		vp.add(hp);
		
		Button ok = new Button("OK");
		
		ok.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				ret.hide();
				
				handler.onClick(ret, event);
			}
			
		});
		
		Button cancel = new Button("Cancel");
		hp.add(ok);
		hp.add(cancel);
		cancel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				ret.hide();
			}
			
		});
		ret.center();
		ret.show();
	}
	
	public static String getTextChild(final Node n) {
		for (Node c = n.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c.getNodeType() == Node.TEXT_NODE)
				return c.getNodeValue();
		}
		return "";
	}
	
	public static Node getChildElement(final Node n, final String name) {
		for (Node c = n.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c.getNodeType() == Node.ELEMENT_NODE && c.getNodeName().equals(name))
				return c;
		}
		return null;
	}
	
	public static List<String> getTextGrandchildren(final Node n, final String name) {
		List<String> matches = new ArrayList<String>();
		for (Node c = n.getFirstChild(); c != null; c = c.getNextSibling()) {
			if (c.getNodeType() == Node.ELEMENT_NODE && c.getNodeName().equals(name))
				matches.add(getTextChild(c));
		}
		return matches;
	}
	
	public static String expandMacros(String str, ContextSupplier cx, 
			List<TraceInfo> traces, DisplayConfiguration defFilter) {
		if (str.equals("$CHANNELS")) {
			//return cx.getChannelNames().toString();
			List<String> cNames = new ArrayList<String>(traces.size());
			for (TraceInfo ti: traces)
				cNames.add(ti.getLabel());
			return cNames.toString();
		} else if (str.equals("$CHANNELIDS")) {
			List<String> cIDs = new ArrayList<String>(traces.size());
//			return cx.getChannelRevIDs().toString();
			for (TraceInfo ti: traces)
				cIDs.add(ti.getRevId());
			return cIDs.toString();
		} else if (str.equals("$WIDTH"))
			return String.valueOf(cx.getWidth());
		else if (str.equals("$OFFSET"))
			return String.valueOf(cx.getStartOffset());
		else if (str.equals("$START"))
			return String.valueOf(cx.getSnapshotStart());
		else if (str.equals("$LENGTH"))
			return String.valueOf(cx.getSnapshotLength());
//		else if (str.equals("$USER"))
//			return cx.getUserId();
		else if (str.equals("$SAMPLE_FREQUENCY"))
			return String.valueOf(Math.round(cx.getFrequency()));
		else if (str.equals("$NYQUIST_FREQUENCY"))
			return String.valueOf((int)cx.getFrequency() / 2);
		else if (str.equals("$FILTER_TYPE")) {
			if (defFilter.includesFilterType(DisplayConfiguration.BANDPASS_FILTER))
				return "Bandpass";
			else if (defFilter.includesFilterType(DisplayConfiguration.LOWPASS_FILTER))
				return "Lowpass";
			else if (defFilter.includesFilterType(DisplayConfiguration.HIGHPASS_FILTER))
				return "Highpass";
			else
				return "None";
		} else
			return str;
	}
	
	public static String[] getList(String str) {
		char start = str.charAt(0);
		char end = 0;
		
		switch (start) {
		case '(':
			end = ')';
			break;
		case '{':
			end = '}';
			break;
		case '[':
			end = ']';
			break;
		}
		
		// Trim the collection indicators
		String body = str.substring(1);
		if (body.charAt(body.length() - 1) == end)
			body = body.substring(0, body.length() - 1);
		
		String[] tokens = body.split("[ ,]+");
		// To do: parse
		
		return tokens;
	}
}
