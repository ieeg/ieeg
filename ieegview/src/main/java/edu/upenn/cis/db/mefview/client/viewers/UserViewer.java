/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import static com.google.common.collect.Lists.newArrayList;

import java.util.Collection;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class UserViewer extends TableViewer<UserInfo> {
	boolean favorites;
	boolean selectable;
	boolean subset;
	
	public UserViewer(
			Collection<UserInfo> users, int pageSize, boolean favorites, boolean selectable,
			boolean subset) {
		super(users, pageSize);
		this.favorites = favorites;
		this.selectable = selectable;
		this.subset = subset;
		
		initialize();
	}
	
	public UserViewer(
			int pageSize, boolean favorites, boolean selectable, boolean subset) {
		super(pageSize);
		this.favorites = favorites;
		this.selectable = selectable;
		this.subset = subset;
		
		initialize();
	}
	
	@Override
	protected void init() {
		clear();

		List<String> userColNames = newArrayList();
		List<TextColumn<UserInfo>> userColumns = newArrayList();

		userColNames.add("Name");
		userColumns.add(LASTNAME_COL);
//		userColNames.add("First name");
//		userColumns.add(FIRSTNAME_COL);
		if (!subset) {
			userColNames.add("Institution");
			userColumns.add(INSTITUTION_COL);
		}
		if (selectable) {
			table = new PagedTable<UserInfo>(
					pageSize,
					true,
					true,
					values,
					USER_KEY_PROVIDER,
					userColNames,
					userColumns);
		} else {
			table = new PagedTable<UserInfo>(
					pageSize,
					false,
					false,
					values,
					USER_KEY_PROVIDER,
					userColNames,
					userColumns);
		}

		add(table);

	}
	
	public static final ProvidesKey<UserInfo> USER_KEY_PROVIDER = 
			new ProvidesKey<UserInfo>() {

		public Object getKey(UserInfo item) {
			return (item == null) ? null : item.getName();
		}

	};

//	public static final TextColumn<UserInfo> FIRSTNAME_COL = new TextColumn<UserInfo>() {
//		@Override
//		public String getValue(UserInfo object) {
//			return (object == null) ? null : object.getFields().get("firstname");
//		}
//	};

	public static final TextColumn<UserInfo> LASTNAME_COL = new TextColumn<UserInfo>() {
		@Override
		public String getValue(UserInfo object) {
			return (object == null) ? null : 
				((object.getProfile().getLastName().isPresent()) ? 
						object.getProfile().getFirstName().orNull() + " " + object.getProfile().getLastName().get():
						object.getName());
		}
	};

	public static final TextColumn<UserInfo> INSTITUTION_COL = new TextColumn<UserInfo>() {
		@Override
		public String getValue(UserInfo object) {
			return (object == null) ? null : object.getProfile().getInstitutionName().orNull();
		}
	};

}
