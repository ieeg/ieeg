/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import com.google.gwt.user.client.Timer;

import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

/**
 * Polling manager that periodically takes events out of the periodic queue,
 * then processes them.
 * 
 * @author zives
 *
 */
public class PeriodicEvents {
	Timer pending;
	
	AstroPanel foregroundWindow;
	ServerAccess prefetcher;
	
	public static int DELAY = 150;
	
	public boolean doRequests = true;

	public PeriodicEvents(ServerAccess fetcher) {
		prefetcher = fetcher;
		
		pending = new Timer() {

			@Override
			public void run() {
				if (foregroundWindow != null)
					foregroundWindow.refresh();
				
				if (doRequests)
					prefetcher.checkPendingRequests();

				doRequests = !doRequests;
			}
			
		};
		
		pending.scheduleRepeating(DELAY);
		
	}
	
	public void ensureScreenRefreshed() {
		if (foregroundWindow != null) {
			foregroundWindow.refresh();
		}
	}
	
	public void stopTimer() {
		pending.cancel();
	}
	
	public void resumeTimer() {
		pending.scheduleRepeating(DELAY);
	}
	
	public void restartTimer() {
		stopTimer();
		resumeTimer();
	}
	
	public void setForegroundWindow(AstroPanel window) {
		foregroundWindow = window;
	}
}
