/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.texteditor;

import java.util.Collection;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class TextEditorPresenter extends BasicPresenter implements ITextEditorPresenter {
	AppModel control;

	DataSnapshotModel model;
	
	public interface Display extends BasicPresenter.Display {
		public void init(boolean readonly);
		
		public String getContent();
		public void setFileContent(String content);
		public void setTitle(String title);
		
		public void setSaveHandler(ClickHandler handler);

		public void disableSave();
		
		public FileInfo getFile();
	}
	
	public final static String NAME = "text-edit";
	public final static Set<String> TYPES = Sets.newHashSet(BuiltinDataTypes.JAVA,
			BuiltinDataTypes.JS, BuiltinDataTypes.TEXT, BuiltinDataTypes.MATLAB);
	final static TextEditorPresenter seed = new TextEditorPresenter();
	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public TextEditorPresenter() {
		super(null, NAME, TYPES);
	}

	public TextEditorPresenter( 
			final DataSnapshotModel model, 
			final AppModel control, 
			final ClientFactory clientFactory
			) {

		super(clientFactory, NAME, TYPES);

		this.control = control;

		this.model = model;
	  }

	@Override
	public TextEditorPresenter.Display getDisplay() {
		return (TextEditorPresenter.Display)display;
	}
	
	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		display.addPresenter(this);
		
		getDisplay().setSaveHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				getClientFactory().getSnapshotServices().saveContents(
						getSessionModel().getSessionID(), 
						(model != null) ? model.getSnapshotID() : null,
						getDisplay().getFile(), getDisplay().getContent(), 
						new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Error saving file");
							}

							@Override
							public void onSuccess(Void result) {
								Window.alert("File saved");
							}
							
						});
			}
			
		});
		//Everything readonly for now
		getDisplay().init(true);
		getDisplay().bindDomEvents();
	}
	
	DataSnapshotModel getDataSnapshotState() {
		return model;
	}

	@Override
	public Presenter create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews snapController,
			PresentableMetadata file, boolean writePermissions) {
		return new TextEditorPresenter((snapController == null) ? null : snapController.getState(), 
				controller, clientFactory);
	}

	@Override
	public void loadContent(final FileInfo file) {
		getClientFactory().getSnapshotServices().getContents(
				getSessionModel().getSessionID(),
				(model == null) ? null : model.getSnapshotID(),
				file, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Error fetching "  + file.getDetails() + ": " + caught.getMessage());
						
						getDisplay().setTitle(file.getFriendlyName());

						getDisplay().disableSave();
						getDisplay().setFileContent("Unable to load file");
					}

					@Override
					public void onSuccess(String result) {
						getDisplay().setTitle(//file.getFriendly() 
								//+ "/" 
								//+ 
								file.getFriendlyName());
						getDisplay().disableSave();
						getDisplay().setFileContent(result);
					}
					
				});
	}

}
