/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

public class MetadataViewer extends TableViewer<PresentableMetadata> {
	boolean favorites;
	boolean selectable;
	boolean subset;
	boolean readOnly;
	
	public static final ProvidesKey<PresentableMetadata> KEY_PROVIDER = new ProvidesKey<PresentableMetadata>() {

		public Object getKey(PresentableMetadata item) {
			return (item == null) ? null : item.getId();// .getFriendlyName();
		}

	};

	public static final TextColumn<PresentableMetadata> nameColumn = new TextColumn<PresentableMetadata>() {
		@Override
		public String getValue(PresentableMetadata object) {
			return object.getFriendlyName();
		}
	};
	
	public static final Column<PresentableMetadata, Date> creationTimeColumn = new Column<PresentableMetadata, Date>(new DateCell(DateTimeFormat.getFormat(PredefinedFormat.DATE_LONG))) {
		@Override
		public Date getValue(PresentableMetadata object) {
			return object.getCreationTime();
		}
	};
//	public static final TextColumn<SearchResult> baseColumn = new TextColumn<SearchResult>() {
//		@Override
//		public String getValue(SearchResult object) {
//			// if (object.getBaseRevId() != null)
//			// return
//			// MainSnapshotsWindow.this.parent.getStudyFriendlyName(object.getBaseRevId());
//			// else
//			// return object.getFriendlyName();
//			if (object.getBaseFriendly() != null)
//				return object.getBaseFriendly();
//			else
//				return ("-");
//		}
//	};
//	public static final TextColumn<SearchResult> studySeries = new TextColumn<SearchResult>() {
//		@Override
//		public String getValue(SearchResult object) {
//			return String.valueOf(object.getDatasetRevId());
//		}
//	};
//	public static final TextColumn<SearchResult> studyChannels = new TextColumn<SearchResult>() {
//		@Override
//		public String getValue(SearchResult object) {
//			if (object.getBaseRevId() == null)
//				return String.valueOf(object.getTimeSeriesRevIds().size());
//			else
//				return "-";
//		}
//	};
//	public static final TextColumn<SearchResult> studyImages = new TextColumn<SearchResult>() {
//		@Override
//		public String getValue(SearchResult object) {
//			return String.valueOf(object.getImageCount());
//		}
//	};
//	public static final TextColumn<SearchResult> studyAnnotations = new TextColumn<SearchResult>() {
//		@Override
//		public String getValue(SearchResult object) {
//			if (object.getBaseRevId() == null)
//				return String.valueOf(object.getAnnotationCount());
//			else
//				return "-";
//		}
//	};
//
	
	
	public MetadataViewer(
			Collection<PresentableMetadata> users, int pageSize, boolean favorites, 
			boolean selectable, boolean subset) {
		super(users, pageSize);

		this.selectable = selectable;
		this.favorites = favorites;
		this.subset = subset;
		this.readOnly = false;
		initialize();
	}
	
	public MetadataViewer(ClientFactory clientFactory,
			int pageSize, boolean favorites, boolean selectable, boolean subset) {
		super(pageSize);

		this.selectable = selectable;
		this.favorites = favorites;
		this.subset = subset;
		this.readOnly = false;
		
		initialize();
	}
	
	public MetadataViewer(ClientFactory clientFactory,
	    int pageSize, boolean favorites, boolean selectable, boolean subset, boolean readOnly) {
	  super(pageSize);

	  this.selectable = selectable;
	  this.favorites = favorites;
	  this.subset = subset;
	  this.readOnly = readOnly;

	  initialize();
	}
	
	@Override
	protected void init() {
		clear();

//		Label l = new Label("Data snapshots:"); 
//		add(l);
//		setWidgetLeftRight(l, 5, Unit.PX, 5, Unit.PX);
//		setWidgetTopHeight(l, 5, Unit.PX, 35, Unit.PX);
		
		List<String> studyColNames = new ArrayList<String>();
		List<Column<PresentableMetadata, ?>> studyColumns = new ArrayList<>();
		
		studyColNames.add("Name");
		studyColumns.add(nameColumn);
		studyColNames.add("Creation Date");
		studyColumns.add(creationTimeColumn);
//		studyColNames.add("Channels");
//		studyColumns.add(SearchResult.studyChannels);
//		if (!subset) {
//			studyColNames.add("Images");
//			studyColumns.add(SearchResult.studyImages);
//			studyColNames.add("Annotations");
//			studyColumns.add(SearchResult.studyAnnotations);
//		}
//		studyColNames.add("Analyzed Snapshot");
//		studyColumns.add(SearchResult.baseColumn);

		if (favorites) {
		  GWT.log("Adding favorites");
		  table = new PagedTable<PresentableMetadata>(pageSize, false, false,true, 
		      new ArrayList<PresentableMetadata>(), KEY_PROVIDER, studyColNames, 
		      studyColumns, true, ResourceFactory.getStarSel(), ResourceFactory.getStarUnsel());
		} else if (selectable) {
		  GWT.log("Adding multi-selectable");
		  table = new PagedTable<PresentableMetadata>(pageSize, true, true, true, 
		      new ArrayList<PresentableMetadata>(), KEY_PROVIDER, studyColNames, 
		      studyColumns, false, null, null);
		} else if (readOnly){
		  GWT.log("Adding ReadOnly searchResultViewer");
		  table = new PagedTable<PresentableMetadata>(pageSize, false, false, false, 
		      new ArrayList<PresentableMetadata>(), KEY_PROVIDER, studyColNames, 
		      studyColumns, false, null, null);

		} else {
		  GWT.log("Adding no checkbox but selectable");
		  table = new PagedTable<PresentableMetadata>(pageSize, false, false,false, 
		    new ArrayList<PresentableMetadata>(), KEY_PROVIDER, studyColNames, 
		    studyColumns, false, null, null);
	
	    }
		
		nameColumn.setSortable(true);
		creationTimeColumn.setSortable(true);
//		SearchResult.baseColumn.setSortable(true);
		
	    table.addColumnSort(nameColumn,
		        new Comparator<PresentableMetadata>() {
	          public int compare(PresentableMetadata o1, PresentableMetadata o2) {
	            if (o1 == o2) {
	              return 0;
	            }

	            // Compare the name columns.
	            if (o1 != null) {
	              return (o2 != null) ? o1.getFriendlyName().compareTo(o2.getFriendlyName()) : 1;
	            }
	            return -1;
	          }
	        });
	    table.addColumnSort(creationTimeColumn,
		        new Comparator<PresentableMetadata>() {
	          public int compare(PresentableMetadata o1, PresentableMetadata o2) {
	            if (o1 == o2) {
	              return 0;
	            }

	            // Compare the name columns.
	            if (o1 != null) {
	              return (o2 != null) ? o1.getCreationTime().compareTo(o2.getCreationTime()) : 1;
	            }
	            return -1;
	          }
	        });
	    
//		if (!subset) {
//			table.setColumnWidth(SearchResult.studyChannels, "80px");
//			table.setColumnWidth(SearchResult.studyImages, "70px");
//			table.setColumnWidth(SearchResult.studyAnnotations, "90px");
//		}
	    
	    //table.getColumnSortList().push(creationTimeColumn);
	    table.getColumnSortList().push(MetadataViewer.nameColumn);
	    
		add(table);
		setWidgetLeftRight(table, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(table, 2, Unit.PX, 2, Unit.PX);
	}

	public void refresh() {
		table.getDataProvider().refresh();
		table.redraw();
	}
}
