  /*
   * Copyright 2014 Trustees of the University of Pennsylvania
   *
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *
   * http://www.apache.org/licenses/LICENSE-2.0
   *
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   */

package edu.upenn.cis.db.mefview.client.viewers;

import java.util.Map;

import org.gwt.advanced.client.datamodel.ComboBoxDataModel;
import org.gwt.advanced.client.ui.widget.ComboBox;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.AnnotationRefreshEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationSchemeCreatedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationSchemeEditedEvent;
import edu.upenn.cis.db.mefview.client.events.AnnotationViewRefreshEvent;
import edu.upenn.cis.db.mefview.client.models.AnnotationGroup;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs.CreateAnnotationLayerScheme;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.AnnotationScheme;

public class AnnotationSchemePane extends LayoutPanel
implements AnnotationSchemeCreatedEvent.Handler{

    SessionModel sessionModel;
    TextBox schemeColor = new TextBox();
    TextBox schemeName = new TextBox();
    
    AnnotationScheme curScheme; // Scheme for which properties are displayed.
    Button remSchemeButton = new Button("Remove selected Style");
    Button createNew = new Button("New Style");
    ComboBoxDataModel annSchemes;
    DataSnapshotModel currentModel;
    ComboBox<ComboBoxDataModel> schemeSelect;
    Map<String, AnnotationScheme> schemeSet;
    


   public AnnotationSchemePane(final SessionModel sessionModel, final DataSnapshotModel theModel) {
      this.currentModel = theModel;
      this.sessionModel = sessionModel;
      
      IeegEventBusFactory.getGlobalEventBus().registerHandler(AnnotationSchemeCreatedEvent.getType(), this);
      
      this.setStylePrimaryName("gwt-StackLayoutPanel gwt-StackLayoutPanelContent-managePanel");

      // Create list of schemes
      schemeSet = sessionModel.getUserPreferences().getAnnSchemes();      
      annSchemes = new ComboBoxDataModel();
      for(final String key : schemeSet.keySet()) {
        annSchemes.add(key, schemeSet.get(key).getName());
      }
      
      schemeSelect = new ComboBox<ComboBoxDataModel>();
      schemeSelect.setModel(annSchemes);//apply the model

      // Scheme Select Div
      VerticalPanel sp = new VerticalPanel();
      sp.add(schemeSelect);
      schemeSelect.setWidth("275px");
      schemeSelect.select(0);
      curScheme = schemeSet.get(schemeSelect.getSelected());
      if (curScheme != null){
        schemeColor.setText(curScheme.getColor());
        schemeName.setText(curScheme.getName());
      }

      // Property Div
      VerticalPanel gp = new VerticalPanel();      
      
      gp.add(new Label("Style Name:"));
      gp.add(schemeName);
      
      gp.add(new Label("Style Color:"));
      gp.add(schemeColor);
      
      
      add(sp);
      setWidgetTopHeight(sp, 5, Unit.PX, 50, Unit.PX);
      setWidgetLeftRight(sp, 5, Unit.PX, 5, Unit.PX);
      
      add(gp);
      setWidgetTopHeight(gp, 55, Unit.PX, 250, Unit.PX);
      setWidgetLeftRight(gp, 5, Unit.PX, 5, Unit.PX);
    
      // Button Div
      HorizontalPanel hp = new HorizontalPanel();
      hp.add(createNew);
      hp.add(remSchemeButton);

      add(hp);
      setWidgetBottomHeight(hp, 1, Unit.PX, 30, Unit.PX);
      setWidgetLeftRight(hp, 1, Unit.PX, 1, Unit.PX);

      
      // Handlers etc.
      createNew.addClickHandler(new ClickHandler() {
        
        @Override
        public void onClick(ClickEvent event){
          CreateAnnotationLayerScheme ca = new CreateAnnotationLayerScheme(theModel, sessionModel);
          ca.center();
          ca.show();
        }
      });
      
      schemeSelect.addChangeHandler(new ChangeHandler() {

        @Override
        public void onChange(ChangeEvent event) {

          AnnotationScheme selectedScheme = schemeSet.get(schemeSelect.getText()); 
          
          // Update scheme objects from previous selected item
          updateScheme();
          
          // Set property inputs in dialogbox
          curScheme = selectedScheme;
          schemeColor.setText(selectedScheme.getColor());
          schemeName.setText(selectedScheme.getName());
        }

      });
      
      remSchemeButton.addClickHandler(new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {

          AnnotationScheme selectedScheme = schemeSet.get(schemeSelect.getText()); 
          schemeSet.remove(schemeSelect.getText());
          annSchemes.remove(schemeSelect.getText());
          
          AnnotationGroup<Annotation> grp = currentModel.getAnnotationModel().getRootGroup();

          
          grp.replaceStyle(selectedScheme, grp.getScheme());
          
          schemeSelect.select(0);
          
          
          // Set property inputs in dialogbox
          selectedScheme = schemeSet.get(schemeSelect.getText()); 
          curScheme = selectedScheme;
          schemeColor.setText(selectedScheme.getColor());
          schemeName.setText(selectedScheme.getName());
          
          IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationViewRefreshEvent());
          
          
          
          
        }

      });
  
    };

  // Support methods
  public void updateScheme(){
    
    boolean isChanged = false;
    
    String oldColor = curScheme.getColor();
    String newColor = schemeColor.getText();
    
    if (!oldColor.equals(newColor)){
      curScheme.setColor(newColor);
      isChanged = true;
      IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationRefreshEvent(currentModel.getSnapshotID()));
    }
    
    String newName = schemeName.getText();
    String oldName = curScheme.getName();
    if (!newName.equals(oldName)){
            
      // Update Scheme-hash table in UserPrefs
    	sessionModel.getUserPreferences().renameAnnotationScheme(oldName, newName);
      
      // Update SchemeSet
      schemeSet = sessionModel.getUserPreferences().getAnnSchemes();
      isChanged = true;
    }
    
    if (isChanged)
    	IeegEventBusFactory.getGlobalEventBus().fireEvent(new AnnotationSchemeEditedEvent());
    
  }
  
  public void close() {
	  IeegEventBusFactory.getGlobalEventBus().unregisterHandler(this);
  }

  @Override
  public void onAnnotationSchemeCreatedEvent(DataSnapshotModel action,
      AnnotationGroup<Annotation> group) {
   
    schemeSet = sessionModel.getUserPreferences().getAnnSchemes();      
    for(final String key : schemeSet.keySet()) {
      if (annSchemes.get(key) == null)
        annSchemes.add(key, schemeSet.get(key).getName());
    }
    
    
  }
  
}
