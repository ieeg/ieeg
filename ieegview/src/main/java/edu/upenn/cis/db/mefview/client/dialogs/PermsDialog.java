/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import static com.google.common.collect.Iterables.getOnlyElement;

import java.util.Set;

import com.google.common.base.Optional;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ProvidesKey;

import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IPermsView;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.ClosedComplexDialogEvent;
import edu.upenn.cis.db.mefview.client.panels.AppPanel;

public class PermsDialog implements IPermsView {

	private static PermsDialogUiBinder uiBinder = GWT
			.create(PermsDialogUiBinder.class);

	interface PermsDialogUiBinder extends UiBinder<ComplexDialog, PermsDialog> {}

	@UiField
	Button cancelButton;

	private final ComplexDialog dialogBox;
	@UiField(provided = true)
	DataGrid<ExtUserAce> userAceGrid;

	@UiField(provided = true)
	DataGrid<ExtProjectAce> projectAceGrid;

	@UiField(provided = true)
	DataGrid<ExtWorldAce> worldAceGrid;
	@UiField
	VerticalPanel verticalPanel;
	@UiField
	Button saveButton;
	@UiField
	Button addUserButton;
	@UiField
	Button addProjectGroupButton;

	private ClientFactory clientFactory;

	private final IPermsPresenter presenter;
	private final TextColumn<ExtUserAce> userColumn = new TextColumn<ExtUserAce>() {

		@Override
		public String getValue(ExtUserAce object) {
			return object.getName();
		}
	};
	public final String USER_COL_NAME = "User";
	public final String DESCR_COL_NAME = "Description";

	private final TextColumn<ExtProjectAce> projectGroupColumn = new TextColumn<ExtProjectAce>() {

		@Override
		public String getValue(ExtProjectAce object) {
			return object.getProjectGroup().getProjectName()
					+ " "
					+ object.getProjectGroup().getType().toString()
							.toLowerCase();
		}
	};
	public final String PROJ_COL_NAME = "Project Group";

	public PermsDialog(IPermsPresenter presenter, ClientFactory factory) {
		this.presenter = presenter;
		this.clientFactory = factory;
		final boolean canEdit = this.presenter.userIsOwner();
		if (canEdit) {
			userAceGrid = initEditableUserAceGrid();
			projectAceGrid = initEditableProjectAceGrid();
			worldAceGrid = initEditableWorldAceGrid();
		} else {
			userAceGrid = initReadOnlyUserAceGrid();
			projectAceGrid = initReadOnlyProjectAceGrid();
			worldAceGrid = initReadOnlyWorldAceGrid();
		}

		this.presenter.setView(this);
		this.presenter.getUserAceProvider().addDataDisplay(userAceGrid);
		this.presenter.getProjectAceProvider().addDataDisplay(projectAceGrid);
		this.presenter.getWorldAceProvider().addDataDisplay(worldAceGrid);
		dialogBox = uiBinder.createAndBindUi(this);
		dialogBox
				.setText("Permissions for " + this.presenter.getFriendlyName());

		if (!canEdit) {
			cancelButton.setText("OK");
			hideButtons();
		}

		setHeights(Window.getClientHeight() - AppPanel.HEADER);
		setWidths(Window.getClientWidth() - AppPanel.LEFT_PANE);

	}

	@UiHandler({
		"cancelButton",
		"saveButton",
		"addUserButton",
		"addProjectGroupButton" })
	void onClick(ClickEvent e) {
		final Object source = e.getSource();
		if (source == cancelButton) {
			dialogBox.hide();
		} else if (source == saveButton) {
			presenter.doSave();
		} else if (source == addUserButton) {
			final UserListDialog userList = new UserListDialog(presenter);
			userList.centerAndShow();
		} else if (source == addProjectGroupButton) {
			final ProjectGroupListDialog pgList = new ProjectGroupListDialog(
					presenter);
			pgList.centerAndShow();
		}
	}

	public void centerAndShow() {
		dialogBox.setGlassEnabled(true);
		dialogBox.center();
		dialogBox.show();
	}

	private int getPageSize(int height) {
		int rowHeight = 36;
		// System.err.println("Row height = " + rowHeight);

		height -= 36 * 4;

		int pageSize = height / rowHeight;
		// System.err.println("Page size = " + pageSize);

		if (pageSize < 1) {
			pageSize = 1;
		}
		return pageSize;

	}

	private void hideButtons() {
		saveButton.setEnabled(false);
		saveButton.setVisible(false);
		addUserButton.setEnabled(false);
		addUserButton.setVisible(false);
		addProjectGroupButton.setEnabled(false);
		addProjectGroupButton.setVisible(false);
	}

	private DataGrid<ExtUserAce> initEditableUserAceGrid() {
		final DataGrid<ExtUserAce> userAceGrid = new DataGrid<ExtUserAce>(100,
				new ProvidesKey<ExtUserAce>() {

					@Override
					public Object getKey(ExtUserAce item) {
						return item.getAceId();
					}
				});
		userAceGrid.setEmptyTableWidget(new Label("No items to display."));
		// User/Group name Column
		userColumn.setSortable(false);
		userAceGrid.addColumn(userColumn, USER_COL_NAME);
		userAceGrid.setColumnWidth(userColumn, 7, Unit.EM);

		// Perms column
		final Column<ExtUserAce, String> permsColumn = new Column<ExtUserAce, String>(
				new SelectionCell(presenter.getUserPermsOptions())) {

			@Override
			public String getValue(ExtUserAce object) {
				final ExtPermission perm = getOnlyElement(object.getPerms());
				return presenter.userPermToString(perm);
			}
		};
		permsColumn.setSortable(false);
		final FieldUpdater<ExtUserAce, String> userPermsFieldUpdater = new FieldUpdater<ExtUserAce, String>() {

			@Override
			public void update(int index, ExtUserAce object, String value) {
				presenter.updateUserAce(object,
						presenter.stringToUserPerm(value));
			}
		};
		permsColumn.setFieldUpdater(userPermsFieldUpdater);
		userAceGrid.addColumn(permsColumn);
		userAceGrid.setColumnWidth(permsColumn, 2, Unit.EM);

		final ActionCell.Delegate<ExtUserAce> removeUserDelegate = new ActionCell.Delegate<ExtUserAce>() {

			@Override
			public void execute(ExtUserAce object) {
				presenter.removeUserAce(object);
			}
		};

		final Column<ExtUserAce, ExtUserAce> removeUserColumn = new Column<ExtUserAce, ExtUserAce>(
				new ActionCell<ExtUserAce>("Remove",
						removeUserDelegate)) {

			@Override
			public ExtUserAce getValue(ExtUserAce object) {
				return object;
			}
		};
		removeUserColumn.setSortable(false);
		removeUserColumn
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		userAceGrid.addColumn(removeUserColumn);
		userAceGrid.setColumnWidth(removeUserColumn,
				2, Style.Unit.EM);
		return userAceGrid;
	}

	private DataGrid<ExtProjectAce> initEditableProjectAceGrid() {
		final DataGrid<ExtProjectAce> projectAceGrid = new DataGrid<ExtProjectAce>(
				100,
				new ProvidesKey<ExtProjectAce>() {

					@Override
					public Object getKey(ExtProjectAce item) {
						return item.getAceId();
					}
				});
		projectAceGrid.setEmptyTableWidget(new Label("No items to display."));
		// User/Group name Column
		projectGroupColumn.setSortable(false);
		projectAceGrid.addColumn(projectGroupColumn, PROJ_COL_NAME);
		projectAceGrid.setColumnWidth(projectGroupColumn, 7, Unit.EM);

		// Perms column
		final Column<ExtProjectAce, String> permsColumn = new Column<ExtProjectAce, String>(
				new SelectionCell(presenter.getProjectPermsOptions())) {

			@Override
			public String getValue(ExtProjectAce object) {
				final ExtPermission perm = getOnlyElement(object.getPerms());
				return presenter.projectPermToString(perm);
			}
		};
		permsColumn.setSortable(false);
		final FieldUpdater<ExtProjectAce, String> projectPermsFieldUpdater = new FieldUpdater<ExtProjectAce, String>() {

			@Override
			public void update(int index, ExtProjectAce object, String value) {
				presenter.updateProjectAce(object,
						presenter.stringToProjectPerm(value));
			}
		};
		permsColumn.setFieldUpdater(projectPermsFieldUpdater);
		projectAceGrid.addColumn(permsColumn);
		projectAceGrid.setColumnWidth(permsColumn, 2, Unit.EM);

		final ActionCell.Delegate<ExtProjectAce> removeProjectGroupDelegate = new ActionCell.Delegate<ExtProjectAce>() {

			@Override
			public void execute(ExtProjectAce object) {
				presenter.removeProjectAce(object);
			}
		};

		final Column<ExtProjectAce, ExtProjectAce> removeProjectGroupColumn = new Column<ExtProjectAce, ExtProjectAce>(
				new ActionCell<ExtProjectAce>("Remove",
						removeProjectGroupDelegate)) {

			@Override
			public ExtProjectAce getValue(ExtProjectAce object) {
				return object;
			}
		};
		removeProjectGroupColumn.setSortable(false);
		removeProjectGroupColumn
				.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		projectAceGrid.addColumn(removeProjectGroupColumn);
		projectAceGrid.setColumnWidth(removeProjectGroupColumn,
				2, Style.Unit.EM);
		return projectAceGrid;
	}

	private DataGrid<ExtWorldAce> initEditableWorldAceGrid() {
		final DataGrid<ExtWorldAce> worldAceTable = new DataGrid<ExtWorldAce>(1);
		worldAceTable.setEmptyTableWidget(new Label("No items to display."));
		// world Column
		TextColumn<ExtWorldAce> labelColumn = new TextColumn<ExtWorldAce>() {

			@Override
			public String getValue(ExtWorldAce object) {
				return "Everyone else";
			}
		};
		labelColumn.setSortable(false);
		worldAceTable.addColumn(labelColumn, "");
		worldAceTable.setColumnWidth(labelColumn, 7, Unit.EM);

		// Perms column
		final Column<ExtWorldAce, String> permsColumn = new Column<ExtWorldAce, String>(
				new SelectionCell(presenter.getWorldPermsOptions())) {

			@Override
			public String getValue(ExtWorldAce object) {
				final Set<ExtPermission> permSet = object.getPerms();
				Optional<ExtPermission> worldPerm;
				if (permSet.isEmpty()) {
					worldPerm = Optional.absent();
				} else {
					worldPerm = Optional.of(getOnlyElement(permSet));
				}
				return presenter.worldPermToString(worldPerm);
			}
		};
		permsColumn.setSortable(false);
		final FieldUpdater<ExtWorldAce, String> worldPermsFieldUpdater = new FieldUpdater<ExtWorldAce, String>() {

			@Override
			public void update(int index, ExtWorldAce object, String value) {
				presenter.updateWorldAce(object,
						presenter.stringToWorldPerm(value));
			}
		};

		permsColumn.setFieldUpdater(worldPermsFieldUpdater);
		worldAceTable.addColumn(permsColumn);
		worldAceTable.setColumnWidth(permsColumn, 2, Unit.EM);
		final TextColumn<ExtWorldAce> paddingColumn = new TextColumn<ExtWorldAce>() {

			@Override
			public String getValue(ExtWorldAce object) {
				return "";
			}
		};
		paddingColumn.setSortable(false);
		worldAceTable.addColumn(paddingColumn);
		worldAceTable.setColumnWidth(paddingColumn, 2, Unit.EM);
		return worldAceTable;
	}

	private DataGrid<ExtWorldAce> initReadOnlyWorldAceGrid() {
		final DataGrid<ExtWorldAce> worldAceTable = new DataGrid<ExtWorldAce>(1);
		worldAceTable.setEmptyTableWidget(new Label("No items to display."));

		// world Column
		TextColumn<ExtWorldAce> labelColumn = new TextColumn<ExtWorldAce>() {

			@Override
			public String getValue(ExtWorldAce object) {
				return "Everyone else";
			}
		};
		labelColumn.setSortable(false);
		worldAceTable.addColumn(labelColumn, "");
		worldAceTable.setColumnWidth(labelColumn, 7, Unit.EM);

		// Perms column
		final TextColumn<ExtWorldAce> permsColumn = new TextColumn<ExtWorldAce>() {

			@Override
			public String getValue(ExtWorldAce object) {
				final Set<ExtPermission> permSet = object.getPerms();
				if (permSet.isEmpty()) {
					return presenter.worldPermToString(Optional
							.<ExtPermission> absent());
				} else {
					final ExtPermission worldPerm = getOnlyElement(permSet);
					return presenter.worldPermToString(Optional.of(worldPerm));
				}
			}
		};
		permsColumn.setSortable(false);
		worldAceTable.addColumn(permsColumn);
		worldAceTable.setColumnWidth(permsColumn, 2, Unit.EM);

		return worldAceTable;
	}

	private DataGrid<ExtUserAce> initReadOnlyUserAceGrid() {
		final DataGrid<ExtUserAce> userAceGrid = new DataGrid<ExtUserAce>(
				getPageSize(Window.getClientHeight() - AppPanel.HEADER));
		userAceGrid.setEmptyTableWidget(new Label("No items to display."));
		// aclGrid.setMinimumTableWidth(80, Unit.EM);

		// User/Group name column
		userColumn.setSortable(false);
		userAceGrid.addColumn(userColumn, USER_COL_NAME);
		userAceGrid.setColumnWidth(userColumn, 7, Unit.EM);

		// Read Perms column
		final TextColumn<ExtUserAce> readColumn = new TextColumn<ExtUserAce>() {

			@Override
			public String getValue(ExtUserAce object) {
				final ExtPermission userPerm = getOnlyElement(object.getPerms());
				return presenter.userPermToString(userPerm);
			}
		};
		readColumn.setSortable(false);
		userAceGrid.addColumn(readColumn);
		userAceGrid.setColumnWidth(readColumn, 2, Unit.EM);

		return userAceGrid;
	}

	private DataGrid<ExtProjectAce> initReadOnlyProjectAceGrid() {
		final DataGrid<ExtProjectAce> projectAceGrid = new DataGrid<ExtProjectAce>(
				getPageSize(Window.getClientHeight() - AppPanel.HEADER));
		projectAceGrid.setEmptyTableWidget(new Label("No items to display."));
		// aclGrid.setMinimumTableWidth(80, Unit.EM);

		// User/Group name column
		projectGroupColumn.setSortable(false);
		projectAceGrid.addColumn(projectGroupColumn, PROJ_COL_NAME);
		projectAceGrid.setColumnWidth(projectGroupColumn, 7, Unit.EM);

		// Read Perms column
		final TextColumn<ExtProjectAce> readColumn = new TextColumn<ExtProjectAce>() {

			@Override
			public String getValue(ExtProjectAce object) {
				final ExtPermission groupPerm = getOnlyElement(object
						.getPerms());
				return presenter.projectPermToString(groupPerm);
			}
		};
		readColumn.setSortable(false);
		projectAceGrid.addColumn(readColumn);
		projectAceGrid.setColumnWidth(readColumn, 2, Unit.EM);

		return projectAceGrid;
	}

	public void setHeights(int height) {
		int rowHeight = 36;
		height -= 36 * 4;

		int pageSize = getPageSize(height);
		if (pageSize < 1) {
			pageSize = 1;
		}
		verticalPanel.setHeight((int) (pageSize * rowHeight * 2 / 3) + "px");
	}

	public void setWidths(int width) {
		verticalPanel.setWidth((int) (width * 0.5) + "px");
	}

	@Override
	public void closeDialog() {
		dialogBox.hide();
	}

	public DialogBox getDialogBox() {
		return dialogBox;
	}
}
