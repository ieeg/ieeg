/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.events.ClosedComplexDialogEvent;
import edu.upenn.cis.db.mefview.client.events.OpenSignedUrlEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenSignedUrl;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.EEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.IEEGViewerPanel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.EEGPane;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class DownloadTracesDialog extends ComplexDialog {
	PagedTable<INamedTimeSegment > traceTable;
	ListDataProvider<INamedTimeSegment > traceProvider = new ListDataProvider<INamedTimeSegment >();
//	ScrollPanel traceScroll;
	
	TextBox startTime = new TextBox();
	TextBox endTime = new TextBox();
	DoubleBox frequency = new DoubleBox();
	
	long start;
	long duration;
	double freq;
	String filterString = "";
	
	public DownloadTracesDialog(//final AppController main,
			final ClientFactory factory,
//			final Collection<TraceInfo> traces,
			final DataSnapshotModel model,
			final String studyId,
			final IEEGViewerPanel selectedEeg, 
			final boolean isCsv) {
		FocusPanel fpd = new FocusPanel();
		setTitle("Download Data - " + model.getFriendlyName() + " - " + (!isCsv ? " JSON" : " CSV"));
		setText("Download Data - " + model.getFriendlyName() + " - " + (!isCsv ? " JSON" : " CSV"));

		setHeight("600px");
		
		
		VerticalPanel content = new VerticalPanel();
		
		if (model.getTraceInfo().isEmpty())
			return;
		
		content.add(new HTML("<b>Settings for time spans and coordinates are<br/>based on those in the viewer</b>"));
		
		List<String> traceColNames = new ArrayList<String>();
		List<TextColumn<INamedTimeSegment>> traceColumns = new ArrayList<TextColumn<INamedTimeSegment>>();
		
		traceColNames.add("Channel");
		traceColumns.add(TraceInfo.traceName);
		traceColNames.add("Comments");
		traceColumns.add(TraceInfo.traceComments);
//		traceScroll = new ScrollPanel();
		final ArrayList<INamedTimeSegment> traceList = new ArrayList<INamedTimeSegment>();
		traceTable = new PagedTable<INamedTimeSegment>(20, true, false, true,traceList, TraceInfo.KEY_PROVIDER, traceColNames, traceColumns, false, null, null);
		traceTable.getList().addAll(model.getTraceInfo());//traces);
		traceTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);//.BOUND_TO_SELECTION);
		traceTable.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		traceTable.setRowCount(traceTable.getList().size());
		traceTable.setPageSize(traceTable.getList().size());

		traceTable.setWidth("400px");
		traceTable.setHeight("300px");

		VerticalPanel traceVPanel = new VerticalPanel();
//		traceScroll.add(traceVPanel);
		traceVPanel.add(traceTable);
		

//		channelList = new ListBox(true);
//		for (String s: sources)
//		for (TraceInfo ti: traces)
//			channelList.addItem(ti.getLabel(), ti.getRevId());
		
		for (INamedTimeSegment ti: traceTable.getList())
			if (model.getTraceInfo().contains(ti))
				traceTable.getSelectionModel().setSelected(ti, true);
		
		VerticalPanel fromPane = new VerticalPanel();
		fromPane.add(new Label("Channels:"));
//		fromPane.add(channelList);
		fromPane.add(traceVPanel);//Scroll);
		content.add(fromPane);
		content.add(new Label("Start time: "));
		content.add(startTime);
		content.add(new Label("Duration (usec): "));
		content.add(endTime);
		
		HorizontalPanel hp = new HorizontalPanel();
		VerticalPanel rawP = new VerticalPanel();
		
		RadioButton ds = new RadioButton("raw", "Downsampled"); 
		rawP.add(ds);
		RadioButton raw = new RadioButton("raw", "RAW"); 
		rawP.add(raw);
		
		VerticalPanel freqP = new VerticalPanel();
		hp.add(rawP);
		hp.add(freqP);
		
		ds.setValue(true);

		ds.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue())
					frequency.setEnabled(true);
			}
			
		});
		
		raw.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if (event.getValue())
					frequency.setEnabled(false);
			}
			
		});
		
		freqP.add(new Label("Frequency (Hz): "));
		freqP.add(frequency);
		content.add(hp);
		
		if (selectedEeg != null) {
			IEEGPane ep = selectedEeg.getEEGPane();
			
			double start = ep.getPosition();
			double duration = ep.getPageWidth();
			//double freq = 1.E6 / ep.getPeriod();
			
			double freq = traceList.get(0).getSampleRate();
			
			this.start = (long)start;
			this.duration = (long)duration;
			this.freq = freq;
			
//			startTime.setText(Long.toString((long)start));
//			endTime.setText(Long.toString((long)start + duration));
			startTime.setText(Long.toString((long)start));//EEGPaneData.getDateValue(start));
			endTime.setText(Long.toString((long)duration));
			frequency.setValue(Double.valueOf(freq));
			
			filterString = ep.getFilters().get(0).paramsToURL();

		} else
			filterString = new DisplayConfiguration().paramsToURL();
		
		startTime.setEnabled(false);
		endTime.setEnabled(false);
		
//		content.add(new Label("Filters: (none)"));
		
		HorizontalPanel buttons = new HorizontalPanel();
		Button done = new Button("Download");
		Button cancel = new Button("Cancel");
		done.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DownloadTracesDialog.this.hide();
				
				String channelList = "";
				boolean first = true;
				for (INamedTimeSegment t: traceList) {
					if (traceTable.getSelectionModel().isSelected(t)) {
						if (first)
							first = false;
						else 
							channelList = channelList + ",";
					
						channelList = channelList + t.getId();
					}
				}
				
				if (channelList.isEmpty()) {
					Dialogs.messageBox("No channels were selected", "Sorry, you must select at least one channel");
					return;
				}
				
				if (frequency.isEnabled()) {
					final Double requestedFreq = frequency.getValue();
					if (requestedFreq == null) {
						Dialogs.messageBox("Invalid frequency",
								"Sorry, you must enter a valid number for frequency");
						return;
					}
					if (!isCsv)
						openURL(
								getTitle(),
								"./services/timeseries/getTraceListJSON/" + studyId + ".json",
								studyId,
								"&list=" + channelList + "&start=" + Long.toString(start) + "&duration=" + Long.toString(duration) +
								"&frequency=" + requestedFreq + filterString);
					else
						openURL(
								getTitle(),
								"./services/timeseries/getTraceListCSV/" + studyId + ".csv",
								studyId,
								"&list=" + channelList + "&start=" + Long.toString(start) + "&duration=" + Long.toString(duration) +
								"&frequency=" + requestedFreq + filterString);
				} else {
					if (!isCsv)
						openURL(
								getTitle(),
								"./services/timeseries/getTraceListRawJSON/" + studyId + ".json",
								studyId,
								"&list=" + channelList + "&start=" + Long.toString(start) + "&duration=" + Long.toString(duration) +
						"&sampleFactor=1");
					else
						openURL(
								getTitle(),
								"./services/timeseries/getTraceListRawCSV/" + studyId + ".csv",
								studyId,
								"&list=" + channelList + "&start=" + Long.toString(start) + "&duration=" + Long.toString(duration) +
								"&sampleFactor=1");
				}
//				ControlPane.messageBox("Download Generated", "Please ensure your browser is not preventing pop-ups");
			}
			
		});
		cancel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DownloadTracesDialog.this.hide();
			}
			
		});
		buttons.add(done);
		buttons.add(cancel);
		content.add(buttons);
		fpd.add(content);
		fpd.setFocus(true);
		setWidget(fpd);
	}

	public void openURL(final String title, final String url, final String snapshot, final String other) {
		OpenSignedUrl action = new OpenSignedUrl(title, url, snapshot, other, null);
		IeegEventBusFactory.getGlobalEventBus().fireEvent(new OpenSignedUrlEvent(action));
	}
}
