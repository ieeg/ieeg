/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.actions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickHandler;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.events.OpenDisplayPanelEvent;
import edu.upenn.cis.db.mefview.client.messages.requests.OpenDisplayPanel;
import edu.upenn.cis.db.mefview.client.panels.AttachmentDownloader;
import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;

public class ClickAction extends PluginAction {
	
	ClickHandler handler;
	
	Map<SerializableMetadata,Set<PresentableMetadata>> snapshots = new HashMap<SerializableMetadata,Set<PresentableMetadata>>();

	public ClickAction(String actionName, ClickHandler ch, ClientFactory cf, Presenter pres) {
		super(actionName, cf, pres, null);
		handler = ch;
	}

	@Override
	public void execute() {
		handler.onClick(null);
//		return null;
	}
	
	public ClickHandler getClickHandler() {
		return handler;
	}

	@Override
	public IPluginAction create(Set<PresentableMetadata> md,
			ClientFactory factory, Presenter pres) {
		return new ClickAction(getName(), handler, factory, pres);
	}
	
};

