/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import com.google.gwt.place.shared.PlaceTokenizer;

import edu.upenn.cis.db.mefview.client.plugins.images.old.ImageViewerPanel;



public class ImagePlace extends WindowPlace {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String imageName;
	int mode;
	
	public ImagePlace() {}
	
	public ImagePlace(String name, String snapshot, int mode) {
		super(snapshot);
		imageName = name;
		this.mode = mode;
	}


//	public static class Tokenizer implements PlaceTokenizer<ImagePlace> {
//
//		@Override
//		public ImagePlace getPlace(String str) {
//			int img = str.indexOf("img:{") + 5;
//			int imgEnd = str.indexOf('}', img);
//			
//			int snap = str.indexOf("s:{", imgEnd) + 3;
//			int snapEnd = str.indexOf('}', snap);
//
//			return new ImagePlace(str.substring(img, imgEnd), str.substring(snap, snapEnd), 0);
//		}
//
//		@Override
//		public String getToken(ImagePlace place) {
//			return "img:{" + place.imageName + "},s:{" + place.snapshot + "}"; 
//		}
//		
//	}
	
//	public ImagePlace(String str){
//		JAXBContext context;
//		try {
//			context = JAXBContext.newInstance(ImagePlace.class);
//			
//			Unmarshaller unmarshaller = context.createUnmarshaller();
//			
//			StringReader sr = new StringReader(str);
//			
//			unmarshaller.unmarshal(sr);
//		} catch (JAXBException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}


	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	public int getImageMode() {
		return mode;
	}


	@Override
	public String getType() {
		return ImageViewerPanel.NAME;
	}


//	@Override
//	public String getSerializableForm() {
//		return snapshot + "\\" + imageName + "\\" + mode;
//	}
//
//
//	public static WindowPlace getFromSerialized(String serial) {
//		String[] items = serial.split("\\");
//		
//		return new ImagePlace(items[0], items[1], Integer.parseInt(items[2]));
//	}

}
