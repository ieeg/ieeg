/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

import edu.upenn.cis.braintrust.security.AuthenticationException;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.SessionExpirationException;
import edu.upenn.cis.db.mefview.shared.util.ToolUploadCookies;

public abstract class SecFailureAsyncCallback<T>
		implements
		AsyncCallback<T> {

	public abstract static class SimpleSecFailureCallback<T> extends
			SecFailureAsyncCallback<T> {
		public SimpleSecFailureCallback(ClientFactory clientFactory) {
			super(clientFactory);
		}

		@Override
		public void onFailureCleanup(Throwable caught) {}
	}

	public abstract static class SecAlertCallback<T> extends
			SecFailureAsyncCallback.SimpleSecFailureCallback<T> {

		public SecAlertCallback(ClientFactory clientFactory) {
			super(clientFactory);
		}

		@Override
		public void onNonSecFailure(Throwable caught) {}
	}

	public abstract static class AlertCallback<T> extends
			SecFailureAsyncCallback.SimpleSecFailureCallback<T> {

		private final String nonAuthzAlert;

		public AlertCallback(ClientFactory clientFactory, String nonAuthzAlert) {
			super(clientFactory);
			this.nonAuthzAlert = checkNotNull(nonAuthzAlert);
		}

		@Override
		public void onNonSecFailure(Throwable caught) {
			Window.alert(nonAuthzAlert);
		}
	}

	private final ClientFactory clientFactory;
	private static boolean handlingExpiration = false;

	public SecFailureAsyncCallback(ClientFactory clientFactory) {
		this.clientFactory = checkNotNull(clientFactory);
	}

	@Override
	public final void onFailure(Throwable caught) {
		onFailureCleanup(caught);
		final boolean secFailure = SecFailureAsyncCallback.onSecFailure(
				caught,
				clientFactory);
		if (!secFailure) {
			onNonSecFailure(caught);
		}
	}

	protected abstract void onFailureCleanup(Throwable caught);

	protected abstract void onNonSecFailure(Throwable caught);

	public static final boolean onSecFailure(Throwable caught,
			ClientFactory clientFactory) {
		if (caught instanceof BtSecurityException) {
			if (caught instanceof AuthenticationException) {
				if (caught instanceof SessionExpirationException) {
					if (handlingExpiration == false) {
						handlingExpiration = true;
						Window.alert("Your session has expired. You will be sent to the login page.");
						logout(clientFactory);
					}
				} else {
					Window.alert("Unable to authenticate. Please log in.");
					// Remove closing/close handlers so that location
					// reassigning is not interrupted.
					clientFactory.getSessionModel().getSaveAndShutdownController()
							.removeCloseHandlers();
					Window.Location.assign(GWT
							.getHostPageBaseURL()
							+ "main.html");
				}
			} else if (caught instanceof AuthorizationException) {
				Window.alert("You are not authorized to perform this operation.");
				caught.printStackTrace();
			} else {
				// We should not end up here.
				Window.alert("Unable to complete operation.");
			}
			return true;
		}
		return false;
	}

	public static final void logout(ClientFactory clientFactory) {
		// Remove closing/close handlers so that location
		// reassigning is not interrupted.
		clientFactory.getSessionModel().getSaveAndShutdownController()
				.removeCloseHandlers();
		Cookies.removeCookie(
				ToolUploadCookies.COOKIE_NAME,
				ToolUploadCookies.SERVLET_PATH);
		if (clientFactory.getSessionModel().isRegisteredUser())
		clientFactory.getPortal().saveWorkPlace(
				clientFactory.getSessionModel().getSessionID(),
				clientFactory.getSessionModel().getSaveAndShutdownController()
						.getWorkPlace(),
				clientFactory.getSessionModel().getUserPreferences(),
				new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.Location.assign(GWT
								.getHostPageBaseURL()
								+ "main.html");
					}

					@Override
					public void onSuccess(Boolean result) {
						Window.Location.assign(GWT
								.getHostPageBaseURL()
								+ "main.html");
					}
				});
	}
}