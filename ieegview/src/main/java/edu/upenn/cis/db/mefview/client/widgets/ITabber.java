package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.db.mefview.client.panels.AstroPanel;

public interface ITabber {
	public int getSelectedTab();

	public void addStyleDependentName(String string);

	public int getWidgetCount();

	public void selectTab(int i);

	public void selectTab(Widget w);

	public HandlerRegistration addSelectionHandler(SelectionHandler<Integer> handler);

	public void clear();

	public int getWidgetIndex(Widget w);

	public void insert(Widget w, Widget tab, int inx);

	public void add(Widget panel, Widget tab);

	public int getSelectedIndex();

	public Widget getWidget(int selectedIndex);

	public boolean remove(Widget widget);

	public boolean remove(int inx);
}
