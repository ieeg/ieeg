/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.events;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;

public class SearchResultsUpdatedEvent extends GwtEvent<SearchResultsUpdatedEvent.Handler> {
	private static final Type<SearchResultsUpdatedEvent.Handler> TYPE = new Type<SearchResultsUpdatedEvent.Handler>();
	
	List<PresentableMetadata> newOrUpdatedResults = new ArrayList<PresentableMetadata>();
	List<PresentableMetadata> deletedResults = new ArrayList<PresentableMetadata>();
	
	String searchString = null;
	
	private boolean doReplace = false;;
	
	public SearchResultsUpdatedEvent(String search, List<? extends PresentableMetadata> result,
			List<? extends PresentableMetadata> deleted, boolean replace) {
		searchString = search;
		this.newOrUpdatedResults.addAll(result);
		deletedResults.addAll(deleted);
		doReplace = replace;
	}
	
	public SearchResultsUpdatedEvent(String search, Collection<? extends PresentableMetadata> results, boolean replace) {
		this.newOrUpdatedResults = new ArrayList<PresentableMetadata>();
		newOrUpdatedResults.addAll(results);
		deletedResults = null;
		doReplace = replace;

		searchString = search;
	}
	
	public void setDoReplace(boolean value) {
		doReplace = value;
	}
	
	public static com.google.gwt.event.shared.GwtEvent.Type<SearchResultsUpdatedEvent.Handler> getType() {
		return TYPE;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SearchResultsUpdatedEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(SearchResultsUpdatedEvent.Handler handler) {
		handler.onSearchResultsChanged(searchString, newOrUpdatedResults, deletedResults, doReplace);
	}

	public static interface Handler extends EventHandler {
		void onSearchResultsChanged(String search, List<PresentableMetadata> newResults,
				List<PresentableMetadata> deletedResults, boolean replaceResults);
	}
}
