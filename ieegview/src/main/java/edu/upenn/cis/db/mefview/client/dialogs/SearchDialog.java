/*******************************************************************************
 * Copyright 2012 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.mefview.client.dialogs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ExperimentSearchCriteria;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.events.SearchResultsUpdatedEvent;
import edu.upenn.cis.db.mefview.client.viewers.ExperimentSearchComposite;
import edu.upenn.cis.db.mefview.client.viewers.StudySearch;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SearchResult;

/**
 * @author Sam Donnelly, revised heavily by Zack Ives
 */
public class SearchDialog extends DialogBox {
	private TabLayoutPanel tabLayoutPanel = new TabLayoutPanel(40, Unit.PX);
	private DialogBox errorMessage = new DialogBox();
	ExperimentSearchComposite experimentSearch;
	
	TextBox searchTerms = new TextBox();
	
	public SearchDialog(
			final String userID,
			final Button mainSearchButton,
			final ClientFactory clientFactory) {
		this(userID, mainSearchButton, clientFactory, true);
	}

	public SearchDialog(
			final String userID,
			final Button mainSearchButton,
			final ClientFactory clientFactory,
			final boolean showIt) {
		setTitle("Search for Studies or Experiments");
		setText("Search for Studies or Experiments");
		StudySearch studySearch = new StudySearch(
				userID,
				clientFactory.getSearchServices(),
				mainSearchButton,
				clientFactory,
				this);

		GWT.log("Search dialog");
		
		if (clientFactory.getSessionModel().getServerConfiguration().isKeywordSearch())
			showKeywordSearch(userID, mainSearchButton, clientFactory, showIt);
		
		tabLayoutPanel.add(studySearch, "Study Search");

		final boolean mainSearchButtonPrevEnabled = (mainSearchButton == null) ? false : mainSearchButton
				.isEnabled();
		// final Set<ExperimentSearchCriteria> expSearchCrit = newHashSet();

		clientFactory.getSearchServices().getExperimentSearchCriteria(
				clientFactory.getSessionModel().getSessionID(),
				new SecFailureAsyncCallback<ExperimentSearchCriteria>(clientFactory) {

					public void onFailureCleanup(Throwable caught) {
						if (mainSearchButton != null)
							mainSearchButton
								.setEnabled(mainSearchButtonPrevEnabled);
						SearchDialog.this.hide();
					}
					
					public void onNonSecFailure(Throwable caught) {
						errorMessage.setText("Server Error");
						VerticalPanel vp = new VerticalPanel();
						vp.add(new HTML(
								"There was an error loading search criteria, please try again..."));
						Button ok = new Button("OK");
						vp.setCellHorizontalAlignment(ok,
								HasHorizontalAlignment.ALIGN_CENTER);
						ok.addClickHandler(new ClickHandler() {

							public void onClick(ClickEvent event) {
								errorMessage.hide();
							}

						});
						vp.add(ok);
						errorMessage.setWidget(vp);
						errorMessage.center();
					}

					public void onSuccess(ExperimentSearchCriteria result) {
						showExperimentSearch(result, userID, mainSearchButton, clientFactory, showIt);
					}
				});

		setWidget(tabLayoutPanel);
		tabLayoutPanel.setHeight("45em");
		tabLayoutPanel.setWidth("45em");
		// setWidth("50em");
	}
	
	public void showKeywordSearch(String userID,
			final Button mainSearchButton,
			final ClientFactory clientFactory,
			final boolean showIt) {
//		final FocusPanel focus = new FocusPanel();
		FlowPanel fp = new FlowPanel();
//		focus.add(fp);
		
		fp.add(new Label("Keywords: "));
		fp.add(searchTerms);
		final Button searchButton = new Button("Search");
		final Button cancelButton = new Button("Cancel");
		fp.add(searchButton);
		fp.add(cancelButton);

		searchTerms.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER || event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					searchButton.click();
				}
			}
			
		});
		searchTerms.addKeyPressHandler(new KeyPressHandler() {

			public void onKeyPress(KeyPressEvent arg0) {
				if (arg0.getCharCode() == '\r' || arg0.getCharCode() == '\n' || arg0.getNativeEvent().getCharCode() == KeyCodes.KEY_ENTER) {
					searchButton.click();
				}
			}
			
		});
		
		cancelButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(true);
				SearchDialog.this.hide();
			}

		});

		searchButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				cancelButton.setEnabled(false);
				searchButton.setEnabled(false);
				if (mainSearchButton != null)
					mainSearchButton.setEnabled(false);

				SearchDialog.this.hide();
				
				clientFactory.getSearchServices().getSearchMatches(
						clientFactory.getSessionModel().getSessionID(), searchTerms.getText(), 0, -1, 
						new SecFailureAsyncCallback<List<? extends PresentableMetadata>>(clientFactory) {

							public void onFailureCleanup(Throwable caught) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
								if (mainSearchButton != null)
									mainSearchButton.setEnabled(true);
							}
							
							public void onNonSecFailure(Throwable caught) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
							}

							public void onSuccess(List<? extends PresentableMetadata> result) {
								cancelButton.setEnabled(true);
								searchButton.setEnabled(true);
								if (!result.isEmpty()) {
									
									Set<SearchResult> searchResults = new HashSet<SearchResult>();
									for (PresentableMetadata md: result)
										if (md instanceof SearchResult)
											searchResults.add((SearchResult)md);
									IeegEventBusFactory.getGlobalEventBus().fireEvent(
											new SearchResultsUpdatedEvent(searchTerms.getText(),
													searchResults, true));
									
									triggerRefresh(searchTerms.getText(), searchResults, clientFactory);
								}
							}
						});
			}
		});

		tabLayoutPanel.add(
				fp,
				"Keyword Search");
		
		SearchDialog.this.setModal(true);
		SearchDialog.this.setGlassEnabled(true);
		SearchDialog.this.center();
		if (showIt) {
			SearchDialog.this.show();
			
			Timer t = new Timer() {
			      @Override
			      public void run() {
//						focus.setFocus(true);
						searchTerms.setFocus(true);
			      }
			    };
			    
			t.schedule(300);
		} else
			SearchDialog.this.hide();
	}
	
	public void showExperimentSearch(
			ExperimentSearchCriteria criteria, 
			String userID,
			Button mainSearchButton,
			ClientFactory clientFactory,
			boolean showIt) {
		experimentSearch =
				new ExperimentSearchComposite(
						userID,
						mainSearchButton,
						clientFactory,
						SearchDialog.this,
						criteria);
		
		tabLayoutPanel.add(
				experimentSearch,
				"Experiment Search");
		SearchDialog.this.setModal(true);
		SearchDialog.this.setGlassEnabled(true);
		SearchDialog.this.center();
		if (showIt)
			SearchDialog.this.show();
		else
			SearchDialog.this.hide();
	}
	
	Timer trefresh;
	
	void triggerRefresh(
			final String terms, 
			final Set<SearchResult> results2,
			final ClientFactory clientFactory
			) {
		trefresh = new Timer() {
		      @Override
		      public void run() {
		    	  boolean refresh = false;
		    	  
		    	  GWT.log("Polling for changes");
		    	  final Set<SearchResult> results = new HashSet<SearchResult>();
		    	  for (SearchResult sr: results2) {
		    		  refresh |= (sr.getIsValid() != null);
		    		  
		    		  if (sr.getIsValid() != null) {
		    			  SearchResult sr2 = new SearchResult(sr);
		    			  sr2.setTermsMatched(sr.getTermsMatched());
		    			  sr2.setScore(sr.getScore());
		    			  sr2.setIsValid(sr.getIsValid());
		    			  sr2.setTermRate(sr.getTermRate());
		    			  sr2.setFeatures(sr.getFeatures());
		    			  results.add(sr2);
		    		  }
		    	  }
		    	  
		    	  if (refresh) {
			    	  GWT.log("Changes detected");
		    		  clientFactory.getSearchServices().updateSearchMatches(results, 
		    				  clientFactory.getSessionModel().getSessionID(), terms, 0, -1, 
						new AsyncCallback<List<? extends PresentableMetadata>>() {

							@Override
							public void onFailure(Throwable caught) {
								// TODO Auto-generated method stub
								GWT.log("Failed : " + caught);
								triggerRefresh(terms, results2, clientFactory);
							}

							@Override
							public void onSuccess(List<? extends PresentableMetadata> result) {
								
								boolean newWeight = false;
								
								Map<String, Double> weights = new HashMap<String, Double>();
								Map<String, Boolean> fb = new HashMap<String, Boolean>();
								
								for (SearchResult sr: results) {
									weights.put(sr.getId(), sr.getScore());
									if (sr.getIsValid() != null)
										fb.put(sr.getId(), sr.getIsValid());
								}
								
								GWT.log("Detecting weight changes");
								// Update search results!
								Set<SearchResult> searchResults = new HashSet<SearchResult>();
								for (PresentableMetadata md: result)
									if (md instanceof SearchResult) {
										SearchResult sr = (SearchResult)md;
										searchResults.add(sr);
								
										// If a new result is here or a weight has changed
										
										if (!newWeight && 
												(!weights.containsKey(sr.getId()) ||
												weights.get(sr.getId()) != sr.getScore()))  {
												newWeight = true;
										}
										// Restore feedback
										if (fb.containsKey(sr.getId())) {
											sr.setIsValid(fb.get(sr.getId()));
										}
									}

								if (newWeight) {
									GWT.log("New weights!");
									IeegEventBusFactory.getGlobalEventBus().fireEvent(
											new SearchResultsUpdatedEvent(searchTerms.getText(),
													searchResults, true));
								}								
								triggerRefresh(terms, searchResults, clientFactory);
							}
		    			  
		    		  });
		    	  } else
		    		  trefresh.schedule(8000);
		      }
		    };
		    
		trefresh.schedule(8000);
	}
}
