/*
 * Copyright 2013 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.tools;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.dialogs.RegisterTool;
import edu.upenn.cis.db.mefview.client.events.ToolMarkedAsFavoriteEvent;
import edu.upenn.cis.db.mefview.client.events.ToolUpdatedEvent;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.presenters.BasicPresenter;
import edu.upenn.cis.db.mefview.client.presenters.Presenter;
import edu.upenn.cis.db.mefview.client.presenters.PresenterExistsException;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory;
import edu.upenn.cis.db.mefview.client.presenters.Presenter.Display;
import edu.upenn.cis.db.mefview.client.presenters.PresenterFactory.PresenterInfo;
import edu.upenn.cis.db.mefview.client.widgets.Dialogs;
import edu.upenn.cis.db.mefview.client.widgets.PagedTable;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;

//import edu.upenn.cis.db.mefview.shared.ToolInfo;

public class ToolBrowserPresenter extends BasicPresenter implements 
		ToolUpdatedEvent.Handler,
		ToolMarkedAsFavoriteEvent.Handler {
	public interface Display extends BasicPresenter.Display {
		public void enableAddHandler(boolean sel);

		public void enableRemoveHandler(boolean sel);

		public void enableUpdateHandler(boolean sel);

		public void enableBookmarkHandler(boolean sel);

		public void setAddHandler(ClickHandler handler);

		public void setRemoveHandler(ClickHandler handler);

		public void setUpdateHandler(ClickHandler handler);

		public void setBookmarkHandler(ClickHandler handler);

		public void setFavoriteClickHandler(
				PagedTable.StarToggleHandler<ToolDto> handler);

		public void setToolSelectionChangeHandler(Handler handler);

		public ToolDto getSelectedTool();

		public boolean isSelectedTool(ToolDto ti);

		public List<ToolDto> getTools();

		void refreshTools();

		public void addTool(ToolDto ti);

		public void addTools(Collection<ToolDto> ti);

		public void removeTool(ToolDto ti);

		public Set<ToolDto> getFavoriteTools();

		public void setFavoriteTools(Collection<ToolDto> ti);

		public void addFavoriteTool(ToolDto ti);

		public void removeFavoriteTool(ToolDto ti);
	}

	Set<String> openTools = new HashSet<String>();
	Set<String> recentTools = new HashSet<String>();

	AppModel control;
//	ClientFactory clientFactory;

	com.google.web.bindery.event.shared.HandlerRegistration last1;
	com.google.web.bindery.event.shared.HandlerRegistration last2;

	// Button findMoreTools = new Button("Find more tools...");

	public static final String NAME = "tool-browser";
	public static final Set<String> TYPES = Sets.newHashSet(
			BuiltinDataTypes.MATLAB,
			BuiltinDataTypes.BIN,
			BuiltinDataTypes.JAVA);
	static final ToolBrowserPresenter seed = new ToolBrowserPresenter();

	static {
		try {
			PresenterFactory.register(NAME, new PresenterFactory.PresenterInfo(
					seed, true));
		} catch (PresenterExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ToolBrowserPresenter() { super(null, NAME, TYPES); }

	public ToolBrowserPresenter(
			final AppModel parent,
			final ClientFactory factory) {

		super(factory, NAME, TYPES);
		
//		clientFactory = factory;
		control = parent;
	}

	public void closeTool(final String tool) {
		if (openTools.remove(tool))
			recentTools.add(tool);
	}

	public void openTool(final String tool) {
		openTools.add(tool);
	}

	// public void updateRevId(String oldRevId, String newRevId) {
	// for (ToolDto sr: getDisplay().getTools()) {
	// if (sr.getPubId().equals(oldRevId))
	// sr.setPubId(newRevId);
	// }
	//
	// }

	public void addTool(ToolDto ti) {
		getDisplay().addTool(ti);
	}

	public void removeTool(ToolDto ti) {
		getDisplay().removeTool(ti);
	}

	public void refresh() {
		// getDisplay().setFavoriteTools(control.getFavoriteTools());
	}

	@Override
	public void onChangedTool(final ToolDto tool) {
		final boolean newTool = !tool.getId().isPresent();

		final String failureMessage = newTool
				? "Could not add new tool"
				: "Could not update tool";

		// Create list with one tool
		final List<ToolDto> toolList = newArrayList(tool);

		getClientFactory().getPortal().registerTool(
				getSessionModel().getSessionID(),
				toolList,
				new SecFailureAsyncCallback.AlertCallback<List<String>>(
						getClientFactory(),
						failureMessage) {

					@Override
					public void onSuccess(List<String> result) {
						if (newTool) {
							final String toolId = result.get(0);

							// TODO: fix this revID mess
							tool.setId(toolId);
							addTool(tool);
						}

						getDisplay().refreshTools();
					}
				});

	}

	public void setFavorites(Collection<ToolDto> tools) {
		getDisplay().setFavoriteTools(tools);
	}

	@Override
	public void onToolMarkedAsFavorite(ToolDto tool) {
		getDisplay().addFavoriteTool(tool);
	}

	@Override
	public void onToolUnmarkedAsFavorite(ToolDto tool) {
		getDisplay().removeFavoriteTool(tool);
	}

	@Override
	public Presenter create(ClientFactory clientFactory,
			AppModel controller, DataSnapshotViews model,
			PresentableMetadata project, boolean writePermissions) {
		// TODO Auto-generated method stub
		return new ToolBrowserPresenter(controller, clientFactory);
	}

	@Override
	public Display getDisplay() {
		return (Display) display;
	}

	@Override
	public void bind(Collection<? extends PresentableMetadata> selected) {
		getDisplay().addPresenter(this);

		getDisplay().setToolSelectionChangeHandler(new Handler() {

			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				getDisplay().enableRemoveHandler(true);
				getDisplay().enableUpdateHandler(true);
				getDisplay().enableBookmarkHandler(true);
			}
		}
				);

		getDisplay().setAddHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				RegisterTool rt = new RegisterTool(getClientFactory());
				rt.center();
			}

		});

		getDisplay().setUpdateHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				ToolDto ti = getDisplay().getSelectedTool();

				RegisterTool rt = new RegisterTool(getClientFactory(), ti);
				rt.center();
			}

		});

		getDisplay().setRemoveHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				ToolDto thisOne = null;
				for (ToolDto ti : getDisplay().getTools()) {
					if (getDisplay().isSelectedTool(ti))
						thisOne = ti;
				}

				if (thisOne == null)
					return;

				final DialogBox d = new DialogBox();
				d.setTitle("Delete " + thisOne.getLabel() + ": Are you sure?");
				VerticalPanel pan = new VerticalPanel();
				d.setWidget(pan);
				pan.add(new Label("Delete " + thisOne.getLabel()
						+ ": Are you sure?"));
				HorizontalPanel hp = new HorizontalPanel();
				pan.add(hp);
				Button yes = new Button("Yes");
				hp.add(yes);
				hp.add(new HTML("&nbsp;&nbsp;"));
				Button no = new Button("No");
				hp.add(no);

				final ToolDto res = thisOne;
				yes.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						d.hide();
						// Collections.SingletonSet caused problems with GWT-rpc
						final Set<String> idSet = newHashSet(res.getId().get());
						getClientFactory()
								.getPortal()
								.removeTool(
										getSessionModel().getSessionID(),
										idSet,
										new SecFailureAsyncCallback.AlertCallback<Boolean>(
												getClientFactory(),
												"Could not remove tool") {

											@Override
											public void onSuccess(
													Boolean result) {
												if (result) {
													getDisplay().removeTool(res);
												} else {
													Window.alert("Could not remove tool");
												}
											}
										});

					}

				});
				no.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent arg0) {
						d.hide();
					}

				});
				d.center();

			}

		});

		getDisplay().setFavoriteClickHandler(new PagedTable.StarToggleHandler<ToolDto>() {
			@Override
			public void onToggle(ToolDto res, boolean isSet) {
				if (isSet)
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new ToolMarkedAsFavoriteEvent(res,
							true));
				else
					IeegEventBusFactory.getGlobalEventBus().fireEvent(new ToolMarkedAsFavoriteEvent(res,
							false));
			}

		});

		getDisplay().setBookmarkHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				ToolDto thisOne = null;
				for (ToolDto ti : getDisplay().getTools()) {
					if (getDisplay().isSelectedTool(ti))
						thisOne = ti;
				}

				if (thisOne == null)
					return;

				IeegEventBusFactory.getGlobalEventBus().fireEvent(new ToolMarkedAsFavoriteEvent(thisOne,
						true));
			}

		});

		getDisplay().enableAddHandler(true);
		getDisplay().enableRemoveHandler(false);
		getDisplay().enableUpdateHandler(false);
		getDisplay().enableBookmarkHandler(false);

		getClientFactory()
				.getPortal()
				.getRegisteredTools(
						getSessionModel().getSessionID(),
						new SecFailureAsyncCallback.SimpleSecFailureCallback<List<ToolDto>>(
								getClientFactory()) {

							@Override
							public void onNonSecFailure(Throwable caught) {
								Dialogs.messageBox("Error Reading Tools",
										"We were unable to load the tools");
							}

							@Override
							public void onSuccess(List<ToolDto> result) {
								getDisplay().addTools(result);
								// getDisplay().setFavoriteTools(control.getFavoriteTools());
							}

						});

		IeegEventBusFactory.getGlobalEventBus().registerHandler(ToolUpdatedEvent.getType(), this);
		IeegEventBusFactory.getGlobalEventBus()
				.registerHandler(ToolMarkedAsFavoriteEvent.getType(), this);
		
		getDisplay().bindDomEvents();
	}

}
