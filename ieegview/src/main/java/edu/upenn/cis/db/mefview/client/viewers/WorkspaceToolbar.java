/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.viewers;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;

public class WorkspaceToolbar extends FlowPanel {
	Button showEeg;
	Button showImages;
	Button documents;
	Button dicoms;
	Button links;
	Button openProject;
	Button createProject;
	
	public WorkspaceToolbar() {
		this(null,null, null, null, null, null, null);
	}
	
	public WorkspaceToolbar(ClickHandler createProjectHandler,ClickHandler openProjectHandler, 
			ClickHandler viewEegHandler,
	    ClickHandler viewImageHandler, ClickHandler documentHandler, ClickHandler dicomHandler,
	    ClickHandler linkHandler
	    ) {
	  
	  setStyleName("IeegTabToolbar");     

      createProject = new Button("Create Project");
      add(createProject);
      if (createProjectHandler != null)
        createProject.addClickHandler(createProjectHandler);
      else
        createProject.setEnabled(false);
      
	  openProject = new Button("Open Project");
	  add(openProject);
	  if (openProjectHandler != null)
	    openProject.addClickHandler(openProjectHandler);
	  else
	    openProject.setEnabled(false);


	  showEeg = new Button("View EEG");
	  add(showEeg);
	  if (viewEegHandler != null)
	    showEeg.addClickHandler(viewEegHandler);
	  else
	    showEeg.setEnabled(false);

	  showImages = new Button("View Images");
	  add(showImages);
	  if (viewImageHandler != null)
	    showImages.addClickHandler(viewImageHandler);
	  else
	    showImages.setEnabled(false);

	  documents = new Button("View Documentation");
	  add(documents);
	  if (documentHandler != null)
	    documents.addClickHandler(documentHandler);
	  else
	    documents.setEnabled(false);

	  links = new Button("Open Link");
	  add(links);
	  if (linkHandler != null)
	    links.addClickHandler(linkHandler);
	  else
	    links.setEnabled(false);

	  dicoms = new Button("Download DICOMs");
	  add(dicoms);
	  if (dicomHandler != null)
	    dicoms.addClickHandler(dicomHandler);
	  else
	    dicoms.setEnabled(false);

	}
	
	public void setDicomHandler(ClickHandler dicomHandler) {
		if (dicomHandler != null) {
			dicoms.addClickHandler(dicomHandler);
		} else
			dicoms.setEnabled(false);
	}
	
	public void setDocumentHandler(ClickHandler documentHandler) {
		if (documentHandler != null) {
			documents.addClickHandler(documentHandler);
		} else
			documents.setEnabled(false);
	}
	
//	public void setRunHandler(ClickHandler runHandler) {
//		if (runHandler != null)
//			run.addClickHandler(runHandler);
//		else
//			run.setEnabled(false);
//	}
	public void setEegHandler(ClickHandler viewEegHandler) {
		if (viewEegHandler != null)
			showEeg.addClickHandler(viewEegHandler);
		else
			showEeg.setEnabled(false);
	}
	
	public void setLinkHandler(ClickHandler linkHandler) {
		if (linkHandler != null)
			links.addClickHandler(linkHandler);
		else
			links.setEnabled(false);
	}

	public void setImageHandler(ClickHandler viewImageHandler) {
		if (viewImageHandler != null)
			showImages.addClickHandler(viewImageHandler);
		else
			showImages.setEnabled(false);
	}
	
	
	public void enableImageHandler(boolean enabled) {
		showImages.setEnabled(enabled);
	}
	
	public void enableLinkHandler(boolean enabled) {
		links.setEnabled(enabled);
	}
	
	public void enableEegHandler(boolean enabled) {
		showEeg.setEnabled(enabled);
	}



	public Button getEegButton() {
		return showEeg;
	}

	public Button getImageButton() {
		return showImages;
	}

	public Button getDocumentButton() {
		return documents;
	}
	public Button getDicomButton() {
		return dicoms;
	}
	
	public void enableDocumentHandler(boolean enabled) {
		documents.setEnabled(enabled);
	}

	public void enableDicomHandler(boolean enabled) {
		dicoms.setEnabled(enabled);
	}

	public void setOpenProjectHandler(ClickHandler openProjectHandler) {
	  if (openProjectHandler != null){ 
	    openProject.addClickHandler(openProjectHandler);
	  }else {
	    openProject.setEnabled(false);
	  }
	}

    public void setCreateProjectHandler(ClickHandler createProjectHandler) {
      if (createProjectHandler != null){ 
        createProject.addClickHandler(createProjectHandler);
        createProject.setEnabled(true);
      }else {
        createProject.setEnabled(false);
      }
    }

    public void setOpenProjectEnable(boolean enabled) {
      openProject.setEnabled(enabled);
      
    }
  }

  


