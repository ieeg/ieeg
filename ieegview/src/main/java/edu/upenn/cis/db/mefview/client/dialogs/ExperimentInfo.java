/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.braintrust.shared.ToolDto;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.EEGDisplayAsync;
import edu.upenn.cis.db.mefview.client.IeegEventBusFactory;
import edu.upenn.cis.db.mefview.client.events.RequestReloadSnapshotEvent;
//import edu.upenn.cis.db.mefview.shared.ToolInfo;

public class ExperimentInfo extends DialogBox {
	Timer poller;
	private ClientFactory clientFactory;
	
	public ExperimentInfo(//final AppController main, 
			final String friendly, 
			final String theID,
			final ToolDto toolInfo, final String userid, 
			final ClientFactory factory) {
		super(false, false);
		clientFactory = factory;
		FocusPanel fpd = new FocusPanel();
		setTitle("Run Experiment");
		setText("Run Experiment");
		VerticalPanel content = new VerticalPanel();
		content.add(new Label("The analysis snapshot ID is:"));
		TextBox revid = new TextBox();
		revid.setText(theID);
		revid.setReadOnly(true);
		revid.setSelectionRange(0, theID.length());
		content.add(revid);
		
		final ListBox box = new ListBox();
//		box.setHeight("100px");
		box.setVisibleItemCount(10);
//		box.setEnabled(false);
		content.add(new Label("Remaining tasks..."));
		content.add(box);
		box.addItem("Job not yet started...");
		
		Button refresh = new Button("Refresh");
		content.add(refresh);
		
		content.add(new Label("Click to let the job run in the background..."));
		Button done = new Button("Close");
		
		refresh.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
//				doRefresh(//main, 
//						service, userid, theID, friendly, box);
			}
			
		});
		
//		service.initializeJobTasks(clientFactory.getSessionID(), theID, new SecFailureAsyncCallback<List<String>>(clientFactory) {
//
//			@Override
//			public void onFailureCleanup(Throwable caught) {
//				box.clear();
//				box.addItem("Unable to initialize job");
//			}
//			
//			@Override
//			public void onNonSecFailure(Throwable caught) {}
//
//			@Override
//			public void onSuccess(List<String> result) {
//				factory.fireEvent(new LaunchedToolEvent(toolInfo));
//				poller = new Timer() {
//					public void run() {
//						doRefresh(//main, 
//								service, userid, theID, friendly, box);
//					}
//				};
//				poller.scheduleRepeating(10000);
//			}
//			
//		});
		

		done.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (poller != null)
					poller.cancel();
				hide();
//				main.reloadStudy(theID);
				IeegEventBusFactory.getGlobalEventBus().fireEvent(new RequestReloadSnapshotEvent(theID, theID));
			}
			
		});
		content.add(done);
		fpd.add(content);
		fpd.setFocus(true);
		revid.setFocus(true);
		setWidget(fpd);
	}
	
//	private void doRefresh(//final AppController main,
//			final EEGDisplayAsync service, final String userid, final String theID, 
//			final String friendly, final ListBox box) {
//		service.getTaskList(clientFactory.getSessionID(), theID, new SecFailureAsyncCallback<List<String>>(clientFactory) {
//
//			@Override
//			public void onFailureCleanup(Throwable caught) {
//				poller.cancel();
//				hide();
////				main.reloadStudy(theID);
//				clientFactory.fireEvent(new RequestReloadSnapshotEvent(theID, theID));
//			}
//			
//			@Override
//			public void onNonSecFailure(Throwable caught) {}
//
//			@Override
//			public void onSuccess(List<String> result) {
//				if (result.isEmpty()) {
//					poller.cancel();
//					hide();
//					clientFactory.getMainLogger().info("ExperimentInfo: reloading snapshot " + theID);
////					main.loadOrReloadStudy(theID);
//					clientFactory.fireEvent(new RequestReloadSnapshotEvent(theID, theID));
//				} else {
//					box.clear();
//					for (String str : result)
//						box.addItem(str);
//				}
//			}
//			
//		});
//	}

}
