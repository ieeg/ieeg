/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.sample;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Frame;
import com.google.web.bindery.event.shared.HandlerRegistration;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.controller.DataSnapshotViews;
import edu.upenn.cis.db.mefview.client.models.AppModel;
import edu.upenn.cis.db.mefview.client.panels.AstroPanel;
import edu.upenn.cis.db.mefview.client.panels.AstroTab;
import edu.upenn.cis.db.mefview.client.panels.PanelExistsException;
import edu.upenn.cis.db.mefview.client.panels.PanelFactory;
import edu.upenn.cis.db.mefview.client.plugins.PanelTypes;
import edu.upenn.cis.db.mefview.client.resources.ResourceFactory;
import edu.upenn.cis.db.mefview.shared.places.WindowPlace;


public class TemplatePanel extends AstroTab implements  
TemplatePresenter.Display {
	Frame f = new Frame();
	HandlerRegistration hr;

	public final static String NAME = "an1";
	final static TemplatePanel seed = new TemplatePanel();
	static {
		try {
			PanelFactory.register(NAME, new PanelFactory.PanelInfo(seed, true));
		} catch (PanelExistsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TemplatePanel() {
		super();
	}

	public TemplatePanel( 
			final DataSnapshotViews model, 
			final ClientFactory clientFactory,
			final String title
			) {
		
		super(clientFactory, model, title);
		
		add(f);
		f.setSize("100%", "100%");
		setWidgetLeftRight(f, 2, Unit.PX, 2, Unit.PX);
		setWidgetTopBottom(f, 1, Unit.PX, 1, Unit.PX);

		if (getDataSnapshotState() != null && 
				!getDataSnapshotState().getFiles().isEmpty()) {
			initState();
		}
	  }

	void initState() {
		// Initialization.  Note that getDataSnapshotState() should
		// give you access to the current metadata.
		
		// You'll probably just add widgets here, and whether you populate them depends
		
		
		// List of all predefined annotation types (not specific to the study)
		//getDataSnapshotState().getAnnotationDefinitions()
		
		// This model holds all of the annotations, which are in a set of Groups
		// (~ layers) that can be nested tree-style
		// getDataSnapshotState().getAnnotationModel()
		
		// The detailed metadata on the channels
		// getDataSnapshotState().getTraceInfo()
		
		// Also revIDs, labels, ...
		
		// This is also metadata but includes things like number of annotations,
		// number of associated images, and a few other things
		// getDataSnapshotState().getSearchResult()
	}

	@Override
	public String getPanelType() {
		return NAME;
	}

	/**
	 * This is the real constructor for the panel.  It gets called by the 
	 * PanelFactory, then calls the constructor with the appropriate
	 * parameters.
	 */
	@Override
	public TemplatePanel create(ClientFactory clientFactory, 
			AppModel controller, DataSnapshotViews model,
			boolean writePermissions,
			final String title) {
		return new TemplatePanel(model, clientFactory, title);
	}

	/**
	 * This ultimately maps to a Resource that corresponds to a PNG in
	 * ...mefview.client.images.
	 */
	@Override
	public ImageResource getIcon(ClientFactory clientFactory) {
		return ResourceFactory.getDocument();
	}

	/**
	 * This is for saving state on shutdown, but can be ignored
	 */
	@Override
	public WindowPlace getPlace() {
		// TODO if we ever want to save on shutdown
		return null;
	}
	
	@Override
	public void setPlace(WindowPlace place) {
		// TODO if we ever support this
	}

//	@Override
//	public WindowPlace getPlaceFor(String params) {
//		return PDFPlace.getFromSerialized(params);
//	}

	/**
	 * A friendly name for what our panel does
	 */
	public String getTitle() {
		return "Analysis";
	}

	/**
	 * This stub would be used to attach a handler to
	 * some control widget on the panel. 
	 */
	@Override
	public void addClickHandlerToButton(ClickHandler handler) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isSearchable() {
		return false;
	}
	
	public int getMinHeight() {
      return minHeight;
    }

	@Override
	public String getPanelCategory() {
		return PanelTypes.DATA;
	}

	@Override
	public void bindDomEvents() {
	}
}
