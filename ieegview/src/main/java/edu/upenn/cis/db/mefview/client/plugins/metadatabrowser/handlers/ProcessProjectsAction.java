/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.handlers;

import java.util.Set;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.SecFailureAsyncCallback;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.MetadataBrowserPresenter;
import edu.upenn.cis.db.mefview.shared.Project;

public class ProcessProjectsAction extends SecFailureAsyncCallback.SecAlertCallback<Set<Project>> {
	
	MetadataBrowserPresenter.Display display;
	
	public ProcessProjectsAction(ClientFactory clientFactory, MetadataBrowserPresenter.Display display) {
		super(clientFactory);
		this.display = display;
	}
	
	public MetadataBrowserPresenter.Display getDisplay() {
		return display;
	}

	  @Override
	  public void onNonSecFailure(Throwable caught) {
	    getDisplay().log(1, "Project request failed! " + caught.getMessage());
	  }

	  @Override
	  public void onSuccess(Set<Project> arg0) {
	    getDisplay().log(3, "Adding " + arg0.size() + " projects");

	    for (Project p : arg0) {
	    	getDisplay().addProject(p);
	    }
	  }}
