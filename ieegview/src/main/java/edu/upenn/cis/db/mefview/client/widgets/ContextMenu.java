/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.widgets;

import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

public class ContextMenu extends PopupPanel implements ContextMenuHandler, Attachable {

	private MenuBar menu;

	public ContextMenu(Widget parent) {
		super(true);

		menu = new MenuBar(true);
		setWidget(menu);
		// Trap the right-click with 
		menu.addDomHandler(new ContextMenuHandler() {
			@Override
			public void onContextMenu(ContextMenuEvent event) {
				event.preventDefault();
				event.stopPropagation();
				
//				GWT.log("Trapped right-click");
//				NativeEvent newEvent = Document.get().createClickEvent(1, event.getNativeEvent().getScreenX(), 
//						event.getNativeEvent().getScreenY(), event.getNativeEvent().getClientX(), event.getNativeEvent().getClientY(), 
//						false, false, false, false);
//				menu.getElement().dispatchEvent(newEvent);
//				DomEvent.fireNativeEvent(newEvent, menu);
//				menu.fireEvent(new ClickEvent() {});
			}
			
		}, ContextMenuEvent.getType());
		

		// of course it would be better if base would implement HasContextMenuHandlers, but the effect is the same
		parent.addDomHandler(this, ContextMenuEvent.getType());
	}
	
	public MenuBar getMenu() {
		return menu;
	}
	
	public void addMenuItem(MenuItem item) {
		item.addStyleName("contextMenu");
		menu.addItem(item);
	}
	
	public void addMenuSeparator() {
		menu.addSeparator();
	}


	@Override
	public void onContextMenu(ContextMenuEvent event) {
		// stop the browser from opening the context menu
		event.preventDefault();
		event.stopPropagation();
		
		int x = event.getNativeEvent().getClientX() - 8;
		int y = event.getNativeEvent().getClientY() - 8;
		
		if (x < 0)
			x = 0;
		if (y < 0)
			y = 0;
		
		setPopupPosition(x, y);
		
		show();
		if (x + getOffsetWidth() > Window.getClientWidth()) {
			x = Window.getClientWidth() - getOffsetWidth();
			setPopupPosition(x, y);
		}
		if (y + getOffsetHeight() > Window.getClientHeight()) {
			y = Window.getClientHeight() - getOffsetHeight();
			setPopupPosition(x, y);
		}
		
	}
	  
	  @Override
	  public void attach() {
		  onAttach();
	  }
}
