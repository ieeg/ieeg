package edu.upenn.cis.db.mefview.client.plugins.useradmin.passwordreset;

import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;

public class IllegalDialogBox extends DialogBox {
	public IllegalDialogBox() {
		super(true, true);
		
		setTitle("Invalid password");
		add(new Label("Illegal password.  Please use at least 6 characters."));
	}
}
