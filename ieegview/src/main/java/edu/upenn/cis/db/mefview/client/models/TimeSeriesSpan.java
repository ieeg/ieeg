/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;


public abstract class TimeSeriesSpan {
	private TimeSeriesSpanSpecifier specifier;

	public static class Gap {
		int start;
		int end;
		
		public Gap(int s, int e) {
			start = s;
			end = e;
		}
		
		public int getStart() {
			return start;
		}
		
		public int getEnd() {
			return end;
		}
		
		public boolean isInGap(int i) {
			return i >= start && i < end; 
		}
	}
	
	private double scale;
	private double period;
	private long end;
	private long startTime;

	public TimeSeriesSpan(TimeSeriesSpanSpecifier specifier, long startTime, long endTime, double period, 
			double scale) {
		this.scale = scale;
		this.startTime = startTime;
		this.period = period;
		this.end = endTime;
		
		this.specifier = specifier;
	}
	
	public TimeSeriesSpan(TimeSeriesSpanSpecifier specifier) {
		this.specifier = specifier;
	}
	
	public abstract boolean isInGap(int index);

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}
	
	public long getEnd() {
		return end;
	}
	
	public void setEnd(long end) {
		this.end = end;
	}

	public double getPeriod() {
		return period;
	}

	public void setPeriod(double period) {
		this.period = period;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public abstract int[] getSeries();
	
//	public abstract JsArrayInteger getSeriesJs();

	public TimeSeriesSpanSpecifier getSpecifier() {
		return specifier;
	}
	
	public void setSpecifier(TimeSeriesSpanSpecifier spec) {
		specifier = spec;
	}
}
