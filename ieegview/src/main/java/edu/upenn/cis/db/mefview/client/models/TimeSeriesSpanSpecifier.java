/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.models;

import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

/**
 * A specifier for a time series span
 * 
 * @author zives
 *
 */
public class TimeSeriesSpanSpecifier implements Comparable<TimeSeriesSpanSpecifier> {
	private INamedTimeSegment segment;
	private long startTime;
	private long width;
	private double frequency;
	private TimeSeriesFilterSpecifier filter;
	
	public TimeSeriesSpanSpecifier(INamedTimeSegment meta, long startTime,
			long width, double frequency, TimeSeriesFilterSpecifier filter) {
		this.segment = meta;

		setStartTime(startTime);
		setFrequency(frequency);
		setFilter(filter);
		setWidth(width);
	}
	
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public long getWidth() {
		return width;
	}
	public void setWidth(long width) {
		this.width = width;
	}
	public double getFrequency() {
		return frequency;
	}
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	public TimeSeriesFilterSpecifier getFilter() {
		return filter;
	}
	public void setFilter(TimeSeriesFilterSpecifier filter) {
		this.filter = filter;
	}

	public String getRevId() {
		return segment.getId();
	}

	public String getFriendlyName() {
		return segment.getLabel();
	}
	
	/**
	 * Returns true if the two specifiers have *identical* filter specs
	 * and "this" is a superset of o
	 * 
	 * @param o
	 * @return
	 */
	public boolean subsumes(TimeSeriesSpanSpecifier o) {
		boolean filtersOK;
		if (filter == null && o.filter == null)
			filtersOK = true;
		else if (filter != null && o.filter != null)
			filtersOK = filter.equals(o.filter);
		else
			filtersOK = false;

		if (contains(o) && frequency >= o.frequency)
			return filtersOK;
		
		return false;
	}
	
	/**
	 * Returns true if the range of this contains the range of o
	 * @param o
	 * @return
	 */
	public boolean contains(TimeSeriesSpanSpecifier o) {
		return (startTime <= o.startTime && width >= o.width);
	}
	
	/**
	 * Returns true if the range of this overlaps the range of o
	 * @param o
	 * @return
	 */
	public boolean overlaps(TimeSeriesSpanSpecifier o) {
		long endTime = startTime + width;
		long oEndTime = o.startTime + o.width;
		
		return (startTime <= o.startTime && endTime >= o.startTime) ||
				(o.startTime <= startTime && oEndTime >= startTime) ||
				(startTime <= oEndTime && endTime >= oEndTime) ||
				(o.startTime <= endTime && oEndTime >= endTime);
	}
	
	// TODO:  add union, difference, intersection
	
	@Override
	public int compareTo(TimeSeriesSpanSpecifier o) {
		if (o == this)
			return 0;
		else {
			if (!getRevId().equals(o.getRevId()))
				return getRevId().compareTo(o.getRevId());
			
			if (startTime < o.startTime || width < o.width || frequency < o.frequency)
				return -1;
			else if (startTime > o.startTime || width > o.width || frequency > o.frequency)
				return 1;
			else
				return 0;
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof TimeSeriesSpanSpecifier))
			return false;
		
		TimeSeriesSpanSpecifier other = (TimeSeriesSpanSpecifier)o;
		
		boolean filtersOK;
		if (filter == null && other.filter == null)
			filtersOK = true;
		else if (filter != null && other.filter != null)
			filtersOK = filter.equals(other.filter);
		else
			filtersOK = false;
		
		return compareTo(other) == 0 && filtersOK;
	}
	
	public INamedTimeSegment getTimeSegment() {
		return segment;
	}
}
