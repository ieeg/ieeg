/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.plugins.metadatabrowser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.ui.Widget;

import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.WebConsole;
import edu.upenn.cis.db.mefview.client.widgets.ResizingLayout;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SerializableMetadata;
import edu.upenn.cis.db.mefview.shared.StudyMetadata;

public class MetadataBrowserChildPanel extends ResizingLayout {
	public static enum VIEW {FAVORITES,PROJECT,EXPERIMENT,STUDY};
	
	public interface ChildPane {
		public void setSelected(Set<SerializableMetadata> selectedItem);
		public void setSelected(SerializableMetadata selectedItem);
		
		public void bind();
		public void unbind();
		
		public ChildPane create(ClientFactory factory, MetadataBrowserPresenter presenter);
	}

	// TODO:
	//  replace with a Map between metadata type --> {set of views}
	//  have a stacked layout with the views?
	//  implement the setSelected function in each
//	ChildPane thePanel = null;

//	SnapshotPane studyDetails;
//	ExperimentPane experimentDetails;
//	ProjectDetails projectPanel;
	ClientFactory clientFactory;
	MetadataBrowserPresenter presenter;
	
	Map<Class<?>,Set<ChildPane>> childMap = new HashMap<Class<?>, Set<ChildPane>>();
	
	public MetadataBrowserChildPanel(ClientFactory cf, MetadataBrowserPresenter presenter) {
		this.presenter = presenter;
		clientFactory = cf;
		
		for (Class<?> c: WebConsole.registry.getTypeMap().keySet()) {
			childMap.put(c,  new HashSet<ChildPane>());
			for (ChildPane pane: WebConsole.registry.getTypeMap().get(c)) {
				ChildPane newPane = pane.create(cf, presenter);
				((Widget)newPane).setVisible(false);
				childMap.get(c).add(newPane);
			}
		}
	}
	
	public void setHeight(String height) {
		for (Set<ChildPane> cpSet : childMap.values())
			for (ChildPane cp: cpSet)
				((Widget)cp).setHeight(height);
		
	}
	
	public void setWidth(String width) {
		for (Set<ChildPane> cpSet : childMap.values())
			for (ChildPane cp: cpSet)
				((Widget)cp).setWidth(width);
	}

	public void setDetails(String experimentId, String friendlyName, PresentableMetadata pm, 
			ExperimentMetadata em) { 
		setView(em);
	}
	
	public void setDetails(final String studyId, final String friendlyName, PresentableMetadata pm, 
			final StudyMetadata sm) {
		setView(sm);
	}
	
	public void setView(SerializableMetadata data) {
		for (Set<ChildPane> cpSet : childMap.values())
			for (ChildPane cp: cpSet) {
				((Widget)cp).setVisible(false);
				remove((Widget) cp);
			}
		
		if (childMap.containsKey(data.getClass())) {
			for (ChildPane cp : childMap.get(data.getClass())) {
					add((Widget) cp);
					((Widget)cp).setVisible(true);
					cp.setSelected(data);
			}
		}
	}
	
	public Set<ChildPane> getChildPanes() {
		Set<ChildPane> ret = new HashSet<ChildPane>();
		for (Set<ChildPane> cpSet : childMap.values())
			ret.addAll(cpSet);
		
		return ret;
	}
}
