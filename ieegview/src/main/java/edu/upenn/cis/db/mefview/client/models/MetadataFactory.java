/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.mefview.client.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;

import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.ProjectFormatter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.drilldown.CollectionDrillDown;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.drilldown.SearchDrillDown;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting.CollectionNodeFormatter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting.SearchResultFormatter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting.StudyInfoFormatter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting.TraceInfoFormatter;
import edu.upenn.cis.db.mefview.client.plugins.metadatabrowser.formatting.UserInfoFormatter;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.CollectionNode;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.MetadataDrillDown;
import edu.upenn.cis.db.mefview.shared.MetadataFormatter;
import edu.upenn.cis.db.mefview.shared.MimeTypeRecognizer;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.Project;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.shared.UserInfo;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;

/**
 * Metadata management, including icon and formatter bindings
 * 
 * @author zives
 *
 */
public class MetadataFactory {
	ClientFactory clientFactory;
	MetadataModel model;
	static MetadataFactory theFactory = null;
	
	static Map<Class<? extends PresentableMetadata>,MetadataDrillDown> drillDown = new HashMap<Class<? extends PresentableMetadata>, MetadataDrillDown>();
	static Map<Class<? extends PresentableMetadata>,MetadataFormatter> formatters = new HashMap<Class<? extends PresentableMetadata>, MetadataFormatter>();
	static Map<Class<? extends PresentableMetadata>,String> icons = new HashMap<Class<? extends PresentableMetadata>, String>();
	
	public MetadataFactory(ClientFactory clientFactory, MetadataModel model) {
		this.clientFactory = clientFactory;
		this.model = model;

		drillDown.put(CollectionNode.class, new CollectionDrillDown());
		
		initFormattersAsNecessary();
	}
	

	public static synchronized MetadataFactory getFactory(
			ClientFactory clientFactory, 
			MetadataModel model) {
		if (theFactory == null) {
			theFactory = new MetadataFactory(clientFactory, model);
		}
		return theFactory;
	}


	public void initFormattersAsNecessary() {
		if (getFormatter(SearchResult.class) == null) { //SearchResult.getDefaultFormatter() == null) {
			registerFormatter(SearchResult.class, new SearchResultFormatter());
			registerDrillDown(SearchResult.class, new SearchDrillDown(model, clientFactory));
			
			registerIcon(SearchResult.class, "folder");//"description");

		}

		if (getFormatter(FileInfo.class) == null) {
			registerFormatter(FileInfo.class,
					new StudyInfoFormatter()
			);
			registerIcon(FileInfo.class, "communication:comment");
		}

		if (getFormatter(UserInfo.class) == null) {
			registerFormatter(UserInfo.class,
					new UserInfoFormatter());
			registerIcon(UserInfo.class, "social:person");
		}

		//		if (TraceInfo.getDefaultPresenter() == null) {
		if (getFormatter(TraceInfo.class) == null) {
//			TraceInfo.setPresenter
			registerFormatter(TraceInfo.class, new TraceInfoFormatter());
		}
//		if (CollectionNode.getDefaultFormatter() == null) {
//			CollectionNode.setFormatter(
		if (getFormatter(CollectionNode.class) == null) {
			registerFormatter(CollectionNode.class,
				new CollectionNodeFormatter());
//			}
			registerIcon(CollectionNode.class, "folder");
			
		}
		if (getFormatter(BasicCollectionNode.class) == null) {
			registerFormatter(BasicCollectionNode.class,
				new CollectionNodeFormatter());
//			}
			registerIcon(BasicCollectionNode.class, "notification:folder-special");
		}
//		if (Project.getDefaultFormatter() == null) {
//			Project.setFormatter(
		if (getFormatter(Project.class) == null) {
			registerFormatter(Project.class,
				new ProjectFormatter());
//			}
			registerIcon(Project.class, "folder-shared");
		}
	}
	
		
		public static void registerDrillDown(Class<? extends PresentableMetadata> c, MetadataDrillDown d) {
			drillDown.put(c, d);
		}
		
		public static MetadataDrillDown getDrillDown(Class<? extends PresentableMetadata> c) {
			GWT.log("MDF requesting drill-down");
			return drillDown.get(c);
		}
		
		public static void registerFormatter(Class<? extends PresentableMetadata> c, MetadataFormatter f) {
			formatters.put(c, f);
		}
		
		public static MetadataFormatter getFormatter(Class<? extends PresentableMetadata> c) {
			return formatters.get(c);
		}

		public static void registerIcon(Class<? extends PresentableMetadata> c, String icon) {
			icons.put(c, icon);
		}
		
		public static String getIcon(PresentableMetadata md) {
			Class<? extends PresentableMetadata> c = md.getClass();

			if (md instanceof FileInfo) // && MimeTypeRecognizer.isImage(((FileInfo)md).getMediaType()))
				return MimeTypeRecognizer.getIconForMimeType(((FileInfo)md).getMediaType());
//			else if (md instanceof FileInfo && MimeTypeRecognizer.isDocument(((FileInfo) md).getMediaType()))
//				return "description";
//			else if (md instanceof FileInfo && MimeTypeRecognizer.isTimeseries(((FileInfo) md).getMediaType()))
//				return "image:blur-linear";
			else if (md instanceof SearchResult && ((SearchResult)md).getBaseRevId() != null) {
				return "icons:cloud-download";
			} else if (md instanceof BasicCollectionNode &&
					((BasicCollectionNode)md).getId().equals("/")) {
				return "chevron-left";
			} else if (icons.get(c) != null)
				return icons.get(c);
			else
				return icons.get(SearchResult.class);
		}

}
