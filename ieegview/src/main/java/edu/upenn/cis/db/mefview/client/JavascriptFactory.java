package edu.upenn.cis.db.mefview.client;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.user.client.Command;

public class JavascriptFactory {
	static Set<String> loadedItems = new HashSet<String>();
	
	
	public static void loadJavascript(final String scriptFileUrl, 
			final Command callbackOnSuccess, 
			final Command callbackOnFailure) {
		if (loadedItems.contains(scriptFileUrl)) {
			callbackOnSuccess.execute();
		} else {
			ScriptInjector.fromUrl(scriptFileUrl)
				.setWindow(ScriptInjector.TOP_WINDOW)
				.setCallback(new Callback<Void,Exception>() {
		
					@Override
					public void onFailure(Exception reason) {
						GWT.log("Failure loading " + scriptFileUrl + ": " + reason);
						if (callbackOnFailure != null)
							callbackOnFailure.execute();
					}
		
					@Override
					public void onSuccess(Void result) {
						loadedItems.add(scriptFileUrl);
						
						callbackOnSuccess.execute();
					}
				}).inject();
		}
	}

	public static void loadJavascriptString(final String scriptStringUrl) {
//		if (!loadedItems.contains(scriptStringUrl)) {
			ScriptInjector.fromString(scriptStringUrl)
//				.setWindow(ScriptInjector.TOP_WINDOW)
				.inject();
//		}
	}
}
