/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client.plugins.eeg.dialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import edu.upenn.cis.db.mefview.client.SessionModel;
import edu.upenn.cis.db.mefview.client.dialogs.ComplexDialog;
import edu.upenn.cis.db.mefview.client.dialogs.IAnnotationGroupDialog;
import edu.upenn.cis.db.mefview.client.models.DataSnapshotModel;
import edu.upenn.cis.db.mefview.client.plugins.eeg.viewer.IEEGPane;
import edu.upenn.cis.db.mefview.client.viewers.AnnotationGroupPane;
import edu.upenn.cis.db.mefview.client.viewers.AnnotationSchemePane;

public class AnnotationGroupDialog extends ComplexDialog implements IAnnotationGroupDialog {
	AnnotationGroupPane groups;
	
	public AnnotationGroupDialog() {
	}

	@Override
	public void init(final AnnotationGroupPane groupPane, final DataSnapshotModel currentModel, final SessionModel sessionModel,
			final IEEGPane display, final Command doAfter) {
		groups = groupPane;
		setStyleName("ChanSelectDialog");
		
		final VerticalPanel AnnLayerPaneWrapper = new VerticalPanel(); 
		
		setText("Annotation Layers");

		setWidget(AnnLayerPaneWrapper);
		AnnLayerPaneWrapper.add(groups);
		groups.setSize("300px", "400px");
		
		 HorizontalPanel bP = new HorizontalPanel();
		
		
		Button close = new Button("Close");
		bP.add(close);
		
		Button prefs = new Button("Edit Styles");
		bP.add(prefs);
		AnnLayerPaneWrapper.add(bP);
		
		
		close.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				AnnotationGroupDialog.this.hide();
				display.refreshAnnotationsOnGraph(true);
				display.refresh();
				display.resetFocus();
				groups.close();
				if (doAfter != null)
					doAfter.execute();
			}
			
		});
		
		prefs.addClickHandler(new ClickHandler() {
          
          @Override
          public void onClick(ClickEvent event){
            AnnotationSchemePane schemePane = new AnnotationSchemePane(sessionModel, currentModel );

            final AnnotationSchemeInfo eegPrefDialog = new AnnotationSchemeInfo(schemePane);

            eegPrefDialog.center();
            eegPrefDialog.show();
            
            
          }
        });
		
		center();
		show();
		
	}




}
