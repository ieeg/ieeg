/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.upenn.cis.db.mefview.client.PermsPresenterTest;
import edu.upenn.cis.db.mefview.server.AnnotationAssemblerTest;
import edu.upenn.cis.db.mefview.server.EEGServiceImplTest;
import edu.upenn.cis.db.mefview.server.SafeFileNameTest;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegmentTest;
import edu.upenn.cis.db.mefview.services.assembler.TsAnnotationAssemblerTest;
import edu.upenn.cis.db.mefview.shared.places.PDFPlaceTest;

/**
 * This is a suite of tests which do not require any setup and so should run and
 * pass in any environment. For example, test classes listed here should not
 * require resources which are not checked into the Maven project.
 * 
 * @author John Frommeyer
 */
@RunWith(Suite.class)
@SuiteClasses({
		AnnotationAssemblerTest.class,
		EEGServiceImplTest.class,
		PermsPresenterTest.class,
		TsAnnotationAssemblerTest.class,
		UnscaledTimeSeriesSegmentTest.class,
		SafeFileNameTest.class,
		PDFPlaceTest.class})
public class NoSetupTests {}
