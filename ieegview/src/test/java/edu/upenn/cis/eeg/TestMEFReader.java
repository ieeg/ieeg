/*******************************************************************************
 * Copyright 2010 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.eeg;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.server.PageReader.PageList;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.eeg.mef.MEFObjectReader;

public class TestMEFReader {
	MEFObjectReader mr;
	long start;

	@Before
	public void setUp() throws Exception {
		start = System.currentTimeMillis();
		String path = Thread.currentThread().getContextClassLoader().getResource("ieegview.properties").getPath();

		IvProps.init(path);
		IStoredObjectReference handle = new FileReference("war/studies/004/LF4.mef");
		mr = new MEFObjectReader(handle);
		System.out.println("Index with " + mr.getTimeIndex().length + " entries");
	}

	@After
	public void tearDown() throws Exception {
//		mr.close();
	}

//	@Test
//	public void testLoadPagesIndividually() throws Exception {
//		long startExp = System.currentTimeMillis();
//		long offset = mr.timeIndex[0];
//		
//		for (int i = 0; i < 1000; i++) {
//			PageList next = mr.getPages(offset + i * 30000000, offset + 1000000 + i * 30000000,1);
//			for (long j = next.startPage; j <= next.endPage; j++)
//				mr.readContents(j, 1);
//		}
//		
//		System.out.println("Finished in " + (System.currentTimeMillis() - startExp));
//	}

	@Test
	public void testLoadPagesTogether() throws Exception {
		long startExp = System.currentTimeMillis();
		long offset = mr.getTimeIndex()[0];
		for (int i = 0; i < 1000; i++) {
			PageList next = mr.getPages(offset + i * 30000000, offset + 1000000 + i * 30000000,1);
			mr.readPages(next.startPage, (int)(next.endPage - next.startPage + 1), true);
		}
		System.out.println("Finished in " + (System.currentTimeMillis() - startExp));
	}
}
