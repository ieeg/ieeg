/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services.assembler;

import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newTreeSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Set;
import java.util.SortedSet;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;
import edu.upenn.cis.db.mefview.testhelper.ClientTstObjectFactory;

public class TsAnnotationAssemblerTest {

	private final TsAnnotationAssembler assembler = new TsAnnotationAssembler(
			new TimeSeriesAssembler());
	private final TstObjectFactory btTstFac = new TstObjectFactory(false);
	private final ClientTstObjectFactory clientTstFac = new ClientTstObjectFactory();

	@Test
	public void toTimeSeriesAnnotation() {
		final Set<TimeSeriesDto> tsDtos = newHashSet(
				btTstFac.newTimeSeriesDto(),
				btTstFac.newTimeSeriesDto(),
				btTstFac.newTimeSeriesDto());
		final TsAnnotationDto tsaDto = btTstFac
				.newTsAnnotationDto(tsDtos);
		final TimeSeriesAnnotation tsa = assembler
				.toTimeSeriesAnnotation(tsaDto);

		assertEquals(3,
				tsa.getAnnotatedSeries().size());
		for (final TimeSeriesDto tsDto : tsaDto.getAnnotated()) {
			final TimeSeries ts = find(
					tsa.getAnnotatedSeries(),
					and(compose(equalTo(tsDto.getLabel()), TimeSeries.getLabel),
							compose(equalTo(tsDto.getId()),
									TimeSeries.getRevIdFunc)), null);
			assertNotNull(ts);
		}
		assertEquals(tsaDto.getAnnotator(), tsa.getAnnotator());
		assertEquals(tsaDto.getDescription(), tsa.getDescription());
		assertEquals(tsaDto.getEndOffsetMicros(), tsa.getEndTimeUutc());
		assertEquals(tsaDto.getLayer(), tsa.getLayer());
		assertEquals(tsaDto.getId(), tsa.getRevId());
		assertEquals(tsaDto.getStartOffsetMicros(), tsa.getStartTimeUutc());
		assertEquals(tsaDto.getType(), tsa.getType());

	}

	@Test
	public void toTsAnnotationDto() {
		final SortedSet<TimeSeries> tsSet = newTreeSet(newHashSet(
				clientTstFac.newTimeSeries(),
				clientTstFac.newTimeSeries(),
				clientTstFac.newTimeSeries()));
		final TimeSeriesAnnotation tsa = 
				clientTstFac.newTimeSeriesAnnotation(tsSet);
		final TsAnnotationDto tsaDto = assembler
				.toTsAnnotationDto(tsa);

		assertEquals(3,
				tsaDto.getAnnotated().size());
		for (final TimeSeries ts : tsa.getAnnotatedSeries()) {
			TimeSeriesDto actualDto = null;
			for (final TimeSeriesDto tsDto : tsaDto.getAnnotated()) {
				if (tsDto.getLabel().equals(ts.getLabel())
						&& tsDto.getId().equals(
								ts.getRevId())) {
					actualDto = tsDto;
					break;
				}

			}
			assertNotNull(actualDto);
		}
		assertEquals(tsa.getAnnotator(), tsaDto.getAnnotator());
		assertEquals(tsa.getDescription(), tsaDto.getDescription());
		assertEquals(tsa.getEndTimeUutc(), tsaDto.getEndOffsetMicros());
		assertEquals(tsa.getLayer(), tsaDto.getLayer());
		assertEquals(tsa.getRevId(), tsaDto.getId());
		assertEquals(tsa.getStartTimeUutc(), tsaDto.getStartOffsetMicros());
		assertEquals(tsa.getType(), tsaDto.getType());
	}

}
