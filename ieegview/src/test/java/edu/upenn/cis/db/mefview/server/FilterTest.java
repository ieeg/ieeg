/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import edu.upenn.cis.db.mefview.eeg.IntArrayWrapper;
import edu.upenn.cis.eeg.filters.TimeSeriesFilterButterworth;

public class FilterTest {
	public static final int SECONDS = 1;
	
	public static void main(String[] args) throws Exception {
//		if (args.length < 5 || args.length > 6)
//			System.err.println("USAGE: test type sampleFreq nPoles fc1 fc2");
		
//		TimeSeriesFilter filt = new TimeSeriesFilter(32, FilterParameters.LOWPASS_FILTER,
//				499.906994, 0, 5, 0, 0);
		
		int freq = 500;
		int sineFreq = 30;
		
		TimeSeriesFilterButterworth filt = new TimeSeriesFilterButterworth();
		
		filt.init(3, 
				TimeSeriesFilterButterworth.LOWPASS_FILTER,
				freq, 
				50, 
				50, 
				0, 
				0);
		
//		Butterworth bw = new Butterworth(100, 16, 10, 500, false);
		
		int[] base = getScaledSineWave(freq, sineFreq);
		
		int[][] arrays = new int[1][];
		
		arrays[0] = base;
		IntArrayWrapper wrap = new IntArrayWrapper(arrays);
		
		boolean first = true;
		for (int i = 0; i < base.length; i++) {
			if (first)
				first = false;
			else
				System.out.print("\t");
			System.out.print(base[i]);
		}
		System.out.println();
		
		int[] data = new int[base.length];
		double[] results = filt.apply(wrap, data);
		
//		double[] buf = new double[base.length];
//		double[] buf2 = new double[base.length];
//		for (int i = 0; i < base.length; i++)
//			buf[i] = base[i];
//		
//		boolean first = true;
//		for (int i = 0; i < wrap.length(); i++) {
//			if (first)
//				first = false;
//			else
//				System.err.print("\t");
//			System.err.print(buf[i]);
//		}
//		System.err.println();

//		bw.apply(buf, buf2, base.length);
		
		double scale = Math.pow(2, 16 - 1) - 1;
//		first = true;
//		for (int i = 0; i < wrap.length(); i++) {
//			if (first)
//				first = false;
//			else
//				System.err.print("\t");
//			System.err.print(buf2[i]);//wrap.get(i));
//		}
//		System.err.println();

		first = true;
		for (int i = 0; i < base.length; i++) {
			if (first)
				first = false;
			else
				System.out.print("\t");
			System.out.print(base[i]);//results[i]);
//			System.err.print((short)(results[i] * scale));//wrap.get(i));
		}
		System.out.println();
	}
	
	public static int[] getScaledSineWave(int sampleRate, int freq) {
		double[] first = getSineWave(sampleRate, freq);
		
		int[] ret = new int[first.length];
		
		for (int i = 0; i < first.length; i++)
			ret[i] = (int)(first[i] * 255);
		
		return ret;
	}
	
	public static double[] getSineWave(int sampleRate, int freq) {
		double[] ret = new double[sampleRate * SECONDS];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = Math.sin(i * 2 * Math.PI * freq / sampleRate);
			ret[i] += Math.sin(i * 2 * Math.PI * (freq * 2) / sampleRate) / 4;
			ret[i] += Math.sin(i * 2 * Math.PI * (freq * 4) / sampleRate) / 4;
			ret[i] += Math.sin(i * 2 * Math.PI * (freq * 8) / sampleRate) / 4;
		}
		
		return ret;
	}
}
