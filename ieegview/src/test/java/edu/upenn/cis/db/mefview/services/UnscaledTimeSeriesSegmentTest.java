/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class UnscaledTimeSeriesSegmentTest {

	@Test
	public void testIsInGap() {
		final long startTime = 0;
		final double period = 1.0;
		final double scale = 1.0;
		final int[] values = { 0, 2, 4, 6,
				8, 10, 0, 0,
				0, 0, 0, 3,
				5, 7, 9, 10,
				20, 30, 0, 0,
				0 };
		final int[] gapStart = { 6, 18 };
		final int[] gapEnd = { 10, 21 };
		final UnscaledTimeSeriesSegment segment = new UnscaledTimeSeriesSegment(
				startTime, period, scale, values, gapStart, gapEnd);

		assertFalse(segment.isInGap(0));
		assertFalse(segment.isInGap(1));
		assertFalse(segment.isInGap(2));
		assertFalse(segment.isInGap(3));
		assertFalse(segment.isInGap(4));
		assertFalse(segment.isInGap(5));
		assertTrue(segment.isInGap(6));
		assertTrue(segment.isInGap(7));
		assertTrue(segment.isInGap(8));
		assertTrue(segment.isInGap(9));
		assertFalse(segment.isInGap(10));
		assertFalse(segment.isInGap(11));
		assertFalse(segment.isInGap(12));
		assertFalse(segment.isInGap(13));
		assertFalse(segment.isInGap(14));
		assertFalse(segment.isInGap(15));
		assertFalse(segment.isInGap(16));
		assertFalse(segment.isInGap(17));
		assertTrue(segment.isInGap(18));
		assertTrue(segment.isInGap(19));
		assertTrue(segment.isInGap(20));

	}

	@Test
	public void testIsInGap2() {
		final double scale = 1.0;
		final int[] values = {
				0,
				2,
				4,
				6,
				8,
				10,
				Integer.MIN_VALUE,
				Integer.MIN_VALUE,
				Integer.MIN_VALUE,
				Integer.MIN_VALUE,
				0,
				3,
				5,
				7,
				9,
				10,
				20,
				30,
				Integer.MIN_VALUE,
				Integer.MIN_VALUE,
				Integer.MIN_VALUE };

		final UnscaledTimeSeriesSegment segment = new UnscaledTimeSeriesSegment(
				scale, values);

		assertFalse(segment.isInGap(0));
		assertFalse(segment.isInGap(1));
		assertFalse(segment.isInGap(2));
		assertFalse(segment.isInGap(3));
		assertFalse(segment.isInGap(4));
		assertFalse(segment.isInGap(5));
		assertTrue(segment.isInGap(6));
		assertTrue(segment.isInGap(7));
		assertTrue(segment.isInGap(8));
		assertTrue(segment.isInGap(9));
		assertFalse(segment.isInGap(10));
		assertFalse(segment.isInGap(11));
		assertFalse(segment.isInGap(12));
		assertFalse(segment.isInGap(13));
		assertFalse(segment.isInGap(14));
		assertFalse(segment.isInGap(15));
		assertFalse(segment.isInGap(16));
		assertFalse(segment.isInGap(17));
		assertTrue(segment.isInGap(18));
		assertTrue(segment.isInGap(19));
		assertTrue(segment.isInGap(20));

	}
}
