/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import java.io.IOException;

import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.db.mefview.client.util.GithubProject;


@GwtIncompatible("GithubProject")
public class GithubTest {

  public static void main (String[] args) {
    
    try {
      GithubProject obj = new GithubProject("ieeg-portal","Nicolet-Reader");
      
      System.out.println(obj); 
//      System.out.println(obj.getRelease)

//      GithubRelease rel = obj.getLatestRelease();
      
      System.out.println("--- ---");
      System.out.println(obj.getLatestZipLink());
      System.out.println(obj.getLatestTarLink());
      
      
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
   
    
  }
  
}
