/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Set;

import org.junit.Test;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.persistence.mapping.AnnotationAssembler;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.TraceInfo;
import edu.upenn.cis.db.mefview.testhelper.CoreTstObjectFactory;

public class AnnotationAssemblerTest {

	private final TstObjectFactory btTstFac = new TstObjectFactory(false);
	private final CoreTstObjectFactory coreTstFac = new CoreTstObjectFactory();

	@Test
	public void toTsAnnotationDtoTest() {
		final List<TraceInfo> traceInfos = newArrayList(
				coreTstFac.newTraceInfo(),
				coreTstFac.newTraceInfo(),
				coreTstFac.newTraceInfo());
		final Annotation a = coreTstFac.newAnnotation(traceInfos);

		final TsAnnotationDto tsaDto = AnnotationAssembler.toTsAnnotationDto(a);
		assertEquals(a.getCreator(), tsaDto.getAnnotator());
		assertEquals(a.getDescription(), tsaDto.getDescription());
		assertEquals(Long.valueOf((long) a.getEnd()), tsaDto.getEndOffsetMicros());
		assertEquals(Long.valueOf((long) a.getStart()),
				tsaDto.getStartOffsetMicros());
		assertEquals(a.getType(), tsaDto.getType());
		assertEquals(a.getRevId(), tsaDto.getId());
		assertEquals(a.getLayer(), tsaDto.getLayer());

		assertEquals(traceInfos.size(), tsaDto.getAnnotated().size());
		for (final TraceInfo expected : traceInfos) {
			TimeSeriesDto actualDto = null;
			for (final TimeSeriesDto tsDto : tsaDto.getAnnotated()) {
				if (tsDto.getLabel().equals(expected.getLabel())
						&& tsDto.getId().equals(
								expected.getRevId())) {
					actualDto = tsDto;
					break;
				}

			}
			assertNotNull(
					"Missing TimeSeriesDto with revId [" + expected.getRevId()
							+ "] and label [" + expected.getLabel()
							+ "].", actualDto);
		}

	}

	@Test
	public void toAnnotationTest() {
		final Set<TimeSeriesDto> tsDtos = newHashSet(
				btTstFac.newTimeSeriesDto(),
				btTstFac.newTimeSeriesDto(),
				btTstFac.newTimeSeriesDto());

		final TsAnnotationDto tsaDto = btTstFac
				.newTsAnnotationDto(tsDtos);

		final Annotation a = AnnotationAssembler.toAnnotation(tsaDto);
		assertEquals(tsaDto.getAnnotator(), a.getCreator());
		assertEquals(tsaDto.getDescription(), a.getDescription());
		assertEquals(Double.valueOf(tsaDto.getEndOffsetMicros()).doubleValue(),
				a.getEnd(), 0.01);
		assertEquals(Double.valueOf(tsaDto.getStartOffsetMicros()).doubleValue(),
				a.getStart(), 0.01);
		assertEquals(tsaDto.getLayer(), a.getLayer());
		assertEquals(tsaDto.getId(), a.getRevId());
		assertEquals(tsaDto.getType(), a.getType());

		// No TraceInfos in this form
		assertEquals(0, a.getChannelInfo().size());
		assertEquals(tsDtos.size(), a.getChannels().size());
		for (final TimeSeriesDto expected : tsDtos) {
			String actualLabel = null;
			for (final String label : a.getChannels()) {
				if (label.equals(expected.getLabel())) {
					actualLabel = label;
					break;
				}
			}
			assertNotNull("Missing Channel with label [" + expected.getLabel()
					+ "].", actualLabel);
		}
	}

	@Test
	public void toAnnotationWTraceInfosTest() {

		final List<TraceInfo> traceInfos = newArrayList(
				coreTstFac.newTraceInfo(),
				coreTstFac.newTraceInfo(),
				coreTstFac.newTraceInfo());
		final Set<TimeSeriesDto> tsDtos = newHashSet();
		for (final TraceInfo traceInfo : traceInfos) {
			tsDtos.add(btTstFac.newTimeSeriesDto(traceInfo.getLabel(),
					traceInfo.getRevId()));
		}

		final TsAnnotationDto tsaDto = btTstFac
				.newTsAnnotationDto(tsDtos);

		final Annotation a = AnnotationAssembler.toAnnotation(tsaDto,
				traceInfos);
		assertEquals(tsaDto.getAnnotator(), a.getCreator());
		assertEquals(tsaDto.getDescription(), a.getDescription());
		assertEquals(Double.valueOf(tsaDto.getEndOffsetMicros()).doubleValue(),
				a.getEnd(), 0.01);
		assertEquals(Double.valueOf(tsaDto.getStartOffsetMicros()).doubleValue(),
				a.getStart(), 0.01);
		assertEquals(tsaDto.getLayer(), a.getLayer());
		assertEquals(tsaDto.getId(), a.getRevId());
		assertEquals(tsaDto.getType(), a.getType());

		assertEquals(traceInfos.size(), a.getChannelInfo().size());
		assertEquals(traceInfos.size(), a.getChannels().size());
		for (final INamedTimeSegment expected : traceInfos) {
			String actualLabel = null;
			for (final String label : a.getChannels()) {
				if (label.equals(expected.getLabel())) {
					actualLabel = label;
					break;
				}
			}
			assertNotNull("Missing Channel with label [" + expected.getLabel()
					+ "].", actualLabel);

			INamedTimeSegment actualTraceInfo = null;
			for (final INamedTimeSegment traceInfo : a.getChannelInfo()) {
				if (traceInfo.getLabel()
						.equals(expected.getLabel())) {
					actualTraceInfo = traceInfo;
					break;
				}
			}
			assertNotNull(
					"Missing TraceInfo with TraceInfo ["
							+ expected.getLabel()
							+ "].", actualTraceInfo);
		}
	}
}
