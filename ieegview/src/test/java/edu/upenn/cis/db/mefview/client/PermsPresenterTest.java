/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.client;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newArrayList;
import static edu.upenn.cis.braintrust.BtUtil.newUuid;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Optional;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.shared.DerivedSnapshot;
import edu.upenn.cis.db.mefview.testhelper.CoreTstObjectFactory;

@GwtIncompatible("A test")
public class PermsPresenterTest {

	@Mock
	private EEGDisplayAsync eegService;
	@Mock
	private ClientFactory factory;
	@Mock
	private ServerAccess cbs;
	private static final TstObjectFactory btTstFac = new TstObjectFactory(false);
	private final CoreTstObjectFactory coreTstFac = new CoreTstObjectFactory();
	private static final User aceUserReader = btTstFac
			.newUserRegularEnabled();
	private static final User aceUserEditor = btTstFac
			.newUserRegularEnabled();
	private ExtUserAce userAceOwn;
	private ExtUserAce userAceRead;
	private ExtUserAce userAceEdit;
	private final String targetId = newUuid();
	private final ExtWorldAce worldAce = btTstFac.newExtWorldAce(targetId);
	private final DerivedSnapshot toolResult = coreTstFac.newToolResult();
	private final BiMap<String, ExtPermission> displayStrToUserPerm = ImmutableBiMap
			.of("Can read", CorePermDefs.READ_PERM);
	private final BiMap<String, ExtPermission> displayStrToProjectPerm = ImmutableBiMap
			.copyOf(displayStrToUserPerm);
	private final BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm = ImmutableBiMap
			.of(
					"None",
					Optional.<ExtPermission> absent(),
					"Can read",
					Optional.of(CorePermDefs.READ_PERM));
	private PermsPresenter ownerPresenter;
	private PermsPresenter readerPresenter;

	@Before
	public void setup() {
		//Set up mocks.
		MockitoAnnotations.initMocks(this);
		when(factory.getPortalServices()).thenReturn(cbs);
		
		userAceOwn = btTstFac.newExtUserAce(
				aceUserEditor,
				targetId);
		userAceOwn.getPerms().add(CorePermDefs.OWNER_PERM);

		userAceRead = btTstFac.newExtUserAce(
				aceUserReader,
				targetId);
		userAceRead.getPerms().add(CorePermDefs.READ_PERM);

		userAceEdit = btTstFac.newExtUserAce(
				aceUserEditor,
				targetId);
		userAceEdit.getPerms().add(CorePermDefs.EDIT_PERM);

		List<ExtUserAce> userAces = newArrayList(
				userAceOwn,
				userAceRead,
				userAceEdit);

		List<ExtProjectAce> projectAces = newArrayList();
		
		readerPresenter = new PermsPresenter(factory,
				false,
				worldAce,
				projectAces,
				userAces,
				toolResult,
				displayStrToWorldPerm,
				displayStrToProjectPerm,
				displayStrToUserPerm);
		ownerPresenter = new PermsPresenter(factory,
				true,
				worldAce,
				projectAces,
				userAces,
				toolResult,
				displayStrToWorldPerm,
				displayStrToProjectPerm,
				displayStrToUserPerm);
	}

	// @Test
	// public void noOp() {
	// assertTrue(true);
	// }

	@Test
	public void constructorTest() {
		assertFalse(readerPresenter.userIsOwner());
		assertTrue(ownerPresenter.userIsOwner());

		final List<ExtWorldAce> worldAces = ownerPresenter
				.getWorldAceProvider()
				.getList();
		assertEquals(1, worldAces.size());
		assertSame(worldAce, getOnlyElement(worldAces, null));

		final List<ExtUserAce> userAces = ownerPresenter.getUserAceProvider()
				.getList();
		assertEquals(3, userAces.size());
		assertTrue(userAces.contains(userAceOwn));
		assertTrue(userAces.contains(userAceRead));
		assertTrue(userAces.contains(userAceEdit));

		assertEquals(newArrayList("Can read"),
				readerPresenter.getUserPermsOptions());
		assertEquals(newArrayList("None", "Can read"),
				readerPresenter.getWorldPermsOptions());

		assertFalse(ownerPresenter.stringToWorldPerm("None").isPresent());
	}

}
