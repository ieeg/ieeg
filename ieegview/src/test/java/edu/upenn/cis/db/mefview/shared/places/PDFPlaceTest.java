/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.shared.places;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.annotations.GwtIncompatible;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.mefview.client.plugins.pdf.PDFPresenter;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;

/**
 * @author John Frommeyer
 *
 */
@GwtIncompatible("A test")
public class PDFPlaceTest {
	private final ObjectMapper om = ObjectMapperFactory.getObjectMapper();

	@Test
	public void testSerializableForm() throws JsonProcessingException,
			IOException {
		ObjectReader reader = om.reader(WindowPlace.class);
		ObjectWriter writer = om.writer();

		final String expectedSnapshotId = BtUtil.newUuid();
		final String expectedPdf = BtUtil.newUuid();
		final URLPlace place = new URLPlace(expectedSnapshotId, expectedPdf, PDFPresenter.NAME);
		final String serializableForm = writer.writeValueAsString(place);

		final URLPlace actual = reader.readValue(serializableForm);
		assertEquals(expectedSnapshotId, actual.getSnapshotID());
		assertEquals(expectedPdf, actual.getDocument());
	}
}
