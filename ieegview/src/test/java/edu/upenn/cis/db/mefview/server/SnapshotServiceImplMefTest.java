/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.IeegException;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.BtSecurityException;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.INamedTimeSegment;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.TraceInfo;

public class SnapshotServiceImplMefTest {

	@Mock
	private IUserService userService;
	@Mock
	private ClientFactory clientFactory;
	@Mock
	private IDataSnapshotServer dataSnapshotServer;
	
	@Mock
	private ISessionManager sessionManager;
	
	// Cannot match user directly because EEGServiceImpl maintains a User cache.
	@Captor
	private ArgumentCaptor<User> userArg;
	@Captor
	private ArgumentCaptor<Set<String>> includedTsRevIdsArg;
	@Captor
	private ArgumentCaptor<Set<String>> includedAnnRevIdsArg;

	private SnapshotServicesImpl service;

	private final List<String> channelIDs = newArrayList("tsRevId-1",
			"tsRevId-2",
			"tsRevId-3");
	private static final String toolName = "tool";
	private static final String friendlyName = "friendly";
	private static final String studyRevId = "studyRevId";
	private static final String dataSnapshotRevId = "dataSnapshotRevId";
	private User user;
	private SessionToken userSessionId;
	private User adminUser;
	private SessionToken adminUserSessionId;
	private User aceUserOwner;
	private User aceUserReader;
	private User aceUserEditor;
	private SessionToken aceUserReaderSessionId;
	private SessionToken aceUserOwnerSessionId;

	private final TstObjectFactory btTstFac = new TstObjectFactory(false);

	@Before
	public void setupServiceMocks() throws AuthorizationException {
		MockitoAnnotations.initMocks(this);
		Map<UserId, Optional<User>> idToUser = newHashMap();
		user = btTstFac.newUserRegularEnabled();
		userSessionId = btTstFac.newSessionToken();
		adminUser = btTstFac.newUserAdminEnabled();
		idToUser.put(adminUser.getUserId(), Optional.of(adminUser));
		adminUserSessionId = btTstFac.newSessionToken();

		aceUserOwner = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserOwner.getUserId(), Optional.of(aceUserOwner));
		aceUserOwnerSessionId = btTstFac.newSessionToken();

		aceUserReader = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserReader.getUserId(), Optional.of(aceUserReader));
		aceUserReaderSessionId = btTstFac.newSessionToken();

		aceUserEditor = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserEditor.getUserId(), Optional.of(aceUserEditor));

		when(userService.findUserByUid(user.getUserId())).thenReturn(
				user);
		when(userService.findUserByUid(adminUser.getUserId()))
				.thenReturn(adminUser);
		when(userService.findUserByUid(aceUserOwner.getUserId()))
				.thenReturn(aceUserOwner);
		when(userService.findUserByUid(aceUserReader.getUserId()))
				.thenReturn(aceUserReader);
		when(userService.findUserByUid(aceUserEditor.getUserId()))
				.thenReturn(aceUserEditor);

		when(userService.findUsersByUid(anySetOf(UserId.class))).thenReturn(
				idToUser);

		when(userService.findUserByUsername(user.getUsername())).thenReturn(
				user);
		when(userService.findUserByUsername(adminUser.getUsername()))
				.thenReturn(adminUser);
		when(userService.findUserByUsername(aceUserOwner.getUsername()))
				.thenReturn(aceUserOwner);
		when(userService.findUserByUsername(aceUserReader.getUsername()))
				.thenReturn(aceUserReader);
		when(userService.findUserByUsername(aceUserEditor.getUsername()))
				.thenReturn(aceUserEditor);

		when(dataSnapshotServer.getUserIdSessionStatus(eq(userSessionId),
				any(Date.class))).thenReturn(new UserIdSessionStatus(
				user.getUserId(),
				SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(eq(adminUserSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						adminUser.getUserId(),
						SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(
				eq(aceUserReaderSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						aceUserReader.getUserId(),
						SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(
				eq(aceUserOwnerSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						aceUserOwner.getUserId(),
						SessionStatus.ACTIVE));
		
		when(sessionManager.authenticateSession(eq(aceUserOwnerSessionId)))
			.thenReturn(aceUserOwner.getUserId());
		when(sessionManager.authenticateSession(eq(aceUserReaderSessionId)))
			.thenReturn(aceUserReader.getUserId());
		when(sessionManager.getUserIdSessionStatus(eq(aceUserOwnerSessionId)))
			.thenReturn(new UserIdSessionStatus(aceUserOwner.getUserId(), SessionStatus.ACTIVE));
		when(sessionManager.getUserIdSessionStatus(eq(aceUserReaderSessionId)))
		.thenReturn(new UserIdSessionStatus(aceUserReader.getUserId(), SessionStatus.ACTIVE));

		service = new SnapshotServicesImpl(userService, dataSnapshotServer);
		service.initTest(sessionManager);
	}

	final String study5 = "3cf5e56c-8320-11e0-8800-0015c5e207d6";
	final String study5channelLtd5 = "3d86f714-8320-11e0-8800-0015c5e207d6";
	final String study5channelLtd7 = "3d86f8e0-8320-11e0-8800-0015c5e207d6";
	
	final String study6 = "3cf5c96a-8320-11e0-8800-0015c5e207d6";
	final String study10 = "3cf5c96a-8320-11e0-8800-0015c5e207d6";
	
	final double start = 1000 * 499.906994;//500000; // 0

//	@Test
//	public void getSampleRangeRaw() throws BtSecurityException, IeegException, ServerTimeoutException {
//		List<String> traceIds = new ArrayList<String>();
//		traceIds.add(study5channelLtd7);
//		// RAW
//		String ret = service.getResultsJSON(
//				aceUserOwnerSessionId, 
//				"foo", 
//				study5, 
//				traceIds, 
//				start, 
//				100000, 
//				1, 
//				null);
//		
//		ResponseHeader hdr500 = getResponseHeader(ret);
//		Integer[] results500 = getSamples(ret);
//		System.out.println(ret);
//		
//		assertEquals(hdr500.count, results500.length);
//		// RAW / 2 (compare with RAW)
//		String ret2 = service.getResultsJSON(
//				aceUserOwnerSessionId, 
//				"foo", 
//				study5, 
//				traceIds, 
//				start, 
//				100000, 
//				2, 
//				null);
//		
//		ResponseHeader hdr250 = getResponseHeader(ret2);
//		Integer[] results250 = getSamples(ret2);
//		System.out.println(ret2);
//		
//		assertEquals(hdr250.count, results250.length);
//		assertEquals(hdr250.count, hdr500.count / 2);
//	}
	
	@Test
	public void getSampleRangeAvg() throws Exception {
		List<INamedTimeSegment> traceIds = new ArrayList<INamedTimeSegment>();
		traceIds.add(new TraceInfo(new SearchResult(study5, "creator", "Test", new ArrayList<String>(), new ArrayList<String>()), 
				study5channelLtd7));
		List<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();
		filters.add(new DisplayConfiguration());
		String ret = service.getResultsJSON(
				aceUserOwnerSessionId, 
				"foo", 
				study5, 
				traceIds, 
				start, 
				100000, 
				1.E6 / 500,	// 500 Hz
				filters,
				null);
		ResponseHeader hdr500 = getResponseHeader(ret);
		Integer[] results500 = getSamples(ret);
		System.out.println(ret);
		
		assertEquals(hdr500.count, results500.length);
		// AVG
		// AVG / 10
		// Plot against RAW
	}
	
	@Test
	public void getSampleRangeMinMax() {
		// RAW + MINMAX
		// DOWN + MINMAX
	}
	
	private ResponseHeader getResponseHeader(String json) {
		ResponseHeader ret = new ResponseHeader();
		
		if (!json.startsWith("[["))
			throw new RuntimeException("Invalid string");

		String hdr = json.substring(2, json.indexOf("],["));
			
//		System.out.println("Header: " + hdr);
		
		String[] hdrParms = hdr.split(",");
		
		if (hdrParms.length != 5)
			throw new RuntimeException("Header should have precisely 5 items!");
		
		ret.start = Double.valueOf(hdrParms[0]);
		ret.sample = Double.valueOf(hdrParms[1]);
		ret.count = Integer.valueOf(hdrParms[2]);
		ret.reqStart = Double.valueOf(hdrParms[3]);
		ret.reqWidth = Double.valueOf(hdrParms[4]);
		
		return ret;
	}
	
	private String stripBrackets(String str) {
		if (str.charAt(0) == '[')
			return str.substring(1, str.lastIndexOf(']'));
		else
			return str;
	}
	
	private Integer[] getSamples(String json) {
		String body = json.substring(json.indexOf("],[") + 2);
		body = body.substring(0, body.length() - 1);
//		System.out.println("Body: " + body);

		String[] samples = body.split(",");
		Integer[] ret = new Integer[samples.length];
		
		int i = 0;
		for (String str: samples) {
			String tmp = stripBrackets(str);
			if (tmp.isEmpty() || tmp.equals("null")) {
				ret[i++] = null;
			} else
				ret[i++] = Integer.valueOf(tmp);
		}
		
		return ret;
	}
	
	static class ResponseHeader {
		double start;
		double sample;
		int count;
		double reqStart;
		double reqWidth;
	}
	
	static class MinMax {
		int min;
		int max;
	}
}
