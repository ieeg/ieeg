/*
 * Copyright 2012 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.mefview.server;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.base.Optional;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Iterables;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtUserAce;
import edu.upenn.cis.braintrust.security.ExtWorldAce;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.IUserService.UsersAndTotalCount;
import edu.upenn.cis.braintrust.security.SessionStatus;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.UserIdSessionStatus;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.client.ClientFactory;
import edu.upenn.cis.db.mefview.shared.GetUsersResponse;
import edu.upenn.cis.db.mefview.shared.UserInfo;

public class EEGServiceImplTest {

	@Mock
	private IUserService userService;
	@Mock
	private ClientFactory clientFactory;
	@Mock
	private IDataSnapshotServer dataSnapshotServer;

	// Cannot match user directly because EEGServiceImpl maintains a User cache.
	@Captor
	private ArgumentCaptor<User> userArg;
	@Captor
	private ArgumentCaptor<Set<String>> includedTsRevIdsArg;
	@Captor
	private ArgumentCaptor<Set<String>> includedAnnRevIdsArg;

	private EEGServiceImpl service;

	private final List<String> channelIDs = newArrayList("tsRevId-1",
			"tsRevId-2",
			"tsRevId-3");
	private final BiMap<String, ExtPermission> displayStrToUserPerm = ImmutableBiMap
			.of(
					"Can read",
					CorePermDefs.READ_PERM,
					"Can edit",
					CorePermDefs.EDIT_PERM,
					"Is owner",
					CorePermDefs.OWNER_PERM);
	private final BiMap<String, ExtPermission> displayStrToProjPerm = ImmutableBiMap
			.copyOf(displayStrToUserPerm);
	private final BiMap<String, Optional<ExtPermission>> displayStrToWorldPerm = ImmutableBiMap
			.of(
					"None",
					Optional.<ExtPermission> absent(),
					"Can read",
					Optional.of(CorePermDefs.READ_PERM),
					"Can edit",
					Optional.of(CorePermDefs.EDIT_PERM));
	private static final String toolName = "tool";
	private static final String friendlyName = "friendly";
	private static final String studyRevId = "studyRevId";
	private static final String dataSnapshotRevId = "dataSnapshotRevId";
	private User user;
	private SessionToken userSessionId;
	private User adminUser;
	private SessionToken adminUserSessionId;
	private User aceUserOwner;
	private User aceUserReader;
	private User aceUserEditor;
	private SessionToken aceUserReaderSessionId;
	private SessionToken aceUserOwnerSessionId;

	private final TstObjectFactory btTstFac = new TstObjectFactory(false);

	@Before
	public void setupServiceMocks() throws AuthorizationException {
		MockitoAnnotations.initMocks(this);
		Map<UserId, Optional<User>> idToUser = newHashMap();
		user = btTstFac.newUserRegularEnabled();
		userSessionId = btTstFac.newSessionToken();
		adminUser = btTstFac.newUserAdminEnabled();
		idToUser.put(adminUser.getUserId(), Optional.of(adminUser));
		adminUserSessionId = btTstFac.newSessionToken();

		aceUserOwner = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserOwner.getUserId(), Optional.of(aceUserOwner));
		aceUserOwnerSessionId = btTstFac.newSessionToken();

		aceUserReader = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserReader.getUserId(), Optional.of(aceUserReader));
		aceUserReaderSessionId = btTstFac.newSessionToken();

		aceUserEditor = btTstFac.newUserRegularEnabled();
		idToUser.put(aceUserEditor.getUserId(), Optional.of(aceUserEditor));

		when(userService.findUserByUid(user.getUserId())).thenReturn(
				user);
		when(userService.findUserByUid(adminUser.getUserId()))
				.thenReturn(adminUser);
		when(userService.findUserByUid(aceUserOwner.getUserId()))
				.thenReturn(aceUserOwner);
		when(userService.findUserByUid(aceUserReader.getUserId()))
				.thenReturn(aceUserReader);
		when(userService.findUserByUid(aceUserEditor.getUserId()))
				.thenReturn(aceUserEditor);

		when(userService.findUsersByUid(anySetOf(UserId.class))).thenReturn(
				idToUser);

		when(userService.findUserByUsername(user.getUsername())).thenReturn(
				user);
		when(userService.findUserByUsername(adminUser.getUsername()))
				.thenReturn(adminUser);
		when(userService.findUserByUsername(aceUserOwner.getUsername()))
				.thenReturn(aceUserOwner);
		when(userService.findUserByUsername(aceUserReader.getUsername()))
				.thenReturn(aceUserReader);
		when(userService.findUserByUsername(aceUserEditor.getUsername()))
				.thenReturn(aceUserEditor);

		when(dataSnapshotServer.getUserIdSessionStatus(eq(userSessionId),
				any(Date.class))).thenReturn(new UserIdSessionStatus(
				user.getUserId(),
				SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(eq(adminUserSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						adminUser.getUserId(),
						SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(
				eq(aceUserReaderSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						aceUserReader.getUserId(),
						SessionStatus.ACTIVE));
		when(dataSnapshotServer.getUserIdSessionStatus(
				eq(aceUserOwnerSessionId),
				any(Date.class))).thenReturn(
				new UserIdSessionStatus(
						aceUserOwner.getUserId(),
						SessionStatus.ACTIVE));
		Map<
		String, String> ivProps = newHashMap();
		IvProps.setIvProps(ivProps);
		UserServiceFactory.setUserService(userService);
		DataSnapshotServerFactory.setDataSnapshotServer(dataSnapshotServer);

		// Since we rebuild this for each test, make sure we clear out the factory
		HabitatUserServiceFactory.setUserService(null);
		service = new EEGServiceImpl(userService, dataSnapshotServer);
	}

	@Test
	public void deriveDatasnapshotTestIncludeAnnotations()
			throws IllegalArgumentException,
			AuthorizationException, DuplicateNameException {

		final boolean includeAnnotations = true;

		service.deriveSnapshot(
				userSessionId,
				studyRevId,
				friendlyName,
				toolName,
				channelIDs, includeAnnotations);
		verify(dataSnapshotServer).deriveDataset(userArg.capture(),
				eq(studyRevId),
				eq(friendlyName), eq(toolName), includedTsRevIdsArg.capture(),
				includedAnnRevIdsArg.capture());
		assertEquals(user.getUserId(), userArg.getValue().getUserId());
		// DataSnapshotServer interprets null annRevId set to mean: include all
		// annotations in source dataSnapshot associated with a time series in
		// time series revId set.
		assertNull(includedAnnRevIdsArg.getValue());
		assertEquals(newHashSet(channelIDs),
				newHashSet(includedTsRevIdsArg.getValue()));
	}

	@Test
	public void deriveDatasnapshotTestDoNotIncludeAnnotations()
			throws IllegalArgumentException, AuthorizationException, DuplicateNameException {
		final boolean includeAnnotations = false;

		service.deriveSnapshot(
				userSessionId,
				studyRevId,
				friendlyName,
				toolName,
				channelIDs, includeAnnotations);
		verify(dataSnapshotServer).deriveDataset(userArg.capture(),
				eq(studyRevId),
				eq(friendlyName), eq(toolName), includedTsRevIdsArg.capture(),
				includedAnnRevIdsArg.capture());
		assertEquals(user.getUserId(), userArg.getValue().getUserId());
		// DataSnapshotServer interprets empty annRevId set to mean: include no
		// annotations.
		assertTrue(Iterables.isEmpty(includedAnnRevIdsArg.getValue()));
		assertEquals(newHashSet(channelIDs),
				newHashSet(includedTsRevIdsArg.getValue()));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getEnabledUsersTest() {
		final User enabledUser1 = btTstFac.newUserRegularEnabled();
		final User enabledUser2 = btTstFac.newUserRegularEnabled();
		final User enabledUser3 = btTstFac.newUserRegularEnabled();
		final UsersAndTotalCount uAc = new UsersAndTotalCount(newArrayList(
				enabledUser1, enabledUser2, enabledUser3), 15);
		when(
				userService.findEnabledUsers(anyInt(), anyInt(),
						anySetOf(UserId.class))).thenReturn(uAc);

		final GetUsersResponse usersResponse = service.getEnabledUsers(
				userSessionId,
				0,
				3,
				Collections.EMPTY_SET);
		assertEquals(15, usersResponse.getTotalCount());
		assertEquals(3, usersResponse.getUsers().size());
	}

	@Test
	public void getUserId() {
		final UserInfo userInfo = service.getUserId(user.getUsername(),
				user.getPassword());
		assertEquals(user.getUserId(), userInfo.getUid());
		assertEquals(user.getPhoto(), userInfo.getPhoto());
	}

	private ExtUserAce newUserAceWPerm(User user, String dsId,
			ExtPermission perm) {
		final ExtUserAce userAce = btTstFac.newExtUserAce(user, dsId);
		userAce.getPerms().add(perm);
		return userAce;
	}

	private ExtWorldAce newWorldAceWPerm(String dsId,
			ExtPermission perm) {
		final ExtWorldAce worldAce = btTstFac.newExtWorldAce(dsId);
		worldAce.getPerms().add(perm);
		return worldAce;
	}
}
