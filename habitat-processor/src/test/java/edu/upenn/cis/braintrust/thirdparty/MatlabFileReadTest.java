package edu.upenn.cis.braintrust.thirdparty;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.jmatio.io.MatFileHeader;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLChar;
import com.jmatio.types.MLDouble;
import com.jmatio.types.MLStructure;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.thirdparty.matlab.IeegMatFileReader;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;

public class MatlabFileReadTest {

	IeegMatFileReader reader;
	MatFileHeader header;

	private static final URL MAT_FILE_URL = MatlabFileReadTest.class
			.getClassLoader()
			.getResource("testMatFile.mat");

	@Before
	public void setUp() throws Exception {

		try {
			IvProps.setIvProps(ImmutableMap.of(IvProps.SOURCEPATH, "/"));
			FileReference file = new FileReference(MAT_FILE_URL.toURI().getPath());
			file.setDirBucketContainer(new DirectoryBucketContainer(null, ""));

			reader = new IeegMatFileReader(file);

			header = reader.getMatFileHeader();

		} catch (Exception e) {
			throw e;
		}
	}
	
	@After
	public void propsTearDown() {
		//Don't infect other tests with our props
		IvProps.setIvProps(null);
	}

	@Test
	public void testStruct() throws Exception {

		try {
			System.out.println(header.getDescription());
			Map<String, MLArray> test = reader.getContent();

			for (String s : test.keySet()) {
				System.out.println(s);
			}

			MLStructure testData = (MLStructure) test.get("testStruct");
			MLChar testString = (MLChar) testData.getField("name");
			String aux = testString.getString(0);

			assertEquals("Patient 1", aux);

		} catch (Exception e) {

			throw e;
		}
	}

	@Test
	public void testArray() throws Exception {
		Map<String, MLArray> test = reader.getContent();
		MLArray testArray = (MLArray) test.get("testArray");
		double[][] doubles = null;
		assertTrue(testArray.isDouble());
		doubles = ((MLDouble) testArray).getArray();
		final double[] a = { 1, 2, 3, 4 };
		assertArrayEquals(a, doubles[0], 0);
	}

}
