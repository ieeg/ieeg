/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.google.common.io.Files;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.db.habitat.io.FileSystemObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

/**
 * 
 * @author John Frommeyer
 *
 */
public class FileSystemObjectServerTest {

	private static final class FileAndFileReference {
		public final File file;
		public final FileReference fileRef;

		public FileAndFileReference(File file, FileReference fileRef) {
			this.file = file;
			this.fileRef = fileRef;
		}

	}

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private FileSystemObjectServer server = null;

	@Before
	public void createServer() {
		server = new FileSystemObjectServer(
				testFolder.getRoot().getPath());
	}

	@Test
	public void testCopyFrom() throws IOException {
		final String srcString = "The contents of the source file\n";
		final FileAndFileReference src = newFileAbsoluteContainer(srcString);
		final File srcFile = src.file;
		final FileReference srcHandle = src.fileRef;

		final FileAndFileReference target = newFileAbsoluteContainer(null);
		final File targetFile = target.file;
		final FileReference targetHandle = target.fileRef;

		IInputStream srcStream = null;
		IOutputStream targetStream = null;
		try {
			srcStream = server.openForInput(srcHandle);
			targetStream = server.openForOutput(targetHandle);

			targetStream.copyFrom(srcStream);
		} finally {
			if (srcStream != null) {
				srcStream.close();
			}
			if (targetStream != null) {
				targetStream.close();
			}
		}
		assertTrue(targetFile.exists());
		assertEquals(srcFile.length(), targetFile.length());
		String targetString = Files.toString(
				targetFile,
				Charset.defaultCharset());
		assertEquals(srcString, targetString);
	}

	@Test
	public void testCopyFromRelativeContainer() throws IOException {
		final String srcString = "The contents of the source file\n";
		final FileAndFileReference src = newFileRelativeContainer(srcString);
		final File srcFile = src.file;
		final FileReference srcHandle = src.fileRef;

		final FileAndFileReference target = newFileRelativeContainer(null);
		final File targetFile = target.file;
		final FileReference targetHandle = target.fileRef;

		IInputStream srcStream = null;
		IOutputStream targetStream = null;
		try {
			srcStream = server.openForInput(srcHandle);
			targetStream = server.openForOutput(targetHandle);

			targetStream.copyFrom(srcStream);
		} finally {
			if (srcStream != null) {
				srcStream.close();
			}
			if (targetStream != null) {
				targetStream.close();
			}
		}
		assertTrue(targetFile.exists());
		assertEquals(srcFile.length(), targetFile.length());
		String targetString = Files.toString(
				targetFile,
				Charset.defaultCharset());
		assertEquals(srcString, targetString);
	}

	@Test
	public void testDeleteItem() throws IOException {
		final FileAndFileReference toBeDeleted = newFileAbsoluteContainer("This file will be deleted.\n");
		final File toBeDeletedFile = toBeDeleted.file;
		assertTrue("Precondition failed", toBeDeletedFile.exists());
		final FileReference toBeDeleltedHandle = toBeDeleted.fileRef;

		server.deleteItem(toBeDeleltedHandle);
		assertFalse(toBeDeletedFile.exists());
	}

	@Test
	public void testDeleteItemRelativeContainer() throws IOException {
		final FileAndFileReference toBeDeleted = newFileRelativeContainer("This file will be deleted.\n");
		final File toBeDeletedFile = toBeDeleted.file;
		assertTrue("Precondition failed", toBeDeletedFile.exists());
		final FileReference toBeDeleltedHandle = toBeDeleted.fileRef;

		server.deleteItem(toBeDeleltedHandle);
		assertFalse(toBeDeletedFile.exists());
	}

	@Test
	public void testGetDirectoryContents() throws IOException {

		// Container has non-empty directory
		final File containerDirectory = testFolder.newFolder();
		final IStoredObjectContainer container = new CredentialedDirectoryBucketContainer(
				null,
				containerDirectory.getName());
		testGetDirectoryContents(container);

		// Container has empty directory
		final IStoredObjectContainer emptyContainer = new CredentialedDirectoryBucketContainer(
				null,
				"");
		testGetDirectoryContents(emptyContainer);

		// Container has null directory
		final IStoredObjectContainer nullContainer = new CredentialedDirectoryBucketContainer(
				null,
				null);
		testGetDirectoryContents(nullContainer);
	}

	/**
	 * Creates a directory under container and files in this directory. Checks
	 * that {@link
	 * FileSystemObjectServer.getDirectoryContents(IStoredObjectContainer,
	 * String)} returns the expected contents.
	 * 
	 * @param container
	 * @throws IOException
	 */
	private void testGetDirectoryContents(final IStoredObjectContainer container)
			throws IOException {
		final Path containerPath = container.getFileDirectory() == null
				? Paths.get(server.getBasePath())
				: Paths.get(server.getBasePath(),
						container.getFileDirectory());

		final Path contentsPath = containerPath
				.resolve("submittedDirectory-"
						+ UUID.randomUUID());
		contentsPath.toFile().mkdir();

		final Path inputFile1Path = contentsPath.resolve("inputFile1");
		inputFile1Path.toFile().createNewFile();
		final Path expectedInputFile1Path = containerPath
				.relativize(inputFile1Path);

		final Path inputFile2Path = contentsPath.resolve("inputFile2");
		inputFile2Path.toFile().createNewFile();
		final Path expectedInputFile2Path = containerPath
				.relativize(inputFile2Path);

		final Set<IStoredObjectReference> directoryContents = server
				.getDirectoryContents(
						container,
						contentsPath.getFileName().toString());

		assertEquals(2, directoryContents.size());
		boolean foundInputFile1 = false;
		boolean foundInputFile2 = false;
		for (IStoredObjectReference actualRef : directoryContents) {
			assertEquals(
					container.getFileDirectory(),
					actualRef.getDirBucketContainer().getFileDirectory());
			final String actualFilePath = actualRef.getFilePath();
			if (actualFilePath.equals(expectedInputFile1Path.toString())) {
				foundInputFile1 = true;
			} else if (actualFilePath.equals(expectedInputFile2Path.toString())) {
				foundInputFile2 = true;
			} else {
				assertFalse("Unexpected file path: " + actualFilePath, true);
			}
		}
		assertTrue(foundInputFile1);
		assertTrue(foundInputFile2);
	}

	private FileAndFileReference newFileAbsoluteContainer(
			@Nullable String contents) throws IOException {
		File testFile = null;
		if (contents == null) {
			testFile = new File(testFolder.getRoot(), BtUtil.newUuid());
			assertFalse(testFile.exists());
		} else {
			testFile = testFolder.newFile();
			try (final FileWriter testWriter = new FileWriter(testFile)) {
				testWriter.write(contents);
			}
		}
		final FileReference handle = new FileReference(
				(DirectoryBucketContainer) server.getDefaultContainer(),
				testFile.getName(),
				testFile.getName());
		return new FileAndFileReference(testFile, handle);

	}

	private FileAndFileReference newFileRelativeContainer(
			@Nullable String contents) throws IOException {
		final File containerFolder = testFolder.newFolder();
		final DirectoryBucketContainer container = new DirectoryBucketContainer(
				null, containerFolder.getName());
		File testFile = null;
		if (contents == null) {
			testFile = new File(containerFolder, BtUtil.newUuid());
			assertFalse(testFile.exists());
		} else {
			testFile = File.createTempFile("FSOSTest", null,
					containerFolder);
			try (final FileWriter testWriter = new FileWriter(testFile)) {
				testWriter.write(contents);
			}
		}
		final FileReference handle = new FileReference(
				container,
				testFile.getName(),
				testFile.getName());
		return new FileAndFileReference(testFile, handle);

	}

}
