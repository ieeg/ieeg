/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.kahana;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * 
 * @author John Frommeyer
 *
 */
public class KahanaParametersTest {

	@Before
	public void propsSetUp() {
		IvProps.setIvProps(ImmutableMap.of(IvProps.SOURCEPATH, "/"));
	}

	@After
	public void propsTearDown() {
		IvProps.setIvProps(null);
	}

	@Test
	public void setOrCheckParamsSamplerateRounding() throws IOException, URISyntaxException {
		final URL aParamsUrl = getClass().getResource("/a.params.txt");
		final URL bParamsUrl = getClass().getResource("/b.params.txt");

		final DirectoryBucketContainer container = new DirectoryBucketContainer(
				null,
				"");
		final FileReference aFile = new FileReference(
				container,
				aParamsUrl.toURI().getPath(),
				aParamsUrl.toURI().getPath());

		final FileReference bFile = new FileReference(
				container,
				bParamsUrl.toURI().getPath(),
				bParamsUrl.toURI().getPath());

		final KahanaParameters params = new KahanaParameters();

		final boolean aResult = params.setOrCheckParams(aFile);
		assertTrue(aResult);
		final boolean bResult = params.setOrCheckParams(bFile);
		assertTrue(bResult);
	}

	@Test
	public void setOrCheckParamsSamplerateInteger() throws IOException, URISyntaxException {
		final URL aParamsUrl = getClass().getResource(
				"/intSamplerate.params.txt");

		final DirectoryBucketContainer container = new DirectoryBucketContainer(
				null,
				"");
		final FileReference aFile = new FileReference(
				container,
				aParamsUrl.toURI().getPath(),
				aParamsUrl.toURI().getPath());

		final KahanaParameters params = new KahanaParameters();

		final boolean aResult = params.setOrCheckParams(aFile);
		assertTrue(aResult);
		assertEquals(Double.valueOf(500), params.getSamplingRate());
	}

	@Test 
	public void setMontage() throws FileNotFoundException, IOException, URISyntaxException {
		final URL bpPairsUrl = getClass().getResource(
				"/bpPairs.mat");

		final DirectoryBucketContainer container = new DirectoryBucketContainer(
				null,
				"");
		final FileReference bpPairsFile = new FileReference(
				container,
				bpPairsUrl.toURI().getPath(),
				bpPairsUrl.toURI().getPath());

		final KahanaParameters params = new KahanaParameters();
		params.setMontage(bpPairsFile);
		final EEGMontage actualMontage = params.getBpMontage();
		assertNotNull(actualMontage);
		final List<EEGMontagePair> actualPairs = actualMontage.getPairs();
		assertEquals(58,  actualPairs.size());
		
		assertEquals("LAF1", actualPairs.get(0).getEl1());
		assertEquals("LAF2", actualPairs.get(0).getEl2());
		
		assertEquals("LAF2", actualPairs.get(1).getEl1());
		assertEquals("LAF3", actualPairs.get(1).getEl2());
		
		assertEquals("RPT2", actualPairs.get(56).getEl1());
		assertEquals("RPT3", actualPairs.get(56).getEl2());
		
		assertEquals("RPT3", actualPairs.get(57).getEl1());
		assertEquals("RPT4", actualPairs.get(57).getEl2());
	}
}
