/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.db.habitat.io.FileSystemObjectServer;
import edu.upenn.cis.db.habitat.io.S3ObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.ICursor;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.mefview.shared.AWSContainerCredentials;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

@Ignore
public class TestIo {
	static FileSystemObjectServer fss;
	static S3ObjectServer sss;
	
	static CredentialedDirectoryBucketContainer container;
	
	@BeforeClass
	public static void classSetup() {
		IvProps.init(".");
		System.out.println("Initializing for " + IvProps.getIvProps().get(IvProps.SOURCEPATH) + " and " + IvProps.getDataBucket());

		container = new CredentialedDirectoryBucketContainer(
				"upload2ieeg",//IvProps.getDataBucket(),
				IvProps.getIvProps().get(IvProps.SOURCEPATH));
		
		container.setCredentials(new AWSContainerCredentials(AwsUtil.getCreds()));
		
		fss = new FileSystemObjectServer(IvProps.getIvProps().get(IvProps.SOURCEPATH));
		sss = new S3ObjectServer(IvProps.getDataBucket());
		
		fss.init();
		sss.init();
	}
	
	@Test
	public void testFileWriteReadDelete() throws IOException {
		IOutputStream ios = fss.openForOutput(new FileReference(container, "./my.fil", "./my.fil"));
		
		ByteBuffer temp = ByteBuffer.allocate(10240);
		
		for (int i = 0; i < 2000; i++)
			temp.putInt(i);
		
		temp.flip();
		System.out.println("Temp buffer is " + temp.remaining());
		
		ICursor cursor = fss.putBytes(ios, temp);
		
		temp.rewind();
		for (int i = 0; i < 2000; i++)
			temp.putInt(-i);
		
		temp.flip();
		System.out.println("Temp buffer is " + temp.remaining());

		cursor = ios.putBytes(cursor, temp);
		
		ios.close();

		IInputStream iis = fss.openForInput(new FileReference(container, "./my.fil", "./my.fil"));
		
		temp.rewind();
		
		cursor = iis.getBytes(temp, 8000);
		
		temp.flip();
		
		for (int i = 0; i < 2000; i++)
			assertEquals(temp.getInt(), i);

		temp.rewind();
		
		cursor = iis.getBytes(cursor, temp, 8000);
		
		temp.flip();
		
		for (int i = 0; i < 2000; i++)
			assertEquals(temp.getInt(), -i);
		
		iis.close();
	}
	
	@Test
	public void testDirectoryDump() throws IOException {
		Set<IStoredObjectReference> handles = fss.getDirectoryContents(
				new CredentialedDirectoryBucketContainer("upload2ieeg",
				IvProps.getIvProps().get(IvProps.SOURCEPATH)));
		
		boolean found = false;
		for (IStoredObjectReference item : handles) {
			System.out.println(item.getFilePath());
			if (item.getFilePath().contains("my.fil"))
				found = true;
		}
		
//		assertEquals(found, true);
	}
	
	@Test
	public void testS3WriteReadDelete() throws IOException {
		IOutputStream ios = sss.openForOutput(new FileReference(container, "./my.fil", "./my.fil"));
		
		ByteBuffer temp = ByteBuffer.allocate(10240);
		
		for (int i = 0; i < 2000; i++)
			temp.putInt(i);
		
		temp.flip();
		System.out.println("Temp buffer is " + temp.remaining());
		
		ICursor cursor = sss.putBytes(ios, temp);
		
		temp.rewind();
		for (int i = 0; i < 2000; i++)
			temp.putInt(-i);
		
		temp.flip();
		System.out.println("Temp buffer is " + temp.remaining());

		cursor = ios.putBytes(cursor, temp);
		
		ios.close();

		IInputStream iis = sss.openForInput(new FileReference(container, "./my.fil", "./my.fil"));
		
		temp.rewind();
		
		cursor = iis.getBytes(temp, 8000);
		
		temp.flip();
		
		System.out.println("Read " + temp.remaining() + " entries");
		
		for (int i = 0; i < 2000; i++)
			assertEquals(temp.getInt(), i);

		temp.rewind();
		
		cursor = iis.getBytes(cursor, temp, 8000);
		
		temp.flip();
		
		for (int i = 0; i < 2000; i++)
			assertEquals(temp.getInt(), -i);
		
		iis.close();
	}
	
	@Test
	public void testS3ContainerDump() throws IOException {
		Set<IStoredObjectReference> handles = sss.getDirectoryContents(
				new CredentialedDirectoryBucketContainer("upload2ieeg",
				IvProps.getIvProps().get(IvProps.SOURCEPATH)));
		
		System.out.println("Here");
		
		boolean found = false;
		for (IStoredObjectReference item : handles) {
			System.out.println(item.getFilePath());
			if (item.getFilePath().contains("my.fil"))
				found = true;
		}
	}

	@Test 
	public void testCopyFromS3() throws IOException {
		FileReference handle = new FileReference(
				new CredentialedDirectoryBucketContainer("upload2ieeg",
				"./"), 
				"medtronic/test/test_bioeng_2.ascii", "test_bioeng_2.ascii");
//				"medtronic/test/review.ascii", "review.ascii");
		IInputStream iis = sss.openForInput(handle);

		IOutputStream os = fss.openForOutput(handle);
		iis.copyTo(os.getOutputStream());
		
		iis.close();
		os.close();
	}

	@Test 
	public void testCopyToS3() throws IOException {
	}
}
