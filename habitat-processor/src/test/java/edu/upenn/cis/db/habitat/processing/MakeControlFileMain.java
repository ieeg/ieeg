/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction.TimeSeriesSettings;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

/**
 * 
 * @author John Frommeyer
 * 
 */
public final class MakeControlFileMain {
	public static final class MakeControlFileMainParams {

		@Parameter(names = {
				"-i",
				"--inputBucket" },
				required = true)
		private String inputBucket;

		@Parameter(names = {
				"-o",
				"--outputBucket" },
				required = true)
		private String outputBucket;

		@Parameter(names = {
				"-f",
				"--inputsFile" },
				required = true)
		private String inputsFile;

		@Parameter(names = {
				"-n",
				"--datasetName" },
				required = true)
		private String datasetName;

		@Parameter(names = {
				"-p",
				"--human" },
				arity = 1,
				required = true)
		private boolean isHuman = true;
		
		@Parameter(names = {
				"-t",
				"--datasetType" })
		private SnapType datasetType = SnapType.unknown;

		@Parameter(names = {
				"-g",
				"--institutionName" },
				required = true)
		private String institutionName;

		@Parameter(
				names = {
						"-h",
						"--help" },
				description = "print help message",
				help = true)
		private boolean help = false;

		public String getInputBucket() {
			return inputBucket;
		}

		public String getOutputBucket() {
			return outputBucket;
		}

		public String getInputsFile() {
			return inputsFile;
		}

		public String getDatasetName() {
			return datasetName;
		}
		
		public boolean isHuman() {
			return isHuman;
		}

		public SnapType getDatasetType() {
			return datasetType;
		}

		public String getInstitutionName() {
			return institutionName;
		}

		public boolean isHelp() {
			return help;
		}

	}

	public static void main(String[] args) throws IOException {
		final MakeControlFileMainParams mainParams = new MakeControlFileMainParams();

		final JCommander jc = new JCommander(mainParams);
		jc.setProgramName("makecontrolfile");
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			jc.usage();
			System.exit(2);
		}
		if (mainParams.isHelp()) {
			System.out
					.println('\n' + "makecontrolfile writes a control file for the given inputs");
			jc.usage();
			return;
		}

		final DirectoryBucketContainer inputContainer = new DirectoryBucketContainer(
				mainParams.getInputBucket(),
				"");

		final IStoredObjectContainer outputContainer = new DirectoryBucketContainer(
				mainParams.getOutputBucket(),
				"");

		ControlFileParameters controlFileParams =
				new ControlFileParameters();

		SnapshotParameters datasetParams = new SnapshotParameters(
				mainParams.getDatasetName(),
				null,
				mainParams.getDatasetType(),
				new ArrayList<String>());
		datasetParams
				.setOwner(mainParams.getInstitutionName());
		datasetParams.setHuman(mainParams.isHuman());
		
		controlFileParams.setSnapshotParameters(datasetParams);
		controlFileParams.getOutputs().add(outputContainer);
		

		final List<String> inputLines = Files
				.readAllLines(
						Paths.get(
								mainParams.getInputsFile()),
						Charset.forName("UTF-8"));
		int channelIdx = 0;
		for (final String inputLine : inputLines) {
			String trimmed = inputLine.trim();
			if (trimmed.startsWith("#")
					|| trimmed.endsWith("/")) {
				continue;
			}
			// Get rid of leading slashes
			trimmed = trimmed.replaceAll("^[\\\\/]+", "");
			String label = null;
			if (trimmed.endsWith(".mef")) {
				label = com.google.common.io.Files
						.getNameWithoutExtension(trimmed);
			}
			IStoredObjectReference handle = new FileReference(
					inputContainer,
					trimmed,
					trimmed);
			controlFileParams.getInputs().add(handle);
			if (label != null) {
				final TimeSeriesSettings tsSettings = new TimeSeriesSettings();
				tsSettings.setChannelLabel(label);
				tsSettings.setChannelIndex(channelIdx++);
				tsSettings.setKey(trimmed);
				datasetParams.getSettings().add(tsSettings);
				datasetParams.addChannelLabel(label);
			}

		}
		// Create the control file
		try (FileOutputStream fo = new FileOutputStream("work.control")) {
			ObjectMapper om = ObjectMapperFactory.getObjectMapper();
			om.writeValue(fo, controlFileParams);
		}
		;
		System.exit(0);
	}
}
