/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.upenn.cis.db.habitat.processing.actions.ReadSubmittedDirectoryAction.IDirectoryParser.IParsedDirectory;

/**
 * @author John Frommeyer
 *
 */
public class IeegDirectoryParserTest {

	private final IeegDirectoryParser parser = new IeegDirectoryParser();

	@Test
	public void testThreeComponents() {
		final String expectedOrg = "Hospital of X";
		final String expectedDataset = "x.dataset-001";
		final String dirString = "123/Animal_Data/"
				+ expectedOrg
				+ "/"
				+ expectedDataset;

		final IParsedDirectory parsed = parser.parseDirectory(dirString);
		assertFalse(parsed.isHuman().get());
		assertEquals(
				expectedOrg,
				parsed.getOrganizationName().get());
		assertFalse(parsed.getProjectName().isPresent());
		assertEquals(
				expectedDataset,
				parsed.getDatasetName().get());

	}

	@Test
	public void testFourComponents() {
		final String expectedOrg = "Hospital of X";
		final String expectedProject = "X Project";
		final String expectedDataset = "x.dataset-001";
		final String dirString = "91011/Human_Data/"
				+ expectedOrg
				+ "/"
				+ expectedProject
				+ "/"
				+ expectedDataset;
		final IParsedDirectory parsed = parser.parseDirectory(dirString);
		assertTrue(parsed.isHuman().get());
		assertEquals(
				expectedOrg,
				parsed.getOrganizationName().get());
		assertEquals(
				expectedProject,
				parsed.getProjectName().get());
		assertEquals(
				expectedDataset,
				parsed.getDatasetName().get());

	}

	@Test(expected = IllegalArgumentException.class)
	public void badDir() {
		parser.parseDirectory("456/Human_Data/something");
	}
}
