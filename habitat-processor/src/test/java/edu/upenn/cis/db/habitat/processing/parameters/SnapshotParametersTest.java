/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;

/**
 * 
 * @author John Frommeyer
 *
 */
public class SnapshotParametersTest {
	@Rule
	public final TemporaryFolder testFolder = new TemporaryFolder();

	@Test
	public void toAndfromJsonTest() throws JsonParseException,
			JsonMappingException,
			IOException {
		final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

		final String expectedTargetSnapshot = BtUtil.newUuid();
		final String expectedTargetSnapshotId = BtUtil.newUuid();
		final String expectedUnmappedChannel = BtUtil.newUuid();
		final String expectedMappedChannelOriginal = BtUtil.newUuid();
		final String expectedMappedChannelMapped = BtUtil.newUuid();

		final SnapType expectedSnapType = TstObjectFactory
				.randomEnum(SnapType.class);

		final SnapshotParameters expectedSnapshotParameters = new SnapshotParameters(
				expectedTargetSnapshot,
				expectedTargetSnapshotId,
				expectedSnapType,
				newArrayList(expectedUnmappedChannel));

		expectedSnapshotParameters.addChannelMapping(
				expectedMappedChannelOriginal,
				expectedMappedChannelMapped);

		final File toJsonFile = testFolder.newFile();
		mapper.writeValue(toJsonFile, expectedSnapshotParameters);

		// System.out.println(mapper.writeValueAsString(expectedSnapshotParameters));

		final InputStream jsonStream = new FileInputStream(toJsonFile);

		final SnapshotParameters fromJson = mapper.readValue(jsonStream,
				SnapshotParameters.class);

		assertEquals(
				expectedSnapshotParameters.getTargetSnapshot(),
				fromJson.getTargetSnapshot());
		assertEquals(
				expectedSnapshotParameters.getTargetSnapshotId(),
				fromJson.getTargetSnapshotId());
		assertEquals(
				expectedSnapshotParameters.getSnapshotType(),
				fromJson.getSnapshotType());
		assertEquals(
				newArrayList(expectedUnmappedChannel,
						expectedMappedChannelOriginal),
				fromJson.getChannelLabels());
		assertEquals(expectedUnmappedChannel,
				fromJson.getMappedChannel(expectedUnmappedChannel));
		assertEquals(expectedMappedChannelMapped,
				fromJson.getMappedChannel(expectedMappedChannelOriginal));

	}

	@Test
	public void noDefaultDestinationSet() throws IOException {
		final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

		final String expectedTargetSnapshot = BtUtil.newUuid();
		final String expectedTargetSnapshotId = BtUtil.newUuid();
		final SnapType expectedSnapType = TstObjectFactory
				.randomEnum(SnapType.class);
		final String expectedInstitution = BtUtil.newUuid();

		final SnapshotParameters expectedSnapshotParameters = new SnapshotParameters(
				expectedTargetSnapshot,
				expectedTargetSnapshotId,
				expectedSnapType,
				new ArrayList<String>());
		expectedSnapshotParameters.setHuman(true);
		expectedSnapshotParameters.setOwner(expectedInstitution);
		final String expectedDefaultDestination =
				"Human_Data/"
						+ expectedInstitution
						+ "/"
						+ expectedTargetSnapshot
						+ "/";

		final File toJsonFile = testFolder.newFile();
		mapper.writeValue(toJsonFile, expectedSnapshotParameters);

		// System.out.println(mapper.writeValueAsString(expectedSnapshotParameters));

		final InputStream jsonStream = new FileInputStream(toJsonFile);

		final SnapshotParameters fromJson = mapper.readValue(jsonStream,
				SnapshotParameters.class);

		assertEquals(
				expectedSnapshotParameters.getTargetSnapshot(),
				fromJson.getTargetSnapshot());
		assertEquals(
				expectedSnapshotParameters.getTargetSnapshotId(),
				fromJson.getTargetSnapshotId());
		assertEquals(
				expectedSnapshotParameters.getSnapshotType(),
				fromJson.getSnapshotType());
		assertEquals(
				expectedDefaultDestination,
				fromJson.getDefaultDestination());
	}

	@Test
	public void defaultDestinationSet() throws IOException {
		final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

		final String expectedTargetSnapshot = BtUtil.newUuid();
		final String expectedTargetSnapshotId = BtUtil.newUuid();
		final SnapType expectedSnapType = TstObjectFactory
				.randomEnum(SnapType.class);

		final SnapshotParameters expectedSnapshotParameters = new SnapshotParameters(
				expectedTargetSnapshot,
				expectedTargetSnapshotId,
				expectedSnapType,
				new ArrayList<String>());
		expectedSnapshotParameters.setHuman(true);
		final String defaultDestinationToSet = BtUtil.newUuid();
		final String expectedDefaultDestination = defaultDestinationToSet + "/";
		expectedSnapshotParameters
				.setDefaultDestination(defaultDestinationToSet);

		final File toJsonFile = testFolder.newFile();
		mapper.writeValue(toJsonFile, expectedSnapshotParameters);

		// System.out.println(mapper.writeValueAsString(expectedSnapshotParameters));

		final InputStream jsonStream = new FileInputStream(toJsonFile);

		final SnapshotParameters fromJson = mapper.readValue(jsonStream,
				SnapshotParameters.class);

		assertEquals(
				expectedSnapshotParameters.getTargetSnapshot(),
				fromJson.getTargetSnapshot());
		assertEquals(
				expectedSnapshotParameters.getTargetSnapshotId(),
				fromJson.getTargetSnapshotId());
		assertEquals(
				expectedSnapshotParameters.getSnapshotType(),
				fromJson.getSnapshotType());
		assertEquals(
				expectedDefaultDestination,
				fromJson.getDefaultDestination());
	}
}
