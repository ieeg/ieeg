/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.aws.AwsUtil;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.AWSContainerCredentials;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

@Ignore
public class TestProcessingPipeline {
	static IObjectServer fss;
	static IObjectServer sss;
	
	static CredentialedDirectoryBucketContainer container;
	
	static SnapshotProcessorDaemon daemon;
	
	private static final TstObjectFactory tstObjFactory = new TstObjectFactory();
	
	@BeforeClass
	public static void classSetup() throws UnknownHostException {
//		UserServiceFactory.setUserService(Mockito.mock(IUserService.class));
//		Set<Role> authedUsersRoles = new HashSet<> ();
//		authedUsersRoles.add(Role.USER);
//		authedUsersRoles.add(Role.ADMIN);
//		User authedUser = new User(
//				new UserId(42),
//				"btuser",
//				"password",
//				Status.ENABLED,
//				authedUsersRoles,
//				null,
//				"email",
//				new UserProfile("x", "y", "z", "w", null, null));
//
//		IUserService userService = Mockito.mock(IUserService.class);
//		Mockito.when(userService.findUserByUid(Mockito.any(UserId.class))).thenReturn(
//				authedUser);
		String path = Thread.currentThread().getContextClassLoader().getResource("ieegview.properties").getPath();
		IvProps.init(path);
		System.out.println("Initializing for " + 
				IvProps.getIvProps().get(IvProps.SOURCEPATH) + " and " + IvProps.getDataBucket());

		container = new CredentialedDirectoryBucketContainer(
				IvProps.getDataBucket(),
				IvProps.getIvProps().get(IvProps.SOURCEPATH));
		
		container.setCredentials(new AWSContainerCredentials(AwsUtil.getCreds()));
		
		fss = StorageFactory.getStagingServer();
		sss = StorageFactory.getPersistentServer();

		System.out.println("Initialized AWS and file server...");
		daemon = (SnapshotProcessorDaemon)TaskTrackerFactory.getTracker();//new SnapshotProcessorDaemon();
		daemon.open();
		System.out.println("Initialized daemon...");
	}
	
	private void createControlFile(FileReference handle) 
			throws JsonGenerationException, JsonMappingException, IOException {
		ControlFileParameters px = 
				new ControlFileParameters(
						new FileReference(handle.getDirBucketContainer(), 
								"mef/Report005.pdf", 
								"Mayo_IEED_data/Mayo_IEED_005/Report005.pdf"));
	
		SnapshotParameters snap = new SnapshotParameters(
				"Snapshot xT", 
				null, 
				SnapType.brainMapping, 
				new ArrayList<String>());
		snap.setOwner("Hospital of the University of Pennsylvania");
		px.setSnapshotParameters(snap);
//		px.setSnapshot("Mayo_IEED_005");
		
		px.getInputs().add(new FileReference(handle.getDirBucketContainer(), "edf.edf", "edf.edf"));
		px.getInputs().add(new FileReference(handle.getDirBucketContainer(), "mef/LTD1-100.mef", "Mayo_IEED_data/Mayo_IEED_005/IC_005/LTD1-100.mef"));
		px.getInputs().add(new FileReference(handle.getDirBucketContainer(), "mef/RTD1-100.mef", "Mayo_IEED_data/Mayo_IEED_005/IC_005/RTD1-100.mef"));
		
		px.getOutputs().add(new CredentialedDirectoryBucketContainer("dir1", IvProps.getDataBucket()));
		px.getOutputs().add(new CredentialedDirectoryBucketContainer("dir2", IvProps.getDataBucket()));
		
		snap.getChannelLabels().add("LTD1");
		snap.getChannelLabels().add("RTD1");
		
		// Create the control file
		IObjectServer server = StorageFactory.getBestServerFor(handle);
		IOutputStream os = server.openForOutput(handle);
//		FileOutputStream fo = new FileOutputStream("work.control");
		OutputStream fo = os.getOutputStream();
		ObjectMapper om = ObjectMapperFactory.getObjectMapper();
		om.writeValue(fo, px);
		os.close();
	}
	
	@Test
	public void testControlFile() throws IOException, InterruptedException {
		System.out.println("Test control file ... ");
		
		CredentialedDirectoryBucketContainer here = new CredentialedDirectoryBucketContainer(
				IvProps.getDataBucket(), IvProps.getIvProps().get(IvProps.SOURCEPATH));
		
		FileReference controlFile = new FileReference(here, "mef/work.control", "work.control");
		createControlFile(controlFile);
		
		
		daemon.addToQueue(new WorkItem("start", null,
				controlFile,
				Collections.<IStoredObjectReference>emptySet(),
				tstObjFactory.newUserRegularEnabled(),
				(new Date()).getTime(), 
				"application/control",
				"Control File", null));
		
		System.out.println("Requesting processing");
		daemon.workOnQueue();
	}

	//@Test
	public void testLayFile() throws IOException {
	}

//	@Test
	public void testMdtFile() throws IOException, InterruptedException {
		System.out.println("Test MDT file ... ");
		
		CredentialedDirectoryBucketContainer here = new CredentialedDirectoryBucketContainer(
				IvProps.getDataBucket(), IvProps.getIvProps().get(IvProps.SOURCEPATH));
		
		FileReference controlFile = new FileReference(here, 
				"medtronic/rich1/rich1_2013_09_08_05_06_59__MR_8.xml", 
				"rich1/rich1_2013_09_08_05_06_59__MR_8.xml");
		
		daemon.addToQueue(new WorkItem("start", null,
				controlFile,
				Collections.<IStoredObjectReference>emptySet(),
				tstObjFactory.newUserRegularEnabled(),
				(new Date()).getTime(), 
				"application/medtronic",
				"Medtronic", null));
		
		System.out.println("Requesting processing");
		daemon.workOnQueue();
	}

//	@Test
	public void testMdtDir() throws IOException, InterruptedException {
		System.out.println("Test MDT dir... ");
		
		CredentialedDirectoryBucketContainer here = new CredentialedDirectoryBucketContainer(
				IvProps.getDataBucket(), IvProps.getIvProps().get(IvProps.SOURCEPATH));
		
		FileReference controlFile = new FileReference(here, 
				"medtronic/rich1/.", 
				"medtronic/rich1/.");
		
		daemon.addToQueue(new WorkItem("start", null,
				controlFile,
				Collections.<IStoredObjectReference>emptySet(),
				tstObjFactory.newUserRegularEnabled(),
				(new Date()).getTime(), 
				"application/directory",
				"Medtronic", null));
		
		System.out.println("Requesting processing");
		daemon.workOnQueue();
	}
}
