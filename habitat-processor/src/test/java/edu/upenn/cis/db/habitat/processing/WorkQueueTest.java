/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Lists.newLinkedList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.model.UserEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TestDbCleaner;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * @author John Frommeyer
 *
 */
public class WorkQueueTest {

	private final static TstObjectFactory tstObjectFactory =
			new TstObjectFactory(false);
	private String bucket1 = BtUtil.newUuid();
	private String fileKey1 = BtUtil.newUuid();

	private String bucket2 = BtUtil.newUuid();
	private String fileKey2 = BtUtil.newUuid();
	private User creator1 = tstObjectFactory.newUserRegularEnabled();
	private User creator2 = tstObjectFactory.newUserRegularEnabled();

	@Before
	public void setUp() throws Exception {
		final TestDbCleaner cleaner = new TestDbCleaner(
				HibernateUtil.getSessionFactory(),
				HibernateUtil.getConfiguration());
		cleaner.deleteEverything();

		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			final UserEntity creator1Entity = tstObjectFactory
					.newUserEntity(creator1.getUserId());
			session.saveOrUpdate(creator1Entity);

			final UserEntity creator2Entity = tstObjectFactory
					.newUserEntity(creator2.getUserId());
			session.saveOrUpdate(creator2Entity);

			final ControlFileEntity controlFile1 = new ControlFileEntity(
					bucket1,
					fileKey1,
					creator1Entity);
			controlFile1.setCreateTime(
					new Date(
							controlFile1
									.getCreateTime()
									.getTime() - 10000));
			session.saveOrUpdate(controlFile1);

			final ControlFileEntity controlFile2 = new ControlFileEntity(
					bucket2,
					fileKey2,
					creator2Entity);
			session.saveOrUpdate(controlFile2);

			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Test
	public void testDbLoading() throws InterruptedException {

		IUserService mockUserService = mock(IUserService.class);
		when(mockUserService.findUserByUid(creator1.getUserId())).thenReturn(
				creator1);
		when(mockUserService.findUserByUid(creator2.getUserId())).thenReturn(
				creator2);
		UserServiceFactory.setUserService(mockUserService);
		final HibernateWorkQueue workQueue = new HibernateWorkQueue();
		final List<WorkItemSet> workItemSets = newLinkedList();
		while (workQueue.hasNext(0, 0)) {
			workItemSets.add(workQueue.getNext());
		}
		assertEquals(
				2,
				workItemSets.size());
		final WorkItemSet workSet1 = workItemSets.get(0);
		final IWorkItem item1 = getOnlyElement(
				workSet1,
				null);
		assertNotNull(item1);
		assertEquals(
				bucket1,
				item1.getMainHandle().getDirBucketContainer().getBucket());
		assertEquals(
				fileKey1,
				item1.getMainHandle().getObjectKey());

		final WorkItemSet workSet2 = workItemSets.get(1);
		final IWorkItem item2 = getOnlyElement(
				workSet2,
				null);
		assertNotNull(item2);
		assertEquals(
				bucket2,
				item2.getMainHandle().getDirBucketContainer().getBucket());
		assertEquals(
				fileKey2,
				item2.getMainHandle().getObjectKey());

		// Are the control files gone from the db?
		Session session = null;
		Transaction trx = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			trx = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<ControlFileEntity> allControlFiles = session
					.createQuery(
							"From ControlFile")
					.list();
			assertTrue(allControlFiles.isEmpty());
			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			throw e;
		} finally {
			PersistenceUtil.close(session);
		}
	}

}
