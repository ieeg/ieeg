/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.EDF.EDFParameters;
import edu.upenn.cis.eeg.ISimpleChannel;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.edf.EDFHeader;
import edu.upenn.cis.eeg.edf.EDFHeader.Builder;

/**
 * @author John Frommeyer
 *
 */
public class ImportEDFFilesActionTest {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void findUniqueChannelsTwoFilesWithDupInFile() {
		final String m = "findUniqueChannelsTwoFilesWithDupInFile(...)";
		final ImportEDFFilesAction action = new ImportEDFFilesAction(
				null,
				null,
				null,
				new ControlFileParameters());
		final String expectedLabel = "DUP";
		final Date firstFileStartDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(firstFileStartDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 0.365500;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin1 = -39.7460;
		final double expectedPhysicalMax1 = 85.83984;
		final int expectedDigitalMin1 = -407;
		final int expectedDigitalMax1 = 879;
		final int expectedSamplesPerBlock = 731;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		final double expectedPhysicalMin2 = -33.3984;
		final double expectedPhysicalMax2 = 87.50000;
		final int expectedDigitalMin2 = -342;
		final int expectedDigitalMax2 = 896;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();
		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);

		final Date secondFileStartTime = new Date(firstFileStartDate.getTime()
				+ (int) (header1.getDurationOfRecords() * 1000));
		final Builder builder2 = new EDFHeader.Builder(header1);
		builder2.setStartTime(secondFileStartTime);
		final EDFHeader header2 = builder2.build();
		final EDFParameters params2 = mock(EDFParameters.class);
		when(params2.getHeader()).thenReturn(header2);
		final ArgumentCaptor<EDFHeader> actualHeader1Captor = ArgumentCaptor
				.forClass(EDFHeader.class);
		final ArgumentCaptor<EDFHeader> actualHeader2Captor = ArgumentCaptor
				.forClass(EDFHeader.class);

		final List<EDFParameters> edfParams = newArrayList(
				params1,
				params2);
		final Collection<SimpleChannel> uniqueChannels = action
				.findUniqueChannels(edfParams);

		logger.debug("{}: Resulting channels: {}", m, uniqueChannels);
		verify(params1, times(2)).setHeader(actualHeader1Captor.capture());
		final List<EDFHeader> actualHeaders1 = actualHeader1Captor
				.getAllValues();

		verify(params2, times(2)).setHeader(actualHeader2Captor.capture());
		final List<EDFHeader> actualHeaders2 = actualHeader2Captor
				.getAllValues();

		// setHeader() will be called 4 times:
		// For second DUP in header1
		// For third DUP in header1 -- this is our actualHeader1. The final
		// header for the first file.
		// For second DUP in header2
		// For third DUP in header2 -- this is our actualHeader2. The final
		// header for the second file.
		final EDFHeader actualHeader1 = actualHeaders1.get(1);
		final EDFHeader actualHeader2 = actualHeaders2.get(1);

		assertEquals(3, uniqueChannels.size());
		checkHeaderDates(firstFileStartDate, actualHeader1);
		checkHeaderDates(secondFileStartTime, actualHeader2);

		final SimpleChannel actual1 = findChannelByLabel(uniqueChannels,
				expectedLabel);
		checkSimpleChannel(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual1);
		checkHeader(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				0,
				actualHeader1);
		checkHeader(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				0,
				actualHeader2);

		final SimpleChannel actual2 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_1");
		checkSimpleChannel(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual2);
		checkHeader(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				1,
				actualHeader1);
		checkHeader(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				1,
				actualHeader2);

		final SimpleChannel actual3 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_2");
		checkSimpleChannel(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				actual3);
		checkHeader(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				2,
				actualHeader1);
		checkHeader(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				2,
				actualHeader2);

	}

	@Test
	public void findUniqueChannelsTwoFilesWithNoDupInFile() {
		final String m = "findUniqueChannelsTwoFilesWithNoDupInFile(...)";
		final ImportEDFFilesAction action = new ImportEDFFilesAction();
		final String expectedLabel = "DUP";
		final Date firstFileStartDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(firstFileStartDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 0.365500;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin1 = -39.7460;
		final double expectedPhysicalMax1 = 85.83984;
		final int expectedDigitalMin1 = -407;
		final int expectedDigitalMax1 = 879;
		final int expectedSamplesPerBlock = 731;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder1.addChannel("X1",
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		final double expectedPhysicalMin2 = -33.3984;
		final double expectedPhysicalMax2 = 87.50000;
		final int expectedDigitalMin2 = -342;
		final int expectedDigitalMax2 = 896;
		builder1.addChannel("Y2",
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();
		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);

		final Date secondFileStartTime = new Date(firstFileStartDate.getTime()
				+ (int) (header1.getDurationOfRecords() * 1000));
		final Builder builder2 = new EDFHeader.Builder(secondFileStartTime);
		builder2.setDurationOfRecords(expectedDurationOfRecords);
		builder2.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		builder2.addChannel("X1",
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder2.addChannel("Y2",
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header2 = builder2.build();
		final EDFParameters params2 = mock(EDFParameters.class);
		when(params2.getHeader()).thenReturn(header2);

		final List<EDFParameters> edfParams = newArrayList(
				params1,
				params2);

		boolean exception = false;
		try {
			action
					.findUniqueChannels(edfParams);
		} catch (IllegalStateException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	@Test
	public void findUniqueChannelsOneFile() {
		final String m = "findUniqueChannelsOneFile(...)";
		final ControlFileParameters actionParameters = new ControlFileParameters();
		final ImportEDFFilesAction action = new ImportEDFFilesAction(
				null,
				null,
				null,
				actionParameters);
		final String expectedLabel = "DUP";
		final Date expectedDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(expectedDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 0.365500;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin1 = -39.7460;
		final double expectedPhysicalMax1 = 85.83984;
		final int expectedDigitalMin1 = -407;
		final int expectedDigitalMax1 = 879;
		final int expectedSamplesPerBlock = 731;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		final double expectedPhysicalMin2 = -33.3984;
		final double expectedPhysicalMax2 = 87.50000;
		final int expectedDigitalMin2 = -342;
		final int expectedDigitalMax2 = 896;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();

		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);
		final ArgumentCaptor<EDFHeader> actualHeaderCaptor = ArgumentCaptor
				.forClass(EDFHeader.class);

		final List<EDFParameters> edfParams = newArrayList(params1);
		final Collection<SimpleChannel> uniqueChannels = action
				.findUniqueChannels(edfParams);
		logger.debug("{}: Resulting channels: {}", m, uniqueChannels);
		verify(params1, times(2)).setHeader(actualHeaderCaptor.capture());
		final EDFHeader actualHeader = actualHeaderCaptor.getValue();

		assertEquals(3, uniqueChannels.size());
		checkHeaderDates(expectedDate, header1);

		final SimpleChannel actual1 = findChannelByLabel(uniqueChannels,
				expectedLabel);
		checkSimpleChannel(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual1);
		checkHeader(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				0,
				actualHeader);

		final SimpleChannel actual2 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_1");
		checkSimpleChannel(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual2);
		checkHeader(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				1,
				actualHeader);

		final SimpleChannel actual3 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_2");
		checkSimpleChannel(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				actual3);
		checkHeader(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				2,
				actualHeader);

	}

	@Test
	public void findUniqueChannelsOneFileWithChannelsSpecified() {
		final String m = "findUniqueChannelsOneFileWithChannelsSpecified(...)";
		final ControlFileParameters actionParameters = new ControlFileParameters();
		final ImportEDFFilesAction action = new ImportEDFFilesAction(
				null,
				null,
				null,
				actionParameters);
		final String expectedLabel = "DUP";
		actionParameters.getSnapshotParameters().addChannelLabel(expectedLabel);
		final Date expectedDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(expectedDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 0.365500;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin1 = -39.7460;
		final double expectedPhysicalMax1 = 85.83984;
		final int expectedDigitalMin1 = -407;
		final int expectedDigitalMax1 = 879;
		final int expectedSamplesPerBlock = 731;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		final double expectedPhysicalMin2 = -33.3984;
		final double expectedPhysicalMax2 = 87.50000;
		final int expectedDigitalMin2 = -342;
		final int expectedDigitalMax2 = 896;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();

		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);
		final ArgumentCaptor<EDFHeader> actualHeaderCaptor = ArgumentCaptor
				.forClass(EDFHeader.class);

		final List<EDFParameters> edfParams = newArrayList(params1);
		final Collection<SimpleChannel> uniqueChannels = action
				.findUniqueChannels(edfParams);
		logger.debug("{}: Resulting channels: {}", m, uniqueChannels);
		verify(params1, times(2)).setHeader(actualHeaderCaptor.capture());
		final EDFHeader actualHeader = actualHeaderCaptor.getValue();

		assertEquals(3, uniqueChannels.size());
		checkHeaderDates(expectedDate, header1);

		final SimpleChannel actual1 = findChannelByLabel(uniqueChannels,
				expectedLabel);
		checkSimpleChannel(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual1);
		checkHeader(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				0,
				actualHeader);

		final SimpleChannel actual2 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_1");
		checkSimpleChannel(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual2);
		checkHeader(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				1,
				actualHeader);

		final SimpleChannel actual3 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_2");
		checkSimpleChannel(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				actual3);
		checkHeader(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				2,
				actualHeader);

	}

	@Test
	public void findUniqueChannelsSampleRateChange() throws IOException {
		final String m = "findUniqueChannelsSampleRateChange(...)";
		final ImportEDFFilesAction action = new ImportEDFFilesAction();
		final String expectedLabel = "DUP";
		final Date firstFileStartDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(firstFileStartDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 1;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin = -6553.4;
		final double expectedPhysicalMax = 6553.4;
		final int expectedDigitalMin = -32768;
		final int expectedDigitalMax = 32767;
		final int expectedSamplesPerBlock = 400;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin,
				expectedPhysicalMax,
				expectedDigitalMin,
				expectedDigitalMax,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();
		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);

		final Date secondFileStartTime = new Date(firstFileStartDate.getTime()
				+ (int) (header1.getDurationOfRecords() * 1000));
		final int expectedSamplesPerBlock2 = 1600;
		final Builder builder2 = new EDFHeader.Builder(secondFileStartTime);
		builder2.setDurationOfRecords(expectedDurationOfRecords);
		builder2.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin,
				expectedPhysicalMax,
				expectedDigitalMin,
				expectedDigitalMax,
				"",
				expectedSamplesPerBlock2);
		final EDFHeader header2 = builder2.build();
		final EDFParameters params2 = mock(EDFParameters.class);
		when(params2.getHeader()).thenReturn(header2);

		final Date thirdFileStartTime = new Date(secondFileStartTime.getTime()
				+ (int) (header2.getDurationOfRecords() * 1000));
		final Builder builder3 = new EDFHeader.Builder(thirdFileStartTime);
		builder3.setDurationOfRecords(expectedDurationOfRecords);
		builder3.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin,
				expectedPhysicalMax,
				expectedDigitalMin,
				expectedDigitalMax,
				"",
				expectedSamplesPerBlock2);
		final EDFHeader header3 = builder3.build();
		final EDFParameters params3 = mock(EDFParameters.class);
		when(params3.getHeader()).thenReturn(header3);

		final List<EDFParameters> edfParams = newArrayList(
				params1,
				params2,
				params3);
		boolean exception = false;
		try {
			action
					.findUniqueChannels(edfParams);
		} catch (IllegalStateException e) {
			exception = true;
		}
		assertTrue(exception);
	}

	private void checkSimpleChannel(
			final String expectedLabel,
			final double expectedDurationOfRecords,
			final double expectedPhyMin,
			final double expectedPhyMax,
			final int expectedDigitalMin,
			final int expectedDigitalMax,
			final int expectedSamplesPerBlock,
			final SimpleChannel actualChannel) {
		assertEquals(expectedLabel, actualChannel.getChannelName());
		final double delta = 0.1;
		assertEquals(
				expectedSamplesPerBlock / expectedDurationOfRecords,
				actualChannel.getSamplingFrequency(),
				delta);
		final double[] expectedConversionParms1 = getConversionParams(
				expectedPhyMin,
				expectedPhyMax,
				expectedDigitalMin,
				expectedDigitalMax);
		assertEquals(expectedConversionParms1[0],
				actualChannel.getVoltageConversionFactor(),
				delta);
		assertEquals(expectedConversionParms1[1],
				actualChannel.getVoltageOffset(),
				delta);
	}

	private void checkHeader(
			final String expectedLabel,
			final double expectedDurationOfRecords,
			final double expectedPhyMin,
			final double expectedPhyMax,
			final int expectedDigitalMin,
			final int expectedDigitalMax,
			final int expectedSamplesPerBlock,
			final int channelIndex,
			final EDFHeader actualHeader) {
		assertEquals(expectedLabel,
				actualHeader.getChannelLabels()[channelIndex]);
		final double delta = 0.1;
		assertEquals(
				expectedDurationOfRecords,
				actualHeader.getDurationOfRecords(),
				delta);
		assertEquals(
				expectedPhyMin,
				actualHeader.getMinInUnits()[channelIndex].doubleValue(),
				delta);
		assertEquals(
				expectedPhyMax,
				actualHeader.getMaxInUnits()[channelIndex].doubleValue(),
				delta);
		assertEquals(
				expectedDigitalMin,
				actualHeader.getDigitalMin()[channelIndex].intValue());
		assertEquals(
				expectedDigitalMax,
				actualHeader.getDigitalMax()[channelIndex].intValue());
		assertEquals(
				expectedSamplesPerBlock,
				actualHeader.getNumberOfSamples()[channelIndex].intValue());
	}

	private void checkHeaderDates(Date expectedDate, EDFHeader actualHeader) {
		final SimpleDateFormat expectedStartDateFormat = new SimpleDateFormat(
				"dd.MM.yy");
		expectedStartDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		final String expectedStartDate = expectedStartDateFormat
				.format(expectedDate);
		assertEquals(expectedStartDate, actualHeader.getStartDate());

		final SimpleDateFormat expectedStartTimeFormat = new SimpleDateFormat(
				"HH.mm.ss");
		expectedStartTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		final String expectedStartTime = expectedStartTimeFormat
				.format(expectedDate);
		assertEquals(expectedStartTime, actualHeader.getStartTime());

	}

	private double[] getConversionParams(
			double physicalMin,
			double physicalMax,
			int digitalMin,
			int digitalMax) {
		final double[] params = new double[2];
		double conv = ((physicalMax - physicalMin) / (digitalMax - digitalMin));
		params[0] = conv;

		params[1] = (physicalMin / conv)
				- digitalMin;
		return params;
	}

	private SimpleChannel findChannelByLabel(Iterable<SimpleChannel> channels,
			String label) {
		return find(channels,
				compose(equalTo(label), ISimpleChannel.GET_CHANNEL_NAME));
	}

	@Test
	public void findUniqueChannelsOneFileWithChannelMappingSpecified() {
		final String m = "findUniqueChannelsOneFileWithChannelMappingSpecified(...)";
		final ControlFileParameters actionParameters = new ControlFileParameters();
		final ImportEDFFilesAction action = new ImportEDFFilesAction(
				null,
				null,
				null,
				actionParameters);
		final String expectedLabel = "DUP";
		final String expectedMappedLabel = "MAPPED_DUP";
		actionParameters.getSnapshotParameters().addChannelMapping(
				expectedLabel, expectedMappedLabel);
		final Date expectedDate = new Date();
		final Builder builder1 = new EDFHeader.Builder(expectedDate);
		builder1.setNumberOfRecords(1);
		final double expectedDurationOfRecords = 0.365500;
		builder1.setDurationOfRecords(expectedDurationOfRecords);
		final double expectedPhysicalMin1 = -39.7460;
		final double expectedPhysicalMax1 = 85.83984;
		final int expectedDigitalMin1 = -407;
		final int expectedDigitalMax1 = 879;
		final int expectedSamplesPerBlock = 731;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				"",
				expectedSamplesPerBlock);
		final double expectedPhysicalMin2 = -33.3984;
		final double expectedPhysicalMax2 = 87.50000;
		final int expectedDigitalMin2 = -342;
		final int expectedDigitalMax2 = 896;
		builder1.addChannel(expectedLabel,
				"",
				"uV",
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				"",
				expectedSamplesPerBlock);
		final EDFHeader header1 = builder1.build();

		final EDFParameters params1 = mock(EDFParameters.class);
		when(params1.getHeader()).thenReturn(header1);
		final ArgumentCaptor<EDFHeader> actualHeaderCaptor = ArgumentCaptor
				.forClass(EDFHeader.class);

		final List<EDFParameters> edfParams = newArrayList(params1);
		final Collection<SimpleChannel> uniqueChannels = action
				.findUniqueChannels(edfParams);
		logger.debug("{}: Resulting channels: {}", m, uniqueChannels);

		final Map<String, String> actualChannelMapping = action.parameters
				.getSnapshotParameters().getChannelMapping();

		verify(params1, times(2)).setHeader(actualHeaderCaptor.capture());
		final EDFHeader actualHeader = actualHeaderCaptor.getValue();

		assertEquals(3, uniqueChannels.size());
		checkHeaderDates(expectedDate, header1);

		final SimpleChannel actual1 = findChannelByLabel(uniqueChannels,
				expectedLabel);
		checkSimpleChannel(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual1);
		checkHeader(
				expectedLabel,
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				0,
				actualHeader);
		assertEquals(expectedMappedLabel,
				actualChannelMapping.get(expectedLabel));

		final SimpleChannel actual2 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_1");
		checkSimpleChannel(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				actual2);
		checkHeader(
				expectedLabel + "_1",
				expectedDurationOfRecords,
				expectedPhysicalMin1,
				expectedPhysicalMax1,
				expectedDigitalMin1,
				expectedDigitalMax1,
				expectedSamplesPerBlock,
				1,
				actualHeader);
		assertEquals(expectedMappedLabel + "_1",
				actualChannelMapping.get(expectedLabel + "_1"));

		final SimpleChannel actual3 = findChannelByLabel(uniqueChannels,
				expectedLabel + "_2");
		checkSimpleChannel(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				actual3);
		checkHeader(
				expectedLabel + "_2",
				expectedDurationOfRecords,
				expectedPhysicalMin2,
				expectedPhysicalMax2,
				expectedDigitalMin2,
				expectedDigitalMax2,
				expectedSamplesPerBlock,
				2,
				actualHeader);
		assertEquals(expectedMappedLabel + "_2",
				actualChannelMapping.get(expectedLabel + "_2"));

	}
}
