/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getLast;
import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import edu.upenn.cis.db.habitat.processing.actions.IAnnotationImporter.AnnotationImportException;
import edu.upenn.cis.db.habitat.processing.actions.NatusTextAnnotations.NatusTextAnnotation;

/**
 * @author John Frommeyer
 *
 */
public class NatusTextAnnotationsTest {

	@Test
	public void testLeadingZero() throws AnnotationImportException {
		final InputStream inputStream = NatusTextAnnotationsTest.class
				.getResourceAsStream("/natus-annotations.ent.txt");
		final NatusTextAnnotations actualAnnotations = NatusTextAnnotations
				.read(inputStream);
		checkCalendar(2017, 8, 12, 7, 42, 25,
				actualAnnotations.getCreationTime());
		checkCalendar(2017, 8, 12, 7, 42, 27,
				actualAnnotations.getRecordingStartTime());
		final List<NatusTextAnnotation> actual = actualAnnotations
				.getTextAnnotations();
		assertEquals(9, actual.size());
		final NatusTextAnnotation first = getFirst(actual, null);
		assertEquals(1, first.day);
		assertEquals(7, first.hourOfDay);
		assertEquals(42, first.minute);
		assertEquals(27, first.second);
		assertEquals("Beginning of Recording", first.text);
		final NatusTextAnnotation last = getLast(actual);
		assertEquals(1, last.day);
		assertEquals(8, last.hourOfDay);
		assertEquals(44, last.minute);
		assertEquals(8, last.second);
		assertEquals("End of Study", last.text);
	}

	@Test
	public void testUtf8() throws AnnotationImportException {
		final InputStream inputStream = NatusTextAnnotationsTest.class
				.getResourceAsStream("/natus-annotations.utf8.ent.txt");
		final NatusTextAnnotations natusTextAnnotations = NatusTextAnnotations
				.read(inputStream);
		checkAnnotation(natusTextAnnotations);

	}

	@Test
	public void testUtf16() throws
			AnnotationImportException {
		final InputStream inputStream = NatusTextAnnotationsTest.class
				.getResourceAsStream("/natus-annotations.utf16.ent.txt");
		final NatusTextAnnotations natusTextAnnotations = NatusTextAnnotations
				.read(inputStream);
		checkAnnotation(natusTextAnnotations);
	}
	
	@Test
	public void testCase2Utf16() throws
			AnnotationImportException {
		final InputStream inputStream = NatusTextAnnotationsTest.class
				.getResourceAsStream("/case2.utf16.ent.txt");
		final NatusTextAnnotations natusTextAnnotations = NatusTextAnnotations
				.read(inputStream);
		checkAnnotation(natusTextAnnotations);
	}

	private void checkAnnotation(NatusTextAnnotations actualAnnotations) {
		checkCalendar(2017, 8, 12, 10, 42, 25,
				actualAnnotations.getCreationTime());
		checkCalendar(2017, 8, 12, 10, 42, 27,
				actualAnnotations.getRecordingStartTime());
		final List<NatusTextAnnotation> actual = actualAnnotations
				.getTextAnnotations();
		assertEquals(9, actual.size());
		final NatusTextAnnotation first = getFirst(actual, null);
		assertEquals(1, first.day);
		assertEquals(10, first.hourOfDay);
		assertEquals(42, first.minute);
		assertEquals(27, first.second);
		assertEquals("Beginning of Recording", first.text);
		final NatusTextAnnotation last = getLast(actual);
		assertEquals(1, last.day);
		assertEquals(11, last.hourOfDay);
		assertEquals(44, last.minute);
		assertEquals(8, last.second);
		assertEquals("End of Study", last.text);
	}

	private void checkCalendar(int expectedYear,
			int expectedZeroBasedMonth,
			int expectedDayOfMonth,
			int expectedHourOfDay,
			int expectedMinute,
			int expectedSecond,
			Calendar actual) {
		assertEquals(expectedYear,
				actual.get(Calendar.YEAR));
		assertEquals(expectedZeroBasedMonth,
				actual.get(Calendar.MONTH));
		assertEquals(expectedDayOfMonth,
				actual.get(Calendar.DAY_OF_MONTH));
		assertEquals(expectedHourOfDay,
				actual.get(Calendar.HOUR_OF_DAY));
		assertEquals(expectedMinute,
				actual.get(Calendar.MINUTE));
		assertEquals(expectedSecond,
				actual.get(Calendar.SECOND));
	}

}
