/*
 * Copyright 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author John Frommeyer
 *
 */
public class RegisterActionsTest {

	@Test
	public void getMimeTypeString() {
//		String medtronicTxtType = RegisterActions
//				.getMimeType("215/medtronic/snapshot from dir test/xxxx_0000_00_11_22_33_44__xx_1.txt");
//		assertEquals(
//				"application/mdt-csv",
//				medtronicTxtType);
//
//		String medtronicXmlType = RegisterActions
//				.getMimeType("215/medtronic/snapshot from dir test/xxxx_0000_00_11_22_33_44__xx_1.xml");
//		assertEquals(
//				"application/medtronic",
//				medtronicXmlType);
//		
//		String medtronicXmlType2 = RegisterActions
//				.getMimeType("/215/medtronic/snapshot from dir test/xxxx_0000_00_11_22_33_44__xx_1.xml");
//		assertEquals(
//				"application/medtronic",
//				medtronicXmlType2);
//		
//		String medtronicXmlType3 = RegisterActions
//				.getMimeType("/medtronic/snapshot from dir test/xxxx_0000_00_11_22_33_44__xx_1.xml");
//		assertEquals(
//				"application/medtronic",
//				medtronicXmlType3);
	}
}
