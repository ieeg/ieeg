/*
 * Copyright 2017 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.getLast;
import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.testhelper.TstObjectFactory;
import edu.upenn.cis.db.habitat.processing.actions.IAnnotationImporter.AnnotationImportException;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * @author John Frommeyer
 *
 */
public class NatusAnnotationImporterTest {

	private final TstObjectFactory objFac = new TstObjectFactory(true);

	@Before
	public void propsSetUp() throws Exception {
		IvProps.setIvProps(ImmutableMap.of(IvProps.SOURCEPATH, "/"));
	}

	@After
	public void propsTearDown() {
		// Don't infect other tests with our props
		IvProps.setIvProps(null);
	}

	@Test
	public void testUtf8() throws URISyntaxException, AnnotationImportException {
		final FileReference annRef = resourceToFileReference("natus-annotations.utf8.ent.txt");
		final User creator = objFac.newUserRegularEnabled();
		final DataSnapshot dataset = objFac.newDataSnapshotDto();
		dataset.getTimeSeries().add(objFac.newTimeSeriesDto());
		final NatusAnnotationImporter importer = new NatusAnnotationImporter();
		final List<TsAnnotationDto> annotations = importer.importAnnotations(
				creator,
				dataset,
				annRef);
		checkAnnotation(annotations);

	}

	@Test
	public void testUtf16() throws URISyntaxException,
			AnnotationImportException {
		final FileReference annRef = resourceToFileReference("natus-annotations.utf16.ent.txt");
		final User creator = objFac.newUserRegularEnabled();
		final DataSnapshot dataset = objFac.newDataSnapshotDto();
		dataset.getTimeSeries().add(objFac.newTimeSeriesDto());
		final NatusAnnotationImporter importer = new NatusAnnotationImporter();
		final List<TsAnnotationDto> annotations = importer.importAnnotations(
				creator,
				dataset,
				annRef);
		checkAnnotation(annotations);

	}

	private void checkAnnotation(List<TsAnnotationDto> actual) {
		assertEquals(9, actual.size());
		final TsAnnotationDto first = getFirst(actual, null);
		assertEquals("Beginning of Recording", first.getType());
		assertEquals(0, first.getStartOffsetMicros().longValue());
		assertEquals(0, first.getEndOffsetMicros().longValue());
		TsAnnotationDto last = getLast(actual);
		assertEquals("End of Study", last.getType());
		assertEquals((33 + 60 + 8 + 3600) * 1_000_000L, last
				.getStartOffsetMicros().longValue());
		assertEquals((33 + 60 + 8 + 3600) * 1_000_000L, last
				.getEndOffsetMicros().longValue());
	}

	private FileReference resourceToFileReference(String resource)
			throws URISyntaxException {
		final String annPath = NatusAnnotationImporterTest.class
				.getClassLoader()
				.getResource(resource).toURI()
				.getPath();
		final FileReference annRef = new FileReference(annPath);
		annRef.setDirBucketContainer(new DirectoryBucketContainer(null, ""));
		return annRef;
	}
}
