/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing;

import java.util.LinkedList;
import java.util.Set;

import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class ActionQueue {
	LinkedList<IProcessingAction> queue;
	
	public ActionQueue() {
		queue = new LinkedList<IProcessingAction>();
	}
	
	public boolean hasNext() {
		return !queue.isEmpty();
	}

	/**
	 * Returns item @ head of queue, without popping it.
	 * Returns null if no item.
	 * 
	 * @return
	 */
	public IProcessingAction peek() {
		return queue.peek();
	}
	
	public IProcessingAction getNext() {
		return queue.poll();
	}
	
	public void add(IProcessingAction item) {
		queue.add(item);
	}
}
