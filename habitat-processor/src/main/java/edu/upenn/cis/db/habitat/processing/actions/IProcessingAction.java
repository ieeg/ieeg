/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.List;
import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Basic processing action for the ETL/workflow engine.
 * 
 * ** All actions are required to be idempotent **
 * ** This means re-running them will repeat the same results **
 * ** Since we do not have full transactional rollback, idempotence **
 * ** is necessary to allow for retries and failure recovery **
 * 
 * @author zives
 *
 */
public interface IProcessingAction extends JsonTyped {
	/**
	 * Execute the action
	 * 
	 * @param arguments Any optional runtime arguments (JSON-format)
	 * 
	 * @return State after execution
	 * @throws Exception
	 */
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception;
	
	/**
	 * Return the set of work items that triggered the workflow
	 * 
	 * @return
	 */
	public WorkItemSet getWorkItemSet();
	
	/**
	 * After the action has completed, what should we do next?
	 * Can be multiple items that all need to complete successfully.
	 * 
	 * @return
	 */
	public Set<IProcessingAction> getNextActions();

	/**
	 * After the action has completed, what work items has it
	 * produced for us to work on next?
	 * 
	 * @return
	 */
	public List<WorkItemSet> getNextWorkItems();
	
	/**
	 * Create a new instance of this action 
	 * 
	 * @param origin Original work item
	 * @param session IEEG connection session
	 * @param arguments Arguments to the action, during construction
	 * @return New action
	 */
	public IProcessingAction create(IMessageQueue queue, WorkItemSet origin, RemoteHabitatSession session, 
			Parameters arguments);
	
	/**
	 * Create a templated argument for the action
	 * 
	 * @return
	 */
	public Parameters getDefaultArguments();
	
	/**
	 * Can the action execute or is it missing a precondition?
	 * @return
	 */
	public boolean isReady();
	
	/**
	 * What are the parameters the action is using?
	 * @return
	 */
	public Parameters getCurrentParameters();
	
	/**
	 * Who created the data the action is working on?
	 * 
	 * @return
	 */
	public IUser getCreator();
	
	/**
	 * The message queue back to the requestor or log
	 * 
	 * @return
	 */
	public IMessageQueue getMessageQueue();
}
