package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CopyAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.habitat.processing.parameters.convert.WFDBParameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.WFDBParameters.WFDBSignal;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class WFDBImportAction extends BaseProcessingAction {

	/*
	 * Analyze fileformat is combination of two files {.hdr, .img}
	 * and can be converted to DICOM by xmedcon. 
	 * 
	 * XMedCon will convert Analyze format into a single DICOM file that may contain
	 * multiple slices.
	 * 
	 */

	public static final String NAME = "WFBDImportAction";
	private static final long BYTESPERSAMPLE = 2;
	private static final int SAMPLESPERREAD = 5000;
	final private Logger logger = LoggerFactory.getLogger(getClass());
	private List<IStoredObjectReference> tempHandles;
	private List<IOutputStream> tempOutputs;


	RemoteHabitatSession session;
	ControlFileParameters parameters;
	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();


	public WFDBImportAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
			ControlFileParameters parameters) {
		super(mQueue, origin);
		this.session = session;
		this.parameters = parameters;
	}

	public WFDBImportAction() {
		super(null, null);
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		/*
		 *	WFDB is format that contains a .hea and a .dat file. This fileformat is used by 
		 *	Physionet.org. We parse the header to get sampling rate, channel info and conversion
		 *	and create MEF File as well as capture meta information.
		 * 		
		 */

		// TODO: Allow for multiple segment files
		final String m = "execute(...)";
		logger.debug("Executing WFBDImportAction");
		
		
		if(getWorkItemSet().size()>1)
			logger.error("WFDBIMPORT: CURRENT IMPORTER ONLY SUPPORTS ONE .HEA/.DAT FILE PAIR PER DATASET (converting first)");
		
		//TODO: This should be fixed with support of collections with multiple datasets. 
		// Each .hea file is the equivalent of a dataset in our platform (multiple channels).
		
		for (IWorkItem item: getWorkItemSet()){
			IStoredObjectReference inFile = item.getMainHandle();
			WFDBParameters header = WFDBParameters.buildFromRef(inFile);

			String snapshotName = parameters.getTargetSnapshot();
			
			processSnapshot(header, snapshotName);
			
//			// Copy .hea file to dataset
//			String s3Key = item.getHandle().getObjectKey();
//			String fName = item.getHandle().getFilePath();
//			
//			FileReference handle = new FileReference((DirectoryBucketContainer)item.getHandle().getDirBucketContainer(),
//					s3Key, fName); 
			
			WorkItemSet newSet = new WorkItemSet(getWorkItemSet());//.getDescription(), getOriginatingItemSet().getJson());
			WorkItem newItem = new WorkItem(inFile.getFilePath(), 
					item.getParentId(), 
					inFile,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					BASIC_CATEGORIES.Metadata.name(), 
					"WFDB Header File", 
					null);
			newSet.add(newItem);
			
			getNextActions().add(new CopyAction(getMessageQueue(), newSet, this.session, this.parameters));
			
			
			
			break;
		}
		

		return STATE.SUCCEEDED;
	}

	private void processSnapshot(WFDBParameters header, String snapshotName){
		final String m = "processSnapshot(...)";
		
		List<String> channelIds = new ArrayList<String>();
        SnapshotParameters ssp = new SnapshotParameters("",
                null, SnapType.other, channelIds);
        
        ssp.merge(parameters);

        String institution = parameters.getSnapshotParameters().getInstitution();
        ssp.setInstitution(institution);
        
        String owner = parameters.getSnapshotParameters().getOwner();
        ssp.setOwner(owner);

        boolean isHuman = parameters.getSnapshotParameters().isHuman();
        ssp.setHuman(isHuman);

        for(WFDBSignal s:header.getSignals()){
        	channelIds.add(s.getDescription());
        }
        
     // Create snapshot as necessary
        IProcessingAction snap = new CreateSnapshotAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session,
                ssp);
        getNextActions().add(snap);

        // Map metadata to it as necessary
        IProcessingAction map = new MapMetadataAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session, ssp);
        snap.getNextActions().add(map);

		
        
        
        // Add Transcode action
        TranscodeFromWFDBAction transcode = new TranscodeFromWFDBAction(
        		getMessageQueue(), 
        		this.getWorkItemSet(),
                session, ssp, header);

        map.getNextActions().add(transcode);
        
        
        
		
	}
	

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new WFDBImportAction(mQueue, origin, session, (ControlFileParameters)arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}



}
