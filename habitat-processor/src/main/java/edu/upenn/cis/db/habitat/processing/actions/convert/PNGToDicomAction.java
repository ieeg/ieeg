package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.util.List;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.FileSystemObjectServer;
import edu.upenn.cis.db.habitat.io.FileSystemObjectServer.OutputFileStream;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CopyAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.ShellAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters.CommandLine;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class PNGToDicomAction extends BaseProcessingAction {

	public static final String NAME = "PNGToDICOMAction";
	RemoteHabitatSession session;
	ControlFileParameters parameters;
	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public PNGToDicomAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
            ControlFileParameters parameters) {
        super(mQueue, origin);
        this.session = session;
        this.parameters = parameters;
    }
	
	public PNGToDicomAction() {
        super(null, null);
    }

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		// TODO Auto-generated method stub
		
		logger.debug("Executing PNGToDicomAction");
		
		String tempFileFolder = "dirConvertImagesTemp";
		
		for (IWorkItem item: getWorkItemSet()){
			logger.debug("ConvertPNGToDICOM: " + item.toString());
			
			IStoredObjectReference inFile = item.getMainHandle();
			
			File ff = new File(inFile.getFilePath());

			IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
					
			IInputStream h = bestServer.openForInput(inFile);
			
			// Create temp Folder
			final DirectoryBucketContainer container = new DirectoryBucketContainer(
					null, 
					tempFileFolder);
			// Create local copy of file
			final FileReference pngFileHandle = new FileReference(
					container, 
					null, 
					ff.getName());
			IObjectServer localServer = StorageFactory.getStagingServer();
			IOutputStream hh = localServer.openForOutput(pngFileHandle);
			
			h.copyTo(hh.getOutputStream());
			
			List<String> cmdParams = new ArrayList<String>();
			String tempFileContainer = localServer.getDefaultContainer().getFileDirectory() + "/";
			String tempFilePath = tempFileContainer + tempFileFolder + "/"+ pngFileHandle.getFilePath();
			String tempOutputPath = tempFileContainer +"/" + tempFileFolder + "/"+ pngFileHandle.getFilePath() + ".dcm";
			
			cmdParams.add("-f " +  tempFilePath);
			cmdParams.add("-c dicom");
			cmdParams.add("-g");
			cmdParams.add("-o " + tempOutputPath);

			CommandLine t = new ShellParameters.CommandLine("/usr/local/xmedcon/bin/medcon", cmdParams);
			
			ShellParameters shellParams = new ShellParameters();
			shellParams.getCommandLines().add(t);
			

			ShellAction convertAction = new ShellAction(
					getMessageQueue(), this.getWorkItemSet(), this.session, shellParams);
			
			ParametersForSnapshotContent parms = new ParametersForSnapshotContent();
			parms.merge(this.parameters);
			
			WorkItemSet newSet = new WorkItemSet(getWorkItemSet());//.getDescription(), getOriginatingItemSet().getJson());
			
			FileReference handle = new FileReference(container, pngFileHandle.getFilePath() + ".dcm", pngFileHandle.getFilePath() + ".dcm"); 
			
			WorkItem newItem = new WorkItem(tempOutputPath, 
					item.getParentId(), 
					handle,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					"application/dcm", 
					"DICOM Image", 
					null);
			
			newSet.add(newItem);
//			
//			
			CopyAction copyAction = new CopyAction(getMessageQueue(), newSet, this.session, parms );
//			
			
			getNextActions().add(convertAction);
			getNextActions().add(copyAction);
			
		}
		
		
		
		
		
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new PNGToDicomAction(mQueue, origin, session, (ControlFileParameters)arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

	

}
