package edu.upenn.cis.db.habitat.processing.workflows;

import java.util.List;

import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;

public class ActionStep extends Step {
	String actionName;
	IProcessingAction action;
	
	public ActionStep(String stepName, List<Input> inputs, List<Output> outputs, String action) {
		super(stepName, inputs, outputs);

		actionName = action;
	}
	
	@Override
	public boolean bind(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		
		//  TODO: create named action
		return false;
	}
	@Override
	public boolean execute(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean revert(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

}
