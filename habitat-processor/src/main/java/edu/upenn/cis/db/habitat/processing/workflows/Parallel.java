package edu.upenn.cis.db.habitat.processing.workflows;

import java.util.List;
import java.util.Set;

public class Parallel extends Step {
	Set<Step> parallelSteps;

	public Parallel(String stepName, List<Input> inputs, List<Output> outputs, Set<Step> parallelSteps) {
		super(stepName, inputs, outputs);

		this.parallelSteps = parallelSteps;
	}

	@Override
	public boolean bind(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean execute(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean revert(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
