package edu.upenn.cis.db.habitat.processing.parameters.kahana;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.jmatio.types.MLArray;
import com.jmatio.types.MLCell;
import com.jmatio.types.MLChar;
import com.jmatio.types.MLStructure;

import edu.upenn.cis.braintrust.thirdparty.matlab.IeegMatFileReader;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.EEGMontagePair;
import edu.upenn.cis.db.mefview.shared.FileReference;

public class KahanaParameters {

	private Double samplingRate;
	private Double gain;
	private String dataFormat;
	private List<KahanaChannelParameters> chanParams = new ArrayList<KahanaChannelParameters>();
	private List<KahanaFileParameters> fileParams = new ArrayList<KahanaFileParameters>();
	private Boolean paramsSet = false;
	private EEGMontage bpMontage;

	public Boolean setOrCheckParams(FileReference inFile) throws IOException {

		IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);

		IInputStream inStream = bestServer.openForInput(inFile);

		BufferedInputStream inFStream =
				new BufferedInputStream(inStream.getInputStream());

		Properties ini = new Properties();
		ini.load(inFStream);
		inFStream.close();
		inStream.close();

		Double ga = Double.valueOf((String) ini.get("gain"));
		String df = (String) ini.get("dataformat");
		Double sf = new BigDecimal((String) ini.get("samplerate"))
				.setScale(2, RoundingMode.HALF_UP)
				.doubleValue();

		if (!paramsSet) {
			this.setGain(ga);
			this.setDataFormat(df);
			this.setSamplingRate(sf.doubleValue());
			paramsSet = true;
			return true;

		} else if (ga.equals(gain) && df.equals(dataFormat)
				&& sf.equals(samplingRate)) {
			// Params the same.
			return true;
		}

		// Params not the same.
		return false;

	}

	public Double getSamplingRate() {
		return samplingRate;
	}

	private void setSamplingRate(Double samplingRate) {
		this.samplingRate = samplingRate;
	}

	public Double getGain() {
		return gain;
	}

	private void setGain(Double gain) {
		this.gain = gain;
	}

	public String getDataFormat() {
		return dataFormat;
	}

	private void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

	public List<KahanaFileParameters> getFileParams() {
		return fileParams;
	}

	public List<KahanaChannelParameters> getChanParams() {
		return chanParams;
	}

	public EEGMontage getBpMontage() {
		return bpMontage;
	}

	public void setBpMontage(EEGMontage bpMontage) {
		this.bpMontage = bpMontage;
	}

	public boolean setMontage(FileReference inFile) throws FileNotFoundException, IOException {
		bpMontage = new EEGMontage("bpPairs");
		bpMontage.setIsPublic(true);
		
		IeegMatFileReader matFile = new IeegMatFileReader(inFile);

		MLCell namesArray = (MLCell) matFile.getContent().get(
				"allTagNames");

		List<EEGMontagePair> pairs = bpMontage.getPairs();		
		for (int i = 0; i < namesArray.getM(); i++){
			final MLChar mlChar1 = (MLChar) namesArray.get(i, 0);
			final String el1 = removeWhiteSpace(mlChar1.getString(0)); 
			final MLChar mlChar2 = (MLChar) namesArray.get(i, 1);
			final String el2 = removeWhiteSpace(mlChar2.getString(0));
			pairs.add(new EEGMontagePair(el1,el2));
		}		
		
		return true;
	}

	 private String removeWhiteSpace(String input){
		  input = input.replaceAll("\\t", "");
		  input = input.replaceAll("\\n", "");
		  return input;
	  }
	
}
