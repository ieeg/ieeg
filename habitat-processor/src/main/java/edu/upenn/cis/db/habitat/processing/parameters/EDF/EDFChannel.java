package edu.upenn.cis.db.habitat.processing.parameters.EDF;

public class EDFChannel {
  
  public String label;
  public double samplingRate;
  public double conversionFactor;
  public double offset;

  public EDFChannel(String lbl, double sf, double cf, double of){
    this.label = lbl;
    this.samplingRate = sf;
    this.conversionFactor = cf;
    this.offset = of;
  }
  
  public String toString(){
    return "Channel with label: " + this.label + 
        " - SamplingRate: " + this.samplingRate + " - ConversionFactor: " + this.conversionFactor;
  }



  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(conversionFactor);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((label == null) ? 0 : label.hashCode());
    temp = Double.doubleToLongBits(offset);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(samplingRate);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }



  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EDFChannel other = (EDFChannel) obj;
    if (Double.doubleToLongBits(conversionFactor) != Double
        .doubleToLongBits(other.conversionFactor))
      return false;
    if (label == null) {
      if (other.label != null)
        return false;
    } else if (!label.equals(other.label))
      return false;
    if (Double.doubleToLongBits(offset) != Double
        .doubleToLongBits(other.offset))
      return false;
    if (Double.doubleToLongBits(samplingRate) != Double
        .doubleToLongBits(other.samplingRate))
      return false;
    return true;
  }

}