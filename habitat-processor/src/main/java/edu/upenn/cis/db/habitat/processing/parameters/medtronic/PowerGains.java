/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

public class PowerGains {
	List<GainList> bandwidths;
	
	@XmlElements({
		@XmlElement(name="BW5Hz", type=GainList.class),
		@XmlElement(name="BW16Hz", type=GainList.class),
		@XmlElement(name="BW32Hz", type=GainList.class)
	})
	public List<GainList> getBandwidths() {
		return bandwidths;
	}

	public void setBandwidths(List<GainList> bandwidths) {
		this.bandwidths = bandwidths;
	}
	
	
/*
      <PowerGainActuals>
        <BW5Hz>
          <Gain500>156.862745098039</Gain500>
          <Gain1000>313.725490196078</Gain1000>
          <Gain2000>627.450980392157</Gain2000>
          <Gain4000>1254.90196078431</Gain4000>
        </BW5Hz>
        <BW16Hz>
          <Gain500>200</Gain500>
          <Gain1000>400</Gain1000>
          <Gain2000>800</Gain2000>
          <Gain4000>1600</Gain4000>
        </BW16Hz>
        <BW32Hz>
          <Gain500>321.56862745098</Gain500>
          <Gain1000>643.137254901961</Gain1000>
          <Gain2000>1286.27450980392</Gain2000>
          <Gain4000>2572.54901960784</Gain4000>
        </BW32Hz>
      </PowerGainActuals>
 */
}
