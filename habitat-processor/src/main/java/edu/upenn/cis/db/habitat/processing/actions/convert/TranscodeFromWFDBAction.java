package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.WFDBParameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.WFDBParameters.WFDBSignal;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodeFromWFDBAction extends BaseSnapshotProcessingAction {
	public static final class SimpleChannelMefWriter {
		private final SimpleChannel simpleChannel;
		private final MefWriter mefWriter;
		private boolean startedWriting = false;

		public SimpleChannelMefWriter(
				SimpleChannel simpleChannel,
				MefWriter mefWriter) {
			this.simpleChannel = checkNotNull(simpleChannel);
			this.mefWriter = checkNotNull(mefWriter);
		}

		public boolean isStartedWriting() {
			return startedWriting;
		}

		public void setStartedWriting(boolean startedWriting) {
			this.startedWriting = startedWriting;
		}

		public SimpleChannel getSimpleChannel() {
			return simpleChannel;
		}

		public MefWriter getMefWriter() {
			return mefWriter;
		}
	}
	
	public static final String NAME = "TranscodeFromWFDBAction";
	final private Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final int SAMPLESPERREAD = 5000;
	
	private WFDBParameters params;
	private List<SimpleChannel> chInfo; // List of Channel objects.
	private List<IStoredObjectReference> tempHandles;
	private List<IOutputStream> tempOutputs;
	private long nrSamplesWritten = 0;
	
	public TranscodeFromWFDBAction() {
		super();
	}

	public TranscodeFromWFDBAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters arguments,
			WFDBParameters params) {
		super(mQueue, origin, session, arguments);
		this.params = params;
	}
	
	
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";

		
		IObjectServer perServ = StorageFactory.getPersistentServer();
		IStoredObjectContainer perCont = perServ.getDefaultContainer();
		
		File headerFile = new File(params.getHandle().getFilePath());
		String datFileStr = headerFile.getName().replace(".hea", ".dat");
		File datFile = new File(headerFile.getParent(), datFileStr);

		IStoredObjectReference inFile2 = new FileReference((DirectoryBucketContainer) params.getHandle().getDirBucketContainer(), 
				datFile.getPath(), datFile.getPath());  
		
		IObjectServer bestServer = StorageFactory.getBestServerFor(inFile2);
		IInputStream rawStream = bestServer.openForInput(inFile2);
		
		
		// MEF Writers to each channel
		final Map<String, SimpleChannelMefWriter> labelToChannelWriter = newHashMap();
		// The actual output file handles
		tempHandles = new ArrayList<IStoredObjectReference>();
		// The actual output file streams
		tempOutputs = new ArrayList<IOutputStream>();
		
		chInfo = new ArrayList<SimpleChannel>();
		List<String> allLabels = new ArrayList<String>();
		
		// Make sure format/file is the same for all channels.
		// TODO Allow for different files in .hea info.
		Integer format = params.getSignals().get(0).getDataFormatID(); 
		String fileStr = params.getSignals().get(0).getFileName();

		// Check that all is okay.
		for (WFDBSignal h : params.getSignals()) {
			if(!(h.getDataFormatID().equals(format)))
				logger.error("WFDBACTION: We can currently not deal with multiple formats in the signals of a single header file. "
						+ "Transcode will likely fail.");
			if(!(h.getFileName().equals(fileStr)))
				logger.error("WFDBACTION: We can currently not deal with multiple files in the signals of a single header file. "
						+ "Transcode will likely fail.");
		}
		
		
		
		// Create MEF file for each channel;
		for (WFDBSignal h : params.getSignals()) {
			
			double baseUnit = getBaseScale(h);
			double mefScale = 1/(baseUnit * h.getADCGain());

			String uniqueLabel = h.getDescription();
			int iter = 0;
			if (labelToChannelWriter.keySet().contains(uniqueLabel))
				uniqueLabel = uniqueLabel + "_1";
			while(labelToChannelWriter.keySet().contains(uniqueLabel)){
				uniqueLabel = uniqueLabel.replace("_" + String.valueOf(iter), "_"
                    + String.valueOf(++iter));

			}
				
			allLabels.add(uniqueLabel);
			
			SimpleChannel ch = new SimpleChannel(uniqueLabel, params.getSamplingFreq(), 
					mefScale , (double)h.getADCBaseline()*mefScale);
			
			chInfo.add(ch);
			
			String fileName = ch.getChannelName() + ".mef";
//			File ff = new File(params.getHandle().getFilePath());
//			File file2 = new File(ff.getParent(), fileName);
			
			final IUser creator = getCreator();
			String destinationKey = fileName;
			
			SnapshotParameters parms = session.getSnapshotParametersFor((User)creator, 
					getCurrentParameters().getTargetSnapshot());
			
			final String defaultDestinationPrefix = parms
					.getDefaultDestination();
			final Path baseFilename = Paths.get(destinationKey).getFileName();
			if (defaultDestinationPrefix != null && baseFilename != null) {
					destinationKey = defaultDestinationPrefix + baseFilename.toString();
			} else {
				String prefix = (parms.isHuman()) ? "Human_Data"
						: "Animal_Data";

				String userIdPrefix = ((User) creator).getUserId()
						.getValue() + "/";
				if (destinationKey.startsWith(userIdPrefix)) {
					// Strip off user id which was added when using default
					// inbox
					destinationKey = destinationKey.substring(userIdPrefix
							.length());
				}
				if (!destinationKey.startsWith(prefix)) {
					destinationKey =
							prefix
									+ "/"
									+ (creator.getProfile()
											.getInstitutionName()
											.isPresent() ?
											creator.getProfile()
													.getInstitutionName()
													.get() :
											"everywhere")
									+ "/"
									+ destinationKey;

				}
				while (destinationKey.contains("//"))
					destinationKey = destinationKey.replace("//", "/");
			}
			
			
			IStoredObjectReference mefFile = new FileReference((DirectoryBucketContainer) perCont, 
					destinationKey,destinationKey);//file2.getPath(), file2.getPath()); 
			
			MefWriter newWriter = createMEFWriter(mefFile, ch);
			labelToChannelWriter.put(
					ch.getChannelName(),
					new SimpleChannelMefWriter(ch, newWriter));
		}

		// Open inputstream for each raw data file.
		long previousFileEndUutc = -1;
		try {

				

				// Set StartTime and initiate buffer to read from Raw File
				int nrSamples = params.getNrSamplesPerSignal();
				int totalNrSamples = nrSamples;
				
				long fileStartTS = params.Date2uUTC();

				try (final BufferedInputStream curStream = new BufferedInputStream(
						rawStream.getInputStream())) {


					long blockStartUutc = fileStartTS;
					// Iterate of data as long as there is data and write blocks
					// to MEF
					while (this.nrSamplesWritten < totalNrSamples) {

						Short[][] datByChan;
						switch(format){
							case 16: 
								datByChan = readFormat80(curStream, totalNrSamples, allLabels.size());
								break;
							case 212:
								datByChan = readFormat212(curStream, totalNrSamples, allLabels.size());
								break;
							default:
								throw(new Exception("TRANSCODEWFDB: Do not recognize data format for converter."));

							}
	
						int samplesInCurBlock = datByChan[0].length;
//						nrSamplesWritten += samplesInCurBlock * allLabels.size();
						
						// Iterate over Labels in file and write to correct
						// MEFWriter
						long[] stamps = new long[samplesInCurBlock];
						for (int iCh = 0; iCh < allLabels.size(); iCh++) {
							String curLab = allLabels.get(iCh);

							final SimpleChannelMefWriter curChWriter = labelToChannelWriter
									.get(curLab);
							if (curChWriter == null) {
								logger.error(
										"{}: Error: label not found: {} in {} in file {}",
										m, curLab, chInfo,
										params.toString());
								continue;
							}
							//
							SimpleChannel curInfo = curChWriter.getSimpleChannel();
							MefWriter curWriter = curChWriter.getMefWriter();


							// Create TimeStamp vector
							int[] samps = new int[samplesInCurBlock];
							stamps[0] = blockStartUutc;
							logger.trace("{} FileStartTime: {}", m,
									stamps[0]);
							samps[0] = (int) (curInfo.getVoltageOffset() + datByChan[iCh][0]);
							for (int j = 1; j < samplesInCurBlock; j++) {
								samps[j] = (int) (curInfo
										.getVoltageOffset() + datByChan[iCh][j]);
								//
								stamps[j] = (long) (stamps[j - 1] + 1000000 / curInfo
										.getSamplingFrequency());
							}
							curWriter.writeData(samps, stamps);
							curChWriter.setStartedWriting(true);
						}
						blockStartUutc += 1000000 * (1 / params.getSamplingFreq())
								* samplesInCurBlock;
					}
					previousFileEndUutc = blockStartUutc;
					logger.debug(m + ": Timestamp at end of file = "
							+ previousFileEndUutc);

				} catch (IOException e) {
					logger.error(m + ": Exception. Returning STATE.FAILED", e);
					return STATE.FAILED;
				} finally {
					rawStream.close();
				}
//			}
		} finally {
			logger.debug("{} Closing MEF Files", m);
			for (SimpleChannelMefWriter w : labelToChannelWriter.values()) {
				w.getMefWriter().close();
			}
			logger.debug("{} Closing IOutputStreams", m);
			for (IOutputStream s : tempOutputs) {
				try {
					s.close();
				} catch (IOException e) {
					logger.info(
							m
							+ ": Ignoring exception when closing IOutputStream",
							e);
				}
			}
		}

		ParametersForSnapshotContent snapP = new ParametersForSnapshotContent();

		snapP.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		snapP.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());

		for (IStoredObjectReference s : tempHandles) {
			snapP.getInputs().add(s);
		}

		getNextActions().add(
				new AddToSnapshotAction(
						getMessageQueue(), 
						this.getWorkItemSet(),
						getSession(),
						snapP));

		return STATE.SUCCEEDED;
				
	}
	

	private Short[][] readFormat212(BufferedInputStream curStream, int totalNrSamples, int nrChannels ) throws IOException{
		final String m = "Read:";
		
		// Making sure that we can completely assign buffer to values and even number of values per channel.
		int realSamplesPerRead = SAMPLESPERREAD - (SAMPLESPERREAD % (3*nrChannels));
		
		int samplesInCurBlock = realSamplesPerRead;
		int bPerBlock = (int) ((nrChannels * realSamplesPerRead* 3)/2);
		byte[] b = new byte[bPerBlock];
		
		try {
			
			if ((totalNrSamples - nrSamplesWritten) > realSamplesPerRead) {
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += realSamplesPerRead;
			}else{
				samplesInCurBlock = (int) (totalNrSamples - this.nrSamplesWritten);
				bPerBlock = (int) (nrChannels * samplesInCurBlock * 1.5);
				b = new byte[bPerBlock];
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += samplesInCurBlock;
				logger.debug(m
						+ ": Getting last partial block of data from WFDB Format 212 Data file.");
			}
			
			
			
		} catch (EOFException e) {
			logger.error(m
					+ ": Unexpected EOF while reading WFDB Format 212 data file." , e);
		}
		
		
		// Reorder
		Short[][] datByChan = new Short[chInfo.size()][samplesInCurBlock];
		int curIdx = 0;
		int iter = 0;
		for (int j = 0; j < samplesInCurBlock; j++) {
			for (int i = 0; i < chInfo.size(); i++) {
				
				if ( (iter & 1) == 0 ) {
					// use first byte and first 4 bits of second byte
					byte x = (byte) (((b[curIdx+1])&0x0F) << 4);
					datByChan[i][j] = (short) (b[curIdx]&0x00FF | ( (( x << 8) >> 4) )) ;
					
				} else { 
					// Use last 4 bits of first short and 8 bits of second byte.
					byte x = (byte) (((byte) (b[curIdx+1]&0xF0))>>4);
					datByChan[i][j] = (short)( ((short)(x<<8)) | ((short)(b[curIdx+2]&0x00FF)));
												
					curIdx += 3;
				}
					
				iter++;
			}
		}
		
		
		
		return datByChan;
	
	}
	
	
	private Short[][] readFormat80(BufferedInputStream curStream, int totalNrSamples, int nrChannels ) throws IOException{
		
		final String m = "Read:";
		// Get fixed block size or partial block if near EOF
		int samplesInCurBlock = SAMPLESPERREAD;
		int bPerBlock = (int) (nrChannels	* SAMPLESPERREAD * 2);
		byte[] b = new byte[bPerBlock];
		ShortBuffer shortBuf = ByteBuffer.wrap(b)
				.order(ByteOrder.LITTLE_ENDIAN)
				.asShortBuffer();
		try {
			
			
			if ((totalNrSamples - nrSamplesWritten) > SAMPLESPERREAD) {
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += SAMPLESPERREAD;
			} else
			{
				samplesInCurBlock = ((int) (totalNrSamples - this.nrSamplesWritten));
				bPerBlock = (int) (nrChannels * samplesInCurBlock * 2);
				b = new byte[bPerBlock];
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += samplesInCurBlock;
				logger.debug(m
						+ ": Getting last partial block of data from BNI Raw Data file.");
			}
		} catch (EOFException e) {
			logger.error(m
					+ ": Unexpected EOF while reading BNI Raw data file." , e);
		}

		shortBuf.rewind();

		// Reorder
		Short[][] datByChan = new Short[chInfo.size()][samplesInCurBlock];
		for (int j = 0; j < samplesInCurBlock; j++) {
			for (int i = 0; i < chInfo.size(); i++) {
				datByChan[i][j] = shortBuf.get();
			}
		}
		
		return datByChan;

	}
	
	private MefWriter createMEFWriter(
			IStoredObjectReference mefFile,
			SimpleChannel chInfo) throws IOException {

		System.out.println("Creating MEF File " + mefFile.getFilePath() + "...");

		IObjectServer perServ = StorageFactory.getPersistentServer();
		IOutputStream output = perServ.openForOutput(mefFile);

		FileOutputStream tempStream = (FileOutputStream) output
				.getOutputStream();
		tempHandles.add(mefFile);
		tempOutputs.add(output);

		MefHeader2 meh = new MefHeader2();

		meh.setChannelName(chInfo.getChannelName());
		meh.setSamplingFrequency(chInfo.getSamplingFrequency());
		meh.setHeaderLength((short)1024);

		// Set voltage conversion factor
		meh.setVoltageConversionFactor(chInfo.getVoltageConversionFactor());

		double secPerMEFBlock = Math.floor(6000 / chInfo.getSamplingFrequency());
		long discontinuity_threshold = 10000;

		MefWriter newMefWriter = new MefWriter(
				tempStream,
				chInfo.getChannelName(),
				meh,
				secPerMEFBlock,
				chInfo.getSamplingFrequency(),
				discontinuity_threshold);

		return newMefWriter;

	}

	public double getBaseScale(WFDBSignal signal){

		// Get Scale based on Units.
		double baseScale = 1;
		String curScale = signal.getUnits();
		if(curScale==null){
			// Assume microVolts
			return 0.001;
		}
		switch (curScale) {
		case "v":
		case "volt":
		case "volts":
			baseScale = 0.000001;
			break;
		case "mv":
		case "millivolt":
		case "millivolts":
			baseScale = 0.001;
			break;
		case "uv":
		case "microvolt":
		case "microvolts":
			baseScale = 1;
			break;
		default:
			//Assume microvolt
			baseScale = 0.001;
			break;
		}
		return baseScale;


	}


	
	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new TranscodeFromWFDBAction(mQueue, origin, session,
				(SnapshotParameters) arguments, null);
	}
	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
