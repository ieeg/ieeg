/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.shared.EegStudyType;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.dto.GwtEegStudy;
import edu.upenn.cis.braintrust.shared.dto.GwtExperiment;
import edu.upenn.cis.braintrust.shared.dto.GwtSnapshot;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Create a new snapshot according to the SnapshotParameters.  Populate
 * the SnapshotParameters with details so we can register later data files
 * with the Snapshot.
 * 
 * @author zives
 *
 */
public class CreateSnapshotAction extends BaseSnapshotProcessingAction {

	public static final String NAME = "CreateSnapshotAction";
	private final Logger logger = LoggerFactory.getLogger(getClass());
	

	CreateSnapshotAction() {
		super(null, null, null, null);
	}
	
	public CreateSnapshotAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, SnapshotParameters parameters) {
		super(mQueue, origin, session, parameters);
	}

	/**
	 * Takes the SnapshotParameters and creates a corresponding
	 * internal DTO called GwtSnapshot, then creates either
	 * a Study (for human data) or an Experiment (for animal data)
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";
		EegStudyType typ = EegStudyType.INTRAOPERATIVE_MONITORING;
		
		switch (getCurrentParameters().getSnapshotType()) {
		case brainMapping:
			typ = EegStudyType.BRAIN_MAPPING;
			break;
		case emuRecording:
			typ = EegStudyType.EMU_RECORDING;
			break;
		case cognitiveMonitoring:
			typ = EegStudyType.COGNITIVE;
			break;
		case intraoperativeMonitoring:
			typ = EegStudyType.INTRAOPERATIVE_MONITORING;
			break;
		default:
//			typ = null;
		}
		
		GwtSnapshot snapshotDesc;
		
		if (getCurrentParameters().isHuman()) {
			logger.debug("{}: Creating Study", m);
			snapshotDesc = new GwtEegStudy(getCurrentParameters().getTargetSnapshot(), null,
					typ, 0, (long)0, 0);
		} else {
			logger.debug("{}: Creating Experiment", m);
			snapshotDesc = new GwtExperiment(
					getCurrentParameters().getTargetSnapshot(), 
					null, 
					false,
					null, 
					null, 
					null,
					null,
					null,
					null,
					null);
		}
		
		session.registerSnapshotParametersFor((User)getWorkItemSet().iterator().next().getCreator(),
				getCurrentParameters().getTargetSnapshot(), getCurrentParameters());
		
		@SuppressWarnings("unused")
        String snap = session.createSnapshot((User)getWorkItemSet().iterator().next().getCreator(),
				getCurrentParameters().getTargetSnapshot(),
				snapshotDesc);
		
		this.getWorkItemSet().getNewSnapshots().add(snap);
		derived.add(new DerivedItem("snapshot", snap));

		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			Parameters arguments) {
		return new CreateSnapshotAction(mQueue, origin, session, (SnapshotParameters)arguments);
	}

	@Override
	public SnapshotParameters getDefaultArguments() {
		return new SnapshotParameters();
	}

	@Override
	public SnapshotParameters getCurrentParameters() {
		return (SnapshotParameters)super.getCurrentParameters();
	}
	
	@Override
	public boolean isReady() {
		return true;
	}
}
