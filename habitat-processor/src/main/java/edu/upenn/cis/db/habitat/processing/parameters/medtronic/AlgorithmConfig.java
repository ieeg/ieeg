/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

public class AlgorithmConfig {
/*
  <AlgorithmConfig>
    <DetectionState>Enabled</DetectionState>
    <Ch1>Disabled</Ch1>
    <Ch2>Disabled</Ch2>
    <Ch3>Disabled</Ch3>
    <Ch4>Enabled</Ch4>
    <UpdateRate>200ms</UpdateRate>
    <DetectionTrigger>True</DetectionTrigger>
    <DetectionClusterCountLimit>00:00:05</DetectionClusterCountLimit>
    <TermDurationConstraint>00:00:02</TermDurationConstraint>
    <OnsetDurationConstraint>00:00:10</OnsetDurationConstraint>
    <Coefficients>
      <W1>-1</W1>
      <W2>-1</W2>
      <W3>-1</W3>
      <W4>-1</W4>
      <b>-600</b>
      <NormConstB1>1</NormConstB1>
      <NormConstB2>1</NormConstB2>
      <NormConstB3>1</NormConstB3>
      <NormConstB4>1</NormConstB4>
      <NormConstA1>0</NormConstA1>
      <NormConstA2>0</NormConstA2>
      <NormConstA3>0</NormConstA3>
      <NormConstA4>0</NormConstA4>
    </Coefficients>
  </AlgorithmConfig>
 */
}
