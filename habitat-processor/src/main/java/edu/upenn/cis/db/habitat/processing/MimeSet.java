package edu.upenn.cis.db.habitat.processing;

import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

/*
 * Mimeset is set of files that have single mime-type and belong together.
 * I.E.  Analyze image file contain a header file and a data file.
 */
public class MimeSet {
	
	private String mimeType = "";
	private JsonTyped entryFile;
	private Set<JsonTyped> otherFiles;
	
	public MimeSet(JsonTyped entryFile) {
		super();
		this.entryFile = entryFile;
		this.otherFiles = new HashSet<JsonTyped>();
	}
	

	public MimeSet(String mimeType, JsonTyped entryFile, Set<JsonTyped> otherFiles) {
		super();
		this.mimeType = mimeType;
		this.entryFile = entryFile;
		this.otherFiles = otherFiles;
	}

	public String getMimeType() {
		return mimeType;
	}

	public JsonTyped getEntryFile() {
		return entryFile;
	}

	public Set<JsonTyped> getOtherFiles() {
		return otherFiles;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	

}
