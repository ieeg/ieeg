/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.List;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class ParametersForSnapshotContent extends Parameters {
	String targetSnapshot;
	String targetSnapshotId;

	@Override
	public void merge(Parameters parameters2) {
		super.merge(parameters2);
		
		if (parameters2 instanceof ParametersForSnapshotContent && 
				((ParametersForSnapshotContent)parameters2).targetSnapshot != null)
			targetSnapshot = ((ParametersForSnapshotContent)parameters2).targetSnapshot;
		
		if (parameters2 instanceof ParametersForSnapshotContent && 
				((ParametersForSnapshotContent)parameters2).targetSnapshotId != null)
			targetSnapshotId = ((ParametersForSnapshotContent)parameters2).targetSnapshotId;
		
	}

	public String getTargetSnapshot() {
		return targetSnapshot;
	}

	public void setTargetSnapshot(String targetSnapshot) {
		this.targetSnapshot = targetSnapshot;
	}

	public void setInputs(List<JsonTyped> inputs) {
		this.inputs = inputs;
	}

	public String getTargetSnapshotId() {
		return targetSnapshotId;
	}

	public void setTargetSnapshotId(String targetSnapshotId) {
		this.targetSnapshotId = targetSnapshotId;
	}
	
	
}
