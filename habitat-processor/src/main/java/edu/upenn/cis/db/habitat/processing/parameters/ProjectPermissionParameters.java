/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * 
 * @author John Frommeyer
 *
 */
public class ProjectPermissionParameters implements JsonTyped {

	String projectName = null;
	String projectId = null;
	String projectAdminsPerm = CorePermDefs.OWNER_PERMISSION_NAME;
	String projectTeamPerm = CorePermDefs.READ_PERMISSION_NAME;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectAdminsPerm() {
		return projectAdminsPerm;
	}

	public void setProjectAdminsPerm(String projectAdminsPerm) {
		this.projectAdminsPerm = checkPermString(projectAdminsPerm);
	}

	public String getProjectTeamPerm() {
		return projectTeamPerm;
	}

	public void setProjectTeamPerm(String projectTeamPerm) {
		this.projectTeamPerm = checkPermString(projectTeamPerm);
	}

	private String checkPermString(String perm) {
		checkNotNull(perm);
		if (!perm.equalsIgnoreCase(CorePermDefs.READ_PERMISSION_NAME)
				&& !perm.equalsIgnoreCase(CorePermDefs.EDIT_PERMISSION_NAME)
				&& !perm.equalsIgnoreCase(CorePermDefs.OWNER_PERMISSION_NAME)) {
			throw new IllegalArgumentException("Illegal permission string: ["
					+ perm + "]");
		} else {
			return perm;
		}
	}
}
