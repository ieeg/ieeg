/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.braintrust.shared.WorkerResponse;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.IWorkQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * Reads the contents of a directory and executes a workflow
 * 
 * @author zives
 *
 */
public class SendCompleteAction extends BaseSnapshotProcessingAction {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	public static final String NAME = "SendCompleteAction";
	public static final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

	public SendCompleteAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters parameters) {
		super(mQueue, origin, session, parameters);
	}

	SendCompleteAction() {
		super();
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";
		try {
			final WorkerRequest req = getCurrentParameters().getNotificationParms().getOriginalRequest();
			
			final IWorkQueue queue = getCurrentParameters().getNotificationParms().getWorkQueue();
			
			System.out.println("Sending completion to " + req.toString());
			
			getMessageQueue().logEvent(getCreator(), WorkerRequest.PORTAL, WorkerRequest.ETL, getWorkItemSet(), "The job has completed.");
			
			WorkerResponse response = new WorkerResponse(req.getUser(),
					req.getSource(), req.getChannel(), req.getId(),
					"complete", "ok", getCurrentParameters().getTargetSnapshot(),
					"");
			
			queue.sendDone(this.getWorkItemSet(), req.getSource(), 
					mapper.writeValueAsString(response));
			
			return STATE.SUCCEEDED;
		} catch (RuntimeException e) {
			logger.error(m + ": task failed", e);
			return STATE.FAILED;
		}
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			Parameters arguments) {
		return new SendCompleteAction(mQueue, origin, session,
				(SnapshotParameters) arguments);
	}

	@Override
	public SnapshotParameters getDefaultArguments() {
		return new SnapshotParameters();
	}

	@Override
	public SnapshotParameters getCurrentParameters() {
		return (SnapshotParameters) super.getCurrentParameters();
	}
	
}
