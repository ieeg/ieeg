package edu.upenn.cis.db.habitat.processing.workflows;

import java.io.Serializable;
import java.util.List;

/**
 * A step in a workflow
 * 
 * @author Zack Ives
 *
 */
public abstract class Step implements Serializable {
	String stepName;
	
	List<Input> inputs;
	List<Output> outputs;
	
	
	
	public Step(String stepName, List<Input> inputs, List<Output> outputs) {
		super();
		this.stepName = stepName;
		this.inputs = inputs;
		this.outputs = outputs;
	}

	public String getStepName() {
		return stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public List<Input> getInputs() {
		return inputs;
	}

	public void setInputs(List<Input> inputs) {
		this.inputs = inputs;
	}

	public List<Output> getOutputs() {
		return outputs;
	}

	public void setOutputs(List<Output> outputs) {
		this.outputs = outputs;
	}

	public abstract boolean bind(ExecutionContext context, ExecutionLog log);
	
	public abstract boolean execute(ExecutionContext context, ExecutionLog log);
	
	public abstract boolean revert(ExecutionContext context, ExecutionLog log);
}
