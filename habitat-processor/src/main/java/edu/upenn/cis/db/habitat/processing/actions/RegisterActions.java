/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.ActionMapper;
import edu.upenn.cis.db.habitat.processing.CreateParameters;
import edu.upenn.cis.db.habitat.processing.HasMimeType;
import edu.upenn.cis.db.habitat.processing.ITaskTracker;
import edu.upenn.cis.db.habitat.processing.IWorkFilter;
import edu.upenn.cis.db.habitat.processing.MimeSet;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.convert.AnalyzeToDicomAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.ImportBCI2000Action;
import edu.upenn.cis.db.habitat.processing.actions.convert.ImportBNI1FilesAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.ImportEDFFilesAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.ImportKahanaFilesAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.PNGToDicomAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.TranscodeFromBCI2000Action;
import edu.upenn.cis.db.habitat.processing.actions.convert.TranscodeFromEDFAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.TranscodeFromWFDBAction;
import edu.upenn.cis.db.habitat.processing.actions.convert.WFDBImportAction;
import edu.upenn.cis.db.habitat.processing.actions.export.TranscodeToEdfAction;
import edu.upenn.cis.db.habitat.processing.actions.export.TranscodeToMatAction;
import edu.upenn.cis.db.habitat.processing.actions.export.TranscodeToMefAction;
import edu.upenn.cis.db.habitat.processing.actions.export.TranscodeToRawAction;
import edu.upenn.cis.db.habitat.processing.actions.medtronic.ReadMdtXmlFileAction;
import edu.upenn.cis.db.habitat.processing.actions.medtronic.TranscodeMdtCsvFileAction;
import edu.upenn.cis.db.habitat.processing.actions.neuropace.ReadNPLayFileAction;
import edu.upenn.cis.db.habitat.processing.actions.persyst.ReadPersystLayFileAction;
import edu.upenn.cis.db.habitat.processing.actions.persyst.TranscodePersystDatFileAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.MimeTypeRecognizer;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class RegisterActions {
	private final static Logger logger = LoggerFactory
			.getLogger(RegisterActions.class);
//	private final static String medtronicRegex = "/?[^/]*/medtronic/.*";
//	private final static String neuropaceRegex = "/?[^/]*/neuropace/.*";
//	private final static String kahanaRegex = "/?[^/]*/kahana/.*";

	public static void registerKnownActions() {
		ActionFactory factory = ActionFactory.getFactory();

		factory.register(AnnotateAction.NAME, new AnnotateAction());
		factory.register(CopyAction.NAME, new CopyAction());
		factory.register(IfElseAction.NAME, new IfElseAction());
		factory.register(IndexTextAction.NAME, new IndexTextAction());
		factory.register(MapMetadataAction.NAME, new MapMetadataAction());
		factory.register(ReadControlFileAction.NAME,
				new ReadControlFileAction());
		factory.register(ReadMdtXmlFileAction.NAME, new ReadMdtXmlFileAction());
		factory.register(ReadSubmittedDirectoryAction.NAME,
				new ReadSubmittedDirectoryAction());
		factory.register(ShellAction.NAME, new ShellAction());
		factory.register(CreateSnapshotAction.NAME, new CreateSnapshotAction());
		factory.register(TranscodeToEdfAction.NAME, new TranscodeToEdfAction());
		factory.register(TranscodeToMatAction.NAME, new TranscodeToMatAction());
		factory.register(TranscodeToMefAction.NAME, new TranscodeToMefAction());
		factory.register(TranscodeToRawAction.NAME, new TranscodeToRawAction());
		factory.register(TranscodeMdtCsvFileAction.NAME,
				new TranscodeMdtCsvFileAction());
		factory.register(TranscodePersystDatFileAction.NAME,
				new TranscodePersystDatFileAction());
		factory.register(TranscodeFromEDFAction.NAME,
				new TranscodeFromEDFAction());
		factory.register(ImportEDFFilesAction.NAME, new ImportEDFFilesAction());
		factory.register(ImportKahanaFilesAction.NAME,
				new ImportKahanaFilesAction());
		factory.register(ImportBNI1FilesAction.NAME,
				new ImportBNI1FilesAction());
		factory.register(PNGToDicomAction.NAME,
				new PNGToDicomAction());
		factory.register(AnalyzeToDicomAction.NAME,
				new AnalyzeToDicomAction());
		factory.register(DeleteAction.NAME,
				new DeleteAction());
		factory.register(WFDBImportAction.NAME,
				new WFDBImportAction());
		factory.register(TranscodeFromWFDBAction.NAME, 
				new TranscodeFromWFDBAction());
		factory.register(ImportBCI2000Action.NAME,
				new ImportBCI2000Action());
		factory.register(TranscodeFromBCI2000Action.NAME, 
				new TranscodeFromBCI2000Action());
		factory.register(ImportAnnotationsAction.NAME, 
				new ImportAnnotationsAction());
		factory.register(ImportMontagesAction.NAME, 
				new ImportMontagesAction());

	}

	
	/*
	 * 
	 */
	public static MimeSet getMimePairType(JsonTyped fileSpec, List<JsonTyped> list){
		final String m = "getMimePairType(String, allFiles)";
		
		FileReference fil = (FileReference)fileSpec;
		
		ArrayList<String> allFileNames = new ArrayList<>();
		for (JsonTyped item: list){
			FileReference it = (FileReference)item;
			allFileNames.add(it.getFilePath());
		}
		
		MimeSet mSet = new MimeSet(fileSpec);
		String curName = fil.getFilePath().toLowerCase();
		if (curName.endsWith(".hdr")) {
			int idx = 0;
			for (String fn: allFileNames){
				String matchName = fn.toLowerCase();
				String[] s1 = matchName.split("\\.");
				String[] s2 = curName.split("\\.");
				
				
				if (s1[0].equals(s2[0]) && matchName.endsWith(".img")){
					mSet.getOtherFiles().add(list.get(idx));
					mSet.setMimeType("application/analyze");
					return mSet;				
				}
				idx++;
				
			}
			
		}
		if (curName.endsWith(".hea")) {
			// Check for .hea / .dat combo's 
			int idx = 0;
			for (String fn: allFileNames){
				String matchName = fn.toLowerCase();
				String[] s1 = matchName.split("\\.");
				String[] s2 = curName.split("\\.");
				
				
				if (s1[0].equals(s2[0]) && matchName.endsWith(".dat")){
					mSet.getOtherFiles().add(list.get(idx));
					mSet.setMimeType("application/x-wfdb");
					return mSet;				
				}
				idx++;
				
			}
		}
		
		if (curName.endsWith(".BRIK")) {
			// Check for .BRIK / .HEAD combo's
			logger.debug("{}: Found BRIK", m);

			int idx = 0;
			for (String fn: allFileNames){
				String matchName = fn.toLowerCase();
				String[] s1 = matchName.split("\\.");
				String[] s2 = curName.split("\\.");
				
				
				if (s1[0].equals(s2[0]) && matchName.endsWith(".HEAD")){
					logger.debug("{}: Found HEAD", m);
					mSet.getOtherFiles().add(list.get(idx));
					mSet.setMimeType("application/x-afni");
					return mSet;				
				}
				idx++;

			}
		}
		
		if (curName.endsWith(".bni")) {
			logger.debug("{}: Found bni1 header: {}", m, curName);
			final String curNameNoExt = curName.substring(0, curName.length() - 4);
			//matchName1 = curName without .bni
			final String matchName1 = curNameNoExt;
			//matchName2 = curName with .bni replaced by .eeg
			final String matchName2 = curNameNoExt.concat(".eeg");
			logger.debug("{}: Looking for datafile {} or {}", m, matchName1, matchName2);
			int idx = 0;
			for (String fn : allFileNames) {
				final String lowerFn = fn.toLowerCase();
				if (lowerFn.equals(matchName1) || lowerFn.equals(matchName2)) {
					logger.debug("{}: Found datafile {}", m, fn);
					mSet.getOtherFiles().add(list.get(idx));
					mSet.setMimeType(MimeTypeRecognizer.getMimeTypeForFile(curName));
					return mSet;
				}
				idx++;
			}
		}
		
		// Check for mimeType of single file if no Paired Set available.
		if (mSet.getMimeType().isEmpty()){
			String fileMimeType = getMimeType(fil);
			mSet.setMimeType(fileMimeType);
		}
		
		
		return mSet;
	}
	
	public static String getMimeType(IStoredObjectReference handle) {
		String file  = handle.getFilePath();
		
		Boolean hasContainer = !(handle.getDirBucketContainer()==null);
		
		final String m = "getMimeType(String)";
		if (file.toLowerCase().endsWith(".dat")){
			// Check for beginning of file
//			if (hasContainer){
				IInputStream rawStream = null;
				try{
					IObjectServer bestServer = StorageFactory.getBestServerFor(handle);
					rawStream = bestServer.openForInput(handle);
					
					byte[] b = new byte[8];
					ByteBuffer bb =  ByteBuffer.wrap(b);
					bb.rewind();
					rawStream.getBytes(bb, 8);
					bb.rewind();
					String test = new String(b);

					if(test.equals("BCI2000V") || test.equals("HeaderLe")){
						return "application/x-bci2000";
					}

				} catch (IOException e) {
					logger.error(m + ": Exception. Ca", e);
				}finally {
					try {
						rawStream.close();
					} catch (IOException e) {}
					
				}
//			}
			
			
			
		}
		/*
		if (file.toLowerCase().endsWith(".png"))
			return "image/png";
		if (file.toLowerCase().endsWith(".jpg"))
			return "image/jpeg";
		if (file.toLowerCase().endsWith(".xls"))
			return "application/excel";
		if (file.toLowerCase().endsWith(".doc"))
			return "application/msword";
		if (file.toLowerCase().endsWith(".mef"))
			return "application/mef";
		else if (file.toLowerCase().endsWith(".mat"))
			return "application/mat";
		else if (file.toLowerCase().matches(kahanaRegex))
			return "application/kahana";
		else if (file.toLowerCase().endsWith(".pdf"))
			return "application/pdf";
		else if (file.toLowerCase().endsWith(".edf"))
			return "application/edf";
		else if (file.toLowerCase().endsWith(".bni"))
			return "application/bni";
		else if (file.toLowerCase().matches(medtronicRegex) &&
				file.toLowerCase().endsWith(".xml"))
			return "application/medtronic";
		else if (file.toLowerCase().matches(medtronicRegex) &&
				(file.toLowerCase().endsWith(".text")
				|| file.toLowerCase().endsWith(".txt")))
			return "application/mdt-csv";
		else if (file.toLowerCase().matches(neuropaceRegex) &&
				(file.toLowerCase().endsWith(".lay")))
			return "application/neuropace";
		else if (file.toLowerCase().endsWith(".lay"))
			return "application/persyst";
		else if (file.toLowerCase().endsWith(".dcm"))
			return "application/dicom";
		else if (file.toLowerCase().endsWith(".nii"))
			return "application/x-nifti";
		else if (file.toLowerCase().endsWith(".nii.gz"))
			return "application/x-nifti-gz";
		else if (file.toLowerCase().endsWith(".mgz"))
			return "application/x-mgh-nmr-gz";
		else if (file.toLowerCase().endsWith(".mgh"))
			return "application/x-mgh-nmr";
		else if (file.toLowerCase().endsWith(".xml"))
			return "application/xml";
		else if (file.toLowerCase().endsWith(".txt")
				|| file.toLowerCase().endsWith(".text"))
			return "text/plain";
		else if (file.toLowerCase().endsWith(".html"))
			return "text/html";
		else if (file.toLowerCase().endsWith(".json"))
			return "application/json";
		else if (file.toLowerCase().endsWith(".control"))
			return "application/control";
		else if (file.toLowerCase().endsWith(".zip"))
			return "application/zip";
		else if (file.toLowerCase().endsWith(".mnc"))
			return "application/x-minc";
		else if (file.toLowerCase().endsWith(".ima"))
			return "application/x-siemens-ima";
		else if (file.toLowerCase().endsWith(".plx"))
			return "application/x-plexon";
		else if (file.toLowerCase().endsWith(".events"))
			return "application/x-blackfynn-events";
		 */
		return MimeTypeRecognizer.getMimeTypeForFile(file);
		

//		logger.debug("{}: Unknown mime type for file {}", m, file);
//		return "";

	}

	/**
	 * Returns TRUE if the Work Item's path ends with "." + the extension
	 * 
	 * @author zives
	 *
	 */
	public static class HasExtension implements IWorkFilter {
		String extension;

		public HasExtension(String ext) {
			this.extension = ext.toLowerCase();
		}

		@Override
		public boolean matches(IWorkItem item, ITaskTracker otherTasks) {
			return item.getMainHandle().getFilePath().toLowerCase()
					.endsWith("." + extension);
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof HasExtension))
				return false;
			else
				return (extension.equals(((HasExtension) o).extension));
		}

		@Override
		public int hashCode() {
			return extension.hashCode();
		}

		@Override
		public boolean matchesSet(WorkItemSet items, ITaskTracker otherTasks) {
			for (IWorkItem item : items)
				if (!item.getMainHandle().getFilePath().toLowerCase()
						.endsWith("." + extension))
					return false;
			return true;
		}

		@Override
		public boolean matchesSet(Set<String> items, ITaskTracker otherTasks) {
			for (String item : items)
				if (!item.toLowerCase().endsWith("." + extension))
					return false;
			return true;
		}
	}

	/**
	 * Given a set of work items upon which we are dependent, verify they have
	 * all succeeded before we have the go-ahead
	 * 
	 * @author zives
	 *
	 */
	public static class HasDependenciesSatisfied implements IWorkFilter {
		Set<WorkItemSet> dep;

		public HasDependenciesSatisfied(Set<WorkItemSet> dependsOn) {
			this.dep = dependsOn;
		}

		@Override
		public boolean matches(IWorkItem item, ITaskTracker otherTasks) {
			for (WorkItemSet dependency: dep)
				for (IProcessingAction act : otherTasks
						.getTransitiveClosureOfTasks(dependency))
					if (otherTasks.getStatus(act).getStatus() != STATE.SUCCEEDED)
						return false;

			return true;
		}

		@Override
		public boolean matchesSet(WorkItemSet item, ITaskTracker otherTasks) {
			for (WorkItemSet dependency: dep)
				for (IProcessingAction act : otherTasks
						.getTransitiveClosureOfTasks(dependency))
					if (otherTasks.getStatus(act).getStatus() != STATE.SUCCEEDED)
						return false;

			return true;
		}

		@Override
		public boolean matchesSet(Set<String> items, ITaskTracker otherTasks) {
			return false;
		}
	}

	public static class DefaultMapper implements CreateParameters {
		private final Logger logger = LoggerFactory.getLogger(getClass()
				.getName().replace('$', '.'));

		@Override
		public Parameters mapFrom(WorkItemSet operand, Parameters inputs,
				String operation) {
			final String m = "mapFrom(...)";
			Parameters parms = ActionFactory.getFactory().getDefaultParameters(
					operation);

			String parentColl = "";
			for (IWorkItem item : operand) {
				logger.debug("{}: Default mapping adding {} as input", m,
						item.getMainHandle());
				parms.getInputs().add(item.getMainHandle());

				if (item.getParentId() != null)
					parentColl = item.getParentId();
			}

			if (inputs != null) {
				if (logger.isTraceEnabled()) {
					try {
						final ObjectWriter om = ObjectMapperFactory
								.newPasswordFilteredWriter();
						final String inputsString = om
								.writeValueAsString(inputs);
						final String parmsString = om.writeValueAsString(parms);
						logger.trace("{}: Merging {} into {}", m, inputsString,
								parmsString);
					} catch (JsonProcessingException e) {
						logger.error(m
								+ ": Exception trying to write debug logging",
								e);
					}
				}
				parms.merge(inputs);
				if (logger.isTraceEnabled()) {
					try {
						final ObjectWriter om = ObjectMapperFactory
								.newPasswordFilteredWriter();
						final String parmsString = om.writeValueAsString(parms);
						logger.trace("{}: Result of merge {}", m, parmsString);
					} catch (JsonProcessingException e) {
						logger.error(m
								+ ": Exception trying to write debug logging",
								e);
					}
				}

			}

			if (parms instanceof ParametersForSnapshotContent
					&& !parentColl.isEmpty()) {
				((ParametersForSnapshotContent) parms)
						.setTargetSnapshot(parentColl);
			}

			if (inputs instanceof ParametersForSnapshotContent &&
					parms instanceof ParametersForSnapshotContent) {
				ParametersForSnapshotContent in = (ParametersForSnapshotContent) inputs;
				ParametersForSnapshotContent p = (ParametersForSnapshotContent) parms;

				p.setTargetSnapshot(in.getTargetSnapshot());
				p.setTargetSnapshotId(in.getTargetSnapshotId());
			}
			if (logger.isTraceEnabled()) {
				try {
					final ObjectWriter om = ObjectMapperFactory
							.newPasswordFilteredWriter();
					final String parmsString = om.writeValueAsString(parms);
					logger.trace("{}: Returning {}", m, parmsString);
				} catch (JsonProcessingException e) {
					logger.error(m
							+ ": Exception trying to write debug logging", e);
				}
			}
			return parms;
		}

	}

	public static void registerActionMappings() {

		ActionMapper mapper = ActionMapper.getMap();
		DefaultMapper defMap = new DefaultMapper();

		mapper.register(new HasMimeType("application/json"),
				MapMetadataAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/control"),
				ReadControlFileAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/medtronic"),
				ReadMdtXmlFileAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/directory"),
				ReadSubmittedDirectoryAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/neuropace"),
				ReadNPLayFileAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/persyst"),
				ReadPersystLayFileAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/zip"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/java"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/javascript"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/python"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("text/plain"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("text/html"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("analysis/linelength"),
				AnnotateAction.NAME,
				defMap);				 
		mapper.register(new HasMimeType("application/xml"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/excel"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/msword"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("image/png"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("image/jpeg"), CopyAction.NAME,
				defMap);
//		mapper.register(new HasExtension("pdf"), IndexTextAction.NAME,
//				defMap);
//		mapper.register(new HasExtension("json"), MapMetadataAction.NAME,
//				defMap);
//		mapper.register(new HasExtension("sh"), ShellAction.NAME,
//				defMap);
		mapper.register(new HasMimeType("application/control"),
				ReadControlFileAction.NAME,
				defMap);
//		mapper.register(new HasExtension("mef"), CopyAction.NAME, // AnnotateAction.NAME,
//				defMap);
		mapper.register(new HasMimeType("application/edf"), ImportEDFFilesAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/bni"), ImportBNI1FilesAction.NAME,
				defMap);
//		mapper.register(new HasExtension("mat"), CopyAction.NAME,
//				defMap);
//		mapper.register(new HasExtension("xslx"), CopyAction.NAME,
//				defMap);
		mapper.register(new HasMimeType("application/octet-stream"), CopyAction.NAME,
				defMap);///
		mapper.register(new HasMimeType("application/dicom"), CopyAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/analyze"), AnalyzeToDicomAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/x-wfdb"), WFDBImportAction.NAME,
				defMap);
		mapper.register(new HasMimeType("application/x-bci2000"), ImportBCI2000Action.NAME,
				defMap);
		mapper.register(new HasMimeType("application/x-nifti"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/x-nifti-gz"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/x-matlab-source"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/x-shell"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/perl"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/python"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/java"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/cpp"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/x-gzip"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/zip"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/json"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/pdf"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/mef"),
				CopyAction.NAME,defMap);
		mapper.register(new HasMimeType("application/mat"),
				CopyAction.NAME,defMap);
		mapper.register(
				new HasMimeType("application/x-natus-annotations"), 
				ImportAnnotationsAction.NAME, 
				defMap);
		mapper.register(
				new HasMimeType(MimeTypeRecognizer.GRASS_TWIN_ANNOTATION_MIME_TYPE_NAME), 
				ImportAnnotationsAction.NAME, 
				defMap);
		mapper.register(
				new HasMimeType(MimeTypeRecognizer.IEEG_JSON_ANNOTATION_MIME_TYPE_NAME), 
				ImportAnnotationsAction.NAME, 
				defMap);
		mapper.register(
				new HasMimeType(MimeTypeRecognizer.IEEG_JSON_MONTAGE_MIME_TYPE_NAME), 
				ImportMontagesAction.NAME, 
				defMap);
	}
}
