package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.io.HabitatMefWriter;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.BNI1Parameters;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;

public class TranscodeFromBNI1Action extends BaseSnapshotProcessingAction {
	public static final class SimpleChannelMefWriter {
		private final SimpleChannel simpleChannel;
		private final HabitatMefWriter mefWriter;
		private boolean startedWriting = false;

		public SimpleChannelMefWriter(
				SimpleChannel simpleChannel,
				HabitatMefWriter mefWriter) {
			this.simpleChannel = checkNotNull(simpleChannel);
			this.mefWriter = checkNotNull(mefWriter);
		}

		public boolean isStartedWriting() {
			return startedWriting;
		}

		public void setStartedWriting(boolean startedWriting) {
			this.startedWriting = startedWriting;
		}

		public SimpleChannel getSimpleChannel() {
			return simpleChannel;
		}

		public HabitatMefWriter getMefWriter() {
			return mefWriter;
		}
	}

	public static final String NAME = "TranscodeFromBNI1Action";
	private static final long BYTESPERSAMPLE = 2;
	private static final int SAMPLESPERREAD = 5000;
	final private Logger logger = LoggerFactory.getLogger(getClass());

	private List<BNI1Parameters> params;
	private List<SimpleChannel> chInfo; // List of Channel objects.
	private List<IStoredObjectReference> tempHandles;

	public TranscodeFromBNI1Action() {
		super();
	}

	public TranscodeFromBNI1Action(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters arguments, List<SimpleChannel> chInfo,
			List<BNI1Parameters> params) {
		super(mQueue, origin, session, arguments);
		this.chInfo = chInfo;
		this.params = params;
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		logger.debug("Executing TranscodeFromBNI1 for "
				+ getCurrentParameters().getInputs());

		if (params.isEmpty()) {
			logger.debug("{}: No BNI1 to transcode.", m);
			return STATE.SUCCEEDED;
		}

		// *** Sort EDFParameter lists based on startDate of File. ***
		Collections.sort(params);
		final long recordingStartUutc = params.get(0).Date2uUTC();

		// Display sorted startTimestamps
		for (BNI1Parameters p : params) {
			long retTime = p.Date2uUTC();
			logger.debug(
					"{}: Reading startDate/time: {} - From: {}-{} from file {}",
					m,
					retTime,
					p.getStartDate(),
					p.getStartTime(),
					p.getHandle());
		}
		// **********

		// MEF Writers to each channel
		final Map<String, SimpleChannelMefWriter> labelToChannelWriter = newHashMap();
		// The actual output file handles
		tempHandles = new ArrayList<IStoredObjectReference>(chInfo.size());

		// Create MEF file for each channel;
		for (SimpleChannel ch : chInfo) {
			HabitatMefWriter newWriter = createMEFWriter(getCurrentParameters()
					.getTargetSnapshot(), ch);
			labelToChannelWriter.put(
					ch.getChannelName(),
					new SimpleChannelMefWriter(ch, newWriter));
		}

		// Open inputstream for each raw data file.
		long previousFileEndUutc = -1;
		try {
			// Iterate over sorted BNI-Files
			for (BNI1Parameters h : params) {

				logger.debug("{}: Writing for BNI1 file: {}", m, h.getHandle()
						.getFilePath());
				logger.debug(m + ": Timestamp at beginning of file = "
						+ h.Date2uUTC());

				// Get FileName for the raw data file associated with the BNI
				// file and
				// open Stream.
				File s3Key = new File(h.getHandle().getFilePath());
				String s3Str = s3Key.getParentFile().toString()
						.concat("/" + h.getDataFileName());

				FileReference rawFile = new FileReference(h.getHandle()
						.getDirBucketContainer(), s3Str, s3Str);
				IObjectServer bestServer = StorageFactory
						.getBestServerFor(rawFile);
				IInputStream rawStream = bestServer.openForInput(rawFile);

				// Set StartTime and initiate buffer to read from Raw File
				long fileLength = rawStream.getLength();
				int totalNrSamples = (int) (fileLength / BYTESPERSAMPLE / h
						.getChannelLabels().size());

				long fileStartTS = h.Date2uUTC();
				int bPerBlock = (int) (h.getChannelLabels().size()
						* SAMPLESPERREAD * 2);

				byte[] b = new byte[bPerBlock];
				ShortBuffer shortBuf =
						ByteBuffer.wrap(b)
								.order(ByteOrder.LITTLE_ENDIAN)
								.asShortBuffer();

				try (final BufferedInputStream curStream = new BufferedInputStream(
						rawStream.getInputStream())) {

					int nrSamplesWritten = 0;
					final long overlapMicros = previousFileEndUutc
							- fileStartTS;
					if (previousFileEndUutc != -1 && overlapMicros > 0) {
						long overlapSec = overlapMicros / 1_000_000;
						long samplesToSkipPerChannel = (long) Math
								.ceil(overlapSec * h
										.getSamplingRate());
						long bytesToSkip = samplesToSkipPerChannel
								* BYTESPERSAMPLE * h.getChannelLabels().size();
						logger.info(
								"{}: Found overlap of {} seconds between {} and previous file. Skipping {} samples per channel = {} bytes",
								m,
								overlapSec,
								rawFile,
								samplesToSkipPerChannel,
								bytesToSkip);
						if (bytesToSkip >= fileLength) {
							logger.info(
									"{}: skipping all of {} due to overlap",
									m,
									rawFile);
							continue;
						} else {
							ByteStreams.skipFully(curStream, bytesToSkip);
							fileStartTS += overlapMicros;
							totalNrSamples -= samplesToSkipPerChannel;
						}
					}

					long blockStartUutc = fileStartTS;
					// Iterate of data as long as there is data and write blocks
					// to MEF
					while (nrSamplesWritten < totalNrSamples) {

						int samplesInCurBlock = 0;

						// Get fixed block size or partial block if near EOF
						try {
							if ((totalNrSamples - nrSamplesWritten) > SAMPLESPERREAD) {
								ByteStreams.readFully(curStream, b);
								nrSamplesWritten += SAMPLESPERREAD;
								samplesInCurBlock = SAMPLESPERREAD;
							} else
							{
								samplesInCurBlock = (totalNrSamples - nrSamplesWritten);
								bPerBlock = (int) (h.getChannelLabels().size()
										* samplesInCurBlock * 2);
								b = new byte[bPerBlock];
								shortBuf =
										ByteBuffer.wrap(b)
												.order(ByteOrder.LITTLE_ENDIAN)
												.asShortBuffer();
								ByteStreams.readFully(curStream, b);
								nrSamplesWritten += samplesInCurBlock;
								logger.debug(m
										+ ": Getting last partial block of data from BNI Raw Data file.");
							}
						} catch (EOFException e) {
							logger.error(
									m
											+ ": Unexpected EOF while reading BNI Raw data file.",
									e);
						}

						shortBuf.rewind();

						// Reorder
						Short[][] datByChan = new Short[chInfo.size()][samplesInCurBlock];
						for (int j = 0; j < samplesInCurBlock; j++) {
							for (int i = 0; i < chInfo.size(); i++) {
								datByChan[i][j] = shortBuf.get();
							}
						}

						// Iterate over Labels in file and write to correct
						// MEFWriter
						long[] stamps = new long[samplesInCurBlock];
						for (int iCh = 0; iCh < h.getChannelLabels().size(); iCh++) {
							String curLab = h.getChannelLabels().get(iCh);

							final SimpleChannelMefWriter curChWriter = labelToChannelWriter
									.get(curLab);
							if (curChWriter == null) {
								logger.error(
										"{}: Error: label not found: {} in {} in file {}",
										m, curLab, chInfo,
										h.toString());
								continue;
							}
							//
							SimpleChannel curInfo = curChWriter
									.getSimpleChannel();
							HabitatMefWriter curWriter = curChWriter
									.getMefWriter();
							//
							if (blockStartUutc > recordingStartUutc
									&& !curChWriter.isStartedWriting()) {
								// This is a late starting channel, so we
								// need to insert a gap
								final long[] gapStamp = { recordingStartUutc };
								final int[] gapSamp = { 0 };
								curWriter.writeData(gapSamp, gapStamp);
								curChWriter.setStartedWriting(true);

							}

							// Create TimeStamp vector
							int[] samps = new int[samplesInCurBlock];
							stamps[0] = blockStartUutc;
							logger.trace("{} FileStartTime: {}", m,
									stamps[0]);
							samps[0] = (int) (curInfo.getVoltageOffset() + datByChan[iCh][0]);
							for (int j = 1; j < samplesInCurBlock; j++) {
								samps[j] = (int) (curInfo
										.getVoltageOffset() + datByChan[iCh][j]);
								//
								stamps[j] = (long) (stamps[j - 1] + 1000000 / curInfo
										.getSamplingFrequency());
							}
							curWriter.writeData(samps, stamps);
							curChWriter.setStartedWriting(true);
						}
						blockStartUutc += 1000000 * (1 / h.getSamplingRate())
								* samplesInCurBlock;
					}
					previousFileEndUutc = blockStartUutc;
					logger.debug(m + ": Timestamp at end of file = "
							+ previousFileEndUutc);

				} catch (IOException e) {
					logger.error(m + ": Exception. Returning STATE.FAILED", e);
					return STATE.FAILED;
				} finally {
					rawStream.close();
				}
			}

		} finally {
			logger.debug("{} Closing MEF Files", m);
			for (SimpleChannelMefWriter w : labelToChannelWriter.values()) {
				try {
					w.getMefWriter().close();
				} catch (IOException e) {
					logger.info(
							m
									+ ": Ignoring exception when closing MefWriter",
							e);
				}
			}
		}

		ParametersForSnapshotContent snapP = new ParametersForSnapshotContent();

		snapP.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		snapP.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());

		for (IStoredObjectReference s : tempHandles) {
			snapP.getInputs().add(s);
		}

		getNextActions().add(
				new AddToSnapshotAction(
						getMessageQueue(),
						this.getWorkItemSet(),
						getSession(),
						snapP));

		return STATE.SUCCEEDED;

	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new TranscodeFromBNI1Action(mQueue, origin, session,
				(SnapshotParameters) arguments, null, null);
	}

	private HabitatMefWriter createMEFWriter(
			String snapshotName,
			SimpleChannel chInfo) throws IOException {

		String initalPrefixComponent = "Animal_Data/";

		if (getCurrentParameters() instanceof SnapshotParameters) {
			final SnapshotParameters snapshotParameters = (SnapshotParameters) getCurrentParameters();
			final boolean isHuman = snapshotParameters.isHuman();
			if (isHuman) {
				initalPrefixComponent = "Human_Data/";
			}
			final String institution = snapshotParameters.getOwner();
			if (institution != null) {
				initalPrefixComponent += institution + "/";
			}
		}

		String name = initalPrefixComponent + snapshotName + "/"
				+ chInfo.getChannelName() + ".mef";

		IObjectServer perServ = StorageFactory.getPersistentServer();
		IStoredObjectContainer cont = perServ.getDefaultContainer();

		FileReference newHandle = new FileReference(
				(DirectoryBucketContainer) cont, name,
				name);

		tempHandles.add(newHandle);

		return new HabitatMefWriter(perServ, newHandle, chInfo);

	}

	public void setChannelInfo(List<SimpleChannel> info) {
		this.chInfo = info;

	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}

}
