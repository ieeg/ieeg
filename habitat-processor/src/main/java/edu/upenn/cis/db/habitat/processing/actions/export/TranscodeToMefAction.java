/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.ShellAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseTranscodeAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters;
import edu.upenn.cis.db.habitat.processing.parameters.TranscodeParameters;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters.CommandLine;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * TODO: export a snapshot as MEF
 * @author zives
 *
 */
public class TranscodeToMefAction extends BaseTranscodeAction {
	public static final String NAME = "TranscodeToMefAction";
	
	public TranscodeToMefAction() {}

	public TranscodeToMefAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, TranscodeParameters parms) {
		super(mQueue, origin, session, parms);
		// TODO Auto-generated constructor stub
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		if (getCurrentParameters() != null && arguments != null) {
			getCurrentParameters().merge((Parameters)arguments);
		} else if (arguments != null)
			setParameters((TranscodeParameters)arguments);
		
		ShellParameters parms = 
				new ShellParameters();
		
		System.out.println("Executing TranscodeToMef for " + getCurrentParameters().getInputs());
		
		List<CommandLine> cmds = new ArrayList<CommandLine>();
		List<String> args = new ArrayList<String>();
		args.add("/c");
		args.add("convert.bat");
//		for (IWorkItem item: getOriginatingItemSet()) {
//			args.add(item.getHandle().getFilePath());
//		}
		for (JsonTyped handles: getCurrentParameters().getInputs())
			args.add(((FileReference)handles).getFilePath());
		cmds.add(new CommandLine("cmd.exe", args));
		parms.addCommandLines(cmds);
		
		addNextAction(new ShellAction(getMessageQueue(), getWorkItemSet(), getSession(), parms));
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new TranscodeToMefAction(mQueue, origin, session, (TranscodeParameters)arguments);
	}
}
