/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.medtronic;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.medtronic.MedtronicChannel;
import edu.upenn.cis.db.habitat.processing.parameters.medtronic.MedtronicParameters;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Reads the contents of the Medtronic XML (research device) metadata file,
 *  and executes a workflow to convert the associated .TXT files
 *  via TranscodeMdtCsvFileAction etc.
 * 
 * @author zives
 *
 */
public class ReadMdtXmlFileAction extends BaseProcessingAction {
	
	public static final String NAME = "ReadMedtronicFileAction";
	
//	WorkItemSet workItems = new WorkItemSet("Work item set", null,getOriginatingItemSet().getRequest(), getOriginatingItemSet().getOrigin());
	
	RemoteHabitatSession session;
	ControlFileParameters parameters;
	
	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public ReadMdtXmlFileAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
			ControlFileParameters parameters) {
		super(mQueue, origin);
		this.session = session;
		this.parameters = parameters;
	}
	
	public ReadMdtXmlFileAction() {
		super(null, null);
	}


	/**
	 * Core execution: parse XML files from getInput() (assumed to be sorted
	 * by temporal ordering).  Create a MedtronicsParameters instance for the
	 * details of each file.
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		if (arguments != null && parameters != null && arguments instanceof Parameters)
			parameters.merge((Parameters)arguments);
		else if (arguments != null)
			parameters = (ControlFileParameters)arguments;
		
		JAXBContext jaxbContext = JAXBContext.newInstance(MedtronicParameters.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		
		List<MedtronicParameters> myParmsList = new ArrayList<MedtronicParameters>();
		
		for (JsonTyped inFileSpec : parameters.getInputs()) {
			System.out.println("ReadMdtFile: " + inFileSpec.toString());
			FileReference inFile = (FileReference)inFileSpec;

			IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
			
			IInputStream inStream = bestServer.openForInput(inFile);
			
			try {
				BufferedInputStream inFStream = 
						new BufferedInputStream(inStream.getInputStream());
				
				StringBuilder str = new StringBuilder();

				BufferedReader reader = new BufferedReader(new InputStreamReader(inFStream));
				String line = reader.readLine();
				while (line != null) {
					str.append(line.trim());
					str.append("\n");
					line = reader.readLine();
				}
				reader.close();
				inStream.close();
				
				XMLEventReader eventReader = inputFactory.createXMLEventReader(new StringReader(str.toString()));
				MedtronicParameters myParms = (MedtronicParameters)jaxbUnmarshaller.unmarshal(eventReader);
				
				myParmsList.add(myParms);
				
			} catch (IOException ioe) {
				ioe.printStackTrace();
				return STATE.FAILED;
			}
		}
		processSnapshot(myParmsList);
		
		return STATE.SUCCEEDED;
	}

	/**
	 * Get the filename of the associated .TXT file that is paired with
	 * the .XML one
	 * 
	 * @param fil
	 * @return
	 */
	private String replaceXmlWithTxt(String fil) {
		if (fil.endsWith(".xml"))
			return fil.substring(0, fil.length() - 3) + "txt";
		else
			return fil;
	}
		
	/**
	 * Trigger further processing by creating a SnapshotParameters, populating
	 * it with snapshot and channel info, and putting several successor actions
	 * into play.
	 * 
	 * @param parmsList parameters of the various .XML files (assumed sorted
	 * in temporal order)
	 */
	private void processSnapshot(List<MedtronicParameters> parmsList) {
		final String m = "processSnapshot(...)";
		if (parmsList.isEmpty())
			return;
		
		List<String> channelIds = new ArrayList<String>();
		SnapshotParameters ssp = new SnapshotParameters("",//parameters.getPatientID(),
				null, SnapType.other, channelIds);

		ssp.setOwner("Medtronic");
		//Default to non-human
		boolean isHuman = false;
		if (getCurrentParameters() instanceof SnapshotParameters) {
			isHuman = ((SnapshotParameters)getCurrentParameters()).isHuman();
			logger.debug("{}: Setting isHuman based on current parameters: [{}]", m, isHuman);
		}
		ssp.setHuman(isHuman);
		for (JsonTyped mdtXmlInput : getCurrentParameters().getInputs()) {
			final FileReference mdtXmlFile = (FileReference) mdtXmlInput;
			FileReference mdtCsvFile = new FileReference(
					mdtXmlFile.getDirBucketContainer(),
					replaceXmlWithTxt(mdtXmlFile.getObjectKey()), 
					replaceXmlWithTxt(mdtXmlFile.getFilePath()));
			ssp.getInputs().add(mdtCsvFile);
			logger.debug("{}: Adding MDT CSV file {} to inputs for next action", m, mdtCsvFile);
		}

		for (MedtronicParameters parameters : parmsList) {
			ssp.setTargetSnapshot(parameters.getPatientID());
			
			int i = 0;
			for (MedtronicChannel ch: parameters.getSenseChannelConfig().getChannels()) {
				String name = ch.getChannelType() + (i++);
				if (!channelIds.contains(name))
					channelIds.add(name);
			}
		}
		
		// TODO: the event LOG file??
		
		// Create snapshot as necessary
		IProcessingAction snap = new CreateSnapshotAction(
				getMessageQueue(),
				this.getWorkItemSet(),
				session,
				ssp);
		
		getNextActions().add(snap);
		
		// Map metadata to it as necessary
		IProcessingAction map = new MapMetadataAction(
				getMessageQueue(), this.getWorkItemSet(),
				session, ssp);
		
		snap.getNextActions().add(map);
		
		IProcessingAction transcode = new TranscodeMdtCsvFileAction(
				getMessageQueue(),
				getWorkItemSet(),
				session, ssp, parmsList);
		
		map.getNextActions().add(transcode);

		// TODO:
		// Process the contents
//		parameters.setTargetSnapshot(parameters.getSnapshotParameters().getTargetSnapshot());
//		IProcessingAction next = new ProcessSnapshotContentsAction(
//				getOriginatingItemSet(),
//				session,
//				parameters);
//
//		map.getNextActions().add(next);
	}
	
	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new ReadMdtXmlFileAction(mQueue, origin, session, (ControlFileParameters)arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Set<IProcessingAction> getNextActions() {
		return nextActions;
	}
	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}
}
