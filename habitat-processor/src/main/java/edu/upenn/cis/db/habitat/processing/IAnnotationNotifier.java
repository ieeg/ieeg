package edu.upenn.cis.db.habitat.processing;

import java.util.List;

import edu.upenn.cis.db.mefview.shared.Annotation;

public interface IAnnotationNotifier {
	public void notifyAnnotations(String snapshotName, String snapshotId, List<Annotation> annotations);
}
