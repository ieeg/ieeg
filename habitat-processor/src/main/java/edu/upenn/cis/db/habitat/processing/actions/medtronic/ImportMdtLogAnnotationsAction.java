/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.medtronic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Read the LOG text files and add the appropriate time series annotations (on
 * all channels)
 * 
 * @author zives
 *
 */
public class ImportMdtLogAnnotationsAction extends BaseSnapshotProcessingAction {
		public static final String NAME = "ImportMdtLogAnnotationsAction";
		
		public static class AnnotationEvent {
			String label;
			String type;
			Set<String> channels = new HashSet<String>();
			long time;
			
			public AnnotationEvent() {}
			
			public AnnotationEvent(String label, String type, long time) {
				super();
				this.label = label;
				this.type = type;
				this.time = time;
			}
			public String getLabel() {
				return label;
			}
			public void setLabel(String label) {
				this.label = label;
			}
			public long getTime() {
				return time;
			}
			public void setTime(long time) {
				this.time = time;
			}

			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			public Set<String> getChannels() {
				return channels;
			}

			public void setChannels(Collection<String> channels) {
				this.channels.addAll(channels);
			}
			
			
		}
		
		List<String> annChannels = new ArrayList<String>();
		
		public ImportMdtLogAnnotationsAction() {}

		public ImportMdtLogAnnotationsAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, SnapshotParameters ssp) {
			super(mQueue, origin, session, ssp);
		}
		
		/**
		 * Converts month-day-year hours-minutes-seconds am/pm
		 * into microseconds since 1970
		 * 
		 * @param date
		 * @return
		 */
		private long getUutc(String date) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
			DateTime dateTime = formatter.parseDateTime(date);
			
			return dateTime.getMillis() * 1000;
		}

		/**
		 * Parses a log entry of the form:
		 *   Enable Sensing, 9/5/2013 1:22:30 PM
		 * 
		 * @param event
		 * @return
		 */
		private AnnotationEvent parseLogEvent(String event) {
			if (!event.contains(","))
				return null;
			
			String[] fields = event.split(",");
			
			AnnotationEvent ret = new AnnotationEvent(fields[0].trim(),
					"Event", getUutc(fields[1].trim()));

			ret.setChannels(annChannels);
			
			return ret;
		}

		@Override
		public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
			// Compile the set of channel labels
			for (String label : getCurrentParameters().getChannelLabels()) {
				annChannels.add(getCurrentParameters().getChannelId(label));
			}

			for (JsonTyped file: getCurrentParameters().getInputs()) {
				FileReference handle = (FileReference)file;
				
				IObjectServer server = StorageFactory.getBestServerFor(handle);
				try {
					IInputStream stream = server.openForInput(handle);
					
					// Now read the file line-by-line
					BufferedReader reader = new BufferedReader(new InputStreamReader(stream.getInputStream()));
					
					String line = reader.readLine();
					List<AnnotationEvent> events = new ArrayList<AnnotationEvent>(1);

					while (line != null) {
						line = line.trim();
						
						if (!line.isEmpty() && line.contains(",")) {
							AnnotationEvent event = parseLogEvent(line);
							if (event != null)
								events.add(event);
						}
						line = reader.readLine();
					}
					reader.close();
					stream.close();
					if (!process(events))
						return STATE.FAILED;
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return STATE.FAILED;
				}
				
			}

			return STATE.SUCCEEDED;
		}
		
		public boolean process(List<AnnotationEvent> events) {
			return getSession().addAnnotation((User)getCreator(), 
					getCurrentParameters().getTargetSnapshotId(), 
					events);
		}

		@Override
		public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
			return new ImportMdtLogAnnotationsAction(mQueue, origin, session, (SnapshotParameters)arguments);
		}

		@Override
		public ParametersForSnapshotContent getDefaultArguments() {
			// TODO Auto-generated method stub
			return new ParametersForSnapshotContent();
		}
		
		@Override
		public SnapshotParameters getCurrentParameters() {
			return (SnapshotParameters)super.getCurrentParameters();
		}
	}
