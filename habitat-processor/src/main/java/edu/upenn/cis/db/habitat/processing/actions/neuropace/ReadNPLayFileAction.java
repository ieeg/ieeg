/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.neuropace;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.actions.persyst.TranscodePersystDatFileAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.neuropace.NeuropaceParameters;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.IPersystParameters;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.PersystChannelMap;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Reads the contents of the Neuropace .lay metadata file,
 *  and executes a workflow to convert the associated .TXT files
 *  via TranscodePersystLayFileAction etc.
 * 
 * @author jwagenaar
 *
 */
public class ReadNPLayFileAction extends BaseProcessingAction {
    
    public static final String NAME = "ReadNeuropaceLayFileAction";
    
//    WorkItemSet workItems = new WorkItemSet("Work item set", null,getOriginatingItemSet().getRequest(), getOriginatingItemSet().getOrigin());
    
    RemoteHabitatSession session;
    ControlFileParameters parameters;
    
    Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
    
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    public ReadNPLayFileAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
            ControlFileParameters parameters) {
        super(mQueue, origin);
        this.session = session;
        this.parameters = parameters;
    }
    
    public ReadNPLayFileAction() {
        super(null,null);
    }


    /**
     * Core execution: parse XML files from getInput() (assumed to be sorted
     * by temporal ordering).  Create a MedtronicsParameters instance for the
     * details of each file.
     */
    @Override
    public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
        if (arguments != null && parameters != null && arguments instanceof Parameters)
            parameters.merge((Parameters)arguments);
        else if (arguments != null)
            parameters = (ControlFileParameters)arguments;
        
   
        List<NeuropaceParameters> myParmsList = new ArrayList<NeuropaceParameters>();
        
        for (JsonTyped inFileSpec : parameters.getInputs()) {
            System.out.println("ReadLayFile: " + inFileSpec.toString());
            
            FileReference inFile = (FileReference)inFileSpec;

            try {

                NeuropaceParameters myParms = new NeuropaceParameters(inFile);

                myParmsList.add(myParms);
                
            } catch (IOException ioe) {
                ioe.printStackTrace();
                return STATE.FAILED;
            }
        }
        processSnapshot(myParmsList);
        
        return STATE.SUCCEEDED;
    }

    /**
     * Get the filename of the associated .TXT file that is paired with
     * the .XML one
     * 
     * @param fil
     * @return
     */
    private String replaceLayWithDat(String fil) {
        if (fil.endsWith(".lay"))
            return fil.substring(0, fil.length() - 3) + "DAT";
        else
            return fil;
    }
        
    /**
     * Trigger further processing by creating a SnapshotParameters, populating
     * it with snapshot and channel info, and putting several successor actions
     * into play.
     * 
     * @param parmsList parameters of the various .XML files (assumed sorted
     * in temporal order)
     */
    @SuppressWarnings("unchecked")
    private void processSnapshot(List<NeuropaceParameters> parmsList) {
        final String m = "processSnapshot(...)";
        if (parmsList.isEmpty())
            return;
        
        List<String> channelIds = new ArrayList<String>();
        SnapshotParameters ssp = new SnapshotParameters("",//parameters.getPatientID(),
                null, SnapType.other, channelIds);
//
        ssp.setOwner("Neuropace");
//        //Default to non-human
        boolean isHuman = false;
        if (getCurrentParameters() instanceof SnapshotParameters) {
            isHuman = ((SnapshotParameters)getCurrentParameters()).isHuman();
            logger.debug("{}: Setting isHuman based on current parameters: [{}]", m, isHuman);
        }
        ssp.setHuman(isHuman);
        for (JsonTyped layInput : getCurrentParameters().getInputs()) {
            final FileReference layFile = (FileReference) layInput;
            FileReference datFile = new FileReference(
                layFile.getDirBucketContainer(),
                    replaceLayWithDat(layFile.getObjectKey()), 
                    replaceLayWithDat(layFile.getFilePath()));
            ssp.getInputs().add(datFile);
            logger.debug("{}: Adding Neuropace Lay file {} to inputs for next action", m, datFile);
        }
//
        for (NeuropaceParameters parameters : parmsList) {
          System.out.println("Lay File Timestamp: " + parameters.geteCogFileStamp());
            ssp.setTargetSnapshot(parameters.getPatientID());
//            
//            int i = 0;
            for (PersystChannelMap ch: parameters.getChannels()) {
                String name = ch.getLabel();
                if (!channelIds.contains(name))
                    channelIds.add(name);
            }
        }
//        
//        // TODO: the event LOG file??
//        
//        // Create snapshot as necessary
        IProcessingAction snap = new CreateSnapshotAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session,
                ssp);
//        
        getNextActions().add(snap);
//        
        // Map metadata to it as necessary
        IProcessingAction map = new MapMetadataAction(
        		getMessageQueue(), 
        		this.getWorkItemSet(),
                session, ssp);
//        
        snap.getNextActions().add(map);
//        
        List<? extends IPersystParameters> aux = new ArrayList<IPersystParameters>();
        
        for (NeuropaceParameters param : parmsList){
          ((List<IPersystParameters>)aux).add(param);          
        }
        
        IProcessingAction transcode = new TranscodePersystDatFileAction(
        		getMessageQueue(),
        		this.getWorkItemSet(),
                session, ssp, aux);
        
        map.getNextActions().add(transcode);

        // TODO:
        // Process the contents
//      parameters.setTargetSnapshot(parameters.getSnapshotParameters().getTargetSnapshot());
//      IProcessingAction next = new ProcessSnapshotContentsAction(
//              getOriginatingItemSet(),
//              session,
//              parameters);
//
//      map.getNextActions().add(next);
    }
    
    @Override
    public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
        return new ReadNPLayFileAction(mQueue, origin, session, (ControlFileParameters)arguments);
    }

    @Override
    public Parameters getDefaultArguments() {
        return new ControlFileParameters();
    }

    @Override
    public Set<IProcessingAction> getNextActions() {
        return nextActions;
    }
    @Override
    public Parameters getCurrentParameters() {
        return parameters;
    }
}
