/*
 * Copyright (C) 2014 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.ehcache.CacheManager;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.collect.Lists;
import com.google.gwt.user.client.Command;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.braintrust.shared.WorkerResponse;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.processing.ActionMapper.Mapping;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.ActionFactory;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.RegisterActions;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.server.CoralReefServiceFactory;
import edu.upenn.cis.db.mefview.server.ISessionManager;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.events.UserEvent;

/*****
 * Daemon process for automatically annotating a snapshot
 * 
 * @author zives
 *
 */
public class SnapshotProcessorDaemon implements ITaskTracker {
	public static class ProcessorTask {
		public final static String PROCESSOR_ID = "3f2f5776-349b-43e4-baa9-5221a41151a5";
	}

	public static final ObjectMapper mapper = new ObjectMapper();
	RemoteHabitatSession session;
	private static IGraphServer crService;

	private IMessageQueue messageQueue;

	IWorkQueue workload;
	ActionQueue actions = new ActionQueue();

	public static int MAX_RETRIES = 1;// 3;

	// public Map<WorkItemSet,TaskStatus> tasksStarted = new
	// HashMap<WorkItemSet,TaskStatus>();
	public Map<IProcessingAction, TaskStatus> actionStatus = new HashMap<IProcessingAction, TaskStatus>();

	Map<WorkItemSet, Set<IProcessingAction>> actionsForWorkItem =
			new HashMap<WorkItemSet, Set<IProcessingAction>>();
	Map<WorkItemSet, Set<WorkItemSet>> derivedWorkItems =
			new HashMap<WorkItemSet, Set<WorkItemSet>>();

	private final static Logger logger = LoggerFactory
			.getLogger(SnapshotProcessorDaemon.class);
	private final static Logger timeLogger = LoggerFactory.getLogger("time."
			+ SnapshotProcessorDaemon.class);

	private transient ISessionManager sessionManager;

	static {
		try {
			initEhcache();
		} catch (Throwable t) {
			logger.error("Exception initializing "
					+ SnapshotProcessorDaemon.class.getSimpleName(), t);
			throw new ExceptionInInitializerError(t);
		}
	}

	public SnapshotProcessorDaemon(IMessageQueue mQueue, IWorkQueue queue) {
		this();
		this.workload = queue;
		setMessageQueue(mQueue);
	}

	public SnapshotProcessorDaemon() {
		this.session = RemoteHabitatFactory.getSession();
		crService = CoralReefServiceFactory.getGraphServer();
		setMessageQueue(new ConsoleMessageQueue());
	}

	public static class AnnotatorConfig {
		List<String> channelsToAnnotate = Lists.newArrayList();

		List<SignalProcessingStep> steps = Lists.newArrayList();

		public List<String> getChannelsToAnnotate() {
			return channelsToAnnotate;
		}

		public void setChannelsToAnnotate(List<String> channelsToAnnotate) {
			this.channelsToAnnotate = channelsToAnnotate;
		}

		public List<SignalProcessingStep> getSteps() {
			return steps;
		}

		public void setSteps(List<SignalProcessingStep> steps) {
			this.steps = steps;
		}

	}

	/**
	 * Initialize the connection
	 * 
	 * @throws UnknownHostException
	 */
	public void open() throws UnknownHostException {
		System.out.println("Opening portal connection");
		session.open();
		Session session2 = (HibernateUtil.getSessionFactory()).openSession();

		String dbURL;
		try {
			dbURL = ((SessionImpl) session2).connection().getMetaData()
					.getURL().toString();
			logger.info("Database connection: " + dbURL);
		} catch (HibernateException | SQLException e) {
			e.printStackTrace();
			logger.error("", e);
		}
		session2.close();
		RegisterActions.registerActionMappings();
		RegisterActions.registerKnownActions();
		// Override
		// ActionFactory.getFactory().register(ReadSubmittedDirectoryAction.NAME,
		// new ReadSubmittedDirectoryAction());
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws ServerTimeoutException
	 * @throws BadTsAnnotationTimeException
	 * @throws TimeSeriesNotFoundInDatasetException
	 * @throws AuthorizationException
	 */
	public static void main(String[] args) throws IllegalArgumentException,
			IOException, ServerTimeoutException, AuthorizationException,
			TimeSeriesNotFoundInDatasetException, BadTsAnnotationTimeException {
		final String queueName = args.length == 0
				? null
				: args[0];
		SnapshotProcessorDaemon ann = new SnapshotProcessorDaemon(
				new ConsoleMessageQueue(), new HibernateWorkQueue(queueName));

		ann.open();
		if (queueName == null) {
			logger.info("Monitoring work queue");
		} else {
			logger.info("Monitoring work queue {}", queueName);
		}
		Map<WorkItemSet, Set<WorkItemSet>> pending = new HashMap<WorkItemSet, Set<WorkItemSet>>();
		while (true) {
			try {
				ann.work(ann.workload, pending);
				logger.debug(".");
				// System.out.print(".");
			} catch (InterruptedException e) {
				logger.error("", e);
				logger.info("Interrupted. Exiting.");
				System.exit(0);
			}
		}
	}

	public void addToQueue(WorkItem work) {
		workload.add(work);

	}

	public void workOnQueue() throws InterruptedException {
		Map<WorkItemSet, Set<WorkItemSet>> pending = new HashMap<WorkItemSet, Set<WorkItemSet>>();
		work(this.workload, pending);
	}

	public boolean isApplicable(WorkItemSet itemSet) {

		Set<Mapping> feasible = ActionMapper.getMap().getRelevantActions(
				itemSet, this);

		// Dequeue from the head, if we are the head
		if (workload.peek() == itemSet)
			workload.getNext();

		// If nothing to do, put this task back on the back of the queue
		if (feasible.isEmpty()) {
			// System.out.println("Task item " + itemSet.getDescription() +
			// " deferred");
			return false;
		}
		// System.out.println("Task item " + itemSet.getDescription());
		return true;
	}

	public List<IProcessingAction> getNextActions(WorkItemSet itemSet) {
		final String m = "getNextActions(...)";

		Parameters taskParms = (Parameters) itemSet.getJson();

		logger.debug("{}: Finding actions for work item {}", m,
				itemSet.getDescription());
		Set<Mapping> feasible = ActionMapper.getMap().getRelevantActions(
				itemSet, this);

		// Dequeue from the head, if we are the head
		if (workload.peek() == itemSet)
			workload.getNext();

		// Figure out the initial set of eligible actions, and queue them up
		List<IProcessingAction> nextItems = new ArrayList<IProcessingAction>();
		for (Mapping actionName : feasible) {
			logger.debug("{}: Creating action {}", m, actionName.getAction());

			Parameters parms = actionName.getParameterMap().mapFrom(itemSet,
					null, actionName.getAction());

			if (taskParms != null) {
				if (logger.isTraceEnabled()) {
					try {
						final ObjectWriter ow = ObjectMapperFactory
								.newPasswordFilteredWriter();
						final String taskParmsString = ow
								.writeValueAsString(taskParms);
						final String parmsString = ow.writeValueAsString(parms);
						logger.trace("{}: Merging {} into {}", m,
								taskParmsString, parmsString);
					} catch (JsonProcessingException e) {
						logger.error(m
								+ ": Exception trying to write trace logging",
								e);
					}
				}
				parms.merge(taskParms);
				if (parms instanceof ParametersForSnapshotContent
						&& taskParms instanceof ParametersForSnapshotContent) {
					((ParametersForSnapshotContent) parms).setTargetSnapshotId(
							((ParametersForSnapshotContent) taskParms)
									.getTargetSnapshotId());
				}

				if (parms instanceof SnapshotParameters
						&& taskParms instanceof SnapshotParameters) {
					((SnapshotParameters) parms).setOwner(
							((SnapshotParameters) taskParms).getOwner());
					((SnapshotParameters) parms).setInstitution(
							((SnapshotParameters) taskParms).getInstitution());
					((SnapshotParameters) parms).setHuman(
							((SnapshotParameters) taskParms).isHuman());
					((SnapshotParameters) parms)
							.setChannelLabels(
							((SnapshotParameters) taskParms).getChannelLabels());
					((SnapshotParameters) parms).setChannelMapping(
							((SnapshotParameters) taskParms)
									.getChannelMapping());
				} else if (parms instanceof SnapshotParameters
						&& taskParms instanceof ControlFileParameters) {
					((SnapshotParameters) parms).setOwner(
							((ControlFileParameters) taskParms)
									.getSnapshotParameters().getOwner());
					((SnapshotParameters) parms).setInstitution(
							((ControlFileParameters) taskParms)
									.getSnapshotParameters().getInstitution());
					((SnapshotParameters) parms).setHuman(
							((ControlFileParameters) taskParms)
									.getSnapshotParameters().isHuman());
					((SnapshotParameters) parms)
							.setChannelLabels(
							((ControlFileParameters) taskParms)
									.getSnapshotParameters().getChannelLabels());
					((SnapshotParameters) parms).setChannelMapping(
							((ControlFileParameters) taskParms)
									.getSnapshotParameters()
									.getChannelMapping());
				}

				if (parms instanceof ControlFileParameters
						&& taskParms instanceof SnapshotParameters) {
					((ControlFileParameters) parms)
							.getSnapshotParameters()
							.setOwner(
									((SnapshotParameters) taskParms).getOwner());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setInstitution(
									((SnapshotParameters) taskParms)
											.getInstitution());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setHuman(
									((SnapshotParameters) taskParms).isHuman());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setChannelLabels(
									((SnapshotParameters) taskParms)
											.getChannelLabels());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setChannelMapping(
									((SnapshotParameters) taskParms)
											.getChannelMapping());
				} else if (parms instanceof ControlFileParameters
						&& taskParms instanceof ControlFileParameters) {
					((ControlFileParameters) parms)
							.getSnapshotParameters()
							.setOwner(
									((ControlFileParameters) taskParms)
											.getSnapshotParameters().getOwner());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setInstitution(
									((ControlFileParameters) taskParms)
											.getSnapshotParameters()
											.getInstitution());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setHuman(
									((ControlFileParameters) taskParms)
											.getSnapshotParameters().isHuman());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setChannelLabels(
									((ControlFileParameters) taskParms)
											.getSnapshotParameters()
											.getChannelLabels());
					((ControlFileParameters) parms).getSnapshotParameters()
							.setChannelMapping(
									((ControlFileParameters) taskParms)
											.getSnapshotParameters()
											.getChannelMapping());
				}

				if (logger.isTraceEnabled()) {
					try {
						final ObjectWriter ow = ObjectMapperFactory
								.newPasswordFilteredWriter();
						final String parmsString = ow.writeValueAsString(parms);
						logger.trace("{}: Result of merge {}", m, parmsString);
					} catch (JsonProcessingException e) {
						logger.error(m
								+ ": Exception trying to write trace logging",
								e);
					}
				}
			}

			IProcessingAction act = ActionFactory.getFactory().create(
					getMessageQueue(), actionName.getAction(),
					itemSet,
					session,
					parms);

			associateTaskWithItem(itemSet, act);
			if (act == null)
				throw new RuntimeException("Unable to create action "
						+ actionName);

			nextItems.add(act);
		}
		return nextItems;
	}

	public static class DerivedItem {
		String type;
		String value;

		public DerivedItem(String type, String value) {
			super();
			this.type = type;
			this.value = value;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}

	public interface WorkCommand extends Command {
		public void setDerivedItems(Set<DerivedItem> der);
	}

	public void work(
			final IWorkQueue workload,
			final Map<WorkItemSet, Set<WorkItemSet>> pending)
			throws InterruptedException {
		work(workload, pending, new WorkCommand() {
			Set<DerivedItem> derived = new HashSet<DerivedItem>();

			@Override
			public void execute() {
				final String m = "execute()";
				// DONE. A bit of hack
				for (WorkItemSet p : pending.keySet()) {
					final WorkerRequest req = (WorkerRequest) p.getRequest();

					final IWorkQueue queue = (IWorkQueue) p.getOrigin();
					if (req != null && queue != null) {
						Set<String> seen = new HashSet<String>();

						for (DerivedItem snap : derived) {
							if (!snap.getType().equals("snapshot")
									|| seen.contains(snap.getValue()))
								continue;
							else
								seen.add(snap.getValue());

							WorkerResponse response = new WorkerResponse(
									req.getUser(), req
											.getChannel(), req.getSource(),
									req.getId(),
									"complete", "ok", snap.getValue(),
									"");

							try {
								final String responseAsString = mapper
										.writeValueAsString(response);
								logger.info("Sending completion of snapshot "
										+ snap.getValue()
										+ " to "
										+ req.toString()
										+ ": "
										+ responseAsString);
								queue.sendDone(p, req.getSource(),
										responseAsString);
							} catch (JsonProcessingException e1) {
								logger.error(m + ": could not parse response",
										e1);
							}
						}
					}
				}
			}

			@Override
			public void setDerivedItems(Set<DerivedItem> der) {
				derived = der;
			}

		});
	}

	/**
	 * TODO: put this in a thread??
	 * 
	 * @param workload
	 * @throws InterruptedException
	 */
	public void work(IWorkQueue workload,
			Map<WorkItemSet, Set<WorkItemSet>> pending,
			WorkCommand notifyOnDone) throws InterruptedException {
		final String m = "work(...)";

		Set<DerivedItem> derived = new HashSet<DerivedItem>();
		if (notifyOnDone != null) {
			notifyOnDone.setDerivedItems(derived);
		}

		do {
			List<WorkItemSet> deferred = new ArrayList<WorkItemSet>();
			while (workload.hasNext(1, 0)) {// 2)) {
				WorkItemSet taskSet = null;

				boolean isNew = true;

				taskSet = workload.getNext();

				// Record this task and its originator
				if (isNew && !pending.containsKey(taskSet.getStart())) {
					pending.put(taskSet.getStart(), new HashSet<WorkItemSet>());
					logger.info("CREATING NEW INITIAL ITEM "
							+ taskSet.getStart());
					UserEvent.logEvent(UserEvent.EventType.DATASET_UPLOAD,
							taskSet.getStart().toString());

				}
				if (isNew)
					pending.get(taskSet.getStart()).add(taskSet);

				if (isNew && !isApplicable(taskSet)) {
					// Defer
					deferred.add(taskSet);
					System.out.println("Deferring action "
							+ taskSet.getDescription());
				} else {
					List<IProcessingAction> next = getNextActions(taskSet);

					for (IProcessingAction act : next)
						actions.add(act);

					// // Mark this as no longer pending
					// System.out.println("REMOVING ITEM " + taskSet);
					// pending.get(taskSet.getStart()).remove(taskSet);
				}
			}
			for (WorkItemSet item : deferred)
				workload.add(item);

			if (!actions.hasNext())
				break;

			// Figure out the initial set of eligible actions, and queue them up
			// List<IProcessingAction> nextItems = new
			// ArrayList<IProcessingAction>();//actions.getNext();////getNextActions(taskSet);
			IProcessingAction act = actions.getNext();

			// Skip if done or we're supposed to ignore!!
			if (getStatus(act).getStatus() == STATE.SUCCEEDED ||
					getStatus(act).isIgnore() ||
					(getStatus(act).getStatus() == STATE.FAILED &&
					getStatus(act).getRetries() > MAX_RETRIES))
				continue;

			setStatus(act, STATE.STARTED);
			getStatus(act).setRetries(getStatus(act).getRetries() + 1);

			// Each action should be executed, then, if it succeeds, a further
			// action will
			// be triggered. But we break out the moment something fails (no
			// rollback so not
			// a transaction, but we must have 100% success to proceed to next
			// stage).
			boolean success = true;

			List<IProcessingAction> newItems = new ArrayList<IProcessingAction>();

			try {
				System.out.println("Executing action "
						+ act.getClass().getName());
				STATE ret = STATE.DEFERRED;

				if (act.isReady())
					ret = act.execute(null, derived);

				if (ret == STATE.DEFERRED) {
					System.err.println("** Deferring non-ready action "
							+ act.getClass().getName());
					actions.add(act);
				} else if (ret != STATE.SUCCEEDED) {
					System.err.println("** Failed action "
							+ act.getClass().getName());
					setStatus(act, STATE.FAILED);

					success = false;
					actions.add(act);
				} else {
					if (getStatus(act).getStatus() != STATE.FAILED)
						setStatus(act, ret);

					// Get any direct actions pending
					Set<IProcessingAction> next = act.getNextActions();
					if (next != null && !next.isEmpty()) {
						for (IProcessingAction actn : next) {
							System.out.println("Added action "
									+ actn.getClass().getName());
							associateTaskWithItem(act.getWorkItemSet(), actn);
							newItems.add(actn);
						}
					}

					// Get any work items pending
					List<WorkItemSet> more = act.getNextWorkItems();

					for (WorkItemSet is : more) {
						// System.out.println("ADDING DERIVED ITEM " + is +
						// " from " + act.getOriginatingItemSet().getStart());
						setDerivedItem(act.getWorkItemSet(), is);
						is.setStart(act.getWorkItemSet().getStart());

						// if (act != null && act.getOriginatingItemSet() !=
						// null)
						// if
						// (pending.get(act.getOriginatingItemSet().getStart())
						// != null)
						// pending.get(act.getOriginatingItemSet().getStart()).add(is);
						// else
						// System.err.println("CANNOT FIND " +
						// act.getOriginatingItemSet().getStart());

						if (isApplicable(is)) {
							List<IProcessingAction> actions = getNextActions(is);
							// We made progress unless the action is to do the
							// same thing
							newItems.addAll(actions);
						} else {
							workload.add(is);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(m + ": action failed", e);
				setStatus(act, STATE.FAILED);
				success = false;
				actions.add(act);
			}

			for (IProcessingAction item : newItems)
				actions.add(item);

		} while (actions.hasNext());

		if (notifyOnDone != null)
			notifyOnDone.execute();

		pending.clear();
	}

	public void setStatus(IProcessingAction item, TaskStatus status) {
		actionStatus.put(item, status);
	}

	public void setStatus(IProcessingAction item, STATE state) {
		actionStatus.get(item).setStatus(state);
	}

	@Override
	public Set<TaskStatus> getStatus(WorkItemSet item) {
		Set<TaskStatus> ret = new HashSet<TaskStatus>();

		for (IProcessingAction action : getTasksForItem(item))
			ret.add(getStatus(action));
		return ret;
	}

	@Override
	public TaskStatus getStatus(IProcessingAction item) {
		if (!actionStatus.containsKey(item))
			actionStatus.put(item, new TaskStatus(STATE.NEW));

		return actionStatus.get(item);
	}

	@Override
	public Set<IProcessingAction> getTasksForItem(WorkItemSet item) {
		if (!actionsForWorkItem.containsKey(item))
			actionsForWorkItem.put(item, new HashSet<IProcessingAction>());

		return actionsForWorkItem.get(item);
	}

	@Override
	public Set<WorkItemSet> getDerivedItems(WorkItemSet item) {
		if (!derivedWorkItems.containsKey(item))
			derivedWorkItems.put(item, new HashSet<WorkItemSet>());

		return derivedWorkItems.get(item);
	}

	@Override
	public void associateTaskWithItem(WorkItemSet item,
			IProcessingAction action) {
		if (!actionsForWorkItem.containsKey(item))
			actionsForWorkItem.put(item, new HashSet<IProcessingAction>());

		actionsForWorkItem.get(item).add(action);
	}

	@Override
	public void setDerivedItem(WorkItemSet parent, WorkItemSet child) {
		if (!derivedWorkItems.containsKey(parent))
			derivedWorkItems.put(parent, new HashSet<WorkItemSet>());

		derivedWorkItems.get(parent).add(child);
	}

	@Override
	public Set<IProcessingAction> getTransitiveClosureOfTasks(WorkItemSet item) {
		Set<IProcessingAction> ret = new HashSet<IProcessingAction>();

		getTransitiveClosureOfTasksInternal(item, ret);
		return ret;
	}

	/**
	 * Accumulate transitive closure of all derived items' actions
	 * 
	 * @param item
	 * @param results
	 */
	public void getTransitiveClosureOfTasksInternal(WorkItemSet item,
			Set<IProcessingAction> results) {
		results.addAll(getTasksForItem(item));

		for (WorkItemSet child : getDerivedItems(item))
			getTransitiveClosureOfTasksInternal(child, results);
	}

	private static void initEhcache() {
		try (InputStream is =
				Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("ieeg-ehcache.xml")) {
			if (is == null) {
				throw new IllegalStateException(
						"An ieeg-ehcache.xml configuration file must be on the classpath");
			}
			CacheManager.newInstance(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public RemoteHabitatSession getSession() {
		return session;
	}

	public void setWorkloadQueue(BasicWorkQueue workload2) {
		workload = workload2;
	}

	public IWorkQueue getWorkloadQueue() {
		return workload;
	}

	public IMessageQueue getMessageQueue() {
		return messageQueue;
	}

	public void setMessageQueue(IMessageQueue queue) {
		this.messageQueue = queue;
	}
}
