package edu.upenn.cis.db.habitat.processing.parameters.kahana;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;

public class KahanaFileParameters implements Comparable<KahanaFileParameters>{
  private KahanaParameters parent;
  private FileReference handle;
  private final IInputStream inStream;
  private Integer channel;
  private long startDate;
  private String expName;
  
  
  
  
  public KahanaFileParameters(FileReference inFile, KahanaParameters parent) throws IOException, ParseException {
    this.handle = inFile;
    this.parent = parent;
    IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);

    inStream = bestServer.openForInput(inFile);
    
    File f = new File(handle.getObjectKey());
    String name = f.getName();
        
    String[] splitName = name.split("_");
    
    expName = splitName[0];
    
    String[] timeChannel = splitName[2].split("\\.");
    startDate = date2uUTC(splitName[1],timeChannel[0]);
    
    channel = Integer.parseInt(timeChannel[1]);
    
  }
  
  /*
   * Return Start-date of EDF file in MicroUTC. (usec from jan 1, 1970) 
   */
  public long date2uUTC(String startDate,String startTime) throws ParseException {   
    SimpleDateFormat df = new SimpleDateFormat("ddMMMyy.kkmm Z");   
    
    Date startD = df.parse(startDate + "." + startTime + " -0000");
    long retTime = startD.getTime() * 1000;
    return retTime;
  }
  
  public String toString(){
    return "KahanaFileParameters: " + expName + " - " + startDate + " - " + channel; 
  }

  public FileReference getHandle() {
    return handle;
  }

  public BufferedInputStream getiStream() throws IOException {
    return new BufferedInputStream(inStream.getInputStream());
  }

  public IInputStream getInStream() {
    return inStream;
  }

  public Integer getChannel() {
    return channel;
  }

  public long getStartDate() {
    return startDate;
  }

  public String getExpName() {
    return expName;
  }
  
  /**
   * Closes the {@link IInuptStream}
   * 
   * @throws IOException
   */
  public void close() throws IOException {
      inStream.close();
  }

  @Override
	public int compareTo(KahanaFileParameters o) {
		long diff = this.getStartDate() - o.getStartDate();
		return safeLongToInt(diff);
	}

	public static int safeLongToInt(long l) {
		return (int) Math
				.max(Math.min(Integer.MAX_VALUE, l), Integer.MIN_VALUE);
	}

}
