/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.upenn.cis.db.habitat.processing.actions;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.TranscodeParameters;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * Transcode a set of timeseries into a new set of timeseries
 * 
 * @author zives
 *
 */
public abstract class BaseTranscodeAction extends BaseSnapshotProcessingAction {
	
	public BaseTranscodeAction() {
		super(null, null, null, null);
	}
	
	public BaseTranscodeAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, TranscodeParameters parms) {
		super(mQueue, origin, session, parms);
		this.session = session;
	}

	@Override
	public TranscodeParameters getDefaultArguments() {
		return new TranscodeParameters();
	}
}
