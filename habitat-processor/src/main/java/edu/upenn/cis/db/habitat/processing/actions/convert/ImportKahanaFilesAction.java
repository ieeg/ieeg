package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.io.Files;
import com.jmatio.types.MLStructure;

import edu.upenn.cis.braintrust.thirdparty.matlab.IeegMatFileReader;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CopyAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.GrantPermissionsAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.actions.AddMontageToDataSetAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.EEGMontageParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaChannelParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaParameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;

public class ImportKahanaFilesAction extends BaseProcessingAction {

	public static final String NAME = "ImportKahanaFilesAction";
//	WorkItemSet workItems = new WorkItemSet("Work item set", null,getOriginatingItemSet().getRequest(), getOriginatingItemSet().getOrigin());
	RemoteHabitatSession session;
	ControlFileParameters parameters;
	String snapshotName;

	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	List<BufferedInputStream> iStreams = new ArrayList<BufferedInputStream>();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public ImportKahanaFilesAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			ControlFileParameters parameters) {
		super(mQueue, origin);
		final String m = "ImportKahanaFilesAction(WorkItemSet, IeegSession, ControlFileParameters)";
		this.session = session;
		this.parameters = parameters;
		if (logger.isTraceEnabled()) {
			try {
				final String paramsString = ObjectMapperFactory
						.newPasswordFilteredWriter()
						.writeValueAsString(this.parameters);
				logger.trace("{}: Creating Action with parameters {}", m,
						paramsString);
			} catch (JsonProcessingException e) {
				logger.error(m + ": Exception in debug log", e);
			}
		}
	}

	public ImportKahanaFilesAction() {
		super(null, null);
	}

	/*
	 * Create ChannelInfo for each MefFile
	 */

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";

		FileReference talStructFile = null;
		FileReference bpPairsFile = null;

		if (arguments != null && parameters != null
				&& arguments instanceof Parameters)
			parameters.merge((Parameters) arguments);
		else if (arguments != null)
			parameters = (ControlFileParameters) arguments;
		
		KahanaParameters kParams = new KahanaParameters();
		List<KahanaFileParameters> fileParams = kParams.getFileParams();
		try {
			boolean paramsSet = false;
			boolean montageSet = false;
			for (JsonTyped inFileSpec : parameters.getInputs()) {
				logger.debug("{}: ReadKahanaFile: {}", m, inFileSpec.toString());

				FileReference inFile = (FileReference) inFileSpec;

				File f = new File(inFile.getObjectKey());
				String name = f.getName();

				// Get the <exp>_monopol.mat file
				if (name.length() > 10) {
					if (name.toLowerCase().contains("monopol")){
						talStructFile = inFile;
					}
				}
				
				// Get the <exp>_monopol.mat file
				if (name.length() > 7) {
					if (name.toLowerCase().contains("bpPairs")){
						bpPairsFile = inFile;
					}
				}

				// Get all the data files
				String ext = Files.getFileExtension(name);
				if (!ext.isEmpty() && isNumeric(ext))
					fileParams.add(new KahanaFileParameters(inFile, kParams));

				if (name.contains("params")) {
					paramsSet = kParams.setOrCheckParams(inFile);
					if (!paramsSet)
						throw new IOException(
								"ImportKahanaFileAction: Multiple 'params' files with different values --> This is not supported.");
				}
				
				if (name.contains("bpPairs")) {
					if (montageSet) {
						logger.info("{}: Found multiple montage files. Replacing earlier montage with {}", m, inFile);
					} else {
						logger.info("{}: Set montage from {}", m, inFile);
					}
					montageSet = kParams.setMontage(inFile);
					
				}

			}

			if (!paramsSet) {
				logger.error("{}: Uploaded folder does not contain a '<exp>_params.txt' file.");
				throw new IOException(
						"No params file uploaded --> unable to import.");
			}

			if (talStructFile != null) {
				getChannels(talStructFile, kParams);
			} else {
				logger.error("{}: Uploaded folder does not contain a 'talLocs_database_monopol.mat' file.");
				throw new IOException(
						"No tallocs file uploaded --> unable to import.");
			}

		} catch (Throwable e) {
			// Cleanup and rethrow
			for (KahanaFileParameters params : fileParams) {
				try {
					params.close();
				} catch (IOException e2) {
					logger.info(
							m
									+ ": This exception came from close attempt and is being swalled. Original exception will be rethrown.",
							e2);
				}
			}
			throw e;
		}

		String institution = "Kahana";
		logger.info("{}: Assign institution: {}", m, institution);

		snapshotName = parameters.getTargetSnapshot();
		logger.info("{}: Using snapshot name: {}", m, snapshotName);

		boolean isHuman = parameters.getSnapshotParameters().isHuman();
		logger.info("{}: Setting isHuman to {}", m, isHuman);

		processSnapshot(kParams, isHuman, institution);

		return STATE.SUCCEEDED;
	}

	public static boolean isNumeric(String str)
	{
		try
		{
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe)
		{
			return false;
		}
		return true;
	}

	private void getChannels(FileReference talStructFile,
			KahanaParameters KParams) throws FileNotFoundException, IOException {

		IeegMatFileReader matFile = new IeegMatFileReader(talStructFile);

		MLStructure talStruct = (MLStructure) matFile.getContent().get(
				"talStruct");
		List<KahanaChannelParameters> chParmList = KParams.getChanParams();

		for (int i = 0; i < talStruct.getSize(); i++)
			chParmList.add(new KahanaChannelParameters(talStruct, i));

		logger.info("Reading TalStruct File");

	}

	private void processSnapshot(KahanaParameters KParams,
			boolean isHuman, String inst) {
		final String m = "processSnapshot(...)";
		if (KParams.getFileParams().isEmpty())
			return;

		List<String> channelIds = new ArrayList<String>();
		SnapshotParameters ssp = new SnapshotParameters("",
				null, SnapType.other, channelIds);

		ssp.merge(parameters);

		ssp.setOwner(inst);

		ssp.setHuman(isHuman);

		// Get Unique channel names
		List<SimpleChannel> chInfo = findUniqueChannels(KParams);
		//
		// Populate channelIds in SnapshotParameters
		for (SimpleChannel c : chInfo) {
			channelIds.add(c.getChannelName());
			logger.info("{}: Adding Channel: {}", m, c.getChannelName());
		}

		ssp.setTargetSnapshot(snapshotName);

		// Create snapshot as necessary
		IProcessingAction snap = new CreateSnapshotAction(
				getMessageQueue(), 
				this.getWorkItemSet(),
				session,
				ssp);
		getNextActions().add(snap);

		// Map metadata to it as necessary
		IProcessingAction map = new MapMetadataAction(
				getMessageQueue(), 
				this.getWorkItemSet(),
				session, ssp);
		snap.getNextActions().add(map);

		// Add Transcode action
		TranscodeFromKahanaAction transcode = new TranscodeFromKahanaAction(
				getMessageQueue(), 
				this.getWorkItemSet(), session, ssp, chInfo, KParams);

		// Add copy action for talStruct// Create new Workset
		WorkItemSet newSet = new WorkItemSet("KahanaMatFiles", null,getWorkItemSet().getRequest(), getWorkItemSet().getOrigin());
		WorkItemSet test = this.getWorkItemSet();

		for (JsonTyped i : parameters.getInputs()) {
			FileReference item = (FileReference) i;
			String name = ((FileReference) i).getFilePath();
			if (name.toLowerCase().contains("monopol") && name.substring(name.length() - 4).toLowerCase()
					.equals(".mat")) {
				WorkItem work = new WorkItem(
						item.getFilePath(),
						parameters.getTargetSnapshot(),
						item,
						null,
						getWorkItemSet().iterator().next().getCreator(),
						(new Date()).getTime(),
						"application/mat",
						"ProcSnapAction",
						null);
				newSet.add(work);
			}
			if (name.toLowerCase().contains("events") && name.substring(name.length() - 4).toLowerCase()
					.equals(".mat")) {
				WorkItem work = new WorkItem(
						item.getFilePath(),
						parameters.getTargetSnapshot(),
						item,
						null,
						getWorkItemSet().iterator().next().getCreator(),
						(new Date()).getTime(),
						"application/mat",
						"ProcSnapAction",
						null);
				newSet.add(work);
			}

		}

		ControlFileParameters newParams = parameters;
		CopyAction copyTalStruct = new CopyAction(getMessageQueue(), newSet, session, newParams);
		map.getNextActions().add(transcode);
		map.getNextActions().add(copyTalStruct);
		
		// Add Montage Action if we found one
		if (KParams.getBpMontage() != null) {
			EEGMontageParameters mParams = new EEGMontageParameters(KParams.getBpMontage());
			mParams.merge(ssp);
			AddMontageToDataSetAction addMontage = new AddMontageToDataSetAction(getMessageQueue(), newSet, session, mParams);
			map.getNextActions().add(addMontage);
		}
		
		// Grant permissions
		IProcessingAction perms = new GrantPermissionsAction(
				getMessageQueue(), 
				getWorkItemSet(),
				session,
				parameters.getSnapshotParameters());

		map.getNextActions().add(perms);

	}

	private List<SimpleChannel> findUniqueChannels(KahanaParameters KParams) {
		final String m = "findUniqueChannels(...)";
		List<KahanaFileParameters> params = KParams.getFileParams();

		List<SimpleChannel> chInfo = new ArrayList<SimpleChannel>();

		// Make map channelName <--> channel Index
		Map<Integer, String> chMap = new HashMap<>();
		for (KahanaChannelParameters ch : KParams.getChanParams()) {
			chMap.put(Integer.valueOf((int) Math.round(ch.getChannelIdx())),
					ch.getTagName());
		}

		for (KahanaFileParameters curParam : params) {
			SimpleChannel newChannel = new SimpleChannel(chMap.get(curParam
					.getChannel()),
					KParams.getSamplingRate(), KParams.getGain(), 0);

			if (!(chInfo.contains(newChannel))) {
				chInfo.add(newChannel);
			}

		}

		return chInfo;

	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new ImportKahanaFilesAction(mQueue, origin, session,
				(ControlFileParameters) arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

}
