/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RecordingItem")
public class MedtronicParameters {
	/*
	  <PatientID>rich1</PatientID>
	  <INSTimeStamp>9/8/2013 5:06:59 AM</INSTimeStamp>
	  <SPTimeStamp>9/8/2013 6:53:49 AM</SPTimeStamp>
	  <RecordingDuration>00:00:59</RecordingDuration>
	  <InitialTriggerAddr>325268</InitialTriggerAddr>
		 */

	String patientID;
	String INSTimeStamp;
	String SPTimeStamp;
	String RecordingDuration;
	Integer InitialTriggerAddr;

	Triggers triggers;
	SenseChannelConfig senseChannelConfig;
	AlgorithmConfig algorithmConfig;
	RecordingConfig recordingConfig;
	
	@XmlElement(name="PatientID")
	public String getPatientID() {
		return patientID;
	}
	@XmlElement(name="INSTimeStamp")
	public String getINSTimeStamp() {
		return INSTimeStamp;
	}
	@XmlElement(name="SPTimeStamp")
	public String getSPTimeStamp() {
		return SPTimeStamp;
	}
	@XmlElement(name="RecordingDuration")
	public String getRecordingDuration() {
		return RecordingDuration;
	}
	@XmlElement(name="InitialTriggerAddr")
	public Integer getInitialTriggerAddr() {
		return InitialTriggerAddr;
	}
	@XmlElement(name="Triggers")
	public Triggers getTriggers() {
		return triggers;
	}
	@XmlElement(name="SenseChannelConfig")
	public SenseChannelConfig getSenseChannelConfig() {
		return senseChannelConfig;
	}
	@XmlElement(name="AlgorithmConfig")
	public AlgorithmConfig getAlgorithmConfig() {
		return algorithmConfig;
	}
	@XmlElement(name="RecordingConfig")
	public RecordingConfig getRecordingConfig() {
		return recordingConfig;
	}
	
	
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	public void setINSTimeStamp(String iNSTimeStamp) {
		INSTimeStamp = iNSTimeStamp;
	}
	public void setSPTimeStamp(String sPTimeStamp) {
		SPTimeStamp = sPTimeStamp;
	}
	public void setRecordingDuration(String recordingDuration) {
		RecordingDuration = recordingDuration;
	}
	public void setInitialTriggerAddr(Integer initialTriggerAddr) {
		InitialTriggerAddr = initialTriggerAddr;
	}
	public void setTriggers(Triggers triggers) {
		this.triggers = triggers;
	}
	public void setSenseChannelConfig(SenseChannelConfig senseChannelConfig) {
		this.senseChannelConfig = senseChannelConfig;
	}
	public void setAlgorithmConfig(AlgorithmConfig algorithmConfig) {
		this.algorithmConfig = algorithmConfig;
	}
	public void setRecordingConfig(RecordingConfig recordingConfig) {
		this.recordingConfig = recordingConfig;
	}
	
	
}
