package edu.upenn.cis.db.habitat.processing.workflows;

import java.util.List;

public class IfElse extends Step {
	List<Step> ifSteps;
	
	List<Step> elseSteps;

	public IfElse(
			String stepName, 
			List<Input> inputs, 
			List<Output> outputs, 
			List<Step> ifSteps,
			List<Step> elseSteps
			) {
		super(stepName, inputs, outputs);

		this.ifSteps = ifSteps;
		this.elseSteps = elseSteps;
	}

	@Override
	public boolean bind(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean execute(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean revert(ExecutionContext context, ExecutionLog log) {
		// TODO Auto-generated method stub
		return false;
	}

	
}
