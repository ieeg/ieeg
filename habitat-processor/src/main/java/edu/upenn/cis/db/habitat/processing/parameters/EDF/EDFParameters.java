package edu.upenn.cis.db.habitat.processing.parameters.EDF;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.eeg.edf.EDFHeader;

public class EDFParameters implements Comparable<EDFParameters> {
	private EDFHeader header;
	private FileReference handle;
	private final IInputStream inStream;

	public EDFParameters(FileReference inFile) throws IOException {
		this.handle = inFile;
		IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);

		inStream = bestServer.openForInput(inFile);
		try (InputStream iStream = new BufferedInputStream(
				inStream.getInputStream())) {
			this.header = EDFHeader.createFromStream(iStream);
		}

	}

	public EDFHeader getHeader() {
		return header;
	}

	public FileReference getHandle() {
		return handle;
	}

	/**
	 * Returns input stream positioned at the start of the data.
	 * 
	 * @return input stream positioned at the start of the data
	 */
	public BufferedInputStream getiStream() throws IOException {
		final BufferedInputStream ret = new BufferedInputStream(
				inStream.getInputStream());
		ByteStreams.skipFully(ret, header.getBytesInHeader());
		return ret;
	}

	/**
	 * Closes the input stream and other associated resources.
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException {
		inStream.close();

	}

	public void setHeader(EDFHeader header) {
		this.header = header;
	}

	public String toString() {
		return "EDFParameters for: " + handle;
	}

	@Override
	public int compareTo(EDFParameters o) {
		final long thisStart = header.EDFDate2uUTC();
		final long otherStart = o.header.EDFDate2uUTC();
		final int startCompare = Long.compare(thisStart, otherStart);
		if (startCompare != 0) {
			return startCompare;
		}
		// If the start times are the same, we'll put the longer file first to
		// catch overlaps.
		return -Long.compare(getFileEndUutc(), o.getFileEndUutc());
	}

	public long getFileEndUutc() {
		long fileEndUutc = header.EDFDate2uUTC()
				+ (long) (header.getNumberOfRecords()
						* header.getDurationOfRecords() * 1e6);
		return fileEndUutc;
	}

	public static int safeLongToInt(long l) {
		return (int) Math
				.max(Math.min(Integer.MAX_VALUE, l), Integer.MIN_VALUE);
	}

}
