package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.io.HabitatMefWriter;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.BCI2000Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.convert.BCI2000Parameters.StateVec;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodeFromBCI2000Action extends BaseSnapshotProcessingAction {
	public static final class SimpleChannelMefWriter {
		private final SimpleChannel simpleChannel;
		private final HabitatMefWriter mefWriter;
		private boolean startedWriting = false;

		public SimpleChannelMefWriter(
				SimpleChannel simpleChannel,
				HabitatMefWriter mefWriter) {
			this.simpleChannel = checkNotNull(simpleChannel);
			this.mefWriter = checkNotNull(mefWriter);
		}

		public boolean isStartedWriting() {
			return startedWriting;
		}

		public void setStartedWriting(boolean startedWriting) {
			this.startedWriting = startedWriting;
		}

		public SimpleChannel getSimpleChannel() {
			return simpleChannel;
		}

		public HabitatMefWriter getMefWriter() {
			return mefWriter;
		}
	}

	final private Logger logger = LoggerFactory.getLogger(getClass());
	public static final String NAME = "TranscodeFromBCI2000Action";
	private static final int SAMPLESPERREAD = 5000;
	private BCI2000Parameters header = null;
	private List<SimpleChannel> chInfo; // List of Channel objects.
	private List<IStoredObjectReference> tempHandles;
	private long nrSamplesWritten = 0;
	private SnapshotParameters ssp;

	public TranscodeFromBCI2000Action(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			SnapshotParameters ssp, BCI2000Parameters header) {
		super(mQueue,origin,session,ssp);
		this.ssp = ssp; // capture full ssp in this action.
		this.header = header;
	}

	public TranscodeFromBCI2000Action() {
		super();
	}


	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";

		IObjectServer perServ = StorageFactory.getPersistentServer();
		IStoredObjectContainer perCont = perServ.getDefaultContainer();

		IStoredObjectReference inFile = header.getHandle();	
		IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
		IInputStream rawStream = bestServer.openForInput(inFile);
		


		// MEF Writers to each channel
		final Map<String, SimpleChannelMefWriter> labelToChannelWriter = newHashMap();
		// The actual output file handles
		tempHandles = new ArrayList<IStoredObjectReference>();

		chInfo = new ArrayList<SimpleChannel>();
		List<String> allLabels = ssp.getChannelLabels();

		if (allLabels.size()!=header.getNrChannels()){
			logger.error(
					"{}: Error: Labels vector has different size: {} vs {} in file {}",
					m, allLabels.size(), header.getNrChannels(),
					inFile.toString());
			return STATE.FAILED;
		}


		for (int i=0 ; i< header.getNrChannels();i++) {

			String label = allLabels.get(i);
			double mefScale = 1/(header.getSourceChGain().get(i));
			double baseLine = header.getSourceChOffset().get(i);

			SimpleChannel ch = new SimpleChannel(label, header.getSamplingRate(), 
					mefScale , baseLine);

			chInfo.add(ch);

			String fileName = ch.getChannelName() + ".mef";
			File ff = new File(header.getHandle().getFilePath());
			File file2 = new File(ff.getParent(), fileName);
			FileReference mefFile = new FileReference((DirectoryBucketContainer) perCont, 
					file2.getPath(), file2.getPath()); 

			
			tempHandles.add(mefFile);
			HabitatMefWriter newWriter = new HabitatMefWriter(
					perServ, 
					mefFile, 
					ch);
			labelToChannelWriter.put(
					ch.getChannelName(),
					new SimpleChannelMefWriter(ch, newWriter));

		}


		// Open inputstream for each raw data file.
		long previousFileEndUutc = -1;
		try {



			// Set StartTime and initiate buffer to read from Raw File
//			int nrSamples = header. params.getNrSamplesPerSignal();
			
			
			
//			int totalNrSamples = nrSamples;

			long fileStartTS = Math.round(1e6/header.getSamplingRate()); //params.Date2uUTC();

			try (final BufferedInputStream curStream = new BufferedInputStream(
					rawStream.getInputStream())) {
				
				// skip header offset
				curStream.skip(header.getHeaderLength());
				
				int stateVecLen = header.getStateVecLenght();
				

				int dataValueSize = 2; // for int16
				int totalNrSamples = (int) (rawStream.getLength()-header.getHeaderLength())/(dataValueSize*header.getNrChannels() + header.getStateVecLenght());

				long blockStartUutc = fileStartTS;
				// Iterate of data as long as there is data and write blocks
				// to MEF
				while (this.nrSamplesWritten < totalNrSamples) {

					int[][] datByChan;
					switch(header.getDataFormat()){
					case "int16": 
						datByChan = readFormatInt16(curStream, totalNrSamples, allLabels.size());
						break;
					default:
						throw(new Exception("TRANSCODEBCI2000: Do not recognize data format for converter."));

					}

					int samplesInCurBlock = datByChan[0].length;
					//								nrSamplesWritten += samplesInCurBlock * allLabels.size();

					// Iterate over Labels in file and write to correct
					// MEFWriter
					long[] stamps = new long[samplesInCurBlock];
					for (int iCh = 0; iCh < allLabels.size(); iCh++) {
						String curLab = allLabels.get(iCh);

						final SimpleChannelMefWriter curChWriter = labelToChannelWriter
								.get(curLab);
						if (curChWriter == null) {
							logger.error(
									"{}: Error: label not found: {} in {} in file {}",
									m, curLab, chInfo,
									header.toString());
							continue;
						}
						//
						SimpleChannel curInfo = curChWriter.getSimpleChannel();
						HabitatMefWriter curWriter = curChWriter.getMefWriter();


						// Create TimeStamp vector
						int[] samps = new int[samplesInCurBlock];
						stamps[0] = blockStartUutc;
						logger.trace("{} FileStartTime: {}", m,
								stamps[0]);
						samps[0] = (int) (curInfo.getVoltageOffset() + datByChan[iCh][0]);
						for (int j = 1; j < samplesInCurBlock; j++) {
							samps[j] = (int) (curInfo
									.getVoltageOffset() + datByChan[iCh][j]);
							//
							stamps[j] = (long) (stamps[j - 1] + 1000000 / curInfo
									.getSamplingFrequency());
						}
						curWriter.writeData(samps, stamps);
						curChWriter.setStartedWriting(true);
					}
					blockStartUutc += 1000000 * (1 / header.getSamplingRate())
							* samplesInCurBlock;
				}
				previousFileEndUutc = blockStartUutc;
				logger.debug(m + ": Timestamp at end of file = "
						+ previousFileEndUutc);

			} catch (IOException e) {
				logger.error(m + ": Exception. Returning STATE.FAILED", e);
				return STATE.FAILED;
			} finally {
				rawStream.close();
			}
			//					}
		} finally {
			logger.debug("{} Closing MEF Files", m);
			for (SimpleChannelMefWriter w : labelToChannelWriter.values()) {
				w.getMefWriter().close();
			}
		}

		ParametersForSnapshotContent snapP = new ParametersForSnapshotContent();

		snapP.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		snapP.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());

		for (IStoredObjectReference s : tempHandles) {
			snapP.getInputs().add(s);
		}

		getNextActions().add(
				new AddToSnapshotAction(
						getMessageQueue(),
						getWorkItemSet(),
						getSession(),
						snapP));

		return STATE.SUCCEEDED;
	}

	private int[][] readFormatInt16(BufferedInputStream curStream, int totalNrSamples, int size) throws IOException {
  		final String m = "Read:";
		// Get fixed block size or partial block if near EOF
		int samplesInCurBlock = SAMPLESPERREAD;
		int bPerBlock = (int) ((header.getNrChannels()*2)+header.getStateVecLenght())* SAMPLESPERREAD;
		byte[] b = new byte[bPerBlock];
		ByteBuffer byteBuf = ByteBuffer.wrap(b).order(ByteOrder.LITTLE_ENDIAN);
		try {
			
			
			if ((totalNrSamples - nrSamplesWritten) > SAMPLESPERREAD) {
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += SAMPLESPERREAD;
			} else
			{
				samplesInCurBlock = ((int) (totalNrSamples - this.nrSamplesWritten));
				bPerBlock = (int) (header.getNrChannels() * samplesInCurBlock * 2);
				b = new byte[bPerBlock];
				ByteStreams.readFully(curStream, b);
				this.nrSamplesWritten += samplesInCurBlock;
				logger.debug(m
						+ ": Getting last partial block of data from BNI Raw Data file.");
			}
		} catch (EOFException e) {
			logger.error(m
					+ ": Unexpected EOF while reading BNI Raw data file." , e);
		}

		byteBuf.rewind();

		// Reorder
		int[][] datByChan = new int[chInfo.size() + header.getStateVecList().size()][samplesInCurBlock];
		byte[] state = new byte[header.getStateVecLenght()];
		for (int j = 0; j < samplesInCurBlock; j++) {
			for (int i = 0; i < chInfo.size(); i++) {
				datByChan[i][j] = byteBuf.getShort();
			}
			
			byteBuf.get(state);
			int ii = 0;
			
			byte[] stateP = new byte[(state.length + 1)];
			System.arraycopy(state, 0, stateP,0, state.length);
			System.arraycopy(new byte[]{0},0, stateP, state.length, 1);
			
			for (StateVec s: header.getStateVecList()){
				BigInteger bigInt = new BigInteger(stateP);
				byte[] subArray;
				if(s.getBitLoc()>0){
					// Byteshift on BigInteger
					bigInt.shiftRight(8-s.getBitLoc());
					int newByteLoc = s.getByteLoc() + ((s.getBitLoc()>0) ? 1:0);
					subArray = Arrays.copyOfRange(bigInt.toByteArray(), newByteLoc, newByteLoc + 4);
				} else {
					subArray = Arrays.copyOfRange(stateP, s.getByteLoc(), s.getByteLoc() + 4);
				}

				
				
				
				
				// Parse into standard integer
				int val = ByteBuffer.wrap(subArray).getInt();
				val = val >>> (32-s.getVecLengt());
				int iidx = chInfo.size()+ii;
				datByChan[iidx][j] = val;
			}
			
			
			
		}
		
		return datByChan;
	}
	public static byte[] intToByteArray(int a)
	{
	    byte[] ret = new byte[4];
	    ret[3] = (byte) (a & 0xFF);   
	    ret[2] = (byte) ((a >> 8) & 0xFF);   
	    ret[1] = (byte) ((a >> 16) & 0xFF);   
	    ret[0] = (byte) ((a >> 24) & 0xFF);
	    return ret;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new TranscodeFromBCI2000Action(mQueue, origin, session,
				(SnapshotParameters) arguments, null);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}
}
