/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.S3ObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copy a file to our target container (typically our local S3 instance, but
 * depends on StorageFactory.getPersistentServer().)
 * 
 * @author zives
 *
 */
public class CopyAction extends BaseSnapshotProcessingAction {
	
	public final static String NAME = "CopyAction";
	private final static Logger logger = LoggerFactory.getLogger(CopyAction.class);

	/**
	 * Destination server
	 */
	IObjectServer to;
	
	public CopyAction() {
		super();
	}
	
	/**
	 * Full constructor including a specified target server for the copy
	 * 
	 * @param items Objects to copy
	 * @param session IEEG session connection
	 * @param parms Snapshot parameters for the target snapshot
	 * 			    (we will add this file to the contents of the snapshot
	 * 				 by triggering AddToSnapshotAction)
	 * @param to Destination server
	 */
	public CopyAction(IMessageQueue mQueue, 
			WorkItemSet items, 
			RemoteHabitatSession session, 
			ParametersForSnapshotContent parms, 
			IObjectServer to) {
		super(mQueue, items, session, parms);

		this.parameters = parms;
		this.to = to;
	}
	
	/**
	 * Constructor that copies to the persistent storage volume
	 * 
	 * @param items Objects to copy
	 * @param session IEEG session connection
	 * @param parms Snapshot parameters for the target snapshot
	 * 			    (we will add this file to the contents of the snapshot
	 * 				 by triggering AddToSnapshotAction)
	 */
	public CopyAction(IMessageQueue mQueue, WorkItemSet items, RemoteHabitatSession session, ParametersForSnapshotContent parms) {
		super(mQueue, items, session, parms);

		this.parameters = parms;
		this.to = StorageFactory.getPersistentServer();
	}
	
	/**
	 * Main routine:  classify the snapshot as human or animal and create
	 * an S3 path specifier that matches this.  Copy the object.
	 * Trigger AddToSnapshotAction to register the data with the designated
	 * snapshot.
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final IUser creator = getCreator();
		try {
			if (arguments != null && getCurrentParameters() == null) {
				setParameters((ParametersForSnapshotContent)arguments);
			} else if (parameters != null && arguments != null)
				getCurrentParameters().merge((ParametersForSnapshotContent)arguments);
			if (!(creator instanceof User)) {
				throw new IllegalStateException(
						"The creator must have type "
								+ User.class.getCanonicalName()
								+ ". It has type "
								+ creator.getClass().getCanonicalName());
			}
			final ParametersForSnapshotContent destinationParameters = new ParametersForSnapshotContent();
			destinationParameters.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
			destinationParameters.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());
			
			List<IStoredObjectReference> workSet = new ArrayList<>();
			// All inputs plus any parameters
			for (IWorkItem in2: this.getWorkItemSet()) {
				JsonTyped in = in2.getMainHandle();
				IStoredObjectReference item = (IStoredObjectReference)in;
				workSet.add(item);
			}
//			for (JsonTyped in: parameters.getInputs()) {
//				IStoredObjectReference item = (IStoredObjectReference)in;
//				workSet.add(item);
//			}
			
//			for (JsonTyped in: parameters.getInputs()) {
//			for (IWorkItem in2: this.getOriginatingItemSet()) {
//				JsonTyped in = in2.getMainHandle();
//				IStoredObjectReference item = (IStoredObjectReference)in;
			for (IStoredObjectReference item: workSet) {
				String destinationKey = item.getObjectKey();
				
				if (parameters.getTargetSnapshot() == null)
					return STATE.FAILED;

				SnapshotParameters parms = session.getSnapshotParametersFor((User)creator, parameters.getTargetSnapshot());
				
				final String defaultDestinationPrefix = parms
						.getDefaultDestination();
				final Path baseFilename = Paths.get(destinationKey).getFileName();
				if (defaultDestinationPrefix != null && baseFilename != null) {
						destinationKey = defaultDestinationPrefix + baseFilename.toString();
				} else {
					String prefix = (parms.isHuman()) ? "Human_Data"
							: "Animal_Data";

					String userIdPrefix = ((User) creator).getUserId()
							.getValue() + "/";
					if (destinationKey.startsWith(userIdPrefix)) {
						// Strip off user id which was added when using default
						// inbox
						destinationKey = destinationKey.substring(userIdPrefix
								.length());
					}
					if (!destinationKey.startsWith(prefix)) {
						destinationKey =
								prefix
										+ "/"
										+ (creator.getProfile()
												.getInstitutionName()
												.isPresent() ?
												creator.getProfile()
														.getInstitutionName()
														.get() :
												"everywhere")
										+ "/"
										+ destinationKey;

					}
					while (destinationKey.contains("//"))
						destinationKey = destinationKey.replace("//", "/");
				}
				
				final FileReference destination = new FileReference(
						(DirectoryBucketContainer) to.getDefaultContainer(),
						destinationKey, 
						destinationKey);
				destinationParameters.getInputs().add(destination);


				IObjectServer from;
				if (IvProps.getIvProps().get("inboxBucket") !=null ) {
					// we're using fine uploader and the inbox is in S3
					from = StorageFactory.getS3Inbox();
				} else {
					from = StorageFactory.getBestServerFor(item);
				}

				// source from S3 inbox
				logger.info("Copying " + item.getDirBucketContainer().getBucket() +"/" + item.getFilePath() +
						(from instanceof S3ObjectServer ? "/S3" : "/filesystem") + " to " + destination.getFilePath());

				getMessageQueue().logEvent(creator, WorkerRequest.PORTAL, WorkerRequest.ETL, getWorkItemSet(), 
						"Copying " + item.getFilePath() +
						(from instanceof S3ObjectServer ? "/S3" : "/filesystem") +
						" to " + destination.getFilePath());
				
				// Record the association
				session.associateCopiedFile(item, destination);
				
				IInputStream instream = from.openForInput(item);
				IOutputStream outstream = to.openForOutput(destination);
				
				derived.add(new DerivedItem("file", destinationKey));
				
				outstream.copyFrom(instream);
				instream.close();
				outstream.close();
//				System.out.println("Copied " + item);
				getMessageQueue().logEvent(creator, WorkerRequest.PORTAL, WorkerRequest.ETL,  getWorkItemSet(), 
						"Successfully copied " + item);
				
			}
			
			getNextActions().add(new AddToSnapshotAction(
					getMessageQueue(),
					getWorkItemSet(), session,
					destinationParameters));
		} catch (Exception ioe) {
			getMessageQueue().logEvent(creator, WorkerRequest.PORTAL, WorkerRequest.ETL, getWorkItemSet(), 
					"Error processing: " + ioe.getMessage());
			logger.error("CopyAction failed", ioe);
			ioe.printStackTrace();
			return STATE.FAILED;
		}

		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			Parameters arguments) {
		return new CopyAction(mQueue, origin, session, (ParametersForSnapshotContent)arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}

}
