/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import javax.annotation.Nullable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import edu.upenn.cis.braintrust.dao.HibernateDAOFactory;
import edu.upenn.cis.braintrust.dao.IControlFileDAO;
import edu.upenn.cis.braintrust.dao.IDAOFactory;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class HibernateWorkQueue extends BasicWorkQueue {
	protected final IDAOFactory daoFactory;
	protected SessionFactory sessionFactory;
	private final String queueName;

	public HibernateWorkQueue(@Nullable String queueName) {
		this(
				HibernateUtil.getSessionFactory(),
				new HibernateDAOFactory(),
				queueName);
	}

	public HibernateWorkQueue() {
		this(
				HibernateUtil.getSessionFactory(),
				new HibernateDAOFactory(),
				null);
	}

	protected HibernateWorkQueue(
			SessionFactory sessionFactory,
			IDAOFactory daoFactory,
			@Nullable String queueName) {
		final String m = "HibernateWorkQueue(...)";
		this.sessionFactory = sessionFactory;
		this.daoFactory = checkNotNull(daoFactory);
		this.queueName = queueName;

		logger.info("{}: HIBERNATE WorkQueue attaching to database connection",
				m);
		if (this.queueName == null) {
			logger.info("{}: No queue specified for HIBERNATE WorkQueue", m);
		} else {
			logger.info(
					"{}: HIBERNATE WorkQueue watching queue {}",
					m,
					queueName);
		}
		loadFromExternal();
	}

	public boolean isPeekable() {
		return true;
	}

	private void loadFromDb(@Nullable String queueName) {
		final String m = "loadFromDb(...)";
		Session session = null;

		Transaction trx = null;
		try {
			session = sessionFactory.openSession();
			trx = session.beginTransaction();
			IControlFileDAO controlFileDAO = daoFactory
					.getControlFileDAO(session);
			final List<ControlFileEntity> controlFiles = queueName == null
					? controlFileDAO
							.findOrderedByCreateTimeAndId(10)
					: controlFileDAO.findByMetadataOrderedByCreateTimeAndId(
							queueName, 10);
			logger.debug(
					"{}: Loaded {} control files from database",
					m,
					controlFiles.size());
			for (final ControlFileEntity controlFile : controlFiles) {
				final User creator = userService
						.findUserByUid(controlFile
								.getCreator()
								.getId());
				final DirectoryBucketContainer container = new CredentialedDirectoryBucketContainer(
						controlFile.getBucket(),
						"");
				final IStoredObjectReference handle = new FileReference(
						container,
						controlFile.getFileKey(),
						controlFile.getFileKey());
				final String mediaType = getMediaType(controlFile.getFileKey());
				final String description =
						mediaType
								.equals("application/directory")
								? "Directory "
										+ controlFile.getFileKey()
								: "Control File "
										+ controlFile.getFileKey();
				final WorkItem workItem = new WorkItem(
						controlFile.getId().toString(),
						null,
						handle,
						null,
						creator,
						controlFile.getCreateTime().getTime(),
						mediaType,
						description,
						null);
				add(workItem);
				controlFileDAO.delete(controlFile);
			}
			trx.commit();
		} catch (RuntimeException e) {
			PersistenceUtil.rollback(trx);
			logger.error(m + ": Could not load work items from database", e);
		} finally {
			PersistenceUtil.close(session);
		}
	}

	@Override
	public void loadFromExternal() {
		loadFromDb(queueName);
	}

	@Override
	public void sendDone(WorkItemSet originalItem, String requester,
			String message) {
		// TODO Record message in Hibernate??

	}
}
