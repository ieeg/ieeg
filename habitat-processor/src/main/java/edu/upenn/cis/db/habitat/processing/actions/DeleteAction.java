package edu.upenn.cis.db.habitat.processing.actions;

import java.util.Set;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/*
 * Deletes items in the WorkItemSet from the staging server.
 * This is primarily used when files are copied to the staging
 * server prior to a shellAction Action.
 * 
 */
public class DeleteAction extends BaseProcessingAction {

	public final static String NAME = "DeleteAction";

	public DeleteAction(IMessageQueue mQueue, WorkItemSet itemSet) {
		super(mQueue, itemSet);
	}

	DeleteAction() {
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		
		try{
			IObjectServer localServer = StorageFactory.getStagingServer();
			WorkItemSet items = this.getWorkItemSet();
			for (IWorkItem item: items){
				localServer.deleteItem(item.getMainHandle());
			}
		}
		catch (Exception ioe) {
			ioe.printStackTrace();
			return STATE.FAILED;
		}
		
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue queue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new DeleteAction(queue, origin);
	}

	@Override
	public Parameters getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Parameters getCurrentParameters() {
		// TODO Auto-generated method stub
		return null;
	}

}
