/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Maps.newHashMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;


/**
 * Basic processing action
 * 
 * @author zives
 *
 */
public abstract class BaseProcessingAction implements IProcessingAction {
	private Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	private List<WorkItemSet> nextWorkItems = new ArrayList<WorkItemSet>();
	
	private WorkItemSet itemSet;
	IMessageQueue messageQueue;
	
	public BaseProcessingAction() {
		messageQueue = null;
	}
	
	public BaseProcessingAction(IMessageQueue mQueue, WorkItemSet itemSet) {
		this.itemSet = itemSet;
		messageQueue = mQueue;
	}

	@Override
	public Set<IProcessingAction> getNextActions() {
		return nextActions;
	}

	public void setNextActions(Set<? extends IProcessingAction> next) {
		nextActions.addAll(next);
	}
	
	public void addNextAction(IProcessingAction next) {
		nextActions.add(next);
	}

	@Override
	public List<WorkItemSet> getNextWorkItems() {
		return nextWorkItems;
	}

	public void setNextWorkItems(List<WorkItemSet> nextWorkItems) {
		this.nextWorkItems = nextWorkItems;
	}
	
	@Override
	public WorkItemSet getWorkItemSet() {
		return itemSet;
	}
	
	@Override
	public boolean isReady() {
		return true;
	}
	
	@Override
	public IUser getCreator() {
		final Map<String, IUser> creatorCandidates = newHashMap();
		for (IWorkItem workItem : itemSet) {
			IUser itemCreator = workItem.getCreator();
			creatorCandidates.put(
					itemCreator.getUsername(),
					itemCreator);
		}

		if (creatorCandidates.size() != 1) {
			throw new IllegalStateException(
					"There are "
							+ creatorCandidates.size()
							+ " possible creators. There must be exactly one.");
		}
		final Entry<String, IUser> creatorEntry = getOnlyElement(creatorCandidates.entrySet());
		return creatorEntry.getValue();
	}
	
	@Override
	public IMessageQueue getMessageQueue() {
		return messageQueue;
	}
}
