/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * Import annotations from a Grass TWin CSV file
 * 
 * @author John Frommeyer
 *
 */
public class GrassTwinAnnotationImporter implements IAnnotationImporter {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final static Splitter csvSplitter = Splitter.on(',')
			.trimResults()
			.limit(3);
	private final static Splitter timeSplitter = Splitter.on(':')
			.trimResults()
			.limit(3);
	private final static Splitter secondSplitter = Splitter.on('.')
			.trimResults()
			.limit(2);

	@Override
	public List<TsAnnotationDto> importAnnotations(
			User creator,
			DataSnapshot dataset,
			FileReference annotationFileRef)
			throws AnnotationImportException {
		final String m = "importAnnotations(...)";
		checkArgument(dataset.getRecordingStartTimeUutc().isPresent());
		final long recordingStartTimeUutc = dataset.getRecordingStartTimeUutc()
				.get();
		try {
			final IObjectServer bestServer = StorageFactory
					.getBestServerFor(annotationFileRef);
			final List<TsAnnotationDto> annotations = new ArrayList<>();
			final IInputStream inStream = bestServer
					.openForInput(annotationFileRef);
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(inStream.getInputStream()))) {
				Calendar lowerBound = Calendar.getInstance();
				lowerBound
						.setTimeInMillis((long) (recordingStartTimeUutc / 1000.0));
				String line = null;
				while ((line = reader.readLine()) != null) {
					lowerBound = lineToAnnotation(
							annotations,
							creator,
							dataset,
							lowerBound,
							line);
				}
				return annotations;
			} catch (AuthorizationException e) {
				throw new AnnotationImportException(e);
			} finally {
				if (inStream != null) {
					inStream.close();
				}
			}
		} catch (IOException e) {
			throw new AnnotationImportException(e);
		}

	}

	private Calendar lineToAnnotation(
			List<TsAnnotationDto> annotations,
			User creator,
			DataSnapshot dataset,
			Calendar lowerBound,
			String line) {

		final List<String> split = csvSplitter.splitToList(line);
		final String timeStr = split.get(1);
		final String comment = split.get(2);
		final Calendar annStart = calculateAnnStart(
				lowerBound,
				timeStr);
		if (!isNullOrEmpty(comment)) {
			final long annOffsetMicros = annStart.getTimeInMillis() * 1000
					- dataset.getRecordingStartTimeUutc().get();
			final TsAnnotationDto ann = new TsAnnotationDto(
					dataset.getTimeSeries(),
					creator.getUsername(),
					annOffsetMicros,
					annOffsetMicros,
					comment,
					null,
					comment,
					null,
					"Imported Grass TWin annotations",
					null);
			annotations.add(ann);
		}
		return annStart;
	}

	private Calendar calculateAnnStart(
			Calendar lowerBound,
			final String timeStr) {
		final String m = "calculateAnnStart(...)";

		final List<String> timeSplit = timeSplitter.splitToList(timeStr);
		final Integer hourOfDay = Integer.valueOf(timeSplit.get(0));
		final Integer minute = Integer.valueOf(timeSplit.get(1));
		final List<String> secondSplit = secondSplitter.splitToList(timeSplit
				.get(2));
		final Integer second = Integer.valueOf(secondSplit.get(0));
		final Integer millisecond = Integer.valueOf(secondSplit.get(1));

		final Calendar annStart = (Calendar) lowerBound.clone();
		annStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
		annStart.set(Calendar.MINUTE, minute);
		annStart.set(Calendar.SECOND, second);
		annStart.set(Calendar.MILLISECOND, millisecond);

		if (annStart.before(lowerBound)) {
			logger.debug("{}: Incrementing day", m);
			annStart.add(Calendar.DAY_OF_MONTH, 1);
		}
		if (logger.isDebugEnabled()) {
			final SimpleDateFormat debugFormat = (SimpleDateFormat) DateFormat
					.getDateTimeInstance();
			debugFormat.applyPattern("MMM d, yyyy h:mm:ss.SS a");
			logger.debug(
					"{}: Annotation will start at {}",
					m,
					debugFormat.format(annStart.getTime()));
		}
		return annStart;
	}
}
