/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.actions.NatusTextAnnotations.NatusTextAnnotation;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * @author John Frommeyer
 *
 */
public final class NatusAnnotationImporter implements IAnnotationImporter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public List<TsAnnotationDto> importAnnotations(
			User creator,
			DataSnapshot dataset,
			FileReference annotationFileRef) throws
			AnnotationImportException {
		try {
			final IObjectServer bestServer = StorageFactory
					.getBestServerFor(annotationFileRef);

			final IInputStream inStream = bestServer
					.openForInput(annotationFileRef);
			final NatusTextAnnotations natusAnnotations = NatusTextAnnotations
					.read(inStream.getInputStream());
			final List<TsAnnotationDto> annotations = processAnnotations(
					creator, dataset, natusAnnotations);

			return annotations;
		} catch (IOException e) {
			throw new AnnotationImportException(e);
		}
	}

	private List<TsAnnotationDto> processAnnotations(
			User creator,
			DataSnapshot dataset,
			NatusTextAnnotations natusTextAnnotations) {
		final String m = "processAnnotations(...)";
		checkNotNull(creator);
		checkNotNull(dataset);
		checkNotNull(natusTextAnnotations);

		final Date startTime = natusTextAnnotations.getRecordingStartTime()
				.getTime();
		final long startTimeUutc = startTime.getTime() * 1000;
		if (logger.isDebugEnabled()) {
			final DateFormat debugFormat = DateFormat.getDateTimeInstance();
			logger.debug(
					"{}: Using annotation start time of {} as base time",
					m, debugFormat.format(startTime));
		}
		final List<TsAnnotationDto> annotations = new ArrayList<>();
		final Calendar baseTimeCal = Calendar.getInstance();
		baseTimeCal.setTime(startTime);
		for (final NatusTextAnnotation textAnn : natusTextAnnotations
				.getTextAnnotations()) {
			logger.debug("{}: processing: {}",
					m,
					textAnn);
			final int day = textAnn.day;
			final Calendar annStart = (Calendar) baseTimeCal.clone();
			annStart.set(Calendar.HOUR_OF_DAY, textAnn.hourOfDay);
			annStart.set(Calendar.MINUTE, textAnn.minute);
			annStart.set(Calendar.SECOND, textAnn.second);
			if (day > 1) {
				logger.debug("{}: Incrementing to day {}", m, day);
				final int increment = day - 1;
				annStart.add(Calendar.DAY_OF_MONTH, increment);
			}
			if (logger.isDebugEnabled()) {
				final DateFormat debugFormat = DateFormat.getDateTimeInstance();
				logger.debug("{}: Annotation will start at {}", m,
						debugFormat.format(annStart.getTime()));
			}
			final long annTimeUutc = annStart.getTime().getTime() * 1000;
			final long annOffsetMicros = annTimeUutc - startTimeUutc;
			final TsAnnotationDto ann = new TsAnnotationDto(
					dataset.getTimeSeries(),
					creator.getUsername(),
					annOffsetMicros,
					annOffsetMicros,
					textAnn.text,
					null,
					textAnn.text,
					null,
					"Imported Natus annotations",
					null);
			annotations.add(ann);
		}
		return annotations;

	}

}
