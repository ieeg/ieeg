/*******************************************************************************
 * Copyright 2015, IEEG.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.EEGMontageParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaParameters;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Adds a  montage to a dataset
 * 
 * @author joostw
 *
 */
public class AddMontageToDataSetAction extends BaseSnapshotProcessingAction {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public final static String NAME = "addMontageToSnapshotAction";

	private EEGMontageParameters montageParams;
	
	public AddMontageToDataSetAction() {
		super(null, null, null, null);
	}

	public AddMontageToDataSetAction(IMessageQueue mQueue, WorkItemSet origin,RemoteHabitatSession session,
			EEGMontageParameters mParams) {
			super(mQueue, origin, session, mParams);
			this.montageParams = mParams;
			
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";
				
		if (parameters == null && arguments != null)
			parameters = (ControlFileParameters) arguments;
		else if (parameters != null && arguments != null)
			parameters.merge((Parameters) arguments);
		IUser creator = getCreator();
		if (!(creator instanceof User)) {
			throw new IllegalStateException(
					"The creator must have type "
							+ User.class.getCanonicalName()
							+ ". It has type "
							+ creator.getClass().getCanonicalName());
		}
//		SnapshotParameters snap = session.getSnapshotParametersFor(
//				(User) creator,
//				parameters.getTargetSnapshot());
//
//		int channel = 0;
		
//		DataSnapshot out = session.getSnapshotFor((User) creator, parameters.getTargetSnapshot());
		
		session.addMontage((User) creator, montageParams);
		
		//
//		for (JsonTyped item : parameters.getInputs()) {
//			FileReference handle = (FileReference) item;
//
//			if (RegisterActions.getMimeType(handle).equals("application/mef")) {
//				logger.info("{}: Adding the time series {} to {}",
//						m,
//						handle,
//						getCurrentParameters().getTargetSnapshot());
//
//				String channelRevId = session.addTimeSeriesToSnapshot(
//						(User) creator,
//						session.getSnapshotIdFor((User) creator,
//								getCurrentParameters().getTargetSnapshot()),
//						channel,
//						snap,
//						handle);
//
//				snap.setChannelId(snap.getChannelLabels().get(channel),
//						channelRevId);
//
//				channel++;
//			} else {
//				logger.info("{}: Adding the file {} to {}",
//						m,
//						handle,
//						getCurrentParameters().getTargetSnapshot());
//				String fileName = new File(handle.getFilePath()).getName();
//				session.registerHandleFor((User) creator,
//						session.getSnapshotIdFor((User) creator,
//								getCurrentParameters().getTargetSnapshot()),
//						fileName,
//						handle);
//
//				snap.addOtherFile(handle.getFilePath());
//			}
//		}
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new AddMontageToDataSetAction(mQueue, origin, session,
				(EEGMontageParameters) arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}

}
