package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.convert.BNI1Parameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.SimpleChannel;

/*
 * ImportBNI1FilesAction is triggered when one or more files exist with 
 * the .bni extension in the uploaded folder. BNI files have accompanying 
 * .eeg files with the raw data. The first file has a .eeg extension, the
 * following files have .001 ... .xxx extension.  
 */
public class ImportBNI1FilesAction extends BaseProcessingAction {

    public static final String NAME = "ImportBNI1FilesAction";
//    WorkItemSet workItems = new WorkItemSet("Work item set", null,getOriginatingItemSet().getRequest(), getOriginatingItemSet().getOrigin());
    RemoteHabitatSession session;
    ControlFileParameters parameters;
    String snapshotName;

    Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
    List<BufferedInputStream> iStreams = new ArrayList<BufferedInputStream>();

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public ImportBNI1FilesAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
            ControlFileParameters parameters) {
        super(mQueue, origin);
        final String m = "ImportBNI1FilesAction(WorkItemSet, IeegSession, ControlFileParameters)";
        this.session = session;
        this.parameters = parameters;
        if (logger.isTraceEnabled()) {
            try {
                final String paramsString = ObjectMapperFactory.newPasswordFilteredWriter()
                        .writeValueAsString(this.parameters);
                logger.trace("{}: Creating Action with parameters {}", m,
                        paramsString);
            } catch (JsonProcessingException e) {
                logger.error(m + ": Exception in debug log", e);
            }
        }
    }

    public ImportBNI1FilesAction() {
        super(null, null);
    }

    @Override
    public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
      /* 
       * Execute will:
       * 1) create list of BNI parameters from the all the BNI files
       * 2) call processSnapshot which:
       *    a) finds unique channels in dataset
       *    b) add CreateSnapshotAction to NextActions
       *        1) Depending on isHuman parameter in SnapshotParameters, creates Human, or Animal Dataset 
       *    c) add MapMetaData to NextActions
       *        1) Maps generic info from SnapshotParameters, creates HospitalAdmission or Animal
       *    d) add TranscodeFromBNI1 to NextActions
       *        1) Transcodes to MEF files
       *        2) add AddToSnapshotAction to NextActions
       */
      
        final String m = "execute(...)";
        if (arguments != null && parameters != null
                && arguments instanceof Parameters)
            parameters.merge((Parameters) arguments);
        else if (arguments != null)
            parameters = (ControlFileParameters) arguments;

        List<BNI1Parameters> myBNIParams = new ArrayList<BNI1Parameters>();

        try {
            for (JsonTyped inFileSpec : parameters.getInputs()) {
                logger.debug("{}: ReadEdfFile: {}", m, inFileSpec.toString());
//
                FileReference inFile = (FileReference) inFileSpec;
//
                myBNIParams.add(new BNI1Parameters(inFile));
//
            }
        } catch (IOException e) {            
            throw e;
        }
//
        String institution = parameters.getSnapshotParameters()
                .getInstitution();
        logger.debug("{}: Assign institution: {}", m, institution);
        
        String owner = parameters.getSnapshotParameters()
                .getOwner();
        logger.debug("{}: Assign owner: {}", m, owner);
//
        snapshotName = parameters.getTargetSnapshot();
        logger.debug("{}: Using snapshot name: {}", m, snapshotName);
//
        boolean isHuman = parameters.getSnapshotParameters().isHuman();
        logger.debug("{}: Setting isHuman to {}", m, isHuman);

        processSnapshot(myBNIParams, isHuman, institution, owner);

        return STATE.SUCCEEDED;
    }

    private void processSnapshot(
    		List<BNI1Parameters> parmsList,
            boolean isHuman, 
            String inst,
            String owner) {
        final String m = "processSnapshot(...)";
        if (parmsList.isEmpty())
            return;

        List<String> channelIds = new ArrayList<String>();
        SnapshotParameters ssp = new SnapshotParameters("",
                null, SnapType.other, channelIds);

        ssp.merge(parameters);

        ssp.setInstitution(inst);
        ssp.setOwner(owner);

        ssp.setHuman(isHuman);

        // Get Unique channel names
        List<SimpleChannel> chInfo = findUniqueChannels(parmsList);

        // Populate channelIds in SnapshotParameters
        for (SimpleChannel c : chInfo) {
            channelIds.add(c.getChannelName());
        }

        ssp.setTargetSnapshot(snapshotName);

        // Create snapshot as necessary
        IProcessingAction snap = new CreateSnapshotAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session,
                ssp);
        getNextActions().add(snap);

        // Map metadata to it as necessary
        IProcessingAction map = new MapMetadataAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session, ssp);
        snap.getNextActions().add(map);

        // Add Transcode action
        TranscodeFromBNI1Action transcode = new TranscodeFromBNI1Action(
        		getMessageQueue(), 
        		this.getWorkItemSet(),
                session, ssp, chInfo, parmsList);

        map.getNextActions().add(transcode);

    }

    private List<SimpleChannel> findUniqueChannels(List<BNI1Parameters> params) {
        final String m = "findUniqueChannels(...)";
        List<SimpleChannel> chInfo = new ArrayList<SimpleChannel>();
        Set<String> allLabels = new HashSet<String>();

        for (BNI1Parameters curParam : params) {
            logger.debug("{}: Check for unique channels in Header: {}", m,
                    curParam);

//            EDFHeader curVal = curParam.getHeader();
            for (int i = 0; i < curParam.getChannelLabels().size(); i++) {
                double curSf = curParam.getSamplingRate();
                    
                // Get Scale based on Units.
                double baseScale = 1;


                double uVConv = 1;
                double offset = 0;

                String curLabel = curParam.getChannelLabels().get(i).trim();
                SimpleChannel newInfo = new SimpleChannel(
                    curLabel, curSf, uVConv, offset);

                // Add if new channel label; ignore Annotations Channel
                if (!chInfo.contains(newInfo)) {


                        // Check for known label and replace if already present.
                        if (allLabels.contains(newInfo.getChannelName())) {
                          curParam.createAltChannelLabel(newInfo.getChannelName());
                          
                          newInfo = new SimpleChannel(curParam.getChannelLabels().get(i), curSf,
                              newInfo.getVoltageConversionFactor(),
                              newInfo.getVoltageOffset()); 
                        }


                        allLabels.add(newInfo.getChannelName());
                        chInfo.add(newInfo);
                        logger.debug("{}: Adding Channel: {}", m,
                                newInfo.getChannelName());
                    
                }
            }
        }
        return chInfo;

    }

    @Override
    public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
            Parameters arguments) {
        return new ImportBNI1FilesAction(mQueue, origin, session,
                (ControlFileParameters) arguments);
    }

    @Override
    public Parameters getDefaultArguments() {
        return new ControlFileParameters();
    }

    @Override
    public Parameters getCurrentParameters() {
        return parameters;
    }


}
