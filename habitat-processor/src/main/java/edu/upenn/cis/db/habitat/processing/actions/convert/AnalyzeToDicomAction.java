package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.util.List;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.FileSystemObjectServer;
import edu.upenn.cis.db.habitat.io.FileSystemObjectServer.OutputFileStream;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CopyAction;
import edu.upenn.cis.db.habitat.processing.actions.DeleteAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.ShellAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters.CommandLine;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.GeneralMetadata.BASIC_CATEGORIES;

public class AnalyzeToDicomAction extends BaseProcessingAction {

	/*
	 * Analyze fileformat is combination of two files {.hdr, .img}
	 * and can be converted to DICOM by xmedcon. 
	 * 
	 * XMedCon will convert Analyze format into a single DICOM file that may contain
	 * multiple slices.
	 * 
	 */
	
	public static final String NAME = "AnalyzeToDicomAction";
	RemoteHabitatSession session;
	ControlFileParameters parameters;
	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public AnalyzeToDicomAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
            ControlFileParameters parameters) {
        super(mQueue, origin);
        this.session = session;
        this.parameters = parameters;
    }
	
	public AnalyzeToDicomAction() {
        super(null, null);
    }

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		/*
		 *	Analyze Image data contains two files per image. A HDR file and an IMG file. 
		 *	We copy both to the stagingServer where XMEDCON will convert the data to DICOM.
		 * 		
		 */
		
		logger.debug("Executing AnalyzeToDicomAction");
		
		String tempFileFolder = "dirConvertImagesTemp";
		
		for (IWorkItem item: getWorkItemSet()){
			logger.debug("AnalyzeToDicomAction: " + item.toString());
			
			IStoredObjectReference inFile = item.getMainHandle();
			
			File ff = new File(inFile.getFilePath());

			IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
					
			IInputStream h = bestServer.openForInput(inFile);
			
			IObjectServer localServer = StorageFactory.getStagingServer();

			// Create local copy of HDR file
			final FileReference hdrFileHandle = new FileReference(
					(DirectoryBucketContainer) localServer.getDefaultContainer(), 
					null, 
					tempFileFolder + "/" + ff.getName());
			
			IOutputStream hh = localServer.openForOutput(hdrFileHandle);
			
			h.copyTo(hh.getOutputStream());
			h.close();
			hh.close();
			
			// Create local copy of file
			String imgFileStr = ff.getName().replace(".hdr", ".img");
			
			File file2 = new File(ff.getParent(), imgFileStr);
			IStoredObjectReference inFile2 = new FileReference((DirectoryBucketContainer) inFile.getDirBucketContainer(), file2.getPath(), file2.getPath());
			IInputStream h2 = bestServer.openForInput(inFile2);
			
			final FileReference imgFileHandle = new FileReference(
					(DirectoryBucketContainer) localServer.getDefaultContainer(), 
					null, 
					tempFileFolder + "/" + imgFileStr);
			IOutputStream hh2 = localServer.openForOutput(imgFileHandle);

			h2.copyTo(hh2.getOutputStream());
			hh2.close();
			h2.close();
			
			
			List<String> cmdParams = new ArrayList<String>();
			String tempFileContainer = localServer.getDefaultContainer().getFileDirectory() + "/";
			String tempFilePath = tempFileContainer +  hdrFileHandle.getFilePath();
			String tempOutputPath = tempFileContainer +  hdrFileHandle.getFilePath().replace(".hdr", ".dcm");
			
			cmdParams.add("-f " +  tempFilePath);
			cmdParams.add("-c dicom");
			cmdParams.add("-o " + tempOutputPath);

			CommandLine t = new ShellParameters.CommandLine("/usr/local/xmedcon/bin/medcon", cmdParams);
			
			ShellParameters shellParams = new ShellParameters();
			shellParams.getCommandLines().add(t);
			

			ShellAction convertAction = new ShellAction(
					getMessageQueue(), this.getWorkItemSet(), this.session, shellParams);
			
			ParametersForSnapshotContent parms = new ParametersForSnapshotContent();
			parms.merge(this.parameters);
			
			WorkItemSet newSet = new WorkItemSet(getWorkItemSet());
			
			FileReference DCMhandle = new FileReference((DirectoryBucketContainer) localServer.getDefaultContainer(), hdrFileHandle.getFilePath().replace(".hdr", ".dcm"), hdrFileHandle.getFilePath().replace(".hdr", ".dcm")); 
			
			WorkItem newItem = new WorkItem(tempOutputPath, 
					item.getParentId(), 
					DCMhandle,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					BASIC_CATEGORIES.ImageStack.name(), 
					"DICOM Image", 
					null);
			
			newSet.add(newItem);
			
			CopyAction copyAction = new CopyAction(getMessageQueue(), newSet, this.session, parms );			
			
			
			WorkItemSet tempSet = new WorkItemSet(getWorkItemSet());//.getDescription(), getOriginatingItemSet().getJson());
			
			WorkItem tempItem1 = new WorkItem("Img File", 
					item.getParentId(), 
					imgFileHandle,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					"application/dcm", 
					"DICOM Image", 
					null);
			
			
			WorkItem tempItem2 = new WorkItem("Hdr File", 
					item.getParentId(), 
					hdrFileHandle,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					"application/dcm", 
					"DICOM Image", 
					null);
			
			WorkItem tempItem3 = new WorkItem("Dcm File", 
					item.getParentId(), 
					DCMhandle,
					null,
					item.getCreator(), 
					item.getTimestamp(), 
					"application/dcm", 
					"DICOM Image", 
					null);
			
			tempSet.add(tempItem1);
			tempSet.add(tempItem2);
			tempSet.add(tempItem3);
			
			
			DeleteAction deleteAction = new DeleteAction(getMessageQueue(), tempSet);
			
			getNextActions().add(convertAction);
			convertAction.getNextActions().add(copyAction);
			copyAction.getNextActions().add(deleteAction);
			
		}
		
		
		
		
		
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new AnalyzeToDicomAction(mQueue, origin, session, (ControlFileParameters)arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

	

}
