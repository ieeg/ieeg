/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.Set;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * TODO: add further metadata fields!
 * 
 * Take a metadata file and map it into the already-created data
 * snapshot.  Handles things like hospital admissions metadata, etc.
 * @author zives
 *
 */
public class MapMetadataAction extends BaseSnapshotProcessingAction {
	
	public final static String NAME = "MapMetadataAction";
	
	MapMetadataAction() {
		super();
	}
	
	public MapMetadataAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, ParametersForSnapshotContent parms) {
		super(mQueue, origin, session, parms);
	}

	/**
	 * Read hospital admission and other data from the SnapshotParameters, and
	 * create relevant records.
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		IUser creator = getCreator();
		if (!(creator instanceof User)) {
			throw new IllegalStateException(
					"The creator must have type "
							+ User.class.getCanonicalName()
							+ ". It has type "
							+ creator.getClass().getCanonicalName());
		}
		SnapshotParameters fullParameters = session.getSnapshotParametersFor((User)creator, 
				getCurrentParameters().getTargetSnapshot());
		
		if (fullParameters.isHuman()) {
			// EegStudy
			
			session.addHospitalAdmissionData((User)creator,
					fullParameters.getTargetSnapshotId(),
					fullParameters);
			
		} else {
			// ExperimentEntity
		    session.addAnimal(
                (User)creator, 
                fullParameters.getTargetSnapshotId(),
                fullParameters);

		    
		  }
			
			
		
		
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new MapMetadataAction(mQueue, origin, session, (ParametersForSnapshotContent)arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}

}
