/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.export;

import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseTranscodeAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.TranscodeParameters;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * TODO: export a snapshot as EDF
 * @author zives
 *
 */
public class TranscodeToEdfAction extends BaseTranscodeAction {
	public static final String NAME = "TranscodeToEdfAction";

	public TranscodeToEdfAction() {
		super();
	}
	
	public TranscodeToEdfAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, TranscodeParameters parms) {
		super(mQueue, origin, session, parms);
		// TODO Auto-generated constructor stub
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		return STATE.FAILED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new TranscodeToEdfAction(mQueue, origin, session, (TranscodeParameters)arguments);
	}

}
