package edu.upenn.cis.db.habitat.processing;

import java.util.ArrayList;
import java.util.List;

public class AnnotationNotifierFactory {
	public static List<IAnnotationNotifier> notifiers = new ArrayList<IAnnotationNotifier>();
	
	public static void registerNotifier(IAnnotationNotifier notifier) {
		notifiers.add(notifier);
	}
	
	public static synchronized List<IAnnotationNotifier> getNotifiers() {
		return notifiers;
	}
}
