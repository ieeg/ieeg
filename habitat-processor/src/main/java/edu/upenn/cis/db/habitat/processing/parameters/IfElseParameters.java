/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.Set;

import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.conditionals.AndTest;
import edu.upenn.cis.db.habitat.processing.conditionals.ITest;

/**
 * Parameters for if/else
 * 
 * @author zives
 *
 */
public class IfElseParameters extends Parameters {
	ITest test;
	Set<IProcessingAction> ifSatisfied;
	Set<IProcessingAction> ifNotSatisfied;
	public ITest getTest() {
		return test;
	}
	public void setTest(ITest test) {
		this.test = test;
	}
	public Set<IProcessingAction> getIfSatisfied() {
		return ifSatisfied;
	}
	public void setIfSatisfied(Set<IProcessingAction> ifSatisfied) {
		this.ifSatisfied = ifSatisfied;
	}
	public Set<IProcessingAction> getIfNotSatisfied() {
		return ifNotSatisfied;
	}
	public void setIfNotSatisfied(Set<IProcessingAction> ifNotSatisfied) {
		this.ifNotSatisfied = ifNotSatisfied;
	}
	
	public IfElseParameters() {
		
	}
	public IfElseParameters(ITest test, Set<IProcessingAction> ifSatisfied,
			Set<IProcessingAction> ifNotSatisfied) {
		super();
		this.test = test;
		this.ifSatisfied = ifSatisfied;
		this.ifNotSatisfied = ifNotSatisfied;
	}
	
	@Override
	public void merge(Parameters parms) {
		super.merge(parms);
		
		if (parms instanceof IfElseParameters) {
			IfElseParameters ifelse = (IfElseParameters)parms;
			
			ifSatisfied.addAll(ifelse.getIfSatisfied());
			
			ifSatisfied.addAll(ifelse.getIfNotSatisfied());
			
			test = new AndTest(test, ifelse.test);
		}
	}
}
