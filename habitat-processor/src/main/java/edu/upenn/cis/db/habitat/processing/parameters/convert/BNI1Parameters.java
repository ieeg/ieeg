package edu.upenn.cis.db.habitat.processing.parameters.convert;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ini4j.Wini;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.PersystChannelMap;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.eeg.edf.EDFHeader;

public class BNI1Parameters implements Comparable<BNI1Parameters> {
    
    private FileReference handle; // handle to the BNI file
    
    private String fileType;
    private Double samplingRate;
    private Double calibration;
    private Double offset;
    private Integer dataType;
    private String startDate;
    private String startTime;
    private Integer epochsPerSecond;
    private String dataFileName;
    private String dataFileContainer;
    private List<String> channelLabels = new ArrayList<>();

    public BNI1Parameters(FileReference inFile) throws IOException {
      this.handle = inFile;
      IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
      
      IInputStream inStream = bestServer.openForInput(inFile);
      
      BufferedInputStream inFStream = 
                  new BufferedInputStream(inStream.getInputStream());
      
      Wini ini = new Wini(inFStream);
      inFStream.close();
      inStream.close();
      
      String rateStr = ini.get("?", "Rate", String.class).trim();
      
      String[] splitStr = rateStr.split(" ");
      this.setSamplingRate(Double.parseDouble(splitStr[0]));
            
      setStartTime(ini.get("?", "Time", String.class).trim());
      
      String rawDate = ini.get("?", "Date", String.class).trim();
      String rawDate2 = rawDate.replaceAll("/", ".");
      setStartDate(rawDate2);
      
      String rawFile = ini.get("?", "Filename", String.class).trim();
      String rawFile2 = rawFile.replaceAll("\\\\", "/"); //create standard path
      
      File path = new File(rawFile2);
      setDataFileContainer(path.getParent());
      setDataFileName(path.getName());
      
      
      String channelNameString = ini.get("?", "MontageRaw", String.class).trim();
      String[] strArr = channelNameString.split(",");
      for(String s:strArr)
        channelLabels.add(s);
      
      setCalibration(Double.valueOf(ini.get("?", "UvPerBit", String.class).trim()));
      setOffset(Double.valueOf(ini.get("?", "DataOffset", String.class).trim()));
      setEpochsPerSecond(Integer.valueOf(ini.get("?", "EpochsPerSecond", String.class).trim()));
    }

    @Override
    public int compareTo(BNI1Parameters o) {
      long diff;
      try {
          diff = this.Date2uUTC() - o.Date2uUTC();
      } catch (ParseException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          diff = 0;
      }
      return safeLongToInt(diff);
    }
    
    public static int safeLongToInt(long l) {
      return (int) Math
              .max(Math.min(Integer.MAX_VALUE, l), Integer.MIN_VALUE);
  }
    
    /*
     * Return Start-date of BNI1 file in MicroUTC. (usec from jan 1, 1970) 
     */
    public long Date2uUTC() throws ParseException {   
      SimpleDateFormat df = new SimpleDateFormat("MM.dd.yyyy.HH:mm:ss Z");   
      
      Date startD = df.parse(startDate + "." + startTime + " -0000");
      long retTime = startD.getTime() * 1000;
      return retTime;
    }

    /*
     * Used to create alternative label for channel if during importing
     * multiple channels exist with different sampling freq/offset/conversion.
     * Returns true if successful 
     */
    public boolean createAltChannelLabel(String oldLabel){
      // Find label
      int idx = channelLabels.indexOf(oldLabel);
      if (idx<0)
        return false;
      
      String newLabel = channelLabels.get(idx);
      int iter = 0;
      while (channelLabels.contains(newLabel)){
        newLabel = newLabel.replace(
            "_" + String.valueOf(iter), "_"
                    + String.valueOf(iter + 1));
        channelLabels.remove(idx);
        channelLabels.add(idx, newLabel);
        iter++;
      }
      
      return true;
    }
    
    public String getStartDate() {
      return startDate;
    }

    public String getStartTime() {
      return startTime;
    }

    public FileReference getHandle() {
      return handle;
    }

    public String getFileType() {
      return fileType;
    }

    public Double getSamplingRate() {
      return samplingRate;
    }

    public Double getCalibration() {
      return calibration;
    }

    public Integer getDataType() {
      return dataType;
    }

    public List<String> getChannelLabels() {
      return channelLabels;
    }

    private void setSamplingRate(Double samplingRate) {
      this.samplingRate = samplingRate;
    }

    private void setStartDate(String startDate) {
      this.startDate = startDate;
    }

    private void setStartTime(String startTime) {
      this.startTime = startTime;
    }

    public Double getOffset() {
      return offset;
    }

    private void setOffset(Double offset) {
      this.offset = offset;
    }

    public Integer getEpochsPerSecond() {
      return epochsPerSecond;
    }

    private void setEpochsPerSecond(Integer epochsPerSecond) {
      this.epochsPerSecond = epochsPerSecond;
    }

    private void setCalibration(Double calibration) {
      this.calibration = calibration;
    }

    public String getDataFileName() {
      return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
      this.dataFileName = dataFileName;
    }

    public String getDataFileContainer() {
      return dataFileContainer;
    }

    public void setDataFileContainer(String dataFileContainer) {
      this.dataFileContainer = dataFileContainer;
    }
    
    
}