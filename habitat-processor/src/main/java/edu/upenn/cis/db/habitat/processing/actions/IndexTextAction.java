/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.server.search.SearchServer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Text-Index the content of a resource using Apache Lucene
 *  
 * @author zives
 *
 */
public class IndexTextAction extends BaseSnapshotProcessingAction {
	
	public static final String NAME = "IndexTextAction";
	
	/**
	 * The text-search indexer
	 */
	SearchServer indexer;
	
	public IndexTextAction() {
		super(null, null, null, null);
		indexer = SearchServer.getServer();
	}
	
	public IndexTextAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, ParametersForSnapshotContent parms) {
		super(mQueue, origin, session, parms);
		indexer = SearchServer.getServer();
	}

	/**
	 * For each file in getInput(), try to index it using the SearchServer
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		
		for (JsonTyped fileSpec: parameters.getInputs()) {
			FileReference file = (FileReference)fileSpec;
			try {
				System.out.println("Indexing PDF " + file);
				
				IInputStream inStream = StorageFactory.getBestServerFor(file).openForInput(file);
				
				BufferedInputStream inFStream = 
						new BufferedInputStream(inStream.getInputStream());
				
				indexer.indexFileContents(inFStream, file.getFilePath());
				//inFStream is closed by this point, but we need to close our IInputStream
				inStream.close();
				
				Set<IProcessingAction> doNext = new HashSet<IProcessingAction>();
				ParametersForSnapshotContent p = new ParametersForSnapshotContent();
				Set<IStoredObjectReference> inputs = new HashSet<IStoredObjectReference>();
				inputs.add(file);
				p.addInputs(inputs);
				p.setTargetSnapshot(parameters.getTargetSnapshot());
				p.setTargetSnapshotId(parameters.getTargetSnapshotId());
				derived.add(new DerivedItem("pdf", file.getFilePath()));
				
				CopyAction copy = new CopyAction(getMessageQueue(), getWorkItemSet(), session, p);
				
				doNext.add(copy);
				
				this.setNextActions(doNext);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return STATE.FAILED;
			}
		}
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new IndexTextAction(mQueue, origin, session, (ParametersForSnapshotContent)arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}
}
