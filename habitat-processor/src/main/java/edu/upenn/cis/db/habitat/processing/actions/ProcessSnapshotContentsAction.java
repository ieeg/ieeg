/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.MimeSet;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.TaskTrackerFactory;
import edu.upenn.cis.db.habitat.processing.conditionals.SuccessfullyProcessedTest;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Given a set of input files corresponding to things that belong in a snapshot,
 * individually process each of them.  Wait until they are all complete before
 * counting ourselves as done.
 * 
 * @author zives
 *
 */
public class ProcessSnapshotContentsAction extends BaseSnapshotProcessingAction {
		public static final String NAME = "ProcessSnapshotContentsAction";
		
//		WorkItemSet workItems = new WorkItemSet("Work item set", null);
		
		Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
		final Logger logger = LoggerFactory.getLogger(getClass());
		
		public ProcessSnapshotContentsAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, 
				ControlFileParameters parameters) {
			super(mQueue, origin, session, parameters);
		}
		
		public ProcessSnapshotContentsAction() {
			super();
		}


		/**
		 * Iterate through all files in getInputs(), which are assumed to
		 * correspond to a single target snapshot.  Add each as a work
		 * item that needs to be processed.  Add a "barrier" that will complete
		 * once all of the work items are processed.
		 */
		@Override
		public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
			final String m = "execute(...)";
			Map<String,WorkItemSet> itemMap = new HashMap<String,WorkItemSet>();

			// Find single mimeType file pairs/sets			
			List<MimeSet> allInputs = new ArrayList<MimeSet>();
			List<JsonTyped> ignoreList = new ArrayList<JsonTyped>();
			
			for (JsonTyped fileSpec: parameters.getInputs()) {
				
				// Skip if on ignore list
				if (ignoreList.contains(fileSpec))
					continue;

				logger.debug("ProcessFile: " + fileSpec.toString());
				MimeSet mSet = RegisterActions.getMimePairType(fileSpec, parameters.getInputs());					
				logger.debug(mSet.getMimeType());

				allInputs.add(mSet);
				if (mSet.getOtherFiles() !=null)
					ignoreList.addAll(mSet.getOtherFiles());
				
				//remove from map if on ignore list
				for (MimeSet item: allInputs){
					if (ignoreList.contains(item.getEntryFile()))
						allInputs.remove(item);
				}

			}

			// Take each input file
			for (MimeSet fileSpec: allInputs) {
			
				FileReference fil = (FileReference)fileSpec.getEntryFile();
				
				String typ = fileSpec.getMimeType();
				if (typ.equals(""))
					typ = "application/unknown";
//					typ = RegisterActions.getMimeType(fil.getFilePath());		
				
				System.out.println("Process File " + fil.getFilePath());

                Set<IStoredObjectReference> otherFiles = new HashSet<>();
                for (JsonTyped j: fileSpec.getOtherFiles()){
                    otherFiles.add((IStoredObjectReference) j);
                }


				// Convert it into a work item for future processing
				WorkItem work = new WorkItem(
						fil.getFilePath(), 
						parameters.getTargetSnapshot(), 
						fil,
						otherFiles,
						getWorkItemSet().iterator().next().getCreator(), 
						(new Date()).getTime(), 
						typ,
						"ProcSnapAction",
						null);
				
				logger.debug("{} Added work item {} / {}",
						m,
						work.getDescription(),
						work.getMediaType());

				if (!itemMap.containsKey(typ)) {
					final SnapshotParameters snapshotParams= new SnapshotParameters();
					snapshotParams.setTargetSnapshot(parameters.getTargetSnapshot());
					snapshotParams.setTargetSnapshotId(parameters.getTargetSnapshotId());
					String institution = null;
					String owner = null;
					boolean isHuman = false;
					List<String> channelLabels = new ArrayList<>();
					Map<String, String> channelMapping = new HashMap();
					if (parameters instanceof SnapshotParameters) {
						institution = ((SnapshotParameters)parameters).getInstitution();
						owner= ((SnapshotParameters)parameters).getOwner();
						isHuman = ((SnapshotParameters)parameters).isHuman();
						channelLabels.addAll(((SnapshotParameters)parameters).getChannelLabels());
						channelMapping.putAll(((SnapshotParameters)parameters).getChannelMapping());
					} else if (parameters instanceof ControlFileParameters) {
						final SnapshotParameters cfsp = ((ControlFileParameters)parameters).getSnapshotParameters();
						institution = cfsp.getInstitution();
						owner = cfsp.getOwner();
						isHuman = cfsp.isHuman();
						channelLabels.addAll(cfsp.getChannelLabels());
						channelMapping.putAll(cfsp.getChannelMapping());
					}
					snapshotParams.setInstitution(institution);
					snapshotParams.setOwner(owner);
					snapshotParams.setHuman(isHuman);
					snapshotParams.setChannelLabels(channelLabels);
					snapshotParams.setChannelMapping(channelMapping);
					itemMap.put(typ, new WorkItemSet("Work items /" + typ, snapshotParams,getWorkItemSet().getRequest(), getWorkItemSet().getOrigin()));
				}
				final WorkItemSet workItemSet = itemMap.get(typ);
				workItemSet.add(work);
				// Link back to the originating it
				workItemSet.setStart(getWorkItemSet().getStart());
			}
			Set<WorkItemSet> allItems = new HashSet<WorkItemSet>();//("Items for " + inFile, null);
			for (WorkItemSet set : itemMap.values()) {
				getNextWorkItems().add(set);
				allItems.add(set);
			}

			Set<IProcessingAction> whenDone = new HashSet<IProcessingAction>();
			final ParametersForSnapshotContent mapMetadataParams = new ParametersForSnapshotContent();
			mapMetadataParams.merge(parameters);
			IProcessingAction mapMeta = new MapMetadataAction(getMessageQueue(), this.getWorkItemSet(), session, mapMetadataParams);
			whenDone.add(mapMeta);
			Set<IProcessingAction> whenNotDone = new HashSet<IProcessingAction>();

			IfElseAction barrier = new IfElseAction(getMessageQueue(), getWorkItemSet(),
					new SuccessfullyProcessedTest(allItems, TaskTrackerFactory.getTracker()),
					whenDone,
					whenNotDone);

			// Repeat self if not done
			
			// Push the things that happen after we're done to after the MapMetadata is done
			for (IProcessingAction action: getNextActions())
				mapMeta.getNextActions().add(action);
			
			getNextActions().clear();
			getNextActions().add(barrier);
			
			return STATE.SUCCEEDED;
		}
		
		@Override
		public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
			return new ReadControlFileAction(mQueue, origin, session, (ControlFileParameters)arguments);
		}

		@Override
		public ControlFileParameters getDefaultArguments() {
			return new ControlFileParameters();
		}

		@Override
		public Set<IProcessingAction> getNextActions() {
			return nextActions;
		}

		@Override
		public ControlFileParameters getCurrentParameters() {
			return (ControlFileParameters)super.getCurrentParameters();
		}
}
