/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.medtronic;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.medtronic.MedtronicChannel;
import edu.upenn.cis.db.habitat.processing.parameters.medtronic.MedtronicParameters;
import edu.upenn.cis.db.habitat.processing.parameters.medtronic.SenseChannelConfig;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodeMdtCsvFileAction extends BaseSnapshotProcessingAction {
		public static final String NAME = "TranscodeMdtAction";
		
		List<MedtronicParameters> parmsList;
		
		static int FREQ = 422;
		
		List<MefWriter> tempFiles;
		long[] timestamps;
		
		private final Logger logger = LoggerFactory.getLogger(getClass());
		
		public TranscodeMdtCsvFileAction() {}

		public TranscodeMdtCsvFileAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, SnapshotParameters ssp,
				List<MedtronicParameters> parmsList) {
			super(mQueue, origin, session, ssp);
			this.parmsList = parmsList;
		}
		
		private long process(SenseChannelConfig channels, String line, int lineCount) {//int channelInx, long time, String line) {
			String[] components = line.split(",");
			double[] values = new double[components.length];
			
			int[] samps = new int[1];
			long[] stamps = new long[1];
			for (int i = 0; i < components.length; i++) {
				values[i] = Double.valueOf(components[i]);
			}
			

			int i = 0;
			for (MedtronicChannel channel: channels.getChannels()) {
				int mult = 1;
				double convF = 1;
				if (channel.getChannelType().equals("PW") ||
						channel.getChannelType().equals("Power")) {
				} else if (channel.getChannelType().equals("TD")) {
					mult = 1000;
					convF = 10;
				}
					
				stamps[0] = timestamps[i];
				samps[0] = (int) (values[i] * mult * convF);
				tempFiles.get(i).writeData(samps, stamps);
				timestamps[i] += Math.round(1.E6 / FREQ);
				i++;
			}
			return 0;
		}
		
		/**
		 * Converts month-day-year hours-minutes-seconds am/pm
		 * into microseconds since 1970
		 * 
		 * @param date
		 * @return
		 */
		private long getUutc(String date) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
			DateTime dateTime = formatter.parseDateTime(date);
			
			return dateTime.getMillis() * 1000;
		}
		
	   /**
	    * Converts the date & time formed from replacing the year, month, and day
	    * from {@code date} with {@code newYear}, {@code newMonthOfYear}, and
	    * {@code newDayOfMonth} respectively into microseconds since 1970
	    * 
	    * @param date
	    * @return
	    */
	   private long getUutcWithNewDate(String date, int newYear, int newMonthOfYear, int newDayOfMonth) {
		   DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
		   DateTime dateTime = formatter.parseDateTime(date)
			    	                    .withDate(newYear, newMonthOfYear, newDayOfMonth);
		   return dateTime.getMillis() * 1000;
	   }

		/**
		 * Splits "xx Hz" and returns the xx
		 * 
		 * @param freqLabel
		 * @return
		 */
		private double getFrequency(String freqLabel) {
			String[] items = freqLabel.split(" ");
			
			return Double.valueOf(items[0]);
		}
		
		private void createChannel(
				MedtronicChannel channel,
				int inx, 
				FileReference handle,
				List<IOutputStream> tempOutputs,
				List<IStoredObjectReference> tempHandles) throws IOException {
			String initalPrefixComponent = "Animal_Data/";
			if (getCurrentParameters() instanceof SnapshotParameters) {
				final boolean isHuman = ((SnapshotParameters)getCurrentParameters()).isHuman();
				if (isHuman) {
					initalPrefixComponent = "Human_Data/";
				}
			}
			String name = initalPrefixComponent + handle.getObjectKey() + "/" + channel.getChannelType() + inx + ".mef";
			FileReference newHandle = new FileReference(null,
					name, name);
			System.out.println("Trying to create channel " + newHandle);
			IOutputStream output = StorageFactory.getPersistentServer().openForOutput(newHandle);
			FileOutputStream tempStream = (FileOutputStream)output.getOutputStream();
			tempOutputs.add(output);
			tempHandles.add(newHandle);
			
			MefHeader2 meh = new MefHeader2();
			
			meh.setChannelName(channel.getChannelType() + inx);
			meh.setSamplingFrequency(FREQ);
			meh.setHeaderLength((short)1024);
			meh.setInstitution("Medtronic");

			if (channel.getChannelType().equals("TD"))
				meh.setVoltageConversionFactor(10);
			else
				meh.setVoltageConversionFactor(1);
			
			tempFiles.add(
				new MefWriter(
						tempStream, 
//									getCurrentParameters().getTargetSnapshot() + "-" + channel.getChannelType(),
						channel.getChannelType() + inx,
						meh,
						10,
						FREQ,
						100000));
//									100 /* secsPerBlock */,
//									getFrequency(channel.getCtrFreq()), 
//									(long)getFrequency(channel.getCtrFreq()) * 100));
		}

		@Override
		public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
			final String m = "execute(...)";
			int fil = 0;
			if (parmsList.isEmpty())
				return STATE.SUCCEEDED;
				
			// List of files we'll need...
			
			// MEF Writers to each channel
			tempFiles = new ArrayList<MefWriter>(parmsList.get(0).getSenseChannelConfig().getChannels().size());
			
			// The actual output file handles
			List<IStoredObjectReference> tempHandles = new ArrayList<IStoredObjectReference>(parmsList.get(0).getSenseChannelConfig().getChannels().size());
			// The actual output file streams
			List<IOutputStream> tempOutputs = new ArrayList<IOutputStream>(parmsList.get(0).getSenseChannelConfig().getChannels().size());

			// A timestamp cursor for each channel file
			timestamps = new long[parmsList.get(0).getSenseChannelConfig().getChannels().size()];
			long offsetMicros = 0;
			for (JsonTyped file: getCurrentParameters().getInputs()) {
				FileReference handle = (FileReference)file;
				MedtronicParameters parms = parmsList.get(fil++);
				logger.debug("{}: transcoding file {}", m, handle);
				
				IObjectServer server = StorageFactory.getBestServerFor(handle);
				try {
					IInputStream stream = server.openForInput(handle);
					
					int i = 0;
					for (MedtronicChannel channel: parms.getSenseChannelConfig().getChannels()) {

						// Only create files for each channel for the first input!
						if (fil == 1) {
							createChannel(channel, i, handle, tempOutputs, tempHandles);
							final long realUutc = getUutc(parms.getINSTimeStamp());
							final long movedUutc = getUutcWithNewDate(parms.getINSTimeStamp(), 2000, 1, 1);
							offsetMicros = movedUutc - realUutc;
						}
						// Set the timestamp to the start of the channel file
						timestamps[i++] = getUutc(parms.getINSTimeStamp()) + offsetMicros;
					}
					
					// Now read the file line-by-line
					BufferedReader reader = new BufferedReader(new InputStreamReader(stream.getInputStream()));
					
					String line = reader.readLine();
					int lineCount = 0;
					while (line != null) {
						line = line.trim();
						
						if (!line.isEmpty()) {
							process(parms.getSenseChannelConfig(), line, lineCount++);
						}
						line = reader.readLine();
					}
					reader.close();
					stream.close();
					logger.debug("{}: read {} non-empty lines from {}", m, lineCount, handle);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return STATE.FAILED;
				}
			}
		for (MefWriter w : tempFiles) {
			w.close();
		}
		for (IOutputStream s : tempOutputs) {
			try {
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		ParametersForSnapshotContent newParms = new ParametersForSnapshotContent();

		for (IStoredObjectReference s : tempHandles) {
			newParms.getInputs().add(s);
		}
		newParms.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		newParms.setTargetSnapshotId(getCurrentParameters()
				.getTargetSnapshotId());

		getNextActions().add(
				new AddToSnapshotAction(getMessageQueue(), 
						getWorkItemSet(),
						getSession(),
						newParms));

		return STATE.SUCCEEDED;
	}

		@Override
		public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
			return new TranscodeMdtCsvFileAction(mQueue, origin, session, (SnapshotParameters)arguments, null);
		}

		@Override
		public ParametersForSnapshotContent getDefaultArguments() {
			// TODO Auto-generated method stub
			return null;
		}
	}
