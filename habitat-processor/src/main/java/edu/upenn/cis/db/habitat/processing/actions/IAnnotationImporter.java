/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import java.util.List;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * 
 * @author John Frommeyer
 *
 */
public interface IAnnotationImporter {

	public static class AnnotationImportException extends Exception {
		private static final long serialVersionUID = 1L;

		public AnnotationImportException(String message, Throwable cause) {
			super(message, cause);
		}

		public AnnotationImportException(String message) {
			super(message);
		}

		public AnnotationImportException(Throwable cause) {
			super(cause);
		}

	}

	List<TsAnnotationDto> importAnnotations(
			User creator,
			DataSnapshot dataset,
			FileReference annotationFileRef) throws AnnotationImportException;
}
