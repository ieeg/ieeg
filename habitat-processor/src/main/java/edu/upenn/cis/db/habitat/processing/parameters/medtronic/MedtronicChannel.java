/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import javax.xml.bind.annotation.XmlElement;

public class MedtronicChannel {
	String channelType;
	String bw;
	String ctrFreq;
	Integer powGain;
	Integer tdGain;
	String plusInput;
	String minusInput;
	
	PowerGains powerGains;

	@XmlElement(name="ChannelType")
	public String getChannelType() {
		return channelType;
	}

	@XmlElement(name="BW")
	public String getBw() {
		return bw;
	}

	@XmlElement(name="CtrFreq")
	public String getCtrFreq() {
		return ctrFreq;
	}

	@XmlElement(name="PowGain")
	public Integer getPowGain() {
		return powGain;
	}

	@XmlElement(name="TDGain")
	public Integer getTdGain() {
		return tdGain;
	}

	@XmlElement(name="PlusInput")
	public String getPlusInput() {
		return plusInput;
	}

	@XmlElement(name="MinusInput")
	public String getMinusInput() {
		return minusInput;
	}

	@XmlElement(name="PowerGains")
	public PowerGains getPowerGains() {
		return powerGains;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public void setBw(String bw) {
		this.bw = bw;
	}

	public void setCtrFreq(String ctrFreq) {
		this.ctrFreq = ctrFreq;
	}

	public void setPowGain(Integer powGain) {
		this.powGain = powGain;
	}

	public void setTdGain(Integer tdGain) {
		this.tdGain = tdGain;
	}

	public void setPlusInput(String plusInput) {
		this.plusInput = plusInput;
	}

	public void setMinusInput(String minusInput) {
		this.minusInput = minusInput;
	}

	public void setPowerGains(PowerGains powerGains) {
		this.powerGains = powerGains;
	}

	/*
    <Channel4>
      <ChannelType>Power</ChannelType>
      <BW>±2.5 Hz</BW>
      <CtrFreq>25.0 Hz</CtrFreq>
      <PowGain>4000</PowGain>
      <TDGain>2000</TDGain>
      <PlusInput>E8</PlusInput>
      <MinusInput>E11</MinusInput>
    </Channel4>
	 */

}
