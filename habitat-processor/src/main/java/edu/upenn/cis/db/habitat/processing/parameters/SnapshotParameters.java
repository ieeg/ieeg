/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction.TimeSeriesSettings;

/**
 * Parameters for creating and populating a new data snapshot.
 * 
 * @author zives
 *
 */
public class SnapshotParameters extends ParametersForSnapshotContent {
	public static enum SnapType {
		brainMapping,
		emuRecording,
		cognitiveMonitoring,
		intraoperativeMonitoring,
		other,
		unknown
	};

	/**
	 * The kind of snapshot
	 */
	SnapType snapshotType;

	/**
	 * Human vs. animal (study vs experiment)
	 */
	boolean isHuman = true;

	/**
	 * The labels of the channels we've found
	 */
	List<String> channelLabels = newArrayList();

	private Map<String, String> channelMapping = newHashMap();

	/**
	 * Mapping from channel label to snapshot ID
	 */
	Map<String, String> channelIds = new HashMap<String, String>();

	/**
	 * Channel-specific settings
	 */
	List<TimeSeriesSettings> settings = new ArrayList<TimeSeriesSettings>();

	List<String> otherFiles = new ArrayList<String>();

	/**
	 * Creator institution
	 */
	private String institution;

	String owner;
	NotificationParameters notificationParms;
	private String defaultDestination;

	private PermissionsParameters permissionParameters = new PermissionsParameters();

	public List<TimeSeriesSettings> getSettings() {
		return settings;
	}

	public void setSettings(List<TimeSeriesSettings> settings) {
		this.settings = settings;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isHuman() {
		return this.isHuman;
	}

	public void setHuman(boolean isHuman) {
		this.isHuman = isHuman;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public SnapType getSnapshotType() {
		return snapshotType;
	}

	public void setSnapshotType(SnapType snapshotType) {
		this.snapshotType = snapshotType;
	}

	public List<String> getChannelLabels() {
		return channelLabels;
	}

	public void setChannelLabels(List<String> channelLabels) {
		this.channelLabels = channelLabels;
	}

	public void addChannelLabel(String ch) {
		if (!channelLabels.contains(ch))
			channelLabels.add(ch);
	}

	public Map<String, String> getChannelMapping() {
		return channelMapping;
	}

	public String getMappedChannel(String unmappedChannel) {
		checkArgument(channelLabels.isEmpty() || channelLabels.contains(unmappedChannel));
		String mappedChannel = this.channelMapping.get(unmappedChannel);
		if (mappedChannel == null) {
			return unmappedChannel;
		}
		return mappedChannel;
	}

	public void addChannelMapping(String unmapped, String mapped) {
		addChannelLabel(unmapped);
		channelMapping.put(unmapped, mapped);
	}

	public void setChannelMapping(Map<String, String> mapping) {
		this.channelMapping = mapping;
	}

	public String getDefaultDestination() {
		if (defaultDestination != null) {
			return defaultDestination;
		}

		if (owner == null || targetSnapshot == null) {
			return null;
		}
		final StringBuilder sb = new StringBuilder();
		if (isHuman) {
			sb.append("Human_Data/");
		} else {
			sb.append("Animal_Data/");
		}
		sb.append(owner + "/");
		sb.append(targetSnapshot + "/");

		return sb.toString();
	}

	public void setDefaultDestination(String defaultDestination) {
		if (defaultDestination != null && !defaultDestination.endsWith("/")) {
			this.defaultDestination = defaultDestination + "/";
		} else {
			this.defaultDestination = defaultDestination;
		}
	}

	public SnapshotParameters(String friendlyName, String snapshotId,
			SnapType snapshotType,
			List<String> channelLabels) {
		super();
		super.setTargetSnapshot(friendlyName);
		this.snapshotType = snapshotType;
		super.setTargetSnapshotId(snapshotId);
		// this.snapshotCreator = snapshotCreator;
		this.channelLabels = channelLabels;
	}

	public void setChannelId(String label, String revId) {
		channelIds.put(label, revId);
	}

	public String getChannelId(String label) {
		return channelIds.get(label);
	}

	public PermissionsParameters getPermissionParameters() {
		return permissionParameters;
	}

	public void setPermissionParameters(
			PermissionsParameters permissionParameters) {
		this.permissionParameters = permissionParameters;
	}

	public List<String> getOtherFiles() {
		return otherFiles;
	}

	public void addOtherFile(String file) {
		otherFiles.add(file);
	}

	public void setOtherFiles(List<String> otherFiles) {
		this.otherFiles = otherFiles;
	}

	public SnapshotParameters() {

	}

	public Map<String, String> getChannelIds() {
		return channelIds;
	}

	public void setChannelIds(Map<String, String> channelIds) {
		this.channelIds = channelIds;
	}

	public NotificationParameters getNotificationParms() {
		return notificationParms;
	}

	public void setNotificationParms(NotificationParameters notificationParms) {
		this.notificationParms = notificationParms;
	}

}
