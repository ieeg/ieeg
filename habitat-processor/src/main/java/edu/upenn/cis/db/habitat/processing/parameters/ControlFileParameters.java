/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;

public class ControlFileParameters extends ParametersForSnapshotContent {
	SnapshotParameters snapshotParameters = new SnapshotParameters();
	Set<IStoredObjectContainer> outputs = new HashSet<IStoredObjectContainer>();
	
	public ControlFileParameters() {
		
	}
	
	public ControlFileParameters(FileReference s) {
		getInputs().add(s);
	}
	
	public ControlFileParameters(Collection<FileReference> files) {
		getInputs().addAll(files);
	}

	public SnapshotParameters getSnapshotParameters() {
		return snapshotParameters;
	}

	public void setSnapshotParameters(SnapshotParameters snapshotParameters) {
		this.snapshotParameters = snapshotParameters;
	}

	public Set<IStoredObjectContainer> getOutputs() {
		return outputs;
	}

	public void setOutputs(Set<IStoredObjectContainer> outputs) {
		this.outputs = outputs;
	}
	
	
}
