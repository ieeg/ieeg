package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaChannelParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.kahana.KahanaParameters;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodeFromKahanaAction extends BaseSnapshotProcessingAction {
	public static final class SimpleChannelMefWriter {
		private final SimpleChannel simpleChannel;
		private final MefWriter mefWriter;
		private boolean startedWriting = false;

		public SimpleChannelMefWriter(
				SimpleChannel simpleChannel,
				MefWriter mefWriter) {
			this.simpleChannel = checkNotNull(simpleChannel);
			this.mefWriter = checkNotNull(mefWriter);
		}

		public boolean isStartedWriting() {
			return startedWriting;
		}

		public void setStartedWriting(boolean startedWriting) {
			this.startedWriting = startedWriting;
		}

		public SimpleChannel getSimpleChannel() {
			return simpleChannel;
		}

		public MefWriter getMefWriter() {
			return mefWriter;
		}
	}

	public static final String NAME = "TranscodeFromKahanaAction";
	private static final long BYTESPERSAMPLE = 2;
	private static final int SAMPLESPERREAD = 5000;
	final private Logger logger = LoggerFactory.getLogger(getClass());

	private KahanaParameters params;
	private List<SimpleChannel> chInfo; // List of Channel objects.
	private List<IStoredObjectReference> tempHandles;
	private List<IOutputStream> tempOutputs;

	public TranscodeFromKahanaAction() {
		super();
	}

	public TranscodeFromKahanaAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters arguments, List<SimpleChannel> chInfo,
			KahanaParameters params) {
		super(mQueue, origin, session, arguments);
		this.chInfo = chInfo; // list of chInfo --> one for each MEF file
		this.params = params; // all params for all files to be imported.
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";
		logger.debug("Executing TranscodeFromKahanaAction for "
				+ getCurrentParameters().getInputs());

		if (params == null) {
			logger.debug("{}: No Kahana Files to transcode.", m);
			return STATE.SUCCEEDED;
		}

		// Make map channelName <--> channel Index
		Map<String, Integer> chMap = new HashMap<>();
		for (KahanaChannelParameters ch : params.getChanParams()) {
			chMap.put(ch.getTagName(),
					Integer.valueOf((int) Math.round(ch.getChannelIdx())));
		}
		
		//And a map channelIndex --> file params
		Map<Integer, List<KahanaFileParameters>> idxToFileParams = new HashMap<>();
		for (KahanaFileParameters fileParam : params.getFileParams()) {
			List<KahanaFileParameters> paramsForIdx = idxToFileParams.get(fileParam.getChannel());
			if (paramsForIdx == null) {
				paramsForIdx = new ArrayList<>();
				idxToFileParams.put(fileParam.getChannel(), paramsForIdx);
			}
			paramsForIdx.add(fileParam);
		}

		// The actual output file handles
		tempHandles = new ArrayList<IStoredObjectReference>(chInfo.size());
		// The actual output file streams
		tempOutputs = new ArrayList<IOutputStream>(chInfo.size());

		// Iterate over each MEF File
		for (SimpleChannel curChan : chInfo) {
			Integer curChIndex = chMap.get(curChan.getChannelName());
			// Remove entries as we go so we can close any left-overs with 
			// no matching channel index later
			List<KahanaFileParameters> curFiles = idxToFileParams.remove(curChIndex);
			if (curFiles == null) {
				logger.info("{}: No data files found for channel {}", 
						m, 
						curChan.getChannelName());
				continue;
			}
			
			MefWriter curWriter = createMEFWriter(getCurrentParameters()
					.getTargetSnapshot(), curChan);

			

			// Sort channels
			Collections.sort(curFiles);
			final double periodMicros = 1e6/curChan.getSamplingFrequency();
			// Iterate over each file for current MEF File and write data
			for (KahanaFileParameters cf : curFiles) {
				try (BufferedInputStream curStream = cf.getiStream()) {

					long fileStartTS = cf.getStartDate();
					
					int bPerBlock = (int) (SAMPLESPERREAD * BYTESPERSAMPLE);
					byte[] b = new byte[bPerBlock];
					ShortBuffer shortBuf =
							ByteBuffer.wrap(b)
									.order(ByteOrder.LITTLE_ENDIAN)
									.asShortBuffer();

					logger.debug("{}: Writing for Kahana file: {}", m, cf
							.getHandle()
							.getFilePath());

					long fileLength = cf.getInStream().getLength();
					int totalNrSamples = (int) (fileLength / BYTESPERSAMPLE);

					int nrSamplesWritten = 0;

					int samplesInCurBlock = 0;
					while (nrSamplesWritten < totalNrSamples) {

						// Get fixed block size or partial block if near EOF
						try {
							if ((totalNrSamples - nrSamplesWritten) > SAMPLESPERREAD) {
								ByteStreams.readFully(curStream, b);
								nrSamplesWritten += SAMPLESPERREAD;
								samplesInCurBlock = SAMPLESPERREAD;
							} else
							{
								samplesInCurBlock = (totalNrSamples - nrSamplesWritten);
								bPerBlock = (int) (samplesInCurBlock * BYTESPERSAMPLE);
								b = new byte[bPerBlock];
								shortBuf =
										ByteBuffer.wrap(b)
												.order(ByteOrder.LITTLE_ENDIAN)
												.asShortBuffer();
								ByteStreams.readFully(curStream, b);
								nrSamplesWritten += samplesInCurBlock;
								logger.debug(m
										+ ": Getting last partial block of data from Kahana Raw Data file.");
							}
						} catch (EOFException e) {
							logger.error(
									m
											+ ": Unexpected EOF while reading Kahana Raw data file.",
									e);
						}

						shortBuf.rewind();

						// Create TimeStamp vector
						int[] samps = new int[samplesInCurBlock];
						long[] stamps = new long[samplesInCurBlock];
						final int nrPreviouslyWrittenSamples = nrSamplesWritten - samplesInCurBlock;
						stamps[0] = fileStartTS + (long) (nrPreviouslyWrittenSamples * periodMicros);
						logger.trace("{} FileStartTime: {}", m,
								stamps[0]);
						samps[0] = (int) (curChan.getVoltageOffset() + shortBuf
								.get());
						for (int j = 1; j < samplesInCurBlock; j++) {
							samps[j] = (int) (curChan
									.getVoltageOffset() + shortBuf.get());
							//
							stamps[j] = fileStartTS + (long) ((nrPreviouslyWrittenSamples + j) * periodMicros);
						}
						curWriter.writeData(samps, stamps);
					}
				} finally {
					cf.close();
				}
			}
			curWriter.close();
		}
		for (IOutputStream s : tempOutputs) {
			try {
				s.close();
			} catch (IOException e) {
				logger.info(
						m
								+ ": Ignoring exception when closing IOutputStream",
						e);
			}
		}
		//If there are any file params left in map then they were never processed or closed
		for (Map.Entry<Integer, List<KahanaFileParameters>> entry : idxToFileParams.entrySet()) {
			for (KahanaFileParameters fileParam : entry.getValue()) {
				logger.info("{}: Closing {} which had no match in TalStruct and was not transcoded.",
						m,
						fileParam.getHandle());
				fileParam.close();
			}
		}

		ParametersForSnapshotContent snapP = new ParametersForSnapshotContent();

		snapP.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		snapP.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());

		for (IStoredObjectReference s : tempHandles) {
			snapP.getInputs().add(s);
		}

		getNextActions().add(
				new AddToSnapshotAction(
						getMessageQueue(),
						this.getWorkItemSet(),
						getSession(),
						snapP));

		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new TranscodeFromKahanaAction(mQueue, origin, session,
				(SnapshotParameters) arguments, null, null);
	}

	private MefWriter createMEFWriter(
			String snapshotName,
			SimpleChannel chInfo) throws IOException {

		System.out.println("Creating MEF File...");

		String destination = null;
		if (getCurrentParameters() instanceof SnapshotParameters) {
			final SnapshotParameters snapshotParameters = (SnapshotParameters) getCurrentParameters();
			destination = snapshotParameters.getDefaultDestination();
		}
		if (destination == null) {
			destination = "Animal_Data/" + snapshotName + "/";
		}

		String name = destination
				+ chInfo.getChannelName() + ".mef";

		IObjectServer perServ = StorageFactory.getPersistentServer();
		IStoredObjectContainer cont = perServ.getDefaultContainer();

		FileReference newHandle = new FileReference(
				(DirectoryBucketContainer) cont, name,
				name);
		System.out.println("Trying to create channel " + newHandle);

		IOutputStream output = perServ.openForOutput(newHandle);

		FileOutputStream tempStream = (FileOutputStream) output
				.getOutputStream();
		tempOutputs.add(output);
		tempHandles.add(newHandle);

		MefHeader2 meh = new MefHeader2();

		meh.setChannelName(chInfo.getChannelName());
		meh.setSamplingFrequency(chInfo.getSamplingFrequency());
		meh.setHeaderLength((short)1024);

		// Set voltage conversion factor
		meh.setVoltageConversionFactor(chInfo.getVoltageConversionFactor());

		double secPerMEFBlock = Math
				.floor(6000 / chInfo.getSamplingFrequency());
		long discontinuity_threshold = 10000;

		MefWriter newMefWriter = new MefWriter(
				tempStream,
				chInfo.getChannelName(),
				meh,
				secPerMEFBlock,
				chInfo.getSamplingFrequency(),
				discontinuity_threshold);

		return newMefWriter;

	}

	public void setChannelInfo(List<SimpleChannel> info) {
		this.chInfo = info;

	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}

}