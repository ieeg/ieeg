/*
 * Copyright 2016 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Splitter;

import edu.upenn.cis.db.habitat.processing.actions.ReadSubmittedDirectoryAction.IDirectoryParser;

/**
 * @author John Frommeyer
 *
 */
public final class IeegDirectoryParser implements IDirectoryParser {

	public static final class IeegParsedDirectory implements IParsedDirectory {

		private final boolean isHuman;
		private final String organizationName;
		private final Optional<String> projectName;
		private final String datasetName;
		private final String directory;

		public IeegParsedDirectory(
				boolean isHuman,
				String organizationName,
				@Nullable String projectName,
				String datasetName,
				String directory) {
			this.isHuman = isHuman;
			this.organizationName = checkNotNull(organizationName);
			this.projectName = Optional.fromNullable(projectName);
			this.datasetName = checkNotNull(datasetName);
			this.directory = checkNotNull(directory);
		}

		@Override
		public Optional<Boolean> isHuman() {
			return Optional.of(isHuman);
		}

		@Override
		public Optional<String> getOrganizationName() {
			return Optional.of(organizationName);
		}

		@Override
		public Optional<String> getProjectName() {
			return projectName;
		}

		@Override
		public Optional<String> getDatasetName() {
			return Optional.of(datasetName);
		}

		@Override
		public String getDirectory() {
			return directory;
		}

	}

	private final static Splitter splitter = Splitter
			.on('/')
			.trimResults()
			.omitEmptyStrings();

	@Override
	public IParsedDirectory parseDirectory(String directory) {
		checkNotNull(directory);
		final List<String> split = splitter.splitToList(directory);
		final int nameCount = split.size();
		checkArgument(nameCount == 4 || nameCount == 5);
		//Skip the first component which should be the userid
		final boolean isHuman = "Human_Data".equals(split.get(1));
		final String organizationName = split.get(2);
		String projectName;
		String datasetName;
		if (nameCount == 4) {
			projectName = null;
			datasetName = split.get(3);
		} else {
			projectName = split.get(3);
			datasetName = split.get(4);
		}
		return new IeegParsedDirectory(
				isHuman,
				organizationName,
				projectName,
				datasetName,
				directory);

	}

}
