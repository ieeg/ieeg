package edu.upenn.cis.db.habitat.processing;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import edu.upenn.cis.braintrust.dao.IControlFileDAO;
import edu.upenn.cis.braintrust.model.ControlFileEntity;
import edu.upenn.cis.braintrust.persistence.PersistenceUtil;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public interface IWorkQueue {
	public boolean hasNext(
			int maxLoadingAttempts,
			int secondsBetweenAttempts)
			throws InterruptedException;
	
	public WorkItemSet peek();

	public WorkItemSet getNext();

	public WorkItemSet add(Set<? extends IWorkItem> item, String description,
			JsonTyped parameters);

	public WorkItemSet add(WorkItemSet item);

	public WorkItemSet add(IWorkItem item);

	String getMediaType(String fileKey);
	
	public void sendDone(WorkItemSet originalItem, String requester, String message);
	
}
