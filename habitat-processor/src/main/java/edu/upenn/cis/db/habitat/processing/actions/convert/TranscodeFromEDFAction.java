package edu.upenn.cis.db.habitat.processing.actions.convert;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Maps.newHashMap;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.ByteStreams;
import com.google.common.primitives.Bytes;

import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.ImportAnnotationsAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.EDF.EDFParameters;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.edf.EDFHeader;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodeFromEDFAction extends BaseSnapshotProcessingAction {

	public static final class TimestampedAnnotationList {
		private final double onsetSeconds;
		private final double durationSeconds;
		private final List<String> comments = new ArrayList<>();

		private static final Byte delimiter = Byte.valueOf((byte) 20);
		private static final Byte timestampDelimiter = Byte.valueOf((byte) 21);

		public TimestampedAnnotationList(List<Byte> bytes)
				throws UnsupportedEncodingException {
			final int tsIndex = bytes.indexOf(delimiter);
			final List<Byte> timestampBytes = bytes.subList(0, tsIndex);

			final int durationIndex = timestampBytes
					.indexOf(timestampDelimiter);
			if (durationIndex == -1) {
				final String onsetStr = new String(
						Bytes.toArray(timestampBytes), "UTF-8");
				onsetSeconds = Double.valueOf(onsetStr);
				durationSeconds = 0;
			} else {
				final List<Byte> onsetBytes = timestampBytes.subList(0,
						durationIndex);
				final List<Byte> durationBytes = timestampBytes.subList(
						durationIndex + 1, timestampBytes.size());
				onsetSeconds = Double.valueOf(new String(Bytes
						.toArray(onsetBytes), "UTF-8"));
				durationSeconds = Double.valueOf(new String(Bytes
						.toArray(durationBytes), "UTF-8"));
				checkArgument(durationSeconds >= 0);
			}

			final List<Byte> commentsBytes = bytes.subList(tsIndex + 1,
					bytes.size());
			final List<Byte> currentCommentBytes = new ArrayList<>();
			for (final Byte b : commentsBytes) {
				if (!b.equals(delimiter)) {
					currentCommentBytes.add(b);
				} else {
					addNonEmptyComment(currentCommentBytes);
					currentCommentBytes.clear();
				}
			}
			// Get the last one
			if (!currentCommentBytes.isEmpty()) {
				addNonEmptyComment(currentCommentBytes);
			}
		}

		// Think we can ignore empty comments
		private void addNonEmptyComment(List<Byte> commentBytes)
				throws UnsupportedEncodingException {
			final String comment = new String(
					Bytes.toArray(commentBytes), "UTF-8").trim();
			if (!comment.isEmpty()) {
				comments.add(comment);
			}
		}

		public double getOnsetSeconds() {
			return onsetSeconds;
		}

		public double getDurationSeconds() {
			return durationSeconds;
		}

		public List<String> getComments() {
			return comments;
		}

		@Override
		public String toString() {
			return "TimestampedAnnotationList [onsetSeconds=" + onsetSeconds
					+ ", durationSeconds=" + durationSeconds + ", comments="
					+ comments + "]";
		}

	}

	public static final class SimpleChannelMefWriter {
		private final SimpleChannel simpleChannel;
		private final MefWriter mefWriter;
		private boolean startedWriting = false;

		public SimpleChannelMefWriter(
				SimpleChannel simpleChannel,
				MefWriter mefWriter) {
			this.simpleChannel = checkNotNull(simpleChannel);
			this.mefWriter = checkNotNull(mefWriter);
		}

		public boolean isStartedWriting() {
			return startedWriting;
		}

		public void setStartedWriting(boolean startedWriting) {
			this.startedWriting = startedWriting;
		}

		public SimpleChannel getSimpleChannel() {
			return simpleChannel;
		}

		public MefWriter getMefWriter() {
			return mefWriter;
		}
	}

	public static final String NAME = "TranscodeFromEDFAction";
	final private Logger logger = LoggerFactory.getLogger(getClass());

	private List<EDFParameters> params;
	private Collection<SimpleChannel> chInfo;
	private List<IStoredObjectReference> tempHandles;
	private List<IOutputStream> tempOutputs;
	private Set<String> includedChannels = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

	public TranscodeFromEDFAction() {
		super();
	}

	public TranscodeFromEDFAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters arguments, Collection<SimpleChannel> chInfo,
			List<EDFParameters> params) {
		super(mQueue, origin, session, arguments);
		this.chInfo = chInfo;
		this.params = params;
		includedChannels.addAll(arguments.getChannelLabels());
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		logger.debug("Executing TranscodeFromEDF for "
				+ getCurrentParameters().getInputs());

		if (params.isEmpty()) {
			logger.debug("{}: No EDF to transcode.", m);
			return STATE.SUCCEEDED;
		}
		if (chInfo.isEmpty()) {
			// Most likely the mapping in the ieeg-dataset.ini file is completely wrong. So just get out now.
			logger.info("{}: No EDF channels to transcode.", m);
			return STATE.SUCCEEDED;
		}
		final List<TimestampedAnnotationList> tals = new ArrayList<>();
		// Sort EDFParameter lists based on startDate of File.
		Collections.sort(params);
		final long recordingStartUutc = params.get(0).getHeader()
				.EDFDate2uUTC();

		long previousFileEndUutc = -1;
		// Display sorted startTimestamps
		boolean overlaps = false;
		for (EDFParameters p : params) {
			long fileStartUutc = p.getHeader().EDFDate2uUTC();
			logger.debug(
					"{}: Reading startDate/time: {} - From: {}-{} from file {}",
					m,
					fileStartUutc,
					p.getHeader().startDate,
					p.getHeader().startTime,
					p.getHandle());
			
			if (fileStartUutc < previousFileEndUutc) {
				final long overlapMicros = previousFileEndUutc - fileStartUutc;
				logger.error("{}: File {} overlaps by {} microseconds with previous file", m,
						p.getHandle(),
						overlapMicros);
				getMessageQueue()
						.logError(
								getCreator(),
								WorkerRequest.PORTAL,
								WorkerRequest.ETL,
								getWorkItemSet(),
								"File "
										+ p.getHandle()
										+ " time series data overlaps with previous file!");
				overlaps = true;
				break;
			}
			previousFileEndUutc = p.getFileEndUutc();
		}
		// If there was an overlap, clean up EDF files and return.
		if (overlaps) {
			for (final EDFParameters p : params) {
				p.close();
			}
			return STATE.FAILED;
		}

		// MEF Writers to each channel
		final Map<String, SimpleChannelMefWriter> labelToChannelWriter = newHashMap();
		// The actual output file handles
		tempHandles = new ArrayList<IStoredObjectReference>(chInfo.size());
		// The actual output file streams
		tempOutputs = new ArrayList<IOutputStream>(chInfo.size());

		// Create MEF file for each channel;
		for (SimpleChannel ch : chInfo) {
			MefWriter newWriter = createMEFWriter(getCurrentParameters()
					.getTargetSnapshot(), ch);
			labelToChannelWriter.put(
					ch.getChannelName(),
					new SimpleChannelMefWriter(ch, newWriter));
		}
		try {
			// Iterate over sorted EDF-Files
			for (EDFParameters h : params) {
				logger.debug("{}: Writing for EDF file: {}", m, h.getHandle()
						.getFilePath());

				EDFHeader curHeader = h.getHeader();

				final long fileStartUutc = curHeader.EDFDate2uUTC();
				long[] samplesWritten = new long[curHeader
						.getNumberOfChannels()];

				int sPerBlock = 0;
				for (int i : curHeader.numberOfSamples)
					sPerBlock += i;

				int bPerBlock = sPerBlock * 2;
				int totNrBlocks = curHeader.getNumberOfRecords();

				// Spec says first EDF Annotation signal in file should be used
				// for time keeping.
				// Applies to both EDF+C and EDF+D although only strictly
				// necessary for +D.
				int timeKeepingAnnChannel = -1;
				if (isEdfPlus(curHeader)) {
					timeKeepingAnnChannel = Arrays.asList(
							curHeader.getChannelLabels())
							.indexOf("EDF Annotations");
					if (timeKeepingAnnChannel == -1) {
						throw new IllegalStateException(
								"File is EDF+ but contains no time keeping 'EDF Annotations' channel");
					}
					logger.debug(
							"{}: index of EDF Annotations channel to be used for time keeping: {}",
							m, timeKeepingAnnChannel);
				}

				try {
					byte[] b = new byte[bPerBlock];

					try (BufferedInputStream curStream = h.getiStream()) {
						for (int i = 0; i < totNrBlocks; i++) {
							ByteStreams.readFully(curStream, b);

							TimestampedAnnotationList timeKeepingTal = null;
							if (timeKeepingAnnChannel != -1) {
								int preTKAnnSamplesPerBlock = 0;
								for (int chIndex = 0; chIndex < timeKeepingAnnChannel; chIndex++) {
									preTKAnnSamplesPerBlock += curHeader
											.getNumberOfSamples()[chIndex];
								}
								int timeKeepingAnnChannelOffsetInBlock = preTKAnnSamplesPerBlock * 2;
								int annLengthBytes = curHeader
										.getNumberOfSamples()[timeKeepingAnnChannel] * 2;
								final List<TimestampedAnnotationList> firstTals = processAnnotationBytes(
										b,
										timeKeepingAnnChannelOffsetInBlock,
										annLengthBytes);
								timeKeepingTal = getFirst(firstTals, null);
								if (timeKeepingTal == null) {
									throw new IllegalStateException(
											"File contains 'EDF Annotations' channel but no time keeping TAL found in block "
													+ i);
								}
								tals.addAll(firstTals);
							}

							int blockBytePosition = 0;
							logger.trace("{}: reading number of bytes: {}", m,
									bPerBlock);



							// Iterate over Labels in file and write to correct
							// MEFWriter
							for (int iCh = 0; iCh < curHeader
									.getNumberOfChannels(); iCh++) {

								String curLab = curHeader.channelLabels[iCh];
								Integer curNumSamples = curHeader.numberOfSamples[iCh];
								if (ignoreChannel(curLab)
										|| iCh == timeKeepingAnnChannel) {
									// Skip bytes for ignored channel. Also skip
									// bytes for time
									// keeping ann channel since we handled that
									// above
									blockBytePosition += (curNumSamples * 2);
								} else if (isAnnotationChannel(curLab)) {
									// We already handled the time keeping anns
									// if present. Might be other ann channels
									// though.
									tals.addAll(processAnnotationBytes(b,
											blockBytePosition,
											curNumSamples * 2));
									blockBytePosition += (curNumSamples * 2);
								} else {
									final ShortBuffer shortBuf = ByteBuffer
											.wrap(b, blockBytePosition,
													curNumSamples * 2)
											.order(ByteOrder.LITTLE_ENDIAN)
											.asShortBuffer();
									blockBytePosition += (curNumSamples * 2);

									// Find corresponding outputHandle
									final SimpleChannelMefWriter curChWriter = labelToChannelWriter
											.get(curLab);
									if (curChWriter == null) {
										logger.error(
												"{}: Error: MEF writer for label not found: {} in {} in file {}",
												m, curLab, labelToChannelWriter.keySet(),
												h.getHandle().getFilePath());
										getMessageQueue().logError(
												getCreator(),
												WorkerRequest.PORTAL,
												WorkerRequest.ETL,
												getWorkItemSet(),
												"MEF writer for label " + curLab
														+ " not found in "
														+ labelToChannelWriter.keySet()
														+ " in file "
														+ h.getHandle().getFilePath());
										continue;
									}

									SimpleChannel curInfo = curChWriter
											.getSimpleChannel();
									MefWriter curWriter = curChWriter
											.getMefWriter();
									final double periodMicros = 1e6 / curInfo
											.getSamplingFrequency();

									if (fileStartUutc > recordingStartUutc
											&& !curChWriter.isStartedWriting()) {
										// This is a late starting channel, so
										// we
										// need to insert a gap
										final long[] gapStamp = { recordingStartUutc };
										final int[] gapSamp = { 0 };
										curWriter.writeData(gapSamp, gapStamp);
										curChWriter.setStartedWriting(true);

									}
									// Create TimeStamp vector
									long[] stamps = new long[curNumSamples];
									int[] samps = new int[curNumSamples];

									double blockStartUutc = timeKeepingTal == null
											? fileStartUutc
													+ (samplesWritten[iCh]
													* periodMicros)
											: fileStartUutc + (timeKeepingTal
													.getOnsetSeconds() * 1e6);

									for (int j = 0; j < curNumSamples; j++) {
										samps[j] = (int) (curInfo
												.getVoltageOffset() + shortBuf
												.get());

										stamps[j] = (long) (blockStartUutc + j
												* periodMicros);
									}
									logger.trace("{} EDF Block start time: {}",
											m,
											stamps[0]);

									curWriter.writeData(samps, stamps);
									curChWriter.setStartedWriting(true);
									samplesWritten[iCh] += curNumSamples;

								}
							}
						}

					}
				} catch (RuntimeException | Error | IOException e) {
					logger.error(m + ": Exception. Returning STATE.FAILED", e);
					getMessageQueue().logError(getCreator(),
							WorkerRequest.PORTAL, WorkerRequest.ETL,
							getWorkItemSet(),
							"Error transcoding file: " + e.getMessage());
					return STATE.FAILED;
				} finally {
					try {
						h.close();
					} catch (IOException e) {
						getMessageQueue().logError(getCreator(),
								WorkerRequest.PORTAL, WorkerRequest.ETL,
								getWorkItemSet(),
								"Error closing file: " + e.getMessage());
						logger.info(
								m
										+ ": Ignoring exception when closing EDFParameters",
								e);
					}
				}
			}
		} finally {
			getMessageQueue().logEvent(getCreator(), WorkerRequest.PORTAL,
					WorkerRequest.ETL, getWorkItemSet(),
					"Transcoder shutting down");
			logger.debug("{} Closing MEF Files", m);
			for (SimpleChannelMefWriter w : labelToChannelWriter.values()) {
				try {
					w.getMefWriter().close();
				} catch (RuntimeException | Error e) {
					logger.info(
							m
									+ ": Ignoring exception when closing MefWriter",
							e);
				}
			}
			logger.debug("{} Closing IOutputStreams", m);
			for (IOutputStream s : tempOutputs) {
				try {
					s.close();
				} catch (IOException e) {
					logger.info(
							m
									+ ": Ignoring exception when closing IOutputStream",
							e);
				}
			}
		}

		ParametersForSnapshotContent snapP = new ParametersForSnapshotContent();

		snapP.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
		snapP.setTargetSnapshotId(getCurrentParameters().getTargetSnapshotId());

		for (IStoredObjectReference s : tempHandles) {
			snapP.getInputs().add(s);
		}

		getNextActions().add(
				new AddToSnapshotAction(
						getMessageQueue(),
						getWorkItemSet(),
						getSession(),
						snapP));
		final List<TsAnnotationDto> annotations = talToAnn(tals);
		if (!annotations.isEmpty()) {
			getNextActions().add(
					new ImportAnnotationsAction(
							getMessageQueue(),
							getWorkItemSet(),
							getSession(),
							snapP,
							annotations));

		}

		return STATE.SUCCEEDED;
	}

	private List<TsAnnotationDto> talToAnn(List<TimestampedAnnotationList> tals) {
		final List<TsAnnotationDto> annotations = new ArrayList<>();
		for (final TimestampedAnnotationList tal : tals) {
			final long startOffsetMicros = (long) (tal.getOnsetSeconds() * 1e6);
			// EDF Annotations can start before start of recording. We will
			// ignore those.
			if (startOffsetMicros >= 0) {
				final long endOffsetMicros = startOffsetMicros
						+ (long) (tal.getDurationSeconds() * 1e6);
				for (final String comment : tal.getComments()) {
					final TsAnnotationDto annotation = new TsAnnotationDto(
							new HashSet<TimeSeriesDto>(),
							getCreator().getUsername(),
							startOffsetMicros,
							endOffsetMicros,
							comment,
							null,
							comment,
							null,
							"EDF Annotations",
							null);
					annotations.add(annotation);
				}
			}
		}
		return annotations;
	}

	/**
	 * Returns a list of TALs in the order they were found in the block
	 * 
	 * @param blockBytes
	 * @param annOffset
	 * @param annLengthBytes
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private List<TimestampedAnnotationList> processAnnotationBytes(
			byte[] blockBytes,
			int annOffset,
			int annLengthBytes) throws UnsupportedEncodingException {
		checkArgument(annOffset >= 0 && annOffset < blockBytes.length);
		checkArgument(annOffset + annLengthBytes <= blockBytes.length);
		final List<TimestampedAnnotationList> tals = new ArrayList<>();
		final List<Byte> currentTALBytes = new ArrayList<>();
		for (int i = annOffset; i < annOffset + annLengthBytes - 1; i++) {
			final byte b1 = blockBytes[i];
			final byte b2 = blockBytes[i + 1];
			if (b1 == 20 && b2 == 0) {
				final TimestampedAnnotationList tal = new TimestampedAnnotationList(
						currentTALBytes);
				tals.add(tal);
				currentTALBytes.clear();
				// Skip over that 0
				i++;
			} else {
				currentTALBytes.add(b1);
			}
		}

		return tals;

	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new TranscodeFromEDFAction(mQueue, origin, session,
				(SnapshotParameters) arguments, null, null);
	}

	private MefWriter createMEFWriter(
			String snapshotName,
			SimpleChannel chInfo) throws IOException {
		final String origLabel = chInfo.getChannelName();
		String mappedLabel = origLabel;
		if (getCurrentParameters() instanceof SnapshotParameters) {
			final SnapshotParameters snapParams = (SnapshotParameters) getCurrentParameters();
			mappedLabel = snapParams.getMappedChannel(origLabel);
		}
		// System.out.println("Creating MEF File...");
		final String channelLogStr = origLabel.equals(mappedLabel) ? mappedLabel : origLabel + " -> " + mappedLabel;
		getMessageQueue().logEvent(
				getCreator(),
				WorkerRequest.PORTAL,
				WorkerRequest.ETL,
				getWorkItemSet(),
				"Creating MEF file for " + snapshotName + " channel "
						+ channelLogStr);

		String initalPrefixComponent = "Animal_Data/";

		if (getCurrentParameters() instanceof SnapshotParameters) {
			final SnapshotParameters snapshotParameters = (SnapshotParameters) getCurrentParameters();
			final boolean isHuman = snapshotParameters.isHuman();
			if (isHuman) {
				initalPrefixComponent = "Human_Data/";
			}
			final String institution = snapshotParameters.getOwner();
			if (institution != null) {
				initalPrefixComponent += institution + "/";
			}
		}

		String name = initalPrefixComponent + snapshotName + "/"
				+ mappedLabel + ".mef";

		IObjectServer perServ = StorageFactory.getPersistentServer();
		IStoredObjectContainer cont = perServ.getDefaultContainer();

		FileReference newHandle = new FileReference(
				(DirectoryBucketContainer) cont, name,
				name);
		System.out.println("Trying to create channel " + newHandle);

		IOutputStream output = perServ.openForOutput(newHandle);

		FileOutputStream tempStream = (FileOutputStream) output
				.getOutputStream();
		tempOutputs.add(output);
		tempHandles.add(newHandle);

		MefHeader2 meh = new MefHeader2();

		meh.setChannelName(mappedLabel);
		meh.setSamplingFrequency(chInfo.getSamplingFrequency());
		meh.setHeaderLength((short) 1024);

		// Set voltage conversion factor
		meh.setVoltageConversionFactor(chInfo.getVoltageConversionFactor());

		double secPerMEFBlock = Math.max(
				6000.0 / chInfo.getSamplingFrequency(), 1e-6);
		long discontinuity_threshold = (long) Math.ceil(1.5 * (1e6 / chInfo
				.getSamplingFrequency()));

		MefWriter newMefWriter = new MefWriter(
				tempStream,
				mappedLabel,
				meh,
				secPerMEFBlock,
				chInfo.getSamplingFrequency(),
				discontinuity_threshold);

		getMessageQueue().logEvent(
				getCreator(),
				WorkerRequest.PORTAL,
				WorkerRequest.ETL,
				getWorkItemSet(),
				"Initialized MEF file for " + snapshotName + " channel "
						+ channelLogStr);

		return newMefWriter;

	}

	public void setChannelInfo(List<SimpleChannel> info) {
		this.chInfo = info;

	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		// TODO Auto-generated method stub
		return null;
	}

	private boolean isAnnotationChannel(String channelName) {
		final boolean isAnnChannel = channelName.equals("EDF Annotations");
		return isAnnChannel;
	}

	private boolean ignoreChannel(String channelName) {
		boolean ignoreChannel = !isAnnotationChannel(channelName)
				&& !includedChannels.isEmpty()
				&& !includedChannels.contains(channelName);
		return ignoreChannel;
	}

	private boolean isEdfPlus(EDFHeader edfHeader) {
		final String m = "isEdfPlus(...)";
		final String format = edfHeader.getFormatVersion();
		logger.debug("{}: EDF version [{}]", m, format);
		return format.startsWith("EDF+");
	}

}

