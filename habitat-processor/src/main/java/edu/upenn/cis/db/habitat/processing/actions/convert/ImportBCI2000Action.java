/*******************************************************************************
 * Copyright 2015 Trustees of the University of Pennsylvania / Blackfynn Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.convert.BCI2000Parameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class ImportBCI2000Action extends BaseProcessingAction {

	public static final String NAME = "ImportBCI2000Action";
//	WorkItemSet workItems = new WorkItemSet("Work item set", null,getOriginatingItemSet().getRequest(), getOriginatingItemSet().getOrigin());
    RemoteHabitatSession session;
    ControlFileParameters parameters;
    String snapshotName;

    Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
    List<BufferedInputStream> iStreams = new ArrayList<BufferedInputStream>();

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public ImportBCI2000Action(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
            ControlFileParameters parameters) {
        super(mQueue, origin);
        final String m = "ImportBCI2000Action(WorkItemSet, IeegSession, ControlFileParameters)";
        this.session = session;
        this.parameters = parameters;
        if (logger.isTraceEnabled()) {
            try {
                final String paramsString = ObjectMapperFactory.newPasswordFilteredWriter()
                        .writeValueAsString(this.parameters);
                logger.trace("{}: Creating Action with parameters {}", m,
                        paramsString);
            } catch (JsonProcessingException e) {
                logger.error(m + ": Exception in debug log", e);
            }
        }
    }

    public ImportBCI2000Action() {
        super(null, null);
    }
    
    
	
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
//		Execute will:
//		       * 1) create BCI200 parameters from the header of the dat.file
//		       * 2) call processSnapshot which:
//		       *    a) finds unique channels in dataset
//		       *    b) add CreateSnapshotAction to NextActions
//		       *        1) Depending on isHuman parameter in SnapshotParameters, creates Human, or Animal Dataset 
//		       *    c) add MapMetaData to NextActions
//		       *        1) Maps generic info from SnapshotParameters, creates HospitalAdmission or Animal
//		       *    d) add TranscodeBCI200Action to NextActions
//		       *        1) Transcodes to MEF files
//		       *        2) add AddToSnapshotAction to NextActions
//		       */
		 
		final String m = "execute(...)";

		 if(getWorkItemSet().size()>1)
				logger.error("BCI200Import: CURRENT IMPORTER ONLY SUPPORTS ONE .DAT FILE PER DATASET (converting first)");
		
		 //TODO:  Make this work for multiple datasets in folder --> Should turn into collection of snapshots.
		 
		 for (IWorkItem item: getWorkItemSet()){
			 
				IStoredObjectReference inFile = item.getMainHandle();
				BCI2000Parameters header = BCI2000Parameters.buildFromRef(inFile);

				String snapshotName = parameters.getTargetSnapshot();
				
				processSnapshot(header, snapshotName);
										
				break;
			}
		 
		return STATE.SUCCEEDED;
	}

	private void processSnapshot(BCI2000Parameters header, String snapshotName2) {
		final String m = "processSnapshot(...)";
		
		List<String> channelIds = new ArrayList<String>();
        SnapshotParameters ssp = new SnapshotParameters("",
                null, SnapType.other, channelIds);
        
        ssp.merge(parameters);

        String institution = parameters.getSnapshotParameters().getInstitution();
        ssp.setInstitution(institution);
        
        String owner = parameters.getSnapshotParameters().getOwner();
        ssp.setOwner(owner);

        boolean isHuman = parameters.getSnapshotParameters().isHuman();
        ssp.setHuman(isHuman);

        for(int i=0; i< header.getNrChannels();i++){
        	String str = String.format("Signal_%03d", i);
        	channelIds.add(str);
        }
        
        // Create snapshot as necessary
        IProcessingAction snap = new CreateSnapshotAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session,
                ssp);
        getNextActions().add(snap);
               

        // Map metadata to it as necessary
        IProcessingAction map = new MapMetadataAction(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session, ssp);
        snap.getNextActions().add(map);
        
        // Add Transcode action
        TranscodeFromBCI2000Action transcode = new TranscodeFromBCI2000Action(
        		getMessageQueue(),
                this.getWorkItemSet(),
                session, ssp, header);

        map.getNextActions().add(transcode);
        
		
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		 return new ImportBCI2000Action(
				 mQueue,
				 origin, 
				 session,
	             (ControlFileParameters) arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

}
