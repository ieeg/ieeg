/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class BaseSnapshotProcessingAction extends BaseProcessingAction {

	protected RemoteHabitatSession session;
	ParametersForSnapshotContent parameters;
	
	public BaseSnapshotProcessingAction() {
		super(null, null);
		this.session = null;
		this.parameters = null;
	}
	
	public BaseSnapshotProcessingAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			ParametersForSnapshotContent parameters) {
		super(mQueue, origin);
		this.session = session;
		this.parameters = parameters;
	}

	@Override
	public abstract ParametersForSnapshotContent getDefaultArguments();

	@Override
	public ParametersForSnapshotContent getCurrentParameters() {
		return parameters;
	}
	
	public void setParameters(ParametersForSnapshotContent parameters) {
		this.parameters = parameters;
	}
	
	public RemoteHabitatSession getSession() {
		return session;
	}
	
	public void setSession(RemoteHabitatSession session) {
		this.session = session;
	}

	@Override
	public boolean isReady() {
		if (getCurrentParameters() != null &&
				getCurrentParameters().getTargetSnapshot() != null && 
				getCurrentParameters().getTargetSnapshotId() == null) {
			final IUser creator = getCreator();
			if (!(creator instanceof User)) {
				throw new IllegalStateException(
						"The creator must have type "
								+ User.class.getCanonicalName()
								+ ". It has type "
								+ creator.getClass().getCanonicalName());
			}
			String ret = session.getSnapshotIdFor((User)creator, 
					getCurrentParameters().getTargetSnapshot());
			
			if (ret != null)
				getCurrentParameters().setTargetSnapshotId(ret);
		}
		
		return getCurrentParameters() == null ||
				getCurrentParameters().getTargetSnapshot() == null ||
				(getCurrentParameters() != null && 
				getCurrentParameters().getTargetSnapshotId() != null);
	}

	public ParametersForSnapshotContent getParameters() {
		return parameters;
	}

	
}
