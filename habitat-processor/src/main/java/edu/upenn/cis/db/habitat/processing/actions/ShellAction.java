/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters;
import edu.upenn.cis.db.habitat.processing.parameters.ShellParameters.CommandLine;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Execute a command-line script
 * 
 * @author zives
 *
 */
public class ShellAction extends BaseSnapshotProcessingAction {
	public static final String NAME = "ShellAction";
	
	private final static Logger logger = LoggerFactory
			.getLogger(ShellAction.class);
	
	ShellAction() {
		super();
	}
	
	public ShellAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, ShellParameters parms) {
		super(mQueue, origin, session, parms);
	}


	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		if (getCurrentParameters() != null && arguments != null)
			getCurrentParameters().merge((ShellParameters)arguments);
		else if (arguments != null)
			setParameters((ShellParameters)arguments);
		
		int exitCode = 0;
//		Process p;
		
		try {
			ProcessBuilder pb = new ProcessBuilder();
			Map<String, String> env = pb.environment();
			Map<String, String> vars = this.getCurrentParameters().getVars();
			for (Entry<String, String> var:vars.entrySet()){
				env.put(var.getKey(), var.getValue());
			}

			List<String> commands = pb.command();
//			String cmdStr = "";
//			int i=0;
			for (CommandLine cmd: getCurrentParameters().getCommandLines()) {
				StringBuilder cmdLine = new StringBuilder();
				cmdLine.append(cmd.getCommand());

				for (String arg: cmd.getArgs())
					cmdLine.append(" " + arg);

//				if (i>0)
//					cmdStr+=";";
//				
//				cmdStr += cmdLine.toString();
//				i++;
				
				commands.add(cmdLine.toString());
			}
			
			Process p = pb.start();

			// TODO: do we look at the output to determine success??
			BufferedReader is = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String str = is.readLine();
			while (str != null) {
				System.out.println((str));
				str = is.readLine();
			}

			is = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			str = is.readLine();
			while (str != null) {
				System.err.println((str));
				str = is.readLine();
			}  
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return STATE.FAILED;
		}
		
		return (exitCode == 0) ? STATE.SUCCEEDED : STATE.FAILED;
	}


	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
		return new ShellAction(mQueue, origin, session, (ShellParameters)arguments);
	}

	@Override
	public ShellParameters getDefaultArguments() {
		return new ShellParameters();
	}
	
	@Override
	public ShellParameters getCurrentParameters() {
		return (ShellParameters)super.getCurrentParameters();
	}
}