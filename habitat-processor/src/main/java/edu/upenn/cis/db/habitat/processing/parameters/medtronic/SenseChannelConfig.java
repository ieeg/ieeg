/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

public class SenseChannelConfig {
	String e0HPF;
	String e1HPF;
	String e2HPF;
	String e3HPF;
	String e8HPF;
	String e9HPF;
	String e10HPF;
	String e11HPF;
	String frontEndLPF;
	String tdSampleRate;
	String fs4FilterEnabled;
	String compressionEnabled;
	String powSampleRate;
	String bridging;
	List<MedtronicChannel> channels;
	
	
	@XmlElement(name="E0HPF")
	public String getE0HPF() {
		return e0HPF;
	}

	@XmlElement(name="E1HPF")
	public String getE1HPF() {
		return e1HPF;
	}

	@XmlElement(name="E2HPF")
	public String getE2HPF() {
		return e2HPF;
	}

	@XmlElement(name="E3HPF")
	public String getE3HPF() {
		return e3HPF;
	}


	@XmlElement(name="E8HPF")
	public String getE8HPF() {
		return e8HPF;
	}

	@XmlElement(name="E9HPF")
	public String getE9HPF() {
		return e9HPF;
	}

	@XmlElement(name="E10HPF")
	public String getE10HPF() {
		return e10HPF;
	}

	@XmlElement(name="E11HPF")
	public String getE11HPF() {
		return e11HPF;
	}

	@XmlElement(name="FrontEndLPF")
	public String getFrontEndLPF() {
		return frontEndLPF;
	}



	@XmlElement(name="TDSampleRate")
	public String getTdSampleRate() {
		return tdSampleRate;
	}

	@XmlElement(name="Fs4FilterEnabled")
	public String getFs4FilterEnabled() {
		return fs4FilterEnabled;
	}


	@XmlElement(name="CompressionEnabled")
	public String getCompressionEnabled() {
		return compressionEnabled;
	}

	@XmlElement(name="PowSampleRate")
	public String getPowSampleRate() {
		return powSampleRate;
	}

	@XmlElement(name="Briding")
	public String getBridging() {
		return bridging;
	}

	@XmlElements({
		@XmlElement(name="Channel1", type=MedtronicChannel.class),
		@XmlElement(name="Channel2", type=MedtronicChannel.class),
		@XmlElement(name="Channel3", type=MedtronicChannel.class),
		@XmlElement(name="Channel4", type=MedtronicChannel.class),
		@XmlElement(name="Channel5", type=MedtronicChannel.class),
		@XmlElement(name="Channel6", type=MedtronicChannel.class),
		@XmlElement(name="Channel7", type=MedtronicChannel.class),
		@XmlElement(name="Channel8", type=MedtronicChannel.class)
	})
	public List<MedtronicChannel> getChannels() {
		return channels;
	}

	public void setE0HPF(String e0hpf) {
		e0HPF = e0hpf;
	}

	public void setE1HPF(String e1hpf) {
		e1HPF = e1hpf;
	}

	public void setE2HPF(String e2hpf) {
		e2HPF = e2hpf;
	}

	public void setE3HPF(String e3hpf) {
		e3HPF = e3hpf;
	}

	public void setE8HPF(String e8hpf) {
		e8HPF = e8hpf;
	}

	public void setE9HPF(String e9hpf) {
		e9HPF = e9hpf;
	}

	public void setE10HPF(String e10hpf) {
		e10HPF = e10hpf;
	}

	public void setE11HPF(String e11hpf) {
		e11HPF = e11hpf;
	}

	public void setFrontEndLPF(String frontEndLPF) {
		this.frontEndLPF = frontEndLPF;
	}

	public void setTdSampleRate(String tdSampleRate) {
		this.tdSampleRate = tdSampleRate;
	}

	public void setFs4FilterEnabled(String fs4FilterEnabled) {
		this.fs4FilterEnabled = fs4FilterEnabled;
	}

	public void setCompressionEnabled(String compressionEnabled) {
		this.compressionEnabled = compressionEnabled;
	}

	public void setPowSampleRate(String powSampleRate) {
		this.powSampleRate = powSampleRate;
	}

	public void setBridging(String bridging) {
		this.bridging = bridging;
	}

	public void setChannels(List<MedtronicChannel> channels) {
		this.channels = channels;
	}

	
	
	/*
  <SenseChannelConfig>
    <E0HPF>0.5 Hz</E0HPF>
    <E1HPF>0.5 Hz</E1HPF>
    <E2HPF>0.5 Hz</E2HPF>
    <E3HPF>0.5 Hz</E3HPF>
    <E8HPF>0.5 Hz</E8HPF>
    <E9HPF>0.5 Hz</E9HPF>
    <E10HPF>0.5 Hz</E10HPF>
    <E11HPF>0.5 Hz</E11HPF>
    <FrontEndLPF>100 Hz</FrontEndLPF>
    <TDSampleRate>422 Hz</TDSampleRate>
    <Fs4FilterEnabled>Disabled</Fs4FilterEnabled>
    <CompressionEnabled>Enabled</CompressionEnabled>
    <PowSampleRate>5 Hz</PowSampleRate>
    <Bridging>No Bridging</Bridging>
    <Channel1>
      <ChannelType>TD</ChannelType>
      <BW>±2.5 Hz</BW>
      <CtrFreq>2.5 Hz</CtrFreq>
      <PowGain>4000</PowGain>
      <TDGain>2000</TDGain>
      <PlusInput>E2</PlusInput>
      <MinusInput>E1</MinusInput>
      <TimeGainActuals>
        <Gain250>237.254901960784</Gain250>
        <Gain500>470.588235294118</Gain500>
        <Gain1000>945.098039215686</Gain1000>
        <Gain2000>1890.19607843137</Gain2000>
      </TimeGainActuals>
      <PowerGainActuals>
        <BW5Hz>
          <Gain500>611.764705882353</Gain500>
          <Gain1000>1223.52941176471</Gain1000>
          <Gain2000>2447.05882352941</Gain2000>
          <Gain4000>4894.11764705882</Gain4000>
        </BW5Hz>
        <BW16Hz>
          <Gain500>631.372549019608</Gain500>
          <Gain1000>1262.74509803922</Gain1000>
          <Gain2000>2525.49019607843</Gain2000>
          <Gain4000>5050.98039215686</Gain4000>
        </BW16Hz>
        <BW32Hz>
          <Gain500>631.372549019608</Gain500>
          <Gain1000>1262.74509803922</Gain1000>
          <Gain2000>2525.49019607843</Gain2000>
          <Gain4000>5050.98039215686</Gain4000>
        </BW32Hz>
      </PowerGainActuals>
    </Channel1>
    <Channel2>
      <ChannelType>Power</ChannelType>
      <BW>±2.5 Hz</BW>
      <CtrFreq>25.0 Hz</CtrFreq>
      <PowGain>4000</PowGain>
      <TDGain>2000</TDGain>
      <PlusInput>E0</PlusInput>
      <MinusInput>E3</MinusInput>
      <PowerGainActuals>
        <BW5Hz>
          <Gain500>639.21568627451</Gain500>
          <Gain1000>1278.43137254902</Gain1000>
          <Gain2000>2556.86274509804</Gain2000>
          <Gain4000>5113.72549019608</Gain4000>
        </BW5Hz>
        <BW16Hz>
          <Gain500>658.823529411765</Gain500>
          <Gain1000>1317.64705882353</Gain1000>
          <Gain2000>2635.29411764706</Gain2000>
          <Gain4000>5270.58823529412</Gain4000>
        </BW16Hz>
        <BW32Hz>
          <Gain500>650.980392156863</Gain500>
          <Gain1000>1301.96078431373</Gain1000>
          <Gain2000>2603.92156862745</Gain2000>
          <Gain4000>5207.8431372549</Gain4000>
        </BW32Hz>
      </PowerGainActuals>
    </Channel2>
    <Channel3>
      <ChannelType>TD</ChannelType>
      <BW>±2.5 Hz</BW>
      <CtrFreq>2.5 Hz</CtrFreq>
      <PowGain>4000</PowGain>
      <TDGain>2000</TDGain>
      <PlusInput>E10</PlusInput>
      <MinusInput>E9</MinusInput>
      <TimeGainActuals>
        <Gain250>232.352941176471</Gain250>
        <Gain500>460.78431372549</Gain500>
        <Gain1000>925.490196078431</Gain1000>
        <Gain2000>1850.98039215686</Gain2000>
      </TimeGainActuals>
      <PowerGainActuals>
        <BW5Hz>
          <Gain500>517.64705882353</Gain500>
          <Gain1000>1035.29411764706</Gain1000>
          <Gain2000>2070.58823529412</Gain2000>
          <Gain4000>4141.17647058824</Gain4000>
        </BW5Hz>
        <BW16Hz>
          <Gain500>564.705882352941</Gain500>
          <Gain1000>1129.41176470588</Gain1000>
          <Gain2000>2258.82352941176</Gain2000>
          <Gain4000>4517.64705882353</Gain4000>
        </BW16Hz>
        <BW32Hz>
          <Gain500>623.529411764706</Gain500>
          <Gain1000>1247.05882352941</Gain1000>
          <Gain2000>2494.11764705882</Gain2000>
          <Gain4000>4988.23529411765</Gain4000>
        </BW32Hz>
      </PowerGainActuals>
    </Channel3>
    <Channel4>
      <ChannelType>Power</ChannelType>
      <BW>±2.5 Hz</BW>
      <CtrFreq>25.0 Hz</CtrFreq>
      <PowGain>4000</PowGain>
      <TDGain>2000</TDGain>
      <PlusInput>E8</PlusInput>
      <MinusInput>E11</MinusInput>
      <PowerGainActuals>
        <BW5Hz>
          <Gain500>156.862745098039</Gain500>
          <Gain1000>313.725490196078</Gain1000>
          <Gain2000>627.450980392157</Gain2000>
          <Gain4000>1254.90196078431</Gain4000>
        </BW5Hz>
        <BW16Hz>
          <Gain500>200</Gain500>
          <Gain1000>400</Gain1000>
          <Gain2000>800</Gain2000>
          <Gain4000>1600</Gain4000>
        </BW16Hz>
        <BW32Hz>
          <Gain500>321.56862745098</Gain500>
          <Gain1000>643.137254901961</Gain1000>
          <Gain2000>1286.27450980392</Gain2000>
          <Gain4000>2572.54901960784</Gain4000>
        </BW32Hz>
      </PowerGainActuals>
    </Channel4>
  </SenseChannelConfig>
 */
}
