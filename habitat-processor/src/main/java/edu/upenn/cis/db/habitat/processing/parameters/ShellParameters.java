/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class ShellParameters extends ParametersForSnapshotContent {
	public static class CommandLine implements JsonTyped {
		String command;
		List<String> args;
		
		
		public CommandLine() {
			args = new ArrayList<String>();
		}
		
		public CommandLine(String command, List<String> args) {
			this.command = command;
			this.args = args;
		}
		
		public CommandLine(String command) {
			this.command = command;
			this.args = new ArrayList<String>();
		}

		public String getCommand() {
			return command;
		}
 
		public void setCommand(String command) {
			this.command = command;
		}

		public List<String> getArgs() {
			return args;
		}

		public void setArgs(List<String> args) {
			this.args = args;
		}
		
		
	}
	
	List<CommandLine> commandLines = new ArrayList<CommandLine>();
	Map<String,String> vars = new HashMap<String,String>(); //Environment variables
	
	public Map<String, String> getVars() {
		return vars;
	}

	public void setVars(Map<String, String> vars) {
		this.vars = vars;
	}

	public List<CommandLine> getCommandLines() {
		return commandLines;
	}
	
	public void setCommandLines(List<CommandLine> cmds) {
		commandLines = cmds;
	}
	
	public void addCommandLines(Collection<CommandLine> cmds) {
		commandLines.addAll(cmds);
	}
	
	@Override
	public void merge(Parameters parameters2) {
		super.merge(parameters2);
		
		if (parameters2 instanceof ShellParameters) {
			ShellParameters two = (ShellParameters)parameters2;
			addCommandLines(two.getCommandLines());
		}
	}
}
