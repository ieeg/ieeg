/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.collect.Lists.newArrayList;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Optional;


import com.google.common.base.Splitter;

import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction.TimeSeriesSettings;
import edu.upenn.cis.db.habitat.processing.actions.ReadSubmittedDirectoryAction.IDirectoryParser.IParsedDirectory;
import edu.upenn.cis.db.habitat.processing.actions.convert.ImportKahanaFilesAction;
import edu.upenn.cis.db.habitat.processing.actions.medtronic.ReadMdtXmlFileAction;
import edu.upenn.cis.db.habitat.processing.actions.neuropace.ReadNPLayFileAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ProjectPermissionParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.DirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * Reads the contents of a directory and executes a workflow
 * 
 * @author zives
 *
 */
public class ReadSubmittedDirectoryAction extends BaseProcessingAction {
	public interface IDirectoryParser {
		public interface IParsedDirectory {
			Optional<Boolean> isHuman();

			Optional<String> getOrganizationName();

			Optional<String> getProjectName();

			Optional<String> getDatasetName();

			String getDirectory();
		}

		IParsedDirectory parseDirectory(String directory);
	}

	private final Splitter commaSplitter = Splitter
			.on(",")
			.trimResults()
			.omitEmptyStrings();
	private final Splitter colonSplitter = Splitter
			.on(":")
			.trimResults()
			.omitEmptyStrings();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	public static final String NAME = "ReadSubmittedDirectoryAction";

	// protected WorkItemSet workItems = new WorkItemSet("Work item set", null);

	protected RemoteHabitatSession session;
	protected ControlFileParameters parameters;

	protected Set<IProcessingAction> nextActions = new LinkedHashSet<>();
	protected IDirectoryParser directoryParser;

	public ReadSubmittedDirectoryAction(IMessageQueue mQueue,
			WorkItemSet origin, RemoteHabitatSession session,
			ControlFileParameters parameters) {
		super(mQueue, origin);
		this.session = session;
		this.parameters = parameters;
		this.directoryParser = new IeegDirectoryParser();
	}

	public ReadSubmittedDirectoryAction() {
		super(null, null);
		this.directoryParser = new IeegDirectoryParser();
	}

	/**
	 * Reads the input directories and looks for indicators of a control /
	 * layout file. Triggers the appropriate ReadXXXFilAction.
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		if (arguments != null && parameters != null
				&& arguments instanceof Parameters)
			parameters.merge((Parameters) arguments);
		else if (arguments != null)
			parameters = (ControlFileParameters) arguments;

		/*
		 * 1) Iterate over inputs, which usually is a single folder 2) Separate
		 * control files from other files 3) If no control file exists, create a
		 * control file with contents of folder 4) Check if special importers
		 * should be used 5) If not, create action that reads/processes
		 * available control files.
		 */

		final IUser creator = getCreator();
		boolean failed = false;
		for (JsonTyped inFileSpec : parameters.getInputs()) {
			try {
				FileReference inFile = ((FileReference)inFileSpec);
				getMessageQueue().logEvent(creator, WorkerRequest.PORTAL, WorkerRequest.ETL, getWorkItemSet(), "Reading submitted directory " + 
						inFile.getFilePath());
	
				IObjectServer bestServer = StorageFactory.getS3Inbox();
				String dirName = inFile.getFilePath();

				if (dirName.endsWith("/."))
					dirName = dirName.substring(0, dirName.length() - 2);
				final IParsedDirectory parsedDirectory = directoryParser
						.parseDirectory(dirName);
				
				Set<IStoredObjectReference> contents = bestServer.getDirectoryContents(inFile.getDirBucketContainer(), dirName);
				logger.debug("{}: Got {} files from directory {}", m, contents.size(), dirName);

				List<FileReference> allFiles = newArrayList();
				List<FileReference> ctrFiles = newArrayList();

				boolean hasMedtronic = false;
				boolean hasNeuropace = false;
				boolean hasControl = false;
				boolean hasKahana = false;

				// Recursive match
				Set<IStoredObjectReference> dirs = new HashSet<>();
				List<IStoredObjectReference> newFiles = new ArrayList<>();
				for (IStoredObjectReference possibleDir : contents) {
					if (possibleDir.isContainer()) {
						newFiles.addAll(bestServer.getFilesRecursivelyMatching(
								bestServer.getContainerFrom(
										inFile.getDirBucketContainer(),
										possibleDir), ".*"));
						dirs.add(possibleDir);
					}
				}
				contents.addAll(newFiles);

				for (IStoredObjectReference handle : contents) {
					FileReference fHandle = (FileReference) handle;

					// Ignore hidden files.
					File curFile = new File(fHandle.getFilePath());
					if (curFile.getName().startsWith("."))
						continue;

					// Skip directory
					if (fHandle.isContainer())
						continue;

					String mime = RegisterActions.getMimeType(fHandle);
					if (mime.equals("application/control")) {
						ctrFiles.add(fHandle);
						hasControl = true;
						getMessageQueue().logEvent(creator,
								WorkerRequest.PORTAL, WorkerRequest.ETL,
								getWorkItemSet(), "Found control file");
					} else {
						allFiles.add(fHandle);
					}

					if (mime.equals("application/medtronic")) {
						getMessageQueue().logEvent(creator,
								WorkerRequest.PORTAL, WorkerRequest.ETL,
								getWorkItemSet(), "Medtronic data");
						hasMedtronic = true;
					} else if (mime.equals("application/neuropace")) {
						getMessageQueue().logEvent(creator,
								WorkerRequest.PORTAL, WorkerRequest.ETL,
								getWorkItemSet(), "Neuropace data");
						hasNeuropace = true;
					} else if (mime.equals("application/kahana")) {
						getMessageQueue().logEvent(creator,
								WorkerRequest.PORTAL, WorkerRequest.ETL,
								getWorkItemSet(), "Kahana Lab data");
						hasKahana = true;
					}
				}
				final Optional<String> designatedNameOpt = parsedDirectory
						.getDatasetName();
				String designatedName = designatedNameOpt.isPresent()
						? designatedNameOpt.get()
						: creator.getUsername()
								+ "-";

				// Create Control File if none exists. This creates a file with
				// references to all files except controlfiles in folder.
				Optional<FileReference> dirControlFileOpt = Optional.absent();
				if (!hasControl) {
					final FileReference dirControlFile = createDirControlFile(
							designatedName, parsedDirectory, allFiles);
					if (dirControlFile == null) {
						return STATE.FAILED;
					}
					ctrFiles.add(dirControlFile);
					dirControlFileOpt = Optional.of(dirControlFile);
				}

				// Create control File Parameters.
				Collections.sort(allFiles);
				ControlFileParameters cp = new ControlFileParameters(ctrFiles);
				cp.getSnapshotParameters().setSnapshotType(SnapType.other);
				cp.setTargetSnapshot(designatedName);
				cp.getSnapshotParameters().setTargetSnapshot(designatedName);

				// Create new Workset
				WorkItemSet newSet = new WorkItemSet(getWorkItemSet());// .getDescription(),
																		// getOriginatingItemSet().getJson());

				// Add items to Workset: hasControl should always be true at
				// this time.
				for (IWorkItem item : getWorkItemSet()) {
					String s3Key = item.getMainHandle().getObjectKey();
					String fName = item.getMainHandle().getFilePath();

					if (s3Key.endsWith("/."))
						s3Key = s3Key.substring(0, s3Key.length() - 2);
					if (fName.endsWith("/."))
						fName = fName.substring(0, fName.length() - 2);

					FileReference handle = new FileReference(
							(DirectoryBucketContainer) item.getMainHandle()
									.getDirBucketContainer(),
							s3Key, fName);
					WorkItem newItem = new WorkItem(item.getItemId(),
							item.getParentId(),
							handle,
							null,
							item.getCreator(),
							item.getTimestamp(),
							item.getMediaType(),
							item.getDescription(),
							item.getJson());
					logger.debug(
							"{}: Adding {} to next action's work item set", m,
							handle);
					newSet.add(newItem);
				}

				// Check for special importers
				if (hasMedtronic) {
					cp.getSnapshotParameters().setHuman(false);
					getNextActions().add(new ReadMdtXmlFileAction(
							getMessageQueue(),
							newSet,
							session,
							cp));
				} else if (hasNeuropace) {
					cp.getSnapshotParameters().setHuman(true);
					getNextActions().add(new ReadNPLayFileAction(
							getMessageQueue(),
							newSet,
							session,
							cp));
				} else if (hasKahana) {
					cp.getSnapshotParameters().setHuman(true);
					cp.getInputs().addAll(ctrFiles);
					final Optional<String> projectName = parsedDirectory.getProjectName();
					if (projectName.isPresent()) {
						final ProjectPermissionParameters projPermParam = new ProjectPermissionParameters();
						projPermParam.setProjectName(projectName.get());
						cp
								.getSnapshotParameters()
								.getPermissionParameters()
								.getProjectPermissionParameters()
								.add(projPermParam);
						logger.debug(
								"{}: Setting project name to [{}] for Kahana dataset [{}]",
								m,
								projectName.get(),
								designatedName);
					}
					getNextActions().add(new ImportKahanaFilesAction(
							getMessageQueue(),
							newSet,
							session,
							cp));
				} else {
					getNextActions().add(new ReadControlFileAction(
							getMessageQueue(),
							newSet,
							session,
							cp));
				}

				// Clean up the control file if we made one.
				if (dirControlFileOpt.isPresent()) {
					final Date now = new Date();
					final IWorkItem deleteTempItem = new WorkItem(
							"clean temp",
							null,
							dirControlFileOpt.get(),
							Collections.<IStoredObjectReference> emptySet(),
							null,
							now.getTime(),
							"application/control",
							"Programmatically created temporary control file",
							null);
					final WorkItemSet deleteTempWork = new WorkItemSet(
							Collections.singleton(deleteTempItem),
							"Delete temporary control file",
							null);
					final DeleteAction deleteTempAction = new DeleteAction(
							getMessageQueue(),
							deleteTempWork);
					getNextActions().add(deleteTempAction);
				}
			} catch (Exception e) {
				getMessageQueue()
						.logError(
								creator,
								WorkerRequest.PORTAL,
								WorkerRequest.ETL,
								getWorkItemSet(),
								"Error processing submitted directory "
										+ inFileSpec.toString() + ": "
										+ e.getMessage());
				logger.error(m + ": Action failed.", e);
				failed = true;
			}
		}

		return (!failed) ? STATE.SUCCEEDED : STATE.FAILED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session, Parameters arguments) {
		return new ReadSubmittedDirectoryAction(mQueue, origin, session,
				(ControlFileParameters) arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Set<IProcessingAction> getNextActions() {
		return nextActions;
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

	protected FileReference createDirControlFile(String datasetName,
			IParsedDirectory directory, List<FileReference> otherFiles) {
		final String m = "createDirControlFile(...)";
//		checkArgument(!otherFiles.isEmpty());
		final DirectoryBucketContainer container = new DirectoryBucketContainer(
				null,
				"dirControlFileTemp");
		final FileReference controlFileHandle = new FileReference(
				container,
				null,
				datasetName + ".control");
		final ControlFileParameters controlFile = new ControlFileParameters();
		controlFile.setTargetSnapshot(datasetName);
		controlFile.getSnapshotParameters().setTargetSnapshot(datasetName);
		final boolean isHuman = directory.isHuman().isPresent() ? directory
				.isHuman().get() : false;
		controlFile.getSnapshotParameters().setHuman(isHuman);

		if (directory.getOrganizationName().isPresent()) {
			controlFile.getSnapshotParameters().setInstitution(
					directory.getOrganizationName().get());
		}
		if (directory.getProjectName().isPresent()) {
			final ProjectPermissionParameters projPermParameters = new ProjectPermissionParameters();
			projPermParameters.setProjectName(directory.getProjectName().get());
			controlFile
					.getSnapshotParameters()
					.getPermissionParameters()
					.getProjectPermissionParameters()
					.add(projPermParameters);
		}

		controlFile.getSnapshotParameters()
				.setOwner(getCreator().getUsername());
		controlFile.getSnapshotParameters().setSnapshotType(
				SnapType.emuRecording);
		Collections.sort(otherFiles);
		int channelIdx = 0;
		for (FileReference otherFile : otherFiles) {
			final String label = com.google.common.io.Files
					.getNameWithoutExtension(otherFile.getObjectKey());
			if (RegisterActions.getMimeType(otherFile).equals(
					"application/x-ieeg-import-config")) {
				try {
					setParamsFromConfigFile(otherFile, controlFile);
				} catch (IOException e) {
					getMessageQueue().logError(getCreator(), WorkerRequest.PORTAL,
							WorkerRequest.ETL, getWorkItemSet(),
							"Exception reading import config file: " + e.getMessage());
					logger.error(
							m + ": Exception reading import config file, returning null", e);
					return null;
				}
				continue;
			}
			controlFile.getInputs().add(otherFile);
			if (RegisterActions.getMimeType(otherFile)
					.equals("application/mef")) {
				final TimeSeriesSettings tsSettings = new TimeSeriesSettings();
				tsSettings.setChannelLabel(label);
				tsSettings.setChannelIndex(channelIdx++);
				tsSettings.setKey(otherFile.getObjectKey());
				controlFile.getSnapshotParameters().getSettings()
						.add(tsSettings);
				controlFile.getSnapshotParameters().addChannelLabel(label);
			}
		}
		IObjectServer stagingServer = StorageFactory.getStagingServer();
		IOutputStream os = null;
		try {
			os = stagingServer.openForOutput(controlFileHandle);
			final OutputStream fo = os.getOutputStream();

			final ObjectMapper om = ObjectMapperFactory.getObjectMapper();
			if (logger.isTraceEnabled()) {
				final ObjectWriter ow = ObjectMapperFactory
						.newPasswordFilteredWriter();
				final String controlFileString = ow
						.writeValueAsString(controlFile);
				logger.trace("{}: Created control file: [{}]", m,
						controlFileString);
			}
			om.writeValue(fo, controlFile);
		} catch (IOException e) {
			getMessageQueue().logError(getCreator(), WorkerRequest.PORTAL,
					WorkerRequest.ETL, getWorkItemSet(),
					"Exception writing control file: " + e.getMessage());
			logger.error(
					m + ": Exception writing control file, returning null", e);
			return null;
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					getMessageQueue()
							.logError(
									getCreator(),
									WorkerRequest.PORTAL,
									WorkerRequest.ETL,
									getWorkItemSet(),
									"Exception closing control file: "
											+ e.getMessage());
					logger.error(
							m
									+ ": Exception closing control file stream. Ignoring.",
							e);
				}
			}
		}
		return controlFileHandle;
	}

	/**
	 * 
	 * @param otherFile a ieeg-dataset.ini config file
	 * @param controlFile
	 */
	private void setParamsFromConfigFile(
			FileReference otherFile,
			ControlFileParameters controlFile) throws IOException {
		final String m = "setParamsFromConfigFile(...)";

		final IObjectServer bestServer = StorageFactory
				.getBestServerFor(otherFile);
		logger.debug("{}: Reading {} for additional parameters",
				m,
				otherFile);
		IInputStream inStream = null;
		try {
			inStream = bestServer.openForInput(otherFile);

			try (final BufferedInputStream inFStream =
					new BufferedInputStream(inStream.getInputStream())) {

				final Ini ini = new Ini();
				final Config iniConfig = new Config();
				// Tell the parser we allow a global section
				// and what name to use for it internally
				iniConfig.setGlobalSection(true);
				final String iniGlobalName = "org.ieeg-global";
				iniConfig.setGlobalSectionName(iniGlobalName);
				// Ignore case in option names (but not section names).
				iniConfig.setLowerCaseOption(true);
				ini.setConfig(iniConfig);
				ini.load(inFStream);
				
				String worldPerm = ini.get(iniGlobalName, "world");
				controlFile.getSnapshotParameters()
						.getPermissionParameters()
						.setWorldPerm(worldPerm);
				
				String csChannels = ini.get(iniGlobalName, "channels");
				if (csChannels != null) {
					final Iterable<String> channelMappings = commaSplitter
							.split(csChannels);
					for (final String channelMapping : channelMappings) {
						final List<String> origToMapped = colonSplitter
								.splitToList(channelMapping);
						if (origToMapped.size() == 2) {
							final String orig = origToMapped.get(0);
							final String mapped = origToMapped.get(1);
							logger.debug(
									"{}: mapping channel label from [{}] to [{}]",
									m, orig, mapped);
							controlFile.getSnapshotParameters()
									.addChannelMapping(
											orig,
											mapped);
						} else {
							controlFile.getSnapshotParameters()
									.getChannelLabels()
									.add(origToMapped.get(0));
						}
					}

				}
				final String projectPrefix = "Project.";
				for (Entry<String, Section> entry : ini.entrySet()) {
					logger.debug("{}: Found section [{}] in {}",
							m,
							entry.getKey(),
							otherFile);
					if (entry.getKey().startsWith(projectPrefix)) {
						final String projectName = entry.getKey().substring(projectPrefix.length());
						logger.debug("{}: Found section for project [{}] in {}",
								m,
								projectName,
								otherFile);
						final List<ProjectPermissionParameters> existingProjParams = controlFile
								.getSnapshotParameters()
								.getPermissionParameters()
								.getProjectPermissionParameters();
						ProjectPermissionParameters projectParam = null;
						for (final ProjectPermissionParameters existingProjParam : existingProjParams) {
							if (projectName.equals(existingProjParam.getProjectName())) {
								projectParam = existingProjParam;
								break;
							}
						}
						if (projectParam == null) {
							projectParam = new ProjectPermissionParameters();
							existingProjParams.add(projectParam);
							projectParam.setProjectName(projectName);
						}
						final Section projectSection = entry.getValue();
						final String projectAdminsPerm = projectSection.get("admins");
						if (projectAdminsPerm != null) {
							logger.debug("{}: Found admins perm [{}] for project [{}] in {}",
									m,
									projectAdminsPerm,
									projectName,
									otherFile);
							projectParam.setProjectAdminsPerm(projectAdminsPerm);
						}
						final String projectTeamPerm = projectSection.get("team");
						if (projectTeamPerm != null) {
							logger.debug("{}: Found team perm [{}] for project [{}] in {}",
									m,
									projectTeamPerm,
									projectName,
									otherFile);
							projectParam.setProjectTeamPerm(projectTeamPerm);
						}
					}
				}
			}
		} finally {
			if (inStream != null) {
				inStream.close();
			}
		}

	}

}
