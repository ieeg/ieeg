package edu.upenn.cis.db.habitat.processing.parameters.convert;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

public class BCI2000Parameters {

	public static class Param {
		String section;
		String dataType;
		String name;
		String value;
		String defaultValue;
		String lowRange;
		String highRange;
		String comment;

		public String toString(){
			return "BCI200Param: " + name;
		}

		public String getSection() {
			return section;
		}

		public String getDataType() {
			return dataType;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public String getLowRange() {
			return lowRange;
		}

		public String getHighRange() {
			return highRange;
		}

		public String getComment() {
			return comment;
		}
	}
	
	public static class StateVec {
		String name;
		Integer vecLengt;
		String value;
		Integer byteLoc;
		Integer bitLoc;
		
		public String getName() {
			return name;
		}
		public Integer getVecLengt() {
			return vecLengt;
		}
		public String getValue() {
			return value;
		}
		public Integer getByteLoc() {
			return byteLoc;
		}
		public Integer getBitLoc() {
			return bitLoc;
		}

	}

	private IStoredObjectReference handle; 
	
	private Integer headerLength; // in Bytes
	private Integer stateVecLenght;
	private String dataFormat = "int16";

	private Integer sampleBlockSize;
	private Double samplingRate;
	private Integer nrChannels;
	private List<Double> sourceChOffset = new ArrayList<Double>();
	private List<Double> sourceChGain =new ArrayList<Double>();
	private List<Param> paramList = new ArrayList<Param>();
	private List<StateVec> stateVecList = new ArrayList<StateVec>();
	int alignChannels;


	private final Logger logger = LoggerFactory.getLogger(getClass());

	private BCI2000Parameters(){
	};

	public static BCI2000Parameters buildFromRef(IStoredObjectReference inFile) throws IOException {
		// TODO Auto-generated method stub
		
		@SuppressWarnings("unused")
		final String m = "buildFromRef(...)";

		
		
		IInputStream input = null; 
		String header = null;
		Integer headerLength;
		Integer nrChannels;
		String format = "int16";
		Integer stateVecLength;
		BCI2000Parameters obj = new BCI2000Parameters();
		obj.handle = inFile;
		try{

			IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
			input = bestServer.openForInput(inFile);
			byte[] b = new byte[64];
			ByteBuffer bb =  ByteBuffer.wrap(b);
			bb.rewind();
			bestServer.getBytes(input, bb, 64);
			String headerStartStr = new String(b);
			String[] s = headerStartStr.split("\r\n");
			String firstLn = s[0];

			//			String firstLn2 = "Version1.1 HeaderLen=  8110 SourceCh= 64 StatevectorLen= 11 DataFormat= int24";
			//			firstLn = "HeaderLen=  8110 SourceCh= 64 StatevectorLen= 11";

			Pattern p = Pattern.compile("[\\s\\S]*=\\s*(?<headerLength>\\d+)\\s*SourceCh=\\s*(?<nrChannels>\\d+)\\s*StatevectorLen=\\s*(?<stateVecLength>\\d+)\\s*(?:DataFormat=)?(?<format>[\\d\\s\\w]+)?");
			Matcher mat = p.matcher(firstLn);
			
			if (mat.matches()) {
				headerLength = Integer.parseInt(mat.group("headerLength"));
				nrChannels = Integer.parseInt(mat.group("nrChannels"));
				stateVecLength = Integer.parseInt(mat.group("stateVecLength"));

				String ff = mat.group("format");
				if (!(null==ff)){
					format = ff;
				}

				byte[] b2 = new byte[headerLength];
				ByteBuffer bb2 =  ByteBuffer.wrap(b2);
				bestServer.getBytes(input, bb2, headerLength);
				header = new String(b2);
				
				obj.headerLength = headerLength;
				obj.nrChannels = nrChannels;
				obj.stateVecLenght = stateVecLength;
				obj.dataFormat = format;
			}
			
		} catch (IOException e) {
			throw(e);
		}finally {
			try {
				input.close();
			} catch (IOException e) {}

		}

		

		String[] headerLines = header.split("\r\n");
		boolean inParamDef = false;
		boolean inStateDef = false;
		
		for (String ln : headerLines){

			if (ln.trim().equals("[ Parameter Definition ]")){
				inParamDef = true;
				inStateDef = false;
				continue;
			}else if (ln.trim().equals("[ State Vector Definition ]")){
				inParamDef = false;
				inStateDef = true;
				continue;
			}

			if (inParamDef){
				String ptrn = "(?<section>[\\W\\w\\d]+)\\s+" +
						"(?<format>[\\w\\d]+)\\s+" +
						"(?<name>\\w+)=\\s+" +
						"(?<value>-?[\\w\\d\\.]+)\\s*" +
						"(?<defaultValue>[\\w\\d]+)?\\s*" +
						"(?<lowRange>[\\w\\d]+)?\\s*" +
						"(?<highRange>[\\w\\d]+)?\\s*" +
						"(?<comment>[\\W\\D\\w\\d\\s]+)?";
				Pattern p = Pattern.compile(ptrn);
				Matcher mat = p.matcher(ln.trim());

				if(mat.matches()){
					int count = mat.groupCount();
					Param pLine = new Param();
					if(count > 1){
						pLine.section = mat.group("section");
						pLine.dataType = mat.group("format");
						pLine.name = mat.group("name");

						switch (pLine.name){
						case "SourceChOffset": case "SourceChGain":
							String[] line = ln.trim().split(" ");
							int nrCh = Integer.parseInt(line[3]);

							int i=0;
							for (i=0; i<nrCh; i++){
								pLine.value += " " + line[i+4];
								if(pLine.name.equals("SourceChGain")){
									obj.sourceChGain.add(Double.parseDouble(line[i+4]));
								}else {
									obj.sourceChOffset.add(Double.parseDouble(line[i+4]));
								}
								
							}
							pLine.value = pLine.value.substring(1);

							pLine.defaultValue = line[i+4];
							pLine.lowRange = line[i+4+1];
							pLine.highRange = line[i+4+2];
							pLine.comment = line[i+4+3];
							for(int j =(i+4+4);j<line.length;j++){
								pLine.comment += " " + line[j];
							}


							break;
						default: 
							pLine.value = mat.group("value");
							if (count>4)
								pLine.defaultValue = mat.group("defaultValue");
							if (count >5)
								pLine.lowRange = mat.group("lowRange");
							if (count >6) 
								pLine.highRange = mat.group("highRange");
							if (count >7)
								pLine.comment = mat.group("comment");

						}

					}
					obj.paramList.add(pLine);				

					switch (pLine.name){
					case "SampleBlockSize":
						obj.sampleBlockSize = Integer.parseInt(pLine.value);
						break;
					case "SamplingRate":
						obj.samplingRate = Double.parseDouble(pLine.value);
						break;
					case "SourceCh":
						obj.nrChannels = Integer.parseInt(pLine.value);
						break;
					}

				}

			} else if(inStateDef){
				String ptrn = "(?<name>[\\W\\w\\d]+)\\s+" +
						"(?<length>[\\d\\.]+)\\s+" +
						"(?<value>[\\w\\d\\.]+)\\s+" +
						"(?<byteLoc>[\\d]+)\\s+" +
						"(?<bitLoc>[\\d]+)\\s*";
				
				Pattern p = Pattern.compile(ptrn);
				Matcher mat = p.matcher(ln.trim());

				if(mat.matches()){
					StateVec vec = new StateVec();
					
					vec.name = mat.group("name");
					vec.vecLengt = Integer.parseInt(mat.group("length"));
					vec.value = mat.group("value");
					vec.byteLoc = Integer.parseInt(mat.group("byteLoc"));
					vec.bitLoc = Integer.parseInt(mat.group("bitLoc"));
					
					obj.stateVecList.add(vec);
					
				}

			}

		}




		return obj;
	}

	public Integer getHeaderLength() {
		return headerLength;
	}

	public Integer getStateVecLenght() {
		return stateVecLenght;
	}

	public String getDataFormat() {
		return dataFormat;
	}

	public Integer getSampleBlockSize() {
		return sampleBlockSize;
	}

	public Double getSamplingRate() {
		return samplingRate;
	}

	public Integer getNrChannels() {
		return nrChannels;
	}

	public List<Double> getSourceChOffset() {
		return sourceChOffset;
	}

	public List<Double> getSourceChGain() {
		return sourceChGain;
	}

	public List<Param> getParamList() {
		return paramList;
	}

	public int getAlignChannels() {
		return alignChannels;
	}

	public Logger getLogger() {
		return logger;
	}

	public IStoredObjectReference getHandle() {
		return handle;
	}

	public List<StateVec> getStateVecList() {
		return stateVecList;
	}

}



