package edu.upenn.cis.db.habitat.processing.actions.convert;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.annotations.VisibleForTesting;

import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.CreateSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.habitat.processing.parameters.EDF.EDFParameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.SimpleChannel;
import edu.upenn.cis.eeg.edf.EDFHeader;
import edu.upenn.cis.eeg.edf.EDFHeader.Builder;

public class ImportEDFFilesAction extends BaseProcessingAction {

	public static final String NAME = "ImportEDFFilesAction";
	// WorkItemSet workItems = new WorkItemSet("Work item set",
	// null,getOriginatingItemSet().getRequest(),
	// getOriginatingItemSet().getOrigin());
	RemoteHabitatSession session;
	ControlFileParameters parameters;
	String snapshotName;
	private Set<String> includedChannelLabels = new TreeSet<>(
			String.CASE_INSENSITIVE_ORDER);

	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();
	List<BufferedInputStream> iStreams = new ArrayList<BufferedInputStream>();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public ImportEDFFilesAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			ControlFileParameters parameters) {
		super(mQueue, origin);
		final String m = "ImportEDFFilesAction(WorkItemSet, IeegSession, ControlFileParameters)";
		this.session = session;
		this.parameters = parameters;
		if (logger.isTraceEnabled()) {
			try {
				final String paramsString = ObjectMapperFactory
						.newPasswordFilteredWriter()
						.writeValueAsString(this.parameters);
				logger.trace("{}: Creating Action with parameters {}", m,
						paramsString);
			} catch (JsonProcessingException e) {
				logger.error(m + ": Exception in debug log", e);
			}
		}
	}

	public ImportEDFFilesAction() {
		super(null, null);
	}

	/*
	 * Create ChannelInfo for each MefFile
	 */

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		if (arguments != null && parameters != null
				&& arguments instanceof Parameters)
			parameters.merge((Parameters) arguments);
		else if (arguments != null)
			parameters = (ControlFileParameters) arguments;

		includedChannelLabels.addAll(parameters.getSnapshotParameters()
				.getChannelLabels());

		List<EDFParameters> myEDFParams = new ArrayList<EDFParameters>();

		try {
			for (JsonTyped inFileSpec : parameters.getInputs()) {
				logger.debug("{}: ReadEdfFile: {}", m, inFileSpec.toString());

				FileReference inFile = (FileReference) inFileSpec;

				myEDFParams.add(new EDFParameters(inFile));
			}
			String institution = parameters.getSnapshotParameters()
					.getInstitution();
			logger.debug("{}: Assign institution: {}", m, institution);

			String owner = parameters.getSnapshotParameters()
					.getOwner();
			logger.debug("{}: Assign owner: {}", m, owner);

			snapshotName = parameters.getTargetSnapshot();
			logger.debug("{}: Using snapshot name: {}", m, snapshotName);

			boolean isHuman = parameters.getSnapshotParameters().isHuman();
			logger.debug("{}: Setting isHuman to {}", m, isHuman);

			processSnapshot(myEDFParams, isHuman, institution, owner);

			return STATE.SUCCEEDED;
		} catch (RuntimeException | Error | IOException e) {
			// Cleanup and report error
			for (EDFParameters params : myEDFParams) {
				try {
					params.close();
				} catch (IOException e2) {
					logger.info(
							m
									+ ": This exception came from close attempt and is being swallowed. Original exception will be logged separately.",
							e2);
				}
			}
			logger.error(m + ": Exception. Returning STATE.FAILED", e);
			getMessageQueue().logError(getCreator(),
					WorkerRequest.PORTAL, WorkerRequest.ETL,
					getWorkItemSet(),
					"Error importing file: " + e.getMessage());
			return STATE.FAILED;
		}

	}

	private void processSnapshot(List<EDFParameters> parmsList,
			boolean isHuman, String inst, String owner) {
		final String m = "processSnapshot(...)";
		if (parmsList.isEmpty())
			return;

		List<String> channelIds = new ArrayList<String>();
		SnapshotParameters ssp = new SnapshotParameters("",
				null, SnapType.other, channelIds);

		ssp.merge(parameters);

		ssp.setInstitution(inst);
		ssp.setOwner(owner);

		ssp.setHuman(isHuman);

		// Get Unique channel names
		Collection<SimpleChannel> chInfo = findUniqueChannels(parmsList);

		// Populate channelIds in SnapshotParameters
		for (SimpleChannel c : chInfo) {
			channelIds.add(c.getChannelName());
		}
		ssp.setChannelMapping(parameters.getSnapshotParameters()
				.getChannelMapping());

		ssp.setTargetSnapshot(snapshotName);

		// Create snapshot as necessary
		IProcessingAction snap = new CreateSnapshotAction(
				getMessageQueue(),
				this.getWorkItemSet(),
				session,
				ssp);
		getNextActions().add(snap);

		// Map metadata to it as necessary
		IProcessingAction map = new MapMetadataAction(
				getMessageQueue(),
				this.getWorkItemSet(),
				session, ssp);
		snap.getNextActions().add(map);

		// Add Transcode action
		TranscodeFromEDFAction transcode = new TranscodeFromEDFAction(
				getMessageQueue(),
				this.getWorkItemSet(),
				session, ssp, chInfo, parmsList);

		map.getNextActions().add(transcode);

	}

	@VisibleForTesting
	Collection<SimpleChannel> findUniqueChannels(List<EDFParameters> params) {
		final String m = "findUniqueChannels(...)";
		final Map<String, SimpleChannel> labelToSimpleChannel = new HashMap<>();

		for (EDFParameters curParam : params) {
			logger.debug("{}: Check for unique channels in Header: {}", m,
					curParam);

			EDFHeader curVal = curParam.getHeader();
			// First make sure channel names in file are unique
			final Set<String> labelsInFile = new HashSet<>();
			for (int i = 0; i < curVal.channelLabels.length; i++) {
				final String originalLabel = curVal.channelLabels[i];
				// Skip ignored channels
				if (ignoreChannel(originalLabel)) {
					continue;
				}
				final String uniqueInFileLabel = uniquifyChannelLabel(
						labelsInFile, originalLabel);
				labelsInFile.add(uniqueInFileLabel);
				if (!uniqueInFileLabel.equals(originalLabel)) {
					logger.debug(
							"{}: Found channel with same label in file: [{}]. Changing label [{}] to [{}]",
							m,
							curParam.getHandle(),
							originalLabel,
							uniqueInFileLabel);
					final EDFHeader newHeader = modifyHeader(
							curVal,
							i,
							originalLabel,
							uniqueInFileLabel);
					curVal = newHeader;
					curParam.setHeader(newHeader);
					final String mappedLabel = parameters
							.getSnapshotParameters().getMappedChannel(
									originalLabel);
					if (!mappedLabel.equals(originalLabel)) {
						final String uniqueInMappingLabel = uniquifyChannelLabel(
								parameters.getSnapshotParameters()
										.getChannelMapping().values(),
								mappedLabel);
						parameters.getSnapshotParameters().addChannelMapping(
								uniqueInFileLabel, uniqueInMappingLabel);
					}
				}
			}

			for (int i = 0; i < curVal.channelLabels.length; i++) {
				final String currentLabel = curVal.channelLabels[i];
				// Skip ignored channels
				if (ignoreChannel(currentLabel)) {
					continue;
				}
				double curSf = curVal.numberOfSamples[i]
						/ curVal.durationOfRecords;

				// Get Scale based on Units.
				double baseScale = 1;
				String curScale = curVal.dimensions[i].toLowerCase().trim();
				switch (curScale) {
					case "v":
					case "volt":
					case "volts":
						baseScale = 0.000001;
						break;
					case "mv":
					case "millivolt":
					case "millivolts":
						baseScale = 0.001;
						break;
					case "uv":
					case "microvolt":
					case "microvolts":
						baseScale = 1;
						break;
					default:
						logger.debug(
								"{}: Unknown EDF Channel Units, assuming microVolts: [{}]",
								m, curVal);
						// Do not calculate offset or conv
						baseScale = -1;

						break;
				}

				double uVConv = 1;
				double offset = 0;
				if (baseScale >= 0) {
					// Get ConversionFactor
					double conv = ((curVal.maxInUnits[i] - curVal.minInUnits[i]) / (curVal.digitalMax[i] - curVal.digitalMin[i]));
					uVConv = baseScale * conv;

					offset = (curVal.minInUnits[i] / conv)
							- curVal.digitalMin[i];
				}

				final String origLabel = currentLabel.trim();
				final SimpleChannel newChannel = new SimpleChannel(
						origLabel, curSf, uVConv, offset);
				final SimpleChannel existingChannel = labelToSimpleChannel
						.get(origLabel);
				if (existingChannel == null) {
					labelToSimpleChannel.put(origLabel, newChannel);
					logger.debug("{}: Adding Channel: {}", m,
							newChannel);
				} else if (existingChannel.conflictsWith(newChannel)) {
					throw new IllegalStateException(
							String.format(
									"new channel: [%s] in file %s conflicts with existing channel: [%s]",
									newChannel,
									curParam.getHandle(),
									existingChannel));
				}

			}
		}
		// Client doesn't really care about order.
		return labelToSimpleChannel.values();

	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new ImportEDFFilesAction(mQueue, origin, session,
				(ControlFileParameters) arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}

	private EDFHeader modifyHeader(
			EDFHeader curVal,
			int channelIdx,
			final String originalChannelLabel,
			final String newChannelLabel) {
		Builder altHeader = new EDFHeader.Builder(
				curVal);
		altHeader.editChannelName(channelIdx, newChannelLabel);
		EDFHeader newHeader = altHeader.build();
		return newHeader;
	}

	/**
	 * Returns the first of {@code labelToUniquify},
	 * {@code labelToUniquify + "_1"}, {@code labelToUniquify + "_2"}, ... not
	 * contained in {@code channelLabels}
	 * 
	 * @param channelLabels
	 * @param labelToUniquify
	 * @return {@code labelToUniquify} if it is not contained in
	 *         {@code channelLablels}, otherwise first of
	 *         {@code labelToUniquify + "_n"} not contained in
	 *         {@code channelLablels}
	 */
	private String uniquifyChannelLabel(
			final Collection<String> channelLabels,
			final String labelToUniquify) {
		String newLabel = labelToUniquify;
		int tries = 1;
		while (channelLabels.contains(newLabel)) {
			newLabel = labelToUniquify + "_" + tries;
			tries++;
		}
		return newLabel;
	}

	private boolean ignoreChannel(String channelLabel) {
		boolean ignoreChannel = channelLabel.equals("EDF Annotations")
				|| (!includedChannelLabels.isEmpty() && !includedChannelLabels
						.contains(channelLabel));
		return ignoreChannel;
	}

}
