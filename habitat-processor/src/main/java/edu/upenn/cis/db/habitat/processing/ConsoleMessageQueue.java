package edu.upenn.cis.db.habitat.processing;

import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class ConsoleMessageQueue implements IMessageQueue {

	@Override
	public void logEvent(IUser user, String channel, String source, WorkItemSet set, String event) {
		System.out.println("For " + user.getUsername() + ": " + event);
	}

	@Override
	public void logAlarm(IUser user, String channel, String source, WorkItemSet set, String alarm) {
		System.err.println("For " + user.getUsername() + ": " + alarm);
	}

	@Override
	public void logError(IUser user, String channel, String source, WorkItemSet set, String error) {
		System.err.println("For " + user.getUsername() + ": " + error);
	}

}
