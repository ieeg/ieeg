/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class PermissionsParameters implements JsonTyped {

	List<ProjectPermissionParameters> projectPermParams = new ArrayList<>();
	String worldPerm = null;

	public List<ProjectPermissionParameters> getProjectPermissionParameters() {
		return projectPermParams;
	}

	public void setProjectPermissionParamters(
			List<ProjectPermissionParameters> projectPermParameters) {
		this.projectPermParams = projectPermParameters;
	}

	public String getWorldPerm() {
		return worldPerm;
	}

	public void setWorldPerm(@Nullable String worldPerm) {
		if (worldPerm == null
				|| worldPerm
						.equalsIgnoreCase(CorePermDefs.READ_PERMISSION_NAME)
				|| worldPerm
						.equalsIgnoreCase(CorePermDefs.EDIT_PERMISSION_NAME)) {
			this.worldPerm = worldPerm;
		} else {
			throw new IllegalArgumentException(
					"Cannot set world permssion to [" + worldPerm + "]");
		}
	}

}
