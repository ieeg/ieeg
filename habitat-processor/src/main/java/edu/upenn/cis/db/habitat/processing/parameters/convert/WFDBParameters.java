package edu.upenn.cis.db.habitat.processing.parameters.convert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.eeg.SimpleChannel;

public class WFDBParameters {
	
	public static class WFDBSignal {
		String fileName;
		Integer dataFormatID;
		Integer samplesPerFrame = null;
		Integer skew = null;
		Integer byteOffset = null;
		Double ADCGain = (double) 200;
		Integer ADCBaseline = 0;
		String units = null;
		Integer ADCResolution = null;
		Integer ADCZero = null;
		Integer initValue = null;
		Integer checksum = null;
		Integer blockSize = null;
		String description = null;
		
		WFDBSignal(){
			
		}
		public String getFileName() {
			return fileName;
		}

		public Integer getDataFormatID() {
			return dataFormatID;
		}

		public Integer getSamplesPerFrame() {
			return samplesPerFrame;
		}

		public Integer getSkew() {
			return skew;
		}

		public Integer getByteOffset() {
			return byteOffset;
		}

		public Double getADCGain() {
			return ADCGain;
		}

		public Integer getADCBaseline() {
			return ADCBaseline;
		}

		public String getUnits() {
			return units;
		}

		public Integer getADCResolution() {
			return ADCResolution;
		}

		public Integer getADCZero() {
			return ADCZero;
		}

		public Integer getInitValue() {
			return initValue;
		}

		public Integer getChecksum() {
			return checksum;
		}

		public Integer getBlockSize() {
			return blockSize;
		}

		public String getDescription() {
			return description;
		}

		

	}

	private IStoredObjectReference handle; // handle to the .HEA file
	
	private String name;
	private Integer nrSegments = null;
	private Integer nrSignals;
	private Double samplingFreq = (double) 250;
	private Double counterFreq = null;
	private Integer baseCounterValue = null;
	private Integer nrSamplesPerSignal = null;
	private String baseTime = null;
	private String baseDate = null;
	private List<WFDBSignal> signals = new ArrayList<WFDBSignal>();
	private List<String> info = new ArrayList<String>();
	
	private WFDBParameters(){
		
	}
	
	
	public IStoredObjectReference getHandle() {
		return handle;
	}
	
	public String getName() {
		return name;
	}


	public Integer getNrSegments() {
		return nrSegments;
	}


	public Integer getNrSignals() {
		return nrSignals;
	}


	public Double getSamplingFreq() {
		return samplingFreq;
	}


	public Double getCounterFreq() {
		return counterFreq;
	}


	public Integer getBaseCounterValue() {
		return baseCounterValue;
	}


	public Integer getNrSamplesPerSignal() {
		return nrSamplesPerSignal;
	}


	public String getBaseTime() {
		return baseTime;
	}


	public String getBaseDate() {
		return baseDate;
	}

    /*
     * Return Start-date of BNI1 file in MicroUTC. (usec from jan 1, 1970) 
     */
    public long Date2uUTC() throws ParseException {   
      SimpleDateFormat df = new SimpleDateFormat("MM.dd.yyyy.HH:mm:ss Z");   
      
      Date startD = new Date();
      if (baseDate!=null && baseTime!=null){
    	  startD = df.parse(baseDate + "." + baseTime + " -0000");
      } 
      
      long retTime = startD.getTime() * 1000;
      return retTime;
    }
	
    public List<String> getChannelLabels(){
    	// Get Unique channel names
        List<String> labels = new ArrayList<String>();

        // Populate channelIds in SnapshotParameters
        for (WFDBSignal s : this.signals) {
            labels.add(s.description);
        }
    	
    	return labels;
    }
    
    public static WFDBParameters buildFromRef(IStoredObjectReference inFile) throws IOException{
		
		IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
		IInputStream input = bestServer.openForInput(inFile);
		BufferedReader reader = new BufferedReader(new InputStreamReader(input.getInputStream()));
		
		
		
		// Ignore any Commented or empty lines
		String curLine = null;
		while (curLine==null || curLine.isEmpty() || curLine.startsWith("#"))
			curLine = reader.readLine().trim();
		
		
		String[] comps = curLine.split(" ");
		
		WFDBParameters obj = new WFDBParameters();
		
		obj.handle = inFile;
		
		// LINE 1 ----> RECORD LINE
		String[] comp1 = comps[0].split("/");
		obj.name = comp1[0];
		if (comp1.length > 1)
			obj.nrSegments = Integer.parseInt(comp1[1]);
		
		obj.nrSignals = Integer.parseInt(comps[1]);
		
		if (comps.length > 2){
			String[] comp2 = comps[2].split("/");
			obj.samplingFreq = Double.parseDouble(comp2[0]);
			if(comp2.length > 1){
				String[] comp21 = comp2[1].split("\\p{Punct}");
				obj.counterFreq = Double.parseDouble(comp21[0]);
				if (comp21.length >1){
					obj.baseCounterValue = Integer.parseInt(comp21[1]);
				}	
			}
			
			if (comps.length > 3){
				obj.nrSamplesPerSignal = Integer.parseInt(comps[3]);
				if (comps.length > 4){
					obj.baseTime = comps[4];
					if (comps.length > 5)
						obj.baseDate = comps[5];
				}
					
			}

		} 
		
		// SIGNAL LINES
		int curSignal = 1;
		while (curSignal <= obj.nrSignals){
			curLine = null;
			while (curLine==null || curLine.isEmpty() || curLine.startsWith("#"))
				curLine = reader.readLine().trim();

			
			WFDBSignal curSigObj = new WFDBSignal();
			comps = curLine.split(" ");
			
			curSigObj.fileName = comps[0];
			
			comp1 = comps[1].split("x");
			curSigObj.dataFormatID = Integer.parseInt(comp1[0]);
			
			if (comp1.length>1){
				String[] comps11 = comp1[1].split(":");
				curSigObj.samplesPerFrame = Integer.parseInt(comps11[0]);
				if (comps11.length > 1){
					String[] comps111 = comps11[1].split("\\p{Punct}");
					curSigObj.skew = Integer.parseInt(comps111[0]);
					if (comps111.length > 1)
						curSigObj.byteOffset = Integer.parseInt(comps111[1]);
				}	
			}
			
			if (comps.length > 2){
				String[] comp2 = comps[2].split("[()]");
				String[] comp3 = comps[2].split("[///]");
				
				if (comp2.length > 1){
					curSigObj.ADCGain = Double.parseDouble(comp2[0]);
				}else{
					curSigObj.ADCGain = Double.parseDouble(comp3[0]);
				}

				if(comp2.length>1){
					curSigObj.ADCBaseline = Integer.parseInt(comp2[1]);
					if (comp3.length > 1)
						curSigObj.units = comp3[1].trim();
				}else if(comp3.length>1){
					curSigObj.units = comp3[1].trim();
				}
				
				if (comps.length > 3){
					curSigObj.ADCResolution = Integer.parseInt(comps[3]);
					
					if (comps.length > 4){
						curSigObj.ADCZero = Integer.parseInt(comps[4]);
						
						if (comps.length > 5) {
							curSigObj.initValue = Integer.parseInt(comps[5]);
							
							if (comps.length > 6){
								curSigObj.checksum = Integer.parseInt(comps[6]);
								
								if (comps.length > 7){
									curSigObj.blockSize = Integer.parseInt(comps[7]);
									
									if (comps.length > 8){
										curSigObj.description = comps[8];
										for (int i = 9; i< comps.length;i++)
											curSigObj.description += " " + comps[i];
									}
									
								}
							}
						
						}
					}
				}
			}
			
			obj.signals.add(curSigObj);
			
			curSignal++;
			
			
		}
		
		// CAPTURE INFO LINES
		curLine = reader.readLine();
		while (curLine!=null){
			curLine = curLine.trim();
			if(curLine.startsWith("#"))
				obj.info.add(curLine.substring(1));
			
			curLine = reader.readLine();
		}
		
		
		input.close();
		
		return obj;
		
	}


	public List<WFDBSignal> getSignals() {
		return signals;
	}


	public List<String> getInfo() {
		return info;
	}
	
	

}
