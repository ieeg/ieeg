/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.processing.AnnotationNotifierFactory;
import edu.upenn.cis.db.habitat.processing.IAnnotationNotifier;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.AnnotatorConfig;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.AnnotateParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.server.SessionCacheEntry;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.DisplayConfiguration;
import edu.upenn.cis.db.mefview.shared.IDetection;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm;
import edu.upenn.cis.eeg.processing.ISignalProcessingAlgorithm.AnnotatedTimeSeries;
import edu.upenn.cis.eeg.processing.SignalProcessingFactory;

/**
 * Annotate a set of time series
 * 
 * @author zives
 *
 */
public class AnnotateAction extends BaseSnapshotProcessingAction {
	static Map<String, Set<String>> badChannels = new HashMap<String, Set<String>>();

	// / Blacklist of snapshot / channel
	static {
		HashSet<String> channelLabel = new HashSet<String>();

		badChannels.put("3cf5e56c-8320-11e0-8800-0015c5e207d6", channelLabel);
		channelLabel.add("LTD2");
	}

	// Find your Account Sid and Token at twilio.com/user/account
	// public static final String ACCOUNT_SID =
	// "AC7c91c0b2978b0578a9ccc122a3d52a96";
	// public static final String AUTH_TOKEN =
	// "9e7c9d69c61f535d5bd4c6a856cbb492";

	public static final String NAME = "AnnotateAction";

	/**
	 * Just used for the seed instance
	 */
	public AnnotateAction() {
		super();
	}

	public AnnotateAction(IMessageQueue mQueue, WorkItemSet item,
			RemoteHabitatSession session, AnnotateParameters parms) {
		super(mQueue, item, session, parms);
	}

	/**
	 * Is this channel one we should ignore?
	 * 
	 * @param datasetID
	 * @param channelName
	 * @return
	 */
	public boolean isBlackListed(String datasetID, String channelName) {
		// Skip any channel on the blacklist
		if (badChannels.containsKey(datasetID)) {
			if (badChannels.get(datasetID).contains(channelName))
				return true;
		}
		return false;
	}

	/**
	 * Run an annotator
	 * 
	 * @param user
	 * @param token
	 * @param datasetID
	 * @param threshold
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws ServerTimeoutException
	 */
	public List<IDetection> getAnnotationsForSnapshot(String datasetID,
			int threshold) throws IllegalArgumentException, IOException,
			ServerTimeoutException {
		IUser creator = getCreator();
		if (!(creator instanceof User)) {
			throw new IllegalStateException(
					"The creator must have type "
							+ User.class.getCanonicalName()
							+ ". It has type "
							+ creator.getClass().getCanonicalName());
		}
		DataSnapshot snap = session.getSnapshotFor((User) creator, datasetID);
		// Need dataset and channels
		AnnotatorConfig ac = new AnnotatorConfig();

		List<String> channelLabels = new ArrayList<String>();
		List<String> channelUuids = new ArrayList<String>();
		Iterator<TimeSeriesDto> it = snap.getTimeSeries().iterator();
		List<DisplayConfiguration> filters = new ArrayList<DisplayConfiguration>();
		List<FilterSpec> filterSpec = new ArrayList<FilterSpec>();
		while (it.hasNext()) {
			TimeSeriesDto series = it.next();

			// Skip if there's a specified set of inputs
			if (getCurrentParameters().getInputs() != null &&
					getCurrentParameters().getInputs().size() > 0) {
				// TODO fix this incompatible type bug when more is known about
				// type of inputs.
				if (!getCurrentParameters().getInputs().contains(
						series.getLabel()))
					continue;
			}

			channelUuids.add(series.getId());
			channelLabels.add(series.getLabel());
			filters.add(new DisplayConfiguration());
			filterSpec.add(new FilterSpec());
			System.out.println(series.getLabel() + "/ "
					+ channelUuids.get(channelUuids.size() - 1));
		}
		ac.setChannelsToAnnotate(channelUuids);

		List<ChannelSpecifier> channelSpecs = session.getChannels(
				(User) creator, datasetID, ac.getChannelsToAnnotate(),
				1.e6 / 200);

		double frequency = 200;
		double duration = 600 * 1e6;
		double overlap = 10 * 1e6;

		double dur = session.getDuration(channelSpecs.get(0));

		// SignalProcessingStep step = new SignalProcessingStep("LL_echauz",
		// new LineLengthEchauzParameters(10, 100, 400, 25));

		SignalProcessingStep step = getCurrentParameters().getStep();

		// "{\"threshold\": "+ threshold +
		// ", \"samplingFrequency\": 100, \"windowSize\": 400, \"stepSize\": 25 }");
		System.err.println("Processing via " + step.getStepName());
		ISignalProcessingAlgorithm algo = SignalProcessingFactory
				.getAlgorithm(step);

		double start = 0;
		List<IDetection> annList = new ArrayList<IDetection>();
		SessionCacheEntry se = new SessionCacheEntry(channelUuids,
				channelSpecs, filters, frequency, duration);

		while (start < dur) {
			List<List<IDetection>> annotations = new ArrayList<List<IDetection>>();
			List<List<TimeSeriesData>> data = session.getTimeSeriesSet(
					(User) creator, datasetID, channelSpecs,
					start, duration, frequency, filterSpec, se, step);

			// System.out.println("Read " + data.size() + " data items");
			System.out.print(start + " on way to " + (dur) + " ");
			annotations.clear();
			int inx = 0;
			for (List<TimeSeriesData> channel : data) {
				annotations.add(new ArrayList<IDetection>());
				double startTime = Double.MAX_VALUE;
				int size = 0;

				if (isBlackListed(datasetID, channelLabels.get(inx)))
					continue;

				System.out.print(channelLabels.get(inx) + " ");

				for (TimeSeriesData segment : channel) {
					if (startTime > segment.getStartTime())
						startTime = segment.getStartTime();
					size += segment.getSeriesLength();
				}
				TimeSeriesData totalChannel = new TimeSeriesData(
						(long) startTime, (long) 1.e6 / frequency, 1.0, size);
				int startInx = 0;
				for (TimeSeriesData segment : channel) {
					for (int i = 0; i < segment.getSeriesLength(); i++)
						totalChannel.addSample(segment.getSeries()[i]);

					for (int i = 0; i < segment.getGapStart().size(); i++) {
						totalChannel.getGapStart().add(
								segment.getGapStart().get(i) + startInx);
						totalChannel.getGapEnd().add(
								segment.getGapEnd().get(i) + startInx);
					}

					startInx += segment.getSeriesLength();
				}
				totalChannel.setChannelName(channelLabels.get(inx));
				totalChannel.setRevId(channelUuids.get(inx));
				List<AnnotatedTimeSeries> annotList =
						algo.process(channelLabels.get(inx),
								channelUuids.get(inx),
								start, 1.e6 / frequency,
								totalChannel);

				for (AnnotatedTimeSeries annot : annotList) {
					if (annot.getAnnotations().size() > 0)
						System.out.println("Added "
								+ annot.getAnnotations().size()
								+ " annotations");

					// TODO: merge annotations with the previous ones if they
					// overlap
					annList.addAll(annot.getAnnotations());
				}
				inx++;
			}
			System.out.println();
			start += duration - overlap;
		}

		mergeAndWriteAnnotations(annList);

		return annList;
	}

	public void mergeAndWriteAnnotations(List<IDetection> annList) {
		Collections.sort(annList, new Comparator<IDetection>() {

			@Override
			public int compare(IDetection o1, IDetection o2) {
				return new Double(o1.getStart()).compareTo(o2.getStart());
			}
		});
		for (int inx = annList.size() - 1; inx >= 0; inx--) {
			if (inx > 0) {
				Annotation prev = (Annotation) annList.get(inx - 1);
				Annotation cur = (Annotation) annList.get(inx);

				if (prev.getStart() == cur.getStart()
						&& prev.getEnd() == cur.getEnd()) {
					prev.getChannelInfo().addAll(cur.getChannelInfo());
					prev.getChannels().addAll(cur.getChannels());
					annList.remove(inx);
				}
			}
		}
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
		List<String> datasetIDs = new ArrayList<String>();
		try {
			IUser creator = getCreator();
			if (!(creator instanceof User)) {
				throw new IllegalStateException(
						"The creator must have type "
								+ User.class.getCanonicalName()
								+ ". It has type "
								+ creator.getClass().getCanonicalName());
			}
			this.setParameters((ParametersForSnapshotContent) getWorkItemSet()
					.getJson());

			String snapID = "";

			if (parameters.getTargetSnapshotId().isEmpty())
				snapID = session.getSnapshotIdFor((User) creator,
						this.parameters.getTargetSnapshot());
			else
				snapID = parameters.getTargetSnapshotId();
			datasetIDs.add(snapID);

			AnnotateParameters annP = new AnnotateParameters();
			this.setParameters(annP);

			for (String datasetID : datasetIDs) {
				System.out.println("Handling dataset " + datasetID);

				List<IDetection> detections = getAnnotationsForSnapshot(
						datasetID,
						getCurrentParameters().threshold);

				List<Annotation> annList = new ArrayList<Annotation>();
				for (IDetection det : detections)
					annList.add((Annotation) det);
				List<String> channelUuids = new ArrayList<String>();

				System.out.println("Grand total of " + annList.size()
						+ " annotations");

				DataSnapshot snap = session.getSnapshotFor((User) creator,
						datasetID);

				Iterator<TimeSeriesDto> it = snap.getTimeSeries().iterator();
				while (it.hasNext()) {
					TimeSeriesDto series = it.next();
					channelUuids.add(series.getId());
				}
				String newDS = session.createNewSnapshot((User) creator,
						channelUuids, snap);
				session.saveAnnotations((User) creator, newDS, annList);

				this.getWorkItemSet().getNewSnapshots().add(newDS);

				derived.add(new DerivedItem("snapshot", newDS));

				System.out.println("Saved annotations to database");
				if (!annList.isEmpty())
					getMessageQueue().logAlarm(getCreator(),
							WorkerRequest.PORTAL, WorkerRequest.ETL,
							getWorkItemSet(), "Annotated " + snap.getLabel() +
									" with " + annList.size() + " annotations");
				else
					getMessageQueue().logEvent(getCreator(),
							WorkerRequest.PORTAL, WorkerRequest.ETL,
							getWorkItemSet(),
							"No annotations for " + snap.getLabel());

				if (!annList.isEmpty())
					for (IAnnotationNotifier notifier : AnnotationNotifierFactory
							.getNotifiers())
						notifier.notifyAnnotations(
								"Annotated " + snap.getLabel(), newDS, annList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return STATE.FAILED;
		}

		// IProcessingAction notify = new
		// SendCompleteAction(this.getOriginatingItemSet(), session,
		// getSnapshotParameters());
		// getNextActions().add(notify);
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session, Parameters arguments) {
		return new AnnotateAction(mQueue, origin, session,
				(AnnotateParameters) arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new AnnotateParameters();
	}

	@Override
	public AnnotateParameters getCurrentParameters() {
		return (AnnotateParameters) super.getCurrentParameters();
	}

}
