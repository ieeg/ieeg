/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.collect.Lists.newArrayList;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.ExtPermission;
import edu.upenn.cis.braintrust.security.ExtProjectAce;
import edu.upenn.cis.braintrust.security.ProjectGroup;
import edu.upenn.cis.braintrust.security.ProjectGroupType;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.NewProjectAce;
import edu.upenn.cis.braintrust.shared.UpdateProjectAce;
import edu.upenn.cis.braintrust.shared.UpdateWorldAce;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ProjectPermissionParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * TODO: Given a snapshot, grant permissions to it
 * 
 * @author zives
 *
 */
public class GrantPermissionsAction extends BaseSnapshotProcessingAction {
	public static final String NAME = "GrantPermissionsAction";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public GrantPermissionsAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			SnapshotParameters parameters) {
		super(mQueue, origin, session, parameters);
	}

	GrantPermissionsAction() {
		super();
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		try {
			final String datasetName = getCurrentParameters()
					.getTargetSnapshot();
			final String datasetId = getCurrentParameters()
					.getTargetSnapshotId();

			logger.debug(
					"{}: dataset: [{}], dataset id: [{}]",
					m,
					datasetName,
					datasetId);
			final User creator = (User) getCreator();
			final List<IEditAclAction<?>> editAclActions = new ArrayList<>();
			final List<ProjectPermissionParameters> projectPermParams = determineProjectInfo(
					creator,
					datasetName,
					datasetId);
			final Optional<String> worldPerm = determineWorldPerm(
					creator,
					datasetName,
					datasetId);
			if (!projectPermParams.isEmpty() || worldPerm.isPresent()) {
				final GetAcesResponse acesResponse = session
						.getCoreDatasetPerms(
								creator,
								datasetId);
				final Map<String, Integer> projectIdsToEditActionCount = new HashMap<>();
				if (!projectPermParams.isEmpty()) {
					for (final ProjectPermissionParameters projParam : projectPermParams) {
						final List<IEditAclAction<?>> projectEditAclActions = determineProjectEditAclActions(
								acesResponse,
								datasetId,
								projParam);
						editAclActions
								.addAll(projectEditAclActions);
						projectIdsToEditActionCount.put(
								projParam.getProjectId(),
								projectEditAclActions.size());
					}
				}
				if (worldPerm.isPresent()) {
					editAclActions.add(determineWorldEditAclAction(
							acesResponse,
							datasetId,
							worldPerm.get()));
				}
				final List<EditAclResponse> responses = session
						.modifyCoreDatasetPerms(
								creator,
								editAclActions);
				if (!projectPermParams.isEmpty()) {
					// Add dataset to project if there were project ace actions
					// and they all succeeded.

					int responseIdx = 0;
					for (final ProjectPermissionParameters projParam : projectPermParams) {
						final String projectId = projParam.getProjectId();
						int successfulProjectAceActionCount = 0;
						int projectAclEditCount = projectIdsToEditActionCount
								.get(projectId);
						for (int i = 0; i < projectAclEditCount; i++) {
							EditAclResponse response = responses
									.get(responseIdx++);
							if (response.isSuccess()) {
								successfulProjectAceActionCount++;
							}
						}
						if (successfulProjectAceActionCount == projectAclEditCount) {
							session.addDatasetToProject(
									creator,
									datasetId,
									projectId);
						}
					}

				}
			}
			return STATE.SUCCEEDED;
		} catch (RuntimeException e) {
			logger.error(m + ": task failed", e);
			return STATE.FAILED;
		}
	}

	private IEditAclAction<?> determineWorldEditAclAction(
			GetAcesResponse acesResponse,
			String datasetId,
			String worldPerm) {
		final String m = "determineWorldEditAclAction(...)";
		ExtPermission newWorldPerm = null;
		if (worldPerm.equalsIgnoreCase(CorePermDefs.READ_PERMISSION_NAME)) {
			newWorldPerm = CorePermDefs.READ_PERM;
		} else if (worldPerm
				.equalsIgnoreCase(CorePermDefs.EDIT_PERMISSION_NAME)) {
			newWorldPerm = CorePermDefs.EDIT_PERM;
		} else if (!worldPerm.equalsIgnoreCase("none")) {
			throw new IllegalArgumentException("Cannot set world perm to: ["
					+ worldPerm + "]");
		}

		final UpdateWorldAce updateWorldAce = new UpdateWorldAce(
				acesResponse.getWorldAce(),
				newWorldPerm);
		return updateWorldAce;

	}

	private Optional<String> determineWorldPerm(
			User creator,
			String datasetName,
			String datasetId)
			throws UnsupportedEncodingException {
		final String m = "determineWorldPerm(...)";
		final String worldPerm = getCurrentParameters()
				.getPermissionParameters()
				.getWorldPerm();
		if (worldPerm != null) {
			logger.debug("{}: Found world perm [{}]",
					m,
					worldPerm);
		}
		return Optional.fromNullable(worldPerm);
	}

	private List<ProjectPermissionParameters> determineProjectInfo(
			User creator,
			String datasetName,
			String datasetId)
			throws UnsupportedEncodingException {
		final String m = "determineProjectInfo(...)";
		final List<ProjectPermissionParameters> projectPermParams = new ArrayList<>();
		projectPermParams.addAll(getCurrentParameters()
				.getPermissionParameters().getProjectPermissionParameters());

		final Optional<String> defaultProjectName = getOrgQualifiedProperty("defaultProject");
		if (defaultProjectName.isPresent()) {
			boolean redunantDefaultProject = false;
			for (final ProjectPermissionParameters projParams : projectPermParams) {
				if (defaultProjectName.get()
						.equals(projParams.getProjectName())) {
					redunantDefaultProject = true;
					break;
				}
			}
			if (!redunantDefaultProject) {
				final ProjectPermissionParameters defaultProjectForOrg = new ProjectPermissionParameters();
				defaultProjectForOrg.setProjectName(defaultProjectName.get());
				projectPermParams.add(defaultProjectForOrg);
			}
		}

		if (projectPermParams.isEmpty()) {
			logger.debug(
					"{}: No project for [{}]. Checking organization for default project",
					m,
					datasetName);
		}
		final List<ProjectPermissionParameters> foundProjectPermParams = new ArrayList<>();
		for (final ProjectPermissionParameters projectPermParam : projectPermParams) {
			final String projectName = projectPermParam.getProjectName();
			final List<String> projectIds = session.getProjectIdsForName(
					creator,
					projectName);
			if (projectIds.size() == 1) {
				projectPermParam.setProjectId(projectIds.get(0));
				foundProjectPermParams.add(projectPermParam);
			} else if (projectIds.isEmpty()) {
				logger.debug("{}: no project [{}] for user [{}] found",
						m,
						projectName,
						creator.getUsername());
			} else if (projectIds.size() > 1) {
				throw new IllegalStateException(
						"Cannot grant permissions. Found "
								+ projectIds.size()
								+ " projects with name ["
								+ projectName
								+ "] for user ["
								+ creator.getUsername()
								+ "]");

			}
		}
		return foundProjectPermParams;
	}

	private List<IEditAclAction<?>> determineProjectEditAclActions(
			GetAcesResponse acesResponse,
			String datasetId,
			ProjectPermissionParameters projParam) {
		final String m = "determineProjectEditAclActions(...)";

		final String projectId = projParam.getProjectId();
		final String projectName = projParam.getProjectName();

		final ProjectGroup adminsGroup = new ProjectGroup(
				projectId,
				projectName,
				ProjectGroupType.ADMINS);
		final ProjectGroup teamGroup = new ProjectGroup(
				projectId,
				projectName,
				ProjectGroupType.TEAM);
		final ExtPermission adminsPerm = string2Perm(projParam
				.getProjectAdminsPerm());
		final ExtPermission teamPerm = string2Perm(projParam
				.getProjectTeamPerm());
		List<IEditAclAction<?>> actions = newArrayList();
		for (ExtProjectAce projectAce : acesResponse.getProjectAces()) {
			if (projectAce.getProjectGroup().equals(adminsGroup)) {
				final UpdateProjectAce updateProjectAce = new UpdateProjectAce(
						projectAce,
						adminsPerm);
				actions.add(updateProjectAce);
				logger.debug(
						"{}: Adding core {} perm to existing ace for admins of [{}]",
						m,
						adminsPerm,
						projectName);
			} else if (projectAce.getProjectGroup().equals(teamGroup)) {
				final UpdateProjectAce updateProjectAce = new UpdateProjectAce(
						projectAce,
						teamPerm);
				actions.add(updateProjectAce);
				logger.debug(
						"{}: Adding core {} perm to existing ace for team of [{}]",
						m,
						teamPerm,
						projectName);
			}
		}
		if (actions.isEmpty()) {
			ExtProjectAce adminsAce = new ExtProjectAce(
					projectId,
					projectName,
					ProjectGroupType.ADMINS,
					datasetId,
					acesResponse.getWorldAce().getAclVersion(),
					null,
					null);
			adminsAce.getPerms().add(adminsPerm);
			actions.add(new NewProjectAce(adminsAce));
			ExtProjectAce teamAce = new ExtProjectAce(
					projectId,
					projectName,
					ProjectGroupType.TEAM,
					datasetId,
					acesResponse.getWorldAce().getAclVersion(),
					null,
					null);
			teamAce.getPerms().add(teamPerm);
			actions.add(new NewProjectAce(teamAce));
			logger.debug("{}: Adding new project aces for [{}] groups",
					m,
					projectName);
		}
		return actions;

	}

	private Optional<String> getUrlEncodedOrgName()
			throws UnsupportedEncodingException {
		final String m = "getUrlEncodedOrgName(...)";
		final String datasetName = getCurrentParameters()
				.getTargetSnapshot();
		final String organizationName = getCurrentParameters()
				.getInstitution();
		if (organizationName == null) {
			logger.debug("{}: No organization for [{}]",
					m,
					datasetName);
			return Optional.absent();
		}
		logger.debug("{}: Dataset [{}] has organization [{}]",
				m,
				datasetName,
				organizationName);
		final String encodedOrgName = URLEncoder.encode
				(organizationName,
						"UTF-8");
		return Optional.of(encodedOrgName);
	}

	private Optional<String> getOrgQualifiedProperty(
			String propPrefix)
			throws UnsupportedEncodingException {
		final String m = "getOrgQualifiedProperty(...)";
		final Optional<String> encodedOrgName = getUrlEncodedOrgName();
		if (!encodedOrgName.isPresent()) {
			return Optional.absent();
		}
		final String propName = propPrefix
				+ "."
				+ encodedOrgName.get();
		final String propValue = IvProps.getIvProps().get(
				propName);
		if (propValue == null) {
			logger.debug("{}: No value for property [{}]",
					m,
					propName);
			return Optional.absent();
		}
		logger.debug(
				"{}: Found value [{}] for property [{}]",
				m,
				propValue,
				propName);
		return Optional.of(propValue);
	}

	private ExtPermission string2Perm(String permString) {
		if (permString.equalsIgnoreCase(CorePermDefs.READ_PERMISSION_NAME)) {
			return CorePermDefs.READ_PERM;
		}
		if (permString.equalsIgnoreCase(CorePermDefs.EDIT_PERMISSION_NAME)) {
			return CorePermDefs.EDIT_PERM;
		}
		if (permString.equalsIgnoreCase(CorePermDefs.OWNER_PERMISSION_NAME)) {
			return CorePermDefs.OWNER_PERM;
		}
		throw new IllegalArgumentException("Invalid permission name: ["
				+ permString + "]");

	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new GrantPermissionsAction(mQueue, origin, session,
				(SnapshotParameters) arguments);
	}

	@Override
	public SnapshotParameters getDefaultArguments() {
		return new SnapshotParameters();
	}

	@Override
	public SnapshotParameters getCurrentParameters() {
		return (SnapshotParameters) super.getCurrentParameters();
	}
}
