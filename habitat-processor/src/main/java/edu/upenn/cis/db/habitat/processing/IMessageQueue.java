package edu.upenn.cis.db.habitat.processing;

import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public interface IMessageQueue {
	/**
	 * Status update event
	 * @param event
	 */
	public void logEvent(IUser user, String channel, String source, WorkItemSet set, String event);
	
	/**
	 * Status update that should trigger higher priority
	 * 
	 * @param user
	 * @param alarm
	 */
	public void logAlarm(IUser user, String channel, String source, WorkItemSet set, String alarm);
	
	/**
	 * Error event
	 * @param error
	 */
	public void logError(IUser user, String channel, String source, WorkItemSet set, String error);

}
