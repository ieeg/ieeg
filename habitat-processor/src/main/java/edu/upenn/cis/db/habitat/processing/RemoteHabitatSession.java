/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.upenn.cis.db.habitat.processing;

import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import edu.upenn.cis.braintrust.BtUtil;
import edu.upenn.cis.braintrust.IvProps;
import edu.upenn.cis.braintrust.datasnapshot.DataSnapshotServerFactory;
import edu.upenn.cis.braintrust.datasnapshot.HasAclType;
import edu.upenn.cis.braintrust.datasnapshot.IDataSnapshotServer;
import edu.upenn.cis.braintrust.datasnapshot.IProjectUpdater;
import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.security.CorePermDefs;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.SessionToken;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.ChannelInfoDto;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.DataSnapshotId;
import edu.upenn.cis.braintrust.shared.DataSnapshotIds;
import edu.upenn.cis.braintrust.shared.DataSnapshotSearchResult;
import edu.upenn.cis.braintrust.shared.DataSnapshotType;
import edu.upenn.cis.braintrust.shared.EditAclResponse;
import edu.upenn.cis.braintrust.shared.EegProject;
import edu.upenn.cis.braintrust.shared.EegStudyMetadata;
import edu.upenn.cis.braintrust.shared.Ethnicity;
import edu.upenn.cis.braintrust.shared.ExperimentMetadata;
import edu.upenn.cis.braintrust.shared.Gender;
import edu.upenn.cis.braintrust.shared.GetAcesResponse;
import edu.upenn.cis.braintrust.shared.Handedness;
import edu.upenn.cis.braintrust.shared.IEditAclAction;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.UserId;
import edu.upenn.cis.braintrust.shared.dto.GwtAnimal;
import edu.upenn.cis.braintrust.shared.dto.GwtOrganization;
import edu.upenn.cis.braintrust.shared.dto.GwtPatient;
import edu.upenn.cis.braintrust.shared.dto.GwtStrain;
import edu.upenn.cis.braintrust.shared.dto.IHasGwtRecording;
import edu.upenn.cis.braintrust.shared.exception.BadTsAnnotationTimeException;
import edu.upenn.cis.braintrust.shared.exception.DataSnapshotNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DatasetNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.braintrust.shared.exception.EegMontageNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.RecordingNotFoundException;
import edu.upenn.cis.braintrust.shared.exception.TimeSeriesNotFoundInDatasetException;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.persistence.IGraphServer;
import edu.upenn.cis.db.habitat.persistence.exceptions.PersistenceCannotSerializeException;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction.TimeSeriesSettings;
import edu.upenn.cis.db.habitat.processing.actions.RegisterActions;
import edu.upenn.cis.db.habitat.processing.actions.medtronic.ImportMdtLogAnnotationsAction.AnnotationEvent;
import edu.upenn.cis.db.habitat.processing.parameters.EEGMontageParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters.SnapType;
import edu.upenn.cis.db.mefview.server.CoralReefServiceFactory;
import edu.upenn.cis.db.mefview.server.DataMarshalling;
import edu.upenn.cis.db.mefview.server.IEventServer;
import edu.upenn.cis.db.mefview.server.ISessionManager;
import edu.upenn.cis.db.mefview.server.ITimeSeriesPageServer;
import edu.upenn.cis.db.mefview.server.LruCache;
import edu.upenn.cis.db.mefview.server.SessionCacheEntry;
import edu.upenn.cis.db.mefview.server.SessionManager;
import edu.upenn.cis.db.mefview.server.TraceServer;
import edu.upenn.cis.db.mefview.server.TraceServerFactory;
import edu.upenn.cis.db.mefview.server.exceptions.ServerTimeoutException;
import edu.upenn.cis.db.mefview.services.ChannelSpecifier;
import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesData;
import edu.upenn.cis.db.mefview.shared.Annotation;
import edu.upenn.cis.db.mefview.shared.BasicCollectionNode;
import edu.upenn.cis.db.mefview.shared.BuiltinDataTypes;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileInfo;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.SearchResult;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.UnauthorizedException;
import edu.upenn.cis.eeg.mef.MefHeader2;

public class RemoteHabitatSession implements ISnapshotMapper {
	private final static Logger logger = LoggerFactory
			.getLogger(RemoteHabitatSession.class);

	private static DocumentBuilderFactory dbf;

	public static Map<UserId, Map<String, DataSnapshot>> snapshots = Collections
			.synchronizedMap(new LruCache<UserId, Map<String, DataSnapshot>>(
					500));

	static IEventServer events;

	static TraceServer traces;

	private transient IDataSnapshotServer datasets;

	IGraphServer graph;

	public static String sourcePath = "traces/";
	public final String path = "traces/Mayo_IEED_data/Mayo_IEED_";

	Integer remaining = -1;

	ITimeSeriesPageServer server;

	private IUserService dus;

	private transient ISessionManager sessionManager;

	public RemoteHabitatSession() {
		this.dus = UserServiceFactory.getUserService();
		if (dbf == null) {
			final DocumentBuilderFactory dbf = DocumentBuilderFactory
					.newInstance();
			dbf.setIgnoringElementContentWhitespace(true);
			RemoteHabitatSession.dbf = dbf;
			try {
				Class<IEventServer> c = (Class<IEventServer>) Class
						.forName("com.blackfynn.dsp.server.timeseries.EventTimeSeriesServer");
				events = c.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	Map<String, String> ivProps;

	public boolean open() throws UnknownHostException {
		logger.info("Initializing HABITAT remote connection");
		if (Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("ieegview.properties") != null) {
			try {
				logger.info("Configuring HABITAT connection from properties");
				String path = Thread.currentThread().getContextClassLoader()
						.getResource("ieegview.properties").getPath();
				// ivProps.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("ieegview.properties"));

				IvProps.init(path);

				ivProps = IvProps.getIvProps();

				sourcePath = BtUtil
						.get(ivProps, IvProps.SOURCEPATH, sourcePath);
				logger.info("Source path: " + sourcePath);

				final int sessionTimeoutMinutes = IvProps
						.getSessionExpirationMins();
				final int sessionCleanupMins = IvProps.getSessionCleanupMins();
				final int sessionCleanupDenom = IvProps
						.getSessionCleanupDenom();

				datasets = DataSnapshotServerFactory.getDataSnapshotServer();
				sessionManager = new SessionManager(datasets,
						sessionTimeoutMinutes, sessionCleanupMins,
						sessionCleanupDenom);

				traces = TraceServerFactory.getTraceServer();

				server = traces.getMEFServer();

				graph = CoralReefServiceFactory.getGraphServer();

			} catch (Exception e) {
				logger.error("", e);
				e.printStackTrace();
				System.out.println("Failed");
			}

			return true;
		} else
			throw new RuntimeException(
					"Need ieegview.properties file in classpath");

	}

	private User lookupUser(String username) {
		User ret = dus.findUserByUsername(username);

		if (ret == null)
			throw new RuntimeException("User " + username + " not found");
		return ret;
	}

	public DataSnapshot getSnapshotFor(User user, String datasetID) {
		return datasets.getDataSnapshot(user, datasetID);
	}

	public SessionToken getSession(User user) {
		return sessionManager.createSession(user);
	}

	public String createNewSnapshot(User user, Collection<String> channelIds,
			DataSnapshot original) throws DuplicateNameException {
		Set<String> channels = new HashSet<String>();
		channels.addAll(channelIds);

		String newLabel = original.getLabel() + "_LL_annot";

		// Clear out the snapshot if it already exists!
		String str;
		try {
			str = datasets.getDataSnapshotId(user, newLabel, true);
			DataSnapshot ds = datasets.getDataSnapshot(user, str);

			// Set of existing annotations
			Set<String> annot = new HashSet<String>();
			for (TsAnnotationDto tsa : ds.getTsAnnotations()) {
				annot.add(tsa.getId());
			}

			// Clear them out
			datasets.removeTsAnnotations(user, str, annot);

			// ///////// For graph store
			if (graph != null) {
				DataSnapshotSearchResult dsa = datasets.getDataSnapshotForId(
						user, ds.getId());

				dsa.setImageCount(original.getImages().size());
				dsa.setTsAnnCount(original.getTsAnnotations().size());
				dsa.setFrozen(original.isFrozen());
				dsa.setType(!original.isAnimal() ? DataSnapshotType.EEG_STUDY
						: DataSnapshotType.EXPERIMENT);
				dsa.setOwner(user.getUserId());

				logger.debug("Adding " + dsa.getLabel()
						+ " snapshot to graph store");

				try {
					graph.saveSnapshot(dsa, user);
				} catch (UnauthorizedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.debug("Unauthorized in adding " + dsa.getLabel()
							+ " snapshot to graph store");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.debug("IOException in adding " + dsa.getLabel()
							+ " snapshot to graph store");
				}
			}

			return str;

		} catch (DataSnapshotNotFoundException e) {
			return datasets.deriveDataset(user, original.getId(), newLabel,
					"LL_echauz",
					channels, new HashSet<String>());

		}
	}

	public List<ChannelSpecifier> getChannels(User creator, String datasetID,
			List<String> channels, double period) {
		return traces.getChannels(creator, datasetID, channels, period);
	}

	public void saveAnnotations(
			User creator,
			String datasetID,
			List<Annotation> newAnnotations) throws
			AuthorizationException, TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException, IOException {
		traces.saveAnnotations(creator, datasetID, newAnnotations);

		if (graph != null) {
			logger.debug("Saving " + newAnnotations.size() + " " + datasetID
					+ " annotations to graph store");
			try {
				graph.saveAnnotations(creator, datasetID, newAnnotations);
			} catch (UnauthorizedException e) {
				e.printStackTrace();
				throw new AuthorizationException(e.getMessage());
			}
		}
	}

	public DataSnapshotIds saveAnnotations(
			User user,
			String datasetId,
			Iterable<? extends TsAnnotationDto> tsAnnDtos)
			throws
			AuthorizationException,
			TimeSeriesNotFoundInDatasetException,
			BadTsAnnotationTimeException {
		return datasets.storeTsAnnotations(
				user,
				datasetId,
				tsAnnDtos);
	}

	public double getDuration(ChannelSpecifier spec)
			throws IllegalArgumentException, IOException {
		return traces.getDuration(spec);
	}

	public List<List<TimeSeriesData>> getTimeSeriesSet(User creator, String dataset,
			List<ChannelSpecifier> channelSpecs,
			double start, double duration, double frequency,
			List<FilterSpec> filterSpec,
			SessionCacheEntry se, SignalProcessingStep step)
			throws ServerTimeoutException, IOException {
		final DataSnapshotSearchResult data = this.datasets
				.getDataSnapshotForId(creator, dataset);
		final SessionToken token = sessionManager.createSession(creator);
		return traces.getTimeSeriesSet(creator, events, data, channelSpecs,
				start, duration, frequency, filterSpec, se,
				step, token);
	}

	public String createSnapshot(User user, String containerName,
			IHasGwtRecording recInfo) {
		String ret = datasets.createDataSnapshot(user, containerName, recInfo);

		// ///////// For graph store
		if (graph != null) {
			DataSnapshotSearchResult dsa = datasets.getDataSnapshotForId(user,
					ret);

			if (recInfo.getRecording() != null) {
				dsa.setStartTimeUutc(recInfo.getRecording().getStartTimeUutc());
				dsa.setEndTimeUutc(recInfo.getRecording().getEndTimeUutc());
			}
			dsa.setOwner(user.getUserId());

			logger.debug("Adding " + dsa.getLabel()
					+ " snapshot to graph store");
			try {
				graph.saveSnapshot(dsa, user);
			} catch (UnauthorizedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.debug("Unauthorized in adding " + dsa.getLabel()
						+ " snapshot to graph store");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.debug("IOException in adding " + dsa.getLabel()
						+ " snapshot to graph store");
			}
		}
		return ret;
	}

	@Override
	public String getSnapshotIdFor(User user, String snapshotName) {
		try {
			return datasets.getDataSnapshotId(user, snapshotName, true);
		} catch (DataSnapshotNotFoundException dnf) {
			return null;
		}
	}

	public List<String> getProjectIdsForName(User user, String projectName) {
		return datasets.getProjectIdsByName(user, projectName);
	}

	public void addDatasetToProject(final User user, final String datasetId,
			String projectId) {
		datasets.updateProject(user, projectId, new IProjectUpdater() {

			@Override
			public boolean update(EegProject project) {
				if (project.getAdmins().contains(user.getUserId())) {
					project.getSnapshots().add(datasetId);
					return true;
				}
				return false;
			}
		});

		if (graph != null) {
			logger.debug("Adding " + datasetId + " snapshot to project");
			EegProject project = datasets.getProject(user, projectId);

			if (project != null) {

				// Ensure project exists
				try {
					logger.debug("Attempting to create folder "
							+ "http://www.habitat.upenn.edu/homes/"
							+ user.getUsername() + "/" + project.getName());
					graph.createFolder(user, project.getName(),
							"http://www.habitat.upenn.edu/homes/"
									+ user.getUsername());
				} catch (UnauthorizedException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					logger.debug("Folder exists or unable to write");
				}

				DataSnapshotSearchResult dsa = datasets.getDataSnapshotForId(
						user, datasetId);

				BasicCollectionNode folder = new BasicCollectionNode(
						"http://www.habitat.upenn.edu/homes/"
								+ user.getUsername(), project.getName(),
						"Folders");

				try {
					logger.debug("Adding link from " + folder.getKey() + " to "
							+ dsa.getLabel());
					graph.addToFolder(dsa, folder, user);
				} catch (PersistenceCannotSerializeException
						| UnauthorizedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else
				logger.debug("Project " + projectId + " not found");
		}
	}

	public GetAcesResponse getCoreDatasetPerms(User user, String datasetId) {
		return datasets.getAcesResponse(user,
				HasAclType.DATA_SNAPSHOT,
				CorePermDefs.CORE_MODE_NAME,
				datasetId);
	}

	public List<EditAclResponse> modifyCoreDatasetPerms(
			User user,
			List<IEditAclAction<?>> actions) {
		return datasets.editAcl(
				user,
				HasAclType.DATA_SNAPSHOT,
				CorePermDefs.CORE_MODE_NAME,
				actions);
	}

	private MefHeader2 readHeader(IStoredObjectReference handle)
			throws IOException {

		IInputStream input =
				StorageFactory.getBestServerFor(handle).openForInput(handle);

		ByteBuffer target = ByteBuffer.allocate(1024);

		input.getBytes(target, 1024);

		MefHeader2 mefH =
				new MefHeader2(target);

		return mefH;
	}

	private ChannelInfoDto getChannelInfo(IUser user, MefHeader2 header) {
		String inst = (user.getProfile().getInstitutionName().isPresent() ?
				user.getProfile().getInstitutionName().get() :
				"everywhere");

		ChannelInfoDto ret = new ChannelInfoDto(
				"id",
				(Long) null,
				inst,
				(int) header.getHeaderLength(),
				header.getSubjectFirstName(),
				header.getSubjectSecondName(),
				header.getSubjectThirdName(),
				header.getSubjectId(),
				(long) header.getNumberOfSamples(),
				header.getChannelName(),
				(long) header.getRecordingStartTime(),
				(long) header.getRecordingEndTime(),
				(double) header.getSamplingFrequency(),
				(double) header.getLowFrequencyFilterSetting(),
				(double) header.getHighFrequencyFilterSetting(),
				(double) header.getNotchFilterFrequency(),
				header.getAcquisitionSystem(),
				header.getChannelComments(),
				header.getStudyComments(),
				(int) header.getPhysicalChannelNumber(),
				(long) header.getMaximumBlockLength(),
				(long) header.getBlockInterval(),
				(int) header.getBlockHeaderLength(),
				(int) header.getMaximumDataValue(),
				(int) header.getMinimumDataValue(),
				(double) header.getVoltageConversionFactor(),
				null
				);

		return ret;
	}

	public String addTimeSeriesToSnapshot(User user, String snapshot,
			int channelInx,
			SnapshotParameters parms, IStoredObjectReference handle)
			throws AuthorizationException, DatasetNotFoundException {
		return addTimeSeriesToSnapshot(user, snapshot, channelInx, parms,
				handle,
				BuiltinDataTypes.EEG, "V", null, null);
	}

	public String addTimeSeriesToSnapshot(User user, String snapshot,
			int channelInx,
			SnapshotParameters parms, IStoredObjectReference handle, String
			modality, String units, @Nullable Double multiplier,
			@Nullable Double baseline)
			throws AuthorizationException, DatasetNotFoundException {
		final String m = "addTimeSeriesToSnapshot(...)";

		try {
			ChannelInfoDto dto = getChannelInfo(user, readHeader(handle));

			dto.setModality(modality);
			if (baseline != null)
				dto.setBaseline(baseline);
			dto.setUnits(units);
			if (multiplier != null)
				dto.setMultiplier(dto.getVoltageConversionFactor());

			dto.setPhysicalChannelNumber(channelInx);

			List<ChannelInfoDto> channels = new ArrayList<ChannelInfoDto>();

			if (parms.getSettings() != null
					&& parms.getSettings().size() > channelInx) {
				TimeSeriesSettings det = parms.getSettings().get(channelInx);

				String paramChannelLabel = det.getChannelLabel();
				if (paramChannelLabel != null && !paramChannelLabel.isEmpty()) {
					String headerChannelLabel = dto.getChannelName();
					dto.setChannelName(paramChannelLabel);
					logger.debug(
							"{}: Overriding label [{}] from header with label [{}] from parameters for channel {} index {}",
							m,
							headerChannelLabel,
							paramChannelLabel,
							handle.getObjectKey(),
							channelInx);
				}
			}

			String paramInstitution = null;// parms.getOwner();
			if (paramInstitution != null && !paramInstitution.isEmpty()) {
				String headerInstitution = dto.getInstitution();
				dto.setInstitution(paramInstitution);
				logger.debug(
						"{}: Overriding institution [{}] from header with institution [{}] from parameters for channel {} index {}",
						m,
						headerInstitution,
						paramInstitution,
						handle.getObjectKey(),
						channelInx);
			}

			channels.add(dto);

			List<String> traces = new ArrayList<String>();
			String newTrace = datasets.addMEF(user, handle.getObjectKey(), dto);
			traces.add(newTrace);
			dto.setId(newTrace);

			// Store the cached entry for this one
			datasets.writeChannelInfo(snapshot, Lists.newArrayList(dto));

			Map<String, TimeSeriesDto> series = datasets.getTimeSeries(user,
					snapshot);
			logger.debug("{}: Dataset currently has {} time series.", m,
					series.size());
			Set<String> tracesSeen = new HashSet<String>();
			for (TimeSeriesDto tsDto : series.values()) {
				int index = 0;
				for (ChannelInfoDto chan : channels) {

					if (graph != null) {
						logger.debug("addTimeSeriesToSnapshot");
						String traceId = chan.getLabel();

						try {
							if (!tracesSeen.contains(traceId)) {
								graph.storeTimeSeriesObject(
										user,
										snapshot,
										chan,
										traceId,
										StorageFactory.getBestServerFor(handle),
										handle);
								graph.attachTimeSeries(user, snapshot, chan,
										traceId);
								tracesSeen.add(traceId);
							}
						} catch (UnauthorizedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (chan.getChannelName().equals(tsDto.getLabel())) {
						channels.remove(index);
						logger.debug(
								"{}: Time series [{}] already in dataset. Will not add.",
								m, chan.getChannelName());
						break;
					} else {
						index++;
						logger.trace(
								"{}: Time series [{}] does not match existing time series [{}]. Still considering for addition.",
								m,
								chan.getChannelName(),
								tsDto.getLabel());
					}
				}
			}
			if (!channels.isEmpty()) {
				String ret = datasets.addTimeSeriesToDataSnapshot(user,
						snapshot, channels, traces);

				// if (graph != null) {
				// logger.debug("addTimeSeriesToSnapshot");
				// for (int i = 0; i < channels.size(); i++) {
				// ChannelInfoDto channel = channels.get(i);
				// String traceId = traces.get(i);
				//
				// try {
				// graph.storeTimeSeriesObject(user, snapshot, channel, traceId,
				// StorageFactory.getBestServerFor(handle), handle);
				// graph.attachTimeSeries(user, snapshot, channel, traceId);
				// } catch (UnauthorizedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// }
				// }

				return ret;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

		// datasets.addLogEntry(user, new LogMessage(
		// user.getUserId(),
		// user.getUserId(),
		// "Added " + path + " to " + snapshot,
		// (new Date()).getTime(),
		// ));
	}

	@Override
	public void registerHandleFor(User user, String snapshotId, String contrib,
			IStoredObjectReference handle) throws IOException,
			DuplicateNameException {
		String mimeType = RegisterActions.getMimeType(handle);
		if (mimeType.equals(""))
			mimeType = "application/unknown";

		int length;
		try {
			length = (int) StorageFactory.getBestServerFor(handle)
					.openForInput(handle).getLength();
			datasets.addAsRecordingObject(user, handle.getObjectKey(), contrib,// +
																				// handle.getFilePath(),
					mimeType, length, snapshotId);

			if (graph != null) {
				Set<DataSnapshotSearchResult> dsrs = Sets.newHashSet(datasets
						.getDataSnapshotForId(user, snapshotId));
				SearchResult sr = DataMarshalling
						.convertSearchResults(dsrs, user, null).iterator()
						.next();

				FileInfo metadata = new FileInfo(handle.getFilePath(),
						sr.getDatasetRevId(),
						contrib,
						"User upload", mimeType);

				graph.storeMetadataObject(user, sr, metadata,
						StorageFactory.getPersistentServer(),
						handle);

				graph.indexDocument(user, sr, metadata,
						StorageFactory.getPersistentServer(),
						handle);
			}
		} catch (RecordingNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (UnauthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public IStoredObjectReference getHandleFor(User user, String snapshotId,
			String contrib) {
		// TODO Auto-generated method stub
		return null;
	}

	Map<String, SnapshotParameters> cachedParameters = new HashMap<>();

	@Override
	public void registerSnapshotParametersFor(User user,
			String snapshotName, SnapshotParameters parms) {

		cachedParameters.put(snapshotName, parms);
	}

	@Override
	public SnapshotParameters getSnapshotParametersFor(User user,
			String snapshotName) {
		final String m = "getSnapshotParametersFor(...)";
		if (cachedParameters.containsKey(snapshotName))
			return cachedParameters.get(snapshotName);

		String id = getSnapshotIdFor(user, snapshotName);

		DataSnapshot snap = datasets.getDataSnapshot(user, id);

		// TODO: perhaps we should get this somehow from the server...?
		SnapType typ = SnapType.unknown;

		SnapshotParameters ret = new SnapshotParameters(snapshotName, id,
				typ, new ArrayList<String>());

		ret.setHuman(snap.isHuman());
		logger.debug("{}: Returned parameters isHuman: [{}]", m, ret.isHuman());
		return ret;
	}

	public boolean addAnnotation(User creator, String dataSnapshotID,
			List<AnnotationEvent> events) {
		final String m = "addAnnotation(...)";
		List<Annotation> annotations = new ArrayList<Annotation>();
		for (AnnotationEvent event : events) {
			annotations.add(
					new Annotation(creator.getUsername(),
							event.getType(), event.getTime(), event.getTime(),
							event.getChannels()));
		}
		try {
			traces.saveAnnotations(creator, dataSnapshotID, annotations);

			if (graph != null) {
				logger.debug("Saving " + annotations.size() + " "
						+ dataSnapshotID + " annotations to graph store");
				graph.saveAnnotations(creator, dataSnapshotID, annotations);
			}

			return true;
		} catch (AuthorizationException | TimeSeriesNotFoundInDatasetException
				| BadTsAnnotationTimeException e) {
			logger.error(m + ": ", e);
		} catch (UnauthorizedException e) {
			logger.error(m + ": ", e);
		} catch (IOException e) {
			logger.error(m + ": ", e);
		}
		return false;
	}

	public void addHospitalAdmissionData(User user2, String targetSnapshotId,
			SnapshotParameters fullParameters) {

		GwtOrganization org = new GwtOrganization("Temporary", "", (long) 0, 0);

		if (fullParameters.getInstitution() != null
				&& !fullParameters.getInstitution().isEmpty())
			org.setName(fullParameters.getInstitution());

		GwtPatient origPatient = new GwtPatient(
				fullParameters.getTargetSnapshot(),
				org,
				Handedness.UNKNOWN,
				Ethnicity.UNKNOWN,
				Gender.UNKOWN,
				"",
				null,
				null,
				null,
				null,
				null,
				null,
				null);
		origPatient.setLabel(fullParameters.getTargetSnapshot());

		int ageAtAdmission = 0;

		try {
			datasets.setAdmissionInfo(user2, targetSnapshotId, origPatient,
					ageAtAdmission);

			if (graph != null) {
				EegStudyMetadata meta = datasets.getEegStudyMetadata(user2,
						targetSnapshotId);

				logger.debug("addHospitalAdmissionData: Saving hospital admission metadata for "
						+ targetSnapshotId + " to graph");

				graph.savePatientSnapshotMetadata(user2, targetSnapshotId, meta);
			}
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DatasetNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnauthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Creates a new folder
	 * 
	 * @param creator
	 * @param label
	 * @param folderPathName
	 * @return
	 * @throws UnauthorizedException
	 * @throws IOException
	 */
	public BasicCollectionNode createFolder(User creator, String label,
			String folderPathName)
			throws UnauthorizedException, IOException {
		return graph.createFolder(creator, label, folderPathName);
	}

	/**
	 * Add an object to a folder. Note that the Habitat portal currently only
	 * recognizes snapshots added to folders; there you may wish to use
	 * <b>addDatasetToProject</b>.
	 * 
	 * @param creator
	 * @param toAdd
	 * @param folder
	 * @throws UnauthorizedException
	 * @throws IOException
	 */
	public void addObjectToFolder(User creator, Object toAdd,
			BasicCollectionNode folder) throws UnauthorizedException,
			IOException {
		graph.addToFolder(toAdd, folder, creator);
	}

	public void addMontage(User creator,
			EEGMontageParameters parameters) throws EegMontageNotFoundException {

		String dataSetID = datasets.getDataSnapshotId(creator,
				parameters.getTargetSnapshot(), true);
		datasets.createEditEegMontage(creator, parameters.getMontage(),
				dataSetID);

	}
	
	public void addMontages(User creator,
			String datasetId,
			Iterable<? extends EEGMontage> montages) throws EegMontageNotFoundException {

		for (final EEGMontage montage : montages) {
			datasets.createEditEegMontage(creator, montage,
				datasetId);
		}

	}

	public void addAnimal(User creator,
			String targetSnapshotId,
			SnapshotParameters fullParameters) {

		GwtOrganization org = new GwtOrganization(
				"Temporary",
				"",
				0L,
				0);

		String institution = Strings.nullToEmpty(fullParameters
				.getInstitution());
		if (!institution.isEmpty()) {
			org.setName(institution);
		}
		GwtStrain strain = new GwtStrain(
				"none",
				null,
				null,
				"unknown",
				null);
		GwtAnimal animal = new GwtAnimal(
				fullParameters.getTargetSnapshot(),
				org,
				strain,
				null,
				null);
		datasets.setAnimal(
				creator,
				new DataSnapshotId(targetSnapshotId),
				animal);

		if (graph != null) {
			ExperimentMetadata meta2 = datasets.getExperimentMetadata(creator,
					targetSnapshotId);

			logger.debug("addAnimal: Saving hospital admission metadata for "
					+ targetSnapshotId + " to graph");

			try {
				graph.saveAnimalSnapshotMetadata(creator, targetSnapshotId,
						meta2);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnauthorizedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void associateSnapshot(String snapshotId,
			IStoredObjectReference container,
			IStoredObjectReference controlFile) {

	}

	// public void associateFile(RecordingObject file, IStoredObjectReference
	// container,
	// IStoredObjectReference controlFile) {
	//
	// }
	//
	// public void associateTimeseries(ChannelInfoDto channel,
	// IStoredObjectReference container,
	// IStoredObjectReference controlFile) {
	//
	// }

	public void associateCopiedFile(IStoredObjectReference source,
			IStoredObjectReference target) {

	}
}