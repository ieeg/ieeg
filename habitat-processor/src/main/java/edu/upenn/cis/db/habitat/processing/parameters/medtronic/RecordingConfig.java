/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import javax.xml.bind.annotation.XmlElement;

public class RecordingConfig {
	
	String recordingMode;
	Integer logSize;
	Boolean logWrapEnabled;
	Integer delayTimerMinutes;
	Integer interalTimerMinutes;
	Integer recordingTimeoutDays;
	Integer preTriggerSeconds;
	Integer durationSeconds;
	Boolean externalMarkerStart;
	Boolean externalMarkerStop;
	Boolean recordingWrapEnabled;
	Boolean algDataWritten;
	Boolean algStatusWritten;
	
	@XmlElement(name="RecordingMode")
	public String getRecordingMode() {
		return recordingMode;
	}
	public void setRecordingMode(String recordingMode) {
		this.recordingMode = recordingMode;
	}

	@XmlElement(name="LogSize")
	public Integer getLogSize() {
		return logSize;
	}
	public void setLogSize(Integer logSize) {
		this.logSize = logSize;
	}

	@XmlElement(name="LogWrapEnabled")
	public Boolean getLogWrapEnabled() {
		return logWrapEnabled;
	}
	public void setLogWrapEnabled(Boolean logWrapEnabled) {
		this.logWrapEnabled = logWrapEnabled;
	}

	@XmlElement(name="DelayTimerMinutes")
	public Integer getDelayTimerMinutes() {
		return delayTimerMinutes;
	}
	public void setDelayTimerMinutes(Integer delayTimerMinutes) {
		this.delayTimerMinutes = delayTimerMinutes;
	}

	@XmlElement(name="IntervalTimerMinutes")
	public Integer getInteralTimerMinutes() {
		return interalTimerMinutes;
	}
	public void setInteralTimerMinutes(Integer interalTimerMinutes) {
		this.interalTimerMinutes = interalTimerMinutes;
	}
	
	@XmlElement(name="RecordingTimeoutDays")
	public Integer getRecordingTimeoutDays() {
		return recordingTimeoutDays;
	}
	public void setRecordingTimeoutDays(Integer recordingTimeoutDays) {
		this.recordingTimeoutDays = recordingTimeoutDays;
	}

	@XmlElement(name="PreTriggerSeconds")
	public Integer getPreTriggerSeconds() {
		return preTriggerSeconds;
	}
	public void setPreTriggerSeconds(Integer preTriggerSeconds) {
		this.preTriggerSeconds = preTriggerSeconds;
	}
	@XmlElement(name="DurationSeconds")
	public Integer getDurationSeconds() {
		return durationSeconds;
	}
	public void setDurationSeconds(Integer durationSeconds) {
		this.durationSeconds = durationSeconds;
	}
	
	@XmlElement(name="ExternalMarkerStart")
	public Boolean getExternalMarkerStart() {
		return externalMarkerStart;
	}
	public void setExternalMarkerStart(Boolean externalMarkerStart) {
		this.externalMarkerStart = externalMarkerStart;
	}
	
	@XmlElement(name="ExternalMarkerStop")
	public Boolean getExternalMarkerStop() {
		return externalMarkerStop;
	}
	public void setExternalMarkerStop(Boolean externalMarkerStop) {
		this.externalMarkerStop = externalMarkerStop;
	}

	@XmlElement(name="RecordingWrapEnabled")
	public Boolean getRecordingWrapEnabled() {
		return recordingWrapEnabled;
	}
	public void setRecordingWrapEnabled(Boolean recordingWrapEnabled) {
		this.recordingWrapEnabled = recordingWrapEnabled;
	}

	@XmlElement(name="AlgDataWritten")
	public Boolean getAlgDataWritten() {
		return algDataWritten;
	}
	public void setAlgDataWritten(Boolean algDataWritten) {
		this.algDataWritten = algDataWritten;
	}

	@XmlElement(name="AlgStatusWritten")
	public Boolean getAlgStatusWritten() {
		return algStatusWritten;
	}
	public void setAlgStatusWritten(Boolean algStatusWritten) {
		this.algStatusWritten = algStatusWritten;
	}
	
	/*
  <RecordingConfig>
    <RecordingMode>On-Demand</RecordingMode>
    <LogSize>100</LogSize>
    <LogWrapEnabled>False</LogWrapEnabled>
    <DelayTimerMinutes>0</DelayTimerMinutes>
    <IntervalTimerMinutes>0</IntervalTimerMinutes>
    <RecordingTimeoutDays>90</RecordingTimeoutDays>
    <PreTriggerSeconds>30</PreTriggerSeconds>
    <DurationSeconds>60</DurationSeconds>
    <ExternalMarkerStart>True</ExternalMarkerStart>
    <ExternalMarkerStop>False</ExternalMarkerStop>
    <RecordingWrapEnabled>False</RecordingWrapEnabled>
    <AlgDataWritten>True</AlgDataWritten>
    <AlgStatusWritten>True</AlgStatusWritten>
  </RecordingConfig>	 */
}
