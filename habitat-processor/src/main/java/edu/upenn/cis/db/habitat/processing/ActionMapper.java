/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class ActionMapper {
	public static class Mapping {
		String action;
		CreateParameters parameterMap;
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public CreateParameters getParameterMap() {
			return parameterMap;
		}
		public void setParameterMap(CreateParameters parameterMap) {
			this.parameterMap = parameterMap;
		}
		public Mapping(String action, CreateParameters parameterMap) {
			super();
			this.action = action;
			this.parameterMap = parameterMap;
		}
		
		@Override
		public int hashCode() {
			return action.hashCode();
		}
		
		@Override
		public boolean equals(Object two) {
			if (!(two instanceof Mapping))
				return false;
			
			return action.equals(((Mapping)two).action);
		}
	}
	static Map<IWorkFilter,Set<Mapping>> actionMap = new HashMap<IWorkFilter,Set<Mapping>>();
	static ActionMapper theMap = null;
	
	public static synchronized ActionMapper getMap() {
		if (theMap == null)
			theMap = new ActionMapper();
		
		return theMap;
	}
	
	public ActionMapper() {
		
	}
	
	/**
	 * Register an action if it passes a filter
	 * 
	 * @param filterMatched
	 * @param actionName
	 */
	public void register(IWorkFilter filterMatched, String actionName, CreateParameters parameterMap) {
		if (!actionMap.containsKey(filterMatched))
			actionMap.put(filterMatched, new HashSet<Mapping>());
		Set<Mapping> items = actionMap.get(filterMatched);
		
		items.add(new Mapping(actionName, parameterMap));
	}
	
	/**
	 * Given all registered work item filters, see which ones
	 * apply to the work item and return the set of all relevant actions
	 * 
	 * @param item
	 * @return
	 */
	public Set<Mapping> getRelevantActions(IWorkItem item, ITaskTracker otherTasks) {
		Set<Mapping> matched = new HashSet<Mapping>();
		
		for (IWorkFilter filter: actionMap.keySet()) {
			if (filter.matches(item, otherTasks))
				matched.addAll(actionMap.get(filter));
		}
		
		return matched;
	}

	public Set<Mapping> getRelevantActions(WorkItemSet item, ITaskTracker otherTasks) {
		Set<Mapping> matched = new HashSet<Mapping>();
		
		for (IWorkFilter filter: actionMap.keySet()) {
			if (filter.matchesSet(item, otherTasks))
				matched.addAll(actionMap.get(filter));
		}
		
		return matched;
	}

//	public Set<Mapping> getRelevantActions(Set<String> items, ITaskTracker otherTasks) {
//		Set<Mapping> matched = new HashSet<Mapping>();
//		
//		for (IWorkFilter filter: actionMap.keySet()) {
//			if (filter.matchesSet(items, otherTasks))
//				matched.addAll(actionMap.get(filter));
//		}
//		
//		return matched;
//	}

	/**
	 * 
	 * @param item
	 * @param filterMatched
	 * @return
	 */
	public Set<Mapping> getRelevantActions(IWorkItem item, IWorkFilter filterMatched) {
		Set<Mapping> matched = new HashSet<Mapping>();
		
		matched.addAll(actionMap.get(filterMatched));
		
		return matched;
	}
}
