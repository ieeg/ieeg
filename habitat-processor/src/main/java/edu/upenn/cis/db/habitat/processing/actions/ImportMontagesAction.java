/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.MimeTypeRecognizer;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class ImportMontagesAction extends BaseSnapshotProcessingAction {
	public static final String NAME = "ImportMontagesAction";
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public ImportMontagesAction(
			IMessageQueue mQueue,
			WorkItemSet origin,
			RemoteHabitatSession session,
			ParametersForSnapshotContent parameters) {
		super(mQueue, origin, session, parameters);
	}

	public ImportMontagesAction() {
		super();
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		try {
			final String datasetName = getCurrentParameters()
					.getTargetSnapshot();
			final String datasetId = getCurrentParameters()
					.getTargetSnapshotId();
			final User creator = (User) getCreator();

			logger.debug(
					"{}: creator: [{}], dataset: [{}], dataset id: [{}]",
					m,
					creator.getUsername(),
					datasetName,
					datasetId);
			final DataSnapshot dataset = session.getSnapshotFor(creator,
					datasetId);
			final List<EEGMontage> montages = new ArrayList<>();
			for (JsonTyped item : parameters.getInputs()) {
				final FileReference handle = (FileReference) item;
				final Optional<IMontageImporter> importer = newMontageImporter(handle);
				if (!importer.isPresent()) {
					logger.info("{}: Ignoring non-montage file {} for {}",
							m,
							handle,
							datasetName);
					continue;
				}
				logger.info("{}: Processing montage file {} for {}",
						m,
						handle,
						datasetName);
				final List<EEGMontage> importedMontages = importer
						.get().importMontages(
								creator,
								dataset,
								handle);
				montages.addAll(importedMontages);
				logger.info("{}: Added {} montages from montage file {} for {}",
						m,
						importedMontages.size(),
						handle,
						datasetName);

			}
			session.addMontages(creator, datasetId, montages);
			return STATE.SUCCEEDED;
		} catch (RuntimeException e) {
			logger.error(m + ": task failed", e);
			return STATE.FAILED;
		}
	}

	private Optional<IMontageImporter> newMontageImporter(
			FileReference handle) {
		if (RegisterActions.getMimeType(handle).equals(
				MimeTypeRecognizer.IEEG_JSON_MONTAGE_MIME_TYPE_NAME)) {
			final IMontageImporter importer = new IeegJsonMontageImporter();
			return Optional.of(importer);
		}
		return Optional.absent();
	}

	@Override
	public IProcessingAction create(
			IMessageQueue mQueue,
			WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new ImportMontagesAction(
				mQueue,
				origin,
				session,
				(ParametersForSnapshotContent) arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}

	@Override
	public ParametersForSnapshotContent getCurrentParameters() {
		return parameters;
	}
}
