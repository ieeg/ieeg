/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IfElseAction;
import edu.upenn.cis.db.habitat.processing.actions.MapMetadataAction;
import edu.upenn.cis.db.habitat.processing.actions.RegisterActions;
import edu.upenn.cis.db.habitat.processing.conditionals.SuccessfullyProcessedTest;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.WorkItem;
import edu.upenn.cis.db.mefview.shared.CredentialedDirectoryBucketContainer;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class ApplyToDirectoriesAction extends BaseSnapshotProcessingAction {
	
	public final static String NAME = "ApplyToDirectories";
	
	ApplyToDirectoriesAction() {
		super();
	}
	
	public ApplyToDirectoriesAction(
			IMessageQueue mQueue, 
			WorkItemSet orig, 
			RemoteHabitatSession session, 
			ParametersForSnapshotContent fp) {
		super(mQueue, orig, session, fp);
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		if (getCurrentParameters() == null)
			setParameters((ParametersForSnapshotContent)arguments);
		else if (arguments != null) {
			getCurrentParameters().merge((Parameters)arguments);
		}
		
		// TODO: for each directory, add to a work item set
		Map<String,WorkItemSet> itemMap = new HashMap<String,WorkItemSet>();
//		WorkItemSet items = new WorkItemSet("Work items for " + inFile, null);
		for (JsonTyped fileSpec: getCurrentParameters().getInputs()) {
			
			CredentialedDirectoryBucketContainer fil = (CredentialedDirectoryBucketContainer)fileSpec;
			
			Set<IStoredObjectReference> contents = StorageFactory.getStagingServer().getDirectoryContents(fil);

			for (IStoredObjectReference fn : contents) {
				String typ = RegisterActions.getMimeType(fn);
				
				WorkItem work = new WorkItem(
						fn.toString(), 
						null, 
						fn,
						null,
						getWorkItemSet().iterator().next().getCreator(), 
						(new Date()).getTime(), 
						typ,
						"DFAction",
						null);
				
				System.out.println("Added work item "  + work.getDescription() + " / " + 
						work.getMediaType());
				
				if (!itemMap.containsKey(typ))
					itemMap.put(typ, new WorkItemSet("Work items for " + fileSpec + "/" + typ, null,
							getWorkItemSet().getRequest(), getWorkItemSet().getOrigin()));
				itemMap.get(typ).add(work);
			}
		}
		Set<WorkItemSet> allItems = new HashSet<WorkItemSet>();//("Items for " + inFile, null);
		for (WorkItemSet set : itemMap.values()) {
			getNextWorkItems().add(set);
			allItems.add(set);
		}
		
		// TODO: add a follow-up action to check that the whole transaction completed...?
		Set<IProcessingAction> whenDone = new HashSet<IProcessingAction>();
		whenDone.add(new MapMetadataAction(getMessageQueue(), this.getWorkItemSet(), getSession(), 
				new ParametersForSnapshotContent()));
		Set<IProcessingAction> whenNotDone = new HashSet<IProcessingAction>();
		
		IfElseAction barrier = new IfElseAction(getMessageQueue(), getWorkItemSet(),
				new SuccessfullyProcessedTest(allItems, TaskTrackerFactory.getTracker()),
				whenDone,
				whenNotDone);
		
		// Repeat self if not done
//		whenNotDone.add(barrier);
		getNextActions().add(barrier);

		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session,
			Parameters arguments) {
		return new ApplyToDirectoriesAction(mQueue, origin, session, (ParametersForSnapshotContent)arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}
}
