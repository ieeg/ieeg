/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.persyst;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ini4j.Wini;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.mefview.shared.FileReference;

public class PersystParameters implements IPersystParameters {
  
  String fileType;
  Double samplingRate;
  Double calibration;
  Integer waveformCount;
  Integer headerLength;
  Integer dataType;
  List<PersystChannelMap> channels;
  
//  PersystChannel[] channels;
  
  public PersystParameters(FileReference inFile) throws IOException{
    IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
    
    IInputStream inStream = bestServer.openForInput(inFile);
    
    BufferedInputStream inFStream = 
                new BufferedInputStream(inStream.getInputStream());
    
    Wini ini = new Wini(inFStream);
    inFStream.close();
    inStream.close();

    this.setSamplingRate(ini.get("FileInfo", "SamplingRate", double.class));  
    this.setCalibration(ini.get("FileInfo", "Calibration", double.class));
    this.setFileType(ini.get("FileInfo", "FileType", String.class));
    this.setDataType(ini.get("FileInfo","DataType",int.class));
    this.setHeaderLength(ini.get("FileType","HeaderLength",int.class));
    this.setWaveformCount(ini.get("FileInfo","WaveformCount",int.class));
    
    Map<String, String> map = ini.get("ChannelMap");
    Set<String> items = map.keySet();
    
    channels = new ArrayList<PersystChannelMap>();
    for (String item : items){
      String aux = map.get(item);
      channels.add(new PersystChannelMap(Integer.valueOf(aux), item));
    }

  }
  



  public String getFileType() {
    return fileType;
  }

  private void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public Double getSamplingRate() {
    return samplingRate;
  }

  private void setSamplingRate(Double samplingRate) {
    this.samplingRate = samplingRate;
  }

  public Double getCalibration() {
    return calibration;
  }

  private void setCalibration(Double calibration) {
    this.calibration = calibration;
  }

  public Integer getWaveformCount() {
    return waveformCount;
  }

  private void setWaveformCount(Integer waveformCount) {
    this.waveformCount = waveformCount;
  }

  public int getHeaderLength() {
    return headerLength;
  }

  private void setHeaderLength(int headerLength) {
    this.headerLength = headerLength;
  }

  public int getDataType() {
    return dataType;
  }

  private void setDataType(int dataType) {
    this.dataType = dataType;
  }

  public List<PersystChannelMap> getChannels() {
    return channels;
  }

  public void setChannels(List<PersystChannelMap> channels) {
    this.channels = channels;
  }




  @Override
  public Long findTimeOffset() {
    // TODO Auto-generated method stub
    return Long.valueOf(0);
  }




  @Override
  public Long getFileTimeStamp() {
    // TODO Auto-generated method stub
    return Long.valueOf(0);
  }



}
