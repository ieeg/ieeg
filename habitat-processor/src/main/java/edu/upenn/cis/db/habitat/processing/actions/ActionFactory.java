/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Factory:  Create an action by name
 * 
 * @author zives
 *
 */
public class ActionFactory {
	static Map<String,IProcessingAction> actionMap = new HashMap<String,IProcessingAction>();

	static ActionFactory theFactory = null;
	
	/**
	 * Get the singleton factory
	 * 
	 * @return
	 */
	public static synchronized ActionFactory getFactory() {
		if (theFactory == null)
			theFactory = new ActionFactory();
		
		return theFactory;
	}
	
	public ActionFactory() {
		
	}
	
	/**
	 * Register a new action "seed" under its unique name
	 * 
	 * @param name
	 * @param seed
	 */
	public void register(String name, IProcessingAction seed) {
		actionMap.put(name, seed);
	}
	
	/**
	 * Create a new action by name, based on the "seed" instance
	 * 
	 * @param name Action name
	 * @param origin Originating work item
	 * @param session IEEG connection session
	 * @return new processing Action
	 */
	public IProcessingAction create(IMessageQueue mQueue, String name, WorkItemSet origin, RemoteHabitatSession session) {
		return create(mQueue, name, origin, session, null);
	}
	
	/**
	 * Create a new action by name, based on the "seed" instance
	 * 
	 * @param name Action name
	 * @param origin Originating work item
	 * @param session IEEG connection session
	 * @param arguments Optional additional JSON-formatted arguments
	 * @return new processing Action
	 */
	public IProcessingAction create(IMessageQueue mQueue, String name, WorkItemSet origin, RemoteHabitatSession session, 
			Parameters arguments) {
		
		if (arguments == null)
			arguments = actionMap.get(name).getDefaultArguments();
		
		return actionMap.get(name).create(mQueue, origin, session, arguments);
	}

	/**
	 * Get a "base" instance of the parameters associated with the named
	 * action type
	 * 
	 * @param name Action name
	 * @return object representing parameters
	 */
	public Parameters getDefaultParameters(String name) {
	  System.out.println("getting defaultparameters for " + name);
		return actionMap.get(name).getDefaultArguments();
	}
}
