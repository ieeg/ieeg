/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.IUser;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

import java.util.Set;

public class WorkItem implements IWorkItem {
	
	public String id;
	public String parentId;
	public IStoredObjectReference mainHandle;
	public Set<IStoredObjectReference> otherHandles;
	public IUser creator;
	public Long timestamp;
	String mediaType;
	String description;
	JsonTyped json;

	@Override
	public String getItemId() {
		return id;
	}

	@Override
	public String getParentId() {
		return parentId;
	}

	@Override
	public IStoredObjectReference getMainHandle() {
		return mainHandle;
	}

    @Override
    public Set<IStoredObjectReference> getOtherHandles() {return otherHandles;}

	@Override
	public IUser getCreator() {
		return creator;
	}

	@Override
	public Long getTimestamp() {
		return timestamp;
	}

	@Override
	public String getMediaType() {
		return mediaType;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public JsonTyped getJson() {

		return json;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setHandle(IStoredObjectReference handle) {
		this.mainHandle = handle;
	}

	public void setCreator(IUser creator) {
		this.creator = creator;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setJson(JsonTyped json) {
		this.json = json;
	}

	public WorkItem(String id, String parentId, IStoredObjectReference mainHandle, Set<IStoredObjectReference> otherHandles,
			IUser creator, Long timestamp, String mediaType,
			String description, JsonTyped json) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.mainHandle = mainHandle;
        this.otherHandles = otherHandles;
		this.creator = creator;
		this.timestamp = timestamp;
		this.mediaType = mediaType;
		this.description = description;
		this.json = json;
	}

	public WorkItem() {
	}

	@Override
	public String toString() {
		return "WorkItem{" +
				"creator=" + creator +
				", id='" + id + '\'' +
				", parentId='" + parentId + '\'' +
				", mainHandle=" + mainHandle +
				", otherHandles=" + otherHandles +
				", timestamp=" + timestamp +
				", mediaType='" + mediaType + '\'' +
				", description='" + description + '\'' +
				", json=" + json +
				'}';
	}
}
