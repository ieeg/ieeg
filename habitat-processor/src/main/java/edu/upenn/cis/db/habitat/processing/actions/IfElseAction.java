/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.Set;

import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.conditionals.ITest;
import edu.upenn.cis.db.habitat.processing.parameters.IfElseParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

/**
 * Conditional in a workflow, triggers a given set of Actions if
 * the conditional is satisfied, else a different set if unsatisfied.
 * 
 * @author zives
 *
 */
public class IfElseAction extends BaseProcessingAction {
	
	public static final String NAME = "IfElseAction";
	
	boolean isSatisfied = false;
	
	IfElseParameters parameters;
	
	IfElseAction() {
		super(null, null);
	}

	public IfElseAction(IMessageQueue mQueue, WorkItemSet origin, 
			ITest condition, 
			Set<IProcessingAction> succeeds, 
			Set<IProcessingAction> fails) {
		super(mQueue, origin);
		
		parameters = new IfElseParameters(condition, succeeds, fails);
	}

	/**
	 * See if the test is satisfied, and determine which set of actions
	 * should be next.
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		if (arguments != null && arguments instanceof Parameters)
			parameters.merge((Parameters)arguments);

		isSatisfied = parameters.getTest().isSatisfied();
		
		System.out.println("If/Else took " + (isSatisfied ? " satisfied" : " unsatisfied"));
		return STATE.SUCCEEDED;
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
//		throw new RuntimeException("Cannot create a if/else w/o detailed specs");
		IfElseParameters ifelse = (IfElseParameters)arguments;
		return new IfElseAction(
				mQueue,
				origin, 
				ifelse.getTest(),
				ifelse.getIfSatisfied(),
				ifelse.getIfNotSatisfied());
	}

	@Override
	public Parameters getDefaultArguments() {
		return new IfElseParameters();
	}

	/**
	 * Return a set of actions depending on whether the
	 * condition was satisfied or not, in the <b>execute</b> method
	 */
	@Override
	public Set<IProcessingAction> getNextActions() {
		if (isSatisfied)
			return parameters.getIfSatisfied();
		else
			return parameters.getIfNotSatisfied();
	}
	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}
}
