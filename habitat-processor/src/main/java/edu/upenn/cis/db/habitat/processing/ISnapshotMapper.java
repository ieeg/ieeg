/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.io.IOException;

import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.braintrust.shared.exception.DuplicateNameException;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;

public interface ISnapshotMapper {
	/**
	 * Given a snapshot name, find its snapshot ID.  If none has been assigned yet,
	 * return null
	 * 
	 * @param user
	 * @param snapshotName
	 * @return
	 */
	public String getSnapshotIdFor(User user, String snapshotName);
	
	public SnapshotParameters getSnapshotParametersFor(User user, String snapshotName);

	/**
	 * Given a snapshot ID and a contribution name, register an associated object handle.
	 * 
	 * @param user
	 * @param snapshotId
	 * @param contrib
	 * @param handle
	 */
	public void registerHandleFor(User user, String snapshotId, String contrib,
			IStoredObjectReference handle) throws IOException, DuplicateNameException;
	
	/**
	 * Given a snapshot ID and a contribution name, retrieve the associated object handle.
	 * 
	 * @param user
	 * @param snapshotId
	 * @param contrib
	 * @return
	 */
	public IStoredObjectReference getHandleFor(User user, String snapshotId, String contrib);

	void registerSnapshotParametersFor(User user, String snapshotName,
			SnapshotParameters parms);
}
