/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.medtronic;

import javax.xml.bind.annotation.XmlElement;

public class GainList {
	String gain500;
	String gain1000;
	String gain2000;
	String gain4000;
	
	@XmlElement(name="Gain500")
	public String getGain500() {
		return gain500;
	}
	public void setGain500(String gain500) {
		this.gain500 = gain500;
	}
	@XmlElement(name="Gain1000")
	public String getGain1000() {
		return gain1000;
	}
	public void setGain1000(String gain1000) {
		this.gain1000 = gain1000;
	}
	@XmlElement(name="Gain2000")
	public String getGain2000() {
		return gain2000;
	}
	public void setGain2000(String gain2000) {
		this.gain2000 = gain2000;
	}
	@XmlElement(name="Gain4000")
	public String getGain4000() {
		return gain4000;
	}
	public void setGain4000(String gain4000) {
		this.gain4000 = gain4000;
	}
	
	
}
