package edu.upenn.cis.db.habitat.processing.workflows;

import java.io.Serializable;
import java.util.List;

import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.WorkCommand;

/**
 * v2 Workflow script...
 * 
 * The main workflow consists of a sequence of Steps that are interconnected by Inputs and Outputs.
 * There is also an ExecutionContext for when we execute, and a SnapshotContext for any snapshot
 * that is created / updated.
 * 
 * Goal: make this transactional and have a log!
 * 
 * @author Zack
 *
 */
public class Workflow implements Serializable {
	String name;
	List<Step> steps;
	boolean isBound = false;
	
	public void bind() {
		if (!isBound) {
			isBound = true;
			
			// TODO
		}
	}
	
	/**
	 * Execute the workflow, writing the results to the log and ultimately calling onComplete for
	 * success, or onFailure for failure.
	 * 
	 * @param context
	 * @param log
	 * @param onComplete
	 * @param onFailure
	 */
	public void execute(
			ExecutionContext context, 
			ExecutionLog log, 
			WorkCommand onComplete, 
			WorkCommand onFailure
			) {
		
		bind();
		// TODO.  Create and bind the actions.  Determine the snapshot(s) and their contexts.
		// Execute the steps.
		
		onComplete.execute();
	}
}
