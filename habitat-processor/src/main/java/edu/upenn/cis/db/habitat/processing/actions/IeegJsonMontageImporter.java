/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.EEGMontage;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * @author John Frommeyer
 *
 */
public class IeegJsonMontageImporter implements IMontageImporter {

	private static final class EEGMontageList {
		public List<EEGMontage> montages;
	}

	private final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

	@Override
	public List<EEGMontage> importMontages(User creator, DataSnapshot dataset,
			FileReference montageFileRef) throws MontageImportException {
		try {
			final IObjectServer bestServer = StorageFactory
					.getBestServerFor(montageFileRef);
			final List<EEGMontage> montages = new ArrayList<>();
			final IInputStream inStream = bestServer
					.openForInput(montageFileRef);
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(inStream.getInputStream()))) {
				final EEGMontageList read = mapper
						.readValue(reader, EEGMontageList.class);
				for (final EEGMontage montage : read.montages) {
					montages.add(montage);
				}
				return montages;
			} catch (AuthorizationException e) {
				throw new MontageImportException(e);
			} finally {
				if (inStream != null) {
					inStream.close();
				}
			}
		} catch (IOException e) {
			throw new MontageImportException(e);
		}

	}

}
