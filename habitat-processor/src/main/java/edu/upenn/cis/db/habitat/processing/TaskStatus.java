/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.util.Date;

import edu.upenn.cis.db.mefview.shared.JsonTyped;

public class TaskStatus implements JsonTyped {
	public enum STATE {NONE, NEW, DEFERRED, STARTED, FAILED, SUCCEEDED};
	
	STATE status;
	
	int retries = 0;
	long startTime;
	
	boolean ignore = false;
	
	public STATE getStatus() {
		return status;
	}
	
	public void setStatus(STATE state) {
		if (state == STATE.STARTED)
			startTime = (new Date()).getTime();
		status = state;
	}
	
	public TaskStatus() {
		startTime = (new Date()).getTime();
		status = STATE.NONE;
	}
	
	public TaskStatus(STATE state) {
		setStatus(state);
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}

}
