/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions.persyst;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.IObjectServer.IOutputStream;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.actions.AddToSnapshotAction;
import edu.upenn.cis.db.habitat.processing.actions.BaseSnapshotProcessingAction;
import edu.upenn.cis.db.habitat.processing.actions.IProcessingAction;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.habitat.processing.parameters.SnapshotParameters;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.IPersystParameters;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.PersystChannelMap;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.IStoredObjectReference;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.eeg.mef.MefHeader2;
import edu.upenn.cis.eeg.mef.MefWriter;

public class TranscodePersystDatFileAction extends BaseSnapshotProcessingAction {
        public static final String NAME = "TranscodePersystAction";
        
        List<? extends IPersystParameters> parmsList;
        
        double sampleFreq; // sampling frequency
        int waveformCount; // Number of channels
        double calibration; // VoltageConversionFactor 
                
        List<MefWriter> tempFiles;
        long[] timestamps;
        
        private final Logger logger = LoggerFactory.getLogger(getClass());

        String institution;
        
        public TranscodePersystDatFileAction() {}

        public TranscodePersystDatFileAction(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, SnapshotParameters ssp,
                List<? extends IPersystParameters> parmsList) {
            super(mQueue, origin, session, ssp);
            this.parmsList = parmsList;
            
            //TODO: Assert that all parms have same samplingrate, calibration, and number of channels.
            this.sampleFreq = parmsList.get(0).getSamplingRate();
            this.waveformCount = parmsList.get(0).getWaveformCount();
            this.calibration = parmsList.get(0).getCalibration();
            this.institution = ssp.getInstitution();
        }
        
        @SuppressWarnings("unused")
        private long process(List<PersystChannelMap> channels, int[] array) {
            
            int[] samps = new int[array.length / channels.size()];
            long[] stamps = new long[array.length / channels.size()];
            
                       
            int ichan = 0;
            for (PersystChannelMap channel: channels) {
                int mult = 1;
                
                int i = 0;
                for (int j = 0; j < array.length; j = j+channels.size()) {
                  samps[i] = (int) (Double.valueOf(array[j]) * (calibration * mult));
                                    
                  stamps[i] = timestamps[ichan];
                  timestamps[ichan] += Math.round(1.E6 / this.sampleFreq);
                  i++;
                }

                tempFiles.get(ichan).writeData(samps, stamps);
                ichan++;
            }
            return 0;
        }
//        
//        /**
//         * Converts month-day-year hours-minutes-seconds am/pm
//         * into microseconds since 1970
//         * 
//         * @param date
//         * @return
//         */
//        private long getUutc(String date) {
//            DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
//            DateTime dateTime = formatter.parseDateTime(date);
//            
//            return dateTime.getMillis() * 1000;
//        }
//        
//       /**
//        * Converts the date & time formed from replacing the year, month, and day
//        * from {@code date} with {@code newYear}, {@code newMonthOfYear}, and
//        * {@code newDayOfMonth} respectively into microseconds since 1970
//        * 
//        * @param date
//        * @return
//        */
//       private long getUutcWithNewDate(String date, int newYear, int newMonthOfYear, int newDayOfMonth) {
//           DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a");
//           DateTime dateTime = formatter.parseDateTime(date)
//                                        .withDate(newYear, newMonthOfYear, newDayOfMonth);
//           return dateTime.getMillis() * 1000;
//       }
//
//        /**
//         * Splits "xx Hz" and returns the xx
//         * 
//         * @param freqLabel
//         * @return
//         */
//        private double getFrequency(String freqLabel) {
//            String[] items = freqLabel.split(" ");
//            
//            return Double.valueOf(items[0]);
//        }
        
        private void createChannel(
                PersystChannelMap channel,
                String institution, 
                FileReference handle,
                List<IOutputStream> tempOutputs,
                List<IStoredObjectReference> tempHandles) throws IOException {
            String initalPrefixComponent = "Animal_Data/";
            if (getCurrentParameters() instanceof SnapshotParameters) {
                final boolean isHuman = ((SnapshotParameters)getCurrentParameters()).isHuman();
                if (isHuman) {
                    initalPrefixComponent = "Human_Data/";
                }
            }
            String name = initalPrefixComponent + handle.getObjectKey() + "/" + channel.getChannelIndex() + ".mef";
            FileReference newHandle = new FileReference(null,
                    name, name);
            System.out.println("Trying to create channel " + newHandle);
            IOutputStream output = StorageFactory.getPersistentServer().openForOutput(newHandle);
            FileOutputStream tempStream = (FileOutputStream)output.getOutputStream();
            tempOutputs.add(output);
            tempHandles.add(newHandle);
            
            MefHeader2 meh = new MefHeader2();
            
            meh.setChannelName(channel.getLabel() + channel.getChannelIndex());
            meh.setInstitution(institution);
            meh.setSamplingFrequency(this.sampleFreq);
            meh.setHeaderLength((short)1024);
            meh.setVoltageConversionFactor(this.calibration);

            tempFiles.add(
                new MefWriter(
                        tempStream, 
                        name,
                        meh,
                        10,
                        this.sampleFreq,
                        100000));
        }

        @Override
        public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) {
            final String m = "execute(...)";
            int fil = 0;
            if (parmsList.isEmpty())
                return STATE.SUCCEEDED;
                
            // List of files we'll need...
            
            // MEF Writers to each channel
            tempFiles = new ArrayList<MefWriter>(waveformCount);
            
            // The actual output file handles
            List<IStoredObjectReference> tempHandles = new ArrayList<IStoredObjectReference>(waveformCount);
            // The actual output file streams
            List<IOutputStream> tempOutputs = new ArrayList<IOutputStream>(waveformCount);

            // A timestamp cursor for each channel file
            timestamps = new long[waveformCount];
            long offsetMicros = 0;
            for (JsonTyped file: getCurrentParameters().getInputs()) {
                FileReference handle = (FileReference)file;
                IPersystParameters parms = parmsList.get(fil++);
                logger.debug("{}: transcoding file {}", m, handle);
                
                IObjectServer server = StorageFactory.getBestServerFor(handle);
                try {
                    IInputStream stream = server.openForInput(handle);
                    
                    int i = 0;
                    for (PersystChannelMap channel: parms.getChannels()) {

                        // Only create files for each channel for the first input!
                        if (fil == 1) {
                          createChannel(channel, this.institution, handle, tempOutputs, tempHandles);
                          offsetMicros = parms.findTimeOffset(); // Offset to start at 1/1/2000

                        }
                        // Set the timestamp to the start of the channel file
                        timestamps[i++] = parms.getFileTimeStamp() + offsetMicros;
                        System.out.println("Received timestamp: " + timestamps[i-1]);                        
                    }                
                    
                    long bufferLength = 100000;  //Max length of buffer which is written to MEF per cycle
                    ByteBuffer bbuf = ByteBuffer.allocate((int) bufferLength).order(ByteOrder.LITTLE_ENDIAN);
                    bbuf.rewind();
                    stream.getBytes(bbuf, bufferLength);
                    bbuf.rewind();
                    while (bbuf.remaining() > 0){
                      IntBuffer intBuf = bbuf.asIntBuffer();
                      bbuf.rewind();
                      long maxL = (bufferLength > intBuf.limit()) ?  intBuf.limit() : bufferLength ;  
                      intBuf.rewind();
                      int[] arr = new int[(int) maxL];
                                           
                      intBuf.get(arr, 0, arr.length);
                      process(parms.getChannels(), arr);
                      
                      stream.getBytes(bbuf, bufferLength);
                    }
                    
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return STATE.FAILED;
                }
            }
        for (MefWriter w : tempFiles) {
            w.close();
        }
        for (IOutputStream s : tempOutputs) {
            try {
                s.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        ParametersForSnapshotContent newParms = new ParametersForSnapshotContent();

        for (IStoredObjectReference s : tempHandles) {
            newParms.getInputs().add(s);
        }
        newParms.setTargetSnapshot(getCurrentParameters().getTargetSnapshot());
        newParms.setTargetSnapshotId(getCurrentParameters()
                .getTargetSnapshotId());

        getNextActions().add(
                new AddToSnapshotAction(getMessageQueue(), this.getWorkItemSet(),
                        getSession(),
                        newParms));

        return STATE.SUCCEEDED;
    }


        @Override
        public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin, RemoteHabitatSession session, Parameters arguments) {
            return new TranscodePersystDatFileAction(mQueue, origin, session, (SnapshotParameters)arguments, null);
        }

        @Override
        public ParametersForSnapshotContent getDefaultArguments() {
            return null;
        }

    }
