/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.parameters.neuropace;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;

import org.ini4j.Wini;

import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.processing.parameters.persyst.PersystParameters;
import edu.upenn.cis.db.mefview.shared.FileReference;

public class NeuropaceParameters extends PersystParameters {
  
  long eCogFileStamp; //ECog time is in 100-nanosecond intervals since jan 1, 1601.
  long timeZoneOffset; // in uSsec
  long preBuf; // recording start offset from recording event timestamp. in usec
  String patientID;
  
  static Long ZEROTIME = Long.valueOf("11644473600000000"); // offset between 1/1/1601 and 1/1/1970 in usec.
  
  public NeuropaceParameters(FileReference inFile) throws IOException{
    super(inFile);
        
    IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);
    
    IInputStream inStream = bestServer.openForInput(inFile);
    
    BufferedInputStream inFStream = 
                new BufferedInputStream(inStream.getInputStream());
    
    Wini ini = new Wini(inFStream);
    inFStream.close();
    inStream.close();
    
    this.seteCogFileStamp(ini.get("NP_FileInfo", "ECoGTimeStampAsUTC", long.class));
    this.setTimeZoneOffset((long) (ini.get("NP_FileInfo", "ECoGTimeStampOffsetMinutesFromGMT", long.class) * 60 * 1e6));
    this.setPreBuf((long) (ini.get("NP_Parameters", "NPPreTriggerBufferLength", long.class) * 1e6));
    
    File file = new File(inFile.getObjectKey());
    String pID = file.getParent().substring(file.getParent().lastIndexOf(File.separator) + 1);//page01   
    this.setPatientID(pID);
    System.out.println("Lay Patient ID: " + this.getPatientID());
    System.out.println("Lay File Timestamp: " + this.geteCogFileStamp());
    System.out.println("Lay File calibration: " + this.getCalibration());
    System.out.println("ChannelMap: " + this.getChannels());
    
    
  }
  
  public long geteCogFileStamp() {
    return eCogFileStamp;
  }
  private void seteCogFileStamp(long eCogFileStamp) {
    this.eCogFileStamp = eCogFileStamp;
  }

  public String getPatientID() {
    return patientID;
  }
  private void setPatientID(String patientID) {
    this.patientID = patientID;
  }

  @Override
  public Long findTimeOffset() {
    
    return Long.valueOf(0);
  }

  @Override
  public Long getFileTimeStamp() {
   
    long start2 = eCogFileStamp - ZEROTIME + timeZoneOffset; // time in usec since 1/1/1970;
    double hoursMod = start2%(1e6*3600*24);
    
    
    
   
    return this.eCogFileStamp/10 ;
  }

  public long getTimeZoneOffset() {
    return timeZoneOffset;
  }

  private void setTimeZoneOffset(long timeZoneOffset) {
    this.timeZoneOffset = timeZoneOffset;
  }

  public long getPreBuf() {
    return preBuf;
  }

  private void setPreBuf(long preBuf) {
    this.preBuf = preBuf;
  }
  

}
