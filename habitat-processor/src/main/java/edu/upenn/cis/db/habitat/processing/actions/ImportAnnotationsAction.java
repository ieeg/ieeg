/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.habitat.processing.parameters.ParametersForSnapshotContent;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.MimeTypeRecognizer;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

public class ImportAnnotationsAction extends BaseSnapshotProcessingAction {
	public static final String NAME = "ImportAnnotationsAction";
	private static final int maxDeferrals = 10;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private int deferralCount = 0;
	private final List<TsAnnotationDto> annotations = new ArrayList<>();

	public ImportAnnotationsAction(
			IMessageQueue mQueue,
			WorkItemSet origin,
			RemoteHabitatSession session,
			ParametersForSnapshotContent parameters) {
		super(mQueue, origin, session, parameters);
	}

	public ImportAnnotationsAction(
			IMessageQueue mQueue,
			WorkItemSet origin,
			RemoteHabitatSession session,
			ParametersForSnapshotContent parameters,
			List<TsAnnotationDto> annotations) {
		super(mQueue, origin, session, parameters);
		this.annotations.addAll(annotations);
	}

	public ImportAnnotationsAction() {
		super();
	}

	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived)
			throws Exception {
		final String m = "execute(...)";
		try {
			final String datasetName = getCurrentParameters()
					.getTargetSnapshot();
			final String datasetId = getCurrentParameters()
					.getTargetSnapshotId();
			final User creator = (User) getCreator();

			logger.debug(
					"{}: creator: [{}], dataset: [{}], dataset id: [{}]",
					m,
					creator.getUsername(),
					datasetName,
					datasetId);
			final DataSnapshot dataset = session.getSnapshotFor(creator,
					datasetId);
			if (dataset == null || dataset.getTimeSeries().isEmpty()) {
				deferralCount++;
				if (deferralCount <= maxDeferrals) {
					logger.info("{}: Deferral {} of {} for {}",
							m,
							deferralCount,
							maxDeferrals,
							datasetName);
					return STATE.DEFERRED;
				}
				logger.error("{}: No more deferrals for {}",
						m,
						datasetName);
				return STATE.FAILED;
			}
			// Add timeseries to any annotations we were given in the
			// constructor if necessary
			for (final TsAnnotationDto annotation : annotations) {
				if (annotation.getAnnotated().isEmpty()) {
					annotation.getAnnotated().addAll(dataset.getTimeSeries());
				}
			}

			for (JsonTyped item : parameters.getInputs()) {
				final FileReference handle = (FileReference) item;
				final Optional<IAnnotationImporter> importer = newAnnotationImporter(handle);
				if (!importer.isPresent()) {
					logger.info("{}: Ignoring non-annotation file {} for {}",
							m,
							handle,
							datasetName);
					continue;
				}
				logger.info("{}: Processing annotation file {} for {}",
						m,
						handle,
						datasetName);
				final List<TsAnnotationDto> importedAnnotations = importer
						.get().importAnnotations(
								creator,
								dataset,
								handle);
				this.annotations.addAll(importedAnnotations);

			}
			if (annotations.size() > 0) {
				logger.info("{}: Saving {} annotations", m,
						annotations.size());
				session.saveAnnotations(
						creator,
						datasetId,
						annotations);
			}
			return STATE.SUCCEEDED;
		} catch (RuntimeException e) {
			logger.error(m + ": task failed", e);
			return STATE.FAILED;
		}
	}

	private Optional<IAnnotationImporter> newAnnotationImporter(
			FileReference handle) {
		if (RegisterActions.getMimeType(handle).equals(
				"application/x-natus-annotations")) {
			final IAnnotationImporter importer = new NatusAnnotationImporter();
			return Optional.of(importer);
		}
		if (RegisterActions.getMimeType(handle).equals(
				MimeTypeRecognizer.GRASS_TWIN_ANNOTATION_MIME_TYPE_NAME)) {
			final IAnnotationImporter importer = new GrassTwinAnnotationImporter();
			return Optional.of(importer);
		}
		if (RegisterActions.getMimeType(handle).equals(
				MimeTypeRecognizer.IEEG_JSON_ANNOTATION_MIME_TYPE_NAME)) {
			final IAnnotationImporter importer = new IeegJsonAnnotationImporter();
			return Optional.of(importer);
		}
		return Optional.absent();
	}

	@Override
	public IProcessingAction create(
			IMessageQueue mQueue,
			WorkItemSet origin,
			RemoteHabitatSession session,
			Parameters arguments) {
		return new ImportAnnotationsAction(
				mQueue,
				origin,
				session,
				(ParametersForSnapshotContent) arguments);
	}

	@Override
	public ParametersForSnapshotContent getDefaultArguments() {
		return new ParametersForSnapshotContent();
	}

	@Override
	public ParametersForSnapshotContent getCurrentParameters() {
		return parameters;
	}
}
