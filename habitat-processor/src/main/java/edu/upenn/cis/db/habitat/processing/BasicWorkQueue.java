package edu.upenn.cis.db.habitat.processing;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.upenn.cis.braintrust.persistence.HibernateUtil;
import edu.upenn.cis.braintrust.security.IUserService;
import edu.upenn.cis.braintrust.security.UserServiceFactory;
import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;
import edu.upenn.cis.db.mefview.shared.JsonTyped;

public abstract class BasicWorkQueue implements IWorkQueue {
	protected LinkedList<WorkItemSet> queue;
	protected final IUserService userService = UserServiceFactory
			.getUserService();
	protected Logger logger = LoggerFactory.getLogger(getClass());

	public BasicWorkQueue() {
		queue = new LinkedList<WorkItemSet>();
//		loadFromExternal();
	}
	
	public abstract boolean isPeekable();

	public boolean hasNext(
			int maxLoadingAttempts,
			int secondsBetweenAttempts)
			throws InterruptedException {
		checkArgument(maxLoadingAttempts >= 0);
		checkArgument(secondsBetweenAttempts >= 0);
		for (int i = 0; i < maxLoadingAttempts; i++) {
			if (!queue.isEmpty()) {
				return true;
			}
			TimeUnit.SECONDS.sleep(secondsBetweenAttempts);
			loadFromExternal();
		}
		return !queue.isEmpty();
	}

	/**
	 * Returns item @ head of queue, without popping it. Returns null if no
	 * item.
	 * 
	 * @return
	 */
	public WorkItemSet peek() {
		final WorkItemSet next = queue.peek();
		if (next != null) {
			return next;
		} else {
			if (isPeekable())
				loadFromExternal();
			return queue.peek();
		}
	}

	public WorkItemSet getNext() {
		final WorkItemSet next = queue.poll();
		if (next != null) {
			return next;
		} else {
			loadFromExternal();
			return queue.poll();
		}

	}

	public WorkItemSet add(Set<? extends IWorkItem> item, String description,
			JsonTyped parameters) {
		WorkItemSet set = new WorkItemSet(item, description, parameters); 
		queue.add(set);
		return set;
	}

	public WorkItemSet add(WorkItemSet item) {
		queue.add(item);
		return item;
	}

	public WorkItemSet add(IWorkItem item) {
		HashSet<IWorkItem> itemSet = new HashSet<IWorkItem>();
		itemSet.add(item);
		WorkItemSet set = new WorkItemSet(itemSet, item.getDescription(), item
				.getJson()); 
		final WorkerRequest req = new WorkerRequest(
				WorkerRequest.PORTAL,
				WorkerRequest.ETL, 
				item.getItemId(), 
				null, 
				item.getMediaType(), 
				item.getMainHandle().getObjectKey(), 
				item.getMainHandle().getFilePath(), 
				null, 
				item.getCreator().getUsername(), 
				item.getMainHandle().isContainer());
		set.setRequest(req);
		set.setOrigin(this);
		queue.add(set);
		return set;
	}

	public abstract void loadFromExternal();

	public String getMediaType(String fileKey) {
		String mediaType = "application/control";
		if (fileKey.endsWith("/")
				|| fileKey.endsWith("/.")
				|| fileKey.endsWith("/*")) {
			mediaType = "application/directory";
		}
		return mediaType;
	}
}
