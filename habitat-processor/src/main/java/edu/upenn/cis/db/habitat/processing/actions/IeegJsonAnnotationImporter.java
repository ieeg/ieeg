/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.collect.Iterables.find;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.braintrust.security.AuthorizationException;
import edu.upenn.cis.braintrust.shared.DataSnapshot;
import edu.upenn.cis.braintrust.shared.TimeSeriesDto;
import edu.upenn.cis.braintrust.shared.TsAnnotationDto;
import edu.upenn.cis.braintrust.shared.User;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;

/**
 * @author John Frommeyer
 *
 */
public class IeegJsonAnnotationImporter implements IAnnotationImporter {

	private static final class TsAnnotationList {
		public List<TsAnnotationDto> annotations;
	}

	private final ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public List<TsAnnotationDto> importAnnotations(
			User creator,
			DataSnapshot dataset,
			FileReference annotationFileRef)
			throws AnnotationImportException {
		final String m = "importAnnotations(...)";
		try {
			final IObjectServer bestServer = StorageFactory
					.getBestServerFor(annotationFileRef);
			final List<TsAnnotationDto> annotations = new ArrayList<>();
			final IInputStream inStream = bestServer
					.openForInput(annotationFileRef);
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(inStream.getInputStream()))) {
				final TsAnnotationList read = mapper
						.readValue(reader, TsAnnotationList.class);
				for (final TsAnnotationDto ann : read.annotations) {
					final Set<TimeSeriesDto> actualAnnotated = new HashSet<>();
					for (final TimeSeriesDto annotatedTs : ann
							.getAnnotated()) {
						final String annotatedLabel = annotatedTs.getLabel();
						final TimeSeriesDto actualAnnotatedTs = find(
								dataset.getTimeSeries(),
								compose(equalTo(annotatedLabel),
										TimeSeriesDto.getLabel), null);
						if (actualAnnotatedTs != null) {
							actualAnnotated.add(actualAnnotatedTs);
						} else {
							logger.warn(
									"{}: Cannot find annotated channel: [{}] in dataset. Ignoring.",
									m, annotatedLabel);
						}
					}
					if (actualAnnotated.isEmpty()) {
						ann.setAnnotated(dataset.getTimeSeries());
					} else {
						ann.setAnnotated(actualAnnotated);
					}

					annotations.add(ann);
				}
				return annotations;
			} catch (AuthorizationException e) {
				throw new AnnotationImportException(e);
			} finally {
				if (inStream != null) {
					inStream.close();
				}
			}
		} catch (IOException e) {
			throw new AnnotationImportException(e);
		}
	}

}
