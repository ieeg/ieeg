package edu.upenn.cis.db.habitat.processing.parameters;

import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.processing.IWorkQueue;

/**
 * Parameters for notifying original requestor
 * 
 * @author Zack
 *
 */
public class NotificationParameters extends Parameters {
	WorkerRequest originalRequest;
	IWorkQueue workQueue;
	
	public NotificationParameters(IWorkQueue queue, WorkerRequest request) {
		workQueue = queue;
		originalRequest = request;
	}

	public WorkerRequest getOriginalRequest() {
		return originalRequest;
	}

	public void setOriginalRequest(WorkerRequest originalRequest) {
		this.originalRequest = originalRequest;
	}

	public IWorkQueue getWorkQueue() {
		return workQueue;
	}

	public void setWorkQueue(IWorkQueue workQueue) {
		this.workQueue = workQueue;
	}
	
	
}
