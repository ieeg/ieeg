package edu.upenn.cis.db.habitat.processing.workflows;

import java.io.Serializable;

public abstract class Pipe implements Serializable {
	String name;
	
	public abstract void writeJson(String json);
	
	public abstract String getJson();
	
	public abstract void writeBytes(byte[] bytes);
	
	public abstract byte[] getBytes(int no);
}
