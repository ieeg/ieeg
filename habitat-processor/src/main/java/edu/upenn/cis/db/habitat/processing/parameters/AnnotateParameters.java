/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.parameters;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.PresentableMetadata;
import edu.upenn.cis.db.mefview.shared.SignalProcessingStep;
import edu.upenn.cis.db.mefview.shared.parameters.LineLengthEchauzParameters;

public class AnnotateParameters extends ParametersForSnapshotContent {
	public int threshold = 10000;
	
	SignalProcessingStep step;
	
	public List<PresentableMetadata> dataItems;// = new ArrayList<PresentableMetadata>();
	
	public AnnotateParameters() {
		this(new SignalProcessingStep("LL_echauz", 
				new LineLengthEchauzParameters(10000, 200, 200, 50)), 400, 
				new ArrayList<PresentableMetadata>());
	}
	
	public AnnotateParameters(SignalProcessingStep step, int threshold, List<PresentableMetadata> items) {
		this.step = step;
		this.threshold = threshold;
		this.dataItems = items;
	}
	
	
	public SignalProcessingStep getStep() {
		return step;
	}
}
