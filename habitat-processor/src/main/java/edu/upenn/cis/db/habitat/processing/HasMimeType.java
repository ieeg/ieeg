/*
 * Copyright 2015 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing;

import java.util.Set;

import edu.upenn.cis.db.habitat.processing.actions.RegisterActions;
import edu.upenn.cis.db.mefview.shared.IWorkItem;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * Match on mime (media) type
 * 
 * @author zives
 *
 */
public class HasMimeType implements IWorkFilter {
	String type;
	
	public HasMimeType(String typ) {
		this.type = typ.toLowerCase();
	}

	@Override
	public boolean matches(IWorkItem item, ITaskTracker otherTasks) {		// TODO Auto-generated method stub
		return item.getMediaType().equals(type) ||
				RegisterActions.getMimeType(item.getMainHandle()).equals(type);
	}

	@Override
	public boolean matchesSet(WorkItemSet items, ITaskTracker otherTasks) {
		for (IWorkItem item: items) {
			if (!item.getMediaType().equals(type) &&
					!RegisterActions.getMimeType(item.getMainHandle()).equals(type))
				return false;
		}
		
		return true;
	}

	@Override
	public boolean matchesSet(Set<String> items, ITaskTracker otherTasks) {
		for (String item: items) {
			if (!item.equals(type))
				return false;
		}
		
		return true;
	}

}
