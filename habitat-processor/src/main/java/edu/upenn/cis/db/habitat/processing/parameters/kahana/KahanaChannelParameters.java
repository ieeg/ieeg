package edu.upenn.cis.db.habitat.processing.parameters.kahana;

import com.jmatio.types.MLArray;
import com.jmatio.types.MLChar;
import com.jmatio.types.MLDouble;
import com.jmatio.types.MLStructure;

public class KahanaChannelParameters {
  private String tagName;
  private double channelIdx;
  private String groupName;
  private double xCoord;
  private double yCoord;
  private double zCoord;
  private String loc1;
  private String loc2;
  private String loc3;
  private String loc4;
  private String loc5;
  private String loc6;
  private String montage;
  private String eType;
  private String eName;
  
  public KahanaChannelParameters(MLStructure struct, int index){ 
	  // struct can be a single structure or an array of structures
    
	  MLArray var = struct.getField("tagName",index);
	  if (var.getSize() > 0) {tagName = removeWhiteSpace(((MLChar) var).getString(0)); }	  
	  var = struct.getField("channel",index);
	  if (var.getSize() > 0) {channelIdx = ((MLDouble) var).get(0);}
	  var = struct.getField("grpName",index);
	  if (var.getSize() > 0) {groupName = removeWhiteSpace(((MLChar) var).getString(0)); }	  
	  
	  var = struct.getField("x",index);
	  if (var.getSize() > 0) {xCoord = ((MLDouble) var).get(0);}  
	  var = struct.getField("y",index);
	  if (var.getSize() > 0) {yCoord = ((MLDouble) var).get(0);}
	  var = struct.getField("z",index);
	  if (var.getSize() > 0) {zCoord = ((MLDouble) var).get(0);}
	  
	  var = struct.getField("Loc1",index);
	  if (var.getSize() > 0) {loc1 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("Loc2",index);
	  if (var.getSize() > 0) {loc2 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("Loc3",index);
	  if (var.getSize() > 0) {loc3 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("Loc4",index);
	  if (var.getSize() > 0) {loc4 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("Loc5",index);
	  if (var.getSize() > 0) {loc5 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("Loc6",index);
	  if (var.getSize() > 0) {loc6 = removeWhiteSpace(((MLChar) var).getString(0)); }
	  
	  var = struct.getField("Montage",index);
	  if (var.getSize() > 0) {montage = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("eType",index);
	  if (var.getSize() > 0) {eType = removeWhiteSpace(((MLChar) var).getString(0)); }
	  var = struct.getField("eName",index);
	  if (var.getSize() > 0) {eName = removeWhiteSpace(((MLChar) var).getString(0)); }
    
  }
  
  private String removeWhiteSpace(String input){
	  input = input.replaceAll("\\t", "");
	  input = input.replaceAll("\\n", "");
	  return input;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public double getChannelIdx() {
    return channelIdx;
  }

  public void setChannelIdx(double channelIdx) {
    this.channelIdx = channelIdx;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public double getxCoord() {
    return xCoord;
  }

  public void setxCoord(double xCoord) {
    this.xCoord = xCoord;
  }

  public double getyCoord() {
    return yCoord;
  }

  public void setyCoord(double yCoord) {
    this.yCoord = yCoord;
  }

  public double getzCoord() {
    return zCoord;
  }

  public void setzCoord(double zCoord) {
    this.zCoord = zCoord;
  }

  public String getLoc1() {
    return loc1;
  }

  public void setLoc1(String loc1) {
    this.loc1 = loc1;
  }

  public String getLoc2() {
    return loc2;
  }

  public void setLoc2(String loc2) {
    this.loc2 = loc2;
  }

  public String getLoc3() {
    return loc3;
  }

  public void setLoc3(String loc3) {
    this.loc3 = loc3;
  }

  public String getLoc4() {
    return loc4;
  }

  public void setLoc4(String loc4) {
    this.loc4 = loc4;
  }

  public String getLoc5() {
    return loc5;
  }

  public void setLoc5(String loc5) {
    this.loc5 = loc5;
  }

  public String getLoc6() {
    return loc6;
  }

  public void setLoc6(String loc6) {
    this.loc6 = loc6;
  }

  public String getMontage() {
    return montage;
  }

  public void setMontage(String montage) {
    this.montage = montage;
  }

  public String geteType() {
    return eType;
  }

  public void seteType(String eType) {
    this.eType = eType;
  }

  public String geteName() {
    return eName;
  }

  public void seteName(String eName) {
    this.eName = eName;
  }
  
  
  

}
