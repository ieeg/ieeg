/*******************************************************************************
 * Copyright 2014 Trustees of the University of Pennsylvania
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package edu.upenn.cis.db.habitat.processing.actions;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import edu.upenn.cis.braintrust.shared.WorkerRequest;
import edu.upenn.cis.db.habitat.io.IObjectServer;
import edu.upenn.cis.db.habitat.io.IObjectServer.IInputStream;
import edu.upenn.cis.db.habitat.io.StorageFactory;
import edu.upenn.cis.db.habitat.processing.IMessageQueue;
import edu.upenn.cis.db.habitat.processing.IWorkQueue;
import edu.upenn.cis.db.habitat.processing.RemoteHabitatSession;
import edu.upenn.cis.db.habitat.processing.SnapshotProcessorDaemon.DerivedItem;
import edu.upenn.cis.db.habitat.processing.TaskStatus.STATE;
import edu.upenn.cis.db.habitat.processing.parameters.ControlFileParameters;
import edu.upenn.cis.db.habitat.processing.parameters.NotificationParameters;
import edu.upenn.cis.db.habitat.processing.parameters.Parameters;
import edu.upenn.cis.db.mefview.server.ObjectMapperFactory;
import edu.upenn.cis.db.mefview.shared.FileReference;
import edu.upenn.cis.db.mefview.shared.JsonTyped;
import edu.upenn.cis.db.mefview.shared.WorkItemSet;

/**
 * Reads the contents of a .control file and executes a workflow
 * 
 * @author zives
 *
 */
public class ReadControlFileAction extends BaseProcessingAction {

	public static final String NAME = "ReadControlFileAction";

//	WorkItemSet workItems = new WorkItemSet("Work item set", null);

	private ObjectMapper mapper = ObjectMapperFactory.getObjectMapper();

	RemoteHabitatSession session;
	ControlFileParameters parameters;

	Set<IProcessingAction> nextActions = new HashSet<IProcessingAction>();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public ReadControlFileAction(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session,
			ControlFileParameters parameters) {
		super(mQueue, origin);
		this.session = session;
		this.parameters = parameters;
	}

	ReadControlFileAction() {
		super(null, null);
	}

	/**
	 * For each control file in getInputs(), process the snapshot associated with
	 * that file
	 */
	@Override
	public STATE execute(JsonTyped arguments, Set<DerivedItem> derived) throws Exception {
		final String m = "execute(...)";
		if (arguments != null && parameters != null
				&& arguments instanceof Parameters)
			parameters.merge((Parameters) arguments);
		else if (arguments != null)
			parameters = (ControlFileParameters) arguments;

		for (JsonTyped inFileSpec : parameters.getInputs()) {
			System.out.println("ReadControlFile: " + inFileSpec.toString());
			FileReference inFile = (FileReference) inFileSpec;

			IObjectServer bestServer = StorageFactory.getBestServerFor(inFile);

			IInputStream inStream = bestServer.openForInput(inFile);

			try {
				BufferedInputStream inFStream =
						new BufferedInputStream(inStream.getInputStream());

				ControlFileParameters myParms =
						mapper.readValue(inFStream, ControlFileParameters.class);
				if (logger.isTraceEnabled()) {
					final ObjectWriter ow = ObjectMapperFactory.newPasswordFilteredWriter();
					final String controlFileString = ow
							.writeValueAsString(myParms);
					logger.trace("{}: Read control file [{}]", m,
							controlFileString);
				}
				inFStream.close();
				inStream.close();
				processSnapshot(myParms);

			} catch (IOException ioe) {
				ioe.printStackTrace();
				getMessageQueue().logError(getCreator(), WorkerRequest.PORTAL, WorkerRequest.ETL, getWorkItemSet(), 
						"Error processing data submission: " + ioe.getMessage());
				return STATE.FAILED;
			}
		}
		return STATE.SUCCEEDED;
	}

	/**
	 * Take the ControlFileParameters, create a snapshot, then map the metadata,
	 * then process the contents. (Each of these is done by an action chained
	 * form one to the next to the next.)
	 * 
	 * @param parameters
	 */
	private void processSnapshot(ControlFileParameters parameters) {
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// ReadControlFile --> CreateSnapshot --> MapMetadata --> ProcessSnapshotContents --> SendComplete
		//													  --> GrantPermissions
		//
		
		
		// Create snapshot as necessary
		IProcessingAction snap = new CreateSnapshotAction(
				getMessageQueue(), 
				this.getWorkItemSet(),
				session,
				parameters.getSnapshotParameters());

		getNextActions().add(snap);

		// Map metadata to it as necessary
		IProcessingAction map = new MapMetadataAction(
				getMessageQueue(), 
				this.getWorkItemSet(),
				session, parameters.getSnapshotParameters());

		snap.getNextActions().add(map);

		// Process the contents
		parameters.setTargetSnapshot(parameters.getSnapshotParameters()
				.getTargetSnapshot());
		IProcessingAction next = new ProcessSnapshotContentsAction(
				getMessageQueue(),
				getWorkItemSet(),
				session,
				parameters);

		map.getNextActions().add(next);

		// Grant permissions
		IProcessingAction perms = new GrantPermissionsAction(
				getMessageQueue(), 
				getWorkItemSet(),
				session,
				parameters.getSnapshotParameters());
		map.getNextActions().add(perms);
		
		parameters.getSnapshotParameters().setNotificationParms(
				new NotificationParameters((IWorkQueue)getWorkItemSet().getOrigin(), 
						(WorkerRequest)getWorkItemSet().getRequest()));
		
		IProcessingAction notify = new SendCompleteAction(getMessageQueue(), getWorkItemSet(), session,
				parameters.getSnapshotParameters());
		next.getNextActions().add(notify);
	}

	@Override
	public IProcessingAction create(IMessageQueue mQueue, WorkItemSet origin,
			RemoteHabitatSession session, Parameters arguments) {
		return new ReadControlFileAction(mQueue, origin, session,
				(ControlFileParameters) arguments);
	}

	@Override
	public Parameters getDefaultArguments() {
		return new ControlFileParameters();
	}

	@Override
	public Set<IProcessingAction> getNextActions() {
		return nextActions;
	}

	@Override
	public Parameters getCurrentParameters() {
		return parameters;
	}
}
