/*
 * Copyright 2019 Trustees of the University of Pennsylvania
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.db.habitat.processing.actions;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PushbackInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;

import edu.upenn.cis.db.habitat.processing.actions.IAnnotationImporter.AnnotationImportException;

/**
 * @author John Frommeyer
 *
 */
public final class NatusTextAnnotations {

	public static final class NatusTextAnnotation {
		public final int day;
		public final int hourOfDay;
		public final int minute;
		public final int second;
		public final String text;

		public NatusTextAnnotation(int day,
				int hourOfDay,
				int minute,
				int second,
				String text) {
			checkArgument(day >= 1);
			this.day = day;
			checkArgument(hourOfDay >= 0);
			this.hourOfDay = hourOfDay;
			checkArgument(minute >= 0);
			this.minute = minute;
			checkArgument(second >= 0);
			this.second = second;
			this.text = text.trim();
		}

		@Override
		public String toString() {
			return "NatusTextAnnotation [day=" + day + ", time="
					+ hourOfDay + ":" + minute + ":" + second
					+ ", text=" + text + "]";
		}

	}

	private final static Logger logger = LoggerFactory
			.getLogger(NatusTextAnnotations.class);
	private static final int bomSize = 2;
	private static final String startTimeLinePrefix = "Creation Date:";
	private static final SimpleDateFormat startTimeFormat = new SimpleDateFormat(
			"HH:mm:ss MMM dd, yyyy");
	private static final Pattern annotationLinePattern = Pattern
			.compile("^\\s*d(\\d)\\s+(\\d{2}):(\\d{2}):(\\d{2})\\s+(\\S.*)$");
	private static final String recordingStartTimeText = "Beginning of Recording";

	private final Calendar creationTime;
	private final List<NatusTextAnnotation> textAnnotations;

	public NatusTextAnnotations(Calendar creationTime,
			List<NatusTextAnnotation> textAnnotations) {
		this.creationTime = checkNotNull(creationTime);
		ImmutableList.Builder<NatusTextAnnotation> builder = ImmutableList
				.builder();
		builder.addAll(textAnnotations);
		this.textAnnotations = builder.build();
	}

	public Calendar getCreationTime() {
		return creationTime;
	}

	public List<NatusTextAnnotation> getTextAnnotations() {
		return textAnnotations;
	}

	public Calendar getRecordingStartTime() {

		final NatusTextAnnotation recordingStartAnn = textAnnotations.isEmpty()
				? null
				: textAnnotations.get(0);
		if (recordingStartAnn == null
				|| !recordingStartAnn.text.equals(recordingStartTimeText)) {
			return creationTime;
		}
		final Calendar recordingStartTime = (Calendar) creationTime.clone();
		recordingStartTime.set(Calendar.HOUR_OF_DAY,
				Integer.valueOf(recordingStartAnn.hourOfDay));
		recordingStartTime.set(Calendar.MINUTE,
				Integer.valueOf(recordingStartAnn.minute));
		recordingStartTime.set(Calendar.SECOND,
				Integer.valueOf(recordingStartAnn.second));
		if (recordingStartAnn.day > 1) {
			final int increment = recordingStartAnn.day - 1;
			recordingStartTime.add(Calendar.DAY_OF_MONTH, increment);
		}
		return recordingStartTime;

	}

	public static NatusTextAnnotations read(InputStream inStream)
			throws AnnotationImportException {
		final String m = "read(...)";
		try (PushbackInputStream pushback = new PushbackInputStream(
				inStream,
				bomSize)) {
			final Charset charset = getCharset(pushback);
			logger.debug("{}: Using charset {} to read Natus annotation file",
					m,
					charset);
			try (final BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							pushback,
							charset))) {
				final List<NatusTextAnnotation> natusNotes = new ArrayList<>();
				Date startTime = null;
				String line = null;
				while ((line = reader.readLine()) != null) {
					final int index = line.indexOf(startTimeLinePrefix);
					if (index != -1) {
						final String startTimeStr = line.substring(index +
								startTimeLinePrefix.length()).trim();
						startTime = startTimeFormat.parse(startTimeStr);
						logger.debug(
								"{}: Creation Date: [{}]",
								m,
								startTime);
					} else {
						final Matcher annMatcher = annotationLinePattern
								.matcher(line);
						if (annMatcher.matches()) {
							final int day = Integer.parseInt(annMatcher
									.group(1));
							final int hourOfDay = Integer.parseInt(annMatcher
									.group(2));
							final int minute = Integer.parseInt(annMatcher
									.group(3));
							final int second = Integer.parseInt(annMatcher
									.group(4));
							final String text = annMatcher.group(5);
							final NatusTextAnnotation textAnn = new NatusTextAnnotation(
									day, hourOfDay, minute, second, text);
							natusNotes.add(textAnn);
						}
					}

				}
				final Calendar createTime = Calendar.getInstance();
				createTime.setTime(startTime);
				return new NatusTextAnnotations(createTime, natusNotes);
			}
		} catch (IOException | ParseException e) {
			throw new AnnotationImportException(e);
		}
	}

	private static Charset getCharset(PushbackInputStream pushback)
			throws IOException {
		// So far we've only seen .ent.txt files as UTF-8 with no BOM or
		// UTF-16LE with a BOM
		final byte[] bomBuffer = new byte[bomSize];
		ByteStreams.readFully(pushback, bomBuffer);
		pushback.unread(bomBuffer);

		if (bomBuffer[0] == (byte) 0xFF && bomBuffer[1] == (byte) 0xFE) {
			return StandardCharsets.UTF_16LE;
		}

		if (bomBuffer[0] == (byte) 0xFE && bomBuffer[1] == (byte) 0xFF) {
			return StandardCharsets.UTF_16BE;
		}

		return StandardCharsets.UTF_8;
	}

}
