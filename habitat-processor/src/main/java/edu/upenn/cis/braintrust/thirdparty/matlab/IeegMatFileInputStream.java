package edu.upenn.cis.braintrust.thirdparty.matlab;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.google.common.io.ByteStreams;
import com.jmatio.common.MatDataTypes;
import com.jmatio.types.ByteStorageSupport;

/**
 * MAT-file input stream class. 
 * 
 * @author Wojciech Gradkowski <wgradkowski@gmail.com>
 */
public class IeegMatFileInputStream
{
    private final int type;
    private final InputStream st;
    private final ByteBuffer transferBuffer = ByteBuffer.allocate(8);
    
    /**
     * Attach MAT-file input stream to <code>InputStream</code>
     * 
     * @param is - input stream
     * @param type - type of data in the stream
     * @see com.jmatio.common.MatDataTypes
     */
    public IeegMatFileInputStream( InputStream st, int type, ByteOrder order )
    {
        this.type = type;
        this.st = st;
        transferBuffer.order(order);
    }
    
    /**
     * Reads data (number of bytes red is determined by <i>data type</i>)
     * from the stream to <code>int</code>.
     * 
     * @return
     * @throws IOException
     */
    public int readInt() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (int)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (int) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (int)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (int) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (int)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (int) transferBuffer.getInt();
            case MatDataTypes.miUINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (int) transferBuffer.getLong();
            case MatDataTypes.miINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (int) transferBuffer.getLong();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (int) transferBuffer.getDouble();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }
    /**
     * Reads data (number of bytes red is determined by <i>data type</i>)
     * from the stream to <code>char</code>.
     * 
     * @return - char
     * @throws IOException
     */
    public char readChar() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (char)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (char) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (char)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (char) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (char)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (char) transferBuffer.getInt();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (char) transferBuffer.getDouble();
            case MatDataTypes.miUTF8:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (char) transferBuffer.get();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }
    /**
     * Reads data (number of bytes red is determined by <i>data type</i>)
     * from the stream to <code>double</code>.
     * 
     * @return - double
     * @throws IOException
     */
    public double readDouble() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (double)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (double) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (double)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (double) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (double)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (double) transferBuffer.getInt();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (double) transferBuffer.getDouble();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }

    public byte readByte() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (byte)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (byte) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (byte)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (byte) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (byte)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (byte) transferBuffer.getInt();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (byte) transferBuffer.getDouble();
            case MatDataTypes.miUTF8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (byte) transferBuffer.get();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }

    /**
     * Reads the data into a <code>{@link ByteBuffer}</code>. This method is
     * only supported for arrays with backing ByteBuffer (<code>{@link ByteStorageSupport}</code>).
     * 
     * @param dest
     *            the destination <code>{@link ByteBuffer}</code>
     * @param elements
     *            the number of elements to read into a buffer
     * @param storage
     *            the backing <code>{@link ByteStorageSupport}</code> that
     *            gives information how data should be interpreted
     * @return reference to the destination <code>{@link ByteBuffer}</code>
     * @throws IOException
     *             if buffer is under-fed, or another IO problem occurs
     */
    public ByteBuffer readToByteBuffer(ByteBuffer dest, int elements,
                    ByteStorageSupport<?> storage) throws IOException
    {
        
        int bytesAllocated = storage.getBytesAllocated();
        int size = elements * storage.getBytesAllocated();
        
        //direct buffer copy
//        if ( MatDataTypes.sizeOf(type) == bytesAllocated ) //&& st.order().equals(dest.order()) )
//        {
//            int bufMaxSize = 1024;
//            st.
//            int bufSize = Math.min(st.remaining(), bufMaxSize);
//            int bufPos = st.position();
//            
//            byte[] tmp = new byte[ bufSize ];
//            
//            while ( dest.remaining() > 0 )
//            {
//                int length = Math.min(dest.remaining(), tmp.length);
//                st.get( tmp, 0, length );
//                dest.put( tmp, 0, length );
//            }
//            st.position( bufPos + size );
//        }
//        else
//        {
            //because Matlab writes data not respectively to the declared
            //matrix type, the reading is not straight forward (as above)
            Class<?> clazz = storage.getStorageClazz();
            while ( dest.remaining() > 0 )
            {
                if ( clazz.equals( Double.class) )
                {
                    dest.putDouble( readDouble() );
                }
                else if ( clazz.equals( Byte.class) )
                {
                    dest.put( readByte() );
                }
                else if ( clazz.equals( Integer.class) )
                {
                    dest.putInt( readInt() );
                }
                else if ( clazz.equals( Long.class) )
                {
                    dest.putLong( readLong() );
                }
                else  if ( clazz.equals( Float.class) )
                {
                    dest.putFloat( readFloat() );
                }
                else if ( clazz.equals( Short.class) )
                {
                    dest.putShort( readShort() );
                }
                else
                {
                    throw new RuntimeException("Not supported buffer reader for " + clazz );
                }
            }
//        }
        dest.rewind();
        return dest;
    }

    private float readFloat() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (float)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (float) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (float)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (float) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (float)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (float) transferBuffer.getInt();
            case MatDataTypes.miSINGLE:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (float) transferBuffer.getFloat();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (float) transferBuffer.getDouble();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }
    private short readShort() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (short)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (short) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (short)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (short) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (short)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (short) transferBuffer.getInt();
            case MatDataTypes.miUINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (short) transferBuffer.getLong();
            case MatDataTypes.miINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (short) transferBuffer.getLong();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (short) transferBuffer.getDouble();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }
    private long readLong() throws IOException
    {
      transferBuffer.rewind();
      byte[] tmp = transferBuffer.array();
        switch ( type )
        {
            case MatDataTypes.miUINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (long)( transferBuffer.get() & 0xFF);
            case MatDataTypes.miINT8:
              ByteStreams.readFully(st, tmp, 0, 1);
                return (long) transferBuffer.get();
            case MatDataTypes.miUINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (long)( transferBuffer.getShort() & 0xFFFF);
            case MatDataTypes.miINT16:
              ByteStreams.readFully(st, tmp, 0, 2);
                return (long) transferBuffer.getShort();
            case MatDataTypes.miUINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (long)( transferBuffer.getInt() & 0xFFFFFFFF);
            case MatDataTypes.miINT32:
              ByteStreams.readFully(st, tmp, 0, 4);
                return (long) transferBuffer.getInt();
            case MatDataTypes.miUINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (long) transferBuffer.getLong();
            case MatDataTypes.miINT64:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (long) transferBuffer.getLong();
            case MatDataTypes.miDOUBLE:
              ByteStreams.readFully(st, tmp, 0, 8);
                return (long) transferBuffer.getDouble();
            default:
                throw new IllegalArgumentException("Unknown data type: " + type);
        }
    }

	public void skip(int padding) throws IOException 
	{
	  st.skip((long) padding);
//		st.position( st.position() + padding );
	}
    

}

